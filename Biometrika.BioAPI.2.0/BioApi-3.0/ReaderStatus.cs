﻿using System;
using System.Collections.Generic;

namespace BioAPI
{
    public class ReaderStatus
    {
        public bool IsOpen { get; set; }
        public ReaderFactory.ReaderResultCode ReaderResultCode { get; set; }
    }
}
