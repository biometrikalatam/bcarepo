﻿using System;

namespace BioAPI
{
    public class ReaderImage : ICloneable
    {
        public byte[] Image { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public object Clone()
        {
            ReaderImage newReader = (ReaderImage)this.MemberwiseClone();

            //newReader.Image = (byte[])this.Image.Clone();

            //newReader.Image = new Byte[this.Image.Length];
            //this.Image.CopyTo(newReader.Image, 0);

            return newReader;
        }
    }
}