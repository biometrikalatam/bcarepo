﻿using log4net;
using System;
using System.Diagnostics;

namespace BioAPI
{
    public abstract class AbstractReader : IDisposable
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(AbstractReader));

        public event ReaderFactory.CaptureCallbackDelegate OnCapturedEvent;

        protected int Finger { get; set; }

        public ReaderStatus ReaderStatus { get; set; }

        public ReaderInfo ReaderInfo { get; set; }

        public abstract void Capture(int finger);

        public abstract BioAPI.ReaderFactory.ReaderResultCode Status();

        public abstract void CloseReader();

        public abstract void CancelCapture();

        protected void OnCaptureMethod(IFingerCaptured fingerCaptured)
        {
            //CloseReader()
            if (OnCapturedEvent != null)
                OnCapturedEvent(fingerCaptured);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <param name="level"> Valores: 
        ///    -3 - FATAL
        ///    -2 - ERROR 
        ///    -1 - WARN 
        ///     0 - INFO 
        ///     1 - DEBUG 
        /// </param>
        protected void ELog(string p, int level)
        {
            //new EventLog() { Source = "FingerReader" }.WriteEntry(p, EventLogEntryType.Information);
            switch (level)
            {
                case -3: //FATAL
                    LOG.Fatal(p);
                    break;
                case -2: //ERROR
                    LOG.Error(p);
                    break;
                case -1: //WARN 
                    LOG.Warn(p);
                    break;
                case 0: //INFO
                    LOG.Info(p);
                    break;
                case 1: //DEBUG
                    LOG.Debug(p);
                    break;
            }

        }

        public bool IsEventHandlerCaptureRegistered()
        {
            if (OnCapturedEvent != null)
                foreach (Delegate existingHandler in OnCapturedEvent.GetInvocationList())
                    return true;
            return false;
        }

        public abstract void Dispose();
    }
}