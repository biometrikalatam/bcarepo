﻿namespace BioAPI
{
    public enum TemplateFormat
    {
        ANSITemplate,
        ISOTemplate,
        BASICRegistryTemplate,
        BASICVerifyTemplate
    }
}
