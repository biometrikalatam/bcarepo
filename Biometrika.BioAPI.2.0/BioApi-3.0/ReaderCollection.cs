﻿#if DIGITALPERSONA
using DPUruNet;
#endif
using System;
using System.Collections;
using System.Collections.Generic;

namespace BioAPI
{
    /// <summary>
    /// ReaderCollection
    /// </summary>
#if DIGITALPERSONA
    public class ReaderCollection : IEnumerable<Reader>, IEnumerable, IDisposable
    {
        private List<Reader> readerCollection = new List<Reader>();

        public int Count
        {
            get
            {
                return readerCollection.Count;
            }
        }

        public Reader this[int index]
        {
            get
            {
                return readerCollection[index];
            }
            set
            {
                readerCollection.Insert(index, value);
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerator GetEnumerator()
        {
            return readerCollection.GetEnumerator();
        }

        IEnumerator<Reader> IEnumerable<Reader>.GetEnumerator()
        {
            return readerCollection.GetEnumerator();
        }
    }
#endif
}
