﻿#if DIGITALPERSONA
using DPUruNet;
using log4net;
#endif
using log4net;
using System;
using System.Collections.Generic;

namespace BioAPI
{


    /// <summary>
    ///
    /// </summary>
    public class DigitalPersonaReader : AbstractReader
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(DigitalPersonaReader));

#if DIGITALPERSONA
        private DPUruNet.Reader readerDP;
#endif
        public DigitalPersonaReader()
        {
            ReaderInfo = new ReaderInfo();
#if DIGITALPERSONA
            LOG.Info("DigitalPersonaReader Create - BioApi DigitalPErsona Compiled!");
            var readerCollection = DPUruNet.ReaderCollection.GetReaders();
            if (readerCollection.Count == 1)
            {
                readerDP = readerCollection[0];

                var readerDescription = readerDP.Description;
                ReaderInfo.DeviceID = readerDescription.SerialNumber;
                ReaderInfo.SerialNumber = readerDescription.SerialNumber;
                ReaderInfo.ImageHeight = 512;
                ReaderInfo.ImageWidth = 512;
            }
#else
            LOG.Info("DigitalPersonaReader Create - BioApi Secugen Compiled!");
#endif
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="finger"></param>
        public override void Capture(int finger)
        {
            Finger = finger;
#if DIGITALPERSONA
            var resultCode = readerDP.Open(Constants.CapturePriority.DP_PRIORITY_COOPERATIVE);
            if (resultCode != Constants.ResultCode.DP_SUCCESS)
            {
                resultCode = readerDP.Open(Constants.CapturePriority.DP_PRIORITY_EXCLUSIVE);
            }
            if (resultCode == Constants.ResultCode.DP_SUCCESS)
            {
                readerDP.On_Captured += new DPUruNet.Reader.CaptureCallback(OnCaptureMethodDP);

                readerDP.GetStatus();


                resultCode = readerDP.CaptureAsync(
                                       DPUruNet.Constants.Formats.Fid.ANSI,
                                       DPUruNet.Constants.CaptureProcessing.DP_IMG_PROC_DEFAULT,
                                       readerDP.Capabilities.Resolutions[0]
                                       );

                if (resultCode != Constants.ResultCode.DP_SUCCESS)
                {
                    ReaderStatus.ReaderResultCode = ReaderFactory.ReaderResultCode.DEVICE_FAILURE;
                }
            }
            else
            {
                ReaderStatus.ReaderResultCode = ReaderFactory.ReaderResultCode.DEVICE_FAILURE;
            }
#endif
        }

#if DIGITALPERSONA
        /// <summary>
        /// 
        /// </summary>
        /// <param name="captureResult"></param>
        private void OnCaptureMethodDP(CaptureResult captureResult)
        {

            FingerCaptureProcessor.SetCapture(captureResult);
            OnCaptureMethod(FingerCaptureProcessor.GetFingerCaptured(Finger));
    }
#endif

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override BioAPI.ReaderFactory.ReaderResultCode Status()
        {

            var resultCode = ReaderFactory.ReaderResultCode.INVALID_DEVICE;
#if DIGITALPERSONA
            switch (readerDP.GetStatus())
            {
                case Constants.ResultCode.DP_DEVICE_BUSY:
                    resultCode = ReaderFactory.ReaderResultCode.DEVICE_BUSY;
                    break;
                case Constants.ResultCode.DP_DEVICE_FAILURE:
                    resultCode = ReaderFactory.ReaderResultCode.DEVICE_FAILURE;
                    break;
                case Constants.ResultCode.DP_ENROLLMENT_INVALID_SET:
                    break;
                case Constants.ResultCode.DP_ENROLLMENT_IN_PROGRESS:
                    break;
                case Constants.ResultCode.DP_ENROLLMENT_NOT_READY:
                    break;
                case Constants.ResultCode.DP_ENROLLMENT_NOT_STARTED:
                    break;
                case Constants.ResultCode.DP_FAILURE:
                    resultCode = ReaderFactory.ReaderResultCode.DEVICE_FAILURE;
                    break;
                case Constants.ResultCode.DP_INVALID_DEVICE:
                    resultCode = ReaderFactory.ReaderResultCode.DEVICE_FAILURE;
                    break;
                case Constants.ResultCode.DP_INVALID_FID:
                    break;
                case Constants.ResultCode.DP_INVALID_FMD:
                    break;
                case Constants.ResultCode.DP_INVALID_PARAMETER:
                    break;
                case Constants.ResultCode.DP_MORE_DATA:
                    break;
                case Constants.ResultCode.DP_NOT_IMPLEMENTED:
                    break;
                case Constants.ResultCode.DP_NO_DATA:
                    break;
                case Constants.ResultCode.DP_SUCCESS:
                    resultCode = ReaderFactory.ReaderResultCode.DEVICE_READY;
                    break;
                case Constants.ResultCode.DP_TOO_SMALL_AREA:
                    break;
                case Constants.ResultCode.DP_VERSION_INCOMPATIBILITY:
                    break;
                default:
                    break;
            }
#endif

            return resultCode;

        }

        public override void CloseReader()
        {
#if DIGITALPERSONA
            readerDP.CancelCapture();
            readerDP.Dispose();
#endif
        }

        public override void CancelCapture()
        {
            CloseReader();
        }

        public override void Dispose()
        {
#if DIGITALPERSONA
            readerDP = null;
#endif
        }
    }


}
