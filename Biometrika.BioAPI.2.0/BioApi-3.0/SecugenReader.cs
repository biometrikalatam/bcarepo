﻿#if SECUGEN
using SecuGen.FDxSDKPro.Windows;
#endif
using System;
using System.Text;

namespace BioAPI
{
    /// <summary>
    ///
    /// </summary>
    public class SecugenReader : AbstractReader
    {
#if SECUGEN
        private static SGFingerPrintManager readerSecugen ; //= null;
#endif

        public SecugenReader()
        {
            InitializeSecugenReader();
        }

#if SECUGEN
        public void SetReaderSecugen(SGFingerPrintManager readerSecugenp )
        {
            readerSecugen = readerSecugenp;
        }

        public SGFingerPrintManager ActualReader()
        {
            return readerSecugen;
        }
#endif

        public void InitializeSecugenReader()
        {
            ReaderInfo = new ReaderInfo();
            Int32 error;

            ELog(string.Format("SecugenReader -> ctor -> Init"), 0);

            try
            {
#if SECUGEN
                if (readerSecugen == null)
                    readerSecugen = new SGFingerPrintManager();

                SGFPMDeviceName device_name = SGFPMDeviceName.DEV_AUTO;
                int device_id = (Int32)(SGFPMPortAddr.USB_AUTO_DETECT);

                error = readerSecugen.Init(device_name);
                if (error != 0)
                    ELog(string.Format("Reader -> GetInitialStatus() -> Init : error {0} dn {1}", error, device_name), -3);

                error = readerSecugen.OpenDevice(device_id);
                if (error != 0)
                    ELog(string.Format("Reader -> GetInitialStatus() -> OpenDevice : error {0} di {1}", error, device_id), -3);

                SGFPMDeviceInfoParam pInfo = new SGFPMDeviceInfoParam();
                int iError = readerSecugen.GetDeviceInfo(pInfo);

                if (iError == (Int32)SGFPMError.ERROR_NONE)
                {
                    ReaderInfo.ImageWidth = pInfo.ImageWidth;
                    ReaderInfo.ImageHeight = pInfo.ImageHeight;

                    ReaderInfo.DeviceID = Convert.ToString(pInfo.DeviceID);
                    ReaderInfo.SerialNumber = new ASCIIEncoding().GetString(pInfo.DeviceSN);
                    ELog(string.Format("ReaderInfo.SerialNumber -> {0}", ReaderInfo.SerialNumber), 1);
                    FingerCaptureProcessor.InitializeSecugen(readerSecugen, ReaderInfo.ImageWidth, ReaderInfo.ImageHeight);
                    ELog("FingerCaptureProcessor.InitializeSecugen OK!", 1);
                }
#endif
            }
            catch (Exception e)
            {
                ELog(string.Format("SecugenReader -> ctor -> Init -> Error {0}", e.Message),-2);
            }
        }

#if SECUGEN
        public  void HardCapture (SGFingerPrintManager readerSecugenp , int finger )
        {
            if (readerSecugen== null)
                readerSecugen = readerSecugenp;
            
            Capture(finger);
        }
#endif

        public override void Capture(int finger)
        {
            try
            {
#if SECUGEN
                if (readerSecugen == null)
                    InitializeSecugenReader();

                ELog(string.Format("SecugenReader -> Capture-> f: {0}", finger), 0);
                Finger = finger;

                Byte[] image = new Byte[ReaderInfo.ImageWidth * ReaderInfo.ImageHeight];
                ELog(string.Format("SecugenReader -> Capture-> img: {0}", image.Length), 1);

                if (image.Length == 0)
                {
                    image = new Byte[120000];
                    InitializeSecugenReader();
                }
                    
                ELog(string.Format("SecugenReader -> Capture-> img: {0}", image.Length), 1);

                readerSecugen.GetImageEx(buffer: image, timeout: 5000, dispWnd: 0, quality: 70);

                FingerCaptureProcessor.SetCapture(image);

                ELog(string.Format("SecugenReader -> Capture-> oc: {0}", image.Length), 1);
                OnCaptureMethod(FingerCaptureProcessor.GetFingerCaptured(Finger));
#endif
            }
            catch ( Exception e)
            {
                ELog(string.Format("SecugenReader -> Capture-> Error: {0} - {1}", e.Message, e.StackTrace),-2);

            }
        }

        public override BioAPI.ReaderFactory.ReaderResultCode Status()
        {
            return BioAPI.ReaderFactory.ReaderResultCode.DEVICE_READY;
        }

        public override void CloseReader()
        {
#if SECUGEN
            if (readerSecugen!= null)
                readerSecugen.CloseDevice();
#endif
        }

        public override void CancelCapture()
        {
            CloseReader();
        }

        public override void Dispose()
        {
#if SECUGEN
            readerSecugen = null;
#endif
        }

       

    }
}