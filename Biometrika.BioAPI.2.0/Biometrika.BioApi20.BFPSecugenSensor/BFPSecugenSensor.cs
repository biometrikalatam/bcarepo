﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Bio.Core.Matcher.Constant;
using Biometrika.BioApi20.Interfaces;
using Biometrika.Standards;
using log4net;
using SecuGen.FDxSDKPro.Windows;

namespace Biometrika.BioApi20.BFPSecugenSensor
{
    public class BFPSecugenSensor : ISensor
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BFPSecugenSensor));

        private static SGFingerPrintManager _readerSecugen;

        public event CaptureCallbackDelegate OnCapturedEvent;
        public event ErrorCallbackDelegate OnErrorEvent;
        public event ConnectCallbackDelegate OnConnectEvent;
        public event DisconnectCallbackDelegate OnDisconnectEvent;
        public event TimeoutCallbackDelegate OnTimeoutEvent;

        public string Vendor { get; private set; }

        public int AuthenticationFactor { get; private set; }

        public object CurrentSensor { get; private set; }

        public string SerialSensor { get; private set; }

        public UnitIndicatorStatus IndicatorStatus { get; private set; }

        public int ImageWidth { get; private set; }

        public int ImageHeight { get; private set; }

        public string DeviceID { get; private set; }

        public int SensorType { get; private set; }

        public void Calibrate(int timeout)
        {
            
        }

        public int EnableAutoEvent(string serialSensor, bool enable, int hwnd)
        {
            int ret = 0;
            try
            {
                if (CurrentSensor == null)
                {
                    if (InstanceSensor(serialSensor) < 0)
                    {
                        //error.ErrorCode = -3;
                        //error.ErrorDescription = "BFPSecugenSensor Error [No pudo instanciar Sensor " + sensorSerial + "]";
                        return -3;
                    }
                }

                //if (!CurrentSensor.Equals(serialSensor))
                //{
                //    CurrentSensor = 
                //}

                ret = ((SGFingerPrintManager)CurrentSensor).EnableAutoOnEvent(enable, hwnd);
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BFPSecugenSensor.EnableAutoEvent - Exc Error: " + ex.Message);
            }
            return ret;
        }

        public List<Sample> Capture(int sensorType, int bodypart, string sensorSerial, int timeout, int captureType, 
                                    double qualityCapture, out Error error)
        {

            List<Sample> listSampleCaptured = new List<Sample>();
            Sample sample;
            error = new Error(0, null);
            LOG.Debug("BFPSecugenSensor.Capture IN...");
            try
            {
                CurrentSensor = null;
                //Chequeo que este abierto sensor sino abro
                if (CurrentSensor == null)
                {
                    if (InstanceSensor(sensorSerial) < 0)
                    {
                        error.ErrorCode = -3;
                        error.ErrorDescription = "BFPSecugenSensor Error [No pudo instanciar Sensor " + sensorSerial + "]";
                        return null;
                    }
                }

                byte[] image = new byte[ImageWidth * ImageHeight];
                //ELog(string.Format("SecugenReader -> Capture-> img: {0}", image.Length), 1);

                if (image.Length == 0)
                {
                    image = new Byte[120000];
                }

                //ELog(string.Format("SecugenReader -> Capture-> img: {0}", image.Length), 1);
                int qualityC = (int)qualityCapture;
                int iError = ((SGFingerPrintManager)CurrentSensor).GetImageEx(buffer: image, timeout: timeout, dispWnd: 0, quality: qualityC);
                LOG.Debug("BFPSecugenSensor.Capture GetImageEx Return = " + iError);

                if (iError != 0)
                {
                    error.ErrorCode = Biometrika.BioApi20.Error.IERR_TIMEOUT;
                    error.ErrorDescription = "BFPSecugenSensor.Capture Error [SerialSensor=" + SerialSensor + 
                                                " - Error = Devolucion Error en Captura = " + iError.ToString() + "]";
                    OnTimeoutEvent(error.ErrorCode, error.ErrorDescription, null);
                    return null;

                }
                else
                {
                    //FingerCaptureProcessor.SetCapture(image);
                    byte[] rawImage = image;
                    var img_qlty = 0;
                    //LOG.Debug("SetCapture IN...");
                    var response = ((SGFingerPrintManager)CurrentSensor).GetImageQuality(ImageWidth, ImageHeight, rawImage, ref img_qlty);
                    LOG.Debug("BFPSecugenSensor.Capture GetImageEx Return = " + response.ToString() + " - Quality = " + img_qlty.ToString());

                    //TODO CONTROL DE CALIDAD
                    if (img_qlty < qualityC)
                    {
                        error.ErrorCode = Biometrika.BioApi20.Error.IERR_BAD_QUALITY;
                        error.ErrorDescription = "BFPSecugenSensor.Capture Error [SerialSensor=" + SerialSensor +
                                                " - Imagen baja calidad Calidad / Umbral => "
                                                + img_qlty.ToString() + "/" + qualityC.ToString() + "]";
                        OnTimeoutEvent(error.ErrorCode, error.ErrorDescription, null);
                        return null;

                    }
                    else
                    {
                        byte[] birArray = rawImage;

                        //Armo lista de samples de salida de acuerdo a captureType
                        //      0 - All | 1 - Solo WSQ | 2 - Solo Minucias | 3 - Solo Image
                        if (captureType < 0 || captureType > 4)
                        {
                            captureType = 0; //All
                        }
                        //      0 - All | 1 - Solo WSQ
                        byte[] raw512x512 = null;
                        if (captureType == 0 || captureType == 1)
                        {
                            raw512x512 = Bio.Core.Imaging.ImageProcessor.FillRaw(rawImage, ImageWidth, ImageHeight, 512, 512);
                            Bio.Core.Wsq.Encoder.WsqEncoder enc = new Bio.Core.Wsq.Encoder.WsqEncoder();
#if DEBUG
                            //System.IO.File.WriteAllText("c:\\tmp\\raw_1_GGS_orig.txt", Convert.ToBase64String(rawImage));
                            //byte[] wsqOrig;
                            //enc.EncodeMemory(rawImage, 260, 300, out wsqOrig);
                            //System.IO.File.WriteAllText("c:\\tmp\\wsq_1_GGS_orig.txt", Convert.ToBase64String(wsqOrig));
#endif

                            sample = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                                                      Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW,
                                                      bodypart, (int)Biometrika.Standards.Purpose.Verify, 512, 512, raw512x512);
                            listSampleCaptured.Add(sample);

                            //BIR
                            Standards.DataFormat dataFormat = new Standards.DataFormat(10,0x02);
                            
                            byte[] wsq;
                            if (enc.EncodeMemory(raw512x512, 512, 512, out wsq))
                            {
                                sample = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                                                      Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ,
                                                      bodypart, (int)Biometrika.Standards.Purpose.Verify, 512, 512, wsq);
                                listSampleCaptured.Add(sample);
                            }
                            //BIR bir = new BIR(0,2,BiometricType.Finger,ProcessedLevel.Intermediate,Purpose.Verify,-1,
                            //                  new DataFormat((int)Owners.ISO,(int)BDBTypes.ISO_WSQ),wsq);
                            //byte[] bybir = bir.BIRToByteArray();
                            //System.IO.File.WriteAllBytes("c:\\std\\bir_wsq_secugen_512x512", bybir);

                        }

                        //      0 - All  | 2 - Solo Minucias
                        if (captureType == 0 || captureType == 2)
                        {
                            Error terror;
                            List<Sample> sout;
                            Sample sin = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                                                      Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW,
                                                      bodypart, (int)Biometrika.Standards.Purpose.Verify, ImageWidth, ImageHeight, rawImage);
                            if (Extract(sin, Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004, out sout, out terror) == 0)
                            {
                                foreach (Sample item in sout)
                                {
                                    listSampleCaptured.Add(item);
                                }
                            }
                            else
                            {
                                LOG.Debug("BFPSecugenSensor.Capture Error generando Minucias ANSI [" +
                                    (terror != null ? terror.ErrorCode + "-" + terror.ErrorDescription : "Error null") + "]");
                            }

                            sout = null;
                            terror = null;
                            if (Extract(sin, Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005, out sout, out terror) == 0)
                            {
                                foreach (Sample item in sout)
                                {
                                    listSampleCaptured.Add(item);
                                }
                            }
                            else
                            {
                                LOG.Debug("BFPSecugenSensor.Capture Error generando Minucias ISO [" +
                                    (terror != null ? terror.ErrorCode + "-" + terror.ErrorDescription : "Error null") + "]");
                            }

                            sout = null;
                            terror = null;
                            if (Extract(sin, Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_COMPACT_ISO_IEC_19794_2_2005, out sout, out terror) == 0)
                            {
                                foreach (Sample item in sout)
                                {
                                    listSampleCaptured.Add(item);
                                }
                            }
                            else
                            {
                                LOG.Debug("BFPSecugenSensor.Capture Error generando Minucias ISO Compact [" +
                                    (terror != null ? terror.ErrorCode + "-" + terror.ErrorDescription : "Error null") + "]");
                            }

                            sout = null;
                            terror = null;
                            if (Extract(sin, Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_SECUGEN, out sout, out terror) == 0)
                            {
                                foreach (Sample item in sout)
                                {
                                    listSampleCaptured.Add(item);
                                }
                            }
                            else
                            {
                                LOG.Debug("BFPSecugenSensor.Capture Error generando Minucias Secugen [" +
                                    (terror != null ? terror.ErrorCode + "-" + terror.ErrorDescription : "Error null") + "]");
                            }
                        }

                        //      0 - All | 3 - Solo Image
                        if (captureType == 0 || captureType == 3)
                        {
                            sample = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                                                      Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW,
                                                      bodypart, (int)Biometrika.Standards.Purpose.Verify, ImageWidth, ImageHeight, rawImage);
                            Sample sout;
                            Error terr;
                            //Toma imagen sin completar 512x512
                            if (GetImage(sample, 0, out sout, out terr) == 0)
                            {
                                listSampleCaptured.Add(sout);
                            }
                            sout = null;
                            terr = null;
                            //Toma imagen y completa 512x512 si hace falta
                            if (GetImage(sample, 1, out sout, out terr) == 0)
                            {
                                listSampleCaptured.Add(sout);
                            }
                        }

                        OnCapturedEvent(0, "Capture OK!", this.SerialSensor, listSampleCaptured);
                        //BIR bir = new BIR(null, 0, 0, outputFormat, false, false, BiometricType.Finger, subtype, null, null, null,
                        //     ProcessedLevel.Raw, null, 0, 0, null, null, null, null, null, null, null, birArray, null);

                        //return bir;
                    }
                }
            }
            catch (Exception ex)
            {
                error.ErrorCode = -1;
                error.ErrorDescription = "BFPSecugenSensor.Capture Error [SerialSensor=" + SerialSensor +
                                                " - Error = " + ex.Message + "]";
                listSampleCaptured = null;
                //OnErrorEvent(error.ErrorCode, error.ErrorDescription, null);
            }
            return listSampleCaptured;
        }

        /// <summary>
        /// Dado un RAW devuelto por el sensor, extrae minucias de los tipos que soporta
        /// </summary>
        /// <param name="sampleIn"></param>
        /// <param name="minutiaeTypeOut"></param>
        /// <param name="sampleOut"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public int Extract(object samplep, int minutiaeTypeOut, out List<Sample> sampleOut, out Error error)
        {
            int ret = 0;
            sampleOut = null;
            error = new Error(0, null);
            byte[] rawToImage;
            try
            {
                Sample sample = (Sample)samplep;
                //Chequeo que este abierto sensor sino abro
                if (CurrentSensor == null)
                {
                    if (InstanceSensor(null) < 0)
                    {
                        error.ErrorCode = -3;
                        error.ErrorDescription = "BFPSecugenSensor.Extract Error [No pudo instanciar Sensor]";
                        return -3;
                    }
                }

                //Chequeo consistencia entre entrada y salida
                if (sample == null || sample.Data == null ||
                   (sample.MinutiaeType != Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW &&
                   sample.MinutiaeType != Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ))
                {
                    LOG.Debug("BFPSecugenSensor.Extract sample.Data != null => " + (sample.Data != null).ToString());
                    LOG.Debug("BFPSecugenSensor.Extract sample.MinutiaeType = " + sample.MinutiaeType.ToString());
                    ret = Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA;
                    LOG.Error("BFPSecugenSensor.Extract Error - Sample o Data == null");
                    sampleOut = null;
                    error = new Error(Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA, "ProcessingFactory.Extract Error - Sample o Data == null");
                    return ret;
                }

                //Si es WSQ => Descomprimo para usear el RAW
                short sw, sh;
                int w, h;
                if (sample.MinutiaeType == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                {
                    LOG.Error("BFPSecugenSensor.Extract Decomprime WSQ...");
                    Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                    if (!dec.DecodeMemory((byte[])sample.Data, out sw, out sh, out rawToImage))
                    {
                        ret = Bio.Core.Constant.Errors.IERR_WSQ_DECOMPRESSING;
                        LOG.Error("BFPSecugenSensor.Extract Error - Descomprimiendo WSQ");
                        sampleOut = null;
                        error = new Error(Bio.Core.Constant.Errors.IERR_WSQ_DECOMPRESSING, "BFPSecugenSensor.Extract Error - Descomprimiendo WSQ");
                        return ret;
                    }
                    else
                    {
                        w = (int)sw;
                        h = (int)sh;
                        LOG.Debug("BFPSecugenSensor.Extract RAW Obtenido desde WSQ size => w x h = " + w.ToString() + " x " + h.ToString());
                    }
                }
                else
                {
                    rawToImage = (byte[])sample.Data;
                    w = sample.SampleWith;
                    h = sample.SampleHeight;
                    LOG.Debug("BFPSecugenSensor.Extract RAW Recibido size => w x h = " + w.ToString() + " x " + h.ToString());
                }

                int ierror;
                
                if (minutiaeTypeOut == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
                {
                    ierror = ((SGFingerPrintManager)CurrentSensor).SetTemplateFormat(SGFPMTemplateFormat.ANSI378);
                }
                else
                {
                    if (minutiaeTypeOut == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
                    {
                        ierror = ((SGFingerPrintManager)CurrentSensor).SetTemplateFormat(SGFPMTemplateFormat.ISO19794);
                    }
                    else if (minutiaeTypeOut == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_COMPACT_ISO_IEC_19794_2_2005)
                    {
                        ierror = ((SGFingerPrintManager)CurrentSensor).SetTemplateFormat(SGFPMTemplateFormat.ISO19794_COMPACT);
                    }
                    else
                    {
                        ierror = ((SGFingerPrintManager)CurrentSensor).SetTemplateFormat(SGFPMTemplateFormat.SG400);
                    }
                }

                var max_template_size = 800;
                
                ierror = ((SGFingerPrintManager)CurrentSensor).GetMaxTemplateSize(ref max_template_size);
                var templateGenerated = new byte[max_template_size]; // = TemplateInit(TemplateFormat.ANSITemplate);
                ierror = ((SGFingerPrintManager)CurrentSensor).CreateTemplate(FingerInfo(), rawToImage, templateGenerated);
                LOG.Debug("BFPSecugenSensor.Extract From Secugen ANSITEmplate = " + Convert.ToBase64String(templateGenerated));

                if (ierror != 0)
                {
                    ret = Bio.Core.Constant.Errors.IERR_EXTRACTING;
                    error = new Error(ret, "BFPSecugenSensor.Extract Error - Extrayendo Minucias [" + ierror.ToString() + "]");
                    LOG.Error("BFPSecugenSensor.Extract Error - Extrayendo Minucias [" + ierror.ToString() + "]");
                    sampleOut = null;
                } else
                {
                    sampleOut = new List<Sample>();
                    sampleOut.Add(new Sample(sample.AuthenticationFactor, minutiaeTypeOut, sample.BodyPart, (int)Biometrika.Standards.Purpose.Verify, 0, 0, templateGenerated));
                    sampleOut.Add(new Sample(sample.AuthenticationFactor, minutiaeTypeOut, sample.BodyPart, (int)Biometrika.Standards.Purpose.Enrol, 0, 0, templateGenerated));
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                error.ErrorCode = -1;
                error.ErrorDescription = "BFPSecugenSensor.Extract Error [" + ex.Message + "]";
                sampleOut = null;
            }
            return ret;
        }

        public int Close()
        {
            int ret = 0;
            try
            {
                if (CurrentSensor != null)
                {
                    ret = ((SGFingerPrintManager)CurrentSensor).CloseDevice();
                    CurrentSensor = null;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
            }
            return ret;
        }


        /// <summary>
        /// Retorna lista de seriales conectados de esta marca
        /// </summary>
        /// <param name="listSerial"></param>
        /// <returns></returns>
        public List<string> GetSensorConnected(List<string> listSerial)
        {
            List<string> listDepured = new List<string>();
            SGFingerPrintManager m_FPM;
            SGFPMDeviceList[] m_DevList;
            string auxSerial;
            try
            {
                AuthenticationFactor = Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;
                Vendor = "Secugen Corporation";
                SensorType = Bio.Core.Matcher.Constant.Devices.DEVICE_SECUGEN;
                m_FPM = new SGFingerPrintManager();
                
                // Enumerate Device
                int iError = m_FPM.EnumerateDevice();
                if (m_FPM.NumberOfDevice == 0)
                    return null;

                // Get enumeration info into SGFPMDeviceList
                m_DevList = new SGFPMDeviceList[m_FPM.NumberOfDevice];

                for (int i = 0; i < m_FPM.NumberOfDevice; i++)
                {
                    m_DevList[i] = new SGFPMDeviceList();
                    m_FPM.GetEnumDeviceInfo(i, m_DevList[i]);
                    auxSerial = new ASCIIEncoding().GetString(m_DevList[i].DevSN);
                    if (auxSerial.Contains("\0"))
                    {
                        auxSerial = auxSerial.Replace("\0", "").Trim();
                    }

                    if (MustInclude(auxSerial, listSerial))
                    {
                        listDepured.Add(auxSerial);
                    }
                }
            }
            catch (Exception)
            {
                listDepured = null;
            }
            return listDepured;
        }

        public int InstanceSensor(string serial)
        {
            List<string> listDepured = new List<string>();
            SGFingerPrintManager m_FPM;
            SGFPMDeviceList[] m_DevList;
            string auxSerial;
            SGFPMDeviceName device_name;
            int device_id;
            int ret = 0;
            try
            {
                m_FPM = new SGFingerPrintManager();
                // Enumerate Device
                int iError = m_FPM.EnumerateDevice();
                if (m_FPM.NumberOfDevice == 0)
                    return -1;

                // Get enumeration info into SGFPMDeviceList
                m_DevList = new SGFPMDeviceList[m_FPM.NumberOfDevice];

                for (int i = 0; i < m_FPM.NumberOfDevice; i++)
                {
                    m_DevList[i] = new SGFPMDeviceList();
                    m_FPM.GetEnumDeviceInfo(i, m_DevList[i]);
                    auxSerial = new ASCIIEncoding().GetString(m_DevList[i].DevSN);

                    device_name = m_DevList[i].DevName;
                    device_id = m_DevList[i].DevID;
                    
                    if (auxSerial.Contains("\0"))
                    {
                        auxSerial = auxSerial.Replace("\0", "").Trim();
                    }

                    if (auxSerial.Equals(serial) || string.IsNullOrEmpty(serial))
                    {
                        iError = m_FPM.Init(device_name);
                        if (iError != 0) return iError;
                        iError = m_FPM.OpenDevice(device_id);
                        if (iError == 0)
                        {
                            SGFPMDeviceInfoParam pInfo = new SGFPMDeviceInfoParam();
                            iError = m_FPM.GetDeviceInfo(pInfo);
                            ImageWidth = pInfo.ImageWidth;
                            ImageHeight = pInfo.ImageHeight;
                            SerialSensor = auxSerial;
                            CurrentSensor = m_FPM;
                        }
                        else return iError;
                    }
                }
            }
            catch (Exception)
            {
                ret = -1;
            }
            return ret;
        }

        public void SetIndicatorStatus(UnitIndicatorStatus indicatorStatus)
        {
            throw new NotImplementedException();
        }

        public void SetPowerMode(UnitPowerMode powerMode)
        {
            throw new NotImplementedException();
        }

        #region Continue Capture

        public int CaptureContinue(string sensorSerial, int qualityCapture, out Error error)
        {
            error = null;
            int res = 0;
            try
            {
                if (CurrentSensor == null)
                {
                    if (InstanceSensor(sensorSerial) < 0)
                    {
                        error.ErrorCode = -3;
                        error.ErrorDescription = "BFPSecugenSensor Error [No pudo instanciar Sensor " + sensorSerial + "]";
                        return -3;
                    }
                }

                byte[] raw = new Byte[ImageWidth * ImageHeight];

                //log.Info("Tamaño de imagen Width:" + this._SGInfo.ImageWidth);
                //log.Info("Tamaño de imagen Height:" + this._SGInfo.ImageHeight);
                //imagewidth = ImageWidth; //this._SGInfo.ImageWidth;
                //imageheight = ImageHeight; //this._SGInfo.ImageHeight;
                //res = this._SGPM.GetImageEx(this.raw, _timeout, 0, _quality);
                res = ((SGFingerPrintManager)CurrentSensor).GetImage(raw);// this._SGPM.GetImage(this.raw);
                if (res == 0)
                {
                    //log.Info("Imagen obtenida según calidad.");

                    //log.Info("Validamos la calidad de la imagen leída");
                    int _qualityex = 0;
                    res = ((SGFingerPrintManager)CurrentSensor).GetImageQuality(ImageWidth, ImageHeight, raw, ref _qualityex);

                    if (_qualityex >= qualityCapture)
                    {
                        //log.Info("Se cumple con la calidad requerida: q:" + _quality + ", calidad extendida:" + _qualityex);
                        res = 0;
                        List<Sample> listSampleCaptured = new List<Sample>();
                        Sample s = new Sample(2, 22, 1, 1, ImageWidth, ImageHeight, raw);
                        listSampleCaptured.Add(s);

                        byte[] raw512x512 = Bio.Core.Imaging.ImageProcessor.FillRaw(raw, ImageWidth, ImageHeight, 512, 512);
                        Bio.Core.Wsq.Encoder.WsqEncoder enc = new Bio.Core.Wsq.Encoder.WsqEncoder();

                        s = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                                                  Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW,
                                                  1, (int)Biometrika.Standards.Purpose.Verify, 512, 512, raw512x512);
                        listSampleCaptured.Add(s);

                        //BIR
                        Standards.DataFormat dataFormat = new Standards.DataFormat(10, 0x02);

                        byte[] wsq;
                        if (enc.EncodeMemory(raw512x512, 512, 512, out wsq))
                        {
                            s = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                                                  Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ,
                                                  1, (int)Biometrika.Standards.Purpose.Verify, 512, 512, wsq);
                            listSampleCaptured.Add(s);
                        }

                        s = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                                                      Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW,
                                                      1, (int)Biometrika.Standards.Purpose.Verify, ImageWidth, ImageHeight, raw);
                        Sample sout;
                        Error terr;
                        //Toma imagen sin completar 512x512
                        if (GetImage(s, 0, out sout, out terr) == 0)
                        {
                            listSampleCaptured.Add(sout);
                        }
                        sout = null;
                        terr = null;
                        //Toma imagen y completa 512x512 si hace falta
                        if (GetImage(s, 1, out sout, out terr) == 0)
                        {
                            listSampleCaptured.Add(sout);
                        }

                        OnCapturedEvent(0, "Capture OK!", this.SerialSensor, listSampleCaptured);

                    }
                    else
                    {
                        res = -2;
                        //log.Info("Calidad menor a la calidad requerida: q:" + _quality + ", calidad extendida:" + _qualityex);
                    }
                }
                else
                {
                    //log.Info("No se puede capturar la imagen");
                    res = -1;
                }

            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }
            return res;
        }

        /// <summary>
        /// Permite encender un lector
        /// </summary>
        public void On()
        {
            ((SGFingerPrintManager)CurrentSensor).SetLedOn(true);
        }
        /// <summary>
        /// Permite apagar el lector
        /// </summary>
        public void Off()
        {
            ((SGFingerPrintManager)CurrentSensor).SetLedOn(false);
        }


        #endregion Continue Capture

        #region Private

        private bool MustInclude(string auxSerial, List<string> listSerial)
        {
            bool ret = false;
            try
            {
                if (listSerial == null || listSerial.Count == 0) return true;

                foreach (string item in listSerial)
                {
                    if (item.Equals(auxSerial)) return true;
                }
            }
            catch (Exception)
            {
                ret = false;
            }
            return ret;
        }

        private static SGFPMFingerInfo FingerInfo()
        {
            return new SGFPMFingerInfo()
            {
                FingerNumber = (SGFPMFingerPosition)0,
                ImageQuality = (Int16)70,
                ImpressionType = (Int16)SGFPMImpressionType.IMPTYPE_LP,
                ViewNumber = 1
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sample"></param>
        /// <param name="typeImage">0-JPG desde RAW sin Completar | 1-JPG desde RAW 512x512</param>
        /// <param name="sampleout"></param>
        /// <returns></returns>
        internal int GetImage(Sample sample, int typeImage, out Sample sampleout, out Error error)
        {
            int ret = 0;
            sampleout = null;
            error = new Error(0, null);
            byte[] rawToImage;
            LOG.Debug("BFPSecugenSensor.GetImage IN...");
            try
            {
                LOG.Debug("BFPSecugenSensor.GetImage sample != null => " + (sample != null).ToString());
                if (sample == null || sample.Data == null ||
                   (sample.MinutiaeType != Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW &&
                   sample.MinutiaeType != Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ))
                {
                    LOG.Debug("BFPSecugenSensor.GetImage sample.Data != null => " + (sample.Data != null).ToString());
                    LOG.Debug("BFPSecugenSensor.GetImage sample.MinutiaeType = " + sample.MinutiaeType.ToString());
                    ret = Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA;
                    LOG.Error("BFPSecugenSensor.GetImage Error - Sample o Data == null");
                    sampleout = null;
                    error = new Error(Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA, "ProcessingFactory.GetImage Error - Sample o Data == null");
                    return ret;
                }

                //Si es WSQ => Descomprimo para usear el RAW
                short sw, sh;
                int w, h;
                if (sample.MinutiaeType == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                {
                    LOG.Error("BFPSecugenSensor.GetImage Decomprime WSQ...");
                    Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                    if (!dec.DecodeMemory((byte[])sample.Data, out sw, out sh, out rawToImage))
                    {
                        ret = Bio.Core.Constant.Errors.IERR_WSQ_DECOMPRESSING;
                        LOG.Error("BFPSecugenSensor.GetImage Error - Descomprimiendo WSQ");
                        sampleout = null;
                        error = new Error(Bio.Core.Constant.Errors.IERR_WSQ_DECOMPRESSING, "BFPSecugenSensor.GetImage Error - Descomprimiendo WSQ");
                        return ret;
                    }
                    else
                    {
                        w = (int)sw;
                        h = (int)sh;
                        LOG.Error("BFPSecugenSensor.GetImage RAW Obtenido desde WSQ size => w x h = " + w.ToString() + " x " + h.ToString());
                    }
                }
                else
                {
                    rawToImage = (byte[])sample.Data;
                    w = sample.SampleWith;
                    h = sample.SampleHeight;
                    LOG.Error("BFPSecugenSensor.GetImage RAW Recibido size => w x h = " + w.ToString() + " x " + h.ToString());
                }

                LOG.Debug("BFPSecugenSensor.GetImage typeImage = " + typeImage + " [" + (typeImage == 0 ? "Sin Completar" : "Completa 512x512") + "]");
                if (typeImage == 1)
                {
                    rawToImage = Bio.Core.Imaging.ImageProcessor.FillRaw((byte[])sample.Data, sample.SampleWith, sample.SampleHeight, 512, 512);
                    w = 512;
                    h = 512;
                }

                Bitmap bmpImage = Bio.Core.Imaging.ImageProcessor.RawToBitmap(rawToImage, w, h);
                byte[] bmpByte;
                if (bmpImage != null)
                {
                    //System.Drawing.Image img = (System.Drawing.Image)(((System.Drawing.Image)bmpImage).Clone());
                    //using (var ms = new MemoryStream())
                    //{
                    //    img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    //    bmpByte = ms.ToArray();
                    //}
                    sampleout = new Sample(sample.AuthenticationFactor, MinutiaeType.MINUTIAETYPE_JPG, sample.BodyPart, (int)Biometrika.Standards.Purpose.Verify, w, h, bmpImage); // bmpByte);
                }
                else
                {
                    LOG.Debug("BFPSecugenSensor.GetImage Error ImageProcessor.RawToBitmap generating JPG");
                    ret = Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA;
                    LOG.Error("BFPSecugenSensor.GetImage Error ImageProcessor.RawToBitmap generating JPG");
                    sampleout = null;
                    error = new Error(Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA,
                        "BFPSecugenSensor.GetImage Error ImageProcessor.RawToBitmap generating JPG");
                    return ret;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BFPSecugenSensor.GetImage Error " + ex.Message);
                sampleout = null;
                error = new Error(-1, "BFPSecugenSensor.GetImage Error = " + ex.Message);
            }
            LOG.Debug("BFPSecugenSensor.GetImage OUT!");
            return ret;
        }


        private string GetSerial(object currentSensor)
        {
            try
            {
                SGFPMDeviceInfoParam info = new SGFPMDeviceInfoParam();
                var ret = ((SGFingerPrintManager)currentSensor).GetDeviceInfo(info);

                if (ret == 0)
                {
                    var auxSerial = new ASCIIEncoding().GetString(info.DeviceSN);
                    if (auxSerial.Contains("\0"))
                    {
                        auxSerial = auxSerial.Replace("\0", "").Trim();
                    }
                    return auxSerial;
                }
            }
            catch (Exception ex)
            {

            }
            return "Serial";
        }


        #endregion Private
    }
}
