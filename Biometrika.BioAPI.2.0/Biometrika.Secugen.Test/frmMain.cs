﻿using SecuGen.FDxSDKPro.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

namespace Biometrika.Secugen.Test
{
    public partial class frmMain : Form
    {
        public delegate void CaptureCallbackDelegate(int errCode, string errMessage, string sensorId, List<object> samplesCaptured);
        public event CaptureCallbackDelegate OnCapturedEvent;
        private SGFingerPrintManager m_FPM;
        private SGFingerPrintManager m_FPM2;
        private SGFPMDeviceList[] m_DevList; // Used for EnumerateDevice

        bool _IdentifyRunning = true;


        public string SerialSensor { get; private set; }
        public int ImageWidth { get; private set; }
        public int ImageHeight { get; private set; }




        public frmMain()
        {
            InitializeComponent();
            m_FPM = new SGFingerPrintManager();
            m_FPM2 = new SGFingerPrintManager();

            this.OnCapturedEvent += OnCapturedEvent;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            //Enumerate
            Int32 iError;
            string enum_device;
            string auxSerial;

            //comboBoxDeviceName.Items.Clear();

            // Enumerate Device
            SGFPMDeviceName device_name;
            Int32 device_id;
            iError = m_FPM.EnumerateDevice();

            // Get enumeration info into SGFPMDeviceList
            m_DevList = new SGFPMDeviceList[m_FPM.NumberOfDevice];

            for (int i = 0; i < m_FPM.NumberOfDevice; i++)
            {
                m_DevList[i] = new SGFPMDeviceList();
                m_FPM.GetEnumDeviceInfo(i, m_DevList[i]);
                enum_device = m_DevList[i].DevName.ToString() + " : " + m_DevList[i].DevID;
                auxSerial = new ASCIIEncoding().GetString(m_DevList[i].DevSN);
                auxSerial = auxSerial.Replace("\0", "").Trim();
                //richTextBox1.Text += Environment.NewLine + "Lector : " + enum_device + " - " + auxSerial;

                if (iError == 0)
                {
                    SGFPMDeviceInfoParam pInfo = new SGFPMDeviceInfoParam();
                    iError = m_FPM.GetDeviceInfo(pInfo);
                    ImageWidth = pInfo.ImageWidth;
                    ImageHeight = pInfo.ImageHeight;
                    SerialSensor = auxSerial;
                    //CurrentSensor = m_FPM;
                }

                if (i == 0)
                {
                    device_name = m_DevList[i].DevName;
                    device_id = m_DevList[i].DevID;
                    iError = m_FPM.Init(device_name);
                    iError = m_FPM.OpenDevice(device_id);
                    m_FPM.SetLedOn(true);
                    m_FPM.EnableAutoOnEvent(true, (int)this.Handle);

                }
                else
                {
                    device_name = m_DevList[i].DevName;
                    device_id = m_DevList[i].DevID;
                    iError = m_FPM2.Init(device_name);
                    iError = m_FPM2.OpenDevice(device_id);
                    m_FPM2.SetLedOn(true);
                    m_FPM2.EnableAutoOnEvent(true, (int)this.Handle);
                }
            }
            richTextBox1.Text += Environment.NewLine + "Waiting...";
        }

        /// <summary>
        /// Evento del lector de secugen, que permite capturar una huella. Esto es necesario solo por como 
        /// trabaja Secugen, en el modo de AutoEvent. 
        /// </summary>
        /// <param name="message"></param>
        /// 

        bool _CAPTURE_PROCESSING = false;
        protected override void WndProc(ref Message message)
        {
            try
            {
                int res = 0;
                if (_IdentifyRunning == true)
                {
                    if (message.Msg == (int)SGFPMMessages.DEV_AUTOONEVENT)
                    {
                        if (message.WParam.ToInt32() == (Int32)SGFPMAutoOnEvent.FINGER_ON)
                        {

                            if (!_CAPTURE_PROCESSING && _IdentifyRunning)
                            {
                                _CAPTURE_PROCESSING = true;
                                CaptureContinue1("H456778990", 50);
                                _CAPTURE_PROCESSING = false;
                            }
                            
                            //Error err;
                            ////res = _SCUGEN_SENSOR.CaptureContinue("H54181019063", 50, out err);// bcrFpr.Capture();
                            //LOG.Debug("frmMain.WndProc - Entro a WndProc...");
                            //res = Program._BSP.CaptureContinue(Program._UOWKA._CONFIG.SerialSensor1, Program._UOWKA._CONFIG.QualityCapture, out err);// bcrFpr.Capture();
                            //if (!string.IsNullOrEmpty(Program._UOWKA._CONFIG.SerialSensor2) &&
                            //    Program._UOWKA._CONFIG.SerialSensor2.Length > 0 &&
                            //    !Program._UOWKA._CONFIG.SerialSensor2.Equals("NA"))
                            //{
                            //    res = Program._BSP.CaptureContinue(Program._UOWKA._CONFIG.SerialSensor2, Program._UOWKA._CONFIG.QualityCapture, out err);// bcrFpr.Capture();
                            //}
                        }
                        else if (message.WParam.ToInt32() == (Int32)SGFPMAutoOnEvent.FINGER_OFF)
                        {
                        }
                    }
                }
                base.WndProc(ref message);
            }
            catch (Exception)
            {

            }
        }

        public int CaptureContinue1(string sensorSerial, int qualityCapture) //, out Error error)
        {
            int[] res = new int[2] { -1, -1 };
            string auxSerial = "";
            //error = null;
            //int res = 0;
            try
            {
                //if (CurrentSensor == null)
                //{
                //    if (InstanceSensor(sensorSerial) < 0)
                //    {
                //        error.ErrorCode = -3;
                //        error.ErrorDescription = "BFPSecugenSensor Error [No pudo instanciar Sensor " + sensorSerial + "]";
                //        return -3;
                //    }
                //}

                byte[] raw = new Byte[ImageWidth * ImageHeight];
                byte[] raw2 = new Byte[ImageWidth * ImageHeight];

                SGFPMDeviceName device_name;
                Int32 device_id;
                Int32 iError;
                string enum_device;
                
                
                int res2 = -1;


                for (int i = 0; i < m_DevList.Length; i++)
                {
                    //m_DevList[i] = new SGFPMDeviceList();
                    //m_FPM.GetEnumDeviceInfo(i, m_DevList[i]);
                    //enum_device = m_DevList[i].DevName.ToString() + " : " + m_DevList[i].DevID;
                    auxSerial = new ASCIIEncoding().GetString(m_DevList[i].DevSN);
                    auxSerial = auxSerial.Replace("\0", "").Trim();

                    device_name = m_DevList[i].DevName;
                    device_id = m_DevList[i].DevID;
                    iError = m_FPM.Init(device_name);
                    iError = m_FPM.OpenDevice(device_id);
                    res[i] = m_FPM.GetImage(raw);// this._SGPM.GetImage(this.raw);
                    //m_FPM.SetLedOn(true);
                    //m_FPM.EnableAutoOnEvent(true, (int)this.Handle);

                    if (res[i] == 0)
                        break;
                } 


                if (res[0] == 0)
                {
                    byte[] by512x512 = Bio.Core.Imaging.ImageProcessor.Adjust(raw, 300, 400, 512, 512);
                    Bitmap bmapo5 = Bio.Core.Imaging.ImageProcessor.RawToBitmap(by512x512, 512, 512);
                    //bmapo5.Save(@"c:\tmp\aa\s1_Test_512_orig_img_" + (DateTime.Now.ToString("ddMMyyhhmmss")) + ".jpg", ImageFormat.Jpeg);
                    //bmapo5.Dispose();
                    picSample.Image = bmapo5;
                    Bitmap bmapo = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw, ImageWidth, ImageHeight);
                    //bmapo.Save(@"c:\tmp\aa\s1_Test_orig_img_" + (DateTime.Now.ToString("ddMMyyhhmmss")) + ".jpg", ImageFormat.Jpeg);
                    //bmapo.Dispose();
                    //log.Info("Imagen obtenida según calidad.");

                    //log.Info("Validamos la calidad de la imagen leída");
                    int _qualityex = 0;
                    res[0] = m_FPM.GetImageQuality(ImageWidth, ImageHeight, raw, ref _qualityex);

                    if (_qualityex >= qualityCapture)
                    {
                        
                    }
                    else
                    {
                        res[0] = -2;
                        //log.Info("Calidad menor a la calidad requerida: q:" + _quality + ", calidad extendida:" + _qualityex);
                    }
                    richTextBox1.Text += Environment.NewLine + "Evento del Sensor: " + auxSerial + " - [res:" + res[0] + "] - " +
                                                               " - Quality/Umbral: " + _qualityex + "/" + qualityCapture;
                }
                else
                {
                    //log.Info("No se puede capturar la imagen");
                    //richTextBox1.Text += Environment.NewLine + "Evento del Sensor: " + SerialSensor + " - Error Captura:" + res;
                    res[0] = -1;
                }

                if (res[1] == 0)
                {
                    byte[] by512x512 = Bio.Core.Imaging.ImageProcessor.Adjust(raw, 300, 400, 512, 512);
                    Bitmap bmapo5 = Bio.Core.Imaging.ImageProcessor.RawToBitmap(by512x512, 512, 512);
                    
                    //bmapo5.Save(@"c:\tmp\aa\s2_Test_512_orig_img_" + (DateTime.Now.ToString("ddMMyyhhmmss")) + ".jpg", ImageFormat.Jpeg);
                    //bmapo5.Dispose();
                    picSample.Image = bmapo5;

                    Bitmap bmapo = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw, ImageWidth, ImageHeight);
                    //bmapo.Save(@"c:\tmp\aa\s2_Test_orig_img_" + (DateTime.Now.ToString("ddMMyyhhmmss")) + ".jpg", ImageFormat.Jpeg);
                    //bmapo.Dispose();
                    //log.Info("Imagen obtenida según calidad.");

                    //log.Info("Validamos la calidad de la imagen leída");
                    int _qualityex = 0;
                    res[1] = m_FPM.GetImageQuality(ImageWidth, ImageHeight, raw, ref _qualityex);

                    if (_qualityex >= qualityCapture)
                    {
                        
                    }
                    else
                    {
                        res[1] = -2;
                        //log.Info("Calidad menor a la calidad requerida: q:" + _quality + ", calidad extendida:" + _qualityex);
                    }
                    richTextBox1.Text += Environment.NewLine + "Evento del Sensor: " + auxSerial + " - [res:" + res[1] + "] - " +
                                                               " - Quality/Umbral: " + _qualityex + "/" + qualityCapture;
                }
                else
                {
                    //log.Info("No se puede capturar la imagen");
                    //richTextBox1.Text += Environment.NewLine + "Evento del Sensor: " + SerialSensor + " - Error Captura:" + res;
                    res[1] = -1;
                }
                frmMain_Load(null, null);
                //_CAPTURE_PROCESSING = false;
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
            _IdentifyRunning = true;
            return res[0];
        }


        private void OnCaptureEvent(int errCode, string errMessage, string serialId, List<object> samplesCaptured)
        {
            richTextBox1.Text += Environment.NewLine + "Evento : " + serialId + " - " + errCode + " - " + errMessage;
            //LOG.Debug("frmMain.OnCaptureEvent - Entro a OnCaptureEvent...");
            //lock (OBJECT_TO_BLOCK)
            //{
            //    _IdentifyRunning = false;
            //    _SamplesCaptured = samplesCaptured;
            //    _PROCESA_FINGERSAMPLE = true;
            //    //Si hubo una lista de samples devueltas, entre las que está el JPG real que sale del lector
            //    //y el WSQ, entonces entro a procesar.
            //    if (_SamplesCaptured != null && _SamplesCaptured.Count > 0)
            //    {
            //        LOG.Debug("frmMain.OnCaptureEvent - Entro a BiometricHelper.Identify...");
            //        //Se realiza la identificación d ela huella capturada
            //        int ret = _BIOMETRIC_HELPER.Identify(_BIOMETRIC_HELPER.GetSample(_SamplesCaptured), out _ID_RECOGNIZED);
            //        LOG.Debug("frmMain.OnCaptureEvent - BiometricHelper.Identify ret = " + ret.ToString());
            //        //if (ret == 0 || ret == -52) // && !string.IsNullOrEmpty(_ID_RECOGNIZED))
            //        //{
            //        if (!string.IsNullOrEmpty(_LAST_ID_RECOGNIZED) ||
            //            (!string.IsNullOrEmpty(_ID_RECOGNIZED) && !_ID_RECOGNIZED.Equals(_LAST_ID_RECOGNIZED)))
            //        {
            //            _LAST_ID_RECOGNIZED = _ID_RECOGNIZED;
            //            _ClearLastIdRecognized = true;
            //            LOG.Debug("frmMain.OnCaptureEvent - ID Reconocido desde Huella => " +
            //                        (string.IsNullOrEmpty(_ID_RECOGNIZED) ? "NULL => No Reconocio!" : _ID_RECOGNIZED));
            //            _ID_DEVICE = serialId; //Program._UOWKA._CONFIG.SerialSensor1;
            //        }
            //        //Se muestra la huella capturada y reinicia el Sensor (prende ON)
            //        ProcessFingerSample();
            //        //Se procesa la huella, eso significa que se realiza chequeos de acceso si se identifico 
            //        //se acciona el acceso, etc. Si no se identifico, solo se muestra NO Reconocido!
            //        ProccessIdRecognized();
            //        //threadProcessSample = new Thread(ProccessThreadSample);
            //        //threadProcessSample.Name = "ProccessThreadSample";
            //        //threadProcessSample.Start();
            //        //}
            //        //else
            //        //{
            //        //    LOG.Debug("frmMain.OnCaptureEvent - No Identificada Huella!");
            //        //}

            //    }
            //}
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void btnInitContinueCapture_Click(object sender, EventArgs e)
        {

        }

        private void btnStopContinueCapture_Click(object sender, EventArgs e)
        {

        }

        private void btnStartContinueCapture_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void buttonCapture_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        

    }
}
