﻿namespace Biometrika.Secugen.Test
{
    partial class frmMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.txtPrefix = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.btnInitContinueCapture = new System.Windows.Forms.Button();
            this.btnStopContinueCapture = new System.Windows.Forms.Button();
            this.btnStartContinueCapture = new System.Windows.Forms.Button();
            this.chkSaveHDD = new System.Windows.Forms.CheckBox();
            this.lbSamples = new System.Windows.Forms.ListBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCalidad = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.chkRetBIR = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBP = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAF = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTimeout = new System.Windows.Forms.TextBox();
            this.chkOrigianal = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.buttonCapture = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.picSample = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).BeginInit();
            this.SuspendLayout();
            // 
            // txtPrefix
            // 
            this.txtPrefix.Location = new System.Drawing.Point(646, 371);
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(100, 20);
            this.txtPrefix.TabIndex = 99;
            this.txtPrefix.Text = "212844152_7_";
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(1045, 218);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(241, 29);
            this.button4.TabIndex = 98;
            this.button4.Text = "Set _CONTINUE";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // btnInitContinueCapture
            // 
            this.btnInitContinueCapture.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInitContinueCapture.Location = new System.Drawing.Point(1045, 404);
            this.btnInitContinueCapture.Name = "btnInitContinueCapture";
            this.btnInitContinueCapture.Size = new System.Drawing.Size(241, 29);
            this.btnInitContinueCapture.TabIndex = 97;
            this.btnInitContinueCapture.Text = "Init Continue Capture";
            this.btnInitContinueCapture.UseVisualStyleBackColor = true;
            this.btnInitContinueCapture.Visible = false;
            // 
            // btnStopContinueCapture
            // 
            this.btnStopContinueCapture.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStopContinueCapture.Location = new System.Drawing.Point(1045, 474);
            this.btnStopContinueCapture.Name = "btnStopContinueCapture";
            this.btnStopContinueCapture.Size = new System.Drawing.Size(241, 29);
            this.btnStopContinueCapture.TabIndex = 96;
            this.btnStopContinueCapture.Text = "Stop Continue Capture";
            this.btnStopContinueCapture.UseVisualStyleBackColor = true;
            this.btnStopContinueCapture.Visible = false;
            // 
            // btnStartContinueCapture
            // 
            this.btnStartContinueCapture.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartContinueCapture.Location = new System.Drawing.Point(1045, 439);
            this.btnStartContinueCapture.Name = "btnStartContinueCapture";
            this.btnStartContinueCapture.Size = new System.Drawing.Size(241, 29);
            this.btnStartContinueCapture.TabIndex = 95;
            this.btnStartContinueCapture.Text = "Start Continue Capture";
            this.btnStartContinueCapture.UseVisualStyleBackColor = true;
            this.btnStartContinueCapture.Visible = false;
            // 
            // chkSaveHDD
            // 
            this.chkSaveHDD.AutoSize = true;
            this.chkSaveHDD.Location = new System.Drawing.Point(523, 374);
            this.chkSaveHDD.Name = "chkSaveHDD";
            this.chkSaveHDD.Size = new System.Drawing.Size(78, 17);
            this.chkSaveHDD.TabIndex = 94;
            this.chkSaveHDD.Text = "Save HDD";
            this.chkSaveHDD.UseVisualStyleBackColor = true;
            // 
            // lbSamples
            // 
            this.lbSamples.FormattingEnabled = true;
            this.lbSamples.Location = new System.Drawing.Point(754, 399);
            this.lbSamples.Name = "lbSamples";
            this.lbSamples.Size = new System.Drawing.Size(275, 264);
            this.lbSamples.TabIndex = 93;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(563, 296);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(239, 13);
            this.label13.TabIndex = 92;
            this.label13.Text = "(Valor 0 a 100 para control de calidad de imagen)";
            // 
            // txtCalidad
            // 
            this.txtCalidad.Location = new System.Drawing.Point(457, 293);
            this.txtCalidad.Name = "txtCalidad";
            this.txtCalidad.Size = new System.Drawing.Size(100, 20);
            this.txtCalidad.TabIndex = 91;
            this.txtCalidad.Text = "70";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(373, 296);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 90;
            this.label12.Text = "Umbral Calidad";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(563, 274);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(253, 13);
            this.label11.TabIndex = 89;
            this.label11.Text = "(0-All | 1-Solo WSQ | 2-Solo Minucias | 3-Solo Image)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(563, 244);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(272, 13);
            this.label10.TabIndex = 88;
            this.label10.Text = "(Parte del cuerpo a capturar: 1-10 Dedos | 16-Cara, etc))";
            // 
            // chkRetBIR
            // 
            this.chkRetBIR.AutoSize = true;
            this.chkRetBIR.Location = new System.Drawing.Point(346, 342);
            this.chkRetBIR.Name = "chkRetBIR";
            this.chkRetBIR.Size = new System.Drawing.Size(162, 17);
            this.chkRetBIR.TabIndex = 87;
            this.chkRetBIR.Text = "Retorno BIR BioAPI Enabled";
            this.chkRetBIR.UseVisualStyleBackColor = true;
            this.chkRetBIR.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(563, 218);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(245, 13);
            this.label9.TabIndex = 86;
            this.label9.Text = "(Tipo de tecnologia a usar: 2-Fingerprint | 4-Facial))";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(563, 192);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(183, 13);
            this.label8.TabIndex = 85;
            this.label8.Text = "(Timeout de captura en milisegundos)";
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(457, 267);
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(100, 20);
            this.txtContent.TabIndex = 84;
            this.txtContent.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(407, 270);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 83;
            this.label7.Text = "Content";
            // 
            // txtBP
            // 
            this.txtBP.Location = new System.Drawing.Point(457, 241);
            this.txtBP.Name = "txtBP";
            this.txtBP.Size = new System.Drawing.Size(100, 20);
            this.txtBP.TabIndex = 82;
            this.txtBP.Text = "1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(401, 244);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 81;
            this.label6.Text = "BodyPart";
            // 
            // txtAF
            // 
            this.txtAF.Location = new System.Drawing.Point(457, 215);
            this.txtAF.Name = "txtAF";
            this.txtAF.Size = new System.Drawing.Size(100, 20);
            this.txtAF.TabIndex = 80;
            this.txtAF.Text = "2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(343, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 13);
            this.label5.TabIndex = 79;
            this.label5.Text = "Authentication Factor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(366, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 78;
            this.label4.Text = "Timeout Captura";
            // 
            // txtTimeout
            // 
            this.txtTimeout.Location = new System.Drawing.Point(457, 189);
            this.txtTimeout.Name = "txtTimeout";
            this.txtTimeout.Size = new System.Drawing.Size(100, 20);
            this.txtTimeout.TabIndex = 77;
            this.txtTimeout.Text = "5000";
            // 
            // chkOrigianal
            // 
            this.chkOrigianal.AutoSize = true;
            this.chkOrigianal.Location = new System.Drawing.Point(140, 166);
            this.chkOrigianal.Name = "chkOrigianal";
            this.chkOrigianal.Size = new System.Drawing.Size(123, 17);
            this.chkOrigianal.TabIndex = 76;
            this.chkOrigianal.Text = "Show Image Original";
            this.chkOrigianal.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(369, 87);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(149, 30);
            this.button3.TabIndex = 75;
            this.button3.Text = "UnLoad BPS";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // buttonCapture
            // 
            this.buttonCapture.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCapture.Location = new System.Drawing.Point(346, 365);
            this.buttonCapture.Name = "buttonCapture";
            this.buttonCapture.Size = new System.Drawing.Size(149, 29);
            this.buttonCapture.TabIndex = 74;
            this.buttonCapture.Text = "Capture";
            this.buttonCapture.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(204, 87);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(159, 30);
            this.button2.TabIndex = 73;
            this.button2.Text = "Load BPS";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(147, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 72;
            this.label3.Text = "Sensores";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(204, 133);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(800, 21);
            this.comboBox1.TabIndex = 71;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(150, 400);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(586, 269);
            this.richTextBox1.TabIndex = 70;
            this.richTextBox1.Text = "";
            // 
            // picSample
            // 
            this.picSample.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picSample.Location = new System.Drawing.Point(150, 189);
            this.picSample.Name = "picSample";
            this.picSample.Size = new System.Drawing.Size(187, 205);
            this.picSample.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSample.TabIndex = 69;
            this.picSample.TabStop = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1426, 757);
            this.Controls.Add(this.txtPrefix);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnInitContinueCapture);
            this.Controls.Add(this.btnStopContinueCapture);
            this.Controls.Add(this.btnStartContinueCapture);
            this.Controls.Add(this.chkSaveHDD);
            this.Controls.Add(this.lbSamples);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtCalidad);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.chkRetBIR);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtContent);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtBP);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtAF);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtTimeout);
            this.Controls.Add(this.chkOrigianal);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.buttonCapture);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.picSample);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Biometrika Secugen Test...";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPrefix;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnInitContinueCapture;
        private System.Windows.Forms.Button btnStopContinueCapture;
        private System.Windows.Forms.Button btnStartContinueCapture;
        private System.Windows.Forms.CheckBox chkSaveHDD;
        private System.Windows.Forms.ListBox lbSamples;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtCalidad;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkRetBIR;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAF;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTimeout;
        private System.Windows.Forms.CheckBox chkOrigianal;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button buttonCapture;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.PictureBox picSample;
    }
}

