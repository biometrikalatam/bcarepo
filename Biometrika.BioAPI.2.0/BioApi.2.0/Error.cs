﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BioApi20
{
    public class Error
    { 
        public int ErrorCode { get; set; }
        public string ErrorDescription { get; set; }

        public Error(int errcode, string errdesc)
        {
            ErrorCode = errcode;
            ErrorDescription = errdesc;
        }

        //1 - 100 - Generales
        static public int IERR_OK = 0;
        static public string SERR_OK = "Funcionamiento Correcto";

        static public int IERR_UNKNOWN = -1;
        static public string SERR_UNKNOWN = "Error Desconocido";

        static public int IERR_REINTENT = -2;
        static public string SERR_REINTENT = "Reintente";

        static public int IERR_LICENSE = -3;
        static public string SERR_LICENSE = "Licencia Erronea";

        static public int IERR_DATABASE = -4;
        static public string SERR_DATABASE = "Error de Base de Datos";

        static public int IERR_RUT_INVALID = -5;
        static public string SERR_RUT_INVALID = "RUT Invalido";

        static public int IERR_BAD_PARAMETER = -6;
        static public string SERR_BAD_PARAMETER = "Parametro Erroneo";

        static public int IERR_CONX_WS = -7;
        static public string SERR_CONX_WS = "Error en la conexón al web service";

        static public int IERR_NORMALIZING_DATA = -8;
        static public string SERR_NORMALIZING_DATA = "Error normalizando data";

        static public int IERR_SERIALIZING_DATA = -9;
        static public string SERR_SERIALIZING_DATA = "Error serializando data";

        static public int IERR_DESERIALIZING_DATA = -10;
        static public string SERR_DESERIALIZING_DATA = "Error deserializando data";

        static public int IERR_SAVING_TX = -11;
        static public string SERR_SAVING_TX = "Error grabando transaccion";

        static public int IERR_SAVING_VERFIED = -12;
        static public string SERR_SAVING_VERFIED = "Error grabando data verificada";

        static public int IERR_INCONSISTENT_ACTION = -13;
        static public string SERR_INCONSISTENT_ACTION = "Id Acción inconsistente para este servicio";

        static public int IERR_ACTION_NOT_SUPPORTED = -14;
        static public string SERR_ACTION_NOT_SUPPORTED = "Acción no soportada en esta version";

        static public int IERR_COMPANY_NOT_EXIST = -15;
        static public string SERR_COMPANY_NOT_EXIST = "Compañia no existente";

        //30
        static public int IERR_NO_LOCAL_DATA = -30;
        static public string SERR_NO_LOCAL_DATA = "No hay datos locales";

        static public int IERR_IDENTITY_NOT_FOUND = -31;
        static public string SERR_IDENTITY_NOT_FOUND = "Identidad no encontrada";

        static public int IERR_BIR_NOT_FOUND = -32;
        static public string SERR_BIR_NOT_FOUND = "No hay datos para realizar la comparacion";

        static public int IERR_IDENTITY_EXIST = -33;
        static public string SERR_IDENTITY_EXIST = "Ya existe la identidad";

        //40
        static public int IERR_WSQ_INVALID = -40;
        static public string SERR_WSQ_INVALID = "WSQ Invalido";

        static public int IERR_WSQ_LOW_QUALITY = -41;
        static public string SERR_WSQ_LOW_QUALITY = "WSQ de baja calidad";

        static public int IERR_WSQ_DECOMPRESSING = -42;
        static public string SERR_WSQ_DECOMPRESSING = "Descomprimiendo WSQ";

        static public int IERR_WSQ_COMPRESSING = -43;
        static public string SERR_WSQ_COMPRESSING = "Comprimiendo WSQ";

        //50
        static public int IERR_MATCH_VALIDATION = -50;
        static public string SERR_MATCH_VALIDATION = "Error en proceso de matching";

        static public int IERR_VERIFY = -51;
        static public string SERR_VERIFY = "Error en proceso de verificacion";

        static public int IERR_IDENTIFY = -52;
        static public string SERR_NO_IDENTIFY = "Error en proceso de identifiacion";

        static public int IERR_MATCHER_NOT_AVAILABLE = -53;
        static public string SERR_MATCHER_NOT_AVAILABLE = "No existe Matcher disponible";

        static public int IERR_EXTRACTING = -54;
        static public string SERR_EXTRACTING = "Error extrayendo minucias";

        static public int IERR_INVALID_TEMPLATE = -55;
        static public string SERR_INVALID_TEMPLATE = "Template invalido";

        static public int IERR_NULL_TEMPLATE = -56;
        static public string SERR_NULL_TEMPLATE = "Template nulo";

        static public int IERR_INVALID_TOKEN = -57;
        static public string SERR_INVALID_TOKEN = "Token invalido";

        static public int IERR_NULL_TOKEN = -58;
        static public string SERR_NULL_TOKEN = "Token nulo";

        static public int IERR_UNKNOWN_TOKEN = -59;
        static public string SERR_UNKNOWN_TOKEN = "Token desconocido";

        //70 - 79 - Connectors
        static public int IERR_CONNECTOR_NOT_CONFIGURED = -70;
        static public string SERR_CONNECTOR_NOT_CONFIGURED = "Conector no configurado correctamente";

        static public int IERR_CONNECTOR_NOT_AVAILABLE = -71;
        static public string SERR_CONNECTOR_NOT_AVAILABLE = "Conector no disponible";

        static public int IERR_CONNECTOR_REMOTE_ERROR = -72;
        static public string SERR_CONNECTOR_REMOTE_ERROR = "Error en la ejecución remota del web services";

        static public int IERR_CONNECTOR_MALFORMED_ANSWER = -73;
        static public string SERR_CONNECTOR_MALFORMED_ANSWER = "Respuesta de conector mal formada";

        //80 - 90 - PKI
        static public int IERR_CERTIFICATE_EXPIRED = -80;
        static public string SERR_CERTIFICATE_EXPIRED = "Certificado vencido";

        static public int IERR_CERTIFICATE_NOT_EQUAL = -81;
        static public string SERR_CERTIFICATE_NOT_EQUAL = "Certificado No Igual";

        //100 - 110 - SRCeI
        static public int IERR_USER_INVALID_IN_SRCeI = -100;
        static public string SERR_USER_INVALID_IN_SRCeI = "Usuario invalido en SRCeI";

        static public int IERR_CONX_WS_SRCeI = -101;
        static public string SERR_CONX_WS_SRCeI = "Error en la conexion al web service del SRCeI";

        static public int IERR_XML_INVALID_IN_SRCeI = -102;
        static public string SERR_XML_INVALID_IN_SRCeI = "XML del SRCeI erroneo";

        static public int IERR_NO_DATA_IN_SRCeI = -103;
        static public string SERR_NO_DATA_IN_SRCeI = "No existen datos en SRCeI";

        static public int IERR_IMAGE_BAD_QUALITY_IN_SRCeI = -104;
        static public string SERR_IMAGE_BAD_QUALITY_IN_SRCeI = "Imagen de mala calidad para SRCeI";

        //200
        static public int IERR_BAD_SERIALIZER_CONFIG_FILE = -201;
        static public string SERR_BAD_SERIALIZER_CONFIG_FILE = "El paso de parámetros no permitió realizar la serialización del objeto correctamente";

        static public int IERR_SELECT_SERIALIZER_CONFIG_FILE = -202;
        static public string SERR_SELECT_SERIALIZER_CONFIG_FILE = "No se han seleccionado correctamente los datos de configuración";

        static public int IERR_SAVE_CONFIG_FILE = -203;
        static public string SERR_SAVE_CONFIG_FILE = "El archivo de configuración fue modificado con éxito";

        //300
        static public int IERR_BAD_SERIALIZER_COMPANYS = -301;
        static public string SERR_BAD_SERIALIZER_COMPANYS = "Imposible serializar la lista de companias";


        //400
        static public int IERR_DUPLICATE_ORIGIN = -401;
        static public string SERR_DUPLICATE_ORIGIN = "No se puede modificar el origen porque crearía valores duplicados";

        static public int IERR_DELETE_ORIGIN = -402;
        static public string SERR_DELETE_ORIGIN = "No se puede eliminar el origen.";

        //500
        static public int IERR_CLIENT_NOT_AUTHORIZED = -501;
        static public string SERR_CLIENT_NOT_AUTHORIZED = "Cliente No Autorizado en la plataforma";

        //600
        static public int IERR_TX_NOT_EXIST = -601;
        static public string SERR_TX_NOT_EXIST = "Transacción no existe";
        static public int IERR_TX_CONSUMED = -602;
        static public string SERR_TX_CONSUMED = "Transacción consumida";

        //800 - BioAPI + Standards
        static public int IERR_TIMEOUT = -801;
        static public string SERR_TTIMEOUT = "TImeout en la operacion";
        static public int IERR_BAD_QUALITY = -802;
        static public string SERR_BAD_QUALITY = "Mala calidad de imágen";
        static public int IERR_NO_SENSOR_CONNECTED = -803;
        static public string SERR_NO_SENSOR_CONNECTED = "No hay sensores conectados";
        static public int IERR_NO_SENSOR_ENABLEDAUTOEVENT_CONNECTED = -804;
        static public string SERR_NO_SENSOR_ENABLEDAUTOEVENT_CONNECTED = "No pudo setear Auto Event el lector";


    }
}
