﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BioApi20
{

    public class ProcessingFactorySchema
    {
        /// <summary>
        /// Default en milisegundos para extractor
        /// </summary>
        public int TimeoutProcessing { get; set; }

        /// <summary>
        /// Parametros si necesita. Con formato:
        ///            param1=value1|param2=value2
        /// </summary>
        public string ParametersPF { get; set; }

        public List<ExtractorItem> listExtractors;
    }

    public class ExtractorItem
    {
        /// <summary>
        /// Default en milisegundos para extractor. Si es 0 rige el global
        /// </summary>
        public int TimeoutProcessing { get; set; }

        /// <summary>
        /// Tecnologia a utilizar para extraccion
        /// </summary>
        public int AuthenticationFactor { get; set; }

        /// <summary>
        /// Tipo d eminucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType { get; set; }

        /// <summary>
        /// Umbral de extracción considerada aceptable
        /// </summary>
        public double Threshold { get; set; }

        /// <summary>
        /// Path de la dll para instanciar
        /// </summary>
        public string PathExtractor { get; set; }

        /// <summary>
        /// Se envian parametros si el Extractor lo necesita, de la forma key1=value1|key2=value2|...|keyN=valueN
        /// </summary>
        public string Parameters { get; set; }

    }
}
