﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bio.Core.Matcher.Constant;
using Biometrika.Standards;

namespace Biometrika.BioApi20
{
    public class BSPSchema
    {
        public string BSPUUID { get; private set; }
        public string BSPDescription { get; private set; }
        public string Path { get; private set; }
        public string SpecVersion { get; private set; }
        public string ProductVersion { get; private set; }
        public string Vendor { get; private set; }
        public List<DataFormat> BSPSupportedFormats { get; private set; }
        public List<AuthenticationFactor> FactorsMask { get; private set; }
        public int DefaultVerifyTimeout { get; private set; }
        public int DefaultIdentifyTimeout { get; private set; }
        public int DefaultCaptureTimeout { get; private set; }
        public int DefaultEnrolTimeout { get; private set; }
        public int DefaultCalibrateTimeout { get; private set; }
        public byte[] HostingEndpointIRI { get; private set; }
        public List<DataFormat> BSPSupportedAlgorithms { get; private set; }

        //public List<UUID> BSPSupportedTransformOperations { get; private set; }
        //public UUID BSPAccessUUID { get; private set; }
        //public List<BSPSchemaOperations> Operations { get; private set; }
        //public List<BSPSchemaOptions> Options { get; private set; }
        //public int PayloadPolicy { get; private set; }
        //public int MaxPayloadSize { get; private set; }
        //public int MaxBSPDbSize { get; private set; }
        //public int MaxIdentify { get; private set; }
        //public int MaxNumEnrolInstances { get; private set; }
        /*
        public BSPSchema
           (string bspUUID, string bspDescription, string path, string specVersion, string productVersion, string vendor,
            List<DataFormat> bspSupportedFormats, List<AuthenticationFactor> factorsMask, List<BSPSchemaOperations> operations,
            List<BSPSchemaOptions> options, int payloadPolicy, int maxPayloadSize, int defaultVerifyTimeout, int defaultIdentifyTimeout,
            int defaultCaptureTimeout, int defaultEnrolTimeout, int defaultCalibrateTimeout, int maxBSPDbSize, int maxIdentify,
            int maxNumEnrolInstances, byte[] hostingEndpointIRI, UUID bspAccessUUID, List<DataFormat> bspSupportedAlgorithms,
            List<UUID> bspSupportedTransformOperations) */
        public BSPSchema
            (string bspUUID, string bspDescription, string path, string specVersion, string productVersion, string vendor,
             List<DataFormat> bspSupportedFormats, List<AuthenticationFactor> factorsMask, int payloadPolicy, int maxPayloadSize, 
             int defaultVerifyTimeout, int defaultIdentifyTimeout,
             int defaultCaptureTimeout, int defaultEnrolTimeout, int defaultCalibrateTimeout, int maxBSPDbSize, int maxIdentify,
             int maxNumEnrolInstances, byte[] hostingEndpointIRI, List<DataFormat> bspSupportedAlgorithms)
        {
            BSPUUID = bspUUID;
            BSPDescription = bspDescription;
            Path = path;
            SpecVersion = specVersion;
            ProductVersion = productVersion;
            Vendor = vendor;
            BSPSupportedFormats = bspSupportedFormats;
            FactorsMask = factorsMask;
            
            DefaultVerifyTimeout = defaultVerifyTimeout;
            DefaultIdentifyTimeout = defaultIdentifyTimeout;
            DefaultCaptureTimeout = defaultCaptureTimeout;
            DefaultEnrolTimeout = defaultEnrolTimeout;
            DefaultCalibrateTimeout = defaultCalibrateTimeout;
            HostingEndpointIRI = hostingEndpointIRI;
            //MaxBSPDbSize = maxBSPDbSize;
            //MaxIdentify = maxIdentify;
            //MaxNumEnrolInstances = maxNumEnrolInstances;
            //BSPAccessUUID = bspAccessUUID;
            //BSPSupportedAlgorithms = bspSupportedAlgorithms;
            //BSPSupportedTransformOperations = bspSupportedTransformOperations;
            //Operations = operations;
            //Options = options;
            //PayloadPolicy = payloadPolicy;
            //MaxPayloadSize = maxPayloadSize;
        }
    }
}
