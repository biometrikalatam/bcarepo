﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Biometrika.Standards;

namespace Biometrika.BioApi20
{
    public class BFPListElement
    {
        /// <summary>
        /// Informacion de BFPs configurados
        /// </summary>

        public int AuthenticationFactor { get; set; }
        public int MinutiaeType { get; set; }

        public UnitCategoryType TypeBFP { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }
        public string SerialSensor { get; set; }

        public BFPListElement(int af, int mt, UnitCategoryType typeBFP, string brand, string model, int imageWidth, int imageHeight, string serialSensor)
        {
            AuthenticationFactor = af;
            MinutiaeType = mt;
            TypeBFP = typeBFP;
            Brand = brand;
            Model = model;
            ImageWidth = imageWidth;
            ImageHeight = imageHeight;
            SerialSensor = serialSensor;
        }
    }
}
