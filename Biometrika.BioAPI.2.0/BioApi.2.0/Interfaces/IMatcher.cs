﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BioApi20.Interfaces
{
    public interface IMatcher : IDisposable
    {

        /// <summary>
        /// Tecnologia a utilizar para matching
        /// </summary>
        int AuthenticationFactor { get; set; }

        /// <summary>
        /// Tipo de minucia dentro de la tecnologia utilziada
        /// </summary>
        int MinutiaeType { get; set; }

        /// <summary>
        /// Umbral de verificación considerada aceptable
        /// </summary>
        double Threshold { get; set; }

        /// <summary>
        /// Dos posibles valores:
        ///     0 - FIRST   -> El primer matching positivo
        ///     1 - BEST    -> El mejor matching positivo
        /// </summary>
        int MatchingType { get; set; }

        /// <summary>
        /// Se envian parametros si el Matcher lo necesita, de la forma key1=value1|key2=value2|...|keyN=valueN
        /// </summary>
        string Parameters { get; set; }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// y nuevo, parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="sampleCurrent1">Sample 1 para verify</param>
        /// <param name="sampleCurrent2">Sample 1 para verify</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        /// <param name="score">Score retornado</param>
        /// <returns>
        ///     Retorna:
        ///     1.- Verificacion Positiva (No error)
        ///     2.- Verificacion Negativa (No error)
        ///     Menor a 0.- Codigo de error en la operacion
        /// </returns>
        int Verify(Sample sampleCurrent1, Sample sampleCurrent2, out float score);

        /// <summary>
        /// Creado para WA Kiosk para no tener que deserializar tod o el tiempo la lista de todas
        /// las personas par aidentificar
        /// </summary>
        /// <param name="templateCurrent">Template actual capturado</param>
        /// <param name="listTemplatePeople">Lista de templates contra que chequear</param>
        /// <param name="score">Score obtenido (el mejor de todos los de la lista)</param>
        /// <param name="samplesIdentified">samples identidficados si son varios</param>
        /// <returns></returns>
        int Identify(Sample sampleCurrent1, List<Sample> listSamples, out float score, out List<Sample> samplesIdentified);
    }
}
