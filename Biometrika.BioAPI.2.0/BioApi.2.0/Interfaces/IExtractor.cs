﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BioApi20.Interfaces
{
    public interface IExtractor : IDisposable
    {
        /// <summary>
        /// Tecnologia a utilizar para extraccion
        /// </summary>
        int AuthenticationFactor { get; set; }

        /// <summary>
        /// Tipo d eminucia dentro de la tecnologia utilziada
        /// </summary>
        int MinutiaeType { get; set; }

        /// <summary>
        /// Umbral de extracción considerada aceptable
        /// </summary>
        double Threshold { get; set; }

        /// <summary>
        /// Se envian parametros si el Extractor lo necesita, de la forma key1=value1|key2=value2|...|keyN=valueN
        /// </summary>
        string Parameters { get; set; }

         /// <summary>
        /// Idem anterior pero ingresa un ITemplate y entrega un ITemplate
        /// </summary>
        /// <param name="SampleIn">Sample base para extraccion</param>
        /// <param name="purpose">Determina si es template para 0-Verify | 2-Enroll </param>
        /// <param name="templateout">Sample generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        int Extract(Sample SampleIn, int purpose, out Sample sampleOut);

    }
}
