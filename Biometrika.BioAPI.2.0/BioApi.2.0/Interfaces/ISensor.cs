﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Biometrika.Standards;

namespace Biometrika.BioApi20.Interfaces
{
    public interface ISensor
    {
        string Vendor { get; }
        int SensorType { get; }   //Codigo de sensor para busquedas y seleccion rapida y filtros
        int AuthenticationFactor { get; }
        object CurrentSensor { get; }
        string SerialSensor { get; }
        UnitIndicatorStatus IndicatorStatus { get; }
        int ImageWidth { get; }
        int ImageHeight { get; }
        string DeviceID { get; }

        /* EVENTS */
        event CaptureCallbackDelegate OnCapturedEvent; //(int errCode, string errMessage, List<Sample> samplesCaptured);
        event ErrorCallbackDelegate OnErrorEvent; // (int errCode, string errMessage, ISensor sensor);
        event ConnectCallbackDelegate OnConnectEvent; // (int errCode, string errMessage, ISensor sensor);
        event DisconnectCallbackDelegate OnDisconnectEvent; // (int errCode, string errMessage, ISensor sensor);
        event TimeoutCallbackDelegate OnTimeoutEvent;  //(int errCode, string errMessage, ISensor sensor);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sensorType"></param>
        /// <param name="bodypart"></param>
        /// <param name="sensorSerial"></param>
        /// <param name="timeout"></param>
        /// <param name="captureType">0 - All | 1 - Solo WSQ | 2 - Solo Minucias | 3 - Solo Image</param>
        /// <param name="qualityCapture"></param>
        /// <param name="error"></param>
        /// <returns></returns>        
        List<Sample> Capture(int sensorType, int bodypart, string sensorSerial, int timeout, int captureType, double qualityCapture, out Error error);

        /// <summary>
        /// Dado un RAW devuelto por el sensor, extrae minucias de los tipos que soporta
        /// </summary>
        /// <param name="sampleIn"></param>
        /// <param name="minutiaeTypeOut"></param>
        /// <param name="sampleOut"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        int Extract(object sampleIn, int minutiaeTypeOut, out List<Sample> sampleOut, out Error error);

        /// <summary>
        /// Performs a calibration of the attached
        /// sensor BioAPI Unit if that sensor unit supports it.
        /// </summary>
        /// <param name="timeout">
        /// an integer specifying the timeout value (in milliseconds) for the operation. If this timeout is reached an
        /// exception is thrown. This value can be any positive number. A -1 value means the BSPs default timeout value
        /// will be used. 
        /// </param>
        void Calibrate(int timeout);

        void SetIndicatorStatus(UnitIndicatorStatus indicatorStatus);

        void SetPowerMode(UnitPowerMode powerMode);

        /// <summary>
        /// Dado lista de seriales que quiero configurar, revisa con todos los conectados, y devuelve lista depurada.
        /// </summary>
        /// <param name="listSerial"></param>
        /// <returns></returns>
        List<string> GetSensorConnected(List<string> listSerial);

        /// <summary>
        /// Dado un serial, se instancia en CurrentSensor una instancia de dicho sensor.
        /// </summary>
        /// <param name="serial"></param>
        /// <returns></returns>
        int InstanceSensor(string serial);

        /// <summary>
        /// Cierra el sensor y lbera recursos
        /// </summary>
        /// <returns></returns>
        int Close();

        /// <summary>
        /// Habilita el sensor para captura constante con eventos, para situaciones de control de acceso. 
        /// Se retorna el evento de captura cuando se produce el evento de captura
        /// </summary>
        /// <param name="enable"></param>
        /// <param name="hwnd"></param>
        /// <returns></returns>
        int EnableAutoEvent(string serialSensor, bool enable, int hwnd);

        /// <summary>
        /// Habilita la captura continua. Para uso de kioskos con toma continua.
        /// </summary>
        /// <param name="sensorSerial"></param>
        /// <param name="qualityCapture"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        int CaptureContinue(string sensorSerial, int qualityCapture, out Error error);

        /// <summary>
        /// Permite prender led lector
        /// </summary>
        void On();

        /// <summary>
        /// Permite apagar el lector
        /// </summary>
        void Off();

        //UnitSchema UnitSchema { get; }
        //byte[] ACBioInstance { get; }
        //byte[] AuxiliaryData { get; }
        /// <summary>
        /// Captures samples for the purpose specified, and the BSP returns either an "intermediate" type BIR (if the
        /// Processing.Process() method needs to be called), or a "processed" BIR (if not). The purpose is recorded in
        /// the header of the captured BIR. If RequestAuditData option is specified a BIR of type "raw" may be returned
        /// in CaptureResult. By default, the BSP is responsible for providing the user interface associated with the
        /// capture operation. The application may, however, request control of the GUI "look-and-feel" by providing a
        /// GUI callback vis the Session.SetGUIObservers() method. Capture serializes use of the sensor device. If two
        /// or more biometric applications are racing for the sensor, the losers will wait until the operation completes
        /// or the timeout expires. This serialization takes place in all functions that capture data. The BSP is
        /// responsible for serializing. It may do this by either throwning an exception to indicate that device is
        /// busy or by queuing requests.
        /// </summary>
        /// <param name="purpose">
        /// indicates the purpose of the biometric data capture. 
        /// </param>
        /// <param name="subtype">
        /// specifies which subtype (e.g., left/right eye) to capture. null indicates that the value is not provided.
        /// </param>
        /// <param name="outputFormat">
        /// specifies which BDB format to use for the returned processed BIR, if the BSP supports more than one format.
        /// null indicates that the BSP is to select the format. 
        /// </param>
        /// <param name="timeout">
        /// an integer specifying the timeout value (in milliseconds) for the operation. If this timeout is reached an
        /// exception is thrown. This value can be any positive number. A -1 value means the BSPs default timeout value
        /// will be used. 
        /// </param>
        /// <param name="options">
        /// requests additional output such as audit data. 
        /// </param>
        /// <returns>
        /// the CaptureResult object that represents the result of capture operation.
        /// </returns>
        //BIR Capture(List<Purpose> purpose, BiometricSubtype subtype, DataFormat outputFormat, int timeout, List<ResultOptions> options);



    }
}
