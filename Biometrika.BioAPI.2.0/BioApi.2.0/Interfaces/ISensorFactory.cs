﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BioApi20.Interfaces
{
    public interface ISensorFactory 
    {
        /* EVENTS */
        event CaptureCallbackDelegate OnCapturedEvent; //(int errCode, string errMessage, List<Sample> samplesCaptured);
        event ErrorCallbackDelegate OnErrorEvent; // (int errCode, string errMessage, ISensor sensor);
        event ConnectCallbackDelegate OnConnectEvent; // (int errCode, string errMessage, ISensor sensor);
        event DisconnectCallbackDelegate OnDisconnectEvent; // (int errCode, string errMessage, ISensor sensor);
        event TimeoutCallbackDelegate OnTimeoutEvent;  //(int errCode, string errMessage, ISensor sensor);

        /// <summary>
        /// Configraucion para levantar lo correcto
        /// </summary>
        SensorFactorySchema Schema { get; set; }

        /// <summary>
        /// USado para hacer las operaciones 
        /// </summary>
        ISensor currentSensor { get; set; }

        /// <summary>
        /// Lista se sensores que se cargan desde la configuración
        /// </summary>
        IList<ISensor> listSensor { get; set; }

        /// <summary>
        /// HashTable para acceso directo x Serial
        /// </summary>
        System.Collections.Hashtable htSensor { get; set; }

        /// <summary>
        /// Toma el primer lector de la lista
        /// </summary>
        /// <returns></returns>
        ISensor GetSensor();

        /// <summary>
        /// Toma el primer lector de la lista de esa marca
        /// </summary>
        /// <param name="sensorBrand"></param>
        /// <returns></returns>
        ISensor GetSensor(int sensorBrand);

        /// <summary>
        /// Toma el lector con ese serial si existe
        /// </summary>
        /// <param name="serial"></param>
        /// <returns></returns>
        ISensor GetSensor(string serial);

        /// <summary>
        /// Levanta el schema y carga las listas para posterior procesos
        /// </summary>
        /// <returns></returns>
        int Initialization();

        /// <summary>
        /// Cierra todos los sensores y libera recursos
        /// </summary>
        /// <returns></returns>
        int Close();

        /// <summary>
        /// Captura de acuerdo a lo indicado en parametros
        /// </summary>
        /// <param name="SensorType">Tipo de device, </param>
        /// <param name="bodypart">id finger o parte del cuerpo </param>
        /// <param name="SensorSerial">Si serial es != null => se toma desde ese serial si existe</param>
        /// <param name="timeout">Tempo en milisegundos para la captura sino manda evento de timeout. 
        /// Si va 0 se fija en el default del BSP</param>
        /// <param name="CaptureType">Valores Posibles:
        ///     0 - Todos
        ///     1 - Solo WSQ (Para Fingerprint)
        ///     2 - Solo Minucias
        ///     3 - Solo JPG (Para Facial)
        /// Estos se devuelven planos o como BIR si la configuracion de BCP es BioApi20Enabled = true
        /// </param>
        /// <returns></returns>
        List<Sample> Capture(int sensorType, int bodypart, string sensorSerial, int timeout, int captureType, int qualityCapture, out Error error);

        /// <summary>
        /// Habilita el sensor para captura constante con eventos, para situaciones de control de acceso. 
        /// Se retorna el evento de captura cuando se produce el evento de captura
        /// </summary>
        /// <param name="enable"></param>
        /// <param name="hwnd"></param>
        /// <returns></returns>
        int EnableAutoEvent(string sensorSerial, bool enable, int hwnd, out Error error);

        /// <summary>
        /// Habilita la captura continua. Para uso de kioskos con toma continua.
        /// </summary>
        /// <param name="sensorSerial"></param>
        /// <param name="qualityCapture"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        int CaptureContinue(string sensorSerial, int qualityCapture, out Error error);

        /// <summary>
        /// Permite prender led lector
        /// </summary>
        void On(string sensorSerial);

        /// <summary>
        /// Permite apagar el lector
        /// </summary>
        void Off(string sensorSerial);
    }
}
