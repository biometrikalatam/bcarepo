﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Bio.Core.Matcher.Constant
{
    /// <summary>
    /// Acciones posibles
    ///      1-Verify
    ///      2-Identify
    ///      3-Enroll
    ///      4-Get
    ///      5-Modify
    ///      6-Delete
    ///      7-DeleteBir
    /// </summary>
    static public class Action
    {
        static public int ACTION_ANY = -2;
        static public string SACTION_ANY = "Cualquiera";
        static public int ACTION_All = -1;
        static public string SACTION_All = "Todos";
        static public int ACTION_NONE = 0;
        static public string SACTION_NONE = "Ninguno";

        static public int ACTION_VERIFY = 1;
        static public string SACTION_VERIFY = "Verify";

        static public int ACTION_IDENTIFY = 2;
        static public string SACTION_IDENTIFY = "Identify";

        static public int ACTION_ENROLL = 3;
        static public string SACTION_ENROLL = "Enroll";
        static public int ACTION_GET = 4;
        static public string SACTION_GET = "Get";
        static public int ACTION_MODIFY = 5;
        static public string SACTION_MODIFY = "Modify";
        static public int ACTION_DELETE = 6;
        static public string SACTION_DELETE = "Delete";
        static public int ACTION_ADDBIR = 7;
        static public string SACTION_ADDBIR = "Add BIR";
        static public int ACTION_DELETEBIR = 8;
        static public string SACTION_DELETEBIR = "Delete BIR";

        static public int ACTION_VERIFYANDGET = 9;
        static public string SACTION_VERIFYANDGET = "Verify y Get";

        static public int ACTION_GETDATA = 10;
        static public string SACTION_GETDATA = "Get Solo Datos";
        static public int ACTION_GETPHOTO = 11;
        static public string SACTION_GETPHOTO = "Get Solo Foto";
        static public int ACTION_GETSIGNATURE = 12;
        static public string SACTION_GETSIGNATURE = "Get Solo Firma";

        static public int ACTION_ENROLLORADDBIR = 14;
        static public string SACTION_ENROLLORADDBIR = "Enroll o Add BIR";

        static public int ACTION_GETACTIONS = 20;
        static public string SACTION_GETACTIONS = "Get Actions";
        static public int ACTION_GETORIGINS = 21;
        static public string SACTION_GETORIGINS = "Get Origin";
        static public int ACTION_GETMATCHESRAVAILABLES = 22;
        static public string SACTION_GETMATCHESRAVAILABLES = "Get Matchers Availables";
        static public int ACTION_GETCONNECTORSAVAILABLES = 23;
        static public string SACTION_GETCONNECTORSAVAILABLES = "Get Connectors Availables";
        static public int ACTION_GETAUTHENTICATIONFACTORSAVAILABLES = 24;
        static public string SACTION_GETAUTHENTICATIONFACTORSAVAILABLES = "Get Authentication Factor Availables";
        static public int ACTION_GETMINUTIAETYPESAVAILABLES = 25;
        static public string SACTION_GETMINUTIAETYPESAVAILABLES = "Get Minutiae Types Availables";
        static public int ACTION_GETBODYPARTENROLLED = 26;
        static public string SACTION_GETBODYPARTENROLLED = "Get BodyPArt Enroled";

        //static public int ACTION_GETCOMPANY = 26;
        //static public string SACTION_GETCOMPANY = "Get Company";

        static public int ACTION_VERIFY_BIOSIGNATURE = 40;
        static public string SACTION_VERIFY_BIOSIGNATURE = "Verify BioSignature";
        

        static public int ACTION_SYNC_GETBIRS = 100;
        static public string SACTION_SYNC_GETBIRS = "Get Birs For Sync";
        static public int ACTION_SYNC_ADDBIRS = 101;
        static public string SACTION_SYNC_ADDBIRS = "Add Birs For Sync";

        //Added 01-05 - Actions de Proxy 
        static public int ACTION_PROXY_GET = 200;
        static public string SACTION_PROXY_GET = "Get Proxy All";
        static public int ACTION_PROXY_CONSUME_TX = 201;
        static public string SACTION_PROXY_CONSUME_TX = "Consume Proxy Tx";  //Anula para no poder reutilizarla en otro proceso de verificación
        static public int ACTION_PROXY_GET_JUSTTXDATA = 202;
        static public string SACTION_PROXY_GET_JUSTTXDATA = "Get Proxy Just Tx Data";
        static public int ACTION_PROXY_GET_JUSTABS = 203;
        static public string SACTION_PROXY_GET_JUSTABS = "Get Proxy Just ABS";
        static public int ACTION_PROXY_GET_TXDATAABS = 204;
        static public string SACTION_PROXY_GET_TXDATAABS = "Get Proxy Tx Data And ABS";

        //Added 16-02-2018 Actions de Plugin Process 
        static public int ACTION_PLUGIN_ISOTOISOCOMPACT = 1000;
        static public string SACTION_PLUGIN_ISOTOISOCOMPACT = "Convert ISO to ISOCompact";


        static public string GetName(int af)
        {
            switch (af)
            {
                case -2:
                    return SACTION_ANY;
                case -1:
                    return SACTION_All;
                case 0:
                    return SACTION_NONE;
                case 1:
                    return SACTION_VERIFY;
                case 2:
                    return SACTION_IDENTIFY;
                case 3:
                    return SACTION_ENROLL;
                case 4:
                    return SACTION_GET;
                case 5:
                    return SACTION_MODIFY;
                case 6:
                    return SACTION_DELETE;
                case 7:
                    return SACTION_ADDBIR;
                case 8:
                    return SACTION_DELETEBIR;
                case 9:
                    return SACTION_VERIFYANDGET;
                case 10:
                    return SACTION_GETDATA;
                case 11:
                    return SACTION_GETPHOTO;
                case 12:
                    return SACTION_GETSIGNATURE;
                case 20:
                    return SACTION_GETACTIONS;
                case 21:
                    return SACTION_GETORIGINS;
                case 22:
                    return SACTION_GETMATCHESRAVAILABLES;
                case 23:
                    return SACTION_GETCONNECTORSAVAILABLES;
                case 24:
                    return SACTION_GETAUTHENTICATIONFACTORSAVAILABLES;
                case 25: 
                    return SACTION_GETMINUTIAETYPESAVAILABLES;
                case 26:
                    return SACTION_GETBODYPARTENROLLED;
                case 40:
                    return SACTION_VERIFY_BIOSIGNATURE;
                case 100:
                    return SACTION_SYNC_GETBIRS;
                case 101:
                    return SACTION_SYNC_ADDBIRS;

                case 200:
                    return SACTION_PROXY_GET;
                case 201:
                    return SACTION_PROXY_CONSUME_TX;
                case 202:
                    return SACTION_PROXY_GET_JUSTTXDATA;
                case 203:
                    return SACTION_PROXY_GET_JUSTABS;
                case 204:
                    return SACTION_PROXY_GET_TXDATAABS;

                case 1000:
                    return SACTION_PLUGIN_ISOTOISOCOMPACT;

                default:
                    return "Action no documentada";
            }
        }

        /// <summary>
        /// Retorna un hashtable con acciones, con clave = codigo y 
        /// Value = descripcion
        /// </summary>
        /// <returns>Hastable con coleccion</returns>
        static public Hashtable GetActions()
        {
            /*
                {ACTION_ANY, SACTION_ANY},
                               {ACTION_All, SACTION_All},
                               {ACTION_NONE, SACTION_NONE},
                               
             */
            Hashtable ht = new Hashtable
                           {
                               {ACTION_VERIFY, SACTION_VERIFY},
                               {ACTION_IDENTIFY, SACTION_IDENTIFY},
                               {ACTION_ENROLL, SACTION_ENROLL},
                               {ACTION_GET, SACTION_GET},
                               {ACTION_MODIFY, SACTION_MODIFY},
                               {ACTION_DELETE, SACTION_DELETE},
                               {ACTION_ADDBIR, SACTION_ADDBIR},
                               {ACTION_DELETEBIR, SACTION_DELETEBIR},
                               {ACTION_VERIFYANDGET, SACTION_VERIFYANDGET},
                               {ACTION_GETDATA, SACTION_GETDATA},
                               {ACTION_GETPHOTO, SACTION_GETPHOTO},
                               {ACTION_GETSIGNATURE, SACTION_GETSIGNATURE},
                               {ACTION_GETACTIONS, SACTION_GETACTIONS},
                               {ACTION_GETORIGINS, SACTION_GETORIGINS},
                               {ACTION_GETMATCHESRAVAILABLES, SACTION_GETMATCHESRAVAILABLES},
                               {ACTION_GETCONNECTORSAVAILABLES, SACTION_GETCONNECTORSAVAILABLES},
                               {ACTION_GETAUTHENTICATIONFACTORSAVAILABLES,SACTION_GETAUTHENTICATIONFACTORSAVAILABLES},
                               {ACTION_GETMINUTIAETYPESAVAILABLES, SACTION_GETMINUTIAETYPESAVAILABLES}
                           };
//                               {ACTION_GETCOMPANY, SACTION_GETCOMPANY}

            return ht;
        }
    }
}
