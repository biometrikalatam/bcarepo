﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Bio.Core.Api;
using Bio.Core.Api.Matcher.Interface;
using log4net;

namespace Bio.Core.Matcher
{
    /// <summary>
    /// Esta clase esta destinada a levantar los tipos de matcher existentes 
    /// y crear las clases base, para clonar cuando sea neceario usarlas.
    /// </summary>
    [Serializable]
    public class ConnectorManager
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ConnectorManager));

#region Serializable

        private List<ConnectorDefinition> _connectorsConfigured;

        ///<summary>
        /// Lista de Connectors configurados en config XML.
        ///</summary>
        public List<ConnectorDefinition> ConnectorsConfigured
        {
            get { return _connectorsConfigured; }
            set { _connectorsConfigured = value; }
        }

#endregion Serializable

#region Internal Operations
        private Hashtable _connectorsAvailables = new Hashtable();

        ///<summary>
        /// Tabla indexada por MinutiaeType, con instancias creadas para trabajar.
        ///</summary>
        internal Hashtable ConnectorsAvailables
        {
            get { return _connectorsAvailables; }
            set { _connectorsAvailables = value; }
        }

        ///<summary>
        /// Recorre la estructura de Matchers Configured, y va generando las
        /// instancias en MatcherAvailables. 
        ///</summary>
        ///<returns>0-OK | -1-Error</returns>
        public int Initialization()
        {
            int ret = 0;
            ConnectorInstance ci;

            lock (_connectorsAvailables)
            {
                try
                {
                    _connectorsAvailables = new Hashtable();
                    bool connectorInstanced;
                    foreach (ConnectorDefinition oConnector in _connectorsConfigured)
                    {
                        LOG.Debug("ConnectorManager.Initialization IN: Name=" + oConnector.Name + " - Id="
                            + oConnector.ConnectorId + " - Assembly="
                            + oConnector.Assembly);
                        LOG.Debug(" -- Config=");
                        foreach (var item in oConnector.Config.DynamicDataItems)
	                    {
                            LOG.Debug("   => " + item.key.ToString() + " = " + item.value.ToString()); ;
	                    }
                            //+ oConnector.Config);
                        Assembly asmLogin = Assembly.Load(oConnector.Assembly);
                        ci = new ConnectorInstance(oConnector.ConnectorId,
                                                   oConnector.Name,
                                                   oConnector.Assembly, 
                                                   oConnector.Config);
                        connectorInstanced = true;
                        //Type currenttype = null;
                        object instance;
                        foreach (Type t in asmLogin.GetTypes())
                        {
                            try
                            {

                                if ((typeof(IConnector)).IsAssignableFrom(t))
                                {
                                    LOG.Debug("ConnectorManager.Initialization Activator.CreateInstance IN: " + t.FullName);
                                    instance = Activator.CreateInstance(t);
                                    LOG.Debug("ConnectorManager.Initialization Activator.CreateInstance OUT!");

                                    if (instance is IConnector) //(t.Name.IndexOf("Matcher") > -1)
                                    {
                                        ci.Connector = (IConnector) instance;
                                        ci.Connector.ConnectorId = oConnector.ConnectorId;
                                        ci.Connector.Config = oConnector.Config;
                                    }
                                    instance = null;
                                }
                            }
                            catch (Exception ex)
                            {
                                LOG.Error("ConnectorManager.Initialization - Creando instancias de Connector = " +
                                          oConnector.ConnectorId + " - " + oConnector.Name, ex);
                                connectorInstanced = false;
                            }
                            
                        }

                        //Agrego la minucia con la sinstancias creadas, si se instancio lo necesario
                        if (connectorInstanced)
                        {
                            _connectorsAvailables.Add(ci.ConnectorId, ci);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ret = -1;
                    LOG.Error("ConnectorManager.Initialization", ex);
                }
            }
            return ret;
        }

        /// <summary>
        /// Metodo para ejecutar el Dispose o Release de cada Connector de ser necesario
        /// liberar temas en cada tecnologia.
        /// </summary>
        ///<returns>0-OK | -1-Error</returns>
        public int Release()
        {
            int ret = 0;

            lock (_connectorsAvailables)
            {
                try
                {
                    foreach (ConnectorInstance oConnector in _connectorsAvailables)
                    {
                        if (oConnector.Connector is IDisposable) oConnector.Connector.Dispose();
                    }
                    _connectorsAvailables = null;
                }
                catch (Exception ex)
                {
                    ret = -1;
                    LOG.Error("ConnectorManager.Release", ex);
                }
            }
            return ret;
        }

        ///<summary>
        /// Dado un ConnectorId, si es que existe en los MatcherAvailables, devuelve una instancia, sino null.
        ///</summary>
        ///<param name="connectorid">Identificador del conector</param>
       ///<returns>Un clon del objeto encontrado o null</returns>
        public ConnectorInstance GetConnectorInstance(string connectorid)
        {
            ConnectorInstance objRet = null;
            try
            {
                if (_connectorsAvailables != null)
                {
                    if (_connectorsAvailables.ContainsKey(connectorid))
                    {
                        objRet = (ConnectorInstance)_connectorsAvailables[connectorid];
                        if (objRet == null)
                        {
                            LOG.Warn("ConnectorManager.GetConnectorInstance - ConnectorInstance con ConnectorId=" +
                                     connectorid + " es nulo");
                        } 
                    }
                }
                else
                {
                    LOG.Warn("ConnectorManager.GetConnectorInstance - ConnectorsAvailables = null");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("ConnectorManager.GetConnectorInstance Exception", ex);
            }
            return objRet;
        }

        /// <summary>
        /// Retorna la cantidad de instancias existentes en _connectorsAvailables
        /// </summary>
        /// <returns></returns>
        public int QuantityConnectorsAvailables()
        {
            return _connectorsAvailables == null ? 0 : _connectorsAvailables.Count;
        }

        /// <summary>
        /// Devuelve el Hashtable completo de Connector Availables
        /// </summary>
        /// <returns></returns>
        public Hashtable GetConnectorsAvailables()
        {
            return _connectorsAvailables;  
        }

        /// <summary>
        /// Formatea info de Connectors Availables
        /// </summary>
        /// <returns>String conteniendo los valores de los Connectors instanciados</returns>
        public string ConnecotorsAvailablesToString()
        {
            if (_connectorsAvailables == null)
            {
                return null;
            }
            string aux = null;
            bool first = true;
           
            //for (int i = 0; i < _matchersAvailables.Count; i++)
            ConnectorInstance ci;
            foreach (object o in _connectorsAvailables)
            {
                //MatcherInstance ma = (MatcherInstance)_matchersAvailables[i];
                ci = (ConnectorInstance)((DictionaryEntry)o).Value;
                if (first)
                {
                    aux = "ConnectorId=" + ci.ConnectorId + "-" +
                          "Name=" + ci.Name + "-" +
                          "Asm=" + ci.Assembly + "-" +
                          "Connector=" + (ci.Connector != null ? "OK" : "NULL");
                    first = false;
                }
                else
                {
                    aux = aux + "|" +
                          "|ConnectorId=" + ci.ConnectorId + "-" +
                          "|Name=" + ci.Name + "-" +
                          "|Asm=" + ci.Assembly + "-" +
                          "|Connector=" + (ci.Connector != null ? "OK" : "NULL");
                }
            }
            return aux;
        }
    }

#endregion Internal Operations

    /// <summary>
    /// Define una estrucura para levantar los matcher configurados desde
    /// XML (Matcher.cfg), para luego inicializar en Availables.
    /// </summary>
    [Serializable]
    public class ConnectorDefinition
    {
        private string _connectorid;
        private string _name;
        private string _assembly;
        private DynamicData _config;

        ///<summary>
        ///</summary>
        public string ConnectorId
        {
            get { return _connectorid; }
            set { _connectorid = value; }
        }

        ///<summary>
        ///</summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        ///<summary>
        ///</summary>
        public string Assembly
        {
            get { return _assembly; }
            set { _assembly = value; }
        }

        ///<summary>
        ///</summary>
        public DynamicData Config
        {
            get { return _config; }
            set { _config = value; }
        }

    }

    /// <summary>
    /// Define una estrucura para levantar los connectors configurados y crea las clases
    /// para los posteriores clones cuando se deba usar.
    /// </summary>
    [Serializable]
    public class ConnectorInstance
    {
        internal ConnectorInstance(string connectorid, string name, string assembly, DynamicData config)
        {
            _connectorid = connectorid;
            _name = name;
            _assembly = assembly;
            _config = config;
        }

        private string _connectorid;
        private string _name;
        private string _assembly;
        private DynamicData _config;
        private IConnector _connector;

        ///<summary>
        ///</summary>
        public string ConnectorId
        {
            get { return _connectorid; }
            set { _connectorid = value; }
        }

        ///<summary>
        ///</summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        ///<summary>
        ///</summary>
        public DynamicData Config
        {
            get { return _config; }
            set { _config = value; }
        }

        ///<summary>
        ///</summary>
        public string Assembly
        {
            get { return _assembly; }
            set { _assembly = value; }
        }

        ///<summary>
        ///</summary>
        public IConnector Connector
        {
            get { return _connector; }
            set { _connector = value; }
        }

    }
}