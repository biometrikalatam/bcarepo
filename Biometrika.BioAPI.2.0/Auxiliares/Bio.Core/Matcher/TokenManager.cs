﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Bio.Core.Matcher.Token;
using log4net;
using Bio.Core.Constant;

namespace Bio.Core.Matcher
{

    /// <summary>
    /// Esta clase esta destinada a levantar los tipos de token existentes 
    /// y crear las clases base, para clonar cuando sea neceario usarlas.
    /// </summary>
    [Serializable]
    public class TokenManager
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(TokenManager));

        /// <summary>
        /// De acuerdo al encabezamiento, usa una clase u otra para llenar un token.
        /// </summary>
        /// <param name="token">Token en base 64</param>
        /// <param name="oToken">Objeto Token devuelto</param>
        /// <returns>Codigo de ejecucion</returns>
        public int HandleToken(string token, out IToken oToken)
        {
            oToken = null;
            int ret = Errors.IERR_OK;
            try
            {
                if (token == null)
                {
                    LOG.Debug("HandleToken - parametro nulo");
                    return Errors.IERR_NULL_TOKEN;
                }

                TokenInstance tokeninstance = (TokenInstance)_tokensAvailables[token.Trim().Substring(0, 8)];

                if (tokeninstance != null)
                {
                    ret = tokeninstance.Token.Extract(token);
                    if (ret == Errors.IERR_OK)
                    {
                        oToken = tokeninstance.Token;
                    }
                }
                else
                {
                    ret = Errors.IERR_UNKNOWN_TOKEN;
                }

                //if (token.Trim().Substring(0, 8).Equals("JV010501"))
                //{
                //    //oToken = new TokenJV010501(token);
                //    return ret;
                //}

                //if (token.Trim().Substring(0, 6).Equals("AV0305") ||
                //    token.Trim().Substring(0, 6).Equals("AE0305"))
                //{
                //    //oToken = new TokenAV0305(token);
                //    return ret;
                //}

                //if (token.Trim().Substring(0, 8).Equals("AV040101") ||
                //    token.Trim().Substring(0, 8).Equals("AV030901"))
                //{
                //    //oToken = new TokenAV040101(token);
                //    return ret;
                //}

                
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("TokenManager.HandleToken", ex);
            }
            return ret;
        }


#region Serializable

        private List<TokenDefinition> _tokensConfigured;

        ///<summary>
        /// Lista de Matchers configurados en config XML.
        ///</summary>
        public List<TokenDefinition> TokensConfigured
        {
            get { return _tokensConfigured; }
            set { _tokensConfigured = value; }
        }

#endregion Serializable

#region Internal Operations

        private Hashtable _tokensAvailables = new Hashtable();

        ///<summary>
        /// Tabla indexada por Prefix, con instancias creadas para trabajar.
        ///</summary>
        internal Hashtable TokensAvailables
        {
            get { return _tokensAvailables; }
            set { _tokensAvailables = value; }
        }


        ///<summary>
        /// Recorre la estructura de Matchers Configured, y va generando las
        /// instancias en MatcherAvailables. 
        ///</summary>
        ///<returns>0-OK | -1-Error</returns>
        public int Initialization()
        {
            int ret = 0;
            TokenInstance ti;

            lock (_tokensAvailables)
            {
                try
                {
                    _tokensAvailables = new Hashtable();
                    bool tokenInstanced;
                    foreach (TokenDefinition oToken in _tokensConfigured)
                    {
                        Assembly asmLogin = Assembly.Load(oToken.Assembly);
                        ti = new TokenInstance(oToken.Name,
                                                 oToken.Prefix,
                                                 oToken.Assembly,
                                                 oToken.Parameters);
                        tokenInstanced = true;
                        //Type currenttype = null;
                        object instance;

                        Type[] types = null;
                        try
                        {
                            types = asmLogin.GetTypes();
                        }
                        catch (Exception exloader)
                        {
                            LOG.Warn("TokenManager.Initialization.Loader Warn - Activator.CreateInstance(t)", exloader);
                        }

                        foreach (Type t in types)
                        {
                            try
                            {
                                instance = null;
                                try
                                {
                                    LOG.Debug("TokenManager.Initialization Activator.CreateInstance IN: " + t.FullName);
                                    instance = Activator.CreateInstance(t);
                                    LOG.Debug("TokenManager.Initialization Activator.CreateInstance OUT!");
                                }
                                catch (Exception exC)
                                {
                                    try
                                    {
                                        LOG.Warn("TokenManager.Initialization Warn - Activator.CreateInstance(t) " + t.FullName, exC);
                                    }
                                    catch (Exception ex1)
                                    {
                                        LOG.Warn("TokenManager.Initialization Warn - Activator.CreateInstance(t)", ex1);                                        
                                    }
                                    
                                    instance = null;
                                }

                                if (instance != null)
                                {
                                    if (instance is IToken) 
                                    {
                                        ti.Token = (IToken)instance;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LOG.Error("TokenManager.Initialization - Creando instancias de Prefix=" + ti.Prefix, ex);
                                tokenInstanced = false;
                            }

                        }

                        //Agrego la minucia con las instancias creadas, si se instancio lo necesario
                        if (tokenInstanced)
                        {
                            _tokensAvailables.Add(ti.Prefix, ti);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ret = -1;
                    LOG.Error("TokenManager.Initialization", ex);
                }
            }
            return ret;
        }

        /// <summary>
        /// Metodo para ejecutar el Dispose o Release de cada Matcher de ser necesario
        /// liberar temas en cada tecnologia (Ej. Relaese License en VF6).
        /// </summary>
        ///<returns>0-OK | -1-Error</returns>
        public int Release()
        {
            int ret = 0;

            lock (_tokensAvailables)
            {
                try
                {
                    //foreach (TokenInstance oToken in _tokensAvailables)
                    //{
                    //    if (oToken.Token is IDisposable) oToken.Token.Dispose();
                    //}
                    _tokensAvailables = null;
                }
                catch (Exception ex)
                {
                    ret = -1;
                    LOG.Error("TokenManager.Release", ex);
                }
            }
            return ret;
        }

        ///<summary>
        /// Dado un prefix, devuelve un clon del token, si es que existe en 
        /// los TokenAvailables, sino null.
        ///</summary>
        ///<param name="prefix">Tipo token a procesar</param>
        ///<returns>Un clon del objeto encontrado o null</returns>
        public MatcherInstance GetTokenInstance(string prefix)
        {
            MatcherInstance objRet = null;
            try
            {
                if (_tokensAvailables != null)
                {
                    if (_tokensAvailables.ContainsKey(prefix))
                    {
                        objRet = (MatcherInstance)_tokensAvailables[prefix];
                        if (objRet == null)
                        {
                            LOG.Warn("TokenManager.GetMatcherInstance - TokenInstance con Prefix=" +
                                prefix + " - " + ((objRet != null) ? "[OK]" : "[null]"));
                            objRet = null;
                        }
                    }
                }
                else
                {
                    LOG.Warn("TokenManager.GetMatcherInstance - TokensrsAvailables = null");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("TokenManager.GetTokenInstance Exception", ex);
            }
            return objRet;
        }

        /// <summary>
        /// Retorna la cantidad de instancias existentes en _matchersAvailables
        /// </summary>
        /// <returns></returns>
        public int QuantityTokensAvailables()
        {
            return _tokensAvailables == null ? 0 : _tokensAvailables.Count;
        }

        /// <summary>
        /// Devuelve el Hashtable completo de Matchers Availables
        /// </summary>
        /// <returns></returns>
        public Hashtable GetTokensAvailables()
        {
            return _tokensAvailables;
        }

        /// <summary>
        /// Formatea info de Matchers Availables
        /// </summary>
        /// <returns>String conteniendo los valores de los Matchers instanciados</returns>
        public string TokensAvailablesToString()
        {
            if (_tokensAvailables == null)
            {
                return null;
            }
            string aux = null;
            bool first = true;

            //for (int i = 0; i < _matchersAvailables.Count; i++)
            TokenInstance ma;
            foreach (object o in _tokensAvailables)
            {
                //MatcherInstance ma = (MatcherInstance)_matchersAvailables[i];
                ma = (TokenInstance)((DictionaryEntry)o).Value;
                if (first)
                {
                    aux = "Name=" + ma.Name + "-" +
                          "Prefix=" + ma.Prefix.ToString() + "-" +
                          "Asm=" + ma.Assembly + "-" +
                          "Tok=" + (ma.Token != null ? "OK" : "NULL");
                    first = false;
                }
                else
                {
                    aux = aux + "|" +
                         "Name=" + ma.Name + "-" +
                          "Prefix=" + ma.Prefix.ToString() + "-" +
                          "Asm=" + ma.Assembly + "-" +
                          "Tok=" + (ma.Token != null ? "OK" : "NULL");
                }
            }
            return aux;
        }
    }

        #endregion Internal Operations

    /// <summary>
    /// Define una estrucura para levantar los token configurados desde
    /// XML (Token.cfg), para luego inicializar en Availables.
    /// </summary>
    [Serializable]
    public class TokenDefinition
    {
        private string _name;
        private string _prefix;
        private string _assembly;
        private string _parameters;

        ///<summary>
        ///</summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        ///<summary>
        ///</summary>
        public string Prefix
        {
            get { return _prefix; }
            set { _prefix = value; }
        }

        ///<summary>
        ///</summary>
        public string Assembly
        {
            get { return _assembly; }
            set { _assembly = value; }
        }

        ///<summary>
        ///</summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }
    }

    /// <summary>
    /// Define una estrucura para levantar los matcher configurados y crea las clases
    /// para los posteriores clones cuando se deba usar.
    /// </summary>
    [Serializable]
    public class TokenInstance
    {
        internal TokenInstance(string name, string prefix, string assembly, string parameters)
        {
            _name = name;
            _prefix = prefix;
            _assembly = assembly;
            _parameters = parameters;
        }

        private string _name;
        private string _prefix;
        private string _assembly;
        private string _parameters;
        private IToken _token;
        
        ///<summary>
        ///</summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        ///<summary>
        ///</summary>
        public string Prefix
        {
            get { return _prefix; }
            set { _prefix = value; }
        }

        ///<summary>
        ///</summary>
        public string Assembly
        {
            get { return _assembly; }
            set { _assembly = value; }
        }

        ///<summary>
        ///</summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        ///<summary>
        ///</summary>
        public IToken Token
        {
            get { return _token; }
            set { _token = value; }
        }
    }
}
