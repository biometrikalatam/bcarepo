using System;

namespace Bio.Core.Wsq.Decoder
{
	/// <summary>
	/// Descripción breve de DecoderException.
	/// </summary>
	public class DecoderException : SystemException
	{
		public DecoderException() : base()
		{
		}

		public DecoderException(string message) : base(message)
		{
			
		}

		public DecoderException(string message, Exception exception) : base(message, exception)
		{
			
		}

		public DecoderException(string format, Exception exception, params object[] args) : base(String.Format(format == null ? String.Empty : format, args), exception)
		{
			
		}

		public DecoderException(string message, params object[] args) : base(String.Format(message == null ? String.Empty : message, args))
		{
			
		}

		

	}
}
