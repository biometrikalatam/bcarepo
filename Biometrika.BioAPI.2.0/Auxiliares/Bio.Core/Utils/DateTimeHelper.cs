﻿using System;
using log4net;

namespace Bio.Core.Utils
{
    ///<summary>
    /// Clase destinada a generar metodos de ayuda con tipo de datos DateTime
    ///</summary>
    public class DateTimeHelper
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(DateTimeHelper));


        ///<summary>
        /// Minima fecha considerada por Bio.Core
        ///</summary>
        public static DateTime DTMinimal = new DateTime(1900,1,1,0,0,0,1);

        /// <summary>
        /// Chequea que la variable ingresada no sea menor que el mínimo permitido
        /// en la base de datos, para evitar problemas en la grabación.
        /// </summary>
        /// <param name="param">Variable del tipo DateTime</param>
        /// <returns></returns>
        public static DateTime CheckDateTimeMinimal(DateTime? param)
        {
            DateTime dtRet = new DateTime();
            try {
                if (!param.HasValue) return DTMinimal;
                if (param < DTMinimal) return DTMinimal;
                dtRet = param.Value;
            } catch(Exception ex)
            {
                LOG.Error("DateTimeHelper.CheckDateTimeMinimal", ex);
            }
            return dtRet;
        }

    }
}
