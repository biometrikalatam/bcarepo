﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using log4net;

namespace Bio.Core.Utils
{
    public class SmtpSenderSimple
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(SmtpSenderSimple));

        static public int SendMail(string server, int port, string to, string cc, string bcc, string from, 
                                    string subject, string body, bool sslenable, string user, string psw,
                                    bool isHTML)
        {

            int ret = 0;
            string[] aux;

            try
            {
                if (String.IsNullOrEmpty(server) || String.IsNullOrEmpty(to) ||
                    String.IsNullOrEmpty(from))
                    return -2;

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                aux = to.Trim().Split(new string[] {";"}, StringSplitOptions.None);
                foreach (string sTo in aux)
                {
                    mail.To.Add(new MailAddress(sTo));
                }

                if (!String.IsNullOrEmpty(cc))
                {
                    aux = cc.Trim().Split(new string[] {";"}, StringSplitOptions.None);
                    foreach (string sCC in aux)
                    {
                        mail.CC.Add(new MailAddress(sCC));
                    }
                }
                if (!String.IsNullOrEmpty(bcc))
                {
                    aux = bcc.Trim().Split(new string[] {";"}, StringSplitOptions.None);
                    foreach (string sBCC in aux)
                    {
                        mail.Bcc.Add(new MailAddress(sBCC));
                    }
                }
                mail.Sender = new MailAddress(from);
                mail.From = new MailAddress(from);
                mail.Subject = subject;
                mail.IsBodyHtml = isHTML;
                mail.Body = body;

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "mail.biometrika.cl";
                smtp.Port = 25;

                smtp.Credentials = new NetworkCredential(user, psw);
                smtp.EnableSsl = sslenable;
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                LOG.Error("Error SmtpSenderSimple", ex);
                ret = -1;
            }
            return ret;

        }

    }
}
