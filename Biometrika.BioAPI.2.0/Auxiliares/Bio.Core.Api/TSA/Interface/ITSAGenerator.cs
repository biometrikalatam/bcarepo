﻿using System;

namespace Bio.Core.Api.TSA.Interface
{
    ///<summary>
    /// Interface que deberán implementar los Matcher de cada marca
    ///</summary>
    public interface ITSAGenerator : IDisposable
    {

        /// <summary>
        /// URL para generar TS
        /// </summary>
        string UrlGeneration { get; set; }

        /// <summary>
        /// Tipo de minucia dentro de la tecnologia utilziada
        /// </summary>
        string UrlVerification { get; set; }

        /// <summary>
        /// Umbral de verificación considerada aceptable
        /// </summary>
        int TimeoutWS { get; set; }

        /// <summary>
        /// Se envian parametros si el Matcher lo necesita, de la forma key1=value1|key2=value2|...|keyN=valueN
        /// </summary>
        string Parameters { get; set; }

        /// <summary>
        /// Geenra TS en la TSA desiggnada
        /// </summary>
        /// <param name="b64doc">Documento en B64 a enviar a TSA</param>
        /// <param name="xmloutts">XML con TS</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        int GenerateTS(string b64doc, out string xmloutts);

        /// <summary>
        /// Configura lo necesario en el TSAGenerator
        /// </summary>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        bool ConfigureTSAGenerator();

    }
}
