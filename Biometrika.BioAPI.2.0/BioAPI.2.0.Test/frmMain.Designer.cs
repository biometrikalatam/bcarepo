﻿namespace BioAPI._2._0.Test
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage0 = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPrefix = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.btnInitContinueCapture = new System.Windows.Forms.Button();
            this.btnStopContinueCapture = new System.Windows.Forms.Button();
            this.btnStartContinueCapture = new System.Windows.Forms.Button();
            this.chkSaveHDD = new System.Windows.Forms.CheckBox();
            this.lbSamples = new System.Windows.Forms.ListBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCalidad = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.chkRetBIR = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBP = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAF = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTimeout = new System.Windows.Forms.TextBox();
            this.chkOrigianal = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.buttonCapture = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.picSample = new System.Windows.Forms.PictureBox();
            this.tabStandard = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.grpDestination = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rbCBEEFIso = new System.Windows.Forms.RadioButton();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbOrigen = new System.Windows.Forms.GroupBox();
            this.chkMinutiae = new System.Windows.Forms.CheckBox();
            this.chkWSQ = new System.Windows.Forms.CheckBox();
            this.btnDeleteOrigenStd = new System.Windows.Forms.Button();
            this.chkF1 = new System.Windows.Forms.CheckBox();
            this.picSampleStd = new System.Windows.Forms.PictureBox();
            this.btnConvert = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timerContinuos = new System.Windows.Forms.Timer(this.components);
            this.timerProcessFingerSample = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).BeginInit();
            this.tabStandard.SuspendLayout();
            this.grpDestination.SuspendLayout();
            this.gbOrigen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSampleStd)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage0);
            this.tabControl1.Controls.Add(this.tabStandard);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1419, 654);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage0
            // 
            this.tabPage0.Controls.Add(this.label17);
            this.tabPage0.Controls.Add(this.txtPrefix);
            this.tabPage0.Controls.Add(this.button4);
            this.tabPage0.Controls.Add(this.btnInitContinueCapture);
            this.tabPage0.Controls.Add(this.btnStopContinueCapture);
            this.tabPage0.Controls.Add(this.btnStartContinueCapture);
            this.tabPage0.Controls.Add(this.chkSaveHDD);
            this.tabPage0.Controls.Add(this.lbSamples);
            this.tabPage0.Controls.Add(this.label13);
            this.tabPage0.Controls.Add(this.txtCalidad);
            this.tabPage0.Controls.Add(this.label12);
            this.tabPage0.Controls.Add(this.label11);
            this.tabPage0.Controls.Add(this.label10);
            this.tabPage0.Controls.Add(this.chkRetBIR);
            this.tabPage0.Controls.Add(this.label9);
            this.tabPage0.Controls.Add(this.label8);
            this.tabPage0.Controls.Add(this.txtContent);
            this.tabPage0.Controls.Add(this.label7);
            this.tabPage0.Controls.Add(this.txtBP);
            this.tabPage0.Controls.Add(this.label6);
            this.tabPage0.Controls.Add(this.txtAF);
            this.tabPage0.Controls.Add(this.label5);
            this.tabPage0.Controls.Add(this.label4);
            this.tabPage0.Controls.Add(this.txtTimeout);
            this.tabPage0.Controls.Add(this.chkOrigianal);
            this.tabPage0.Controls.Add(this.button3);
            this.tabPage0.Controls.Add(this.buttonCapture);
            this.tabPage0.Controls.Add(this.button2);
            this.tabPage0.Controls.Add(this.label3);
            this.tabPage0.Controls.Add(this.comboBox1);
            this.tabPage0.Controls.Add(this.richTextBox1);
            this.tabPage0.Controls.Add(this.picSample);
            this.tabPage0.Location = new System.Drawing.Point(4, 22);
            this.tabPage0.Name = "tabPage0";
            this.tabPage0.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage0.Size = new System.Drawing.Size(1411, 628);
            this.tabPage0.TabIndex = 1;
            this.tabPage0.Text = "BioAPI 2.0 Test...";
            this.tabPage0.UseVisualStyleBackColor = true;
            this.tabPage0.Click += new System.EventHandler(this.tabPage0_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(487, 310);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 13);
            this.label17.TabIndex = 69;
            this.label17.Text = "Prefix";
            // 
            // txtPrefix
            // 
            this.txtPrefix.Location = new System.Drawing.Point(526, 307);
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(100, 20);
            this.txtPrefix.TabIndex = 68;
            this.txtPrefix.Text = "212844152_7_";
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(925, 154);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(241, 29);
            this.button4.TabIndex = 67;
            this.button4.Text = "Set _CONTINUE";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnInitContinueCapture
            // 
            this.btnInitContinueCapture.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInitContinueCapture.Location = new System.Drawing.Point(925, 340);
            this.btnInitContinueCapture.Name = "btnInitContinueCapture";
            this.btnInitContinueCapture.Size = new System.Drawing.Size(241, 29);
            this.btnInitContinueCapture.TabIndex = 66;
            this.btnInitContinueCapture.Text = "Init Continue Capture";
            this.btnInitContinueCapture.UseVisualStyleBackColor = true;
            this.btnInitContinueCapture.Visible = false;
            this.btnInitContinueCapture.Click += new System.EventHandler(this.btnInitContinueCapture_Click);
            // 
            // btnStopContinueCapture
            // 
            this.btnStopContinueCapture.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStopContinueCapture.Location = new System.Drawing.Point(925, 410);
            this.btnStopContinueCapture.Name = "btnStopContinueCapture";
            this.btnStopContinueCapture.Size = new System.Drawing.Size(241, 29);
            this.btnStopContinueCapture.TabIndex = 65;
            this.btnStopContinueCapture.Text = "Stop Continue Capture";
            this.btnStopContinueCapture.UseVisualStyleBackColor = true;
            this.btnStopContinueCapture.Visible = false;
            this.btnStopContinueCapture.Click += new System.EventHandler(this.btnStopContinueCapture_Click);
            // 
            // btnStartContinueCapture
            // 
            this.btnStartContinueCapture.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartContinueCapture.Location = new System.Drawing.Point(925, 375);
            this.btnStartContinueCapture.Name = "btnStartContinueCapture";
            this.btnStartContinueCapture.Size = new System.Drawing.Size(241, 29);
            this.btnStartContinueCapture.TabIndex = 64;
            this.btnStartContinueCapture.Text = "Start Continue Capture";
            this.btnStartContinueCapture.UseVisualStyleBackColor = true;
            this.btnStartContinueCapture.Visible = false;
            this.btnStartContinueCapture.Click += new System.EventHandler(this.btnStartContinueCapture_Click);
            // 
            // chkSaveHDD
            // 
            this.chkSaveHDD.AutoSize = true;
            this.chkSaveHDD.Location = new System.Drawing.Point(403, 310);
            this.chkSaveHDD.Name = "chkSaveHDD";
            this.chkSaveHDD.Size = new System.Drawing.Size(78, 17);
            this.chkSaveHDD.TabIndex = 63;
            this.chkSaveHDD.Text = "Save HDD";
            this.chkSaveHDD.UseVisualStyleBackColor = true;
            // 
            // lbSamples
            // 
            this.lbSamples.FormattingEnabled = true;
            this.lbSamples.Location = new System.Drawing.Point(634, 335);
            this.lbSamples.Name = "lbSamples";
            this.lbSamples.Size = new System.Drawing.Size(275, 264);
            this.lbSamples.TabIndex = 62;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(443, 232);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(239, 13);
            this.label13.TabIndex = 61;
            this.label13.Text = "(Valor 0 a 100 para control de calidad de imagen)";
            // 
            // txtCalidad
            // 
            this.txtCalidad.Location = new System.Drawing.Point(337, 229);
            this.txtCalidad.Name = "txtCalidad";
            this.txtCalidad.Size = new System.Drawing.Size(100, 20);
            this.txtCalidad.TabIndex = 60;
            this.txtCalidad.Text = "70";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(253, 232);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 59;
            this.label12.Text = "Umbral Calidad";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(443, 210);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(253, 13);
            this.label11.TabIndex = 58;
            this.label11.Text = "(0-All | 1-Solo WSQ | 2-Solo Minucias | 3-Solo Image)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(443, 180);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(272, 13);
            this.label10.TabIndex = 57;
            this.label10.Text = "(Parte del cuerpo a capturar: 1-10 Dedos | 16-Cara, etc))";
            // 
            // chkRetBIR
            // 
            this.chkRetBIR.AutoSize = true;
            this.chkRetBIR.Location = new System.Drawing.Point(226, 278);
            this.chkRetBIR.Name = "chkRetBIR";
            this.chkRetBIR.Size = new System.Drawing.Size(162, 17);
            this.chkRetBIR.TabIndex = 56;
            this.chkRetBIR.Text = "Retorno BIR BioAPI Enabled";
            this.chkRetBIR.UseVisualStyleBackColor = true;
            this.chkRetBIR.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(443, 154);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(245, 13);
            this.label9.TabIndex = 55;
            this.label9.Text = "(Tipo de tecnologia a usar: 2-Fingerprint | 4-Facial))";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(443, 128);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(183, 13);
            this.label8.TabIndex = 54;
            this.label8.Text = "(Timeout de captura en milisegundos)";
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(337, 203);
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(100, 20);
            this.txtContent.TabIndex = 53;
            this.txtContent.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(287, 206);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 52;
            this.label7.Text = "Content";
            // 
            // txtBP
            // 
            this.txtBP.Location = new System.Drawing.Point(337, 177);
            this.txtBP.Name = "txtBP";
            this.txtBP.Size = new System.Drawing.Size(100, 20);
            this.txtBP.TabIndex = 51;
            this.txtBP.Text = "1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(281, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 50;
            this.label6.Text = "BodyPart";
            // 
            // txtAF
            // 
            this.txtAF.Location = new System.Drawing.Point(337, 151);
            this.txtAF.Name = "txtAF";
            this.txtAF.Size = new System.Drawing.Size(100, 20);
            this.txtAF.TabIndex = 49;
            this.txtAF.Text = "2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(223, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 13);
            this.label5.TabIndex = 48;
            this.label5.Text = "Authentication Factor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(246, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 46;
            this.label4.Text = "Timeout Captura";
            // 
            // txtTimeout
            // 
            this.txtTimeout.Location = new System.Drawing.Point(337, 125);
            this.txtTimeout.Name = "txtTimeout";
            this.txtTimeout.Size = new System.Drawing.Size(100, 20);
            this.txtTimeout.TabIndex = 45;
            this.txtTimeout.Text = "5000";
            // 
            // chkOrigianal
            // 
            this.chkOrigianal.AutoSize = true;
            this.chkOrigianal.Location = new System.Drawing.Point(20, 102);
            this.chkOrigianal.Name = "chkOrigianal";
            this.chkOrigianal.Size = new System.Drawing.Size(123, 17);
            this.chkOrigianal.TabIndex = 44;
            this.chkOrigianal.Text = "Show Image Original";
            this.chkOrigianal.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(249, 23);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(149, 30);
            this.button3.TabIndex = 43;
            this.button3.Text = "UnLoad BPS";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // buttonCapture
            // 
            this.buttonCapture.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCapture.Location = new System.Drawing.Point(226, 301);
            this.buttonCapture.Name = "buttonCapture";
            this.buttonCapture.Size = new System.Drawing.Size(149, 29);
            this.buttonCapture.TabIndex = 38;
            this.buttonCapture.Text = "Capture";
            this.buttonCapture.UseVisualStyleBackColor = true;
            this.buttonCapture.Click += new System.EventHandler(this.buttonCapture_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(84, 23);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(159, 30);
            this.button2.TabIndex = 9;
            this.button2.Text = "Load BPS";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Sensores";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(84, 69);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(800, 21);
            this.comboBox1.TabIndex = 7;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(30, 336);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(586, 269);
            this.richTextBox1.TabIndex = 5;
            this.richTextBox1.Text = "";
            // 
            // picSample
            // 
            this.picSample.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picSample.Location = new System.Drawing.Point(30, 125);
            this.picSample.Name = "picSample";
            this.picSample.Size = new System.Drawing.Size(187, 205);
            this.picSample.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSample.TabIndex = 4;
            this.picSample.TabStop = false;
            // 
            // tabStandard
            // 
            this.tabStandard.Controls.Add(this.label16);
            this.tabStandard.Controls.Add(this.textBox11);
            this.tabStandard.Controls.Add(this.label15);
            this.tabStandard.Controls.Add(this.textBox10);
            this.tabStandard.Controls.Add(this.label14);
            this.tabStandard.Controls.Add(this.textBox9);
            this.tabStandard.Controls.Add(this.textBox8);
            this.tabStandard.Controls.Add(this.textBox7);
            this.tabStandard.Controls.Add(this.textBox6);
            this.tabStandard.Controls.Add(this.textBox5);
            this.tabStandard.Controls.Add(this.textBox4);
            this.tabStandard.Controls.Add(this.textBox3);
            this.tabStandard.Controls.Add(this.textBox2);
            this.tabStandard.Controls.Add(this.textBox1);
            this.tabStandard.Controls.Add(this.button1);
            this.tabStandard.Controls.Add(this.grpDestination);
            this.tabStandard.Controls.Add(this.gbOrigen);
            this.tabStandard.Controls.Add(this.btnConvert);
            this.tabStandard.Location = new System.Drawing.Point(4, 22);
            this.tabStandard.Name = "tabStandard";
            this.tabStandard.Padding = new System.Windows.Forms.Padding(3);
            this.tabStandard.Size = new System.Drawing.Size(1411, 628);
            this.tabStandard.TabIndex = 0;
            this.tabStandard.Text = "Standard Test...";
            this.tabStandard.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(821, 33);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(104, 13);
            this.label16.TabIndex = 19;
            this.label16.Text = "SBH Security Option";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(860, 329);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(253, 20);
            this.textBox11.TabIndex = 18;
            this.textBox11.Text = "c:\\std\\sample_mt_RAW.bin";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(857, 312);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Path BLBData";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(860, 369);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(253, 20);
            this.textBox10.TabIndex = 16;
            this.textBox10.Text = "c:\\std\\";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(857, 352);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "Path Destino";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(937, 238);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(92, 20);
            this.textBox9.TabIndex = 14;
            this.textBox9.Text = "0";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(937, 212);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(92, 20);
            this.textBox8.TabIndex = 13;
            this.textBox8.Text = "0";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(937, 186);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(92, 20);
            this.textBox7.TabIndex = 12;
            this.textBox7.Text = "0";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(937, 160);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(92, 20);
            this.textBox6.TabIndex = 11;
            this.textBox6.Text = "0";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(937, 134);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(92, 20);
            this.textBox5.TabIndex = 10;
            this.textBox5.Text = "0";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(937, 108);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(92, 20);
            this.textBox4.TabIndex = 9;
            this.textBox4.Text = "0";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(937, 82);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(92, 20);
            this.textBox3.TabIndex = 8;
            this.textBox3.Text = "0";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(937, 56);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(92, 20);
            this.textBox2.TabIndex = 7;
            this.textBox2.Text = "0";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(937, 30);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(92, 20);
            this.textBox1.TabIndex = 6;
            this.textBox1.Text = "0";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1038, 395);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Crear BIR";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // grpDestination
            // 
            this.grpDestination.Controls.Add(this.label2);
            this.grpDestination.Controls.Add(this.rbCBEEFIso);
            this.grpDestination.Controls.Add(this.txtPath);
            this.grpDestination.Controls.Add(this.label1);
            this.grpDestination.Location = new System.Drawing.Point(362, 20);
            this.grpDestination.Name = "grpDestination";
            this.grpDestination.Size = new System.Drawing.Size(413, 359);
            this.grpDestination.TabIndex = 5;
            this.grpDestination.TabStop = false;
            this.grpDestination.Text = "Destino...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Formato Destino";
            // 
            // rbCBEEFIso
            // 
            this.rbCBEEFIso.AutoSize = true;
            this.rbCBEEFIso.Location = new System.Drawing.Point(47, 120);
            this.rbCBEEFIso.Name = "rbCBEEFIso";
            this.rbCBEEFIso.Size = new System.Drawing.Size(341, 17);
            this.rbCBEEFIso.TabIndex = 6;
            this.rbCBEEFIso.TabStop = true;
            this.rbCBEEFIso.Text = "CBEEF v2.0 (ISO/IEC 19785-1:2006 (with ISO/IEC 19785-3:2007))";
            this.rbCBEEFIso.UseVisualStyleBackColor = true;
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(25, 44);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(253, 20);
            this.txtPath.TabIndex = 5;
            this.txtPath.Text = "c:\\std\\";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Path Destino";
            // 
            // gbOrigen
            // 
            this.gbOrigen.Controls.Add(this.chkMinutiae);
            this.gbOrigen.Controls.Add(this.chkWSQ);
            this.gbOrigen.Controls.Add(this.btnDeleteOrigenStd);
            this.gbOrigen.Controls.Add(this.chkF1);
            this.gbOrigen.Controls.Add(this.picSampleStd);
            this.gbOrigen.Location = new System.Drawing.Point(20, 20);
            this.gbOrigen.Name = "gbOrigen";
            this.gbOrigen.Size = new System.Drawing.Size(278, 359);
            this.gbOrigen.TabIndex = 4;
            this.gbOrigen.TabStop = false;
            this.gbOrigen.Text = "Origen...";
            // 
            // chkMinutiae
            // 
            this.chkMinutiae.AutoSize = true;
            this.chkMinutiae.Location = new System.Drawing.Point(161, 23);
            this.chkMinutiae.Name = "chkMinutiae";
            this.chkMinutiae.Size = new System.Drawing.Size(78, 17);
            this.chkMinutiae.TabIndex = 6;
            this.chkMinutiae.Text = "Es Minucia";
            this.chkMinutiae.UseVisualStyleBackColor = true;
            // 
            // chkWSQ
            // 
            this.chkWSQ.AutoSize = true;
            this.chkWSQ.Location = new System.Drawing.Point(88, 23);
            this.chkWSQ.Name = "chkWSQ";
            this.chkWSQ.Size = new System.Drawing.Size(67, 17);
            this.chkWSQ.TabIndex = 5;
            this.chkWSQ.Text = "Es WSQ";
            this.chkWSQ.UseVisualStyleBackColor = true;
            // 
            // btnDeleteOrigenStd
            // 
            this.btnDeleteOrigenStd.Location = new System.Drawing.Point(178, 320);
            this.btnDeleteOrigenStd.Name = "btnDeleteOrigenStd";
            this.btnDeleteOrigenStd.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteOrigenStd.TabIndex = 4;
            this.btnDeleteOrigenStd.Text = "Borrar";
            this.btnDeleteOrigenStd.UseVisualStyleBackColor = true;
            this.btnDeleteOrigenStd.Click += new System.EventHandler(this.btnDeleteOrigenStd_Click);
            // 
            // chkF1
            // 
            this.chkF1.AutoSize = true;
            this.chkF1.Location = new System.Drawing.Point(22, 23);
            this.chkF1.Name = "chkF1";
            this.chkF1.Size = new System.Drawing.Size(60, 17);
            this.chkF1.TabIndex = 3;
            this.chkF1.Text = "Es B64";
            this.chkF1.UseVisualStyleBackColor = true;
            // 
            // picSampleStd
            // 
            this.picSampleStd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picSampleStd.Location = new System.Drawing.Point(16, 42);
            this.picSampleStd.Name = "picSampleStd";
            this.picSampleStd.Size = new System.Drawing.Size(237, 263);
            this.picSampleStd.TabIndex = 2;
            this.picSampleStd.TabStop = false;
            this.picSampleStd.Click += new System.EventHandler(this.picSample_Click);
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(585, 385);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(101, 30);
            this.btnConvert.TabIndex = 2;
            this.btnConvert.Text = "Convertir...";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timerContinuos
            // 
            this.timerContinuos.Interval = 10;
            this.timerContinuos.Tick += new System.EventHandler(this.timerContinuos_Tick);
            // 
            // timerProcessFingerSample
            // 
            this.timerProcessFingerSample.Interval = 10;
            this.timerProcessFingerSample.Tick += new System.EventHandler(this.timerProcessFingerSample_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1443, 678);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "BSP Test v1.0...";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage0.ResumeLayout(false);
            this.tabPage0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).EndInit();
            this.tabStandard.ResumeLayout(false);
            this.tabStandard.PerformLayout();
            this.grpDestination.ResumeLayout(false);
            this.grpDestination.PerformLayout();
            this.gbOrigen.ResumeLayout(false);
            this.gbOrigen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSampleStd)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabStandard;
        private System.Windows.Forms.TabPage tabPage0;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox gbOrigen;
        private System.Windows.Forms.Button btnDeleteOrigenStd;
        private System.Windows.Forms.CheckBox chkF1;
        private System.Windows.Forms.PictureBox picSampleStd;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.GroupBox grpDestination;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbCBEEFIso;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkMinutiae;
        private System.Windows.Forms.CheckBox chkWSQ;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.PictureBox picSample;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button buttonCapture;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox chkOrigianal;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkRetBIR;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAF;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTimeout;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtCalidad;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ListBox lbSamples;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chkSaveHDD;
        private System.Windows.Forms.Button btnStartContinueCapture;
        private System.Windows.Forms.Button btnStopContinueCapture;
        private System.Windows.Forms.Button btnInitContinueCapture;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Timer timerContinuos;
        private System.Windows.Forms.Timer timerProcessFingerSample;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtPrefix;
    }
}

