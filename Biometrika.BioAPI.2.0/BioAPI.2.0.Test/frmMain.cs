﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biometrika.BioApi20;
using Biometrika.BioApi20.Interfaces;
using SecuGen.FDxSDKPro.Windows;

namespace BioAPI._2._0.Test
{
    public partial class frmMain : Form
    {
        Biometrika.BioApi20.BFPSecugenSensor.BFPSecugenSensor _SCUGEN_SENSOR;

        Biometrika.BioApi20.BSP.BSPBiometrika BSP;
        List<Sample> _SamplesCaptured;
        byte[] _Sample_Standard;

        public frmMain()
        {
            InitializeComponent();
        }

        private void picSample_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Seleccione sample en Base 64...";
                string aux = null;
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    //System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
                    //image1 = sr.ReadToEnd();
                    //sr.Close();

                    if (!chkF1.Checked)
                    {
                        _Sample_Standard = System.IO.File.ReadAllBytes(openFileDialog1.FileName);
                        //image1 = Convert.ToBase64String(byimage1);
                    }
                    else
                    {
                        aux = System.IO.File.ReadAllText(openFileDialog1.FileName);
                    }
                    //System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(aux));
                    //_Sample_Standard = ms.ToArray();
                    //ms.Close();
                    //System.IO.File.WriteAllBytes(openFileDialog1.FileName + ".bmp", _Sample_Standard);
                    //picSampleStd.Image = Bitmap.FromStream(ms);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception = " + ex.StackTrace);
            }
        }

        private void btnDeleteOrigenStd_Click(object sender, EventArgs e)
        {
            _Sample_Standard = null;
            picSampleStd.Image = null;
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            //byte[] destination;
            //string msg;
            ////int i = Biometrika.Standards.Convertion.Convert(_Sample_Standard, Biometrika.Standards.Constant.SAMPLE_FORMAT_IMAGE_RAW, Biometrika.Standards.Constant.DESTINATION_FORMAT_CBEEF_ISO,
            ////out destination, out msg);

            ////string s = System.IO.File.ReadAllText(@"c:\std\wsq.txt");
            ////System.IO.File.WriteAllBytes(@"c:\std\Sample.WSQ", Convert.FromBase64String(s));
            //byte[] wsq = System.IO.File.ReadAllBytes(@"c:\std\Sample.WSQ");

            //int i = Biometrika.Standards.Convertion.ConvertRAWToCBEEF(wsq, Biometrika.Standards.Constant.DESTINATION_FORMAT_CBEEF_ISO, out destination, out msg);


            ////int i = Biometrika.Standards.Convertion.Convert(_Sample_Standard, Biometrika.Standards.Constant.SAMPLE_FORMAT_IMAGE_RAW, Biometrika.Standards.Constant.DESTINATION_FORMAT_CBEEF_ISO,
            ////                                                out destination, out msg);

            //System.IO.File.WriteAllBytes(@"c:\std\NTemplateSample.CBEEFv2", destination);
            //i = 0;

        }

        /// <summary>
        /// Evento del lector de secugen, que permite capturar una huella.
        /// </summary>
        /// <param name="message"></param>
        //protected override void WndProc(ref Message message)
        //{
        //    int res = 0;
        //    if (_mustStop == true)
        //    {
        //        if (message.Msg == (int)SGFPMMessages.DEV_AUTOONEVENT)
        //        {
        //            if (message.WParam.ToInt32() == (Int32)SGFPMAutoOnEvent.FINGER_ON)
        //            {
        //                //lblStatus.Text = "Dedo en el lector";

        //                bcrFpr.TimeOut = 3000;
        //                bcrFpr.Quality = 50;
        //                res = bcrFpr.Capture();
        //                MostrarHuella();
        //                Thread.Sleep(200);
        //                //this.Invoke((MethodInvoker)delegate
        //                //{
        //                //    picHuellas.Image = bcrFpr.GetBitmapImage();
        //                //});
        //                if (res == 0)
        //                {
        //                    log.Info("Se obtiene una imagen, se procede a revisar si existe en la BD");
        //                    _mustStop = false;
        //                    Identificar();


        //                    bcrFpr.On();
        //                    _mustStop = true;

        //                }
        //                else
        //                {

        //                    picHuellas.Image = null;
        //                    _mustStop = true;
        //                }

        //            }
        //            else if (message.WParam.ToInt32() == (Int32)SGFPMAutoOnEvent.FINGER_OFF)
        //            {
        //                //lblStatus.Text = "Dedo fuera en el lector";
        //                //picHuellas.Image = null;

        //                _mustStop = true;
        //                log.Info("Al sacar el dedo, Valor de _muststop:" + _mustStop);
        //            }
        //        }
        //        base.WndProc(ref message);
        //    }
        //}

        private void frmMain_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;

            //this.BackColor = Color.FromName("Red");
            //_SCUGEN_SENSOR = new Biometrika.BioApi20.BFPSecugenSensor.BFPSecugenSensor();
            //int i = _SCUGEN_SENSOR.InstanceSensor("H54181019063"); // "H39150402301");
            //_SCUGEN_SENSOR.On();
            //_SCUGEN_SENSOR.EnableAutoEvent(true, (int)this.Handle);

            //_SCUGEN_SENSOR.OnCapturedEvent += OnCaptureEventSolo;
            button2_Click(null, null);
        }

        private void OnCaptureEventSolo(int errCode, string errMessage, List<Sample> samplesCaptured)
        {
            MessageBox.Show("OnCaptureEventSolo => len = " + (samplesCaptured != null ? samplesCaptured.Count.ToString() : "null"));
        }

        //public string GetWSQImage64()
        //{
        //    var raw512x512 = ImageProcessor.FillRaw(raw, imagewidth, imageheight, 512, 512);
        //    //ELog(Convert.ToBase64String(raw512x512));
        //    var encoder = new WsqEncoder();
        //    byte[] wsqCaptured;

        //    if (encoder.EncodeMemory(raw512x512, 512, 512, out wsqCaptured))
        //    {
        //        return Convert.ToBase64String(wsqCaptured);
        //    }
        //    else
        //    {
        //        return string.Empty;
        //    }
        //}

        /// <summary>
        /// Evento del lector de secugen, que permite capturar una huella.
        /// </summary>
        /// <param name="message"></param>
        protected override void WndProc(ref Message message)
        {
            int res = 0;
            if (_mustStop == true)
            {
                if (message.Msg == (int)SGFPMMessages.DEV_AUTOONEVENT)
                {
                    if (message.WParam.ToInt32() == (Int32)SGFPMAutoOnEvent.FINGER_ON)
                    {
                        //lblStatus.Text = "Dedo en el lector";

                        //bcrFpr.TimeOut = 3000;
                        //bcrFpr.Quality = 50;
                        Error err;
                        //res = _SCUGEN_SENSOR.CaptureContinue("H54181019063", 50, out err);// bcrFpr.Capture();
                        res = BSP.CaptureContinue("H54181019063", 50, out err);// bcrFpr.Capture();

                        //MostrarHuella();

                        //picSample.Image 

                        Thread.Sleep(200);
                        //this.Invoke((MethodInvoker)delegate
                        //{
                        //    picHuellas.Image = bcrFpr.GetBitmapImage();
                        //});
                        if (res == 0)
                        {
                            //log.Info("Se obtiene una imagen, se procede a revisar si existe en la BD");
                            
                            _mustStop = false;
                            //Identificar();
                            //_SamplesCaptured = 
                            //ProcessFingerSample()

                            //bcrFpr.On();
                            _mustStop = true;

                        }
                        else
                        {

                            picSample.Image = null;
                            _mustStop = true;
                        }

                    }
                    else if (message.WParam.ToInt32() == (Int32)SGFPMAutoOnEvent.FINGER_OFF)
                    {
                        //lblStatus.Text = "Dedo fuera en el lector";
                        //picHuellas.Image = null;

                        _mustStop = true;
                        //log.Info("Al sacar el dedo, Valor de _muststop:" + _mustStop);
                    }
                }
                base.WndProc(ref message);
            }
        }


        bool _mustStop = true;
        /// <summary>
        /// Evento del lector de secugen, que permite capturar una huella.
        /// </summary>
        /// <param name="message"></param>
        //protected override void WndProc(ref Message message)
        //{
        //    int res = 0;
        //    if (_mustStop == true)
        //    {
        //        if (message.Msg == (int)SGFPMMessages.DEV_AUTOONEVENT)
        //        {
        //            if (message.WParam.ToInt32() == (Int32)SGFPMAutoOnEvent.FINGER_ON)
        //            {
        //                //lblStatus.Text = "Dedo en el lector";

        //                bcrFpr.TimeOut = 3000;
        //                bcrFpr.Quality = 50;
        //                res = bcrFpr.Capture();
        //                MostrarHuella();
        //                Thread.Sleep(200);
        //                //this.Invoke((MethodInvoker)delegate
        //                //{
        //                //    picHuellas.Image = bcrFpr.GetBitmapImage();
        //                //});
        //                if (res == 0)
        //                {
        //                    log.Info("Se obtiene una imagen, se procede a revisar si existe en la BD");
        //                    _mustStop = false;
        //                    Identificar();


        //                    bcrFpr.On();
        //                    _mustStop = true;

        //                }
        //                else
        //                {

        //                    picHuellas.Image = null;
        //                    _mustStop = true;
        //                }

        //            }
        //            else if (message.WParam.ToInt32() == (Int32)SGFPMAutoOnEvent.FINGER_OFF)
        //            {
        //                //lblStatus.Text = "Dedo fuera en el lector";
        //                //picHuellas.Image = null;

        //                _mustStop = true;
        //                log.Info("Al sacar el dedo, Valor de _muststop:" + _mustStop);
        //            }
        //        }
        //        base.WndProc(ref message);
        //    }
        //}

        private void button2_Click(object sender, EventArgs e)
        {

            InitBSP();

        }

        private void InitBSP()
        {
            BSP = new Biometrika.BioApi20.BSP.BSPBiometrika();

            BSP.BSPAttach("2.0", true);

            GetInfoFromBSP();

            BSP.OnCapturedEvent += OnCaptureEvent;
            BSP.OnTimeoutEvent += OnTimeoutEvent;
        }

        private void buttonAttach_Click(object sender, EventArgs e)
        {

        }

        public void GetInfoFromBSP()
        {
            List<BFPListElement> listBFP = BSP.QueryBFPs();

            foreach (BFPListElement item in listBFP)
            {
                comboBox1.Items.Add(item.SerialSensor + "    | " + GetTec(item.AuthenticationFactor) + " | " + item.Brand);
            }


        }

        private string GetTec(int authenticationFactor)
        {
            switch (authenticationFactor)
            {
                case 2:
                    return "Fingerprint";
                case 4:
                    return "Facial";
                default:
                    return "AF=" + authenticationFactor.ToString();
            }
        }

        private static string GetLast(string source, int tailLength)
        {
            if (tailLength >= source.Length)
                return source;
            return source.Substring(source.Length - tailLength);
        }

        private void buttonCapture_Click(object sender, EventArgs e)
        {
            Error err;

            if (BSP == null) InitBSP();

            _SamplesCaptured = null;
            string serialSelected = SetSerialSelected();

            if (!string.IsNullOrEmpty(serialSelected))
            {
                richTextBox1.Text = "Capturando desde Sensor = " + serialSelected + "..." + Environment.NewLine;
                BSP.Capture(Convert.ToInt32(txtAF.Text), Convert.ToInt32(txtBP.Text), serialSelected,
                            Convert.ToInt32(txtTimeout.Text), Convert.ToInt32(txtContent.Text),
                            Convert.ToInt32(txtCalidad.Text), out err);
            } else
            {
                richTextBox1.Text = "Capturando desde 1er Sensor configurado en BSP..." + Environment.NewLine; ;
                BSP.Capture(Convert.ToInt32(txtAF.Text), Convert.ToInt32(txtBP.Text), null,
                            Convert.ToInt32(txtTimeout.Text), Convert.ToInt32(txtContent.Text),
                            Convert.ToInt32(txtCalidad.Text), out err);
            }

        }

        private string SetSerialSelected()
        {
            try
            {
                string s = (string)comboBox1.SelectedItem;
                if (string.IsNullOrEmpty(s)) return null;
                else
                {
                    return s.Split('|')[0].Trim();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void OnTimeoutEvent(int errCode, string errMessage, ISensor sensor)
        {
            picSample.Image = null;
            MessageBox.Show(errCode + " - " + errMessage);
            //if (_CAPTURING)
            //{
            //    _ENABLE_CAPTURE = true;
            //    //timerContinuos.Enabled = true;
            //}
            //buttonCapture_Click(null, null);

        }

        private void OnCaptureEvent(int errCode, string errMessage, string serialId, List<Sample> samplesCaptured)
        {
            try
            {
                _SamplesCaptured = samplesCaptured;

                Sample s;
                try
                {
                    if (samplesCaptured != null && samplesCaptured.Count > 0)
                    {
                        if (chkOrigianal.Checked)
                        {
                            s = samplesCaptured[samplesCaptured.Count - 2];
                        }
                        else
                        {
                            s = samplesCaptured[samplesCaptured.Count - 1];
                        }
                        if (s.MinutiaeType == 41)
                            picSample.Image = (Bitmap)s.Data;
                    }
                }
                catch (Exception ex)
                {
                    richTextBox1.Text = "Error obteniendo imagen!" + Environment.NewLine;
                }

                richTextBox1.Text += "Samples Obtenidos: Serial = " + serialId + Environment.NewLine;
                richTextBox1.Text += "----------------------------------------------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;
                foreach (Sample item in samplesCaptured)
                {
                    string saux = item.SampleWith + "x" + item.SampleHeight;
                    richTextBox1.Text += "AFactor=" + item.AuthenticationFactor.ToString() +
                                         " - MType=" + item.MinutiaeType.ToString() +
                                         " - BodyPart=" + item.BodyPart.ToString() +
                                         " - Operacion=" + (item.Operation == 0 ? "Verify" : "Enroll") +
                                         " - WxH=" + saux +
                                         " - Sample=" + (item.MinutiaeType == 41 ? "Image" : Convert.ToBase64String((byte[])item.Data).Substring(0, 10) + "...")
                                         + Environment.NewLine;
                    if (chkSaveHDD.Checked)
                    {
                        int idxi = 0;
                        string sauxname;
                        sauxname = "AF-" + item.AuthenticationFactor.ToString() +
                                         "_MT-" + item.MinutiaeType.ToString() +
                                         "_BP-" + item.BodyPart.ToString() +
                                         "_OP-" + (item.Operation == 0 ? "V" : "E") +
                                         "_WxH-" + saux + DateTime.Now.ToString("ddMMyyyyHHmmss");
                        if (item.MinutiaeType == 41)
                        {
                            //System.IO.File.WriteAllBytes(Properties.Settings.Default.RootPath + "\\data\\" + sauxname + ".jpg", (byte[])item.Data);
                            Bitmap b = (Bitmap)item.Data;
                            b.Save(Properties.Settings.Default.RootPath + "\\data\\" + sauxname + "_" + (idxi++) + ".bmp", ImageFormat.Bmp);
                        }
                        else
                        {
                            System.IO.File.WriteAllText(Properties.Settings.Default.RootPath + "\\data\\" + sauxname + ".txt", Convert.ToBase64String((byte[])item.Data));
                        }
                    }
                }
                richTextBox1.Text += "----------------------------------------------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;

                string aux = "";
                int idx = 0;
                if (chkSaveHDD.Checked)
                {
                    bool save = false;
                    foreach (Sample item in samplesCaptured)
                    {
                        save = false;
                        if (item.MinutiaeType == 41)
                        {
                            //System.IO.File.WriteAllBytes(@"c:\std\sample_mt_41_" + (idx++) + ".bmp", (byte[])item.Data);
                            Bitmap b = (Bitmap)item.Data;
                            b.Save(Properties.Settings.Default.RootPath + "\\data\\" + txtPrefix.Text + 
                                    "sample_mt_41_" + (idx++) + ".jpg", ImageFormat.Jpeg);
                        }
                        else
                        {
                            if (item.MinutiaeType == 13)
                            {
                                aux = "ANSI";
                                save = true;
                            }
                            if (item.MinutiaeType == 14)
                            {
                                aux = "ISO";
                                save = true;
                            }
                            if (item.MinutiaeType == 15)
                            {
                                aux = "ISOCC";
                                save = true;
                            }
                            if (item.MinutiaeType == 4)
                            {
                                aux = "DP";
                                save = true;
                            }
                            if (item.MinutiaeType == 5)
                            {
                                aux = "SECUGEN";
                                save = true;
                            }
                            if (item.MinutiaeType == 21)
                            {
                                aux = "WSQ";
                                save = true;
                            }
                            if (item.MinutiaeType == 22)
                            {
                                aux = "RAW";
                                save = true;
                            }
                            if (save)
                            {
                                //string saux = item.SampleWith + "x" + item.SampleHeight;
                                //string sauxname = "AF-" + item.AuthenticationFactor.ToString() +
                                //                     "_MT-" + item.MinutiaeType.ToString() +
                                //                     "_BP-" + item.BodyPart.ToString() +
                                //                     "_OP-" + (item.Operation == 0 ? "V" : "E") +
                                //                     "_WxH-" + saux + "_";
                                //System.IO.File.WriteAllText(Properties.Settings.Default.RootPath + "\\data\\" + 
                                //    txtPrefix.Text + sauxname + aux + ".txt", Convert.ToBase64String((byte[])item.Data));
                                //System.IO.File.WriteAllBytes(Properties.Settings.Default.RootPath + "\\data\\" + 
                                //    txtPrefix.Text + sauxname + aux + ".bin", (byte[])item.Data);
                                //if (item.MinutiaeType == 15) //Es ISOCC => Paso a Hexa para uso en demo MOC
                                //{
                                //    string[] strISOCC = ConvertToHexa((byte[])item.Data);
                                //    System.IO.File.WriteAllText(Properties.Settings.Default.RootPath + "\\data\\" +
                                //                txtPrefix.Text + sauxname + aux + "_HEXA.txt", ConvertStringArrayToString(strISOCC));
                                //}

                                save = false;
                            }
                        }
                    }
                }

                //BSP.OnCapturedEvent -= OnCaptureEvent;
                //BSP.OnTimeoutEvent -= OnTimeoutEvent;
            }
            catch (Exception ex)
            {

            }
            //if (_CAPTURING)
            //{
            //    _ENABLE_CAPTURE = true;
            //    //timerContinuos.Enabled = true;
            //} else
            //{
            //    _PROCESS_FINGERSAMPLE = true;
            //}
            //timer1.Enabled = true;
        }

        public void FourthThreadFunction()
        {
            //if (checkBox1.Checked)
            //{
            //    //Subscription to callbacks
            //    attachsession.SubscribeToGUIEvents(null, null, ShowMessage);
            //}
            //else
            //{
            //    //Unsubscription to callbacks
            //    attachsession.UnsubscribeFromGUIEvents();
            //}

            //label1.Text = "stringFinger"; // Properties.Resources.stringFinger;

            ////Call to Capture (monolithic system) instead to Enrol
            //BIR bir = attachsession.Capture(null, BiometricSubtype.RightIndexFinger, null, 40000, null);
            ////byte[] birByteArray = bir.BIRToByteArray();

            ////panel3.BackgroundImage = Properties.Resources.ProgressWheel;
            ////label1.Text = Properties.Resources.stringWork;

            //if (bir != null)
            //{
            //    byte[] image = bir.BDBData;
            //    //panel3.BackgroundImage = null;
            //    label1.Text = " ";

            //    DialogResult result = ShowImage(image);
            //    //if (result == DialogResult.Yes)
            //    //{
            //    //    //Process
            //    //    bir = attachsession.Process(bir, null);

            //    //    //Store
            //    //    UUID reference = attachsession.StoreBIR(bir);
            //    //    MessageBox.Show(Properties.Resources.stringStored + GetLast(reference.ID.ToString(), 4), Properties.Resources.stringSuccess,
            //    //        MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    //}
            //    //else { }
            //}
            //else
            //{
            //    MessageBox.Show("stringFailure", "stringFail", MessageBoxButtons.OK,
            //        MessageBoxIcon.Error);
            //}

            ////panel3.BackgroundImage = null;
            ////label1.Text = " ";
            //comboBox2.SelectedIndex = -1;
            //buttonAttach.Enabled = true;
            ////panel2.Enabled = true;
            //checkBox1.Checked = false;
            //checkBox1.Enabled = false;
            ////buttonSelectionID.Enabled = false;
        }

        //Callback from the BSP
        //public bool ShowMessage(UUID bspUUID, int unitID, GUIOperation operation, GUISuboperation suboperation,
        //    Purpose purpose, GUIMoment moment, byte suboperationProgress, List<GUIBitmap> bitmaps, string text, GUIResponse response)
        //{
        //    CheckForIllegalCrossThreadCalls = false;
        //    label1.Text = text;
        //    return true;
        //}

        private DialogResult ShowImage(byte[] byteImage)
        {
            DialogResult result = DialogResult.No;

            //if (checkBox1.Checked && byteImage != null)
            //{
            Bitmap byteJPG = RawToBitmap(byteImage, 300, 400);
            //MemoryStream ms = new MemoryStream(byteImage);
            //FormSetImage SI = new FormSetImage(byteJPG);
            //result = SI.ShowDialog();
            picSample.Image = byteJPG;
            //}
            //else
            //{
            //    //MemoryStream ms = null;
            //    //FormSetImage SI = new FormSetImage(ms);
            //    //result = SI.ShowDialog();
            //}

            return result;
        }

        public static Bitmap RawToBitmap(byte[] raw, int width, int height)
        {
            Bitmap img = null;

            try
            {
                img = new Bitmap(width, height, PixelFormat.Format24bppRgb);
                BitmapData bmd = img.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, img.PixelFormat);

                unsafe
                {
                    byte* imgPtr = (byte*)bmd.Scan0.ToPointer();
                    int stride = bmd.Stride;
                    int imgPos = 0;
                    int rawPos = 0;
                    for (int row = 0; row < height; row++)
                    {
                        int rowPos = imgPos;
                        for (int col = 0; col < width; col++)
                        {
                            byte val = raw[rawPos++];
                            imgPtr[rowPos] = imgPtr[rowPos + 1] = imgPtr[rowPos + 2] = val;
                            rowPos += 3;
                        }
                        imgPos += stride;
                    }
                }
                img.UnlockBits(bmd);
            }
            catch (Exception ex)
            {
                //LOG.Error("ImageProcessor.RawToBitmap", ex);
            }
            return img;



        }

        private void button3_Click(object sender, EventArgs e)
        {
            BSP.BSPUnload();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BSP != null)
            {
                BSP.OnCapturedEvent -= OnCaptureEvent;
                BSP.OnTimeoutEvent -= OnTimeoutEvent;
            }
            //_SCUGEN_SENSOR.Off();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                timer1.Enabled = false;
                Sample s = _SamplesCaptured[_SamplesCaptured.Count - 2];
                //System.IO.MemoryStream ms = new System.IO.MemoryStream(s.Data);
                picSample.Image = (Bitmap)s.Data; // Image.FromStream(ms);
                                                  //ms.Close();
                                                  //picSample.Refresh();

                richTextBox1.Text = "Samples Obtenidos:" + Environment.NewLine;
                foreach (Sample item in _SamplesCaptured)
                {
                    richTextBox1.Text += item.AuthenticationFactor + "  " +
                                         item.MinutiaeType + " " +
                                         item.BodyPart + " " +
                                         item.Operation + " " +
                                         item.SampleWith + "x" + item.SampleHeight + " " +
                                         (item.MinutiaeType == 41 ? "Image" : Convert.ToBase64String((byte[])item.Data).Substring(0, 10) + "...")
                                         + Environment.NewLine;
                }

            }
            catch (Exception)
            {

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            byte[] data = System.IO.File.ReadAllBytes(@"C:\std\sample_mt_ISO.bin");
            int size = 52 + data.Length;
            byte[] bir = new byte[size];

            byte[] lenBIR = new byte[4];

            byte[] SHSecurityOption = new byte[1];
            SHSecurityOption[0] = 0;
            Buffer.BlockCopy(SHSecurityOption, 0, bir, 0, SHSecurityOption.Length);

            byte[] PatronHeaderVersion = new byte[1];
            PatronHeaderVersion[0] = 0;
            Buffer.BlockCopy(PatronHeaderVersion, 0, bir, 1, PatronHeaderVersion.Length);

            byte[] BiometricTypes = new byte[2];
            BiometricTypes[0] = 0;
            BiometricTypes[1] = 8;
            Buffer.BlockCopy(BiometricTypes, 0, bir, 2, BiometricTypes.Length);
            byte[] DataType = new byte[2];
            DataType[0] = 0;
            DataType[1] = 2;
            Buffer.BlockCopy(DataType, 0, bir, 4, DataType.Length);

            byte[] Purpose = new byte[1];
            Purpose[0] = 1;
            Buffer.BlockCopy(Purpose, 0, bir, 6, Purpose.Length);

            byte[] Quality = new byte[1];
            Quality = BitConverter.GetBytes(-1);
            Buffer.BlockCopy(Quality, 0, bir, 7, Quality.Length);

            byte[] BSMBFormatOwner = new byte[2];
            BSMBFormatOwner = BitConverter.GetBytes(257);
            //BSMBFormatOwner[0] = 0;
            //BSMBFormatOwner[1] = 0;
            Buffer.BlockCopy(BSMBFormatOwner, 0, bir, 8, BSMBFormatOwner.Length);
            byte[] BSMBFormatType = new byte[2];
            BSMBFormatType[0] = 0;
            BSMBFormatType[1] = 1;
            Buffer.BlockCopy(BSMBFormatType, 0, bir, 10, BSMBFormatType.Length);

            Buffer.BlockCopy(data, 0, bir, 52, data.Length);

            System.IO.File.WriteAllBytes(@"c:\std\BIR_ISO.bin", bir);
        }


        bool _CAPTURING = false;
        string _SERIAL_SENSOR = null;

        private void btnInitContinueCapture_Click(object sender, EventArgs e)
        {
            _CAPTURING = true;

            if (BSP == null)
                InitBSP();

            _SERIAL_SENSOR = SetSerialSelected();

            Error err;
            
            BSP.EnableAutoEvent(_SERIAL_SENSOR, true, (int)this.Handle, out err);
            BSP.On(_SERIAL_SENSOR);

            BSP.CaptureContinue(_SERIAL_SENSOR, 50, out err);
        } 


        private void btnStartContinueCapture_Click(object sender, EventArgs e)
        {
            Error err;
            _SamplesCaptured = null;
            while (_CAPTURING)
            {
                if (!string.IsNullOrEmpty(_SERIAL_SENSOR))
                {
                    richTextBox1.Text = "Capturando desde Sensor = " + _SERIAL_SENSOR + "..." + Environment.NewLine;
                    BSP.Capture(Convert.ToInt32(txtAF.Text), Convert.ToInt32(txtBP.Text), _SERIAL_SENSOR,
                                Convert.ToInt32(txtTimeout.Text), Convert.ToInt32(txtContent.Text),
                                Convert.ToInt32(txtCalidad.Text), out err);
                }
                else
                {
                    richTextBox1.Text = "Capturando desde 1er Sensor configurado en BSP..." + Environment.NewLine;
                    BSP.Capture(Convert.ToInt32(txtAF.Text), Convert.ToInt32(txtBP.Text), null,
                                Convert.ToInt32(txtTimeout.Text), Convert.ToInt32(txtContent.Text),
                                Convert.ToInt32(txtCalidad.Text), out err);
                }
            }
        }

        private void btnStopContinueCapture_Click(object sender, EventArgs e)
        {
            _CAPTURING = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            _CAPTURING = true;

            //_SCUGEN_SENSOR.EnableAutoEvent

        }

        bool _ENABLE_CAPTURE = false;
        private void timerContinuos_Tick(object sender, EventArgs e)
        {
            try
            {
                //timerContinuos.Enabled = false;
                if (_ENABLE_CAPTURE)
                {
                    _ENABLE_CAPTURE = false;
                    _PROCESS_FINGERSAMPLE = true;
                }
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        bool _PROCESS_FINGERSAMPLE = false;
        private void timerProcessFingerSample_Tick(object sender, EventArgs e)
        {
            try
            {
                //timerContinuos.Enabled = false;
                //if (_PROCESS_FINGERSAMPLE)
                //{
                //    _PROCESS_FINGERSAMPLE = false;
                //    //buttonCapture_Click(null, null);
                //    ProcessFingerSample();
                //}
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        public int ProcessFingerSample()
        {
            //try
            //{
            //    if (_SamplesCaptured != null)
            //    {
            //        Sample s;
            //        try
            //        {
            //            if (_SamplesCaptured != null && _SamplesCaptured.Count > 0)
            //            {
            //                if (chkOrigianal.Checked)
            //                {
            //                    s = _SamplesCaptured[_SamplesCaptured.Count - 2];
            //                }
            //                else
            //                {
            //                    s = _SamplesCaptured[_SamplesCaptured.Count - 1];
            //                }
            //                if (s.MinutiaeType == 41)
            //                    picSample.Image = (Bitmap)s.Data;
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            richTextBox1.Text = "Error obteniendo imagen!" + Environment.NewLine;
            //        }

            //        richTextBox1.Text += "Samples Obtenidos:" + Environment.NewLine;
            //        richTextBox1.Text += "----------------------------------------------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;
            //        foreach (Sample item in _SamplesCaptured)
            //        {
            //            string saux = item.SampleWith + "x" + item.SampleHeight;
            //            richTextBox1.Text += "AFactor=" + item.AuthenticationFactor.ToString() +
            //                                 " - MType=" + item.MinutiaeType.ToString() +
            //                                 " - BodyPart=" + item.BodyPart.ToString() +
            //                                 " - Operacion=" + (item.Operation == 0 ? "Verify" : "Enroll") +
            //                                 " - WxH=" + saux +
            //                                 " - Sample=" + (item.MinutiaeType == 41 ? "Image" : Convert.ToBase64String((byte[])item.Data).Substring(0, 10) + "...")
            //                                 + Environment.NewLine;
            //            //if (chkSaveHDD.Checked)
            //            //{
            //            //    string sauxname;
            //            //    sauxname = "AF-" + item.AuthenticationFactor.ToString() +
            //            //                     "_MT-" + item.MinutiaeType.ToString() +
            //            //                     "_BP-" + item.BodyPart.ToString() +
            //            //                     "_OP-" + (item.Operation == 0 ? "V" : "E") +
            //            //                     "_WxH-" + saux;
            //            //    if (item.MinutiaeType == 41)
            //            //    {
            //            //        System.IO.File.WriteAllBytes(Properties.Settings.Default.RootPath + "\\data\\" + sauxname + ".jpg", (byte[])item.Data);
            //            //    }
            //            //    else
            //            //    {
            //            //        System.IO.File.WriteAllText(Properties.Settings.Default.RootPath + "\\data\\" + sauxname + ".txt", Convert.ToBase64String((byte[])item.Data));
            //            //    }
            //            //}
            //        }
            //        richTextBox1.Text += "----------------------------------------------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;

            //        string aux = "";
            //        int idx = 0;
            //        if (chkSaveHDD.Checked)
            //        {
            //            bool save = false;
            //            foreach (Sample item in _SamplesCaptured)
            //            {
            //                save = false;
            //                if (item.MinutiaeType == 41)
            //                {
            //                    //System.IO.File.WriteAllBytes(@"c:\std\sample_mt_41_" + (idx++) + ".bmp", (byte[])item.Data);
            //                    //Bitmap b = (Bitmap)item.Data;
            //                    //b.Save(@"c:\std\sample_mt_41_" + (idx++) + ".jpg", ImageFormat.Jpeg);
            //                }
            //                else
            //                {
            //                    if (item.MinutiaeType == 13)
            //                    {
            //                        aux = "ANSI";
            //                        save = true;
            //                    }
            //                    if (item.MinutiaeType == 14)
            //                    {
            //                        aux = "ISO";
            //                        save = true;
            //                    }
            //                    if (item.MinutiaeType == 4)
            //                    {
            //                        aux = "DP";
            //                        save = true;
            //                    }
            //                    if (item.MinutiaeType == 5)
            //                    {
            //                        aux = "SECUGEN";
            //                        save = true;
            //                    }
            //                    if (item.MinutiaeType == 21)
            //                    {
            //                        aux = "WSQ";
            //                        save = true;
            //                    }
            //                    if (item.MinutiaeType == 22)
            //                    {
            //                        aux = "RAW";
            //                        save = true;
            //                    }
            //                    if (save)
            //                    {
            //                        string saux = item.SampleWith + "x" + item.SampleHeight;
            //                        string sauxname = "AF-" + item.AuthenticationFactor.ToString() +
            //                                             "_MT-" + item.MinutiaeType.ToString() +
            //                                             "_BP-" + item.BodyPart.ToString() +
            //                                             "_OP-" + (item.Operation == 0 ? "V" : "E") +
            //                                             "_WxH-" + saux + "_";
            //                        System.IO.File.WriteAllText(Properties.Settings.Default.RootPath + "\\data\\" + sauxname + aux + ".txt", Convert.ToBase64String((byte[])item.Data));
            //                        System.IO.File.WriteAllBytes(Properties.Settings.Default.RootPath + "\\data\\" + sauxname + aux + ".bin", (byte[])item.Data);
            //                        save = false;
            //                    }
            //                }
            //            }
            //        }
            //        this.Refresh();
            //        System.Threading.Thread.Sleep(3000);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //    //LOG.Error(" Error: " + ex.Message);
            //}

            ////if (_CAPTURING)
            ////{
            //    buttonCapture_Click(null, null);
            //    //BSP.On("H54181019063");
            ////}

            return 0;
        }


        internal static string[] ConvertToHexa(byte[] data)
        {
            byte[] _temp = new Byte[4000];
            Int32 _isoLength = 0, _isocLength = 0;
            try
            {
                //log.Debug("Utils.ConvertToHexa Reorder Minutiae IN...");
                //Re-ordenamiento de minucias
                for (Int32 n = 0; n < data.Length; n++)
                {
                    _temp[n] = data[n + 28];

                    if (n > 0 && _temp[n - 1] == 0x00)
                    {
                        _isocLength = n;

                        break;
                    }
                }

                _isocLength = _isocLength - 1;

                Byte[] _isocTemplate = new Byte[_isocLength];

                for (Int32 n = 0; n < _isocLength; n++)
                {
                    _isocTemplate[n] = _temp[n];
                }
                //log.Debug("Utils.ConvertToHexa Reorder Minutiae OUT!");

                string _isoCCtoHexa = BitConverter.ToString(_isocTemplate);
                return _isoCCtoHexa.Split('-');
            }
            catch (Exception ex)
            {
                //log.Error("Utils.ConvertToHexa Error: " + ex.Message);
                return null;
            }
        }

        static string ConvertStringArrayToString(string[] array)
        {
            //
            // Concatenate all the elements into a StringBuilder.
            //
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
            }
            return builder.ToString();
        }

        private void tabPage0_Click(object sender, EventArgs e)
        {

        }
    }
}
