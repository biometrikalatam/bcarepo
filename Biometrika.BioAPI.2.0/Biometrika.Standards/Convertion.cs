﻿using log4net;
using Neurotec;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Biometrics.Standards;
using Neurotec.Images;
using Neurotec.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Standards
{

    /// <summary>
    /// Clase destinada a la congversión de templates entre diferetnes formatos. 
    /// Permite pasar RAW/WSQ o minucias a estandares CBEEF y viceversa. 
    /// </summary>
    public static class Convertion
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Convertion));
        /// <summary>
        /// Dado un sample origen con un tipo, lo convierte al tipo de destino
        /// </summary>
        /// <param name="sampleorigin"></param>
        /// <param name="typeorigin"></param>
        /// <param name="typedestination"></param>
        /// <param name="sampledestination"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        public static int Convert(byte[] sampleorigin, int typeorigin, int typedestination, out byte[] sampledestination, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            sampledestination = null;

            try
            {
                LOG.Debug("Biometrika.Standards.Convertion.Convert IN...");

                if (!NTUtils.CheckLicenseNT(null))
                {
                    ret = Constant.ERROR_INVALID_LICENSE;
                    msgerr = "Licencia NT erronea!";
                    LOG.Warn("Biometrika.Standards.Convertion.Convert Conversion - Licencia NT erronea!");
                    return ret;
                }

                LOG.Debug("Biometrika.Standards.Convertion.Convert Conversion de TypeOrigin=" + typeorigin + " a DestinationOrigin=" + typedestination + "...");
                switch (typeorigin)
                {
                    case 1: //Constant.SAMPLE_FORMAT_IMAGE_RAW
                        if (typedestination == Constant.DESTINATION_FORMAT_CBEEF_ISO)
                        {
                            ret = ConvertRAWtoNTemplateCBEEFISO(sampleorigin, out sampledestination, out msgerr);
                        }
                        else if (typedestination == Constant.DESTINATION_FORMAT_CBEEF_ANSI)
                        {
                            ret = ConvertRAWtoNTemplateCBEEFANSI(sampleorigin, out sampledestination, out msgerr);
                        }
                        else
                        {
                            ret = Constant.ERROR_CONVERTION_NOT_SUPPORTED;
                            sampledestination = null;
                            msgerr = "Conversion de TypeOrigin=" + typeorigin + " a DestinationOrigin=" + typedestination + " NO Soportado!";
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Biometrika.Standards.Convertion.Convert Ex Error " + ex.Message);
            }
            LOG.Debug("Biometrika.Standards.Convertion.Convert ret = " + ret + " - msg=" + msgerr);
            LOG.Debug("Biometrika.Standards.Convertion.Convert OUT!");
            return ret;
        }

#region IMAGE

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampleorigin"></param>
        /// <param name="sampledestination"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static int ConvertRAWtoNTemplateCBEEFISO(byte[] sampleorigin, out byte[] sampledestination, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            sampledestination = null;

            //Pasos:
            //  1.- Convertir RAW a FIRecord
            //  2.- Convertir FIRecord to NTemplate
            //  3.- Convertir NTEmplate to CBEEF

            try
            {
                LOG.Debug("Biometrika.Standards.Convertion.ConvertRAWtoNTemplateCBEEFISO IN...");

                if (!NTUtils.CheckLicenseNT(null))
                {
                    ret = Constant.ERROR_INVALID_LICENSE;
                    msgerr = "Licencia NT erronea!";
                    LOG.Warn("Biometrika.Standards.Convertion.ConvertRAWtoNTemplateCBEEFISO Conversion - Licencia NT erronea!");
                    return ret;
                }

                //  1.- Convertir RAW a FIRecord
                byte[] FIRecordTemp;
                ret = ConvertRAWtoFIRecord(sampleorigin, out FIRecordTemp, out msgerr);
                if (ret < 0) return ret;

                //  2.- Convertir FIRecord to NTemplate
                byte[] NTemplateTemp;
                ret = ConvertFIRecordToNTemplate(FIRecordTemp, out NTemplateTemp, out msgerr);
                if (ret < 0) return ret;


                //  3.- Convertir NTEmplate to CBEEF
                ret = ConvertNTemplateToCBEEF(NTemplateTemp, Constant.DESTINATION_FORMAT_CBEEF_ISO, out sampledestination, out msgerr);
            }
            catch (Exception ex)
            {
                ret = Constant.ERROR_UNKNOW;
                LOG.Error("Biometrika.Standards.Convertion.ConvertRAWtoNTemplateCBEEFISO Ex Error " + ex.Message);
            }
            LOG.Debug("Biometrika.Standards.Convertion.ConvertRAWtoNTemplateCBEEFISO ret = " + ret);
            LOG.Debug("Biometrika.Standards.Convertion.ConvertRAWtoNTemplateCBEEFISO OUT!");
            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampleorigin"></param>
        /// <param name="sampledestination"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static int ConvertRAWtoFIRecord(byte[] sampleorigin, out byte[] sampledestination, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            sampledestination = null;
            FIRecord fi = null;

            try
            {
                LOG.Debug("Biometrika.Standards.Convertion.ConvertRAWtoFIRecord IN...");

                if (!NTUtils.CheckLicenseNT(null))
                {
                    ret = Constant.ERROR_INVALID_LICENSE;
                    msgerr = "Licencia NT erronea!";
                    LOG.Warn("Biometrika.Standards.Convertion.ConvertRAWtoFIRecord Conversion - Licencia NT erronea!");
                    return ret;
                }

                var standard = BdifStandard.Iso; 
                NVersion version = FIRecord.VersionIso20;

                LOG.Debug("Biometrika.Standards.Convertion.ConvertRAWtoFIRecord - Init Convertion RAW to FIRecrod...");
                //using (NImage imageFromByte = NImage.FromMemory(sampleorigin))
                //using (NImage grayscaleImage = NImage.FromImage(NPixelFormat.Grayscale8U, 0, imageFromByte))
                using (NBuffer buffer = new NBuffer(sampleorigin)) { 
                    using (NImage grayscaleImage = NImage.FromData(NPixelFormat.Grayscale8U, 512, 512, 0, 512, buffer))
                    {
                        if (grayscaleImage.ResolutionIsAspectRatio
                                || grayscaleImage.HorzResolution < 250
                                || grayscaleImage.VertResolution < 250)
                        {
                            grayscaleImage.HorzResolution = 500;
                            grayscaleImage.VertResolution = 500;
                            grayscaleImage.ResolutionIsAspectRatio = false;
                        }

                        if (fi == null)
                        {
                            fi = new FIRecord(standard, version);
                            if (IsRecordFirstVersion(fi))
                            {
                                fi.PixelDepth = 8;
                                fi.HorzImageResolution = (ushort)grayscaleImage.HorzResolution;
                                fi.HorzScanResolution = (ushort)grayscaleImage.HorzResolution;
                                fi.VertImageResolution = (ushort)grayscaleImage.VertResolution;
                                fi.VertScanResolution = (ushort)grayscaleImage.VertResolution;
                            }
                        }
                        LOG.Debug("Biometrika.Standards.Convertion.ConvertRAWtoFIRecord - Adding FirFingerView...");
                        FirFingerView fingerView = new FirFingerView(fi.Standard, fi.Version);
                        if (!IsRecordFirstVersion(fi))
                        {
                            fingerView.PixelDepth = 8;
                            fingerView.HorzImageResolution = (ushort)grayscaleImage.HorzResolution;
                            fingerView.HorzScanResolution = (ushort)grayscaleImage.HorzResolution;
                            fingerView.VertImageResolution = (ushort)grayscaleImage.VertResolution;
                            fingerView.VertScanResolution = (ushort)grayscaleImage.VertResolution;
                        }
                        fi.FingerViews.Add(fingerView);
                        fingerView.SetImage(grayscaleImage);
                    }
                }

                if (fi != null)
                {
                    sampledestination = fi.Save().ToArray();
                    LOG.Warn("Biometrika.Standards.Convertion.ConvertRAWtoFIRecord - Sample Destination OK = " + System.Convert.ToBase64String(sampledestination));
                    ret = Constant.ERROR_OK;
                }
                else
                {
                    LOG.Warn("Biometrika.Standards.Convertion.ConvertRAWtoFIRecord - No se agrego Sample RAW a FIRecord!");
                    ret = Constant.ERROR_CONVERTION_NOT_PROCESSED;
                }
            }
            catch (Exception ex)
            {
                ret = Constant.ERROR_UNKNOW;
                LOG.Error("Biometrika.Standards.Convertion.ConvertRAWtoFIRecord Ex Error " + ex.Message);
            }

            LOG.Debug("Biometrika.Standards.Convertion.ConvertRAWtoFIRecord Out! ret = " + ret);
            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampleorigin"></param>
        /// <param name="sampledestination"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static int ConvertFIRecordToNTemplate(byte[] sampleorigin, out byte[] sampledestination, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            sampledestination = null;
            FIRecord fi = null;

            try
            {
                LOG.Debug("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate IN...");

                if (!NTUtils.CheckLicenseNT(null))
                {
                    ret = Constant.ERROR_INVALID_LICENSE;
                    msgerr = "Licencia NT erronea!";
                    LOG.Warn("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate Conversion - Licencia NT erronea!");
                    return ret;
                }

                LOG.Debug("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate - Init Convertion FIRecrod to NTemplate...");
                using (var biometricClient = new NBiometricClient())
                using (var subject = new NSubject())
                {
                    // Read FIRecord from param
                    byte[] fiRecordData = sampleorigin;

                    LOG.Debug("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate - Create FIRecord...");
                    // Create FIRecord
                    var standard = BdifStandard.Iso;
                    NVersion version = FIRecord.VersionIso20;
                    var fiRec = new FIRecord(fiRecordData, BdifStandard.Iso);

                    LOG.Debug("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate - Readd All images from FIRecord...");
                    // Read all images from FIRecord
                    foreach (FirFingerView fv in fiRec.FingerViews)
                    {
                        var finger = new NFinger { Image = fv.ToNImage() };
                        subject.Fingers.Add(finger);
                    }

                    // Set finger template size (recommended, for enroll to database, is large) (optional)
                    biometricClient.FingersTemplateSize = NTemplateSize.Large;

                    LOG.Debug("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate - Create NTemplate...");
                    // Create template from added finger image(s)
                    var status = biometricClient.CreateTemplate(subject);

                    // Save template to file
                    if (status == NBiometricStatus.Ok)
                    {
                        sampledestination = subject.GetTemplateBuffer().ToArray();
                        LOG.Warn("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate - Sample Destination OK = " + System.Convert.ToBase64String(sampledestination));
                        ret = Constant.ERROR_OK;
                    } else
                    {
                        LOG.Warn("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate - No se agrego Sample RAW a FIRecord!");
                        ret = Constant.ERROR_CONVERTION_NOT_PROCESSED;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Constant.ERROR_UNKNOW;
                LOG.Error("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate Ex Error " + ex.Message);
            }

            LOG.Debug("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate Out! ret = " + ret);
            return ret;
        }

        internal static int ConvertNTemplateToCBEEF(byte[] sampleorigin, int destinationcbeeftype, out byte[] sampledestination, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            sampledestination = null;
            FIRecord fi = null;

            try
            {
                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF IN...");

                if (!NTUtils.CheckLicenseNT(null))
                {
                    ret = Constant.ERROR_INVALID_LICENSE;
                    msgerr = "Licencia NT erronea!";
                    LOG.Warn("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF Conversion - Licencia NT erronea!");
                    return ret;
                }

                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Init Convertion FIRecrod to NTemplate...");

                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Reat NTemplate from param...");
                // Read NTemplate buffer
                var packedNTemplate = new NBuffer(sampleorigin);

                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Set format BDB...");
                // Combine NTemplate BDB format
                uint bdbFormat = BdifTypes.MakeFormat(CbeffBiometricOrganizations.Neurotechnologija, CbeffBdbFormatIdentifiers.NeurotechnologijaNTemplate);

                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Set Patron...");
                // Get CbeffRecord patron format
                // all supported patron formats can be found in CbeffRecord class documentation
                uint patronFormat = CbeffRecord.PatronFormatIsoIecJtc1SC37BiometricsSimpleByteOriented; // uint.Parse(args[2], System.Globalization.NumberStyles.HexNumber);

                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Creating CBEEF Record...");
                // Create CbeffRecord from NTemplate buffer
                using (var cbeffRecord = new CbeffRecord(bdbFormat, packedNTemplate, patronFormat))
                {
                    sampledestination = cbeffRecord.Save().ToArray();
                    if (sampledestination == null)
                    {
                        LOG.Warn("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate - No se agrego Sample RAW a FIRecord!");
                        ret = Constant.ERROR_CONVERTION_NOT_PROCESSED;
                    }
                    else
                    {
                        LOG.Warn("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate - Sample Destination OK = " + System.Convert.ToBase64String(sampledestination));
                        ret = Constant.ERROR_OK;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Constant.ERROR_UNKNOW;
                LOG.Error("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate Ex Error " + ex.Message);
            }

            LOG.Debug("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate Out! ret = " + ret);
            return ret;
        }






        internal static int ConvertRAWtoNTemplate(byte[] sampleorigin, out byte[] sampledestination, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            sampledestination = null;

           
            try
            {
                LOG.Debug("Biometrika.Standards.Convertion IN...");

                //1.- Convertir RAW a FIRecord
                //ret = ConvertRAWtoFIRecord()



            }
            catch (Exception ex)
            {
                LOG.Error("Biometrika.Standards.Convertion Ex Error " + ex.Message);
            }

            LOG.Debug("Biometrika.Standards.Convertion OUT!");
            return ret;
        }

        internal static int ConvertWSQtoNTemplate(byte[] sampleorigin, out byte[] sampledestination, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            sampledestination = null;



            return ret;
        }

        internal static int ConvertRAWtoNTemplateCBEEFANSI(byte[] sampleorigin, out byte[] sampledestination, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            sampledestination = null;


            try
            {
                LOG.Debug("Biometrika.Standards.Convertion IN...");

                //1.- Convertir RAW a FIRecord
                //ret = ConvertRAWtoFIRecord()



            }
            catch (Exception ex)
            {
                LOG.Error("Biometrika.Standards.Convertion Ex Error " + ex.Message);
            }

            LOG.Debug("Biometrika.Standards.Convertion OUT!");
            return ret;
        }


        public static int ConvertRAWToCBEEF(byte[] sampleorigin, int destinationcbeeftype, out byte[] sampledestination, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            sampledestination = null;
            FIRecord fi = null;

            try
            {
                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF IN...");

                if (!NTUtils.CheckLicenseNT(null))
                {
                    ret = Constant.ERROR_INVALID_LICENSE;
                    msgerr = "Licencia NT erronea!";
                    LOG.Warn("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF Conversion - Licencia NT erronea!");
                    return ret;
                }

                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Init Convertion FIRecrod to NTemplate...");

                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Reat NTemplate from param...");
                // Read NTemplate buffer
                var packedNTemplate = new NBuffer(sampleorigin);

                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Set format BDB...");
                // Combine NTemplate BDB format
                uint bdbFormat = BdifTypes.MakeFormat(CbeffBiometricOrganizations.SecuGen, CbeffBdbFormatIdentifiers.IncitsTCM1BiometricsWsqImage);

                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Set Patron...");
                // Get CbeffRecord patron format
                // all supported patron formats can be found in CbeffRecord class documentation
                uint patronFormat = CbeffRecord.PatronFormatIsoIecJtc1SC37BiometricsSimpleByteOriented; // uint.Parse(args[2], System.Globalization.NumberStyles.HexNumber);


                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Creating CBEEF Record...");
                // Create CbeffRecord from NTemplate buffer
                using (var cbeffRecord = new CbeffRecord(patronFormat))
                {
                    cbeffRecord.BdbBuffer = new NBuffer(sampleorigin);
                    cbeffRecord.BdbFormat = bdbFormat;
                    cbeffRecord.BiometricType = CbeffBiometricType.Finger;
                    cbeffRecord.BirCreationDate = DateTime.Now;
                    cbeffRecord.BdbCreationDate = DateTime.Now;
                    cbeffRecord.CompressionAlgorithmId = CbeffCompressionAlgorithmIdentifiers.IncitsTCM1BiometricsWsq;
                    cbeffRecord.Creator = "Biometrika";
                    //cbeffRecord.IntegrityOptions = CbeffIntegrityOptions.Signed;
                    cbeffRecord.ProcessedLevel = CbeffProcessedLevel.Processed;
                    cbeffRecord.Purpose = CbeffPurpose.EnrollForVerificationOnly;

                    sampledestination = cbeffRecord.Save().ToArray();
                    if (sampledestination == null)
                    {
                        LOG.Warn("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate - No se agrego Sample RAW a FIRecord!");
                        ret = Constant.ERROR_CONVERTION_NOT_PROCESSED;
                    }
                    else
                    {
                        LOG.Warn("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate - Sample Destination OK = " + System.Convert.ToBase64String(sampledestination));
                        ret = Constant.ERROR_OK;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Constant.ERROR_UNKNOW;
                LOG.Error("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate Ex Error " + ex.Message);
            }

            LOG.Debug("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate Out! ret = " + ret);
            return ret;
        }

        public static int ConvertCBEEFToRAW(byte[] sampleorigin, int destinationcbeeftype, out byte[] sampledestination, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            sampledestination = null;
            FIRecord fi = null;

            try
            {
                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF IN...");

                if (!NTUtils.CheckLicenseNT(null))
                {
                    ret = Constant.ERROR_INVALID_LICENSE;
                    msgerr = "Licencia NT erronea!";
                    LOG.Warn("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF Conversion - Licencia NT erronea!");
                    return ret;
                }

                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Init Convertion FIRecrod to NTemplate...");

                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Reat NTemplate from param...");

                
                // Read NTemplate buffer
                var packedNTemplate = new NBuffer(sampleorigin);
                //CbeffRecord cbeffRecord = new CbeffRecord()

           


                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Set format BDB...");
                // Combine NTemplate BDB format
                uint bdbFormat = BdifTypes.MakeFormat(CbeffBiometricOrganizations.SecuGen, CbeffBdbFormatIdentifiers.IncitsTCM1BiometricsWsqImage);

                LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Set Patron...");
                // Get CbeffRecord patron format
                // all supported patron formats can be found in CbeffRecord class documentation
                uint patronFormat = CbeffRecord.PatronFormatIsoIecJtc1SC37BiometricsSimpleByteOriented; // uint.Parse(args[2], System.Globalization.NumberStyles.HexNumber);
                


                CbeffRecord cbeffRecord = new CbeffRecord(bdbFormat, packedNTemplate, patronFormat);
                int i = 0;

                //LOG.Debug("Biometrika.Standards.Convertion.ConvertNTemplateToCBEEF - Creating CBEEF Record...");
                //// Create CbeffRecord from NTemplate buffer
                //using (var cbeffRecord = new CbeffRecord(bdbFormat, packedNTemplate, patronFormat))
                //{
                //    sampledestination = cbeffRecord.Save().ToArray();
                //    if (sampledestination == null)
                //    {
                //        LOG.Warn("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate - No se agrego Sample RAW a FIRecord!");
                //        ret = Constant.ERROR_CONVERTION_NOT_PROCESSED;
                //    }
                //    else
                //    {
                //        LOG.Warn("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate - Sample Destination OK = " + System.Convert.ToBase64String(sampledestination));
                //        ret = Constant.ERROR_OK;
                //    }
                //}
            }
            catch (Exception ex)
            {
                ret = Constant.ERROR_UNKNOW;
                LOG.Error("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate Ex Error " + ex.Message);
            }

            LOG.Debug("Biometrika.Standards.Convertion.ConvertFIRecordToNTemplate Out! ret = " + ret);
            return ret;
        }

        #endregion IMAGE



        #region Auxiliares


        private static bool IsRecordFirstVersion(FIRecord record)
        {
            return record.Standard == BdifStandard.Ansi && record.Version == FIRecord.VersionAnsi10
                || record.Standard == BdifStandard.Iso && record.Version == FIRecord.VersionIso10;
        }



#endregion Auxiliares

    }
}
