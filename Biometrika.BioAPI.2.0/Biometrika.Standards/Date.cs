﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Standards
{
    /// <summary>
    /// Represents calendar date. For security reasons no methods to modify the existing Date object are provided.
    /// Instead, an application should use the corresponding method of the DataFactory to create new Date containing modified data.
    /// </summary>
    /// <author>
    /// See CREDITS.txt
    /// </author>
    public class Date
    {
        public int DayOfMonth { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        public int Second { get; set; }

        public Date(int dayOfMonth, int month, int year, int hour, int minute, int second)
        {
            DayOfMonth = dayOfMonth;
            Month = month;
            Year = year;
            Hour = hour;
            Minute = minute;
            Second = second;
        }

        /// <summary>
        /// Compares the date and time of the object within the interface, with the one represented by the parameters of called method. 
        /// The name of the method refers to the following operations: "date and time given by parameters" <= "object's date and time"
        /// </summary>
        /// <param name="Day">
        /// day of the month for the date to be compared with the object's date
        /// </param>
        /// <param name="Month">
        /// month for the date to be compared with the object's date
        /// </param>        
        /// <param name="Year">
        /// year for the date to be compared with the object's date
        /// </param>        
        /// <param name="Hour">
        /// hour for the date to be compared with the object's date
        /// </param>        
        /// <param name="Minute">
        /// minute for the date to be compared with the object's date
        /// </param>        
        /// <param name="Second">
        /// secondfor the date to be compared with the object's date
        /// </param>        
        /// <param name="Date">
        /// date to be compared with the object's date
        /// </param>
        /// <returns>
        /// True if the condition is met, False in any other case
        /// </returns>
        bool IsLowerOrEqual(int day, int month, int year)
        {
            return true;
        }

        bool IsLowerOrEqual(int day, int month, int year, int hour, int minute, int second)
        {
            return true;
        }

        bool IsLowerOrEqual(Date date)
        {
            return true;
        }

        /// <summary>
        /// Compares the date and time of the object within the interface, with the one represented by the parameters of called method. 
        /// The name of the method refers to the following operations: "date and time given by parameters" >= "object's date and time"
        /// </summary>
        /// <param name="day">
        /// day of the month for the date to be compared with the object's date
        /// </param>
        /// <param name="month">
        /// month for the date to be compared with the object's date
        /// </param>        
        /// <param name="year">
        /// year for the date to be compared with the object's date
        /// </param>        
        /// <param name="Hour">
        /// hour for the date to be compared with the object's date
        /// </param>        
        /// <param name="Minute">
        /// minute for the date to be compared with the object's date
        /// </param>        
        /// <param name="Second">
        /// secondfor the date to be compared with the object's date
        /// </param>        
        /// <param name="Date">
        /// date to be compared with the object's date
        /// </param>
        /// <returns>
        /// True if the condition is met, False in any other case
        /// </returns>
        bool IsHigherOrEqual(int day, int month, int year)
        {
            return true;
        }

        bool IsHigherOrEqual(int day, int month, int year, int hour, int minute, int second)
        {
            return true;
        }

        bool IsHigherOrEqual(Date date)
        {
            return true;
        }

    }
}
