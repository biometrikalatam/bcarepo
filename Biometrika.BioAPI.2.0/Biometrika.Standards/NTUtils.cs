﻿using log4net;
using Neurotec.Licensing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Standards
{
    internal class NTUtils
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NTUtils));

        public static bool IsLicenseConfigured;

        private static Dictionary<string, string> _licenseCfg;

        //private const string LicensesConfiguration = "NLicenses.cfg";
        private const string ComponentLicensesToCheck = "Biometrics.FingerExtraction,Biometrics.Standards.Fingers";

        /// <summary>
        /// Inicializa la licencia la primera vez que se usa una clase de este Namespace.
        /// </summary>
        public static bool CheckLicenseNT(string parameters)
        {
            //IList<string> licensesMain = new List<string>(new string[] { "FingersExtractor", "FingersMatcher" });

            try
            {
                LOG.Debug("Biometrika.Standards.NTUtils.CheckLicenseNT IN...");
                string server = Properties.Settings.Default.NTServerLicense;
                int port = Convert.ToInt32(Properties.Settings.Default.NTPortLicense);
                string componentlicensecfg = ComponentLicensesToCheck; // ExtractParams(parameters, out server, out port);

                if (!IsLicenseConfigured)
                {
                    if (componentlicensecfg == null)
                    {
                        LOG.Error("Biometrika.Standards.NTUtils.CheckLicenseNT Error - Component License Cfg = NULL");
                    }

                    LOG.Debug("Biometrika.Standards.NTUtils.CheckLicenseNT - Entrando a VfeUtils.ObtainLicenses");
                    IsLicenseConfigured = NLicense.ObtainComponents(server, port, componentlicensecfg);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Verifinger7.VfeUtils.ConfigureLicense Error", ex);
            }
            LOG.Debug("Biometrika.Standards.NTUtils.CheckLicenseNT OUT! - IsLicenseConfigured=" + IsLicenseConfigured.ToString());
            return IsLicenseConfigured;
            //finally
            //{
            //    Utils.ReleaseLicenses(licensesMain);
            //    Utils.ReleaseLicenses(licensesBss);
            //}
        }

        //Extrae el pathj de NLicense.cfg de los parametros
        // Formato: PathLicenseCfg=C:\\NLicense.cfg
        private static string ExtractPath(string parameters)
        {
            string path = null;
            try
            {
                string[] arr = parameters.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                if (arr.Length == 0) return null;

                for (int i = 0; i < arr.Length; i++)
                {
                    string[] item = parameters.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                    if (item[0].Equals("ComponentLicenseCfg") || item[0].Equals("ConfigLicense"))
                    {
                        path = item[1];
                        break;
                    }
                }

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Verifinger7.VfeUtils.ExtractPath Error", ex);
            }
            return path;
        }


        //Extrae el pathj de NLicense.cfg de los parametros
        // Formato: Server=/local|Port=5000|ComponentLicenseCfg=Biometrics.FingerExtraction,Biometrics.FingerMatching
        private static string ExtractParams(string parameters, out string server, out int port)
        {
            server = "/local";
            port = 5000;
            string path = null;
            try
            {
                string[] arr = parameters.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                LOG.Debug("ExtractParams arr.length=" + arr.Length.ToString());
                if (arr.Length == 0) return null;

                for (int i = 0; i < arr.Length; i++)
                {
                    LOG.Debug("ExtractParams procesando arr[" + i.ToString() + "]=" + arr[i]);
                    string[] item = arr[i].Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                    LOG.Debug("ExtractParams item.length=" + item.Length.ToString());
                    if (item[0].Equals("ComponentLicenseCfg") || item[0].Equals("ConfigLicense"))
                    {
                        path = item[1];
                        LOG.Debug("ExtractParams path=" + path);
                    }
                    if (item[0].Equals("Server") || item[0].Equals("server"))
                    {
                        server = item[1];
                        LOG.Debug("ExtractParams Server=" + server);
                    }
                    if (item[0].Equals("Port") || item[0].Equals("port"))
                    {
                        try
                        {
                            port = Convert.ToInt32(item[1].Trim());
                        }
                        catch (Exception ex)
                        {
                            LOG.Debug("ExtractParams Parse Port Exr=" + ex.Message);
                            port = 5000;
                        }
                        LOG.Debug("ExtractParams Port=" + port.ToString());
                    }

                }

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Verifinger7.VfeUtils.ExtractParams Error", ex);
            }
            return path;
        }
    }
}
