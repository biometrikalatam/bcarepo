﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Standards
{
    public class DataFormat
    {
        public int Owner { get; set; }
        public int Type { get; set; }

        public DataFormat(int owner, int type)
        {
            Owner = owner;
            type = Type;
        }
    }
    /*
              000A	10	Secugen Corporation	Dan Riley	USA	N
              0011	17	NEC Solutions America, Inc.	Theresa Giordano	USA	N
              0012	18	UPEK, Inc.	Vito Fabbrizio	USA	N
              0017	23	Cogent Systems, Inc.	James Jasinski	USA	N
              0018	24	Cross Match Technologies, Inc.	Greg Cannon	USA	N
              0019	25	Recognition Systems, Inc.	Frank Perry	USA	N
              001D	29	Sagem Morpho	Creed Jones	USA	N
              0031	49	Neurotechnologija	Irmantas Naujikas	Lithuania	N
              0033	51	DigitalPersona, Inc.	Brian Farley	USA	N
              0035	53	Innovatrics	Jan Lunter	France	N
              003A	58	Griaule Tecnologia LTDA	Iron Calil Daher	Brazil	software
              003B	59	Aware, Inc.	David Benini	USA	fingerprint template
              0044	68	Suprema, Inc.	Brian Song	Korea	fingerprint
              004E	78	IriTech, Inc.	Joyce Y. Kim	USA	iris
              0103	259	Vendor Unknown	Lisa Rajchel	USA	Standards Development Organization
    */
    public enum Owners
    {
        Secugen = 10,
        NEC = 17,
        UPEK = 18,
        Cogent = 23,
        CrossMatch = 24,
        RecognitionSystems = 25,
        ANSI = 27,
        SagemMorpho = 29,
        Neurotechnologija = 49,
        DigitalPersona = 51,
        Innovatrics = 53,
        Griaule = 58,
        Aware = 59,
        Suprema = 68,
        IriTech = 78,
        ISO = 257,
        VendorUnknown = 259
    }

    /*
    INCITS Technical Committee M1	001B	27	0201	513	    Finger Minutiae - no extended data	ANSI/INCITS 378-2004
    INCITS Technical Committee M1	001B	27	0401	1025	Finger Image format                 ANSI/INCITS 381-2004
    INCITS Technical Committee M1	001B	27	0501	1281	Face Image Format	                ANSI/INCITS 385-2004
    INCITS Technical Committee M1	001B	27	8003	32771	JPEG image	                        ISO/IEC 10918 - Information Technology - Digital Compression and coding of continuous-tone still images (JPEG)
    INCITS Technical Committee M1	001B	27	8004	32772	JPEG2000 image	                    ISO/IEC 15444 - Information Technology - JPEG 2000 Image Coding System
    INCITS Technical Committee M1	001B	27	8005	32773	TIFF image	                        ISO/IEC 12639 - Tag image file format for image technology (TIFF/IT)
    ISO/IEC JTC 1 SC 37-Biometrics	0101	257	0001	1	    finger-minutiae-record-n	        ISO/IEC 19794-2 {iso registration-authority cbeff(19785) biometric-organization(0) jtc1-sc37(257) bdbs(0) finger-minutiae-record-n (1)} See ISO/IEC 19794-2:2005/Cor.1:2007 Table 16 (Finger minutiae, record format, no extended data)
    ISO/IEC JTC 1 SC 37-Biometrics	0101	257	0007	7	    finger-image	                    ISO/IEC 19794-4 {iso registration-authority cbeff(19785) biometric-organization(0) 257 bdbs(0) finger-image (7)}
    ISO/IEC JTC 1 SC 37-Biometrics	0101	257	0008	8	    face-image	face-image              ISO/IEC 19794-5 {iso registration-authority cbeff(19785) biometric-organization(0) 257 bdbs(0) face-image (8)}
    ISO/IEC JTC 1 SC 37-Biometrics	0101	257	001D	29	    finger-minutiae-record-format	    ISO/IEC 19794-2 {iso registration-authority cbeff(19785)biometric-organization(0) 257 bdbs(0) finger-minutiae-record-format (29)}
    ISO/IEC JTC 1 SC 37-Biometrics	0101	257	0005	5	    finger-minutiae-card-compact-v-nh	ISO/IEC 19794-2 with Cor. 1 {iso registration-authority cbeff(19785) biometric-organization(0) 257 bdbs(0) finger-minutiae-card-compact-v-nh (5)} See ISO/IEC 19794-2:2005/Cor.1:2007 Table 16 (Finger minutiae, card compact size format, valley bifurcations with no headers) 
    INCITS Technical Committee M1	001B	27	8001	32769	WSQ compressed fingerprint image	IAFIS-IC-0010 (V3) - WSQ Gray-scale fingerprint image compression specification; 1997
     */
    public enum BDBTypes
    {
        ISO_MINUTIAE_19794_2 = 1,	        //finger-minutiae-record-n            ISO/IEC 19794-2 {iso registration-authority cbeff(19785) biometric-organization(0) jtc1-sc37(257) bdbs(0) finger-minutiae-record-n (1)}See ISO/IEC 19794-2:2005/Cor.1:2007 Table 16 (Finger minutiae, record format, no extended data)
        ISO_FINGER_IMAGE_19794_4 = 7,       //finger-image                        ISO/IEC 19794-4 {iso registration-authority cbeff(19785) biometric-organization(0) 257 bdbs(0) finger-image(7)}
        ISO_FACE_IMAGE_19794_4 = 8,         //face-image face-image ISO/IEC 19794-5 {iso registration-authority cbeff(19785) biometric-organization(0) 257 bdbs(0) face-image(8)}
        ISO_MINUTIAE_COMPACT_19794_2 = 5,   //finger-minutiae-card-compact-v-nh ISO/IEC 19794-2 with Cor. 1 { iso registration-authority cbeff(19785) biometric - organization(0) 257 bdbs(0) finger - minutiae - card - compact - v - nh(5)}See ISO/IEC 19794-2:2005/Cor.1:2007 Table 16 (Finger minutiae, card compact size format, valley bifurcations with no headers) 
        ANSI_MINUTIAE_378 = 513,            //Finger Minutiae - no extended data  ANSI/INCITS 378-2004
        ANSI_FINGER_IMAGE_381 = 1025,       //Finger Image format                 ANSI/INCITS 381-2004
        ANSI_FACE_IMAGE_385 = 1281,         //Face Image Format                   ANSI/INCITS 385-2004
        ISO_WSQ = 32769,                    //WSQ compressed fingerprint image	IAFIS-IC-0010 (V3) - WSQ Gray-scale fingerprint image compression specification; 1997
        ISO_JPG_IMAGE = 32771,              //JPEG image                          ISO/IEC 10918 - Information Technology - Digital Compression and coding of continuous-tone still images (JPEG)
        ISO_JPG2000_IMAGE = 32772,          //JPEG2000 image                      ISO/IEC 15444 - Information Technology - JPEG 2000 Image Coding System
        ISO_TIFF_IMAGE = 32773             //TIFF image                          ISO/IEC 12639 - Tag image file format for image technology (TIFF/IT)
    }
}