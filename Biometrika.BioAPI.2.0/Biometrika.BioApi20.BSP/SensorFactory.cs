﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Bio.Core.Utils;
using Biometrika.BioApi20.Interfaces;
using log4net;

namespace Biometrika.BioApi20.BSP
{
    public class SensorFactory : ISensorFactory
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(SensorFactory));

        /* PROPERTIES */
        public SensorFactorySchema Schema { get; set; }
        public ISensor currentSensor { get; set; }
        public IList<ISensor> listSensor { get; set; }
        public Hashtable htSensor { get; set; }


        /* EVENT */
        public event CaptureCallbackDelegate OnCapturedEvent;
        public event ConnectCallbackDelegate OnConnectEvent;
        public event DisconnectCallbackDelegate OnDisconnectEvent;
        public event TimeoutCallbackDelegate OnTimeoutEvent;
        public event ErrorCallbackDelegate OnErrorEvent;

        /// <summary>
        /// Inicializa el objeto, crea y Activa los sensores configurados, y los carga para exponerlos
        /// para su uso desde BSP.
        /// </summary>
        /// <returns></returns>
        public int Initialization()
        {
            int ret = 0;
            try
            {
                //1.- LEvanta y procesa el Schema, sino lo crea.
                ret = ProcesaSchema();

                if (ret < 0)
                {
                    LOG.Error("Biometrika.BioApi20.BSP.Initialization Error Procesando Schema = " + ret.ToString());
                    OnErrorEvent(ret, "Initialization Error Procesando Schema = " + ret.ToString(), null);
                    return ret;
                }

                //2.- Instancia los BFP configurados y los carga en la lista y Dictionary/Hashtable
                ret = LoadBFPs();

                if (ret < 0)
                {
                    LOG.Error("Biometrika.BioApi20.BSP.Initialization Error Cargando BFPs = " + ret.ToString());
                    OnErrorEvent(ret, "Initialization Error Cargando BFPs = " + ret.ToString(), null);
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Biometrika.BioApi20.BSP.Initialization Error = " + ex.Message);
                OnErrorEvent(ret, "Initialization Error = " + ex.Message, null);
            }
            return ret;
        }

        public int Close()
        {
            int ret = 0;
            try
            {
                foreach (DictionaryEntry item in htSensor)
                {
                    ((ISensor)item.Value).Close();
                }
                htSensor = null;
                listSensor = null;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Biometrika.BioApi20.BSP.Close Error = " + ex.Message);
                OnErrorEvent(ret, "Close Error = " + ex.Message, null);
            }
            return ret;
        }


        public List<Sample> Capture(int sensorType, int bodypart, string sensorSerial, int timeout, int captureType, int qualityCapture, out Error error)
        {
            //Si es nulo el serial, toma el primero de la lista.
            if (string.IsNullOrEmpty(sensorSerial))
            {
                LOG.Debug("SensorFactory.Capture - Serial = null => Toma primero...");
                //Si no hay sensores retorno error para evitar problemas
                if (listSensor == null | listSensor.Count == 0)
                {
                    error = new Error(Error.IERR_NO_SENSOR_CONNECTED, Error.SERR_NO_SENSOR_CONNECTED);
                    LOG.Debug("SensorFactory.Capture - " + Error.SERR_NO_SENSOR_CONNECTED);
                    return null;
                } else //Selecciono el primero conectado
                {
                    foreach (DictionaryEntry item in htSensor)
                    {
                        if (((ISensor)item.Value).SensorType == sensorType)
                        {
                            sensorSerial = ((ISensor)item.Value).SerialSensor; // (htSensor.Keys.OfType<string>().ToArray()[0]);
                            LOG.Debug("SensorFactory.Capture - Serial = " + sensorSerial + "!"); 
                            break;
                        }
                    }
                    if (string.IsNullOrEmpty(sensorSerial))
                    {
                        LOG.Debug("SensorFactory.Capture - No encontro serial de ese tipo => Set primero que encuentra...");
                        //Si es nulo enotnces no encontro ninguno de ese sensorType (ej: primero de secugen)
                        //   => Setea primero que encuentra de cualqueir marca.
                        sensorSerial = (htSensor.Keys.OfType<string>().ToArray()[0]);
                        LOG.Debug("SensorFactory.Capture - Serial = " + sensorSerial + "!");
                    }
                }
            }
            LOG.Debug("SensorFactory.Capture - Seteando currentSensor => " + sensorSerial + "...");
            LOG.Debug("SensorFactory.Capture - htSensor != null => " + (htSensor != null).ToString());
            LOG.Debug("SensorFactory.Capture - sensorSerial in htSensor => " + htSensor.ContainsKey(sensorSerial).ToString());  
            currentSensor = (ISensor)htSensor[sensorSerial];
            if (currentSensor == null) {
                LOG.Error("SensorFactory.Capture - currentSensor == null. Sale...");
                error = new Error(Error.IERR_NO_SENSOR_CONNECTED, Error.SERR_NO_SENSOR_CONNECTED);
                return null;
            } 
            currentSensor.OnCapturedEvent += OnCapturedEvent;
            currentSensor.OnTimeoutEvent += OnTimeoutEvent;
            List<Sample> sampleout = currentSensor.Capture(sensorType, bodypart, sensorSerial, timeout, captureType, qualityCapture, out error);
            currentSensor.OnCapturedEvent -= OnCapturedEvent;
            currentSensor.OnTimeoutEvent -= OnTimeoutEvent;
            return sampleout;
        }


        public int EnableAutoEvent(string sensorSerial, bool enable, int hwnd, out Error error)
        {
            error = null;
            //if (string.IsNullOrEmpty(sensorSerial))
            //{
            //Si no hay sensores retorno error para evitar problemas
            if (listSensor == null | listSensor.Count == 0)
            {
                error = new Error(Error.IERR_NO_SENSOR_CONNECTED, Error.SERR_NO_SENSOR_CONNECTED);
                return -1;
            }
            else //Selecciono el primero conectado
            {
                if (string.IsNullOrEmpty(sensorSerial))
                {
                    //Si es nulo enotnces no encontro ninguno de ese sensorSerial (ej: primero de secugen)
                    //   => Setea primero que encuentra de cualqueir marca.
                    error = null;
                    sensorSerial = (htSensor.Keys.OfType<string>().ToArray()[0]);
                    currentSensor = (ISensor)htSensor[sensorSerial];
                    return currentSensor.EnableAutoEvent(sensorSerial, enable, hwnd);
                }
                else
                {
                    foreach (DictionaryEntry item in htSensor)
                    {
                        if (((ISensor)item.Value).SerialSensor.Equals(sensorSerial))
                        {
                            error = null;
                            currentSensor = (ISensor)htSensor[sensorSerial];
                            return currentSensor.EnableAutoEvent(sensorSerial, enable, hwnd); // (htSensor.Keys.OfType<string>().ToArray()[0]);
                            //break;
                        }
                    }
                    //Si llega aca es que no encontro, devuevlo el primero
                    error = null;
                    currentSensor = (ISensor)htSensor[sensorSerial];
                    return currentSensor.EnableAutoEvent(sensorSerial, enable, hwnd);
                } 
            }
            //}
            //else
            //{
            //error = new Error(Error.IERR_NO_SENSOR_CONNECTED, Error.SERR_NO_SENSOR_CONNECTED);
            //return -1;
            //}
        }

        public int CaptureContinue(string sensorSerial, int qualityCapture, out Error error)
        {
            //Si es nulo el serial, toma el primero de la lista.
            if (string.IsNullOrEmpty(sensorSerial))
            {
                //Si no hay sensores retorno error para evitar problemas
                if (listSensor == null | listSensor.Count == 0)
                {
                    error = new Error(Error.IERR_NO_SENSOR_CONNECTED, Error.SERR_NO_SENSOR_CONNECTED);
                    return Error.IERR_NO_SENSOR_CONNECTED;
                }
                else //Selecciono el primero conectado
                {
                    if (string.IsNullOrEmpty(sensorSerial))
                    {
                        //Si es nulo enotnces no encontro ninguno de ese sensorType (ej: primero de secugen)
                        //   => Setea primero que encuentra de cualqueir marca.
                        sensorSerial = (htSensor.Keys.OfType<string>().ToArray()[0]);
                    }
                }
            }
            currentSensor = null;
            currentSensor = (ISensor)htSensor[sensorSerial];
            currentSensor.OnCapturedEvent += OnCapturedEvent;
            currentSensor.OnTimeoutEvent += OnTimeoutEvent;
            //List<Sample> sampleout 
            int ret = currentSensor.CaptureContinue(sensorSerial, qualityCapture, out error);
            currentSensor.OnCapturedEvent -= OnCapturedEvent;
            currentSensor.OnTimeoutEvent -= OnTimeoutEvent;
            error = null;
            return 0; //ret;
        }

        public void On(string sensorSerial)
        {
            //Si es nulo el serial, toma el primero de la lista.
            if (string.IsNullOrEmpty(sensorSerial))
            {
                //Si no hay sensores retorno error para evitar problemas
                if (listSensor == null | listSensor.Count == 0)
                {
                    //error = new Error(Error.IERR_NO_SENSOR_CONNECTED, Error.SERR_NO_SENSOR_CONNECTED);
                    return; //Error.IERR_NO_SENSOR_CONNECTED;
                }
                else //Selecciono el primero conectado
                {
                    if (string.IsNullOrEmpty(sensorSerial))
                    {
                        //Si es nulo enotnces no encontro ninguno de ese sensorType (ej: primero de secugen)
                        //   => Setea primero que encuentra de cualqueir marca.
                        sensorSerial = (htSensor.Keys.OfType<string>().ToArray()[0]);
                    }
                }
            }
            currentSensor = (ISensor)htSensor[sensorSerial];
            currentSensor.On();
        }

        public void Off(string sensorSerial)
        {
            //Si es nulo el serial, toma el primero de la lista.
            if (string.IsNullOrEmpty(sensorSerial))
            {
                //Si no hay sensores retorno error para evitar problemas
                if (listSensor == null | listSensor.Count == 0)
                {
                    //error = new Error(Error.IERR_NO_SENSOR_CONNECTED, Error.SERR_NO_SENSOR_CONNECTED);
                    return; //Error.IERR_NO_SENSOR_CONNECTED;
                }
                else //Selecciono el primero conectado
                {
                    if (string.IsNullOrEmpty(sensorSerial))
                    {
                        //Si es nulo enotnces no encontro ninguno de ese sensorType (ej: primero de secugen)
                        //   => Setea primero que encuentra de cualqueir marca.
                        sensorSerial = (htSensor.Keys.OfType<string>().ToArray()[0]);
                    }
                }
            }
            currentSensor = (ISensor)htSensor[sensorSerial];
            currentSensor.Off();
        }

        public ISensor GetSensor()
        {
            throw new NotImplementedException();
        }

        public ISensor GetSensor(int sensorBrand)
        {
            throw new NotImplementedException();
        }

        public ISensor GetSensor(string serial)
        {
            throw new NotImplementedException();
        }

#region Private

        /// <summary>
        /// Lee Schema o lo crea si no existe. Lo caraga en Propiedad
        /// </summary>
        /// <returns></returns>
        private int ProcesaSchema()
        {
            int ret = 0;
            try
            {
                LOG.Debug("Biometrika.BioApi20.BSP.ProcesaSchema IN. Chequeando Biometrika.BioApi20.BSP.SensorFactorySchema.xml...");
                if (!System.IO.File.Exists("Biometrika.BioApi20.BSP.SensorFactorySchema.xml"))
                {
                    LOG.Debug("Biometrika.BioApi20.BSP.ProcesaSchema Archvio no existe! Creando...");
                    this.Schema = new SensorFactorySchema();
                    SensorFactorySchemaItem item = new SensorFactorySchemaItem();
                    item.AuthenticationFactor = 2; //Fingerprint
                    item.Brand = Bio.Core.Matcher.Constant.Devices.DEVICE_SECUGEN; //Secugen
                    item.TimeoutCapture = 5000;
                    item.PathBFP = "Biometrika.BioApi20.BFPSensorSecugen";
                    item.ListSeriales = new List<string>();
                    item.ListSeriales.Add("SERIALDEMO");
                    item.ParametersBFP = "Param1=Value1|PAram2=Value2";
                    this.Schema.listSensors = new List<SensorFactorySchemaItem>();
                    this.Schema.listSensors.Add(item);
                    if (SerializeHelper.SerializeToFile(this.Schema, "Biometrika.BioApi20.BSP.SensorFactorySchema.xml"))
                    {
                        LOG.Debug("Biometrika.BioApi20.BSP.ProcesaSchema Creado!");
                    } else
                    {
                        LOG.Error("Biometrika.BioApi20.BSP.ProcesaSchema Error Creando Biometrika.BioApi20.BSP.SensorFactorySchema.xml!");
                    }

                }
                else
                {
                    this.Schema = SerializeHelper.DeserializeFromFile<SensorFactorySchema>("Biometrika.BioApi20.BSP.SensorFactorySchema.xml");
                    LOG.Debug("Biometrika.BioApi20.BSP.ProcesaSchema Leido! Schema!=null => " + (this.Schema!=null).ToString());
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Biometrika.BioApi20.BSP.ProcesaSchema Error = " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Dado el Schema levantado, se instancian los sensores configurados y se levantan a memoria.
        /// </summary>
        /// <returns></returns>
        private int LoadBFPs()
        {
            int ret = 0;
            ISensor _AuxSensor = null;
            try
            {
                LOG.Debug("Biometrika.BioApi20.BSP.LoadBFPs IN. Procesando Schema...");
                if (this.Schema == null) 
                {
                    ret = Bio.Core.Constant.Errors.IERR_BAD_SERIALIZER_CONFIG_FILE;
                    LOG.Error("Biometrika.BioApi20.BSP.LoadBFPs Schema nulo! Aborta [Err="+ 
                        Bio.Core.Constant.Errors.IERR_BAD_SERIALIZER_CONFIG_FILE.ToString()+"]");
                }
                else
                {
                    LOG.Debug("Biometrika.BioApi20.BSP.LoadBFPs - Proceso Schema: listSensor.length=" + 
                        ((Schema.listSensors!=null)? Schema.listSensors.Count.ToString():"NULL"));

                    this.listSensor = new List<ISensor>();
                    this.htSensor = new Hashtable();
                    foreach (SensorFactorySchemaItem item in Schema.listSensors)
                    {
                        LOG.Debug("Biometrika.BioApi20.BSP.LoadBFPs Cargando: " + item.PathBFP);
                        try
                        {
                            LOG.Debug("Biometrika.BioApi20.BSP.LoadBFPs - Ingresando a Assembly.Load...");
                            Assembly a = Assembly.Load(item.PathBFP);
                            LOG.Debug("Biometrika.BioApi20.BSP.LoadBFPs Instanciando Types...");
                            foreach (Type type in a.GetTypes()) 
                            {
                                if (type.IsClass && typeof(ISensor).IsAssignableFrom(type)) // && type.IsNested)
                                {
                                    ISensor currentSensor = (ISensor)Activator.CreateInstance(type);
                                    List<string> listDepured = currentSensor.GetSensorConnected(item.ListSeriales);
                                    LOG.Debug("Biometrika.BioApi20.BSP.LoadBFPs listDepured != null => " + (listDepured != null).ToString());
                                    if (listDepured != null)
                                    {
                                        foreach (string itemSerial in listDepured)
                                        {
                                            _AuxSensor = (ISensor)Activator.CreateInstance(type);
                                            if (_AuxSensor.InstanceSensor(itemSerial) == 0)
                                            {
                                                //_AuxSensor.OnCapturedEvent += OnCapturedEvent;
                                                //_AuxSensor.OnTimeoutEvent += OnTimeoutEvent;
                                                this.listSensor.Add(_AuxSensor);
                                                this.htSensor.Add(itemSerial, _AuxSensor);
                                                LOG.Debug("Biometrika.BioApi20.BSP.LoadBFPs    ==> Agregado Serial: " + itemSerial);
                                            }
                                            else
                                            {
                                                LOG.Warn("Biometrika.BioApi20.BSP.LoadBFPs    ==> Error inicializando Serial [No Agregado]: " + itemSerial);
                                            }
                                        }
                                    } else
                                    {
                                        LOG.Warn("Biometrika.BioApi20.BSP.LoadBFPs  => No existen lectores conectados para Marca = " + item.Brand);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LOG.Error("Biometrika.BioApi20.BSP.LoadBFPs Error loading " + item.PathBFP + " => " + ex.Message);
                            LOG.Error(ex.StackTrace);
                        }
                        //catch (Exception ex)
                        //{
                        //    LOG.Error("Biometrika.BioApi20.BSP.LoadBFPs Error loading " + item.PathBFP + " => " + ex.Message);
                        //}
                        
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Biometrika.BioApi20.BSP.LoadBFPs Error = " + ex.Message);
            }
            LOG.Debug("Biometrika.BioApi20.BSP.LoadBFPs OUT!");
            return ret;
        }

        public void On()
        {
            throw new NotImplementedException();
        }

        public void Off()
        {
            throw new NotImplementedException();
        }





        #endregion Private
    }
}
