﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Biometrika.Ferias.Server.Migrations
{
    public partial class InfoProd_NaN : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Productos_Informaciones_InformacionId",
                table: "Productos");

            migrationBuilder.DropIndex(
                name: "IX_Productos_InformacionId",
                table: "Productos");

            migrationBuilder.DropColumn(
                name: "InformacionId",
                table: "Productos");

            migrationBuilder.AlterColumn<string>(
                name: "UrlInfo",
                table: "Productos",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "InformacionProducto",
                columns: table => new
                {
                    InformacionesId = table.Column<int>(type: "int", nullable: false),
                    RelatedProductsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InformacionProducto", x => new { x.InformacionesId, x.RelatedProductsId });
                    table.ForeignKey(
                        name: "FK_InformacionProducto_Informaciones_InformacionesId",
                        column: x => x.InformacionesId,
                        principalTable: "Informaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InformacionProducto_Productos_RelatedProductsId",
                        column: x => x.RelatedProductsId,
                        principalTable: "Productos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InformacionProducto_RelatedProductsId",
                table: "InformacionProducto",
                column: "RelatedProductsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InformacionProducto");

            migrationBuilder.AlterColumn<string>(
                name: "UrlInfo",
                table: "Productos",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddColumn<int>(
                name: "InformacionId",
                table: "Productos",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Productos_InformacionId",
                table: "Productos",
                column: "InformacionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Productos_Informaciones_InformacionId",
                table: "Productos",
                column: "InformacionId",
                principalTable: "Informaciones",
                principalColumn: "Id");
        }
    }
}
