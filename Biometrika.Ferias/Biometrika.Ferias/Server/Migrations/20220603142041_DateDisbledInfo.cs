﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Biometrika.Ferias.Server.Migrations
{
    public partial class DateDisbledInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DateUpdated",
                table: "Informaciones",
                type: "datetime2",
                nullable: false,
                defaultValueSql: "GETDATE()",
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateDisabled",
                table: "Informaciones",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateDisabled",
                table: "Informaciones");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateUpdated",
                table: "Informaciones",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValueSql: "GETDATE()");
        }
    }
}
