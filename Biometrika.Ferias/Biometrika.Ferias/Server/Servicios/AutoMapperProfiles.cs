﻿using AutoMapper;
using Biometrika.Ferias.Shared.Entities;
using Biometrika.Ferias.Shared.Model.DTOs;

namespace Biometrika.Ferias.Server.Servicios
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<InformacionCreateDTO, Informacion>()
                .ForMember(rp => rp.RelatedProducts,
                    dto => dto.MapFrom(
                        c => c.RelatedProducts.Select(Id => new Producto { Id = Id })));
        }
    }
}
