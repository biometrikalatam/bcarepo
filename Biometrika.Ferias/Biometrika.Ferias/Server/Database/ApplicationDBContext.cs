﻿using Biometrika.Ferias.Shared.Entities;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Ferias.Server.Database
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Acceso>().Property(prop => prop.AccessDate)
                .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Informacion>().Property(prop => prop.DateUpdated)
                .HasDefaultValueSql("GETDATE()");
        }

        public DbSet<Persona> Personas { get; set; }
        public DbSet<Acceso> Accesos { get; set; }
        public DbSet<Informacion> Informaciones { get; set; }
        public DbSet<Producto> Productos { get; set; }

    }
}
