﻿using AutoMapper;
using Biometrika.Ferias.Server.Database;
using Biometrika.Ferias.Shared.DTOs;
using Biometrika.Ferias.Shared.Entities;
using Biometrika.Ferias.Shared.Model.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Biometrika.Ferias.Server.Controllers
{

    [ApiController]
    [Route("api/v1/informaciones")]
    public class InformacionController : ControllerBase
    {
        private readonly ApplicationDBContext _Context;
        private readonly IConfiguration _Configuration;
        private readonly ILogger<PersonasController> _Logger;
        private readonly IMapper _Mapper;

        public InformacionController(ApplicationDBContext Context,
                                  IConfiguration Configuration,
                                  ILogger<PersonasController> Logger,
                                  IMapper Mapper)
        {
            this._Context = Context;
            this._Configuration = Configuration;
            this._Logger = Logger;
            this._Mapper = Mapper;
        }

        /// <summary>
        /// Retorno lista de personas en el sistema
        /// </summary>
        /// <returns></returns>
        [HttpGet("{withChild:bool}")]
        public async Task<ActionResult<BFResponse>> Get(bool withChild = false)
        {
            BFResponse ret = new BFResponse(0, null, null);
            List<Informacion> informaciones = null;
            try
            {
                if (withChild)
                {
                    informaciones = await _Context.Informaciones
                                        .Where(i => i.DateDisabled == null)
                                        .Include(i => i.RelatedProducts)
                                        .OrderByDescending(i => i.DateUpdated)
                                        .ToListAsync();
                    if (informaciones != null)
                    {
                        foreach (var itemI in informaciones)
                        {
                            foreach (var itemRP in itemI.RelatedProducts)
                            {
                                itemRP.Informaciones = null;
                            }
                        }
                    }
                } else
                {
                    informaciones = await _Context.Informaciones
                                                    .Where(i => i.DateDisabled == null)
                                                    .OrderByDescending(i => i.DateUpdated)
                                                    .ToListAsync();
                }
                //_JSON_INFO = System.IO.File.ReadAllText(@"D:\ACODE\bcarepo\Biometrika.Ferias\Biometrika.Ferias\Client\wwwroot\data\Information.json");
                //informaciones = await _Context.Informaciones.ToListAsync();
                //informaciones = JsonConvert.DeserializeObject<List<Informacion>>(_JSON_INFO);
                ret.response = informaciones;
                return Ok(ret);
            }
            catch (Exception ex)
            {
                ret.code = -1;
                ret.message = "InformacionController.Get Excp = " + ex.Message;
                ret.response = null;
                _Logger.LogError(ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }
        }

        /// <summary>
        /// Agrega Informacion de articulos
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<BFResponse>> Post(InformacionCreateDTO informacionDTO)
        {
            BFResponse ret = new BFResponse(0, null, null);
            try
            {
                var informacion = _Mapper.Map<Informacion>(informacionDTO);
                //Marco para que no se modifique los related product asi no se intenta crear el produc to, sino solo
                //la tabla muchos a muchos
                informacion.RelatedProducts.ForEach(rp => _Context.Entry(rp).State = EntityState.Unchanged);
                _Context.Add(informacion);
                int qSaved = await _Context.SaveChangesAsync();
                if (qSaved == 0)
                {
                    ret.code = StatusCodes.Status304NotModified;
                    ret.message = "Error grabando en repositorio la informacion";
                    ret.response = null;
                    return StatusCode(StatusCodes.Status304NotModified, ret);
                }
                else
                {
                    ret.response = informacion;
                    return Ok(ret);
                }
            }
            catch (Exception ex)
            {
                ret.code = -1;
                ret.message = "InformacionController.Post Excp = " + ex.Message;
                ret.response = null;
                _Logger.LogError(ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }
        }


        //internal string _JSON_INFO = System.IO.File.ReadAllText(@"D:\ACODE\bcarepo\Biometrika.Ferias\Biometrika.Ferias\Client\wwwroot\data\Information.json");
    }
}