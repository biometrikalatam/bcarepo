﻿using Biometrika.Ferias.Server.Database;
using Biometrika.Ferias.Shared.DTOs;
using Biometrika.Ferias.Shared.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Ferias.Server.Controllers
{

    /// <summary>
    /// Controller para manejo de personas en Biometrika Ferias
    /// </summary>
    [ApiController]
    [Route("api/v1/personas")]
    public class PersonasController : ControllerBase
    {
        private readonly ApplicationDBContext _Context;
        private readonly IConfiguration _Configuration;
        private readonly ILogger<PersonasController> _Logger;

        public PersonasController(ApplicationDBContext Context, 
                                  IConfiguration Configuration,
                                  ILogger<PersonasController> Logger)
        {
            this._Context = Context;
            this._Configuration = Configuration;
            this._Logger = Logger;
        }

        /// <summary>
        /// Retorno lista de personas en el sistema
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<BFResponse>> Get()
        {
            BFResponse ret = new BFResponse(0, null, null);
            List<Persona> personas = null;
            try
            {
                personas = await _Context.Personas.ToListAsync();
                ret.response = personas;
                return Ok(ret);
            }
            catch (Exception ex)
            {
                ret.code = -1;
                ret.message = "PersonasController.Get Excp = " + ex.Message;
                ret.response = null;
                _Logger.LogError(ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }
            
        }

        /// <summary>
        /// Retorno lista de personas en el sistema
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<ActionResult<BFResponse>> GetById(int id)
        {
            BFResponse ret = new BFResponse(0, null, null);
            Persona persona = null;
            try
            {
                persona = await _Context.Personas.FirstOrDefaultAsync(p => p.Id == id);

                if (persona == null)
                {
                    ret.code = StatusCodes.Status404NotFound;
                    ret.message = "Persona con id = " + id.ToString() + " no existe!";
                    ret.response = null;
                    return StatusCode(StatusCodes.Status404NotFound, ret);
                }
                else
                {
                    ret.response = persona;
                }
                return Ok(ret);
            }
            catch (Exception ex)
            {
                ret.code = -1;
                ret.message = "PersonasController.Get Excp = " + ex.Message;
                ret.response = null;
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }

        }

        /// <summary>
        /// Retorno lista de personas en el sistema
        /// </summary>
        /// <returns></returns>
        [HttpGet("{mail}")]
        public async Task<ActionResult<BFResponse>> GetByMail(string mail)
        {
            BFResponse ret = new BFResponse(0, null, null);
            Persona persona = null;
            try
            {
                persona = await _Context.Personas.FirstOrDefaultAsync(p => p.Mail.Equals(mail));

                if (persona == null)
                {
                    ret.code = StatusCodes.Status404NotFound;
                    ret.message = "Persona con mail = " + mail + " no existe!";
                    ret.response = null;
                    return StatusCode(StatusCodes.Status404NotFound, ret);
                }
                else
                {
                    ret.response = persona;
                }
                return Ok(ret);
            }
            catch (Exception ex)
            {
                ret.code = -1;
                ret.message = "PersonasController.Get Excp = " + ex.Message;
                ret.response = null;
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }

        }

        /// <summary>
        /// Inserta nueva persona en el repositorio
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<BFResponse>> Create([FromBody] Persona persona)
        {
            BFResponse ret = new BFResponse(0, null, null);
            try
            {
                if (persona == null ||
                    string.IsNullOrEmpty(persona.Name) ||
                    string.IsNullOrEmpty(persona.Mail) ||
                    string.IsNullOrEmpty(persona.Company) ||
                    string.IsNullOrEmpty(persona.Position))
                {
                    ret.code = StatusCodes.Status400BadRequest;
                    ret.message = "Persona con datos nulos deben ser completados!";
                    ret.response = null;
                    _Logger.LogError("PersonasController.Create - " + ret.code + " - " + ret.message);
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                }

                Persona personaCheck = await _Context.Personas.FirstOrDefaultAsync(p => p.Mail.Equals(persona.Mail));

                if (personaCheck == null)
                {
                    _Context.Add(persona);
                    int qSaved = await _Context.SaveChangesAsync();
                    if (qSaved == 0)
                    {
                        ret.code = StatusCodes.Status304NotModified;
                        ret.message = "Error grabanbdo en repositorio";
                        ret.response = null;
                        _Logger.LogError("PersonasController.Create - " + ret.code + " - " + ret.message);
                        return StatusCode(StatusCodes.Status304NotModified, ret);
                    }
                    else
                    {
                        _Logger.LogDebug("PersonasController.Create OK! Mail=" + persona.Mail + " - Id = " + persona.Id);

                        //Armo token a encriptar => Id|Datetime.Now
                        //   Luego cuando chequeo pido renovar si la fecha de acceso es mayor a > Configuration["TokenValidity"] horas
                        //   Solo para tener mas control de accesos. Sino que accedan por el Login
                        string tokenPlain = persona.Id.ToString() + "|" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                        string token = Biometrika.Core._6.Crypto.TripleDES.Encrypt(tokenPlain);
                        _Logger.LogDebug("PersonasController.CheckAcces - Token encriptado = " + token);
                        token = token.Replace("/", "<BF>Barra<BR>");
                        _Logger.LogDebug("PersonasController.CheckAcces - Token reemplazado = " + token);

                        ret.response = persona;
                        string[] mailto = new string[2];
                        mailto[0] = _Configuration.GetValue<string>("InternalMailAlarm");
                        mailto[1] = persona.Mail;
                        foreach (var item in mailto)
                        {
                            _Logger.LogDebug("PersonasController.Create - Mail enviado a => " + item.ToString());
                        }
                        string[] strbody = new string[4];
                        strbody[0] = "Usted completó el registro en <b>Biometrika Latam!</b>";
                        strbody[1] = "Los datos regsitrados son:";
                        strbody[2] = "<ol>" + 
                                         "<li><b>Nombre:</b> " + persona.Name + "</li>" +
                                         "<li><b>Compañía:</b> " + persona.Company + "</li>" +
                                         "<li><b>Posición:</b> " + persona.Position + "</li>" +
                                         "<li><b>Mail:</b> " + persona.Mail + "</li>" +
                                         "<li><b>Teléfono:</b> " + persona.Phone + "</li>" +
                                     "</ol>";
                        strbody[3] = "Para acceder a la información ingrese: " +
                                    "<a href=\"" + _Configuration.GetValue<string>("URLBase") + "/listinfo/" + token + "\" " +
                                    "target=\"_blank\"><b>Aquí</b><a>";
                        
                        _Logger.LogDebug("PersonasController.Create - Link de Acceso => " +
                                            _Configuration.GetValue<string>("URLBase") + "/listinfo/" + token);

                        bool bRet = Core._6.Notify.NotifyService.Notify(_Configuration.GetSection("Notify").GetValue<string>("SMTPServer"),
                            _Configuration.GetSection("Notify").GetValue<string>("SMTPUser"),
                            _Configuration.GetSection("Notify").GetValue<string>("SMTPPsw"),
                            _Configuration.GetSection("Notify").GetValue<string>("SMTPDisplayName"),
                            _Configuration.GetSection("Notify").GetValue<string>("SMTPImageHeader"),
                            _Configuration.GetSection("Notify").GetValue<string>("SMTPImageFooter"),
                            mailto, "Registro Biometrika Latam", strbody, null, null, null);
                        _Logger.LogDebug("PersonasController.Create Status Mail Sent => " + bRet.ToString());
                        return Ok(ret);
                    }
                }
                else
                {
                    ret.code = StatusCodes.Status302Found;
                    ret.message = "Persona con mail = " + persona.Mail + " YA existe!";
                    ret.response = null;
                    _Logger.LogError("PersonasController.Create - " + ret.code + " - " + ret.message);
                    return StatusCode(StatusCodes.Status302Found, ret);
                }
            }
            catch (Exception ex)
            {
                ret.code = -1;
                ret.message = "PersonasController.Create Excp = " + ex.Message;
                ret.response = null;
                _Logger.LogError("PersonasController.Create - " + ret.code + " - " + ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="personaid"></param>
        /// <returns></returns>
        [HttpGet("checkaccess/{token}")]
        public async Task<ActionResult<BFResponse>> CheckAcces(string token)
        {
            BFResponse ret = new BFResponse(0, null, null);
            try
            {
                _Logger.LogDebug("PersonasController.CheckAcces IN...");
                if (string.IsNullOrEmpty(token))
                {
                    ret.code = StatusCodes.Status400BadRequest;
                    ret.message = "Parámetro nulo!";
                    ret.response = null;
                    _Logger.LogError("PersonasController.CheckAcces - " + ret.code + " - " + ret.message);
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                }

                // OLD => string personaid = Biometrika.Core._6.Crypto.TripleDES.Decrypt(token);

                //Desarmo token => Id|Datetime.Now
                //   Luego cuando chequeo pido renovar si la fecha de acceso es mayor a > Configuration["TokenValidity"] horas
                //   Solo para tener mas control de accesos. Sino que accedan por el Login
                string personaid = null;
                _Logger.LogDebug("PersonasController.CheckAcces - Token recibido = " + token);
                token = token.Replace("<BF>Barra<BR>", "/");
                _Logger.LogDebug("PersonasController.CheckAcces - Inicio proceso token = " + token);
                string tokenPlain = Biometrika.Core._6.Crypto.TripleDES.Decrypt(token);
                if (string.IsNullOrEmpty(tokenPlain))
                {
                    ret.code = StatusCodes.Status401Unauthorized;
                    ret.message = "Error procesando credenciales de acceso!";
                    ret.response = null;
                    _Logger.LogError("PersonasController.CheckAcces - " + ret.code + " - " + ret.message);
                    return StatusCode(StatusCodes.Status401Unauthorized, ret);
                } else
                {
                    try
                    {
                        _Logger.LogDebug("PersonasController.CheckAcces - Inicio proceso tokenPlain = " + tokenPlain);
                        string[] dataToken = tokenPlain.Split("|");
                        personaid = dataToken[0];
                        DateTime dtToken = DateTime.ParseExact(dataToken[1], "dd/MM/yyyy HH:mm:ss", null);

                        //Chequeo que el token entregado no sea superior a XX horas configuradas de validez. Sino rechazo acceso apra que entre
                        // por login y renueve el token. Solo pa joder y que no sea tan facil entrar.
                        // For testing uso Secondsif (dtToken.AddSeconds(Convert.ToInt32(_Configuration.GetValue<string>("TokenValidity"))) < DateTime.Now)
                        _Logger.LogDebug("PersonasController.CheckAcces - Checking validez => " +
                                            dtToken.AddHours(Convert.ToInt32(_Configuration.GetValue<string>("TokenValidity"))).ToString() +
                                            " < " + DateTime.Now.ToString());
                        if (dtToken.AddHours(Convert.ToInt32(_Configuration.GetValue<string>("TokenValidity"))) < DateTime.Now)
                        {
                            ret.code = StatusCodes.Status401Unauthorized;
                            ret.message = "Credenciales de acceso vencidas! Debe renovarlas accediendo por Login.";
                            ret.response = null;
                            _Logger.LogError("PersonasController.CheckAcces - " + ret.code + " - " + ret.message);
                            return StatusCode(StatusCodes.Status401Unauthorized, ret);
                        } else
                        {
                            _Logger.LogDebug("PersonasController.CheckAcces - Fin check token OK!");
                        }//Sino sigue nomas y genera Acceso.
                    }
                    catch (Exception ext)
                    {
                        ret.code = StatusCodes.Status401Unauthorized;
                        ret.message = "Error procesando credenciales de acceso!";
                        ret.response = null;
                        _Logger.LogError("PersonasController.CheckAcces - " + ret.code + " - " + ret.message);
                        return StatusCode(StatusCodes.Status401Unauthorized, ret);
                    }
                }

                _Logger.LogDebug("PersonasController.CheckAcces - Get persona con Id = " + personaid);
                Persona personaCheck = 
                    await _Context.Personas.FirstOrDefaultAsync(p => p.Id == Convert.ToInt32(personaid));

                if (personaCheck != null)
                {
                    _Logger.LogDebug("PersonasController.CheckAcces - PErsona existetne => Genero Acceso...");
                    //Agrego Acceso
                    Acceso acceso = new Acceso();
                    acceso.PersonaId = personaCheck.Id;
                    acceso.AccessDate = DateTime.Now;
                    _Context.Add(acceso);
                    int qSaved = await _Context.SaveChangesAsync();
                    if (qSaved == 0)
                    {
                        ret.code = StatusCodes.Status304NotModified;
                        ret.message = "Error grabando en repositorio";
                        ret.response = null;
                        _Logger.LogError("PersonasController.CheckAcces - " + ret.code + " - " + ret.message);
                        return StatusCode(StatusCodes.Status304NotModified, ret);
                    }
                    else
                    {
                        _Logger.LogDebug("PersonasController.CheckAcces OK! => Id = " + personaid);
                        acceso.Persona.Accesos = null;
                        ret.response = acceso;
                        //Alarma de acceso
                        string[] mailto = new string[1];
                        mailto[0] = _Configuration.GetValue<string>("InternalMailAlarm");
                        string[] strbody = new string[3];
                        strbody[0] = "<b>Acceso a información de Biometrika Latam! => Fecha: " +
                                        acceso.AccessDate.ToString("dd/MM/yyyy HH:mm:ss") + "</b>";
                        strbody[1] = "Los datos regsitrados son:";
                        strbody[2] = "<ol>" +
                                         "<li><b>Nombre:</b> " + personaCheck.Name + "</li>" +
                                         "<li><b>Compañía:</b> " + personaCheck.Company + "</li>" +
                                         "<li><b>Posición:</b> " + personaCheck.Position + "</li>" +
                                         "<li><b>Mail:</b> " + personaCheck.Mail + "</li>" +
                                         "<li><b>Teléfono:</b> " + personaCheck.Phone + "</li>" +
                                     "</ol>";
                        _Logger.LogDebug("PersonasController.CheckAcces - Envio mail interno a => " + mailto[0] + "...");
                        bool bRet = Core._6.Notify.NotifyService.Notify(_Configuration.GetSection("Notify").GetValue<string>("SMTPServer"),
                            _Configuration.GetSection("Notify").GetValue<string>("SMTPUser"),
                            _Configuration.GetSection("Notify").GetValue<string>("SMTPPsw"),
                            _Configuration.GetSection("Notify").GetValue<string>("SMTPDisplayName"),
                            _Configuration.GetSection("Notify").GetValue<string>("SMTPImageHeader"),
                            _Configuration.GetSection("Notify").GetValue<string>("SMTPImageFooter"),
                            mailto, "ACCESO a Biometrika Latam", strbody, null, null, null);
                        _Logger.LogDebug("PersonasController.CheckAcces Status Mail Sent => " + bRet.ToString());

                        return Ok(ret);
                    }
                }
                else
                {
                    ret.code = StatusCodes.Status404NotFound;
                    ret.message = "Persona con Id = " + personaid + " NO existe!";
                    ret.response = null;
                    _Logger.LogError("PersonasController.CheckAcces - " + ret.code + " - " + ret.message);
                    return StatusCode(StatusCodes.Status404NotFound, ret);
                }
            }
            catch (Exception ex)
            {
                ret.code = StatusCodes.Status500InternalServerError;
                ret.message = "PersonasController.CheckAcces Excp = " + ex.Message;
                ret.response = null;
                _Logger.LogError("PersonasController.CheckAcces - " + ret.code + " - " + ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="personaid"></param>
        /// <returns></returns>
        [HttpGet("checkaccesslogin/{mail}")]
        public async Task<ActionResult<BFResponse>> CheckAccesLogin(string mail)
        {
            BFResponse ret = new BFResponse(0, null, null);
            try
            {
                _Logger.LogDebug("PersonasController.CheckAccesLogin IN...");
                if (string.IsNullOrEmpty(mail))
                {
                    ret.code = StatusCodes.Status400BadRequest;
                    ret.message = "Parámetro nulo!";
                    ret.response = null;
                    _Logger.LogError("PersonasController.CheckAccesLogin - " + ret.code + " - " + ret.message);
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                }

                //string personaid = Biometrika.Core._6.Crypto.TripleDES.Decrypt(token);
                _Logger.LogDebug("PersonasController.CheckAccesLogin - Recupero persona con Mail = " + mail + "...");
                Persona personaCheck =
                    await _Context.Personas.FirstOrDefaultAsync(p => p.Mail.Equals(mail));

                if (personaCheck != null)
                {
                    _Logger.LogDebug("PersonasController.CheckAccesLogin - Perosna recuperada ok => genero nuevo Token...");
                    //string token = Biometrika.Core._6.Crypto.TripleDES.Encrypt(personaCheck.Id.ToString());
                    //Armo token a encriptar => Id|Datetime.Now
                    //   Luego cuando chequeo pido renovar si la fecha de acceso es mayor a > Configuration["TokenValidity"] horas
                    //   Solo para tener mas control de accesos. Sino que accedan por el Login
                    string tokenPlain = personaCheck.Id.ToString() + "|" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"); 
                    _Logger.LogDebug("PersonasController.CheckAccesLogin - Encriptando token para => " + tokenPlain);
                    string token = Biometrika.Core._6.Crypto.TripleDES.Encrypt(tokenPlain);
                    _Logger.LogDebug("PersonasController.CheckAcces - Token encriptado = " + token);
                    token = token.Replace("/", "<BF>Barra<BR>");
                    _Logger.LogDebug("PersonasController.Create Status: Id = " + personaCheck.Id.ToString() + " - Token Generated => " + token);
                    ret.response = token;
                    return Ok(ret);
                }
                else
                {
                    ret.code = StatusCodes.Status404NotFound;
                    ret.message = "Persona con Mail = " + mail + " NO existe!";
                    ret.response = null;
                    _Logger.LogError("PersonasController.CheckAccesLogin - " + ret.code + " - " + ret.message);
                    return StatusCode(StatusCodes.Status404NotFound, ret);
                }
            }
            catch (Exception ex)
            {
                ret.code = StatusCodes.Status500InternalServerError;
                ret.message = "PersonasController.CheckAccesLogin Excp = " + ex.Message;
                ret.response = null;
                _Logger.LogError("PersonasController.CheckAccesLogin - " + ret.code + " - " + ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }

        }

    }
}
