﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Biometrika.Ferias.Shared.DTOs
{
    [NotMapped]
    public class BFResponse
    {
        public BFResponse()
        {

        }
        public BFResponse(int _code, string _message, object _response)
        {
            code = _code;   
            message = _message;
            response = _response;
        }

        public int code { get; set; }
        public string message { get; set; }
        public object response { get; set; }
    }
}
