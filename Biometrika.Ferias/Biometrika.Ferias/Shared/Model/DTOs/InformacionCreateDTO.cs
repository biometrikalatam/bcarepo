﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Ferias.Shared.Model.DTOs
{
    public class InformacionCreateDTO
    {
        public int Id { get; set; }
        public int Type { get; set; } = 0; //0-Solo texto | 1-PDF | 2-Video
        public string Title { get; set; }
        public string Detail { get; set; }
        public string Image { get; set; } //URL de image sino se coloca una generica
        public string UrlInfo { get; set; } //URL del recurso
        public DateTime DateUpdated { get; set; }

        public List<int> RelatedProducts { get; set; }
    }
}
