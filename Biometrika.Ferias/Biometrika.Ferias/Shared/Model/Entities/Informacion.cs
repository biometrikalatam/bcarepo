﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Ferias.Shared.Entities
{
    public class Informacion : ICloneable
    {
        public int Id { get; set; }
        public int Type { get; set; } = 0; //0-Solo texto | 1-PDF | 2-Video
        
        [Required]
        public string Title { get; set; }
        [Required]
        public string Detail { get; set; }
        public string Image { get; set; } //URL de image sino se coloca una generica
        public string UrlInfo { get; set; } //URL del recurso
        public DateTime DateUpdated { get; set; }
        public DateTime? DateDisabled { get; set; }  //Si es null => vigente | Si tiene fecha => Dado de baja NO debe mostrarse

        public List<Producto> RelatedProducts { get; set; }

        public object Clone()
        {
            Informacion i = new Informacion();
            i.Id = Id;
            i.Title = Title;    
            i.UrlInfo = UrlInfo;    
            i.RelatedProducts = RelatedProducts;
            i.Detail = Detail;  
            i.Type = Type;
            return i;
        }
    }
}
