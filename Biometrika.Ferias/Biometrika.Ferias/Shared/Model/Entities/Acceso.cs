﻿namespace Biometrika.Ferias.Shared.Entities
{
    public class Acceso
    {
        public int Id { get; set; }
        public int PersonaId { get; set; }
        public Persona Persona { get; set; }
        public DateTime AccessDate { get; set; }
    }
}
