﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Ferias.Shared.Entities
{
    public class Producto
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string UrlInfo { get; set; }
        public List<Informacion> Informaciones { get; set; }
    }
}
