﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Biometrika.Ferias.Shared.Entities
{
    /// <summary>
    /// Entidad Persona
    /// </summary>
    [Index(nameof(Mail), IsUnique = true)]
    public class Persona
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Debe ingresar un nombre")]
        [StringLength(100)]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Debe ingresar una compañía")]
        [StringLength(100)]
        public string Company { get; set; }
        
        [Required(ErrorMessage = "Debe ingresar una posición")]
        [StringLength(100)] 
        public string Position { get; set; }
        
        [Required(ErrorMessage = "Debe ingresar un mail válido")]
        [StringLength(100)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage ="Debe ingresar un mail válido")]
        public string Mail { get; set; }

        [RegularExpression(@"^([0-9]{9}|[0-9]{10}|[0-9]{11})$", ErrorMessage ="Debe ingresar un teléfono válido")]
        [Required(ErrorMessage = "Debe ingresar un teléfono válido")]
        public string Phone { get; set; }

        public List<Acceso> Accesos { get; set; } 

    }
}
