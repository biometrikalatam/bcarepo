﻿using Biometrika.Ferias.Client.Services.Interfaces;
using Biometrika.Ferias.Shared.DTOs;
using Biometrika.Ferias.Shared.Entities;
using Newtonsoft.Json;
using Radzen;
using System.Net.Http.Json;

namespace Biometrika.Ferias.Client.Services
{
    public class ServiceInformacion : IServiceInformacion
    {
        private readonly HttpClient _httpClient;

        public ServiceInformacion(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<List<Informacion>> InformacionGetList()
        {
            BFResponse response = new BFResponse(0, null, null);
            List<Informacion> listRet = new List<Informacion>();
            try
            {
                HttpResponseMessage ret = await _httpClient.GetAsync("api/v1/informaciones/true");
                if (ret != null)
                {
                    var content = await ret.Content.ReadAsStringAsync();

                    response = JsonConvert.DeserializeObject<BFResponse>(content);

                    if (response != null)
                    {
                        if (response.code == 0)
                        {
                            listRet = JsonConvert.DeserializeObject<List<Informacion>>(response.response.ToString());
                            return listRet;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }
    }
}
