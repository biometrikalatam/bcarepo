﻿using Biometrika.Ferias.Shared.DTOs;
using Biometrika.Ferias.Shared.Entities;
using Radzen;

namespace Biometrika.Ferias.Client.Services.Interfaces
{
    public interface IServicePersona
    {
        Task<bool> CheckAndRegisterAccess(string id);
        Task<NotificationMessage> Register(Persona persona);
        Task<string> CheckAccess(string mail);
    }
}
