﻿using Biometrika.Ferias.Shared.Entities;

namespace Biometrika.Ferias.Client.Services.Interfaces
{
    public interface IServiceInformacion
    {
        Task<List<Informacion>> InformacionGetList(); 

    }
}
