﻿using Biometrika.Ferias.Client.Services.Interfaces;
using Biometrika.Ferias.Shared.DTOs;
using Biometrika.Ferias.Shared.Entities;
using Newtonsoft.Json;
using Radzen;
using System.Net.Http.Json;


namespace Biometrika.Ferias.Client.Services
{
    public class ServicePersona : IServicePersona
    {

        private readonly HttpClient _httpClient;

        public ServicePersona(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<NotificationMessage> Register(Persona persona)
        {
            BFResponse response = new BFResponse(0, null, null);
            NotificationMessage msgret = null;
            try
            {
                HttpResponseMessage ret = await _httpClient.PostAsJsonAsync<Persona>("api/v1/personas", persona);
                if (ret != null)
                {
                    var content = await ret.Content.ReadAsStringAsync();

                    response = JsonConvert.DeserializeObject<BFResponse>(content);

                    if (response != null)
                    {
                        if (response.code == 0)
                        {
                            msgret = new NotificationMessage {
                                        Style = "position: center;",
                                        Severity = NotificationSeverity.Info,
                                        Summary = "Registro completado!",
                                        Detail = "Registro completado con éxito. Llegará un mail con el link de acceso a la información!.",
                                        Duration = 5000
                                    };

                        } else
                        {
                            msgret = new NotificationMessage
                            {
                                Style = "position: center;",
                                Severity = NotificationSeverity.Error,
                                Summary = "Error de Registro!",
                                Detail = "Se produjo el error [" + response.code.ToString() + " - " + 
                                            response.message + "]. Reintente o comuniquese con info@biometrika.cl para pedir la información!.",
                                Duration = 5000
                            };
                        }
                    } else
                    {
                        msgret = new NotificationMessage
                        {
                            Style = "position: center;",
                            Severity = NotificationSeverity.Error,
                            Summary = "Error de Registro!",
                            Detail = "Se produjo el error [Respuesta Nula]. Reintente o comuniquese con info@biometrika.cl para pedir la información!.",
                            Duration = 5000
                        };
                    }
                    //System.Text.Json.JsonSerializer.Deserialize<BFResponse>(content);
                    //Persona p = JsonConvert.DeserializeObject<Persona>(response.response.ToString());
                    //System.Text.Json.JsonSerializer.Deserialize<Persona>((string)response.response);
                }
            }
            catch (Exception ex)
            {
                msgret = new NotificationMessage
                {
                    Style = "position: center;",
                    Severity = NotificationSeverity.Error,
                    Summary = "Excepcion en el registro!",
                    Detail = "Se produjo una excepcion [" + ex.Message + "]. Reintente o comuniquese con info@biometrika.cl para pedir la información!.",
                    Duration = 5000
                };
            }
            return msgret;
        }

        public async Task<bool> CheckAndRegisterAccess(string id)
        {
            BFResponse response = new BFResponse(0, null, null);
            //NotificationMessage msgret = null;
            try
            {
                HttpResponseMessage ret = await _httpClient.GetAsync("api/v1/personas/checkaccess/" + id);
                if (ret != null)
                {
                    var content = await ret.Content.ReadAsStringAsync();

                    response = JsonConvert.DeserializeObject<BFResponse>(content);

                    if (response != null)
                    {
                        if (response.code == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public async Task<string> CheckAccess(string mail)
        {
            BFResponse response = new BFResponse(0, null, null);
            //NotificationMessage msgret = null;
            string tokenRet = "";
            try
            {
                HttpResponseMessage ret = await _httpClient.GetAsync("api/v1/personas/checkaccesslogin/" + mail);
                if (ret != null)
                {
                    var content = await ret.Content.ReadAsStringAsync();

                    response = JsonConvert.DeserializeObject<BFResponse>(content);

                    if (response != null)
                    {
                        if (response.code == 0)
                        {
                            tokenRet = response.response.ToString();
                        }
                        else
                        {
                            return tokenRet;
                        }
                    }
                    else
                    {
                        return tokenRet;
                    }
                }
            }
            catch (Exception ex)
            {
                return "";
            }
            return tokenRet;
        }
    }
}
