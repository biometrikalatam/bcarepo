using Biometrika.Ferias.Client;
using Biometrika.Ferias.Client.Services;
using Biometrika.Ferias.Client.Services.Interfaces;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Radzen;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });



//Radzen
builder.Services.AddScoped<ContextMenuService>();
builder.Services.AddScoped<DialogService>();
builder.Services.AddScoped<NotificationService>();
builder.Services.AddScoped<TooltipService>();

//Services Locales
builder.Services.AddScoped<IServicePersona, ServicePersona>();
builder.Services.AddScoped<IServiceInformacion, ServiceInformacion>();

await builder.Build().RunAsync();
