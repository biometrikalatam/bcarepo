﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bio.Guia.Trabajo
{
    public partial class ucBiometric : UserControl
    {

        public Image imgHuella;
        public ucGenerateGuide _ucGG;
        public int _type = 1; //1=Empleado | 2=Receptor
        public bool _matchOK;
        public double _score;
        public string _trackidbp;

        public ucBiometric()
        {
            InitializeComponent();
        }

        private void ubpClientCompact1_Load(object sender, EventArgs e)
        {
            //ubpClientCompact1.Typeid = "RUT";
            //ubpClientCompact1.Valueid = "21284415-2";
            //ubpClientCompact1.Tokencontent = 0;
            //ubpClientCompact1.OperationType = 1;
            //ubpClientCompact1.Minutiaetype = 4;
            //ubpClientCompact1.Finger = 7;
        }

        private void sbtnClose_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        public void SetParams(ucGenerateGuide ucGG, int type, string typeid, string valueid, int optype)
        {
            this._ucGG = ucGG;
            this._type = type;
            ubpClientCompact1.Typeid = typeid;
            ubpClientCompact1.Valueid = valueid;
            ubpClientCompact1.Tokencontent = 0;
            ubpClientCompact1.OperationType = optype;
            ubpClientCompact1.Minutiaetype = 14;
            ubpClientCompact1.Finger = 7;
            labTypeOp.Text = (optype == 2) ? "E" : "V";
        }

        private void ubpClientCompact1_OnCompleteEvent(object sender, EventArgs e)
        {
            string sImg = ubpClientCompact1.GetSampleJPG();
            string[] arrImg = sImg.Split('~');

            Bio.Core.Wsq.Encoder.WsqEncoder enc = new Core.Wsq.Encoder.WsqEncoder();
            byte[] byWsq;
            bool b = enc.EncodeMemory(Convert.FromBase64String(arrImg[2]), (short)Convert.ToInt16(arrImg[0]), (short)Convert.ToInt16(arrImg[1]), out byWsq);
            string sWsqToGuide = Convert.ToBase64String(byWsq); 
            if (_type == 1)
            {
                this._ucGG.SetImghuellaEmpl(Bio.Core.Imaging.ImageProcessor.RawToBitmap(Convert.FromBase64String(arrImg[2]), Convert.ToInt32(arrImg[0]), Convert.ToInt32(arrImg[1])),
                                                                                       ubpClientCompact1.GetToken(), sWsqToGuide);
            }
            else
            {
                this._ucGG.SetImghuellaRecep(Bio.Core.Imaging.ImageProcessor.RawToBitmap(Convert.FromBase64String(arrImg[2]),
                                                                                       Convert.ToInt32(arrImg[0]),
                                                                                       Convert.ToInt32(arrImg[1])), ubpClientCompact1.GetToken(), sWsqToGuide);
            }

            this.sbtnSendBP.Enabled = true;
            //this.sbtnClose_Click(null, null);

        }

        internal void ResetAll()
        {
            this.ubpClientCompact1.ClearAll();
            this.sbtnSendBP.Enabled = false;
        }

        private void sbtnErase_Click(object sender, EventArgs e)
        {
            this.ubpClientCompact1.ClearBio();
            this.sbtnSendBP.Enabled = false;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ubpClientCompact1.Token))
            {
                pbWS.Visible = true;
                pbWS.Show();
                int actual = BioPortal.BPHelper.VerifyBioPortal(ubpClientCompact1.Valueid.Trim(), "IdClienteTest", "EndUser",
                                                                          "127.0.0.1", "HA20987498", 1, ubpClientCompact1.GetToken(),
                                                                          1, out _matchOK, out _score, out _trackidbp);
                if (actual == 0 && _matchOK)
                {
                    if (_type == 1) this._ucGG.SetVisibleImgVerifyEmplOK(true, _trackidbp);
                    else this._ucGG.SetVisibleImgVerifyRecepOK(true, _trackidbp);
                }
                else
                {
                    if (_type == 1) this._ucGG.SetVisibleImgVerifyEmplOK(false, _trackidbp);
                    else this._ucGG.SetVisibleImgVerifyRecepOK(false, _trackidbp);
                }
                pbWS.Visible = false;
                this.sbtnClose_Click(null, null);
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
