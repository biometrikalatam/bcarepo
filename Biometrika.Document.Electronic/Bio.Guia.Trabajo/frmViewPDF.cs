﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Bio.Guia.Trabajo
{
    public partial class frmViewPDF : DevExpress.XtraEditors.XtraForm
    {

        public string _path = "";

        public frmViewPDF()
        {
            InitializeComponent();
        }

        private void frmViewPDF_Load(object sender, EventArgs e)
        {

            //LoadPaths();
        }

        private void LoadPaths()
        {
            try
            {
                listBoxControl1.Items.Clear();
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(Bio.Guia.Trabajo.Properties.Settings.Default.PATH_GUIDE_LOCAL);
                System.IO.FileInfo[] fi = di.GetFiles("*.pdf");
                for (int i = 0; i < fi.Length; i++)
                {
                    listBoxControl1.Items.Add(fi[i].Name);
                }

                if (String.IsNullOrEmpty(_path) && listBoxControl1.Items.Count > 0)
                {
                    listBoxControl1.SelectedIndex = 0;
                    _path = Bio.Guia.Trabajo.Properties.Settings.Default.PATH_GUIDE_LOCAL + "\\" + listBoxControl1.SelectedItem;
                }

                if (String.IsNullOrEmpty(_path)) this.pdfViewer1.LoadDocument(_path);
                listBoxControl1.Refresh();
            }
            catch (Exception ex)
            {
                
                
            }
            
        }

        private void listBoxControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            _path = Bio.Guia.Trabajo.Properties.Settings.Default.PATH_GUIDE_LOCAL + "\\" + listBoxControl1.SelectedItem; 
            this.pdfViewer1.LoadDocument(_path);
        }

        private void frmViewPDF_Activated(object sender, EventArgs e)
        {
            LoadPaths();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            pdfViewer1.Print();
        }
    }
}