﻿namespace Bio.Guia.Trabajo
{
    partial class ucBiometric
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucBiometric));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbWS = new DevExpress.XtraWaitForm.ProgressPanel();
            this.labTypeOp = new System.Windows.Forms.Label();
            this.sbtnSendBP = new DevExpress.XtraEditors.SimpleButton();
            this.ubpClientCompact1 = new Bio.Portal.Client.v5.UBPClientCompact();
            this.sbtnErase = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnClose = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pbWS);
            this.groupBox1.Controls.Add(this.labTypeOp);
            this.groupBox1.Controls.Add(this.sbtnSendBP);
            this.groupBox1.Controls.Add(this.ubpClientCompact1);
            this.groupBox1.Controls.Add(this.sbtnErase);
            this.groupBox1.Controls.Add(this.sbtnClose);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(170, 180);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Token Biométrico...";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // pbWS
            // 
            this.pbWS.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pbWS.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pbWS.Appearance.Options.UseBackColor = true;
            this.pbWS.Appearance.Options.UseFont = true;
            this.pbWS.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.pbWS.AppearanceCaption.Options.UseFont = true;
            this.pbWS.AppearanceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.pbWS.AppearanceDescription.Options.UseFont = true;
            this.pbWS.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pbWS.Caption = "Por Favor Espere...";
            this.pbWS.Description = "Verificando...";
            this.pbWS.Location = new System.Drawing.Point(0, 125);
            this.pbWS.Name = "pbWS";
            this.pbWS.Size = new System.Drawing.Size(170, 40);
            this.pbWS.TabIndex = 40;
            this.pbWS.Text = "Verificando...";
            this.pbWS.Visible = false;
            // 
            // labTypeOp
            // 
            this.labTypeOp.AutoSize = true;
            this.labTypeOp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labTypeOp.Location = new System.Drawing.Point(145, 150);
            this.labTypeOp.Name = "labTypeOp";
            this.labTypeOp.Size = new System.Drawing.Size(17, 15);
            this.labTypeOp.TabIndex = 5;
            this.labTypeOp.Text = "V";
            // 
            // sbtnSendBP
            // 
            this.sbtnSendBP.Image = ((System.Drawing.Image)(resources.GetObject("sbtnSendBP.Image")));
            this.sbtnSendBP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnSendBP.Location = new System.Drawing.Point(142, 83);
            this.sbtnSendBP.Name = "sbtnSendBP";
            this.sbtnSendBP.Size = new System.Drawing.Size(24, 23);
            this.sbtnSendBP.TabIndex = 4;
            this.sbtnSendBP.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // ubpClientCompact1
            // 
            this.ubpClientCompact1.AuthenticationFactor = 2;
            this.ubpClientCompact1.BackColor = System.Drawing.SystemColors.Control;
            this.ubpClientCompact1.DEBUG = true;
            this.ubpClientCompact1.Device = 1;
            this.ubpClientCompact1.Finger = 1;
            this.ubpClientCompact1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ubpClientCompact1.Location = new System.Drawing.Point(6, 19);
            this.ubpClientCompact1.Minutiaetype = 4;
            this.ubpClientCompact1.Model = "4500";
            this.ubpClientCompact1.Name = "ubpClientCompact1";
            this.ubpClientCompact1.OperationType = 2;
            this.ubpClientCompact1.PATHDEBUG = "c:\\";
            this.ubpClientCompact1.SerialSensor = null;
            this.ubpClientCompact1.Size = new System.Drawing.Size(135, 155);
            this.ubpClientCompact1.TabIndex = 3;
            this.ubpClientCompact1.Timeout = 0;
            this.ubpClientCompact1.Tokencontent = 0;
            this.ubpClientCompact1.Typeid = "RUT";
            this.ubpClientCompact1.Umbralcalidad = 400;
            this.ubpClientCompact1.Valueid = null;
            this.ubpClientCompact1.OnCompleteEvent += new System.EventHandler(this.ubpClientCompact1_OnCompleteEvent);
            // 
            // sbtnErase
            // 
            this.sbtnErase.Image = ((System.Drawing.Image)(resources.GetObject("sbtnErase.Image")));
            this.sbtnErase.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnErase.Location = new System.Drawing.Point(142, 54);
            this.sbtnErase.Name = "sbtnErase";
            this.sbtnErase.Size = new System.Drawing.Size(24, 23);
            this.sbtnErase.TabIndex = 2;
            this.sbtnErase.Click += new System.EventHandler(this.sbtnErase_Click);
            // 
            // sbtnClose
            // 
            this.sbtnClose.Image = ((System.Drawing.Image)(resources.GetObject("sbtnClose.Image")));
            this.sbtnClose.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnClose.Location = new System.Drawing.Point(142, 25);
            this.sbtnClose.Name = "sbtnClose";
            this.sbtnClose.Size = new System.Drawing.Size(24, 23);
            this.sbtnClose.TabIndex = 1;
            this.sbtnClose.Click += new System.EventHandler(this.sbtnClose_Click);
            // 
            // ucBiometric
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "ucBiometric";
            this.Size = new System.Drawing.Size(170, 180);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.SimpleButton sbtnErase;
        private DevExpress.XtraEditors.SimpleButton sbtnClose;
        private Portal.Client.v5.UBPClientCompact ubpClientCompact1;
        private System.Windows.Forms.Label labTypeOp;
        private DevExpress.XtraEditors.SimpleButton sbtnSendBP;
        private DevExpress.XtraWaitForm.ProgressPanel pbWS;
    }
}
