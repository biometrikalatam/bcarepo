﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using log4net;

namespace Bio.Guia.Trabajo
{
    public partial class frmEnroll : DevExpress.XtraEditors.XtraForm
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(frmEnroll));

        string _token1;
        string _token2;

        public frmEnroll()
        {
            InitializeComponent();
        }

        private void frmEnroll_Load(object sender, EventArgs e)
        {
            PopulateComboCustomers();
            ClearForm();

        }

        private void ClearForm()
        {
            _token1 = null;
            _token2 = null;

        }

        private void PopulateComboCustomers()
        {
            try
            {
                foreach (Source.DBHelper.Customer cus in Program.LIST_CUSTOMERS.LisCustomers)
                {
                    comboCustomer.Properties.Items.Add(cus._UID);
                }
                comboCustomer.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                LOG.Error("frmEnroll.PopulateComboCustomers Error - " + ex.Message);
            }
        }

        private void btnSetBPC_Click(object sender, EventArgs e)
        {
            ubpClientCompact1.Typeid = "RUT";
            ubpClientCompact1.Valueid = textEdit1.Text;
            ubpClientCompact1.Tokencontent = 0;
            ubpClientCompact1.OperationType = 2; //Enroll
            ubpClientCompact1.Minutiaetype = 4;
            ubpClientCompact1.Finger = SetFinger();
        }

        private int SetFinger()
        {
            int iret = 0;
            try
            {
                return comboCustomer.SelectedIndex + 1;
            }
            catch 
            {
                return 1;
            }
        }

        private void ubpClientCompact1_OnCompleteEvent(object sender, EventArgs e)
        {
            //string sImg = ubpClientCompact1.GetSampleJPG();
            //string[] arrImg = sImg.Split('~');

            //Bio.Core.Wsq.Encoder.WsqEncoder enc = new Core.Wsq.Encoder.WsqEncoder();
            //byte[] byWsq;
            //bool b = enc.EncodeMemory(Convert.FromBase64String(arrImg[2]), (short)Convert.ToInt16(arrImg[0]), (short)Convert.ToInt16(arrImg[1]), out byWsq);
            //string sWsqToGuide = Convert.ToBase64String(byWsq);
            //if (_type == 1)
            //{
            //    this._ucGG.SetImghuellaEmpl(Bio.Core.Imaging.ImageProcessor.RawToBitmap(Convert.FromBase64String(arrImg[2]), Convert.ToInt32(arrImg[0]), Convert.ToInt32(arrImg[1])),
            //                                                                           ubpClientCompact1.GetToken(), sWsqToGuide);
            //}
            //else
            //{
            //    this._ucGG.SetImghuellaRecep(Bio.Core.Imaging.ImageProcessor.RawToBitmap(Convert.FromBase64String(arrImg[2]),
            //                                                                           Convert.ToInt32(arrImg[0]),
            //                                                                           Convert.ToInt32(arrImg[1])), ubpClientCompact1.GetToken(), sWsqToGuide);
            //}

            //this.sbtnSendBP.Enabled = true;
        }

        private void btnGetToken1_Click(object sender, EventArgs e)
        {
            string sImg = ubpClientCompact1.GetSampleJPG();
            string[] arrImg = sImg.Split('~');

            Bio.Core.Wsq.Encoder.WsqEncoder enc = new Core.Wsq.Encoder.WsqEncoder();
            byte[] byWsq;
            bool b = enc.EncodeMemory(Convert.FromBase64String(arrImg[2]), (short)Convert.ToInt16(arrImg[0]), (short)Convert.ToInt16(arrImg[1]), out byWsq);
            string sWsqToGuide = Convert.ToBase64String(byWsq);
            _token1 = ubpClientCompact1.GetToken();
            this.picH1.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(Convert.FromBase64String(arrImg[2]), Convert.ToInt32(arrImg[0]), Convert.ToInt32(arrImg[1]));
            this.picH1.Refresh();
        }

        private void btnEnroll_Click(object sender, EventArgs e)
        {
            string _trackidbp;
            int i = BioPortal.BPHelper.EnrollBioPortal(textEdit1.Text.Trim(), "IdClienteTest", "EndUser",
                                                                          "127.0.0.1", "HA20987498", 1, null,
                                                                          1, out _trackidbp);
            int i2 = 0;
        }

        private void btnGetToken2_Click(object sender, EventArgs e)
        {
            string sImg = ubpClientCompact1.GetSampleJPG();
            string[] arrImg = sImg.Split('~');

            Bio.Core.Wsq.Encoder.WsqEncoder enc = new Core.Wsq.Encoder.WsqEncoder();
            byte[] byWsq;
            bool b = enc.EncodeMemory(Convert.FromBase64String(arrImg[2]), (short)Convert.ToInt16(arrImg[0]), (short)Convert.ToInt16(arrImg[1]), out byWsq);
            string sWsqToGuide = Convert.ToBase64String(byWsq);
            _token2 = ubpClientCompact1.GetToken();
            this.picH1.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(Convert.FromBase64String(arrImg[2]), Convert.ToInt32(arrImg[0]), Convert.ToInt32(arrImg[1]));
            this.picH1.Refresh();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            _token2 = null;
        }

        private void sbtnEraseHEmpl_Click(object sender, EventArgs e)
        {
            _token1 = null;
        }

    }
}