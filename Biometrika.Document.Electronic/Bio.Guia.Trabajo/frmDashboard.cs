﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;

namespace Bio.Guia.Trabajo
{
    public partial class frmDashboard : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public frmDashboard()
        {
            InitializeComponent();
        }

        private void frmDashboard_Load(object sender, EventArgs e)
        {
            dashboardDesigner1.LoadDashboard(@"c:\tmp\dashboard.xml");
        }
    }
}