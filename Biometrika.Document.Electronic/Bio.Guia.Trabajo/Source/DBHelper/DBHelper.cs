﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace Bio.Guia.Trabajo.Source.DBHelper
{
    public class DBHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(DBHelper));
    }

    public class Employees
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Employees));

        public List<Employee> LisEmployees;

        internal Hashtable htEmployees;
        //public Employees() { }

        public Employees()
        {
            LisEmployees = new List<Employee>();
        }

        internal void Init()
        {
            htEmployees = new Hashtable();
            foreach (Source.DBHelper.Employee emp in Program.LIST_EMPLOYEES.LisEmployees)
            {
                htEmployees.Add(emp._UID, emp);
            }
        }
    }

    public class Employee
    {
        public int _Id;
        public string _UID;
        public string _Rut;
        public string _Name;
        public string _LastName;
        public string _Mail;

    }

    public class Customers
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Customers));

        internal Hashtable htCustomers;

        public List<Customer> LisCustomers;

        //public Customers() { }

        public Customers() { 
             LisCustomers = new List<Customer>();
        }

        internal void Init()
        {
            htCustomers = new Hashtable();
            foreach (Source.DBHelper.Customer cus in Program.LIST_CUSTOMERS.LisCustomers)
            {
                 htCustomers.Add(cus._UID, cus);
            }
            //throw new NotImplementedException();
            
        }
    }

    public class Customer
    {
        public int _Id {get; set;}
        public string _UID {get; set;}
        public string _Rut {get; set;}
        public string _Company { get; set; }
        public string _Address { get; set; }
        public string _TE { get; set; }
        public string _Mail { get; set; }

    }

}
