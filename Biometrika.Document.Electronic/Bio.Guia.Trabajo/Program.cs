﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using log4net;
using log4net.Config;
using BioPortal.Server.Api;

namespace Bio.Guia.Trabajo
{
    static class Program
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Program));
        
        internal static Bio.Guia.Trabajo.Source.DEHelper.DEHelper DE_HELPER = new Source.DEHelper.DEHelper();

        internal static Bio.Guia.Trabajo.Source.DBHelper.Employees LIST_EMPLOYEES = new Source.DBHelper.Employees();
        internal static Bio.Guia.Trabajo.Source.DBHelper.Customers LIST_CUSTOMERS = new Source.DBHelper.Customers();

        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {

            //string s = "<?xml version=\"1.0\" encoding=\"utf-8\"?><XmlParamOut xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><Trackid>92cd46822f1b442295b840a6d327a961</Trackid><Timestampend>2016-03-21T15:46:48.1998303-03:00</Timestampend><Actionid>1</Actionid><PersonalData><Id>0</Id><Typeid>RUT</Typeid><Valueid>51490983</Valueid><Documentexpirationdate>0001-01-01T00:00:00</Documentexpirationdate><Birthdate>0001-01-01T00:00:00</Birthdate><Creation>0001-01-01T00:00:00</Creation><Companyidenroll>0</Companyidenroll><Useridenroll>0</Useridenroll></PersonalData><Authenticationfactor>2</Authenticationfactor><Bodypart>1</Bodypart><Minutiaetype>21</Minutiaetype><Matchingtype>1</Matchingtype><Threshold>1000</Threshold><Score>0</Score><Result>2</Result><Companyid>7</Companyid><Userid>0</Userid><Origin>1</Origin><Clientid>H39151000782</Clientid><Ipenduser>192.168.56.1~192.168.5.91</Ipenduser><Enduser>192.168.56.1~192.168.5.91</Enduser><Resultconnector><DynamicDataItems><DynamicDataItem><key>message</key><value /></DynamicDataItem><DynamicDataItem><key>trackid</key><value /></DynamicDataItem><DynamicDataItem><key>status</key><value /></DynamicDataItem><DynamicDataItem><key>result</key><value>2</value></DynamicDataItem><DynamicDataItem><key>score</key><value>0</value></DynamicDataItem><DynamicDataItem><key>threshold</key><value>1000</value></DynamicDataItem><DynamicDataItem><key>timestamp</key><value>21/03/2016 15:46:48</value></DynamicDataItem><DynamicDataItem><key>externaltxs</key><value>﻿&lt;?xml version=\"1.0\" encoding=\"utf-8\"?&gt;&lt;DynamicData xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" /&gt;</value></DynamicDataItem><DynamicDataItem><key>bodypart</key><value>1</value></DynamicDataItem><DynamicDataItem><key>verificationsource</key><value>DMANec</value></DynamicDataItem></DynamicDataItems></Resultconnector><Verificationsource>DMANec</Verificationsource><Companyidenroll>0</Companyidenroll><Useridenroll>0</Useridenroll></XmlParamOut>";
            //XmlParamOut outXml = XmlUtils.DeserializeObject<XmlParamOut>(s);
            //int i = 0;
            //Bio.Guia.Trabajo.Source.DBHelper.Customer cus = new Source.DBHelper.Customer();
            //cus._Id = 1;
            //cus._UID = "SportlifeSA";
            //cus._Company = "Sportlife S.A.";
            //cus._Address = "Cerro el Plomo 5600 Piso 3, Las condes, Santiago, RM";
            //cus._TE = "+56-2-24408000";
            //cus._Mail = "info@sportlife.cl";
            //LIST_CUSTOMERS.LisCustomers.Add(cus);
            //Bio.Core.Utils.SerializeHelper.SerializeToFile(LIST_CUSTOMERS, @"C:\Biometrika\Desarrollo\Bio.Contrato.Electronico\Bio.Guia.Trabajo\Bio.Guia.Trabajo\bin\Debug\Customers.xml");

            XmlConfigurator.Configure(new System.IO.FileInfo(Application.ExecutablePath + "\\log4net.config.xml"));
            LOG.Info("Bio.Guia.Trabajo Starting...");

            LoadData();
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            Application.Run(new frmMainR());



            //frmViewPDF viewer = new frmViewPDF();
            //viewer._path = @"C:\tmp\GuiasPDF\94f097eb-cf1e-48d1-a39f-c2019201652a.pdf";
            //viewer.Show();


            // labBottom.Text = "Biometrika BioPortal Enroll v" + this.GetType().Assembly.GetName().Version + " - Fecha: " + DateTime.Now.ToString("dd/MM/yyyy");
            
            //Application.Run(new Form1());
        }

        internal static void LoadData()
        {
            try
            {
                LIST_EMPLOYEES = Bio.Core.Utils.SerializeHelper.DeserializeFromFile<Source.DBHelper.Employees>("Employees.xml");
            LIST_EMPLOYEES.Init();
            if (LIST_EMPLOYEES.LisEmployees != null && LIST_EMPLOYEES.LisEmployees.Count > 0)
            {
                LOG.Info("Bio.Guia.Trabajo Employees cargados OK.");
            }
            else
            {
                LOG.Warn("Bio.Guia.Trabajo Employees NO cargados!");
            }

            LIST_CUSTOMERS = Bio.Core.Utils.SerializeHelper.DeserializeFromFile<Source.DBHelper.Customers>("Customers.xml");
            LIST_CUSTOMERS.Init();
            if (LIST_CUSTOMERS.LisCustomers != null && LIST_CUSTOMERS.LisCustomers.Count > 0)
            {
                LOG.Info("Bio.Guia.Trabajo Customers cargados OK.");
            }
            else
            {
                LOG.Warn("Bio.Guia.Trabajo Customers NO cargados!");
            }
            }
            catch (Exception ex)
            {
                LOG.Error("Program.LoadData Error " + ex.Message);
            }
        }
    }
}
