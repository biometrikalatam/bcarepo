﻿namespace Bio.Guia.Trabajo
{
    partial class frmEnroll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEnroll));
            this.label1 = new System.Windows.Forms.Label();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnEnroll = new DevExpress.XtraEditors.SimpleButton();
            this.comboCustomer = new DevExpress.XtraEditors.ComboBoxEdit();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnGetToken2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.picH2 = new System.Windows.Forms.PictureBox();
            this.sbtnEraseHEmpl = new DevExpress.XtraEditors.SimpleButton();
            this.picH1 = new System.Windows.Forms.PictureBox();
            this.btnGetToken1 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnErase = new DevExpress.XtraEditors.SimpleButton();
            this.ubpClientCompact1 = new Bio.Portal.Client.v5.UBPClientCompact();
            this.label5 = new System.Windows.Forms.Label();
            this.comboFinger = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnSetBPC = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboCustomer.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picH2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picH1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboFinger.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(50, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "RUT";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(86, 30);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(138, 20);
            this.textEdit1.TabIndex = 2;
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(86, 56);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(138, 20);
            this.textEdit2.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nombre";
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(86, 82);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(138, 20);
            this.textEdit3.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(29, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Apellido";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Empresa";
            // 
            // btnEnroll
            // 
            this.btnEnroll.Image = ((System.Drawing.Image)(resources.GetObject("btnEnroll.Image")));
            this.btnEnroll.Location = new System.Drawing.Point(186, 161);
            this.btnEnroll.Name = "btnEnroll";
            this.btnEnroll.Size = new System.Drawing.Size(93, 37);
            this.btnEnroll.TabIndex = 11;
            this.btnEnroll.Text = "Enrolar";
            this.btnEnroll.Click += new System.EventHandler(this.btnEnroll_Click);
            // 
            // comboCustomer
            // 
            this.comboCustomer.Location = new System.Drawing.Point(86, 108);
            this.comboCustomer.Name = "comboCustomer";
            this.comboCustomer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboCustomer.Size = new System.Drawing.Size(193, 20);
            this.comboCustomer.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnGetToken2);
            this.groupBox1.Controls.Add(this.simpleButton1);
            this.groupBox1.Controls.Add(this.picH2);
            this.groupBox1.Controls.Add(this.sbtnEraseHEmpl);
            this.groupBox1.Controls.Add(this.picH1);
            this.groupBox1.Controls.Add(this.btnGetToken1);
            this.groupBox1.Controls.Add(this.sbtnErase);
            this.groupBox1.Controls.Add(this.ubpClientCompact1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.comboFinger);
            this.groupBox1.Controls.Add(this.btnSetBPC);
            this.groupBox1.Location = new System.Drawing.Point(320, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(501, 189);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Huellas a Enrolar...";
            // 
            // btnGetToken2
            // 
            this.btnGetToken2.Image = ((System.Drawing.Image)(resources.GetObject("btnGetToken2.Image")));
            this.btnGetToken2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnGetToken2.Location = new System.Drawing.Point(312, 148);
            this.btnGetToken2.Name = "btnGetToken2";
            this.btnGetToken2.Size = new System.Drawing.Size(40, 23);
            this.btnGetToken2.TabIndex = 50;
            this.btnGetToken2.Click += new System.EventHandler(this.btnGetToken2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(464, 102);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(24, 25);
            this.simpleButton1.TabIndex = 49;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // picH2
            // 
            this.picH2.BackColor = System.Drawing.Color.White;
            this.picH2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picH2.Image = global::Bio.Guia.Trabajo.Properties.Resources.FondoBPC_Transp;
            this.picH2.Location = new System.Drawing.Point(391, 102);
            this.picH2.Name = "picH2";
            this.picH2.Size = new System.Drawing.Size(67, 69);
            this.picH2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picH2.TabIndex = 48;
            this.picH2.TabStop = false;
            // 
            // sbtnEraseHEmpl
            // 
            this.sbtnEraseHEmpl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnEraseHEmpl.Image")));
            this.sbtnEraseHEmpl.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnEraseHEmpl.Location = new System.Drawing.Point(464, 27);
            this.sbtnEraseHEmpl.Name = "sbtnEraseHEmpl";
            this.sbtnEraseHEmpl.Size = new System.Drawing.Size(24, 25);
            this.sbtnEraseHEmpl.TabIndex = 47;
            this.sbtnEraseHEmpl.Click += new System.EventHandler(this.sbtnEraseHEmpl_Click);
            // 
            // picH1
            // 
            this.picH1.BackColor = System.Drawing.Color.White;
            this.picH1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picH1.Image = global::Bio.Guia.Trabajo.Properties.Resources.FondoBPC_Transp;
            this.picH1.Location = new System.Drawing.Point(391, 27);
            this.picH1.Name = "picH1";
            this.picH1.Size = new System.Drawing.Size(67, 69);
            this.picH1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picH1.TabIndex = 46;
            this.picH1.TabStop = false;
            // 
            // btnGetToken1
            // 
            this.btnGetToken1.Image = ((System.Drawing.Image)(resources.GetObject("btnGetToken1.Image")));
            this.btnGetToken1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnGetToken1.Location = new System.Drawing.Point(312, 33);
            this.btnGetToken1.Name = "btnGetToken1";
            this.btnGetToken1.Size = new System.Drawing.Size(40, 23);
            this.btnGetToken1.TabIndex = 42;
            this.btnGetToken1.Click += new System.EventHandler(this.btnGetToken1_Click);
            // 
            // sbtnErase
            // 
            this.sbtnErase.Image = ((System.Drawing.Image)(resources.GetObject("sbtnErase.Image")));
            this.sbtnErase.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnErase.Location = new System.Drawing.Point(312, 88);
            this.sbtnErase.Name = "sbtnErase";
            this.sbtnErase.Size = new System.Drawing.Size(40, 23);
            this.sbtnErase.TabIndex = 41;
            // 
            // ubpClientCompact1
            // 
            this.ubpClientCompact1.AuthenticationFactor = 2;
            this.ubpClientCompact1.BackColor = System.Drawing.SystemColors.Control;
            this.ubpClientCompact1.DEBUG = true;
            this.ubpClientCompact1.Device = 1;
            this.ubpClientCompact1.Finger = 1;
            this.ubpClientCompact1.Location = new System.Drawing.Point(168, 27);
            this.ubpClientCompact1.Minutiaetype = 4;
            this.ubpClientCompact1.Model = "4500";
            this.ubpClientCompact1.Name = "ubpClientCompact1";
            this.ubpClientCompact1.OperationType = 2;
            this.ubpClientCompact1.PATHDEBUG = "c:\\";
            this.ubpClientCompact1.SerialSensor = null;
            this.ubpClientCompact1.Size = new System.Drawing.Size(136, 150);
            this.ubpClientCompact1.TabIndex = 15;
            this.ubpClientCompact1.Timeout = 0;
            this.ubpClientCompact1.Tokencontent = 0;
            this.ubpClientCompact1.Typeid = "RUT";
            this.ubpClientCompact1.Umbralcalidad = 400;
            this.ubpClientCompact1.Valueid = null;
            this.ubpClientCompact1.OnCompleteEvent += new System.EventHandler(this.ubpClientCompact1_OnCompleteEvent);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Dedo a Enrolar";
            // 
            // comboFinger
            // 
            this.comboFinger.EditValue = "1 - Dedo Pulgar Derecho";
            this.comboFinger.Location = new System.Drawing.Point(17, 43);
            this.comboFinger.Name = "comboFinger";
            this.comboFinger.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboFinger.Properties.DropDownRows = 10;
            this.comboFinger.Properties.Items.AddRange(new object[] {
            "1 - Dedo Pulgar Derecho",
            "2 - Dedo Pulgar Derecho",
            "3 - Dedo Pulgar Derecho",
            "4 - Dedo Pulgar Derecho",
            "5 - Dedo Pulgar Derecho",
            "6 - Dedo Pulgar Izquierdo",
            "7 - Dedo Pulgar Izquierdo",
            "8 - Dedo Pulgar Izquierdo",
            "9 - Dedo Pulgar Izquierdo",
            "10 - Dedo Pulgar Izquierdo"});
            this.comboFinger.Size = new System.Drawing.Size(142, 20);
            this.comboFinger.TabIndex = 13;
            // 
            // btnSetBPC
            // 
            this.btnSetBPC.Image = ((System.Drawing.Image)(resources.GetObject("btnSetBPC.Image")));
            this.btnSetBPC.Location = new System.Drawing.Point(60, 69);
            this.btnSetBPC.Name = "btnSetBPC";
            this.btnSetBPC.Size = new System.Drawing.Size(99, 23);
            this.btnSetBPC.TabIndex = 12;
            this.btnSetBPC.Text = "Configurar >>";
            this.btnSetBPC.Click += new System.EventHandler(this.btnSetBPC_Click);
            // 
            // frmEnroll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 216);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.comboCustomer);
            this.Controls.Add(this.btnEnroll);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textEdit3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textEdit2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmEnroll";
            this.Text = "Bio Guia - Enrolamiento en BioPortal...";
            this.Load += new System.EventHandler(this.frmEnroll_Load);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboCustomer.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picH2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picH1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboFinger.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.SimpleButton btnEnroll;
        private DevExpress.XtraEditors.ComboBoxEdit comboCustomer;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.SimpleButton btnSetBPC;
        private DevExpress.XtraEditors.ComboBoxEdit comboFinger;
        private System.Windows.Forms.Label label5;
        private Portal.Client.v5.UBPClientCompact ubpClientCompact1;
        private DevExpress.XtraEditors.SimpleButton btnGetToken1;
        private DevExpress.XtraEditors.SimpleButton sbtnErase;
        private DevExpress.XtraEditors.SimpleButton btnGetToken2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.PictureBox picH2;
        private DevExpress.XtraEditors.SimpleButton sbtnEraseHEmpl;
        private System.Windows.Forms.PictureBox picH1;
    }
}