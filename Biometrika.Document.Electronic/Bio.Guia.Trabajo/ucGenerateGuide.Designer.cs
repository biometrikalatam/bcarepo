﻿namespace Bio.Guia.Trabajo
{
    partial class ucGenerateGuide
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucGenerateGuide));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRefreshNroGui = new DevExpress.XtraEditors.SimpleButton();
            this.ucBiometric1 = new Bio.Guia.Trabajo.ucBiometric();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtTicket = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtNroFactura = new DevExpress.XtraEditors.TextEdit();
            this.progressPanel1 = new DevExpress.XtraWaitForm.ProgressPanel();
            this.sBtnClear = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.sbtnGenerate = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtHoras = new DevExpress.XtraEditors.TextEdit();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.picVerifyEmploNOOK = new System.Windows.Forms.PictureBox();
            this.picVerifyEmploOK = new System.Windows.Forms.PictureBox();
            this.sbtnEraseHEmpl = new DevExpress.XtraEditors.SimpleButton();
            this.picHEmpl = new System.Windows.Forms.PictureBox();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.sBtnVerifyEmployee = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtApellidoEmpl = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtNombreEmpl = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtMailEmpl = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtRUTEmpl = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.comboEmpleado = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtMailEmpresa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtTEEmpresa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtRUTEmpresa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.rtBody = new System.Windows.Forms.RichTextBox();
            this.txtAddressEmpresa = new DevExpress.XtraEditors.TextEdit();
            this.txtEmpresa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.comboCustomer = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtNroGuia = new DevExpress.XtraEditors.TextEdit();
            this.deDate = new DevExpress.XtraEditors.DateEdit();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.picVerifyRecepNOOK = new System.Windows.Forms.PictureBox();
            this.picVerifyRecepOK = new System.Windows.Forms.PictureBox();
            this.sbtnEraseHRecep = new DevExpress.XtraEditors.SimpleButton();
            this.picHRecep = new System.Windows.Forms.PictureBox();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.txtApellidoRecep = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtNombreRecep = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtMailrecep = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtRUTRecep = new DevExpress.XtraEditors.TextEdit();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.timerVEmpl = new System.Windows.Forms.Timer(this.components);
            this.timerVRecep = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTicket.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNroFactura.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoras.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVerifyEmploNOOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVerifyEmploOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHEmpl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApellidoEmpl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreEmpl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailEmpl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRUTEmpl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboEmpleado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailEmpresa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEEmpresa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRUTEmpresa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddressEmpresa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmpresa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNroGuia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDate.Properties)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVerifyRecepNOOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVerifyRecepOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHRecep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApellidoRecep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreRecep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailrecep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRUTRecep.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRefreshNroGui);
            this.groupBox1.Controls.Add(this.ucBiometric1);
            this.groupBox1.Controls.Add(this.labelControl21);
            this.groupBox1.Controls.Add(this.txtTicket);
            this.groupBox1.Controls.Add(this.labelControl13);
            this.groupBox1.Controls.Add(this.txtNroFactura);
            this.groupBox1.Controls.Add(this.progressPanel1);
            this.groupBox1.Controls.Add(this.sBtnClear);
            this.groupBox1.Controls.Add(this.labelControl17);
            this.groupBox1.Controls.Add(this.sbtnGenerate);
            this.groupBox1.Controls.Add(this.labelControl16);
            this.groupBox1.Controls.Add(this.txtHoras);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.labelControl8);
            this.groupBox1.Controls.Add(this.txtMailEmpresa);
            this.groupBox1.Controls.Add(this.labelControl7);
            this.groupBox1.Controls.Add(this.txtTEEmpresa);
            this.groupBox1.Controls.Add(this.labelControl6);
            this.groupBox1.Controls.Add(this.txtRUTEmpresa);
            this.groupBox1.Controls.Add(this.labelControl5);
            this.groupBox1.Controls.Add(this.labelControl4);
            this.groupBox1.Controls.Add(this.rtBody);
            this.groupBox1.Controls.Add(this.txtAddressEmpresa);
            this.groupBox1.Controls.Add(this.txtEmpresa);
            this.groupBox1.Controls.Add(this.labelControl3);
            this.groupBox1.Controls.Add(this.comboCustomer);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Controls.Add(this.txtNroGuia);
            this.groupBox1.Controls.Add(this.deDate);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1255, 540);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Generando Guía de Trabajo...";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // btnRefreshNroGui
            // 
            this.btnRefreshNroGui.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefreshNroGui.Appearance.Options.UseFont = true;
            this.btnRefreshNroGui.Image = ((System.Drawing.Image)(resources.GetObject("btnRefreshNroGui.Image")));
            this.btnRefreshNroGui.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.BottomCenter;
            this.btnRefreshNroGui.Location = new System.Drawing.Point(293, 27);
            this.btnRefreshNroGui.Name = "btnRefreshNroGui";
            this.btnRefreshNroGui.Size = new System.Drawing.Size(29, 21);
            this.btnRefreshNroGui.TabIndex = 46;
            this.btnRefreshNroGui.Click += new System.EventHandler(this.btnRefreshNroGui_Click);
            // 
            // ucBiometric1
            // 
            this.ucBiometric1.Location = new System.Drawing.Point(594, 181);
            this.ucBiometric1.Name = "ucBiometric1";
            this.ucBiometric1.Size = new System.Drawing.Size(205, 180);
            this.ucBiometric1.TabIndex = 45;
            this.ucBiometric1.Visible = false;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Location = new System.Drawing.Point(560, 56);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(73, 16);
            this.labelControl21.TabIndex = 44;
            this.labelControl21.Text = "Nro Ticket :";
            // 
            // txtTicket
            // 
            this.txtTicket.Location = new System.Drawing.Point(639, 55);
            this.txtTicket.Name = "txtTicket";
            this.txtTicket.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtTicket.Size = new System.Drawing.Size(140, 20);
            this.txtTicket.TabIndex = 43;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(549, 30);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(84, 16);
            this.labelControl13.TabIndex = 42;
            this.labelControl13.Text = "Nro Factura :";
            // 
            // txtNroFactura
            // 
            this.txtNroFactura.Location = new System.Drawing.Point(639, 29);
            this.txtNroFactura.Name = "txtNroFactura";
            this.txtNroFactura.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtNroFactura.Size = new System.Drawing.Size(140, 20);
            this.txtNroFactura.TabIndex = 41;
            // 
            // progressPanel1
            // 
            this.progressPanel1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressPanel1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressPanel1.Appearance.Options.UseBackColor = true;
            this.progressPanel1.Appearance.Options.UseFont = true;
            this.progressPanel1.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.progressPanel1.AppearanceCaption.Options.UseFont = true;
            this.progressPanel1.AppearanceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.progressPanel1.AppearanceDescription.Options.UseFont = true;
            this.progressPanel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.progressPanel1.Caption = "Por Favor Espere...";
            this.progressPanel1.Description = "Generando Guía ...";
            this.progressPanel1.Location = new System.Drawing.Point(854, 446);
            this.progressPanel1.Name = "progressPanel1";
            this.progressPanel1.Size = new System.Drawing.Size(268, 65);
            this.progressPanel1.TabIndex = 39;
            this.progressPanel1.Text = "Generando Guía...";
            this.progressPanel1.Visible = false;
            this.progressPanel1.Click += new System.EventHandler(this.progressPanel1_Click);
            // 
            // sBtnClear
            // 
            this.sBtnClear.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sBtnClear.Appearance.Options.UseFont = true;
            this.sBtnClear.Image = ((System.Drawing.Image)(resources.GetObject("sBtnClear.Image")));
            this.sBtnClear.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.sBtnClear.Location = new System.Drawing.Point(991, 446);
            this.sBtnClear.Name = "sBtnClear";
            this.sBtnClear.Size = new System.Drawing.Size(94, 63);
            this.sBtnClear.TabIndex = 40;
            this.sBtnClear.Text = "&Borrrar";
            this.sBtnClear.Click += new System.EventHandler(this.sBtnClear_Click);
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Location = new System.Drawing.Point(36, 184);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(191, 16);
            this.labelControl17.TabIndex = 38;
            this.labelControl17.Text = "Ingrese las notas de la guía...";
            // 
            // sbtnGenerate
            // 
            this.sbtnGenerate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sbtnGenerate.Appearance.Options.UseFont = true;
            this.sbtnGenerate.Image = ((System.Drawing.Image)(resources.GetObject("sbtnGenerate.Image")));
            this.sbtnGenerate.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.sbtnGenerate.Location = new System.Drawing.Point(817, 446);
            this.sbtnGenerate.Name = "sbtnGenerate";
            this.sbtnGenerate.Size = new System.Drawing.Size(168, 63);
            this.sbtnGenerate.TabIndex = 37;
            this.sbtnGenerate.Text = "&Generar Guía...";
            this.sbtnGenerate.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Location = new System.Drawing.Point(606, 181);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(47, 16);
            this.labelControl16.TabIndex = 36;
            this.labelControl16.Text = "Horas :";
            // 
            // txtHoras
            // 
            this.txtHoras.Location = new System.Drawing.Point(659, 180);
            this.txtHoras.Name = "txtHoras";
            this.txtHoras.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtHoras.Size = new System.Drawing.Size(140, 20);
            this.txtHoras.TabIndex = 35;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.picVerifyEmploNOOK);
            this.groupBox2.Controls.Add(this.picVerifyEmploOK);
            this.groupBox2.Controls.Add(this.sbtnEraseHEmpl);
            this.groupBox2.Controls.Add(this.picHEmpl);
            this.groupBox2.Controls.Add(this.pictureEdit1);
            this.groupBox2.Controls.Add(this.sBtnVerifyEmployee);
            this.groupBox2.Controls.Add(this.labelControl18);
            this.groupBox2.Controls.Add(this.txtApellidoEmpl);
            this.groupBox2.Controls.Add(this.labelControl12);
            this.groupBox2.Controls.Add(this.txtNombreEmpl);
            this.groupBox2.Controls.Add(this.labelControl10);
            this.groupBox2.Controls.Add(this.txtMailEmpl);
            this.groupBox2.Controls.Add(this.labelControl11);
            this.groupBox2.Controls.Add(this.txtRUTEmpl);
            this.groupBox2.Controls.Add(this.labelControl9);
            this.groupBox2.Controls.Add(this.comboEmpleado);
            this.groupBox2.Location = new System.Drawing.Point(817, 31);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(420, 210);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "      Empleado";
            // 
            // picVerifyEmploNOOK
            // 
            this.picVerifyEmploNOOK.BackColor = System.Drawing.Color.White;
            this.picVerifyEmploNOOK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picVerifyEmploNOOK.Image = global::Bio.Guia.Trabajo.Properties.Resources.nook;
            this.picVerifyEmploNOOK.Location = new System.Drawing.Point(382, 132);
            this.picVerifyEmploNOOK.Name = "picVerifyEmploNOOK";
            this.picVerifyEmploNOOK.Size = new System.Drawing.Size(18, 21);
            this.picVerifyEmploNOOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picVerifyEmploNOOK.TabIndex = 47;
            this.picVerifyEmploNOOK.TabStop = false;
            this.picVerifyEmploNOOK.Visible = false;
            // 
            // picVerifyEmploOK
            // 
            this.picVerifyEmploOK.BackColor = System.Drawing.Color.White;
            this.picVerifyEmploOK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picVerifyEmploOK.Image = global::Bio.Guia.Trabajo.Properties.Resources.ok;
            this.picVerifyEmploOK.Location = new System.Drawing.Point(382, 132);
            this.picVerifyEmploOK.Name = "picVerifyEmploOK";
            this.picVerifyEmploOK.Size = new System.Drawing.Size(18, 21);
            this.picVerifyEmploOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picVerifyEmploOK.TabIndex = 46;
            this.picVerifyEmploOK.TabStop = false;
            this.picVerifyEmploOK.Visible = false;
            // 
            // sbtnEraseHEmpl
            // 
            this.sbtnEraseHEmpl.Image = ((System.Drawing.Image)(resources.GetObject("sbtnEraseHEmpl.Image")));
            this.sbtnEraseHEmpl.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnEraseHEmpl.Location = new System.Drawing.Point(376, 30);
            this.sbtnEraseHEmpl.Name = "sbtnEraseHEmpl";
            this.sbtnEraseHEmpl.Size = new System.Drawing.Size(24, 25);
            this.sbtnEraseHEmpl.TabIndex = 45;
            this.sbtnEraseHEmpl.Click += new System.EventHandler(this.sbtnEraseHEmpl_Click);
            // 
            // picHEmpl
            // 
            this.picHEmpl.BackColor = System.Drawing.Color.White;
            this.picHEmpl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picHEmpl.Image = global::Bio.Guia.Trabajo.Properties.Resources.FondoBPC_Transp;
            this.picHEmpl.Location = new System.Drawing.Point(284, 27);
            this.picHEmpl.Name = "picHEmpl";
            this.picHEmpl.Size = new System.Drawing.Size(120, 131);
            this.picHEmpl.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picHEmpl.TabIndex = 44;
            this.picHEmpl.TabStop = false;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(8, -2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit1.Size = new System.Drawing.Size(24, 28);
            this.pictureEdit1.TabIndex = 42;
            // 
            // sBtnVerifyEmployee
            // 
            this.sBtnVerifyEmployee.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sBtnVerifyEmployee.Appearance.Options.UseFont = true;
            this.sBtnVerifyEmployee.Image = ((System.Drawing.Image)(resources.GetObject("sBtnVerifyEmployee.Image")));
            this.sBtnVerifyEmployee.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.sBtnVerifyEmployee.Location = new System.Drawing.Point(284, 164);
            this.sBtnVerifyEmployee.Name = "sBtnVerifyEmployee";
            this.sBtnVerifyEmployee.Size = new System.Drawing.Size(120, 28);
            this.sBtnVerifyEmployee.TabIndex = 41;
            this.sBtnVerifyEmployee.Text = "&Verificar";
            this.sBtnVerifyEmployee.Click += new System.EventHandler(this.sBtnVerifyEmployee_Click);
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Location = new System.Drawing.Point(10, 132);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(60, 16);
            this.labelControl18.TabIndex = 34;
            this.labelControl18.Text = "Apellido :";
            // 
            // txtApellidoEmpl
            // 
            this.txtApellidoEmpl.Location = new System.Drawing.Point(76, 131);
            this.txtApellidoEmpl.Name = "txtApellidoEmpl";
            this.txtApellidoEmpl.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtApellidoEmpl.Size = new System.Drawing.Size(186, 20);
            this.txtApellidoEmpl.TabIndex = 33;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(12, 102);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(58, 16);
            this.labelControl12.TabIndex = 32;
            this.labelControl12.Text = "Nombre :";
            // 
            // txtNombreEmpl
            // 
            this.txtNombreEmpl.Location = new System.Drawing.Point(76, 101);
            this.txtNombreEmpl.Name = "txtNombreEmpl";
            this.txtNombreEmpl.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtNombreEmpl.Size = new System.Drawing.Size(186, 20);
            this.txtNombreEmpl.TabIndex = 31;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(10, 155);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(34, 16);
            this.labelControl10.TabIndex = 30;
            this.labelControl10.Text = "Mail :";
            // 
            // txtMailEmpl
            // 
            this.txtMailEmpl.Location = new System.Drawing.Point(10, 174);
            this.txtMailEmpl.Name = "txtMailEmpl";
            this.txtMailEmpl.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtMailEmpl.Size = new System.Drawing.Size(250, 20);
            this.txtMailEmpl.TabIndex = 29;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(37, 72);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(33, 16);
            this.labelControl11.TabIndex = 28;
            this.labelControl11.Text = "RUT :";
            // 
            // txtRUTEmpl
            // 
            this.txtRUTEmpl.Location = new System.Drawing.Point(76, 69);
            this.txtRUTEmpl.Name = "txtRUTEmpl";
            this.txtRUTEmpl.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtRUTEmpl.Size = new System.Drawing.Size(140, 20);
            this.txtRUTEmpl.TabIndex = 27;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(12, 24);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(70, 16);
            this.labelControl9.TabIndex = 26;
            this.labelControl9.Text = "Empleado :";
            // 
            // comboEmpleado
            // 
            this.comboEmpleado.Location = new System.Drawing.Point(12, 46);
            this.comboEmpleado.Name = "comboEmpleado";
            this.comboEmpleado.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboEmpleado.Size = new System.Drawing.Size(250, 20);
            this.comboEmpleado.TabIndex = 25;
            this.comboEmpleado.SelectedIndexChanged += new System.EventHandler(this.comboEmpleado_SelectedIndexChanged);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(104, 160);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(34, 16);
            this.labelControl8.TabIndex = 16;
            this.labelControl8.Text = "Mail :";
            // 
            // txtMailEmpresa
            // 
            this.txtMailEmpresa.Location = new System.Drawing.Point(144, 159);
            this.txtMailEmpresa.Name = "txtMailEmpresa";
            this.txtMailEmpresa.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMailEmpresa.Properties.Appearance.Options.UseBackColor = true;
            this.txtMailEmpresa.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtMailEmpresa.Size = new System.Drawing.Size(425, 20);
            this.txtMailEmpresa.TabIndex = 15;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(589, 134);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(23, 16);
            this.labelControl7.TabIndex = 14;
            this.labelControl7.Text = "TE :";
            // 
            // txtTEEmpresa
            // 
            this.txtTEEmpresa.Location = new System.Drawing.Point(618, 133);
            this.txtTEEmpresa.Name = "txtTEEmpresa";
            this.txtTEEmpresa.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTEEmpresa.Properties.Appearance.Options.UseBackColor = true;
            this.txtTEEmpresa.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtTEEmpresa.Size = new System.Drawing.Size(140, 20);
            this.txtTEEmpresa.TabIndex = 13;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(579, 108);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(33, 16);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "RUT :";
            this.labelControl6.Click += new System.EventHandler(this.labelControl6_Click);
            // 
            // txtRUTEmpresa
            // 
            this.txtRUTEmpresa.Location = new System.Drawing.Point(618, 107);
            this.txtRUTEmpresa.Name = "txtRUTEmpresa";
            this.txtRUTEmpresa.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtRUTEmpresa.Properties.Appearance.Options.UseBackColor = true;
            this.txtRUTEmpresa.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtRUTEmpresa.Size = new System.Drawing.Size(140, 20);
            this.txtRUTEmpresa.TabIndex = 11;
            this.txtRUTEmpresa.EditValueChanged += new System.EventHandler(this.textEdit3_EditValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(70, 134);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(68, 16);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Dirección :";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(74, 108);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(64, 16);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Empresa :";
            // 
            // rtBody
            // 
            this.rtBody.Location = new System.Drawing.Point(24, 206);
            this.rtBody.Name = "rtBody";
            this.rtBody.Size = new System.Drawing.Size(775, 304);
            this.rtBody.TabIndex = 8;
            this.rtBody.Text = "";
            // 
            // txtAddressEmpresa
            // 
            this.txtAddressEmpresa.Location = new System.Drawing.Point(144, 133);
            this.txtAddressEmpresa.Name = "txtAddressEmpresa";
            this.txtAddressEmpresa.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtAddressEmpresa.Properties.Appearance.Options.UseBackColor = true;
            this.txtAddressEmpresa.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtAddressEmpresa.Size = new System.Drawing.Size(425, 20);
            this.txtAddressEmpresa.TabIndex = 7;
            // 
            // txtEmpresa
            // 
            this.txtEmpresa.Location = new System.Drawing.Point(144, 107);
            this.txtEmpresa.Name = "txtEmpresa";
            this.txtEmpresa.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtEmpresa.Properties.Appearance.Options.UseBackColor = true;
            this.txtEmpresa.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtEmpresa.Size = new System.Drawing.Size(425, 20);
            this.txtEmpresa.TabIndex = 6;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(80, 82);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(53, 16);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "Cliente :";
            // 
            // comboCustomer
            // 
            this.comboCustomer.Location = new System.Drawing.Point(144, 81);
            this.comboCustomer.Name = "comboCustomer";
            this.comboCustomer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboCustomer.Size = new System.Drawing.Size(495, 20);
            this.comboCustomer.TabIndex = 4;
            this.comboCustomer.SelectedIndexChanged += new System.EventHandler(this.comboCustomer_SelectedIndexChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(331, 28);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(46, 16);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Fecha :";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(24, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(109, 16);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Número de Guía :";
            // 
            // txtNroGuia
            // 
            this.txtNroGuia.Enabled = false;
            this.txtNroGuia.Location = new System.Drawing.Point(144, 27);
            this.txtNroGuia.Name = "txtNroGuia";
            this.txtNroGuia.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNroGuia.Properties.Appearance.Options.UseBackColor = true;
            this.txtNroGuia.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtNroGuia.Size = new System.Drawing.Size(145, 20);
            this.txtNroGuia.TabIndex = 1;
            // 
            // deDate
            // 
            this.deDate.EditValue = new System.DateTime(2016, 3, 6, 11, 9, 50, 369);
            this.deDate.Location = new System.Drawing.Point(392, 27);
            this.deDate.Name = "deDate";
            this.deDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDate.Size = new System.Drawing.Size(145, 20);
            this.deDate.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.picVerifyRecepNOOK);
            this.groupBox3.Controls.Add(this.picVerifyRecepOK);
            this.groupBox3.Controls.Add(this.sbtnEraseHRecep);
            this.groupBox3.Controls.Add(this.picHRecep);
            this.groupBox3.Controls.Add(this.pictureEdit2);
            this.groupBox3.Controls.Add(this.simpleButton1);
            this.groupBox3.Controls.Add(this.labelControl19);
            this.groupBox3.Controls.Add(this.txtApellidoRecep);
            this.groupBox3.Controls.Add(this.labelControl20);
            this.groupBox3.Controls.Add(this.txtNombreRecep);
            this.groupBox3.Controls.Add(this.labelControl14);
            this.groupBox3.Controls.Add(this.txtMailrecep);
            this.groupBox3.Controls.Add(this.labelControl15);
            this.groupBox3.Controls.Add(this.txtRUTRecep);
            this.groupBox3.Location = new System.Drawing.Point(817, 247);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(420, 193);
            this.groupBox3.TabIndex = 34;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "       Receptor";
            // 
            // picVerifyRecepNOOK
            // 
            this.picVerifyRecepNOOK.BackColor = System.Drawing.Color.White;
            this.picVerifyRecepNOOK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picVerifyRecepNOOK.Image = global::Bio.Guia.Trabajo.Properties.Resources.nook;
            this.picVerifyRecepNOOK.Location = new System.Drawing.Point(382, 116);
            this.picVerifyRecepNOOK.Name = "picVerifyRecepNOOK";
            this.picVerifyRecepNOOK.Size = new System.Drawing.Size(18, 21);
            this.picVerifyRecepNOOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picVerifyRecepNOOK.TabIndex = 49;
            this.picVerifyRecepNOOK.TabStop = false;
            this.picVerifyRecepNOOK.Visible = false;
            // 
            // picVerifyRecepOK
            // 
            this.picVerifyRecepOK.BackColor = System.Drawing.Color.White;
            this.picVerifyRecepOK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picVerifyRecepOK.Image = global::Bio.Guia.Trabajo.Properties.Resources.ok;
            this.picVerifyRecepOK.Location = new System.Drawing.Point(382, 116);
            this.picVerifyRecepOK.Name = "picVerifyRecepOK";
            this.picVerifyRecepOK.Size = new System.Drawing.Size(18, 21);
            this.picVerifyRecepOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picVerifyRecepOK.TabIndex = 48;
            this.picVerifyRecepOK.TabStop = false;
            this.picVerifyRecepOK.Visible = false;
            // 
            // sbtnEraseHRecep
            // 
            this.sbtnEraseHRecep.Image = ((System.Drawing.Image)(resources.GetObject("sbtnEraseHRecep.Image")));
            this.sbtnEraseHRecep.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbtnEraseHRecep.Location = new System.Drawing.Point(376, 19);
            this.sbtnEraseHRecep.Name = "sbtnEraseHRecep";
            this.sbtnEraseHRecep.Size = new System.Drawing.Size(24, 25);
            this.sbtnEraseHRecep.TabIndex = 46;
            this.sbtnEraseHRecep.Click += new System.EventHandler(this.sbtnEraseHRecep_Click);
            // 
            // picHRecep
            // 
            this.picHRecep.BackColor = System.Drawing.Color.White;
            this.picHRecep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picHRecep.Image = global::Bio.Guia.Trabajo.Properties.Resources.FondoBPC_Transp;
            this.picHRecep.Location = new System.Drawing.Point(284, 15);
            this.picHRecep.Name = "picHRecep";
            this.picHRecep.Size = new System.Drawing.Size(120, 131);
            this.picHRecep.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picHRecep.TabIndex = 45;
            this.picHRecep.TabStop = false;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.EditValue = ((object)(resources.GetObject("pictureEdit2.EditValue")));
            this.pictureEdit2.Location = new System.Drawing.Point(11, -2);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit2.Size = new System.Drawing.Size(24, 28);
            this.pictureEdit2.TabIndex = 44;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.simpleButton1.Location = new System.Drawing.Point(284, 155);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(120, 28);
            this.simpleButton1.TabIndex = 43;
            this.simpleButton1.Text = "&Verificar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Location = new System.Drawing.Point(8, 98);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(60, 16);
            this.labelControl19.TabIndex = 42;
            this.labelControl19.Text = "Apellido :";
            // 
            // txtApellidoRecep
            // 
            this.txtApellidoRecep.Location = new System.Drawing.Point(74, 97);
            this.txtApellidoRecep.Name = "txtApellidoRecep";
            this.txtApellidoRecep.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtApellidoRecep.Size = new System.Drawing.Size(186, 20);
            this.txtApellidoRecep.TabIndex = 41;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Location = new System.Drawing.Point(10, 60);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(58, 16);
            this.labelControl20.TabIndex = 40;
            this.labelControl20.Text = "Nombre :";
            // 
            // txtNombreRecep
            // 
            this.txtNombreRecep.Location = new System.Drawing.Point(74, 59);
            this.txtNombreRecep.Name = "txtNombreRecep";
            this.txtNombreRecep.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtNombreRecep.Size = new System.Drawing.Size(186, 20);
            this.txtNombreRecep.TabIndex = 39;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(12, 130);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(34, 16);
            this.labelControl14.TabIndex = 36;
            this.labelControl14.Text = "Mail :";
            // 
            // txtMailrecep
            // 
            this.txtMailrecep.Location = new System.Drawing.Point(12, 152);
            this.txtMailrecep.Name = "txtMailrecep";
            this.txtMailrecep.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtMailrecep.Size = new System.Drawing.Size(250, 20);
            this.txtMailrecep.TabIndex = 35;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Location = new System.Drawing.Point(12, 26);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(33, 16);
            this.labelControl15.TabIndex = 34;
            this.labelControl15.Text = "RUT :";
            // 
            // txtRUTRecep
            // 
            this.txtRUTRecep.Location = new System.Drawing.Point(51, 25);
            this.txtRUTRecep.Name = "txtRUTRecep";
            this.txtRUTRecep.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtRUTRecep.Size = new System.Drawing.Size(140, 20);
            this.txtRUTRecep.TabIndex = 33;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // timerVEmpl
            // 
            this.timerVEmpl.Enabled = true;
            this.timerVEmpl.Interval = 1000;
            this.timerVEmpl.Tick += new System.EventHandler(this.timerVEmpl_Tick);
            // 
            // timerVRecep
            // 
            this.timerVRecep.Interval = 1000;
            this.timerVRecep.Tick += new System.EventHandler(this.timerVRecep_Tick);
            // 
            // ucGenerateGuide
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.groupBox1);
            this.Name = "ucGenerateGuide";
            this.Size = new System.Drawing.Size(1255, 540);
            this.Load += new System.EventHandler(this.ucGenerateGuide_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTicket.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNroFactura.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoras.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVerifyEmploNOOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVerifyEmploOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHEmpl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApellidoEmpl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreEmpl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailEmpl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRUTEmpl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboEmpleado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailEmpresa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEEmpresa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRUTEmpresa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddressEmpresa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmpresa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNroGuia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDate.Properties)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVerifyRecepNOOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVerifyRecepOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHRecep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApellidoRecep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreRecep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailrecep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRUTRecep.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit txtAddressEmpresa;
        private DevExpress.XtraEditors.TextEdit txtEmpresa;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit comboCustomer;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtNroGuia;
        private DevExpress.XtraEditors.DateEdit deDate;
        private System.Windows.Forms.RichTextBox rtBody;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtRUTEmpresa;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtMailEmpresa;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtTEEmpresa;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtMailrecep;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtRUTRecep;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtNombreEmpl;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtMailEmpl;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtRUTEmpl;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.ComboBoxEdit comboEmpleado;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit txtHoras;
        private DevExpress.XtraEditors.SimpleButton sbtnGenerate;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraWaitForm.ProgressPanel progressPanel1;
        private DevExpress.XtraEditors.SimpleButton sBtnClear;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txtApellidoRecep;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtNombreRecep;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.SimpleButton sBtnVerifyEmployee;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtApellidoEmpl;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtTicket;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtNroFactura;
        //private ucBiometric ucBiometric1;
        private System.Windows.Forms.PictureBox picHEmpl;
        private System.Windows.Forms.PictureBox picHRecep;
        private DevExpress.XtraEditors.SimpleButton sbtnEraseHEmpl;
        private DevExpress.XtraEditors.SimpleButton sbtnEraseHRecep;
        private System.Windows.Forms.Timer timerVEmpl;
        private System.Windows.Forms.Timer timerVRecep;
        private System.Windows.Forms.PictureBox picVerifyEmploOK;
        private System.Windows.Forms.PictureBox picVerifyEmploNOOK;
        private System.Windows.Forms.PictureBox picVerifyRecepNOOK;
        private System.Windows.Forms.PictureBox picVerifyRecepOK;
        private ucBiometric ucBiometric1;
        private DevExpress.XtraEditors.SimpleButton btnRefreshNroGui;
    }
}
