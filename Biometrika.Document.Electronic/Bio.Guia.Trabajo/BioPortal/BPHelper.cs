﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bio.Core.Api;
using BioPortal.Server.Api;

namespace Bio.Guia.Trabajo.BioPortal
{
    internal static class BPHelper
    {
        
        internal static int VerifyBioPortal(string _rut, string _clienteId, string _endUser, string _ipUser, string _serialIdSensor,
                                                    short _fingerId, string _token, short _typeDevice,
                                                    out bool _matchok, out double _score, out string _trackid)
        {
            int _ret_actual = 0;
            string xmlparamout = string.Empty;
            _matchok = false;
            _score = 0;
            _trackid = null;

            try
            {
                using (BioPortalServerWSWEB.BioPortalServerWSWeb target = new BioPortalServerWSWEB.BioPortalServerWSWeb())
                {
                    target.Timeout = Properties.Settings.Default.WSTimeout;
                    target.Url = Properties.Settings.Default.Bio_Guia_Trabajo_BioPortalServerWSWEB_BioPortalServerWSWeb;
                    XmlParamIn pin = new XmlParamIn();
                    pin.Actionid = 1;  //Verify
                    pin.Additionaldata = null;
                    pin.Authenticationfactor = 2; //Fingerprint
                    pin.Minutiaetype = 14; //Token
                    pin.Bodypart = 1;  //pulgar derecho
                    pin.Clientid = _clienteId;  //Puedes sacarlo del Id de estacion de trabajo, algun identificador del cliente o mismo Companyid
                    pin.Companyid = Properties.Settings.Default.IdCompany_BP;  //Biometrika o un ID valido segun sea la empresa dada de alta en BioPortal
                    pin.Enduser = _endUser; //identificador de operador, quiza el ID Sensor
                    pin.Ipenduser = _ipUser; //ip estación de trabajo
                    pin.Matchingtype = 1;
                    pin.Origin = Properties.Settings.Default.OriginBP; //Este valor debemos definirlo. Es el Origen de la verificación, por lo que debemos definir 
                                                                       //Ese valor fijo en el BioPortal. Por ahora configuralo en 1 = Desconocido (Pero configurable)   

                    pin.SampleCollection = new List<Sample>();
                    Sample sample = new Sample();
                    sample.Data = _token;
                    sample.Minutiaetype = 24; //Token
                    sample.Additionaldata = null;
                    pin.SampleCollection.Add(sample);

                    pin.SaveVerified = 1;
                    pin.Threshold = 10000; //Definido por BioPortal
                    pin.Userid = 0;
                    pin.Verifybyconnectorid = "NONE"; //Fijo
                    pin.InsertOption = 2;  //No
                    pin.PersonalData = new PersonalData();
                    pin.PersonalData.Typeid = "RUT";
                    pin.PersonalData.Valueid = _rut;
                    pin.OperationOrder = 1;  //Solo Local

                    string xmlparamin = XmlUtils.SerializeAnObject(pin);

                    //System.IO.StreamWriter sw = new System.IO.StreamWriter(@"c:\tmp\xmlNew.xml");
                    //sw.Write(xmlparamin);
                    //sw.Close();


                    _ret_actual = target.Verify(xmlparamin, out xmlparamout);
                }

                //Si el retorno = 0 => Está ok la ejecución, y hay que ver que resultado de verificación dio
                if (_ret_actual == 0) { //Ejecuto ok, deserializopara usar la info
                    XmlParamOut paramout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                    _matchok = (paramout.Result == 1) ;//  "Positiva" = 1 : "Negativa" = 2);
                    _score = paramout.Score;
                    _trackid = paramout.Trackid;

                }
            }
            catch (Exception ex)
            {
                _ret_actual = -1;
                //LOG - Agregar para tner log de errores
            }
            return _ret_actual;
        }

        internal static int EnrollBioPortal(string _rut, string _clienteId, string _endUser, string _ipUser, string _serialIdSensor,
                                            short _fingerId, string _token, short _typeDevice, out string _trackid)
        {
            int _ret_actual = 0;
            string xmlparamout = string.Empty;
            _trackid = null;

            try
            {
                using (BioPortalServerWSWEB.BioPortalServerWSWeb target = new BioPortalServerWSWEB.BioPortalServerWSWeb())
                {
                    target.Timeout = Properties.Settings.Default.WSTimeout;
                    target.Url = Properties.Settings.Default.Bio_Guia_Trabajo_BioPortalServerWSWEB_BioPortalServerWSWeb;
                    XmlParamIn pin = new XmlParamIn();
                    pin.Actionid = Bio.Portal.Server.Api.Constant.Action.ACTION_ENROLL;  //Enroll
                    
                    pin.Additionaldata = null;
                    pin.Authenticationfactor = 2; //Fingerprint
                    pin.Minutiaetype = 24; //Token
                    pin.Bodypart = 1;  //pulgar derecho
                    pin.Clientid = _clienteId;  //Puedes sacarlo del Id de estacion de trabajo, algun identificador del cliente o mismo Companyid
                    pin.Companyid = Properties.Settings.Default.IdCompany_BP;  //Biometrika o un ID valido segun sea la empresa dada de alta en BioPortal
                    pin.Enduser = _endUser; //identificador de operador, quiza el ID Sensor
                    pin.Ipenduser = _ipUser; //ip estación de trabajo
                    pin.Matchingtype = 1;
                    pin.Origin = Properties.Settings.Default.OriginBP; //Este valor debemos definirlo. Es el Origen de la verificación, por lo que debemos definir 
                    //Ese valor fijo en el BioPortal. Por ahora configuralo en 1 = Desconocido (Pero configurable)   

                    pin.SampleCollection = null; //new List<Sample>();
                    //Sample sample = new Sample();
                    //sample.Data = _token;
                    //sample.Minutiaetype = 24; //Token
                    //sample.Additionaldata = null;
                    //pin.SampleCollection.Add(sample);

                    pin.SaveVerified = 1;
                    pin.Threshold = 10000; //Definido por BioPortal
                    pin.Userid = 0;
                    pin.Verifybyconnectorid = "NONE"; //Fijo
                    pin.InsertOption = 2;  //No
                    pin.PersonalData = new PersonalData();
                    pin.PersonalData.Typeid = "RUT";
                    pin.PersonalData.Valueid = _rut;
                    pin.PersonalData.Name = "Pepe";
                    pin.PersonalData.Patherlastname = "Candia";
                    pin.PersonalData.Motherlastname = "Coco";
                    pin.PersonalData.Dynamicdata = new DynamicDataItem[1];
                    DynamicDataItem item = new DynamicDataItem();
                    item.key = "TipoIdentity";
                    item.value = "Usuario";
                    pin.PersonalData.Dynamicdata[0] = item;

                    pin.OperationOrder = 1;  //Solo Local

                    string xmlparamin = XmlUtils.SerializeAnObject(pin);

                    //System.IO.StreamWriter sw = new System.IO.StreamWriter(@"c:\tmp\xmlNew.xml");
                    //sw.Write(xmlparamin);
                    //sw.Close();


                    _ret_actual = target.Enroll(xmlparamin, out xmlparamout);
                }

                //Si el retorno = 0 => Está ok la ejecución, y hay que ver que resultado de verificación dio
                if (_ret_actual == 0)
                { //Ejecuto ok, deserializopara usar la info
                    XmlParamOut paramout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                    //_matchok = (paramout.Result == 1);//  "Positiva" = 1 : "Negativa" = 2);
                    //_score = paramout.Score;
                    _trackid = paramout.Trackid;

                }
            }
            catch (Exception ex)
            {
                _ret_actual = -1;
                //LOG - Agregar para tner log de errores
            }
            return _ret_actual;
        }


        //internal static int GetISOCompactFromBioPortal(string _sampleISO, out string _sampleISOCompact)
        //{
        //    int _ret_actual;
        //    string xmlparamout = string.Empty;
        //    _sampleISOCompact = null;

        //    try
        //    {
        //        using (BioPortalServerUtils.Bio_Portal_Server_Utils target = new BioPortalServerUtils.Bio_Portal_Server_Utils())
        //        {
        //            target.Timeout = 60000;
        //            target.Url = Properties.Settings.Default.Bio_Core_TokenCI050101_Gen_Test_BioPortalServerUtils_Bio_Portal_Server_Utils;

        //            _ret_actual = target.ISOToISOC(_sampleISO, out _sampleISOCompact);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _ret_actual = -1;
        //        //LOG - Agregar para tner log de errores
        //    }
        //    return _ret_actual;
        //}

    }
}
