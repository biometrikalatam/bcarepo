﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Document.Electronic.Common.Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Database.PersistentManager.PathConf = "D:\\TFSN\\Documento Electronico\\Proyecto\\v6.0-branch\\Biometrika.Document.Electronic.Common.Test\\bin\\Debug";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "Buscando company con ID = " + textBox1.Text;
            Database.Company company = Database.AdministradorCompany.BuscarCompanyById(Convert.ToInt32(textBox1.Text));
            if (company != null)
            {
                richTextBox1.Text = richTextBox1.Text + Environment.NewLine + company.Name + " | " + company.Rut;
            } else
            {
                richTextBox1.Text = richTextBox1.Text + Environment.NewLine + "Company == NULL";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "Buscando DeDcoument con trackid = " + textBox2.Text;
            Database.DeDocument DeDoc = Database.AdministradorDeDocument.BuscarDocumentosByTrackId(textBox2.Text, true);
            if (DeDoc != null)
            {
                richTextBox1.Text = richTextBox1.Text + Environment.NewLine + DeDoc.Id + " | " + DeDoc.Status.ToString();
                richTextBox1.Text = richTextBox1.Text + Environment.NewLine + " Property=Callbackurl / Value=" +  
                                        GetValueOfProperty(DeDoc, "Callbackurl") + " Property=CreateDate / Value=" +
                                        GetValueOfProperty(DeDoc, "Createdate", "dd/MM/yyyy");
            }
            else
            {
                richTextBox1.Text = richTextBox1.Text + Environment.NewLine + "DeDoc == NULL";
            }
        }

        public string GetValueOfProperty(object obj, string property, string format = null)
        {
            string ret = null;
            try
            {
                //log.Debug("Utils.GetValueOfPropert IN...");
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
                {
                    if (descriptor.Name.Equals(property))
                    {
                        object objValue = descriptor.GetValue(obj);
                        if (objValue is DateTime && !string.IsNullOrEmpty(format))
                        {
                            ret = ((DateTime)objValue).ToString(format);
                        }
                        else
                        {
                            ret = descriptor.GetValue(obj).ToString();
                        }
                        //log.Debug("Utils.GetValueOfPropert ret => property=" + property + " => value=" + ret);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                //log.Error("Utils.GetValueOfProperty Excp Error: ", ex);
            }
            //log.Debug("Utils.GetValueOfPropert OUT!");
            return ret;
        }
    }
}
