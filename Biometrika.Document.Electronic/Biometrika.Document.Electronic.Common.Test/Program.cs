﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Document.Electronic.Common.Test
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            double d = 0.8855;
            string s = d.ToString("##,##%");
            s = d.ToString("##.##%");
            s = d.ToString("##%");
            s = d.ToString("##.##") + "%";

            d = 88.55;
            s = d.ToString("##.%");

            Dictionary<string, string> _ConfigItems1 =
                JsonConvert.DeserializeObject<Dictionary<string, string>>(
                    System.IO.File.ReadAllText(@"D:\\TFSN\\Documento Electronico\\Proyecto\\v6.0-branch\\Bio.Document.Manager\\bin\\notifyconfig\\notify_de_tramitestag.json"));

            Dictionary<string, string> _ConfigItems = new Dictionary<string, string>();
            _ConfigItems.Add("DestinatariesList", "gsuhit@biometrika.cl,gsuhit@gmail.com");
            _ConfigItems.Add("SmtpServer", "smtp.gmail.com");
            _ConfigItems.Add("SmtpPort", "587");
            _ConfigItems.Add("SmtpUser", "soporte@biometrika.cl");
            _ConfigItems.Add("SmtpClave", "B1ometrika2014");
            _ConfigItems.Add("SmtpDisplayName", "Notify Biometrika DE (Dev)");
            _ConfigItems.Add("PathLogoHeader", @"D:\TFSN\Documento Electronico\Proyecto\v6.0-branch\Bio.Document.Manager\bin\notifyconfig\1_logoheader.jpg");
            _ConfigItems.Add("PathLogoFooter", @"D:\TFSN\Documento Electronico\Proyecto\v6.0-branch\Bio.Document.Manager\bin\notifyconfig\logo_Mail_Fotter_Gral.jpg");

            Config Conf = new Config();
            Conf.ConfigItems = _ConfigItems;
            //System.IO.File.WriteAllText(@"c:\tmp\test.xml", Bio.Core.Utils.XmlUtils.SerializeObject<Config>(Conf));
            //System.IO.File.WriteAllText(@"c:\tmp\test.json", JsonConvert.SerializeObject(Conf));
            System.IO.File.WriteAllText(@"c:\tmp\testDict.json", JsonConvert.SerializeObject(_ConfigItems));

            //double d = 0.885669;
            //string s = d.ToString("##.##%");
            //int i = 0;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            ConfigurationManager.AppSettings["PathHibernate"] = 
                "D:\\TFSN\\Documento Electronico\\Proyecto\\v6.0-branch\\Biometrika.Document.Electronic.Common.Test\\bin\\Debug";
        }

        public class Config
        {
            public Dictionary<string, string> ConfigItems;
            public Config() { }
        }
    }
}
