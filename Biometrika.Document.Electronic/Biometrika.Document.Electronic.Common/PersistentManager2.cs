using System;
using NHibernate.Cfg;
using NHibernate;

namespace Bio.Digital.Receipt.Core.Database
{
    public static class  PersistentManager2
    {
        public static ISession sesion;
        public static String PathConf;

       
                public static void Init()
                {
                    Configuration cfg = new Configuration();

                    cfg.Configure(PathConf + "\\Biometrika.Document.Electronic.cfg.xml");
    	
				    ISessionFactory sesiones = cfg.BuildSessionFactory();
    	
				    sesion = sesiones.OpenSession();
                }
                

    }
}
