using System;
using System.Collections.Generic;
using System.Text;
using NHibernate;
using System.Web;
using NHibernate.Cfg;
using log4net;

namespace Biometrika.Document.Electronic.Database
{
    public static class PersistentManager
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(PersistentManager));

        private const string CurrentSessionKey = "nhibernate.current_session";
        private static readonly ISessionFactory sessionFactory;
        private static ISession _SessionCurrent;
        public static string PathConf;

        static PersistentManager()
        {
            try
            {
                LOG.Debug("PersistentManager IN...");
                Configuration cfg = new Configuration();

                string path = "";

                if (System.Configuration.ConfigurationManager.AppSettings != null &&
                    System.Configuration.ConfigurationManager.AppSettings.Get("PathHibernate") != null)
                {
                    path = System.Configuration.ConfigurationManager.AppSettings.Get("PathHibernate").ToString();
                } else
                {
                    path = "Biometrika.Document.Electronic.cfg.xml";
                }
                LOG.Debug("PersistentManager - Path de cfg.Configure(path) => " + path + "...");
                cfg.Configure(path);
                LOG.Debug("PersistentManager - cfg.BuildSessionFactory() IN...");
                sessionFactory = cfg.BuildSessionFactory();//new Configuration().Configure().BuildSessionFactory();
                LOG.Debug("PersistentManager - sessionFactory!=null => " + (sessionFactory!=null).ToString());
            }
            catch (Exception ex)
            {
                LOG.Error("PersistentManager Excp Error: " + ex.Message);
            }
            LOG.Debug("PersistentManager OUT!");
        }

        public static ISession session()
        {
            ISession currentSession = null;
            HttpContext context = HttpContext.Current;
            if (context == null)
            {
                currentSession = _SessionCurrent;
            } else
            {
                currentSession = context.Items[CurrentSessionKey] as ISession;
            }
            

            if (currentSession == null || !currentSession.IsOpen)
            {
                currentSession = sessionFactory.OpenSession();
                if (context == null)
                {
                    _SessionCurrent = currentSession;
                } else
                {
                    context.Items[CurrentSessionKey] = currentSession;
                }
                    
            }

            return currentSession;
        }

        public static void CloseSession()
        {
            ISession currentSession = null;
            HttpContext context = HttpContext.Current;
            if (context == null)
            {
                currentSession = _SessionCurrent;
            }
            else
            {
                currentSession = context.Items[CurrentSessionKey] as ISession;
            }
            

            if (currentSession == null)
            {
                // No current session
                return;
            }
            currentSession.Close();
            if (context == null)
            {
                _SessionCurrent = null;
            } else
            {
                context.Items.Remove(CurrentSessionKey);
            }
                
        }

        public static void CloseSessionFactory()
        {
            if (sessionFactory != null)
            {
                sessionFactory.Close();
            }
        }
    }

}

