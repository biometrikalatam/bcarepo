using System;
using System.Text;
using System.Collections.Generic;


namespace Biometrika.Document.Electronic.Database {
    
    public partial class DeSignatoryEvidences {
        public virtual int Id { get; set; }
        public virtual DeSignatory DeSignatory { get; set; }
        public virtual string Name { get; set; }
        public virtual string Value { get; set; }
        public virtual DateTime? Createdate { get; set; }
        public virtual TypeImage Typeimage { get; set; }
        public virtual TypeImageFormat Typeimageformat { get; set; } //= TypeImageFormat.jpg; //0-jpg | 1-png | 2-bmp

        public DeSignatoryEvidences() { }

        public DeSignatoryEvidences(DeSignatory _DeSignatory, string _Name, string _Value, 
                                    TypeImage _Typeimage, TypeImageFormat _Typeimageformat) {
            DeSignatory = _DeSignatory;
            Name = _Name;
            Value = _Value;
            Createdate = DateTime.Now;
            Typeimage = _Typeimage;
            Typeimageformat = _Typeimageformat;
        }


    }

    public enum TypeImageFormat
    {
        jpg = 0,
        png = 1,
        bmp = 2,
        pdf = 3,
        json = 4,
        video = 5,
        videourl = 6
    }

    public enum TypeImage
    {
        cedulafront = 0,
        cedulaback = 1,
        selfie = 2,
        cedulasignature = 3,
        cedulafoto = 4,
        manualsignature = 5,
        qr = 6,
        image = 7,
        facemap = 8,
        pdf = 9,
        json = 10,
        video = 11
    }
}
