using System;
using System.Text;
using System.Collections.Generic;


namespace Biometrika.Document.Electronic.Database {

    public class DeTemplate
    {
        public DeTemplate()
        {
            DeTemplateSignatory = new List<DeTemplateSignatory>();
            DeTemplateTag = new List<DeTemplateTag>();
        }
        public virtual int Id { get; set; }
        public virtual Company Company { get; set; }
        public virtual int? Resumedetemplateid { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string Version { get; set; }
        public virtual string Bytetemplate { get; set; }
        public virtual string Createdate { get; set; }
        public virtual int Singnaturatype { get; set; }
        public virtual string Certificatepfx { get; set; }
        public virtual string Certificatepfxpassword { get; set; }
        public virtual string Certificatecer { get; set; }
        public virtual string Imagesignaturetoshow { get; set; }
        public virtual int Fesshowtype { get; set; }
        public virtual string Fesx { get; set; }
        public virtual string Fesy { get; set; }
        public virtual string Fesw { get; set; }
        public virtual string Fesh { get; set; }
        public virtual string Fesmsgtoshow { get; set; }
        public virtual string Feaurl { get; set; }
        public virtual string Feaapp { get; set; }
        public virtual string Feauser { get; set; }
        public virtual string Listnotifymails { get; set; }
        public virtual string Mailtemplate { get; set; }
        public virtual string Assemblypostgenerate { get; set; }
        
        public virtual IList<DeTemplateSignatory> DeTemplateSignatory { get; set; }
        public virtual IList<DeTemplateTag> DeTemplateTag { get; set; }
    }
}
