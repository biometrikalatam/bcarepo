using System;
using System.Text;
using System.Collections.Generic;


namespace Biometrika.Document.Electronic.Database {
    
    public partial class DeTemplateSignatory {
        public virtual int Id { get; set; }
        public virtual DeTemplate DeTemplate { get; set; }
        public virtual int Signatoryorder { get; set; }
        public virtual int Signaturetype { get; set; }
        public virtual int Workflowid { get; set; }
    }
}
