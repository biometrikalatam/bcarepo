using System;
using System.Text;
using System.Collections.Generic;


namespace Biometrika.Document.Electronic.Database {
    
    public partial class DeDocumentTag {
        public virtual int Id { get; set; }
        public virtual DeDocument DeDocument { get; set; }
        public virtual string Name { get; set; }
        public virtual string Value { get; set; }
        public virtual int? Type { get; set; }
        public virtual int Templatetagid { get; set; }
        public virtual DateTime Createdate { get; set; }

        public DeDocumentTag() { }

        public DeDocumentTag(DeDocument _DeDocument, string _Name, string _Value, int? _Type, 
                             int _Templatetagid, DateTime _Createdate)
        {
            Id = 0;
            DeDocument = _DeDocument;
            Name = _Name;
            Value = _Value;
            Type = _Type;
            Templatetagid = _Templatetagid;
            Createdate = _Createdate;
        }
    }
}
