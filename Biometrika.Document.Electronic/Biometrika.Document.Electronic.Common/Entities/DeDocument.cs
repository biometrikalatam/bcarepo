using System;
using System.Text;
using System.Collections.Generic;


namespace Biometrika.Document.Electronic.Database {

    public class DeDocument
    {
        public DeDocument()
        {
            DeDocumentTag = new List<DeDocumentTag>();
            DeSignatory = new List<DeSignatory>();
            DeTx = new List<DeTx>();
        }
        public virtual int Id { get; set; }
        public virtual Company Company { get; set; }
        public virtual DeTemplate DeTemplate { get; set; }
        public virtual int Status { get; set; }
        public virtual string Trackid { get; set; }
        public virtual string Extension { get; set; }
        public virtual DateTime Createdate { get; set; }
        public virtual DateTime? Expirationdate { get; set; }
        public virtual DateTime? Enddate { get; set; }
        public virtual int Userid { get; set; }
        public virtual string Bytedocument { get; set; }
        public virtual string Attachment { get; set; }
        public virtual string Receipt { get; set; }
        public virtual string Mailtonotify { get; set; }
        public virtual string Carregisterimagefront { get; set; }
        public virtual string Carregisterimageback { get; set; }
        public virtual string Writingimage { get; set; }
        public virtual string Form { get; set; }
        public virtual string Securitycode { get; set; }
        public virtual string Dinamicparam { get; set; }
        public virtual string Callbackurl { get; set; }
        public virtual string QRVerify { get; set; }
        public virtual string Resume { get; set; }

        public virtual IList<DeDocumentTag> DeDocumentTag { get; set; }
        public virtual IList<DeSignatory> DeSignatory { get; set; }
        public virtual IList<DeTx> DeTx { get; set; }
    }
}
