using System;
using System.Text;
using System.Collections.Generic;


namespace Biometrika.Document.Electronic.Database {
    
    public partial class DeTx {
        public virtual int Id { get; set; }
        public virtual DeDocument DeDocument { get; set; }
        public virtual int Type { get; set; }
        public virtual int Actionid { get; set; }
        public virtual string Receipt { get; set; }
        public virtual string Trackidbp { get; set; }
        public virtual string Trackidde { get; set; }
        public virtual DateTime? Dategen { get; set; }
        public virtual int Resultcode { get; set; }
        public virtual string Qrgenerated { get; set; }
        public virtual int Companyid { get; set; }
        public virtual DateTime? Lastmodify { get; set; }
        public virtual string Typeid { get; set; }
        public virtual string Valueid { get; set; }
        public virtual string Score { get; set; }
        public virtual string Threshold { get; set; }
        public virtual string Username { get; set; }
    }
}
