using System;
using System.Text;
using System.Collections.Generic;


namespace Biometrika.Document.Electronic.Database {
    
    public partial class DeTemplateTag {
        public virtual int Id { get; set; }
        public virtual DeTemplate DeTemplate { get; set; }
        public virtual string Name { get; set; }
        public virtual int Type { get; set; }
        public virtual string Parameters { get; set; }
        public virtual string Translate { get; set; }
        public virtual string Source { get; set; } //detx | dedocument | designatory | designatoryevidences | form | custom
        public virtual string Format { get; set; } //PAra generar formateo de dato por ejemplo en Date => dd-MM-yyyy | dd-MM-yyyy HH:mm:ss | DATETIMEMILISEC 
        public virtual int Signatoryorder { get; set; } //Si Source = designatory | designatoryevidences | form | custom
                                                        // debemos saber de cual de los signatory se trata para obtener el valor
    }
}
