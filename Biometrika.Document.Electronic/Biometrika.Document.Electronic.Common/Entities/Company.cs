using System;
using System.Text;
using System.Collections.Generic;


namespace Biometrika.Document.Electronic.Database
{

    public class Company
    {
        public Company()
        {
            DeCompanyWorkflow = new List<DeCompanyWorkflow>();
            DeDocument = new List<DeDocument>();
            DeTemplate = new List<DeTemplate>();
        }
        public virtual int Id { get; set; }
        public virtual string Rut { get; set; }
        public virtual string Address { get; set; }
        public virtual string Name { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Phone2 { get; set; }
        public virtual string Fax { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual DateTime? Endate { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string Domain { get; set; }
        public virtual string Additionaldata { get; set; }
        public virtual string Contactname { get; set; }
        public virtual int Status { get; set; }
        public virtual int? Holding { get; set; }
        public virtual string Accessname { get; set; }
        public virtual string Secretkey { get; set; }
        public virtual int Companyidbp { get; set; }
        public virtual string Accessnamebp { get; set; }
        public virtual string Secretkeybp { get; set; }
        public virtual int Companyidnv { get; set; }
        public virtual string Accessnamenv { get; set; }
        public virtual string Secretkeynv { get; set; }
        public virtual string Mailtemplate { get; set; }
        public virtual IList<DeCompanyWorkflow> DeCompanyWorkflow { get; set; }
        public virtual IList<DeDocument> DeDocument { get; set; }
        public virtual IList<DeTemplate> DeTemplate { get; set; }
    }
}
