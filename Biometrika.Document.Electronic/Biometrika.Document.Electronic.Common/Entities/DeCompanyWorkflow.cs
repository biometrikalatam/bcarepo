using System;
using System.Text;
using System.Collections.Generic;


namespace Biometrika.Document.Electronic.Database {
    
    public partial class DeCompanyWorkflow {
        public virtual int Id { get; set; }
        public virtual Company Company { get; set; }
        public virtual string Name { get; set; }
        public virtual int Onboardingmandatory { get; set; }
        public virtual string Bpwebtypeonboarding { get; set; }
        public virtual string Bpwebtypeverify { get; set; }
        public virtual string Receiverlist { get; set; }
        public virtual float? Threshold { get; set; }
        public virtual DateTime Datecreate { get; set; }
        public virtual DateTime Dateupdate { get; set; }
        public virtual int Autorization { get; set; }
        public virtual string Autorizationmessage { get; set; }
        public virtual int Signerinclude { get; set; }
        public virtual int Checklocksrcei { get; set; }
        public virtual int Videoinclude { get; set; }
        public virtual string Videomessage { get; set; }
        public virtual string Theme { get; set; }
        public virtual int Checkadult { get; set; }
        public virtual int Checkexpired { get; set; }
        public virtual int Georefmandatory { get; set; }
        public virtual string Redirecturl { get; set; }
        public virtual string Successurl { get; set; }
        public virtual int Carregisterinclude { get; set; }
        public virtual int Writinginclude { get; set; }
        public virtual string Forminclude { get; set; }
    }
}
