using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using System.Collections;
using Biometrika.Document.Electronic.Api.Model;
using Biometrika.Document.Electronic.Api;
using System.Security.Cryptography;

namespace Biometrika.Document.Electronic.Database
{
    public class AdministradorDeDocument
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AdministradorDeDocument));

        public static string GetTrackIdUnique()
        {
            DeDocument nvUP;
            string retGuid = null;
            try
            {
                bool isOK = false;
                log.Debug("AdministradorDeDocument.GetTrackIdUnique IN...");
                while (!isOK)
                {
                    retGuid = Guid.NewGuid().ToString("N");
                    log.Debug("AdministradorDeDocument.GetTrackIdUnique - Chequeando TrackId = " + retGuid);
                    using (ISession session = PersistentManager.session())
                    {
                        nvUP = PersistentManager.session().CreateCriteria(typeof(DeDocument))
                                         .Add(NHibernate.Criterion.Expression.Eq("Trackid", retGuid))
                                         .UniqueResult<DeDocument>();
                    }
                    if (nvUP != null)
                    {
                        log.Debug("AdministradorDeDocument.GetTrackIdUnique - Existe GUID => Se genera una nueva...");
                        nvUP = null;
                    }
                    else
                    {
                        log.Debug("AdministradorDeDocument.GetTrackIdUnique GUID OK!");
                        isOK = true;
                    }
                }
            }
            catch (Exception ex)
            {
                retGuid = Guid.NewGuid().ToString("N"); ;
                log.Error("AdministradorDeDocument.GetTrackIdUnique Error", ex);
            }
            log.Debug("AdministradorDeDocument.GetTrackIdUnique OUT!");
            return retGuid;
        }

        public static DeDocument BuscarDocumentoById(int Id)
        {
            DeDocument documento = null;
            using (ISession session = PersistentManager.session())
            {
                documento = session.CreateCriteria(typeof(DeDocument))
                                                 .Add(Expression.Eq("Id", Id))
                                                 .UniqueResult<DeDocument>();
            }
            return documento;
        }

        public static IList<DeDocument> BuscarDocumentosByFilter(DateTime fdesde, DateTime fhasta,
                                                                   int companyid, int templateid)
        {
            IList<DeDocument> doc = null;
            try
            {
                using (ISession session = PersistentManager.session())
                {
                    ICriteria crit = session.CreateCriteria(typeof(DeDocument));

                    crit.Add(Expression.Between("Createdate", fdesde, fhasta));
                    if (companyid != 0)
                    {
                        crit.Add(Expression.Eq("Company", AdministradorCompany.BuscarCompanyById(companyid)));
                    }

                    if (templateid != 0)
                    {
                        crit.Add(Expression.Eq("DeTemplate", AdministradorDeTemplate.SearchDeTemplateById(templateid)));
                    }

                    doc = crit.AddOrder(Order.Desc("Createdate")).List<DeDocument>();
                }

            }
            catch (Exception ex)
            {
                log.Error("Error AdministradorDeDocument.BuscarDocumentosByFilter", ex);
                doc = null;
            }


            return doc;

        }

        public static DeDocument BuscarDocumentosByTrackId(string TrackId, bool withChild = false)
        {
            DeDocument doc = null;
            try
            {
                using (ISession session = PersistentManager.session())
                {
                    ICriteria crit = session.CreateCriteria(typeof(DeDocument));

                    doc = session.CreateCriteria(typeof(DeDocument))
                                                  .Add(Expression.Eq("Trackid", TrackId))
                                                  .UniqueResult<DeDocument>();

                    if (withChild)
                    {
                        NHibernateUtil.Initialize(doc.Company);
                        NHibernateUtil.Initialize(doc.DeDocumentTag);
                        NHibernateUtil.Initialize(doc.DeSignatory);
                        foreach (DeSignatory item in doc.DeSignatory)
                        {
                            NHibernateUtil.Initialize(item.DeSignatoryEvidences);
                        }
                        NHibernateUtil.Initialize(doc.DeSignatory);
                        NHibernateUtil.Initialize(doc.DeTemplate);
                        NHibernateUtil.Initialize(doc.DeTx);
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error("Error AdministradorDeDocument.BuscarDocumentosByTrackId", ex);
                doc = null;
            }


            return doc;

        }

        public static DeDocument BuscarDocumentoByTrackIdBP(string trackidbp, out string msgerr)
        {
            DeDocument doc = null;
            msgerr = null;
            try
            {
                log.Debug("AdministradorDeDocument.BuscarDocumentoByTrackIdBP IN...");
                using (ISession session = PersistentManager.session())
                {
                    ICriteria crit = session.CreateCriteria(typeof(DeSignatory));
                    log.Debug("AdministradorDeDocument.BuscarDocumentoByTrackIdBP - Ingreso a buscar DeSignatory con " +
                                "Trackidbp = " + trackidbp + "...");
                    DeSignatory deSig = session.CreateCriteria(typeof(DeSignatory))
                                                  .Add(Expression.Eq("Trackidbp", trackidbp))
                                                  .UniqueResult<DeSignatory>();
                    if (deSig != null)
                    {
                        log.Debug("AdministradorDeDocument.BuscarDocumentoByTrackIdBP - " + 
                                    "DeSignatory encontrado [id=" + deSig.Id + "|valueid=" + deSig.Valueid +   
                                    "] => Fill child DeDocument...");
                        NHibernateUtil.Initialize(deSig.DeDocument);
                        log.Debug("AdministradorDeDocument.BuscarDocumentoByTrackIdBP - Recupero DeDocuemnt completo...");
                        doc = BuscarDocumentosByTrackId(deSig.DeDocument.Trackid, true);
                        log.Debug("AdministradorDeDocument.BuscarDocumentoByTrackIdBP - doc recuperado => " +
                                    (doc!=null? "Id=" + doc.Id + "-TrackId=" + doc.Trackid : "NULL"));

                    } else
                    {
                        log.Warn("AdministradorDeDocument.BuscarDocumentoByTrackIdBP - DeSignatory con trackidbp = " +
                            trackidbp + " no se pudo recuoperar => Sale con DeDoc = Null!");
                    }
                    
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorDeDocument.BuscarDocumentoByTrackIdBP Excp Error", ex);
                doc = null;
            }
            log.Debug("AdministradorDeDocument.BuscarDocumentoByTrackIdBP OUT!");
            return doc;
        }

        public static int CreateDocument(string bydocument, int companyid, int templateid, IList<Api.Model.TagProcessed> tagvalues,
                                         string dynamicparam, DateTime? expdate, string form, string maillist,
                                         string writingimage, string receipt, out DeDocument oDeDocument, out string msgerr)
        {
            int ret = 0;
            oDeDocument = null;
            msgerr = null;
            try
            {
                oDeDocument = new DeDocument();
                oDeDocument.Id = 0;
                oDeDocument.Bytedocument = bydocument;
                oDeDocument.Company = AdministradorCompany.BuscarCompanyById(companyid);
                oDeDocument.Createdate = DateTime.Now;
                oDeDocument.DeTemplate = AdministradorDeTemplate.SearchDeTemplateById(templateid);
                oDeDocument.Dinamicparam = dynamicparam;
                oDeDocument.Expirationdate = expdate != null ? expdate : null;
                oDeDocument.Extension = "pdf";
                oDeDocument.Form = form;
                oDeDocument.Mailtonotify = maillist;
                oDeDocument.Trackid = GetTrackIdUnique();
                oDeDocument.Writingimage = writingimage;
                oDeDocument.Receipt = receipt;
                oDeDocument.DeDocumentTag = AdministradorDeDocumentTag.CreateListTags(oDeDocument, tagvalues);
                using (ISession session = PersistentManager.session())
                {
                    session.Save(oDeDocument);
                    session.Flush();
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                oDeDocument = null;
                log.Error("AdministradorDeDocument.CreateDeTx", ex);
            }

            return ret;
        }

        

        public static int CreateDocument(CreateDeModel param, Company company, List<DeSignatory> listSignatories, 
                                         string trackid, string qrverify, out string msg, out DeDocument oDeDocument)
        {
            int ret = 0;
            oDeDocument = null;
            msg = null;
            try
            {
                log.Debug("CreateDocument IN - trackid = " + trackid);
                oDeDocument = new DeDocument();
                oDeDocument.Id = 0;
                oDeDocument.Trackid = trackid;
                oDeDocument.Bytedocument = null; // bydocument;
                oDeDocument.Company = company; // AdministradorCompany.BuscarCompanyById(companyid);
                oDeDocument.Createdate = DateTime.Now;
                oDeDocument.DeTemplate = AdministradorDeTemplate.SearchDeTemplateById(param.templateid, true);
                oDeDocument.Dinamicparam = null; // dynamicparam;
                oDeDocument.Createdate = DateTime.Now;
                DateTime? dtexp = null;
                if (param.daysforexpiration > 0) {
                    dtexp = oDeDocument.Createdate.AddDays(param.daysforexpiration);
                }
                oDeDocument.Expirationdate = dtexp; // expdate != null ? expdate : null;
                oDeDocument.Extension = "pdf";
                oDeDocument.Form = null; // form;
                oDeDocument.Mailtonotify = GenerateMailListToNotify(oDeDocument.DeTemplate.Listnotifymails, listSignatories);
                //oDeDocument.Trackid = GetTrackIdUnique();
                oDeDocument.Writingimage = null; // writingimage;
                oDeDocument.Receipt = null; // receipt;
                oDeDocument.DeSignatory = listSignatories;
                oDeDocument.DeDocumentTag = null; // AdministradorDeDocumentTag.CreateListTags(oDeDocument, tagvalues);
                oDeDocument.Callbackurl = param.callbackurl;
                oDeDocument.QRVerify = qrverify; 
                foreach (DeSignatory item in listSignatories)
                {
                    item.DeDocument = oDeDocument;
                }
                oDeDocument.DeSignatory = listSignatories;

                //Si vienen tags con valores desde app de 3ros se graban ahora para usarse cuando se genera el documento
                if (param.tagsgivenlist != null)
                {
                    log.Debug("CreateDocument - Ingresa a grabar Tags Givens...");
                    int type;
                    int templatetagid;
                    oDeDocument.DeDocumentTag = new List<DeDocumentTag>();
                    foreach (Tag itemTag in param.tagsgivenlist)
                    {
                        templatetagid = 0;
                        try
                        {
                            type = Convert.ToInt32(itemTag.Type);
                        }
                        catch (Exception ex)
                        {
                            type = 0; //string
                        }
                        try
                        {
                            foreach (DeTemplateTag itemT in oDeDocument.DeTemplate.DeTemplateTag)
                            {
                                if (itemT.Name.Equals(itemTag.IdTag))
                                {
                                    templatetagid = itemT.Id;
                                    break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            templatetagid = 0;
                        }
                        DeDocumentTag dedt = new DeDocumentTag(oDeDocument, itemTag.IdTag, itemTag.Value, type, templatetagid,
                                                               DateTime.Now);
                        oDeDocument.DeDocumentTag.Add(dedt);
                    }
                    log.Debug("CreateDocument - Agregados " + oDeDocument.DeDocumentTag.Count.ToString() + " Tags Givens...");
                }

                log.Debug("CreateDocument - Entra a Save...");
                using (ISession session = PersistentManager.session())
                {
                    session.Save(oDeDocument);
                    session.Flush();
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                oDeDocument = null;
                log.Error("AdministradorDeDocument.CreateDocument Excp Error", ex);
            }
            log.Debug("AdministradorDeDocument.CreateDocument OUT! ret=" + ret.ToString());
            return ret;
        }


        public static int Update(string trackid, IList<TagProcessed> tagsProcessed, byte[] byteDE)
        {
            int ret = 0;
            DeDocumentTag dbTag;
            try
            {
                log.Debug("AdministradorDeDocument.Update IN...");
                DeDocument doc = BuscarDocumentosByTrackId(trackid, false);
                if (doc != null)
                {
                    doc.Bytedocument = Convert.ToBase64String(byteDE);
                    //Agrego los valores de tags para grabarlos y con esto se saca el SecurityCode
                    doc.DeDocumentTag = new List<DeDocumentTag>();
                    foreach (TagProcessed item in tagsProcessed)
                    {
                        dbTag = null;
                        dbTag = new DeDocumentTag(doc, item.IdTag, item.value.ToString(), item.Type, 0, DateTime.Now);
                        doc.DeDocumentTag.Add(dbTag);
                    }
                    doc.Securitycode = GenerateSecurityCode(doc.DeDocumentTag);
                    doc.Status = Constant.DEDOCUMENT_STATUS_COMPLETED;
                    log.Debug("AdministradorDeDocument.Update - Updating doc con trackid = " + trackid + 
                                " - Len Doc = " + byteDE.Length.ToString());
                    using (ISession session = PersistentManager.session())
                    {
                        session.SaveOrUpdate(doc);
                        session.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                log.Error("AdministradorDeDocument.Update Excp Error", ex);
            }
            log.Debug("AdministradorDeDocument.Update OUT! ret=" + ret.ToString());
            return ret;
        }

        public static int Update(DeDocument doc)
        {
            int ret = 0;
            try
            {
                log.Debug("AdministradorDeDocument.Update IN...");
                if (doc != null)
                {
                    log.Debug("AdministradorDeDocument.Update - Updating doc con trackid = " + doc.Trackid +
                                " - Len Doc = " + doc.Bytedocument.Length.ToString());
                    using (ISession session = PersistentManager.session())
                    {
                        session.SaveOrUpdate(doc);
                        session.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                log.Error("AdministradorDeDocument.Update Excp Error", ex);
            }
            log.Debug("AdministradorDeDocument.Update OUT! ret=" + ret.ToString());
            return ret;
        }

        private static string GenerateMailListToNotify(string listnotifymails, List<DeSignatory> listSignatories)
        {
            string ret = null;
            bool isFirst = true;
            try
            {
                if (!string.IsNullOrEmpty(listnotifymails)) ret = listnotifymails;

                foreach (DeSignatory item in listSignatories)
                {
                    if (isFirst)
                    {
                        isFirst = false;
                        ret = item.Mail;
                    } else
                    {
                        ret = ret + "," + item.Mail;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                log.Error(" Error: " + ex.Message);
            }
            return ret;
        }

       

        /// <summary>
        /// Genera Hash con datos de los tags para poder ser verificado.
        /// </summary>
        /// <param name="tags"></param>
        /// <returns></returns>
        private static string GenerateSecurityCode(IList<DeDocumentTag> tags)
        {
            string codeReturn = "Null";
            string aux = "";
            try
            {
                log.Debug("AdministradorDeDocument.GenerateSecurityCode IN...");
                if (tags == null)
                {
                    log.Warn("AdministradorDeDocument.GenerateSecurityCode Param tags=NULL => Sale con valor Null");
                    return codeReturn;
                }

                log.Debug("DEUnitOfWork.GenerateSecurityCode - Concatena " + tags.Count.ToString() + " tags...");
                foreach (DeDocumentTag item in tags)
                {
                    log.Debug("DEUnitOfWork.GenerateSecurityCode - Concatena " + item.Name + "...");
                    aux = aux + (string.IsNullOrEmpty(item.Value) ? "" : item.Value);
                }

                log.Debug("AdministradorDeDocument.GenerateSecurityCode - GetBytes...");
                byte[] origen = System.Text.ASCIIEncoding.ASCII.GetBytes(aux);

                log.Debug("DEUnitOfWork.GenerateSecurityCode - Calcula SHA256...");
                byte[] result = ((HashAlgorithm)CryptoConfig.CreateFromName("SHA256")).ComputeHash(origen); 
                //Bio.Core.pki.hash.BKHash.Hash(origen, "SHA256");

                codeReturn = Convert.ToBase64String(result);
            }
            catch (Exception ex)
            {
                codeReturn = "Null";
                log.Error("AdministradorDeDocument.GenerateSecurityCode Error: " + ex.Message);
            }
            log.Debug("AdministradorDeDocument.GenerateSecurityCode - OUT! - SecurityCode => " + codeReturn);
            return codeReturn;
        }
    }
}
