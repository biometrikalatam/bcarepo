﻿using log4net;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Document.Electronic.Database
{
    public class AdministradorDeSignatory
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AdministradorDeSignatory));

        public static int Update(DeSignatory signatory, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            try
            {
                log.Debug("AdministradorDeSignatory.Update IN...");
                using (ISession session = PersistentManager.session())
                {
                    session.SaveOrUpdate(signatory);
                    session.Flush();
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Expc Error=" + ex.Message;
                log.Error("AdministradorDeSignatory.Update Excp Error", ex);
            }
            log.Debug("AdministradorDeSignatory.Update OUT! ret=" + ret.ToString());
            return ret;
        }
    }
}
