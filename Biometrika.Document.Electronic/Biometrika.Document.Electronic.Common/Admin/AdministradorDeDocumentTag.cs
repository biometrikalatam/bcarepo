﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Document.Electronic.Database
{
    public class AdministradorDeDocumentTag
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdministradorDeDocumentTag));

        /// <summary>
        /// Dada una lista de tags procesdas, se genera la lista de DeDocumentTag para ser 
        /// salvadas con del DeDocument
        /// </summary>
        /// <param name="tagvalues"></param>
        /// <returns></returns>
        public static IList<DeDocumentTag> CreateListTags(DeDocument document, IList<Api.Model.TagProcessed> tagvalues)
        {
            string auxValue;
            DeDocumentTag itemTag;
            List<DeDocumentTag> listTags = new List<DeDocumentTag>();
            try
            {
                LOG.Debug("AdministradorDeDocumentTag IN...");
                if (tagvalues == null || tagvalues.Count == 0)
                {
                    LOG.Debug("AdministradorDeDocumentTag - Param Nulo => Sale null!");
                    listTags = null;
                } else
                {
                    LOG.Debug("AdministradorDeDocumentTag - Creando lista de tags...");
                    foreach (Api.Model.TagProcessed item in tagvalues)
                    {
                        auxValue = GetValue(item.Type, (string)item.value);
                        itemTag = new DeDocumentTag(document, item.IdTag, auxValue, item.Type, document.DeTemplate.Id,
                                                    document.Createdate);
                        listTags.Add(itemTag);
                        auxValue = null;
                        LOG.Debug("AdministradorDeDocumentTag - Added IdTag = " + item.IdTag + "...");
                    }
                }
            }
            catch (Exception ex)
            {
                listTags = null;
                LOG.Error("AdministradorDeDocumentTag.CreateListTags Excp Error" , ex);
            }
            LOG.Debug("AdministradorDeDocumentTag OUT! => listTags.count = " + 
                        (listTags==null?"Null":listTags.Count.ToString()));
            return listTags;
        }

        private static string GetValue(int type, string value)
        {
            string ret = null;
            try
            {
                /*
                    if (type.Equals("string")) return 0;
                    if (type.Equals("int")) return 1;
                    if (type.Equals("double")) return 2;
                    if (type.Equals("image")) return 3;
                    if (type.Equals("wsq")) return 4;
                */
                switch (type)
                {
                    case 0:
                    case 1:
                    case 2:
                        ret = value;
                        break;
                    case 3:
                    case 4: //Si es image o wsq => Recupero data y paso a Base64 via el path del value
                        try
                        {
                            if (System.IO.File.Exists(value))
                            {
                                ret = Convert.ToBase64String(System.IO.File.ReadAllBytes(value));
                            } else
                            {
                                ret = value;
                            }
                        }
                        catch (Exception ex)
                        {
                            LOG.Error("AdministradorDeDocumentTag.GetValue Excp Error Leyendo data de disco!", ex);
                        }

                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("AdministradorDeDocumentTag.GetValue Ecxp Error: ", ex);
            }
            return ret;
        }
    }
}
