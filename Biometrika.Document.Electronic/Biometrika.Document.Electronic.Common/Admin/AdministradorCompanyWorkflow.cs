using System;
using log4net;
using NHibernate;
//using NHibernate.Expression;

namespace Biometrika.Document.Electronic.Database
{

    /// <summary>
    /// 
    /// </summary>
    public class AdministradorCompanyWorkflow
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(AdministradorCompanyWorkflow));

        public static DeCompanyWorkflow BuscarCompanyWokflowByName(int companyid, string name)
        {
            DeCompanyWorkflow rec = null;

            try
            {
                Company company = AdministradorCompany.BuscarCompanyById(companyid);
                using (ISession session = PersistentManager.session())
                {
                    rec = PersistentManager.session().CreateCriteria(typeof(DeCompanyWorkflow))
                                     .Add(NHibernate.Criterion.Expression.Eq("Company", company))
                                     .Add(NHibernate.Criterion.Expression.Eq("Name", name))
                                     .UniqueResult<DeCompanyWorkflow>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorCompanyWorkflow.BuscarCompanyByName", ex);
            }

            return rec;
        }

        public static DeCompanyWorkflow BuscarCompanyWokflowById(int id)
        {
            DeCompanyWorkflow rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = PersistentManager.session().CreateCriteria(typeof(DeCompanyWorkflow))
                                     .Add(NHibernate.Criterion.Expression.Eq("Id", id))
                                     .UniqueResult<DeCompanyWorkflow>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorCompanyWorkflow.BuscarCompanyWokflowById", ex);
            }

            return rec;
        }


    }
}
