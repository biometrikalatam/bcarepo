﻿using Biometrika.Document.Electronic.Api;
using Biometrika.Document.Electronic.Api.Model;
using log4net;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Biometrika.Document.Electronic.Database
{
    public class AdministradorDeTx
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(AdministradorDeTx));

        //public static DeTx CreateCITx(string trackid, int actionid, int companyid, int validityType, string qrgenerated,
        //                                string typeid, string valueid, string mail, string dynamicparam,
        //                                out string msg)
        public static DeTx CreateDeTx(int actionid, int companyid, int validityType, string qrgenerated,
                                        string typeid, string valueid, out string msg)
        {
            DeTx oDeTx = null;
            msg = null;
            try
            {
                oDeTx = new DeTx();
                oDeTx.Actionid = actionid;
                oDeTx.Companyid = companyid;
                //oDeTx.Validitytype = validityType;
                oDeTx.Dategen = DateTime.Now;
                oDeTx.Lastmodify = DateTime.Now;
                oDeTx.Qrgenerated = qrgenerated;
                //oDeTx.QRValidity = Properties.Settings.Default.QRValidity;
                //oDeTx.ValidityDays = Properties.Settings.Default.CIValidityDays;
                oDeTx.Typeid = typeid;
                oDeTx.Valueid = valueid;
                //oDeTx.DestinataryMail = mail;
                //oDeTx.Dynamicparam = dynamicparam;
                using (ISession session = PersistentManager.session())
                {
                    session.Save(oDeTx);
                    session.Flush();
                }
            }
            catch (Exception ex)
            {
                oDeTx = null;
                log.Error("AdministradorDeTx.CreateDeTx", ex);
            }

            return oDeTx;
        }

        public static DeTx CreateDeTx(int actionid, DeDocument DeDoc, out string msg)
        {
            DeTx oDeTx = null;
            msg = null;
            try
            {
                oDeTx = new DeTx();
                oDeTx.Actionid = actionid;
                oDeTx.Dategen = DateTime.Now;
                oDeTx.Lastmodify = DateTime.Now;
                oDeTx.Resultcode = DeDoc.Status;
                oDeTx.Trackidde = DeDoc.Trackid;
                oDeTx.Companyid = DeDoc.Company.Id;
                oDeTx.DeDocument = DeDoc;
                using (ISession session = PersistentManager.session())
                {
                    session.Save(oDeTx);
                    session.Flush();
                }
            }
            catch (Exception ex)
            {
                oDeTx = null;
                log.Error("AdministradorDeTx.CreateDeTx", ex);
            }

            return oDeTx;
        }

        internal static DeTx GetDeTxById(int txid, out string msg)
        {
            DeTx oDeTx = null;
            msg = null;
            try
            {
                using (ISession session = PersistentManager.session())
                {
                    oDeTx = session.CreateCriteria(typeof(DeTx))
                                     .Add(NHibernate.Criterion.Expression.Eq("Id", txid))
                                     .UniqueResult<DeTx>();
                }
            }
            catch (Exception ex)
            {
                oDeTx = null;
                msg = ex.Message;
                log.Error("AdministradorDeTx.GetDeTxById", ex);
            }

            return oDeTx;
        }

        public static int Update(string trackidde)
        {
            int ret = 0;
            string msg;
            try
            {
                log.Debug("AdministradorDeTx.Update IN...");
                DeTx tx = GetDeTxByTrackIdDE(trackidde, out msg);
                if (tx != null)
                {
                    tx.Lastmodify = DateTime.Now;
                    tx.Resultcode = Constant.DEDOCUMENT_STATUS_COMPLETED;
                    log.Debug("AdministradorDeTx.Update - Updating tx con trackidde = " + trackidde);
                    using (ISession session = PersistentManager.session())
                    {
                        session.SaveOrUpdate(tx);
                        session.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                log.Error("AdministradorDeTx.Update Excp Error", ex);
            }
            log.Debug("AdministradorDeTx.Update OUT! ret=" + ret.ToString());
            return ret;
        }

        //internal static DeTx GetDeTxByTrackId(int actionid, string trackIdCI, int companyid, int validityType, bool createIfNotExist,
        //                                        string typeid, string valueid, string name, string mail,
        //                                        string workstationId, string geoRef, string username, out string msg)
        //{
        //    DeTx oDeTx = null;
        //    msg = null;
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(trackIdCI))
        //        {
        //            using (ISession session = PersistentManager.session())
        //            {
        //                oDeTx = session.CreateCriteria(typeof(DeTx))
        //                                 .Add(NHibernate.Criterion.Expression.Eq("TrackId", trackIdCI))
        //                                 .UniqueResult<DeTx>();
        //            }
        //        }
        //        //Si no existe y pide que se cree => se crea
        //        if (createIfNotExist && oDeTx == null)
        //        {
        //            oDeTx = new DeTx();
        //            oDeTx.ActionId = 1; // actionid; => Creo ActionId 1 dado que aun no he ralizado nada
        //            oDeTx.CompanyId = companyid;
        //            oDeTx.TrackId = GetTrackIdUnique(); // Guid.NewGuid().ToString("N");
        //            oDeTx.ValidityType = validityType;
        //            oDeTx.DateGen = DateTime.Now;
        //            oDeTx.QRValidity = Properties.Settings.Default.QRValidity;
        //            oDeTx.ValidityDays = Properties.Settings.Default.CIValidityDays;
        //            oDeTx.LastModify = DateTime.Now;
        //            oDeTx.TypeId = typeid;
        //            oDeTx.ValueId = valueid;
        //            oDeTx.Name = name;
        //            oDeTx.GeoRef = geoRef;
        //            oDeTx.WorkstationId = workstationId;
        //            oDeTx.UserName = username;
        //            oDeTx.DestinataryMail = string.IsNullOrEmpty(mail) ? "info@notariovirtual.cl" : mail;
        //            using (ISession session = PersistentManager.session())
        //            {
        //                session.Save(oDeTx);
        //                session.Flush();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        oDeTx = null;
        //        msg = ex.Message;
        //        log.Error("AdministradorMdTx.GetCITxByTrackId", ex);
        //    }

        //    return oDeTx;
        //}

        //internal static void UpdateCITx(XmlParamIn oXmlIn, RdRecibos objRD, string trackIdBP, string trackIDE, string certifyPDF, int resulteCode, double score, double threshold,
        //                                string name, string phaterlastname, string motherlastname, string urlvideo, out string msg)
        //{
        //    DeTx oDeTx = null;
        //    msg = null;
        //    try
        //    {
        //        log.Debug("AdministradorMdTx.UpdateCITx IN...");
        //        using (ISession session = PersistentManager.session())
        //        {
        //            log.Debug("AdministradorMdTx.UpdateCITx - Recupero txid=" + oXmlIn.CIParam.TrackId);
        //            oDeTx = PersistentManager.session().CreateCriteria(typeof(DeTx))
        //                             .Add(NHibernate.Criterion.Expression.Eq("TrackId", oXmlIn.CIParam.TrackId))
        //                             .UniqueResult<DeTx>();
        //        }
        //        if (oDeTx != null)
        //        {
        //            log.Debug("AdministradorMdTx.UpdateCITx - Comienza a actualizar...");
        //            oDeTx.ActionId = oXmlIn.Actionid;
        //            oDeTx.CertifyPDF = certifyPDF;
        //            log.Debug("AdministradorMdTx.UpdateCITx - CertifyPDF len = " +
        //                      (string.IsNullOrEmpty(oDeTx.CertifyPDF) ? "null" : oDeTx.CertifyPDF.Length.ToString()));
        //            oDeTx.TrackIdBP = trackIdBP;
        //            log.Debug("AdministradorMdTx.UpdateCITx - trackIdBP len = " +
        //                      (string.IsNullOrEmpty(oDeTx.TrackIdBP) ? "null" : oDeTx.TrackIdBP.Length.ToString()));
        //            oDeTx.TrackIdDE = trackIDE;
        //            log.Debug("AdministradorMdTx.UpdateCITx - TrackIdDE len = " +
        //                      (string.IsNullOrEmpty(oDeTx.TrackIdDE) ? "null" : oDeTx.TrackIdDE.Length.ToString()));
        //            oDeTx.ResultCode = resulteCode;
        //            oDeTx.Score = score.ToString();
        //            log.Debug("AdministradorMdTx.UpdateCITx - Score len = " +
        //                      (string.IsNullOrEmpty(oDeTx.Score) ? "null" : oDeTx.Score.Length.ToString()));
        //            oDeTx.Threshold = threshold.ToString();
        //            log.Debug("AdministradorMdTx.UpdateCITx - Threshold len = " +
        //                      (string.IsNullOrEmpty(oDeTx.Threshold) ? "null" : oDeTx.Threshold.Length.ToString()));
        //            oDeTx.LastModify = DateTime.Now;
        //            oDeTx.ReceiptId = objRD.Id;
        //            oDeTx.Name = string.IsNullOrEmpty(name) ? oDeTx.Name : name;
        //            log.Debug("AdministradorMdTx.UpdateCITx - Name len = " +
        //                      (string.IsNullOrEmpty(oDeTx.Name) ? "null" : oDeTx.Name.Length.ToString()));
        //            oDeTx.PhaterLastName = string.IsNullOrEmpty(phaterlastname) ? oDeTx.PhaterLastName : phaterlastname;
        //            log.Debug("AdministradorMdTx.UpdateCITx - PhaterLastName len = " +
        //                      (string.IsNullOrEmpty(oDeTx.PhaterLastName) ? "null" : oDeTx.PhaterLastName.Length.ToString()));
        //            oDeTx.MotherLastName = string.IsNullOrEmpty(motherlastname) ? oDeTx.MotherLastName : motherlastname;
        //            log.Debug("AdministradorMdTx.UpdateCITx - MotherLastName len = " +
        //                      (string.IsNullOrEmpty(oDeTx.MotherLastName) ? "null" : oDeTx.MotherLastName.Length.ToString()));

        //            oDeTx.IdCardImageFront = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageFront) ? oXmlIn.CIParam.IDCardImageFront : oDeTx.IdCardImageFront;
        //            log.Debug("AdministradorMdTx.UpdateCITx - IdCardImageFront len = " +
        //                      (string.IsNullOrEmpty(oDeTx.IdCardImageFront) ? "null" : oDeTx.IdCardImageFront.Length.ToString()));
        //            oDeTx.IdCardImageBack = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? oXmlIn.CIParam.IDCardImageBack : oDeTx.IdCardImageBack;
        //            log.Debug("AdministradorMdTx.UpdateCITx - IdCardImageBack len = " +
        //                      (string.IsNullOrEmpty(oDeTx.IdCardImageBack) ? "null" : oDeTx.IdCardImageBack.Length.ToString()));
        //            oDeTx.IdCardPhotoImage = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardPhotoImage) ? oXmlIn.CIParam.IDCardPhotoImage : oDeTx.IdCardPhotoImage;
        //            log.Debug("AdministradorMdTx.UpdateCITx - IdCardPhotoImage len = " +
        //                      (string.IsNullOrEmpty(oDeTx.IdCardPhotoImage) ? "null" : oDeTx.IdCardPhotoImage.Length.ToString()));
        //            oDeTx.IdCardSignatureImage = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardSignatureImage) ? oXmlIn.CIParam.IDCardSignatureImage : oDeTx.IdCardSignatureImage;
        //            log.Debug("AdministradorMdTx.UpdateCITx - IdCardSignatureImage len = " +
        //                      (string.IsNullOrEmpty(oDeTx.IdCardSignatureImage) ? "null" : oDeTx.IdCardSignatureImage.Length.ToString()));
        //            oDeTx.Selfie = !string.IsNullOrEmpty(oXmlIn.CIParam.Selfie) ? oXmlIn.CIParam.Selfie : oDeTx.Selfie;
        //            log.Debug("AdministradorMdTx.UpdateCITx - Selfie len = " +
        //                      (string.IsNullOrEmpty(oDeTx.Selfie) ? "null" : oDeTx.Selfie.Length.ToString()));
        //            oDeTx.IssueDate = !string.IsNullOrEmpty(oXmlIn.CIParam.IssueDate) ? oXmlIn.CIParam.IssueDate : oDeTx.IssueDate;
        //            oDeTx.ExprationDate = !string.IsNullOrEmpty(oXmlIn.CIParam.ExpirationDate) ? oXmlIn.CIParam.ExpirationDate : oDeTx.ExprationDate;
        //            oDeTx.BirthDate = !string.IsNullOrEmpty(oXmlIn.CIParam.BirthDate) ? oXmlIn.CIParam.BirthDate : oDeTx.BirthDate;
        //            oDeTx.GeoRef = !string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef) ? oXmlIn.CIParam.GeoRef : oDeTx.GeoRef;
        //            log.Debug("AdministradorMdTx.UpdateCITx - GeoRef len = " +
        //                      (string.IsNullOrEmpty(oDeTx.GeoRef) ? "null" : oDeTx.GeoRef.Length.ToString()));
        //            oDeTx.WorkstationId = !string.IsNullOrEmpty(oXmlIn.CIParam.WorkStationID) ? oXmlIn.CIParam.WorkStationID : oDeTx.WorkstationId;
        //            log.Debug("AdministradorMdTx.UpdateCITx - WorkstationId len = " +
        //                      (string.IsNullOrEmpty(oDeTx.WorkstationId) ? "null" : oDeTx.WorkstationId.Length.ToString()));
        //            oDeTx.Serial = !string.IsNullOrEmpty(oXmlIn.CIParam.Serial) ? oXmlIn.CIParam.Serial : oDeTx.Serial;
        //            oDeTx.Sex = !string.IsNullOrEmpty(oXmlIn.CIParam.Sex) ? oXmlIn.CIParam.Sex : oDeTx.Sex;
        //            oDeTx.Nacionality = !string.IsNullOrEmpty(oXmlIn.CIParam.Nacionality) ? oXmlIn.CIParam.Nacionality : oDeTx.Nacionality;

        //            oDeTx.Map = string.IsNullOrEmpty(oDeTx.Map) ? Utils.GetMapFromGoogleMap(oDeTx.GeoRef) : oDeTx.Map;
        //            log.Debug("AdministradorMdTx.UpdateCITx - Map len = " +
        //                      (string.IsNullOrEmpty(oDeTx.Map) ? "null" : oDeTx.Map.Length.ToString()));
        //            oDeTx.ManualSignatureImage = !string.IsNullOrEmpty(oXmlIn.CIParam.ManualSignatureImage) ? oXmlIn.CIParam.ManualSignatureImage : oDeTx.ManualSignatureImage;
        //            log.Debug("AdministradorMdTx.UpdateCITx - ManualSignatureImage len = " +
        //                      (string.IsNullOrEmpty(oDeTx.ManualSignatureImage) ? "null" : oDeTx.ManualSignatureImage.Length.ToString()));
        //            oDeTx.UrlBPWeb = string.IsNullOrEmpty(oDeTx.UrlBPWeb) ? null : oDeTx.UrlBPWeb;
        //            log.Debug("AdministradorMdTx.UpdateCITx - UrlBPWeb len = " +
        //                      (string.IsNullOrEmpty(oDeTx.UrlBPWeb) ? "null" : oDeTx.UrlBPWeb.Length.ToString()));
        //            oDeTx.CellPhone = string.IsNullOrEmpty(oDeTx.CellPhone) ? null : oDeTx.CellPhone;
        //            log.Debug("AdministradorMdTx.UpdateCITx - CellPhone len = " +
        //                      (string.IsNullOrEmpty(oDeTx.CellPhone) ? "null" : oDeTx.CellPhone.Length.ToString()));
        //            oDeTx.VideoURL = string.IsNullOrEmpty(urlvideo) ? null : urlvideo;
        //            log.Debug("AdministradorMdTx.UpdateCITx - VideoURL len = " +
        //                      (string.IsNullOrEmpty(oDeTx.VideoURL) ? "null" : oDeTx.VideoURL));
        //            log.Debug("AdministradorMdTx.UpdateCITx - Save in...");
        //            using (ISession session = PersistentManager.session())
        //            {
        //                session.Update(oDeTx);
        //                session.Flush();
        //            }
        //            log.Debug("AdministradorMdTx.UpdateCITx - Save out!");
        //        }
        //        log.Debug("AdministradorMdTx.UpdateCITx OUT!");
        //    }
        //    catch (Exception ex)
        //    {
        //        oDeTx = null;
        //        msg = ex.Message;
        //        log.Error("AdministradorMdTx.UpdateCITx", ex);
        //    }

        //}

        //internal static int SaveCITx(DeTx oRdCITx)
        //{
        //    int ret = Errors.IRET_OK;
        //    try
        //    {
        //        log.Debug("AdministradorMdTx.SaveCITx IN...");
        //        if (oRdCITx != null)
        //        {
        //            using (ISession session = PersistentManager.session())
        //            {
        //                session.Update(oRdCITx);
        //                session.Flush();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = Errors.IRET_ERR_DESCONOCIDO;
        //        log.Error("AdministradorMdTx.SaveCITx", ex);
        //    }
        //    log.Debug("AdministradorMdTx.SaveCITx OUT!");
        //    return ret;
        //}

        //internal static void UpdateCITx(XmlParamIn oXmlIn, RdRecibos objRD, string trackIdBP, string trackIDE, string certifyPDF,
        //                                int resulteCode, out string msg)
        //{
        //    DeTx oDeTx = null;
        //    msg = null;
        //    try
        //    {
        //        using (ISession session = PersistentManager.session())
        //        {
        //            oDeTx = PersistentManager.session().CreateCriteria(typeof(DeTx))
        //                             .Add(NHibernate.Criterion.Expression.Eq("TrackId", oXmlIn.CIParam.TrackId))
        //                             .UniqueResult<DeTx>();
        //        }
        //        if (oDeTx != null)
        //        {
        //            oDeTx.ActionId = oXmlIn.Actionid;
        //            oDeTx.CertifyPDF = certifyPDF;
        //            oDeTx.TrackIdBP = trackIdBP;
        //            oDeTx.TrackIdDE = trackIDE;
        //            oDeTx.ResultCode = resulteCode;
        //            oDeTx.LastModify = DateTime.Now;
        //            oDeTx.ReceiptId = objRD.Id;
        //            oDeTx.Name = string.IsNullOrEmpty(oXmlIn.CIParam.Name) ? oDeTx.Name : oXmlIn.CIParam.Name;
        //            oDeTx.PhaterLastName = string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName) ? oDeTx.PhaterLastName : oXmlIn.CIParam.PhaterLastName;
        //            oDeTx.MotherLastName = string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName) ? oDeTx.MotherLastName : oXmlIn.CIParam.MotherLastName;

        //            oDeTx.IdCardImageFront = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageFront) ? oXmlIn.CIParam.IDCardImageFront : oDeTx.IdCardImageFront;
        //            oDeTx.IdCardImageBack = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? oXmlIn.CIParam.IDCardImageBack : oDeTx.IdCardImageBack;
        //            oDeTx.IdCardPhotoImage = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardPhotoImage) ? oXmlIn.CIParam.IDCardPhotoImage : oDeTx.IdCardPhotoImage;
        //            oDeTx.IdCardSignatureImage = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardSignatureImage) ? oXmlIn.CIParam.IDCardSignatureImage : oDeTx.IdCardSignatureImage;
        //            oDeTx.Selfie = !string.IsNullOrEmpty(oXmlIn.CIParam.Selfie) ? oXmlIn.CIParam.Selfie : oDeTx.Selfie;
        //            oDeTx.IssueDate = !string.IsNullOrEmpty(oXmlIn.CIParam.IssueDate) ? oXmlIn.CIParam.IssueDate : oDeTx.IssueDate;
        //            oDeTx.ExprationDate = !string.IsNullOrEmpty(oXmlIn.CIParam.ExpirationDate) ? oXmlIn.CIParam.ExpirationDate : oDeTx.ExprationDate;
        //            oDeTx.BirthDate = !string.IsNullOrEmpty(oXmlIn.CIParam.BirthDate) ? oXmlIn.CIParam.BirthDate : oDeTx.BirthDate;
        //            oDeTx.GeoRef = !string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef) ? oXmlIn.CIParam.GeoRef : oDeTx.GeoRef;
        //            oDeTx.WorkstationId = !string.IsNullOrEmpty(oXmlIn.CIParam.WorkStationID) ? oXmlIn.CIParam.WorkStationID : oDeTx.WorkstationId;
        //            oDeTx.Serial = !string.IsNullOrEmpty(oXmlIn.CIParam.Serial) ? oXmlIn.CIParam.Serial : oDeTx.Serial;
        //            oDeTx.Sex = !string.IsNullOrEmpty(oXmlIn.CIParam.Sex) ? oXmlIn.CIParam.Sex : oDeTx.Sex;


        //            using (ISession session = PersistentManager.session())
        //            {
        //                session.Update(oDeTx);
        //                session.Flush();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        oDeTx = null;
        //        msg = ex.Message;
        //        log.Error("AdministradorMdTx.UpdateCITx", ex);
        //    }

        //}

        //internal static void UpdateCITx(DeTx oDeTx, out string msg)
        //{
        //    msg = null;
        //    try
        //    {
        //        using (ISession session = PersistentManager.session())
        //        {
        //            session.Update(oDeTx);
        //            session.Flush();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        oDeTx = null;
        //        msg = ex.Message;
        //        log.Error("AdministradorMdTx.UpdateCITx", ex);
        //    }

        //}


        /// <summary>
        /// Genera un TrickId y revisa que no este duplicado
        /// </summary>
        /// <returns></returns>
        internal static string GetTrackIdUnique()
        {
            DeTx oDeTx;
            string retGuid = null;
            try
            {
                bool isOK = false;

                while (!isOK)
                {
                    retGuid = Guid.NewGuid().ToString("N");
                    using (ISession session = PersistentManager.session())
                    {
                        oDeTx = PersistentManager.session().CreateCriteria(typeof(DeTx))
                                         .Add(NHibernate.Criterion.Expression.Eq("TrackId", retGuid))
                                         .UniqueResult<DeTx>();
                    }
                    if (oDeTx != null)
                    {
                        oDeTx = null;
                    }
                    else
                    {
                        isOK = true;
                    }
                }
            }
            catch (Exception ex)
            {
                retGuid = Guid.NewGuid().ToString("N"); ;
                log.Error("AdministradorMdTx.GetTrackIdUnique", ex);
            }

            return retGuid;
        }

        //internal static int CreateCITxWeb(string citrackid, CICreateModel paramIn, BPTxCreateR bPResponse, string trackidbp,
        //                                  out string msgerr)
        //{
        //    int ret = 0;
        //    DeTx oDeTx = null;
        //    msgerr = null;
        //    try
        //    {
        //        oDeTx = new DeTx();
        //        oDeTx.ActionId = DefineActionId(paramIn);
        //        oDeTx.CompanyId = paramIn.company;
        //        oDeTx.TrackId = citrackid;
        //        oDeTx.ValidityType = 0;
        //        oDeTx.DateGen = DateTime.Now;
        //        oDeTx.LastModify = DateTime.Now;
        //        oDeTx.QRGenerated = null;
        //        oDeTx.QRValidity = Properties.Settings.Default.QRValidity;
        //        oDeTx.ValidityDays = Properties.Settings.Default.CIValidityDays;
        //        oDeTx.TypeId = paramIn.typeId;
        //        oDeTx.ValueId = paramIn.valueId;
        //        oDeTx.DestinataryMail = paramIn.mailCertify;
        //        oDeTx.TrackIdBP = trackidbp;
        //        oDeTx.UrlBPWeb = bPResponse.verifyUrl;
        //        oDeTx.CellPhone = paramIn.cellNumber;
        //        if (paramIn.listMailsDestinatary != null)
        //        {
        //            bool first = true;
        //            oDeTx.ListMailsDistribution = "";
        //            foreach (string item in paramIn.listMailsDestinatary)
        //            {
        //                if (first)
        //                {
        //                    oDeTx.ListMailsDistribution += item; first = false;
        //                }
        //                else
        //                {
        //                    oDeTx.ListMailsDistribution += ";" + item;
        //                }
        //            }
        //            log.Debug("AdministradorMdTx.CreateCITx - oDeTx.ListMailsDistribution = " + oDeTx.ListMailsDistribution);
        //        }

        //        using (ISession session = PersistentManager.session())
        //        {
        //            session.Save(oDeTx);
        //            session.Flush();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = -1;
        //        oDeTx = null;
        //        log.Error("AdministradorMdTx.CreateCITx", ex);
        //    }

        //    return ret;
        //}


        //internal static int CreateSignerTxWeb(string citrackid, SignerCreateModel paramIn, BPTxCreateR bPResponse, string trackidbp,
        //                                  out string msgerr)
        //{
        //    int ret = 0;
        //    DeTx oDeTx = null;
        //    msgerr = null;
        //    try
        //    {
        //        oDeTx = new DeTx();
        //        oDeTx.Type = 1;
        //        oDeTx.ActionId = 10; //DefineActionId(paramIn);
        //        oDeTx.CompanyId = paramIn.company;
        //        oDeTx.TrackId = citrackid;
        //        oDeTx.ValidityType = 0;
        //        oDeTx.DateGen = DateTime.Now;
        //        oDeTx.LastModify = DateTime.Now;
        //        oDeTx.QRGenerated = null;
        //        oDeTx.QRValidity = Properties.Settings.Default.QRValidity;
        //        oDeTx.ValidityDays = Properties.Settings.Default.CIValidityDays;
        //        oDeTx.TypeId = paramIn.typeId;
        //        oDeTx.ValueId = paramIn.valueId;
        //        oDeTx.DestinataryMail = paramIn.mailCertify;
        //        oDeTx.TrackIdBP = trackidbp;
        //        oDeTx.UrlBPWeb = bPResponse.verifyUrl;
        //        oDeTx.CellPhone = paramIn.cellNumber;
        //        oDeTx.TaxIdCompany = paramIn.taxidcompany;
        //        if (paramIn.listMailsDestinatary != null)
        //        {
        //            bool first = true;
        //            oDeTx.ListMailsDistribution = "";
        //            foreach (string item in paramIn.listMailsDestinatary)
        //            {
        //                if (first)
        //                {
        //                    oDeTx.ListMailsDistribution += item;
        //                    first = false;
        //                }
        //                else
        //                {
        //                    oDeTx.ListMailsDistribution += ";" + item;
        //                }
        //            }
        //            log.Debug("AdministradorMdTx.CreateSignerTxWeb - oDeTx.ListMailsDistribution = " + oDeTx.ListMailsDistribution);
        //        }

        //        using (ISession session = PersistentManager.session())
        //        {
        //            session.Save(oDeTx);
        //            session.Flush();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = -1;
        //        oDeTx = null;
        //        log.Error("AdministradorMdTx.CreateSignerTxWeb", ex);
        //    }

        //    return ret;
        //}

        //private static int DefineActionId(CICreateModel paramIn)
        //{
        //    return 1;
        //}

        //internal static DeTx GetCITxByTrackId(string trackid, out string msgRet)
        //{
        //    DeTx oDeTx = null;
        //    msgRet = null;
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(trackid))
        //        {
        //            using (ISession session = PersistentManager.session())
        //            {
        //                oDeTx = session.CreateCriteria(typeof(DeTx))
        //                                 .Add(NHibernate.Criterion.Expression.Eq("TrackId", trackid))
        //                                 .UniqueResult<DeTx>();
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        oDeTx = null;
        //        msgRet = ex.Message;
        //        log.Error("AdministradorMdTx.GetCITxByTrackId", ex);
        //    }

        //    return oDeTx;
        //}

        internal static DeTx GetDeTxByTrackIdBP(string trackidbp, out string msgerr)
        {
            DeTx oDeTx = null;
            msgerr = null;
            try
            {
                if (!string.IsNullOrEmpty(trackidbp))
                {
                    using (ISession session = PersistentManager.session())
                    {
                        oDeTx = session.CreateCriteria(typeof(DeTx))
                                         .Add(NHibernate.Criterion.Expression.Eq("Trackidbp", trackidbp))
                                         .UniqueResult<DeTx>();
                    }
                }

            }
            catch (Exception ex)
            {
                oDeTx = null;
                msgerr = ex.Message;
                log.Error("AdministradorDeTx.GetDeTxByTrackIdBP", ex);
            }

            return oDeTx;
        }

        internal static DeTx GetDeTxByTrackIdDE(string trackidde, out string msgerr)
        {
            DeTx oDeTx = null;
            msgerr = null;
            try
            {
                if (!string.IsNullOrEmpty(trackidde))
                {
                    using (ISession session = PersistentManager.session())
                    {
                        //Recupero solo la Tx ade la creacion a que es unica, el resto puede ser muchas (GET por ejemplo)
                        oDeTx = session.CreateCriteria(typeof(DeTx))
                                         .Add(NHibernate.Criterion.Expression.Eq("Trackidde", trackidde))
                                         .Add(NHibernate.Criterion.Expression.Eq("Actionid", 1))
                                         .UniqueResult<DeTx>();
                    }
                }

            }
            catch (Exception ex)
            {
                oDeTx = null;
                msgerr = ex.Message;
                log.Error("AdministradorDeTx.GetDeTxByTrackIdDE", ex);
            }

            return oDeTx;
        }

        public static int CreateDeTx(int reultcode, Electronic.Api.XmlParamIn oXmlIn, DeDocument documento, 
                                     string score, string threshold, string trackidbp, out string msgerr)
        {
            int ret = 0;
            DeTx oDeTx = null;
            msgerr = null;
            try
            {
                oDeTx = new DeTx();
                oDeTx.Actionid = oXmlIn.Actionid;
                oDeTx.Companyid = oXmlIn.Company;
                oDeTx.Dategen = documento.Createdate;
                oDeTx.DeDocument = documento;
                oDeTx.Lastmodify = DateTime.Now;
                oDeTx.Receipt = documento.Receipt;
                oDeTx.Resultcode = reultcode;
                oDeTx.Score = score;
                oDeTx.Threshold = threshold;
                oDeTx.Trackidde = documento.Trackid;
                oDeTx.Trackidbp = trackidbp;
                oDeTx.Typeid = documento.DeTemplate.Id.ToString();
                oDeTx.Username = oXmlIn.User;

                using (ISession session = PersistentManager.session())
                {
                    session.Save(oDeTx);
                    session.Flush();
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                oDeTx = null;
                log.Error("AdministradorDeTx.CreateDeTx", ex);
            }

            return ret;
        }

        public static int CreateDeTx(int reultcode, CreateDeModel param , Company company, DeDocument documento,
                                     out string msgerr)
        {
            int ret = 0;
            DeTx oDeTx = null;
            msgerr = null;
            try
            {
                oDeTx = new DeTx();
                oDeTx.Actionid = 1; //Creacion de DE
                oDeTx.Companyid = company.Id;
                oDeTx.Dategen = documento.Createdate;
                oDeTx.DeDocument = documento;
                oDeTx.Lastmodify = DateTime.Now;
                oDeTx.Receipt = documento.Receipt;
                oDeTx.Resultcode = reultcode;
                oDeTx.Score = null; // score;
                oDeTx.Threshold = null; // threshold;
                oDeTx.Trackidde = documento.Trackid;
                oDeTx.Trackidbp = null; // trackidbp;
                oDeTx.Typeid = documento.DeTemplate.Id.ToString();
                oDeTx.Username = param.username;
                using (ISession session = PersistentManager.session())
                {
                    session.SaveOrUpdate(oDeTx);
                    session.Flush();
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                oDeTx = null;
                log.Error("AdministradorDeTx.CreateDeTx", ex);
            }

            return ret;
        }

        //internal static void UpdateCITxSigner(DeTx tx, XmlParamIn oXmlIn, RdRecibos objRD, string trackIdBP, string trackIDE,
        //                                      string certifyPDF, int resulteCode, double score, double threshold,
        //                                      string name, string phaterlastname, string motherlastname,
        //                                      string securitycode, out string msg)
        //{
        //    DeTx oDeTx = null;
        //    msg = null;
        //    try
        //    {
        //        log.Debug("AdministradorMdTx.UpdateCITx IN...");
        //        using (ISession session = PersistentManager.session())
        //        {
        //            log.Debug("AdministradorMdTx.UpdateCITx - Recupero txid=" + oXmlIn.CIParam.TrackId);
        //            oDeTx = PersistentManager.session().CreateCriteria(typeof(DeTx))
        //                             .Add(NHibernate.Criterion.Expression.Eq("TrackId", oXmlIn.CIParam.TrackId))
        //                             .UniqueResult<DeTx>();
        //        }
        //        if (oDeTx != null)
        //        {
        //            log.Debug("AdministradorMdTx.UpdateCITx - Comienza a actualizar...");
        //            oDeTx.ActionId = oXmlIn.Actionid;
        //            oDeTx.CertifyPDF = certifyPDF;
        //            log.Debug("AdministradorMdTx.UpdateCITx - CertifyPDF len = " +
        //                      (string.IsNullOrEmpty(oDeTx.CertifyPDF) ? "null" : oDeTx.CertifyPDF.Length.ToString()));
        //            oDeTx.TrackIdBP = trackIdBP;
        //            log.Debug("AdministradorMdTx.UpdateCITx - trackIdBP len = " +
        //                      (string.IsNullOrEmpty(oDeTx.TrackIdBP) ? "null" : oDeTx.TrackIdBP.Length.ToString()));
        //            oDeTx.TrackIdDE = trackIDE;
        //            log.Debug("AdministradorMdTx.UpdateCITx - TrackIdDE len = " +
        //                      (string.IsNullOrEmpty(oDeTx.TrackIdDE) ? "null" : oDeTx.TrackIdDE.Length.ToString()));
        //            oDeTx.ResultCode = resulteCode;
        //            log.Debug("AdministradorMdTx.UpdateCITx - ResultCode = " + oDeTx.ResultCode.ToString());
        //            oDeTx.Score = score.ToString();
        //            log.Debug("AdministradorMdTx.UpdateCITx - Score len = " +
        //                      (string.IsNullOrEmpty(oDeTx.Score) ? "null" : oDeTx.Score.Length.ToString()));
        //            oDeTx.Threshold = threshold.ToString();
        //            log.Debug("AdministradorMdTx.UpdateCITx - Threshold len = " +
        //                      (string.IsNullOrEmpty(oDeTx.Threshold) ? "null" : oDeTx.Threshold.Length.ToString()));
        //            oDeTx.LastModify = DateTime.Now;
        //            oDeTx.ReceiptId = objRD.Id;
        //            oDeTx.Name = string.IsNullOrEmpty(name) ? oDeTx.Name : name;
        //            log.Debug("AdministradorMdTx.UpdateCITx - Name len = " +
        //                      (string.IsNullOrEmpty(oDeTx.Name) ? "null" : oDeTx.Name.Length.ToString()));
        //            oDeTx.PhaterLastName = string.IsNullOrEmpty(phaterlastname) ? oDeTx.PhaterLastName : phaterlastname;
        //            log.Debug("AdministradorMdTx.UpdateCITx - PhaterLastName len = " +
        //                      (string.IsNullOrEmpty(oDeTx.PhaterLastName) ? "null" : oDeTx.PhaterLastName.Length.ToString()));
        //            oDeTx.MotherLastName = string.IsNullOrEmpty(motherlastname) ? oDeTx.MotherLastName : motherlastname;
        //            log.Debug("AdministradorMdTx.UpdateCITx - MotherLastName len = " +
        //                      (string.IsNullOrEmpty(oDeTx.MotherLastName) ? "null" : oDeTx.MotherLastName.Length.ToString()));

        //            oDeTx.IdCardImageFront = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageFront) ? oXmlIn.CIParam.IDCardImageFront : oDeTx.IdCardImageFront;
        //            log.Debug("AdministradorMdTx.UpdateCITx - IdCardImageFront len = " +
        //                      (string.IsNullOrEmpty(oDeTx.IdCardImageFront) ? "null" : oDeTx.IdCardImageFront.Length.ToString()));
        //            oDeTx.IdCardImageBack = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? oXmlIn.CIParam.IDCardImageBack : oDeTx.IdCardImageBack;
        //            log.Debug("AdministradorMdTx.UpdateCITx - IdCardImageBack len = " +
        //                      (string.IsNullOrEmpty(oDeTx.IdCardImageBack) ? "null" : oDeTx.IdCardImageBack.Length.ToString()));
        //            oDeTx.IdCardPhotoImage = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardPhotoImage) ? oXmlIn.CIParam.IDCardPhotoImage : oDeTx.IdCardPhotoImage;
        //            log.Debug("AdministradorMdTx.UpdateCITx - IdCardPhotoImage len = " +
        //                      (string.IsNullOrEmpty(oDeTx.IdCardPhotoImage) ? "null" : oDeTx.IdCardPhotoImage.Length.ToString()));
        //            oDeTx.IdCardSignatureImage = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardSignatureImage) ? oXmlIn.CIParam.IDCardSignatureImage : oDeTx.IdCardSignatureImage;
        //            log.Debug("AdministradorMdTx.UpdateCITx - IdCardSignatureImage len = " +
        //                      (string.IsNullOrEmpty(oDeTx.IdCardSignatureImage) ? "null" : oDeTx.IdCardSignatureImage.Length.ToString()));
        //            oDeTx.Selfie = !string.IsNullOrEmpty(oXmlIn.CIParam.Selfie) ? oXmlIn.CIParam.Selfie : oDeTx.Selfie;
        //            log.Debug("AdministradorMdTx.UpdateCITx - Selfie len = " +
        //                      (string.IsNullOrEmpty(oDeTx.Selfie) ? "null" : oDeTx.Selfie.Length.ToString()));
        //            oDeTx.IssueDate = !string.IsNullOrEmpty(oXmlIn.CIParam.IssueDate) ? oXmlIn.CIParam.IssueDate : oDeTx.IssueDate;
        //            oDeTx.ExprationDate = !string.IsNullOrEmpty(oXmlIn.CIParam.ExpirationDate) ? oXmlIn.CIParam.ExpirationDate : oDeTx.ExprationDate;
        //            oDeTx.BirthDate = !string.IsNullOrEmpty(oXmlIn.CIParam.BirthDate) ? oXmlIn.CIParam.BirthDate : oDeTx.BirthDate;
        //            oDeTx.GeoRef = !string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef) ? oXmlIn.CIParam.GeoRef : oDeTx.GeoRef;
        //            log.Debug("AdministradorMdTx.UpdateCITx - GeoRef len = " +
        //                      (string.IsNullOrEmpty(oDeTx.GeoRef) ? "null" : oDeTx.GeoRef.Length.ToString()));
        //            oDeTx.WorkstationId = !string.IsNullOrEmpty(oXmlIn.CIParam.WorkStationID) ? oXmlIn.CIParam.WorkStationID : oDeTx.WorkstationId;
        //            log.Debug("AdministradorMdTx.UpdateCITx - WorkstationId len = " +
        //                      (string.IsNullOrEmpty(oDeTx.WorkstationId) ? "null" : oDeTx.WorkstationId.Length.ToString()));
        //            oDeTx.Serial = !string.IsNullOrEmpty(oXmlIn.CIParam.Serial) ? oXmlIn.CIParam.Serial : oDeTx.Serial;
        //            oDeTx.Sex = !string.IsNullOrEmpty(oXmlIn.CIParam.Sex) ? oXmlIn.CIParam.Sex : oDeTx.Sex;
        //            oDeTx.Nacionality = !string.IsNullOrEmpty(oXmlIn.CIParam.Nacionality) ? oXmlIn.CIParam.Nacionality : oDeTx.Nacionality;

        //            oDeTx.Map = string.IsNullOrEmpty(oDeTx.Map) ? Utils.GetMapFromGoogleMap(oDeTx.GeoRef) : oDeTx.Map;
        //            log.Debug("AdministradorMdTx.UpdateCITx - Map len = " +
        //                      (string.IsNullOrEmpty(oDeTx.Map) ? "null" : oDeTx.Map.Length.ToString()));
        //            oDeTx.ManualSignatureImage = !string.IsNullOrEmpty(oXmlIn.CIParam.ManualSignatureImage) ? oXmlIn.CIParam.ManualSignatureImage : oDeTx.ManualSignatureImage;
        //            log.Debug("AdministradorMdTx.UpdateCITx - ManualSignatureImage len = " +
        //                      (string.IsNullOrEmpty(oDeTx.ManualSignatureImage) ? "null" : oDeTx.ManualSignatureImage.Length.ToString()));
        //            oDeTx.UrlBPWeb = string.IsNullOrEmpty(oDeTx.UrlBPWeb) ? null : oDeTx.UrlBPWeb;
        //            log.Debug("AdministradorMdTx.UpdateCITx - UrlBPWeb len = " +
        //                      (string.IsNullOrEmpty(oDeTx.UrlBPWeb) ? "null" : oDeTx.UrlBPWeb.Length.ToString()));
        //            oDeTx.CellPhone = string.IsNullOrEmpty(oDeTx.CellPhone) ? null : oDeTx.CellPhone;
        //            log.Debug("AdministradorMdTx.UpdateCITx - CellPhone = " +
        //                      (string.IsNullOrEmpty(oDeTx.CellPhone) ? "null" : oDeTx.CellPhone));

        //            oDeTx.CarRegisterImageFront = tx.CarRegisterImageFront;
        //            log.Debug("AdministradorMdTx.UpdateCITx - CarRegisterImageFront len = " +
        //                     (string.IsNullOrEmpty(oDeTx.CarRegisterImageFront) ? "null" : oDeTx.CarRegisterImageFront.Length.ToString()));
        //            oDeTx.CarRegisterImageBack = tx.CarRegisterImageBack;
        //            log.Debug("AdministradorMdTx.UpdateCITx - CarRegisterImageBack len = " +
        //                     (string.IsNullOrEmpty(oDeTx.CarRegisterImageBack) ? "null" : oDeTx.CarRegisterImageBack.Length.ToString()));
        //            oDeTx.WritingImage = tx.WritingImage;
        //            log.Debug("AdministradorMdTx.UpdateCITx - WritingImage len = " +
        //                     (string.IsNullOrEmpty(oDeTx.WritingImage) ? "null" : oDeTx.WritingImage.Length.ToString()));
        //            oDeTx.Form = tx.Form;
        //            log.Debug("AdministradorMdTx.UpdateCITx - Form len = " +
        //                     (string.IsNullOrEmpty(oDeTx.Form) ? "null" : oDeTx.Form.Length.ToString()));

        //            oDeTx.SecurityCode = securitycode;
        //            log.Debug("AdministradorMdTx.UpdateCITx - SecurityCode = " +
        //                      (string.IsNullOrEmpty(oDeTx.SecurityCode) ? "null" : oDeTx.SecurityCode));

        //            log.Debug("AdministradorMdTx.UpdateCITx - Save in...");
        //            using (ISession session = PersistentManager.session())
        //            {
        //                session.Update(oDeTx);
        //                session.Flush();
        //            }
        //            log.Debug("AdministradorMdTx.UpdateCITx - Save out!");
        //        }
        //        log.Debug("AdministradorMdTx.UpdateCITx OUT!");
        //    }
        //    catch (Exception ex)
        //    {
        //        oDeTx = null;
        //        msg = ex.Message;
        //        log.Error("AdministradorMdTx.UpdateCITx", ex);
        //    }
        //}

        //internal static int CreateNotarizeTxWeb(string trackId, v2NotarizeInitModel paramIn, BPTxCreateR bPResponse, string trackidbp, out string msgerr)
        //{
        //    int ret = 0;
        //    DeTx oDeTx = null;
        //    msgerr = null;
        //    try
        //    {
        //        log.Debug("AdministradorMdTx.CreateNotarizeTxWeb IN...");
        //        oDeTx = new DeTx();
        //        oDeTx.Type = 1;
        //        oDeTx.ActionId = 10; //DefineActionId(paramIn);
        //        oDeTx.CompanyId = Properties.Settings.Default.NVCompanyCI;
        //        oDeTx.TrackId = trackId;
        //        oDeTx.ValidityType = 0;
        //        oDeTx.DateGen = DateTime.Now;
        //        oDeTx.LastModify = DateTime.Now;
        //        oDeTx.QRGenerated = null;
        //        oDeTx.QRValidity = Properties.Settings.Default.QRValidity;
        //        oDeTx.ValidityDays = Properties.Settings.Default.CIValidityDays;
        //        oDeTx.TypeId = "RUT"; //paramIn.typeId;
        //        oDeTx.ValueId = paramIn.rut;
        //        oDeTx.DestinataryMail = paramIn.mail;
        //        oDeTx.TrackIdBP = trackidbp;
        //        oDeTx.UrlBPWeb = bPResponse.verifyUrl;
        //        oDeTx.CellPhone = null;  //paramIn.cellNumber;
        //        oDeTx.TaxIdCompany = null;  //paramIn.taxidcompany;
        //                                     //if (paramIn.listMailsDestinatary != null)
        //                                     //{
        //                                     //    bool first = true;
        //                                     //    oDeTx.ListMailsDistribution = "";
        //                                     //    foreach (string item in paramIn.listMailsDestinatary)
        //                                     //    {
        //                                     //        if (first)
        //                                     //        {
        //                                     //            oDeTx.ListMailsDistribution += item;
        //                                     //            first = false;
        //                                     //        }
        //                                     //        else
        //                                     //        {
        //                                     //            oDeTx.ListMailsDistribution += ";" + item;
        //                                     //        }
        //                                     //    }
        //                                     //    log.Debug("AdministradorMdTx.CreateNotarizeTxWeb - oDeTx.ListMailsDistribution = " + oDeTx.ListMailsDistribution);
        //                                     //}

        //        using (ISession session = PersistentManager.session())
        //        {
        //            session.Save(oDeTx);
        //            session.Flush();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = -1;
        //        oDeTx = null;
        //        log.Error("AdministradorMdTx.CreateNotarizeTxWeb", ex);
        //    }
        //    log.Debug("AdministradorMdTx.CreateNotarizeTxWeb OUT!");
        //    return ret;
        //}
    }
}