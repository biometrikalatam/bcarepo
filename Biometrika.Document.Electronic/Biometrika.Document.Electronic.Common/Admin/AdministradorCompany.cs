using System;
using System.Collections.Generic;
using Biometrika.Document.Electronic.Database;
using log4net;
using NHibernate;
using NHibernate.Criterion;
//using NHibernate.Expression;

namespace Biometrika.Document.Electronic.Database
{

    /// <summary>
    /// 
    /// </summary>
    public class AdministradorCompany
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(AdministradorCompany));

        public static Company RetrieveAuthCredential(string accessname, out string msgErr)
        {
            msgErr = "S/C";
            Company company;
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    company = PersistentManager.session().CreateCriteria(typeof(Company))
                                                  .Add(Expression.Eq("Accessname", accessname))
                                                  .UniqueResult<Company>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorCompany.RetrieveAuthCredential (accessname)", ex);
                msgErr = ex.Message;
                company = null;
            }
            return company;
        }

        public static Company BuscarCompanyByRut(string rut)
        {
            Company rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = PersistentManager.session().CreateCriteria(typeof(Company))
                                     .Add(NHibernate.Criterion.Expression.Eq("Rut", rut))
                                     .UniqueResult<Company>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorCompany.BuscarCompanyByRut", ex);
            }
            
            return rec;
        }

        public static Company BuscarCompanyById(int id)
        {
            Company rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = PersistentManager.session().CreateCriteria(typeof(Company))
                                     .Add(NHibernate.Criterion.Expression.Eq("Id", id))
                                     .UniqueResult<Company>();
                }
                if (rec == null)
                {
                    log.Warn("AdministradorCompany.BuscarCompanyById => id = " + id.ToString() + " NO Encontrada!");
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorCompany.BuscarCompanyById", ex);
            }

            return rec;
        }

        public static IList<Company> ListCompanies(bool withChilds = false)
        {
            IList<Company> rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = PersistentManager.session()
                            .CreateCriteria(typeof(Company))
                                .List<Company>();
                    if (rec != null && withChilds)
                    {
                        foreach (Company item in rec)
                        {
                            NHibernateUtil.Initialize(item.DeTemplate);
                            foreach (DeTemplate item1 in item.DeTemplate)
                            {
                                NHibernateUtil.Initialize(item1.DeTemplateTag);
                                NHibernateUtil.Initialize(item1.DeTemplateSignatory);
                            }

                        }
                    }
                }
                if (rec == null)
                {
                    log.Warn("AdministradorCompany.ListCompanies - No Existen copa�ias habilitadas!!");
                } 
            }
            catch (Exception ex)
            {
                log.Error("AdministradorCompany.ListCompanies", ex);
            }

            return rec;
        }
    }
}
