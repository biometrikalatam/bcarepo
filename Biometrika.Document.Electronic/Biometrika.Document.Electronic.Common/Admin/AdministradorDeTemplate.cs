﻿using log4net;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Document.Electronic.Database
{
    public class AdministradorDeTemplate
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AdministradorDeTemplate));

        public static DeTemplate SearchDeTemplateById(int id, bool withChild = false)
        {
            DeTemplate rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = PersistentManager.session().CreateCriteria(typeof(DeTemplate))
                                     .Add(NHibernate.Criterion.Expression.Eq("Id", id))
                                     .UniqueResult<DeTemplate>();

                    if (withChild)
                    {
                        NHibernateUtil.Initialize(rec.DeTemplateTag);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorDeTemplate.SearchDeTemplateById", ex);
            }

            return rec;
        }

        public static IList<DeTemplate> AllDeTemplate()
        {
            IList<DeTemplate> ret = null;

            try
            {
                log.Debug("AdministradorDeTemplate.AllDeTemplate IN...");
                using (ISession session = PersistentManager.session())
                {
                    ret = PersistentManager.session().CreateCriteria(typeof(DeTemplate))
                                     .List<DeTemplate>();
                }
            }
            catch (Exception ex)
            {
                ret = null;
                log.Error("AdministradorDeTemplate.AllDeTemplate", ex);
            }
            log.Debug("AdministradorDeTemplate.AllDeTemplate OUT! ret => " + (ret==null?"NULL":ret.Count.ToString()));
            return ret;
        }

    }
}
