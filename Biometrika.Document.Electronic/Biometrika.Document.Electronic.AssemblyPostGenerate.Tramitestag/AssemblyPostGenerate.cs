﻿using Bio.Core.Notify;
using Biometrika.Document.Electronic.Api;
using Biometrika.Document.Electronic.Api.Interfaces;
using Biometrika.Document.Electronic.Database;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Document.Electronic.AssemblyPostGenerate.Tramitestag
{
   
    public class AssemblyPostGenerate : IAssemblyPostGenerate
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(AssemblyPostGenerate));

        private Dictionary<string, string> _Config; 
        public Dictionary<string, string> Config {
            get { return _Config; }
            set { _Config = value; }
        }

        private string _PathBase;
        public string PathBase {
            get { return _PathBase; }
            set { _PathBase = value;  }
        }

        /// <summary>
        /// Lee el archivo de config llamado: BDE.AssemblyPostGenerate.Tramitestag.cfg
        /// </summary>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        public int Initialize(string pathbase, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            try
            {
                LOG.Debug("AssemblyPostGenerate.Tramitestag.Initialize IN => pathbase = " + 
                            (string.IsNullOrEmpty(pathbase)?"Null":pathbase));
                if (string.IsNullOrEmpty(pathbase)) _PathBase = "";
                else _PathBase = pathbase;
                if (System.IO.File.Exists(_PathBase + "BDE.AssemblyPostGenerate.Tramitestag.cfg"))
                {
                    Config = JsonConvert.DeserializeObject<Dictionary<string, string>>
                                (System.IO.File.ReadAllText(_PathBase + "BDE.AssemblyPostGenerate.Tramitestag.cfg"));
                } else
                {
                    Config = new Dictionary<string, string>();
                    Config.Add("DestinatariesList", "info@biometrika.cl,info@gmail.com");
                    Config.Add("SmtpServer", "smtp.gmail.com");
                    Config.Add("SmtpPort", "587");
                    Config.Add("SmtpUser", "soporte@biometrika.cl");
                    Config.Add("SmtpClave", "password");
                    Config.Add("SmtpDisplayName", "Notify Biometrika DE");
                    Config.Add("PathLogoHeader", "logoheader.jpg");
                    Config.Add("PathLogoFooter", "logofotter.jpg");
                    Config.Add("MailConfig", "mailConfig.json");
                    Config.Add("UrlView", "http://de.biometrikalatam.com/viewdetail/");
                    System.IO.File.WriteAllText(_PathBase + "BDE.AssemblyPostGenerate.Tramitestag.cfg",
                                                JsonConvert.SerializeObject(Config));
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "AssemblyPostGenerate.Tramitestag.Initialize Excp Error [" + ex.Message + "]";
                LOG.Error("AssemblyPostGenerate.Tramitestag.Initialize Excp Error [" + ex.Message + "]");
            }
            LOG.Debug("AssemblyPostGenerate.Tramitestag.Initialize OUT!");
            return ret;
        }

        /// <summary>
        /// Recibe el DeDocument generado, y envía mail interno a la lista configurada.
        /// </summary>
        /// <param name="paramin"></param>
        /// <param name="msgerr"></param>
        /// <param name="paramout"></param>
        /// <returns></returns>
        public int DoAction(object paramin, out string msgerr, out object paramout)
        {
            int ret = 0;
            msgerr = null;
            paramout = paramin;
            try
            {
                /*
                    1.- Casteo paramin, que deberia ser un DeDocument
                    2.- Envío mail específico a lista configurada en config
                    3.- Retorno codigo de error
                    4.- Retorno DeDocument sin modificaciones
                */
                LOG.Debug("AssemblyPostGenerate.Tramitestag.DoAction IN...");
                //1.- Casteo paramin, que deberia ser un DeDocument
                DeDocument _Doc = (DeDocument)paramin;
                LOG.Debug("AssemblyPostGenerate.Tramitestag.DoAction - Casteo hehoc con éxito. Recibido DeDoc => Id: " +
                            _Doc.Id.ToString() + "|Trackid:" + _Doc.Trackid);

                //2.- Envío mail específico a lista configurada en config
                string[] listDestinataries = Config["DestinatariesList"].Split(',');
                byte[] docpdf = Convert.FromBase64String(_Doc.Bytedocument);

                //Armo cuerpo
                LOG.Debug("AssemblyPostGenerate.Tramitestag.DoAction - Arma cuerpo...");
                Dictionary<string, string> dict =
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(
                        System.IO.File.ReadAllText(Config["MailConfig"]));
                string[] bodyLines = new string[4];
                bodyLines[0] = "Se completó el proceso de la generación del documento, con el firmante <b>" +
                                _Doc.DeSignatory[0].Typeid + " " + _Doc.DeSignatory[0].Valueid + "</b>";
                bodyLines[1] = "El codigo unico del tramite para consultas es: <b>" + _Doc.Trackid + "</b>";
                bodyLines[2] = "Verificación " +
                                (_Doc.DeSignatory[0].Verifyresult.Equals("POSITIVO") ?
                                "<b><font style=\"color:green; font-family:'Arial'\">POSITIVA</font></b>" :
                                "<b><font style=\"color:red; font-family:'Arial'\">NEGATIVA</font></b>") +
                                " [Score=" + Convert.ToDouble(_Doc.DeSignatory[0].Score).ToString("##.##") + "%]";
                bodyLines[3] = "Visualice los detalles de la transacción presionando  <a href=\"" +
                            Config["UrlView"] + "?trackid=" + _Doc.Trackid +
                            "\" target =\"_blank\"><font style=\"color:ligth-blue; font-family:'Arial'\">aqui</font></a>.";
                //Armo HTML COmpleto
                LOG.Debug("AssemblyPostGenerate.Tramitestag.DoAction - Arma HTML...");
                string strbody = "<table border=\"0\" width=\"95%\" align=\"center\" bgcolor=\"" + dict["mailColorBackgroungBody"] + "\"" + ">" +
                                         "<tr><td align=\"center\">" +
                                         "</td></tr>";
                for (int i = 0; i < bodyLines.Length; i++)
                {
                    strbody += "<tr><td><p style=\"font-family:'Arial'\">" + bodyLines[i] + "</p></td></tr>";
                }
                strbody += "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
                                "<a href=\"http://www.biometrikalatam.com\" target=\"_blank\">Biometrika S.A.<a></p></td></tr>" +
                                "<tr height=\"5\" bgcolor=\"" + dict["mailColorSeparatorLine"] + "\"><td></td></tr>" +
                             "</table>";  //5882ba

                string body2 = @"<table border=""0"" width=""95%"" align=""center"">" +
                               @"<tr><td align=""" + dict["mailAlignImageHeader"] + "\"><a href=\"" + dict["mailLinkImageHeader"] + "\" target=\"_blank\">" +
                               @"<img src=""cid:HDIImageH"" width=""" + dict["mailPercentImageHeader"] + "%\" /></a></td></tr></table>" + strbody +
                               @"<table border=""0"" width=""95%"" align=""center"">" +
                               @"<tr><td align=""center""><a href=""http://www.biometrikalatam.com"" target=""_blank"">" +
                               @"<img src=""cid:HDIImageF""  width=""100%"" /></a></td></tr></table>";
                body2 = @"<table border=""0"" width=""100%"" align=""center"" bgcolor=""" + dict["mailColorBackgroundPage"] + "\"><tr><td align=\"center\">" +
                        body2 +
                        "<tr><td></table>";

                //Envio mail a backend de TTag, con link a detalle si necesita.
                string logoHeader = Convert.ToBase64String(System.IO.File.ReadAllBytes(Config["PathLogoHeader"]));
                string logoFooter = Convert.ToBase64String(System.IO.File.ReadAllBytes(Config["PathLogoFooter"]));
                LOG.Debug("AssemblyPostGenerate.Tramitestag.DoAction - Leyo logos...");
                string datatocontat = GetPatenteFromJson(_Doc);
                LOG.Debug("AssemblyPostGenerate.Tramitestag.DoAction - Envia mails [Q=" + 
                                listDestinataries.Length.ToString() + "]...");
                bool bret = NotifyByMail.SendMailHtml(Config["SmtpServer"], Convert.ToInt32(Config["SmtpPort"]), 
                                                      Config["SmtpUser"], Config["SmtpClave"], 
                                                      Config["SmtpDisplayName"], listDestinataries, "Documento Generado [RUT=" 
                                                      + _Doc.DeSignatory[0].Valueid + 
                                                      (string.IsNullOrEmpty(datatocontat) ? "" : "|" + datatocontat) + "]",
                                                      body2, docpdf, "pdf", _Doc.Trackid + ".pdf", logoHeader, logoFooter);

                if (bret)
                {
                    LOG.Debug("SignerController.SendMsgToDestinataries - Enviado PDF de trackid = " +
                        _Doc.Trackid + "/rut = " + _Doc.DeSignatory[0].Valueid + " => " + (bret.ToString()));
                    ret = 0;
                } else
                {
                    LOG.Debug("SignerController.SendMsgToDestinataries - NO Enviado PDF de trackid = " +
                        _Doc.Trackid + "/rut = " + _Doc.DeSignatory[0].Valueid + " => " + (bret.ToString()));
                    ret = Errors.RET_ERR_CALLING_ASSEMBLY_POST_GENERATE;
                    msgerr = "NO Enviado PDF de trackid = " + _Doc.Trackid + "/rut = " + _Doc.DeSignatory[0].Valueid;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "AssemblyPostGenerate.Tramitestag Excp Error [" + ex.Message + "]";
                LOG.Error("AssemblyPostGenerate.Tramitestag Excp Error: " + ex.Message);
            }
            LOG.Debug("AssemblyPostGenerate.Tramitestag IN...");
            return ret;
        }

        private static string GetPatenteFromJson(DeDocument _Doc)
        {
            string sRet = null;
            string patente = null;
            string mailfirmante = null;
            mailfirmante = null;
            bool hayPatente = false;
            bool hayMailfirmante = false;

            try
            {
                //LOG.Debug("SignerController.GetPatenteFromJson IN...");
                //if (string.IsNullOrEmpty(tx.Form))
                //{
                //    LOG.Debug("SignerController.GetPatenteFromJson - Sale porque entrada => jsonform = null");
                //    return null;
                //}

                //LOG.Debug("SignerController.GetPatenteFromJson - Deserialiso jsonform...");
                //Dictionary<string, string> _FORM = JsonConvert.DeserializeObject<Dictionary<string, string>>(tx.Form);

                //LOG.Debug("SignerController.GetPatenteFromJson - _FORM != Null => " + (_FORM != null).ToString());

                //if (!string.IsNullOrEmpty(_FORM["patentevehiculo"]))
                //{
                //    patente = "Patente=" + _FORM["patentevehiculo"];
                //    LOG.Debug("SignerController.GetPatenteFromJson - Set => " + patente);
                //    hayPatente = true;
                //}
                //if (!string.IsNullOrEmpty(_FORM["mailfirmante"]))
                //{
                //    mailfirmante = "eMail=" + _FORM["mailfirmante"];
                //    LOG.Debug("SignerController.GetPatenteFromJson - Set => " + mailfirmante);
                //    hayMailfirmante = true;
                //}

                //if (hayPatente && hayMailfirmante)
                //{
                //    sRet = patente + "|" + mailfirmante;
                //}
                //else if (hayPatente && !hayMailfirmante)
                //{
                //    sRet = patente;
                //}
                //else if (!hayPatente && hayMailfirmante)
                //{
                //    sRet = mailfirmante;
                //}

                //sRet = sRet + "|" + (tx.ResultCode == 1 ? "POSITIVO=" : "NEGATIVO=") +
                //                     Convert.ToDouble(tx.Score).ToString("##.##") + "%";

                //foreach (KeyValuePair<string, string> item in _FORM) 
                //{
                //    if (item.Key.Equals(_key))
                //    {
                //        keytoreturn = item.Value;
                //        break;
                //    }
                //}
            }
            catch (Exception ex)
            {
                LOG.Error("SignerController.GetPatenteFromJson Error: " + ex.Message);
            }
            LOG.Debug("SignerController.GetPatenteFromJson OUT! - Retorno = " + (string.IsNullOrEmpty(sRet) ? "Null" : sRet));
            return sRet;
        }

    }
}
