﻿using Bio.DocumentElectronic.Web.API.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Bio.DocumentElectronic.Web.API.Controllers
{
    public class BaseApiController : ApiController
    {
        protected WsDeHelper WsDeHelper => new WsDeHelper();
        protected XmlHelper XmlHelper => new XmlHelper();
    }
}
