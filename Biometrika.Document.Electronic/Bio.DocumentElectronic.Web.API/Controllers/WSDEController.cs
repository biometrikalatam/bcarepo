﻿using Bio.Document.Manager.Api;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace Bio.DocumentElectronic.Web.API.Controllers
{
    public class WSDEController : BaseApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [Route("api/WSDE/Process")]
        [HttpPost]
        public XmlParamOut Process([FromBody] XmlParamIn xmlParamIn)
        {
            Log.Info("WSDEController.Process");
            string sParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);

            if (string.IsNullOrEmpty(sParamIn))
                return new XmlParamOut {
                    Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
                };

            string sParamOut;
            int ret = WsDeHelper.Process(sParamIn, out sParamOut);

            Log.Info("Codigo respuesta BioPortal: " + ret + " - Mensaje: " + Errors.GetDescription(ret));

            XmlParamOut response = XmlHelper.DeserializeXmlParamOut(sParamOut);
            if (ret != 0)
            {
                response.ExecutionResult = ret;
                response.Message += ", " + Errors.GetDescription(ret);
            }

            return response;
        }
    }
}
