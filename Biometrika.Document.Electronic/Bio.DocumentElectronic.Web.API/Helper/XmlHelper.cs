﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Bio.Document.Manager.Api;

namespace Bio.DocumentElectronic.Web.API.Helper
{
    public class XmlHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        internal string SerializeXmlParamIn(XmlParamIn xmlParamIn)
        {
            Log.Debug("Serializando xmlParamIn...");

            try
            {
                return XmlUtils.SerializeObject(xmlParamIn);
            }
            catch (Exception e)
            {
                Log.Error("Error serializando XmlParamIn", e);
                return null;
            }
        }

        internal XmlParamOut DeserializeXmlParamOut(string xmlParamOut)
        {
            Log.Debug("Deserializando xmlParamOut...");

            try
            {
                return XmlUtils.DeserializeObject<XmlParamOut>(xmlParamOut);
            }
            catch (Exception ex)
            {
                Log.Error("Error deserializando XmlParamOut", ex);
                return new XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_DESERIALIZE_XMLPARAMOUT)
                };
            }
        }
    }
}