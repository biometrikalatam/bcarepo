﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Configuration;

namespace Bio.DocumentElectronic.Web.API.Helper
{
    public class WsDeHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        internal int Process(string sParamIn, out string sParamOut)
        {
            Log.Info("WsDeHelper.Process");
            int _ret_actual = 0;
            sParamOut = null;
            try
            {
                Log.Debug("Llamando a DocumentoElectronico.WSDE.Process");
                int timeOut = 0;
                if (!Int32.TryParse(WebConfigurationManager.AppSettings["WS_TimeOut"], out timeOut))
                    timeOut = 30000;
                Log.Debug("Timeout configurado: " + timeOut);

                using (DocumentoElectronicoWS.WSDE target = new DocumentoElectronicoWS.WSDE())
                {
                    target.Url = Properties.Settings.Default.Bio_DocumentElectronic_Web_API_DocumentoElectronicoWS_WSDE;
                    target.Timeout = timeOut;
                    _ret_actual = target.Process(sParamIn, out sParamOut);
                }
                Log.Info("Respuesta DocumentoElectronico.WSDE.Process: " + _ret_actual);
            }
            catch (Exception e)
            {
                _ret_actual = -1;
                Log.Error("Error WsDeHelper.Process", e);
            }
            return _ret_actual;
        }
    }
}