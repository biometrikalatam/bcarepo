using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;



public partial class MDMasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
            if(Session["menu"]!=null)
                CrearMenu();
    }
    
    public void CrearMenuUsuario(MasterPage Master, HttpRequest request)  //},MINVU.Menu[] menues)
    {
        TreeNode node = null;
        //for (int n = 0; n < menues.Length; n++)
        //{
        //    node = new TreeNode(menues[n]._Descripcion
        //                      , menues[n]._Id,
        //                      "~/images/" + menues[n]._Descripcion.Replace(" ", "_") + ".png",
        //                      "~/pages/" + menues[n]._Url, "");
        //    ((TreeView)Master.FindControl("TreeViewMenu")).Nodes.Add(node);
        //}
        CrearMenu();
    }

    ///
    private void FillTree(XmlNode node, TreeNodeCollection parentnode)
    {
        // End recursion if the node is a text type
        if (node == null || node.NodeType == XmlNodeType.Text || node.NodeType == XmlNodeType.CDATA)
            return;

        TreeNodeCollection tmptreenodecollection = AddNodeToTree(node, parentnode);


        // Add all the children of the current node to the treeview
        foreach (XmlNode tmpchildnode in node.ChildNodes)
        {
            FillTree(tmpchildnode, tmptreenodecollection);
        }

    }
    private  TreeNodeCollection AddNodeToTree(XmlNode node, TreeNodeCollection parentnode)
    {

        TreeNode newchildnode = CreateTreeNodeFromXmlNode(node);

        // if nothing to add, return the parent item
        if (newchildnode == null) return parentnode;

        // add the newly created tree node to its parent
        if (parentnode != null) parentnode.Add(newchildnode);

        return newchildnode.ChildNodes;

    }


    private TreeNode CreateTreeNodeFromXmlNode(XmlNode node)
    {
        TreeNode tmptreenode = new TreeNode();

        if ((node.HasChildNodes) && (node.FirstChild.Value != null))
        {
            tmptreenode = new TreeNode(node.Name);
            TreeNode tmptreenode2 = new TreeNode(node.FirstChild.Value);
            tmptreenode.ChildNodes.Add(tmptreenode2);
        }
        else if (node.NodeType != XmlNodeType.CDATA)
        {
            if (node.Attributes.Count > 0)
            {
                tmptreenode = new TreeNode(node.Attributes["descripcion"].Value);
                tmptreenode.NavigateUrl = node.Attributes["url"].Value.Replace("..", "~");
                tmptreenode.ImageUrl = "~/images/" + node.Attributes["descripcion"].Value + ".png";
            }
            else
            {
                if ((node.Name == "menus") || (node.Name == "ICE") || (node.Name == "RESPUESTA") || (node.Name == "sistema"))
                {
                    tmptreenode = null;
                }
                else
                {
                    tmptreenode = new TreeNode(node.Name);
                }
            }
        }

        return tmptreenode;
    }



    ///
    private void CrearMenu()
    {
        XmlDocument tmpxmldoc = new XmlDocument();
        tmpxmldoc.LoadXml(Session["menu"].ToString());
        FillTree(tmpxmldoc.DocumentElement, ((TreeView)this.FindControl("TreeViewMenu")).Nodes);       

        TreeNode BandejaEntrada, BandejaSalida,Preingreso,Ingreso, Mantenedor, Metadato;

        BandejaEntrada = new TreeNode();
        BandejaEntrada.Text = "Bandeja de Entrada";
        BandejaEntrada.NavigateUrl = "~/pages/Bandeja.aspx?ver=0";
        BandejaEntrada.ToolTip = "Accede a la Bandeja de Entrada";
        BandejaEntrada.ImageUrl = "~/images/Inbox.gif";
        ((TreeView)this.FindControl("TreeViewMenu")).Nodes.Add(BandejaEntrada);

        BandejaSalida = new TreeNode();
        BandejaSalida.Text = "Bandeja de Salida";
        BandejaSalida.NavigateUrl = "~/pages/Bandeja.aspx?ver=1";
        BandejaSalida.ToolTip = "Accede a la Bandeja de Salida";
        BandejaSalida.ImageUrl = "~/images/Outbox.gif";
        ((TreeView)this.FindControl("TreeViewMenu")).Nodes.Add(BandejaSalida);

        Preingreso = new TreeNode();
        Preingreso.Text = "Preingreso";
        Preingreso.NavigateUrl = "~/pages/preingreso.aspx";
        Preingreso.ToolTip = "Accede al Preingreso de un documento";
        Preingreso.ImageUrl = "~/images/Folder.gif";
        ((TreeView)this.FindControl("TreeViewMenu")).Nodes.Add(Preingreso);

        Ingreso = new TreeNode();
        Ingreso.Text = "Ingreso";
        Ingreso.NavigateUrl = "~/pages/ingreso.aspx?modo=";
        Ingreso.ToolTip = "Accede al ingreso de un documento";
        Ingreso.ImageUrl = "~/images/Folder.gif";
        ((TreeView)this.FindControl("TreeViewMenu")).Nodes.Add(Ingreso);

        //if (Session["OFPA_Administrador"] != null)
        //{
            Mantenedor = new TreeNode();
            Mantenedor.Text = "Mantenedores";
            Mantenedor.ImageUrl = "~/images/Tools.gif";
            ((TreeView)this.FindControl("TreeViewMenu")).Nodes.Add(Mantenedor);

            Metadato = new TreeNode();
            Metadato.Text = "Metadatos";
            Metadato.NavigateUrl = "~/pages/TipoDocumento.aspx";
            Metadato.ToolTip = "Accede al mantenedor de metadato";
            Metadato.ImageUrl = "~/images/Tools.gif";
            Mantenedor.ChildNodes.Add(Metadato);

            ((TreeView)this.FindControl("TreeViewMenu")).Nodes.Add(Metadato);
        //}
    }

    public static void SetMasterPage(MasterPage Master, HttpRequest request,
                                     string urlInicio, string urlSalir,
                                     string userconnected, string currenpage,
                                     string navegacion)
    {
        Label labeluser = (Label)Master.FindControl("labelusuario");
        if (labeluser != null)
        {
            labeluser.Text = userconnected;
        }

        Label labelnav = (Label)Master.FindControl("labelnavegacion");
        if (labelnav != null)
        {
            labelnav.Text = navegacion;
        }
        
        Master.Page.Title = "Biometrika Manejador Documental - " + currenpage;
    }
            
    //public static void SetMasterPage(MasterPage Master, HttpRequest request, 
    //                                 string urlInicio, string urlSalir, 
    //                                 string userconnected, string currenpage)
    //{
    //    Literal labelTituloMain = (Literal)Master.FindControl("labelTituloMain");
    //    if (labelTituloMain != null)
    //    {
    //        labelTituloMain.Text = currenpage;
    //    }
    //    Literal labelFeedbackTop = (Literal)Master.FindControl("labelFeedbackTop");
    //    if (labelFeedbackTop != null)
    //    {
    //        labelFeedbackTop.Text = "Usuario Conectado : " + userconnected + " ";
    //    }
    //    HyperLink hlinkInicio = (HyperLink)Master.FindControl("hlinkInicio");
    //    if (hlinkInicio != null)
    //    {
    //        hlinkInicio.Text = "Inicio";
    //        hlinkInicio.NavigateUrl = "~/pages/Main.aspx";
    //            //ExtractURLBase(request.Url.AbsoluteUri, request.CurrentExecutionFilePath) + "/pages/Main.aspx";
    //    }

    //    HyperLink hlinkEntrarSalir = (HyperLink)Master.FindControl("hlinkEntrarSalir");
    //    if (hlinkEntrarSalir != null)
    //    {
    //        hlinkEntrarSalir.Text = "Salir";
    //        hlinkEntrarSalir.NavigateUrl = ConfigurationManager.AppSettings["URLLogin"].ToString();
    //    }
    //}
    
    public static string ExtractURLBase(string urlcompleta, string urlrelativa)
    {
        return urlcompleta.Substring(0, urlcompleta.IndexOf(urlrelativa));
    }
    
}
