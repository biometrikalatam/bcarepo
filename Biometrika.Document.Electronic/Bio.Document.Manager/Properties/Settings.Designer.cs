﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Biometrika.Document.Electronic.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "17.1.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:8888/WSBDR.asmx")]
        public string Biometrika_Document_Electronic_WSBDR_WSBDR {
            get {
                return ((string)(this["Biometrika_Document_Electronic_WSBDR_WSBDR"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("soporte@biometrika.cl")]
        public string UserSMTP {
            get {
                return ((string)(this["UserSMTP"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("B1ometrika2014")]
        public string ClaveSMTP {
            get {
                return ((string)(this["ClaveSMTP"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("smtp.gmail.com")]
        public string ServerSMTP {
            get {
                return ((string)(this["ServerSMTP"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SMTPEnabled {
            get {
                return ((bool)(this["SMTPEnabled"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool NVEnabled {
            get {
                return ((bool)(this["NVEnabled"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:8888/WSBDR.asmx")]
        public string NVURL {
            get {
                return ((string)(this["NVURL"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("3")]
        public int NV_IDApp {
            get {
                return ((int)(this["NV_IDApp"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("300000")]
        public int NVTimeoutWS {
            get {
                return ((int)(this["NVTimeoutWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\\\ACODE\\\\bcarepo\\\\Biometrika.Document.Electronic\\\\Bio.Document.Manager\\\\bin")]
        public string PathNH {
            get {
                return ((string)(this["PathNH"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\\\ACODE\\\\bcarepo\\\\Biometrika.Document.Electronic\\\\Bio.Document.Manager\\\\bin")]
        public string PathLOG4 {
            get {
                return ((string)(this["PathLOG4"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\\\ACODE\\\\bcarepo\\\\Biometrika.Document.Electronic\\\\Bio.Document.Manager\\\\bin\\\\DT" +
            "Config.cfg")]
        public string DTConfig {
            get {
                return ((string)(this["DTConfig"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\\\ACODE\\\\bcarepo\\\\Biometrika.Document.Electronic\\\\Bio.Document.Manager\\\\bin\\\\te" +
            "mp\\\\")]
        public string PathTemp {
            get {
                return ((string)(this["PathTemp"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\\\ACODE\\\\bcarepo\\\\Biometrika.Document.Electronic\\\\Bio.Document.Manager\\\\bin\\\\te" +
            "mplatessig\\\\")]
        public string PathTemplatesSignatures {
            get {
                return ((string)(this["PathTemplatesSignatures"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\\\ACODE\\\\bcarepo\\\\Biometrika.Document.Electronic\\\\Bio.Document.Manager\\\\bin\\\\te" +
            "mplatessig\\\\LogoNV.jpg")]
        public string NVLogo {
            get {
                return ((string)(this["NVLogo"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://esigner6-test.servisign.cl/SignServerEsign/WSIntercambiaDocSoap")]
        public string Biometrika_Document_Electronic_cl_servisign_esigner6_WSIntercambiaDocSoapService {
            get {
                return ((string)(this["Biometrika_Document_Electronic_cl_servisign_esigner6_WSIntercambiaDocSoapService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("30000")]
        public int FEATimeoutWS {
            get {
                return ((int)(this["FEATimeoutWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://nvprod.cloudapp.net/BioPortal.Services.v5.8/BioPortal.Server.WS.Web.asmx")]
        public string Biometrika_Document_Electronic_BioPortalWSWEB_BioPortalServerWSWeb {
            get {
                return ((string)(this["Biometrika_Document_Electronic_BioPortalWSWEB_BioPortalServerWSWeb"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://nvprod.cloudapp.net/BioPortal.Services.v5.8/BioPortal.Server.WS.asmx")]
        public string Biometrika_Document_Electronic_BioPortalWS_BioPortalServerWS {
            get {
                return ((string)(this["Biometrika_Document_Electronic_BioPortalWS_BioPortalServerWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("10043999326")]
        public string KeyLibPDF {
            get {
                return ((string)(this["KeyLibPDF"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("https://qabpservice5.9.biometrikalatam.com/api/")]
        public string BPWebUrlBase {
            get {
                return ((string)(this["BPWebUrlBase"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("60000")]
        public int BPTimeout {
            get {
                return ((int)(this["BPTimeout"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("https://maps.googleapis.com/maps/api/staticmap")]
        public string GoogleMApURLApi {
            get {
                return ((string)(this["GoogleMApURLApi"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("15")]
        public string GoogleMapZoom {
            get {
                return ((string)(this["GoogleMapZoom"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("250x180")]
        public string GoogleMapSize {
            get {
                return ((string)(this["GoogleMapSize"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("https://tinyurl.com/yyrgzv26")]
        public string GoogleMapICON {
            get {
                return ((string)(this["GoogleMapICON"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("AIzaSyCsNCxJprwolv_rncjZ8RJX3LHzkkOsqoA")]
        public string GoogleMapKEY {
            get {
                return ((string)(this["GoogleMapKEY"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\ACODE\\bcarepo\\Biometrika.Document.Electronic\\Bio.Document.Manager\\bin\\template" +
            "ssig\\map_default.png")]
        public string GoogleMapDefault {
            get {
                return ((string)(this["GoogleMapDefault"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:8888/Verify")]
        public string DEUrlVerify {
            get {
                return ((string)(this["DEUrlVerify"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\ACODE\\bcarepo\\Biometrika.Document.Electronic\\Bio.Document.Manager\\bin\\notifyco" +
            "nfig\\logo_Mail_Fotter_Gral.jpg")]
        public string MailLogoFooterGral {
            get {
                return ((string)(this["MailLogoFooterGral"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\ACODE\\bcarepo\\Biometrika.Document.Electronic\\Bio.Document.Manager\\bin\\notifyco" +
            "nfig\\")]
        public string MailConfigPath {
            get {
                return ((string)(this["MailConfigPath"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Notify Biometrika Document Electronic (Dev)")]
        public string MailDisplayName {
            get {
                return ((string)(this["MailDisplayName"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\ACODE\\bcarepo\\Biometrika.Document.Electronic\\Bio.Document.Manager\\bin\\")]
        public string PathBaseBDEAssembly {
            get {
                return ((string)(this["PathBaseBDEAssembly"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public int DeTemplateIdDefault {
            get {
                return ((int)(this["DeTemplateIdDefault"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public int DeCompanyDeTemplateIdDefault {
            get {
                return ((int)(this["DeCompanyDeTemplateIdDefault"]));
            }
        }
    }
}
