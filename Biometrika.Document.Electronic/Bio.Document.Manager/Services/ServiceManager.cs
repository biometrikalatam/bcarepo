﻿using Biometrika.Document.Electronic.Api;
using Biometrika.Document.Electronic.Api.Model;
using Biometrika.Document.Electronic.Core.BP;
using Biometrika.Document.Electronic.Database;
using Biometrika.Document.Electronic.libs;
using Biometrika.Document.Electronic.PDF;
using log4net;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Biometrika.Document.Electronic.Services
{
    public class ServiceManager
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ServiceManager));

        /// <summary>
        /// Metodo proncipal de dispatcher de operaciones. Segun ActionId selecciona el subproceso a realizar.
        /// </summary>
        /// <param name="oXmlIn">Param entrada (XmlParamIn)</param>
        /// <param name="xmlparamout">Param salida (XmlParamOut)</param>
        /// <returns></returns>
        internal static int Process(XmlParamIn oXmlIn, out string xmlparamout)
        {
            xmlparamout = null;
            
            switch (oXmlIn.Actionid)
            {
                case 3:
                    return DEGet(oXmlIn, out xmlparamout);
                    break;
                case 5:
                    return DESign(oXmlIn, out xmlparamout);
                    break;
                default: //1 o cualqueir otra actionid
                    return DEGenerate(oXmlIn, out xmlparamout);
                    break;
            }
        }

        /// <summary>
        /// Metodo que recibe un PDF, y agrega 1 o mas firmas biometricas (BS) y firma con firma 
        /// electrónica simple o avanzada. Para esto agrega hojas al final del documento PDF enviado, 
        /// para firmar. 
        /// </summary>
        /// <param name="oXmlIn">Param entrada (XmlParamIn)</param>
        /// <param name="xmlparamout">Param salida (XmlParamOut)</param>
        /// <returns></returns>
        private static int DESign(XmlParamIn oXmlIn, out string xmlparamout)
        {
            int ret = 0;
            XmlParamOut xmlout = new XmlParamOut();
            xmlparamout = null;
            byte[] deout;
            string _seed = Guid.NewGuid().ToString("N");
            DateTime start, end;
            /*
             *  3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
             *  4.- Genero el reemplazo en word y grabo como PDF
             *  5.- Si viene con opción de Notarización => Genero RD
             *  6.- Save en BD y Retorno
             */
            try
            {
                start = DateTime.Now;
                LOG.Info(">>> Start DESign Start - " + start.ToString("dd/MM/yyyy HH:mm:ss"));
                LOG.Info(">>> Start DESign Semilla - " + _seed);
                xmlout.Trackid = _seed;  //Devuelvo misma SEED porque no estoy grabando TXs. En nueva version agregar TxLog
                xmlout.Actionid = oXmlIn.Actionid;

                if (oXmlIn.Actionid != 2)
                {
                    xmlout.Message = "Acción Inconsistente [" + oXmlIn.Actionid + "]";
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return Errors.RET_ERR_PARAM_INCORRECTOS;
                }
                LOG.Debug(">>> Start DESign Check ActionId OK...");

                if (oXmlIn.Company == 0 ||
                    oXmlIn.IdDE == 0 ||
                    String.IsNullOrEmpty(oXmlIn.EmployeeId) ||
                    String.IsNullOrEmpty(oXmlIn.User) ||
                    String.IsNullOrEmpty(oXmlIn.Password) ||
                    String.IsNullOrEmpty(oXmlIn.DE) || 
                    (oXmlIn.VerificationType > -1 && oXmlIn.ListCredentials == null))  //Added v5.6
                {
                    xmlout.Message = "Algunos datos faltantes";
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return Errors.RET_ERR_PARAM_INCORRECTOS;
                }
                LOG.Debug(">>> Start DESign Check datos IN - OK...");

                //3.- Chequeo que esté el id de aplicación, 
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                LOG.Debug(">>> Start DESign Check Login - [" + oXmlIn.Company + "-" + oXmlIn.User + "-" + oXmlIn.Password + "]...");
                int retaux = CheckLogin(oXmlIn.Company, oXmlIn.User, oXmlIn.Password);
                if (retaux < 0)
                {
                    LOG.Warn(">>> Start DESign Error Login => [" + oXmlIn.Company + "-" + oXmlIn.User + "-" + oXmlIn.Password + "]...");
                    xmlout.Message = "Credenciales presentadas errones";
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return retaux;
                }
                LOG.Debug(">>> Start DESign Check login OK...");

                LOG.Debug(">>> Start DESign BuscarClientePorID IN...");
                Database.Company oCliente = AdministradorCompany.BuscarCompanyById(oXmlIn.Company);
                if (oCliente == null)
                {
                    LOG.Warn(">>> Start DESign Error Compañia no existe...");
                    xmlout.Message = "Compañia no ingresada en el sistema";
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return Errors.RET_ERR_COMPANY_NOT_EXIST;
                }
                LOG.Debug(">>> Start DESign Cilente existe...");
                if (oCliente.Endate != null)
                {
                    xmlout.Message = "Compañia deshabilitada desde " + oCliente.Endate.Value.ToString("dd/MM/yyyy");
                    LOG.Warn(">>> Start DESign - " + xmlout.Message); 
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return Errors.RET_ERR_COMPANY_DISABLED;
                }
                LOG.Debug(">>> Start DESign Cilente Habilitado...");

                LOG.Debug(">>> Start DESign Check Vendedor IN - EmployeeId = " + oXmlIn.EmployeeId);
                //Chequeo vendedor
                //MdUsers oUser = database.AdministradorMdUsers.BuscarUserPorRut(oXmlIn.Company, oXmlIn.EmployeeId);
                //if (oUser == null)
                //{
                //    xmlout.Message = "User no configurado [" + oXmlIn.EmployeeId + "]";
                //    LOG.Warn(">>> Start DESign Error - " + xmlout.Message);
                //    xmlout.Timestampend = DateTime.Now;
                //    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                //    return Errors.RET_ERR_VENDOR_NOT_EXIST;
                //}
                LOG.Debug(">>> Start DESign Vendedor configurado OK...");

                List <Api.Model.Tag> tagsconfigured;
                Api.Model.Company oCompany;
                DocumentType oDType;
                LOG.Debug(">>> Start DESign DOCUMENT_TYPES.ExistDE [Company/IdDE = " + oXmlIn.Company + "/" + oXmlIn.IdDE);
                ret = Global.DOCUMENT_TYPES.ExistDE(oXmlIn.Company, oXmlIn.IdDE, out tagsconfigured, out oCompany, out oDType);
                if (ret != 0)
                {
                    xmlout.Message = "Compañia o Documento no configurado";
                    LOG.Warn(">>> Start DESign Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return ret;
                }
                LOG.Debug(">>> Start DESign DOCUMENT_TYPES.ExistDE OK...");
                string msgerr;

                XmlParamIn oXmlParamInProcessed;
                LOG.Debug(">>> Start DESign ProcesaParamIn IN...");
                ret = ProcesaParamIn(_seed, oXmlIn, out oXmlParamInProcessed, out msgerr);
                if (ret != 0)
                {
                    xmlout.Message = "Error de consistencia en los tags enviados [" + msgerr + "]";
                    LOG.Warn(">>> Start DESign Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return ret;
                }
                LOG.Debug(">>> Start DESign ProcesaParamIn OK...");

                //Added v5.6 - Si hay qeu verificar biometrico hacerlo antes de seguir
                bool _verificationOK = false;
                LOG.Debug(">>> DESign Verification IN! VerificationType = " + oXmlIn.VerificationType.ToString());
                if (oXmlIn.VerificationType > -1)
                {
                    _verificationOK = BPHelper.VerifyCredential(oXmlIn, out msgerr);
                }

                if (!_verificationOK)
                {
                    xmlout.Message = "Verificación biométrica negativa [" + msgerr + "]";
                    LOG.Warn(">>> DESign Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return Errors.RET_ERR_VERIFICATION_CREDENTIAL;
                }
                LOG.Debug(">>> DESign Verification OK");

                LOG.Debug(">>> Start DESign DOCUMENT_HELPER.SigneDE IN...");
                //4.- Genero el reemplazo en word y grabo como PDF
                ret = Global.DOCUMENT_HELPER.SigneDE(oXmlParamInProcessed, _seed, out deout, out msgerr);
                if (ret != 0)
                {
                    xmlout.Message = "Error firmando documento [" + msgerr + "]";
                    LOG.Warn(">>> Start DESign Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return ret;
                }
                LOG.Debug(">>> Start DESign DOCUMENT_HELPER.SigneDE OUT...");

                bool bToSave = true;
                int rdRet;
                string xmlEnvelope = null;

                LOG.Debug(">>> Start DESign Notarize Start...");
                LOG.Debug(">>> Start DESign Notarize Check => [oXmlIn.Notarize=" + oXmlIn.Notarize + " - " +
                                                               "NVEnabled=" + Properties.Settings.Default.NVEnabled + " - " +
                                                               "Config DE Notarize=" +
                    (((DocumentType)(Global.DOCUMENT_TYPES.htDocumentTypes[oXmlIn.Company.ToString() + "-" + oXmlIn.IdDE.ToString()]))).Notarize);
                // 5.- Si viene con opción de Notarización => Genero RD
                // Generar RD via WS
                if (oXmlIn.Notarize && Properties.Settings.Default.NVEnabled &&
                    (((DocumentType)(Global.DOCUMENT_TYPES.htDocumentTypes[oXmlIn.Company.ToString() + "-" + oXmlIn.IdDE.ToString()]))).Notarize)
                {
                    LOG.Info("   >>> Start Notarize - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    rdRet = NV.NVHelper.Notarize(_seed, oXmlIn.Company.ToString(), oXmlIn.IdDE.ToString(),
                                                     Global.DOCUMENT_TYPES.GetNameDE(oXmlIn.Company, oXmlIn.IdDE),
                                                     Convert.ToBase64String(deout), "pdf", out xmlEnvelope);
                    bToSave = bToSave && (rdRet == 0);
                    LOG.Info("   >>> Finish Notarize - rdRet=" + rdRet + "  - "  + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                }
                LOG.Debug(">>> Start DESign Notarize End...");

                LOG.Debug(">>> Start DESign SaveDB IN...");
                // 6.- Save en BD y Retorno
                if (bToSave)
                {
                    //Esto hay que sacarlo, solo para completar este piloto
                    //MdTipodocumentos oTypeDoc = database.AdministradorMdTipodocumentos.BuscarTipoDocumentoPorID(oXmlIn.IdDE);
                    //DeTemplate oTypeDoc = Database.AdministradorMdTipodocumentos.BuscarTipoDocumentoPorID(oXmlIn.IdDE);
                    //DeDocument documento = new DeDocument(oTypeDoc,
                    //                     _seed, ".pdf", DateTime.Now, DateTime.Now.AddDays(30),
                    //                     oCliente, null, Convert.ToBase64String(deout), null, xmlEnvelope,
                    //                     GetListMailsToNotify(oXmlIn.MailsToNotify),
                    //                     oXmlIn.TypeId, oXmlIn.ValueId);
                    LOG.Debug(">>> Start DESign SaveDB documento...");
                    try
                    {
                        //using (ISession session = PersistentManager.session())
                        //{
                        //    //session.Evict(oCliente);
                        //    //session.Clear();
                        //    LOG.Debug(">>> Start DESign Saving...");
                        //    session.Save(documento);
                        //    session.Flush();
                        //}

                        // TODO - HACER!!!!
                        //AdministradorDeDocument.CreateDocument();
                        LOG.Debug(">>> Start DESign Save OK...");

                        xmlout.De = Convert.ToBase64String(deout);
                        LOG.Debug(">>> Start DESign xmlout.De = " + xmlout.De);
                        xmlout.Notary = xmlEnvelope;
                        LOG.Debug(">>> Start DESign xmlout.xmlEnvelope = " + xmlEnvelope);

                        LOG.Debug(">>> Start DESign Check to send mail => SMTPEnabled = " +
                            Biometrika.Document.Electronic.Properties.Settings.Default.SMTPEnabled.ToString() +
                            " oXmlIn.MailsToNotify.Length = " + oXmlIn.MailsToNotify.Length);
                        //Si viene lista d emails, envio por mail
                        if (Biometrika.Document.Electronic.Properties.Settings.Default.SMTPEnabled && oXmlIn.MailsToNotify.Length > 0)
                        {
                            LOG.Debug(">>> Start DESign Sending mails...");
                            byte[] byattachRD = null;
                            //if (!String.IsNullOrEmpty(documento.Recibo)) byattachRD = System.Text.Encoding.UTF8.GetBytes(documento.Recibo);
                            byte[] byattachDE = deout;

                            //var thread = new Thread(new ParameterizedThreadStart(Utils.SendAllMailsByThread));
                            //thread.Start(filename)

                            LOG.Debug(">>> Start DESign Lanza Thread SendAllMailsByThread...");
                            //Thread thread = new Thread(() => Utils.SendAllMailsByThread(byattachRD, byattachDE, oXmlIn, documento));
                            //thread.Start();
                            LOG.Debug(">>> Start DESign Lanzado Thread SendAllMailsByThread!");

                        }
                    }
                    catch (Exception ex)
                    {
                        ret = Errors.RET_ERR_DE_NOT_SAVE_OR_NOTARIZE;
                        xmlout.Timestampend = DateTime.Now;
                        xmlout.Message = "Error salvando Tx en BD";
                        LOG.Error(">>> Start DESign Error - " + xmlout.Message);
                    }
                }
                LOG.Debug(">>> Start DESign SaveDB OUT...");
                end = DateTime.Now;
                TimeSpan dif = (end - start);
                xmlout.Timestampend = end;
                xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                LOG.Debug(">>> Start DESign Retorno = " + xmlparamout);

                LOG.Info(">>> Finish DESign - " + end.ToString("dd/MM/yyyy HH:mm:ss"));
                LOG.Info(">>> Tiempo total DESign - " + dif.ToString());
                ret = 0;
            }
            catch (Exception ex)
            {
                LOG.Error("Error DEGenerate", ex);
                ret = Errors.RET_ERR_UNKKNOW;
                xmlout.Message = "Error DEGenerate [" + ex.Message + "]";
            }
            LOG.Info(">>> Start DESign End! -> " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            return ret;
        }

 


        /// <summary>
        /// Genera un documento desde una planitlla Word, con Tags, y se firma con FES o FEA segun configuracion
        /// de la empresa y tipo de documento
        /// </summary>
        /// <param name="oXmlIn">Param entrada (XmlParamIn)</param>
        /// <param name="xmlparamout">Param salida (XmlParamOut)</param>
        /// <returns></returns>
        private static int DEGenerate(XmlParamIn oXmlIn, out string xmlparamout)
        {
            int ret = 0;
            XmlParamOut xmlout = new XmlParamOut();
            xmlparamout = null;
            byte[] deout;
            string _seed = Guid.NewGuid().ToString("N");
            DateTime start, end;
            /*
             *  3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
             *  4.- Genero el reemplazo en word y grabo como PDF
             *  5.- Si viene con opción de Notarización => Genero RD
             *  6.- Save en BD y Retorno
             */
            try
            {
                start = DateTime.Now;
                LOG.Info(">>> Start GenerateDE - " + start.ToString("dd/MM/yyyy HH:mm:ss"));
                LOG.Info(">>> Start GenerateDE Semilla - " + _seed);

                xmlout.Trackid = _seed;  //Devuelvo misma SEED porque no estoy grabando TXs. En nueva version agregar TxLog
                xmlout.Actionid = oXmlIn.Actionid;


                if (oXmlIn.Actionid != 1)
                {
                    xmlout.Message = "Acción Inconsistente [" + oXmlIn.Actionid + "]";
                    LOG.Warn(">>> GenerateDE Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return Errors.RET_ERR_PARAM_INCORRECTOS;
                }
                LOG.Debug(">>> Start DESign Check ActionId OK...");

                if (oXmlIn.Company == 0 ||
                    oXmlIn.IdDE == 0 ||
                    String.IsNullOrEmpty(oXmlIn.EmployeeId) ||
                    String.IsNullOrEmpty(oXmlIn.User) ||
                    String.IsNullOrEmpty(oXmlIn.Password) ||
                    oXmlIn.Tags == null || oXmlIn.Tags.Count == 0)
                {
                    xmlout.Message = "Algunos datos faltantes";
                    LOG.Warn(">>> GenerateDE Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return Errors.RET_ERR_PARAM_INCORRECTOS;
                }
                LOG.Debug(">>> Start DESign Check Data OK...");

                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                //      Obtengo documento enviado y veo si al menos están todas las extensiones (sipuedo tipo)
                LOG.Debug(">>> GenerateDE Check Login - [" + oXmlIn.Company + "-" + oXmlIn.User + "-" + oXmlIn.Password + "]...");
                int retaux = CheckLogin(oXmlIn.Company, oXmlIn.User, oXmlIn.Password);
                if (retaux < 0)
                {
                    xmlout.Message = "Credenciales presentadas errones";
                    LOG.Warn(">>> GenerateDE Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return retaux;
                }
                LOG.Debug(">>> GenerateDE Check Login OK...");

                LOG.Debug(">>> GenerateDE Check company IN...");
                Database.Company oCliente = AdministradorCompany.BuscarCompanyById(oXmlIn.Company);
                if (oCliente == null)
                {
                    xmlout.Message = "Compañia no ingresada en el sistema";
                    LOG.Warn(">>> GenerateDE Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return Errors.RET_ERR_COMPANY_NOT_EXIST;
                }
                if (oCliente.Endate != null)
                {
                    xmlout.Message = "Compañia deshabilitada desde " + oCliente.Endate.Value.ToString("dd/MM/yyyy");
                    LOG.Warn(">>> GenerateDE Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return Errors.RET_ERR_COMPANY_DISABLED;
                }
                LOG.Debug(">>> GenerateDE Check company OUT...");

                //LOG.Debug(">>> GenerateDE Check vendedor IN - EmployeeId = " + oXmlIn.EmployeeId);
                ////Chequeo vendedor
                //MdUsers oUser = database.AdministradorMdUsers.BuscarUserPorRut(oXmlIn.Company, oXmlIn.EmployeeId);
                //if (oUser == null)
                //{
                //    xmlout.Message = "User no configurado [" + oXmlIn.EmployeeId + "]";
                //    LOG.Warn(">>> GenerateDE Error - " + xmlout.Message);
                //    xmlout.Timestampend = DateTime.Now;
                //    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                //    return Errors.RET_ERR_VENDOR_NOT_EXIST;
                //}
                //LOG.Debug(">>> GenerateDE Check vendedor OK...");

                List<Tag> tagsconfigured;
                Api.Model.Company oCompany;
                DocumentType oDType;
                LOG.Debug(">>> GenerateDE Check DOCUMENT_TYPES.ExistDE IN [CompanyId/idDE = " + oXmlIn.Company + "/" + oXmlIn.IdDE + "]");
                ret = Global.DOCUMENT_TYPES.ExistDE(oXmlIn.Company, oXmlIn.IdDE, out tagsconfigured, out oCompany, out oDType);
                if (ret != 0)
                {
                    xmlout.Message = "Compañia o Documento no configurado";
                    LOG.Warn(">>> GenerateDE Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return ret;
                }
                LOG.Debug(">>> GenerateDE Check DOCUMENT_TYPES.ExistDE OK...");
                string msgerr;

                List<TagProcessed> tagsprocessed;
                LOG.Debug(">>> GenerateDE Check DOCUMENT_TYPES.CheckTagsIntegrity IN...");
                ret = Global.DOCUMENT_TYPES.CheckTagsIntegrity(_seed, oXmlIn.Tags, tagsconfigured, out tagsprocessed, out msgerr);
                if (ret != 0)
                {
                    xmlout.Message = "Error de consistencia en los tags enviados [" + msgerr + "]";
                    LOG.Warn(">>> GenerateDE Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return ret;
                }
                LOG.Debug(">>> GenerateDE Check DOCUMENT_TYPES.CheckTagsIntegrity OUT...");

                LOG.Debug(">>> GenerateDE DOCUMENT_HELPER.GenerateDE IN...");
                //4.- Genero el reemplazo en word y grabo como PDF
                ret = Global.DOCUMENT_HELPER.GenerateDE(oXmlIn, _seed, tagsprocessed, out deout, out msgerr);
                if (ret != 0)
                {
                    xmlout.Message = "Error generando documento [" + msgerr + "]";
                    LOG.Warn(">>> GenerateDE Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return ret;
                }
                LOG.Debug(">>> GenerateDE DOCUMENT_HELPER.GenerateDE OK => Doc = " + Convert.ToBase64String(deout));

                bool bToSave = true;
                int rdRet;
                string xmlEnvelope = null;

                LOG.Debug(">>> GenerateDE Notarize Start...");
                LOG.Debug(">>> GenerateDE Notarize Check => [oXmlIn.Notarize=" + oXmlIn.Notarize + " - " +
                                                               "NVEnabled=" + Properties.Settings.Default.NVEnabled + " - " +
                                                               "Config DE Notarize=" +
                    (((DocumentType)(Global.DOCUMENT_TYPES.htDocumentTypes[oXmlIn.Company.ToString() + "-" + oXmlIn.IdDE.ToString()]))).Notarize);
                // 5.- Si viene con opción de Notarización => Genero RD
                // Generar RD via WS
                if (oXmlIn.Notarize && Properties.Settings.Default.NVEnabled &&
                    (((DocumentType)(Global.DOCUMENT_TYPES.htDocumentTypes[oXmlIn.Company.ToString() + "-" + oXmlIn.IdDE.ToString()]))).Notarize) 
                {
                    LOG.Info("   >>> Start Notarize - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    rdRet = NV.NVHelper.Notarize(_seed, oXmlIn.Company.ToString(), oXmlIn.IdDE.ToString(),
                                                     Global.DOCUMENT_TYPES.GetNameDE(oXmlIn.Company, oXmlIn.IdDE),
                                                     Convert.ToBase64String(deout), "pdf", out xmlEnvelope);
                    bToSave = bToSave && (rdRet == 0);
                    LOG.Info("   >>> Finish Notarize - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                }
                LOG.Debug(">>> GenerateDE Notarize End...");

                LOG.Debug(">>> GenerateDE Notarize SaveDB IN...");
                // 6.- Save en BD y Retorno
                if (bToSave)
                {
                    //Esto hay que sacarlo, solo para completar este piloto
                    //MdTipodocumentos oTypeDoc = database.AdministradorMdTipodocumentos.BuscarTipoDocumentoPorID(oXmlIn.IdDE);
                    //MdDocumentos documento = new MdDocumentos(oTypeDoc,
                    //                     _seed, ".pdf", DateTime.Now, DateTime.Now.AddDays(30),
                    //                     oCliente, oUser, Convert.ToBase64String(deout), null, xmlEnvelope,
                    //                     GetListMailsToNotify(oXmlIn.MailsToNotify),
                    //                     oXmlIn.TypeId, oXmlIn.ValueId);
                    DeDocument documento = null;
                    string maillist = null;
                    if (oXmlIn.MailsToNotify != null)
                    {
                        bool isFirst = true;
                        foreach (string item in oXmlIn.MailsToNotify)
                        {
                            if (isFirst)
                            {
                                maillist = item;
                                isFirst = false;
                            } else
                            {
                                maillist = maillist + "," + item;
                            }

                        }
                    }
                    LOG.Debug(">>> GenerateDE CreateDocument documento<>null => " + (documento != null).ToString());
                    ret = AdministradorDeDocument.CreateDocument(Convert.ToBase64String(deout), oXmlIn.Company, oXmlIn.IdDE,
                                                                 tagsprocessed, null, null, null, maillist,
                                                                 null, xmlEnvelope, out documento, out msgerr);
                    LOG.Debug(">>> GenerateDE CreateDocument ret = " + ret.ToString());


                    if (ret == 0 && documento!=null)
                    {
                        try
                        {
                            LOG.Debug(">>> GenerateDE Save OK => Genero Transaccion...");
                            int rettx = AdministradorDeTx.CreateDeTx(ret, oXmlIn, documento, null, null, null, out msgerr);
                            if (rettx == 0) LOG.Debug(">>> GenerateDE Save Tx ret = " + rettx.ToString());
                            else LOG.Warn(">>> GenerateDE Save Tx ret = " + rettx.ToString());

                            xmlout.Trackid = documento.Trackid;
                            xmlout.De = Convert.ToBase64String(deout);
                            LOG.Debug(">>> GenerateDE Saved Doc = " + xmlout.De);
                            xmlout.Notary = xmlEnvelope;
                            LOG.Debug(">>> GenerateDE Saved Notary = " + xmlout.Notary);

                            LOG.Debug(">>> GenerateDE Check to send mail => SMTPEnabled = " +
                                Biometrika.Document.Electronic.Properties.Settings.Default.SMTPEnabled.ToString() +
                                " oXmlIn.MailsToNotify.Length = " + ((oXmlIn.MailsToNotify == null) ? "NULL" :
                                                                                                     oXmlIn.MailsToNotify.Length.ToString()));
                            //Si viene lista d emails, envio por mail
                            if (Properties.Settings.Default.SMTPEnabled && oXmlIn.MailsToNotify != null && 
                                oXmlIn.MailsToNotify.Length > 0)
                            {
                                byte[] byattachRD = null;
                                if (!String.IsNullOrEmpty(documento.Receipt)) byattachRD = System.Text.Encoding.UTF8.GetBytes(documento.Receipt);
                                byte[] byattachDE = deout;

                                //var thread = new Thread(new ParameterizedThreadStart(Utils.SendAllMailsByThread));
                                //thread.Start(filename)

                                LOG.Info(">>>>>> Lanza Thread SendAllMailsByThread...");
                                Utils.SendAllMailsByThread(byattachRD, byattachDE, oXmlIn, documento);
                                //Thread thread = new Thread(() => Utils.SendAllMailsByThread(byattachRD, byattachDE, oXmlIn, documento));
                                //thread.Start();
                                LOG.Info(">>>>>> Lanzado Thread SendAllMailsByThread!");

                            }
                        }
                        catch (Exception ex)
                        {
                            ret = Errors.RET_ERR_DE_NOT_SAVE_OR_NOTARIZE;
                            xmlout.Timestampend = DateTime.Now;
                            xmlout.Message = "Error salvando Tx en BD";
                            LOG.Warn(">>> GenerateDE Error - " + xmlout.Message);
                        }
                        ret = 0;
                    } else
                    {
                        LOG.Debug(">>> GenerateDE Save Tx NOOK => Genero Transaccion NO OK...");
                        int rettx = AdministradorDeTx.CreateDeTx(ret, oXmlIn, documento, null, null, null, out msgerr);
                        if (rettx == 0) LOG.Debug(">>> GenerateDE Save Tx ret = " + rettx.ToString());
                        else LOG.Warn(">>> GenerateDE Save Tx ret = " + rettx.ToString());
                        xmlout.Message = "Error creando documento [ret=" + ret.ToString() + "-" + msgerr + "]";
                    }
                }
                end = DateTime.Now;
                TimeSpan dif = (end - start);
                xmlout.Timestampend = end;
                xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                LOG.Debug(">>> GenerateDE xmlparamout = " + xmlparamout);

                //6.- Elimino datos temporales generados en HDD
                LOG.Debug("DocumentHelper.GenerateDE Clean tmp...");
                Global.DOCUMENT_HELPER.DeleteTemporalFiles(_seed);
                LOG.Info("   >>> Finish Generate in Word - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

                LOG.Info(">>> Finish GenerateDE - " + end.ToString("dd/MM/yyyy HH:mm:ss"));
                LOG.Info(">>> Tiempo total GenerateDE - " + dif.ToString());

            }
            catch (Exception ex)
            {
                LOG.Error("GenerateDE Excp Error DEGenerate", ex);
                ret = Errors.RET_ERR_UNKKNOW;
                xmlout.Message = "Error DEGenerate [" + ex.Message + "]";
            }
            LOG.Info(">>> End GenerateDE " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            return ret;
        }

        /// <summary>
        /// Verifica que vengan los parámetros necesarios para cada accion
        /// </summary>
        /// <param name="oXmlIn">Parametros de entrada (XmlParamIn)</param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static bool IsCorrectParamInForAction(XmlParamIn oXmlIn, out string msgerr)
        {
            msgerr = null;
            bool ret = false;
            try
            {
                //Es Generate desde word
                if (oXmlIn.Actionid == 1 && (oXmlIn.IdDE == 0 ||
                    (oXmlIn.Actionid == 1 && oXmlIn.Tags.Count == 0) || 
                    oXmlIn.Company == 0))
                {
                    msgerr = "Tags vacíos y/o Id Documento = 0. Imposible Generar Documento";
                    LOG.Debug("IsCorrectParamInForAction Error - " + msgerr);
                    ret = false;
                }

                //es GET
                if (oXmlIn.Actionid == 3 && (String.IsNullOrEmpty(oXmlIn.TxId)))
                {
                    msgerr = "Debe ingresar id (TxId) para recupear un documento";
                    LOG.Debug("IsCorrectParamInForAction Error - " + msgerr);
                    ret = false;
                }

                //Es sign desde PDF
                if ((oXmlIn.Actionid == 2 && String.IsNullOrEmpty(oXmlIn.DE)) ||
                    oXmlIn.Company == 0)
                {
                    msgerr = "Tags vacíos y/o Id Documento = 0. Imposible Generar Documento";
                    LOG.Debug("IsCorrectParamInForAction Error - " + msgerr);
                    ret = false;
                }

                if (String.IsNullOrEmpty(oXmlIn.EmployeeId) ||
                    String.IsNullOrEmpty(oXmlIn.User) ||
                    String.IsNullOrEmpty(oXmlIn.Password))
                {
                    msgerr = "Employee/User/Password no pueden ser nulos";
                    LOG.Debug("IsCorrectParamInForAction Error - " + msgerr);
                    ret = false;
                }

                ret = true;
            }
            catch (Exception ex)
            {
                LOG.Error("ServiceManager.IsCorrectParamInForAction Error - " + ex.Message);
                ret = false;
            }
            return ret;
        }

        /// <summary>
        /// Dado in TxId se recuepra el documento antes generado o firmado.
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        internal static int DEGet(XmlParamIn oXmlIn, out string xmlparamout)
        {
            int ret = 0;
            XmlParamOut xmlout = new XmlParamOut();
            xmlparamout = null;
            byte[] deout;
            //string _seed = Guid.NewGuid().ToString("N");
            /*
             *  1.- Deserializo parametro
             *  2.- Chequeo datos básicos
             *  3.- Chequeo que esté doc a recuperar
             *  4.- Si viene con opción de verificación de Notarización => Check via NV
             *  5.- Save en BD y Retorno
             */
            try
            {
                LOG.Info(">>> Start DEGet [Semilla - " + oXmlIn.TxId + "] - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                // 1.- Deserializo parametro
                xmlout.Trackid = oXmlIn.TxId;
                xmlout.Actionid = oXmlIn.Actionid;

                LOG.Debug(">>> DEGet checkLogin  - [" + oXmlIn.Company + "-" + oXmlIn.User + "-" + oXmlIn.Password + "]...");
                int retaux = CheckLogin(oXmlIn.Company, oXmlIn.User, oXmlIn.Password);
                if (retaux < 0)
                {
                    xmlout.Message = "Credenciales presentadas errones";
                    LOG.Warn(">>> DEGet Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return retaux;
                }
                LOG.Debug(">>> DEGet checkLogin OK...");

                LOG.Debug(">>> DEGet Check company IN...");
                Database.Company oCliente = AdministradorCompany.BuscarCompanyById(oXmlIn.Company);
                if (oCliente == null)
                {
                    xmlout.Message = "Compañia no ingresada en el sistema";
                    LOG.Warn(">>> DEGet Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return Errors.RET_ERR_COMPANY_NOT_EXIST;
                }
                if (oCliente.Endate != null)
                {
                    xmlout.Message = "Compañia deshabilitada desde " + oCliente.Endate.Value.ToString("dd/MM/yyyy");
                    LOG.Warn(">>> GenerateDE Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return Errors.RET_ERR_COMPANY_DISABLED;
                }
                LOG.Debug(">>> DEGet Check company OUT...");

                LOG.Debug(">>> DEGet BuscarDocumentosByIdDoc IN Id = " + oXmlIn.TxId);
                //3.- Chequeo que esté doc a recuperar
                DeDocument oDocumento = Database.AdministradorDeDocument.BuscarDocumentosByTrackId(oXmlIn.TxId);
                if (oDocumento == null)
                {
                    xmlout.Message = "Documento no existente";
                    LOG.Warn(">>> DEGet Error - " + xmlout.Message);
                    xmlout.Timestampend = DateTime.Now;
                    xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                    return Errors.RET_ERR_DE_NOT_EXIST;
                }
                LOG.Debug(">>> DEGet BuscarDocumentosByIdDoc OUT - Doc = " + oDocumento.Bytedocument);
                xmlout.De = oDocumento.Bytedocument;
                LOG.Debug(">>> DEGet BuscarDocumentosByIdDoc OUT - Recibo = " + oDocumento.Receipt);
                xmlout.Notary = oDocumento.Receipt;
                xmlout.Timestampend = DateTime.Now;
                xmlparamout = libs.XmlUtils.SerializeObject<XmlParamOut>(xmlout);
                LOG.Debug(">>> DEGet xmlparamout 0 " + xmlparamout);
            }
            catch (Exception ex)
            {
                LOG.Error("Error DEGet", ex);
                ret = Errors.RET_ERR_UNKKNOW;
                xmlout.Message = "Error DEGet [" + ex.Message + "]";
            }
            LOG.Info(">>> End DEGet [Semilla - " + oXmlIn.TxId + "] - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            return ret;
        }

        #region private Method

        /// <summary>
        /// Toma los datos de XmlParamIn y toma los datos apra la generacion de las firmas necesarias si hay firma ABS
        /// Si solo es firma simple electronica o FEA se tienen los datos en la definicion del documento
        /// </summary>
        /// <param name="seed"></param>
        /// <param name="tags"></param>
        /// <param name="tagsprocessed"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        private static int ProcesaParamIn(string seed, XmlParamIn paramin, out XmlParamIn oXmlParamInProcessed, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            try
            {
                oXmlParamInProcessed = (XmlParamIn)paramin.Clone();
                List<BS> listBSProcessed;
                if (oXmlParamInProcessed != null && oXmlParamInProcessed.BSSignatures != null)
                {
                    listBSProcessed = new List<BS>();
                    foreach (BS item in oXmlParamInProcessed.BSSignatures)
                    {
                        item.BiometricSignature = PDFHelper.GetImageFromBS(item, item.BiometricSignatureType);
                        listBSProcessed.Add(item);
                    }
                    oXmlParamInProcessed.BSSignatures = listBSProcessed;
                }
            }
            catch (Exception)
            {
                oXmlParamInProcessed = null;
                ret = -1;
            }
            return ret;
        }

        /// <summary>
        /// Chequeo que la empresa + user + password esten ok. 
        /// </summary>
        /// <param name="company"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        static private int CheckLogin(int company, string user, string password)
        {
            int ret = Errors.RET_OK;
            try
            {
                Api.Model.Company com = (Api.Model.Company)Global.DOCUMENT_TYPES.htClientes[company];
                if (com != null)
                {
                    if (!com.User.Equals(user.Trim()))
                    {
                        LOG.Debug("Error WSDE.CheckLogin User " + user + " configurado no corresponde a  IdCompany = " + company.ToString());
                        ret = Errors.RET_ERR_USER_NOT_CONFIGURED;
                    }
                    if (!com.Password.Equals(password.Trim()))
                    {
                        LOG.Debug("Error WSDE.CheckLogin Password presentada no corresponde a  IdCompany = " + company.ToString());
                        ret = Errors.RET_ERR_PASSWORD_NOT_MATCH;
                    }
                }
                else
                {
                    LOG.Debug("Error WSDE.CheckLogin Get Company from ht = Null con key = " + company.ToString());
                    ret = Errors.RET_ERR_COMPANY_NOT_EXIST;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Error WSDE.CheckLogin", ex);
                ret = Errors.RET_ERR_UNKKNOW;
            }
            return ret;
        }

        /// <summary>
        /// Formatea la lista de mails donde mandar notificaciones
        /// </summary>
        /// <param name="listMails"></param>
        /// <returns></returns>
        static private string GetListMailsToNotify(string[] listMails)
        {
            try
            {
                if (listMails == null || listMails.Length == 0) return null;

                bool first = true;
                string strList = null;
                for (int i = 0; i < listMails.Length; i++)
                {
                    if (first)
                    {
                        strList = listMails[i];
                        first = false;
                    }
                    else strList = strList + "," + listMails[i];
                }
                return strList;
            }
            catch (Exception ex)
            {
                LOG.Error("Error GetListMailsToNotify", ex);
                return null;
            }
        }

        #endregion private Method

    }
}