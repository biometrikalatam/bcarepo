﻿using Biometrika.Document.Electronic.Api;
using Biometrika.Document.Electronic.Database;
using Biometrika.Document.Electronic.document;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Biometrika.Document.Electronic.Api;
using Biometrika.Document.Electronic.libs;
using System.Threading;
using Biometrika.Document.Electronic.Services;

namespace Biometrika.Document.Electronic
{
    /// <summary>
    /// Descripción breve de WSDE
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WSDE : System.Web.Services.WebService
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(WSDE));

        [WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }

        /// <summary>
        /// Metodo de entrada. Dependiendo del ActionId indicado es la tarea que realiza.
        /// </summary>
        /// <param name="xmlparamin">Parámetros de entrada (Ver XmlParamIn)</param>
        /// <param name="xmlparamout">Parámetros de salida (Ver XmlParamOut)</param>
        /// <returns></returns>
        [WebMethod]
        public int Process(string xmlparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msgerr = null;

            try
            {
                ////0.- Chequeo licencia valida
                //if (!Global.LicenseValid(Global.SERVICE_WS))
                //{
                //    oXmlOut.Message = Errors.SERR_LICENSE;
                //    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    return Errors.IERR_LICENSE;
                //}
                LOG.Debug("WSDE.asmx.cs.Process IN...");
                //1.- Deserializo objeto xmlparamin
                LOG.Debug("WSDE.asmx.cs.Process Deserializing xmlparamin = " + xmlparamin);
                XmlParamIn oXmlIn = Api.XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                LOG.Debug("WSDE.asmx.cs.Process Deserialized!");
                if (oXmlIn == null)
                {
                    oXmlOut.Message = "Error deserializando parametros de entrada";
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.RET_ERR_PARAM_INCORRECTOS;
                }

                //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                LOG.Debug("WSDE.asmx.cs.Process Checking IsCorrectParamInForAction...");
                if (!ServiceManager.IsCorrectParamInForAction(oXmlIn, out msgerr))
                {
                    oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                        msgerr + "]";
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.RET_ERR_PARAM_INCORRECTOS;
                }

                //3.- Llamo a ServicesManager.Process con parametros deserializados.
                LOG.Debug("WSDE.asmx.cs.Process ServiceManager.Process In...");
                res = ServiceManager.Process(oXmlIn, out xmlparamout);

            }
            catch (Exception ex)
            {
                res = Errors.RET_ERR_UNKKNOW;
                oXmlOut.Message = "Error Desconocido - ex = " + ex.Message;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                LOG.Error("WSDE.Process", ex);
            }
            LOG.Debug("WSDE.asmx.cs.Process Out res=[" + res + "]");
            return res;
        }

     }
}
