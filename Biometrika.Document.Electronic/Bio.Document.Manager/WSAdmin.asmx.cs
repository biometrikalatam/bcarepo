﻿using Bio.Core.Utils;
using Biometrika.Document.Electronic.libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Biometrika.Document.Electronic
{
    /// <summary>
    /// Summary description for WSAdmin
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSAdmin : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public int GetDTConfig(out string dtconfig)
        {
            int ret = 0;
            dtconfig = null;
            try
            {
                dtconfig = SerializeHelper.SerializeToXml(Global.DOCUMENT_TYPES);
            }
            catch (Exception ex)
            {
                ret = -1;
            }

            return ret;
        }

    }
}
