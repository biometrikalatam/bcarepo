﻿using Biometrika.Document.Electronic.Api;
using Biometrika.Document.Electronic.Api.Model;
using log4net;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Biometrika.Document.Electronic.WebApi.Controllers
{
    public class OrchestratorController : ApiController
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(OrchestratorController));

        [Route("api/v2/deCreate")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "DeApiResult", typeof(DeApiResult))]
        public object CreateDe(CreateDeModel param)
        {
            /*
             
                1.- Check Acceso Basic Auth
                2.- Check Company (Existe y Enabled)
                3.- Verify integridad de param
                4.- Crea DeDocument, con sus signatory (en cada uno se debe generar una trackid en bpweb si aplica)
                5.- Retorno resultado
            */
            object result = null;
            Database.Company _Company;
            string msg;
            try
            {
                LOG.Debug("OrchestratorController.CreateDe IN...");
                //1.- Check Acceso Basic Auth
                //2.- Check Company(Existe y Enabled)
                if (!DEUnitOfWork.Authorized(Request, out _Company, out msg))
                {
                    LOG.Warn("OrchestratorController.CreateDe - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new DeApiResult(Errors.RET_ERR_LOGIN_INCORRECT, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msg) ? "" : msg) + "]", null)));
                }

                //3.- Verify integridad de param
                if (param == null || param.templateid == 0 || param.signatorylist == null ||
                    param.signatorylist.Count == 0)
                {
                    LOG.Warn("OrchestratorController.CreateDe - Parametros incorrectos!");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new DeApiResult(Errors.RET_ERR_PARAM_INCORRECTOS, "Parametros incorrectos", null)));
                }

                //4.- Crea DeDocument, con sus signatory(en cada uno se debe generar una trackid en bpweb si aplica)
                string trackid;
                int ret = DEUnitOfWork.CreateDe(param, _Company, out trackid, out msg);

                //5.- Retorno resultado
                if (ret == 0)
                {
                    result = Request.CreateResponse(HttpStatusCode.OK,
                                        (new DeApiResult(Errors.RET_OK, null, trackid)));
                } else
                {
                    result = Request.CreateResponse(HttpStatusCode.NotImplemented,
                        (new DeApiResult(Errors.RET_ERR_DE_NOT_SAVE_OR_NOTARIZE, 
                                         "DE No creado [" + ret + " - " + msg + "]", null)));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("OrchestratorController.CreateDe Excp Error: " + ex.Message);
                result = Request.CreateResponse(HttpStatusCode.InternalServerError,
                        (new DeApiResult(Errors.RET_ERR_UNKKNOW,"Excp Error: " + ex.Message, null)));
            }
            LOG.Debug("OrchestratorController.CreateDe OUT!");
            return result;
        }

        [Route("api/v2/deGet")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "DeApiResult", typeof(DeApiResult))]
        public object GetDe(string trackidde)
        {
            /*
             
                1.- Check Acceso Basic Auth
                2.- Check Company (Existe y Enabled)
                3.- Verify integridad de param
                4.- Crea DeDocument, con sus signatory (en cada uno se debe generar una trackid en bpweb si aplica)
                5.- Retorno resultado
            */
            object result = null;
            Database.Company _Company;
            string msg;
            try
            {
                LOG.Debug("OrchestratorController.GetDe IN...");
                //1.- Check Acceso Basic Auth
                //2.- Check Company(Existe y Enabled)
                if (!DEUnitOfWork.Authorized(Request, out _Company, out msg))
                {
                    LOG.Warn("OrchestratorController.GetDe - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new DeApiResult(Errors.RET_ERR_LOGIN_INCORRECT, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msg) ? "" : msg) + "]", null)));
                }

                //3.- Verify integridad de param
                if (string.IsNullOrEmpty(trackidde))
                {
                    LOG.Warn("OrchestratorController.GetDe - Parametros incorrectos!");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new DeApiResult(Errors.RET_ERR_PARAM_INCORRECTOS, "Parametros incorrectos", null)));
                }

                //4.- Crea DeDocument, con sus signatory(en cada uno se debe generar una trackid en bpweb si aplica)
                DeApiResult response;
                int ret = DEUnitOfWork.GetDe(trackidde, out response, out msg);

                //5.- Retorno resultado
                if (ret == 0)
                {
                    result = Request.CreateResponse(HttpStatusCode.OK, response);
                }
                else
                {
                    result = Request.CreateResponse(HttpStatusCode.NotImplemented,
                        (new DeApiResult(Errors.RET_ERR_DE_NOT_EXIST,
                                         "DE No Existe [" + ret + " - " + msg + "]", null)));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("OrchestratorController.GetDe Excp Error: " + ex.Message);
                result = Request.CreateResponse(HttpStatusCode.InternalServerError,
                        (new DeApiResult(Errors.RET_ERR_UNKKNOW, "Excp Error: " + ex.Message, null)));
            }
            LOG.Debug("OrchestratorController.GetDe OUT!");
            return result;
        }

        [Route("api/v2/deGetStatus")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "DeApiResult", typeof(DeApiResult))]
        public object GetStatus(string trackidde)
        {
            /*
             
                1.- Check Acceso Basic Auth
                2.- Check Company (Existe y Enabled)
                3.- Verify integridad de param
                4.- Crea DeDocument, con sus signatory (en cada uno se debe generar una trackid en bpweb si aplica)
                5.- Retorno resultado
            */
            object result = null;
            Database.Company _Company;
            string msg;
            try
            {
                LOG.Debug("OrchestratorController.GetStatus IN...");
                //1.- Check Acceso Basic Auth
                //2.- Check Company(Existe y Enabled)
                if (!DEUnitOfWork.Authorized(Request, out _Company, out msg))
                {
                    LOG.Warn("OrchestratorController.GetStatus - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new DeApiResult(Errors.RET_ERR_LOGIN_INCORRECT, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msg) ? "" : msg) + "]", null)));
                }

                //3.- Verify integridad de param
                if (string.IsNullOrEmpty(trackidde))
                {
                    LOG.Warn("OrchestratorController.GetStatus - Parametros incorrectos!");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new DeApiResult(Errors.RET_ERR_PARAM_INCORRECTOS, "Parametros incorrectos", null)));
                }

                //4.- Crea DeDocument, con sus signatory(en cada uno se debe generar una trackid en bpweb si aplica)
                DeApiResult response;
                int ret = DEUnitOfWork.GetDeStatus(trackidde, _Company, out response, out msg);

                //5.- Retorno resultado
                if (ret == 0)
                {
                    result = Request.CreateResponse(HttpStatusCode.OK, response);
                }
                else
                {
                    result = Request.CreateResponse(HttpStatusCode.NotImplemented,
                        (new DeApiResult(Errors.RET_ERR_DE_NOT_EXIST,
                                         "DE No Existe [" + ret + " - " + msg + "]", null)));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("OrchestratorController.GetStatus Excp Error: " + ex.Message);
                result = Request.CreateResponse(HttpStatusCode.InternalServerError,
                        (new DeApiResult(Errors.RET_ERR_UNKKNOW, "Excp Error: " + ex.Message, null)));
            }
            LOG.Debug("OrchestratorController.GetDe OUT!");
            return result;
        }

        [Route("api/v2/deCallbackBPWeb")]
        [HttpPost]
        public object CallbackBPWeb()
        {   
            string msgerr;
            try
            {
                LOG.Debug("OrchestratorController.CallbackBPWeb - IN...");
                string trackidbp = System.Web.HttpContext.Current.Request.Params["trackid"];
                LOG.Debug("OrchestratorController.CallbackBPWeb - TrackIdBP recibido => " + 
                                (string.IsNullOrEmpty(trackidbp) ? "Null" : trackidbp));

                LOG.Debug("OrchestratorController.CallbackBPWeb - Ingresa a UpdateStatus...");
                int ret = DEUnitOfWork.UpdateStatus(trackidbp, out msgerr);
                LOG.Debug("OrchestratorController.CallbackBPWeb - Sale de UpdateStatus => ret = " + ret.ToString());
            }
            catch (Exception ex)
            {
                LOG.Error("OrchestratorController.CallbackBPWeb Excp Error: ", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            LOG.Debug("OrchestratorController.CallbackBPWeb - OUT!");
            return Request.CreateResponse(HttpStatusCode.OK);
        }










        // GET: api/Orchestrator
        [Route("api/v2/ping")]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Orchestrator/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Orchestrator
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Orchestrator/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Orchestrator/5
        public void Delete(int id)
        {
        }
    }
}
