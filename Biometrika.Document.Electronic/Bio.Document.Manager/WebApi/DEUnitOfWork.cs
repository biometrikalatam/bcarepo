﻿using Biometrika.Document.Electronic.Api;
using Biometrika.Document.Electronic.Api.Interfaces;
using Biometrika.Document.Electronic.Api.Model;
using Biometrika.Document.Electronic.Core.BP;
using Biometrika.Document.Electronic.Core.libs;
using Biometrika.Document.Electronic.Database;
using Biometrika.Document.Electronic.libs;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography;
using System.Web;

namespace Biometrika.Document.Electronic.WebApi
{
    public class DEUnitOfWork
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(DEUnitOfWork));

        internal static bool Authorized(HttpRequestMessage request, out Database.Company company, out string msg)
        {
            bool ret = false;
            msg = null;
            company = null;
            try
            {
                LOG.Debug("DEUnitOfWork.Authorized IN...");
                IEnumerable<string> values = new List<string>();
                if (request.Headers.TryGetValues("Authorization", out values))
                {
                    foreach (string item in values)
                    {
                        if (CredentialOk(item, out company, out msg))
                        {
                            ret = true;
                            break;
                        }
                    }
                }
                else
                {
                    LOG.Warn("DEUnitOfWork.Authorized => No recibio Authentication en el Header!");
                }

            }
            catch (Exception ex)
            {
                ret = false;
                msg = "DEUnitOfWork.Authorized Excp: " + ex.Message;
                LOG.Error("DEUnitOfWork.Authorized Excp: " + ex.Message);
            }
            return ret;
        }

        private static bool CredentialOk(string encryptcredential, out Database.Company company, out string msg)
        {
            bool ret = false;
            msg = "";
            company = null;
            try
            {
                string[] arrS = encryptcredential.Split(' ');
                string plaincredential = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(arrS[1]));

                string[] arrCredential = plaincredential.Split(':');

                string msg1;
                company = AdministradorCompany.RetrieveAuthCredential(arrCredential[0], out msg1);

                if (company != null)
                {
                    if (company.Status == 1)
                    { //Si está deshabilitado
                        msg = "La compañia esta inhabilitada para hacer consultas";
                        if (company.Endate.HasValue)
                        {
                            msg = msg + " desde el " + company.Endate.Value.ToString("dd/MM/yyyy");
                        }
                        LOG.Debug("DEUnitOfWork.CredentialOk - Acceso NOOK de Username = " + arrCredential[0] +
                                     "[" + msg + "]");
                    }
                    else
                    {
                        if (company.Secretkey.Equals(arrCredential[1].Trim()))
                        {
                            LOG.Debug("DEUnitOfWork.CredentialOk - Acceso OK de Username = " + arrCredential[0]);
                            ret = true;
                        }
                        else
                        {
                            LOG.Debug("DEUnitOfWork.CredentialOk - Acceso NOOK de Username = " + arrCredential[0] +
                                     "[No coinciden SecretKeys => Param=" + arrCredential[1].Trim() + " <> BD=" +
                                     company.Secretkey + "]");
                        }
                    }
                }
                else
                {
                    LOG.Warn("DEUnitOfWork.CredentialOk - Username no encontrado => " + arrCredential[0] + "[msg=" + msg + "]");
                }
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("DEUnitOfWork.CredentialOk Excp: " + ex.Message);
            }
            return ret;
        }

        internal static int CreateDe(CreateDeModel param, Database.Company company, out string trackid, out string msg)
        {
            int ret = 0;
            trackid = null;
            msg = null;
            int wid = 0;
            string listmails;
            string trackidbp;
            BPTxCreateR bptxresponse;
            try
            {
                LOG.Debug("DEUnitOfWork.CreateDe IN...");
                //0.- Genero Trackid unico para crear luego los signer en bpweb
                trackid = AdministradorDeDocument.GetTrackIdUnique();

                //1.- Creo lista de signatores
                DeSignatory sig = null;
                List<DeSignatory> listSignatories = new List<DeSignatory>();
                foreach (Signatory item in param.signatorylist)
                {
                    LOG.Debug("DEUnitOfWork.CreateDe - Creando Tx en BPWeb para typeid/valueid=" +
                        item.typeid + "/" + item.valueid + " - Order = " + item.order + "...");
                    DeCompanyWorkflow dew = null;
                    wid = GetWorkflowidFromTemplateId(param.templateid, item.order, out dew);
                    LOG.Debug("DEUnitOfWork.CreateDe - obtenido WorkFlowId = " + wid.ToString());
                    sig = new DeSignatory(item.order, item.typeid, item.valueid, item.name, item.lastname, 
                                          item.mail, item.phone, item.taxidcompany, wid);
                    LOG.Debug("DEUnitOfWork.CreateDe - Ingresando a BPHelper.TxCreateSigner...");
                    ret = BPHelper.TxCreateSigner(trackid, param, company, sig, dew.Name,
                                                  out msg, out listmails, out trackidbp, out bptxresponse);
                    LOG.Debug("DEUnitOfWork.CreateDe - BPHelper.TxCreateSigner ret = " + ret.ToString());
                    sig.Urlbpweb = bptxresponse.verifyUrl;
                    sig.Trackidbp = trackidbp;
                    LOG.Debug("DEUnitOfWork.CreateDe - Signatory " + item.typeid + " / " + item.valueid +
                                " => trackidbp=" + trackidbp + " - Urlbpweb=" + sig.Urlbpweb);
                    listSignatories.Add(sig);
                }

                //2.- Creo DeDocument para grabar y DeTx
                DeDocument dedocument;
                DeTx detx;
                string qrverify = Utils.GetQR(Properties.Settings.Default.DEUrlVerify + "?" + trackid);
                ret = AdministradorDeDocument.CreateDocument(param, company, listSignatories, trackid, qrverify, 
                                                            out msg, out dedocument);
                if (ret==Errors.RET_OK)
                {
                    LOG.Debug("DEUnitOfWork.CreateDe - Save OK => Genero Transaccion...");
                    int rettx = AdministradorDeTx.CreateDeTx(ret, param, company, dedocument, out msg);
                    if (rettx == 0) LOG.Debug("DEUnitOfWork.CreateDe - Save Tx ret = " + rettx.ToString());
                    else LOG.Warn("DEUnitOfWork.CreateDe - Save Tx ret = " + rettx.ToString());

                    //Send Notify a listas de notificaciones
                    string[] linesBody = new string[3];
                    linesBody[0] = "Estimad@";
                    linesBody[1] = "Se ha creado un documento electrónico para su firma con identificado = " +
                                    "<b>" + dedocument.Trackid + "</b>.";
                    linesBody[2] = "Si usted ha sido indicado como firmante, le llegará otro mail con las indicaciones para " +
                                    "realizar el procesod e firma segura remota. De otra forma cuando se compelte el documento " +
                                    "le llegará una copia debido a que ha sido incluido en la lista de notificación.";
                    Notify.SendNotify(dedocument, null, true, "Documento Electrónico " + dedocument.Trackid, linesBody, null, null, null);

                    //Send Notify a firmantes
                    foreach (DeSignatory item in dedocument.DeSignatory)
                    {
                        linesBody = new string[3];
                        linesBody[0] = "Estimad@ Firmante,";
                        linesBody[1] = "Ha sido designado para firmar el documento electrónico " +
                                        "<b>" + dedocument.Trackid + "</b>.";
                        linesBody[2] = "Por favor ingrese en este <b><a href=\"" + item.Urlbpweb + 
                                        "\" target=\"_blank\">link</a></b> para completar el proceso, siguiendo los pasos indicados";
                        List<string> mailTo = new List<string>();
                        mailTo.Add(item.Mail);
                        Notify.SendNotify(dedocument, mailTo, false, "Documento Electrónico " + dedocument.Trackid, linesBody, null, null, null);
                    }
                }
                else
                {
                    msg = "Error generando DeDocuemnt [ret=" + ret.ToString() + "-Err=" + msg + "]";
                    LOG.Error("DEUnitOfWork.CreateDe - " + msg);
                }
            }
            catch (Exception ex)
            {
                ret = Errors.RET_ERR_UNKKNOW;
                LOG.Error("DEUnitOfWork.CreateDe Excp: " + ex.Message);
            }
            LOG.Debug("DEUnitOfWork.CreateDe OUT! ret = " + ret.ToString());
            return ret;
        }

        internal static int GetDe(string trackidde, out DeApiResult response, out string msg)
        {
            int ret = Errors.RET_OK;
            response = null;
            msg = null;
            try
            {
                LOG.Debug("DEUnitOfWork.GetDe IN...");
                DeDocument deDoc = AdministradorDeDocument.BuscarDocumentosByTrackId(trackidde, true);
                if (deDoc == null)
                {
                    msg = "De con trackid = " + trackidde + " - No Encontrado!";
                    LOG.Warn("DEUnitOfWork.GetDe - " + msg);
                    response = new DeApiResult(Errors.RET_ERR_UNKKNOW, msg, null);
                } else
                {
                    //Genero DeTx por la consulta
                    LOG.Debug("DEUnitOfWork.GetDe - Generando DeTx para get de deDoc.Trackid = " + trackidde + "...");
                    DeTx detx = AdministradorDeTx.CreateDeTx(Constant.DEDOCUMENT_ACTION_GET, deDoc, out msg);
                    LOG.Debug("DEUnitOfWork.GetDe - DeTx Generado con exito => " + (detx != null).ToString());

                    LOG.Warn("DEUnitOfWork.GetDe - deDoc devuelto con trackid = " + trackidde + "" +
                             " - id = " + deDoc.Id.ToString());
                    deDoc = DeleteCycleForJason(deDoc);
                    response = new DeApiResult(Errors.RET_OK, null, deDoc);
                }
            }
            catch (Exception ex)
            {
                ret = Errors.RET_ERR_UNKKNOW;
                msg = "Error recuperando De con trackid = " + trackidde + "[" + ex.Message + "]";
                response = new DeApiResult(Errors.RET_ERR_UNKKNOW, msg, null);
                LOG.Error("DEUnitOfWork.GetDe Excp Error: ", ex);
            }
            LOG.Debug("DEUnitOfWork.GetDe OUT! ret = " + ret.ToString());
            return ret;
        }

        internal static int GetDeStatus(string trackidde, Database.Company company, out DeApiResult response, out string msg)
        {
            int ret = Errors.RET_OK;
            response = null;
            msg = null;
            DeStatus _Status = new DeStatus();
            try
            {
                LOG.Debug("DEUnitOfWork.GetDe IN...");
                _Status.status = -1;
                DeDocument deDoc = AdministradorDeDocument.BuscarDocumentosByTrackId(trackidde, true);
                if (deDoc == null)
                {
                    msg = "De con trackid = " + trackidde + " - No Encontrado!";
                    LOG.Warn("DEUnitOfWork.GetDe - " + msg);
                    _Status.message = msg;  
                    response = new DeApiResult(Errors.RET_ERR_UNKKNOW, msg, null);
                }
                else
                {
                    LOG.Warn("DEUnitOfWork.GetDe - deDoc devuelto con trackid = " + trackidde + "" +
                             " - id = " + deDoc.Id.ToString());

                    if (deDoc.Status != Api.Constant.DEDOCUMENT_STATUS_COMPLETED)
                    {
                        deDoc = UpdateStatusDeDoc(deDoc);
                    }

                    _Status.trackidde = deDoc.Trackid;
                    _Status.status = deDoc.Status;
                    _Status.resultcode = GetResultCodeFromCreate(deDoc.DeTx); //Saca el resultcode de ActionId = 1 (Que es cuando se creo)
                    _Status.createdate = deDoc.Createdate;
                    response = new DeApiResult(Errors.RET_OK, null, _Status);

                    //Genero DeTx por la consulta
                    LOG.Debug("DEUnitOfWork.GetDe - Generando DeTx para get de deDoc.Trackid = " + trackidde + "...");
                    DeTx detx = AdministradorDeTx.CreateDeTx(Constant.DEDOCUMENT_ACTION_GETSTATUS, deDoc, out msg);
                    LOG.Debug("DEUnitOfWork.GetDe - DeTx Generado con exito => " + (detx != null).ToString());
                }
            }
            catch (Exception ex)
            {
                ret = Errors.RET_ERR_UNKKNOW;
                msg = "Error recuperando De con trackid = " + trackidde + "[" + ex.Message + "]";
                response = new DeApiResult(Errors.RET_ERR_UNKKNOW, msg, null);
                LOG.Error("DEUnitOfWork.GetDe Excp Error: ", ex);
            }
            LOG.Debug("DEUnitOfWork.GetDe OUT! ret = " + ret.ToString());
            return ret;
        }

        /// <summary>
        /// Para chequear y actualizar status de DeDcouemnt, cuando se recibe un callback desde 
        /// BPWeb. Se utiliza UpdateStatusDeDoc para facilitar el uso.
        /// </summary>
        /// <param name="trackidbp"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static int UpdateStatus(string trackidbp, out string msgerr)
        {
            /*
                1.- Obtengo el DeDocument
                2.- Llama a UpdateStatusDeDoc con el DeDocument obtenido
            */
            int ret = 0;
            msgerr = null;
            try
            {
                LOG.Debug("DEUnitOfWork.UpdateStatus IN...");
                //1.- Obtengo el DeDocument
                DeDocument _deDoc = AdministradorDeDocument.BuscarDocumentoByTrackIdBP(trackidbp, out msgerr);

                //2.- Llama a UpdateStatusDeDoc con el DeDocument obtenido
                if (_deDoc != null)
                {
                    LOG.Debug("DEUnitOfWork.UpdateStatus - Se recupero DeDocument => llama a UpdateStatusDeDoc...");
                    _deDoc = UpdateStatusDeDoc(_deDoc);
                    LOG.Debug("DEUnitOfWork.UpdateStatus - Salio UpdateStatusDeDoc: DeDoc Retornado != null => " +
                                (_deDoc!=null).ToString() + "!");
                } else
                {
                    LOG.Warn("DEUnitOfWork.UpdateStatus - Se recibio por callback desde BPWeb un trackid, pero " +
                                "no se pudo recuperar DeDocuemnt. TrackidBP recibido => " + trackidbp);
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("DEUnitOfWork.UpdateStatus Excp Error: ", ex);
            }
            LOG.Debug("DEUnitOfWork.UpdateStatus OUT! ret = " + ret.ToString());
            return ret;
        }

        /// <summary>
        /// Revisa status de cada firmante y de todo, para que si está todo completo se genera el DE
        /// y se manda el aviso al callback si aplica. Además updatea DeTx y status del DeDocument.
        /// </summary>
        /// <param name="deDoc"></param>
        /// <returns></returns>
        private static DeDocument UpdateStatusDeDoc(DeDocument deDoc)
        {
            /*
                1.-Recorro cada DeSignature y consulto status en BPWeb. Si esta ok => Update
                2.-Si todos los DeSignatory están ok
                    2.1-Se generan los tags para la generacion del DE. Para esto se toman los DeTemplateTags
                        y se recorren las DeSignatoryEvidences para tomar los que corresponden de acuerdo al mapeo
                        en la tabla
                    2.1-Se realiza la generacion y se firma con la firma final de la empresa o del DeTemplate segun
                        sea la firma.
                3.-Actualiza los estados en DoDocument y en DeTx
                4.-Recupero el doc de nuevo y lo devuelvo
            */
            DeDocument deDocOut = deDoc;
            int ret = 0;
            int qSignatoryOK = 0;
            try
            {
                LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc IN...");
                //1.-Recorro cada DeSignature y consulto status en BPWeb. Si esta ok => Update
                LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - Loop revision cada signatory INB...");
                foreach (DeSignatory item in deDoc.DeSignatory)
                {
                    ret = 0;
                    //Si esta creado pero no es negativo (Error) o 2 completo => Verifica via BPHelper y 
                    //actualiza valores si cambio el estado en BPWeb
                    LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - Checking trackid = " + item.Trackidbp);
                    if (item.Status == Constant.DESIGNATORE_STATUS_CREATED)
                    {
                        LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - BPHelper.TxUpdate para trackid = " + item.Trackidbp);
                        BpTxDataResponse oBpTxDataResponse;
                        string msgerr;
                        ret = BPHelper.TxStatus(deDoc.Company, item.Trackidbp, out msgerr, out oBpTxDataResponse);
                        LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - BPHelper.TxUpdate ret = " + ret.ToString());
                        if (ret < 0) break; //Si da error => corta xq transaccion general se corta 
                        else
                        {
                            DeSignatory desig = item;
                            if (oBpTxDataResponse != null)
                            {
                                desig = FillDeSignatoryFromBPResponse(item, oBpTxDataResponse);
                                ret = AdministradorDeSignatory.Update(item, out msgerr);
                                if (item.Status == Constant.DESIGNATORE_STATUS_COMPLETED)
                                {
                                    qSignatoryOK++;
                                    LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - qSignatoryOK = " + qSignatoryOK.ToString());
                                } else
                                {
                                    LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - Salgo por error en BPWeb = " + 
                                              item.Status.ToString());
                                    break;
                                }
                            } else
                            {
                                LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - Aun sigue pending en BPWeb " + 
                                    "para DeDignatory: Id=" + desig.Id + "-Valueid=" + desig.Valueid + "=> Salgo!");
                                break;
                            }
                        }
                    } else if (item.Status < 0)
                    {
                        LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - Status < 0 => Sale!");
                        ret = item.Status;
                        break; //Si tiene error => corta xq todo fallo
                    } else if (item.Status == Constant.DESIGNATORE_STATUS_COMPLETED)
                    {
                        qSignatoryOK++;
                        LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - qSignatoryOK = " + qSignatoryOK.ToString());
                    }
                }
                //Si la cantidad de OK es igual a la cantidad de firmantes => se inicia el proceso de generacion del DE
                if (qSignatoryOK == deDoc.DeSignatory.Count)
                {
                    List<TagProcessed> tagsprocessed;
                    List<DeDocumentTag> dbTagsList;
                    List<Tag> tagsIn;
                    byte[] byteDE;
                    string msgerr;
                    int idcompany = 0;
                    int idtemplate = 0;
                    ret = GenerateTagIn(deDoc.Trackid, out tagsIn, out dbTagsList, out idcompany, out idtemplate);
                    if (ret == 0)
                    {
                        List<Tag> tagsconfigured;
                        Api.Model.Company oCompany;
                        DocumentType oDType;
                        ret = Global.DOCUMENT_TYPES.ExistDE(deDoc.Company.Id, deDoc.DeTemplate.Id, 
                                                            out tagsconfigured, out oCompany, out oDType);
                        if (ret != 0)
                        {
                            LOG.Error("DEUnitOfWork.UpdateStatusDeDoc - Global.DOCUMENT_TYPES.ExistDE = " +
                                        " => Devuelve mismo DeDoc!");
                            return deDoc;
                        }
                        LOG.Debug(">>> GenerateDE Check DOCUMENT_TYPES.ExistDE OK...");
                        LOG.Debug(">>> GenerateDE Check DOCUMENT_TYPES.CheckTagsIntegrity IN...");
                        ret = Global.DOCUMENT_TYPES.CheckTagsIntegrity(deDoc.Trackid, tagsIn, tagsconfigured, 
                                                                       out tagsprocessed, out msgerr);
                        if (ret != 0)
                        {
                            LOG.Error("DEUnitOfWork.UpdateStatusDeDoc - Error de consistencia en los tags enviados [" + msgerr + "]");
                            return deDoc;
                        }
                        LOG.Debug(">>> GenerateDE Check DOCUMENT_TYPES.CheckTagsIntegrity OUT...");
                        deDoc.DeDocumentTag = dbTagsList;
                        ret = Global.DOCUMENT_HELPER.GenerateDE(deDoc, tagsprocessed, out byteDE, out msgerr);
                        if (ret == 0)
                        {
                            //1.- Update DeDocument
                            LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - AdministradorDeDocument.Update de trackid = " +
                                    deDoc.Trackid + " in...");
                            //ret = AdministradorDeDocument.Update(deDoc.Trackid, tagsprocessed, byteDE);
                            deDoc.Bytedocument = Convert.ToBase64String(byteDE);
                            deDoc.Status = Constant.DEDOCUMENT_STATUS_COMPLETED;
                            deDoc.Enddate = DateTime.Now;
                            //Genera la hora de resumen del DeDocument
                            deDoc.Resume = GenerateResume(deDoc);

                            ret = AdministradorDeDocument.Update(deDoc);
                            LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - AdministradorDeDocument.Update ret = " + ret.ToString());

                            //2.- Update DeTx
                            LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - AdministradorDeTx.Update de trackid = " +
                                    deDoc.Trackid + " in...");
                            ret = AdministradorDeTx.Update(deDoc.Trackid);
                            LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - AdministradorDeTx.Update ret = " + ret.ToString());

                            //2.1.- Si existe Assembly en DeTemplate => Ejecuto
                            object objReturn;
                            ret = ProcessAssemblyPostGenerate(deDoc, out objReturn);
                            if (objReturn != null)
                            {
                                //Todo Ver como procesarolo. Quiza con el recorrido de properties, agregar todo en dinamicdata del dedoc?
                                LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - ProcessAssemblyPostGenerate Retorno Objeto != null!");
                            }
                            LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - ProcessAssemblyPostGenerate ret = " + ret.ToString());

                            //3.- Envio mail si está configurado y estan los mails
                            //Send Notify a listas de notificaciones
                            string[] linesBody = new string[3];
                            linesBody[0] = "Estimad@s";
                            linesBody[1] = "Se ha completado documento electrónico con identificador: " +
                                            "<b>" + deDoc.Trackid + "</b>.";
                            linesBody[2] = "Adjunto encontrará una copia del documento generado.";
                            ret = Notify.SendNotify(deDoc, null, true, "Documento Electrónico " + deDoc.Trackid, linesBody,
                                              Convert.FromBase64String(deDoc.Bytedocument), "pdf", "de_" + deDoc.Trackid + ".pdf");
                            LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - Notify.SendNotify ret = " + ret.ToString());

                            if (!string.IsNullOrEmpty(deDoc.Resume))
                            {
                                linesBody[0] = "Estimad@s";
                                linesBody[1] = "Se ha completado documento electrónico con identificador: " +
                                                "<b>" + deDoc.Trackid + "</b>.";
                                linesBody[2] = "Adjunto encontrará una hoja de resúmen de certificación del proceso.";
                                ret = Notify.SendNotify(deDoc, null, true, "Hoja Resúmen Documento Electrónico " + deDoc.Trackid, linesBody,
                                                  Convert.FromBase64String(deDoc.Resume), "pdf", "audit_de_" + deDoc.Trackid + ".pdf");
                                LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - Notify.SendNotify Audit ret = " + ret.ToString());
                            } 

                            //4.- Envio Callback si está configurado
                            if (!string.IsNullOrEmpty(deDoc.Callbackurl)) {
                                ret = SendCallback(deDoc.Callbackurl, deDoc.Trackid);
                                LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - SendCallback ret = " + ret.ToString());
                            }
 
                            //5.- Recupero de nuevo DeDocument para devolver
                            LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - AdministradorDeDocument.BuscarDocumentosByTrackId de trackid = " +
                                    deDoc.Trackid + " in...");
                            deDoc = AdministradorDeDocument.BuscarDocumentosByTrackId(deDoc.Trackid, true);
                            LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc - AdministradorDeDocument.BuscarDocumentosByTrackId OK!");

                        }
                        else
                        {
                            LOG.Error("DEUnitOfWork.UpdateStatusDeDoc - Error generando GenerateDE de trackid = " +
                                    deDoc.Trackid + " => ret = " + ret.ToString() + " => Devuelve mismo DeDoc!");
                        }
                    } else
                    {
                        LOG.Error("DEUnitOfWork.UpdateStatusDeDoc - Error generando las tagprocessed de trackid = " +
                                    deDoc.Trackid + " => ret = " + ret.ToString() + " => Devuelve mismo DeDoc!");
                    }
                }

            }
            catch (Exception ex)
            {
                deDocOut = deDoc;
                LOG.Error("DEUnitOfWork.UpdateStatusDeDoc Excp Error: ", ex);
            }
            LOG.Debug("DEUnitOfWork.UpdateStatusDeDoc OUT!");
            return deDoc;
        }

        private static string GenerateResume(DeDocument deDoc)
        {
            string retResume = null;
            try
            {
                LOG.Debug("DEUnitOfWork.GenerateResume IN para DeDecument con Trackid = " + deDoc.Trackid + "..."); 
                List <TagProcessed> tagsprocessed;
                List<DeDocumentTag> dbTagsList;
                List<Tag> tagsIn;
                byte[] byteDE;
                string msgerr;
                int idcompany = 0;
                int idtemplate = 0;
                int ret = GenerateTagIn(deDoc.Trackid, out tagsIn, out dbTagsList, 
                                        out idcompany, out idtemplate, true);
                if (ret == 0)
                {
                    List<Tag> tagsconfigured;
                    Api.Model.Company oCompany;
                    DocumentType oDType;
                    ret = Global.DOCUMENT_TYPES.ExistDE(idcompany, idtemplate,
                                                        out tagsconfigured, out oCompany, out oDType);
                    if (ret != 0)
                    {
                        LOG.Error("DEUnitOfWork.GenerateResume - Global.DOCUMENT_TYPES.ExistDE = " +
                                    " => Devuelve Null!");
                        return null;
                    }
                    LOG.Debug(">>> DEUnitOfWork.GenerateResume Check DOCUMENT_TYPES.ExistDE OK...");
                    LOG.Debug(">>> DEUnitOfWork.GenerateResume Check DOCUMENT_TYPES.CheckTagsIntegrity IN...");
                    ret = Global.DOCUMENT_TYPES.CheckTagsIntegrity(deDoc.Trackid, tagsIn, tagsconfigured,
                                                                   out tagsprocessed, out msgerr);
                    if (ret != 0)
                    {
                        LOG.Error("DEUnitOfWork.GenerateResume - Error de consistencia en los tags enviados [" + msgerr + "]");
                        return null;
                    }
                    LOG.Debug(">>> DEUnitOfWork.GenerateResume Check DOCUMENT_TYPES.CheckTagsIntegrity OUT...");
                    //deDoc.DeDocumentTag = dbTagsList;
                    ret = Global.DOCUMENT_HELPER.GenerateDE(deDoc, tagsprocessed, out byteDE, out msgerr,
                                                            idcompany, idtemplate);
                    if (ret == 0)
                    {
                        //1.- Update DeDocument
                        LOG.Debug("DEUnitOfWork.GenerateResume - Retorno resume...");
                        retResume = Convert.ToBase64String(byteDE);
                    }
                    else
                    {
                        LOG.Error("DEUnitOfWork.GenerateResume - Error generando Resume de trackid = " +
                                deDoc.Trackid + " => ret = " + ret.ToString() + " => Devuelve mismo DeDoc!");
                    }
                }
                else
                {
                    LOG.Error("DEUnitOfWork.GenerateResume - Error generando las tagprocessed de Resume de trackid = " +
                                deDoc.Trackid + " => ret = " + ret.ToString() + " => Devuelve null!");
                }
            }
            catch (Exception ex)
            {
                retResume = null;
                LOG.Error("DEUnitOfWork.GenerateResume Excp Error: ", ex);
            }
            LOG.Debug("DEUnitOfWork.GenerateResume OUT!");
            return retResume;
        }

        private static int ProcessAssemblyPostGenerate(DeDocument inDeDoc, out object outDeDoc)
        {
            int ret = 0;
            string msgerr = "";
            outDeDoc = inDeDoc;
            IAssemblyPostGenerate APG = null;
            try
            {
                LOG.Debug("DEUnitOfWork.ProcessAssemblyPostGenerate IN...");
                if (inDeDoc != null && inDeDoc.DeTemplate != null && !string.IsNullOrEmpty(inDeDoc.DeTemplate.Assemblypostgenerate))
                {
                    try
                    {
                        LOG.Debug("DEUnitOfWork.ProcessAssemblyPostGenerate - Assembly = " +
                                            inDeDoc.DeTemplate.Assemblypostgenerate);
      
                        Assembly asmLogin = Assembly.Load(inDeDoc.DeTemplate.Assemblypostgenerate);
                        Type[] types = null;
                        LOG.Debug("DEUnitOfWork.ProcessAssemblyPostGenerate - GetTypes IN => " + asmLogin.FullName);
                        types = asmLogin.GetTypes();

                        //Intancia todos los tipos encontrados en la dll levantada
                        foreach (Type t in types)
                        {
                            try
                            {
                                msgerr = "";
                                object instance = null;
                                //ILoadBIRs oLoadBirs = null;
                                try
                                {
                                    LOG.Debug("DEUnitOfWork.ProcessAssemblyPostGenerate -  Activator.CreateInstance IN: " + 
                                                t.FullName);
                                    instance = Activator.CreateInstance(t);
                                    LOG.Debug("DEUnitOfWork.ProcessAssemblyPostGenerate Activator.CreateInstance IN: OK!");
                                }
                                catch (Exception exC)
                                {
                                    LOG.Warn("DEUnitOfWork.ProcessAssemblyPostGenerate Excp - Activator.CreateInstance(t)", exC);
                                    instance = null;
                                }

                                //Descarta los que no son implementacione sde IAction.
                                //Es similar en todos las implementaciones (IMark, IBusinessRule, etc).
                                if (instance != null)
                                {
                                    LOG.Debug("DEUnitOfWork.ProcessAssemblyPostGenerate - Instancio objeto IAssemblyPostGenerate..:");
                                    if (instance is IAssemblyPostGenerate)
                                    {
                                        LOG.Debug("DEUnitOfWork.ProcessAssemblyPostGenerate - Inicializo IAssemblyPostGenerate instanciado...");
                                        ret = ((IAssemblyPostGenerate)instance).
                                                    Initialize(Properties.Settings.Default.PathBaseBDEAssembly,  out msgerr);
                                        LOG.Debug("DEUnitOfWork.ProcessAssemblyPostGenerate - Initialize IAssemblyPostGenerate ret = " + ret.ToString());
                                        APG = (IAssemblyPostGenerate)instance;
                                        LOG.Debug("DEUnitOfWork.ProcessAssemblyPostGenerate - Objeto APG != null => " + (APG != null).ToString());
                                        ret = 0;
                                    }
                                }

                                if (APG !=null)
                                {
                                    ret = APG.DoAction(inDeDoc, out msgerr, out outDeDoc);
                                }
                            }
                            catch (Exception ex)
                            {
                                LOG.Error("ActionEngine.Initialize - Exc Initialize [" + ex.Message + "]");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ret = Errors.RET_ERR_CALLING_ASSEMBLY_POST_GENERATE;
                        LOG.Warn("DEUnitOfWork.ProcessAssemblyPostGenerate Executing " + inDeDoc.DeTemplate.Assemblypostgenerate 
                                  + "Ecxp Error: ", ex);
                    }
                } else
                {
                    if (inDeDoc == null || inDeDoc.DeTemplate != null)
                    {
                        LOG.Debug("DEUnitOfWork.ProcessAssemblyPostGenerate Parámetro DeDoc o DeTemplate nulo! => Sale sin hacer nada...");
                    } else
                    {
                        LOG.Debug("DEUnitOfWork.ProcessAssemblyPostGenerate Assemblypostgenerate NUlo para DeDoc.Trackid = " +
                                    inDeDoc.Trackid + " | TempllateId = " + inDeDoc.DeTemplate.Id.ToString());
                    } 
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("DEUnitOfWork.ProcessAssemblyPostGenerate Excp Error: " + ex.Message);
            }
            LOG.Debug("DEUnitOfWork.ProcessAssemblyPostGenerate OUT!");
            return ret;
        }

        private static int SendCallback(string urlcallback, string trackid)
        {
            int ret = 0;
            try
            {
                LOG.Debug("DEUnitOfWork.SendCallback IN...");
                RestClient Client = new RestClient();

                if (!urlcallback.EndsWith("/")) urlcallback += "/";
                LOG.Debug("DEUnitOfWork.SendCallback - UrlCallback = " + urlcallback + " - Param = " + trackid + "...");
                var request = new RestRequest(urlcallback, Method.POST);
                request.Timeout = 60000;
                request.AddParameter("trackid", trackid);

                IRestResponse response = Client.Execute(request);
                //var content = response.content;
                LOG.Debug("DEUnitOfWork.SendCallback - Respuesta de destino = " +
                            (response != null ? response.StatusCode.ToString() : "NULL"));
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("DEUnitOfWork.SendCallback Error: " + ex.Message);
            }
            LOG.Debug("DEUnitOfWork.SendCallback OUT!");
            return ret;
            
        }

        private static DeSignatory FillDeSignatoryFromBPResponse(DeSignatory item, BpTxDataResponse oBpTxDataResponse)
        {
            DeSignatory itemOut = item;
            try
            {
                if (oBpTxDataResponse.code == 0)
                {
                    itemOut.Status = Constant.DESIGNATORE_STATUS_COMPLETED;

                    if (!string.IsNullOrEmpty(oBpTxDataResponse.personaldata.birthdate))
                    {
                        itemOut.Birthdate = DateTime.Parse(oBpTxDataResponse.personaldata.birthdate);
                    }
                    if (!string.IsNullOrEmpty(oBpTxDataResponse.personaldata.documentexpirationdate))
                    {
                        itemOut.Expirationdate = DateTime.Parse(oBpTxDataResponse.personaldata.documentexpirationdate);
                    }
                    itemOut.Georef = oBpTxDataResponse.georef;
                    itemOut.Lastmodify = DateTime.Now;
                    itemOut.Lastname = oBpTxDataResponse.personaldata.patherlastname;
                    itemOut.Map = Utils.GetMap(itemOut.Georef); //TODO Add Utils de generacion de mapa desde NV
                    itemOut.Nacionality = oBpTxDataResponse.personaldata.nationality;
                    itemOut.Name = oBpTxDataResponse.personaldata.name;
                    itemOut.Serial = oBpTxDataResponse.personaldata.documentseriesnumber;
                    itemOut.Sex = oBpTxDataResponse.personaldata.sex;
                    itemOut.Status = Constant.DESIGNATORE_STATUS_COMPLETED;
                    itemOut.Taxidcompany = oBpTxDataResponse.taxidcompany;
                    itemOut.Videourl = oBpTxDataResponse.videourl;

                    itemOut.Score = oBpTxDataResponse.score;
                    itemOut.Threshold = oBpTxDataResponse.threshold;
                    itemOut.Verifyresult = oBpTxDataResponse.result == 1 ? "POSITIVO" : "NEGATIVO";
                    //itemOut.Workstationid = oBpTxDataResponse.personaldata.  //TODO - AGREGAR...
                    if (oBpTxDataResponse.Samples != null && oBpTxDataResponse.Samples.Count > 0)
                    {
                        itemOut.DeSignatoryEvidences = new List<DeSignatoryEvidences>();
                        DeSignatoryEvidences aux = null;
                        foreach (ImageSample itemImage in oBpTxDataResponse.Samples)
                        {
                            aux = new DeSignatoryEvidences(itemOut, itemImage.code, itemImage.data,
                                                           (Database.TypeImage)itemImage.typeimage,
                                                           (Database.TypeImageFormat)itemImage.typeimageformat);
                            itemOut.DeSignatoryEvidences.Add(aux);
                        }
                    }
                } else
                {
                    itemOut.Status = oBpTxDataResponse.code;
                }
            }
            catch (Exception ex)
            {
                itemOut = item;
                LOG.Error(" Error: " + ex.Message);
            }
            return itemOut;
        }

        /// <summary>
        /// Este metodo es fundamental para el Qrchestator.
        /// Es el que realiza el mapping entre los datos definidos como tags y valores recibidos en los
        /// diferentes procesos que se hayan definido, ya sea en DeSignatory, DeDocumebnt o Evidencias.
        /// Se utiliza la tabla DeTemplateTag como guia, usando la columna translate y source como guia.
        /// Source = Fuente desde donde se saca el valor:
        ///     * Tablas: detx | dedocument | designatory | designatoryevidences | dedocumenttag (given tags)
        ///     * form => Formulario: Esto es una evidencia almacenada en DeSignatureEvidences
        ///     * custom => Reservado por si se necesita hacer algo en runtime especial (estilo connectors)
        /// Translate: El nombre de la columna o key a utilizar desde la fuente.
        /// </summary>
        /// <param name="trackid"></param>
        /// <param name="tagsprocessed"></param>
        /// <returns></returns>
        private static int GenerateTagIn(string trackid, out List<Tag> tagsIn, out List<DeDocumentTag> dbTagList,
                                         out int _idcompany, out int _idtemplate, bool toResume = false)
        {
            tagsIn = new List<Tag>();
            dbTagList = new List<DeDocumentTag>();
            int ret = Errors.RET_OK;
            _idcompany = 0;
            _idtemplate = 0;
            try
            {
                /*
                    1.- Recupero DeDocument con todos los datos de Sources necesarios 
                        1.1.- La lista de tags desde DeTemplateTags
                    2.- Identifico todas las fuentes posibles (DeTx, Form, etc)
                    3.- Inicio a recorrer los Tags desde DeTemplateTags y por cada uno, se va compleando con e, valor 
                    de acuerdo al translate configurado.
                        3.1.- Agrego a la lista
                    4.- Devuelvo lista de tags procesados.
                */
                LOG.Debug("DEUnitOfWork.GenerateTagIn IN...");
                //1.- Recupero DeDocument con todos los datos de Sources necesarios
                //dedocument
                LOG.Debug("DEUnitOfWork.GenerateTagIn Busco DeDocument...");
                DeDocument _DE_DOC = AdministradorDeDocument.BuscarDocumentosByTrackId(trackid, true);
                //1.1.- La lista de tags desde DeTemplateTags
                //  1.1.1.- Si es toResume = true => Tomo tags desde template de resumen de la cantidad de firmantes
                //  1.1.2.- Sino tomo los tags del doc
                LOG.Debug("DEUnitOfWork.GenerateTagIn Busco Lista Tags desde template...");
                IList<DeTemplateTag> _DE_TEMPLATE_TAG_LIST = null;
                if (toResume)
                {
                    if (_DE_DOC.DeSignatory != null)
                    {
                        if (!_DE_DOC.DeTemplate.Resumedetemplateid.HasValue ||
                            _DE_DOC.DeTemplate.Resumedetemplateid.Value == 0)
                        {
                            _DE_TEMPLATE_TAG_LIST = AdministradorDeTemplate
                                                .SearchDeTemplateById(Properties.Settings.Default.DeTemplateIdDefault)
                                                    .DeTemplateTag;
                            _idcompany = Properties.Settings.Default.DeCompanyDeTemplateIdDefault;
                            _idtemplate = Properties.Settings.Default.DeTemplateIdDefault;
                        } else
                        {
                            _DE_TEMPLATE_TAG_LIST = AdministradorDeTemplate
                                                .SearchDeTemplateById(_DE_DOC.DeTemplate.Resumedetemplateid.Value)
                                                    .DeTemplateTag;
                            _idcompany = _DE_DOC.Company.Id;
                            _idtemplate = _DE_DOC.DeTemplate.Resumedetemplateid.Value;
                        }
                    }
                    else
                    {
                        _DE_TEMPLATE_TAG_LIST = _DE_DOC.DeTemplate.DeTemplateTag;
                        _idcompany = _DE_DOC.Company.Id;
                        _idtemplate = _DE_DOC.DeTemplate.Id;
                    }
                }
                else
                {
                    _DE_TEMPLATE_TAG_LIST = _DE_DOC.DeTemplate.DeTemplateTag;
                    _idcompany = _DE_DOC.Company.Id;
                    _idtemplate = _DE_DOC.DeTemplate.Id;
                }


                //2.- Identifico todas las fuentes posibles (DeTx, Form, etc)
                //designatory
                LOG.Debug("DEUnitOfWork.GenerateTagIn Busco Lista de Firmantes...");
                IList<DeSignatory> _DE_SIGNATORY_LIST = _DE_DOC.DeSignatory;
                //detx 
                LOG.Debug("DEUnitOfWork.GenerateTagIn Busco DeTx...");
                DeTx _DE_TX = GetTxCreation(_DE_DOC.DeTx);
                //designatoryevidencesy form se obtiene por cada signatory

                //3.- Inicio a recorrer los Tags desde DeTemplateTags y por cada uno, se va compleando con e, valor
                //    de acuerdo al translate configurado.
                Tag tagaux = null;
                DeDocumentTag dbTag;

                LOG.Debug("DEUnitOfWork.GenerateTagIn Busco Proceso cada tag...");
                foreach (DeTemplateTag item in _DE_TEMPLATE_TAG_LIST)
                {
                    tagaux = new Tag();
                    tagaux.IdTag = item.Name;
                    tagaux.Parameters = item.Parameters;
                    tagaux.Type = item.Type.ToString();
                    switch (item.Source)
                    {
                        case "dedocumenttag":
                            tagaux.Value = GetValueFromList(_DE_DOC.DeDocumentTag, item.Translate, item.Signatoryorder, item.Format);
                            break;
                        case "company":
                            tagaux.Value = GetValue(_DE_DOC.Company, item.Translate, item.Signatoryorder, item.Format);
                            break;
                        case "dedocument":
                            tagaux.Value = GetValue(_DE_DOC, item.Translate, item.Signatoryorder, item.Format);
                            break;
                        case "detx":
                            tagaux.Value = GetValue(_DE_TX, item.Translate, item.Signatoryorder, item.Format);
                            break;
                        case "designatory":
                            DeSignatory DeSig = GetSignatoryFromList(_DE_SIGNATORY_LIST, item.Signatoryorder);
                            tagaux.Value = GetValue(DeSig, item.Translate, item.Signatoryorder, item.Format);
                            break;
                        case "designatoryevidences":
                            IList<DeSignatoryEvidences> DeSigEv = GetSignatoryEvidencesFromList(_DE_SIGNATORY_LIST, item.Signatoryorder);
                            tagaux.Value = GetValue(DeSigEv, item.Translate, item.Signatoryorder, item.Format);
                            break;
                        case "form":
                            string form = GetFormFromList(_DE_SIGNATORY_LIST, item.Signatoryorder);
                            Dictionary<string,string> dForm = JsonConvert.DeserializeObject<Dictionary<string, string>>(form);
                            tagaux.Value = GetValue(dForm, item.Translate, item.Signatoryorder, item.Format);
                            break;
                        case "custom":
                            break;
                        default: //dedocument
                            tagaux.Value = null;
                            break;
                    }
                    tagsIn.Add(tagaux);
                    dbTag = new DeDocumentTag(_DE_DOC, tagaux.IdTag, tagaux.Value, item.Type, 0, DateTime.Now);
                    dbTagList.Add(dbTag);
                    LOG.Debug("DEUnitOfWork.GenerateTagIn - Added " + item.Translate + " => Value=" +
                                      (string.IsNullOrEmpty(tagaux.Value) ? "Null" : tagaux.Value));
                }
                LOG.Debug("DEUnitOfWork.GenerateTagIn - Update Security Code...");
                foreach (Tag item in tagsIn)
                {
                    if (item.IdTag.Equals("#securitycode#"))
                    {
                        item.Value = GenerateSecurityCode(dbTagList);
                        break;
                    }
                }
                LOG.Debug("DEUnitOfWork.GenerateTagIn - #Q Tags Added = " + 
                                (tagsIn!=null ? tagsIn.Count.ToString() : "NULL"));
            }
            catch (Exception ex)
            {
                ret = Errors.RET_ERR_UNKKNOW;
                LOG.Error("DEUnitOfWork.GenerateTagIn Excp Error: ", ex);
            }
            LOG.Debug("DEUnitOfWork.GenerateTagIn OUT! ret = " + ret.ToString());
            return ret;
        }

        private static string GetValueFromList(IList<DeDocumentTag> deDocumentTag, string translate, 
                                               int signatoryorder, string format)
        {
            string sValue = null;
            try
            {
                foreach (DeDocumentTag item in deDocumentTag)
                {
                    if (item.Name.Equals(translate))
                    {
                        sValue = item.Value;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                sValue = null;
                LOG.Error("DEUnitOfWork.GetValueFromList Excp Error:", ex);
            }
            return sValue;
        }

        private static string GetFormFromList(IList<DeSignatory> dE_SIGNATORY_LIST, int signatoryorder)
        {
            string sForm = null;
            try
            {
                DeSignatory sig = GetSignatoryFromList(dE_SIGNATORY_LIST, signatoryorder);
                if (sig != null && sig.DeSignatoryEvidences != null)
                {
                    foreach (DeSignatoryEvidences item in sig.DeSignatoryEvidences)
                    {
                        if (item.Name.Equals("form")) return item.Value;
                    }
                   
                }
            }
            catch (Exception ex)
            {
                sForm = null;
                LOG.Error("DEUnitOfWork.GetFormFromList Excp Error:", ex);
            }
            return sForm;
        }

        private static IList<DeSignatoryEvidences> GetSignatoryEvidencesFromList(IList<DeSignatory> dE_SIGNATORY_LIST, 
                                                                                 int signatoryorder)
        {
            IList<DeSignatoryEvidences> listRet = null;
            try
            {
                DeSignatory sig = GetSignatoryFromList(dE_SIGNATORY_LIST, signatoryorder);
                if (sig != null && sig.DeSignatoryEvidences != null)
                {
                    return sig.DeSignatoryEvidences;
                }
            }
            catch (Exception ex)
            {
                listRet = null;
                LOG.Error("DEUnitOfWork.GetSignatoryEvidencesFromList Excp Error:", ex);
            }
            return listRet;
        }

        private static DeSignatory GetSignatoryFromList(IList<DeSignatory> dE_SIGNATORY_LIST, int signatoryorder)
        {
            DeSignatory dRet = null;
            try
            {
                foreach (DeSignatory item in dE_SIGNATORY_LIST)
                {
                    if (item.Signatoryorder == signatoryorder) return item;
                }
            }
            catch (Exception ex)
            {
                dRet = null;
                LOG.Error("DEUnitOfWork.GetSignatoryFromList Excp Error:", ex);
            }
            return dRet;
        }

        /// <summary>
        /// Retorna el valor de la propiedad dada su nombre. Esto sirve para conseguir el valor del tag 
        /// configurado en el DeTemplateTag, en diseño. 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="translate"></param>
        /// <param name="signatoryorder"></param>
        /// <returns></returns>
        private static string GetValue(object source, string translate, int signatoryorder = 0, string format = null)
        {
            string sret = null;
            try
            {
                //Si es Objeto del tipo DeDocument o DeTx o DeSignatory => Retorno valor de variable indicada en translate
                //porque son clases nomas.
                    if (source is DeDocument || source is DeTx || source is DeSignatory || source is Database.Company)
                {
                    //Comprobacion de datos compuestos
                    if (translate.Equals("_COMPOSED_COMPLETENAME"))
                    {
                        string name = libs.Utils.GetValueOfProperty(source, "Name");
                        string lastname = libs.Utils.GetValueOfProperty(source, "Lastname");
                        name = string.IsNullOrEmpty(name) ? "" : name;
                        lastname = string.IsNullOrEmpty(lastname) ? "" : lastname;
                        sret = name + " " + lastname;
                    }
                    else if (translate.Equals("_ESTADO_DOCUMENT"))
                    {
                        sret = Constant.GetNameStatusDeDocument(Convert.ToInt32(libs.Utils.GetValueOfProperty(source, "Status")));
                    }
                    else if (translate.Equals("_ESTADO_FIRMANTE"))
                    {
                        sret = Constant.GetNameStatusDeSignatory(Convert.ToInt32(libs.Utils.GetValueOfProperty(source, "Status")));
                    }
                    else if (translate.Equals("_TYPEID_VALUEID"))
                    {
                        string typeid = libs.Utils.GetValueOfProperty(source, "Typeid");
                        string valueid = libs.Utils.GetValueOfProperty(source, "Valueid");
                        sret = (string.IsNullOrEmpty(typeid)?"":typeid) + " " +
                                (string.IsNullOrEmpty(valueid) ? "" : valueid);
                    }
                    else
                    {
                        sret = libs.Utils.GetValueOfProperty(source, translate, format);
                    }
                }

                //Si es lista de Evidencias => Busco en la lista el que coincida con su nombre
                if (source is IList<DeSignatoryEvidences>)
                {
                    if (translate.Equals("_EVIDENCIAS_DETAILS"))
                    {
                        bool isFirst = true;
                        foreach (DeSignatoryEvidences item in ((IList<DeSignatoryEvidences>)source))
                        {
                            if (isFirst)
                            {
                                isFirst = false;
                                sret = "[OK] " + item.Name + (item.Createdate.HasValue ?
                                                       ":" + item.Createdate.Value.ToString("dd/MM/yyyy HH:mm:ss") : ""); 
                            } else
                            {
                                sret += ", [OK] " + item.Name + (item.Createdate.HasValue ?
                                                       ": " + item.Createdate.Value.ToString("dd/MM/yyyy HH:mm:ss") : "");
                            }
                        }
                    }
                    else
                    {
                        foreach (DeSignatoryEvidences item in ((IList<DeSignatoryEvidences>)source))
                        {
                            if (item.Name.Equals(translate))
                            {
                                sret = item.Value;
                                break;
                            }
                        }
                    }
                }

                //Si es formulario => Busco que exista clave que coincida con translate
                if (source is Dictionary<string, string>)
                {
                    if (((Dictionary<string, string>)source).ContainsKey(translate))
                    {
                        sret = ((Dictionary<string, string>)source)[translate];
                    }
                }
            }
            catch (Exception ex)
            {
                sret = null;
                LOG.Error("DEUnitOfWork.GetValue Excp Error: " + ex.Message);
            }
            if (string.IsNullOrEmpty(sret)) sret = "";
             return sret;
        }

       

        /// <summary>
        /// Genera Hash con datos de los tags para poder ser verificado.
        /// </summary>
        /// <param name="tags"></param>
        /// <returns></returns>
        private static string GenerateSecurityCode(IList<DeDocumentTag> tags)
        {
            string codeReturn = "Null";
            string aux = "";
            try
            {
                LOG.Debug("DEUnitOfWork.GenerateSecurityCode IN...");
                if (tags == null)
                {
                    LOG.Warn("DEUnitOfWork.GenerateSecurityCode Param tags=NULL => Sale con valor Null");
                    return codeReturn;
                }

                LOG.Debug("DEUnitOfWork.GenerateSecurityCode - Concatena " + tags.Count.ToString() + " tags...");
                foreach (DeDocumentTag item in tags)
                {
                    LOG.Debug("DEUnitOfWork.GenerateSecurityCode - Concatena " + item.Name + "...");
                    aux = aux + (string.IsNullOrEmpty(item.Value) ? "" : item.Value);
                }

                LOG.Debug("DEUnitOfWork.GenerateSecurityCode - GetBytes...");
                byte[] origen = System.Text.ASCIIEncoding.ASCII.GetBytes(aux);

                LOG.Debug("DEUnitOfWork.GenerateSecurityCode - Calcula SHA256...");
                byte[] result = ((HashAlgorithm)CryptoConfig.CreateFromName("SHA256")).ComputeHash(origen);
                //Bio.Core.pki.hash.BKHash.Hash(origen, "SHA256");

                codeReturn = Convert.ToBase64String(result);
            }
            catch (Exception ex)
            {
                codeReturn = "Null";
                LOG.Error("DEUnitOfWork.GenerateSecurityCode Error: " + ex.Message);
            }
            LOG.Debug("DEUnitOfWork.GenerateSecurityCode - OUT! - SecurityCode => " + codeReturn);
            return codeReturn;
        }

        /// <summary>
        /// Dada una lista de Tx asociadas a un DE, se obtiene la relacionada con su creacion
        /// es decir Actionid = 1
        /// </summary>
        /// <param name="deTxlist"></param>
        /// <returns></returns>
        private static DeTx GetTxCreation(IList<DeTx> deTxlist)
        {
            DeTx tx = null;
            try
            {
                LOG.Debug("DEUnitOfWork.GetTxCreation IN...");
                foreach (DeTx item in deTxlist)
                {
                    if (item.Actionid == 1)
                    {
                        tx = item;
                        LOG.Debug("DEUnitOfWork.GetTxCreation - Tx Id Seleccionado = " + item.Id.ToString());
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                tx = null;
                LOG.Error("DEUnitOfWork.GetTxCreation Excp Error:", ex);
            }
            LOG.Debug("DEUnitOfWork.GetTxCreation OUT! Ret != null => " + (tx!=null).ToString());
            return tx;
        }

        private static int GetResultCodeFromCreate(IList<DeTx> deTxList)
        {
            int ret = 0;
            try
            {
                LOG.Debug("DEUnitOfWork.GetDe IN...");
                if (deTxList != null)
                {
                    foreach (DeTx item in deTxList)
                    {
                        if (item.Actionid == 1) //Es la TX de la creacion del DE
                        {
                            ret = item.Resultcode;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error(" Error: " + ex.Message);
            }
            LOG.Debug("DEUnitOfWork.GetDe OUT! ret = " + ret.ToString());
            return ret;
        }

        private static DeDocument DeleteCycleForJason(DeDocument deDoc)
        {
            DeDocument deOut = deDoc;
            try
            {
                LOG.Debug("DEUnitOfWork.DeleteCycleForJason IN...");
                deOut.DeTemplate = null;
                deOut.DeTx = null;
                deOut.Company = null;
                foreach (DeSignatory item in deOut.DeSignatory)
                {
                    item.DeDocument = null;
                    if (item.DeSignatoryEvidences != null)
                    {
                        foreach (DeSignatoryEvidences iteme in item.DeSignatoryEvidences)
                        {
                            iteme.DeSignatory = null;
                        }
                    }
                }
                foreach (DeDocumentTag item in deOut.DeDocumentTag)
                {
                    item.DeDocument = null;
                }
            }
            catch (Exception ex)
            {
                deOut = null;
                LOG.Error("DEUnitOfWork.DeleteCycleForJason Excp Error: " + ex.Message);
            }
            LOG.Debug("DEUnitOfWork.DeleteCycleForJason OUT!");
            return deOut;
        }

        /// <summary>
        /// Busca el id de workflow seleccionado para este firmante por el orden
        /// </summary>
        /// <param name="templateid"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        private static int GetWorkflowidFromTemplateId(int templateid, int order, out DeCompanyWorkflow dew)
        {
            int wi = 0;
            dew = null;
            try
            {
                LOG.Debug("DEUnitOfWork.GetWorkflowidFromTemplateId IN...");
                DeTemplate det = AdministradorDeTemplate.SearchDeTemplateById(templateid);
                if (det != null)
                {
                    foreach (DeTemplateSignatory item in det.DeTemplateSignatory)
                    {
                        if (item.Signatoryorder == order)
                        {
                            wi = item.Workflowid;
                            dew = AdministradorCompanyWorkflow.BuscarCompanyWokflowById(wi);
                            LOG.Debug("DEUnitOfWork.GetWorkflowidFromTemplateId - Encontrado wi = " + wi.ToString() +
                                        " para firmante order = " + order.ToString() + " en templateid = "
                                        + templateid.ToString());
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                wi = 0;
                LOG.Error("DEUnitOfWork.GetWorkflowidFromTemplateId Error: " + ex.Message);
            }
            LOG.Debug("DEUnitOfWork.GetWorkflowidFromTemplateId OUT! ret = " + wi.ToString());
            return wi;
        }

        internal static int PrintCompaniesEnables()
        {
            int ret = 0;
            try
            {
                IList<Database.Company> CompanyList = AdministradorCompany.ListCompanies(true);
                if (CompanyList == null)
                {
                    ret = -5001;
                } else
                {
                    string aux;
                    LOG.Info("Cantidad de Compañias en el sistema = " + CompanyList.Count.ToString());
                    foreach (Database.Company item in CompanyList)
                    {
                        aux = item.Endate.HasValue ?
                            "Deshabilitada desde " + item.Endate.Value.ToString("dd/MM/yyyy") :
                            "Habilitada";
                        LOG.Info("  >> Compañia = " + item.Id.ToString() + 
                                                        " - Rut: " + item.Rut + 
                                                        " - " + aux);
                        LOG.Info("  >> Templates Definidos = " + 
                            (item.DeTemplate == null || item.DeTemplate.Count == 0 ? "0" : item.DeTemplate.Count.ToString()));

                        foreach (DeTemplate item1 in item.DeTemplate)
                        {
                            LOG.Info("      >> Template = " + item1.Id.ToString() + 
                                                        " - Name: " + item1.Name +
                                                        " - Q Tags: " +
                                                        (item1.DeTemplateTag == null || item1.DeTemplateTag.Count == 0 ?
                                                         "0" : item1.DeTemplateTag.Count.ToString()) +
                                                         " - Q Firmantes: " +
                                                        (item1.DeTemplateSignatory == null || item1.DeTemplateSignatory.Count == 0 ?
                                                         "0" : item1.DeTemplateSignatory.Count.ToString()));

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }
    }
}