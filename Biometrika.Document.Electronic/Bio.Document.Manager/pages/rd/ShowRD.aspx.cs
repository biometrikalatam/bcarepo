using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using Biometrika.Document.Electronic.libs;
using Biometrika.Document.Electronic.rd;
using log4net;

namespace Biometrika.Document.Electronic.pages.rd
{
    public partial class ShowRD : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RDHelper));
        
        protected void Page_Load(object sender, EventArgs e)
        {
            //int iddoc = Convert.ToInt32(Request["Id"]);
            //database.MdDocumentos doc = database.AdministradorMdDocumentos.BuscarDocumentoById(iddoc); 
            //CheckRecibo(doc.Recibo, "0"); //=> 0 es total

        }

        private void CheckRecibo(string reciboxml, string tipoverificacion)
        {
            bool exiteEnDB = false;
            bool fueModificado = false;

            try
            {
                //*  1.- Serializo param
                Rd orecibo = XmlUtils.DeserializeObject<Rd>(reciboxml);

                if (orecibo == null)
                {
                    Response.Redirect("Error.aspx?msg='El formato del Recibo Digital presentado es incorrecto. Chequ�elo y reintente...'");
                }

                ////*  2.- Check en BD 
                //RdRecibos dbRecibo = AdministradorRdRecibos.BuscarRdReciboByReciboId(orecibo.reciboID);

                //if (dbRecibo != null)
                //{
                //    exiteEnDB = true;
                //}

                //// Si se debe chequear por igualdad => Sigo
                //if (exiteEnDB && Convert.ToInt32(tipoverificacion) == 0)
                //{
                //    XmlDocument rdInNV = new XmlDocument();
                //    rdInNV.PreserveWhitespace = false;
                //    rdInNV.LoadXml(dbRecibo.Reciboxml);

                //    XmlDocument rdInParam = new XmlDocument();
                //    rdInParam.PreserveWhitespace = false;
                //    rdInParam.LoadXml(reciboxml);

                //    fueModificado = !(rdInNV.OuterXml.Equals(rdInParam.OuterXml));
                //}
                string msgResultOrErr = null;
                int ret = RDHelper.VerificaRecibo(reciboxml, out msgResultOrErr);
                int result = 0;

                if (ret == 0)
                {
                    result = RDHelper.ParseResultReceipt(msgResultOrErr);

                    switch (result)
                    {
                        case 1:
                            labResBD.ForeColor = Color.Green;
                            labResBD.Text = "El Recibo Digital presentado EXISTE en Biometrika Notario Virtual";
                            labResFirma.ForeColor = Color.Green;
                            labResFirma.Text = "El Recibo Digital presentado NO fue modificado";
                            imgVerifyOK.Visible = false;
                            imgVerifyNOOK.Visible = true;                
                            break;
                        case 2:
                            labResBD.ForeColor = Color.Green;
                            labResBD.Text = "El Recibo Digital presentado EXISTE en Biometrika Notario Virtual";
                            labResFirma.ForeColor = Color.Green;
                            labResFirma.Text = "El Recibo Digital presentado NO fue modificado";
                            imgVerifyOK.Visible = true;
                            imgVerifyNOOK.Visible = false;  
                            break;
                        default:
                            labResBD.ForeColor = Color.Green;
                            labResBD.Text = "El Recibo Digital presentado EXISTE en Biometrika Notario Virtual";
                            labResFirma.Visible = false;
                            imgVerifyOK.Visible = false;
                            imgVerifyNOOK.Visible = true; 
                            break;
                    }
                }
                else if (ret == -30)
                {
                    labResBD.ForeColor = Color.Red;
                    labResBD.Text = "El Recibo Digital presentado NO EXISTE en Biometrika Notario Virtual";
                    labResFirma.Visible = false;
                    imgVerifyOK.Visible = false;
                    imgVerifyNOOK.Visible = true;
                }
                else
                {
                    labResBD.ForeColor = Color.Red;
                    labResBD.Text = "Error desconocido en la verificaci�n [" +
                                    msgResultOrErr + "]";
                    labResFirma.Visible = false;
                    imgVerifyOK.Visible = false;
                    imgVerifyNOOK.Visible = true; 
                }
                

                //Comienzo a mostar o esconder segun resultado obtenido
                //Lleno valores para mostrar
                foreach (RdExtensionItem ext in orecibo.Extensiones)
                {
                    if (ext.key == "NumeroDocumento")
                    {
                        this.labNrocontrato.Text = ext.value;
                    }
                    if (ext.key == "Cliente")
                    {
                        this.labRazonSocial.Text = ext.value;
                    }
                    if (ext.key == "FechaDocumento")
                    {
                        this.labFecha.Text = ext.value;
                    }
                    if (ext.key == "FechaVencimientoDocumento")
                    {
                        this.labFechaVto.Text = ext.value;
                    }
                    if (ext.key == "Vendedor")
                    {
                        this.labVendedor.Text = ext.value;
                    }
                    if (ext.key == "TipoDocumento")
                    {
                        this.labTipoDocumento.Text = ext.value;
                    }
                }

                //Lleno datos Certificado
                X509Certificate cert = null;
                foreach (object objc in orecibo.Signature[0].KeyInfo.Items)
                {
                    if (objc.GetType().ToString() == "Biometrika.Document.Electronic.rd.X509DataType")
                    {
                        X509DataType ocert = (X509DataType)objc;
                        cert = new X509Certificate((byte[])ocert.Items[0]);
                    }
                }
                labSubject.Text = cert.Subject;
                labSerialID.Text = cert.GetSerialNumberString();
                //Fechas
                bool errFechas = false;
                labFechaDesde.Text = cert.GetEffectiveDateString();
                DateTime dtDesde = DateTime.Parse(cert.GetEffectiveDateString());
                if (dtDesde > DateTime.Now)
                {
                    labFechaDesde.ForeColor = Color.Red;
                    errFechas = true;
                }
                labFechaHasta.Text = cert.GetExpirationDateString();
                DateTime dtHasta = DateTime.Parse(cert.GetExpirationDateString());
                if (dtHasta < DateTime.Now)
                {
                    labFechaHasta.ForeColor = Color.Red;
                    errFechas = true;
                }
                if (errFechas)
                {
                    imgFechaNOOK.Visible = true;
                    imgFechaOK.Visible = false;
                }
                else
                {
                    imgFechaNOOK.Visible = false;
                    imgFechaOK.Visible = true;
                }

                
            }
            catch (Exception ex)
            {
                log.Error("Error ShowRD.CheckRecibo", ex);
                Response.Redirect("Error.aspx?msg='CheckRDResult.CheckRecibo - " + ex.Message + "'");
            }
        }
    }
}
