<%@ Page Language="C#" MasterPageFile="~/MDMasterPageLogin.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Biometrika.Document.Electronic.pages.login.login" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <br />
    <br />
    <br />

    <table width="70%" align="center">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;<asp:Login ID="Login1" runat="server" CssClass="login" >
                    <TextBoxStyle CssClass="login_txtlabel" Font-Size="0.8em" />
                    <LoginButtonStyle CssClass="login_button" />
                    <LayoutTemplate>
                        <table border="0" cellpadding="4" cellspacing="0" style="border-collapse: collapse">
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0">
                                        <tr>
                                            <td align="center" colspan="2" style="font-weight: bold; font-size: 0.9em; color: white;
                                                background-color: #356734">
                                                Iniciar sesi�n</td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <br />
                                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Nombre de usuario:&nbsp;&nbsp;</asp:Label></td>
                                            <td>
                                                <br />
                                                <asp:TextBox ID="UserName" runat="server" Font-Size="0.8em"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                    ErrorMessage="El nombre de usuario es obligatorio." ToolTip="El nombre de usuario es obligatorio."
                                                    ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <br />
                                                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Contrase�a:&nbsp;&nbsp;</asp:Label></td>
                                            <td>
                                                <br />
                                                <asp:TextBox ID="Password" runat="server" Font-Size="0.8em" TextMode="Password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                    ErrorMessage="La contrase�a es obligatoria." ToolTip="La contrase�a es obligatoria."
                                                    ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                                <asp:CheckBox ID="RememberMe" runat="server" Text="Record�rmelo la pr�xima vez." />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="color: red">
                                                <br />
                                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" colspan="2" style="height: 17px">
                                                <br />
                                                <asp:Button ID="LoginButton" runat="server" BackColor="#356734" BorderColor="#CCCCCC"
                                                    BorderStyle="Groove" BorderWidth="3px" CommandName="Login" Font-Names="Verdana"
                                                    Font-Size="1.0em" ForeColor="White" Text="Inicio de sesi�n" ValidationGroup="Login1" OnClick="LoginButton_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
                    <TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" Font-Size="0.9em" ForeColor="White" />
                </asp:Login>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
