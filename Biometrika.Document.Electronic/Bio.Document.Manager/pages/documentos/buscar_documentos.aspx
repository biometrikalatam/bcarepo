<%@ Page Language="C#" MasterPageFile="~/MDMasterPage.master" AutoEventWireup="true" CodeBehind="buscar_documentos.aspx.cs" Inherits="Biometrika.Document.Electronic.pages.documentos.buscar_documentos" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <br />
    <table width="80%" align="center" border="0">
        <tr>
            <td align="left" valign="middle" colspan="4" class="titleform">
                Ingrese Filtros
            </td>
        </tr>
        <tr>
        <td align="left" valign="middle" colspan="4">
                <br />
               
            </td>
        </tr>
        <tr>
            <td align="right" valign="middle" class="labelform" style="width:15%">
                Fecha Desde :
                <br /><br />
            </td>
            <td align="left" valign="middle" style="width:35%">
                <asp:TextBox ID="labFechaDesde" runat="server" CssClass="textboxform160"></asp:TextBox>&nbsp;<br /><br />
            </td>
            <td align="right" valign="middle" class="labelform" style="width:15%">
                Fecha Hasta : <br /><br />
            </td>
            <td align="left" valign="middle" style="width:35%">
                <asp:TextBox ID="labFechaHasta" runat="server" CssClass="textboxform160"></asp:TextBox> <br /><br /></td>                
        </tr>
        <tr>
            <td align="right" valign="middle" class="labelform" style="width:15%">
                Cliente :
            </td>
            <td align="left" valign="middle" style="width:35%"><asp:DropDownList ID="cboClientes" runat="server" CssClass="textboxform160">
                <asp:ListItem Value="0">Seleccione Cliente...</asp:ListItem>
            </asp:DropDownList></td>
            <td align="right" valign="middle" class="labelform" style="width:15%">
                Tipo Contrato :
            </td>
            <td align="left" valign="middle" style="width:35%">
                <asp:DropDownList ID="cboTipoDoc" runat="server" CssClass="textboxform160">
                    <asp:ListItem Value="0">Seleccione Tipo de documento...</asp:ListItem>
                </asp:DropDownList></td>                
        </tr>
        <tr>
            <td align="right" valign="middle" colspan="4">
                <br />
                <asp:Button ID="btnBuscar" runat="server" Text="Buscar Documentos" CssClass="boton1140" OnClick="btnBuscar_Click" /></td>
        </tr>        
    </table>    

</asp:Content>
