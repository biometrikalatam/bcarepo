<%@ Page Language="C#" MasterPageFile="~/MDMasterPage.master" AutoEventWireup="true" CodeBehind="resultado_documentos.aspx.cs" Inherits="Biometrika.Document.Electronic.pages.documentos.resultado_documentos" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

<script type="text/javascript" language="javascript">

function ShowRecibo(id) {

    window.open('../rd/ShowRD.aspx?id=' + id,'Recibo_Digital','top=100,left=100,toolbar=no,location=no,status= no,menubar=no,scrollbars=no,resizable=no,width=900,height=475');

}

function ShowDocumento(id) {

    window.open('download_documento.aspx?id=' + id,'Documento','width=1,height=1,top=2000,left=2000,scrollbars=no,titlebar=no,');

}

</script>

    <br />

    <table width="95%" align="center" border="0">
         <tr>
            <td align="right" valign="middle" colspan="4">
                <asp:Button ID="btnBuscar" runat="server" Text="Volver" CssClass="boton1140" OnClick="btnBuscar_Click" /><br />
            </td>
        </tr>   
        <tr>
            <td align="left" valign="middle" colspan="4" class="titleform">
                Documentos Encontrados
            </td>
        </tr>
        <tr>
            <td align="left" valign="middle" colspan="4">
                <br />
                <asp:GridView ID="GridView1" SkinID="GridView1" runat="server" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound">
                    <Columns>
                        
                        <asp:BoundField DataField="Id" HeaderText="Id" />
                        <asp:BoundField DataField="Iddocumento" HeaderText="Nro. Doc." />
                        <asp:TemplateField HeaderText="Tipo Documento">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("MdTipodocumentos.Descripcion") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("MdTipodocumentos.Descripcion") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Fecha" HeaderText="Fecha" />
                        <asp:TemplateField HeaderText="Cliente">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("MdClientes.RazonSocial") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("MdClientes.RazonSocial") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("MdVendedores.Apellidopaterno") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("MdVendedores.Apellidopaterno") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Fechavencimiento" HeaderText="Fecha Vto." />
                        <asp:TemplateField>
                          <ItemTemplate>
                            <a href="javascript:ShowDocumento('<%# Eval("Id")%>')">
                                <img alt="Ver Documento..." style="border:0;" src="../../images/pdf_small.png" /> 
                             </a>
                          </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                          <ItemTemplate>
                            <a href="javascript:ShowRecibo('<%# Eval("Id")%>')">
                                <img alt="Recibo Digital..." style="border:0;" src="../../images/medal_gold_2.png" /> 
                             </a>
                          </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
               
            </td>
        </tr>     
        <tr>
            <td align="left" valign="middle" colspan="4">
              
            </td>
        </tr>     
    </table>  
    <!-- 
    <asp:CommandField ButtonType="Image" EditImageUrl="~/images/layout_edit.png" ShowEditButton="True" />
    <asp:CommandField ButtonType="Image" DeleteImageUrl="~/images/layout_delete.png"
        ShowDeleteButton="True" />
    -->  
</asp:Content>
