using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using Biometrika.Document.Electronic.database;

namespace Biometrika.Document.Electronic.pages.documentos
{
    public partial class resultado_documentos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MDMasterPage.SetMasterPage(Master, Request, null, null,
               "Usuario Conectado : " + (string)Session["userconnected"] + "   ", "Resultado Busqueda Documentos",
               "MD > Documentos > Resultado B�squeda Documentos");
            
            string fechadesde = Request.Form["ctl00$ContentPlaceHolderMain$labFechaDesde"];
            DateTime dtfechadesde = libs.Utils.GetDateTime(fechadesde, true);
            string fechahasta = Request.Form["ctl00$ContentPlaceHolderMain$labFechaHasta"];
            DateTime dtfechahasta = libs.Utils.GetDateTime(fechahasta, false);
            string cliente = Request.Form["ctl00$ContentPlaceHolderMain$cboClientes"];
            int idcliente = 0;
            if (!String.IsNullOrEmpty(cliente)) idcliente = Convert.ToInt32(cliente);
            string tipodoc = Request.Form["ctl00$ContentPlaceHolderMain$cboTipoDoc"];
            int idtipodoc = 0;
            if (!String.IsNullOrEmpty(tipodoc)) idtipodoc = Convert.ToInt32(tipodoc);

            //IList<MdDocumentos> listaDocumentos =
            //        AdministradorMdDocumentos
            //            .BuscarDocumentosByFilter(dtfechadesde, dtfechahasta, idcliente, idtipodoc);
            
            //GridView1.DataSource = listaDocumentos;
            GridView1.DataBind();
            
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridViewRow selectedRow = null;
            string id = null;
            int index = Convert.ToInt32(e.CommandArgument); // -1;

            selectedRow = GridView1.Rows[index];
            TableCell doccell = selectedRow.Cells[2];
            id = doccell.Text;

            if (e.CommandName.ToUpper() == "MODIFICAR" || e.CommandName.ToUpper() == "EDIT")
                Response.Redirect("Editar_Documento.aspx?" + id.ToString());
            else if (e.CommandName.ToUpper() == "ELIMINAR" || e.CommandName.ToUpper() == "DELETE")
            {
               

            }
            else if (e.CommandName.ToUpper() == "VERRECIBO")
            {
               //Button btn = (Button)sender;
               //btn.OnClientClick = "window.open('~/pages/documentos/recibo_digital.aspx','',''); return false;";
                //Response.Redirect("~/pages/documentos/open_recibo_digital.aspx",true);
               
            }
            else if (e.CommandName.ToUpper() == "VERDOCUMENTO")
            {


            }

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string id = e.Row.Cells[2].Text;
            
            string showPopup = "ShowRecibo('" + id + "');";
            e.Row.Attributes.Add("onmouseclick","Javascript:" + showPopup);
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Response.Redirect("buscar_documentos.aspx");
        }
    }
}
