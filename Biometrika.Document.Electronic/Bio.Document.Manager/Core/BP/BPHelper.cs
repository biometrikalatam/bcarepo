﻿using System;
using System.Text;
using Biometrika.Document.Electronic.Api;
using Biometrika.Document.Electronic.Database;
using log4net;
using Newtonsoft.Json;
using RestSharp;

namespace Biometrika.Document.Electronic.Core.BP
{
    public class BPHelper
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(BPHelper));

        /// <summary>
        /// Verifica que el DE deba verificar identidad por configuracion, y si es asi, verifica
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static bool VerifyCredential(XmlParamIn oXmlIn, out string msgerr)
        {
            bool ret = false;
            msgerr = null;
            try
            {

            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("BPHelper.VerifyCredential Error - " + ex.Message);
            }
            return ret; 
        }

#region API Rest x BPWeb

        public static int TxCreateSigner(string trackidCI, Api.Model.CreateDeModel pin, Database.Company company,
                                         Database.DeSignatory signatory, string workflowname,   
                                         out string msgerr,  out string listMails, out string trackidbp, 
                                         out BPTxCreateR bptxresponse)
        {
            int ret = 0;
            msgerr = null;
            BPResponseTxCreate bpr = null;
            bptxresponse = null;
            trackidbp = null;
            listMails = null;
            try
            {
                LOG.Debug("BPHelper.TxCreateSigner IN...");
                //Added 12-08-2020
                //Chequeo que la persona este enrolada. 
                //  - Si no lo está => Genero enroll combined
                //  - Si está => Genero verify como pide los parametros del OB o Auth segun Company
                BPUserEnrolledR bpresponse;
                LOG.Debug("BPHelper.TxCreateSigner - Ingresando a CheckIdentityEnrolled to BP con => " +
                          company.Companyidbp + "/" + signatory.Typeid + "/" + signatory.Valueid);
                ret = CheckIdentityEnrolled(company.Companyidbp, signatory.Typeid, signatory.Valueid, company.Accessnamebp,
                                            company.Secretkeybp, out bpresponse, out msgerr);
                LOG.Debug("BPHelper.TxCreateSigner - CheckIdentityEnrolled ret = " + ret);
                //Si no hubo error o bien no encontro que este enrolado => Enrolo
                if ((ret == 0 || ret == Errors.RET_ERR_DONT_CHECK_ENROLLED)) // && bpresponse != null)
                {
                    LOG.Debug("BPHelper.TxCreateSigner - Ingresa a crear TX segun parámetros y workflow pedido = " +
                        company.Id + "/" + workflowname);
                    //Si no hubo error analizo la informacion retornada para determinar los pasos a hacer 
                    // en conjunto con el workflow 
                    Database.DeCompanyWorkflow cw = Database.AdministradorCompanyWorkflow.
                                        BuscarCompanyWokflowByName(company.Id, workflowname);

                    if (cw == null)
                    {
                        LOG.Warn("BPHelper.TxCreateSigner - Workflow " + company.Id + "/" + workflowname + " no encontrado!. Sale con error " +
                                 Errors.RET_ERR_WORKFLOW_DONT_EXIST);
                        return Errors.RET_ERR_WORKFLOW_DONT_EXIST;
                    }
                    listMails = cw.Receiverlist; //Retorno lista de mails a enviar notify si se debe enviar

                    //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    //var client = new RestClient("https://qabpservice5.9.biometrikalatam.com/api/" + "txCreate");
                    LOG.Debug("BPHelper.TxCreateSigner - LLama a " + Properties.Settings.Default.BPWebUrlBase + "txCreate...");
                    var client = new RestClient(Properties.Settings.Default.BPWebUrlBase + "txCreate");
                    client.Timeout = Properties.Settings.Default.BPTimeout;
                    var request = new RestRequest(Method.POST);
                    client.UserAgent = "biometrika";
                    request.AddHeader("Authorization", "Basic " +
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(company.Accessnamebp + ":" +
                                                                            company.Secretkeybp)));
                    request.AddHeader("Content-Type", "application/json");
                    request.AddHeader("Accept", "application/json");
                    //int iaux = pin.company;
                    //pin.company = Properties.Settings.Default.CIBPCompanyId;
                    //string jsonParams = JsonConvert.SerializeObject(pin);
                    //pin.company = iaux;

                    BPTxCreate paramInBPTxCreate = new BPTxCreate();
                    paramInBPTxCreate.callbackurl = pin.callbackurl;
                    paramInBPTxCreate.company = company.Companyidbp;
                    paramInBPTxCreate.customertrackid = trackidCI;

                    paramInBPTxCreate.redirecturl = pin.redirecturl;
                    //paramInBPTxCreate.signerinclude = pin.signerInclude;
                    paramInBPTxCreate.theme = cw.Theme;
                    paramInBPTxCreate.threshold = cw.Threshold.HasValue ? cw.Threshold.Value : 50;
                    paramInBPTxCreate.typeid = signatory.Typeid;
                    paramInBPTxCreate.valueid = signatory.Valueid;
                    paramInBPTxCreate.taxidcompany = signatory.Taxidcompany;

                    //Check para ver que seteamos
                    //Pongo que no sea mandatorio porque es NV. Si no está enrolado en BP se coloca por default 1 
                    paramInBPTxCreate.onboardingmandatory = cw.Onboardingmandatory;
                    LOG.Debug("BPHelper.TxCreateSigner - Set paramInBPTxCreate.onboardingmandatory = " + paramInBPTxCreate.onboardingmandatory);
                    //Si esta enrolado => Auth sino enroll combined
                    if (bpresponse == null)
                    {
                        paramInBPTxCreate.onboardingtype = 1; //NO esta enrolado => Fuerzo Onboarding
                    }
                    else if (bpresponse.exist == 1 && bpresponse.isenrolledtv3dplus == 1) //&& bpresponse.isenrolledft
                    {
                        paramInBPTxCreate.onboardingtype = 0; //Ya esta enrolado => Pongo Auth
                    }
                    else
                    {
                        paramInBPTxCreate.onboardingtype = 1; //NO esta enrolado => Fuerzo Onboarding
                    }
                    LOG.Debug("BPHelper.TxCreateSigner - Set paramInBPTxCreate.onboardingtype = " + paramInBPTxCreate.onboardingtype);
                    paramInBPTxCreate.typeverifyonboarding = cw.Bpwebtypeonboarding; // (com == null) ? Properties.Settings.Default.CIBPWebTypeOnBoarding : com.BPWebTypeOnBoarding;
                    LOG.Debug("BPHelper.TxCreateSigner - Set paramInBPTxCreate.typeverifyonboarding = " + paramInBPTxCreate.typeverifyonboarding);
                    paramInBPTxCreate.typeverifyauth = cw.Bpwebtypeverify; // (com == null) ? Properties.Settings.Default.CIBPWebTypeAuth : com.BPWebTypeVerify;
                    LOG.Debug("BPHelper.TxCreateSigner - Set paramInBPTxCreate.typeverifyauth = " + paramInBPTxCreate.typeverifyauth);
                    //paramInBPTxCreate.onboardingtype = 1; //Siempre lo mando en Combined, por si se usa

                    paramInBPTxCreate.signerinclude = cw.Signerinclude;
                    paramInBPTxCreate.autorizationinclude = cw.Autorization;
                    LOG.Debug("BPHelper.TxCreateSigner - Set paramInBPTxCreate.autorizationinclude = " + paramInBPTxCreate.autorizationinclude);
                    paramInBPTxCreate.autorizationmessage = cw.Autorizationmessage;
                    paramInBPTxCreate.checklocksrcei = cw.Checklocksrcei;
                    paramInBPTxCreate.videoinclude = cw.Videoinclude;
                    paramInBPTxCreate.videomessage = cw.Videomessage;
                    paramInBPTxCreate.checkadult = cw.Checkadult;
                    paramInBPTxCreate.checkexpired = cw.Checkexpired;
                    LOG.Debug("BPHelper.TxCreateSigner - Set paramInBPTxCreate.georefmandatory = " + cw.Georefmandatory);
                    paramInBPTxCreate.georefmandatory = cw.Georefmandatory;
                    paramInBPTxCreate.redirecturl = string.IsNullOrEmpty(paramInBPTxCreate.redirecturl)?cw.Redirecturl:null;
                    paramInBPTxCreate.titlebpweb = "Proceso de Firma o  Aporobacion";
                    //Database.Company c = Database.AdministradorCompany.BuscarCompanyById(pin.company);
                    Database.Company com = company; // Database.AdministradorCompany.BuscarCompanyById(pin.company);
                    string _companyName = (com == null ? "" : com.Name);
                    paramInBPTxCreate.messagebpweb = "Nuestro cliente " + _companyName + " encargo la certificacion de identidad de " +
                                                     signatory.Typeid + " " + signatory.Valueid;
                    paramInBPTxCreate.theme = cw.Theme;
                    paramInBPTxCreate.successurl = cw.Successurl;
                    paramInBPTxCreate.carregisterinclude = cw.Carregisterinclude;
                    paramInBPTxCreate.writinginclude = cw.Writinginclude;
                    if (!string.IsNullOrEmpty(signatory.Taxidcompany))
                    {
                        paramInBPTxCreate.formid = cw.Forminclude + "_empresa";
                    }
                    else
                    {
                        paramInBPTxCreate.formid = cw.Forminclude;
                    }
                    string jsonParams = JsonConvert.SerializeObject(paramInBPTxCreate);
                    request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                    LOG.Debug("BPHelper.TxCreateSigner - call execute...");
                    IRestResponse response = client.Execute(request);

                    if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK
                        && !string.IsNullOrEmpty(response.Content))
                    {
                        LOG.Debug("BPHelper.TxCreateSigner - procesando response...");
                        bpr = JsonConvert.DeserializeObject<BPResponseTxCreate>(response.Content);
                        LOG.Debug("BPHelper.TxCreateSigner - BPResponseTxCreate != null => " + (bpr != null).ToString());
                    }
                    else
                    {
                        LOG.Warn("BPHelper.TxCreate - response != null => " + (response != null).ToString() + " - " +
                                 ((response != null) ? response.StatusCode.ToString() : "null"));
                    }

                    if (bpr == null)
                    {
                        bptxresponse = null;
                        ret = -1;
                    }
                    else
                    {
                        ret = 0; //limpio salida para que no de error en el controller
                        LOG.Debug("BPHelper.TxCreate - Set salida urlservice = " + bpr.urlservice);
                        trackidbp = bpr.trackid;
                        bptxresponse = new BPTxCreateR(bpr.urlservice);
                    }
                }
                else if (ret < 0 || bpresponse == null) //Salgo e informo
                {
                    msgerr = "Chequeo de enrolamiento con problemas => ret = " + ret +
                             " - (bpresponse!=null)=" + (bpresponse != null).ToString();
                    LOG.Warn("BPHelper.TxCreate - Chequeo de enrolamiento con problemas => ret = " + ret +
                             " - (bpresponse!=null)=" + (bpresponse != null).ToString());
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "TxCreate Excp: " + ex.Message;
                LOG.Error("BPHelper.TxCreate Excp: " + ex.Message);
            }
            LOG.Debug("BPHelper.TxCreate OUT!");
            return ret;
        }
        ///// <summary>
        ///// Debe updatear info del Signatore pero si hay error del DeDocument y DeTx tambien
        ///// </summary>
        ///// <param name="company"></param>
        ///// <param name="trackidbp"></param>
        ///// <returns></returns>
        //internal static int TxUpdate(Company company, string trackidbp)
        //{
        //    try
        //    {

        //    }
        //    catch (Exception ex)
        //    {

        //        LOG.Error(" Error: " + ex.Message);
        //    }
        //}

        //public static int TxCreate(string trackidCI, CICreateModel pin, out string msgerr,
        //                           out string listMails, out string trackidbp, out BPTxCreateR bptxresponse)
        //{
        //    int ret = 0;
        //    msgerr = null;
        //    BPResponseTxCreate bpr = null;
        //    bptxresponse = null;
        //    trackidbp = null;
        //    listMails = null;
        //    try
        //    {
        //        LOG.Debug("BPHelper.TxCreate IN...");
        //        //Added 12-08-2020
        //        //Chequeo que la persona este enrolada. 
        //        //  - Si no lo está => Genero enroll combined
        //        //  - Si está => Genero verify como pide los parametros del OB o Auth segun Company
        //        BPUserEnrolledR bpresponse;
        //        LOG.Debug("BPHelper.TxCreate - Ingresando a CheckIdentityEnrolled con => " +
        //                  Properties.Settings.Default.CIBPCompanyId + "/" + pin.typeId + "/" + pin.valueId);
        //        ret = CheckIdentityEnrolled(Properties.Settings.Default.CIBPCompanyId, pin.typeId, pin.valueId,
        //                                    out bpresponse, out msgerr);
        //        LOG.Debug("BPHelper.TxCreate - CheckIdentityEnrolled ret = " + ret);
        //        //Si no hubo error o bien no encontro que este enrolado => Enrolo
        //        if ((ret == 0 || ret == Errors.IRET_ERR_DONT_CHECK_ENROLLED)) // && bpresponse != null)
        //        {
        //            LOG.Debug("BPHelper.TxCreate - Ingresa a crear TX segun parámetros y workflow pedido = " +
        //                pin.company + "/" + pin.workflowname);
        //            //Si no hubo error analizo la informacion retornada para determinar los pasos a hacer 
        //            // en conjunto con el workflow 
        //            Database.CompanyWorkflow cw = Database.AdministradorCompanyWorkflow.
        //                                BuscarCompanyWokflowByName(pin.company, pin.workflowname);

        //            if (cw == null)
        //            {
        //                LOG.Warn("BPHelper.TxCreate - Workflow " + pin.company + "/" + pin.workflowname + " no encontrado!. Sale con error " +
        //                         Errors.IRET_ERR_WORKFLOW_DONT_EXIST);
        //                return Errors.IRET_ERR_WORKFLOW_DONT_EXIST;
        //            }
        //            listMails = cw.ReceiverList; //Retorno lista de mails a enviar notify si se debe enviar

        //            //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //            //var client = new RestClient("https://qabpservice5.9.biometrikalatam.com/api/" + "txCreate");
        //            LOG.Debug("BPHelper.TxCreate - LLama a " + Properties.Settings.Default.BPWebUrlBase + "txCreate...");
        //            var client = new RestClient(Properties.Settings.Default.BPWebUrlBase + "txCreate");
        //            client.Timeout = Properties.Settings.Default.BPTimeout;
        //            var request = new RestRequest(Method.POST);
        //            client.UserAgent = "biometrika";
        //            request.AddHeader("Authorization", "Basic " +
        //                Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Properties.Settings.Default.BPAccessName + ":" +
        //                                                                    Properties.Settings.Default.BPSecretKey)));
        //            request.AddHeader("Content-Type", "application/json");
        //            request.AddHeader("Accept", "application/json");
        //            //int iaux = pin.company;
        //            //pin.company = Properties.Settings.Default.CIBPCompanyId;
        //            //string jsonParams = JsonConvert.SerializeObject(pin);
        //            //pin.company = iaux;

        //            BPTxCreate paramInBPTxCreate = new BPTxCreate();
        //            paramInBPTxCreate.callbackurl = pin.callbackUrl;
        //            paramInBPTxCreate.company = Properties.Settings.Default.CIBPCompanyId;
        //            paramInBPTxCreate.customertrackid = trackidCI;

        //            paramInBPTxCreate.redirecturl = pin.redirectUrl;
        //            paramInBPTxCreate.successurl = pin.successUrl;
        //            //paramInBPTxCreate.signerinclude = pin.signerInclude;
        //            paramInBPTxCreate.theme = pin.theme;
        //            paramInBPTxCreate.threshold = pin.threshold;
        //            paramInBPTxCreate.typeid = pin.typeId;
        //            paramInBPTxCreate.valueid = pin.valueId;

        //            //Check para ver que seteamos
        //            //Pongo que no sea mandatorio porque es NV. Si no está enrolado en BP se coloca por default 1 
        //            paramInBPTxCreate.onboardingmandatory = cw.OnBoardingMandatory;
        //            LOG.Debug("BPHelper.TxCreate - Set paramInBPTxCreate.onboardingmandatory = " + paramInBPTxCreate.onboardingmandatory);
        //            //Si esta enrolado => Auth sino enroll combined
        //            if (bpresponse == null)
        //            {
        //                paramInBPTxCreate.onboardingtype = 1; //NO esta enrolado => Fuerzo Onboarding
        //            }
        //            else if (bpresponse.exist == 1 && bpresponse.isenrolledtv3dplus == 1) //&& bpresponse.isenrolledft
        //            {
        //                paramInBPTxCreate.onboardingtype = 0; //Ya esta enrolado => Pongo Auth
        //            }
        //            else
        //            {
        //                paramInBPTxCreate.onboardingtype = 1; //NO esta enrolado => Fuerzo Onboarding
        //            }
        //            LOG.Debug("BPHelper.TxCreate - Set paramInBPTxCreate.onboardingtype = " + paramInBPTxCreate.onboardingtype);
        //            paramInBPTxCreate.typeverifyonboarding = cw.BPWebTypeOnboarding; // (com == null) ? Properties.Settings.Default.CIBPWebTypeOnBoarding : com.BPWebTypeOnBoarding;
        //            LOG.Debug("BPHelper.TxCreate - Set paramInBPTxCreate.typeverifyonboarding = " + paramInBPTxCreate.typeverifyonboarding);
        //            paramInBPTxCreate.typeverifyauth = cw.BPWebTypeVerify; // (com == null) ? Properties.Settings.Default.CIBPWebTypeAuth : com.BPWebTypeVerify;
        //            LOG.Debug("BPHelper.TxCreate - Set paramInBPTxCreate.typeverifyauth = " + paramInBPTxCreate.typeverifyauth);
        //            //paramInBPTxCreate.onboardingtype = 1; //Siempre lo mando en Combined, por si se usa

        //            paramInBPTxCreate.signerinclude = cw.SignerInclude;
        //            paramInBPTxCreate.autorizationinclude = cw.Autorization;
        //            LOG.Debug("BPHelper.TxCreate - Set paramInBPTxCreate.autorizationinclude = " + paramInBPTxCreate.autorizationinclude);
        //            paramInBPTxCreate.autorizationmessage = cw.AutorizationMessage;
        //            paramInBPTxCreate.checklocksrcei = cw.CheckLockSRCeI;
        //            paramInBPTxCreate.videoinclude = cw.VideoInclude;
        //            paramInBPTxCreate.videomessage = cw.VideoMessage;
        //            paramInBPTxCreate.checkadult = cw.CheckAdult;
        //            paramInBPTxCreate.checkexpired = cw.CheckExpired;
        //            LOG.Debug("BPHelper.TxCreate - Set paramInBPTxCreate.georefmandatory = " + cw.GeoRefMandatory);
        //            paramInBPTxCreate.georefmandatory = cw.GeoRefMandatory;
        //            paramInBPTxCreate.redirecturl = cw.RedirectUrl;
        //            paramInBPTxCreate.successurl = cw.SuccessUrl;
        //            paramInBPTxCreate.titlebpweb = "Certificacion de Identidad";
        //            //Database.Company c = Database.AdministradorCompany.BuscarCompanyById(pin.company);
        //            Database.Company com = Database.AdministradorCompany.BuscarCompanyById(pin.company);
        //            string _companyName = (com == null ? "" : com.Name);
        //            paramInBPTxCreate.messagebpweb = "Nuestro cliente " + _companyName + " encargo la certificacion de identidad de " +
        //                                             pin.typeId + " " + pin.valueId;
        //            paramInBPTxCreate.theme = cw.Theme;
        //            string jsonParams = JsonConvert.SerializeObject(paramInBPTxCreate);
        //            request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
        //            LOG.Debug("BPHelper.TxCreate - call execute...");
        //            IRestResponse response = client.Execute(request);

        //            if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK
        //                && !string.IsNullOrEmpty(response.Content))
        //            {
        //                LOG.Debug("BPHelper.TxCreate - procesando response...");
        //                bpr = JsonConvert.DeserializeObject<BPResponseTxCreate>(response.Content);
        //                LOG.Debug("BPHelper.TxCreate - BPResponseTxCreate != null => " + (bpr != null).ToString());
        //            }
        //            else
        //            {
        //                LOG.Warn("BPHelper.TxCreate - response != null => " + (response != null).ToString() + " - " +
        //                         ((response != null) ? response.StatusCode.ToString() : "null"));
        //            }

        //            if (bpr == null)
        //            {
        //                bptxresponse = null;
        //                ret = -1;
        //            }
        //            else
        //            {
        //                ret = 0; //limpio salida para que no de error en el controller
        //                LOG.Debug("BPHelper.TxCreate - Set salida urlservice = " + bpr.urlservice);
        //                trackidbp = bpr.trackid;
        //                bptxresponse = new BPTxCreateR(bpr.urlservice);
        //            }
        //        }
        //        else if (ret < 0 || bpresponse == null) //Salgo e informo
        //        {
        //            msgerr = "Chequeo de enrolamiento con problemas => ret = " + ret +
        //                     " - (bpresponse!=null)=" + (bpresponse != null).ToString();
        //            LOG.Warn("BPHelper.TxCreate - Chequeo de enrolamiento con problemas => ret = " + ret +
        //                     " - (bpresponse!=null)=" + (bpresponse != null).ToString());
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = -1;
        //        msgerr = "TxCreate Excp: " + ex.Message;
        //        LOG.Error("BPHelper.TxCreate Excp: " + ex.Message);
        //    }
        //    LOG.Debug("BPHelper.TxCreate OUT!");
        //    return ret;
        //}

        private static int CheckIdentityEnrolled(int cIBPCompanyId, string typeId, string valueId,
                                                 string accessName, string secretKey,
                                                 out BPUserEnrolledR bpresponse, out string msgerr)
        {
            int ret = Errors.RET_OK;
            bpresponse = null;
            msgerr = null;
            try
            {
                LOG.Debug("BPHelper.CheckIdentityEnrolled IN...");
                LOG.Debug("BPHelper.CheckIdentityEnrolled - Calling " + Properties.Settings.Default.BPWebUrlBase + "userEnrolled?" +
                    "companyid=" + cIBPCompanyId + "&typeid=" + typeId + "&valueId=" + valueId);
                var client = new RestClient(Properties.Settings.Default.BPWebUrlBase + "userEnrolled?" +
                    "companyid=" + cIBPCompanyId + "&typeid=" + typeId + "&valueId=" + valueId);
                client.Timeout = Properties.Settings.Default.BPTimeout;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "Basic " +
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(accessName + ":" + secretKey)));
                client.UserAgent = "biometrika";
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                //int iaux = pin.company;
                //pin.company = Properties.Settings.Default.CIBPCompanyId;
                //string jsonParams = JsonConvert.SerializeObject(pin);
                //pin.company = iaux;

                //BPUserEnrolled paramInBPUE = new BPUserEnrolled();
                //paramInBPUE.companyid = cIBPCompanyId;
                //paramInBPUE.typeid = typeId;
                //paramInBPUE.valueid = valueId;

                //string jsonParams = JsonConvert.SerializeObject(paramInBPUE);
                //request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                LOG.Debug("BPHelper.CheckIdentityEnrolled - client.Execute...");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK
                        && !string.IsNullOrEmpty(response.Content))
                {
                    LOG.Debug("BPHelper.CheckIdentityEnrolled - Procewsando respuesta...");
                    bpresponse = JsonConvert.DeserializeObject<BPUserEnrolledR>(response.Content);
                    ret = (bpresponse != null && bpresponse.code == 0) ? 0 :
                                    (bpresponse != null ? bpresponse.code : Errors.RET_ERR_DONT_CHECK_ENROLLED);
                    LOG.Debug("BPHelper.CheckIdentityEnrolled - ret procesado = " + ret);
                }
                else
                {
                    msgerr = "CheckIdentityEnrolled - Error = " + Errors.RET_ERR_DONT_CHECK_ENROLLED;
                    LOG.Warn("BPHelper.CheckIdentityEnrolled - Error = " + Errors.RET_ERR_DONT_CHECK_ENROLLED);
                    ret = Errors.RET_ERR_DONT_CHECK_ENROLLED;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.RET_ERR_UNKKNOW;
                msgerr = "CheckIdentityEnrolled Excp [" + ex.Message + "]";
                LOG.Error("BPHelper.CheckIdentityEnrolled Excp: " + ex.Message);
            }
            LOG.Debug("BPHelper.CheckIdentityEnrolled OUT");
            return ret;
        }

        public static int TxStatus(Company company, string bptrackid, out string msgerr, out BpTxDataResponse bptxresponse)
        {
            int ret = 0;
            msgerr = null;
            BPTxStatusResponse bpsr = null;
            bptxresponse = null;
            try
            {
                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //var client = new RestClient("https://qabpservice5.9.biometrikalatam.com/api/" + "txCreate");
                var client = new RestClient(Properties.Settings.Default.BPWebUrlBase + "txGetStatus?trackid=" + bptrackid);
                client.Timeout = 60000;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "Basic " +
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(company.Accessnamebp + ":" +
                                                                            company.Secretkeybp)));
                client.UserAgent = "biometrika";
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK
                    && !string.IsNullOrEmpty(response.Content))
                {
                    bpsr = JsonConvert.DeserializeObject<BPTxStatusResponse>(response.Content);
                }

                if (bpsr == null)
                {
                    bptxresponse = null;
                    ret = Errors.RET_ERR_UNKKNOW;
                }
                else
                {
                    if (bpsr.status == 1 || bpsr.status == 2) //Si es 1 = DONE o 2 = FAILED => Recupero datos
                    {
                        client = new RestClient(Properties.Settings.Default.BPWebUrlBase + "txGetData?trackid=" + bptrackid + "&flag=2");
                        client.Timeout = 3000000;
                        request = new RestRequest(Method.GET);
                        request.AddHeader("Authorization", "Basic " +
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(company.Accessnamebp + ":" +
                                                                            company.Secretkeybp)));
                        client.UserAgent = "biometrika";
                        request.AddHeader("Content-Type", "application/json");
                        request.AddHeader("Accept", "application/json");
                        response = client.Execute(request);

                        if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK
                            && !string.IsNullOrEmpty(response.Content))
                        {
                            bptxresponse = JsonConvert.DeserializeObject<BpTxDataResponse>(response.Content);

                            if (bptxresponse == null)
                            {
                                ret = Errors.RET_ERR_CALLING_PROVIDER_SERVICE;
                            }
                            else
                            {
                                ret = 0;
                            }
                        }
                        else
                        {
                            ret = Errors.RET_API_INTERNAL_SERVER_ERROR;
                        }
                    }
                    else
                    {
                        ret = 0;
                        bptxresponse = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }

        internal static string GetSample(BpTxDataResponse oBpTxDataResponse, int type)
        {
            string ret = null;
            try
            {
                if (oBpTxDataResponse == null || oBpTxDataResponse.Samples == null) return null;

                foreach (ImageSample item in oBpTxDataResponse.Samples)
                {
                    if ((int)item.typeimage == type)
                    {
                        ret = item.data;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("NotarizeController.GetSample Error: " + ex.Message);
            }
            return ret;
        }

        #endregion API Rest x BPWeb
    }
}