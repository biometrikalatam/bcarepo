﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Biometrika.Document.Electronic.Core.BP
{
    public class BPModels
    {
    }

    public class BPUserEnrolled
    {
        public int companyid;
        public string typeid;
        public string valueid;
    }

    public class BPUserEnrolledR
    {
        public int code;                    //0- Ejecuto bien sino <0 con error
        public int exist;                   //0-No o 1-Si
        public int isenrolledtv2d;          //0-No o 1-Si
        public int isenrolledtv3d;          //0-No o 1-Si
        public int isenrolledtv3dplus;      //0-No o 1-Si
    }

    public class BPResponseTxCreate
    {
        public BPResponseTxCreate() { }

        public BPResponseTxCreate(int _code, string _msg, string _trackid)
        {
            code = _code;
            msgerr = _msg;
            trackid = _trackid;
        }

        public BPResponseTxCreate(int _code, string _msg, string _trackid, string _urlservice)
        {
            code = _code;
            msgerr = _msg;
            trackid = _trackid;
            urlservice = _urlservice;
        }

        public int code;
        public string msgerr;
        public string trackid;
        public string urlservice;

    }

    public class BPTxCreateR
    {

        public BPTxCreateR() { }
        public BPTxCreateR(string _url)
        {
            verifyUrl = _url;
        }

        public BPTxCreateR(string _trackidci, string _url)
        {
            trackidci = _trackidci;
            verifyUrl = _url;
        }

        public string trackidci;
        public string verifyUrl;
    }

    public class BPTxCreate
    {
        public int company { get; set; }
        public string typeid { get; set; }
        public string valueid { get; set; }
        public string taxidcompany { get; set; }
        public string theme { get; set; }
        public string customertrackid { get; set; }
        public string callbackurl { get; set; }
        public string redirecturl { get; set; }
        public string successurl { get; set; }
        public string typeverifyonboarding { get; set; }
        public string typeverifyauth { get; set; }
        public int onboardingmandatory { get; set; }
        public int onboardingtype { get; set; }
        public int signerinclude { get; set; }
        public int autorizationinclude { get; set; }
        public string autorizationmessage { get; set; }
        public double threshold { get; set; }
        public int checklocksrcei { get; set; } // 0-No o 1-Si
        public string titlebpweb { get; set; }
        public string messagebpweb { get; set; }
        public int videoinclude { get; set; }
        public string videomessage { get; set; }
        public int checkadult { get; set; }
        public int checkexpired { get; set; }
        public int georefmandatory { get; set; }

        public int carregisterinclude { get; set; } // 0-No o 1-Si
        public int writinginclude { get; set; } // 0-No o 1-Si
        public string formid { get; set; } // Null=No incluye form o "stringid"=Nombre el form (para futuro que será definible, ahora solo usa el form fijo)

    }

    public class BpTxDataResponse
    {
        public BpTxDataResponse() { }

        public BpTxDataResponse(int _code, string _msgerr)
        {
            code = _code;
            msgerr = _msgerr;
        }

        public int code;
        public string msgerr;
        public int company;
        public int operationcode;
        public TypeVerify typeverify;
        public int onboardingmandatory;
        public int result;
        public double score;
        public double threshold;
        public int codereject;
        public string descreject; //Motivo de rechazo, para caso de chequeos hibridos, adulteraciones, etc.
        public string estadolock; //Motivo de rechazo, para caso de chequeos hibridos, adulteraciones, etc.
        public string razonlock; //Motivo de rechazo, para caso de chequeos hibridos, adulteraciones, etc.
        public string vigenciafromlock; //Motivo de rechazo, para caso de chequeos hibridos, adulteraciones, etc.
        public string georef;
        public string enduser;
        public string videourl;

        public TxtData personaldata;
        public string taxidcompany;

        public IList<ImageSample> Samples;
    }

    public class TxtData
    {
        public string typeid;
        public string valueid;
        public string name;
        public string patherlastname;
        public string motherlastname;
        public string sex;
        public string documentseriesnumber;
        public string documentexpirationdate;
        public string visatype;
        public string birthdate;
        public string birthplace;
        public string nationality;
        public string profession;
        public string creation;
        public string verificationsource;
    }

    public class ImageSample
    {
        public ImageSample() { }

        public ImageSample(string _code, TypeImage _typeimage, TypeImageFormat _typeimageformat, string _data)
        {
            code = _code;
            typeimage = _typeimage;
            typeimageformat = _typeimageformat;
            data = _data;
        }

        public ImageSample(TypeImage _typeimage, TypeImageFormat _typeimageformat, string _data)
        {
            typeimage = _typeimage;
            typeimageformat = _typeimageformat;
            data = _data;
        }

        public TypeImage typeimage; //
        public TypeImageFormat typeimageformat = TypeImageFormat.jpg; //0-jpg | 1-png | 2-bmp
        public string code;
        public string data;
    }

    public enum TypeImageFormat
    {
        jpg = 0,
        png = 1,
        bmp = 2,
        pdf = 3,
        json = 4,
        video = 5,
        videourl = 6
    }

    public enum TypeImage
    {
        cedulafront = 0,
        cedulaback = 1,
        selfie = 2,
        cedulasignature = 3,
        cedulafoto = 4,
        manualsignature = 5,
        qr = 6,
        image = 7,
        facemap = 8,
        pdf = 9,
        json = 10,
        video = 11
    }

    public enum TypeVerify
    {
        tv2d = 1,           //No soportada ahora
        tv3d = 2,           //Facetec
        tv3dplus = 3,       //Jumio Automatico
        tv3dstrong = 4      //Jumio Hibrido
    }

    public class BPTxStatusResponse
    {
        public BPTxStatusResponse() { }

        public BPTxStatusResponse(int _code, string _msg)
        {
            code = _code;
            msgerr = _msg;
            trackid = null;
            status = 0;
        }

        public BPTxStatusResponse(int _code, string _msg, string _trackid, int _status, string _statusdescription)
        {
            code = _code;
            msgerr = _msg;
            trackid = _trackid;
            status = _status;
            statusdescription = _statusdescription;
        }

        public int code;
        public string msgerr;
        public string trackid;
        public int status;
        public string statusdescription;
    }
}