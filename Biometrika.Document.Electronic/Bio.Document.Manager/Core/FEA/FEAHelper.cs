﻿using Biometrika.Document.Electronic.Api.Model;
using log4net;
using System;

namespace Biometrika.Document.Electronic.Core.FEA
{
    public class FEAHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(FEAHelper));

        internal static int SignCloud(int company, string name, DocumentType dtype, string pdftosign, out string pdfsigned)
        {
            int ret = 0;
            pdfsigned = null;
            try
            {
                LOG.Debug("FEAHelper.SignCloud Start...");
                string urlfea = (Global.DOCUMENT_TYPES.GetCompany(company)).UrlFEA;
                string appfea = (Global.DOCUMENT_TYPES.GetCompany(company)).AppFEA;
                string userfea = (Global.DOCUMENT_TYPES.GetCompany(company)).UserFEA;
                string pswfea = (Global.DOCUMENT_TYPES.GetCompany(company)).SignaturePSW;
                if (!string.IsNullOrEmpty(dtype.UrlFEA))
                {
                    urlfea = dtype.UrlFEA;
                    appfea = dtype.AppFEA;
                    userfea = dtype.UserFEA;
                    pswfea = dtype.SignaturePSW;
                }
                LOG.Debug("FEAHelper.SignCloud => urlfea=" + urlfea + " - appfea=" + appfea + " - userfea=" + userfea + " - pswfea=" + pswfea);
                LOG.Debug("FEAHelper.SignCloud => pdftosign = " + pdftosign);

                //string straux = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + xmltosign.Substring(xmltosign.IndexOf("<Rd"));
                //byte[] byFile = System.Text.Encoding.UTF8.GetBytes(straux);
                //string strFile = Convert.ToBase64String(byFile);
                using (cl.servisign.esigner6.WSIntercambiaDocSoapService wssigner = new cl.servisign.esigner6.WSIntercambiaDocSoapService())
                {
                    wssigner.Url = urlfea;
                    wssigner.Timeout = Properties.Settings.Default.FEATimeoutWS;
                    LOG.Debug("FEAHelper.SignCloud FEATimeoutWS=" + Properties.Settings.Default.FEATimeoutWS);
                    cl.servisign.esigner6.intercambiaDocEncabezado enc = new cl.servisign.esigner6.intercambiaDocEncabezado();
                    enc.User = userfea;  //"biometrika"
                    enc.Password = pswfea; //"1234"
                    enc.NombreConfiguracion = appfea; // "BIOPDF453";

                    LOG.Debug("FEAHelper.SignCloud Creando param...");
                    cl.servisign.esigner6.intercambiaDocParametro param = new cl.servisign.esigner6.intercambiaDocParametro();
                    param.Documento = pdftosign;
                    param.NombreDocumento = name;
                    param.MetaData = null;

                    LOG.Debug("FEAHelper.SignCloud Signing IN...");

                    //System.IO.FileStream sw = new System.IO.FileStream(Properties.Settings.Default.PathTemp + "test.pdf", System.IO.FileMode.Create);
                    //byte[] file3 = Convert.FromBase64String(pdftosign);
                    //sw.Write(file3, 0, file3.Length);
                    //sw.Close();

                    cl.servisign.esigner6.intercambiaDocResponseIntercambiaDoc resp = wssigner.intercambiaDoc(enc, param);
                    LOG.Debug("FEAHelper.SignCloud Signing Out...");

                    if (resp != null)
                    {
                        if (resp.IntercambiaDocResult.Estado.Equals("OK"))
                        {
                            pdfsigned = resp.IntercambiaDocResult.Documento;
                            //pdfsigned = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(resp.IntercambiaDocResult.Documento));
                            LOG.Debug("FEAHelper.SignCloud pdfsigned = " + pdfsigned);
                            //xmlsigned = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(resp.IntercambiaDocResult.Documento));
                            //System.IO.FileStream fs1 = new System.IO.FileStream(resp.IntercambiaDocResult.NombreDocumento, System.IO.FileMode.CreateNew);
                            //byte[] byArrResp = Convert.FromBase64String(resp.IntercambiaDocResult.Documento);
                            //fs1.Write(byArrResp, 0, byArrResp.Length);
                            //fs1.Close();
                        }
                        else
                        {
                            LOG.Error("Error FEA = " + resp.IntercambiaDocResult.Comentarios);
                            ret = Api.Errors.RET_ERR_SIGNING_PDF_FEA_SIG;
                        }
                    }
                    else
                    {
                        LOG.Error("Error FEA = Rpta NULL");
                        ret = Api.Errors.RET_ERR_SIGNING_PDF_FEA_SIG;
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("FEAHelper.SignCloud Error = " + ex.Message);
                ret = Api.Errors.RET_ERR_UNKKNOW;
            }
            LOG.Debug("FEAHelper.SignCloud Out!");

            return ret;
        }
    }
}