﻿using Bio.Digital.Receipt.Api;
using Biometrika.Document.Electronic.Api;
using Biometrika.Document.Electronic.WSBDR;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Biometrika.Document.Electronic.NV
{
    /// <summary>
    /// 
    /// </summary>
    public class NVHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NVHelper));

        /// <summary>
        /// Notariza el documento si asi se configuro.
        /// </summary>
        /// <param name="idtx"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="xmlabs"></param>
        /// <param name="xmlRDenvelope"></param>
        /// <returns></returns>
        internal static int Notarize(string seed, string idcompany, string idDE, string nameDE, string de, string extension, out string xmlRDenvelope)
        {

            int ret = Bio.Digital.Receipt.Api.Errors.IRET_OK;
            xmlRDenvelope = null;
            Bio.Digital.Receipt.Api.RdExtensionItem item;
            string xmlparamin;
            string xmlparamout;
            Bio.Digital.Receipt.Api.XmlParamOut oParamout;
            try
            {
                LOG.Debug("NVHelper.Notarize Start - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                Bio.Digital.Receipt.Api.XmlParamIn oParamin = new Bio.Digital.Receipt.Api.XmlParamIn();
                oParamin.Actionid = 1;
                LOG.Debug("NVHelper.Notarize NV_IDApp = " + Properties.Settings.Default.NV_IDApp);
                oParamin.ApplicationId = Properties.Settings.Default.NV_IDApp;
                oParamin.Description = "Biometric Advanced Signature Notarize";
                oParamin.Origin = 2; //Biometrika BioPortal
                LOG.Debug("NVHelper.Notarize Cargando Extensiones...");
                oParamin.Extensiones = new RdExtensionItem[7];

                item = new RdExtensionItem();
                item.key = "IdDocument";
                item.value = seed;
                LOG.Debug("NVHelper.Notarize key=" + item.key + "=> value=" + item.value);
                oParamin.Extensiones[0] = item;
                item = new RdExtensionItem();
                item.key = "IdCompany";
                item.value = idcompany;
                LOG.Debug("NVHelper.Notarize key=" + item.key + "=> value=" + item.value);
                oParamin.Extensiones[1] = item;
                item = new RdExtensionItem();
                item.key = "IdDE";
                item.value = idDE;
                LOG.Debug("NVHelper.Notarize key=" + item.key + "=> value=" + item.value);
                oParamin.Extensiones[2] = item;
                item = new RdExtensionItem();
                item.key = "NameDE";
                item.value = nameDE;
                LOG.Debug("NVHelper.Notarize key=" + item.key + "=> value=" + item.value);
                oParamin.Extensiones[3] = item;
                item = new RdExtensionItem();
                item.key = "Date";
                item.value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                LOG.Debug("NVHelper.Notarize key=" + item.key + "=> value=" + item.value);
                oParamin.Extensiones[4] = item;
                item = new RdExtensionItem();
                item.key = "DE";
                item.value = de;
                LOG.Debug("NVHelper.Notarize key=" + item.key + "=> value=" + item.value);
                oParamin.Extensiones[5] = item;
                item = new RdExtensionItem();
                item.key = "Extension";
                item.value = extension;
                LOG.Debug("NVHelper.Notarize key=" + item.key + "=> value=" + item.value);
                oParamin.Extensiones[6] = item;


                xmlparamin = Bio.Digital.Receipt.Api.XmlUtils.SerializeAnObject(oParamin);
                LOG.Debug("NVHelper.Notarize xmlparamin = " + xmlparamin);
                using (WSBDR.WSBDR NV = new WSBDR.WSBDR())
                {
                    LOG.Debug("NVHelper.Notarize NVURL=" + Properties.Settings.Default.NVURL);
                    NV.Url = Properties.Settings.Default.NVURL;
                    LOG.Debug("NVHelper.Notarize NVTimeoutWS=" + Properties.Settings.Default.NVTimeoutWS);
                    NV.Timeout = Properties.Settings.Default.NVTimeoutWS;

                    LOG.Debug("NVHelper.Notarize Notarizing...");
                    int rdRet = NV.Process(xmlparamin, out xmlparamout);
                    LOG.Debug("NVHelper.Notarize Notarized out...");
                    if (rdRet == 0)
                    {
                        oParamout = Bio.Digital.Receipt.Api.XmlUtils.DeserializeObject<Bio.Digital.Receipt.Api.XmlParamOut>(xmlparamout);
                        xmlRDenvelope = oParamout.Receipt;
                        LOG.Debug("NVHelper.Notarize xmlRDenvelope = " + xmlRDenvelope);
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(xmlparamout))
                        {
                            oParamout = Bio.Digital.Receipt.Api.XmlUtils.DeserializeObject<Bio.Digital.Receipt.Api.XmlParamOut>(xmlparamout);
                            LOG.Error("NVHelper.Notarize Error - " + "de NV [" + rdRet + " - " + oParamout.Message + "]");
                        }
                        else
                        {
                            LOG.Error("NVHelper.Notarize Error de NV [" + rdRet + "]");
                        }

                    }
                    ret = rdRet;
                }
             }
            catch (Exception ex)
            {
                ret = Bio.Digital.Receipt.Api.Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("NVHelper.Notarize Error - " + ex.Message);
            }
            LOG.Debug("NVHelper.Notarize Out - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            return ret;
        }

    }
}