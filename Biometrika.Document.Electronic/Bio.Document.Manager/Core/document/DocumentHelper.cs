﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Office.Interop;
using log4net;
using Biometrika.Document.Electronic.libs;
using Microsoft.Office.Interop.Word;
using Biometrika.Document.Electronic.PDF;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.X509;
using iTextSharp.text.pdf.security;
using System.IO;
using Biometrika.Document.Electronic.Core.FEA;
using SautinSoft.Document;
using SautinSoft.Document.Drawing;
using Biometrika.Document.Electronic.Api;
using Biometrika.Document.Electronic.Database;

namespace Biometrika.Document.Electronic.document
{
    public class DocumentHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(DocumentHelper));

#region WORD

        /// <summary>
        /// Genera un PDf desde una plantilla Word cambiando los TAGs definidos por lo svalores enviados
        /// </summary>
        /// <param name="param"></param>
        /// <param name="seed"></param>
        /// <param name="tagsprocessed"></param>
        /// <param name="deout"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal int GenerateDE(XmlParamIn param, string seed, List<Api.Model.TagProcessed> tagsprocessed, out byte[] deout, out string msgerr)
        {
            int ret = Errors.RET_OK;
            deout = null;
            msgerr = null;
            object oMissing = Type.Missing;
            object readOnly = null;
            object oVisible = null;
            try
            {
                LOG.Info("   >>> Start Generate in Word - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")); 
                LOG.Debug("DocumentHelper.GenerateDE Start...");

                //1.- Recupero Dtype para tener 
                Api.Model.DocumentType dtype = Global.DOCUMENT_TYPES.GetDEType(param.Company, param.IdDE);
                if (dtype == null)
                {
                    msgerr = "No existe documento asociado a clave [" + (param.Company.ToString() + "-" + param.IdDE.ToString()) + "]";
                    LOG.Warn("DocumentHelper.GenerateDE Error [" + msgerr + "]");
                    ret = Errors.RET_ERR_DE_CONFIG_NOT_EXIST;
                }
                else
                {
                    LOG.Debug("DocumentHelper.GenerateDE Dtype Obtenido = " + param.Company.ToString() + "-" + param.IdDE.ToString() + "-" + dtype.Name + "...");
                }

                //2.- Levanto Template de disco
                LOG.Debug("DocumentHelper.GenerateDE Word Zone - IN...");
                //Microsoft.Office.Interop.Word._Application oWord;
                //Microsoft.Office.Interop.Word.Document oDoc;
                DocumentCore dc = DocumentCore.Load(dtype.Template);
                
                LOG.Debug("DocumentHelper.GenerateDE Word Ax Zone - New Application...");
                //oWord = new Microsoft.Office.Interop.Word.Application();
                //oWord.Visible = false;
                object filename = dtype.Template;
                LOG.Debug("DocumentHelper.GenerateDE Open template = " + filename);
                //oDoc = oWord.Documents.Open(ref filename,
                //                            ref oMissing, ref readOnly, ref oMissing, ref oMissing, ref oMissing,
                //                            ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                //                            ref oVisible, ref oMissing, ref oMissing, ref oMissing, ref oMissing);
                
                //3.- Reemplazo Tags
                LOG.Debug("DocumentHelper.GenerateDE Reemplazando Tgas...");
                int tagsreemplazadas = 0;
                short w, h, pos, x, y;
                for (int i = 0; i < tagsprocessed.Count; i++)
                {
                    LOG.Debug("DocumentHelper.GenerateDE Replace " + tagsprocessed[i].IdTag.Trim() + " => " + (string)tagsprocessed[i].value);
                    switch (tagsprocessed[i].Type)
                    {
                        case 0:
                        case 1:
                        case 2:
                            //tagsreemplazadas += InsertarTexto(oDoc, tagsprocessed[i].IdTag.Trim(), (string)tagsprocessed[i].value);
                            tagsreemplazadas += InsertarTexto(dc, tagsprocessed[i].IdTag.Trim(), (string)tagsprocessed[i].value);
                            break;
                        case 3:
                        case 4:
                            //tagsreemplazadas += ReemplazarImagen(oDoc, tagsprocessed[i].IdTag.Trim(), (string)tagsprocessed[i].value);
                            ExtractParams(tagsprocessed[i].Parameters, out w, out h, out pos, out x, out y);
                            tagsreemplazadas += ReemplazarImagen(dc, tagsprocessed[i].IdTag.Trim(), (string)tagsprocessed[i].value, w, h, pos, x, y);
                            break;
                        default:
                            break;
                    }
                }
                LOG.Debug("DocumentHelper.GenerateDE Fin reemplazo Tags! Total por reemplazar = " + tagsprocessed.Count + " - Total reemplazadas = " + tagsreemplazadas);

                //4.- Grabo en PDF
                string pathToSave = Properties.Settings.Default.PathTemp + seed + ".pdf";
                LOG.Debug("DocumentHelper.GenerateDE Path to Save =" + pathToSave);
                //object fileFormat = WdSaveFormat.wdFormatPDF;
                LOG.Debug("DocumentHelper.GenerateDE Save dc.Save...");
                dc.Save(pathToSave, SaveOptions.PdfDefault);
                //oDoc.SaveAs2(ref pathToSave,
                //            ref fileFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                //            ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                //            ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);
                System.Threading.Thread.Sleep(500);
                object refSaveShanges = false;
                //oWord.Documents.Close(ref refSaveShanges, ref oMissing, ref oMissing);
                //refSaveShanges = false;
                LOG.Debug("DocumentHelper.GenerateDE Word Ax Zone - Out!");
                //oWord.Quit(ref refSaveShanges, ref oMissing, ref oMissing);
                //oWord = null;
                //oDoc = null;
                //5.- Leo desde HHD el PDF
                LOG.Debug("DocumentHelper.GenerateDE read Document generated...");
                deout = System.IO.File.ReadAllBytes((string)pathToSave);

                //6.- Agrego las BS si vienen. Esto lo agrego el 17/11/2018 para poder firmar con FE3D generando desde Word
                bool _hayError = false;
                LOG.Debug("DocumentHelper.GenerateDE ListBS.length = " + (param.BSSignatures!= null?param.BSSignatures.Count.ToString():"null"));

                List<byte[]> listFiles;
                listFiles = new List<byte[]>();
                byte[] fileAux = deout;
                listFiles.Add(fileAux);
                byte[] fileAux1 = null;
                byte[] fileAux2 = null;
                byte[] fileAux3 = null;

                if (param.BSSignatures != null && param.BSSignatures.Count > 0)
                {
                    LOG.Debug("DocumentHelper.GenerateDE Insertando Biometrica Signatures...");
                    foreach (BS itemBS in param.BSSignatures)
                    {
                        LOG.Debug("DocumentHelper.GenerateDE Insertando BSId = " + itemBS.BSId + "...");

                        if (!String.IsNullOrEmpty(itemBS.NVreceiptId))
                        {
                            fileAux1 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_NV);
                        }
                        else
                        {
                            fileAux1 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_FES);
                        }
                        fileAux2 = PDFHelper.AddABS(fileAux1, itemBS);
                        fileAux1 = fileAux2;
                        _hayError = (fileAux1 == null);
                        if (!_hayError) LOG.Debug("DocumentHelper.GenerateDE BS Added...");
                        else break;
                    }
                    LOG.Debug("DocumentHelper.GenerateDE Biometrica Signatures Insertados!");
                } else
                {
                    fileAux1 = fileAux;
                }

                if (_hayError)
                {
                    msgerr = "Error agregando BSs en documento generado";
                    LOG.Warn("DocumentHelper.GenerateDE Error [" + msgerr + "]");
                    ret = Errors.RET_ERR_ADDING_PDF_TEMPLATE_SIG;
                } else {
                    deout = fileAux1;
                }

                //3.- Levanto Template de disco de acuerdo al tipo de firma configurada
                LOG.Debug("DocumentHelper.GenerateDE Chequeando que tipo de firma se hará, para agregar template...");
                LOG.Debug("DocumentHelper.GenerateDE SignatureType = " + dtype.SignatureType + "...");
                //    0:Sin Firmar 
                //    1: Firma Electronica Simple con PFX + PSW Abajo Default Compañia 
                //    2: Firma Electronica Simple con PFX + PSW Abajo 
                //    3: Servicio Firma e-Sign Abajo
                if (dtype.SignatureType == 1 || dtype.SignatureType == 2)
                {
                    string pathpfx;
                    string pswpfx;
                    string imageSig;

                    if (dtype.SignatureType == 2) //2: Firma Electronica Simple con PFX + PSW Abajo 
                    {
                        pathpfx = dtype.CertificatePFX;
                        pswpfx = dtype.SignaturePSW;
                        imageSig = (String.IsNullOrEmpty(dtype.ImageSignatureToShow)) ?
                                    Properties.Settings.Default.PathTemplatesSignatures + "\\SignatureNoImage.jpg"
                                    : dtype.ImageSignatureToShow;
                    }
                    else  //1: Firma Electronica Simple con PFX + PSW Abajo Default Compañia 
                    {
                        pathpfx = (Global.DOCUMENT_TYPES.GetCompany(param.Company)).CertificatePFX;
                        pswpfx = (Global.DOCUMENT_TYPES.GetCompany(param.Company)).SignaturePSW;
                        imageSig = (String.IsNullOrEmpty((Global.DOCUMENT_TYPES.GetCompany(param.Company)).ImageSignatureToShow)) ?
                                    Properties.Settings.Default.PathTemplatesSignatures + "\\SignatureNoImage.jpg"
                                    : (Global.DOCUMENT_TYPES.GetCompany(param.Company)).ImageSignatureToShow;
                    }

                    Pkcs12Store ks = new Pkcs12Store();
                    ks.Load(new FileStream(pathpfx, FileMode.Open), pswpfx.ToCharArray());
                    String alias = "";
                    foreach (string al in ks.Aliases)
                    {
                        if (ks.IsKeyEntry(al) && ks.GetKey(al).Key.IsPrivate)
                        {
                            alias = al;
                            break;
                        }
                    }
                    AsymmetricKeyParameter pk = ks.GetKey(alias).Key;
                    ICollection<X509Certificate> chain = new List<X509Certificate>();
                    foreach (X509CertificateEntry entry in ks.GetCertificateChain(alias))
                    {
                        chain.Add(entry.Certificate);
                    }
                    ////Agrego template en blanco
                    //listFiles = new List<byte[]>();
                    //listFiles.Add(deout);
                    //fileAux3 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_FES);
                    ////Firmo
                    //deout = null;
                    //PDFHelper.SignPDF(fileAux3, chain, pk, DigestAlgorithms.SHA256, CryptoStandard.CMS, "Firmado para evitar modificacion!",
                    //                 "Chile", null, null, null, 0, imageSig, 150, 550, 550, 680, out deout);
                    fileAux = null;
                    //Defino si se agrega hoja o se firma en la ultima hoja existente
                    if ((dtype.SignatureType == 1 && Global.DOCUMENT_TYPES.GetCompany(param.Company).FESShowType == 0) ||
                        (dtype.SignatureType == 2 && dtype.FESShowType == 0))
                    {
                        //Agrego template en blanco
                        listFiles = new List<byte[]>();
                        listFiles.Add(deout);
                        fileAux3 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_FES);
                    } else
                    {
                        fileAux3 = deout;  //Si va eFES en misma firma sigo con el doc como esta
                    }

                    //Firmo
                    if (dtype.SignatureType == 1)
                    {
                        PDFHelper.SignPDF(fileAux3, chain, pk, DigestAlgorithms.SHA256, CryptoStandard.CMS,
                                          Global.DOCUMENT_TYPES.GetCompany(param.Company).FESMsgToShow,
                                         "Chile", null, null, null, 0, imageSig,
                                         Global.DOCUMENT_TYPES.GetCompany(param.Company).FESx,
                                         Global.DOCUMENT_TYPES.GetCompany(param.Company).FESy,
                                         Global.DOCUMENT_TYPES.GetCompany(param.Company).FESw,
                                         Global.DOCUMENT_TYPES.GetCompany(param.Company).FESh, out fileAux);
                    }
                    else  //dtype.SignatureType == 2
                    {
                        PDFHelper.SignPDF(fileAux3, chain, pk, DigestAlgorithms.SHA256, CryptoStandard.CMS,
                                          dtype.FESMsgToShow, "Chile", null, null, null, 0, imageSig,
                                         dtype.FESx, dtype.FESy, dtype.FESw, dtype.FESh, out fileAux);
                    }
                    deout = fileAux;
                } else if (dtype.SignatureType == 3)
                {
                    listFiles = new List<byte[]>();
                    listFiles.Add(deout);
                    fileAux3 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_FEA);
                    //Firmo
                    deout = null;
                    string pdfsigned = null;
                    int retsig = FEAHelper.SignCloud(param.Company, seed + ".pdf", dtype, Convert.ToBase64String(fileAux3), out pdfsigned);
                    if (retsig != 0)
                    {
                        msgerr = "Error agregando FEA Cloud [retsig=" + retsig + "]";
                        LOG.Warn("DocumentHelper.GenerateDE Error [" + msgerr + "]");
                        ret = Errors.RET_ERR_SIGNING_PDF_FEA_SIG;
                    } else
                    {
                        deout = Convert.FromBase64String(pdfsigned);
                    }
                }

                ////6.- Elimino datos temporales generados en HDD
                //LOG.Debug("DocumentHelper.GenerateDE Clean tmp...");
                //DeleteTemporalFiles(seed);
                //LOG.Info("   >>> Finish Generate in Word - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")); 
                
            }
            catch (Exception ex)
            {
                msgerr = ex.Message;
                LOG.Debug("DocumentHelper.GenerateDE Error [" + ex.Message + "]");
                ret = Errors.RET_ERR_UNKKNOW;
            }
            LOG.Debug("DocumentHelper.GenerateDE End!");
            return ret;
        }


        /// <summary>
        /// Genera un PDf desde una plantilla Word cambiando los TAGs definidos por los valores enviados
        /// pero desde un doc generado via API
        /// </summary>
        /// <param name="param"></param>
        /// <param name="seed"></param>
        /// <param name="tagsprocessed"></param>
        /// <param name="deout"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        //internal int GenerateDE(string trackidde, List<Api.Model.TagProcessed> tagsprocessed, out byte[] deout, out string msgerr)
        internal int GenerateDE(DeDocument docIn, List<Api.Model.TagProcessed> tagsprocessed, 
                                out byte[] deout, out string msgerr, int _idcompany = 0, int _idtemplate = 0)
        {
            int ret = Errors.RET_OK;
            deout = null;
            msgerr = null;
            object oMissing = Type.Missing;
            object readOnly = null;
            object oVisible = null;
            try
            {
                LOG.Info("   >>> Start Generate in Word - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                LOG.Debug("DocumentHelper.GenerateDE Start...");

                //0.- Recupero DE para tener la info necesaria para procesar.
                DeDocument _DeDoc = docIn; // AdministradorDeDocument.BuscarDocumentosByTrackId(trackidde, true);

                //1.- Recupero Dtype para tener 
                if (_idcompany == 0 || _idtemplate == 0)
                {
                    _idcompany = _DeDoc.Company.Id;
                    _idtemplate = _DeDoc.DeTemplate.Id;
                }
                Api.Model.DocumentType dtype = Global.DOCUMENT_TYPES.GetDEType(_idcompany, _idtemplate);
                if (dtype == null)
                {
                    msgerr = "No existe documento asociado a clave [" + (_idcompany.ToString() + "-" +
                              _idtemplate.ToString()) + "]";
                    LOG.Warn("DocumentHelper.GenerateDE Error [" + msgerr + "]");
                    ret = Errors.RET_ERR_DE_CONFIG_NOT_EXIST;
                }
                else
                {
                    LOG.Debug("DocumentHelper.GenerateDE Dtype Obtenido = " + _idcompany.ToString() + "-" +
                                _idtemplate.ToString() + "-" + dtype.Name + "...");
                }

                //2.- Levanto Template de disco
                LOG.Debug("DocumentHelper.GenerateDE Word Zone - IN...");
                //Microsoft.Office.Interop.Word._Application oWord;
                //Microsoft.Office.Interop.Word.Document oDoc;
                DocumentCore dc = DocumentCore.Load(dtype.Template);

                LOG.Debug("DocumentHelper.GenerateDE Word Ax Zone - New Application...");
                //oWord = new Microsoft.Office.Interop.Word.Application();
                //oWord.Visible = false;
                object filename = dtype.Template;
                LOG.Debug("DocumentHelper.GenerateDE Open template = " + filename);
                //oDoc = oWord.Documents.Open(ref filename,
                //                            ref oMissing, ref readOnly, ref oMissing, ref oMissing, ref oMissing,
                //                            ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                //                            ref oVisible, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

                //3.- Reemplazo Tags
                LOG.Debug("DocumentHelper.GenerateDE Reemplazando Tgas...");
                int tagsreemplazadas = 0;
                short w, h, pos, x, y;
                for (int i = 0; i < tagsprocessed.Count; i++)
                {
                    LOG.Debug("DocumentHelper.GenerateDE Replace " + tagsprocessed[i].IdTag.Trim() + " => " + (string)tagsprocessed[i].value);
                    switch (tagsprocessed[i].Type)
                    {
                        case 0:
                        case 1:
                        case 2:
                            //tagsreemplazadas += InsertarTexto(oDoc, tagsprocessed[i].IdTag.Trim(), (string)tagsprocessed[i].value);
                            tagsreemplazadas += InsertarTexto(dc, tagsprocessed[i].IdTag.Trim(), (string)tagsprocessed[i].value);
                            break;
                        case 3:
                        case 4:
                            //tagsreemplazadas += ReemplazarImagen(oDoc, tagsprocessed[i].IdTag.Trim(), (string)tagsprocessed[i].value);
                            ExtractParams(tagsprocessed[i].Parameters, out w, out h, out pos, out x, out y);
                            tagsreemplazadas += ReemplazarImagen(dc, tagsprocessed[i].IdTag.Trim(), (string)tagsprocessed[i].value, w, h, pos, x, y);
                            break;
                        default:
                            break;
                    }
                }
                LOG.Debug("DocumentHelper.GenerateDE Fin reemplazo Tags! Total por reemplazar = " + tagsprocessed.Count + " - Total reemplazadas = " + tagsreemplazadas);

                //4.- Grabo en PDF
                string pathToSave = Properties.Settings.Default.PathTemp + _DeDoc.Trackid + ".pdf";
                LOG.Debug("DocumentHelper.GenerateDE Path to Save =" + pathToSave);
                //object fileFormat = WdSaveFormat.wdFormatPDF;
                LOG.Debug("DocumentHelper.GenerateDE Save dc.Save...");
                dc.Save(pathToSave, SaveOptions.PdfDefault);
                //oDoc.SaveAs2(ref pathToSave,
                //            ref fileFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                //            ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                //            ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);
                System.Threading.Thread.Sleep(500);
                object refSaveShanges = false;
                //oWord.Documents.Close(ref refSaveShanges, ref oMissing, ref oMissing);
                //refSaveShanges = false;
                LOG.Debug("DocumentHelper.GenerateDE Word Ax Zone - Out!");
                //oWord.Quit(ref refSaveShanges, ref oMissing, ref oMissing);
                //oWord = null;
                //oDoc = null;
                //5.- Leo desde HHD el PDF
                LOG.Debug("DocumentHelper.GenerateDE read Document generated...");
                deout = System.IO.File.ReadAllBytes((string)pathToSave);

                //6.- Agrego las BS si vienen. Esto lo agrego el 17/11/2018 para poder firmar con FE3D generando desde Word
                bool _hayError = false;
                //LOG.Debug("DocumentHelper.GenerateDE ListBS.length = " + (param.BSSignatures != null ? param.BSSignatures.Count.ToString() : "null"));

                List<byte[]> listFiles;
                //listFiles = new List<byte[]>();
                byte[] fileAux = deout;
                //listFiles.Add(fileAux);
                byte[] fileAux1 = null;
                byte[] fileAux2 = null;
                byte[] fileAux3 = null;

                //if (param.BSSignatures != null && param.BSSignatures.Count > 0)
                //{
                //    LOG.Debug("DocumentHelper.GenerateDE Insertando Biometrica Signatures...");
                //    foreach (BS itemBS in param.BSSignatures)
                //    {
                //        LOG.Debug("DocumentHelper.GenerateDE Insertando BSId = " + itemBS.BSId + "...");

                //        if (!String.IsNullOrEmpty(itemBS.NVreceiptId))
                //        {
                //            fileAux1 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_NV);
                //        }
                //        else
                //        {
                //            fileAux1 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_FES);
                //        }
                //        fileAux2 = PDFHelper.AddABS(fileAux1, itemBS);
                //        fileAux1 = fileAux2;
                //        _hayError = (fileAux1 == null);
                //        if (!_hayError) LOG.Debug("DocumentHelper.GenerateDE BS Added...");
                //        else break;
                //    }
                //    LOG.Debug("DocumentHelper.GenerateDE Biometrica Signatures Insertados!");
                //}
                //else
                //{
                //    fileAux1 = fileAux;
                //}

                //if (_hayError)
                //{
                //    msgerr = "Error agregando BSs en documento generado";
                //    LOG.Warn("DocumentHelper.GenerateDE Error [" + msgerr + "]");
                //    ret = Errors.RET_ERR_ADDING_PDF_TEMPLATE_SIG;
                //}
                //else
                //{
                //    deout = fileAux1;
                //}

                //3.- Levanto Template de disco de acuerdo al tipo de firma configurada
                LOG.Debug("DocumentHelper.GenerateDE Chequeando que tipo de firma se hará, para agregar template...");
                LOG.Debug("DocumentHelper.GenerateDE SignatureType = " + dtype.SignatureType + "...");
                //    0:Sin Firmar 
                //    1: Firma Electronica Simple con PFX + PSW Abajo Default Compañia 
                //    2: Firma Electronica Simple con PFX + PSW Abajo 
                //    3: Servicio Firma e-Sign Abajo
                if (dtype.SignatureType == 1 || dtype.SignatureType == 2)
                {
                    string pathpfx;
                    string pswpfx;
                    string imageSig;

                    if (dtype.SignatureType == 2) //2: Firma Electronica Simple con PFX + PSW Abajo 
                    {
                        pathpfx = dtype.CertificatePFX;
                        pswpfx = dtype.SignaturePSW;
                        imageSig = (String.IsNullOrEmpty(dtype.ImageSignatureToShow)) ?
                                    Properties.Settings.Default.PathTemplatesSignatures + "\\SignatureNoImage.jpg"
                                    : dtype.ImageSignatureToShow;
                    }
                    else  //1: Firma Electronica Simple con PFX + PSW Abajo Default Compañia 
                    {
                        pathpfx = (Global.DOCUMENT_TYPES.GetCompany(_DeDoc.Company.Id)).CertificatePFX;
                        pswpfx = (Global.DOCUMENT_TYPES.GetCompany(_DeDoc.Company.Id)).SignaturePSW;
                        imageSig = (String.IsNullOrEmpty((Global.DOCUMENT_TYPES.GetCompany(_DeDoc.Company.Id)).ImageSignatureToShow)) ?
                                    Properties.Settings.Default.PathTemplatesSignatures + "\\SignatureNoImage.jpg"
                                    : (Global.DOCUMENT_TYPES.GetCompany(_DeDoc.Company.Id)).ImageSignatureToShow;
                    }

                    Pkcs12Store ks = new Pkcs12Store();
                    ks.Load(new FileStream(pathpfx, FileMode.Open), pswpfx.ToCharArray());
                    String alias = "";
                    foreach (string al in ks.Aliases)
                    {
                        if (ks.IsKeyEntry(al) && ks.GetKey(al).Key.IsPrivate)
                        {
                            alias = al;
                            break;
                        }
                    }
                    AsymmetricKeyParameter pk = ks.GetKey(alias).Key;
                    ICollection<X509Certificate> chain = new List<X509Certificate>();
                    foreach (X509CertificateEntry entry in ks.GetCertificateChain(alias))
                    {
                        chain.Add(entry.Certificate);
                    }
                    ////Agrego template en blanco
                    listFiles = new List<byte[]>();
                    //listFiles.Add(deout);
                    //fileAux3 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_FES);
                    ////Firmo
                    //deout = null;
                    //PDFHelper.SignPDF(fileAux3, chain, pk, DigestAlgorithms.SHA256, CryptoStandard.CMS, "Firmado para evitar modificacion!",
                    //                 "Chile", null, null, null, 0, imageSig, 150, 550, 550, 680, out deout);
                    fileAux = null;
                    //Defino si se agrega hoja o se firma en la ultima hoja existente
                    if ((dtype.SignatureType == 1 && Global.DOCUMENT_TYPES.GetCompany(_DeDoc.Company.Id).FESShowType == 0) ||
                        (dtype.SignatureType == 2 && dtype.FESShowType == 0))
                    {
                        //Agrego template en blanco
                        listFiles = new List<byte[]>();
                        listFiles.Add(deout);
                        fileAux3 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_FES);
                    }
                    else
                    {
                        fileAux3 = deout;  //Si va eFES en misma firma sigo con el doc como esta
                    }

                    //Firmo
                    if (dtype.SignatureType == 1)
                    {
                        PDFHelper.SignPDF(fileAux3, chain, pk, DigestAlgorithms.SHA256, CryptoStandard.CMS,
                                          Global.DOCUMENT_TYPES.GetCompany(_DeDoc.Company.Id).FESMsgToShow,
                                         "Chile", null, null, null, 0, imageSig,
                                         Global.DOCUMENT_TYPES.GetCompany(_DeDoc.Company.Id).FESx,
                                         Global.DOCUMENT_TYPES.GetCompany(_DeDoc.Company.Id).FESy,
                                         Global.DOCUMENT_TYPES.GetCompany(_DeDoc.Company.Id).FESw,
                                         Global.DOCUMENT_TYPES.GetCompany(_DeDoc.Company.Id).FESh, out fileAux);
                    }
                    else  //dtype.SignatureType == 2
                    {
                        PDFHelper.SignPDF(fileAux3, chain, pk, DigestAlgorithms.SHA256, CryptoStandard.CMS,
                                          dtype.FESMsgToShow, "Chile", null, null, null, 0, imageSig,
                                         dtype.FESx, dtype.FESy, dtype.FESw, dtype.FESh, out fileAux);
                    }
                    deout = fileAux;
                }
                //else if (dtype.SignatureType == 3)
                //{
                //    listFiles = new List<byte[]>();
                //    listFiles.Add(deout);
                //    fileAux3 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_FEA);
                //    //Firmo
                //    deout = null;
                //    string pdfsigned = null;
                //    int retsig = FEAHelper.SignCloud(param.Company, seed + ".pdf", dtype, Convert.ToBase64String(fileAux3), out pdfsigned);
                //    if (retsig != 0)
                //    {
                //        msgerr = "Error agregando FEA Cloud [retsig=" + retsig + "]";
                //        LOG.Warn("DocumentHelper.GenerateDE Error [" + msgerr + "]");
                //        ret = Errors.RET_ERR_SIGNING_PDF_FEA_SIG;
                //    }
                //    else
                //    {
                //        deout = Convert.FromBase64String(pdfsigned);
                //    }
                //}

                ////6.- Elimino datos temporales generados en HDD
                //LOG.Debug("DocumentHelper.GenerateDE Clean tmp...");
                //DeleteTemporalFiles(seed);
                //LOG.Info("   >>> Finish Generate in Word - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")); 

            }
            catch (Exception ex)
            {
                msgerr = ex.Message;
                LOG.Debug("DocumentHelper.GenerateDE Error [" + ex.Message + "]");
                ret = Errors.RET_ERR_UNKKNOW;
            }
            LOG.Debug("DocumentHelper.GenerateDE End!");
            return ret;
        }

        #endregion WORD

        #region PDF

        /// <summary>
        /// Firma un PDf enviado, con BS o FES o FEA
        /// </summary>
        /// <param name="param"></param>
        /// <param name="seed"></param>
        /// <param name="deout"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal int SigneDE(XmlParamIn param, string seed, out byte[] deout, out string msgerr)
        {
            int ret = Errors.RET_OK;
            deout = null;
            msgerr = null;
            object oMissing = Type.Missing;
            object readOnly = null;
            object oVisible = null;
            byte[] fileAux;
            byte[] fileAux1;
            byte[] fileAux2;
            byte[] fileAux3;

            try
            {
                LOG.Info("   >>> Start Sign in PDF - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                LOG.Debug("DocumentHelper.SigneDE Start...");

                //1.- Recupero Dtype para tener 
                Api.Model.DocumentType dtype = Global.DOCUMENT_TYPES.GetDEType(param.Company, param.IdDE);
                if (dtype == null)
                {
                    msgerr = "No existe documento asociado a clave [" + (param.Company.ToString() + "-" + param.IdDE.ToString()) + "]";
                    ret = Errors.RET_ERR_DE_CONFIG_NOT_EXIST;
                }
                else
                {
                    LOG.Debug("DocumentHelper.SigneDE Dtype Obtenido = " + param.Company.ToString() + "-" + param.IdDE.ToString() + "-" + dtype.Name + "...");
                }

                //2.- Reviso si hay BS (Biometric Signatures) para agregar templates y firmas al doc en oja diferente
                //    por cada BS. Si hay NV tambien se pone en cada pagina
                LOG.Debug("DocumentHelper.SigneDE Chequeando si hay BS, para agregar template...");
                LOG.Debug("DocumentHelper.SigneDE BSSignatures = " + (param.BSSignatures!=null?param.BSSignatures.Count.ToString():"0") + "...");
                List<byte[]> listFiles;
                listFiles = new List<byte[]>();
                fileAux = Convert.FromBase64String(param.DE);
                listFiles.Add(fileAux);
                fileAux1 = null;
                fileAux2 = null;
                fileAux3 = null;

                if (param.BSSignatures != null && param.BSSignatures.Count > 0)
                {
                    LOG.Debug("DocumentHelper.SigneDE Insertando Biometrica Signatures...");
                    foreach (BS itemBS in param.BSSignatures)
                    {
                        LOG.Debug("DocumentHelper.SigneDE Insertando BSId = " + itemBS.BSId + "...");
                        
                        if (!String.IsNullOrEmpty(itemBS.NVreceiptId))
                        {
                            fileAux1 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_NV);
                        } else {
                            fileAux1 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_FES);
                        }
                        fileAux2 = PDFHelper.AddABS(fileAux1, itemBS);
                        fileAux1 = fileAux2;
                    }
                    LOG.Debug("DocumentHelper.SigneDE Biometrica Signatures Insertados!");
                }

                if (fileAux2 == null || fileAux2.Length == 0)
                {
                    msgerr = "No se pudo agregar template de BS. revise log...";
                    LOG.Debug("DocumentHelper.SigneDE Sale de signDE => " + msgerr);
                    ret = Errors.RET_ERR_ADDING_PDF_TEMPLATE_SIG;
                }

                //3.- Levanto Template de disco de acuerdo al tipo de firma configurada
                LOG.Debug("DocumentHelper.SigneDE Chequeando que tipo de firma se hará, para agregar template...");
                LOG.Debug("DocumentHelper.SigneDE SignatureType = " + dtype.SignatureType + "...");
                //    0:Sin Firmar 
                //    1: Firma Electronica Simple con PFX + PSW Abajo Default Compañia 
                //    2: Firma Electronica Simple con PFX + PSW Abajo 
                //    3: Servicio Firma e-Sign Abajo
                if (dtype.SignatureType == 1 || dtype.SignatureType == 2) {
                    string pathpfx;
                    string pswpfx;
                    string imageSig;

                    if (dtype.SignatureType == 2) //2: Firma Electronica Simple con PFX + PSW Abajo 
                    {
                        pathpfx = dtype.CertificatePFX;
                        pswpfx = dtype.SignaturePSW;
                        imageSig = (String.IsNullOrEmpty(dtype.ImageSignatureToShow)) ?
                                    Properties.Settings.Default.PathTemplatesSignatures + "\\SignatureNoImage.jpg"
                                    : dtype.ImageSignatureToShow;
                    }
                    else  //1: Firma Electronica Simple con PFX + PSW Abajo Default Compañia 
                    {
                        pathpfx = (Global.DOCUMENT_TYPES.GetCompany(param.Company)).CertificatePFX;
                        pswpfx = (Global.DOCUMENT_TYPES.GetCompany(param.Company)).SignaturePSW;
                        imageSig = (String.IsNullOrEmpty((Global.DOCUMENT_TYPES.GetCompany(param.Company)).ImageSignatureToShow)) ?
                                    Properties.Settings.Default.PathTemplatesSignatures + "\\SignatureNoImage.jpg"
                                    : (Global.DOCUMENT_TYPES.GetCompany(param.Company)).ImageSignatureToShow;
                    }

                    Pkcs12Store ks = new Pkcs12Store();
                    ks.Load(new FileStream(pathpfx, FileMode.Open), pswpfx.ToCharArray());
                    String alias = "";
                    foreach (string al in ks.Aliases)
                    {
                        if (ks.IsKeyEntry(al) && ks.GetKey(al).Key.IsPrivate)
                        {
                            alias = al;
                            break;
                        }
                    }
                    AsymmetricKeyParameter pk = ks.GetKey(alias).Key;
                    ICollection<X509Certificate> chain = new List<X509Certificate>();
                    foreach (X509CertificateEntry entry in ks.GetCertificateChain(alias))
                    {
                        chain.Add(entry.Certificate);
                    }

                    fileAux = null;
                    //Defino si se agrega hoja o se firma en la ultima hoja existente
                    if ((dtype.SignatureType == 1 && Global.DOCUMENT_TYPES.GetCompany(param.Company).FESShowType == 0) ||
                        (dtype.SignatureType == 2 && dtype.FESShowType == 0))
                    {
                        //Agrego template en blanco
                        listFiles = new List<byte[]>();
                        listFiles.Add(fileAux2);
                        fileAux3 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_FES);
                    }

                    //Firmo
                    if (dtype.SignatureType == 1)
                    {
                        PDFHelper.SignPDF(fileAux3, chain, pk, DigestAlgorithms.SHA256, CryptoStandard.CMS,
                                          Global.DOCUMENT_TYPES.GetCompany(param.Company).FESMsgToShow,
                                         "Chile", null, null, null, 0, imageSig,
                                         Global.DOCUMENT_TYPES.GetCompany(param.Company).FESx,
                                         Global.DOCUMENT_TYPES.GetCompany(param.Company).FESy,
                                         Global.DOCUMENT_TYPES.GetCompany(param.Company).FESw,
                                         Global.DOCUMENT_TYPES.GetCompany(param.Company).FESh, out fileAux);
                    }
                    else  //dtype.SignatureType == 2
                    {
                        PDFHelper.SignPDF(fileAux3, chain, pk, DigestAlgorithms.SHA256, CryptoStandard.CMS,
                                          dtype.FESMsgToShow, "Chile", null, null, null, 0, imageSig,
                                         dtype.FESx, dtype.FESy, dtype.FESw, dtype.FESh, out fileAux);
                    }
                }
                else if (dtype.SignatureType == 3)
                {
                    listFiles = new List<byte[]>();
                    listFiles.Add(fileAux2);
                    fileAux3 = PDFHelper.AddTemplateBlankSignature(listFiles, Api.Constant.SIGNATURE_TEMPLATE_FEA);
                    //Firmo
                    deout = null;
                    string pdfsigned = null;
                    int retsig = FEAHelper.SignCloud(param.Company, seed + ".pdf", dtype, Convert.ToBase64String(fileAux3), out pdfsigned);
                    if (retsig != 0)
                    {
                        msgerr = "Error agregando FEA Cloud [retsig=" + retsig + "]";
                        LOG.Warn("DocumentHelper.SigneDE Error [" + msgerr + "]");
                        ret = Errors.RET_ERR_SIGNING_PDF_FEA_SIG;
                    }
                    else
                    {
                        fileAux = Convert.FromBase64String(pdfsigned);
                    }
                }

                //4.- Devuelvo en PDF
                LOG.Debug("DocumentHelper.SigneDE Asign Document generated...");
                deout = fileAux;

                //6.- Elimino datos temporales generados en HDD
                LOG.Debug("DocumentHelper.SigneDE Clean tmp...");
                DeleteTemporalFiles(seed);
                fileAux3 = null;
                fileAux2 = null;
                fileAux1 = null;
                fileAux = null;

                LOG.Info("   >>> Finish SigneDE PDF - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

            }
            catch (Exception ex)
            {
                msgerr = ex.Message;
                LOG.Debug("DocumentHelper.SigneDE Error [" + ex.Message + "]");
                ret = Errors.RET_ERR_UNKKNOW;
            }
            LOG.Debug("DocumentHelper.SigneDE End!");
            return ret;
        }


        #endregion PDF


        #region private methods

        internal void DeleteTemporalFiles(string seed)
        {
            try
            {
                string[] allFilesToDelete = System.IO.Directory.GetFiles(Properties.Settings.Default.PathTemp, seed + "*.*");
                for (int i = 0; i < allFilesToDelete.Length; i++)
                {
                    System.IO.File.Delete(allFilesToDelete[i]);
                }
                LOG.Debug("DocumentHelper.DeleteTemporalFiles Ok Delete Seed = " + seed);
            }
            catch (Exception ex)
            {
                LOG.Error("DocumentHelper.DeleteTemporalFiles Error borrando Seed = " + seed + " [Error=" + ex.Message + "]");
            }
        }

        private int ReemplazarImagen(DocumentCore documentoActivo, string textoReemplazar, string imagenReemplazo, short w, short h, short pos, short x, short y)
        {
            int ret = 0;
            try
            {
                LOG.Debug("DocumentHelper.ReemplazarImagen IN...");
                foreach (SautinSoft.Document.Paragraph par in documentoActivo.GetChildElements(true, ElementType.Paragraph))
                {
                    foreach (ContentRange item in par.Content.Find(textoReemplazar).Reverse())
                    {
                        LOG.Debug("DocumentHelper.ReemplazarImagen Reemplazando " + textoReemplazar + "...");
                        //SautinSoft.Document.Drawing.Picture img =
                        //    new SautinSoft.Document.Drawing.Picture(documentoActivo,
                        //    InlineLayout.Inline(new SautinSoft.Document.Drawing.Size(50, 50)), imagenReemplazo);
                        SautinSoft.Document.Drawing.Picture img = null;
                        if (pos == 0)  //Es InLine, mando a reemplazar justo donde encuentre el tag y desplaza resto para abajo
                        {
                            img = new SautinSoft.Document.Drawing.Picture(documentoActivo,
                                InlineLayout.Inline(new SautinSoft.Document.Drawing.Size(w, h)), imagenReemplazo);
                            item.Delete();
                            par.Inlines.Add(img);
                        } else if (pos == 1)  //=> es Floating por lo que agrego reemplazo con coordenadas absolutas para no desplazar texto restante
                        {
                            FloatingLayout fl = InlineLayout.Floating(new HorizontalPosition(x, LengthUnit.Millimeter, HorizontalPositionAnchor.Page),
                                                                  new VerticalPosition(y, LengthUnit.Millimeter, VerticalPositionAnchor.Page),
                                                                  new SautinSoft.Document.Drawing.Size(w, h));
                            fl.WrappingStyle = WrappingStyle.InFrontOfText;

                            img = new SautinSoft.Document.Drawing.Picture(documentoActivo,fl, imagenReemplazo);
                            item.Replace(img.Content);
                        }
                       
                    }
                }
                ret = 1; //Para contar las reemplazadas
            }
            catch (Exception ex)
            {
                LOG.Error("DocumentHelper.ReemplazarImagen Error - " + ex.Message);
                ret = 0;
            }
            LOG.Debug("DocumentHelper.ReemplazarImagen OUT!");
            return ret;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentoActivo"></param>
        /// <param name="textoReemplazar"></param>
        /// <param name="textoReemplazo"></param>
        /// <returns></returns>
        private int ReemplazarTexto(DocumentCore documentoActivo, string textoReemplazar, string textoReemplazo)
        {
            int iReemp = 1;
            int ret = 0;
            try
            {
                LOG.Debug("DocumentHelper.ReemplazarTexto IN...");
                foreach (SautinSoft.Document.Paragraph par in documentoActivo.GetChildElements(true, ElementType.Paragraph))
                {
                    foreach (ContentRange item in par.Content.Find(textoReemplazar).Reverse())
                    {
                        LOG.Debug("DocumentHelper.ReemplazarTexto Reemplazo => " + iReemp++);
                        item.Replace(textoReemplazo); //, new CharacterFormat() { FontColor = Color.Red } );
                    }
                }

                ret = 1; //Para contar las reemplazadas
            }
            catch (Exception ex)
            {
                LOG.Error("DocumentHelper.ReemplazarTexto Error - " + ex.Message);
                ret = 0;
            }
            LOG.Debug("DocumentHelper.ReemplazarTexto OUT!");
            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentoActivo"></param>
        /// <param name="textoReemplazar"></param>
        /// <param name="textoReemplazo"></param>
        /// <returns></returns>
        private int InsertarTexto(DocumentCore documentoActivo, string textoReemplazar, string textoReemplazo)
        {
            int ret = 0;
            int iCant = 1;
            try
            {
                LOG.Debug("DocumentHelper.InsertarTexto IN...");
                var restOfText = textoReemplazo;
                while (restOfText.Length > 100)
                {
                    LOG.Debug("DocumentHelper.InsertarTexto Corte = " + iCant++);
                    var firstTwentyChars = restOfText.Substring(0, 100);
                    firstTwentyChars += textoReemplazar;
                    restOfText = restOfText.Substring(100);
                    ReemplazarTexto(documentoActivo, textoReemplazar, firstTwentyChars);
                }
                ReemplazarTexto(documentoActivo, textoReemplazar, restOfText);

                ret = 1; //Para contar las reemplazadas

            }
            catch (Exception ex)
            {
                LOG.Error("DocumentHelper.InsertarTexto Error - " + ex.Message);
                ret = 0;
            }
            LOG.Debug("DocumentHelper.InsertarTexto OUT!");
            return ret;
        }

        private bool ExtractParams(string parameters, out short w, out short h, out short pos, out short x, out short y)
        {
            w = 0;
            h = 0;
            pos = 0;
            x = 0;
            y = 0;
            try
            {
                if (String.IsNullOrEmpty(parameters)) return false;

                string[] arrP1 = parameters.Split(new string[] { "|" }, StringSplitOptions.None);
                string sW = arrP1[0];
                string[] sWArr = sW.Split(new string[] { "=" }, StringSplitOptions.None);
                w = (short)Convert.ToInt32(sWArr[1]);

                string sH = arrP1[1];
                string[] sHArr = sH.Split(new string[] { "=" }, StringSplitOptions.None);
                h = (short)Convert.ToInt32(sHArr[1]);

                if (arrP1.Length > 2) //=> Parseo porque puede ser Floating la imagen
                {
                    string sPos = arrP1[2];
                    string[] sPosArr = sPos.Split(new string[] { "=" }, StringSplitOptions.None);
                    pos = (short)Convert.ToInt32(sPosArr[1]);

                    if (pos > 0) //Solo parseo si es 1 porque seria Floating y necesito el X,Y sino dejo en 0 porque no se usa es InLine
                    {
                        string sX = arrP1[3];
                        string[] sXArr = sX.Split(new string[] { "=" }, StringSplitOptions.None);
                        x = (short)Convert.ToInt32(sXArr[1]);
                        string sY = arrP1[4];
                        string[] sYArr = sY.Split(new string[] { "=" }, StringSplitOptions.None);
                        y = (short)Convert.ToInt32(sYArr[1]);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DocumentTypesConfig.GetObjectFromTag Error " + ex.Message);
                return false;
            }
            return true;
        }

        //private int ReemplazarImagen(Microsoft.Office.Interop.Word.Document documentoActivo, string textoReemplazar,
        //                              string imagenReemplazo)
        //{
        //    try
        //    {
        //        Object nullobj = Type.Missing;
        //        Object start = 0;
        //        Object end = documentoActivo.Characters.Count;

        //        Microsoft.Office.Interop.Word.Range rng = documentoActivo.Range(ref start, ref end);
        //        Microsoft.Office.Interop.Word.Find fnd = rng.Find;

        //        fnd.ClearFormatting();

        //        fnd.Text = textoReemplazar;
        //        fnd.Forward = true;

        //        Object linktoFile = false;
        //        Object SaveWithDoc = true;


        //        Object replaceOption = Microsoft.Office.Interop.Word.WdReplace.wdReplaceOne;
        //        Object range = Type.Missing;

        //        fnd.Execute(ref nullobj, ref nullobj, ref nullobj, ref nullobj,
        //        ref nullobj, ref nullobj, ref nullobj, ref nullobj,
        //        ref nullobj, ref nullobj, ref replaceOption, ref nullobj,
        //        ref nullobj, ref nullobj, ref nullobj);

        //        //Insertamos la imagen en la posicion adecuada
        //        string imagento = imagenReemplazo;
        //        rng.InlineShapes.AddPicture(imagento, ref linktoFile, ref SaveWithDoc, ref range);
        //        return 1; //PAra contar las reemplazadas
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("DocumentHelper.ReemplazarImagen Error - " + ex.Message);
        //        return 0;
        //    }

        //}


        //private int ReemplazarTexto(Microsoft.Office.Interop.Word.Document documentoActivo, string textoReemplazar,
        //                            string textoReemplazo)
        //{
        //    try
        //    {
        //        Object nullobj = Type.Missing;
        //        Object start = 0;
        //        Object end = documentoActivo.Characters.Count;

        //        Microsoft.Office.Interop.Word.Range rng = documentoActivo.Range(ref start, ref end);
        //        Microsoft.Office.Interop.Word.Find fnd = rng.Find;

        //        fnd.ClearFormatting();

        //        fnd.Text = textoReemplazar;
        //        fnd.Forward = true;

        //        Object linktoFile = false;
        //        Object SaveWithDoc = true;
        //        Object oTextoReemplazo = textoReemplazo;

        //        Object replaceOption = Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll;
        //        Object range = Type.Missing;

        //        fnd.Execute(ref nullobj, ref nullobj, ref nullobj, ref nullobj,
        //                    ref nullobj, ref nullobj, ref nullobj, ref nullobj,
        //                    ref nullobj, ref oTextoReemplazo, ref replaceOption, ref nullobj,
        //                    ref nullobj, ref nullobj, ref nullobj);
        //        return 1; //PAra contar las reemplazadas
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("DocumentHelper.ReemplazarTexto Error - " + ex.Message);
        //        return 0;
        //    }

        //}

        //private int InsertarTexto(Microsoft.Office.Interop.Word.Document documentoActivo, string textoReemplazar,
        //            string textoReemplazo)
        //{
        //    try
        //    {
        //        var restOfText = textoReemplazo; 
        //        while (restOfText.Length > 100)
        //        {
        //            var firstTwentyChars = restOfText.Substring(0, 100);
        //            firstTwentyChars += textoReemplazar;
        //            restOfText = restOfText.Substring(100);
        //            ReemplazarTexto(documentoActivo, textoReemplazar, firstTwentyChars);
        //        }
        //        ReemplazarTexto(documentoActivo, textoReemplazar, restOfText);

        //        return 1; //Para contar las reemplazadas

        //    }
        //    catch (Exception ex)
        //    {
        //        string s = ex.Message;
        //        return 0;
        //    }

        //}

        #endregion private methods
    }
}