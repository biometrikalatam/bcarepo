﻿using Bio.Core.Notify;
using Biometrika.Document.Electronic.Database;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Biometrika.Document.Electronic.Core.libs
{
    public class Notify
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Notify));

        public static int SendNotify(DeDocument _Doc, IList<string> mailsToSend, bool addMailsToFromDeDoc, 
                                     string subject, string[] bodyLines,
                                     byte[] attach, string attachMimeType, string attachName)
        {
            int ret = 0;
            try
            {
                /*
                    1.- Con el Doc recolecta los mails a donde notificar toando desde: DeDoc, cada DeSignatory
                        listas de DeTemplate, y se agregan a los mailsToSend. Si ese parámetro llego nulo, se crea
                        la lista.
                    2.- Se define el template de mail a enviar:
                        i) Si tiene del DeTemplate se usa ese.
                        ii) Si no tiene i pero si la company, usamos ese
                        iii) Si i y ii no tienen usamos el default del producto.
                    3.- Se hace merge de datos con template y bodyLines
                    4.- Por cada mail de mailsToSend se envia mail
                */

                LOG.Debug("Notify.SendNotify IN...");
                //1.- Con el Doc recolecta los mails a donde notificar toando desde: DeDoc, cada DeSignatory
                //    listas de DeTemplate, y se agregan a los mailsToSend. Si ese parámetro llego nulo, se crea
                //    la lista.
                // TODO - Falta evaluar si se manda toda sla snotificaciones a todos o no.
                if (mailsToSend == null) mailsToSend = new List<string>();
                if (addMailsToFromDeDoc)
                {
                    LOG.Debug("Notify.SendNotify - Entra a recolectar mails para envio...");
                    if (_Doc != null)
                    {
                        LOG.Debug("Notify.SendNotify - Adding _Doc.Mailtonotify...");
                        if (!string.IsNullOrEmpty(_Doc.Mailtonotify))
                        {
                            string[] mailaux = _Doc.Mailtonotify.Split(',');
                            for (int i = 0; i < mailaux.Length; i++)
                            {
                                mailsToSend.Add(mailaux[i]);
                                LOG.Debug("Notify.SendNotify - Adding => " + mailaux[i]);
                            }
                        }
                        LOG.Debug("Notify.SendNotify - Adding _Doc.DeTemplate.Listnotifymails...");
                        if (_Doc.DeTemplate != null && !string.IsNullOrEmpty(_Doc.DeTemplate.Listnotifymails))
                        {
                            string[] mailaux = _Doc.DeTemplate.Listnotifymails.Split(',');
                            for (int i = 0; i < mailaux.Length; i++)
                            {
                                mailsToSend.Add(mailaux[i]);
                                LOG.Debug("Notify.SendNotify - Adding => " + mailaux[i]);
                            }
                        }
                        LOG.Debug("Notify.SendNotify - Adding _Doc.DeSignatory...");
                        if (_Doc.DeSignatory != null && _Doc.DeSignatory.Count > 0)
                        {
                            foreach (DeSignatory item in _Doc.DeSignatory)
                            {
                                if (!string.IsNullOrEmpty(item.Mail))
                                {
                                    mailsToSend.Add(item.Mail);
                                    LOG.Debug("Notify.SendNotify - Adding => " + item.Mail);
                                }
                            }
                        }
                        //if (_Doc.Company != null && !string.IsNullOrEmpty(_Doc.Company.Listnotifymails))
                        //{
                        //    string[] mailaux = _Doc.DeTemplate.Listnotifymails.Split(',');
                        //    for (int i = 0; i < mailaux.Length; i++)
                        //    {
                        //        mailsToSend.Add(mailaux[i]);
                        //        LOG.Debug("Notify.SendNotify - Adding => " + mailaux[i]);
                        //    }
                        //}
                    }
                    else
                    {
                        LOG.Warn("Notify.SendNotify - Se pidio agregar los mails desde DeDocument, pero está vacio DeDoc del param..:");
                    }
                }

                if (mailsToSend == null || mailsToSend.Count == 0)
                {
                    LOG.Warn("Notify.SendNotify - Sale de NOtify porque la lista de mailTo está vacia...");
                }
                else
                {
                    LOG.Debug("Notify.SendNotify - Ingreso a enviar " + mailsToSend.Count.ToString() + " mails...");

                    //2.- Se define el template de mail a enviar:
                    //  i) Si tiene del DeTemplate se usa ese.
                    //  ii) Si no tiene i pero si la company, usamos ese
                    //  iii) Si i y ii no tienen usamos el default del producto.
                    string _ConfigMail = Global.JSON_MAILGRAL_CONFIG;
                    if (_Doc.DeTemplate != null && !string.IsNullOrEmpty(_Doc.DeTemplate.Mailtemplate))
                    {
                        _ConfigMail = _Doc.DeTemplate.Mailtemplate;
                        LOG.Debug("Notify.SendNotify - Tomo Mail Config desde TemplateId = " + _Doc.DeTemplate.Id.ToString());
                    }
                    else if (_Doc.Company != null && !string.IsNullOrEmpty(_Doc.Company.Mailtemplate))
                    {
                        _ConfigMail = _Doc.Company.Mailtemplate;
                        LOG.Debug("Notify.SendNotify - Tomo Mail Config desde CompanyId = " + _Doc.Company.Id.ToString());
                    } else
                    {
                        LOG.Debug("Notify.SendNotify - Tomo Mail Config General!");
                    }
                    LOG.Debug("Notify.SendNotify - Deserializando config..."); 
                    Dictionary<string, string> dict =
                        JsonConvert.DeserializeObject<Dictionary<string, string>>(_ConfigMail);

                    //3.- Se hace merge de datos con template y bodyLines
                    LOG.Debug("Notify.SendNotify - Construyendo cuerpo...");
                    string strbody = "<table border=\"0\" width=\"95%\" align=\"center\" bgcolor=\"" + dict["mailColorBackgroungBody"] + "\"" + ">" +
                                         "<tr><td align=\"center\">" +
                                         "</td></tr>";
                    for (int i = 0; i < bodyLines.Length; i++)
                    {
                        strbody += "<tr><td><p style=\"font-family:'Arial'\">" + bodyLines[i] + "</p></td></tr>";
                    }
                    strbody += "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
                                    "<a href=\"http://www.biometrikalatam.com\" target=\"_blank\">Biometrika S.A.<a></p></td></tr>" +
                                    "<tr height=\"5\" bgcolor=\"" + dict["mailColorSeparatorLine"] + "\"><td></td></tr>" +
                                 "</table>";  //5882ba

                    string body2 = @"<table border=""0"" width=""95%"" align=""center"">" +
                                   @"<tr><td align=""" + dict["mailAlignImageHeader"] + "\"><a href=\"" + dict["mailLinkImageHeader"] + "\" target=\"_blank\">" +
                                   @"<img src=""cid:HDIImageH"" width=""" + dict["mailPercentImageHeader"] + "%\" /></a></td></tr></table>" + strbody +
                                   @"<table border=""0"" width=""95%"" align=""center"">" +
                                   @"<tr><td align=""center""><a href=""http://www.biometrikalatam.com"" target=""_blank"">" +
                                   @"<img src=""cid:HDIImageF""  width=""100%"" /></a></td></tr></table>";
                    body2 = @"<table border=""0"" width=""100%"" align=""center"" bgcolor=""" + dict["mailColorBackgroundPage"] + "\"><tr><td align=\"center\">" +
                            body2 +
                            "<tr><td></table>";

                    //4.- Por cada mail de mailsToSend se envia mail
                    LOG.Debug("Notify.SendNotify - Enviando mails...");
                    bool bret = NotifyByMail.SendMailHtml(Properties.Settings.Default.ServerSMTP, 587,
                                                          Properties.Settings.Default.UserSMTP,
                                                          Properties.Settings.Default.ClaveSMTP,
                                                          Properties.Settings.Default.MailDisplayName,
                                                          mailsToSend.ToArray(), subject,
                                                          body2, attach, attachMimeType, attachName,
                                                          dict["mailPathImageHeader"],
                                                          Global.LOGO_MAIL_FOOTER_GRAL);
                    LOG.Debug("Notify.SendNotify - Retorno envio OK = " + bret.ToString());
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Notify.SendNotify Excp Error: " + ex.Message);
            }
            return ret;
        }
    }
}