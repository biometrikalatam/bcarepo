using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using System.ComponentModel;
using Biometrika.Document.Electronic.Properties;

namespace Biometrika.Document.Electronic.libs
{
    public class Utils
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Utils));
        
        public static DateTime GetDateTime(string strdt, bool isMin)
        {
            DateTime dtret = DateTime.Parse("01/01/1950 00:00:00");
            try
            {
                if (isMin) dtret = DateTime.Parse("01/01/1950 00:00:00");
                else dtret = DateTime.Parse("01/01/2999 23:59:59");
                
                if (!String.IsNullOrEmpty(strdt))
                {
                    if (strdt.Trim().Length <= 10)
                    {
                        if (isMin) strdt = strdt + " 00:00:00";
                        else strdt = strdt + " 23:59:59";
                    }
                    dtret = DateTime.Parse(strdt);
                }
                
            }
            catch (Exception ex)
            {
                log.Error("Error Utils.GetDateTime", ex);
            }
            
            return dtret;
        }
        
        
        public static bool SendMail(string[] mailto, string subject, string body,
                                    byte[] attach, string mimetypeattach,
                                    string nombreattach)
        {
            bool ret = false;

            try
            {
                log.Debug("Utils.SendMail IN...");
                                    //save the data to a memory stream
                MemoryStream ms = new MemoryStream(attach);
                //Envio de Correo
                log.Debug("Utils.SendMail UserSMTP = " + Biometrika.Document.Electronic.Properties.Settings.Default.UserSMTP + " / " + Biometrika.Document.Electronic.Properties.Settings.Default.ClaveSMTP);
                MailAddress SendFrom = new MailAddress(Biometrika.Document.Electronic.Properties.Settings.Default.UserSMTP);
                log.Debug("Utils.SendMail SendTo = " + mailto[0]);
                MailAddress SendTo = new MailAddress(mailto[0]);
                MailMessage MyMessage = null;
                try
                {
                    SmtpClient emailClient = new SmtpClient(Biometrika.Document.Electronic.Properties.Settings.Default.ServerSMTP);
                    emailClient.Port = 587;
                    emailClient.EnableSsl = true;
                    NetworkCredential _Credential =  
                                new NetworkCredential(Biometrika.Document.Electronic.Properties.Settings.Default.UserSMTP, 
                                                        Biometrika.Document.Electronic.Properties.Settings.Default.ClaveSMTP, 
                                                        null);
                    MyMessage = new MailMessage(SendFrom, SendTo);
                    if (mailto.Length > 1)
                    {
                        for (int i = 0; i < mailto.Length; i++)
                        {
                            MyMessage.To.Add(new MailAddress(mailto[i]));
                            log.Debug("Utils.SendMail - Correo => Enviar a:" + mailto[i] + " desde : " + SendFrom);
                        }
                        //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.cl"));
                        //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.pe"));
                        //MyMessage.To.Add(new MailAddress("gsuhit@yahoo.com"));
                    }
                    ms.Position = 0;
                    string mt = MediaTypeNames.Application.Pdf;
                    if ((mimetypeattach == "docx") || (mimetypeattach == "doc"))
                        mt = MediaTypeNames.Application.Rtf;
                    else if ((mimetypeattach == "xls") || (mimetypeattach == "xlsx"))
                        mt = MediaTypeNames.Application.Octet;
                    else if ((mimetypeattach == "pdf"))
                        mt = MediaTypeNames.Application.Pdf;
                    else
                    mt = MediaTypeNames.Application.Octet;
                    MyMessage.Attachments.Add(new Attachment(ms, nombreattach, mt));
                    log.Debug("Utils.SendMail Subject = " + subject);
                    MyMessage.Subject = subject;
                    log.Debug("Utils.SendMail - Body = " + body);
                    MyMessage.Body = body;
                    emailClient.Credentials = _Credential;
                    try
                    {
                        log.Debug("Utils.SendMail Enviando...");
                        emailClient.Send(MyMessage);
                        log.Debug("Utils.SendMail Enviado sin error!");
                        ret = true;
                    }
                    catch (Exception ex)
                    {
                        log.Error("emailClient.Send(MyMessage)- Error", ex);
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    log.Error("emailClient.Send [" + ex.Message + "]");
                    ret = false;
                }

                if (ms != null)
                {
                    ms.Close();
                    ms.Dispose();
                }
            }
            catch (Exception ex)
            {
                log.Error("Utils.SendMail Error SendMail", ex);
                ret = false;
            }
            log.Debug("Utils.SendMail OUT!");
            return ret;
            
        }

        internal static void SendAllMailsByThread(byte[] byattachRD, byte[] byattachDE, Api.XmlParamIn param, Database.DeDocument documento)
        {
            try
            {
                log.Debug("SendAllMailsByThread Start send mails [total = " + param.MailsToNotify.Length + "]...");
                //for (int i = 0; i < param.MailsToNotify.Length; i++)
                //{
                    if (!Utils.SendMail(param.MailsToNotify,
                                        "Biometrika Document Electronic - Documento " + documento.Trackid,
                                        "Ha sido generado un documento asociado a usted. Se env�a a trav�s de " +
                                        "este mail, el documento generado", byattachDE, "pdf",
                                        documento.Trackid + ".pdf"))
                    {
                        for (int i = 0; i < param.MailsToNotify.Length; i++)
                        {
                            log.Warn("No se pudo enviar mail con documento id = " + documento.Trackid + " a " + param.MailsToNotify[i]);
                        }
                    }

                    if (byattachRD != null)
                    {
                        if (!Utils.SendMail(param.MailsToNotify,
                                            "Biometrika Manejador Documental - Documento " + documento.Trackid,
                                            "Ha sido generado un documento asociado a usted. Se env�a a trav�s de " +
                                            "este mail, el Recibo Digital del mismo.", byattachRD, "xml",
                                            documento.Trackid + ".xml"))
                        {
                            for (int i = 0; i < param.MailsToNotify.Length; i++)
                            {
                                log.Warn("No se pudo enviar mail con recibo de documento id = " + documento.Trackid + " a " + param.MailsToNotify[i]);
                            } 
                        }
                    //}
                }
                log.Debug("SendAllMailsByThread End send mails!");
            }
            catch (Exception ex)
            {
                log.Error("SendAllMailsByThread Error - " + ex.Message);
            }
        }

        internal static string GetValueOfProperty(object obj, string property, string format = null)
        {
            string ret = null;
            try
            {
                log.Debug("Utils.GetValueOfPropert IN...");
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
                {
                    if (descriptor.Name.Equals(property))
                    {
                        object objValue = descriptor.GetValue(obj);
                        if (objValue is DateTime && !string.IsNullOrEmpty(format))
                        {
                            ret = ((DateTime)objValue).ToString(format);
                        }
                        else if (objValue is double && !string.IsNullOrEmpty(format))
                        {
                            ret = ((double)objValue).ToString(format);
                        }
                        else
                        {
                            ret = descriptor.GetValue(obj).ToString();
                        }
                        log.Debug("Utils.GetValueOfPropert ret => property=" + property + " => value=" + ret);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                log.Error("Utils.GetValueOfProperty Excp Error: ", ex);
            }
            log.Debug("Utils.GetValueOfPropert OUT!");
            return ret;
        }

        //public static string GetValueOfProperty(object obj, string property)
        //{
        //    string ret = null;
        //    try
        //    {
        //        log.Debug("Utils.GetValueOfPropert IN...");
        //        foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
        //        {
        //            if (descriptor.Name.Equals(property))
        //            {
        //                ret = descriptor.GetValue(obj).ToString();
        //                log.Debug("Utils.GetValueOfPropert ret => property=" + property + " => value=" + ret);
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = null;
        //        log.Error("Utils.GetValueOfProperty Excp Error: ", ex);
        //    }
        //    log.Debug("Utils.GetValueOfPropert OUT!");
        //    return ret;
        //}

        internal static string GetMapFromGoogleMap(string geoRef)
        {
            string retmap = Global.MAP_DEFAULT;
            try
            {
                //- 33.3513061452878,-70.5044428392026
                //string sURL = "https://maps.googleapis.com/maps/api/staticmap?center=" + geoRef +
                //              "&zoom=" + Settings.Default.GoogleMapZoom +
                //              "&size=" + Settings.Default.GoogleMapSize +
                //              "&markers=anchor:24,24%7Cicon:" + Settings.Default.GoogleMapICON + "%7C" + geoRef +
                //              "&key=" + Settings.Default.GoogleMapKEY;
                string sURL = Settings.Default.GoogleMApURLApi + "?zoom=" + Settings.Default.GoogleMapZoom +
                              "&size=" + Settings.Default.GoogleMapSize +
                              "&markers=anchor:24,24%7Cicon:" + Settings.Default.GoogleMapICON + "%7C" + geoRef +
                              "&key=" + Settings.Default.GoogleMapKEY;
                log.Debug("Utils.GetMapFromGoogleMap sURL=" + sURL);
                HttpWebRequest wrGETURL = (HttpWebRequest)WebRequest.Create(sURL);
                HttpWebResponse webresponse = (HttpWebResponse)wrGETURL.GetResponse();
                string ct = webresponse.ContentType;
                Stream objStream;
                objStream = wrGETURL.GetResponse().GetResponseStream();
                byte[] buffer = ReadFully(objStream);
                retmap = Convert.ToBase64String(buffer);
            }
            catch (Exception ex)
            {
                retmap = Global.MAP_DEFAULT;
                log.Error("Utils.GetMapFromGoogleMap Error", ex);
            }
            return retmap;
        }

        private static byte[] ReadFully(Stream input)
        {
            try
            {
                int bytesBuffer = 1024;
                byte[] buffer = new byte[bytesBuffer];
                using (MemoryStream ms = new MemoryStream())
                {
                    int readBytes;
                    while ((readBytes = input.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, readBytes);
                    }
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                log.Error("Utils.ReadFully Error ", ex);
                return null;
            }
        }

        internal static string GetMap(string georef)
        {
            string ret = null;
            try
            {
                if (string.IsNullOrEmpty(georef))
                    ret = Properties.Settings.Default.GoogleMapDefault;
                else
                    ret = GetMapFromGoogleMap(georef);
            }
            catch (Exception ex)
            {
                ret = null;
                log.Error("Utils.GetMap Error", ex);
            }

            return ret;
        }

        /// <summary>
        /// GEnera un QR en base64 con el par�mtero indicado como urlci
        /// </summary>
        /// <param name="urlci"></param>
        /// <returns></returns>
        internal static string GetQR(string urlci)
        {
            string qr = "";
            try
            {
                Bio.Core.QR.FormQR formqr = new Bio.Core.QR.FormQR();
                qr = formqr.GetImgQRB64(urlci);
                log.Debug("Utils.GetQR QR Generated = " + qr);
            }
            catch (Exception ex)
            {
                qr = null;
                log.Error("Utils.GetQR Error - " + ex.Message);
            }
            return qr;
        }
    }
}
