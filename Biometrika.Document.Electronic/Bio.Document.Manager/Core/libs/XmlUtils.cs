using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Text;

namespace Biometrika.Document.Electronic.libs
{
    public class XmlUtils
    {
        /// <summary>
        /// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.
        /// </summary>
        /// <param name="characters">Unicode Byte Array to be converted to String</param>
        /// <returns>String converted from Unicode Byte Array</returns>
        public static string UTF8ByteArrayToString(byte[] characters)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            string constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        /// <summary>
        /// Converts the String to UTF8 Byte array and is used in De serialization
        /// </summary>
        /// <param name="pXmlString"></param>
        /// <returns></returns>
        private static Byte[] StringToUTF8ByteArray(string pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }

        /// <summary>
        /// Serialize an object into an XML string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SerializeObject<T>(T obj)
        {
            try
            {
                string xmlString = null;
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(typeof(T));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);

                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");
                if ((typeof(T).Name != "DocumentoType") || (typeof(T).Name != "DocumentoTypeCGR"))
                {
                    if ((typeof(T).Name == "ContenidoResolucionType") || (typeof(T).Name == "ContenidoDecretoType"))
                    {
                        if(typeof(T).Name == "ContenidoResolucionType") 
                            ns.Add("Resolucion_SELICO", "http://www.contraloria.cl/2005/05/CGRDoc");
                        else
                            ns.Add("Decreto_SELICO", "http://www.contraloria.cl/2005/05/CGRDoc");
                        xs.Serialize(xmlTextWriter, obj, ns);
                    }
                    else
                    {
                        xs.Serialize(xmlTextWriter, obj, ns);
                    }
                }
                else
                    xs.Serialize(xmlTextWriter, obj);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                xmlString = UTF8ByteArrayToString(memoryStream.ToArray()); return xmlString;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Reconstruct an object from an XML string
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T DeserializeObject<T>(string xml)
        {
            //XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            
            //XmlSerializer xs = new XmlSerializer(typeof(T),"");
            //MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(xml));
            //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
            //return (T)xs.Deserialize(memoryStream);
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            XmlSerializer xs = null;
            if ((typeof(T).Name == "ContenidoResolucionType") || (typeof(T).Name == "ContenidoDecretoType"))
                xs = new XmlSerializer(typeof(T), "http://www.contraloria.cl/2005/05/CGRDoc");

            else
                xs = new XmlSerializer(typeof(T), "");

            MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(xml));

            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);

            return (T)xs.Deserialize(memoryStream);
        }
        
        public static string SerializeAnObject(object obj)
        {
            System.Xml.XmlDocument doc = new XmlDocument();
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            try
            {
                if ((obj.GetType().Name != "DocumentoType") && (obj.GetType().Name != "DocumentoTypeCGR"))
                    serializer.Serialize(stream, obj, ns);
                else
                    serializer.Serialize(stream, obj);
                stream.Position = 0; doc.Load(stream);
                return doc.InnerXml;
            }
            catch
            { throw; }
            finally { stream.Close(); stream.Dispose(); }
        }
        /// <summary>
        /// Pasa un conteido Any a String para poder Deserializar.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static String XMLElementToString(XmlElement element)
        {
            XmlElement elemento = element;
            StringWriter sw = new StringWriter();
            XmlTextWriter xtw = new XmlTextWriter(sw);
            elemento.WriteTo(xtw);
            return sw.ToString(); 
        }
    }
}
