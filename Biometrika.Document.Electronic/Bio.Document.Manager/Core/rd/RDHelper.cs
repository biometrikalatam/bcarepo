using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using log4net;

namespace Biometrika.Document.Electronic.rd
{
    public class RDHelper
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RDHelper));
        
        public static int GeneraRecibo(string tipodoc, string nrocdocumento, DateTime fechadocumento, 
                                       DateTime fechavto, string cliente, string vendedor,
                                       string documento, string huellacliente, string huellavendedor,
                                       out string xmlRDorError )
        {
            xmlRDorError = null;  //Devuelve el xml del recibo o el mensaje de error si hay error
            int ret = 0;
            
            try
            {
                //WSBDR.ArrayOfRdExtensionItemRdExtensionItem[] arrExt = new ArrayOfRdExtensionItemRdExtensionItem[9];
                //WSBDR.ArrayOfRdExtensionItemRdExtensionItem item = new ArrayOfRdExtensionItemRdExtensionItem();
                //item.key = "TipoDocumento";
                //item.value = tipodoc;
                //arrExt[0] = item;
                //item = new ArrayOfRdExtensionItemRdExtensionItem();
                //item.key = "NumeroDocumento";
                //item.value = nrocdocumento;
                //arrExt[1] = item;
                //item = new ArrayOfRdExtensionItemRdExtensionItem();
                //item.key = "FechaDocumento";
                //item.value = fechadocumento.ToString("dd/MM/yyyy");
                //arrExt[2] = item;
                //item = new ArrayOfRdExtensionItemRdExtensionItem();
                //item.key = "FechaVencimientoDocumento";
                //item.value = fechavto.ToString("dd/MM/yyyy"); ;
                //arrExt[3] = item;
                //item = new ArrayOfRdExtensionItemRdExtensionItem();
                //item.key = "Cliente";
                //item.value = cliente;
                //arrExt[4] = item;
                //item = new ArrayOfRdExtensionItemRdExtensionItem();
                //item.key = "Vendedor";
                //item.value = vendedor;
                //arrExt[5] = item;
                //item = new ArrayOfRdExtensionItemRdExtensionItem();
                //item.key = "Documento";
                //item.value = documento;
                //arrExt[6] = item;
                //item = new ArrayOfRdExtensionItemRdExtensionItem();
                //item.key = "HuellaCliente";
                //item.value = huellacliente;
                //arrExt[7] = item;
                //item = new ArrayOfRdExtensionItemRdExtensionItem();
                //item.key = "HuellaVendedor";
                //item.value = huellavendedor;
                //arrExt[8] = item;
                
                //using (WSBDR.WSBDR ws = new WSBDR.WSBDR())
                //{
                //    ws.Timeout = 3000000;

                //    string descripcion = "BST - Contrato Nro.: " + nrocdocumento;
                //    if (descripcion.Trim().Length > 50)
                //    {
                //        descripcion = descripcion.Substring(0, 50);
                //    }
                //    //RDRetorno rdRet = ws.GeneraRecibo(1, "Bio.Manejador.Documental",
                //    //                            descripcion,
                //    //                            arrExt);
                //    RDRetorno rdRet = ws.GenerateReceipt(1, "Bio.Manejador.Documental",
                //                               descripcion,
                //                               arrExt);

                //    if (rdRet.codigo == 0)
                //    {
                //        xmlRDorError = rdRet.xmlret;
                //    } else
                //    {
                //        ret = rdRet.codigo;
                //        xmlRDorError = rdRet.descripcion;
                //    }
                //}
                
            }
            catch (Exception ex)
            {
                log.Error("Error", ex);
                ret = -1;
            }
            
            return ret;
        }

        public static int VerificaRecibo(string reciboxml, out string xmlRDorError)
        {
            xmlRDorError = null;  //Devuelve el xml del recibo o el mensaje de error si hay error
            int ret = 0;

            try
            {
                //using (WSBDR ws = new WSBDR())
                //{
                //    ws.Timeout = 3000000;

                //    //RDRetorno rdRet = ws.VerifyReceipt(reciboxml, 2);

                //    //if (rdRet.codigo == 0)
                //    //{
                //    //    xmlRDorError = rdRet.xmlret;
                //    //}
                //    //else
                //    //{
                //    //    ret = rdRet.codigo;
                //    //    xmlRDorError = rdRet.descripcion;
                //    //}
                //}

            }
            catch (Exception ex)
            {
                log.Error("Error", ex);
                ret = -1;
            }

            return ret;
        }

        public static int ParseResultReceipt(string resultxml)
        {
            int ret = 0;

            try
            {
                XmlDocument result = new XmlDocument();
                result.PreserveWhitespace = true;
                result.LoadXml(resultxml);

                string res = result.GetElementsByTagName("result")[0].InnerText;

                ret = Convert.ToInt32(res);
            }
            catch (Exception ex)
            {
                log.Error("Error", ex);
                ret = -1;
            }

            return ret;
        }
    }
}
