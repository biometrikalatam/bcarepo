﻿using System;
using System.Collections.Generic;
using System.IO;
using Org.BouncyCastle.X509;
using iTextSharp.text.pdf.security;
using iTextSharp.text.pdf;
using iTextSharp.text;
using log4net;
using Biometrika.Document.Electronic.Api;

namespace Biometrika.Document.Electronic.PDF
{
    public static class PDFHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(PDFHelper));

        /// <summary>
        /// Firma digitalmente con firma simple un PDF y muestra en una página al final los datos de la firma 
        /// con imágen de la firma caligrádica si existe.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="chain"></param>
        /// <param name="pk"></param>
        /// <param name="digestAlgorithm"></param>
        /// <param name="subfilter"></param>
        /// <param name="reason"></param>
        /// <param name="location"></param>
        /// <param name="crlList"></param>
        /// <param name="ocspClient"></param>
        /// <param name="tsaClient"></param>
        /// <param name="estimatedSize"></param>
        /// <param name="pathImgSignature"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <param name="fileSigned"></param>
        /// <returns></returns>
        static public int SignPDF(byte[] source, 
                         ICollection<X509Certificate> chain, Org.BouncyCastle.Crypto.ICipherParameters pk,
                         String digestAlgorithm, CryptoStandard subfilter,
                         String reason, String location,
                         ICollection<ICrlClient> crlList,
                         IOcspClient ocspClient,
                         ITSAClient tsaClient,
                         int estimatedSize,
                         string pathImgSignature, int x, int y, int w, int h, 
                         out byte[] fileSigned)
        {
            int ret = 0;
            // Creating the reader and the stamper
            PdfReader reader = null;
            PdfStamper stamper = null;
            //FileStream os = null;
            fileSigned = null;
            MemoryStream os = new MemoryStream();
            try
            {
                LOG.Debug("PDFHelper.SignPDF - Init...");
                LOG.Debug("PDFHelper.SignPDF source.length = " + source.Length);
                reader = new PdfReader(source);
                LOG.Debug("PDFHelper.SignPDF reader OK...");
                //os = new FileStream(dest, FileMode.Create);
                os = new MemoryStream();
                LOG.Debug("PDFHelper.SignPDF write ok...");
                
                LOG.Debug("PDFHelper.SignPDF PdfStamper.CreateSignature...");
                stamper = PdfStamper.CreateSignature(reader, os, '\0');
                // Creating the appearance
                PdfSignatureAppearance appearance = stamper.SignatureAppearance;
                appearance.Reason = reason;
                appearance.Location = location;
                appearance.Contact = "info@biometrika.cl";
                appearance.Layer2Font = new Font(Font.FontFamily.COURIER, 8);
                //appearance.Layer2Text = "This document was signed by Bruno Specimen";
                //appearance.Layer4Text = "Layer4Text";
                //appearance.LocationCaption = "LocationCaption";
                //appearance.ReasonCaption = "ReasonCaption";
                //appearance.RunDirection = 0;

                //iTextSharp.text.Rectangle cropBox = reader.GetCropBox(reader.NumberOfPages);
                ////                appearance.SetVisibleSignature(new Rectangle(x, y, w, h), reader.NumberOfPages, "sig");
                ////cropBox.Width = w;
                ////cropBox.hHeight = h;
                //cropBox.Left = x;
                //cropBox.Right = y;
                
                //cropBox = new Rectangle()

                appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(x, y, w, h), reader.NumberOfPages, "Signature");
                //appearance.SetVisibleSignature(cropBox, reader.NumberOfPages, "Signature");
                LOG.Debug("PDFHelper.SignPDF SetVisibleSignature in page " + reader.NumberOfPages + "...");

                LOG.Debug("PDFHelper.SignPDF Adding image signature => " + pathImgSignature + "...");
                Image imgSignature = Image.GetInstance(pathImgSignature);
                appearance.SignatureGraphic = imgSignature;
                appearance.SignatureGraphic.Alignment = 0;
                //objSA.Image = imgSignature;
                appearance.SignatureGraphic.Alignment = Element.ALIGN_CENTER;
                appearance.ImageScale = 0.4f;

                appearance.SignDate = DateTime.Now;
                appearance.Acro6Layers = true;
                appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.GRAPHIC_AND_DESCRIPTION;


                // Creating the signature
                LOG.Debug("PDFHelper.SignPDF Creating signature...");
                IExternalSignature pks = new PrivateKeySignature(pk, digestAlgorithm);
                LOG.Debug("PDFHelper.SignPDF MakeSignature...");
                MakeSignature.SignDetached(appearance, pks, chain, crlList, ocspClient, tsaClient, estimatedSize, subfilter);
                LOG.Debug("PDFHelper.SignPDF MakeSignature End!");
                fileSigned = os.ToArray();
                LOG.Debug("PDFHelper.SignPDF fileSigned.length = " + fileSigned.Length);
            }
            catch (Exception ex)
            {
                ret = -1;
                fileSigned = null;
                LOG.Error("PDFHelper.SignPDF Error", ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (stamper != null)
                    stamper.Close();
                if (os != null)
                    os.Close();
            }
            LOG.Debug("PDFHelper.SignPDF End!");
            return ret;
        }


        /// <summary>
        /// Agrega una hora en blanco desde un template segun sea FES o FEA
        /// </summary>
        /// <param name="sourceFiles">PDF files being merged.</param>
        /// <returns></returns>
        public static byte[] AddTemplateBlankSignature(List<byte[]> sourceFiles, int template)
        {
            byte[] ret = null;
            try
            {
                LOG.Debug("PDFHelper.AddTemplateBlankSignature In...");
                LOG.Debug("PDFHelper.AddTemplateBlankSignature sourceFiles.Length = " + sourceFiles.Count + "...");
                byte[] fileToAddAtEnd; //Para incorporar la firma
                System.IO.FileStream fs;
                LOG.Debug("PDFHelper.AddTemplateBlankSignature Levantando template desde => " +
                                                    Properties.Settings.Default.PathTemplatesSignatures);
                switch (template)
                {
                    case 1: //Levanto FEA
                        fs = new System.IO.FileStream(Properties.Settings.Default.PathTemplatesSignatures + "FEA.pdf", System.IO.FileMode.Open);
                        fileToAddAtEnd = new byte[fs.Length];
                        fs.Read(fileToAddAtEnd, 0, (int)fs.Length);
                        fs.Close();
                        break;
                    case 2: //Levanto NV
                        fs = new System.IO.FileStream(Properties.Settings.Default.PathTemplatesSignatures + "NV.pdf", System.IO.FileMode.Open);
                        fileToAddAtEnd = new byte[fs.Length];
                        fs.Read(fileToAddAtEnd, 0, (int)fs.Length);
                        fs.Close();
                        break;
                    default: //Levanto FES
                        fs = new System.IO.FileStream(Properties.Settings.Default.PathTemplatesSignatures + "FES.pdf", System.IO.FileMode.Open);
                        fileToAddAtEnd = new byte[fs.Length];
                        fs.Read(fileToAddAtEnd, 0, (int)fs.Length);
                        fs.Close();
                        break;
                }
                sourceFiles.Add(fileToAddAtEnd);
                LOG.Debug("PDFHelper.AddTemplateBlankSignature sourceFiles.Length = " + sourceFiles.Count + "...");

                iTextSharp.text.Document document = new iTextSharp.text.Document();
                using (MemoryStream ms = new MemoryStream())
                {
                    LOG.Debug("PDFHelper.AddTemplateBlankSignature Adding IN...");
                    PdfCopy copy = new PdfCopy(document, ms);
                    document.Open();
                    int documentPageCounter = 0;

                    // Iterate through all pdf documents
                    for (int fileCounter = 0; fileCounter < sourceFiles.Count; fileCounter++)
                    {
                        // Create pdf reader
                        byte[] fileAuxReader = sourceFiles[fileCounter];
                        PdfReader reader = new PdfReader(fileAuxReader);
                        int numberOfPages = reader.NumberOfPages;

                        // Iterate through all pages
                        for (int currentPageIndex = 1; currentPageIndex <= numberOfPages; currentPageIndex++)
                        {
                            documentPageCounter++;
                            PdfImportedPage importedPage = copy.GetImportedPage(reader, currentPageIndex);
                            PdfCopy.PageStamp pageStamp = copy.CreatePageStamp(importedPage);

                            pageStamp.AlterContents();

                            copy.AddPage(importedPage);
                        }

                        copy.FreeReader(reader);
                        reader.Close();
                    }

                    document.Close();
                    ret = ms.ToArray();
                    LOG.Debug("PDFHelper.AddTemplateBlankSignature  Adding Out => ret = " + Convert.ToBase64String(ret));
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("PDFHelper.AddTemplateBlankSignature Error", ex);
            }
            LOG.Debug("PDFHelper.AddTemplateBlankSignature Out!");
            return ret;

        }


        public static byte[] AddABS(byte[] file, string signatureimage, string qr, string[][] datos)
        {
            byte[] ret = null;

            try
            {
                LOG.Debug("PDFHelper.AddABS In...");
                iTextSharp.text.Document document = new iTextSharp.text.Document();
                using (MemoryStream ms = new MemoryStream())
                {
                    LOG.Debug("PDFHelper.AddABS Generanting...");
                    PdfCopy copy = new PdfCopy(document, ms);
                    document.Open();
                    // Create pdf reader
                    PdfReader reader = new PdfReader(file);
                    LOG.Debug("PDFHelper.AddABS reader ok...");
                    int numberOfPages = reader.NumberOfPages;
                    int documentPageCounter = 0;
                    // Iterate through all pages
                    for (int currentPageIndex = 1; currentPageIndex <= numberOfPages; currentPageIndex++)
                    {
                        LOG.Debug("PDFHelper.AddABS Adding page = " + documentPageCounter + "...");
                        documentPageCounter++;
                        PdfImportedPage importedPage = copy.GetImportedPage(reader, currentPageIndex);
                        PdfCopy.PageStamp pageStamp = copy.CreatePageStamp(importedPage);

                        if (currentPageIndex == numberOfPages) //Si es ultima hoja => Agrego firma
                        {
                            LOG.Debug("PDFHelper.AddABS Adding Imagesignature...");
                            Stream inputImageStream = new FileStream(signatureimage, FileMode.Open, FileAccess.Read, FileShare.Read);
                            Image image = Image.GetInstance(inputImageStream);
                            image.SetAbsolutePosition(200, 620);
                            image.Alignment = Element.ALIGN_LEFT;
                            image.ScalePercent(40);
                            image.Border = 10;
                            image.BorderColor = BaseColor.GRAY;
                            pageStamp.GetOverContent().AddImage(image);

                            //Add QR si viene
                            if (!String.IsNullOrEmpty(qr))
                            {
                                LOG.Debug("PDFHelper.AddABS Adding QR...");
                                MemoryStream msqr = new MemoryStream(Convert.FromBase64String(qr));
                                Image imageQR = Image.GetInstance(msqr);
                                imageQR.SetAbsolutePosition(450, 650);
                                //imageQR.Alignment = Element.ALIGN_LEFT;
                                imageQR.ScalePercent(60);
                                imageQR.Border = 10;
                                imageQR.BorderColor = BaseColor.GRAY;
                                pageStamp.GetOverContent().AddImage(imageQR);
                            }

                            float Alto = importedPage.Height;
                            Alto = 600; //
                            LOG.Debug("PDFHelper.AddABS Adding Data...");
                            for (int i = 0; i < datos.Length; i++)
                            {
                                LOG.Debug("PDFHelper.AddABS Adding >> " + datos[i][0] + " - " + datos[i][1] + "...");
                                ColumnText.ShowTextAligned(
                                    pageStamp.GetOverContent(), Element.ALIGN_LEFT,
                                    new Phrase(datos[i][0], new Font(Font.FontFamily.HELVETICA, 12, Font.ITALIC, BaseColor.DARK_GRAY)),
                                    importedPage.Width / 4,
                                    Alto,
                                    importedPage.Width < importedPage.Height ? 0 : 1);
                                ColumnText.ShowTextAligned(
                                    pageStamp.GetOverContent(), Element.ALIGN_LEFT,
                                    new Phrase(datos[i][1], new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK)),
                                    (importedPage.Width / 4) + 50,
                                    Alto - 10,
                                    importedPage.Width < importedPage.Height ? 0 : 1);
                                Alto = Alto - 25;
                            }

                            pageStamp.AlterContents();
                        }
                        copy.AddPage(importedPage);
                    }

                    copy.FreeReader(reader);
                    reader.Close();
                    document.Close();

                    ret = ms.ToArray();
                    LOG.Debug("PDFHelper.AddABS ABS Added ok!");
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("PDFHelper.AddABS Error", ex);
            }
            LOG.Debug("PDFHelper.AddABS Out!");
            return ret;
            
        }

        /// <summary>
        /// Agrega una BS en un PDf dado de parámetro, en memoria. Se agrega el template de FES
        /// y se copletan los datos. Si está notarizado se agrega el logo de notario y se agregan 
        /// los datos de esa notarizacion debajo de los datos de la BS 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="itemBS"></param>
        /// <returns></returns>
        internal static byte[] AddABS(byte[] file, BS itemBS)
        {
            byte[] ret = null;

            try
            {
                LOG.Debug("PDFHelper.AddABS In...");
                iTextSharp.text.Document document = new iTextSharp.text.Document();
                using (MemoryStream ms = new MemoryStream())
                {
                    LOG.Debug("PDFHelper.AddABS Generanting...");
                    PdfCopy copy = new PdfCopy(document, ms);
                    document.Open();
                    // Create pdf reader
                    PdfReader reader = new PdfReader(file);
                    LOG.Debug("PDFHelper.AddABS reader ok...");
                    int numberOfPages = reader.NumberOfPages;
                    int documentPageCounter = 0;
                    // Iterate through all pages
                    for (int currentPageIndex = 1; currentPageIndex <= numberOfPages; currentPageIndex++)
                    {
                        LOG.Debug("PDFHelper.AddABS Adding page = " + documentPageCounter + "...");
                        documentPageCounter++;
                        PdfImportedPage importedPage = copy.GetImportedPage(reader, currentPageIndex);
                        PdfCopy.PageStamp pageStamp = copy.CreatePageStamp(importedPage);

                        if (currentPageIndex == numberOfPages) //Si es ultima hoja => Agrego firma
                        {
                            LOG.Debug("PDFHelper.AddABS Adding Imagesignature...");
                            Stream inputImageStream = 
                                new MemoryStream(Convert.FromBase64String(GetImageFromBS(itemBS, itemBS.BiometricSignatureType))); 
                            //new FileStream(, FileMode.Open, FileAccess.Read, FileShare.Read);
                            Image image = Image.GetInstance(inputImageStream);
                            image.SetAbsolutePosition(200, 620);
                            image.Alignment = Element.ALIGN_LEFT;
                            image.ScalePercent(40);
                            image.Border = 10;
                            image.BorderColor = BaseColor.GRAY;
                            pageStamp.GetOverContent().AddImage(image);

                            //Add QR si viene
                            if (!String.IsNullOrEmpty(itemBS.QRVerify))
                            {
                                LOG.Debug("PDFHelper.AddABS Adding QR...");
                                MemoryStream msqr = new MemoryStream(Convert.FromBase64String(itemBS.QRVerify));
                                Image imageQR = Image.GetInstance(msqr);
                                imageQR.SetAbsolutePosition(450, 650);
                                //imageQR.Alignment = Element.ALIGN_LEFT;
                                imageQR.ScalePercent(60);
                                imageQR.Border = 10;
                                imageQR.BorderColor = BaseColor.GRAY;
                                pageStamp.GetOverContent().AddImage(imageQR);
                            }

                            float Alto = importedPage.Height;
                            Alto = 600; //
                            LOG.Debug("PDFHelper.AddABS Adding Data...");

                            LOG.Debug("PDFHelper.AddABS Adding >> Biometrica Signature Id - " + itemBS.BSId + "...");
                            ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_LEFT, 
                                new Phrase("Biometrica Signature Id", new Font(Font.FontFamily.HELVETICA, 12, Font.ITALIC, BaseColor.DARK_GRAY)), 
                                importedPage.Width / 4, Alto, importedPage.Width < importedPage.Height ? 0 : 1);
                            ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_LEFT, 
                                new Phrase(itemBS.BSId, new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK)), 
                                (importedPage.Width / 4) + 80,Alto-15, importedPage.Width < importedPage.Height ? 0 : 1);
                            Alto = Alto - 30;

                            LOG.Debug("PDFHelper.AddABS Adding >> Biometrica Provider - " + itemBS.BiometricProvider + "...");
                            ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_LEFT,
                                   new Phrase("Biometrica Provider", new Font(Font.FontFamily.HELVETICA, 12, Font.ITALIC, BaseColor.DARK_GRAY)),
                                   importedPage.Width / 4, Alto, importedPage.Width < importedPage.Height ? 0 : 1);
                            ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_LEFT,
                                new Phrase(itemBS.BiometricProvider, new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK)),
                                (importedPage.Width / 4) + 80, Alto - 15, importedPage.Width < importedPage.Height ? 0 : 1);
                            Alto = Alto - 30;

                            LOG.Debug("PDFHelper.AddABS Adding >> IdnetityId - " + itemBS.IdentityId + "...");
                            ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_LEFT,
                                   new Phrase("Identity Id", new Font(Font.FontFamily.HELVETICA, 12, Font.ITALIC, BaseColor.DARK_GRAY)),
                                   importedPage.Width / 4, Alto, importedPage.Width < importedPage.Height ? 0 : 1);
                            ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_LEFT,
                                new Phrase(itemBS.IdentityId, new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK)),
                                (importedPage.Width / 4) + 80, Alto - 15, importedPage.Width < importedPage.Height ? 0 : 1);
                            Alto = Alto - 30;

                            LOG.Debug("PDFHelper.AddABS Adding >> IdnetityName - " + itemBS.IdnetityName + "...");
                            ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_LEFT,
                                   new Phrase("Identity Name", new Font(Font.FontFamily.HELVETICA, 12, Font.ITALIC, BaseColor.DARK_GRAY)),
                                   importedPage.Width / 4, Alto, importedPage.Width < importedPage.Height ? 0 : 1);
                            ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_LEFT,
                                new Phrase(itemBS.IdnetityName, new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK)),
                                (importedPage.Width / 4) + 80, Alto - 15, importedPage.Width < importedPage.Height ? 0 : 1);
                            Alto = Alto - 30;

                            LOG.Debug("PDFHelper.AddABS Adding >> TimestampSignature - " + itemBS.TimestampSignature + "...");
                            ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_LEFT,new Phrase("Identity Timestamp Signature", 
                                new Font(Font.FontFamily.HELVETICA, 12, Font.ITALIC, BaseColor.DARK_GRAY)), 
                                importedPage.Width / 4, Alto, importedPage.Width < importedPage.Height ? 0 : 1);
                            ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_LEFT, 
                                new Phrase(itemBS.TimestampSignature, new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK)), 
                                (importedPage.Width / 4) + 80, Alto - 15, importedPage.Width < importedPage.Height ? 0 : 1);
                            Alto = Alto - 70;

                            if (!String.IsNullOrEmpty(itemBS.NVreceiptId))  //Hay recibo de NV => Agrego esa info
                            {
                                LOG.Debug("PDFHelper.AddABS Adding >> Add Logo NV...");
                                MemoryStream msqr1 = new MemoryStream(Convert.FromBase64String(Global.LOGO_NV));
                                Image imageLogoNV = Image.GetInstance(msqr1);
                                imageLogoNV.SetAbsolutePosition(450, Alto);
                                imageLogoNV.ScalePercent(60);
                                pageStamp.GetOverContent().AddImage(imageLogoNV);
                                Alto = Alto - 20;
                                
                                //public string NVReceipttimestamp; //Fecha y hora de cuando se genero el recibo
                                //public string TSA;  // Ejemplo: C=ES,O=Safe Creative, CN = Time Stamp Authority Server
                                //public string TSAPolicy; //Ejemplo 1.3.6.1.4.1.45794.1.1
                                //public string TSADateTimeSign; //Fehcahora del timestamp en TSA => ejemplo 29-03-2018 19:22:34
                                //public string TSASerialNumber; //Ejemplo: 25793306
                                //public string TSAURLVerify; //Ejemplo: https://tsa.safecreative.org/?search=

                                LOG.Debug("PDFHelper.AddABS Adding >> NVreceiptId - " + itemBS.NVreceiptId + "...");
                                ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_RIGHT, new Phrase("Receipt Id", new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC, BaseColor.DARK_GRAY)), importedPage.Width - 90, Alto, importedPage.Width < importedPage.Height ? 0 : 1);
                                ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_RIGHT, new Phrase(String.IsNullOrEmpty(itemBS.NVreceiptId) ? "No Informado" : itemBS.NVreceiptId, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)), importedPage.Width - 90, Alto - 10, importedPage.Width < importedPage.Height ? 0 : 1);
                                Alto = Alto - 20;
                                LOG.Debug("PDFHelper.AddABS Adding >> TSA - " + itemBS.TSA + "...");
                                ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_RIGHT, new Phrase("TSA", new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC, BaseColor.DARK_GRAY)), importedPage.Width - 90, Alto, importedPage.Width < importedPage.Height ? 0 : 1);
                                ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_RIGHT, new Phrase(String.IsNullOrEmpty(itemBS.TSA) ? "No Informado" : itemBS.NVreceiptId, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)), importedPage.Width - 90, Alto - 10, importedPage.Width < importedPage.Height ? 0 : 1);
                                Alto = Alto - 20;
                                LOG.Debug("PDFHelper.AddABS Adding >> TSAPolicy - " + itemBS.TSAPolicy + "...");
                                ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_RIGHT, new Phrase("TSA Policy", new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC, BaseColor.DARK_GRAY)), importedPage.Width - 90, Alto, importedPage.Width < importedPage.Height ? 0 : 1);
                                ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_RIGHT, new Phrase(String.IsNullOrEmpty(itemBS.TSAPolicy) ? "No Informado" : itemBS.TSAPolicy, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)), importedPage.Width - 90, Alto - 10, importedPage.Width < importedPage.Height ? 0 : 1);
                                Alto = Alto - 20;
                                LOG.Debug("PDFHelper.AddABS Adding >> TSADateTimeSign - " + itemBS.TSADateTimeSign + "...");
                                ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_RIGHT, new Phrase("TSA DateTimeSign", new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC, BaseColor.DARK_GRAY)), importedPage.Width - 90, Alto, importedPage.Width < importedPage.Height ? 0 : 1);
                                ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_RIGHT, new Phrase(String.IsNullOrEmpty(itemBS.TSADateTimeSign) ? "No Informado" : itemBS.TSADateTimeSign, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)), importedPage.Width - 90, Alto - 10, importedPage.Width < importedPage.Height ? 0 : 1);
                                Alto = Alto - 20;
                                LOG.Debug("PDFHelper.AddABS Adding >> TSASerialNumber - " + itemBS.TSASerialNumber + "...");
                                ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_RIGHT, new Phrase("TSA SerialNumber", new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC, BaseColor.DARK_GRAY)), importedPage.Width - 90, Alto, importedPage.Width < importedPage.Height ? 0 : 1);
                                ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_RIGHT, new Phrase(String.IsNullOrEmpty(itemBS.TSASerialNumber) ? "No Informado" : itemBS.TSASerialNumber, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)), importedPage.Width - 90, Alto - 10, importedPage.Width < importedPage.Height ? 0 : 1);
                                Alto = Alto - 20;
                                LOG.Debug("PDFHelper.AddABS Adding >> TSAURLVerify - " + itemBS.TSAURLVerify + "...");
                                ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_RIGHT, new Phrase("TSA URL Verify", new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC, BaseColor.DARK_GRAY)), importedPage.Width - 90, Alto, importedPage.Width < importedPage.Height ? 0 : 1);
                                ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_RIGHT, new Phrase(String.IsNullOrEmpty(itemBS.TSAURLVerify) ? "No Informado" : itemBS.TSAURLVerify, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)), importedPage.Width - 90, Alto - 10, importedPage.Width < importedPage.Height ? 0 : 1);
                                Alto = Alto - 20;
                            }

                            pageStamp.AlterContents();
                        }
                        copy.AddPage(importedPage);
                    }

                    copy.FreeReader(reader);
                    reader.Close();
                    document.Close();

                    ret = ms.ToArray();
                    LOG.Debug("PDFHelper.AddABS ABS Added ok!");
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("PDFHelper.AddABS Error", ex);
            }
            LOG.Debug("PDFHelper.AddABS Out!");
            return ret;
        }

        /// <summary>
        /// Dado un Base64, que corresponde a una BS, si no es JPG, lo transforma a JPG para poder
        /// insertarlo en el PDF como imagen. Si es JPG, pasa asi, si es WSQ entonces descomprime
        /// y genera imagne desde RAW obtenido. Y si es RAW usa el W y H enciado para armar la imágen.
        /// </summary>
        /// <param name="itemBS"></param>
        /// <param name="bstype">Id de tipo de Signatura: [41 - JPG | 21 - WSQ | 22 - RAW]</param>
        /// <returns></returns>
        internal static string GetImageFromBS(BS itemBS, int bstype)
        {
            string ret = null;
            short w, h;
            System.Drawing.Image imgAux;
            try
            {
                LOG.Debug("GetImageFromBS IN...");
                LOG.Debug("GetImageFromBS bstype = " + bstype);
                switch (bstype)
                {
                    case 41: //JPG
                        LOG.Debug("GetImageFromBS In 41...");
                        ret = itemBS.BiometricSignature;
                        LOG.Debug("GetImageFromBS ret = " + ret);
                        break;
                    case 21: //WSQ
                        LOG.Debug("GetImageFromBS In 21...");
                        byte[] byRAW;
                        Bio.Core.Wsq.Decoder.WsqDecoder decoder = new Bio.Core.Wsq.Decoder.WsqDecoder();
                        LOG.Debug("GetImageFromBS Decoding...");
                        if (decoder.DecodeMemory(Convert.FromBase64String(itemBS.BiometricSignature), out w, out h, out byRAW))
                        {
                            LOG.Debug("GetImageFromBS Decoding WSQ OK - WRaw=" + w + " - HRaw=" + h);
                            imgAux = Bio.Core.Imaging.ImageProcessor.RawToBitmap(byRAW, w, h);
                            LOG.Debug("GetImageFromBS imgAux != null = " + (imgAux != null).ToString());
                            MemoryStream msImg = new MemoryStream();
                            imgAux.Save(msImg, System.Drawing.Imaging.ImageFormat.Jpeg);
                            byte[] byImgFromRaw = msImg.ToArray();
                            LOG.Debug("GetImageFromBS byImgFromRaw.Length = " + (byImgFromRaw.Length).ToString());
                            msImg.Close();
                            ret = Convert.ToBase64String(byImgFromRaw);
                        } else
                        {
                            ret = null;
                        }
                        LOG.Debug("GetImageFromBS ret = " + ret);
                        break;
                    case 22: //RAW
                        LOG.Debug("GetImageFromBS In 22...");
                        imgAux = Bio.Core.Imaging.ImageProcessor.RawToBitmap(Convert.FromBase64String(itemBS.BiometricSignature), 
                                                                             itemBS.BiometricSignatureW,
                                                                             itemBS.BiometricSignatureH);
                        LOG.Debug("GetImageFromBS imgAux != null = " + (imgAux != null).ToString());
                        MemoryStream ms = new MemoryStream();
                        imgAux.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        byte[] byImg = ms.ToArray();
                        LOG.Debug("GetImageFromBS byImgFromRaw.Length = " + (byImg.Length).ToString());
                        ms.Close();
                        ret = Convert.ToBase64String(byImg);
                        LOG.Debug("GetImageFromBS ret = " + ret);
                        break;
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("PDFHelper.GetImageFromBS Error", ex);
            }
            return ret;
        }

    }

}