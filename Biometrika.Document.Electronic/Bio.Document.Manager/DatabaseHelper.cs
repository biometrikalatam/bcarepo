﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Biometrika.Document.Electronic.Database;
using log4net;

namespace Biometrika.Document.Electronic
{
    public class DatabaseHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(DatabaseHelper));

        internal static List<DeTemplate> GetAllDeTemplates()
        {
            List<DeTemplate> list = null;
            try
            {
                list = (List<DeTemplate>)AdministradorDeTemplate.AllDeTemplate();
            }
            catch (Exception ex)
            {
                list = null;
                LOG.Error("DatabaseHelper.GetAllDeTemplates Excp Error: " + ex.Message);
            }
            return list;
        }
    }
}