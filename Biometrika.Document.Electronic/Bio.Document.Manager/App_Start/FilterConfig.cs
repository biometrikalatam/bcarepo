﻿using System.Web;
using System.Web.Mvc;

namespace Biometrika.Document.Electronic
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
