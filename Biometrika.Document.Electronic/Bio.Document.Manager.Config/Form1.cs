﻿using Bio.Document.Manager.Config.Source;
using Bio.Document.Manager.Config.Source.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bio.Document.Manager.Config
{
    public partial class Form1 : Form
    {
        DocumentTypesConfig CONFIG;
        int CompanyIdSelected = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CONFIG = DEHelper.GetDTConfig();
            CONFIG.Initialize();
            FillcomboEmpresas();
            int i = 0;
        }

        private void FillcomboEmpresas()
        {
            try
            {
                if (CONFIG != null)
                {
                    cbEmpresas.Items.Clear();
                    foreach (Company item in CONFIG.Companies)
                    {
                        cbEmpresas.Items.Add(item.IdCompany);
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FillcomboEmpresas();
        }

        private void cbEmpresas_SelectedIndexChanged(object sender, EventArgs e)
        {
            CompanyIdSelected = Convert.ToInt32(cbEmpresas.SelectedItem);
            FillDataFromCompany(CompanyIdSelected);
        }

        private void FillDataFromCompany(int companyIdSelected)
        {
            try
            {
                cbDTypes.Items.Clear();
                Company company = CONFIG.GetCompany(companyIdSelected);
                if (company != null)
                {
                    foreach (DocumentType item in company.DocumentTypes)
                    {
                        cbDTypes.Items.Add(item.idDT + "-" + item.Description);
                    }

                    
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        int DTTypeCurrent;
        private void cbDTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DTTypeCurrent = Convert.ToInt32(((string)cbDTypes.SelectedItem).Split('-')[0]);
            FillDataFromDTType(DTTypeCurrent);
        }

        private void FillDataFromDTType(int deIdSelected)
        {
            DocumentType dtype;
            Company comp;
            List<Tag> listTags;
            try
            {
                lbTAGs.Items.Clear();
                int iret = CONFIG.ExistDE(CompanyIdSelected, deIdSelected, out listTags, out comp, out dtype);
                if (iret == 0 && dtype != null)
                {
                    foreach (Tag item in dtype.Tags)
                    {
                        lbTAGs.Items.Add(item.IdTag + "|" + item.Type + "|" + item.Parameters);
                    }


                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
