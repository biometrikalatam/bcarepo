﻿using log4net;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using Bio.Document.Manager.Api;
using System.Drawing;

namespace Bio.Document.Manager.Config.Source
{
    public class DocumentTypesConfig
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(DocumentTypesConfig));

        public DocumentTypesConfig() { }
        internal Hashtable htClientes;
        internal Hashtable htDocumentTypes;

#region Serializable

        private List<Company> _companies;

        ///<summary>
        /// Lista de Matchers configurados en config XML.
        ///</summary>
        public List<Company> Companies
        {
            get { return _companies; }
            set { _companies = value; }
        }

        internal DocumentType GetDEType(int company, int idDE)
        {
            try
            {
                return (DocumentType)htDocumentTypes[company.ToString() + "-" + idDE.ToString()];
            }
            catch (Exception ex)
            {
                LOG.Error("DocumentTypesConfig.GetDEType Error " + ex.Message);
            }
            return null;
        }

        internal int Initialize()
        {
            int ret = Errors.RET_OK;
            int auxidCompany;
            try
            {
                htDocumentTypes = new Hashtable();
                htClientes = new Hashtable();

                for (int i = 0; i < _companies.Count; i++)
                {
                    htClientes.Add(_companies[i].IdCompany, _companies[i]);
                    auxidCompany = _companies[i].IdCompany;
                    LOG.Info("  > DocumentTypesConfig.Initialize - Cargando compañia " + _companies[i].IdCompany.ToString() + "-" + _companies[i].User + "...");
                    for (int j = 0; j < _companies[i].DocumentTypes.Count; j++)
                    {
                        htDocumentTypes.Add(auxidCompany.ToString() + "-" + _companies[i].DocumentTypes[j].idDT.ToString(), _companies[i].DocumentTypes[j]);
                        LOG.Info("     >> DocumentTypesConfig.Initialize - Cargando DocumentType " + 
                            (auxidCompany.ToString() + "-" + _companies[i].DocumentTypes[j].idDT.ToString() + 
                            "-" + _companies[i].DocumentTypes[j].Name + "..."));
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DocumentTypesConfig.Iitialize Error " + ex.Message);
                ret = Errors.RET_ERR_UNKKNOW;
            }
            return ret;
        }

        internal Company GetCompany(int company)
        {
            Company oCompany = null;
            try
            {
                if (htClientes.ContainsKey(company))
                {
                    oCompany = (Company)(htClientes[company]);
                }
                else
                {
                    oCompany = null;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DocumentTypesConfig.GetCompany Error " + ex.Message);
                oCompany = null;
            }
            return oCompany;
        }

        internal int ExistDE(int company, int idDE, out List<Tag> tags, out Company oCompany, out DocumentType oDType)
        {
            int ret = Errors.RET_OK;
            tags = null;
            oCompany = null;
            oDType = null;
            try
            {
                if (htDocumentTypes.ContainsKey(company.ToString() + "-" + idDE.ToString())) {
                    tags = ((DocumentType)htDocumentTypes[company.ToString() + "-" + idDE.ToString()]).Tags;
                    oCompany = (Company)(htClientes[company]);
                    oDType = ((DocumentType)htDocumentTypes[company.ToString() + "-" + idDE.ToString()]);
                } else {
                    ret = Errors.RET_ERR_DE_CONFIG_NOT_EXIST;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DocumentTypesConfig.ExistDE Error " + ex.Message);
                ret = Errors.RET_ERR_UNKKNOW;
            }
            return ret;
        }

        //internal int CheckTagsIntegrity(string seed, List<Api.Tag> tagscurrent, List<Tag> tagsconfigured, out List<TagProcessed> tagsprocessed, out string msgerr)
        //{
        //    int ret = Errors.RET_OK;
        //    tagsprocessed = null;
        //    msgerr = null;
        //    try
        //    {
        //        //Controlo largos primero
        //        if (tagscurrent == null ||
        //            tagsconfigured == null ||
        //            tagscurrent.Count == 0 || tagsconfigured.Count == 0 ||
        //            tagscurrent.Count != tagsconfigured.Count)
        //        {
        //            LOG.Error("DocumentTypesConfig.CheckTagsIntegrity Error tagscurren o tagsconfigured null o cantidad diferente de tags!");
        //            LOG.Error("DocumentTypesConfig.CheckTagsIntegrity tagscurrent = " + (tagscurrent == null ? "NULL" : tagscurrent.Count.ToString()));
        //            LOG.Error("DocumentTypesConfig.CheckTagsIntegrity tagsconfigured = " + (tagsconfigured == null ? "NULL" : tagsconfigured.Count.ToString()));
        //            msgerr = "tagscurrent=" + (tagscurrent == null ? "NULL" : tagscurrent.Count.ToString()) + "- tagsconfigured = " + (tagsconfigured == null ? "NULL" : tagsconfigured.Count.ToString());
        //            return Errors.RET_ERR_TAGS_INCONSISTENT;
        //        }

        //        //Recorro cada tag, con su tipo y convierto el tipo al objeto para tenerlo listo
        //        tagsprocessed = new List<TagProcessed>();
        //        TagProcessed tagProcessed = new TagProcessed();

        //        for (int i = 0; i < tagscurrent.Count; i++)
        //        {
        //            Tag tag = GetTagFromList(tagscurrent[i].IdTag, tagsconfigured);
        //            if (tag != null)
        //            {
        //                tagProcessed = new TagProcessed();
        //                tagProcessed.IdTag = tag.IdTag;
        //                tagProcessed.Type = GetTypeNumber(tag.Type);
        //                if (tagProcessed.Type < 0)
        //                {
        //                    msgerr = "DocumentTypesConfig.CheckTagsIntegrity tipo configurado inexistente [" + tag.Type + "]";
        //                    LOG.Error(msgerr);
        //                    return Errors.RET_ERR_TAGS_INCONSISTENT;
        //                }
        //                tagProcessed.Parameters = tag.Parameters;
        //                tagProcessed.value = GetObjectFromTag(seed, i, tagProcessed.Type, tagscurrent[i].value, tagProcessed.Parameters);
        //                tagsprocessed.Add(tagProcessed);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("DocumentTypesConfig.CheckTagsIntegrity Error " + ex.Message);
        //        ret = Errors.RET_ERR_UNKKNOW;
        //    }
        //    return ret;
        //}

        internal string GetNameDE(int company, int idDE)
        {
            try
            {
                return ((DocumentType)htDocumentTypes[company.ToString() + "-" + idDE.ToString()]).Name;
            }
            catch (Exception ex)
            {
                LOG.Error("DocumentTypesConfig.GetNameDE Error " + ex.Message);
            }
            return "";
        }

        //private object GetObjectFromTag(string seed, int index, int type, string value, string parameters)
        //{
        //    object oRet = null;
        //    short w, h;
        //    Image imgAux;
        //    Bitmap resized;
        //    try
        //    {
        //        switch (type)
        //        {
        //            case 0: //String
        //            case 1: //int
        //            case 2: //double
        //                oRet = value;
        //                break;
        //            case 3: //Image asumo JPG
        //                System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(value));
        //                oRet = Properties.Settings.Default.PathTemp + seed + "_" + index + ".jpg";
        //                imgAux = System.Drawing.Image.FromStream(ms);
        //                //int w, h;
        //                if (ExtractParams(parameters, out w, out h))
        //                {
        //                    resized = new Bitmap(imgAux, new Size(w, h));
        //                }
        //                else
        //                {
        //                    resized = new Bitmap(imgAux);
        //                }
        //                resized.Save((string)oRet); 
        //                break;
        //            case 4: //WSQ
        //                oRet = Properties.Settings.Default.PathTemp + seed + "_" + index + ".jpg";
                        
        //                byte[] byRAW;
        //                Bio.Core.Wsq.Decoder.WsqDecoder decoder = new Bio.Core.Wsq.Decoder.WsqDecoder();
        //                if (decoder.DecodeMemory(Convert.FromBase64String(value), out w, out h, out byRAW))
        //                {
        //                    imgAux = Bio.Core.Imaging.ImageProcessor.RawToBitmap(byRAW, w, h);
        //                    //int w, h;
        //                    if (ExtractParams(parameters, out w, out h))
        //                    {
        //                        resized = new Bitmap(imgAux, new Size(w, h));
        //                    }
        //                    else
        //                    {
        //                        resized = new Bitmap(imgAux);
        //                    }
        //                    resized.Save((string)oRet); 
        //                    //if (ExtractParams(parameters, out w, out h))
        //                    //{
        //                    //    imgAux = Bio.Core.Imaging.ImageProcessor.Resize(imgAux, w, h);
        //                    //}
        //                    //imgAux.Save((string)oRet);
        //                }
        //                break;
        //            default:
        //                oRet = null;
        //                break;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("DocumentTypesConfig.GetObjectFromTag Error " + ex.Message);
        //    }
        //    return oRet;
        //}

        private bool ExtractParams(string parameters, out short w, out short h)
        {
            w = 0;
            h = 0;
            try
            {
                if (String.IsNullOrEmpty(parameters)) return false;

                string[] arrP1 = parameters.Split(new string[] { "|" }, StringSplitOptions.None);
                string sW = arrP1[0];
                string[] sWArr = sW.Split(new string[] { "=" }, StringSplitOptions.None);
                w = (short)Convert.ToInt32(sWArr[1]);

                string sH = arrP1[1];
                string[] sHArr = sH.Split(new string[] { "=" }, StringSplitOptions.None);
                h = (short)Convert.ToInt32(sHArr[1]);

            }
            catch (Exception ex)
            {
                LOG.Error("DocumentTypesConfig.GetObjectFromTag Error " + ex.Message);
                return false;
            }
            return true;
        }

        private int GetTypeNumber(string type)
        {
            int ret = Errors.RET_ERR_TYPETAG_INCONSISTENT;
            try
            {
                if (type.Equals("string")) return 0;
                if (type.Equals("int")) return 1;
                if (type.Equals("double")) return 2;
                if (type.Equals("image")) return 3;
                if (type.Equals("wsq")) return 4;
            }
            catch (Exception ex)
            {
                LOG.Error("DocumentTypesConfig.GetTypeNumber Error " + ex.Message);
                ret = Errors.RET_ERR_UNKKNOW;
            }
            return ret;
        }

        private Tag GetTagFromList(string idTag, List<Tag> tagsconfigured)
        {
            try
            {
                for (int i = 0; i < tagsconfigured.Count; i++)
                {
                    if (idTag.Equals(tagsconfigured[i].IdTag)) return tagsconfigured[i];
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DocumentTypesConfig.GetTagFromList Error " + ex.Message);
            }
            return null;
        }

        #endregion Serializable
    }

    public class Company
    {
        public Company() {}
        public int IdCompany;
        public string User;
        public string Password;

        //Como agregar el ABS en PDF??? 

        //public int DocumentSignType;
        //public string CertificatePxf;
        //public string CertificatePfxPsw;
        //public string SignerCloudSignerURL;
        //public string SignerCloudSignerID;
        //public string SignerCloudSignerUser;
        //public string SignerCloudSignerPsw;

        //    0:Sin Firmar 
        //    1: Firma Electronica Simple con PFX + PSW Abajo Default Compañia 
        //    2: Firma Electronica Simple con PFX + PSW Abajo 
        //    3: Servicio Firma e-Sign Abajo
        public int SignatureType;
        public string CertificatePFX;
        public string SignaturePSW;
        public string CertificateCER;
        public string ImageSignatureToShow; //Path imagen que se desea mostrar en la visualizacion de la firma electronica
        public string UrlFEA; // http://esigner6-test.servisign.cl/SignServerEsign/WSIntercambiaDocSoap
        public string AppFEA;
        public string UserFEA;
        public List<DocumentType> DocumentTypes;
    }

    public class DocumentType
    {
        public DocumentType() { }
        public int idDT;
        public string Name;
        public string Description;
        public string Version;
        public string Template;
        public int SignatureType;
        public string CertificatePFX;
        public string SignaturePSW;
        public string CertificateCER;
        public string ImageSignatureToShow; //Path imagen que se desea mostrar en la visualizacion de la firma electronica
        public string UrlFEA; // http://esigner6-test.servisign.cl/SignServerEsign/WSIntercambiaDocSoap
        public string AppFEA;
        public string UserFEA;
        public bool Notarize;
        public List<Tag> Tags;
    }

    public class Tag
    {
        public Tag() { }
        public string IdTag;
        public string Type;
        public string Parameters;
    }

    /// <summary>
    /// Sirve para hacer una lista de TAGS con etiqueta y valor, para que se haga el cehqueo de integridad
    /// y de paso se conviertan los valores al tipo necesario para despues hacer el reemplazo en el templae
    /// </summary>
    public class TagProcessed
    {
        public TagProcessed() { }
        public string IdTag;
        public int Type;
        public object value;
        public string Parameters;
    }


    /*
    <DocumentTypesConfig>
  <Cliente>
    <IdCliente>1</IdCliente>
    <User>Cliente</User>
    <Password>Clave</Password>
    <DocumentTypes>
      <DocumentType>
        <IdDT>1</IdDT>
        <Nombre>ContratoTrabajo</Nombre>
        <Descripcion>Contratos de Trabajo</Descripcion>
        <Version>1</Version>
        <Template>contrato.doc.template</Template>
        <Etiquetas>
            <Etiqueta>
                <IdEtiqueta>#Fecha#</IdEtiqueta>
                <Type>string</Type>
            </Etiqueta>
            <Etiqueta>
                <IdEtiqueta>#Nombre#</IdEtiqueta>
                <Type>string</Type>
            </Etiqueta>
            <Etiqueta>
                <IdEtiqueta>#Huella#</IdEtiqueta>
                <Type>image</Type>
            </Etiqueta>
        </Etiquetas>
      </DocumentType>
    </DocumentTypes>
  </Cliente>
</DocumentTypesConfig>

    */

    //#region Internal Operations

    //        private Hashtable _matchersAvailables = new Hashtable();

    //        ///<summary>
    //        /// Tabla indexada por MinutiaeType, con instancias creadas para trabajar.
    //        ///</summary>
    //        internal Hashtable MatchersAvailables
    //        {
    //            get { return _matchersAvailables; }
    //            set { _matchersAvailables = value; }
    //        }


    //        ///<summary>
    //        /// Recorre la estructura de Matchers Configured, y va generando las
    //        /// instancias en MatcherAvailables. 
    //        ///</summary>
    //        ///<returns>0-OK | -1-Error</returns>
    //        public int Initialization()
    //        {
    //            int ret = 0;
    //            MatcherInstance mi;

    //            lock (_matchersAvailables)
    //            {
    //                try
    //                {
    //                    _matchersAvailables = new Hashtable();
    //                    bool matcherInstanced;
    //                    foreach (MatcherDefinition oMatcher in _matchersConfigured)
    //                    {
    //                        Assembly asmLogin = Assembly.Load(oMatcher.Assembly);
    //                        mi = new MatcherInstance(oMatcher.Name,
    //                                                 oMatcher.Authenticationfactor,
    //                                                 oMatcher.Minutiaetype,
    //                                                 oMatcher.Thresholdextract,
    //                                                 oMatcher.Thresholdmatching,
    //                                                 oMatcher.Assembly,
    //                                                 oMatcher.Parameters);
    //                        matcherInstanced = true;
    //                        //Type currenttype = null;
    //                        object instance;

    //                        Type[] types = null;
    //                        try
    //                        {
    //                            types = asmLogin.GetTypes();
    //                        }
    //                        catch (Exception exloader)
    //                        {
    //                            LOG.Warn("MatcherManager.Initialization.Loader Warn - Activator.CreateInstance(t)", exloader);
    //                        }

    //                        foreach (Type t in types)
    //                        {
    //                            try
    //                            {
    //                                instance = null;
    //                                try
    //                                {
    //                                    instance = Activator.CreateInstance(t);
    //                                }
    //                                catch (Exception exC)
    //                                {
    //                                    LOG.Warn("MatcherManager.Initialization Warn - Activator.CreateInstance(t)", exC);
    //                                    instance = null;
    //                                }

    //                                if (instance != null)
    //                                {
    //                                    if (instance is IMatcher) //(t.Name.IndexOf("Matcher") > -1)
    //                                    {
    //                                        mi.Matcher = (IMatcher)instance;
    //                                        mi.Matcher.AuthenticationFactor = oMatcher.Authenticationfactor;
    //                                        mi.Matcher.MinutiaeType = oMatcher.Minutiaetype;
    //                                        mi.Matcher.Threshold = oMatcher.Thresholdmatching;
    //                                        mi.Matcher.Parameters = oMatcher.Parameters;
    //                                    }
    //                                    if (instance is IExtractor) //(t.Name.IndexOf("Extractor") > -1)
    //                                    {
    //                                        mi.Extractor = (IExtractor)instance;
    //                                        mi.Extractor.AuthenticationFactor = oMatcher.Authenticationfactor;
    //                                        mi.Extractor.MinutiaeType = oMatcher.Minutiaetype;
    //                                        mi.Extractor.Threshold = oMatcher.Thresholdextract;
    //                                        mi.Extractor.Parameters = oMatcher.Parameters;
    //                                    }
    //                                    if (instance is ITemplate) //(t.Name.IndexOf("Template") > -1)
    //                                    {
    //                                        mi.Template = (ITemplate)instance;
    //                                        mi.Template.AuthenticationFactor = oMatcher.Authenticationfactor;
    //                                        mi.Template.MinutiaeType = oMatcher.Minutiaetype;
    //                                    }
    //                                }
    //                            }
    //                            catch (Exception ex)
    //                            {
    //                                LOG.Error("MatcherManager.Initialization - Creando instancias de AF=" +
    //                                          oMatcher.Authenticationfactor + "-MT=" + oMatcher.Minutiaetype, ex);
    //                                matcherInstanced = false;
    //                            }

    //                        }

    //                        //Agrego la minucia con las instancias creadas, si se instancio lo necesario
    //                        if (matcherInstanced)
    //                        {
    //                            _matchersAvailables.Add(mi.Minutiaetype, mi);
    //                        }
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    ret = -1;
    //                    LOG.Error("MatcherManager.Initialization", ex);
    //                }
    //            }
    //            return ret;
    //        }

    //        /// <summary>
    //        /// Metodo para ejecutar el Dispose o Release de cada Matcher de ser necesario
    //        /// liberar temas en cada tecnologia (Ej. Relaese License en VF6).
    //        /// </summary>
    //        ///<returns>0-OK | -1-Error</returns>
    //        public int Release()
    //        {
    //            int ret = 0;

    //            lock (_matchersAvailables)
    //            {
    //                try
    //                {
    //                    foreach (MatcherInstance oMatcher in _matchersAvailables)
    //                    {
    //                        if (oMatcher.Matcher is IDisposable) oMatcher.Matcher.Dispose();
    //                        if (oMatcher.Extractor is IDisposable) oMatcher.Extractor.Dispose();
    //                        if (oMatcher.Template is IDisposable) oMatcher.Template.Dispose();
    //                    }
    //                    _matchersAvailables = null;
    //                }
    //                catch (Exception ex)
    //                {
    //                    ret = -1;
    //                    LOG.Error("MatcherManager.Release", ex);
    //                }
    //            }
    //            return ret;
    //        }

    //        ///<summary>
    //        /// Dado un AF y una MT, devuelve un clon del MinutiaeType, si es que existe en 
    //        /// los MatcherAvailables, sino null.
    //        ///</summary>
    //        ///<param name="authenticationfactor">Tipo de tecnología a utilizar</param>
    //        ///<param name="minutiaetype">Tipo d eminucia a utilizar</param>
    //        ///<returns>Un clon del objeto encontrado o null</returns>
    //        public MatcherInstance GetMatcherInstance(int authenticationfactor, int minutiaetype)
    //        {
    //            MatcherInstance objRet = null;
    //            try
    //            {
    //                if (_matchersAvailables != null)
    //                {
    //                    if (_matchersAvailables.ContainsKey(minutiaetype))
    //                    {
    //                        objRet = (MatcherInstance)_matchersAvailables[minutiaetype];
    //                        if (objRet == null || objRet.Authenticationfactor != authenticationfactor)
    //                        {
    //                            LOG.Warn("MatcherManager.GetMatcherInstance - MatcherInstance con MinutiaeType=" +
    //                                minutiaetype.ToString() + " es nulo o con Authenticationfactor != " +
    //                                authenticationfactor.ToString() +
    //                                ((objRet != null) ? "[" + objRet.Authenticationfactor.ToString() + "]" : "[null]"));
    //                            objRet = null;
    //                        }
    //                    }
    //                }
    //                else
    //                {
    //                    LOG.Warn("MatcherManager.GetMatcherInstance - MatchersAvailables = null");
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                LOG.Error("MatcherManager.GetMatcherInstance Exception", ex);
    //            }
    //            return objRet;
    //        }

    //        /// <summary>
    //        /// Retorna la cantidad de instancias existentes en _matchersAvailables
    //        /// </summary>
    //        /// <returns></returns>
    //        public int QuantityMatchersAvailables()
    //        {
    //            return _matchersAvailables == null ? 0 : _matchersAvailables.Count;
    //        }

    //        /// <summary>
    //        /// Devuelve el Hashtable completo de Matchers Availables
    //        /// </summary>
    //        /// <returns></returns>
    //        public Hashtable GetMatchersAvailables()
    //        {
    //            return _matchersAvailables;
    //        }

    //        /// <summary>
    //        /// Formatea info de Matchers Availables
    //        /// </summary>
    //        /// <returns>String conteniendo los valores de los Matchers instanciados</returns>
    //        public string MatchersAvailablesToString()
    //        {
    //            if (_matchersAvailables == null)
    //            {
    //                return null;
    //            }
    //            string aux = null;
    //            bool first = true;

    //            //for (int i = 0; i < _matchersAvailables.Count; i++)
    //            MatcherInstance ma;
    //            foreach (object o in _matchersAvailables)
    //            {
    //                //MatcherInstance ma = (MatcherInstance)_matchersAvailables[i];
    //                ma = (MatcherInstance)((DictionaryEntry)o).Value;
    //                if (first)
    //                {
    //                    aux = "Name=" + ma.Name + "-" +
    //                          "AF=" + ma.Authenticationfactor.ToString() + "-" +
    //                          "MT=" + ma.Minutiaetype.ToString() + "-" +
    //                          "THExt=" + ma.Thresholdextract.ToString() + "-" +
    //                          "THMat=" + ma.Thresholdmatching.ToString() + "-" +
    //                          "Asm=" + ma.Assembly + "-" +
    //                          "Mat=" + (ma.Matcher != null ? "OK" : "NULL") + "-" +
    //                          "Ext=" + (ma.Extractor != null ? "OK" : "NULL") + "-" +
    //                          "Tpl=" + (ma.Template != null ? "OK" : "NULL");
    //                    first = false;
    //                }
    //                else
    //                {
    //                    aux = aux + "|" +
    //                          "Name=" + ma.Name + "-" +
    //                          "AF=" + ma.Authenticationfactor.ToString() + "-" +
    //                          "MT=" + ma.Minutiaetype.ToString() + "-" +
    //                          "THExt=" + ma.Thresholdextract.ToString() + "-" +
    //                          "THMat=" + ma.Thresholdmatching.ToString() + "-" +
    //                          "Asm=" + ma.Assembly + "-" +
    //                          "Mat=" + (ma.Matcher != null ? "OK" : "NULL") + "-" +
    //                          "Ext=" + (ma.Extractor != null ? "OK" : "NULL") + "-" +
    //                          "Tpl=" + (ma.Template != null ? "OK" : "NULL");

    //                }
    //            }
    //            return aux;
    //        }
    //    }

    //    #endregion Internal Operations

    //    /// <summary>
    //    /// Define una estrucura para levantar los matcher configurados desde
    //    /// XML (Matcher.cfg), para luego inicializar en Availables.
    //    /// </summary>
    //    [Serializable]
    //    public class DocumentTypesDefinition
    //    {
    //        private string _name;
    //        private int _authenticationfactor;
    //        private int _minutiaetype;
    //        private double _thresholdextract;
    //        private double _thresholdmatching;
    //        private string _assembly;
    //        private string _parameters;

    //        ///<summary>
    //        ///</summary>
    //        public string Name
    //        {
    //            get { return _name; }
    //            set { _name = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public int Authenticationfactor
    //        {
    //            get { return _authenticationfactor; }
    //            set { _authenticationfactor = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public int Minutiaetype
    //        {
    //            get { return _minutiaetype; }
    //            set { _minutiaetype = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public double Thresholdextract
    //        {
    //            get { return _thresholdextract; }
    //            set { _thresholdextract = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public double Thresholdmatching
    //        {
    //            get { return _thresholdmatching; }
    //            set { _thresholdmatching = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public string Assembly
    //        {
    //            get { return _assembly; }
    //            set { _assembly = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public string Parameters
    //        {
    //            get { return _parameters; }
    //            set { _parameters = value; }
    //        }
    //    }

    //    /// <summary>
    //    /// Define una estrucura para levantar los matcher configurados y crea las clases
    //    /// para los posteriores clones cuando se deba usar.
    //    /// </summary>
    //    [Serializable]
    //    public class DocumentType
    //    {
    //        internal MatcherInstance(string name, int authenticationfactor, int minutiaetype,
    //                                 double thresholdextract, double thresholdmatching,
    //                                 string assembly, string parameters)
    //        {
    //            _name = name;
    //            _authenticationfactor = authenticationfactor;
    //            _minutiaetype = minutiaetype;
    //            _thresholdextract = thresholdextract;
    //            _thresholdmatching = thresholdmatching;
    //            _assembly = assembly;
    //            _parameters = parameters;
    //        }

    //        private string _name;
    //        private int _authenticationfactor;
    //        private int _minutiaetype;
    //        private double _thresholdextract;
    //        private double _thresholdmatching;
    //        private string _assembly;
    //        private string _parameters;
    //        private IMatcher _matcher;
    //        private IExtractor _extractor;
    //        private ITemplate _template;

    //        ///<summary>
    //        ///</summary>
    //        public string Name
    //        {
    //            get { return _name; }
    //            set { _name = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public int Authenticationfactor
    //        {
    //            get { return _authenticationfactor; }
    //            set { _authenticationfactor = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public int Minutiaetype
    //        {
    //            get { return _minutiaetype; }
    //            set { _minutiaetype = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public double Thresholdextract
    //        {
    //            get { return _thresholdextract; }
    //            set { _thresholdextract = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public double Thresholdmatching
    //        {
    //            get { return _thresholdmatching; }
    //            set { _thresholdmatching = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public string Assembly
    //        {
    //            get { return _assembly; }
    //            set { _assembly = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public string Parameters
    //        {
    //            get { return _parameters; }
    //            set { _parameters = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public IMatcher Matcher
    //        {
    //            get { return _matcher; }
    //            set { _matcher = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public IExtractor Extractor
    //        {
    //            get { return _extractor; }
    //            set { _extractor = value; }
    //        }

    //        ///<summary>
    //        ///</summary>
    //        public ITemplate Template
    //        {
    //            get { return _template; }
    //            set { _template = value; }
    //        }
    //    }
    //}
}