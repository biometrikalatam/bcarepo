﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bio.Document.Manager.Api;

namespace Bio.Document.Manager.Config.Source.Helpers
{
    public class DEHelper
    {
         public static DocumentTypesConfig GetDTConfig()
         {
            DocumentTypesConfig config = null;
            int ret = 0;
            string sconfig = null;

            using (WSAdmin.WSAdmin ws = new WSAdmin.WSAdmin())
            {
                ws.Timeout = 60000;
                ret = ws.GetDTConfig(out sconfig);

                if (ret == 0)
                {
                    config = Api.XmlUtils.DeserializeObject<DocumentTypesConfig>(sconfig);
                }
            }

            return config;
         }

    }
}
