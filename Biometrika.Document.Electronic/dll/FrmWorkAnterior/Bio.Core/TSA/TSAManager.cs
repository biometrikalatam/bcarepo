﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Bio.Core.Api.TSA.Interface;
using log4net;
using Bio.Core.Api.Constant;
using System.Threading;
using System.IO;

namespace Bio.Core.TSA
{
    /// <summary>
    /// Esta clase esta destinada a levantar los tipos de TimeStamping existentes 
    /// y crear las clases base, para clonar cuando sea neceario usarlas.
    /// </summary>
    [Serializable]
    public class TSAManager
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(TSAManager));

#region Serializable
        
        private List<TSADefinition> _TSAConfigured;
        private int _qReintentos = 0;
        private bool _storeEnabled = false; //Habilita a guardar los datos en disco para reintentar enviar nuevamente
        private string _storePath = @"\ToResend\";
        private bool _resendON = false;           //Habilita el reenvio si esta en TRUE
        private int _storeONInterval = 60000;   //Cada cuanto se despierta el thread para reintentar enviar si esta _storeON en TRUE


        ///<summary>
        /// Lista de Matchers configurados en config XML.
        ///</summary>
        public List<TSADefinition> TSAConfigured
        {
            get { return _TSAConfigured; }
            set { _TSAConfigured = value; }
        }

        ///<summary>
        /// Tabla indexada por Order, con instancias creadas para trabajar.
        ///</summary>
        internal Hashtable TSAAvailables
        {
            get { return _TSAAvailables; }
            set { _TSAAvailables = value; }
        }

        public int QReintentos
        {
            get
            {
                return _qReintentos;
            }

            set
            {
                _qReintentos = value;
            }
        }

        public bool StoreEnabled
        {
            get
            {
                return _storeEnabled;
            }

            set
            {
                _storeEnabled = value;
            }
        }

        public string StorePath
        {
            get
            {
                return _storePath;
            }

            set
            {
                _storePath = value;
            }
        }

        public bool ResendON
        {
            get
            {
                return _resendON;
            }

            set
            {
                _resendON = value;
            }
        }

        public int StoreONInterval
        {
            get
            {
                return _storeONInterval;
            }

            set
            {
                _storeONInterval = value;
            }
        }

        #endregion Serializable

        #region Internal Operations
        private Hashtable _TSAAvailables = new Hashtable();

        public TSAManager() { }

        public TSAManager(int _qReintentos, bool _storeEnabled, string _storePath, bool _resendON, int _storeONInterval)
        {
            this.QReintentos = _qReintentos;
            this.StoreEnabled = _storeEnabled;
            this.StorePath = _storePath;
            this.ResendON = _resendON;
            this.StoreONInterval = _storeONInterval;
        }




        ///<summary>
        /// Recorre la estructura de Matchers Configured, y va generando las
        /// instancias en MatcherAvailables. 
        ///</summary>
        ///<returns>0-OK | -1-Error</returns>
        public int Initialization()
        {
            int ret = 0;
            TSAInstance tsai;

            lock (_TSAAvailables)
            {
                try
                {
                    _TSAAvailables = new Hashtable();
                    bool tsaInstanced;
                    foreach (TSADefinition oTSA in _TSAConfigured)
                    {
                        Assembly asmLogin = Assembly.Load(oTSA.Assembly);
                        tsai = new TSAInstance(oTSA.Order, oTSA.Name,
                                                 oTSA.UrlGeneration,
                                                 oTSA.UrlVerification, 
                                                 oTSA.TimeoutWS,
                                                 oTSA.Assembly,
                                                 oTSA.Parameters);
                        tsaInstanced = true;
                        //Type currenttype = null;
                        object instance;

                        Type[] types = null;
                        try
                        {
                            types = asmLogin.GetTypes();
                        } catch (Exception exloader)
                        {
                            LOG.Warn("TSAManager.Initialization.Loader Warn - Activator.CreateInstance(t)", exloader);
                        }

                        foreach (Type t in types)
                        {
                            try
                            {
                                instance = null;
                                try
                                {
                                    instance = Activator.CreateInstance(t);
                                } catch (Exception exC)
                                {
                                    LOG.Warn("TSAManager.Initialization Warn - Activator.CreateInstance(t)", exC);
                                    instance = null;
                                }

                                if (instance != null)
                                {
                                    if (instance is ITSAGenerator) //(t.Name.IndexOf("Matcher") > -1)
                                    {
                                        tsai.TSAGenerator = (ITSAGenerator) instance;
                                        tsai.TSAGenerator.UrlGeneration = oTSA.UrlGeneration;
                                        tsai.TSAGenerator.UrlVerification = oTSA.UrlVerification;
                                        tsai.TSAGenerator.TimeoutWS = oTSA.TimeoutWS;
                                        tsai.TSAGenerator.Parameters = oTSA.Parameters;
                                        tsai.TSAGenerator.ConfigureTSAGenerator();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LOG.Error("TSAManager.Initialization - Creando instancias de UrlG=" +
                                          oTSA.UrlGeneration + "-UrlV=" + oTSA.UrlVerification, ex);
                                tsaInstanced = false;
                            }
                            
                        }

                        //Agrego la minucia con las instancias creadas, si se instancio lo necesario
                        if (tsaInstanced)
                        {
                            _TSAAvailables.Add(tsai.Order, tsai);
                        }
                    }

                    //Chequeo si debo setear Thread para resend a TSAs
                    if (_resendON)
                    {
                        Thread tResednToTS = new Thread(new ThreadStart(ThreadresendToTSA));
                        tResednToTS.Start();
                        LOG.Info("TSAManager.Initialization - Thread to Resend ThreadresendToTSA Iniciado!");
                    }
                }
                catch (Exception ex)
                {
                    ret = -1;
                    LOG.Error("TSAManager.Initialization", ex);
                }
            }
            return ret;
        }

        private void ThreadresendToTSA()
        {
            bool bRunning = true;
            int qResent = 0;
            try
            {
                LOG.Info("TSAManager.ThreadresendToTSA Start - IN...");
                while (bRunning)
                {
                    qResent = 0;
                    //Chequeo licencia, y si da falso, entonces sale del loop
                    qResent = ResendToTS();

                    LOG.Info("TSAManager.ThreadresendToTSA - " +
                                "Procesados " +
                                qResent.ToString() + " archivos encolados!");
                    //Duerme _storeONInterval y rechequea 
                    Thread.Sleep(_storeONInterval);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("TSAManager.ThreadresendToTSA - " + ex.Message);
            }
        }

        private int ResendToTS()
        {
            /*   Obtiene lista de archivos desde sistema de archivos         */
            /*****************************************************************/
            int ret = 0;
            String sRes = "";
            DateTime dStart, dEnd;
            dStart = DateTime.Now;
            int qResent = 0;

            try
            {
                LOG.Info("TSAManager.ResendToTS Start - " + dStart.ToString("dd/MM/yyyy HH:mm:ss"));
                DirectoryInfo dir = new DirectoryInfo(_storePath);

                LOG.Info("TSAManager.ResendToTS Start >> Cantidad archivos de directorio: " + dir.GetFiles("*.sem").Length.ToString());
                
                foreach (FileInfo f in dir.GetFiles("*.sem"))
                {
                    //TODO POr cada .sem, leer el .xml, e intentar reenviar
                    // sumando los que se pueden hacer y guardando en algun otro directorio 
                    //los TS, para luego encadenarlos a la operacion correcta
                    //VER SI VALE LA PENA
                }
            }
            catch (Exception ex)
            {
                LOG.Info("TSAManager.ResendToTS Error - " + ex.Message);
                ret = -1;
            }

            LOG.Info("TSAManager.ResendToTS End - " + dStart.ToString("dd/MM/yyyy HH:mm:ss") + "!!");
            return ret;
        }
    

        /// <summary>
        /// Metodo para ejecutar el Dispose o Release de cada Matcher de ser necesario
        /// liberar temas en cada tecnologia (Ej. Relaese License en VF6).
        /// </summary>
        ///<returns>0-OK | -1-Error</returns>
        public int Release()
        {
            int ret = 0;
           
            lock (_TSAAvailables)
            {
                try
                {
                    foreach (TSAInstance oTSA in _TSAAvailables)
                    {
                        if (oTSA.TSAGenerator is IDisposable) oTSA.TSAGenerator.Dispose();
                    }
                    _TSAAvailables = null;
                }
                catch (Exception ex)
                {
                    ret = -1;
                    LOG.Error("TSAManager.Release", ex);
                }
            }
            return ret;
        }

        ///<summary>
        /// Dado un orden devuelve el TSAGenerator correspondiente si está
        ///</summary>
        ///<param name="order">Tipo de tecnología a utilizar</param>
        ///<returns>Un clon del objeto encontrado o null</returns>
        public TSAInstance GetTSAInstance(int order)
        {
            TSAInstance objRet = null;
            try
            {
                if (_TSAAvailables != null)
                {
                    if (_TSAAvailables.ContainsKey(order))
                    {
                        objRet = (TSAInstance)_TSAAvailables[order];
                        //if (objRet == null || objRet.Authenticationfactor != authenticationfactor)
                        //{
                        //    LOG.Warn("MatcherManager.GetMatcherInstance - MatcherInstance con MinutiaeType=" +
                        //        minutiaetype.ToString() + " es nulo o con Authenticationfactor != " +
                        //        authenticationfactor.ToString() + 
                        //        ((objRet != null) ? "[" + objRet.Authenticationfactor.ToString() + "]" : "[null]"));
                        //    objRet = null;
                        //} 
                    }
                }
                else
                {
                    LOG.Warn("MatcherManager.GetMatcherInstance - MatchersAvailables = null");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("MatcherManager.GetMatcherInstance Exception", ex);
            }
            return objRet;
        }

        /// <summary>
        /// Retorna la cantidad de instancias existentes en _TSAAvailables
        /// </summary>
        /// <returns></returns>
        public int QuantityTSAAvailables()
        {
            return _TSAAvailables == null ? 0 : _TSAAvailables.Count;
        }

        /// <summary>
        /// Devuelve el Hashtable completo de TSA Availables
        /// </summary>
        /// <returns></returns>
        public Hashtable GetTSAAvailables()
        {
            return _TSAAvailables;  
        }

        /// <summary>
        /// Formatea info de Matchers Availables
        /// </summary>
        /// <returns>String conteniendo los valores de los Matchers instanciados</returns>
        public string TSAAvailablesToString()
        {
            if (_TSAAvailables == null)
            {
                return null;
            }
            string aux = null;
            bool first = true;
           
            //for (int i = 0; i < _TSAAvailables.Count; i++)
            TSAInstance ma;
            foreach (object o in _TSAAvailables)
            {
                //MatcherInstance ma = (MatcherInstance)_TSAAvailables[i];
                ma = (TSAInstance) ((DictionaryEntry)o).Value;
                if (first)
                {
                    aux = "Order=" + ma.Order.ToString() + "-" +
                          "Name=" + ma.Name + "-" +
                          "UrlG=" + ma.UrlGeneration + "-" +
                          "UrlV=" + ma.UrlVerification + "-" +
                          "Timeout=" + ma.TimeoutWS.ToString() + "-" +
                          "Asm=" + ma.Assembly + "-" +
                          "TSAGenerator=" + (ma.TSAGenerator != null ? "OK" : "NULL");
                    first = false;
                }
                else
                {
                    aux = aux + "|" +
                          "Order=" + ma.Order.ToString() + "-" +
                          "Name=" + ma.Name + "-" +
                          "UrlG=" + ma.UrlGeneration + "-" +
                          "UrlV=" + ma.UrlVerification + "-" +
                          "Timeout=" + ma.TimeoutWS.ToString() + "-" +
                          "Asm=" + ma.Assembly + "-" +
                          "TSAGenerator=" + (ma.TSAGenerator != null ? "OK" : "NULL");
                }
            }
            return aux;
        }


        public int GenerateTS(string sdocument, out string xmlts)
        {
            int ret = 0;
            xmlts = null;

            try
            {
                //Tomo cada TSA definido en el Hashtable y pruebo desde el 0 al N, con M cantidad de reintentos
                // de acuerdo a lo definido, y sino, grbo en disco. Deo hacr un Thread que intente reenvío cada 
                // cierto tiempo: si hay xmls en el directorio definido, se usa el generator como la primera vez 
                if (_qReintentos == 0) GenerateTSInt(sdocument, true, out xmlts);
                else
                    for (int i = 0; i < _qReintentos; i++)
                    {
                        ret = GenerateTSInt(sdocument, (i == _qReintentos - 1), out xmlts);
                        if (ret == 0 && xmlts!= null) break; //Si genero, salgo del for
                    }
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.TSA.TSAManager.GenerateTS Error [" + ex.Message + "]");
                ret = Errors.IERR_UNKNOWN;
            }
            
            return ret;
        }

        private int GenerateTSInt(string sdocument, bool vUseStore, out string xmlts)
        {
            int ret = 0;
            xmlts = null;

            try
            {
                //Hago for por los TSAAvailables y trato de hacer TS, sino 
                //veo si hay que almacenar en disco si esta habilitado porque
                //es el ultimo itento configurado.
                for (int i = 0; i < TSAAvailables.Count; i++)
                {
                    //TSAInstance instance = (TSAInstance)TSAAvailables[i];
                    //ITSAGenerator Generator = instance.TSAGenerator;
                    //Generator.GenerateTS(sdocument, out xmlts);
                    ret = (((TSAInstance)TSAAvailables[i]).TSAGenerator).GenerateTS(sdocument, out xmlts);
                    if (!String.IsNullOrEmpty(xmlts)) break;
                }

                //Controlo si está ablitado el store ante fallas, y si se llamo a este metodo con 
                //la opcion de grabar en disco. 
                if (String.IsNullOrEmpty(xmlts) && vUseStore && this._storeEnabled)
                {
                    ret = SaveInfoInStoreToResend(sdocument);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.TSA.TSAManager.GenerateTS Error [" + ex.Message + "]");
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }

        private int SaveInfoInStoreToResend(string sdocument)
        {
            int ret = 0;

            try
            {
                LOG.Debug("TSAManager.SaveInfoInStoreToResend - IN...");
                //Grabo en disco en el path definido, un xml para generacion posterior 
                if (_storeEnabled) {
                    if (String.IsNullOrEmpty(_storePath) || !System.IO.Directory.Exists(_storePath))
                    {
                        LOG.Warn("TSAManager.SaveInfoInStoreToResend Error - _storePath Nulo o No Existente! [" + _storePath + "]");
                        ret = Errors.IERR_BAD_PARAMETER;
                    } else
                    {
                        string sXML = "<TSAPending>" +
                                           "<DataToTS>" + sdocument + "</DataToTS>" + 
                                      "</TSAPending>";
                        System.IO.File.WriteAllText(_storePath +
                            "tsapending_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml", sXML);
                        System.IO.File.WriteAllText(_storePath +
                            "tsapending_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml", 
                            "Semaforo de tsapending_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml.sem");
                        ret = 1;
                    }

                }
                LOG.Debug("TSAManager.SaveInfoInStoreToResend - TSAPending XML Grabado en " + _storePath);
                LOG.Debug("TSAManager.SaveInfoInStoreToResend - OUT!");
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.TSA.TSAManager.SaveInfoInStoreToResend Error [" + ex.Message + "]");
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }
    }

#endregion Internal Operations

    /// <summary>
    /// Define una estrucura para levantar los TSA configurados desde
    /// XML (TSA.cfg), para luego inicializar en Availables.
    /// </summary>
    [Serializable]
    public class TSADefinition
    {
        private int _order;  //Indica el orden en que se usará este TSA si falla el anterior, en caso de varios configurados
        private string _name;
        private string _urlgeneration;
        private string _urlverification;
        private int _timeoutws;
        private string _assembly;
        private string _parameters;


        ///<summary>
        ///</summary>
        public int Order
        {
            get { return _order; }
            set { _order = value; }
        }

        ///<summary>
        ///</summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        ///<summary>
        ///</summary>
        public string UrlGeneration
        {
            get { return _urlgeneration; }
            set { _urlgeneration = value; }
        }

        ///<summary>
        ///</summary>
        public string UrlVerification
        {
            get { return _urlverification; }
            set { _urlverification = value; }
        }

        ///<summary>
        ///</summary>
        public int TimeoutWS
        {
            get { return _timeoutws; }
            set { _timeoutws = value; }
        }

        ///<summary>
        ///</summary>
        public string Assembly
        {
            get { return _assembly; }
            set { _assembly = value; }
        }

        ///<summary>
        ///</summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }
    }

    /// <summary>
    /// Define una estrucura para levantar los matcher configurados y crea las clases
    /// para los posteriores clones cuando se deba usar.
    /// </summary>
    [Serializable]
    public class TSAInstance
    {
       
        internal TSAInstance(int order, string name, string urlgeneration, string urlverification, int timeoutws, 
                                 string assembly, string parameters)
        {
            _order = order;
            _name = name;
            _urlgeneration = urlgeneration;
            _urlverification = urlverification;
            _timeoutws = timeoutws;
            _assembly = assembly;
            _parameters = parameters;
        }

        private int _order;
        private string _name;
        private string _urlgeneration;
        private string _urlverification;
        private int _timeoutws;
        private string _assembly;
        private string _parameters;
        private ITSAGenerator _tsagenerator;

        ///<summary>
        ///</summary>
        public int Order
        {
            get { return _order; }
            set { _order = value; }
        }

        ///<summary>
        ///</summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        ///<summary>
        ///</summary>
        public string UrlGeneration
        {
            get { return _urlgeneration; }
            set { _urlgeneration = value; }
        }

        ///<summary>
        ///</summary>
        public string UrlVerification
        {
            get { return _urlverification; }
            set { _urlverification = value; }
        }

        ///<summary>
        ///</summary>
        public int TimeoutWS
        {
            get { return _timeoutws; }
            set { _timeoutws = value; }
        }

        ///<summary>
        ///</summary>
        public string Assembly
        {
            get { return _assembly; }
            set { _assembly = value; }
        }

        ///<summary>
        ///</summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        ///<summary>
        ///</summary>
        public ITSAGenerator TSAGenerator
        {
            get { return _tsagenerator; }
            set { _tsagenerator = value; }
        }
    }
}