using System;
using System.Security.Cryptography;
using System.Text;

namespace Biokey.pki.mentalis
{
	/// <summary>
	/// Descripción breve de BOKSigner.
	/// </summary>
	public class BOKSigner
	{

		#region Constructores		

		public BOKSigner() {}

		public BOKSigner(string path, string psw)
		{
			this.pkcs12 = new BOKpkcs12(path,psw);
		}

		public BOKSigner(BOKpkcs12 oPKCS12) {
			this.pkcs12 = oPKCS12;
		}
		#endregion Constructores		

		#region Methods
		public static bool GetFirma(ref object firma, string data, string hashMethod,
									bool enBase64, string path, string psw) {
			try {
				// Create a UnicodeEncoder to convert between byte array and string.
				ASCIIEncoding ByteConverter = new ASCIIEncoding();

				// Create byte arrays to hold original, encrypted, and decrypted data.
				byte[] originalData = ByteConverter.GetBytes(data);
				byte[] signedData = null;

				AsymmetricAlgorithm alg = BOKpkcs12.GetPrivateKeyFromPFX(path,psw);
				if (hashMethod == null || hashMethod.Trim().Equals("SHA1"))
				{
					signedData = HashAndSignBytes(originalData, alg, 
												  new SHA1CryptoServiceProvider(),	
											      0, originalData.Length );				
				} else
				{
					signedData = HashAndSignBytes(originalData, alg, 
												  new MD5CryptoServiceProvider(),	
						                          0, originalData.Length );
				} 
				if (enBase64)
				{
					firma = Convert.ToBase64String(signedData);
				} else
				{
					firma = signedData;
				}
			}
			catch (Exception ex) {
				Console.WriteLine(ex.Message);
				return false;
			}
			return true;
		} 

		public bool GetFirma(ref object firma, string data, 
							 string hashMethod,bool enBase64) {
			try {
				// Create a UnicodeEncoder to convert between byte array and string.
				ASCIIEncoding ByteConverter = new ASCIIEncoding();

				// Create byte arrays to hold original, encrypted, and decrypted data.
				byte[] originalData = ByteConverter.GetBytes(data);
				byte[] signedData = null;

				AsymmetricAlgorithm alg = pkcs12.GetPrivateKeyFromPFX();
				if (hashMethod == null || hashMethod.Trim().Equals("SHA1")) {
					signedData = HashAndSignBytes(originalData,alg,
												  new SHA1CryptoServiceProvider(),	
												  0, originalData.Length );				
				} else {
					signedData = HashAndSignBytes(originalData, alg,
						  						  new MD5CryptoServiceProvider(),	
												  0, originalData.Length );
				} 
				if (enBase64) {
					firma = Convert.ToBase64String(signedData);
				} else {
					firma = signedData;
				}
			}
			catch (Exception ex) {
				Console.WriteLine(ex.Message);
				return false;
			}
			return true;
		} 




		public static byte[] HashAndSignBytes(byte[] DataToSign, AsymmetricAlgorithm Key,  
											  HashAlgorithm halg, int Index, int Length) {
			try {   

				if (Key.SignatureAlgorithm.IndexOf("RSA") >= 0 || Key.SignatureAlgorithm.IndexOf("rsa") >= 0) {
					RSACryptoServiceProvider RSACryp = (RSACryptoServiceProvider)Key;
					// Hash and sign the data. Pass a new instance of SHA1CryptoServiceProvider
					// to specify the use of SHA1 for hashing.
					return RSACryp.SignData(DataToSign,Index,Length, halg);
				} else {
					DSACryptoServiceProvider DSACryp = (DSACryptoServiceProvider)Key;
					// Hash and sign the data. Pass a new instance of SHA1CryptoServiceProvider
					// to specify the use of SHA1 for hashing.
					return DSACryp.SignData(DataToSign,Index,Length);
				}

			}
			catch(Exception ex) {
				Console.WriteLine(ex.Message);
				return null;
			}
		}

		//VALIDACION
		public static bool VerifyFirma(object signature, string data,
									   bool enBase64, string hashMethod,
									   string path, string psw) {
			try {
				AsymmetricAlgorithm alg = BOKpkcs12.GetPublicKeyFromPFX(path,psw);

				// Create a UnicodeEncoder to convert between byte array and string.
				ASCIIEncoding ByteConverter = new ASCIIEncoding();

				// Create byte arrays to hold original, encrypted, and decrypted data.
				byte[] originalData = ByteConverter.GetBytes(data);
				byte[] signedData = null;
				if (enBase64)
				{
					signedData = Convert.FromBase64String((string)signature);
				} else
				{
					signedData = (byte[])signature;
				}
				
				if (hashMethod == null || hashMethod.Trim().Equals("SHA1")) {
					return VerifySignedHash(originalData,signedData,alg,
									new SHA1CryptoServiceProvider());				
				} else {
					return VerifySignedHash(originalData,signedData,alg,
									new MD5CryptoServiceProvider());				
				} 
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}
			return false;
		}

		public  bool VerifyFirma(object signature, string data,
							     bool enBase64, string hashMethod) {
			try {
				AsymmetricAlgorithm alg = this.pkcs12.GetPublicKeyFromPFX();

				// Create a UnicodeEncoder to convert between byte array and string.
				ASCIIEncoding ByteConverter = new ASCIIEncoding();

				// Create byte arrays to hold original, encrypted, and decrypted data.
				byte[] originalData = ByteConverter.GetBytes(data);
				byte[] signedData = null;
				if (enBase64) {
					signedData = Convert.FromBase64String((string)signature);
				} else {
					signedData = (byte[])signature;
				}
				
				if (hashMethod == null || hashMethod.Trim().Equals("SHA1")) {
					return VerifySignedHash(originalData,signedData,alg,
						new SHA1CryptoServiceProvider());				
				} else {
					return VerifySignedHash(originalData,signedData,alg,
						new MD5CryptoServiceProvider());				
				} 
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}
			return false;
		}

		public static bool VerifySignedHash(byte[] DataToVerify, byte[] SignedData, 
									AsymmetricAlgorithm Key, HashAlgorithm halg) {
			try {
				if (Key.SignatureAlgorithm.IndexOf("RSA") >= 0 || Key.SignatureAlgorithm.IndexOf("rsa") >= 0) {
					RSACryptoServiceProvider RSACryp = (RSACryptoServiceProvider)Key;
					// Hash and sign the data. Pass a new instance of SHA1CryptoServiceProvider
					// to specify the use of SHA1 for hashing.
					return RSACryp.VerifyData(DataToVerify, halg, SignedData); 
				} else {
					DSACryptoServiceProvider DSACryp = (DSACryptoServiceProvider)Key;
					// Hash and sign the data. Pass a new instance of SHA1CryptoServiceProvider
					// to specify the use of SHA1 for hashing.
					return DSACryp.VerifyData(DataToVerify, SignedData); 
				}
			}
			catch(CryptographicException e) {
				Console.WriteLine(e.Message);

				return false;
			}
		}


		//VERIFY
		//		RSA pk = corg.PublicKey;
		//		RSACryptoServiceProvider cryV = (RSACryptoServiceProvider)pk;
		//		// Verify the data and display the result to the 
		//		// console.  
		//		if(VerifySignedHash(originalData, signedData, cryV)) {
		//		Console.WriteLine("The data was verified.");
		//	}
		//	else {
		//	Console.WriteLine("The data does not match the signature.");
		//}



//		public static bool VerifySignedHash(byte[] DataToVerify, 
//			byte[] SignedData, RSAParameters Key) {
//			try {
//				// Create a new instance of RSACryptoServiceProvider using the 
//				// key from RSAParameters.
//				RSACryptoServiceProvider RSAalg = new RSACryptoServiceProvider();
//
//				RSAalg.ImportParameters(Key);
//
//				// Verify the data using the signature.  Pass a new instance of SHA1CryptoServiceProvider
//				// to specify the use of SHA1 for hashing.
//				return RSAalg.VerifyData(DataToVerify, new SHA1CryptoServiceProvider(), SignedData); 
//
//			}
//			catch(CryptographicException e) {
//				Console.WriteLine(e.Message);
//
//				return false;
//			}
//		}
//
////		public static bool VerifySignedHash(byte[] DataToVerify, 
////			byte[] SignedData, RSACryptoServiceProvider Key) {
////			try {
////				// Create a new instance of RSACryptoServiceProvider using the 
////				// key from RSAParameters.
////				//				RSACryptoServiceProvider RSAalg = new RSACryptoServiceProvider();
////				//
////				//				RSAalg.ImportParameters(Key);
////
////				// Verify the data using the signature.  Pass a new instance of SHA1CryptoServiceProvider
////				// to specify the use of SHA1 for hashing.
////				return Key.VerifyData(DataToVerify, new SHA1CryptoServiceProvider(), SignedData); 
////
////			}
////			catch(CryptographicException e) {
////				Console.WriteLine(e.Message);
////
////				return false;
////			}
////		}
//
//		public static byte[] HashAndSignBytes(byte[] DataToSign, RSAParameters Key, int Index, int Length) {
//			try {   
//				// Create a new instance of RSACryptoServiceProvider using the 
//				// key from RSAParameters.  
//				RSACryptoServiceProvider RSAalg = new RSACryptoServiceProvider();
//
//				RSAalg.ImportParameters(Key);
//
//				// Hash and sign the data. Pass a new instance of SHA1CryptoServiceProvider
//				// to specify the use of SHA1 for hashing.
//				return RSAalg.SignData(DataToSign,Index,Length, new SHA1CryptoServiceProvider());
//			}
//			catch(CryptographicException e) {
//				Console.WriteLine(e.Message);
//
//				return null;
//			}
//		}
		#endregion Methods

		#region Private
		BOKpkcs12 pkcs12;
		#endregion Private

	}
}
