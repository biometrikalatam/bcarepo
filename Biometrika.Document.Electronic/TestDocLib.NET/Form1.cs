﻿using SautinSoft.Document;
using SautinSoft.Document.Drawing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection.Emit;
using System.Reflection;
using System.Text.RegularExpressions;

namespace TestDocLib.NET
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            FindAndReplaceImageInParagraphs(1, txtold.Text.Trim(), txtnew.Text.Trim(), Convert.ToDouble(this.txtX.Text), Convert.ToDouble(this.txtY.Text), Convert.ToDouble(this.txtW.Text), Convert.ToDouble(this.txtH.Text));
            FindAndReplaceInParagraphs(1, txtnew.Text.Trim(), "RUT 21284415-2");
            // MailMergeSimpleEnvelope();
            //Type t = BuildType();
            //MailMergeSimpleDynamic();
            //FindAndReplaceImage(1, txtold.Text.Trim(), txtnew.Text.Trim());
        }

        /// <summary>
        /// Find and replace a specific text in all paragraphs in PDF document.
        /// </summary>
        /// </remarks>
        /// Details: https://sautinsoft.com/products/document/examples/replace-text-paragraphs-in-pdf-document-net-csharp-vb.php
        /// </remarks>
        static void FindAndReplaceInParagraphs(int type, string oldtext, string newtext)
        {
            string filePath = @"C:\tmp\DocLibTest\ResultNV2.pdf";
            string fileResult = @"C:\tmp\DocLibTest\ResultNV2.1.pdf";
            if (type == 0)
            {
                filePath = @"C:\tmp\DocLibTest\Original.docx";
                fileResult = @"C:\tmp\DocLibTest\ResultW.pdf";
            }

            DocumentCore dc = DocumentCore.Load(filePath);
            foreach (Paragraph par in dc.GetChildElements(true, ElementType.Paragraph))
                foreach (ContentRange item in par.Content.Find(oldtext).Reverse())
                {
                    item.Replace(newtext);
                }
            dc.Save(fileResult, SaveOptions.PdfDefault);
            //System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(filePath) { UseShellExecute = true });
            //System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(fileResult) { UseShellExecute = true });
        }

        static void FindAndReplaceImageInParagraphs(int type, string oldtext, string newtext, double x, double y, double w, double h)
        {
            string filePath = @"C:\tmp\DocLibTest\NVCICertificate_v2.pdf";
            string fileResult = @"C:\tmp\DocLibTest\ResultNV2.pdf";
            if (type == 0)
            {
                filePath = @"C:\tmp\DocLibTest\Original1.docx";
                fileResult = @"C:\tmp\DocLibTest\ResultW1.pdf";
            }

            string pictPath = @"C:\tmp\DocLibTest\foto.jpg";
            DocumentCore dc = DocumentCore.Load(filePath);
            Regex regex = new Regex(oldtext, RegexOptions.IgnoreCase);
            Picture pict1 = new Picture(dc, InlineLayout.Floating(new HorizontalPosition(x, LengthUnit.Millimeter, HorizontalPositionAnchor.Page),
                                                                  new VerticalPosition(y, LengthUnit.Millimeter, VerticalPositionAnchor.Page),
                                                                  new SautinSoft.Document.Drawing.Size(w, h)), pictPath);
            //Picture pict1 = new Picture(dc, InlineLayout.Inline(new SautinSoft.Document.Drawing.Size(77, 80)), pictPath);
            foreach (ContentRange item in dc.Content.Find(regex).Reverse())
            {
                item.Replace(pict1.Content);
            }


            //foreach (Paragraph par in dc.GetChildElements(true, ElementType.Paragraph))
            //    foreach (ContentRange item in par.Content.Find(oldtext).Reverse())
            //    {
            //        SautinSoft.Document.Drawing.Picture img =
            //            new SautinSoft.Document.Drawing.Picture(dc, InlineLayout.Inline(new SautinSoft.Document.Drawing.Size(50, 50)), @"C:\tmp\DocLibTest\foto.jpg");

            //        //ContentRange cr = new ContentRange(item.Start, item.End);
            //        //cr.Start.Insert(img);
            //        item.Delete();
            //        //img.Layout = FloatingLayout.Floating(
            //        //        new HorizontalPosition(50, LengthUnit.Millimeter, HorizontalPositionAnchor.Page),
            //        //        new VerticalPosition(20, LengthUnit.Millimeter, VerticalPositionAnchor.TopMargin),
            //        //        new SautinSoft.Document.Drawing.Size(LengthUnitConverter.Convert(10, LengthUnit.Centimeter, LengthUnit.Point),
            //        //                 LengthUnitConverter.Convert(10, LengthUnit.Centimeter, LengthUnit.Point))
            //        //     );
            //        //img.Layout = FloatingLayout.Inline(new SautinSoft.Document.Drawing.Size(50, 50));

            //        // Set the wrapping style.
            //        //(img.Layout as FloatingLayout).WrappingStyle = WrappingStyle.Square; // BehindText;
            //        par.Inlines.Add(img);

            //    }
            //foreach (SautinSoft.Document.Drawing.Picture par in dc.GetChildElements(true, ElementType.Picture))
            //    foreach (ContentRange item in par.Content.Find(oldtext).Reverse())
            //    {
            //        ContentRange crn = new ContentRange(item.Start, item.End);

            //        item.Replace(newtext);
            //    }
            dc.Save(fileResult, SaveOptions.PdfDefault);
            //System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(filePath) { UseShellExecute = true });
            //System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(fileResult) { UseShellExecute = true });
        }

        static void FindAndReplaceImage(int type, string oldtext, string newtext)
        {
            string filePath = @"C:\tmp\DocLibTest\Original1.pdf";
            string fileResult = @"C:\tmp\DocLibTest\Result1.pdf";
            if (type == 0)
            {
                filePath = @"C:\tmp\DocLibTest\Original1.docx";
                fileResult = @"C:\tmp\DocLibTest\ResultW1.pdf";
            }


            DocumentCore dc = DocumentCore.Load(filePath);
            foreach (Picture par in dc.GetChildElements(true, ElementType.Picture))
                foreach (ContentRange item in par.Content.Find(oldtext).Reverse())
                {
                    SautinSoft.Document.Drawing.Picture img =
                        new SautinSoft.Document.Drawing.Picture(dc, InlineLayout.Inline(new SautinSoft.Document.Drawing.Size(50, 50)), @"C:\tmp\DocLibTest\foto.jpg");

                    //ContentRange cr = new ContentRange(item.Start, item.End);
                    //cr.Start.Insert(img);
                    item.Delete();
                    //img.Layout = FloatingLayout.Floating(
                    //        new HorizontalPosition(50, LengthUnit.Millimeter, HorizontalPositionAnchor.Page),
                    //        new VerticalPosition(20, LengthUnit.Millimeter, VerticalPositionAnchor.TopMargin),
                    //        new SautinSoft.Document.Drawing.Size(LengthUnitConverter.Convert(10, LengthUnit.Centimeter, LengthUnit.Point),
                    //                 LengthUnitConverter.Convert(10, LengthUnit.Centimeter, LengthUnit.Point))
                    //     );
                    //img.Layout = FloatingLayout.Inline(new SautinSoft.Document.Drawing.Size(50, 50));

                    // Set the wrapping style.
                    //(img.Layout as FloatingLayout).WrappingStyle = WrappingStyle.Square; // BehindText;
                    //par.Inlines.Add(img);

                }
            //foreach (SautinSoft.Document.Drawing.Picture par in dc.GetChildElements(true, ElementType.Picture))
            //    foreach (ContentRange item in par.Content.Find(oldtext).Reverse())
            //    {
            //        ContentRange crn = new ContentRange(item.Start, item.End);

            //        item.Replace(newtext);
            //    }
            dc.Save(fileResult, SaveOptions.PdfDefault);
            //System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(filePath) { UseShellExecute = true });
            //System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(fileResult) { UseShellExecute = true });
        }


        /// <summary>
        /// Generates 5 envelopes "Happy New Year" for Simpson family using the one template.
        /// </summary>
        /// <remarks>
        /// See details at: https://sautinsoft.com/products/document/examples/mail-merge-simple-report-net-csharp-vb.php
        /// </remarks>
        public static void MailMergeSimpleEnvelope()
        {
            string templatePath = @"C:\tmp\DocLibTest\Original.docx";
            string resultPath = @"C:\tmp\DocLibTest\ResultMerge.docx";

            DocumentCore dc = DocumentCore.Load(templatePath);

            var dataSource = new[] { new { ETIQUETA = "VALOREQTIQUETA" }};

            Type t = dataSource.GetType();

            dc.MailMerge.Execute(dataSource);
            dc.Save(resultPath);

            // Open the result for demonstation purposes.
            System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(resultPath) { UseShellExecute = true });

            //string templatePath = @"..\..\envelope-template.docx";
            //string resultPath = "Simpson-family.docx";

            //DocumentCore dc = DocumentCore.Load(templatePath);

            //var dataSource = new[] { new { ETIQUETA = "VALOREQTIQUETA", FamilyName = "Simpson" },
            //                    new { Name = "Marge ", FamilyName = "Simpson" },
            //                    new { Name = "Bart", FamilyName = "Simpson" },
            //                    new { Name = "Lisa", FamilyName = "Simpson" },
            //                    new { Name = "Maggie", FamilyName = "Simpson" }};

            //dc.MailMerge.Execute(dataSource);
            //dc.Save(resultPath);

            //// Open the result for demonstation purposes.
            //System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(resultPath) { UseShellExecute = true });
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public static void MailMergeSimpleDynamic()
        {
            string templatePath = @"C:\tmp\DocLibTest\Original.docx";
            string resultPath = @"C:\tmp\DocLibTest\ResultMerge.docx";

            DocumentCore dc = DocumentCore.Load(templatePath);

            //var dataSource = new[] { new { ETIQUETA = "VALOREQTIQUETA" } };
            //dynamic etiquetas = new DynamicDictionary();
            //AddProper
            //etiquetas.ETIQUETA = "VALOREQTIQUETA_DYNAMIC";
            //TypeBuilder typeBuilder = ;
            //typeBuilder.SetParent(typeof(MyClass));
            //typeBuilder.DefineProperty("Prop1", ..., typeof(System.Int32), null);

            Assembly asmLogin = Assembly.Load("DocumentDataRT.dll");
            Type[] types = asmLogin.GetTypes();
            var instance = Activator.CreateInstance(types[0]);
          
            DocumentData etiquetas = new DocumentData();
            etiquetas.ETIQUETA = "VALOR_ETIQUETA_From_CLASS";

            string s = etiquetas.ETIQUETA;

            dc.MailMerge.Execute(etiquetas);
            dc.Save(resultPath);

            // Open the result for demonstation purposes.
            System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(resultPath) { UseShellExecute = true });

            //string templatePath = @"..\..\envelope-template.docx";
            //string resultPath = "Simpson-family.docx";

            //DocumentCore dc = DocumentCore.Load(templatePath);

            //var dataSource = new[] { new { ETIQUETA = "VALOREQTIQUETA", FamilyName = "Simpson" },
            //                    new { Name = "Marge ", FamilyName = "Simpson" },
            //                    new { Name = "Bart", FamilyName = "Simpson" },
            //                    new { Name = "Lisa", FamilyName = "Simpson" },
            //                    new { Name = "Maggie", FamilyName = "Simpson" }};

            //dc.MailMerge.Execute(dataSource);
            //dc.Save(resultPath);

            //// Open the result for demonstation purposes.
            //System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(resultPath) { UseShellExecute = true });
        }


        public Type BuildType()
        {
            AssemblyName newAssembly = new AssemblyName("DocumentDataRT");
            //AppDomain appDomain = System.Threading.Thread.GetDomain();

            AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain
                .DefineDynamicAssembly(
                newAssembly,
                AssemblyBuilderAccess.RunAndSave);

            ModuleBuilder moduleBuilder = assemblyBuilder
                .DefineDynamicModule(newAssembly.Name, newAssembly.Name + ".dll");

            TypeBuilder parentBuilder = moduleBuilder.DefineType(
                "Parent", TypeAttributes.Public);

            var ctor0 = parentBuilder.DefineConstructor(
                MethodAttributes.Public,
                CallingConventions.Standard,
                Type.EmptyTypes);

            ILGenerator ctor0IL = ctor0.GetILGenerator();
            // For a constructor, argument zero is a reference to the new 
            // instance. Push it on the stack before pushing the default 
            // value on the stack, then call constructor ctor1.
            ctor0IL.Emit(OpCodes.Ldarg_0);
            ctor0IL.Emit(OpCodes.Ldc_I4_S, 42);
            ctor0IL.Emit(OpCodes.Call, ctor0);
            ctor0IL.Emit(OpCodes.Ret);

            //TypeBuilder childBuilder = parentBuilder.DefineNestedType("Child");
            //var chType = childBuilder.CreateType();

            parentBuilder.DefineProperty(
                "MyProperty",
                System.Reflection.PropertyAttributes.HasDefault,
                typeof(string),
                //childBuilder.CreateType(),
                null);
            var type = parentBuilder.CreateType();

            assemblyBuilder.Save(newAssembly.Name + ".dll");


            return type;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DigitalSignature();
        }

        /// <summary>
        /// Load an existing document (*.docx, *.rtf, *.pdf, *.html, *.txt, *.pdf) and save it in a PDF document with the digital signature.
        /// </summary>
        /// <remarks>
        /// Details: https://sautinsoft.com/products/document/examples/digital-signature-net-csharp-vb.php
        /// </remarks>
        public void DigitalSignature()
        {
            // Path to a loadable document.
            string loadPath = @"C:\tmp\DocLibTest\Original.docx"; 
            string savePath = @"C:\tmp\DocLibTest\ResultSigned.pdf";

            DocumentCore dc = DocumentCore.Load(loadPath);

            // Create a new invisible Shape for the digital signature.
            // Place the Shape into top-left corner (0 mm, 0 mm) of page.
            Shape signatureShape = new Shape(dc, SautinSoft.Document.Drawing.Layout.Floating(new HorizontalPosition(2f, LengthUnit.Millimeter, HorizontalPositionAnchor.LeftMargin),
                                    new VerticalPosition(2f, LengthUnit.Millimeter, VerticalPositionAnchor.TopMargin), new SautinSoft.Document.Drawing.Size(100, 100)));
            ((FloatingLayout)signatureShape.Layout).WrappingStyle = WrappingStyle.InFrontOfText;
//            signatureShape.Outline.Fill.SetEmpty();
            signatureShape.Outline.Fill.SetPicture(@"C:\tmp\DocLibTest\FirmaGGS.png");

            // Find a first paragraph and insert our Shape inside it.
            Paragraph firstPar = dc.GetChildElements(true).OfType<Paragraph>().FirstOrDefault();
            firstPar.Inlines.Add(signatureShape);

            // Picture which symbolizes a handwritten signature.
            Picture signaturePict = new Picture(dc, @"C:\tmp\DocLibTest\FirmaGGS.png");

            // Signature picture will be positioned:
            // 14.5 cm from Top of the Shape.
            // 4.5 cm from Left of the Shape.
            //signaturePict.Layout = SautinSoft.Document.Drawing.Layout.Floating(
            //   new HorizontalPosition(4.5, LengthUnit.Centimeter, HorizontalPositionAnchor.Page),
            //   new VerticalPosition(14.5, LengthUnit.Centimeter, VerticalPositionAnchor.Page),
            //   new SautinSoft.Document.Drawing.Size(40, 20, LengthUnit.Millimeter));
            signaturePict.Layout = SautinSoft.Document.Drawing.Layout.Floating(
                new HorizontalPosition(0.2, LengthUnit.Centimeter, HorizontalPositionAnchor.Page),
                new VerticalPosition(25, LengthUnit.Centimeter, VerticalPositionAnchor.Page),
                new SautinSoft.Document.Drawing.Size(40, 20, LengthUnit.Millimeter));

            PdfSaveOptions options = new PdfSaveOptions();

            // Path to the certificate (*.pfx).
            options.DigitalSignature.CertificatePath = @"C:\tmp\DocLibTest\GGS_ValidoHasta2019.pfx";

            // The password for the certificate.
            // Each certificate is protected by a password.
            // The reason is to prevent unauthorized the using of the certificate.
            options.DigitalSignature.CertificatePassword = "B1ometrik@";

            // Additional information about the certificate.
            options.DigitalSignature.Location = "Test DocLib Location";
            options.DigitalSignature.Reason = "Reason Document.Net by SautinSoft";
            options.DigitalSignature.ContactInfo = "info@biometrika.cl";

            // Placeholder where signature should be visualized.
            options.DigitalSignature.SignatureLine = signatureShape;

            // Visual representation of digital signature.
            options.DigitalSignature.Signature = signaturePict;

            dc.Save(savePath, options);

            // Open the result for demonstation purposes.
            //System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(savePath) { UseShellExecute = true });
        }
    }
}
