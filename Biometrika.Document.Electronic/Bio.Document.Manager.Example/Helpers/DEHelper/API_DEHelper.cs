﻿
using Biometrika.Document.Electronic.Api;
using Biometrika.Document.Electronic.Api.Rest;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bio.Document.Manager.Example.Helpers.DEHelper
{
    public static class API_DEHelper
    {
        private static API API = new API(ConfigurationManager.AppSettings["API_Url"]);

        public static XmlParamOut Process(string url, XmlParamIn xmlParamIn)
        {
            int timeOut = 0;
            if (!Int32.TryParse(ConfigurationManager.AppSettings["API_TimeOut"], out timeOut))
                timeOut = 30000;

            if (!String.IsNullOrEmpty(url))
            {
                API.SetURL(url);
            }

            XmlParamOut xmlParamOut = API.Process(xmlParamIn, timeOut);

            //if (xmlParamOut != null && xmlParamOut.ExecutionResult == 0)
            //{
            //    SaveFileDialog saveDialog = new SaveFileDialog
            //    {
            //        FileName = "PDFSign_" + xmlParamOut.Trackid + ".pdf",
            //        Filter = "PDF (*.pdf)|*.pdf"
            //    };

            //    if (saveDialog.ShowDialog() == DialogResult.OK)
            //    {
            //        using (FileStream fileStream = new FileStream(saveDialog.FileName, FileMode.Create))
            //        {
            //            byte[] fileByte = Convert.FromBase64String(xmlParamOut.De);
            //            fileStream.Write(fileByte, 0, fileByte.Length);
            //        }

            //        if (xmlParamIn.Notarize)
            //        {
            //            string fileName = Path.GetFileNameWithoutExtension(saveDialog.FileName) + ".xml";
            //            string filePath = Path.GetDirectoryName(saveDialog.FileName) + "\\" + fileName;
            //            using (StreamWriter streamWriter = new StreamWriter(filePath))
            //            {
            //                streamWriter.Write(xmlParamOut.Notary);
            //            }
            //        }
            //    }
            //}

            return xmlParamOut;
        }
    }
}
