﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Document.Electronic.Api
{
    public static class Constant
    {

        //Tipos Templates
        /// <summary>
        /// Firma electronica Simple (Biometrica o electronica)
        /// </summary>
        public const int SIGNATURE_TEMPLATE_FES = 0;
        /// <summary>
        /// Firma electroncia avanzada
        /// </summary>
        public const int SIGNATURE_TEMPLATE_FEA = 1;
        /// <summary>
        /// Notarizado
        /// </summary>
        public static int SIGNATURE_TEMPLATE_NV = 2;

        //DOCUMENT STATUS
        public const int DEDOCUMENT_STATUS_CREATED = 0; 
        public const int DEDOCUMENT_STATUS_WF_INITIATED = 1; 
        public const int DEDOCUMENT_STATUS_COMPLETED = 2;
        public const int DEDOCUMENT_STATUS_EXPIRED = 3;

        //ACTIONS
        public const int DEDOCUMENT_ACTION_CREATE = 0; 
        public const int DEDOCUMENT_ACTION_GENERATE = 1; 
        public const int DEDOCUMENT_ACTION_VERIFY = 2;
        public const int DEDOCUMENT_ACTION_GET = 3;
        public const int DEDOCUMENT_ACTION_GETSTATUS = 4;
        public const int DEDOCUMENT_ACTION_SIGN = 5;


        //DESIGNATORE STATUS
        public const int DESIGNATORE_STATUS_CREATED = 0;
        public const int DESIGNATORE_STATUS_COMPLETED = 1;
        public const int DESIGNATORE_STATUS_EXPIRED = 2;

        public static string GetNameStatusDeSignatory(int status)
        {
            switch (status)
            {
                case Constant.DESIGNATORE_STATUS_CREATED:
                    return "Creado";
                case DESIGNATORE_STATUS_COMPLETED:
                    return "Completo";
                case DESIGNATORE_STATUS_EXPIRED:
                    return "EXPIRADO";
                default:
                    return "Creado";
            }
        }

        public static string GetNameStatusDeDocument(int status)
        {
            switch (status)
            {
                case Constant.DEDOCUMENT_STATUS_CREATED:
                    return "Creado";
                case DEDOCUMENT_STATUS_WF_INITIATED:
                    return "Iniciado";
                case DEDOCUMENT_STATUS_COMPLETED:
                    return "Completo";
                case DEDOCUMENT_STATUS_EXPIRED:
                    return "EXPIRADO";
                default:
                    return "Creado";
            }
        }
    }
}
