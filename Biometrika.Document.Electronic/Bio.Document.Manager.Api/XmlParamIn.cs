﻿using System;
using System.Collections.Generic;
using Bio.Core.Api;
using Biometrika.Document.Electronic.Api.Model;

namespace Biometrika.Document.Electronic.Api
{
    ///<summary>
    /// Clase destinada a la serializacion de parametros de entrada de Web Services
    ///</summary>
    public class XmlParamIn : System.ICloneable
    {

#region Private

        /// <summary>
        /// Posibles Valores =
        ///      1-Generate DE con Word Template (+ FES + FEA)
        ///      2-Sign PDF (Add BS + FES + FEA)
        ///      3-Get DE
        /// </summary>
        private int _actionid;
        /// <summary>
        /// Id de compañia para controlar acceso y tupos de docs
        /// </summary>
        private int _company;
        /// <summary>
        /// POr si la empresa quiere enviar id de persona que esta mandando los datos
        /// </summary>
        private string _employeeid;
        /// <summary>
        /// Usuario que tiene acceso al servicio
        /// </summary>
        private string _user;
        /// <summary>
        /// Password de acceso
        /// </summary>
        private string _password;
        /// <summary>
        /// Ide de Documento que se debe procesar
        /// </summary>
        private int _idDE; 
        /// <summary>
        /// Documento cuando se envia un PDF a firmar
        /// </summary>
        private string _de;
        /// <summary>
        /// Etiquetas y sus valores para la generación de documento desde plantilla Word
        /// </summary>
        private List<Tag> _tags;
        /// <summary>
        /// Lista de firmas biometricas a incrustar en PDF
        /// </summary>
        private List<BS> _BSSignatures;
        /// <summary>
        /// Opcional si se desea enviar la IP de la estacion de trabajo desde donde se genera el doc
        /// </summary>
        private string _ipenduser;
        /// <summary>
        /// Opcional si se desea enviar el USER que genera el doc
        /// </summary>
        private string _enduser;
        
        private bool _notarize;
        private string[] _mailstonotify;
        private string _txid;

        /// <summary>
        /// Tipo de proceso que se debe realizar
        /// 0 - Generar via Plantilla Word | 1 - Viene PDF se agrega al final las firmas biometrika
        /// </summary>
        //private int _idTypeDE;
        /// <summary>
        /// Tipo de Id (RUT, DNI, etc) de la persona que está firmando
        /// </summary>
        private string _typeid;
        /// <summary>
        /// Value Id de la persona que está firmando
        /// </summary>
        private string _valueid;

        //Added v5.6
        //Agrego parámetros para verificar identidad con biometria contra BioPortal, y si es positiva, firma como hasta ahora 

        /// <summary>
        /// Valores Posibles:
        ///  -1 - Sin verificación (Default) 
        ///   0 - Default 
        ///   1 - Al menos unos positivo: Si vienen vrias muestras, de diversos tipos, al menos una de cada TypeId+ValueId debe ser positivo  
        ///   2 - Todos positivos
        /// </summary>
        private int _verificationType = -1;

        /// <summary>
        /// Listados de credenciales a verificar
        /// </summary>
        private List<CredentialSample> _listCredentials;

        #endregion Private

        #region Public



        public int Actionid
        {
            get
            {
                return _actionid;
            }

            set
            {
                _actionid = value;
            }
        }

        public int Company
        {
            get
            {
                return _company;
            }

            set
            {
                _company = value;
            }
        }

        public string User
        {
            get
            {
                return _user;
            }

            set
            {
                _user = value;
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }

            set
            {
                _password = value;
            }
        }

        public int IdDE
        {
            get
            {
                return _idDE;
            }

            set
            {
                _idDE = value;
            }
        }

        //public int IdTypeDE
        //{
        //    get
        //    {
        //        return _idTypeDE;
        //    }

        //    set
        //    {
        //        _idTypeDE = value;
        //    }
        //}

        public string DE
        {
            get
            {
                return _de;
            }

            set
            {
                _de = value;
            }
        }

        public List<Tag> Tags
        {
            get
            {
                return _tags;
            }

            set
            {
                _tags = value;
            }
        }

        public List<BS> BSSignatures
        {
            get
            {
                return _BSSignatures;
            }

            set
            {
                _BSSignatures = value;
            }
        }

        public string Ipenduser
        {
            get
            {
                return _ipenduser;
            }

            set
            {
                _ipenduser = value;
            }
        }

        public string Enduser
        {
            get
            {
                return _enduser;
            }

            set
            {
                _enduser = value;
            }
        }

        public bool Notarize
        {
            get
            {
                return _notarize;
            }

            set
            {
                _notarize = value;
            }
        }

        public string[] MailsToNotify
        {
            get
            {
                return _mailstonotify;
            }

            set
            {
                _mailstonotify = value;
            }
        }

        public string TxId
        {
            get
            {
                return _txid;
            }

            set
            {
                _txid = value;
            }
        }

        public string EmployeeId
        {
            get
            {
                return _employeeid;
            }

            set
            {
                _employeeid = value;
            }
        }

        public string TypeId
        {
            get
            {
                return _typeid;
            }

            set
            {
                _typeid = value;
            }
        }

        public string ValueId
        {
            get
            {
                return _valueid;
            }

            set
            {
                _valueid = value;
            }
        }

        public int VerificationType
        {
            get => _verificationType;
            set => _verificationType = value;
        }

        public List<CredentialSample> ListCredentials
        {
            get => _listCredentials;
            set => _listCredentials = value;
        }

        #endregion Public

        #region Implementation of ICloneable

        /// <summary>
        /// Crea un nuevo objeto copiado de la instancia actual.
        /// </summary>
        /// <returns>
        /// Nuevo objeto que es una copia de esta instancia.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public object Clone()
        {
            return MemberwiseClone(); 
        }

 
        #endregion
    }

    public class BS : System.ICloneable
    {
        public BS() { }

        public string BSId;
        public string BiometricProvider; //Nombre proveedor biometrico que hizo esta BS
        public string BiometricSignature; //En B64 huella o foto o algo que sirva de firma biometrica
        public int BiometricSignatureType;  //Int que indica que tipo d eimagen tenemos:
                                            //     21-WSQ | 22-RAW | 41-JPG
        public int BiometricSignatureW; //Width de BiometricSignature, para cuando es RAW
        public int BiometricSignatureH; //Heigth de BiometricSignature, para cuando es RAW
        public string QRVerify; //Si existe un QR donde verificar esta firma biometrica en linea
        public string IdentityId; //Identificador de la persona si existe
        public string IdnetityName; //Nombre d ela persona si existe
        public string TimestampSignature; //Fecha y hora de la firma biometrica
        //Adicional por si existe, datos de NV
        public string NVreceiptId; //Id de recibo si existe sino null
        public string NVReceipttimestamp; //Fecha y hora de cuando se genero el recibo
        public string TSA;  // Ejemplo: C=ES,O=Safe Creative, CN = Time Stamp Authority Server
        public string TSAPolicy; //Ejemplo 1.3.6.1.4.1.45794.1.1
        public string TSADateTimeSign; //Fehcahora del timestamp en TSA => ejemplo 29-03-2018 19:22:34
        public string TSASerialNumber; //Ejemplo: 25793306
        public string TSAURLVerify; //Ejemplo: https://tsa.safecreative.org/?search=


        /// <summary>
        /// Crea un nuevo objeto copiado de la instancia actual.
        /// </summary>
        /// <returns>
        /// Nuevo objeto que es una copia de esta instancia.
        /// </returns>
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}