﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Document.Electronic.Api.Interfaces
{

    /// <summary>
    /// Esta interface tiene por objetivo realizar tareas personalizadas con el DE
    /// generado, para un determinado DeTemplate. Esto permite realizar cualquier 
    /// acción posterior especial para un DE generado. Recibe como parámetro el 
    /// objeto DeDocument completo, y puede tener la autonomía deseada, con su propio
    /// archivo de config.
    /// </summary>
    public interface IAssemblyPostGenerate
    {
        string PathBase { get; set; }

        /// <summary>
        /// Para leer el archivo de config si aplica. El formato de los datos es:
        ///         {"Key1":"Value1","Key2":"Value2"}
        /// El nombre del archivo SIEMPRE es el <nombre del assembly>.cfg
        /// </summary>
        Dictionary<string, string> Config { get; set; }

        /// <summary>
        /// Inicializa el objeto si hace falta, sino implenetacion vacia
        /// Se puede usar para leer el archivo de config si se necesita
        /// </summary>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        int Initialize(string pathbase, out string msgerr);

        /// <summary>
        /// Recibe como parámetro un objeto. 
        /// Devuelve un int indicando codigo de error y msg de error si existe error.
        /// Además devuelve opcionalmente un objeto, o bien null si no hace falta.
        /// </summary>
        /// <param name="paramin"></param>
        /// <param name="msgerr"></param>
        /// <param name="paramout"></param>
        /// <returns></returns>
        int DoAction(object paramin, out string msgerr, out object paramout);

    }
}
