﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Document.Electronic.Api.Model
{
    public class ApiModel
    {
    }

    public class DeApiResult
    {
        public int code;
        public string message;
        public object data;

        public DeApiResult() { }

        public DeApiResult(int _code, string _message, object _data)
        {
            code = _code;
            message = _message;
            data = _data;
        }

    }

    public class CreateDeModel
    {
        public int templateid;
        public string username;
        public int daysforexpiration;  //Si es <=0 => No expira
        public List<Signatory> signatorylist;
        public string callbackurl;
        public string redirecturl;
        public List<Tag> tagsgivenlist; //Se envian los valores desde app de 3ros que no se piden pero se usan en la generacion
                                        //Se guardan desde el primer momento en de_document_tag
    }

    public class Signatory
    {
        public string typeid;
        public string valueid;
        public int order;
        public string name;
        public string lastname;
        public string mail;
        public string phone;
        public string taxidcompany;
        //public string workflowname; 
    }

    public class DeStatus
    {
        public string trackidde;
        public int status;
        public string message;
        public int resultcode;
        public DateTime createdate;
        //public DateTime lastmodifydate;
    }
}
