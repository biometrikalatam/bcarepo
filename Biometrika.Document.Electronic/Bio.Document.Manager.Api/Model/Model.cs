﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Document.Electronic.Api.Model
{
    public class Model
    {
    }

    public class Company
    {
        public Company() { }
        public int IdCompany;
        public string User;
        public string Password;
        //    0:Sin Firmar 
        //    1: Firma Electronica Simple con PFX + PSW Abajo Default Compañia 
        //    2: Firma Electronica Simple con PFX + PSW Abajo 
        //    3: Servicio Firma e-Sign Abajo
        public int SignatureType;
        public string CertificatePFX;
        public string SignaturePSW;
        public string CertificateCER;
        public string ImageSignatureToShow; //Path imagen que se desea mostrar en la visualizacion de la firma electronica
        public int FESShowType = 0; //0-Hoja aparte | 1-Ultima Hoja
        public int FESx = 150;
        public int FESy = 550;
        public int FESw = 550;
        public int FESh = 680;
        public string FESMsgToShow = "Firmado para evitar modificacion!";
        public string UrlFEA; // http://esigner6-test.servisign.cl/SignServerEsign/WSIntercambiaDocSoap
        public string AppFEA;
        public string UserFEA;
        public List<DocumentType> DocumentTypes;
    }

    public class DocumentType
    {
        public DocumentType() { }
        public int idDT;
        public string Name;
        public string Description;
        public string Version;
        public string Template;
        public int SignatureType;
        public string CertificatePFX;
        public string SignaturePSW;
        public string CertificateCER;
        public string ImageSignatureToShow; //Path imagen que se desea mostrar en la visualizacion de la firma electronica
        public int FESShowType = 0; //0-Hoja aparte | 1-Ultima Hoja
        public int FESx = 150;
        public int FESy = 550;
        public int FESw = 550;
        public int FESh = 680;
        public string FESMsgToShow = "Firmado para evitar modificacion!";
        public string UrlFEA; // http://esigner6-test.servisign.cl/SignServerEsign/WSIntercambiaDocSoap
        public string AppFEA;
        public string UserFEA;
        public bool Notarize;
        public List<Tag> Tags;
    }

    public class Tag
    {
        public Tag() { }
        public string IdTag;
        /*
            case 0: //String
            case 1: //int
            case 2: //double
            case 3: //Image asumo JPG
            case 4: //WSQ
        */
        public string Type;
        public string Parameters;
        public string Value;
    }

    /// <summary>
    /// Sirve para hacer una lista de TAGS con etiqueta y valor, para que se haga el cehqueo de integridad
    /// y de paso se conviertan los valores al tipo necesario para despues hacer el reemplazo en el templae
    /// </summary>
    public class TagProcessed
    {
        public TagProcessed() { }
        public string IdTag;
        public int Type;
        public object value;
        public string Parameters;
    }
}
