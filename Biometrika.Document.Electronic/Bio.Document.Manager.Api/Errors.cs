﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biometrika.Document.Electronic.Api
{
    public class Errors
    {
        #region Constantes

        static public int RET_OK = 0;

        static public int RET_ERR_UNKKNOW = -1;
        static public int RET_ERR_PARAM_INCORRECTOS = -2;
        static public int RET_ERR_COMPANY_NOT_EXIST = -3;
        static public int RET_ERR_COMPANY_DISABLED = -4;
        static public int RET_ERR_COMPANY_CONFIG_DE_NOT_EXIST = -5;
        static public int RET_ERR_DE_CONFIG_NOT_EXIST = -6;
        static public int RET_ERR_TAGS_INCONSISTENT = -7;
        static public int RET_ERR_TYPETAG_INCONSISTENT = -8;
        static public int RET_ERR_DE_NOT_SAVE_OR_NOTARIZE = -9;
        static public int RET_ERR_LOGIN_INCORRECT = -10;
        static public int RET_ERR_USER_NOT_CONFIGURED = -11;
        static public int RET_ERR_PASSWORD_NOT_MATCH = -12;
        static public int RET_ERR_DE_NOT_EXIST = -13;
        static public int RET_ERR_VENDOR_NOT_EXIST = -14;

        static public int RET_ERR_ADDING_PDF_TEMPLATE_SIG = -15;
        static public int RET_ERR_SIGNING_PDF_FEA_SIG = -16;

        static public int RET_ERR_DONT_CHECK_ENROLLED = -17;
        static public int RET_ERR_WORKFLOW_DONT_EXIST = -18;
        static public int RET_ERR_CALLING_PROVIDER_SERVICE = -19;
        
        static public int RET_ERR_WSRD_ERROR = -20;
        static public int RET_ERR_CALLING_ASSEMBLY_POST_GENERATE = -21;

        static public int RET_ERR_VERIFICATION_CREDENTIAL = -30;

        static public int RET_API_INTERNAL_SERVER_ERROR = -500;
        

        #region 600 - API Error

        static public int IERR_SERIALIZE_XMLPARAMIN = -601;
        static public string SERR_SERIALIZE_XMLPARAMIN = "Error al serializar XmlParamIn";

        static public int IERR_DESERIALIZE_XMLPARAMOUT = -602;
        static public string SERR_DESERIALIZE_XMLPARAMOUT = "Error al deserializar XmlParamOut";

        #endregion

        #endregion Constantes

        static public string GetDescription(int error)
        {
            switch (error)
            {
                case 0:
                    return "Proceso correcto";
                case -601:
                    return SERR_SERIALIZE_XMLPARAMIN;
                case -602:
                    return SERR_DESERIALIZE_XMLPARAMOUT;
                default:
                    return "Error No Documentado";
            }

        }
    }
}
