﻿namespace Biometrika.Document.Electronic.Api
{
    public class CredentialSample
    {
        public CredentialSample() {
        }

        public virtual string TypeId { get; set; }
        public virtual string ValueId { get; set; }
        public virtual string Data { get; set; }
        public virtual int Bodypart { get; set; }
        public virtual int Minutiaetype { get; set; }
        public virtual string Additionaldata { get; set; }
        public int Device { get; set; }
    }
}