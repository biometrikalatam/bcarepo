﻿using System;
using Bio.Core.Api;
using log4net;

namespace Biometrika.Document.Electronic.Api
{
    ///<summary>
    ///</summary>
    public class XmlParamOut : System.ICloneable
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(XmlParamOut));

#region Private

        private string _message;
        private string _trackid;
        private DateTime _timestampend;
        private int _actionid;
        private string _de;
        private string _notary;


        #endregion Private

        #region Public


        /// <summary>
        /// Mensaje de error si el retorno es =! 0
        /// </summary>
        public string Message
        {
            get
            {
                return _message;
            }

            set
            {
                _message = value;
            }
        }

        /// <summary>
        /// Id de trabsaccion (de documento) generado o firmado
        /// </summary>
        public string Trackid
        {
            get
            {
                return _trackid;
            }

            set
            {
                _trackid = value;
            }
        }

        /// <summary>
        /// Timestamp de la accion reaizada
        /// </summary>
        public DateTime Timestampend
        {
            get
            {
                return _timestampend;
            }

            set
            {
                _timestampend = value;
            }
        }

        /// <summary>
        /// Posibles Valores =
        ///      1-Generate DE con Word Template (+ FES + FEA)
        ///      2-Sign PDF (Add BS + FES + FEA)
        ///      3-Get DE
        /// </summary>
        public int Actionid
        {
            get
            {
                return _actionid;
            }

            set
            {
                _actionid = value;
            }
        }

        /// <summary>
        /// Documento PDF en Base 64 (generado o firmado)
        /// </summary>
        public string De
        {
            get
            {
                return _de;
            }

            set
            {
                _de = value;
            }
        }

        /// <summary>
        /// Recibo Digital del Notario Virtual si se notarizo el documento
        /// </summary>
        public string Notary
        {
            get
            {
                return _notary;
            }

            set
            {
                _notary = value;
            }
        }

        public int ExecutionResult { get; set; }

        #endregion Public


        #region Implementation of ICloneable

        /// <summary>
        /// Crea un nuevo objeto copiado de la instancia actual.
        /// </summary>
        /// <returns>
        /// Nuevo objeto que es una copia de esta instancia.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public object Clone()
        {
            return MemberwiseClone(); 
        }

        #endregion
    }

}