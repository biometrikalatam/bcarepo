﻿using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Document.Electronic.Api.Rest
{
    public class API
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        protected IRestClient Client;

        public API(string host)
        {
            Client = new RestClient(host);
        }

        public XmlParamOut Process(XmlParamIn xmlParamIn, int timeOut = 30000)
        {
            var request = new RestRequest("api/WSDE/Process", Method.POST);
            request.Timeout = timeOut;
            request.AddJsonBody(xmlParamIn);

            try
            {
                IRestResponse response = Client.Execute(request);
                var content = response.Content;

                return JsonConvert.DeserializeObject<XmlParamOut>(content);
            }
            catch (Exception e)
            {
                Log.Error("Error API.Process", e);
                return new XmlParamOut
                {
                    ExecutionResult = Errors.IERR_DESERIALIZE_XMLPARAMOUT,
                    Message = Errors.GetDescription(Errors.IERR_DESERIALIZE_XMLPARAMOUT)
                };
            }
        }

        public void SetURL(string url)
        {
            Client = new RestClient(url);
        }
    }
}
