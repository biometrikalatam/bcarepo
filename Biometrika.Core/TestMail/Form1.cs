﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestMail.Properties;

namespace TestMail
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public static bool SendMail(string[] mailto, string subject, string body,
                                    byte[] attach, string mimetypeattach,
                                    string nombreattach)
        {
            bool ret = false;
            MemoryStream ms = null;
            try
            {
                //log.Debug("Utils.SendMail IN...");
                //save the data to a memory stream


                //Envio de Correo
                //log.Debug("Utils.SendMail UserSMTP = " + Settings.Default.UserSMTP + " / " + Settings.Default.ClaveSMTP);
                MailAddress SendFrom = new MailAddress(Settings.Default.UserSMTP, Settings.Default.SMTPDisplayName);
                //log.Debug("Utils.SendMail SendTo = " + mailto[0]);
                MailAddress SendTo = new MailAddress(mailto[0]);
                MailMessage MyMessage = null;
                try
                {
                    SmtpClient emailClient = new SmtpClient(Settings.Default.ServerSMTP);
                    emailClient.Port = 587;
                    emailClient.EnableSsl = true;
                    NetworkCredential _Credential =
                                new NetworkCredential(Settings.Default.UserSMTP, Settings.Default.ClaveSMTP, null);
                    MyMessage = new MailMessage(SendFrom, SendTo);
                    if (mailto.Length > 1)
                    {
                        for (int i = 0; i < mailto.Length; i++)
                        {
                            MyMessage.To.Add(new MailAddress(mailto[i]));
                            //log.Debug("Utils.SendMail - Correo => Enviar a:" + mailto[i] + " desde : " + SendFrom);
                        }
                        //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.cl"));
                        //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.pe"));
                        //MyMessage.To.Add(new MailAddress("gsuhit@yahoo.com"));
                    }

                    if (attach != null && attach.Length > 0)
                    {
                        ms = new MemoryStream(attach);
                        ms.Position = 0;
                        string mt = MediaTypeNames.Application.Pdf;
                        if ((mimetypeattach == "docx") || (mimetypeattach == "doc"))
                            mt = MediaTypeNames.Application.Rtf;
                        else if ((mimetypeattach == "xls") || (mimetypeattach == "xlsx"))
                            mt = MediaTypeNames.Application.Octet;
                        else if ((mimetypeattach == "pdf"))
                            mt = MediaTypeNames.Application.Pdf;
                        else
                            mt = MediaTypeNames.Application.Octet;
                        MyMessage.Attachments.Add(new Attachment(ms, nombreattach, mt));
                    }

                    //log.Debug("Utils.SendMail Set Body HTML...");
                    MyMessage.IsBodyHtml = true;
                    //log.Debug("Utils.SendMail Subject = " + subject);
                    MyMessage.Subject = subject;
                    //log.Debug("Utils.SendMail - Body = " + body);
                    MyMessage.Body = body;
                    emailClient.Credentials = _Credential;
                    try
                    {
                        //log.Debug("Utils.SendMail Enviando...");
                        emailClient.Send(MyMessage);
                        //log.Debug("Utils.SendMail Enviado sin error!");
                        ret = true;
                    }
                    catch (Exception ex)
                    {
                        //log.Error("emailClient.Send(MyMessage)- Error", ex);
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    //log.Error("emailClient.Send [" + ex.Message + "]");
                    ret = false;
                }

                if (ms != null)
                {
                    ms.Close();
                    ms.Dispose();
                }
            }
            catch (Exception ex)
            {
                //log.Error("Utils.SendMail Error SendMail", ex);
                ret = false;
            }
            //log.Debug("Utils.SendMail OUT!");
            return ret;

        }

        public static bool SendMailHtml(string[] mailto, string subject, string body,
                                    byte[] attach, string mimetypeattach,
                                    string nombreattach)
        {
            bool ret = false;
            MemoryStream ms = null;
            try
            {
                //log.Debug("Utils.SendMail IN...");
                //save the data to a memory stream


                //Envio de Correo
                //log.Debug("Utils.SendMail UserSMTP = " + Settings.Default.UserSMTP + " / " + Settings.Default.ClaveSMTP);
                MailAddress SendFrom = new MailAddress(Settings.Default.UserSMTP, Settings.Default.SMTPDisplayName);
                //log.Debug("Utils.SendMail SendTo = " + mailto[0]);
                MailAddress SendTo = new MailAddress(mailto[0]);
                MailMessage MyMessage = null;
                try
                {
                    SmtpClient emailClient = new SmtpClient(Settings.Default.ServerSMTP);
                    emailClient.Port = 587;
                    emailClient.EnableSsl = true;
                    NetworkCredential _Credential =
                                new NetworkCredential(Settings.Default.UserSMTP, Settings.Default.ClaveSMTP, null);
                    MyMessage = new MailMessage(SendFrom, SendTo);
                    if (mailto.Length > 1)
                    {
                        for (int i = 0; i < mailto.Length; i++)
                        {
                            MyMessage.To.Add(new MailAddress(mailto[i]));
                            //log.Debug("Utils.SendMail - Correo => Enviar a:" + mailto[i] + " desde : " + SendFrom);
                        }
                        //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.cl"));
                        //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.pe"));
                        //MyMessage.To.Add(new MailAddress("gsuhit@yahoo.com"));
                    }

                    if (attach != null && attach.Length > 0)
                    {
                        ms = new MemoryStream(attach);
                        ms.Position = 0;
                        string mt = MediaTypeNames.Application.Pdf;
                        if ((mimetypeattach == "docx") || (mimetypeattach == "doc"))
                            mt = MediaTypeNames.Application.Rtf;
                        else if ((mimetypeattach == "xls") || (mimetypeattach == "xlsx"))
                            mt = MediaTypeNames.Application.Octet;
                        else if ((mimetypeattach == "pdf"))
                            mt = MediaTypeNames.Application.Pdf;
                        else
                            mt = MediaTypeNames.Application.Octet;
                        MyMessage.Attachments.Add(new Attachment(ms, nombreattach, mt));
                    }

                    //log.Debug("Utils.SendMail Set Body HTML...");
                    //MyMessage.IsBodyHtml = true;

                    //Create two views, one text, one HTML.
                    string s = ".";
                    System.Net.Mail.AlternateView plainTextView =
                        System.Net.Mail.AlternateView.CreateAlternateViewFromString(s, Encoding.UTF8, "text/html");
                    System.Net.Mail.AlternateView htmlView =
                        System.Net.Mail.AlternateView.CreateAlternateViewFromString(body, Encoding.UTF8, "text/html");

                    //Add image to HTML version

                    System.Net.Mail.LinkedResource imageResource =
                        new System.Net.Mail.LinkedResource(Properties.Settings.Default.LogoMail, MediaTypeNames.Image.Jpeg);
                    imageResource.ContentId = "HDIImage";
                    htmlView.LinkedResources.Add(imageResource);

                    //Add two views to message.

                    //MyMessage.AlternateViews.Add(plainTextView);
                    MyMessage.AlternateViews.Add(htmlView);

                    //log.Debug("Utils.SendMail Subject = " + subject);
                    MyMessage.Subject = subject;
                    //log.Debug("Utils.SendMail - Body = " + body);
                    //MyMessage.Body = body;
                    emailClient.Credentials = _Credential;
                    try
                    {
                        //log.Debug("Utils.SendMail Enviando...");
                        emailClient.Send(MyMessage);
                        //log.Debug("Utils.SendMail Enviado sin error!");
                        ret = true;
                    }
                    catch (Exception ex)
                    {
                        //log.Error("emailClient.Send(MyMessage)- Error", ex);
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    //log.Error("emailClient.Send [" + ex.Message + "]");
                    ret = false;
                }

                if (ms != null)
                {
                    ms.Close();
                    ms.Dispose();
                }
            }
            catch (Exception ex)
            {
                //log.Error("Utils.SendMail Error SendMail", ex);
                ret = false;
            }
            //log.Debug("Utils.SendMail OUT!");
            return ret;

        }

        public static bool NVNotify(string mailto, string subject, string[] body,
                                    byte[] attach, string mimetypeattach,
                                    string nombreattach)
        {
            string[] amailto = new string[1];
            amailto[0] = mailto;
            return NVNotify(amailto, subject, body, attach, mimetypeattach, nombreattach);
        }

        public static bool NVNotify(string[] mailto, string subject, string[] body,
                                    byte[] attach, string mimetypeattach,
                                    string nombreattach)
        {
            bool bret = false;
            try
            {
                string strbody = "<table border=\"0\" width=\"60%\" align=\"center\">" +
                               "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                               "<tr><td align=\"center\">" +
                                   "</td></tr>";
                for (int i = 0; i < body.Length; i++)
                {
                    strbody += "<tr><td><p style=\"font-family:'Arial'\">" + body[i] + "</p></td></tr>";
                }
                strbody += "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
                                "<a href=\"http://www.notariovirtual.cl\" target=\"_blank\">Notario Virtual<a>.</p></td></tr>" +
                                "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                             "</table>";
                string body2 = @"<html><head></head><body><table border=""0"" width=""60%"" align=""center"">" +
                               @"<tr><td align=""center""><a href=""http://www.notariovirtual.cl"" target=""_blank"">" +
                               @"<img src=""cid:HDIImage"" /></a></td></tr></table>" + strbody + "</body></html>";

                //string body2 = @"<html><body><table border=""0"" width=""60%"" align=""center"">" +
                //       @"<tr><td align=""center""><a href=""http://www.notariovirtual.cl"" target=""_blank"">" +
                //       @"</a></td></tr>" +  strbody + " </body></html>";

                bret = SendMailHtml(mailto, "Notario Virtual Notify - " + subject, body2, attach, mimetypeattach, nombreattach);
            }
            catch (Exception ex)
            {
                bret = false;
                //log.Error("Utils.NVNotify Error: " + ex.Message);
            }
            return bret;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string _typeid = "RUT";
            string _valueid = "21284415-2";
            string _trackid = "g5jhw4g5wjh5jhweg5jhg5jh2g";
            string[] strbody = new string[3];
            strbody[0] = "Se inicio un proceso de Certificacion de Identidad para <b>" + _typeid + " " + _valueid + "</b>.";
            strbody[1] = "Cuando la persona termine el proceso de Certificación de Identidad, " +
                "Notario Virtual le enviará una copia de la Certificación realizada a su mail.";
            strbody[2] = "El codigo unico de Certificación de Identidad para consultas es: <b>" + _trackid + "</b>.";

            string[] aListMails = new string[4];
            aListMails[0] = "gsuhit@biometrika.cl";
            aListMails[1] = "gsuhit@yahoo.com";
            aListMails[2] = "gsuhit@hotmail.com";
            aListMails[3] = "gsuhit@outlook.com";
            foreach (string item in aListMails)
            {
                NVNotify(item, "Inicio de Certificacion de Identidad", strbody, null, null, null);
            }
        }
    }
}
