﻿using Biometrika.ManualAccess.src.Helpers;
using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.ManualAccess
{
    public partial class frmMain : Form
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(frmMain));

        //Hashtable _ARDUINO_HELPERS;
        ArduinoHelper _ARDUINO_HELPER;


        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //CheckIllegal
            CheckForIllegalCrossThreadCalls = false;

            //Setea pantalla segun config
            SetDashboard();

        }

        private void SetDashboard()
        {
            LOG.Debug("SetDashboard IN...");
            try
            {
                labVersion.Text = (string)Program._CONFIG.DynamicDataItems["PointName"] + " - " +
                                   Program._VERSION;

                //Habilito y seteo lo configurado. 
                // TODO - Ahora lo hago fijo considerando los tiepos, luego se debería hacer variable, ampliando 
                //          el form con mas puntos incluso

                if (Program.POINT_LIST == null || Program.POINT_LIST.Count == 0)
                {
                    MessageBox.Show(this, "No existen puntos remotos configurados! Revise la configuración y reintente...",
                                    "Atención...",MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    LOG.Fatal("SetDashboard Out! Lista de puntos no configurados! Sale.");
                    Application.Exit();
                }

                //_ARDUINO_HELPERS = new Hashtable();
                _ARDUINO_HELPER = new ArduinoHelper(Program.POINT_LIST[0].timeoutIn.ToString(), "2", Program.POINT_LIST[0].url);
                LOG.Debug("SetDashboard - Configuro Torniquete...");
                grpTorniquete1.Text = "Punto: " + Program.POINT_LIST[0].name;
                labURLTorniquete1.Text = "URL => " + Program.POINT_LIST[0].url;
                labDetailsReleTorniquete1.Text = "Rele IN: " + Program.POINT_LIST[0].timeoutIn.ToString() +
                                                  " - Rele OUT: " + Program.POINT_LIST[0].timeoutOut.ToString();
                grpTorniquete1.Enabled = true;

                if (Program.POINT_LIST.Count > 1)
                {
                    LOG.Debug("SetDashboard - Configuro Puerta...");
                    grpPuerta.Text = "Punto: " + Program.POINT_LIST[1].name;
                    labURLPuerta.Text = "URL => " + Program.POINT_LIST[1].url;
                    labDetailsRelePuerta.Text = "Rele OPEN: " + Program.POINT_LIST[1].timeoutIn.ToString(); // +
                                                   //" - Rele OUT: " + Program.POINT_LIST[0].timeoutOut.ToString();
                    grpPuerta.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("SetDashboard Error - " + ex.Message);
            }
            LOG.Debug("SetDashboard OUT!");
        }

        //ENTRADA TORNIQUETE
        private void btnInTorniquete1_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("SetDashboard - IN en " + Program.POINT_LIST[0].name + "..." );
                _ARDUINO_HELPER.SetURL(Program.POINT_LIST[0].url);
                int ret = _ARDUINO_HELPER.OpenWithDelay(Program.POINT_LIST[0].releIn, Program.POINT_LIST[0].timeoutIn.ToString());
                if (ret == 0) {
                    labFeedbackTorinquete1.Text = "Abrió ENTRADA " + Program.POINT_LIST[0].name + " por " +
                                                  (Program.POINT_LIST[0].timeoutIn / 1000).ToString() + " segundos...";
                    labFeedbackTorinquete1.ForeColor = Color.Green;
                    labFeedbackTorinquete1.Visible = true;
                    this.Refresh();
                    System.Threading.Thread.Sleep(Program.POINT_LIST[0].timeoutIn);
                    labFeedbackTorinquete1.Visible = false;
                    this.Refresh();
                } else
                {
                    LOG.Error("btnInTorniquete1_Click Error => Código = " + ret.ToString());
                    labFeedbackTorinquete1.Text = "Error abriendo ENTRADA => Código = " + ret.ToString();
                    labFeedbackTorinquete1.ForeColor = Color.Red;
                    labFeedbackTorinquete1.Visible = true;
                    this.Refresh();
                    System.Threading.Thread.Sleep(2000);
                    labFeedbackTorinquete1.Visible = false;
                    this.Refresh();
                }
            }
            catch (Exception ex)
            {
                labFeedbackTorinquete1.Text = "Error abriendo ENTRADA => " + ex.Message;
                labFeedbackTorinquete1.ForeColor = Color.Red;
                labFeedbackTorinquete1.Visible = true;
                this.Refresh();
                System.Threading.Thread.Sleep(2000);
                labFeedbackTorinquete1.Visible = false;
                this.Refresh();
                LOG.Error("btnInTorniquete1_Click Error: " + ex.Message);
            }
        }

        //SALIDA TORNIQUETE
        private void btnOutTorniquete1_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("SetDashboard - OUT en " + Program.POINT_LIST[0].name + "...");
                _ARDUINO_HELPER.SetURL(Program.POINT_LIST[0].url);
                int ret = _ARDUINO_HELPER.OpenWithDelay(Program.POINT_LIST[0].releOut, Program.POINT_LIST[0].timeoutOut.ToString());
                if (ret == 0)
                {
                    labFeedbackTorinquete1.Text = "Abrió SALIDA " + Program.POINT_LIST[0].name + " por " +
                                                  (Program.POINT_LIST[0].timeoutOut / 1000).ToString() + " segundos...";
                    labFeedbackTorinquete1.ForeColor = Color.Green;
                    labFeedbackTorinquete1.Visible = true;
                    this.Refresh();
                    System.Threading.Thread.Sleep(Program.POINT_LIST[0].timeoutIn);
                    labFeedbackTorinquete1.Visible = false;
                    this.Refresh();
                }
                else
                {
                    LOG.Error("btnOutTorniquete1_Click Error => Código = " + ret.ToString());
                    labFeedbackTorinquete1.Text = "Error abriendo SALIDA => Código = " + ret.ToString();
                    labFeedbackTorinquete1.ForeColor = Color.Red;
                    labFeedbackTorinquete1.Visible = true;
                    this.Refresh();
                    System.Threading.Thread.Sleep(2000);
                    labFeedbackTorinquete1.Visible = false;
                    this.Refresh();
                }
            }
            catch (Exception ex)
            {
                labFeedbackTorinquete1.Text = "Error abriendo SALIDA => " + ex.Message;
                labFeedbackTorinquete1.ForeColor = Color.Red;
                labFeedbackTorinquete1.Visible = true;
                this.Refresh();
                System.Threading.Thread.Sleep(2000);
                labFeedbackTorinquete1.Visible = false;
                this.Refresh();
                LOG.Error("btnOutTorniquete1_Click Error: " + ex.Message);
            }
        }

        //APERTURA PUERTA
        private void btnPuerta_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("SetDashboard - OPEN en " + Program.POINT_LIST[1].name + "...");
                _ARDUINO_HELPER.SetURL(Program.POINT_LIST[1].url);
                int ret = _ARDUINO_HELPER.OpenWithDelay(Program.POINT_LIST[1].releIn, Program.POINT_LIST[1].timeoutIn.ToString());
                if (ret == 0)
                {
                    labFeedbackPuerta.Text = "Abrió PUERTA " + Program.POINT_LIST[1].name + " por " +
                                                  (Program.POINT_LIST[1].timeoutIn / 1000).ToString() + " segundos...";
                    labFeedbackPuerta.ForeColor = Color.Green;
                    labFeedbackPuerta.Visible = true;
                    this.Refresh();
                    System.Threading.Thread.Sleep(Program.POINT_LIST[1].timeoutIn);
                    labFeedbackPuerta.Visible = false;
                    this.Refresh();
                }
                else
                {
                    LOG.Error("btnPuerta_Click Error => Código = " + ret.ToString());
                    labFeedbackPuerta.Text = "Error abriendo PUERTA => Código = " + ret.ToString();
                    labFeedbackPuerta.ForeColor = Color.Red;
                    labFeedbackPuerta.Visible = true;
                    this.Refresh();
                    System.Threading.Thread.Sleep(2000);
                    labFeedbackPuerta.Visible = false;
                    this.Refresh();
                }
            }
            catch (Exception ex)
            {
                labFeedbackPuerta.Text = "Error abriendo PUERTA => " + ex.Message;
                labFeedbackPuerta.ForeColor = Color.Red;
                labFeedbackTorinquete1.Visible = true;
                this.Refresh();
                System.Threading.Thread.Sleep(2000);
                labFeedbackPuerta.Visible = false;
                this.Refresh();
                LOG.Error("btnPuerta_Click Error: " + ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Convert.ToInt32(textBox1.Text); i++)
            {
                btnInTorniquete1_Click(null, null);
                btnOutTorniquete1_Click(null, null);
            }
        }
    }
}
