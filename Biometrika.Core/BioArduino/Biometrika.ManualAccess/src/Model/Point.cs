﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.ManualAccess.src.Model
{
    public class Point
    {
        public string name { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public string releIn { get; set; } = "1";
        public int timeoutIn { get; set; } = 30000;
        public string releOut { get; set; } = "2";
        public int timeoutOut { get; set; } = 30000;

    }
}
