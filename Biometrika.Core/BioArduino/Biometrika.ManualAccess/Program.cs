﻿using Biometrika.Kiosk.Common.Model;
using Biometrika.ManualAccess.src.Model;
using log4net;
using log4net.Config;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.ManualAccess
{
    static class Program
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Program));
        internal static string _VERSION = "7.5.x";
        internal static DynamicData _CONFIG;

        internal static List<Point> POINT_LIST;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            /*



             */
            //List<Point> PointList = new List<Point>();
            //Point p = new Point();
            //p.name = "Name1";
            //p.url = "http://192.168.1.10/WSReleHandle.asmx";
            //p.type = "ES";
            //p.releIn = "1";
            //p.timeoutIn = 35000;
            //p.releOut = "2";
            //p.timeoutOut = 35000;
            //PointList.Add(p);
            //PointList.Add(p);
            ////Kiosk.Common.Utils.SerializeHelper.SerializeToFile(PointList, "PointList.cfg");
            //System.IO.File.WriteAllText("PointListJson.cfg", JsonConvert.SerializeObject(PointList));


            //Tomo version
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            _VERSION = "v" + version.ToString();

            //TODO - Config Log
            XmlConfigurator.Configure(new FileInfo("Logger.cfg"));
            LOG.Info("Biometrika.ManualAccess.Main ======== START APP!! =====================================");
            LOG.Info("Biometrika.ManualAccess.Main - Init Biometrika.ManualAccess version =>" + _VERSION + "...");

            //TODO - Read Config
            int ret = Initialize();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }

        public static int Initialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("BusinessRuleEngine.Intialize IN...");
                if (!System.IO.File.Exists("Biometrika.ManualAccess.cfg"))
                {
                    _CONFIG = new DynamicData();
                    _CONFIG.AddItem("PointName", "Antofagasta - Sede Angamos");
                    _CONFIG.AddItem("LogoCustomer", "logocustomer.jpg");
                    _CONFIG.AddItem("QPoints", "4");
                    Point p = new Point();
                    p.name = "Name1";
                    p.url = "http://192.168.1.10/WSReleHandle.asmx";
                    p.type = "ES";
                    p.releIn = "1";
                    p.timeoutIn = 35000;
                    p.releOut = "2";
                    p.timeoutOut = 35000;
                    List<Point> PointList = new List<Point>();
                    PointList.Add(p);
                    PointList.Add(p);
                    //_CONFIG.AddItem("Point1", Kiosk.Common.Utils.SerializeHelper.SerializeToXml(PointList));
                    _CONFIG.AddItem("PointList", JsonConvert.SerializeObject(PointList));
                    
                    //Point[] PointList = new Point[2];
                    //PointList[0] = p;
                    //PointList[2] = p;
                    //_CONFIG.AddItem("Point2", "{ \"name\": \"Point2\" , " +
                    //                            " \"url\": \"http://192.168.1.21/\" , " +
                    //                            " \"type\": \"ES\" , " +
                    //                            " \"releIn\" : \"1\"," +
                    //                            " \"releOut\" : \"2\" }");
                    //_CONFIG.AddItem("Point3", "{ \"name\": \"Point3\" , " +
                    //                            " \"url\": \"http://192.168.1.22/\" , " +
                    //                             " \"type\": \"E\" , " +
                    //                            " \"releIn\" : \"1\" }");

                    if (!Kiosk.Common.Utils.SerializeHelper.SerializeToFile(_CONFIG, "Biometrika.ManualAccess.cfg"))
                    {
                        LOG.Warn("Biometrika.ManualAccess.Initialize - No grabo confif en disco (Biometrika.ManualAccess.cfg)");
                    }
                }
                else
                {
                    _CONFIG = Kiosk.Common.Utils.SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.ManualAccess.cfg");
                    _CONFIG.Initialize();
                }

                if (_CONFIG == null)
                {
                    LOG.Fatal("Biometrika.ManualAccess.Initialize - Error leyendo Biometrika.ManualAccess.cfg!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                else
                {
                    LOG.Debug("Biometrika.ManualAccess.Initialize - Pareaeando informacion de puntos habilitados...");
                    POINT_LIST = 
                            JsonConvert.DeserializeObject<List<Point>>((string)_CONFIG.DynamicDataItems["PointList"]);
                    LOG.Debug("Biometrika.ManualAccess.Initialize - Puntos habilitados...");
                    foreach (Point item in POINT_LIST)
                    {
                        LOG.Debug("Biometrika.ManualAccess.Initialize - " + 
                                        "Nombre: " + item.name + " - URL: " + item.url + " - Type: " + item.type +
                                        " - ReleIn: " + item.releIn + " - TimeoutReleIn: " + item.timeoutIn +
                                        " - ReleOut: " + item.releOut + " - TimeoutReleOut: " + item.timeoutOut);
                    }
                    ret = 0;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Biometrika.ManualAccess.Intialize Error: " + ex.Message);
            }
            LOG.Debug("Biometrika.ManualAccess.Intialize OUT! ret = " + ret.ToString());
            return ret;
        }
    }
}
