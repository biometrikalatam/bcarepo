﻿namespace Biometrika.ManualAccess
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labVersion = new System.Windows.Forms.Label();
            this.grpTorniquete1 = new System.Windows.Forms.GroupBox();
            this.labDetailsReleTorniquete1 = new System.Windows.Forms.Label();
            this.labFeedbackTorinquete1 = new System.Windows.Forms.Label();
            this.btnOutTorniquete1 = new System.Windows.Forms.Button();
            this.btnInTorniquete1 = new System.Windows.Forms.Button();
            this.labURLTorniquete1 = new System.Windows.Forms.Label();
            this.grpPuerta = new System.Windows.Forms.GroupBox();
            this.labDetailsRelePuerta = new System.Windows.Forms.Label();
            this.labFeedbackPuerta = new System.Windows.Forms.Label();
            this.btnPuerta = new System.Windows.Forms.Button();
            this.labURLPuerta = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.grpTorniquete1.SuspendLayout();
            this.grpPuerta.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::Biometrika.ManualAccess.Properties.Resources.Biometrika_Bajada_400x140;
            this.pictureBox1.Location = new System.Drawing.Point(26, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(514, 129);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::Biometrika.ManualAccess.Properties.Resources.Biometrika_Bajada_Big_Small_ERP;
            this.pictureBox2.Location = new System.Drawing.Point(831, 770);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(240, 54);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // labVersion
            // 
            this.labVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labVersion.Location = new System.Drawing.Point(721, 747);
            this.labVersion.Name = "labVersion";
            this.labVersion.Size = new System.Drawing.Size(350, 20);
            this.labVersion.TabIndex = 2;
            this.labVersion.Text = "Antofagasta - Sede Angamos - v7.5.x";
            this.labVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grpTorniquete1
            // 
            this.grpTorniquete1.BackColor = System.Drawing.Color.Transparent;
            this.grpTorniquete1.Controls.Add(this.labDetailsReleTorniquete1);
            this.grpTorniquete1.Controls.Add(this.labFeedbackTorinquete1);
            this.grpTorniquete1.Controls.Add(this.btnOutTorniquete1);
            this.grpTorniquete1.Controls.Add(this.btnInTorniquete1);
            this.grpTorniquete1.Controls.Add(this.labURLTorniquete1);
            this.grpTorniquete1.Enabled = false;
            this.grpTorniquete1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTorniquete1.Location = new System.Drawing.Point(26, 177);
            this.grpTorniquete1.Name = "grpTorniquete1";
            this.grpTorniquete1.Size = new System.Drawing.Size(1045, 256);
            this.grpTorniquete1.TabIndex = 3;
            this.grpTorniquete1.TabStop = false;
            this.grpTorniquete1.Text = "Torniquete 1";
            // 
            // labDetailsReleTorniquete1
            // 
            this.labDetailsReleTorniquete1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDetailsReleTorniquete1.Location = new System.Drawing.Point(21, 50);
            this.labDetailsReleTorniquete1.Name = "labDetailsReleTorniquete1";
            this.labDetailsReleTorniquete1.Size = new System.Drawing.Size(473, 26);
            this.labDetailsReleTorniquete1.TabIndex = 4;
            this.labDetailsReleTorniquete1.Text = "Rele IN: 300 - Rele OUT: 100";
            // 
            // labFeedbackTorinquete1
            // 
            this.labFeedbackTorinquete1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labFeedbackTorinquete1.Location = new System.Drawing.Point(22, 191);
            this.labFeedbackTorinquete1.Name = "labFeedbackTorinquete1";
            this.labFeedbackTorinquete1.Size = new System.Drawing.Size(999, 51);
            this.labFeedbackTorinquete1.TabIndex = 3;
            this.labFeedbackTorinquete1.Text = "Apertura OK!";
            this.labFeedbackTorinquete1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labFeedbackTorinquete1.Visible = false;
            // 
            // btnOutTorniquete1
            // 
            this.btnOutTorniquete1.BackColor = System.Drawing.Color.Blue;
            this.btnOutTorniquete1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOutTorniquete1.ForeColor = System.Drawing.Color.White;
            this.btnOutTorniquete1.Location = new System.Drawing.Point(530, 83);
            this.btnOutTorniquete1.Name = "btnOutTorniquete1";
            this.btnOutTorniquete1.Size = new System.Drawing.Size(292, 105);
            this.btnOutTorniquete1.TabIndex = 2;
            this.btnOutTorniquete1.Text = "&Salida";
            this.btnOutTorniquete1.UseVisualStyleBackColor = false;
            this.btnOutTorniquete1.Click += new System.EventHandler(this.btnOutTorniquete1_Click);
            // 
            // btnInTorniquete1
            // 
            this.btnInTorniquete1.BackColor = System.Drawing.Color.Green;
            this.btnInTorniquete1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInTorniquete1.ForeColor = System.Drawing.Color.White;
            this.btnInTorniquete1.Location = new System.Drawing.Point(179, 83);
            this.btnInTorniquete1.Name = "btnInTorniquete1";
            this.btnInTorniquete1.Size = new System.Drawing.Size(292, 105);
            this.btnInTorniquete1.TabIndex = 1;
            this.btnInTorniquete1.Text = "&Entrada";
            this.btnInTorniquete1.UseVisualStyleBackColor = false;
            this.btnInTorniquete1.Click += new System.EventHandler(this.btnInTorniquete1_Click);
            // 
            // labURLTorniquete1
            // 
            this.labURLTorniquete1.AutoSize = true;
            this.labURLTorniquete1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labURLTorniquete1.ForeColor = System.Drawing.Color.DarkBlue;
            this.labURLTorniquete1.Location = new System.Drawing.Point(21, 25);
            this.labURLTorniquete1.Name = "labURLTorniquete1";
            this.labURLTorniquete1.Size = new System.Drawing.Size(284, 25);
            this.labURLTorniquete1.TabIndex = 0;
            this.labURLTorniquete1.Text = "URL => http://localhost:9873";
            // 
            // grpPuerta
            // 
            this.grpPuerta.BackColor = System.Drawing.Color.Transparent;
            this.grpPuerta.Controls.Add(this.labDetailsRelePuerta);
            this.grpPuerta.Controls.Add(this.labFeedbackPuerta);
            this.grpPuerta.Controls.Add(this.btnPuerta);
            this.grpPuerta.Controls.Add(this.labURLPuerta);
            this.grpPuerta.Enabled = false;
            this.grpPuerta.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpPuerta.Location = new System.Drawing.Point(26, 458);
            this.grpPuerta.Name = "grpPuerta";
            this.grpPuerta.Size = new System.Drawing.Size(1045, 257);
            this.grpPuerta.TabIndex = 4;
            this.grpPuerta.TabStop = false;
            this.grpPuerta.Text = "Torniquete 1";
            // 
            // labDetailsRelePuerta
            // 
            this.labDetailsRelePuerta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDetailsRelePuerta.Location = new System.Drawing.Point(9, 50);
            this.labDetailsRelePuerta.Name = "labDetailsRelePuerta";
            this.labDetailsRelePuerta.Size = new System.Drawing.Size(473, 26);
            this.labDetailsRelePuerta.TabIndex = 5;
            this.labDetailsRelePuerta.Text = "Rele OPEN: 300";
            // 
            // labFeedbackPuerta
            // 
            this.labFeedbackPuerta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labFeedbackPuerta.Location = new System.Drawing.Point(25, 190);
            this.labFeedbackPuerta.Name = "labFeedbackPuerta";
            this.labFeedbackPuerta.Size = new System.Drawing.Size(996, 51);
            this.labFeedbackPuerta.TabIndex = 3;
            this.labFeedbackPuerta.Text = "Apertura OK!";
            this.labFeedbackPuerta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labFeedbackPuerta.Visible = false;
            // 
            // btnPuerta
            // 
            this.btnPuerta.BackColor = System.Drawing.Color.Green;
            this.btnPuerta.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPuerta.ForeColor = System.Drawing.Color.White;
            this.btnPuerta.Location = new System.Drawing.Point(179, 79);
            this.btnPuerta.Name = "btnPuerta";
            this.btnPuerta.Size = new System.Drawing.Size(643, 105);
            this.btnPuerta.TabIndex = 1;
            this.btnPuerta.Text = "&Apertura";
            this.btnPuerta.UseVisualStyleBackColor = false;
            this.btnPuerta.Click += new System.EventHandler(this.btnPuerta_Click);
            // 
            // labURLPuerta
            // 
            this.labURLPuerta.AutoSize = true;
            this.labURLPuerta.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labURLPuerta.ForeColor = System.Drawing.Color.DarkBlue;
            this.labURLPuerta.Location = new System.Drawing.Point(8, 27);
            this.labURLPuerta.Name = "labURLPuerta";
            this.labURLPuerta.Size = new System.Drawing.Size(284, 25);
            this.labURLPuerta.TabIndex = 0;
            this.labURLPuerta.Text = "URL => http://localhost:9873";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(854, 99);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(854, 73);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 6;
            this.textBox1.Text = "10";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1102, 843);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.grpPuerta);
            this.Controls.Add(this.grpTorniquete1);
            this.Controls.Add(this.labVersion);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Biometrika Manual Access v7.5.x... ";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.grpTorniquete1.ResumeLayout(false);
            this.grpTorniquete1.PerformLayout();
            this.grpPuerta.ResumeLayout(false);
            this.grpPuerta.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label labVersion;
        private System.Windows.Forms.GroupBox grpTorniquete1;
        private System.Windows.Forms.Label labFeedbackTorinquete1;
        private System.Windows.Forms.Button btnOutTorniquete1;
        private System.Windows.Forms.Button btnInTorniquete1;
        private System.Windows.Forms.Label labURLTorniquete1;
        private System.Windows.Forms.GroupBox grpPuerta;
        private System.Windows.Forms.Label labFeedbackPuerta;
        private System.Windows.Forms.Button btnPuerta;
        private System.Windows.Forms.Label labURLPuerta;
        private System.Windows.Forms.Label labDetailsReleTorniquete1;
        private System.Windows.Forms.Label labDetailsRelePuerta;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
    }
}

