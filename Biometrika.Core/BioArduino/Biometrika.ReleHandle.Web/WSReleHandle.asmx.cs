﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web;
using System.Web.Services;
using Biometrika.ReleHandle.Web.src.Tx;
using log4net;

namespace Biometrika.ReleHandle.Web
{
    /// <summary>
    /// Summary description for WSReleHandle
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class WSReleHandle : System.Web.Services.WebService
    {
         
        private static readonly ILog LOG = LogManager.GetLogger(typeof(WSReleHandle));

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public int OpRele(string origin, int relenumber, int operation, out string msgerr)
        {
            int res = 0;
            msgerr = "";
            try
            {
                //if (Global.CONFIG.TypeRelayDevice == 1)
                //{
                //    if (Global.OBJ_RELE_DEVICE_USB != null)
                //    {
                //        string strAxId = "BiometrikaReleHandleWebId";
                //        short relnumber = (short) relenumber;
                //        if (operation == 1) //ON
                //        {
                //            res = Global.OBJ_RELE_DEVICE_USB.OnRelay(ref strAxId, ref relnumber);
                //        } else
                //        {
                //            res = Global.OBJ_RELE_DEVICE_USB.OffRelay(ref strAxId, ref relnumber);
                //        }
                //    }
                //    if (res != 0) msgerr = "Error en OpRele(" + relenumber + "," + operation + ") => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OpRele", relenumber, operation, 0, Global.OBJ_RELE_DEVICE_USB.USBRelayAxId,
                //                          0, "", Global.CONFIG.PortComUsbRelay);
                //}
                //else if (Global.CONFIG.TypeRelayDevice == 2)
                //{
                //    if (Global.OBJ_RELE_DEVICE != null)
                //    {
                //        res = Global.OBJ_RELE_DEVICE.OpRele(relenumber, operation);
                //    }
                //    if (res != 0) msgerr = "Error en OpRele(" + relenumber + "," + operation + ") => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OpRele", relenumber, operation, 0, Global.OBJ_RELE_DEVICE.ModuleType,
                //                          Global.OBJ_RELE_DEVICE.ModuleId,
                //                          Global.OBJ_RELE_DEVICE.Ip, Global.OBJ_RELE_DEVICE.Port);
                //}
                if (Global.CONFIG.TypeRelayDevice == 3)  //Arduino Local
                {
                    if (Global.ARDUINO_HELPER != null)
                    {
                        if (operation == 1)
                        {
                            res = Global.ARDUINO_HELPER.OpenRele("0" + relenumber.ToString());
                            LOG.Debug("WSReleHandle.OpRele - Open rele = " + relenumber.ToString() +  " => ret = " + res);
                        } else
                        {
                            res = Global.ARDUINO_HELPER.CloseRele("0" + relenumber.ToString());
                            LOG.Debug("WSReleHandle.OpRele - Close rele = " + relenumber.ToString() + " => ret = " + res);
                        }
                    }
                    if (res != 0) msgerr = "Error en OpRele(" + relenumber + "," + operation.ToString() + ") => "
                                            + res.ToString();
                    else
                        TxLog.AddTxRecord(origin, "OpRele", relenumber, operation, 0, "KMTronic2022",
                                          Global.ARDUINO_HELPER.ArduinoPort(), "", Global.ARDUINO_HELPER.ArduinoPort()) ;
                }
                if (Global.CONFIG.TypeRelayDevice == 4) //KMTronic 2022
                {
                    if (Global.KMTRONIC_HELPER != null)
                    {
                        if (operation == 1)
                        {
                            res = Global.KMTRONIC_HELPER.OpenRele("0" + relenumber.ToString());
                            LOG.Debug("WSReleHandle.OpRele - Open rele = " + relenumber.ToString() + " => ret = " + res);
                        }
                        else
                        {
                            res = Global.KMTRONIC_HELPER.CloseRele("0" + relenumber.ToString());
                            LOG.Debug("WSReleHandle.OpRele - Close rele = " + relenumber.ToString() + " => ret = " + res);
                        }
                    }
                    if (res != 0)
                        msgerr = "Error en OpRele(" + relenumber + "," + operation.ToString() + ") => "
                                            + res.ToString();
                    else
                        TxLog.AddTxRecord(origin, "OpRele", relenumber, operation, 0, "Arduino2021",
                                          Global.KMTRONIC_HELPER.KMTronicPort(), "", Global.KMTRONIC_HELPER.KMTronicPort());
                }

            }
            catch (Exception ex)
            {
                LOG.Error("WSReleHandle.OpRele", ex);
                res = -1;
                msgerr = ex.Message;
            }
            return res;
        }

        [WebMethod]
        public int OpRelePulso(string origin, int relenumber, int timeout, out string msgerr)
        {
            int res = 0;
            msgerr = "";
            try
            {

                //if (Global.CONFIG.TypeRelayDevice == 1)
                //{
                //    if (Global.OBJ_RELE_DEVICE_USB != null)
                //    {
                //        string strAxId = "BiometrikaReleHandleWebId";
                //        short relnumber = (short) relenumber;
                //        short milisec = (short) timeout;
                //        res = Global.OBJ_RELE_DEVICE_USB.OnPulsoCustom(ref strAxId, ref relnumber, ref milisec);
                //     }
                //    if (res != 0) msgerr = "Error en OpRelePulso(" + relenumber + "," + timeout + ") => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OnPulsoCustom", relenumber, 0, timeout, Global.OBJ_RELE_DEVICE_USB.USBRelayAxId,
                //                          0, "", Global.CONFIG.PortComUsbRelay);
                //}
                // else if (Global.CONFIG.TypeRelayDevice == 2)
                // {
                //     if (Global.OBJ_RELE_DEVICE != null)
                //     {
                //         res = Global.OBJ_RELE_DEVICE.OpRelePulso(relenumber, timeout);
                //     }
                //     if (res != 0)
                //         msgerr = "Error en OpRelePulso(" + relenumber + "," + timeout + ") => " + res.ToString();
                //     else
                //         TxLog.AddTxRecord(origin, "OpRelePulso", relenumber, 0, timeout, Global.OBJ_RELE_DEVICE.ModuleType,
                //                           Global.OBJ_RELE_DEVICE.ModuleId,
                //                           Global.OBJ_RELE_DEVICE.Ip, Global.OBJ_RELE_DEVICE.Port);
                // }

                if (Global.CONFIG.TypeRelayDevice == 3)
                {
                    if (Global.ARDUINO_HELPER != null)
                    {
                        res = Global.ARDUINO_HELPER.OpenWithDelay("0" + relenumber.ToString() + "0", timeout.ToString());
                    }
                    if (res != 0)
                        msgerr = "Error en OpenWithDelay(" + relenumber + "," + timeout + ") => " + res.ToString();
                    else
                        TxLog.AddTxRecord(origin, "OpRele", relenumber, 0, timeout, "Arduino2021",
                                          Global.ARDUINO_HELPER.ArduinoPort(), "", Global.ARDUINO_HELPER.ArduinoPort());
                }
                if (Global.CONFIG.TypeRelayDevice == 4) //KMTronic 2022
                {
                    if (Global.KMTRONIC_HELPER != null)
                    {
                        res = Global.KMTRONIC_HELPER.OpenWithDelay(relenumber.ToString(),timeout.ToString());
                    }
                    if (res != 0)
                        msgerr = "Error en OpenWithDelay(" + relenumber.ToString() + "," + timeout.ToString() + ") => "
                                            + res.ToString();
                    else
                        TxLog.AddTxRecord(origin, "OpRele", relenumber, 0, timeout, "Arduino2021",
                                          Global.KMTRONIC_HELPER.KMTronicPort(), "", Global.KMTRONIC_HELPER.KMTronicPort());
                }
            }
            catch (Exception ex)
            {
                LOG.Error("WSReleHandle.OpRelePulso", ex);
                res = -1;
                msgerr = ex.Message;
            }
            return res;
        }

        [WebMethod]
        public int OpReleAllON(string origin, out string msgerr)
        {
            int res = 0;
            msgerr = "";
            try
            {
                //if (Global.CONFIG.TypeRelayDevice == 1)
                //{
                //    if (Global.OBJ_RELE_DEVICE_USB != null)
                //    {
                //        string strAxId = "BiometrikaReleHandleWebId";
                //        short relnumber;
                //        for (int i = 1; i <= Global.OBJ_RELE_DEVICE_USB.QRelay; i++)
                //        {
                //            relnumber = (short) i;
                //            res = Global.OBJ_RELE_DEVICE_USB.OnRelay(ref strAxId, ref relnumber);    
                //        }

                //     }
                //    if (res != 0) msgerr = "Error en OpReleAllON() => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OpReleAllON", 0, 0, 0, Global.OBJ_RELE_DEVICE_USB.USBRelayAxId,
                //                          0, "", Global.CONFIG.PortComUsbRelay);
                //}
                //else if (Global.CONFIG.TypeRelayDevice == 2)
                //{
                //    if (Global.OBJ_RELE_DEVICE != null)
                //    {
                //        res = Global.OBJ_RELE_DEVICE.OpReleAllON();
                //    }
                //    if (res != 0) msgerr = "Error en OpReleAllON() => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OpReleAllON", 0, 0, 0, Global.OBJ_RELE_DEVICE.ModuleType,
                //                          Global.OBJ_RELE_DEVICE.ModuleId,
                //                          Global.OBJ_RELE_DEVICE.Ip, Global.OBJ_RELE_DEVICE.Port);
                //}
                if (Global.CONFIG.TypeRelayDevice == 3)
                {
                    if (Global.ARDUINO_HELPER != null)
                    {
                        res = Global.ARDUINO_HELPER.OpenAll();
                    }
                    if (res != 0) msgerr = "Error en OpReleAllON() => " + res.ToString();
                    else
                        TxLog.AddTxRecord(origin, "OpReleAllON", 0, 0, 0, "Arduino2021",
                                         Global.ARDUINO_HELPER.ArduinoPort(), "", Global.ARDUINO_HELPER.ArduinoPort());
                }
                if (Global.CONFIG.TypeRelayDevice == 4) //KMTronic 2022
                {
                    if (Global.KMTRONIC_HELPER != null)
                    {
                        res = Global.KMTRONIC_HELPER.OpenAll();
                    }
                    if (res != 0)
                        msgerr = "Error en OpenAll() => " + res.ToString();
                    else
                        TxLog.AddTxRecord(origin, "OpReleAllON", 0, 0, 0, "KMTronic2022",
                                          Global.KMTRONIC_HELPER.KMTronicPort(), "", Global.KMTRONIC_HELPER.KMTronicPort());
                }
            }
            catch (Exception ex)
            {
                LOG.Error("WSReleHandle.OpReleAllON", ex);
                res = -1;
                msgerr = ex.Message;
            }
            return res;
        }

        [WebMethod]
        public int OpReleAllOFF(string origin, out string msgerr)
        {
            int res = 0;
            msgerr = "";
            try
            {
                //if (Global.CONFIG.TypeRelayDevice == 1)
                //{
                //    if (Global.OBJ_RELE_DEVICE_USB != null)
                //    {
                //        string strAxId = "BiometrikaReleHandleWebId";
                //        short relnumber;
                //        for (int i = 1; i <= Global.OBJ_RELE_DEVICE_USB.QRelay; i++)
                //        {
                //            relnumber = (short) i;
                //            res = Global.OBJ_RELE_DEVICE_USB.OffRelay(ref strAxId, ref relnumber);    
                //        }

                //     }
                //    if (res != 0) msgerr = "Error en OpReleAllOFF() => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OpReleAllOFF", 0, 0, 0, Global.OBJ_RELE_DEVICE_USB.USBRelayAxId,
                //                          0, "", Global.CONFIG.PortComUsbRelay);
                //}
                // else if (Global.CONFIG.TypeRelayDevice == 2)
                // {
                //     if (Global.OBJ_RELE_DEVICE != null)
                //     {
                //         res = Global.OBJ_RELE_DEVICE.OpReleAllOFF();
                //     }
                //     if (res != 0) msgerr = "Error en OpReleAllOFF() => " + res.ToString();
                //     else
                //         TxLog.AddTxRecord(origin, "OpReleAllOFF", 0, 0, 0, Global.OBJ_RELE_DEVICE.ModuleType,
                //                           Global.OBJ_RELE_DEVICE.ModuleId,
                //                           Global.OBJ_RELE_DEVICE.Ip, Global.OBJ_RELE_DEVICE.Port);
                // }
                if (Global.CONFIG.TypeRelayDevice == 3)
                {
                    if (Global.ARDUINO_HELPER != null)
                    {
                        res = Global.ARDUINO_HELPER.CloseAll();
                    }
                    if (res != 0) msgerr = "Error en OpReleAllOFF() => " + res.ToString();
                    else
                        TxLog.AddTxRecord(origin, "OpReleAllOFF", 0, 0, 0, "Arduino2021",
                                         Global.ARDUINO_HELPER.ArduinoPort(), "", Global.ARDUINO_HELPER.ArduinoPort());
                }
                if (Global.CONFIG.TypeRelayDevice == 4) //KMTronic 2022
                {
                    if (Global.KMTRONIC_HELPER != null)
                    {
                        res = Global.KMTRONIC_HELPER.CloseAll();
                    }
                    if (res != 0)
                        msgerr = "Error en CloseAll() => " + res.ToString();
                    else
                        TxLog.AddTxRecord(origin, "OpReleAllOFF", 0, 0, 0, "KMTronic2022",
                                          Global.KMTRONIC_HELPER.KMTronicPort(), "", Global.KMTRONIC_HELPER.KMTronicPort());
                }
            }
            catch (Exception ex)
            {
                LOG.Error("WSReleHandle.OpReleAllOFF", ex);
                res = -1;
                msgerr = ex.Message;
            }
            return res;
        }

        [WebMethod]
        public int OpAllRelePulso(string origin, int timeout, out string msgerr)
        {
            int res = 0;
            msgerr = "";
            try
            {
                LOG.Debug("WSReleHandle.OpAllRelePulso IN...");
                //if (Global.CONFIG.TypeRelayDevice == 1)
                //{
                //    if (Global.OBJ_RELE_DEVICE_USB != null)
                //    {
                //        string strAxId = "BiometrikaReleHandleWebId";
                //        short relnumber = (short) relenumber;
                //        short milisec = (short) timeout;
                //        res = Global.OBJ_RELE_DEVICE_USB.OnPulsoCustom(ref strAxId, ref relnumber, ref milisec);
                //     }
                //    if (res != 0) msgerr = "Error en OpRelePulso(" + relenumber + "," + timeout + ") => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OnPulsoCustom", relenumber, 0, timeout, Global.OBJ_RELE_DEVICE_USB.USBRelayAxId,
                //                          0, "", Global.CONFIG.PortComUsbRelay);
                //}
                // else if (Global.CONFIG.TypeRelayDevice == 2)
                // {
                //     if (Global.OBJ_RELE_DEVICE != null)
                //     {
                //         res = Global.OBJ_RELE_DEVICE.OpRelePulso(relenumber, timeout);
                //     }
                //     if (res != 0)
                //         msgerr = "Error en OpRelePulso(" + relenumber + "," + timeout + ") => " + res.ToString();
                //     else
                //         TxLog.AddTxRecord(origin, "OpRelePulso", relenumber, 0, timeout, Global.OBJ_RELE_DEVICE.ModuleType,
                //                           Global.OBJ_RELE_DEVICE.ModuleId,
                //                           Global.OBJ_RELE_DEVICE.Ip, Global.OBJ_RELE_DEVICE.Port);
                // }

                //if (Global.CONFIG.TypeRelayDevice == 3)
                //{
                //    if (Global.ARDUINO_HELPER != null)
                //    {
                //        res = Global.ARDUINO_HELPER.OpenWithDelay("0" + relenumber.ToString() + "0", timeout.ToString());
                //    }
                //    if (res != 0)
                //        msgerr = "Error en OpenWithDelay(" + relenumber + "," + timeout + ") => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OpRele", relenumber, 0, timeout, "Arduino2021",
                //                          Global.ARDUINO_HELPER.ArduinoPort(), "", Global.ARDUINO_HELPER.ArduinoPort());
                //}
                if (Global.CONFIG.TypeRelayDevice == 4) //KMTronic 2022
                {
                    if (Global.KMTRONIC_HELPER != null)
                    {
                        res = Global.KMTRONIC_HELPER.OpenAllWithDelay(Global.KMTRONIC_HELPER.KMTronicQReles(), timeout.ToString());
                    }
                    if (res != 0)
                        msgerr = "Error en OpenAllWithDelay(" + Global.KMTRONIC_HELPER.KMTronicQReles().ToString() + "," + timeout.ToString() + ") => "
                                            + res.ToString();
                    else
                        TxLog.AddTxRecord(origin, "OpAllRelePulso", 0, 0, timeout, "KMTronic2022",
                                          Global.KMTRONIC_HELPER.KMTronicPort(), "", Global.KMTRONIC_HELPER.KMTronicPort());
                }
            }
            catch (Exception ex)
            {
                LOG.Error("WSReleHandle.OpAllRelePulso", ex);
                res = -1;
                msgerr = ex.Message;
            }
            LOG.Debug("WSReleHandle.OpAllRelePulso OUT! res = " + res.ToString());
            return res;
        }

        //[WebMethod]
        //public int OpReleAllParON(string origin, out string msgerr)
        //{
        //    int res = 0;
        //    msgerr = "";
        //    try
        //    {
        //        if (Global.CONFIG.TypeRelayDevice == 1)
        //        {
        //            if (Global.OBJ_RELE_DEVICE_USB != null)
        //            {
        //                string strAxId = "BiometrikaReleHandleWebId";
        //                short relnumber;
        //                for (int i = 1; i <= Global.OBJ_RELE_DEVICE_USB.QRelay; i++)
        //                {
        //                    relnumber = (short) i;
        //                    if (i % 2 == 0)
        //                    {
        //                        res = Global.OBJ_RELE_DEVICE_USB.OnRelay(ref strAxId, ref relnumber);
        //                    } else
        //                    {
        //                        res = Global.OBJ_RELE_DEVICE_USB.OffRelay(ref strAxId, ref relnumber);
        //                    }
        //                }

        //             }
        //            if (res != 0) msgerr = "Error en OpReleAllParON() => " + res.ToString();
        //            else
        //                TxLog.AddTxRecord(origin, "OpReleAllParON", 0, 0, 0, Global.OBJ_RELE_DEVICE_USB.USBRelayAxId,
        //                                  0, "", Global.CONFIG.PortComUsbRelay);
        //        }
        //        else if (Global.CONFIG.TypeRelayDevice == 2)
        //        {

        //            if (Global.OBJ_RELE_DEVICE != null)
        //            {
        //                res = Global.OBJ_RELE_DEVICE.OpReleAllParON();
        //            }
        //            if (res != 0) msgerr = "Error en OpReleAllParON() => " + res.ToString();
        //            else
        //                TxLog.AddTxRecord(origin, "OpReleAllParON", 0, 0, 0, Global.OBJ_RELE_DEVICE.ModuleType,
        //                                  Global.OBJ_RELE_DEVICE.ModuleId,
        //                                  Global.OBJ_RELE_DEVICE.Ip, Global.OBJ_RELE_DEVICE.Port);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("WSReleHandle.OpReleAllParON", ex);
        //        res = -1;
        //        msgerr = ex.Message;
        //    }
        //    return res;
        //}

        //[WebMethod]
        //public int OpReleAllImparON(string origin, out string msgerr)
        //{
        //    int res = 0;
        //    msgerr = "";
        //    try
        //    {
        //        if (Global.CONFIG.TypeRelayDevice == 1)
        //        {
        //            if (Global.OBJ_RELE_DEVICE_USB != null)
        //            {
        //                string strAxId = "BiometrikaReleHandleWebId";
        //                short relnumber;
        //                for (int i = 1; i <= Global.OBJ_RELE_DEVICE_USB.QRelay; i++)
        //                {
        //                    relnumber = (short) i;
        //                    if (i % 2 == 1)
        //                    {
        //                        res = Global.OBJ_RELE_DEVICE_USB.OnRelay(ref strAxId, ref relnumber);
        //                    } else
        //                    {
        //                        res = Global.OBJ_RELE_DEVICE_USB.OffRelay(ref strAxId, ref relnumber);
        //                    }
        //                }

        //             }
        //            if (res != 0) msgerr = "Error en OpReleAllImparON() => " + res.ToString();
        //            else
        //                TxLog.AddTxRecord(origin, "OpReleAllImparON", 0, 0, 0, Global.OBJ_RELE_DEVICE_USB.USBRelayAxId,
        //                                  0, "", Global.CONFIG.PortComUsbRelay);
        //        }
        //        else if (Global.CONFIG.TypeRelayDevice == 2)
        //        {
        //            if (Global.OBJ_RELE_DEVICE != null)
        //            {
        //                res = Global.OBJ_RELE_DEVICE.OpReleAllImparON();
        //            }
        //            if (res != 0) msgerr = "Error en OpReleAllImparON() => " + res.ToString();
        //            else
        //                TxLog.AddTxRecord(origin, "OpReleAllImparON", 0, 0, 0, Global.OBJ_RELE_DEVICE.ModuleType,
        //                                  Global.OBJ_RELE_DEVICE.ModuleId,
        //                                  Global.OBJ_RELE_DEVICE.Ip, Global.OBJ_RELE_DEVICE.Port);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("WSReleHandle.OpReleAllImparON", ex);
        //        res = -1;
        //        msgerr = ex.Message;
        //    }
        //    return res;
        //}

        //[WebMethod]
        //public int GetStates(string origin, out string states , out string msgerr)
        //{
        //    int res = 0;
        //    msgerr = "";
        //    states = "";
        //    try
        //    {
        //        if (Global.OBJ_RELE_DEVICE != null)
        //        {
        //            int[] istates = Global.OBJ_RELE_DEVICE.GetStates();
        //            bool isFirst = true;
        //            for (int i = 0; i < istates.Length; i++)
        //            {
        //                if (isFirst) states = istates[i].ToString();
        //                states = states + "|" + istates[i].ToString();    
        //            }

        //        }
        //        if (res != 0) msgerr = "Error en GetStates() => " + res.ToString();
        //        else
        //            TxLog.AddTxRecord(origin, "GetStates", 0, 0, 0, Global.OBJ_RELE_DEVICE.ModuleType,
        //                              Global.OBJ_RELE_DEVICE.ModuleId,
        //                              Global.OBJ_RELE_DEVICE.Ip, Global.OBJ_RELE_DEVICE.Port);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("WSReleHandle.GetStates", ex);
        //        res = -1;
        //        msgerr = ex.Message;
        //    }
        //    return res;
        //}

    }
}
