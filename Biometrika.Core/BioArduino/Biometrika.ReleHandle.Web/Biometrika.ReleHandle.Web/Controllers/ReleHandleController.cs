﻿using Biometrika.ReleHandle.Web.src.Tx;
using log4net;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Biometrika.ReleHandle.Web
{
    public class ReleHandleController : ApiController
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(WSReleHandle));

        [Route("api/OpRele")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "ReleHandleResponse", typeof(ReleHandleResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "ReleHandleResponse", typeof(ReleHandleResponse))]
        public object OpRele(string origin, int relenumber, int operation)
        {
            int res = 0;
            ReleHandleResponse RESPONSE = new ReleHandleResponse();
            try
            {
                LOG.Debug("ReleHandleController.OpRele IN...");
                //if (Global.CONFIG.TypeRelayDevice == 1)
                //{
                //    if (Global.OBJ_RELE_DEVICE_USB != null)
                //    {
                //        string strAxId = "BiometrikaReleHandleWebId";
                //        short relnumber = (short) relenumber;
                //        if (operation == 1) //ON
                //        {
                //            res = Global.OBJ_RELE_DEVICE_USB.OnRelay(ref strAxId, ref relnumber);
                //        } else
                //        {
                //            res = Global.OBJ_RELE_DEVICE_USB.OffRelay(ref strAxId, ref relnumber);
                //        }
                //    }
                //    if (res != 0) msgerr = "Error en OpRele(" + relenumber + "," + operation + ") => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OpRele", relenumber, operation, 0, Global.OBJ_RELE_DEVICE_USB.USBRelayAxId,
                //                          0, "", Global.CONFIG.PortComUsbRelay);
                //}
                //else if (Global.CONFIG.TypeRelayDevice == 2)
                //{
                //    if (Global.OBJ_RELE_DEVICE != null)
                //    {
                //        res = Global.OBJ_RELE_DEVICE.OpRele(relenumber, operation);
                //    }
                //    if (res != 0) msgerr = "Error en OpRele(" + relenumber + "," + operation + ") => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OpRele", relenumber, operation, 0, Global.OBJ_RELE_DEVICE.ModuleType,
                //                          Global.OBJ_RELE_DEVICE.ModuleId,
                //                          Global.OBJ_RELE_DEVICE.Ip, Global.OBJ_RELE_DEVICE.Port);
                //}
                if (Global.CONFIG.TypeRelayDevice == 3)
                {
                    LOG.Debug("ReleHandleController.OpRelePulso TypeRelayDevice == 3...");
                    if (Global.ARDUINO_HELPER != null)
                    {
                        if (operation == 1)
                        {
                            LOG.Debug("ReleHandleController.OpRele - OpenRele...");
                            RESPONSE.code = Global.ARDUINO_HELPER.OpenRele("0" + relenumber.ToString());
                            LOG.Debug("ReleHandleController.OpRele - Ret = " + RESPONSE.code.ToString());
                            LOG.Debug("ReleHandleController.OpRele - Open rele = " + relenumber.ToString() + " => ret = " + res);
                        }
                        else
                        {
                            LOG.Debug("ReleHandleController.OpRele - CloseRele...");
                            RESPONSE.code = Global.ARDUINO_HELPER.CloseRele("0" + relenumber.ToString());
                            LOG.Debug("ReleHandleController.OpRele - Ret = " + RESPONSE.code.ToString());
                            LOG.Debug("ReleHandleController.OpRele - Close rele = " + relenumber.ToString() + " => ret = " + res);
                        }
                    }
                    if (res != 0) RESPONSE.message = "Error en OpRele(" + relenumber + "," + operation.ToString() + ") => "
                                            + res.ToString();
                    else
                    {
                        TxLog.AddTxRecord(origin, "OpRele", relenumber, operation, 0, "Arduino2021",
                                          Global.ARDUINO_HELPER.ArduinoPort(), "", Global.ARDUINO_HELPER.ArduinoPort());
                        LOG.Debug("ReleHandleController.OpRele AddTxRecord!");
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("WSReleHandle.OpRele", ex);
                RESPONSE.code = -1;
                RESPONSE.message = ex.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, RESPONSE);
            }
            return Request.CreateResponse(HttpStatusCode.OK, RESPONSE);
        }

        [Route("api/OpRelePulso")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "ReleHandleResponse", typeof(ReleHandleResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "ReleHandleResponse", typeof(ReleHandleResponse))]
        public object OpRelePulso(string origin, int relenumber, int timeout)
        {
            int res = 0;
            ReleHandleResponse RESPONSE = new ReleHandleResponse();
            try
            {
                LOG.Debug("ReleHandleController.OpRelePulso IN...");
                 //if (Global.CONFIG.TypeRelayDevice == 1)
                //{
                //    if (Global.OBJ_RELE_DEVICE_USB != null)
                //    {
                //        string strAxId = "BiometrikaReleHandleWebId";
                //        short relnumber = (short) relenumber;
                //        short milisec = (short) timeout;
                //        res = Global.OBJ_RELE_DEVICE_USB.OnPulsoCustom(ref strAxId, ref relnumber, ref milisec);
                //     }
                //    if (res != 0) msgerr = "Error en OpRelePulso(" + relenumber + "," + timeout + ") => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OnPulsoCustom", relenumber, 0, timeout, Global.OBJ_RELE_DEVICE_USB.USBRelayAxId,
                //                          0, "", Global.CONFIG.PortComUsbRelay);
                //}
                // else if (Global.CONFIG.TypeRelayDevice == 2)
                // {
                //     if (Global.OBJ_RELE_DEVICE != null)
                //     {
                //         res = Global.OBJ_RELE_DEVICE.OpRelePulso(relenumber, timeout);
                //     }
                //     if (res != 0)
                //         msgerr = "Error en OpRelePulso(" + relenumber + "," + timeout + ") => " + res.ToString();
                //     else
                //         TxLog.AddTxRecord(origin, "OpRelePulso", relenumber, 0, timeout, Global.OBJ_RELE_DEVICE.ModuleType,
                //                           Global.OBJ_RELE_DEVICE.ModuleId,
                //                           Global.OBJ_RELE_DEVICE.Ip, Global.OBJ_RELE_DEVICE.Port);
                // }

                if (Global.CONFIG.TypeRelayDevice == 3)
                {
                    LOG.Debug("ReleHandleController.OpRelePulso TypeRelayDevice == 3...");
                    if (Global.ARDUINO_HELPER != null)
                    {
                        LOG.Debug("ReleHandleController.OpReleAllOFF - OpenWithDelay...");
                        RESPONSE.code = Global.ARDUINO_HELPER.OpenWithDelay("0" + relenumber.ToString() + "0", timeout.ToString());
                        LOG.Debug("ReleHandleController.OpReleAllOFF - Ret = " + RESPONSE.code.ToString());
                    }
                    if (RESPONSE.code != 0)
                        RESPONSE.message = "Error en OpRelePulso(" + relenumber + "," + timeout + ") => " + res.ToString();
                    else
                    {
                        TxLog.AddTxRecord(origin, "OpRele", relenumber, 0, timeout, "Arduino2021",
                                          Global.ARDUINO_HELPER.ArduinoPort(), "", Global.ARDUINO_HELPER.ArduinoPort());
                        LOG.Debug("WSReleHandle.OpRele AddTxRecord!");
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("WSReleHandle.OpRelePulso", ex);
                RESPONSE.code = -1;
                RESPONSE.message = ex.Message;
                if (Request != null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, RESPONSE);
                }
                else
                {
                    return RESPONSE;
                }
            }
            if (Request != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, RESPONSE);
            }
            else
            {
                return RESPONSE;
            }
            
        }

        [Route("api/OpReleAllON")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "ReleHandleResponse", typeof(ReleHandleResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "ReleHandleResponse", typeof(ReleHandleResponse))]
        public object OpReleAllON(string origin) //, out string msgerr)
        {
            int res = 0;
            ReleHandleResponse RESPONSE = new ReleHandleResponse();
            try
            {
                LOG.Debug("ReleHandleController.OpReleAllON IN...");
                //if (Global.CONFIG.TypeRelayDevice == 1)
                //{
                //    if (Global.OBJ_RELE_DEVICE_USB != null)
                //    {
                //        string strAxId = "BiometrikaReleHandleWebId";
                //        short relnumber;
                //        for (int i = 1; i <= Global.OBJ_RELE_DEVICE_USB.QRelay; i++)
                //        {
                //            relnumber = (short) i;
                //            res = Global.OBJ_RELE_DEVICE_USB.OnRelay(ref strAxId, ref relnumber);    
                //        }

                //     }
                //    if (res != 0) msgerr = "Error en OpReleAllON() => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OpReleAllON", 0, 0, 0, Global.OBJ_RELE_DEVICE_USB.USBRelayAxId,
                //                          0, "", Global.CONFIG.PortComUsbRelay);
                //}
                //else if (Global.CONFIG.TypeRelayDevice == 2)
                //{
                //    if (Global.OBJ_RELE_DEVICE != null)
                //    {
                //        res = Global.OBJ_RELE_DEVICE.OpReleAllON();
                //    }
                //    if (res != 0) msgerr = "Error en OpReleAllON() => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OpReleAllON", 0, 0, 0, Global.OBJ_RELE_DEVICE.ModuleType,
                //                          Global.OBJ_RELE_DEVICE.ModuleId,
                //                          Global.OBJ_RELE_DEVICE.Ip, Global.OBJ_RELE_DEVICE.Port);
                //}
                if (Global.CONFIG.TypeRelayDevice == 3)
                {
                    LOG.Debug("ReleHandleController.OpReleAllON TypeRelayDevice == 3...");
                    if (Global.ARDUINO_HELPER != null)
                    {
                        LOG.Debug("ReleHandleController.OpReleAllON - CloseAll...");
                        RESPONSE.code = Global.ARDUINO_HELPER.OpenAll();
                        LOG.Debug("ReleHandleController.OpReleAllON - Ret = " + RESPONSE.code.ToString());
                    }
                    if (res != 0) RESPONSE.message = "Error en OpReleAllON() => " + res.ToString();
                    else
                    {
                        TxLog.AddTxRecord(origin, "OpReleAllON", 0, 0, 0, "Arduino2021",
                                         Global.ARDUINO_HELPER.ArduinoPort(), "", Global.ARDUINO_HELPER.ArduinoPort());
                        LOG.Debug("WSReleHandle.OpReleAllON AddTxRecord!");
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("WSReleHandle.OpReleAllON", ex);
                RESPONSE.code = -1;
                RESPONSE.message = ex.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, RESPONSE);
            }
            return Request.CreateResponse(HttpStatusCode.OK, RESPONSE); 
        }

        [Route("api/OpReleAllOFF")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "ReleHandleResponse", typeof(ReleHandleResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "ReleHandleResponse", typeof(ReleHandleResponse))]
        public object OpReleAllOFF(string origin)
        {
            int res = 0;
            ReleHandleResponse RESPONSE = new ReleHandleResponse(); 
            try
            {
                LOG.Debug("ReleHandleController.OpReleAllOFF IN...");
                //if (Global.CONFIG.TypeRelayDevice == 1)
                //{
                //    if (Global.OBJ_RELE_DEVICE_USB != null)
                //    {
                //        string strAxId = "BiometrikaReleHandleWebId";
                //        short relnumber;
                //        for (int i = 1; i <= Global.OBJ_RELE_DEVICE_USB.QRelay; i++)
                //        {
                //            relnumber = (short) i;
                //            res = Global.OBJ_RELE_DEVICE_USB.OffRelay(ref strAxId, ref relnumber);    
                //        }

                //     }
                //    if (res != 0) msgerr = "Error en OpReleAllOFF() => " + res.ToString();
                //    else
                //        TxLog.AddTxRecord(origin, "OpReleAllOFF", 0, 0, 0, Global.OBJ_RELE_DEVICE_USB.USBRelayAxId,
                //                          0, "", Global.CONFIG.PortComUsbRelay);
                //}
                // else if (Global.CONFIG.TypeRelayDevice == 2)
                // {
                //     if (Global.OBJ_RELE_DEVICE != null)
                //     {
                //         res = Global.OBJ_RELE_DEVICE.OpReleAllOFF();
                //     }
                //     if (res != 0) msgerr = "Error en OpReleAllOFF() => " + res.ToString();
                //     else
                //         TxLog.AddTxRecord(origin, "OpReleAllOFF", 0, 0, 0, Global.OBJ_RELE_DEVICE.ModuleType,
                //                           Global.OBJ_RELE_DEVICE.ModuleId,
                //                           Global.OBJ_RELE_DEVICE.Ip, Global.OBJ_RELE_DEVICE.Port);
                // }
                if (Global.CONFIG.TypeRelayDevice == 3)
                {
                    LOG.Debug("ReleHandleController.OpReleAllOFF - TypeRelayDevice == 3...");
                    if (Global.ARDUINO_HELPER != null)
                    {
                        LOG.Debug("ReleHandleController.OpReleAllOFF - CloseAll...");
                        RESPONSE.code = Global.ARDUINO_HELPER.CloseAll();
                        LOG.Debug("ReleHandleController.OpReleAllOFF - Ret = " + RESPONSE.code.ToString());
                    }
                    if (res != 0) RESPONSE.message = "Error en OpReleAllOFF() => " + res.ToString();
                    else
                    {
                        TxLog.AddTxRecord(origin, "OpReleAllOFF", 0, 0, 0, "Arduino2021",
                                         Global.ARDUINO_HELPER.ArduinoPort(), "", Global.ARDUINO_HELPER.ArduinoPort());
                        LOG.Debug("WSReleHandle.OpReleAllOFF AddTxRecord!");
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("ReleHandleController.OpReleAllOFF", ex);
                RESPONSE.code = -1;
                RESPONSE.message = ex.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, RESPONSE);
            }
            LOG.Debug("ReleHandleController.OpReleAllOFF OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, RESPONSE);
        }



        // GET api/<controller>
        [Route("api/ping")]
        public IEnumerable<string> Get()
        {
            return new string[] { "Servicio Vivo! " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "..."};
        }

        // GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}