﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using Bio.Core.Utils;
using BioArduinoRele;
using Biometrika.ReleHandle.Web.Controllers;
using Biometrika.ReleHandle.Web.src.Config;
using log4net;
using log4net.Config;
//using USBRelayAxNoGUI;

namespace Biometrika.ReleHandle.Web
{
    public class Global : System.Web.HttpApplication
    {

        /// <summary>
        /// LOG del modulo
        /// </summary>
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Global));

        //internal static USBRelayAxNoGUI.USBRelayAxDllClass OBJ_RELE_DEVICE_USB;
        //internal static TcpIpReleLib.TcpIpReleLib OBJ_RELE_DEVICE;

        internal static ArduinoHelper ARDUINO_HELPER;

        internal static KMTronicHelper KMTRONIC_HELPER;
        

        internal static Config CONFIG = new Config();

        internal static AccessControllerUoW _ACCESS_CONTROLLER_UoW = new AccessControllerUoW();



        protected void Application_Start(object sender, EventArgs e)
        {
            try
            {
                WriteEventLog("Biometrika.ReleHandle.Web Starting...", LOG_INFO);

                AreaRegistration.RegisterAllAreas();
                GlobalConfiguration.Configure(WebApiConfig.Register);
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                BundleConfig.RegisterBundles(BundleTable.Bundles);

                //1.- Inicializo LOG
                WriteEventLog("Biometrika.ReleHandle.Web Leyendo Logger Config [" +
                                Biometrika.ReleHandle.Web.Properties.Settings.Default.PathLogger + "]", LOG_INFO);
                XmlConfigurator.Configure(new FileInfo(Biometrika.ReleHandle.Web.Properties.Settings.Default.PathLogger));
                LOG.Info("Biometrika.ReleHandle.Web Starting...");

                WriteEventLog("Biometrika.ReleHandle.Web Leyendo Biometrika.ReleHandle.Web Co0nfig [" +
                    Biometrika.ReleHandle.Web.Properties.Settings.Default.PathConfig, LOG_INFO);
                //2.Config
                //0.- Levanto configuración BioPortal
                CONFIG =
                    SerializeHelper.DeserializeFromFile<Config>(
                        Biometrika.ReleHandle.Web.Properties.Settings.Default.PathConfig);
                if (CONFIG == null)
                {
                    LOG.Warn("Biometrika.ReleHandle.Web - Leyo config vacio => crea uno...");
                    CONFIG = new Config();
                    SerializeHelper.SerializeToFile(CONFIG, Biometrika.ReleHandle.Web.Properties.Settings.Default.PathConfig);
                }

                _ACCESS_CONTROLLER_UoW = new AccessControllerUoW();
                int ret = _ACCESS_CONTROLLER_UoW.Initialize(); 
                if (ret < 0)
                {
                    WriteEventLog("Biometrika.ReleHandle.Web Error inicializando AccessConfig!!", LOG_ERROR);
                    LOG.Warn("Biometrika.ReleHandle.Web Error inicializando AccessConfig!!");
                } else
                {
                    WriteEventLog("Biometrika.ReleHandle.Web _ACCESS_CONTROLLER_UoW Inicilizado OK!", LOG_INFO);
                    LOG.Info("Biometrika.ReleHandle.Web _ACCESS_CONTROLLER_UoW Inicilizado OK!");
                    LOG.Info(_ACCESS_CONTROLLER_UoW._CONFIG.ToString());
                }
 
                  //"Biometrika.ReleHandle.Web.cfg");
                WriteEventLog("Biometrika.ReleHandle.Web Initializing Device...", LOG_INFO);
                LOG.Info("Biometrika.ReleHandle.Web -  Initializing Device...");
                InitializationRelayDevice();

                //Levanto TxLogs Si existe
                Biometrika.ReleHandle.Web.src.Tx.TxLog.LoadTxRecord();

            }
            catch (Exception ex)
            {
                LOG.Error("Application_Start", ex);
            }
        }

        internal static void InitializationRelayDevice()
        {
            try
            {
                switch (CONFIG.TypeRelayDevice)
                {
                    case 1:
                        WriteEventLog("Biometrika.ReleHandle.Web (InitializationRelayDevice) Init USB Rele [TypeRelayDevice=" +
                            CONFIG.TypeRelayDevice + "] DEPRECATED!", LOG_WARN);
                        /*
                            WriteEventLog("Biometrika.ReleHandle.Web (InitializationRelayDevice) Init USB Rele [TypeRelayDevice=" +
                                CONFIG.TypeRelayDevice + "]", LOG_INFO);
                            if (OBJ_RELE_DEVICE_USB != null)
                            {
                                try
                                {
                                    //OBJ_RELE_DEVICE_USB.;
                                    OBJ_RELE_DEVICE_USB = null;
                                }
                                catch { }

                            }
                            WriteEventLog("Biometrika.ReleHandle.Web (InitializationRelayDevice) Creando Clase...", LOG_INFO);
                            OBJ_RELE_DEVICE_USB = new USBRelayAxDllClass(); 
                            if (OBJ_RELE_DEVICE_USB != null)
                            {
                                string strAxId = "BiometrikaReleHandleWebId";
                                short portcom = (short)CONFIG.PortComUsbRelay;
                                short qrelay = (short)CONFIG.QRelay;
                                //OBJ_RELE_DEVICE_USB.PortCom = portcom;
                                OBJ_RELE_DEVICE_USB.USBRelayAxId = strAxId;
                                short iconx = OBJ_RELE_DEVICE_USB.InitComponent(ref strAxId, ref portcom, ref qrelay);
                                if (iconx != 0)
                                {
                                    WriteEventLog("Biometrika.ReleHandle.Web (InitializationRelayDevice) OBJ_RELE_DEVICE_USB.InitComponent Error [" +
                                                    iconx + "]", LOG_ERROR);
                                    LOG.Fatal("Application_Start - Error conectando a USBRelayAxDll.InitComponent(" +
                                   strAxId + "," + CONFIG.PortComUsbRelay + "," + CONFIG.QRelay + ") = " + iconx);
                                }
                                else
                                {
                                    WriteEventLog("Biometrika.ReleHandle.Web (InitializationRelayDevice) USBRelayAxDll.InitComponent(" + strAxId + "," + CONFIG.PortComUsbRelay + "," + CONFIG.QRelay +
                                        ") Conectado =>" + OBJ_RELE_DEVICE_USB.PortCom + " - " + OBJ_RELE_DEVICE_USB.QRelay + "]", LOG_INFO);
                                    LOG.Info("USBRelayAxDll.InitComponent(" + strAxId + "," + CONFIG.PortComUsbRelay + "," + CONFIG.QRelay +
                                        ") Conectado =>" + OBJ_RELE_DEVICE_USB.PortCom + " - " + OBJ_RELE_DEVICE_USB.QRelay);
                                    short rele = 1;
                                    OBJ_RELE_DEVICE_USB.OffRelay(ref strAxId, ref rele);
                                    rele = 2;
                                    OBJ_RELE_DEVICE_USB.OffRelay(ref strAxId, ref rele);
                                    if (CONFIG.QRelay == 4)
                                    {
                                        rele = 3;
                                        OBJ_RELE_DEVICE_USB.OffRelay(ref strAxId, ref rele);
                                        rele = 4;
                                        OBJ_RELE_DEVICE_USB.OffRelay(ref strAxId, ref rele);
                                    }
                                }
                            }
                            else
                            {
                                WriteEventLog(
                                    "Biometrika.ReleHandle.Web (InitializationRelayDevice) USBRelayAxDll.InitComponent = Null",
                                    LOG_ERROR);
                                LOG.Fatal("Application_Start - Error creando USBRelayAxDll.InitComponent(" +
                                            "BiometrikaReleHandleWebId," + CONFIG.PortComUsbRelay + "," + CONFIG.QRelay +
                                        ") = null");
                            }
                        */
                        break;
                    case 2:
                        WriteEventLog("Biometrika.ReleHandle.Web (InitializationRelayDevice) Init TCP/IP Rele [TypeRelayDevice=" +
                           CONFIG.TypeRelayDevice + "] DEPRECATED!", LOG_WARN);

                        //WriteEventLog("Biometrika.ReleHandle.Web (InitializationRelayDevice) Init TCP/IP Rele [TypeRelayDevice=" +
                        //   CONFIG.TypeRelayDevice + "]", LOG_INFO);
                        //if (OBJ_RELE_DEVICE != null)
                        //{
                        //    try
                        //    {
                        //        OBJ_RELE_DEVICE.Disconnect();
                        //        OBJ_RELE_DEVICE = null;
                        //    }
                        //    catch {}
                            
                        //}
                        //OBJ_RELE_DEVICE = new TcpIpReleLib.TcpIpReleLib(CONFIG.IpRele, CONFIG.PortRele);
                        //if (OBJ_RELE_DEVICE != null)
                        //{
                        //    int iconx = OBJ_RELE_DEVICE.Connect();
                        //    if (iconx != 0)
                        //    {
                        //        LOG.Fatal("Application_Start - Error coenctando a TcpIpReleLib(" +
                        //       CONFIG.IpRele + "," + CONFIG.PortRele + ") = " + iconx);
                        //    }
                        //    else
                        //    {
                        //        LOG.Info("TcpIpReleLib(" + CONFIG.IpRele + "," + CONFIG.PortRele +
                        //            ") Conectado =>" + +OBJ_RELE_DEVICE.ModuleId +
                        //         " - " + OBJ_RELE_DEVICE.ModuleType +
                        //         " - " + OBJ_RELE_DEVICE.Ip +
                        //         " - " + OBJ_RELE_DEVICE.Port);
                        //        OBJ_RELE_DEVICE.OpReleAllOFF();
                        //    }
                        //}
                        //else
                        //{
                        //    LOG.Fatal("Application_Start - Error creando TcpIpReleLib(" +
                        //        CONFIG.IpRele + "," + CONFIG.PortRele + ") = null");
                        //}
                        break;
                    case 3: //Arduino 2021
                        WriteEventLog("Biometrika.ReleHandle.Web (InitializationRelayDevice) Init TCP/IP Rele [TypeRelayDevice=" +
                           CONFIG.TypeRelayDevice + "]", LOG_INFO);
                        LOG.Info("InitializationRelayDevice - (InitializationRelayDevice) Init TCP/IP Rele [TypeRelayDevice=" +
                           CONFIG.TypeRelayDevice + "]");
                        if (ARDUINO_HELPER != null)
                        {
                            try
                            {
                                ARDUINO_HELPER = null;
                            }
                            catch { }

                        }
                        string comManual = string.IsNullOrEmpty(Properties.Settings.Default.ManualPort) ? null : 
                                            Properties.Settings.Default.ManualPort;
                        LOG.Info("InitializationRelayDevice - comManual = " + (string.IsNullOrEmpty(comManual) ? "NULL": comManual));
                        ARDUINO_HELPER = new ArduinoHelper(CONFIG.TimerRelay.ToString(), comManual);
                        if (ARDUINO_HELPER != null)
                        {
                            int iconx = ARDUINO_HELPER.Initialize();
                            if (iconx != 0)
                            {
                                LOG.Fatal("InitializationRelayDevice - Error ARDUINO_HELPER = " + iconx);
                            }
                            else
                            {
                                LOG.Info("InitializationRelayDevice - ARDUINO_HELPER Initialize OK");
                                ARDUINO_HELPER.CloseAll();
                            }
                        }
                        else
                        {
                            LOG.Fatal("InitializationRelayDevice - Error creando ArduinoHelper = null");
                        }
                        break;
                    case 4: //KMTronic 2022
                        WriteEventLog("Biometrika.ReleHandle.Web (InitializationRelayDevice) Init TCP/IP Rele [TypeRelayDevice=" +
                           CONFIG.TypeRelayDevice + "]", LOG_INFO);
                        LOG.Info("InitializationRelayDevice - (InitializationRelayDevice) Init TCP/IP Rele [TypeRelayDevice=" +
                           CONFIG.TypeRelayDevice + "]");
                        if (KMTRONIC_HELPER != null)
                        {
                            try
                            {
                                KMTRONIC_HELPER = null;
                            }
                            catch { }

                        }
                        string comManualKM = string.IsNullOrEmpty(Properties.Settings.Default.ManualPort) ? null :
                                            Properties.Settings.Default.ManualPort;
                        LOG.Info("InitializationRelayDevice - comManual = " + (string.IsNullOrEmpty(comManualKM) ? "NULL" : comManualKM));
                        KMTRONIC_HELPER = new KMTronicHelper(CONFIG.TimerRelay.ToString(), CONFIG.QRelay, comManualKM);
                        if (KMTRONIC_HELPER != null)
                        {
                            int iconx = KMTRONIC_HELPER.Initialize();
                            if (iconx != 0)
                            {
                                LOG.Fatal("InitializationRelayDevice - Error KMTRONIC_HELPER = " + iconx);
                            }
                            else
                            {
                                LOG.Info("InitializationRelayDevice - KMTRONIC_HELPER Initialize OK");
                                KMTRONIC_HELPER.CloseAll();
                                LOG.Info("InitializationRelayDevice - Info: " + KMTRONIC_HELPER.GetInfoStatus(1));
                                LOG.Info("InitializationRelayDevice - Status: " + KMTRONIC_HELPER.GetInfoStatus(2));
                            }
                        }
                        else
                        {
                            LOG.Fatal("InitializationRelayDevice - Error creando KMTRONIC_HELPER = null");
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                LOG.Fatal("Global.InitializationRelayDevice", ex);
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("Global.Application_End - Salvando log => TxLog.length = " + src.Tx.TxLog.TxRecords.Count.ToString());
                Biometrika.ReleHandle.Web.src.Tx.TxLog.SaveTxRecord();
                //if (OBJ_RELE_DEVICE != null)
                //{
                //    OBJ_RELE_DEVICE.Disconnect();
                //}
            }
            catch (Exception ex)
            {
                LOG.Error("Application_End", ex);
                throw;
            }
        }


        private static int LOG_INFO = 1;
        private static int LOG_WARN = 2;
        private static int LOG_ERROR = 3;

        /// <summary>
        /// level = 0 -> Debug
        /// level = 1 -> Info
        /// level = 2 -> Warn
        /// level = 3 -> Error
        /// </summary>
        /// <param name="message"></param>
        /// <param name="level"></param>
        internal static void WriteEventLog(string message, int level)
        {
            if (CONFIG == null || CONFIG.EnableEventViewer)
            {
                switch (level)
                {
                    case 0:
                        EventLog.WriteEntry("Biometrika.NEC.Validator", message, EventLogEntryType.SuccessAudit);
                        break;
                    case 1:
                        EventLog.WriteEntry("Biometrika.NEC.Validator", message, EventLogEntryType.Information);
                        break;
                    case 2:
                        EventLog.WriteEntry("Biometrika.NEC.Validator", message, EventLogEntryType.Warning);
                        break;
                    case 3:
                        EventLog.WriteEntry("Biometrika.NEC.Validator", message, EventLogEntryType.Error);
                        break;
                    default:
                        EventLog.WriteEntry("Biometrika.NEC.Validator", message, EventLogEntryType.Information);
                        break;
                }
            }
        }
    }
}