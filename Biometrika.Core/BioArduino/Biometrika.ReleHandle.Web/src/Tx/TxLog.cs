﻿using System;
using System.Collections.Generic;
using System.Web;
using Bio.Core.Utils;

namespace Biometrika.ReleHandle.Web.src.Tx
{
    public class TxLog
    {

        public static List<Tx> TxRecords = new List<Tx>();

        public static void AddTxRecord(string origin, string method, int relenumber, int operacion, int timeRelay, string moduleType, 
                                     string moduleId, string ipDevice, string portDevice)
        {
            Tx NewTx = new Tx();
            NewTx.Origin = origin;
            NewTx.Method = method;
            NewTx.ReleNumber = relenumber;
            NewTx.Operacion = operacion;
            NewTx.TimeRelay = timeRelay;
            NewTx.ModuleType = moduleType; 
            NewTx.ModuleId = moduleId; 
            NewTx.IpDevice = ipDevice;
            NewTx.PortDevice = portDevice;
            NewTx.Timestamp = DateTime.Now;
            TxRecords.Add(NewTx);

            if (TxRecords.Count == Global.CONFIG.QTxLogRecords)
            {
                if (SerializeHelper.SerializeToFile(TxRecords, Biometrika.ReleHandle.Web.Properties.Settings.Default.PathTxLog + "." +
                    DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml"))
                {
                    TxRecords = new List<Tx>();
                } 
            } else
            {
                //SerializeHelper.SerializeToFile(TxRecords, Properties.Settings.Default.PathTxLog);
            }
        }

        public static void LoadTxRecord()
        {
            try
            {
                TxRecords =
                    SerializeHelper.DeserializeFromFile<List<Tx>>(
                        Biometrika.ReleHandle.Web.Properties.Settings.Default.PathTxLog);
                if (TxRecords == null) TxRecords = new List<Tx>();
            }
            catch
            {
                TxRecords = new List<Tx>();
            }
        }

        public static void SaveTxRecord()
        {
            try
            {
                
                SerializeHelper.SerializeToFile(src.Tx.TxLog.TxRecords, Properties.Settings.Default.PathTxLog);
            }
            catch { }
        }

    }

    public class Tx
    {
        public DateTime Timestamp;
        public string Origin;
        public string Method;
        public int ReleNumber;
        public int Operacion;
        public int TimeRelay;
        public string ModuleType; 
        public string ModuleId;
        public string IpDevice;
        public string PortDevice;
    }
}