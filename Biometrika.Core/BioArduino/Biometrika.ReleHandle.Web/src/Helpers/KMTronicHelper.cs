﻿using BioArduinoRele;
using Biometrika.Kiosk.Common.Model;
using Biometrika.KMTronic.Api;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.ReleHandle.Web
{
    public class KMTronicHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(KMTronicHelper));

        bool _Initialized;
        KMTronicClient _KMTronicCLIENT;
        string _KMTronicDeviceId;
        string _KMTronicDelay;
        int _KMTronicQReles = 4;

        public string KMTronicPort()
        {
            return _KMTronicDeviceId;
        }

        public int KMTronicQReles()
        {
            return _KMTronicQReles;
        }

        public KMTronicHelper(string _delay, int qReles = 4, string portCom = null)
        {
            try
            {
                LOG.Debug("KMTronicHelper.Constructor IN...");
                _KMTronicDelay = _delay;
                _KMTronicDeviceId = portCom;
                _KMTronicQReles = qReles;
                LOG.Debug("KMTronicHelper.Constructor _KMTronicDelay = " + _KMTronicDelay.ToString());
                LOG.Debug("KMTronicHelper.Constructor _KMTronicDeviceId = " + 
                            (string.IsNullOrEmpty(_KMTronicDeviceId) ? "Null" : _KMTronicDeviceId));
                _Initialized = false;
            }
            catch (Exception ex)
            {
                _Initialized = false;
                LOG.Error("KMTronicHelper.Constructor Excp Error: " + ex.Message);
            }
        }

        public int Initialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("KMTronicHelper.Initialize IN...");

                LOG.Debug("KMTronicHelper.Initialize Creo objeto...");
                _KMTronicCLIENT = new KMTronicClient();
                // LOG.Debug("KMTronicHelper.Initialize - getCOM call...");
               // string comAssigned = KMTronicClient.getCOM();
               // LOG.Debug("KMTronicHelper.Initialize - Result getCOM => " + (string.IsNullOrEmpty(comAssigned) ? "Null" : comAssigned));

                //_ArduinoDeviceId = !string.IsNullOrEmpty(comAssigned) ? comAssigned : _ArduinoDeviceId;
                // LOG.Debug("KMTronicHelper.Initialize - _ArduinoDeviceId = " +
                //     (string.IsNullOrEmpty(_ArduinoDeviceId) ? "Null" : _ArduinoDeviceId));
                // _Initialized = !string.IsNullOrEmpty(comAssigned);  //Si devuelve valor => configuro ok

                if (!_Initialized && !string.IsNullOrEmpty(_KMTronicDeviceId)) //Pruebo con set manual si tengo confgurado el puerto manual
                {
                    LOG.Debug("KMTronicHelper.Initialize - Set Manual => " + _KMTronicDeviceId);
                    var response = _KMTronicCLIENT.SetManualCOM(_KMTronicDeviceId);
                    LOG.Debug("KMTronicHelper.Initialize - Manual => _ArduinoDeviceId = " +
                        (string.IsNullOrEmpty(_KMTronicDeviceId) ? "Null" : _KMTronicDeviceId));
                    _Initialized = !string.IsNullOrEmpty(response); // && response.Equals("OK");
                    _KMTronicCLIENT.SetQRele(_KMTronicQReles);
                }

                LOG.Debug("KMTronicHelper.Initialize -_Initialized = " + _Initialized.ToString());
                ret = _Initialized ? 0 : -2;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("KMTronicHelper.Initialize Error: " + ex.Message);
            }
            LOG.Debug("KMTronicHelper.Initialize OUT! => ret = " + ret.ToString());
            return ret;
        }

        public int AddItemQueue(string rele, string delay)
        {
            int ret = Errors.IERR_OK;
            string msgErr;
            try
            {
                LOG.Debug("KMTronicHelper.AddItemQueue IN...");

                LOG.Debug("KMTronicHelper.AddItemQueue - Call Open via AddItemToQueue method...");
                var resp = _KMTronicCLIENT.AddItemToQueue(Convert.ToInt32(rele), Convert.ToInt32(delay));
                if (!string.IsNullOrEmpty(resp) && resp.Equals("OK"))
                {
                    LOG.Debug("KMTronicHelper.AddItemQueue - Agrego Item a Queue rele = " + rele + "...");
                    ret = Errors.IERR_OK;
                }
                else
                {
                    LOG.Debug("KMTronicHelper.AddItemQueue - NO Agrego Item a Queue rele = " + rele + " [" + resp + "]");
                    ret = Errors.IERR_HANDLE_RELE;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("KMTronicHelper.AddItemQueue Excp Error: " + ex.Message);
            }
            LOG.Debug("KMTronicHelper.AddItemQueue OUT! => ret = " + ret.ToString());
            return ret;
        }


        public int OpenRele(string rele)
        {
            int ret = Errors.IERR_OK;
            string msgErr;
            try
            {
                LOG.Debug("KMTronicHelper.OpenRele IN...");

                LOG.Debug("KMTronicHelper.OpenRele - Call OpenClose method...");
                var resp = _KMTronicCLIENT.OpenClose(rele, "1", out msgErr);
                if (!string.IsNullOrEmpty(resp) && resp.Equals("OK"))
                {
                    LOG.Debug("KMTronicHelper.OpenRele - Abrio rele = " + rele + "...");
                    ret = Errors.IERR_OK;
                }
                else
                {
                    LOG.Debug("KMTronicHelper.OpenRele - NO Abrio rele = " + rele + " [" + msgErr + "]");
                    ret = Errors.IERR_HANDLE_RELE;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("KMTronicHelper.OpenRele Excp Error: " + ex.Message);
            }
            LOG.Debug("KMTronicHelper.OpenRele OUT! => ret = " + ret.ToString());
            return ret;
        }

        public int CloseRele(string rele)
        {
            int ret = Errors.IERR_OK;
            string msgErr;
            try
            {
                LOG.Debug("KMTronicHelper.CloseRele IN...");

                LOG.Debug("KMTronicHelper.CloseRele - Call openClose method...");
                var resp = _KMTronicCLIENT.OpenClose(rele, "0", out msgErr);
                if (!string.IsNullOrEmpty(resp) && resp.Equals("OK"))
                {
                    LOG.Debug("KMTronicHelper.CloseRele - Cerro rele = " + rele + "...");
                    ret = Errors.IERR_OK;
                }
                else
                {
                    LOG.Debug("KMTronicHelper.CloseRele - NO Cerro rele = " + rele + " [" + msgErr + "]");
                    ret = Errors.IERR_HANDLE_RELE;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("KMTronicHelper.CloseRele Excp Error: " + ex.Message);
            }
            LOG.Debug("KMTronicHelper.CloseRele OUT! => ret = " + ret.ToString());
            return ret;
        }

        public int OpenWithDelay(string rele, string delay)
        {
            int ret = Errors.IERR_OK;
            string msgErr;
            try
            {
                LOG.Debug("KMTronicHelper.OpenWithDelay IN...");

                LOG.Debug("KMTronicHelper.OpenWithDelay - Call OpenDelay method...");
                var resp = _KMTronicCLIENT.OpenDelay(rele, delay, out msgErr);
                if (!string.IsNullOrEmpty(resp) && resp.Equals("OK"))
                {
                    LOG.Debug("KMTronicHelper.CloseRele - Cerro rele = " + rele + "...");
                    ret = Errors.IERR_OK;
                }
                else
                {
                    LOG.Debug("KMTronicHelper.CloseRele - NO Cerro rele = " + rele + " [" + msgErr + "]");
                    ret = Errors.IERR_HANDLE_RELE;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("KMTronicHelper.OpenWithDelay Excp Error: " + ex.Message);
            }
            LOG.Debug("KMTronicHelper.OpenWithDelay OUT! => ret = " + ret.ToString());
            return ret;
        }

        public int OpenAllWithDelay(int qReles, string delay)
        {
            int ret = 0;
            string strRele;
            string msgErr;
            try
            {
                LOG.Debug("KMTronicHelper.OpenAllWithDelay IN...");

                LOG.Debug("KMTronicHelper.OpenAllWithDelay - Llamo a OpenCloseAllDelay...");
                var resp = _KMTronicCLIENT.OpenCloseAllDelay(delay, out msgErr);
                if (!string.IsNullOrEmpty(resp) && resp.Equals("OK"))
                {
                    LOG.Debug("KMTronicHelper.OpenAllWithDelay - Abrio reles por " + delay + " milisegundos...");
                    ret = ret + Errors.IERR_OK;
                }
                else
                {
                    LOG.Debug("KMTronicHelper.OpenAllWithDelay - NO Abrio reles [" + msgErr + "]");
                    ret = ret + Errors.IERR_HANDLE_RELE;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("KMTronicHelper.OpenAllWithDelay Error: " + ex.Message);
            }
            LOG.Debug("KMTronicHelper.OpenAllWithDelay OUT! => ret = " + ret.ToString());
            return ret;
        }

        public int CloseAll()
        {
            int res = 0;
            string strRele;
            string msgErr;
            try
            {
                LOG.Debug("KMTronicHelper.CloseAll IN...");

                LOG.Debug("KMTronicHelper.CloseAll - Calling OpenCloseAll...");
                var resp = _KMTronicCLIENT.OpenCloseAll(false, out msgErr);
                if (!string.IsNullOrEmpty(resp) && resp.Equals("OK"))
                {
                    LOG.Debug("KMTronicHelper.CloseAll - Cerro todos los reles...");
                    res = Errors.IERR_OK;
                }
                else
                {
                    LOG.Debug("KMTronicHelper.CloseAll - NO Abrio reles [" + resp + "]");
                    res = Errors.IERR_HANDLE_RELE;
                }
            }
            catch (Exception ex)
            {
                res = -1;
                LOG.Error("KMTronicHelper.CloseAll Error: " + ex.Message);
            }
            LOG.Debug("KMTronicHelper.CloseAll OUT!");
            return res;
        }

        public int OpenAll()
        {
            int res = 0;
            string strRele;
            string msgErr;
            try
            {
                LOG.Debug("KMTronicHelper.OpenAll IN...");

                LOG.Debug("KMTronicHelper.OpenAll - Calling OpenCloseAll...");
                var resp = _KMTronicCLIENT.OpenCloseAll(true, out msgErr);
                if (!string.IsNullOrEmpty(resp) && resp.Equals("OK"))
                {
                    LOG.Debug("KMTronicHelper.OpenAll - Cerro todos los reles...");
                    res = Errors.IERR_OK;
                }
                else
                {
                    LOG.Debug("KMTronicHelper.OpenAll - NO Abrio reles [" + resp + "]");
                    res = Errors.IERR_HANDLE_RELE;
                }
            }
            catch (Exception ex)
            {
                res = -1;
                LOG.Error("KMTronicHelper.OpenAll Error: " + ex.Message);
            }
            LOG.Debug("KMTronicHelper.OpenAll OUT!");
            return res;
        }

        public string GetInfoStatus(int type)  //1-Info | 2-Status
        {
            string respuesta;
            try
            {
                LOG.Debug("KMTronicHelper.GetInfoStatus IN...");

                LOG.Debug("KMTronicHelper.GetInfoStatus - Call OpenClose method...");
                string msgErr;
                respuesta = _KMTronicCLIENT.GetInfoStatus((type==1), out msgErr);
            }
            catch (Exception ex)
            {
                respuesta = "KMTronicHelper.GetInfoStatus Excp Error: " + ex.Message;
                LOG.Error(respuesta);
            }
            LOG.Debug("KMTronicHelper.GetInfoStatus OUT! respuesta = " + respuesta);
            return respuesta;
        }
    }
}
