﻿using BioArduinoRele;
using Biometrika.Kiosk.Common.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.ReleHandle.Web
{
    public class ArduinoHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ArduinoHelper));

        bool _Initialized;
        ArduinoRele _ARDUINO;
        string _ArduinoDeviceId;
        string _ArduinoDelay;

        public string ArduinoPort()
        {
            return _ArduinoDeviceId;
        }

        public ArduinoHelper(string _delay, string arduinoCom = null)
        {
            try
            {
                LOG.Debug("ArduinoHelper.Constructor IN...");
                _ArduinoDelay = _delay;
                _ArduinoDeviceId = arduinoCom;
                LOG.Debug("ArduinoHelper.Constructor _ArduinoDelay = " + _ArduinoDelay.ToString());
                LOG.Debug("ArduinoHelper.Constructor _ArduinoDeviceId = " + 
                            (string.IsNullOrEmpty(_ArduinoDeviceId) ? "Null" : _ArduinoDeviceId));
                _Initialized = false;
            }
            catch (Exception ex)
            {
                _Initialized = false;
                LOG.Error("ArduinoHelper.Constructor Excp Error: " + ex.Message);
            }
        }

        public int Initialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("ArduinoHelper.Initialize IN...");

                LOG.Debug("ArduinoHelper.Initialize Creo objeto...");
                _ARDUINO = new ArduinoRele();
                LOG.Debug("ArduinoHelper.Initialize - getCOM call...");
                string comAssigned = _ARDUINO.getCOM();
                LOG.Debug("ArduinoHelper.Initialize - Result getCOM => " + (string.IsNullOrEmpty(comAssigned) ? "Null" : comAssigned));

               _ArduinoDeviceId = !string.IsNullOrEmpty(comAssigned) ? comAssigned : _ArduinoDeviceId;
                LOG.Debug("ArduinoHelper.Initialize - _ArduinoDeviceId = " +
                    (string.IsNullOrEmpty(_ArduinoDeviceId) ? "Null" : _ArduinoDeviceId));
                _Initialized = !string.IsNullOrEmpty(comAssigned);  //Si devuelve valor => configuro ok

                if (!_Initialized && !string.IsNullOrEmpty(_ArduinoDeviceId)) //Pruebo con set manual si tengo confgurado el puerto manual
                {
                    LOG.Debug("ArduinoHelper.Initialize - Set Manual => " + _ArduinoDeviceId);
                    _ARDUINO.setManualCOM(_ArduinoDeviceId);
                    LOG.Debug("ArduinoHelper.Initialize - getCOM call con set manual en => " + _ArduinoDeviceId + "...");
                    _ArduinoDeviceId = _ARDUINO.getCOM(_ArduinoDeviceId);
                    LOG.Debug("ArduinoHelper.Initialize - Manual => _ArduinoDeviceId = " +
                        (string.IsNullOrEmpty(_ArduinoDeviceId) ? "Null" : _ArduinoDeviceId));
                    _Initialized = !string.IsNullOrEmpty(_ArduinoDeviceId);
                }

                LOG.Debug("ArduinoHelper.Initialize -_Initialized = " + _Initialized.ToString());
                ret = _Initialized ? 0 : -2;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ArduinoHelper.Initialize Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.Initialize OUT! => ret = " + ret.ToString());
            return ret;
        }

        public int OpenRele(string rele)
        {
            int ret = Errors.IERR_OK;
            string msgErr;
            try
            {
                LOG.Debug("ArduinoHelper.OpenRele IN...");

                LOG.Debug("ArduinoHelper.OpenRele - Call OpenClose method...");
                _ARDUINO.openClose(rele, "1", out msgErr);
                if (string.IsNullOrEmpty(msgErr))
                {
                    LOG.Debug("ArduinoHelper.OpenRele - Abrio rele = " + rele + "...");
                    ret = Errors.IERR_OK;
                }
                else
                {
                    LOG.Debug("ArduinoHelper.OpenRele - NO Abrio rele = " + rele + " [" + msgErr + "]");
                    ret = Errors.IERR_HANDLE_RELE;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("ArduinoHelper.OpenRele Excp Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.OpenRele OUT! => ret = " + ret.ToString());
            return ret;
        }

        public int CloseRele(string rele)
        {
            int ret = Errors.IERR_OK;
            string msgErr;
            try
            {
                LOG.Debug("ArduinoHelper.CloseRele IN...");

                LOG.Debug("ArduinoHelper.CloseRele - Call openClose method...");
                _ARDUINO.openClose(rele, "0", out msgErr);
                if (string.IsNullOrEmpty(msgErr))
                {
                    LOG.Debug("ArduinoHelper.CloseRele - Cerro rele = " + rele + "...");
                    ret = Errors.IERR_OK;
                }
                else
                {
                    LOG.Debug("ArduinoHelper.CloseRele - NO Cerro rele = " + rele + " [" + msgErr + "]");
                    ret = Errors.IERR_HANDLE_RELE;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("ArduinoHelper.CloseRele Excp Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.CloseRele OUT! => ret = " + ret.ToString());
            return ret;
        }

        public int OpenWithDelay(string rele, string delay)
        {
            int ret = Errors.IERR_OK;
            string msgErr;
            try
            {
                LOG.Debug("ArduinoHelper.OpenWithDelay IN...");

                LOG.Debug("ArduinoHelper.OpenWithDelay - Call OpenDelay method...");
                _ARDUINO.openDelay(rele, delay, out msgErr);
                if (string.IsNullOrEmpty(msgErr))
                {
                    LOG.Debug("ArduinoHelper.OpenWithDelay - Abrio rele = " + rele + " por " + delay + " milisegundos...");
                    ret = Errors.IERR_OK;
                } else
                {
                    LOG.Debug("ArduinoHelper.OpenWithDelay - NO Abrio rele = " + rele + " [" + msgErr + "]");
                    ret = Errors.IERR_HANDLE_RELE;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("ArduinoHelper.OpenWithDelay Excp Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.OpenWithDelay OUT! => ret = " + ret.ToString());
            return ret;
        }

        public int OpenAllWithDelay(int qReles, string delay)
        {
            int ret = 0;
            string strRele;
            string msgErr;
            try
            {
                LOG.Debug("ArduinoHelper.OpenAllWithDelay IN...");

                LOG.Debug("ArduinoHelper.OpenAllWithDelay Creo objeto...");
                //for (int i = 1; i <= qReles; i++)
                //{
                    msgErr = null;
                    //strRele = "0" + i.ToString() + "0";
                //_ARDUINO.openDelay(strRele, delay, out msgErr);
                _ARDUINO.openCloseAllDelay(delay, out msgErr);
                if (string.IsNullOrEmpty(msgErr))
                {
                    LOG.Debug("ArduinoHelper.OpenAllWithDelay - Abrio reles por " + delay + " milisegundos...");
                    ret = ret + Errors.IERR_OK;
                }
                else
                {
                    LOG.Debug("ArduinoHelper.OpenAllWithDelay - NO Abrio reles [" + msgErr + "]");
                    ret = ret + Errors.IERR_HANDLE_RELE;
                }
                //}
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ArduinoHelper.OpenAllWithDelay Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.OpenAllWithDelay OUT! => ret = " + ret.ToString());
            return ret;
        }

        public int CloseAll()
        {
            int res = 0;
            string strRele;
            string msgErr;
            try
            {
                LOG.Debug("ArduinoHelper.CloseAll IN...");

                LOG.Debug("ArduinoHelper.CloseAll Creo objeto...");
                //for (int i = 1; i <= qReles; i++)
                //{
                msgErr = null;
                //strRele = "0" + i.ToString() + "0";
                //_ARDUINO.openDelay(strRele, delay, out msgErr);
                _ARDUINO.openCloseAll(false, out msgErr);
                if (string.IsNullOrEmpty(msgErr))
                {
                    LOG.Debug("ArduinoHelper.CloseAll - Cerro todos los reles...");
                }
                else
                {
                    LOG.Debug("ArduinoHelper.CloseAll - NO Cerro todos los reles [" + msgErr + "]");
                }
                //}
            }
            catch (Exception ex)
            {
                res = -1;
                LOG.Error("ArduinoHelper.CloseAll Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.CloseAll OUT!");
            return res;
        }

        public int OpenAll()
        {
            int res = 0;
            string strRele;
            string msgErr;
            try
            {
                LOG.Debug("ArduinoHelper.OpenAll IN...");

                LOG.Debug("ArduinoHelper.OpenAll Creo objeto...");
                //for (int i = 1; i <= qReles; i++)
                //{
                msgErr = null;
                //strRele = "0" + i.ToString() + "0";
                //_ARDUINO.openDelay(strRele, delay, out msgErr);
                _ARDUINO.openCloseAll(true, out msgErr);
                if (string.IsNullOrEmpty(msgErr))
                {
                    LOG.Debug("ArduinoHelper.OpenAll - Abrio todos los reles...");
                }
                else
                {
                    LOG.Debug("ArduinoHelper.CloseOpenAllAll - NO Abrio todos los reles [" + msgErr + "]");
                }
                //}
            }
            catch (Exception ex)
            {
                res = -1;
                LOG.Error("ArduinoHelper.OpenAll Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.OpenAll OUT!");
            return res;
        }
    }
}
