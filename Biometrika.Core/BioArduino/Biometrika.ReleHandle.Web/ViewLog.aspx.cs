﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bio.Core.Utils;
using Biometrika.ReleHandle.Web.src.Tx;
using log4net;

namespace Biometrika.ReleHandle.Web
{
    public partial class ViewLog : System.Web.UI.Page
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ViewLog));

        protected void Page_Load(object sender, EventArgs e)
        {
            string tag = "<table border='1'>" +
                                 "<tr><td colspan='11'><strong><span>" +
                                 "<FONT SIZE='4' COLOR='#336600'>" +
                                 "Biometrika ReleHandle Web - LOG de Transacciones Actuales...</span></strong></FONT></td></tr>" +
                                 "<tr>";
            try
            {
                string path = Request.QueryString["file"];

                if (String.IsNullOrEmpty(path)) {
                    tag = tag +
                                 "<td bgcolor='#336600' align='center'><FONT COLOR='White'>#</FONT></td>" +
                                 "<td bgcolor='#336600' align='center'><FONT COLOR='White'>Timestamp</FONT></td>" +
                                 "<td bgcolor='#336600' align='center'><FONT COLOR='White'>Origin</FONT></td>" +
                                 "<td bgcolor='#336600' align='center'><FONT COLOR='White'>Method</FONT></td>" +
                                 "<td bgcolor='#336600' align='center'><FONT COLOR='White'>ReleNumber</FONT></td>" +
                                 "<td bgcolor='#336600' align='center'><FONT COLOR='White'>Operacion</FONT></td>" +
                                 "<td bgcolor='#336600' align='center'><FONT COLOR='White'>TimeRelay</FONT></td>" +
                                 "<td bgcolor='#336600' align='center'><FONT COLOR='White'>ModuleType</FONT></td>" +
                                 "<td bgcolor='#336600' align='center'><FONT COLOR='White'>ModuleId</FONT></td>" +
                                 "<td bgcolor='#336600' align='center'><FONT COLOR='White'>IpDevice</FONT></td>" +
                                 "<td bgcolor='#336600' align='center'><FONT COLOR='White'>PortDevice</FONT></td></tr>";
                    for (int i = 0; i < src.Tx.TxLog.TxRecords.Count; i++)
                    {
                        tag = tag + "<tr>" +
                              "<td>" + i.ToString() + "</td>" +
                              "<td>" + src.Tx.TxLog.TxRecords[i].Timestamp.ToString("dd/MM/yyyy HH:mm:ss") +
                              "</FONT></td>" +
                              "<td>" + src.Tx.TxLog.TxRecords[i].Origin + "</td>" +
                              "<td>" + src.Tx.TxLog.TxRecords[i].Method + "</td>" +
                              "<td>" + src.Tx.TxLog.TxRecords[i].ReleNumber + "</td>" +
                              "<td>" + src.Tx.TxLog.TxRecords[i].Operacion.ToString() + "</td>" +
                              "<td>" + src.Tx.TxLog.TxRecords[i].TimeRelay.ToString() + "</td>" +
                              "<td bgcolor='#EBEBEB'>" + src.Tx.TxLog.TxRecords[i].ModuleType + "</td>" +
                              "<td bgcolor='#EBEBEB'>" + src.Tx.TxLog.TxRecords[i].ModuleId.ToString() + "</td>" +
                              "<td bgcolor='#EBEBEB'>" + src.Tx.TxLog.TxRecords[i].IpDevice + "</td>" +
                              "<td bgcolor='#EBEBEB'>" + src.Tx.TxLog.TxRecords[i].PortDevice.ToString() + "</td></tr>";
                    }
                } else
                {
                    FileInfo fi = new FileInfo(Properties.Settings.Default.PathTxLog);
                    System.IO.DirectoryInfo dir = fi.Directory;
                    List<Tx> TxRecords = SerializeHelper.DeserializeFromFile<List<Tx>>(dir.FullName + "\\" + path);

                    tag = tag +
                                "<td bgcolor='#336600' align='center'><FONT COLOR='White'>#</FONT></td>" +
                                "<td bgcolor='#336600' align='center'><FONT COLOR='White'>Timestamp</FONT></td>" +
                                "<td bgcolor='#336600' align='center'><FONT COLOR='White'>Origin</FONT></td>" +
                                "<td bgcolor='#336600' align='center'><FONT COLOR='White'>Method</FONT></td>" +
                                "<td bgcolor='#336600' align='center'><FONT COLOR='White'>ReleNumber</FONT></td>" +
                                "<td bgcolor='#336600' align='center'><FONT COLOR='White'>Operacion</FONT></td>" +
                                "<td bgcolor='#336600' align='center'><FONT COLOR='White'>TimeRelay</FONT></td>" +
                                "<td bgcolor='#336600' align='center'><FONT COLOR='White'>ModuleType</FONT></td>" +
                                "<td bgcolor='#336600' align='center'><FONT COLOR='White'>ModuleId</FONT></td>" +
                                "<td bgcolor='#336600' align='center'><FONT COLOR='White'>IpDevice</FONT></td>" +
                                "<td bgcolor='#336600' align='center'><FONT COLOR='White'>PortDevice</FONT></td></tr>";
                    for (int i = 0; i < TxRecords.Count; i++)
                    {
                        tag = tag + "<tr>" +
                              "<td>" + i.ToString() + "</td>" +
                              "<td>" + TxRecords[i].Timestamp.ToString("dd/MM/yyyy HH:mm:ss") +
                              "</FONT></td>" +
                              "<td>" + TxRecords[i].Origin + "</td>" +
                              "<td>" + TxRecords[i].Method + "</td>" +
                              "<td>" + TxRecords[i].ReleNumber + "</td>" +
                              "<td>" + TxRecords[i].Operacion.ToString() + "</td>" +
                              "<td>" + TxRecords[i].TimeRelay.ToString() + "</td>" +
                              "<td bgcolor='#EBEBEB'>" + TxRecords[i].ModuleType + "</td>" +
                              "<td bgcolor='#EBEBEB'>" + TxRecords[i].ModuleId.ToString() + "</td>" +
                              "<td bgcolor='#EBEBEB'>" + TxRecords[i].IpDevice + "</td>" +
                              "<td bgcolor='#EBEBEB'>" + TxRecords[i].PortDevice.ToString() + "</td></tr>";
                    }
                    tag = tag + "<tr><td colspan='11'><a href=ViewLog.aspx>Log Actual</a>" +
                            "</td></tr>";
                }

                tag = tag + ArmaMasLogs();

                tag = tag + "</table>";
                Response.Write("<HR>" + tag);

            }
            catch (Exception ex)
            {
                LOG.Error("Default.btnViewLog_Click", ex);
            }
        }

        private string ArmaMasLogs()
        {
            string stag = "";
 
            try
            {
                stag = "<tr><td colspan='11'><p></p></td></tr>" +
                        "<tr><td colspan='11'>Logs Anteriores...</td></tr>";

                FileInfo fi = new FileInfo(Properties.Settings.Default.PathTxLog);
                System.IO.DirectoryInfo dir = fi.Directory;
                foreach (FileInfo f in dir.GetFiles("Biometrika.ReleHandle.Web.TxLog.xml.*.xml"))
                {
                    stag = stag + "<tr><td colspan='11'>" +
                            "<a href=ViewLog.aspx?file=" + f.Name + ">" + f.Name + "</a>" +
                            "</td></tr>";

                }                
            }
            catch (Exception ex)
            {
                LOG.Error("Default.ArmaMasLogs", ex);
            }
            return stag;

        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}