﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using BioArduinoRele;
using System.Diagnostics;
using Biometrika.KMTronic.Api;

namespace Relay_Test
{
    public partial class Relay_Test : Form
    {
        object _lock = new object();

        bool _DO_ACTION_IN_TIMER = false;

        ArduinoRele objarduino = new ArduinoRele();

        KMTronicClient _KMClLIENT = new KMTronicClient();

        bool intervalCheck = false;

        public Relay_Test()
        {
            InitializeComponent();
        }

        private void Relay_Test_Load(object sender, EventArgs e)
        {
            openAll.Enabled = false;
            closeAll.Enabled = false;
            getInfo.Enabled = false;
            getStatus.Enabled = false;
            intervalCheckBox.Enabled = false;
            openRele1.Enabled = false;
            closeRele1.Enabled = false;
            openRele2.Enabled = false;
            closeRele2.Enabled = false;
            openRele3.Enabled = false;
            closeRele3.Enabled = false;
            openRele4.Enabled = false;
            closeRele4.Enabled = false;
        }

        private void detectCom_Click(object sender, EventArgs e)
        {
            tituloCOM.Text = objarduino.getCOM(string.IsNullOrEmpty(txtManualCom.Text)?null: txtManualCom.Text);

            if (tituloCOM.Text != "")
            {
                openAll.Enabled = true;
                closeAll.Enabled = true;
                getInfo.Enabled = true;
                getStatus.Enabled = true;
                intervalCheckBox.Enabled = true;
                openRele1.Enabled = true;
                closeRele1.Enabled = true;
                openRele2.Enabled = true;
                closeRele2.Enabled = true;
                openRele3.Enabled = true;
                closeRele3.Enabled = true;
                openRele4.Enabled = true;
                closeRele4.Enabled = true;
            }
            else
            {
                MessageBox.Show("No se encuentra arduino, revise el log");
            }
        }

        private void openRele1_Click_1(object sender, EventArgs e)
        {
           
            string msgError = "";
            statusBox.Clear();
            if (intervalCheck == true)
            {
                string timeDelay = intervalBox.Text;
                if (timeDelay == "")
                {
                    MessageBox.Show("Debe ingresar tiempo de intervalo");
                }
                else
                {
                    ledRele1.Image = Properties.Resources.LedOn;
                    this.Refresh();
                    if (rbArduino.Checked) 
                        statusBox.AppendText(objarduino.openDelay("010", timeDelay, out msgError));
                    else
                        statusBox.AppendText(_KMClLIENT.OpenDelay("1", timeDelay, out msgError));
                    System.Threading.Thread.Sleep(Convert.ToInt32(timeDelay));
                    ledRele1.Image = Properties.Resources.LedOff;
                    this.Refresh();
                }
            }
            else if (intervalCheck == false)
            {
                string retorno = "";
                if (rbArduino.Checked)
                {
                    retorno = objarduino.openClose("01", "1", out msgError);
                    if (retorno.Contains("Rele: 1 Abierto"))
                    {
                        ledRele1.Image = Properties.Resources.LedOn;
                    }
                }
                else
                {
                    lock (_lock)
                    {
                        retorno = _KMClLIENT.OpenClose("1", "1", out msgError);
                        string timeDelay = intervalBox.Text;

                        if (retorno.Equals("OK"))
                        {
                            ledRele1.Image = Properties.Resources.LedOn;
                            this.Refresh();
                        }
                        System.Threading.Thread.Sleep(Convert.ToInt32("2000"));
                        retorno = _KMClLIENT.OpenClose("1", "0", out msgError);
                        if (retorno.Equals("OK"))
                        {
                            ledRele1.Image = Properties.Resources.LedOff;
                            this.Refresh();
                        }
                    }
                }
                statusBox.AppendText(retorno);
               
            }
        }

        private void closeRele1_Click(object sender, EventArgs e)
        {
            string msgError = "";
            string retorno = "";
            statusBox.Clear();
            if (rbArduino.Checked)
            {
                retorno = objarduino.openClose("01", "0", out msgError);
                if (retorno.Contains("Rele: 1 Cerrado"))
                {
                    ledRele1.Image = Properties.Resources.LedOff;
                }
            }
            else
            {
                retorno = _KMClLIENT.OpenClose("1", "0", out msgError);
                if (retorno.Equals("OK"))
                {
                    ledRele1.Image = Properties.Resources.LedOff;
                }
            } 
            statusBox.AppendText(retorno);
           
        }

        private void intervalCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (intervalCheckBox.Checked)
            {
                intervalBox.Enabled = true;
                closeRele1.Enabled = false;
                closeRele2.Enabled = false;
                closeRele3.Enabled = false;
                closeRele4.Enabled = false;
                closeAll.Enabled = false;
                intervalCheck = true;
            }
            else
            {
                intervalBox.Clear();
                intervalBox.Enabled = false;
                closeRele1.Enabled = true;
                closeRele2.Enabled = true;
                closeRele3.Enabled = true;
                closeRele4.Enabled = true;
                closeAll.Enabled = true;
                intervalCheck = false;
            }
        }

        private void openRele2_Click(object sender, EventArgs e)
        {
            string msgError = "";
            statusBox.Clear();
            if (intervalCheck == true)
            {
                string timeDelay = intervalBox.Text;
                if (timeDelay == "")
                {
                    MessageBox.Show("Debe ingresar tiempo de intervalo");
                }
                else
                {
                    ledRele2.Image = Properties.Resources.LedOn;
                    this.Refresh();
                    if (rbArduino.Checked)
                        statusBox.AppendText(objarduino.openDelay("020", timeDelay, out msgError));
                    else
                        statusBox.AppendText(_KMClLIENT.OpenDelay("2", timeDelay, out msgError));
                    System.Threading.Thread.Sleep(Convert.ToInt32(timeDelay));
                    ledRele2.Image = Properties.Resources.LedOff;
                    this.Refresh();
                }
            }
            else if (intervalCheck == false)
            {
                if (rbArduino.Checked)
                {
                    string retorno = objarduino.openClose("02", "1", out msgError);
                    statusBox.AppendText(retorno);
                    if (retorno.Contains("Rele: 2 Abierto"))
                    {
                        ledRele2.Image = Properties.Resources.LedOn;
                    }
                }
                else
                {
                    lock (_lock)
                    {
                        string retorno = _KMClLIENT.OpenClose("2", "1", out msgError);
                        string timeDelay = intervalBox.Text;

                        if (retorno.Equals("OK"))
                        {
                            ledRele2.Image = Properties.Resources.LedOn;
                            this.Refresh();
                        }
                        System.Threading.Thread.Sleep(Convert.ToInt32("2000"));
                        retorno = _KMClLIENT.OpenClose("2", "0", out msgError);
                        if (retorno.Equals("OK"))
                        {
                            ledRele2.Image = Properties.Resources.LedOff;
                            this.Refresh();
                        }
                    }
                }
            }
        }

        private void closeRele2_Click(object sender, EventArgs e)
        {
            string msgError = "";
            statusBox.Clear();
            string retorno;
            if (rbArduino.Checked)
            {
                retorno = objarduino.openClose("02", "0", out msgError);
                if (retorno.Contains("Rele: 2 Cerrado"))
                {
                    ledRele1.Image = Properties.Resources.LedOff;
                }
            }
            else
            {
                retorno = _KMClLIENT.OpenClose("2", "0", out msgError);
                if (retorno.Equals("OK"))
                {
                    ledRele1.Image = Properties.Resources.LedOff;
                }
            }
            statusBox.AppendText(retorno);
        }

        private void openRele3_Click(object sender, EventArgs e)
        {
            string msgError = "";
            statusBox.Clear();
            if (intervalCheck == true)
            {
                string timeDelay = intervalBox.Text;
                if (timeDelay == "")
                {
                    MessageBox.Show("Debe ingresar tiempo de intervalo");
                }
                else
                {
                    ledRele3.Image = Properties.Resources.LedOn;
                    this.Refresh();
                    if (rbArduino.Checked)
                        statusBox.AppendText(objarduino.openDelay("030", timeDelay, out msgError));
                    else
                        statusBox.AppendText(_KMClLIENT.OpenDelay("3", timeDelay, out msgError));

                    System.Threading.Thread.Sleep(Convert.ToInt32(timeDelay));
                    ledRele3.Image = Properties.Resources.LedOff;
                    this.Refresh();
                }
            }
            else if (intervalCheck == false)
            {
                string retorno = objarduino.openClose("03", "1", out msgError);
                statusBox.AppendText(retorno);
                if (retorno.Contains("Rele: 3 Abierto"))
                {
                    ledRele3.Image = Properties.Resources.LedOn;
                }
            }
        }

        private void closeRele3_Click(object sender, EventArgs e)
        {
            string msgError = "";
            statusBox.Clear();
            string retorno;
            if (rbArduino.Checked)
            {
                retorno = objarduino.openClose("03", "0", out msgError);
                if (retorno.Contains("Rele: 3 Cerrado"))
                {
                    ledRele1.Image = Properties.Resources.LedOff;
                }
            }
            else
            {
                retorno = _KMClLIENT.OpenClose("3", "0", out msgError);
                if (retorno.Equals("OK"))
                {
                    ledRele1.Image = Properties.Resources.LedOff;
                }
            }
        }

        private void openRele4_Click(object sender, EventArgs e)
        {
            string msgError = "";
            statusBox.Clear();
            if (intervalCheck == true)
            {
                string timeDelay = intervalBox.Text;
                if (timeDelay == "")
                {
                    MessageBox.Show("Debe ingresar tiempo de intervalo");
                }
                else
                {
                    ledRele4.Image = Properties.Resources.LedOn;
                    this.Refresh();
                    if (rbArduino.Checked)
                        statusBox.AppendText(objarduino.openDelay("040", timeDelay, out msgError));
                    else
                        statusBox.AppendText(_KMClLIENT.OpenDelay("4", timeDelay, out msgError));

                    System.Threading.Thread.Sleep(Convert.ToInt32(timeDelay));
                    ledRele4.Image = Properties.Resources.LedOff;
                    this.Refresh();
                }
            }
            else if (intervalCheck == false)
            {
                string retorno = objarduino.openClose("04", "1", out msgError);
                statusBox.AppendText(retorno);
                if (retorno.Contains("Rele: 4 Abierto"))
                {
                    ledRele4.Image = Properties.Resources.LedOn;
                }
            }
        }

        private void closeRele4_Click(object sender, EventArgs e)
        {
            string msgError = "";
            statusBox.Clear();
            string retorno;
            if (rbArduino.Checked)
            {
                retorno = objarduino.openClose("04", "0", out msgError);
                if (retorno.Contains("Rele: 4 Cerrado"))
                {
                    ledRele1.Image = Properties.Resources.LedOff;
                }
            }
            else
            {
                retorno = _KMClLIENT.OpenClose("4", "0", out msgError);
                if (retorno.Equals("OK"))
                {
                    ledRele1.Image = Properties.Resources.LedOff;
                }
            }
        }

        private void openAll_Click(object sender, EventArgs e)
        {
            string msgError = "";
            statusBox.Clear();
            if (intervalCheck == true)
            {
                string timeDelay = intervalBox.Text;
                if (timeDelay == "")
                {
                    MessageBox.Show("Debe ingresar tiempo de intervalo");
                }
                else
                {
                    ledRele1.Image = Properties.Resources.LedOn;
                    ledRele2.Image = Properties.Resources.LedOn;
                    ledRele3.Image = Properties.Resources.LedOn;
                    ledRele4.Image = Properties.Resources.LedOn;
                    this.Refresh();
                    if (rbArduino.Checked)
                    {
                        statusBox.AppendText(objarduino.openCloseAllDelay(timeDelay, out msgError));
                        _DO_ACTION_IN_TIMER = true;
                    } else
                    {
                        statusBox.AppendText(_KMClLIENT.OpenCloseAllDelay(timeDelay, out msgError));
                    }
                    
                    //System.Threading.Thread.Sleep(Convert.ToInt32(timeDelay));
                    ledRele1.Image = Properties.Resources.LedOff;
                    ledRele2.Image = Properties.Resources.LedOff;
                    ledRele3.Image = Properties.Resources.LedOff;
                    ledRele4.Image = Properties.Resources.LedOff;
                    this.Refresh();

                }

            }
            else
            {
                if (rbArduino.Checked)
                {
                    statusBox.AppendText(objarduino.openCloseAll(true, out msgError));
                }
                else
                {
                    statusBox.AppendText(_KMClLIENT.OpenCloseAll(true, out msgError));
                }
                
                ledRele1.Image = Properties.Resources.LedOn;
                ledRele2.Image = Properties.Resources.LedOn;
                ledRele3.Image = Properties.Resources.LedOn;
                ledRele4.Image = Properties.Resources.LedOn;
            }
        }

        private void closeAll_Click(object sender, EventArgs e)
        {
            string msgError = "";
            statusBox.Clear();
            
            if (rbArduino.Checked)
            {
                statusBox.AppendText(objarduino.openCloseAll(false, out msgError));
            }
            else
            {
                statusBox.AppendText(_KMClLIENT.OpenCloseAll(false, out msgError));
            }
            ledRele1.Image = Properties.Resources.LedOff;
            ledRele2.Image = Properties.Resources.LedOff;
            ledRele3.Image = Properties.Resources.LedOff;
            ledRele4.Image = Properties.Resources.LedOff;
        }

        private void getInfo_Click(object sender, EventArgs e)
        {
            string msgError = "";
            statusBox.Clear();
            if (rbArduino.Checked) statusBox.AppendText(objarduino.getInfoStatus(true, out msgError));
            else statusBox.AppendText(_KMClLIENT.GetInfoStatus(true, out msgError));
        }

        private void getStatus_Click(object sender, EventArgs e)
        {
            string msgError = "";
            statusBox.Clear();
            if (rbArduino.Checked)
            {
                statusBox.AppendText(objarduino.getInfoStatus(false, out msgError));
            } else {
                statusBox.AppendText(_KMClLIENT.GetInfoStatus(false, out msgError));
            }
            
        }

        private void intervalBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void cleanLog_Click(object sender, EventArgs e)
        {
            statusBox.Clear();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (_DO_ACTION_IN_TIMER)
            {
                string msgError;
                string resultado = objarduino.getInfoStatus(false, out msgError);
                statusBox.AppendText(resultado);
                if (resultado.Contains("Cerrado"))
                {
                    ledRele1.Image = Properties.Resources.LedOff;
                    ledRele2.Image = Properties.Resources.LedOff;
                    ledRele3.Image = Properties.Resources.LedOff;
                    ledRele4.Image = Properties.Resources.LedOff;
                    _DO_ACTION_IN_TIMER = false;
                }
            }
        }

        private void btnSetManual_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtManualCom.Text))
            {
                if (rbArduino.Checked) objarduino.setManualCOM(txtManualCom.Text);
                else
                {
                    string retorno = _KMClLIENT.SetManualCOM(txtManualCom.Text);
                    if (retorno.Equals(txtManualCom.Text)) {
                        openAll.Enabled = true;
                        closeAll.Enabled = true;
                        getInfo.Enabled = true;
                        getStatus.Enabled = true;
                        intervalCheckBox.Enabled = true;
                        openRele1.Enabled = true;
                        closeRele1.Enabled = true;
                        openRele2.Enabled = true;
                        closeRele2.Enabled = true;
                        openRele3.Enabled = true;
                        closeRele3.Enabled = true;
                        openRele4.Enabled = true;
                        closeRele4.Enabled = true;
                    }
                }
            }

        }

        private void rbArduino_CheckedChanged(object sender, EventArgs e)
        {
            detectCom.Enabled = rbArduino.Checked;
            btnCOMs.Visible = rbKMTronic.Checked;
        }

        private void rbKMTronic_CheckedChanged(object sender, EventArgs e)
        {
            detectCom.Enabled = rbArduino.Checked;
            btnCOMs.Visible = rbKMTronic.Checked;
        }

        private void btnCOMs_Click(object sender, EventArgs e)
        {
            string[] coms = _KMClLIENT.GetComs();
            if (coms != null && coms.Length > 0)
            {
                listComs.Items.Clear();
                listComs.Items.AddRange(coms);
                listComs.Visible = true;
                this.Refresh();
            }
        }

        private void listComs_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtManualCom.Text = listComs.Items[listComs.SelectedIndex].ToString();
            listComs.Visible = false;
        }

        private void btnAddQueue_Click(object sender, EventArgs e)
        {
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(2, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(1, 6000);
            _KMClLIENT.AddItemToQueue(2, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(2, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(1, 6000);
            _KMClLIENT.AddItemToQueue(2, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(2, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(1, 6000);
            _KMClLIENT.AddItemToQueue(2, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(2, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(1, 6000);
            _KMClLIENT.AddItemToQueue(2, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(2, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);
            _KMClLIENT.AddItemToQueue(1, 6000);
            _KMClLIENT.AddItemToQueue(2, 2000);
            _KMClLIENT.AddItemToQueue(1, 2000);

        }
    }
}
