﻿// Libreria encargada de manejar el control de relés Arduino
// Se establecen funciones para:

// - Detectar puertos COM utilizados. getCOM()
// - Abrir o cerrar relés por separado. openClose(numero de relé para abrir o cerrar, estado true=abrir, false=cerrar)
// - Abrir o cerrar todos los relés. openCloseAll(true=abrir, false=cerrar)
// - Solicitar informacion a la placa. getInfoStatus(true=status, false=informacion de hardware)

// Desarrollado por Matias Arevalo
// Para Biometrika

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO.Ports;
using System.Management; 
using log4net;
using System.Collections;

namespace BioArduinoRele
{
	public class ArduinoRele
	{
        //Comentario para test git en bitbucket con VS2022 (Now yes)
		private static readonly ILog log = LogManager.GetLogger(typeof(ArduinoRele));
        private string _MANUAL_COMPORT = null; //"COM8"; 
		SerialPort ComPort = new SerialPort();

		/// <summary>
		/// Analiza los puertos COM informados para buscar el que sea de Arduino
		/// </summary>
		/// <returns>COM utilizado por la placa</returns>
		public string getCOM(string comAssigned = null)
		{
            string deviceId = null;

            log.Debug("getCOM IN - Analaizando puertos");
            Hashtable _HTDevices = null;
            try
            {
                _MANUAL_COMPORT = comAssigned;
                //Recupero lista de COMs que provengan desde USB Adapter Converter
                log.Debug("getCOM - Entra a GetCOMFromUSBAdapterConverter...");
                deviceId = GetCOMFromUSBAdapterConverter(); // (_HTDevices, out _HTDevices);

                if (string.IsNullOrEmpty(deviceId))
                {
                    log.Debug("getCOM - Entra a GetCOMFromSerialAdapter xq no encontro Arduino...");
                    deviceId = GetCOMFromSerialAdapter(); // (_HTDevices, out _HTDevices);
                }

                if (string.IsNullOrEmpty(deviceId) && !string.IsNullOrEmpty(_MANUAL_COMPORT))
                {
                    deviceId = setManualCOM(_MANUAL_COMPORT);
                    log.Debug("getCOM - Entra a SetManaul COM => " + 
                        (string.IsNullOrEmpty(_MANUAL_COMPORT)?"NULL":_MANUAL_COMPORT));
                }

                #region DescarteOLD
                //ManagementScope connectionScope = new ManagementScope();
                //SelectQuery serialQuery = new SelectQuery("SELECT * FROM Win32_SerialPort");
                //ManagementObjectSearcher searcher = new ManagementObjectSearcher(connectionScope, serialQuery);

                //try
                //{
                //    foreach (ManagementObject item in searcher.Get())
                //    {
                //        string desc = item["Description"].ToString();
                //        string deviceId = item["DeviceID"].ToString();
                //        log.Debug(desc + ": " + deviceId);

                //        if (desc.Contains("Arduino"))
                //        {
                //            ComPort.PortName = deviceId;
                //            ComPort.Open();
                //            ComPort.Write("99");
                //            System.Threading.Thread.Sleep(100);
                //            string message = ComPort.ReadExisting();
                //            log.Debug(message);
                //            log.Debug("Formateamos la respuesta para hacerla generica");

                //            string msgFormateado = message.Replace(Environment.NewLine, "");
                //            log.Debug("Mensaje formateado: " + msgFormateado);

                //            if (msgFormateado.CompareTo("Arduino UNO") == 0 || msgFormateado.CompareTo("Arduino RedBoard") == 0)
                //            {
                //                log.Debug("COM seleccionada: " + deviceId);
                //                ComPort.Close();
                //                return deviceId;
                //            }
                //            else
                //            {
                //                log.Debug("No se encuentra arduino");
                //                log.Debug("Si arduino esta conectado y aparece en el administrador de dispositivos puede que este bugeado el puerto, para solucioanr desconecte arduino por 10 segundos o reinicie el pc donde esta conectado");
                //                ComPort.Close();
                //                return null;
                //            }
                //        }
                //    }
                #endregion DescarteOLD
            }
            catch (ManagementException e)
            {
                log.Error(e.Message);
                log.Error("Si arduino esta conectado y aparece en el administrador de dispositivos puede que este bugeado el puerto, para solucioanr desconecte arduino por 10 segundos o reinicie el pc donde esta conectado");
                return null;
            }

			return deviceId;
		}

        public string setManualCOM(string comport)
        {
            _MANUAL_COMPORT = comport;
            log.Debug("setManualCOM - Seteado COM Manual => " + _MANUAL_COMPORT);
            try
            {
                ComPort.PortName = _MANUAL_COMPORT;
                ComPort.Open();
                ComPort.Write("99");
                System.Threading.Thread.Sleep(100);
                string message = ComPort.ReadExisting();
                log.Debug("ArduinoRele.GetCOMFromSerialAdapter - " + message);
                log.Debug("ArduinoRele.GetCOMFromSerialAdapter - Formateamos la respuesta para hacerla generica...");

                string msgFormateado = message.Replace(Environment.NewLine, "");
                log.Debug("ArduinoRele.GetCOMFromSerialAdapter - Mensaje formateado: " + msgFormateado);
                ComPort.Close();
            }
            catch (Exception ex)
            {
                _MANUAL_COMPORT = null;
                log.Error("setManualCOM Excp - " + ex.Message);
            }
            return _MANUAL_COMPORT;
        }

        /// <summary>
        /// Funcion de apertura, recive Rele a accionar y la accion a ejecutar(Abrir, cerrar)
        /// </summary>
        /// <param name="rele">Numero del rele a operar, ej: 01 para el rele 1 o 02 para el rele 2</param>
        /// <param name="accion">Accion a ejecutar, ej: 1 abrir, 0 cerrar</param>
        /// <param name="msgError">Msg de vuelta en el caso de errores</param>
        /// <returns>Retorna el msg entregado por la placa</returns>
        public string openClose(string rele, string accion, out string msgError)
		{
			msgError = "";
			string resultado = "";

			try
			{
				log.Debug("Intentamos abrir el puerto: " + ComPort.PortName);
				ComPort.Open();
				log.Debug("Puerto abierto, enviamos: " + rele + accion);
				ComPort.Write(rele + accion);
				System.Threading.Thread.Sleep(100);

				resultado = ComPort.ReadExisting();
				ComPort.Close();
				log.Debug("Escuchamos: " + resultado);
			}
			catch (Exception ex)
			{
				resultado = ex.Message;
				log.Error(ex.Message);
			}

			return resultado;
		}

		/// <summary>
		/// Funcion de apertura con timer, se le entrega el valor del rele y el tiempo que este estara abierto
		/// </summary>
		/// <param name="rele">Rele a operar, ej: 010 rele 1, 020 rele 2</param>
		/// <param name="timer">Tiempo de apertura en milisegundo</param>
		/// <param name="msgError">Msg de error si lo hubiera</param>
		/// <returns>Retorna msg de la placa arduino</returns>
		public string openDelay(string rele, string timer, out string msgError)
		{
			string resultado = "";
			msgError = "";

			try
			{
				log.Debug("Intentamos abrir el puerto: " + ComPort.PortName);
				ComPort.Open();
				log.Debug("Puerto abierto, enviamos pulso al rele: " + rele + ", tiempo: " + timer);
				ComPort.Write(rele + timer);
				System.Threading.Thread.Sleep(100);

				resultado = ComPort.ReadExisting();
				ComPort.Close();
				log.Debug("Escuchamos: " + resultado);
			}
			catch (Exception ex)
			{
				resultado = ex.Message;
				log.Error(ex.Message);
			}

			return resultado;
		}

		/// <summary>
		/// Funcion para abrir o cerrar todos los reles al mismo tiempo
		/// </summary>
		/// <param name="accion">Accion a realizar, ej: true para brir, false para cerrar</param>
		/// <param name="msgError">Msg de error si lo hubiera</param>
		/// <returns>Retorna msg de la placa arduino</returns>
		public string openCloseAll(bool accion, out string msgError)
		{
			string resultado = "";
			msgError = "";
			string accionAux = "";
			string que = "";

			if (accion)
			{
				que = "Abrir";
				accionAux = "981";
			}
			else
			{
				que = "Cerrar";
				accionAux = "980";
			}

			try
			{
				log.Debug("Intentamos abrir el puerto: " + ComPort.PortName);
				ComPort.Open();
				log.Debug("Puerto abierto, enviamos " + que + " todo: " + accionAux);
				ComPort.Write(accionAux);
				System.Threading.Thread.Sleep(100);

				resultado = ComPort.ReadExisting();
				ComPort.Close();
				log.Debug("Escuchamos: " + resultado);
			}
			catch (Exception ex)
			{
				resultado = ex.Message;
				log.Error(ex.Message);
			}

			return resultado;
		}

		/// <summary>
		/// Funcion que entrega la informacion del hardware de la placa o su estado
		/// </summary>
		/// <param name="accion">Accion a realizar, ej: true para informacion, false para status</param>
		/// <param name="msgError">Msg de error si lo hubiera</param>
		/// <returns>Retorna msg de la placa arduino</returns>
		public string getInfoStatus(bool accion, out string msgError)
		{
			string resultado = "";
			msgError = "";
			string accionAux = "";
			string que = "";
			int sleepTime = 0;

			if (accion)
			{
				que = "Info";
				accionAux = "86";
				sleepTime = 1000;
			}
			else
			{
				que = "Status";
				accionAux = "87";
				sleepTime = 100;
			}

			try 
			{
				log.Debug("Intentamos abrir el puerto: " + ComPort.PortName);
				ComPort.Open();
				log.Debug("Puerto abierto, enviamos " + que + ": " + accionAux);
				ComPort.Write(accionAux);
				System.Threading.Thread.Sleep(sleepTime);

				resultado = ComPort.ReadExisting();
				ComPort.Close();
				log.Debug("Escuchamos: " + resultado);
			}
			catch (Exception ex)
			{
				resultado = ex.Message;
				log.Error(ex.Message);
			}

			return resultado;
		}

        public string openCloseAllDelay(string timer, out string msgError)
        {
            string resultado = "";
            msgError = "";

            try
            {
                log.Debug("Intentamos abrir el puerto: " + ComPort.PortName);
                ComPort.Open();
                log.Debug("Puerto abierto, enviamos pulso a todos los rele, tiempo: " + timer);
                ComPort.Write("851" + timer);
                System.Threading.Thread.Sleep(100);

                resultado = ComPort.ReadExisting();
                ComPort.Close();
                log.Debug("Escuchamos: " + resultado);
            }
            catch (Exception ex)
            {
                resultado = ex.Message;
                log.Error(ex.Message);
            }

            return resultado;
        }

        #region Private 

        private string GetCOMFromSerialAdapter() // (Hashtable inhTDevices, out Hashtable outhTDevices)
        {
            string deviceId = null;
            //outhTDevices = null;
            //Hashtable _HT_DEVICES;
            try
            {
                log.Debug("ArduinoRele.GetCOMFromSerialAdapter IN...");

                //if (inhTDevices != null)
                //{
                //    _HT_DEVICES = inhTDevices;
                //} else
                //{
                //    _HT_DEVICES = new Hashtable();
                //}

                log.Debug("ArduinoRele.GetCOMFromSerialAdapter - Select Serial Devices...");
                ManagementScope connectionScope = new ManagementScope();
                SelectQuery serialQuery = new SelectQuery("SELECT * FROM Win32_SerialPort");
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(connectionScope, serialQuery);

                log.Debug("ArduinoRele.GetCOMFromSerialAdapter - Recorro lista si hay...");
                var l = searcher.Get();
                foreach (ManagementObject item in searcher.Get())
                {
                    string desc = item["Description"].ToString();
                    deviceId = item["DeviceID"].ToString();
                    log.Debug("ArduinoRele.GetCOMFromSerialAdapter - Leido => " +  desc + ": " + deviceId);

                    if (desc.Contains("Arduino") ||  desc.Contains("CH340") ||
                        (!string.IsNullOrEmpty(_MANUAL_COMPORT) && _MANUAL_COMPORT.Equals(deviceId)))
                    {
                        log.Debug("ArduinoRele.GetCOMFromSerialAdapter - Ingreso a procesar...");
                        ComPort.PortName = deviceId;
                        ComPort.Open();
                        ComPort.Write("99");
                        System.Threading.Thread.Sleep(100);
                        string message = ComPort.ReadExisting();
                        log.Debug("ArduinoRele.GetCOMFromSerialAdapter - " + message);
                        log.Debug("ArduinoRele.GetCOMFromSerialAdapter - Formateamos la respuesta para hacerla generica...");

                        string msgFormateado = message.Replace(Environment.NewLine, "");
                        log.Debug("ArduinoRele.GetCOMFromSerialAdapter - Mensaje formateado: " + msgFormateado);

                        if (msgFormateado.CompareTo("Arduino UNO") == 0 || msgFormateado.CompareTo("Arduino RedBoard") == 0)
                        {
                            log.Debug("ArduinoRele.GetCOMFromSerialAdapter - COM seleccionada: " + deviceId);
                            ComPort.Close();
                            break;
                            //log.Debug("ArduinoRele.GetCOMFromSerialAdapter - Agrego a lista [len=" + _HT_DEVICES.Count.ToString() + "...");
                            //if (!_HT_DEVICES.ContainsKey(deviceId))
                            //{
                            //    _HT_DEVICES.Add(deviceId, deviceId);
                            //} else
                            //{
                            //    log.Warn("ArduinoRele.GetCOMFromSerialAdapter - Ya existe " + deviceId);
                            //}
                            //log.Debug("ArduinoRele.GetCOMFromSerialAdapter - Agrego a lista [len=" + _HT_DEVICES.Count.ToString() + "...");
                        }
                        else
                        {
                            log.Debug("ArduinoRele.GetCOMFromSerialAdapter - No parece ser Arduino [" + msgFormateado + "]");
                            log.Debug("Si arduino esta conectado y aparece en el administrador de dispositivos puede que este bugeado el puerto, para solucioanr desconecte arduino por 10 segundos o reinicie el pc donde esta conectado");
                            ComPort.Close();
                            deviceId = null;
                        }
                    } else
                    {
                        log.Debug("ArduinoRele.GetCOMFromSerialAdapter - El dospositivo: " + desc + " NO es arduino => Se descarta!");
                        deviceId = null;
                    }
                }
                return deviceId;
            }
            catch (Exception ex)
            {
                log.Error("ArduinoRele.GetCOMFromSerialAdapter - Error: " + ex.Message);
                return null;
            }
        }

        private string GetCOMFromUSBAdapterConverter() //(Hashtable inhTDevices, out Hashtable outhTDevices)
        {
            string deviceId = null;
            //outhTDevices = null;
            //Hashtable _HT_DEVICES;
            try
            {
                log.Debug("ArduinoRele.GetCOMFromUSBAdapterConverter IN...");

                //if (inhTDevices != null)
                //{
                //    _HT_DEVICES = inhTDevices;
                //}
                //else
                //{
                //    _HT_DEVICES = new Hashtable();
                //}

                log.Debug("ArduinoRele.GetCOMFromUSBAdapterConverter - Select USB Adapter Serial Devices...");
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\WMI",
                                                                                 "SELECT * FROM MSSerial_PortName");

                log.Debug("ArduinoRele.GetCOMFromUSBAdapterConverter - Recorro lista si hay...");
                var l = searcher.Get();
                foreach (ManagementObject item in searcher.Get())
                {
                    string desc = item["InstanceName"].ToString();
                    deviceId = item["PortName"].ToString();
                    log.Debug("ArduinoRele.GetCOMFromUSBAdapterConverter - Leido => desc=" + desc + ": deviceId=" + deviceId);

                    if (desc.Contains("USB") ||
                        desc.Contains("CH340") ||
                        (!string.IsNullOrEmpty(_MANUAL_COMPORT) && _MANUAL_COMPORT.Equals(deviceId)))  
                    {
                        log.Debug("ArduinoRele.GetCOMFromUSBAdapterConverter - Ingreso a procesar xq es USB...");
                        ComPort.PortName = deviceId;
                        ComPort.Open();
                        ComPort.Write("99");
                        System.Threading.Thread.Sleep(100);
                        string message = ComPort.ReadExisting();
                        log.Debug("ArduinoRele.GetCOMFromUSBAdapterConverter - ComPort.ReadExisting ret => " + message);
                        log.Debug("ArduinoRele.GetCOMFromUSBAdapterConverter - Formateamos la respuesta para hacerla generica...");

                        string msgFormateado = message.Replace(Environment.NewLine, "");
                        log.Debug("ArduinoRele.GetCOMFromUSBAdapterConverter - Mensaje formateado: " + msgFormateado);

                        if (msgFormateado.CompareTo("Arduino UNO") == 0 || msgFormateado.CompareTo("Arduino RedBoard") == 0
                            || msgFormateado.CompareTo("Biometrika Arduino") == 0)
                        {
                            log.Debug("ArduinoRele.GetCOMFromUSBAdapterConverter - Encontro un Arduino => COM seleccionada: " + deviceId);
                            ComPort.Close();
                            break;
                            //log.Debug("ArduinoRele.GetCOMFromUSBAdapterConverter - Agrego a lista [len=" + _HT_DEVICES.Count.ToString() + "...");
                            //if (!_HT_DEVICES.ContainsKey(deviceId))
                            //{
                            //    _HT_DEVICES.Add(deviceId, deviceId);
                            //}
                            //else
                            //{
                            //    log.Warn("ArduinoRele.GetCOMFromUSBAdapterConverter - Ya existe " + deviceId);
                            //}
                            //log.Debug("ArduinoRele.GetCOMFromUSBAdapterConverter - Agrego a lista [len=" + _HT_DEVICES.Count.ToString() + "...");
                        }
                        else
                        {
                            log.Debug("ArduinoRele.GetCOMFromUSBAdapterConverter - No parece ser Arduino [" + msgFormateado + "]");
                            log.Debug("Si arduino esta conectado y aparece en el administrador de dispositivos puede que este bugeado el puerto, para solucioanr desconecte arduino por 10 segundos o reinicie el pc donde esta conectado");
                            ComPort.Close();
                            deviceId = null;
                        }
                    }
                    else
                    {
                        log.Debug("ArduinoRele.GetCOMFromUSBAdapterConverter - El dospositivo: " + desc + " NO es arduino => Se descarta!");
                        deviceId = null;
                    }
                }
                return deviceId;
            }
            catch (Exception ex)
            {
                log.Error("ArduinoRele.GetCOMFromUSBAdapterConverter - Error: " + ex.Message);
                return null;
            }
        }

        #endregion Private
    }
}
