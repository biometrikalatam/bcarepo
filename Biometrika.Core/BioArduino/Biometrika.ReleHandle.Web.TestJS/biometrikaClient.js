﻿function BiometrikaClient(host) {
    this.BaseUrl = host;
    this.origin = "TestWeb";
    this.relenumber = "1";
    this.timeout = "";
    this.operation = "1";
    
    this.ExecuteOperationALL = function () {
        var returnData = new BiometrikaClientResponse();

        var finalUrl = this.BaseUrl +
            "?origin=" + this.origin;

        var a = function (callback) {
            $.ajax({
                type: "POST",
                url: finalUrl,
                async: false,
                crossDomain: true,
                processData: false,
                error: function (error) {
                    var errMsg = "Error al llamar a BiometrikaReleHandle, Code=" + error.code + " - Message=" + error.message;
                    returnData.Status = false;
                    returnData.Message = errMsg;
                    console.log(errMsg);
                },
                success: callback
            });
        };

        a(function (msg) {
            returnData.Status = true;
            returnData.Data = msg;
        });

        return returnData;
    };

    this.ExecuteOperationOpenPulso = function () {
        var returnData = new BiometrikaClientResponse();

        var finalUrl = this.BaseUrl +
            "?origin=" + this.origin + "&relenumber=" + this.relenumber + "&timeout=" + this.timeout;
        //alert(finalUrl);
        //var finalUrl = this.BaseUrl +
        //    "?origin=" + this.origin + "&relenumber=" + this.relenumber + "&operation=1"; // + this.timeout;

        var a = function (callback) {
            $.ajax({
                type: "POST",
                url: finalUrl,
                async: false,
                crossDomain: true,
                processData: false,
                error: function (error) {
                    var errMsg = "Error al llamar a BiometrikaReleHandle, Code=" + error.code + " - Message=" + error.message;
                    returnData.Status = false;
                    returnData.Message = errMsg;
                    console.log(errMsg);
                },
                success: callback
            });
        };

        a(function (msg) {
            returnData.Status = true;
            returnData.Data = msg;
        });

        return returnData;
    };

    this.ExecuteOperationOpOne = function () {
        var returnData = new BiometrikaClientResponse();

        var finalUrl = this.BaseUrl +
            "?origin=" + this.origin + "&relenumber=" + this.relenumber + "&operation=" + this.operation;
        //alert(finalUrl);
        //var finalUrl = this.BaseUrl +
        //    "?origin=" + this.origin + "&relenumber=" + this.relenumber + "&operation=1"; // + this.timeout;

        var a = function (callback) {
            $.ajax({
                type: "POST",
                url: finalUrl,
                async: false,
                crossDomain: true,
                processData: false,
                error: function (error) {
                    var errMsg = "Error al llamar a BiometrikaReleHandle, Code=" + error.code + " - Message=" + error.message;
                    returnData.Status = false;
                    returnData.Message = errMsg;
                    console.log(errMsg);
                },
                success: callback
            });
        };

        a(function (msg) {
            returnData.Status = true;
            returnData.Data = msg;
        });

        return returnData;
    };

    this.ExecuteAccessAction = function () {
        var returnData = new BiometrikaClientResponse();

        var finalUrl = this.BaseUrl +
            "?origin=" + this.origin + "&relenumber=" + this.relenumber + "&operation=" + this.operation;
        //alert(finalUrl);
        //var finalUrl = this.BaseUrl +
        //    "?origin=" + this.origin + "&relenumber=" + this.relenumber + "&operation=1"; // + this.timeout;

        var a = function (callback) {
            $.ajax({
                type: "POST",
                url: finalUrl,
                async: false,
                crossDomain: true,
                processData: false,
                error: function (error) {
                    var errMsg = "Error al llamar a BiometrikaReleHandle, Code=" + error.code + " - Message=" + error.message;
                    returnData.Status = false;
                    returnData.Message = errMsg;
                    console.log(errMsg);
                },
                success: callback
            });
        };

        a(function (msg) {
            returnData.Status = true;
            returnData.Data = msg;
        });

        return returnData;
    };
}

function BiometrikaClientResponse() {
    this.Status = false;
    this.Message = "";
    this.Data = "";
}