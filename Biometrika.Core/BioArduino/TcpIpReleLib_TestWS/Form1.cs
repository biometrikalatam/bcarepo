﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TcpIpReleLib_TestWS
{
    public partial class Form1 : Form
    {
        ArduinoHelper _ARDUINO_HELPER;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string msg = null;
            using (WSReleHandle.WSReleHandle ws = new WSReleHandle.WSReleHandle())
            {
                ws.Url = textBox3.Text;
                ws.Timeout = 99999999;
                int i = ws.OpRele("test", Convert.ToInt32(txtRele.Text), 1, out msg);

                MessageBox.Show("Res=" + i + " - msg= " + msg);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string msg = null;
            using (WSReleHandle.WSReleHandle ws = new WSReleHandle.WSReleHandle())
            {
                ws.Url = textBox3.Text;
                ws.Timeout = 99999999;
                int i = ws.OpRelePulso("test", Convert.ToInt32(txtRele.Text), Convert.ToInt32(txtTimeout.Text), out msg);

                MessageBox.Show("Res=" + i);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string msg = null;
            using (WSReleHandle.WSReleHandle ws = new WSReleHandle.WSReleHandle())
            {
                ws.Url = textBox3.Text;
                ws.Timeout = 99999999;
                int i = ws.OpRele("test", Convert.ToInt32(txtRele.Text), 2, out msg);

                MessageBox.Show("Res=" + i + " - msg= " + msg);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string msg = null;
            using (WSReleHandle.WSReleHandle ws = new WSReleHandle.WSReleHandle())
            {
                ws.Url = textBox3.Text;
                ws.Timeout = 99999999;
                int i = ws.OpReleAllON("test", out msg);

                MessageBox.Show("Res=" + i + " - msg= " + msg);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string msg = null;
            using (WSReleHandle.WSReleHandle ws = new WSReleHandle.WSReleHandle())
            {
                ws.Url = textBox3.Text;
                ws.Timeout = 99999999;
                int i = ws.OpReleAllOFF("test", out msg);

                MessageBox.Show("Res=" + i + " - msg= " + msg);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //string msg = null;
            //using (WSReleHandle.WSReleHandle ws = new WSReleHandle.WSReleHandle())
            //{
            //    ws.Url = textBox3.Text;
            //    ws.Timeout = 99999999;
            //    int i = ws.OpReleAllImparON("test", out msg);

            //    MessageBox.Show("Res=" + i + " - msg= " + msg);
            //}
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //string msg = null;
            //using (WSReleHandle.WSReleHandle ws = new WSReleHandle.WSReleHandle())
            //{
            //    ws.Url = textBox3.Text;
            //    ws.Timeout = 99999999;
            //    int i = ws.OpReleAllParON("test", out msg);

            //    MessageBox.Show("Res=" + i + " - msg= " + msg);
            //}
        }

        private void button8_Click(object sender, EventArgs e)
        {
            //string msg = "";
            //string strstates = "";
            //using (WSReleHandle.WSReleHandle ws = new WSReleHandle.WSReleHandle())
            //{
            //    ws.Url = textBox3.Text;
            //    ws.Timeout = 99999999;
            //    int i = ws.GetStates("test", out strstates, out msg);

            //    MessageBox.Show("Res=" + i + " - msg= " + msg + " - staets=" + strstates);
            //}
        }

        

        private void button24_Click(object sender, EventArgs e)
        {

        }

        private void button16_Click(object sender, EventArgs e)
        {

        }

        private void button17_Click(object sender, EventArgs e)
        {
            _ARDUINO_HELPER = new ArduinoHelper(textBox4.Text.Trim(), "2", textBox2.Text.Trim());
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (_ARDUINO_HELPER == null) button17_Click(null, null);
            int ret = _ARDUINO_HELPER.OpenWithDelay(textBox1.Text.Trim(), textBox4.Text.Trim());
            if (ret == 0)
            {
                MessageBox.Show("Abrio OK!");
            }
            else
            {
                MessageBox.Show("ret=" + ret.ToString());
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (_ARDUINO_HELPER == null) button17_Click(null, null);
            int ret = _ARDUINO_HELPER.OpenAllWithDelay(4, textBox4.Text.Trim());
            if (ret == 0)
            {
                MessageBox.Show("Abrio OK!");
            }
            else
            {
                MessageBox.Show("ret=" + ret.ToString());
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            string msg = null;
            using (WSReleHandle.WSReleHandle ws = new WSReleHandle.WSReleHandle())
            {
                ws.Url = textBox3.Text;
                ws.Timeout = 99999999;
                int i = ws.OpAllRelePulso("test",Convert.ToInt32(txtTimeout.Text), out msg);

                MessageBox.Show("Res=" + i);
            }
        }
    }
}
