﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bio.Core.Api.TSA.Interface;
using Bio.Core.Api.Constant;
using log4net;
using System.Security.Cryptography;
using Org.BouncyCastle.Tsp;
using Org.BouncyCastle.Math;
using System.Net;
using System.IO;

namespace Bio.Core.TSA.SafeStamper
{
    public class TSAGenerator : ITSAGenerator
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(TSAGenerator));

        public bool IsTSAGeneratorConfigurate;
        private string _user;
        private string _password;

        private int _order;
        private string _name;
        private string _urlgeneration;
        private string _urlverification;
        private int _timeoutws;
        private string _parameters;

        ///<summary>
        ///</summary>
        public int Order
        {
            get { return _order; }
            set { _order = value; }
        }

        ///<summary>
        ///</summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        ///<summary>
        ///</summary>
        public string UrlGeneration
        {
            get { return _urlgeneration; }
            set { _urlgeneration = value; }
        }

        ///<summary>
        ///</summary>
        public string UrlVerification
        {
            get { return _urlverification; }
            set { _urlverification = value; }
        }

        ///<summary>
        ///</summary>
        public int TimeoutWS
        {
            get { return _timeoutws; }
            set { _timeoutws = value; }
        }

        ///<summary>
        ///</summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        //int GenerateTS(string xmlinput, out string xmloutput)
        //{
        //    throw new NotImplementedException();
        //}

        public int GenerateTS(string byteDocB64, out string xmlts)
        {
            int res = Errors.IERR_OK;
            xmlts = null;

            try
            {

                LOG.Debug("SafeStanmper TSAGenerator.GenerateTS - IN...");

                if (!IsTSAGeneratorConfigurate)
                {
                    if (!ConfigureTSAGenerator())
                    {
                        LOG.Debug("SafeStanmper TSAGenerator.GenerateTS - No configurado!");
                        return Errors.IERR_UNKNOWN;
                    }
                }

                if (byteDocB64 == null)
                {
                    LOG.Debug("SafeStanmper TSAGenerator.GenerateTS - byteDocB64 == null. OUT!");
                    return Errors.IERR_NULL_TEMPLATE;
                }

                //1.- Tomo el string de entrada, lo paso a byte[]
                //2.- Saco el hash con SHA1
                //3.- Me conecto al URLGeneration y genero TS
                //4.- Genero el XML de salida

                //1.- Tomo el string de entrada, lo paso a byte[]
                byte[] byDocToTS = Convert.FromBase64String(byteDocB64);

                //2.- Saco el hash con SHA1
                SHA1 sha1 = SHA1CryptoServiceProvider.Create();
                byte[] hash = sha1.ComputeHash(byDocToTS);                    // generar un hash del documento
                string sHash = BitConverter.ToString(hash).Replace("-", "");
                LOG.Debug("TSAGenerator.GenerateTS - Hash To TS = " + sHash);

                //3.- Me conecto al URLGeneration y genero TS
                TimeStampRequestGenerator reqGen = new TimeStampRequestGenerator();
                reqGen.SetCertReq(true);   // ??
                TimeStampRequest request = reqGen.Generate(TspAlgorithms.Sha1, hash, BigInteger.ValueOf(100));
                byte[] reqData = request.GetEncoded();
                HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(this.UrlGeneration);
                httpReq.Method = "POST";
                httpReq.ContentType = "application/timestamp-query";
                //"gsuhit@biometrika.cl:B1ometrika"
                httpReq.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(_user + ":" + _password)));
                httpReq.ContentLength = reqData.Length;

                // Write the request content
                Stream reqStream = httpReq.GetRequestStream();
                reqStream.Write(reqData, 0, reqData.Length);
                reqStream.Close();

                HttpWebResponse httpResp = (HttpWebResponse)httpReq.GetResponse();

                // Read the response
                Stream respStream = new BufferedStream(httpResp.GetResponseStream());
                TimeStampResponse response = new TimeStampResponse(respStream);
                respStream.Close();

                TimeStampToken t = response.TimeStampToken;

                if (t == null)
                {
                    LOG.Debug("SafeStanmper TSAGenerator.GenerateTS - TimeStampToken = null");
                    return Errors.IERR_CONX_WS;
                }
                else
                {
                    //Armo XML de Salida
                    xmlts = "<TimeStamping>" +
                                    "<Policy>" + t.TimeStampInfo.Policy.ToString() + "</Policy>" +
                                    "<DateTimeSign>" + t.TimeStampInfo.GenTime + "</DateTimeSign>" +
                                    "<SerialNumber>" + t.TimeStampInfo.TstInfo.SerialNumber.ToString() + "</SerialNumber>" +
                                    "<TSA>" + t.TimeStampInfo.Tsa.Name.ToString() + "</TSA>" +
                                    "<URLGenerator>" + this.UrlGeneration + "</URLGenerator>" +
                                    "<URLVerify>" + this.UrlVerification + "</URLVerify>" +
                            "</TimeStamping>";
                }
                res = Errors.IERR_OK;
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.TSA.TSAGenerator.GeneraTS Error", ex);
            }
            return res;
        }

        #region IDisposable Support
        private bool disposedValue = false; // Para detectar llamadas redundantes

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: elimine el estado administrado (objetos administrados).
                }

                // TODO: libere los recursos no administrados (objetos no administrados) y reemplace el siguiente finalizador.
                // TODO: configure los campos grandes en nulos.

                disposedValue = true;
            }
        }

        // TODO: reemplace un finalizador solo si el anterior Dispose(bool disposing) tiene código para liberar los recursos no administrados.
        // ~TSAGenerator() {
        //   // No cambie este código. Coloque el código de limpieza en el anterior Dispose(colocación de bool).
        //   Dispose(false);
        // }

        // Este código se agrega para implementar correctamente el patrón descartable.
        void IDisposable.Dispose()
        {
            // No cambie este código. Coloque el código de limpieza en el anterior Dispose(colocación de bool).
            Dispose(true);
            // TODO: quite la marca de comentario de la siguiente línea si el finalizador se ha reemplazado antes.
            // GC.SuppressFinalize(this);
        }
        #endregion


        private int ExtractParams(string parameters)
        {
            int ret = Errors.IERR_OK;
            _user = "gsuhit@bioemtrika.cl";
            _password = "B1ometrika";
            try
            {
                string[] arr = parameters.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                LOG.Debug("SafeStamper TSAGenerator.ExtractParams arr.length=" + arr.Length.ToString());
                if (arr.Length == 0) return Errors.IERR_OK;

                for (int i = 0; i < arr.Length; i++)
                {
                    LOG.Debug("SafeStamper TSAGenerator.ExtractParams procesando arr[" + i.ToString() + "]=" + arr[i]);
                    string[] item = arr[i].Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                    LOG.Debug("SafeStamper TSAGenerator.ExtractParams item.length=" + item.Length.ToString());
                    if (item[0].Equals("User") || item[0].Equals("user"))
                    {
                        _user = item[1];
                        LOG.Debug("SafeStamper TSAGenerator.ExtractParams  user =" + _user);
                    }
                    if (item[0].Equals("Password") || item[0].Equals("password"))
                    {
                        _password = item[1];
                        LOG.Debug("SafeStamper TSAGenerator.ExtractParams Password = " + (String.IsNullOrEmpty(_password) ? "NO OK" : "OK"));
                    }
                }
                IsTSAGeneratorConfigurate = true; 
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.TSA.SafeStamper.TSAGenerator..ExtractParams Error", ex);
                IsTSAGeneratorConfigurate = false;
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }

        public bool ConfigureTSAGenerator()
        {
            bool bRet;
            try
            {
                bRet = (ExtractParams(_parameters) == 0);
            }
            catch (Exception ex)
            {
                LOG.Error("SafeStamper.TSAGenerator.ConfigureTSAGenerator Error = " + ex.Message);
                bRet = false;
            }
            return bRet;
        }
    }
}
