﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Biometrika.Flow.API.Responses;
using CL.Flow.RestAPI.Client;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Biometrika.Flow.API
{
    public class FlowAPI
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(FlowAPI));

        private string _API_KEY = "663E0BF9-B857-4352-85CE-6F12EBL798DE";
        private string _SECRET_KEY = "a8123fce839f11f36c3688a461abdb34b5e496c7";
        private string _URL_BASE = "https://sandbox.flow.cl/api/";

        public string API_KEY { get => this._API_KEY; set => this._API_KEY = value; }
        public string SECRET_KEY { get => this._SECRET_KEY; set => this._SECRET_KEY = value; }
        public string URL_BASE { get => this._URL_BASE; set => this._URL_BASE = value; }

        internal FlowClient _FLOW_CLIENT;



        public FlowAPI() {
            Initialization();
        }


        public FlowAPI(string urlbase, string apikey, string secretkey)
        {
            API_KEY = apikey;
            URL_BASE = urlbase;
            SECRET_KEY = secretkey;

            Initialization();
        }

        private bool Initialization()
        {
            try
            {
                _FLOW_CLIENT = new FlowClient();
                _FLOW_CLIENT.SetApiUrl(URL_BASE);
                _FLOW_CLIENT.SetApiKey(API_KEY);
                _FLOW_CLIENT.SetSecretKey(SECRET_KEY);
            }
            catch (Exception ex)
            {
                //LOG.Error(" Error: " + ex.Message);
                return false;
            }
            return true;
        }


        

#region payment


        /// <summary>
        /// Creao orden de pago en FLow y devuelve url + token para redirigir al pago
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="currency"></param>
        /// <param name="amount"></param>
        /// <param name="mail"></param>
        /// <param name="commerceOrder"></param>
        /// <param name="paymentMethod"></param>
        /// <param name="urlconfirmtation"></param>
        /// <param name="urlreturn"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        public int CreateOrderPayment(string subject, string currency, string amount, string mail, string commerceOrder,
                                      string paymentMethod, string urlconfirmtation, string urlreturn, 
                                      out string strresponse, out object response)
        {
            int ret = -1;
            response = null;
            strresponse = null;
            try
            {
                LOG.Debug("FlowAPI.CreateOrderPayment IN...");
                if (_FLOW_CLIENT != null || Initialization())
                {
                    LOG.Debug("FlowAPI.CreateOrderPayment - Inicia llamado acreate order...");
                    //Create Payment
                    string service = "payment/create";
                    IDictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add("subject", subject);
                    parameters.Add("currency", currency);
                    parameters.Add("amount", amount);
                    parameters.Add("email", mail);
                    parameters.Add("paymentMethod", (string.IsNullOrEmpty(paymentMethod) ? "9": paymentMethod)); //9 - Todos los medios de pago
                    parameters.Add("commerceOrder", commerceOrder);
                    parameters.Add("urlConfirmation", urlconfirmtation);
                    parameters.Add("urlReturn", urlreturn);

                    LOG.Debug("FlowAPI.CreateOrderPayment - Enviando order = " + commerceOrder + " por amount = " +
                        amount + " de mail = " + mail + "...");
                    JObject objResponse = _FLOW_CLIENT.Send(service, parameters, HttpMethod.POST);

                    if (objResponse != null)
                    {
                        LOG.Debug("FlowAPI.CreateOrderPayment - Respuesta no null...");
                        if (HasError(objResponse))
                        {
                            LOG.Error("FlowAPI.CreateOrderPayment - Error informado desde servicio de flow [code = " +
                                         GetValue(objResponse, "code") + " - " + GetValue(objResponse, "message"));
                            response = new ResponseBaseFlow("-2", "FlowAPI.CreateOrderPayment Error informado desde servicio de flow [code = " +
                                         GetValue(objResponse, "code") + " - " + GetValue(objResponse, "message"));
                            ret = -2;
                        }
                        else
                        {
                            //var options = new JsonSerializerOptions
                            //{
                            //    IgnoreNullValues = true,
                            //    WriteIndented = true
                            //};
                            JsonSerializerSettings js = new JsonSerializerSettings();
                            js.NullValueHandling = NullValueHandling.Ignore;
                            response = JsonConvert.DeserializeObject<ResponseCreateOrderPaymentFlow>(objResponse.ToString(),js);
                            if (response == null)
                            {
                                LOG.Error("FlowAPI.CreateOrderPayment - Error parseando respuesta");
                                new ResponseBaseFlow("-4", "Error parseando respuesta");
                                ret = -3;
                            } else
                            {
                                strresponse = objResponse.ToString();
                                ret = 0;
                            }
                        }
                    } else
                    {
                        LOG.Error("FlowAPI.CreateOrderPayment - Error respuesta nula desde servicio de flow");
                        new ResponseBaseFlow("-3", "Error respuesta nula desde servicio de flow");
                        ret = -3;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                response = new ResponseBaseFlow("-1", "FlowAPI.CreateOrderPayment Error Desconocido = " + ex.Message); 
                LOG.Error("FlowAPI.CreateOrderPayment Error: " + ex.Message);
            }
            LOG.Debug("FlowAPI.CreateOrderPayment IN...");
            return ret;
        }

        public int CreateOrderByMailPayment()
        {
            int ret = -1;

            return ret;
        }


        public int GetStatusOrderPayment(string token, string floworder, out string strresponse,  out object response)
        {
            int ret = -1;
            response = null;
            strresponse = null;
            try
            {
                LOG.Debug("FlowAPI.GetStatusOrderPayment IN...");

                if (string.IsNullOrEmpty(token) && string.IsNullOrEmpty(floworder)) return -2;

                if (_FLOW_CLIENT != null || Initialization())
                {
                    LOG.Debug("FlowAPI.GetStatusOrderPayment - Inicia llamado a create order...");
                    //Create Payment
                    string service = "payment/";
                    IDictionary<string, string> parameters = new Dictionary<string, string>();
                    if (!string.IsNullOrEmpty(token))
                    {
                        service += "getStatus";
                        parameters.Add("token", token);
                    } else {
                        service += "getStatusByFlowOrder";
                        parameters.Add("flowOrder", floworder);
                    }
                    
                    LOG.Debug("FlowAPI.GetStatusOrderPayment - Enviando get status order = " +
                                    (string.IsNullOrEmpty(token)?"FlorOrder = "+ floworder : "Token=" + token));
                    JObject objResponse = _FLOW_CLIENT.Send(service, parameters, HttpMethod.GET);

                    if (objResponse != null)
                    {
                        LOG.Debug("FlowAPI.GetStatusOrderPayment - Respuesta no null...");
                        if (HasError(objResponse))
                        {
                            LOG.Error("FlowAPI.GetStatusOrderPayment - Error informado desde servicio de flow [code = " +
                                         GetValue(objResponse, "code") + " - " + GetValue(objResponse, "message"));
                            response = new ResponseBaseFlow("-2", "FlowAPI.GetStatusOrderPayment Error informado desde servicio de flow [code = " +
                                         GetValue(objResponse, "code") + " - " + GetValue(objResponse, "message"));
                            ret = -2;
                        }
                        else
                        {
                            JsonSerializerSettings js = new JsonSerializerSettings();
                            js.NullValueHandling = NullValueHandling.Ignore;
                            response = JsonConvert.DeserializeObject<ResponseStatusOrderPaymentFlow>(objResponse.ToString(), js);
                            if (response == null)
                            {
                                LOG.Error("FlowAPI.GetStatusOrderPayment - Error parseando respuesta");
                                new ResponseBaseFlow("-4", "Error parseando respuesta");
                                ret = -3;
                            } else
                            {
                                strresponse = objResponse.ToString();
                                ret = 0;
                            }
                        }
                    }
                    else
                    {
                        LOG.Error("FlowAPI.GetStatusOrderPayment - Error respuesta nula desde servicio de flow");
                        new ResponseBaseFlow("-3", "Error respuesta nula desde servicio de flow");
                        ret = -3;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                response = new ResponseBaseFlow("-1", "FlowAPI.GetStatusOrderPayment Error Desconocido = " + ex.Message);
                LOG.Error("FlowAPI.GetStatusOrderPayment Error: " + ex.Message);
            }
            LOG.Debug("FlowAPI.GetStatusOrderPayment IN...");
            return ret;
        }



        #endregion payment

        #region private

        private bool HasError(JObject response)
        {
            return !IsNullOrEmpty(GetValue(response, "code"));
        }

        private bool IsNullOrEmpty(string value)
        {
            return value == null || value.Trim().Length == 0;
        }

        private string GetValue(JObject obj, string key)
        {
            var value = obj[key];
            return value != null ? value.Value<String>() : null;
        }

#endregion private
    }
}
