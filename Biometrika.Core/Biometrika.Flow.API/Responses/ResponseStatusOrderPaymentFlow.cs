﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Flow.API.Responses
{
    public class ResponseStatusOrderPaymentFlow
    {
        public int flowOrder { get; set; }
        public string commerceOrder { get; set; }
        //public DateTime requestDate { get; set; }
        public string requestDate { get; set; }
        public int status { get; set; }
        public string subject { get; set; }
        public string currency { get; set; }
        public int amount { get; set; }
        public string payer { get; set; }
        public Optional optional { get; set; }
        public Pending_info pending_info { get; set; }
        public PaymentData paymentData { get; set; }
        public string merchantId { get; set; }
    }

    public class Optional
    {
        public string RUT { get; set; }
        public string ID { get; set; }

    }
    public class Pending_info
    {
        public string media { get; set; }
        //public DateTime date { get; set; }
        public string date { get; set; }

    }
    public class PaymentData
    {
        public string date { get; set; }
        //public DateTime date { get; set; }
        public string media { get; set; }
        public string conversionDate { get; set; }
        public double conversionRate { get; set; }
        public double amount { get; set; }
        public double fee { get; set; }
        public double balance { get; set; }
        public string transferDate { get; set; }

    }

}
