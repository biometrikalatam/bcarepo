﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Flow.API.Responses
{
    public class ResponseCreateOrderPaymentFlow
    {
        public string url { get; set; }
        public string token { get; set; }
        public int flowOrder { get; set; }
    }
}
