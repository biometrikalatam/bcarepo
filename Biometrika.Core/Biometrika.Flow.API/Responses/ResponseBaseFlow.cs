﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Flow.API.Responses
{
    public class ResponseBaseFlow
    {
        public ResponseBaseFlow() { }
        public ResponseBaseFlow(string _code, string _msg) {
            code = _code;
            message = _msg;
        }

        public string code { get; set; }
        public string message { get; set; }

    }
}
