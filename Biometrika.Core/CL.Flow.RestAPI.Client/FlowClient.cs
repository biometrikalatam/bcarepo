﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Net;

namespace CL.Flow.RestAPI.Client
{
    public enum HttpMethod
    {
        GET,
        POST
    }

    public class FlowClient
    {
        #region Private Members

        private string ApiKey;
        private string SecretKey;
        private string ApiUrl;

        #endregion

        #region Public Methods

        /// <summary>
        /// Invoca a un servicio de la API Rest de Flow
        /// </summary>
        /// <param name="service">Nombre del servicio a invocar</param>
        /// <param name="parameters">Parámetros a ser enviados</param>
        /// <param name="method">Método HTTP a utilizar. Si no se recibe valor se asume GET</param>
        /// <returns></returns>
        public JObject Send(string service, IDictionary<string, string> parameters, HttpMethod method = HttpMethod.GET)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            CheckInit();
            RestClient client = new RestClient(ApiUrl);
            RestRequest request = GetRequest(service, parameters, method);
            IRestResponse response = client.Execute(request);

            var cod = response.StatusCode;

            JObject content = JsonConvert.DeserializeObject<JObject>(response.Content);

            if(HasError(content)) {
                string code = GetValue(content, "code");
                string message = GetValue(content, "message");
                throw new FlowException(message, code);
            } 

            return content;
        }

        /// <summary>
        /// Asigna el valor de API Key
        /// </summary>
        /// <param name="apiKey">API Key</param>
        public void SetApiKey(string apiKey)
        {
            this.ApiKey = apiKey;
        }

        /// <summary>
        /// Asigna el valor de Secret Key
        /// </summary>
        /// <param name="secretKey">Secret Key</param>
        public void SetSecretKey(string secretKey)
        {
            this.SecretKey = secretKey;
        }

        /// <summary>
        /// Asigna el valor de API Url
        /// </summary>
        /// <param name="apiUrl">API Url</param>
        public void SetApiUrl(string apiUrl)
        {
            this.ApiUrl = apiUrl;
        }

        #endregion

        #region Private Methods

        private RestRequest GetRequest(string service, IDictionary<string, string> parameters, HttpMethod method)
        {
            RestRequest request = new RestRequest(service, (method == HttpMethod.GET) ? Method.GET : Method.POST);
            IDictionary<string, string> allparameters = (parameters != null) ? parameters : new Dictionary<string, string>();
            allparameters.Add("apiKey", ApiKey);

            var keyList = allparameters.Keys.ToList();
            keyList.Sort();

            foreach(var key in keyList)
            {
                request.AddParameter(key, allparameters[key]);
            }

            request.AddParameter("s", SignParameters(request.Parameters));
            return request;
        }

        private string SignParameters(IList<Parameter> parameters)
        {
            IList<string> paramList = new List<string>();
            foreach (var parameter in parameters)
            {
                paramList.Add(parameter.Name + parameter.Value);
            }
            return EncodeSign(String.Join("", paramList.ToArray()));
        }

        private string ByteToString(byte[] buff)
        {
            string sbinary = "";
            for (int i = 0; i < buff.Length; i++)
                sbinary += buff[i].ToString("X2"); /* hex format */
            return sbinary;
        }    

        private string EncodeSign(string message)
        {
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(SecretKey);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return ByteToString(hashmessage).ToLower();
            }
        }

        private bool HasError(JObject response)
        {
            return !IsNullOrEmpty(GetValue(response, "code"));
        }

        private void CheckInit()
        {
            if (IsNullOrEmpty(ApiKey))
            {
                throw new FlowException("No se ha configurado 'ApiKey'");
            }

            if (IsNullOrEmpty(SecretKey))
            {
                throw new FlowException("No se ha configurado 'SecretKey'");
            }

            if (IsNullOrEmpty(ApiUrl))
            {
                throw new FlowException("No se ha configurado 'ApiUrl'");
            }
        }


        private bool IsNullOrEmpty(string value)
        {
            return value == null || value.Trim().Length == 0;
        }

        private string GetValue(JObject obj, string key)
        {
            var value = obj[key];
            return value != null ? value.Value<String>() : null;
        }

        #endregion
    }
}
