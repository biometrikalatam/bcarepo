﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CL.Flow.RestAPI.Client
{
    public class FlowException : Exception
    {
        private string errorCode;

        public string ErrorCode 
        {
            get {
                return errorCode;
            }
        }

        public FlowException(string message, string errorCode = "0") : base(message)
        {
            this.errorCode = errorCode;
        }
    }
}
