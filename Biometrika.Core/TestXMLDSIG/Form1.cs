using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using Bio.Digital.Receipt.Core;
//using Biokey.pki.mentalis;
using TestXMLDSIG.localhost;
using Org.BouncyCastle.Tsp;
using System.Net;
using Org.BouncyCastle.Math;

namespace TestXMLDSIG
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            richTextBox1.Text = "<rd>" + 
	                            "   <Recibo Id=\"IDRDREF.v1\" app=\"Test\" reciboID=\"112043347951183982674484979034418161202193260356750\" version=\"1.0\">" +
		                        "        <Timestamp>14-08-2009 15:30:00</Timestamp>" +
			                    "        <Descripcion>Test XMLDSIG en .NET</Descripcion>" +
                                "        <Extensiones Id=\"IDRDREF.v2\">" + 
                                "           <FileName>contrato1.pdf</FileName>" +
                                "           <Operador>Juan Perez</Operador>" +
                                "        </Extensiones>" +
                                "	</Recibo>" +
                                "</rd>";

            XmlDocument result = new XmlDocument();
            result.PreserveWhitespace = true;
            result.LoadXml(richTextBox1.Text);

            string res = result.GetElementsByTagName("Operador")[0].InnerText;

            int i = 0;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Biokey.pki.mentalis.BOKpkcs12 pfx = new BOKpkcs12();
            //AsymmetricAlgorithm key = 
            //    BOKpkcs12.GetPrivateKeyFromPFX(
            //        "C:\\Personal\\NP\\Desarrollo\\biometrika_PKI\\notariobiometrika.pfx", "biometrika");

            X509Certificate2 cert = new X509Certificate2();
            cert.Import(@"C:\Biometrika\Desarrollo\biometrika_PKI\E-Cert\GustavoSuhitExportado.pfx", "biometrika", X509KeyStorageFlags.PersistKeySet);
            //.CreateFromCertFile(@"C:\Biometrika\Desarrollo\biometrika_PKI\E-Cert\GustavoSuhitExportado.pfx");
            AsymmetricAlgorithm key = cert.PrivateKey;


            // Create a new CspParameters object to specify
            // a key container.
            //CspParameters cspParams = new CspParameters();
            //cspParams.KeyContainerName = "XML_DSIG_RSA_KEY";

            //// Create a new RSA signing key and save it in the container. 
            //RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider(cspParams);


            // Create a new XML document.
            XmlDocument xmlDoc = new XmlDocument();

            // Load an XML file into the XmlDocument object.
            xmlDoc.PreserveWhitespace = false;
            xmlDoc.LoadXml(richTextBox1.Text);

            // Sign the XML document. 
            SignXml(xmlDoc, key);

            Console.WriteLine("XML file signed.");

            // Save the document.
            richTextBox2.Text = xmlDoc.OuterXml;
            xmlDoc.Save("C:\\Biometrika\\Desarrollo\\Bio.Digital.Receipt\\TestXMLDSIG\\test.xml");
                
        }

        // Sign an XML file. 
        // This document cannot be verified unless the verifying 
        // code has the key with which it was signed.
        public void SignXml(XmlDocument Doc, AsymmetricAlgorithm Key) //RSA Key)
        {
            // Check arguments.
            if (Doc == null)
                throw new ArgumentException("Doc");
            if (Key == null)
                throw new ArgumentException("Key");

            // Create a SignedXml object.
            SignedXml signedXml = new SignedXml(Doc);

            // Add the key to the SignedXml document.
            signedXml.SigningKey = Key;

            // Create a reference to be signed.
            Reference reference = new Reference();
            reference.Uri = "#" + textBox1.Text;

            // Add an enveloped transformation to the reference.
            XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
            reference.AddTransform(env);
            
            // Add the reference to the SignedXml object.
            signedXml.AddReference(reference);

            // Add an RSAKeyValue KeyInfo (optional; helps recipient find key to validate).
            KeyInfo keyInfo = new KeyInfo();
            keyInfo.AddClause(new RSAKeyValue((RSA)Key));

            // Load the X509 certificate.
            X509Certificate MSCert = X509Certificate.CreateFromCertFile(@"C:\Biometrika\Desarrollo\biometrika_PKI\E-Cert\GustavoSuhit.cer");
                //("C:\\Personal\\NP\\Desarrollo\\biometrika_PKI\\notariobiometrika.cer");

            // Load the certificate into a KeyInfoX509Data object
            // and add it to the KeyInfo object.
            keyInfo.AddClause(new KeyInfoX509Data(MSCert));
            
            signedXml.KeyInfo = keyInfo;
            
            // Compute the signature.
            signedXml.ComputeSignature();

            // Get the XML representation of the signature and save
            // it to an XmlElement object.
            XmlElement xmlDigitalSignature = signedXml.GetXml();

            // Append the element to the XML document.
            Doc.DocumentElement.AppendChild(Doc.ImportNode(xmlDigitalSignature, true));

            
        }

        // Verify the signature of an XML file against an asymmetric 
        // algorithm and return the result.
        public static Boolean VerifyXml(XmlDocument Doc, AsymmetricAlgorithm key) //RSA Key)
        {
            // Check arguments.
            if (Doc == null)
                throw new ArgumentException("Doc");
            //if (Key == null)
            //    throw new ArgumentException("Key");

            // Create a new SignedXml object and pass it
            // the XML document class.
            SignedXml signedXml = new SignedXml(Doc);

            // Find the "Signature" node and create a new
            // XmlNodeList object.
            XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");

            // Throw an exception if no signature was found.
            if (nodeList.Count <= 0)
            {
                throw new CryptographicException("Verification failed: No Signature was found in the document.");
            }

            // This example only supports one signature for
            // the entire XML document.  Throw an exception 
            // if more than one signature was found.
            if (nodeList.Count >= 2)
            {
                throw new CryptographicException("Verification failed: More that one signature was found for the document.");
            }

            // Load the first <signature> node.  
            signedXml.LoadXml((XmlElement)nodeList[0]);

            // Check the signature and return the result.
            return signedXml.CheckSignature();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Biokey.pki.mentalis.BOKpkcs12 pfx = new BOKpkcs12();
            //AsymmetricAlgorithm key =
            //    BOKpkcs12.GetPublicKeyFromPFX(
            //        "C:\\Personal\\NP\\Desarrollo\\biometrika_PKI\\notariobiometrika.pfx", "biometrika");
            
                        // Create a new XML document.
            XmlDocument xmlDoc = new XmlDocument();

            // Load an XML file into the XmlDocument object.
            xmlDoc.PreserveWhitespace = false;
            xmlDoc.LoadXml(richTextBox2.Text);
            //bool res = VerifyXml(xmlDoc, key);

            //MessageBox.Show("Resultado Verify = " + res.ToString()); 
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Rd ser = new Rd();
            //ser.fecha = DateTime.Now;
            //ser.descripcion = "Test Serializacion";
            //ser.ip = "127.0.0.1";
            //ser.origen = "1 - OrigenTest";
            //ser.app = "AppTest";
            //ser.Id = "IDRDREF.v1";
            //ser.reciboID = "1234567890";
            //ser.Extensiones = new RdExtensionItem[3];
            //RdExtensionItem ext;
            //for (int i = 0; i <= 2; i++)
            //{
            //    ext = new RdExtensionItem();
            //    ext.key = "key" + i.ToString();
            //    ext.value = i.ToString();
            //    ser.Extensiones[i] = ext;
            //}
            //Rd.SaveConfigFile(ser);    
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Rd recibo = Rd.LoadConfigFile("c:\\temp\\TestSer.xml");

            //richTextBox1.Text = recibo.reciboID + Environment.NewLine + "  >> Extensiones:" + 
            //    Environment.NewLine;
            //for (int i=0; i<recibo.Extensiones.Length; i++)
            //{
            //    richTextBox1.Text = richTextBox1.Text + 
            //                        recibo.Extensiones[i].key + "=" +
            //                        recibo.Extensiones[i].value 
            //                        + Environment.NewLine;
            //}
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string xmlRDorError = null;  //Devuelve el xml del recibo o el mensaje de error si hay error
            int ret = 0;

            try
            {
                
                localhost.ArrayOfRdExtensionItemRdExtensionItem[] arrExt = new ArrayOfRdExtensionItemRdExtensionItem[7];
                localhost.ArrayOfRdExtensionItemRdExtensionItem item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "TipoDocumento";
                item.value = "1";
                arrExt[0] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "NumeroDocumento";
                item.value = "20091031010250";
                arrExt[1] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "FechaDocumento";
                item.value = DateTime.Now.ToString("dd/MM/yyyy");
                arrExt[2] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "FechaVencimientoDocumento";
                item.value = DateTime.Now.AddDays(365).ToString("dd/MM/yyyy"); ;
                arrExt[3] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "Cliente";
                item.value = "76899988-0_BIOMETRIKA";
                arrExt[4] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "Vendedor";
                item.value = "21284415-2_GUSTAVO SUHIT";
                arrExt[5] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "Documento";
                byte[] byDocumento;
                string arch = @"D:\Biometrika\Bio.Contrato\generados\20141201120710.doc";
                System.IO.FileStream fs = new FileStream(arch, FileMode.Open);
                byDocumento = new byte[fs.Length];
                fs.Read(byDocumento, 0, (int)fs.Length);
                fs.Close();
                item.value = Convert.ToBase64String(byDocumento);
                arrExt[6] = item;

                using (localhost.WSBDR ws = new localhost.WSBDR())
                {
                    ws.Timeout = 3000000;

                    RDRetorno rdRet = ws.GenerateReceipt(1, "TestXMLDSIG Project",
                                                "Demo Biometrika-Doc ",
                                                arrExt);

                    if (rdRet.codigo == 0)
                    {
                        xmlRDorError = rdRet.xmlret;
                    }
                    else
                    {
                        ret = rdRet.codigo;
                        xmlRDorError = rdRet.descripcion;
                    }
                    richTextBox2.Text = xmlRDorError;

                }

            }
            catch (Exception ex)
            {
                //log.Error("Error", ex);
                string err = ex.Message;
                ret = -1;
            }

            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            X509Certificate MSCert = X509Certificate.CreateFromCertFile("C:\\Personal\\NP\\Desarrollo\\biometrika_PKI\\notariobiometrika.cer");
            this.richTextBox2.Text = MSCert.ToString(true);
            
            
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string xmlRDorError = null;  //Devuelve el xml del recibo o el mensaje de error si hay error
            int ret = 0;

            try
            {

                localhost.ArrayOfRdExtensionItemRdExtensionItem[] arrExt = new ArrayOfRdExtensionItemRdExtensionItem[5];
                localhost.ArrayOfRdExtensionItemRdExtensionItem item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "TxID";
                item.value = "1950ac4827c14334a807c3be19dd470a";
                arrExt[0] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "TypeID";
                item.value = "RUT";
                arrExt[1] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "ValueID";
                item.value = "21284415-2";
                arrExt[2] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "Fecha";
                item.value = DateTime.Now.ToString("dd/MM/yyyy");
                arrExt[3] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "ABS";
                item.value = "<BioSignature id=\"IDREF.v1\"><TrackID>id1129182</TrackID><Timestamp>31-01-2015 19:10:13.297</Timestamp><Source>SRCeI</Source><Score>13200</Score><ImageSample>/6DakjaksabKJHDKJGKJDGKG</ImageSample><Barcode></Barcode><Photografy></Photografy><DynamicData></DynamicData><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /><SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\" /><Reference URI=\"#IDREF.v1\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\" /></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\" /><DigestValue>FTOHSerLZEz16qcTgBQ3SNtXnno=</DigestValue></Reference></SignedInfo><SignatureValue>d4u+YKSuXzCqIqKPtrVPYh0FF+NS4t8+eJ07u3ErRJaCocQrVUKHoU7Mm9WjaYYxoqlCJ+9IqJtp1P8rfyCf0ZJpCpDqb7DpK6lnaLtG+z8ok7o5CSbVCleoNDkfcCfXqPOndp8PwnSjsT0cgFTk/DalsG4bsth6ZrmXv8U2f8E=</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>t6VZ7kkNFK7lTzHx9kTkexNhnC7MDVc+phMNG28Ye7Geet4cLhhZ6uqJPaBtgv6wFwbXfV4XlS95lOXjEgUSm+izc4qpgrhYC6OfZJG1bpok4UP7wLH80gjWn9b7mXWsMEUQiwMVQlhdFMpgE3JeIZRyFEpBs4OBOaGMmmG8QIM=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue><X509Data><X509Certificate>MIIGBTCCBO2gAwIBAgIKK4HM0wAAAADIeDANBgkqhkiG9w0BAQUFADCB0jELMAkGA1UEBhMCQ0wxHTAbBgNVBAgTFFJlZ2lvbiBNZXRyb3BvbGl0YW5hMREwDwYDVQQHEwhTYW50aWFnbzEUMBIGA1UEChMLRS1DRVJUQ0hJTEUxIDAeBgNVBAsTF0F1dG9yaWRhZCBDZXJ0aWZpY2Fkb3JhMTAwLgYDVQQDEydFLUNFUlRDSElMRSBDQSBGSVJNQSBFTEVDVFJPTklDQSBTSU1QTEUxJzAlBgkqhkiG9w0BCQEWGHNjbGllbnRlc0BlLWNlcnRjaGlsZS5jbDAeFw0xMzA3MjkyMTA1MTBaFw0xNjA3MjgyMTA1MTBaMIHEMQswCQYDVQQGEwJDTDEjMCEGA1UECBMaTUVUUk9QT0xJVEFOQSBERSBTQU5USUFHTyAxETAPBgNVBAcTCFNhbnRpYWdvMSkwJwYDVQQKEyBCQ1IgVEVDTk9MT0dJQSBFIElOTk9WQUNJT04gUy5BLjEKMAgGA1UECwwBKjEhMB8GA1UEAxMYR3VzdGF2byBHZXJhcmRvICBTdWhpdCAuMSMwIQYJKoZIhvcNAQkBFhRnc3VoaXRAYmlvbWV0cmlrYS5jbDCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAt6VZ7kkNFK7lTzHx9kTkexNhnC7MDVc+phMNG28Ye7Geet4cLhhZ6uqJPaBtgv6wFwbXfV4XlS95lOXjEgUSm+izc4qpgrhYC6OfZJG1bpok4UP7wLH80gjWn9b7mXWsMEUQiwMVQlhdFMpgE3JeIZRyFEpBs4OBOaGMmmG8QIMCAwEAAaOCAmswggJnMD0GCSsGAQQBgjcVBwQwMC4GJisGAQQBgjcVCILcgy+Fk4xmhdWdF4Li3CeB7+w8YYHLnhSGqIJYAgFkAgEDMB0GA1UdDgQWBBSA5w018UhLsrsObq8J0cfPWzVhWDALBgNVHQ8EBAMCBPAwHwYDVR0jBBgwFoAUeOE+n9ISs3o8jc0wDlOzQykHs1UwPgYDVR0fBDcwNTAzoDGgL4YtaHR0cDovL2NybC5lLWNlcnRjaGlsZS5jbC9lY2VydGNoaWxlY2FGRVMuY3JsMCMGA1UdEQQcMBqgGAYIKwYBBAHBAQGgDBYKMjEyODQ0MTUtMjAjBgNVHRIEHDAaoBgGCCsGAQQBwQECoAwWCjk2OTI4MTgwLTUwggFNBgNVHSAEggFEMIIBQDCCATwGCCsGAQQBw1IFMIIBLjAtBggrBgEFBQcCARYhaHR0cDovL3d3dy5lLWNlcnRjaGlsZS5jbC9DUFMuaHRtMIH8BggrBgEFBQcCAjCB7x6B7ABDAGUAcgB0AGkAZgBpAGMAYQBkAG8AIABGAGkAcgBtAGEAIABTAGkAbQBwAGwAZQAuACAASABhACAAcwBpAGQAbwAgAHYAYQBsAGkAZABhAGQAbwAgAGUAbgAgAGYAbwByAG0AYQAgAHAAcgBlAHMAZQBuAGMAaQBhAGwALAAgAHEAdQBlAGQAYQBuAGQAbwAgAGgAYQBiAGkAbABpAHQAYQBkAG8AIABlAGwAIABDAGUAcgB0AGkAZgBpAGMAYQBkAG8AIABwAGEAcgBhACAAdQBzAG8AIAB0AHIAaQBiAHUAdABhAHIAaQBvMA0GCSqGSIb3DQEBBQUAA4IBAQBF2HPkNASvnFBt+Acko6B411BPvsEeJPyvg50HNkI5e18pRLV5efeN50mTGh9CFElRvbjNlIDH8VscM/jZ36G/i/GIsa/urIEZP42J+v/0iTlLmI7PJqKDR1f5HQWBY4FviK6jbNf8Jm3GV8l8O74gBTyyDqxQXJRkwGcbsU8LUZMVfy+XR+AL6lZVJ9SzSglIyUNuL/h6jgTB1Z+3kpfUz7t5FtkI2VYkhugaZR7tfwD8K7xjnRnDnLQQLr5kAsj743+B/ljMhakplCnUy7vIx0MnNbE04Y9WDjvSuKN7C2JllAtSBKUNSEFHFGvmq/YcRad/yCI5Fl9HXTK688GT</X509Certificate></X509Data></KeyInfo></Signature></BioSignature>";
                arrExt[4] = item;

                using (localhost.WSBDR ws = new localhost.WSBDR())
                {
                    ws.Timeout = 3000000;

                    RDRetorno rdRet = ws.GenerateReceipt(2, "BioPortal Server",
                                                "Biometric Advanced Signature Notarize",
                                                arrExt);

                    if (rdRet.codigo == 0)
                    {
                        xmlRDorError = rdRet.xmlret;
                        richTextBox2.Text = rdRet.xmlret;
                    }
                    else
                    {
                        ret = rdRet.codigo;
                        xmlRDorError = rdRet.descripcion;
                    }
                }

            }
            catch (Exception ex)
            {
                //log.Error("Error", ex);
                string err = ex.Message;
                ret = -1;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            //    public static TransactionDocument SignWithTSA(TransactionDocument transactionDocument)
            //{

            
            //transactionDocument.HashType = "SHA1";
            SHA1 sha1 = SHA1CryptoServiceProvider.Create();
            byte[] hash = sha1.ComputeHash(UTF8Encoding.UTF8.GetBytes(richTextBox2.Text));                    // generar un hash del documento
            string sHash = BitConverter.ToString(hash).Replace("-", "");
            richTextBox2.Text = "Hash = " + sHash + Environment.NewLine;
            //transactionDocument.Hash = BitConverter.ToString(hash);                  // mantener el hash como control futuro

            TimeStampRequestGenerator reqGen = new TimeStampRequestGenerator();

            reqGen.SetCertReq(true);   // ??
            TimeStampRequest request = reqGen.Generate(TspAlgorithms.Sha1, hash, BigInteger.ValueOf(100));
           
            byte[] reqData = request.GetEncoded();

            try
            {
                HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create("https://tsa.safestamper.com/");
                httpReq.Method = "POST";
                httpReq.ContentType = "application/timestamp-query";
                httpReq.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes("gsuhit@biometrika.cl:B1ometrika")));
                httpReq.ContentLength = reqData.Length;

                // Write the request content
                Stream reqStream = httpReq.GetRequestStream();
                reqStream.Write(reqData, 0, reqData.Length);
                reqStream.Close();

                HttpWebResponse httpResp = (HttpWebResponse)httpReq.GetResponse();

                // Read the response
                Stream respStream = new BufferedStream(httpResp.GetResponseStream());
                TimeStampResponse response = new TimeStampResponse(respStream);
                respStream.Close();

                TimeStampToken t = response.TimeStampToken;

                string res1 = "Serial Number: " + t.TimeStampInfo.TstInfo.SerialNumber.ToString();
                string res2 = "Gen Time: " + t.TimeStampInfo.GenTime.ToString();

                string policy = t.TimeStampInfo.Policy.ToString();
                //string obj = Global.ByteToString(t.TimeStampInfo.GetEncoded());
                //obj = addlinebreaks(obj, 32, 32);
                //string res4 = "Encoded timestamp: " + obj;

                //transactionDocument.Policy = policy;
                //transactionDocument.DateTimeSign = t.TimeStampInfo.GenTime;
                richTextBox2.Text = richTextBox2.Text + "Policy = " + policy + Environment.NewLine;
                richTextBox2.Text = richTextBox2.Text + "DateTimeSign = " + t.TimeStampInfo.GenTime + Environment.NewLine;
                richTextBox2.Text = richTextBox2.Text + res1 + Environment.NewLine;
                richTextBox2.Text = richTextBox2.Text + "TSA = " + t.TimeStampInfo.Tsa.Name.ToString() + Environment.NewLine;
                richTextBox2.Text = richTextBox2.Text + "URL Notarize = https://tsa.safestamper.com/" + Environment.NewLine;
                richTextBox2.Text = richTextBox2.Text + "URL Check = https://tsa.safecreative.org/?search=" + sHash;
            }
            catch (Exception ex)
            {
                richTextBox2.Text = ex.Message;
            }
           
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = false;
                xmlDoc.LoadXml(System.IO.File.ReadAllText(@"c:\de\CI_21284415-2_20190625175701.xml"));

                //XmlDocument xmlDocRD = new XmlDocument();
                //xmlDocRD.PreserveWhitespace = false;

                //xmlDocRD.LoadXml(xmlDoc.Fir.InnerXml);
                //XmlNode n = xmlDoc.GetElementsByTagName("ExtensionItem")[0].InnerText;
                XmlNodeList n = xmlDoc.GetElementsByTagName("ExtensionItem");
                System.Collections.Hashtable ht = new System.Collections.Hashtable();
                string s;
                foreach (XmlNode item in n)
                {
                    if (item.ChildNodes.Count > 1 &&  !string.IsNullOrEmpty(item.ChildNodes[1].InnerText))
                        ht.Add(item.ChildNodes[0].InnerText, item.ChildNodes[1].InnerText);
                    int i = 0;
                }
                string abs = "";

                 // Check arguments.
                //if (xmlDoc != null)
                //{
                //    //                        string sample = xmlDoc.GetElementsByTagName("Sample")[0].InnerText;
                //    string sample = xmlDocABS.GetElementsByTagName("Sample")[0].InnerText;


                //    int sampletype = Convert.ToInt32(xmlDocABS.GetElementsByTagName("SampleType")[0].InnerText);
                //    labSampleTypeAbs.Text = "Sample Type: " + Bio.Portal.Server.Api.Constant.MinutiaeType.GetDescription(sampletype);
                //    //Imagen Smple si aplica
                //    Bitmap bmpSample;
                //    switch (sampletype)
                //    {
                //        case 21: //Bio.Portal.Server.Api.Constant.MinutiaeType.MINUTIAETYPE_WSQ:
                //            Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                //            byte[] wsq = Convert.FromBase64String(sample);
                //            short w, h;
                //            byte[] raw;
                //            bool bOK = dec.DecodeMemory(wsq, out w, out h, out raw);
                //            bmpSample = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw, w, h);
                //            break;
                //        case 22: //Bio.Portal.Server.Api.Constant.MinutiaeType.MINUTIAETYPE_RAW:
                //            bmpSample = Bio.Core.Imaging.ImageProcessor.RawToBitmap(Convert.FromBase64String(sample), 512, 512);
                //            break;
                //        case 41: //Bio.Portal.Server.Api.Constant.MinutiaeType.MINUTIAETYPE_JPEG:
                //            byte[] byImg = Convert.FromBase64String(sample);
                //            System.IO.MemoryStream m = new System.IO.MemoryStream(byImg);
                //            bmpSample = new Bitmap(Image.FromStream(m));
                //            m.Close();
                //            break;
                //        default:
                //            bmpSample = null;
                //            break;
                //    }
                //    picSampleAbs.Image = bmpSample;

                //    //Barcode
                //    Bitmap barcodeBMP = null;
                //    string barcode = xmlDocABS.GetElementsByTagName("Barcode")[0].InnerText;
                //    if (!String.IsNullOrEmpty(barcode))
                //    {
                //        byte[] byImgQR = Convert.FromBase64String(barcode);
                //        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgQR);
                //        barcodeBMP = new Bitmap(Image.FromStream(m));
                //        m.Close();
                //    }
                //    picBarcodeABS.Image = barcodeBMP;

                //    //Foto
                //    Bitmap fotoBMP = null;
                //    string foto = xmlDocABS.GetElementsByTagName("Photografy")[0].InnerText;
                //    if (!String.IsNullOrEmpty(foto))
                //    {
                //        byte[] byImgFoto = Convert.FromBase64String(foto);
                //        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgFoto);
                //        fotoBMP = new Bitmap(Image.FromStream(m));
                //        m.Close();
                //    }
                //    this.picFotoABS.Image = fotoBMP;
            }
            catch (Exception ex)
            {

            }
        }
    }
}
