using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace TestXMLDSIG
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            var rootPatronFormat = uint.Parse("16", System.Globalization.NumberStyles.HexNumber);
            int i = 0;
            //System.Collections.Hashtable Ht = new System.Collections.Hashtable();
            //Ht.Add("k1", "v1");
            //Ht.Add("k2", "v2");
            //Ht.Add("k3", "v3");

            //foreach (var item in Ht)
            //{
            //    int i = 0;
            //}

            //0486dfbfbf2c447ab16a5d546266613d
            //System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(@"C:\TFSN\Notario Virtual\Proyecto\v5.5\Bio.Digital.Receipt\Config\Tmp\");
            //foreach (System.IO.FileInfo fileToDelete in di.GetFiles("0486dfbfbf2c447ab16a5d546266613d*"))
            //{
            //    System.IO.File.Delete(fileToDelete.FullName);

            //}

            //string sa = "BST - Contrato: 20091001100110";
            //string sa1 = sa.Substring(0, 10);


            //Bitmap b =
            //    new Bitmap(
            //        @"C:\Personal\NP\BioClientes\BST\Demo\Bio.Contrato\BIM\BIM\bin\Debug\tmp\20091031002948.huella.jpg");
            //Bitmap b1 = new Bitmap(64,77);


            //byte[] byImgRaw = RawImageManipulator.BitmapToRaw(new Bitmap(@"C:\Personal\NP\BioClientes\BST\Demo\Bio.Contrato\BIM\BIM\bin\Debug\tmp\20091031002948.huella.jpg"));
            //byte[] byImgRawChica = RawImageManipulator.Adjust(byImgRaw, 128, 144, 128 / 2, 144 / 2);
            //Bitmap bich = RawImageManipulator.RawToBitmap(byImgRawChica, 128 / 2, 144 / 2);
            //bich.Save(@"C:\Personal\NP\BioClientes\BST\Demo\Bio.Contrato\BIM\BIM\bin\Debug\tmp20091031002948.huella_chica.jpg");

            //string s = "<?xml version=\"1.0\" encoding=\"utf-8\"?><Rd Id=\"IDRDREF.v1\" " +
            //           "reciboID=\"112043347951183982674484979034418161202193260356750\" version=\"1.0\"" + 
            //           "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" +
            //           "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">";

            //string s1 = s.Substring(s.IndexOf("<Rd"));

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}