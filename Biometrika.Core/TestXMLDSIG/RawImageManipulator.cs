using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace TestXMLDSIG
{
	/// <summary>
	/// Manipulaci�n de imagenes raw, usado principalmente por algoritmos
	/// wsq, y afis.
	/// </summary>
	public sealed class RawImageManipulator
	{
		private RawImageManipulator()
		{
			
		}

		/// <summary>
		/// Convierte una imagen Raw de un ancho y alto en un bitmap de 24 bits por pixel
		/// del mismo tama�o.
		/// </summary>
		/// <param name="raw">La imagen raw, como escala de grises de 0(black)..255(white)</param>
		/// <param name="width">ancho de la imagen raw</param>
		/// <param name="height">altura de la imagen raw</param>
		/// <returns>Un Bitmap (.Net) con la imagen a 24bpp</returns>
		public static Bitmap RawToBitmap (byte[] raw, int width, int height)
		{
			Bitmap img = new Bitmap (width, height, PixelFormat.Format24bppRgb);
			BitmapData bmd = img.LockBits (new Rectangle (0, 0, width, height), ImageLockMode.ReadWrite, img.PixelFormat);

			unsafe
			{
				byte* imgPtr = (byte*) bmd.Scan0.ToPointer ();
				int stride = bmd.Stride;
				int imgPos = 0;
				int rawPos = 0;
				for (int row = 0; row < height; row++)
				{
					int rowPos = imgPos;
					for (int col = 0; col < width; col++)
					{
						byte val = raw[rawPos++];
						imgPtr[rowPos] = imgPtr[rowPos+1] = imgPtr[rowPos+2] = val;
						rowPos += 3;
					}
					imgPos += stride;
				}
			}
			img.UnlockBits (bmd);
			return img;
		}

		/// <summary>
		/// Recibe un bitmap (en cualquier tipo de formato de pixel) y entrega
		/// la imagen raw en formato de arreglo de bytes en escala de grises
		/// 0(black)...255(white)
		/// </summary>
		/// <param name="img">bitmap con la imagen</param>
		/// <returns>El arreglo de img.Width*img.Height bytes</returns>
		public static byte[] BitmapToRaw (Bitmap img)
		{
			if (img == null)
				return null;

			int width = img.Width;
			int height = img.Height;

			byte[] result = new byte[width*height];

			BitmapData bmd = img.LockBits (new Rectangle (0, 0, width, height), ImageLockMode.ReadOnly, img.PixelFormat);
			int step = 1;
			switch (img.PixelFormat)
			{
				case PixelFormat.Format8bppIndexed:
					step = 1;
					break;
				case PixelFormat.Format24bppRgb:
					step = 3;
					break;
				case PixelFormat.Format32bppArgb:
				case PixelFormat.Format32bppPArgb:
				case PixelFormat.Format32bppRgb:
					step = 4;
					break;
			}

			unsafe
			{
				byte* ptr = (byte*) bmd.Scan0.ToPointer ();
				int stride = bmd.Stride;
				if (stride < 0)
				{
					stride = - stride;
					int dstOffset = result.Length - width;
					int outOffset = 0;
					for (int y = height - 1; y >= 0; y--)
					{
						dstOffset = width*y;
						int offset = outOffset;
						for (int x = 0; x < width; x++)
						{
							int luma = (int) (ptr[offset]*0.3 + ptr[offset + 1]*0.59 + ptr[offset + 2]*0.11);
							result[dstOffset++] = (byte) (luma);
							offset += step;
						}
						outOffset += stride;
					}
				}
				else
				{
					int dstOffset = 0;
					int outOffset = 0;
					for (int y = 0; y < height; y++)
					{
						int offset = outOffset;
						for (int x = 0; x < width; x++)
						{
							int luma = (int) (ptr[offset]*0.3 + ptr[offset + 1]*0.59 + ptr[offset + 2]*0.11);
							result[dstOffset++] = (byte) (luma);
							offset += step;
						}
						outOffset += stride;
					}
				}
			}
			img.UnlockBits (bmd);
			return result;
		}

		/// <summary>
		/// Ajusta el tama�o de una imagen raw, esto sirve para cambiar el
		/// tama�o, si la imagen resultante es m�s grande se rellena con negro (0)
		/// </summary>
		/// <param name="raw">imagen raw original</param>
		/// <param name="wOrig">ancho de raw</param>
		/// <param name="hOrig">alto de raw</param>
		/// <param name="wDst">ancho deseado</param>
		/// <param name="hDst">alto deseado</param>
		/// <returns>un arreglo de bytes de tama�o wDst*hDst</returns>
		public static byte[] Adjust (byte[] raw, int wOrig, int hOrig, int wDst, int hDst)
		{
			
			byte[] result = new byte[wDst*hDst];
			int offset = 0;
			int doffset = 0;
			int top = Math.Min(hDst, hOrig);
			int width = Math.Min(wDst, wOrig);
			for (int i = 0; i < top; i++)
			{
				Buffer.BlockCopy(raw, offset, result, doffset, width);
				offset += wOrig;
				doffset += wDst;
			}
			return result;	
		}

	}
}
