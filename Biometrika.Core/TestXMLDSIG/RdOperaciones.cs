using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace TestXMLDSIG
{
    public partial class Rd
    {
        public static string SaveConfigFile(Rd obj)
        {
            string sRes = null;

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Rd));
                TextWriter writer = new StreamWriter("c:\\temp\\TestSer.xml");
                serializer.Serialize(writer, obj);
                writer.Close();
            }
            catch (Exception ex)
            {
                sRes = "Error al Serializar archivo de configuracion [" + ex.Message + "]";
            }
            return sRes;
        }

        public static Rd LoadConfigFile(string path)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Rd));
                TextReader reader = new StreamReader(path);
                Rd cf = (Rd)serializer.Deserialize(reader);
                reader.Close();
                return cf;
            }
            catch
            {
                return null;
            }
        }
    }
}
