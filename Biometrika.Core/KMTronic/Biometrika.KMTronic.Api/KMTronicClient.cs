﻿using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO.Ports;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Biometrika.KMTronic.Api
{
    public class KMTronicClient
    {

        List<EventRele> _FIFO_QUEUE = new List<EventRele>();
        bool _RunningHandleQueue = false;

        object _lock = new object();
        private static readonly ILog log = LogManager.GetLogger(typeof(KMTronicClient));
        private string _MANUAL_COMPORT = null; //"COM8"; 
        private int _Q_RELES = 4; //"COM8"; 
        private SerialPort ComPort = new SerialPort();

        static private System.Timers.Timer _TIMER_TO_CLOSE = new System.Timers.Timer();
        private System.Threading.Timer _TIMER_TH_TO_CLOSE;

        private int[] qOpen = new int[4];
        private int[] qClose = new int[4];

        public string AddItemToQueue(int _rele, int _timer)
        {
            try
            {
                lock (_FIFO_QUEUE)
                {
                    EventRele eventNew = new EventRele()
                    {
                        rele = _rele,
                        timer = _timer
                    };
                    log.Debug("KMTronicClient.AddItemToQueue - Adding rele = " + _rele);
                    _FIFO_QUEUE.Add(eventNew);
                }
                _RunningHandleQueue = true;
                HandleContinueQueue();
            }
            catch (Exception ex)
            {
                log.Error("KMTronicClient.AddItemToQueue Excp [" + ex.Message + "]");
                return "NOOK";
            }
            return "OK";
        }

        private void HandleContinueQueue()
        {
            EventRele _currentEvent = null;
            string msgerr = null;
            string sRet = null;
            try
            {
                while (_RunningHandleQueue)
                {
                    if (_FIFO_QUEUE != null && _FIFO_QUEUE.Count > 0)
                    {
                        lock ( _FIFO_QUEUE)
                        {
                            log.Debug("KMTronicClient.HandleContinueQueue - Getting event from queue => len = " + _FIFO_QUEUE.Count() + "...");
                            _currentEvent = _FIFO_QUEUE[0];
                            log.Debug("KMTronicClient.HandleContinueQueue - Removing event 0 from queue => rele = " + _currentEvent.rele + "...");
                            _FIFO_QUEUE.RemoveAt(0);
                            log.Debug("KMTronicClient.HandleContinueQueue - Removed event 0 from queue => len = " + _FIFO_QUEUE.Count() + "...");
                        }

                        log.Debug("KMTronicClient.HandleContinueQueue - Open rele = " + _currentEvent.rele  + " / time = " + _currentEvent.timer + "...");
                        sRet = OpenClose(_currentEvent.rele.ToString(), "1", out msgerr);
                        System.Threading.Thread.Sleep(_currentEvent.timer);
                        log.Debug("KMTronicClient.HandleContinueQueue - Close rele = " + _currentEvent.rele + "...");
                        sRet = OpenClose(_currentEvent.rele.ToString(), "0", out msgerr);
                    }
                    else
                    {
                        log.Debug("KMTronicClient.HandleContinueQueue - Sale de ciclo!");
                        log.Debug("KMTronicClient.HandleContinueQueue - Opens => r1:" + qOpen[0] + "/r2:" + qOpen[1] + "/r3:" + qOpen[2] + "/r4:" + qOpen[3]);
                        log.Debug("KMTronicClient.HandleContinueQueue - Closes => r1:" + qClose[0] + "/r2:" + qClose[1] + "/r3:" + qClose[2] + "/r4:" + qClose[3]);
                        _RunningHandleQueue = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("KMTronicClient.HandleContinueQueue Excp [" + ex.Message + "]");
            }
        }



        public string[] GetComs()
        {
            return SerialPort.GetPortNames();
        }

        public string SetQRele(int cantidad)
        {
            _Q_RELES = cantidad;
            return "OK";
        }

        public string Release()
        {
            try
            {
                if (ComPort.IsOpen)
                {
                    ComPort.Close();
                }
                _RunningHandleQueue = false;
            }
            catch (Exception ex)
            {

            }
            return "OK";
        }

        public string SetManualCOM(string comport)
        {
            _MANUAL_COMPORT = comport;
            log.Debug("setManualCOM - Seteado COM Manual => " + _MANUAL_COMPORT);
            try
            {
                ComPort.BaudRate = 9600;
                ComPort.DataBits = 8;
                ComPort.ReadTimeout = 500;
                ComPort.WriteTimeout = 500;
                ComPort.PortName = _MANUAL_COMPORT;
                if (!ComPort.IsOpen) ComPort.Open();
                //ComPort.Write("99");
                System.Threading.Thread.Sleep(100);
                string message = ComPort.ReadExisting();
                log.Debug("KMTronicClient.setManualCOM - rpta = " + message);
                log.Debug("KMTronicClient.setManualCOM - Formateamos la respuesta para hacerla generica...");

                string msgFormateado = message.Replace(Environment.NewLine, "");
                log.Debug("KMTronicClient.setManualCOM - Mensaje formateado: " + msgFormateado);
                ComPort.Close();

                //_TIMER_TO_CLOSE.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                //_TIMER_TO_CLOSE.Elapsed += (sender, e) => OnTimedEvent(sender, e, "", 0);
                _TIMER_TO_CLOSE.Interval = 3000;
                _TIMER_TO_CLOSE.Enabled = false;
                ComPort.Open();
                _RunningHandleQueue = true; 
                //_TIMER_TH_TO_CLOSE = new System.Threading.Timer()

            }
            catch (Exception ex)
            {
                _MANUAL_COMPORT = null;
                log.Error("KMTronicClient.setManualCOM Excp - " + ex.Message);
            }
            return _MANUAL_COMPORT;
        }

       
        /// <summary>
        /// Funcion de apertura, recive Rele a accionar y la accion a ejecutar(Abrir, cerrar)
        /// </summary>
        /// <param name="rele">Numero del rele a operar, ej: 01 para el rele 1 o 02 para el rele 2</param>
        /// <param name="accion">Accion a ejecutar, ej: 1 abrir, 0 cerrar</param>
        /// <param name="msgError">Msg de vuelta en el caso de errores</param>
        /// <returns>Retorna el msg entregado por la placa</returns>
        public string OpenClose(string rele, string accion, out string msgError)
        {
            string _command = "NOOK";
            msgError = "";
            string resultado = "";

            try
            {
                log.Debug("Intentamos abrir el puerto: " + ComPort.PortName);
                //if (ComPort.IsOpen)
                //{
                //    return _command;
                //}
                lock (_lock)
                {
                    //ComPort.Open();
                    log.Debug("Puerto abierto, enviamos: rele=" + rele + "/accion=" + accion);
                    //ComPort.Write(rele + accion);
                    switch (Convert.ToInt32(accion))
                    {
                        case 1: //Open
                            switch (Convert.ToInt32(rele))
                            {
                                case 1:
                                    _command = "FF 01 01";
                                    break;
                                case 2:
                                    _command = "FF 02 01";
                                    break;
                                case 3:
                                    _command = "FF 03 01";
                                    break;
                                case 4:
                                    _command = "FF 04 01";
                                    break;
                                default:
                                    _command = "FF 01 01";
                                    break;
                            }
                            qOpen[Convert.ToInt32(rele)-1]++; 
                            break;
                        default: //Close
                            switch (Convert.ToInt32(rele))
                            {
                                case 1:
                                    _command = "FF 01 00";
                                    break;
                                case 2:
                                    _command = "FF 02 00";
                                    break;
                                case 3:
                                    _command = "FF 03 00";
                                    break;
                                case 4:
                                    _command = "FF 04 00";
                                    break;
                                default:
                                    _command = "FF 01 00";
                                    break;
                            }
                            qClose[Convert.ToInt32(rele)-1]++;
                            break;
                    }

                    if (!string.IsNullOrEmpty(_command))
                    {
                        if (_SerialWrite(_StringToHEX(_command), 0, _StringToHEX(_command).Length))
                        {
                            resultado = "OK";
                        }
                        else
                        {
                            resultado = "NOOK";
                        }
                    }

                    System.Threading.Thread.Sleep(100);

                    //resultado = ComPort.ReadExisting();
                    //ComPort.Close();
                }
            }
            catch (Exception ex)
            {
                resultado = "OpenClose Excp [" + ex.Message + "]";
                log.Error(resultado);
            }
            log.Debug("OpenClose resultado: " + resultado);
            return resultado;
        }

        /// <summary>
        /// Funcion de apertura con timer, se le entrega el valor del rele y el tiempo que este estara abierto
        /// </summary>
        /// <param name="rele">Rele a operar, ej: 010 rele 1, 020 rele 2</param>
        /// <param name="timer">Tiempo de apertura en milisegundo</param>
        /// <param name="msgError">Msg de error si lo hubiera</param>
        /// <returns>Retorna msg de la placa arduino</returns>
        public string OpenDelay(string rele, string timer, out string msgError)
        {
            string resultado = "";
            msgError = "";

            try
            {
                lock (_lock)
                {
                    log.Debug("Intentamos abrir el puerto: " + ComPort.PortName);
                    //ComPort.Open();
                    log.Debug("Puerto abierto, enviamos pulso al rele: " + rele + ", tiempo: " + timer);
                    //ComPort.Write(rele + timer);
                    resultado = OpenClose(rele, "1", out msgError);

                    SetThreadToClose(rele, Convert.ToInt32(timer));
                    //System.Threading.Thread.Sleep(Convert.ToInt32(timer) + 100);
                    //resultado = OpenClose(rele, "0", out msgError);
                    //resultado = ComPort.ReadExisting();
                    //ComPort.Close();
                    log.Debug("Escuchamos: " + resultado);
                }

            }
            catch (Exception ex)
            {
                resultado = ex.Message;
                log.Error(ex.Message);
            }

            return resultado;
        }

     
        /// <summary>
        /// Funcion para abrir o cerrar todos los reles al mismo tiempo
        /// </summary>
        /// <param name="accion">Accion a realizar, ej: true para brir, false para cerrar</param>
        /// <param name="msgError">Msg de error si lo hubiera</param>
        /// <returns>Retorna msg de la placa arduino</returns>
        public string OpenCloseAll(bool accion, out string msgError)
        {
            string resultado = "";
            msgError = "";
            string accionAux = "";
            string que = "";

            try
            {
                log.Debug("OpenCloseAll IN...");
                if (accion)
                {
                    for (int i = 1; i <= _Q_RELES; i++)
                    {
                        resultado = OpenClose(i.ToString(), "1", out msgError);
                    }
                }
                else
                {
                    for (int i = 1; i <= _Q_RELES; i++)
                    {
                        resultado = OpenClose(i.ToString(), "0", out msgError);
                    }
                }

            }
            catch (Exception ex)
            {
                resultado = "OpenCloseAll Excp [" + ex.Message + "]";
                log.Error(ex.Message);
            }
            log.Debug("OpenCloseAll OUT! resultado = " + resultado);
            return resultado;
        }



        /// <summary>
        /// Funcion que entrega la informacion del hardware de la placa o su estado
        /// </summary>
        /// <param name="accion">Accion a realizar, ej: true para informacion, false para status</param>
        /// <param name="msgError">Msg de error si lo hubiera</param>
        /// <returns>Retorna msg de la placa arduino</returns>
        public string GetInfoStatus(bool accion, out string msgError)
        {
            string resultado = "";
            msgError = "";
            string accionAux = "";
            string que = "";
            int sleepTime = 0;


            try
            {
                log.Debug("GetInfoStatus IN! accion = " + (accion ? "Info" : "Status"));
                if (accion)
                {
                    //que = "Info";
                    //accionAux = "86";
                    //sleepTime = 1000;
                    resultado = "Biometrika.KMTronic.Api v1" + Environment.NewLine +
                                "Autor: Gustavo Suhit" + Environment.NewLine +
                                "Fecha: 12/2022"; 
                }
                else
                {
                    //que = "Status";
                    //accionAux = "87";
                    //sleepTime = 100;
                    bool isFirst = true;
                    string _Command = null;
                    string _Aux = null;
                    byte[] rxByte = new byte[1];

                    //ComPort.Open();
                    for (int i = 1; i <= _Q_RELES; i++)
                    {
                        _Aux = null;
                        _Command = "FF " + "0" + i.ToString() + " 03";
                        if (!_SerialWrite(_StringToHEX(_Command), 0, _StringToHEX(_Command).Length))
                        {
                            resultado = "NOOK";
                            return resultado;
                        }

                        System.Threading.Thread.Sleep(50);

                        while (_SerialBytesToRead() > 0)
                        {
                            rxByte[0] = _SerialRead();
                            _Aux += _ByteToHex(rxByte).ToString();
                            if (_Aux != null && _Aux.Length > 0)
                            {
                                _Aux = _Aux.EndsWith("01") ? "Rele " + i.ToString() + " Abierto" : "Rele " + i.ToString() + " Cerrado";
                            }
                        }

                        if (isFirst)
                        {
                            resultado = _Aux;
                            isFirst = false;
                        } else
                        {
                            resultado += Environment.NewLine + _Aux;
                        }
                    }
                }
                log.Debug("Escuchamos: " + resultado);
            }
            catch (Exception ex)
            {
                resultado = "GetInfoStatus Excp [" + ex.Message + "]";
                log.Error(ex.Message);
            }
            //if (ComPort.IsOpen)
            //    ComPort.Close();
            log.Debug("GetInfoStatus OUT! resultado = " + resultado);
            return resultado;
        }

        public string OpenCloseAllDelay(string timer, out string msgError)
        {
            string resultado = "";
            msgError = "";
            string accionAux = "";
            string que = "";

            try
            {
                log.Debug("OpenCloseAllDelay IN...");
  
                for (int i = 1; i <= _Q_RELES; i++)
                {
                    resultado = OpenClose(i.ToString(), "1", out msgError);
                }
                System.Threading.Thread.Sleep(Convert.ToInt32(timer) + 100);
                for (int i = 1; i <= _Q_RELES; i++)
                {
                    resultado = OpenClose(i.ToString(), "0", out msgError);
                }
               
            }
            catch (Exception ex)
            {
                resultado = "OpenCloseAllDelay Excp [" + ex.Message + "]";
                log.Error(ex.Message);
            }
            log.Debug("OpenCloseAllDelay OUT! resultado = " + resultado);
            return resultado;

            //string resultado = "";
            //msgError = "";

            //try
            //{
            //    log.Debug("Intentamos abrir el puerto: " + ComPort.PortName);
            //    ComPort.Open();
            //    log.Debug("Puerto abierto, enviamos pulso a todos los rele, tiempo: " + timer);
            //    ComPort.Write("851" + timer);
            //    System.Threading.Thread.Sleep(100);

            //    resultado = ComPort.ReadExisting();
            //    ComPort.Close();
            //    log.Debug("Escuchamos: " + resultado);
            //}
            //catch (Exception ex)
            //{
            //    resultado = ex.Message;
            //    log.Error(ex.Message);
            //}

            //return resultado;
        }

        #region Private 

        private void OnTimedEvent(object sender, ElapsedEventArgs e, string rele, int timer)
        {
            lock (_lock)
            {
                int i = 0;
                string msgError;
                string resultado = OpenClose(rele, "0", out msgError);
                _TIMER_TO_CLOSE.Stop();
                _TIMER_TO_CLOSE.Enabled = false;
                _TIMER_TO_CLOSE.Elapsed -= null;
            }
        }


        private void SetThreadToClose(string rele, int timer)
        {
            try
            {
                _TIMER_TO_CLOSE.Elapsed += (sender, e) => OnTimedEvent(sender, e, rele, timer);
                _TIMER_TO_CLOSE.Interval = timer;
                _TIMER_TO_CLOSE.Enabled = true;
                _TIMER_TO_CLOSE.Start();
                
            }
            catch (Exception ex)
            {

                
            }
        }
        private bool _SerialWrite(byte[] dataBuff, int offset, int count)
        {
            try
            {
                if (ComPort.IsOpen)
                { ComPort.Write(dataBuff, offset, count); return true; }
                else
                { return false; }
            }

            catch
            {
                return false;
            }
        }

        private byte _SerialRead()
        {
            return (byte)ComPort.ReadByte();
        }

        private int _SerialBytesToRead()
        {
            return ComPort.BytesToRead;
        }


        private bool _SerialIsOpen()
        {
            if (ComPort.IsOpen)
                return true;
            else
                return false;
        }

        private string _ByteToHex(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            foreach (byte b in data)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0').PadRight(3, ' '));
            return sb.ToString().ToUpper();

        }


        public byte[] _StringToHEX(string s)
        {


            char[] splitter = { ',', '-', ' ' };
            string[] splitS = s.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

            byte[] buff = new byte[splitS.Length];

            for (int i = 0; i < splitS.Length; i++)
            {
                try
                {
                    buff[i] = Convert.ToByte(splitS[i], 16);
                }

                catch
                {
                    buff[i] = 0x00;
                }
            }
            return buff;
        }



        #region descarte

        /// <summary>
        /// Analiza los puertos COM informados para buscar el que sea de Arduino
        /// </summary>
        /// <returns>COM utilizado por la placa</returns>
        //public string getCOM(string comAssigned = null)
        //{
        //    string deviceId = null;

        //    log.Debug("getCOM IN - Analaizando puertos");
        //    Hashtable _HTDevices = null;
        //    try
        //    {
        //        _MANUAL_COMPORT = comAssigned;
        //        //Recupero lista de COMs que provengan desde USB Adapter Converter
        //        log.Debug("getCOM - Entra a GetCOMFromUSBAdapterConverter...");
        //        deviceId = GetCOMFromUSBAdapterConverter(); // (_HTDevices, out _HTDevices);

        //        if (string.IsNullOrEmpty(deviceId))
        //        {
        //            log.Debug("getCOM - Entra a GetCOMFromSerialAdapter xq no encontro Arduino...");
        //            deviceId = GetCOMFromSerialAdapter(); // (_HTDevices, out _HTDevices);
        //        }

        //        if (string.IsNullOrEmpty(deviceId) && !string.IsNullOrEmpty(_MANUAL_COMPORT))
        //        {
        //            deviceId = setManualCOM(_MANUAL_COMPORT);
        //            log.Debug("getCOM - Entra a SetManaul COM => " +
        //                (string.IsNullOrEmpty(_MANUAL_COMPORT) ? "NULL" : _MANUAL_COMPORT));
        //        }

        //        #region DescarteOLD
        //        //ManagementScope connectionScope = new ManagementScope();
        //        //SelectQuery serialQuery = new SelectQuery("SELECT * FROM Win32_SerialPort");
        //        //ManagementObjectSearcher searcher = new ManagementObjectSearcher(connectionScope, serialQuery);

        //        //try
        //        //{
        //        //    foreach (ManagementObject item in searcher.Get())
        //        //    {
        //        //        string desc = item["Description"].ToString();
        //        //        string deviceId = item["DeviceID"].ToString();
        //        //        log.Debug(desc + ": " + deviceId);

        //        //        if (desc.Contains("Arduino"))
        //        //        {
        //        //            ComPort.PortName = deviceId;
        //        //            ComPort.Open();
        //        //            ComPort.Write("99");
        //        //            System.Threading.Thread.Sleep(100);
        //        //            string message = ComPort.ReadExisting();
        //        //            log.Debug(message);
        //        //            log.Debug("Formateamos la respuesta para hacerla generica");

        //        //            string msgFormateado = message.Replace(Environment.NewLine, "");
        //        //            log.Debug("Mensaje formateado: " + msgFormateado);

        //        //            if (msgFormateado.CompareTo("Arduino UNO") == 0 || msgFormateado.CompareTo("Arduino RedBoard") == 0)
        //        //            {
        //        //                log.Debug("COM seleccionada: " + deviceId);
        //        //                ComPort.Close();
        //        //                return deviceId;
        //        //            }
        //        //            else
        //        //            {
        //        //                log.Debug("No se encuentra arduino");
        //        //                log.Debug("Si arduino esta conectado y aparece en el administrador de dispositivos puede que este bugeado el puerto, para solucioanr desconecte arduino por 10 segundos o reinicie el pc donde esta conectado");
        //        //                ComPort.Close();
        //        //                return null;
        //        //            }
        //        //        }
        //        //    }
        //        #endregion DescarteOLD
        //    }
        //    catch (ManagementException e)
        //    {
        //        log.Error(e.Message);
        //        log.Error("Si arduino esta conectado y aparece en el administrador de dispositivos puede que este bugeado el puerto, para solucioanr desconecte arduino por 10 segundos o reinicie el pc donde esta conectado");
        //        return null;
        //    }

        //    return deviceId;
        //}


        //private string GetCOMFromSerialAdapter() // (Hashtable inhTDevices, out Hashtable outhTDevices)
        //{
        //    string deviceId = null;
        //    //outhTDevices = null;
        //    //Hashtable _HT_DEVICES;
        //    try
        //    {
        //        log.Debug("KMTronicClient.GetCOMFromSerialAdapter IN...");

        //        //if (inhTDevices != null)
        //        //{
        //        //    _HT_DEVICES = inhTDevices;
        //        //} else
        //        //{
        //        //    _HT_DEVICES = new Hashtable();
        //        //}

        //        log.Debug("KMTronicClient.GetCOMFromSerialAdapter - Select Serial Devices...");
        //        ManagementScope connectionScope = new ManagementScope();
        //        SelectQuery serialQuery = new SelectQuery("SELECT * FROM Win32_SerialPort");
        //        ManagementObjectSearcher searcher = new ManagementObjectSearcher(connectionScope, serialQuery);

        //        log.Debug("KMTronicClient.GetCOMFromSerialAdapter - Recorro lista si hay...");
        //        var l = searcher.Get();
        //        foreach (ManagementObject item in searcher.Get())
        //        {
        //            string desc = item["Description"].ToString();
        //            deviceId = item["DeviceID"].ToString();
        //            log.Debug("KMTronicClient.GetCOMFromSerialAdapter - Leido => " + desc + ": " + deviceId);

        //            if (desc.Contains("Arduino") || desc.Contains("CH340") ||
        //                (!string.IsNullOrEmpty(_MANUAL_COMPORT) && _MANUAL_COMPORT.Equals(deviceId)))
        //            {
        //                log.Debug("KMTronicClient.GetCOMFromSerialAdapter - Ingreso a procesar...");
        //                ComPort.PortName = deviceId;
        //                ComPort.Open();
        //                ComPort.Write("99");
        //                System.Threading.Thread.Sleep(100);
        //                string message = ComPort.ReadExisting();
        //                log.Debug("KMTronicClient.GetCOMFromSerialAdapter - " + message);
        //                log.Debug("KMTronicClient.GetCOMFromSerialAdapter - Formateamos la respuesta para hacerla generica...");

        //                string msgFormateado = message.Replace(Environment.NewLine, "");
        //                log.Debug("KMTronicClient.GetCOMFromSerialAdapter - Mensaje formateado: " + msgFormateado);

        //                if (msgFormateado.CompareTo("Arduino UNO") == 0 || msgFormateado.CompareTo("Arduino RedBoard") == 0)
        //                {
        //                    log.Debug("KMTronicClient.GetCOMFromSerialAdapter - COM seleccionada: " + deviceId);
        //                    ComPort.Close();
        //                    break;
        //                    //log.Debug("KMTronicClient.GetCOMFromSerialAdapter - Agrego a lista [len=" + _HT_DEVICES.Count.ToString() + "...");
        //                    //if (!_HT_DEVICES.ContainsKey(deviceId))
        //                    //{
        //                    //    _HT_DEVICES.Add(deviceId, deviceId);
        //                    //} else
        //                    //{
        //                    //    log.Warn("KMTronicClient.GetCOMFromSerialAdapter - Ya existe " + deviceId);
        //                    //}
        //                    //log.Debug("KMTronicClient.GetCOMFromSerialAdapter - Agrego a lista [len=" + _HT_DEVICES.Count.ToString() + "...");
        //                }
        //                else
        //                {
        //                    log.Debug("KMTronicClient.GetCOMFromSerialAdapter - No parece ser Arduino [" + msgFormateado + "]");
        //                    log.Debug("Si arduino esta conectado y aparece en el administrador de dispositivos puede que este bugeado el puerto, para solucioanr desconecte arduino por 10 segundos o reinicie el pc donde esta conectado");
        //                    ComPort.Close();
        //                    deviceId = null;
        //                }
        //            }
        //            else
        //            {
        //                log.Debug("KMTronicClient.GetCOMFromSerialAdapter - El dospositivo: " + desc + " NO es arduino => Se descarta!");
        //                deviceId = null;
        //            }
        //        }
        //        return deviceId;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("KMTronicClient.GetCOMFromSerialAdapter - Error: " + ex.Message);
        //        return null;
        //    }
        //}

        //private string GetCOMFromUSBAdapterConverter() //(Hashtable inhTDevices, out Hashtable outhTDevices)
        //{
        //    string deviceId = null;
        //    //outhTDevices = null;
        //    //Hashtable _HT_DEVICES;
        //    try
        //    {
        //        log.Debug("KMTronicClient.GetCOMFromUSBAdapterConverter IN...");

        //        //if (inhTDevices != null)
        //        //{
        //        //    _HT_DEVICES = inhTDevices;
        //        //}
        //        //else
        //        //{
        //        //    _HT_DEVICES = new Hashtable();
        //        //}

        //        log.Debug("KMTronicClient.GetCOMFromUSBAdapterConverter - Select USB Adapter Serial Devices...");
        //        ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\WMI",
        //                                                                         "SELECT * FROM MSSerial_PortName");

        //        log.Debug("KMTronicClient.GetCOMFromUSBAdapterConverter - Recorro lista si hay...");
        //        var l = searcher.Get();
        //        foreach (ManagementObject item in searcher.Get())
        //        {
        //            string desc = item["InstanceName"].ToString();
        //            deviceId = item["PortName"].ToString();
        //            log.Debug("KMTronicClient.GetCOMFromUSBAdapterConverter - Leido => desc=" + desc + ": deviceId=" + deviceId);

        //            if (desc.Contains("USB") ||
        //                desc.Contains("CH340") ||
        //                (!string.IsNullOrEmpty(_MANUAL_COMPORT) && _MANUAL_COMPORT.Equals(deviceId)))
        //            {
        //                log.Debug("KMTronicClient.GetCOMFromUSBAdapterConverter - Ingreso a procesar xq es USB...");
        //                ComPort.PortName = deviceId;
        //                ComPort.Open();
        //                ComPort.Write("99");
        //                System.Threading.Thread.Sleep(100);
        //                string message = ComPort.ReadExisting();
        //                log.Debug("KMTronicClient.GetCOMFromUSBAdapterConverter - ComPort.ReadExisting ret => " + message);
        //                log.Debug("KMTronicClient.GetCOMFromUSBAdapterConverter - Formateamos la respuesta para hacerla generica...");

        //                string msgFormateado = message.Replace(Environment.NewLine, "");
        //                log.Debug("KMTronicClient.GetCOMFromUSBAdapterConverter - Mensaje formateado: " + msgFormateado);

        //                if (msgFormateado.CompareTo("Arduino UNO") == 0 || msgFormateado.CompareTo("Arduino RedBoard") == 0
        //                    || msgFormateado.CompareTo("Biometrika Arduino") == 0)
        //                {
        //                    log.Debug("KMTronicClient.GetCOMFromUSBAdapterConverter - Encontro un Arduino => COM seleccionada: " + deviceId);
        //                    ComPort.Close();
        //                    break;
        //                    //log.Debug("KMTronicClient.GetCOMFromUSBAdapterConverter - Agrego a lista [len=" + _HT_DEVICES.Count.ToString() + "...");
        //                    //if (!_HT_DEVICES.ContainsKey(deviceId))
        //                    //{
        //                    //    _HT_DEVICES.Add(deviceId, deviceId);
        //                    //}
        //                    //else
        //                    //{
        //                    //    log.Warn("KMTronicClient.GetCOMFromUSBAdapterConverter - Ya existe " + deviceId);
        //                    //}
        //                    //log.Debug("KMTronicClient.GetCOMFromUSBAdapterConverter - Agrego a lista [len=" + _HT_DEVICES.Count.ToString() + "...");
        //                }
        //                else
        //                {
        //                    log.Debug("KMTronicClient.GetCOMFromUSBAdapterConverter - No parece ser Arduino [" + msgFormateado + "]");
        //                    log.Debug("Si arduino esta conectado y aparece en el administrador de dispositivos puede que este bugeado el puerto, para solucioanr desconecte arduino por 10 segundos o reinicie el pc donde esta conectado");
        //                    ComPort.Close();
        //                    deviceId = null;
        //                }
        //            }
        //            else
        //            {
        //                log.Debug("KMTronicClient.GetCOMFromUSBAdapterConverter - El dospositivo: " + desc + " NO es arduino => Se descarta!");
        //                deviceId = null;
        //            }
        //        }
        //        return deviceId;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("KMTronicClient.GetCOMFromUSBAdapterConverter - Error: " + ex.Message);
        //        return null;
        //    }
        //}
        #endregion descarte

        #endregion Private
    }
}
