﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using WSCT.ISO7816;
using System.IO;
using NLog;
using NLog.LayoutRenderers;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Macs;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;

namespace WSCT.Core.ConsoleTests
{
    public class SecureMessagingWrapper
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private HMACSHA1 _ksEnc;
        private HMACSHA1 _ksMac;
        private long _ssc;
        public TripleDESCryptoServiceProvider TDESAlgorithm;

        private bool checkMac(byte[] rapdu, byte[] cc1)
	    {
		    try 
            {
                ByteArrayOutputStream bOut = new ByteArrayOutputStream();
                DataOutputStream dataOut = new DataOutputStream(bOut);
                _ssc++;
                dataOut.WriteLong(_ssc);
                byte[] paso = new byte[rapdu.Length - 2 - 8];
                Array.Copy(rapdu, 0,paso,0, (rapdu.Length - 2 - 8 ));
                byte[] paddedData = Padding(paso);
                dataOut.Write(paddedData, 0, paddedData.Length);
                dataOut.Flush();
                /* Compute cryptographic checksum... */
                IMac macProvider = new ISO9797Alg3Mac(new DesEngine());
                macProvider.Init(new KeyParameter(_ksMac.Key));
                macProvider.Reset();

                macProvider.BlockUpdate(bOut.ToByteArray(), 0,bOut.ToByteArray().Length);
                byte[] cc2 = new byte[macProvider.GetMacSize()];
                macProvider.DoFinal(cc2, 0);

                //mac.init(ksMac);
                //byte[] cc2 = mac.doFinal(bOut.toByteArray());
                //dataOut.close();
                //return Arrays.equals(cc1, cc2);
                return Enumerable.SequenceEqual(cc1, cc2);
		    }
            catch (IOException ioe) {
                logger.Error(ioe, "Error al llamar checkMac");
			    return false;
		    }
	    }
        private short readDO99(DataInputStream input)
        {
            int length = input.Read();
		    if (length != 2) {
			    throw new Exception("DO'99 wrong length");
		    }
            byte sw1 = (byte)input.Read();
            byte sw2 = (byte)input.Read();
		    return (short) (((sw1 & 0x000000FF) << 8) | (sw2 & 0x000000FF));
	    }
        private byte[] readDO8E(DataInputStream input) 
        {
            int length = input.Read();
		    if (length != 8) {
			    throw new Exception("DO'8E wrong length");
		    }
		    byte[] cc1 = new byte[8];
            input.Read(cc1, 0, cc1.Length);
		    return cc1;
	}

        private byte[] readDO87(DataInputStream inputStream, bool do85) 
        {
		    /* Read length... */
		    int length = 0;
            int buf = inputStream.Read();
		    if ((buf & 0x00000080) != 0x00000080) 
            {
			    /* Short form */
			    length = buf;
			    if(!do85) 
                {
                    buf = inputStream.Read(); ; /* should be 0x01... */
				    if (buf != 0x01) 
                    {
                        throw new ExecutionEngineException("DO'87 expected 0x01 marker, found ");
				    }
			    }
		    }
            else
            {
			    /* Long form */
			    int lengthBytesCount = buf & 0x0000007F;
			    for (int i = 0; i < lengthBytesCount; i++) {
                    length = (length << 8) | inputStream.Read();
			    }
			    if(!do85)
                {
				    buf = inputStream.Read(); /* should be 0x01... */
				    if (buf != 0x01) {
					    throw new Exception("DO'87 expected 0x01 marker");
				    }
			    }
		    }
		    if(!do85) {
			    length--; /* takes care of the extra 0x01 marker... */
		    }
		    /* Read, decrypt, unpad the data... */
		    byte[] ciphertext = new byte[length];
		    //inputStream.Read(ciphertext,0,ciphertext.Length);
            inputStream.ReadFully(ciphertext,0,ciphertext.Length);

            MemoryStream memoryStream = new MemoryStream();

            //CryptoStream cryptoStream_Encryptor = new CryptoStream(memoryStream, TDESAlgorithm.CreateEncryptor(), CryptoStreamMode.Write);
            //cryptoStream_Encryptor.Write(ciphertext, 0, ciphertext.Length);
            //cryptoStream_Encryptor.Close();
            byte[] test = new byte[length];
            TDESAlgorithm.Clear();
            TDESAlgorithm.Key = _ksEnc.Key;
            TDESAlgorithm.IV = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            TDESAlgorithm.Mode = CipherMode.CBC;
            TDESAlgorithm.Padding = PaddingMode.None;
            CryptoStream cryptoStream_Encryptor = new CryptoStream(memoryStream, TDESAlgorithm.CreateDecryptor(), CryptoStreamMode.Write);            
            cryptoStream_Encryptor.Write(ciphertext, 0, length);
            memoryStream.Close();
           
            byte[] paddedData = memoryStream.ToArray();                      
		    
            byte[] data = unpad(paddedData);
		    return data;
	    }

        public static byte[] Padding(byte[] arreglo,int offset, int length)
        {
            byte[] res = null;
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(arreglo, 0, arreglo.Length);
                using (BinaryWriter bw = new BinaryWriter(ms))
                {
                    bw.Write((byte)0x80);
                    while (ms.Length % 8 != 0)
                    {
                        bw.Write((byte)0x00);
                    }

                }
                res = ms.ToArray();
            }
            return res;
		    
	    }

        public  byte[] unpad(byte[] input) {
		    int i = input.Length - 1;
            while (i >= 0 && input[i] == 0x00) {
			    i--;
		    }   
            if ((input[i] & 0xFF) != 0x80) {
			    //throw new IllegalStateException("unpad expected constant 0x80, found 0x" + Integer.toHexString((in[i] & 0x000000FF)) + "\nDEBUG: in = " + Hex.bytesToHexString(in) + ", index = " + i);
		    }
		    byte[] output = new byte[i];
            Buffer.BlockCopy(input, 0, output, 0, i);
		    return output;
	}

        public byte[] Padding(byte[] arreglo)
        {
            byte[] res = null;
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(arreglo, 0, arreglo.Length);
                using (BinaryWriter bw = new BinaryWriter(ms))
                {
                    bw.Write((byte)0x80);
                    while (ms.Length % 8 != 0)
                    {
                        bw.Write((byte)0x00);
                    }

                }
                res = ms.ToArray();
            }
            return res;
        }

        public SecureMessagingWrapper(HMACSHA1 ksEnc, HMACSHA1 ksMac, long ssc)
        {
            _ksEnc = ksEnc;
            _ksMac = ksMac;
            _ssc = ssc;
            TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = ksEnc.Key;
            TDESAlgorithm.IV = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            TDESAlgorithm.Mode = CipherMode.CBC;
            TDESAlgorithm.Padding = PaddingMode.None;

        }
        public long getSendSequenceCounter()
        {
            return _ssc;
        }
        public CommandAPDU wrap(CommandAPDU commandAPDU)
        {
            try
            {
                return wrapCommandAPDU2(commandAPDU);
            }
            catch (Exception exe)
            {
                logger.Error(exe, "Error al llamar wrap(commandAPDU commandAPDU");
                return null;   
            }
        }
        public ResponseAPDU unwrap(ResponseAPDU responseAPDU, int len)
        {
            try
            {
                byte[] rapdu = responseAPDU.udr;
                //rapdu = responseAPDU._rAPDU;
               // rapdu=new byte[]{135,7,1,60,180,183,104,211,130,185,97,38,242,149,28,200,9,37,153,2,144,0,142,8,100,192,208,127,75,16,139,110,144,0};
                //rapdu = new byte[] { 135, 17, 1, 54, 95, 59, 124, 145, 71, 94, 216, 64, 65, 140, 244, 100, 36, 225, 127, 153, 2, 144, 0, 142, 8, 93, 253, 224, 102, 20, 108, 26, 0, 144, 0 };
                if ((rapdu.Length == 2) || (rapdu.Length==0))
                {
                    // no sense in unwrapping - card indicates SM error
                    throw new Exception("Card indicates SM error, SW = ");
                    /* FIXME: wouldn't it be cleaner to throw a CardServiceException? */
                }
                return new ResponseAPDU(unwrapResponseAPDU(rapdu, len));
            }
            catch (Exception exe)
            {
                logger.Error(exe, "Error en SecureMessagingWrapper.unwrap");
                return null;
            }
           
        }

        public byte[] unwrapResponseAPDU(byte[] rapdu, int len)
        {

            byte[] res = null;
            long oldssc = _ssc;
            try
            {
                if (rapdu == null || rapdu.Length < 2 || len < 2)
                {
                    return res;
                }
                //TDESAlgorithm.Clear();
                ByteArrayInputStream objtest = new ByteArrayInputStream(rapdu);
                DataInputStream inputStream = new DataInputStream(new ByteArrayInputStream(rapdu));
                byte[] data = new byte[0];
                short sw = 0;
                bool finished = false;
                byte[] cc = null;
                              
			   
                while (!finished) {
                    int tag = inputStream.Read();
				    switch (tag) {
				        case (byte) 0x87:                            
                            data = readDO87(inputStream, false);
				            break;
				        case (byte) 0x85:
                            data = readDO87(inputStream, true);
				            break;
				        case (byte) 0x99:
                            sw = readDO99(inputStream);
				            break;
				        case (byte) 0x8E:
                            cc = readDO8E(inputStream);
				        finished = true;
				        break;
				    }
			   }
                if (!checkMac(rapdu, cc))
                {
                    throw new Exception("Invalid MAC");

                }
                if (_ssc == oldssc)
                {
                    _ssc++;
                }
                ByteArrayOutputStream bOut = new ByteArrayOutputStream();
                bOut.Write(data, 0, data.Length);
                bOut.Write((sw & 0xFF00) >> 8);
                bOut.Write(sw & 0x00FF);
                res=bOut.ToByteArray();
            
		
            }
            catch (Exception exe)
            {
                logger.Error(exe, "Error en unwrapResponseAPDU");
                res = null;
            }
            return res;
        }
        private CommandAPDU wrapCommandAPDU(CommandAPDU commandAPDU)
        {

            int lc = (int)commandAPDU.lc;
            int le = (int)commandAPDU.le;

		    //ByteArrayOutputStream bOut = new ByteArrayOutputStream();

		    byte[] maskedHeader = new byte[] {(byte)(commandAPDU.cla| (byte)0x0C), (byte)commandAPDU.ins, (byte)commandAPDU.p1, (byte)commandAPDU.p2};

            byte[] paddedHeader = new byte[8];
            //Padding Header
            paddedHeader = Padding(maskedHeader);

            //fin Padding Header
            //ISO7816.INS_READ_BINARY2=177
            bool hasDO85 = ((byte)commandAPDU.ins == 177 );
            byte[] do8587 = new byte[0];
            byte[] do97 = new byte[0];
            if (le > 0)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(maskedHeader, 0, maskedHeader.Length);
                    using (BinaryWriter bw = new BinaryWriter(ms))
                    {

                        bw.Write((byte)0x97);
                        bw.Write((byte)0x01);
                        bw.Write((byte)le);
                    }
                    do97 = ms.ToArray();
                }
            }
            MemoryStream msfinal = null;
            BinaryWriter bwfinal = null;
            if (lc > 0)
            {
                byte[] data = Padding(commandAPDU.udc);
                TDESAlgorithm.Clear();
                MemoryStream memoryStream = new MemoryStream();
                byte[] objresfinal = new byte[32];
                CryptoStream cryptoStream_Encryptor = new CryptoStream(memoryStream, TDESAlgorithm.CreateEncryptor(), CryptoStreamMode.Write);
                cryptoStream_Encryptor.Write(data, 0, data.Length);
                cryptoStream_Encryptor.Close();
                byte[] ciphertext  = memoryStream.ToArray();
                msfinal = new MemoryStream();
                bwfinal = new BinaryWriter(msfinal);
                bwfinal.Write(hasDO85 ? (byte)0x85 : (byte)0x87);
                bwfinal.Write(TLVUtils.getLengthAsBytes(ciphertext.Length + (hasDO85 ? 0 : 1)));
                if (!hasDO85) { bwfinal.Write((byte)0x01); };
                bwfinal.Write(ciphertext, 0, ciphertext.Length);
                do8587 = msfinal.ToArray();
                   
                                                               
            }
            
            MemoryStream msf = new MemoryStream();
            msf.Write(paddedHeader, 0, paddedHeader.Length);
            msf.Write(do8587, 0, do8587.Length);
            msf.Write(do97, 0, do97.Length);
            byte[] m = msf.ToArray();


            //bwfinal.Close();
            msfinal = new MemoryStream();
            msfinal.Read(m, 0, m.Length);
            bwfinal = new BinaryWriter(msfinal);
            

            
            _ssc++;
            byte[] n =Padding(msfinal.ToArray());
            
            /* Compute cryptographic checksum... */
            IMac macProvider = new ISO9797Alg3Mac(new DesEngine());
            macProvider.Init(new KeyParameter(_ksMac.Key));
            macProvider.Reset();

            macProvider.BlockUpdate(n, 0, n.Length);
            byte[] output = new byte[macProvider.GetMacSize()];
            macProvider.DoFinal(output, 0);

            msf = new MemoryStream();
            using (BinaryWriter bw = new BinaryWriter(msf))
            {
                bw.Write((byte)0x8E);
                bw.Write(output.Length);
                msf.Write(output, 0, output.Length);
            }
            
            byte[] do8E = msf.ToArray();


            /* Construct protected apdu... */
            msf = new MemoryStream();
            using (BinaryWriter bw = new BinaryWriter(msf))
            {
                bw.Write(do8587);
                bw.Write(do97);
                bw.Write(do8E);
            }
            byte[] datas = msf.ToArray();
            CommandAPDU objCommandAPDU = new CommandAPDU();
            objCommandAPDU.cla = maskedHeader[0];
            objCommandAPDU.ins = maskedHeader[1];
            objCommandAPDU.p1 = maskedHeader[2];
            objCommandAPDU.p2 = maskedHeader[3];
            objCommandAPDU.udc = datas;
            objCommandAPDU.le = 256;
            //CommandAPDU wc = new CommandAPDU(maskedHeader[0], maskedHeader[1], maskedHeader[2], maskedHeader[3], datas, 256);
            return objCommandAPDU;

		
		    
	    }
        private CommandAPDU wrapCommandAPDU2(CommandAPDU commandAPDU)
        {

            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            int lc = (int)commandAPDU.lc;
            int le = (int)commandAPDU.le;

            //ByteArrayOutputStream bOut = new ByteArrayOutputStream();

            byte[] maskedHeader = new byte[] { (byte)(commandAPDU.cla | (byte)0x0C), (byte)commandAPDU.ins, (byte)commandAPDU.p1, (byte)commandAPDU.p2 };

            byte[] paddedHeader = new byte[8];
            //Padding Header
            paddedHeader = Padding(maskedHeader);

            //fin Padding Header
            //ISO7816.INS_READ_BINARY2=177
            bool hasDO85 = ((byte)commandAPDU.ins == 177);
            byte[] do8587 = new byte[0];
            byte[] do97 = new byte[0];
            if (le > 0)
            {
                
                bOut.Write((byte)0x97);
                bOut.Write((byte)0x01);
                bOut.Write((byte)le);
                do97 = bOut.ToByteArray();
                
            }
            if (lc > 0)
            {
                byte[] data = Padding(commandAPDU.udc);
                TDESAlgorithm.Clear();
                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = _ksEnc.Key;
                tdes.Mode = CipherMode.CBC;
                tdes.Padding = PaddingMode.None;
                tdes.IV = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
                MemoryStream memoryStream = new MemoryStream();
                byte[] objresfinal = new byte[32];
                CryptoStream cryptoStream_Encryptor = new CryptoStream(memoryStream, tdes.CreateEncryptor(), CryptoStreamMode.Write);
                cryptoStream_Encryptor.Write(data, 0, data.Length);
                cryptoStream_Encryptor.Close();
                byte[] ciphertext = memoryStream.ToArray();
                bOut = new ByteArrayOutputStream();
                bOut.Write(hasDO85 ? (byte)0x85 : (byte)0x87);
                bOut.Write(TLVUtils.getLengthAsBytes(ciphertext.Length + (hasDO85 ? 0 : 1)));
                if (!hasDO85) { bOut.Write(0x01); };
                bOut.Write(ciphertext, 0, ciphertext.Length);
                do8587 = bOut.ToByteArray();

            }
            bOut = new ByteArrayOutputStream();
           
            bOut.Write(paddedHeader, 0, paddedHeader.Length);
            bOut.Write(do8587, 0, do8587.Length);
            bOut.Write(do97, 0, do97.Length);
            byte[] m = bOut.ToByteArray();

            bOut = new ByteArrayOutputStream();

            DataOutputStream dataOut = new DataOutputStream(bOut);
            _ssc++;
            //dataOut.WriteLong(_ssc);
            //dataOut.Write(BitConverter.GetBytes(_ssc));
            dataOut.WriteLong(_ssc);
            dataOut.Write(m, 0, m.Length);
            dataOut.Flush();            
            byte[] n =Padding(bOut.ToByteArray());


            /* Compute cryptographic checksum... */
            IMac macProvider = new ISO9797Alg3Mac(new DesEngine());
            macProvider.Init(new KeyParameter(_ksMac.Key));
            macProvider.Reset();

            macProvider.BlockUpdate(n, 0, n.Length);
            byte[] cc = new byte[macProvider.GetMacSize()];
            macProvider.DoFinal(cc, 0);

            bOut = new ByteArrayOutputStream();
            bOut.Write((byte)0x8E);
            bOut.Write(cc.Length);
            bOut.Write(cc, 0, cc.Length);
            byte[] do8E = bOut.ToByteArray();

            /* Construct protected apdu... */
            bOut = new ByteArrayOutputStream();
            bOut.Write(do8587);
            bOut.Write(do97);
            bOut.Write(do8E);
            byte[] datas = bOut.ToByteArray();



            CommandAPDU objCommandAPDU = new CommandAPDU();
            objCommandAPDU.cla = maskedHeader[0];
            objCommandAPDU.ins = maskedHeader[1];
            objCommandAPDU.p1 = maskedHeader[2];
            objCommandAPDU.p2 = maskedHeader[3];
            objCommandAPDU.udc = datas;
            objCommandAPDU.le = 256;
            //CommandAPDU wc = new CommandAPDU(maskedHeader[0], maskedHeader[1], maskedHeader[2], maskedHeader[3], datas, 256);
            return objCommandAPDU;


           
        }



    }
}
