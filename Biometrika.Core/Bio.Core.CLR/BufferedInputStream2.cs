﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WSCT.Core.ConsoleTests
{
    internal class BufferedInputStream2 : InputStream
    {
        public BufferedInputStream2(InputStream s)
        {
            BaseStream = s.GetWrappedStream();
            base.Wrapped = new BufferedStream(BaseStream);
            //s.Wrapped = BaseStream;
        }

        public BufferedInputStream2(InputStream s, int bufferSize)
        {
            BaseStream = s.GetWrappedStream();
            base.Wrapped = new BufferedStream(BaseStream, bufferSize);
            //s.Wrapped = BaseStream;
        }
    }
}
