﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;
using System.IO;

namespace WSCT.Core.ConsoleTests
{
    class CardFileInputStream : InputStream
    {
        private FileInfo[] path;
	    private static byte[] buffer;
	    private int bufferLength;
	    private int offsetBufferInFile;
	    private int offsetInBuffer;
	    private int markedOffset;
	    private int fileLength;
        private MRTDFileInfo fs;
        private readonly object syncLock = new object();
        private Stream stream;
        
        public CardFileInputStream(int maxBlockSize, MRTDFileInfo mrtfile)
        {
            this.fs=mrtfile;
            
            lock(syncLock)
            {
                  
                FileInfo[] fsPath=new MRTDFileInfo[]{mrtfile};
                if (fsPath == null || fsPath.Length < 1) {Console.Write("Error"); }//Hay que agregar excepcion
                this.path = new FileInfo[fsPath.Length];
                Array.Copy(fsPath, 0, this.path, 0, fsPath.Length);
                fileLength = fsPath[fsPath.Length - 1].getFileLength();
                buffer = new byte[maxBlockSize];
                bufferLength = 0;
			    offsetBufferInFile = 0;
			    offsetInBuffer = 0;
			    markedOffset = -1;
            }
        }
        
        public int getLength() {
		    return fileLength;
	    }

        public int getPostion() {
		    return offsetBufferInFile + offsetInBuffer;
	    }

        
        

    }
}
