﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;
using System.Reflection;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Linq;
//using log4net;

namespace PrintDemo
{

    internal class Credencial
    {

        //private static readonly ILog log = LogManager.GetLogger(typeof(Credencial));

        ArrayList bodyLines = new ArrayList();
        private Image photoImage = null;

        ArrayList headerLines = new ArrayList();
        ArrayList subHeaderLines = new ArrayList();
        ArrayList items = new ArrayList();
        ArrayList totales = new ArrayList();
        ArrayList footerLines = new ArrayList();
        private Image headerImage = null;
        private Image footerImage = null;

        int count = 0;

        int maxChar = 35;
        int maxCharDescription = 20;

        int imageHeight = 0;

        float leftMargin = 0;
        float topMargin = 3;

        string fontName = "Lucida Console";
        int fontSize = 8;

        Font printFont = null;
        SolidBrush myBrush = new SolidBrush(Color.Black);

        Graphics gfx = null;

        string line = null;

        public Credencial()
        {

        }

        public Image PhotoImage
        {
            get { return photoImage; }
            set
            {
                if (photoImage != value)
                {
                    Image aux = value;
                    photoImage = (Image)aux.Clone();
                }
            }
        }

        public Image FooterImage
        {
            get { return footerImage; }
            set
            {
                if (footerImage != value)
                {
                    Image aux = value;
                    footerImage = (Image)aux.Clone();
                }
            }
        }


        public Image HeaderImage
        {
            get { return headerImage; }
            set
            {
                if (headerImage != value)
                {
                    Image aux = value;
                    headerImage = (Image)aux.Clone();
                }
            }
        }

        public int MaxChar
        {
            get { return maxChar; }
            set { if (value != maxChar) maxChar = value; }
        }

        public int MaxCharDescription
        {
            get { return maxCharDescription; }
            set { if (value != maxCharDescription) maxCharDescription = value; }
        }

        public int FontSize
        {
            get { return fontSize; }
            set { if (value != fontSize) fontSize = value; }
        }

        public string FontName
        {
            get { return fontName; }
            set { if (value != fontName) fontName = value; }
        }

        #region Banco Falabella
        public void PrintCredencialBF(string impresora)
        {
            printFont = new Font(fontName, fontSize, FontStyle.Regular);
            PrintDocument pr = new PrintDocument();
            pr.PrinterSettings.PrinterName = impresora;
           
            pr.PrintPage += new PrintPageEventHandler(pr_PrintPageBF);
            pr.Print();
        }

        private void pr_PrintPageBF(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            gfx = e.Graphics;

            DrawHeaderBF();

            DrawBodyBF();

            DrawFooterBF();

            if (headerImage != null)
            {
                HeaderImage.Dispose();
                headerImage.Dispose();
            }
            if (footerImage != null)
            {
                FooterImage.Dispose();
                footerImage.Dispose();
            }
        }

       
        private void DrawHeaderBF()
        {
            int yPos = (int)YPosition();
            if (headerImage != null)
            {
                try
                {
                    gfx.DrawImage(headerImage, new Point((int)leftMargin + 5 , yPos));
                    double height = ((double)headerImage.Height / 58) * 15;
                    imageHeight = (int)Math.Round(height) + 3;
                }
                catch (Exception ex)
                {
                    //log.Error("Ticket.DrawImage - Error = " + ex.Message);
                }
            }

            string fontName = "Arial Black";
            int fontSize = 18;

            Font printFontH = new Font(fontName, fontSize, FontStyle.Bold);
            gfx.DrawString("VISITA", printFontH, myBrush, leftMargin + 60, 5, new StringFormat());

        }

        private void DrawBodyBF()
        {
            //if (photoImage != null)
            //{
            //    try
            //    {
            //        //e.Graphics.DrawImage(photoImage, 0, 0, 372, 235);
            //        //e.Graphics.DrawImage(bmps[index++], 400, 0, 372, 235);

            //        //gfx.DrawImage(photoImage, new Point((int)leftMargin, (int)YPosition()));

            //        gfx.DrawImage(photoImage,5,25,25,25);
            //        //double height = ((double)photoImage.Height / 58) * 15;
            //        //imageHeight = (int)Math.Round(height) + 3;
            //    }
            //    catch (Exception ex)
            //    {
            //        //log.Error("Ticket.DrawImage - Error = " + ex.Message);
            //    }
            //}
            if (footerImage != null)
            {
                try
                {
                    //gfx.DrawImage(footerImage, new Point((int)leftMargin, (int)YPosition()));
                    gfx.DrawImage(footerImage, 1, 18, 25, 25);
                    //double height = ((double)footerImage.Height / 58) * 15;
                    //imageHeight = (int)Math.Round(height) + 3;
                    imageHeight = 18;
                }
                catch (Exception ex)
                {
                    //log.Error("Ticket.DrawImage - Error = " + ex.Message);
                }
            }
            foreach (string body in bodyLines)
            {
                if (body.Length > maxChar)
                {
                    int currentChar = 0;
                    int headerLenght = body.Length;

                    while (headerLenght > maxChar)
                    {
                        line = body.Substring(currentChar, maxChar);
                        gfx.DrawString(line, printFont, myBrush, leftMargin + 25, YPosition(), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        headerLenght -= maxChar;
                    }
                    line = body;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont,
                                                  myBrush, leftMargin + 25, YPosition(), new StringFormat());
                    count++;
                }
                else
                {
                    line = body;
                    gfx.DrawString(line, printFont, myBrush, leftMargin + 25, YPosition(), new StringFormat());

                    count++;
                }
            }
        }

        private void DrawFooterBF()
        {
            if (footerLines != null && footerLines.Count > 0)
            {
                string fontName = "Lucida Console";
                int fontSize = 12;

                Font printFontH = new Font(fontName, fontSize, FontStyle.Italic);
                gfx.DrawString((string)footerLines[0], printFontH, myBrush, leftMargin,
                                YPosition() + 3, new StringFormat());
            }
        }

        private float YPosition()
        {
            return topMargin + (count * printFont.GetHeight(gfx) + imageHeight);
        }

        #endregion Banco Falabella

        #region Common

        public void AddHeaderLine(string line)
        {
            headerLines.Add(line);
        }

        public void AddSubHeaderLine(string line)
        {
            subHeaderLines.Add(line);
        }

        public void AddItem(string cantidad, string item, string price)
        {
            OrderItem newItem = new OrderItem('?');
            items.Add(newItem.GenerateItem(cantidad, item, price));
        }

        public void AddTotal(string name, string price)
        {
            OrderTotal newTotal = new OrderTotal('?');
            totales.Add(newTotal.GenerateTotal(name, price));
        }

        public void AddFooterLine(string line)
        {
            footerLines.Add(line);
        }

        private string AlignRightText(int lenght)
        {
            string espacios = "";
            int spaces = maxChar - lenght;
            for (int x = 0; x < spaces; x++)
                espacios += " ";
            return espacios;
        }

        private string DottedLine()
        {
            string dotted = "";
            for (int x = 0; x < maxChar; x++)
                dotted += "=";
            return dotted;
        }

        public bool PrinterExists(string impresora)
        {
            foreach (String strPrinter in PrinterSettings.InstalledPrinters)
            {
                if (impresora == strPrinter)
                    return true;
            }
            return false;
        }

        public void PrintCredencial(string impresora)
        {
            printFont = new Font(fontName, fontSize, FontStyle.Regular);
            PrintDocument pr = new PrintDocument();
            pr.PrinterSettings.PrinterName = impresora;
            pr.PrintPage += new PrintPageEventHandler(pr_PrintPage);
            pr.Print();
        }

        private void pr_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            gfx = e.Graphics;

            DrawImage();
            DrawHeader();
            DrawSubHeader();
            //DrawItems();
            //DrawTotales();
            DrawFooter();
            DrawEspacio();
            DrawEspacio();
            DrawEspacio();
            DrawImageFooter();
            if (headerImage != null)
            {
                HeaderImage.Dispose();
                headerImage.Dispose();
            }
        }

       

        public string GetLine()
        {
            string ret = "";

            for (int i = 0; i < MaxChar; i++)
            {
                ret += "-";
            }
            return ret;
        }

        private void DrawImage()
        {
            int yPos = (int)YPosition();
            if (headerImage != null)
            {
                try
                {
                    gfx.DrawImage(headerImage, new Point((int)leftMargin, yPos));
                    double height = ((double)headerImage.Height / 58) * 15;
                    imageHeight = (int)Math.Round(height) + 3;
                }
                catch (Exception ex)
                {
                    //log.Error("Ticket.DrawImage - Error = " + ex.Message);
                }
            }
            
        }

        private void DrawImageFooter()
        {
            if (footerImage != null)
            {
                try
                {
                    //gfx.DrawImage(footerImage, new Point((int)leftMargin, (int)YPosition()));
                    gfx.DrawImage(footerImage,10,10,45,45);
                    //double height = ((double)footerImage.Height / 58) * 15;
                    //imageHeight = (int)Math.Round(height) + 3;
                }
                catch (Exception ex)
                {
                    //log.Error("Ticket.DrawImage - Error = " + ex.Message);
                }
            }
        }

        private void DrawHeader()
        {
            foreach (string header in headerLines)
            {
                if (header.Length > maxChar)
                {
                    int currentChar = 0;
                    int headerLenght = header.Length;

                    while (headerLenght > maxChar)
                    {
                        line = header.Substring(currentChar, maxChar);
                        gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        headerLenght -= maxChar;
                    }
                    line = header;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                    count++;
                }
                else
                {
                    line = header;
                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                    count++;
                }
            }
            DrawEspacio();
        }

        public void AddBodyLine(string line)
        {
            bodyLines.Add(line);
        }
        private void DrawBody()
        {
            //if (photoImage != null)
            //{
            //    try
            //    {
            //        //e.Graphics.DrawImage(photoImage, 0, 0, 372, 235);
            //        //e.Graphics.DrawImage(bmps[index++], 400, 0, 372, 235);

            //        //gfx.DrawImage(photoImage, new Point((int)leftMargin, (int)YPosition()));

            //        gfx.DrawImage(photoImage,5,25,25,25);
            //        //double height = ((double)photoImage.Height / 58) * 15;
            //        //imageHeight = (int)Math.Round(height) + 3;
            //    }
            //    catch (Exception ex)
            //    {
            //        //log.Error("Ticket.DrawImage - Error = " + ex.Message);
            //    }
            //}
            if (footerImage != null)
            {
                try
                {
                    //gfx.DrawImage(footerImage, new Point((int)leftMargin, (int)YPosition()));
                    gfx.DrawImage(footerImage, 3, 20, 18, 18);
                    //double height = ((double)footerImage.Height / 58) * 15;
                    //imageHeight = (int)Math.Round(height) + 3;
                }
                catch (Exception ex)
                {
                    //log.Error("Ticket.DrawImage - Error = " + ex.Message);
                }
            }
            foreach (string body in bodyLines)
            {
                if (body.Length > maxChar)
                {
                    int currentChar = 0;
                    int headerLenght = body.Length;

                    while (headerLenght > maxChar)
                    {
                        line = body.Substring(currentChar, maxChar);
                        gfx.DrawString(line, printFont, myBrush, leftMargin + 20, YPosition(), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        headerLenght -= maxChar;
                    }
                    line = body;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont, 
                                                  myBrush, leftMargin + 20, YPosition(), new StringFormat());
                    count++;
                }
                else
                {
                    line = body;
                    gfx.DrawString(line, printFont, myBrush, leftMargin + 20, YPosition(), new StringFormat());

                    count++;
                }
            }
            DrawEspacio();
        }
   
        private void DrawSubHeader()
        {
            foreach (string subHeader in subHeaderLines)
            {
                if (subHeader.Length > maxChar)
                {
                    int currentChar = 0;
                    int subHeaderLenght = subHeader.Length;

                    while (subHeaderLenght > maxChar)
                    {
                        line = subHeader;
                        gfx.DrawString(line.Substring(currentChar, maxChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        subHeaderLenght -= maxChar;
                    }
                    line = subHeader;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                    count++;
                }
                else
                {
                    line = subHeader;

                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                    count++;

                    line = DottedLine();

                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                    count++;
                }
            }
            DrawEspacio();
        }

        private void DrawItems()
        {
            OrderItem ordIt = new OrderItem('?');

            gfx.DrawString("CANT  DESCRIPCION           IMPORTE", printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            DrawEspacio();

            foreach (string item in items)
            {
                line = ordIt.GetItemCantidad(item);

                gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                line = ordIt.GetItemPrice(item);
                line = AlignRightText(line.Length) + line;

                gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                string name = ordIt.GetItemName(item);

                leftMargin = 0;
                if (name.Length > maxCharDescription)
                {
                    int currentChar = 0;
                    int itemLenght = name.Length;

                    while (itemLenght > maxCharDescription)
                    {
                        line = ordIt.GetItemName(item);
                        gfx.DrawString("      " + line.Substring(currentChar, maxCharDescription), printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                        count++;
                        currentChar += maxCharDescription;
                        itemLenght -= maxCharDescription;
                    }

                    line = ordIt.GetItemName(item);
                    gfx.DrawString("      " + line.Substring(currentChar, line.Length - currentChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                    count++;
                }
                else
                {
                    gfx.DrawString("      " + ordIt.GetItemName(item), printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                    count++;
                }
            }

            leftMargin = 0;
            DrawEspacio();
            line = DottedLine();

            gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            DrawEspacio();
        }

        private void DrawTotales()
        {
            OrderTotal ordTot = new OrderTotal('?');

            foreach (string total in totales)
            {
                line = ordTot.GetTotalCantidad(total);
                line = AlignRightText(line.Length) + line;

                gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                leftMargin = 0;

                line = "      " + ordTot.GetTotalName(total);
                gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                count++;
            }
            leftMargin = 0;
            DrawEspacio();
            DrawEspacio();
        }

        private void DrawFooter()
        {
            foreach (string footer in footerLines)
            {
                if (footer.Length > maxChar)
                {
                    int currentChar = 0;
                    int footerLenght = footer.Length;

                    while (footerLenght > maxChar)
                    {
                        line = footer;
                        gfx.DrawString(line.Substring(currentChar, maxChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        footerLenght -= maxChar;
                    }
                    line = footer;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                    count++;
                }
                else
                {
                    line = footer;
                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                    count++;
                }
            }
            leftMargin = 0;
            DrawEspacio();
        }

        private void DrawEspacio()
        {
            line = "";

            gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
        }
        #endregion Common

        public Bitmap ResizeImage(Bitmap bitmap)
        {
            Bitmap result = new Bitmap(850, 1101);
            using (Graphics grap = Graphics.FromImage(result))
            {
                grap.CompositingQuality = CompositingQuality.HighQuality;
                grap.InterpolationMode = InterpolationMode.Bicubic;
                grap.SmoothingMode = SmoothingMode.HighQuality;
                grap.CompositingQuality = CompositingQuality.HighQuality;

                grap.DrawImage(bitmap, 0, 0, 850, 1101);
            }
            return result;
        }
    }

    public class OrderItem
    {
        char[] delimitador = new char[] { '?' };

        public OrderItem(char delimit)
        {
            delimitador = new char[] { delimit };
        }

        public string GetItemCantidad(string orderItem)
        {
            string[] delimitado = orderItem.Split(delimitador);
            return delimitado[0];
        }

        public string GetItemName(string orderItem)
        {
            string[] delimitado = orderItem.Split(delimitador);
            return delimitado[1];
        }

        public string GetItemPrice(string orderItem)
        {
            string[] delimitado = orderItem.Split(delimitador);
            return delimitado[2];
        }

        public string GenerateItem(string cantidad, string itemName, string price)
        {
            return cantidad + delimitador[0] + itemName + delimitador[0] + price;
        }
    }

    public class OrderTotal
    {
        char[] delimitador = new char[] { '?' };

        public OrderTotal(char delimit)
        {
            delimitador = new char[] { delimit };
        }

        public string GetTotalName(string totalItem)
        {
            string[] delimitado = totalItem.Split(delimitador);
            return delimitado[0];
        }

        public string GetTotalCantidad(string totalItem)
        {
            string[] delimitado = totalItem.Split(delimitador);
            return delimitado[1];
        }

        public string GenerateTotal(string totalName, string price)
        {
            return totalName + delimitador[0] + price;
        }


    }

    

}

