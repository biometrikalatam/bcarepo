﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FlowTest
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            TestSerilizeDict();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        private static void TestSerilizeDict()
        {
            try
            {
                var dic = new Dictionary<string, string>();
                dic.Add("clave1", "value1");
                dic.Add("clave2", "value2");
                dic.Add("clave3", "value3");

                string s = JsonConvert.SerializeObject(dic);

                Dictionary<string, string> dic2 = JsonConvert.DeserializeObject<Dictionary<string, string>>(s);

                string s1 = dic2["clave1"];

            }
            catch (Exception ex)
            {
                int i = 0;
                //log.Error(" Error: " + ex.Message);
            }
        }
    }
}
