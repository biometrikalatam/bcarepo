﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace FlowTest
{
    public class HelperPaymentFlow
    {

        public static string GetSignature(string key, string datatosign)
        {
            string sret = null;
            try
            {
               
                using (HMACSHA256 hmac = new HMACSHA256(ASCIIEncoding.UTF8.GetBytes(key)))
                {
                    byte[] byRet = hmac.ComputeHash(ASCIIEncoding.UTF8.GetBytes(datatosign));
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < byRet.Length; i++)
                    {
                        builder.Append(byRet[i].ToString());// "x2"));
                    }

                    sret = builder.ToString(); //ASCIIEncoding.UTF8.GetString(byRet);
                }
                
            }
            catch (Exception ex)
            {
                sret = null;
                //LOG.Error(" Error: " + ex.Message);
            }
            return sret;
        }

        public static string GetSignature(string key, Hashtable ht)
        {
            string sret = null;
            try
            {
                if (ht==null || ht.Count==0)
                {
                    sret = null;
                } else
                {
                    sret = "";
                    foreach (DictionaryEntry item in ht)
                    {
                        sret += (string)item.Key + (string)item.Value;
                    }
                    using (HMACSHA256 hmac = new HMACSHA256(ASCIIEncoding.UTF8.GetBytes(key)))
                    {
                        byte[] byRet = hmac.ComputeHash(ASCIIEncoding.UTF8.GetBytes(sret));
                        StringBuilder builder = new StringBuilder();
                        for (int i = 0; i < byRet.Length; i++)
                        {
                            builder.Append(byRet[i].ToString());// "x2"));
                        }
                        
                        sret = builder.ToString(); //ASCIIEncoding.UTF8.GetString(byRet);
                    }
                }
            }
            catch (Exception ex)
            {
                sret = null;
                //LOG.Error(" Error: " + ex.Message);
            }
            return sret;
        }

        static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}
