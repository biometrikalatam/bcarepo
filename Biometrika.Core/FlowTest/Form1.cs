﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CL.Flow.RestAPI.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace FlowTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hashtable ht = new Hashtable();
            ht.Add("apiKey", textBox1.Text);
            ht.Add("subject", textBox2.Text);
            ht.Add("currency", textBox3.Text);
            ht.Add("amount", textBox4.Text);
            ht.Add("email", textBox5.Text);
            ht.Add("commerceOrder", textBox6.Text);
            ht.Add("urlConfirmation", textBox7.Text);
            ht.Add("urlReturn", textBox8.Text);

            string sToSign = "amount" + textBox4.Text.Trim() +
                "apiKey" + textBox1.Text.Trim() +
                "commerceOrder" + textBox6.Text.Trim() +
                "currency" + textBox3.Text.Trim() +
                "email" + textBox5.Text.Trim() +
                "subject" + textBox2.Text.Trim() +
                "urlConfirmation" + textBox7.Text.Trim() +
                "urlReturn" + textBox8.Text.Trim();
            textBox11.Text = sToSign;
            textBox9.Text = HelperPaymentFlow.GetSignature(textBox10.Text, sToSign);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var client = new RestClient("https://sandbox.flow.cl/api/payment/create");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("apiKey", "663E0BF9-B857-4352-85CE-6F12EBL798DE");
            request.AddParameter("subject", "Pago de prueba POSTMAN");
            request.AddParameter("currency", "CLP");
            request.AddParameter("amount", "1000");
            request.AddParameter("email", "gsuhit@biometrika.cl");
            request.AddParameter("commerceOrder", "a7d1f78117dd40f183c839b8c10fed14");
            request.AddParameter("urlConfirmation", "http://qanv.enotario.cl/api/WebhookReceivePayment");
            request.AddParameter("urlReturn", "http://flowosccomerce.tuxpan.com/csepulveda/api2/pay/resultPay.php");

            string sToSign = "amount" + textBox4.Text.Trim() +
               "apiKey" + textBox1.Text.Trim() +
               "commerceOrder" + textBox6.Text.Trim() +
               "currency" + textBox3.Text.Trim() +
               "email" + textBox5.Text.Trim() +
               "subject" + textBox2.Text.Trim() +
               "urlConfirmation" + textBox7.Text.Trim() +
               "urlReturn" + textBox8.Text.Trim();

            request.AddParameter("s", HelperPaymentFlow.GetSignature(textBox10.Text, sToSign));
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.StatusCode + " - " + response.ErrorMessage +  " - Rta=" + response.Content);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                FlowClient client = new FlowClient();
                client.SetApiKey("663E0BF9-B857-4352-85CE-6F12EBL798DE");
                client.SetSecretKey("a8123fce839f11f36c3688a461abdb34b5e496c7");
                client.SetApiUrl("https://sandbox.flow.cl/api/");

                //string service = "customer/get";
                //IDictionary<string, string> parameters = new Dictionary<string, string>();
                //parameters.Add("customerId", "cus_1k3xv870h8");

                //JObject response = client.Send(service, parameters, HttpMethod.GET);

                //richTextBox1.Text = response.ToString();


                //Create Payment
                string service = "payment/create";
                IDictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("subject", "Pago de prueba POSTMAN");
                parameters.Add("currency", "CLP");
                parameters.Add("amount", "1000");
                parameters.Add("email", "gsuhit@biometrika.cl");
                parameters.Add("commerceOrder", "123456789");
                parameters.Add("urlConfirmation", "http://flowosccomerce.tuxpan.com/csepulveda/api2/pay/confirmPay.php");
                parameters.Add("urlReturn", "http://flowosccomerce.tuxpan.com/csepulveda/api2/pay/resultPay.php");

                JObject response = client.Send(service, parameters, HttpMethod.POST);

                richTextBox1.Text = response.ToString();

                ResponseCreateOrderPaymentFlow responseP = JsonConvert.DeserializeObject<ResponseCreateOrderPaymentFlow>(response.ToString());
            }
            catch (Exception ex)
            {
                richTextBox1.Text = ex.Message;
            }
            
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                IDictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("token", "JAJAJAJAJAJAJAJAJA");
                RestClient client = new RestClient("http://localhost:9999/");
                RestRequest request = GetRequest("api/WebhookReceivePayment", parameters, HttpMethod.POST);
                IRestResponse response = client.Execute(request);

                var cod = response.StatusCode;

                JObject content = JsonConvert.DeserializeObject<JObject>(response.Content);

            }
            catch (Exception ex)
            {
                int j = 0;
            }
            
            int i = 0;
        }

        private RestRequest GetRequest(string service, IDictionary<string, string> parameters, HttpMethod method)
        {
            RestRequest request = new RestRequest(service, (method == HttpMethod.GET) ? Method.GET : Method.POST);
            IDictionary<string, string> allparameters = (parameters != null) ? parameters : new Dictionary<string, string>();

            var keyList = allparameters.Keys.ToList();
            keyList.Sort();

            foreach (var key in keyList)
            {
                request.AddParameter(key, allparameters[key]);
            }

            return request;
        }
    }

    public class ResponseCreateOrderPaymentFlow
    {
        public string url { get; set; }
        public string token { get; set; }
        public int flowOrder { get; set; }
    }
}
