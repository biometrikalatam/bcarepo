﻿using CSJ2K;
using CSJ2K.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bio.Core.CLR.Test
{
    public partial class Form1 : Form
    {
        internal string _CLR_DOE;
        internal string _CLR_DOB;
        internal string _ISO_COMPACT;
        CLR clr = new Bio.Core.CLR.CLR();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            clr = new Bio.Core.CLR.CLR();
        }

        private void VerifyNewCedula()
        {
            string msgerr;
            Int32 finger;
            bool bReturnMatch = false;
            try
            {
                

                clr.ReaderName = "HID Global OMNIKEY 5022 Smart Card Reader 0";
                clr.CLR_OPEN(10);
                Thread.Sleep(100);
                //LOG.Debug("BVIUI.VerifyNewCedula - Entrando a CLR_CONNECT");

                clr.CLR_CONNECT(Convert.ToUInt32(5000));

                if (clr.GetLasError() == 0)
                {
                    //LOG.Debug("BVIUI.VerifyNewCedula - Extracción de datos de RUT " + _PERSONA.Rut);
                    //ExtractMrzData(clr);
                    clr.CLR_SECURITY(textBox1.Text,
                                     textBox2.Text,
                                     textBox3.Text
                                    );
                    //_PERSONA.FechaNacimiento.Value.ToString("yyMMdd"),
                    //_PERSONA.FechaExpiracion.Value.ToString("yyMMdd")


                    //LOG.Debug("BVIUI.VerifyNewCedula - Extracción de datos terminada!");

                    //LOG.Debug("BVIUI.VerifyNewCedula - Comenzando Verificación contra cédula nueva de RUT " + _PERSONA.Rut);

                    //string _ISOCompact = null;
                    //string msgerr = null;
                    //int ret = _NVHELPER.GenerateISOCompact(_PERSONA.Iso, out _ISOCompact, out msgerr);

                    //if (ret != 0)
                    //{
                    //    //TODO - Show Error 
                    //}
                    //else
                    //{
                    string[] byIsoCompact = null;

                    string resultado = "Verificacion Positiva";
//                    bReturnMatch = clr.Match(_PERSONA.IsoCompact, out finger);
                    bReturnMatch = clr.Match(byIsoCompact, out finger);

                    //picProgresVerify.Visible = false;
                    //picProgresVerify.Refresh();
                    //labProgresVerify.Visible = false;
                    //labProgresVerify.Refresh();
//                    _PERSONA.verifyResult = bReturnMatch ? 1 : 2;
                    //_PERSONA.verifyResult = bReturnMatch ? 1 : 2;

                    //LOG.Debug($"BVIUI.VerifyNewCedula - SetupClr: Resultado Match: {resultado}");


                }
                else if (clr.GetLasError() == -1)
                {
                    //MostrarMensaje(1, Color.Red, "Error al detectar la cédula. Acérquela al dispositivo");
                    //LOG.Error("BVIUI.VerifyNewCedula - La cédula leída no corresponde a la huella capturada");
                }
                clr.CLR_DISCONNECT();
                clr.CLR_CLOSE();
            }
            catch (EndOfStreamException ex)
            {
                //MostrarMensaje(1, Color.Red, "Excepcion EndOfStreamException");
                //LOG.Error("BVIUI.VerifyNewCedula - EndOfStreamException ", ex);
            }
            catch (Exception ex)
            {
                //MostrarMensaje(1, Color.Red, "Excepcion Exception [" + ex.Message + "]");
                //LOG.Error("BVIUI.VerifyNewCedula - Excepcion Exception [" + ex.Message + "]", ex);
            }
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                clr = new Bio.Core.CLR.CLR();
                clr.ReaderName = "HID Global OMNIKEY 5022 Smart Card Reader 0";
                clr.CLR_OPEN(10);
                Thread.Sleep(100);
                //LOG.Debug("BVIUI.VerifyNewCedula - Entrando a CLR_CONNECT");
                clr.CLR_CONNECT(Convert.ToUInt32(5000));
                if (clr.GetLasError() == 0)
                {
                    clr.CLR_SECURITY(textBox1.Text,
                                     textBox2.Text,
                                     textBox3.Text
                                    );
                    clr.CLR_GETMRZ();
                    richTextBox1.Text = "" + clr.DNI + " - " +
                                        clr.Country + " - " +
                                        clr.DateOfExpiry + " - " +
                                        clr.DocumentNumber + " - " +
                                        clr.Name + " - " +
                                        clr.PatherLastName + " - " +
                                        clr.MotherLastName + " - " +
                                        clr.Nationality + " - " +
                                        clr.ReaderName;

                    byte[] foto = clr.CLR_GETFOTO();

                    if (foto != null)
                    {
                        BitmapImageCreator.Register();
                        var por = J2kImage.FromBytes(foto);
                        //Bitmap img = por.As<Bitmap>();//ACA SE CAE
                        int sdf = 0;
                        Bitmap img = por.As<Bitmap>();

                        ImageFormat format = ImageFormat.Jpeg;

                        using (MemoryStream ms = new MemoryStream())
                        {
                            img.Save(ms, format);
                        }

                        //    Foto.Source = BitmapToImageSource(img);
                        //Image imgFoto = Image.FromStream(new MemoryStream(foto));
                        pictureBox1.Image = img; // BitmapToImageSource(img); ;
                    }

                    byte[] firma = clr.CLR_GETFIRMA();
                    if (firma != null)
                    {
                        BitmapImageCreator.Register();
                        var por = J2kImage.FromBytes(firma);
                        //Bitmap img = por.As<Bitmap>();//ACA SE CAE
                        int sdf = 0;
                        Bitmap img = por.As<Bitmap>();

                        ImageFormat format = ImageFormat.Jpeg;

                        using (MemoryStream ms = new MemoryStream())
                        {
                            img.Save(ms, format);
                        }

                        //    Foto.Source = BitmapToImageSource(img);
                        //Image imgFoto = Image.FromStream(new MemoryStream(foto));
                        pictureBox2.Image = img; // BitmapToImageSource(img); ;
                    }
                }
                else if (clr.GetLasError() == -1)
                {
                    //MostrarMensaje(1, Color.Red, "Error al detectar la cédula. Acérquela al dispositivo");
                    //LOG.Error("BVIUI.VerifyNewCedula - La cédula leída no corresponde a la huella capturada");
                }
                clr.CLR_DISCONNECT();
                clr.CLR_CLOSE();
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                clr = new Bio.Core.CLR.CLR();
                clr.ReaderName = "HID Global OMNIKEY 5022 Smart Card Reader 0";
                clr.CLR_OPEN(10);
                Thread.Sleep(100);
                //LOG.Debug("BVIUI.VerifyNewCedula - Entrando a CLR_CONNECT");
                clr.CLR_CONNECT(Convert.ToUInt32(5000));
                if (clr.GetLasError() == 0)
                {
                    //clr.CLR_SECURITY(textBox1.Text,
                    //                textBox2.Text,
                    //                textBox3.Text
                    //               );

                    //clr.CLR_SECURITY_AES(textBox1.Text,
                    //                 textBox2.Text,
                    //                 textBox3.Text
                    //                );

                    string[] _isocc = ReadIsoCC(textBox4.Text);
                    int finger;
                    bool bResult = clr.Match2020(_isocc, out finger);
                    //bool bResult = clr.Match(_isocc, out finger);

                    richTextBox1.Text = "Verificacion => " + bResult.ToString() + " - Dedo=" + finger.ToString();
                }
                else if (clr.GetLasError() == -1)
                {
                    //MostrarMensaje(1, Color.Red, "Error al detectar la cédula. Acérquela al dispositivo");
                    //LOG.Error("BVIUI.VerifyNewCedula - La cédula leída no corresponde a la huella capturada");
                }
                clr.CLR_DISCONNECT();
                clr.CLR_CLOSE();
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private string[] ReadIsoCC(string str)
        {
            string[] sRet = new string[str.Length / 2];
            try
            {
                int pos = 0;
                for (int i = 0; i < sRet.Length; i++)
                {
                    sRet[i] = str.Substring(pos, 2);
                    pos = pos + 2;
                }
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
            return sRet;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Seleccione MInucias ISOCC Hexa...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    string s; 
                    s = System.IO.File.ReadAllText(openFileDialog1.FileName);
                    textBox4.Text = s;
                }
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text = "601014310";
            textBox2.Text = "690928";
            textBox3.Text = "220802";

        }

        //BitmapImage BitmapToImageSource(Bitmap bitmap)
        //{
        //    using (MemoryStream memory = new MemoryStream())
        //    {
        //        bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
        //        memory.Position = 0;
        //        BitmapImage bitmapimage = new BitmapImage();
        //        bitmapimage.BeginInit();
        //        bitmapimage.StreamSource = memory;
        //        bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
        //        bitmapimage.EndInit();

        //        return bitmapimage;
        //    }
        //}
    }
}
