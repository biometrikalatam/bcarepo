﻿namespace CL.Flow.RestAPI.Client.TestForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.txtUrlReturn = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtUrlConfirm = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtComerceOrder = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCurrency = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtApiKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.txtSecretKey = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtUrlBase = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPaymentMethod = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.txtFlowOrder = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtUrlPay = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(41, 361);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(1056, 359);
            this.richTextBox1.TabIndex = 51;
            this.richTextBox1.Text = "";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(651, 17);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(155, 23);
            this.button3.TabIndex = 50;
            this.button3.Text = "Create Order";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtUrlReturn
            // 
            this.txtUrlReturn.Location = new System.Drawing.Point(651, 290);
            this.txtUrlReturn.Name = "txtUrlReturn";
            this.txtUrlReturn.Size = new System.Drawing.Size(446, 20);
            this.txtUrlReturn.TabIndex = 41;
            this.txtUrlReturn.Text = "http://www.biometrikalatam.com";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(545, 293);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 40;
            this.label8.Text = "Url Return";
            // 
            // txtUrlConfirm
            // 
            this.txtUrlConfirm.Location = new System.Drawing.Point(651, 249);
            this.txtUrlConfirm.Name = "txtUrlConfirm";
            this.txtUrlConfirm.Size = new System.Drawing.Size(446, 20);
            this.txtUrlConfirm.TabIndex = 39;
            this.txtUrlConfirm.Text = "http://qanv.enotario.cl/api/WebhookReceivePayment";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(545, 252);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "Url Confirmation";
            // 
            // txtComerceOrder
            // 
            this.txtComerceOrder.Location = new System.Drawing.Point(651, 214);
            this.txtComerceOrder.Name = "txtComerceOrder";
            this.txtComerceOrder.Size = new System.Drawing.Size(446, 20);
            this.txtComerceOrder.TabIndex = 37;
            this.txtComerceOrder.Text = "a7d1f78117dd40f183c839b8c10fed14";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(543, 218);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Commerce Order";
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(651, 180);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(446, 20);
            this.txtMail.TabIndex = 35;
            this.txtMail.Text = "gsuhit@biometrika.cl";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(545, 183);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Email";
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(651, 118);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(446, 20);
            this.txtAmount.TabIndex = 33;
            this.txtAmount.Text = "1000";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(545, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Amount";
            // 
            // txtCurrency
            // 
            this.txtCurrency.Location = new System.Drawing.Point(651, 88);
            this.txtCurrency.Name = "txtCurrency";
            this.txtCurrency.Size = new System.Drawing.Size(446, 20);
            this.txtCurrency.TabIndex = 31;
            this.txtCurrency.Text = "CLP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(543, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Currency";
            // 
            // txtSubject
            // 
            this.txtSubject.Location = new System.Drawing.Point(651, 62);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(446, 20);
            this.txtSubject.TabIndex = 29;
            this.txtSubject.Text = "Pago de prueba FlowAPI";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(545, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Subject";
            // 
            // txtApiKey
            // 
            this.txtApiKey.Location = new System.Drawing.Point(84, 14);
            this.txtApiKey.Name = "txtApiKey";
            this.txtApiKey.Size = new System.Drawing.Size(446, 20);
            this.txtApiKey.TabIndex = 27;
            this.txtApiKey.Text = "663E0BF9-B857-4352-85CE-6F12EBL798DE";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "API Key";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(84, 106);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(155, 23);
            this.button4.TabIndex = 52;
            this.button4.Text = "Create FloAPI";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(73, 309);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(155, 23);
            this.button5.TabIndex = 53;
            this.button5.Text = "Get Status Order";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txtSecretKey
            // 
            this.txtSecretKey.Location = new System.Drawing.Point(84, 40);
            this.txtSecretKey.Name = "txtSecretKey";
            this.txtSecretKey.Size = new System.Drawing.Size(446, 20);
            this.txtSecretKey.TabIndex = 55;
            this.txtSecretKey.Text = "a8123fce839f11f36c3688a461abdb34b5e496c7";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 54;
            this.label9.Text = "Secret Key";
            // 
            // txtUrlBase
            // 
            this.txtUrlBase.Location = new System.Drawing.Point(84, 66);
            this.txtUrlBase.Name = "txtUrlBase";
            this.txtUrlBase.Size = new System.Drawing.Size(446, 20);
            this.txtUrlBase.TabIndex = 57;
            this.txtUrlBase.Text = "https://sandbox.flow.cl/api/";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 56;
            this.label10.Text = "Url Base";
            // 
            // txtPaymentMethod
            // 
            this.txtPaymentMethod.Location = new System.Drawing.Point(651, 150);
            this.txtPaymentMethod.Name = "txtPaymentMethod";
            this.txtPaymentMethod.Size = new System.Drawing.Size(446, 20);
            this.txtPaymentMethod.TabIndex = 59;
            this.txtPaymentMethod.Text = "9";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(545, 153);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 58;
            this.label11.Text = "paymentMethod";
            // 
            // txtToken
            // 
            this.txtToken.Location = new System.Drawing.Point(73, 252);
            this.txtToken.Name = "txtToken";
            this.txtToken.Size = new System.Drawing.Size(446, 20);
            this.txtToken.TabIndex = 60;
            // 
            // txtFlowOrder
            // 
            this.txtFlowOrder.Location = new System.Drawing.Point(73, 283);
            this.txtFlowOrder.Name = "txtFlowOrder";
            this.txtFlowOrder.Size = new System.Drawing.Size(446, 20);
            this.txtFlowOrder.TabIndex = 61;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 255);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 62;
            this.label12.Text = "Token";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 286);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 63;
            this.label13.Text = "F Order";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 221);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 13);
            this.label14.TabIndex = 65;
            this.label14.Text = "Url";
            // 
            // txtUrlPay
            // 
            this.txtUrlPay.Location = new System.Drawing.Point(73, 218);
            this.txtUrlPay.Name = "txtUrlPay";
            this.txtUrlPay.Size = new System.Drawing.Size(446, 20);
            this.txtUrlPay.TabIndex = 64;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1165, 743);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtUrlPay);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtFlowOrder);
            this.Controls.Add(this.txtToken);
            this.Controls.Add(this.txtPaymentMethod);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtUrlBase);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtSecretKey);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txtUrlReturn);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtUrlConfirm);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtComerceOrder);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtMail);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCurrency);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtSubject);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtApiKey);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtUrlReturn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtUrlConfirm;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtComerceOrder;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCurrency;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtApiKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txtSecretKey;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtUrlBase;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPaymentMethod;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtToken;
        private System.Windows.Forms.TextBox txtFlowOrder;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtUrlPay;
    }
}

