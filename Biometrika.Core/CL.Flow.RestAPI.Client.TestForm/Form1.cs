﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biometrika.Flow.API;
using Biometrika.Flow.API.Responses;

namespace CL.Flow.RestAPI.Client.TestForm
{
    public partial class Form1 : Form
    {

        internal FlowAPI FLOW_API;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            FLOW_API = new FlowAPI(txtUrlBase.Text, txtApiKey.Text, txtSecretKey.Text);

            richTextBox1.Text = "FLOW_API Creada => " + (FLOW_API != null).ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            object response;
            string strresponse;
            int ret = FLOW_API.CreateOrderPayment(txtSubject.Text, txtCurrency.Text, txtAmount.Text,
                                              txtMail.Text, txtComerceOrder.Text, txtPaymentMethod.Text,
                                              txtUrlConfirm.Text, txtUrlReturn.Text, out strresponse, out response);
            if (ret == 0)
            {
                ResponseCreateOrderPaymentFlow respG = (ResponseCreateOrderPaymentFlow)response;
                richTextBox1.Text = ret + Environment.NewLine +
                    " - url=" + respG.url + Environment.NewLine+
                    " - token=" + respG.token + Environment.NewLine +
                    " - floworder=" + respG.flowOrder ;
                txtFlowOrder.Text = respG.flowOrder.ToString();
                txtToken.Text = respG.token;
                txtUrlPay.Text = respG.url;
            } else
            {
                ResponseBaseFlow resp = (ResponseBaseFlow)response;
                richTextBox1.Text = ret + " - code=" + resp.code + " - msg=" + resp.message;
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            object response;
            string srtrresponse; 
            int ret = FLOW_API.GetStatusOrderPayment(txtToken.Text, txtFlowOrder.Text, out srtrresponse, out response);
            if (ret == 0)
            {
                ResponseStatusOrderPaymentFlow respG = (ResponseStatusOrderPaymentFlow)response;
                richTextBox1.Text = ret + Environment.NewLine +
                    " - Satus=" + respG.status + Environment.NewLine +
                    " - Pay Date=" + (respG.paymentData!=null && !string.IsNullOrEmpty(respG.paymentData.date)? 
                                                                 respG.paymentData.date:"NO Fecha Pago") + Environment.NewLine +
                    " - Medio=" + (respG.paymentData != null && !string.IsNullOrEmpty(respG.paymentData.media) ? 
                                                                 respG.paymentData.media : "NO Medio de Pago") + Environment.NewLine +
                                                                 srtrresponse; 

            }
            else
            {
                ResponseBaseFlow resp = (ResponseBaseFlow)response;
                richTextBox1.Text = ret + " - code=" + resp.code + " - msg=" + resp.message;
            }
        }
    }
}
