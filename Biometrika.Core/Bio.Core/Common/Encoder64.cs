using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;


namespace Bio.Core.Common
{
	/// <summary>
	/// Codifica/Decodifica en Base 64
	/// </summary>
	public sealed class Encoder64
	{

		private Encoder64()
		{
		}
		
		/// <summary>
		///     Codifica un arreglo de bytes en ascii a base 64
		/// </summary>
		/// <param name="input" type="byte[]">
		///     <para>
		///         Arreglo que contiene los bytes a codificar
		///     </para>
		/// </param>
		/// <param name="index" type="int">
		///     <para>
		///         Posicion dentro del arreglo de bytes desde la cual
		///         empezar a codificar
		///     </para>
		/// </param>
		/// <param name="count" type="int">
		///     <para>
		///         Cantidad de bytes que se quiere codificar.
		///     </para>
		/// </param>
		/// <returns>
		///     Un string en base 64 con el contenido de input en base 64
		/// </returns>
		public static string EncodeFromAscii(byte[] input, int index, int count) 
		{
			return Encode(Encoding.ASCII, input, index, count);	
		}

		public static string EncodeFromAscii(Array input, int index, int count)
		{
			return Encode(Encoding.ASCII, (byte[]) input, index, count);
		}
		
		/// <summary>
		///     Codifica un arreglo de bytes en utf8 a base 64
		/// </summary>
		/// <param name="input" type="byte[]">
		///     <para>
		///         Arreglo de bytes a codificar
		///     </para>
		/// </param>
		/// <param name="index" type="int">
		///     <para>
		///         Posicion inicial dentro del arreglo de bytes
		///         para empezar a codificar
		///     </para>
		/// </param>
		/// <param name="count" type="int">
		///     <para>
		///         Cantidad de bytes a codificar
		///     </para>
		/// </param>
		/// <returns>
		///     un string en base 64 codificado a partir de input
		/// </returns>
		public static string EncodeFromUTF8(byte[] input, int index, int count) 
		{
			return Encode(Encoding.UTF8, input, index, count);
		}

		public static string EncodeFromUTF8(Array input, int index, int count) 
		{
			return Encode(Encoding.UTF8, (byte[]) input, index, count);
		}
		/// <summary>
		///     Retorna un string codificado en ascii a partir
		///     de un string que viene en base 64
		/// </summary>
		/// <param name="input" type="string">
		///     <para>
		///         string en base64
		///     </para>
		/// </param>
		/// <returns>
		///     Un string en ascii decodificado
		/// </returns>
		public static string DecodeToAscii(string input)
		{
			return Decode(Encoding.ASCII, input);
		}

		/// <summary>
		///     Decodifica un string en base 64
		/// </summary>
		/// <param name="input" type="string">
		///     <para>
		///         un string en base 64
		///     </para>
		/// </param>
		/// <returns>
		///     un string en utf8
		/// </returns>
		public static string  DecodeToUTF8(string input)
		{
			return Decode(Encoding.UTF8, input);
		}

		/// <summary>
		///     Decodifica un string en base 64
		/// </summary>
		/// <param name="input" type="string">
		///     <para>
		///         un string en base 64
		///     </para>
		/// </param>
		/// <returns>
		///     un string en utf8
		/// </returns>
		public static string  DecodeToUTF7(string input)
		{
			return Decode(Encoding.UTF8, input);
		}

		/// <summary>
		///     Codifica un arreglo de bytes dejandolo en base 64
		/// </summary>
		/// <param name="input" type="byte[]">
		///     <para>
		///         arreglo con los bytes a codificar
		///     </para>
		/// </param>
		/// <param name="index" type="int">
		///     <para>
		///         posicion dentro del array desde la cual empezar
		///         la codificacion
		///     </para>
		/// </param>
		/// <param name="count" type="int">
		///     <para>
		///         cantidad de bytes a codificar
		///     </para>
		/// </param>
		/// <returns>
		///     Un arreglo de bytes en base 64
		/// </returns>
		public static byte[] Encode(byte[] input, int index, int count)
		{
			if (input == null)
				return null;

			MemoryStream ms = new MemoryStream() ; 
			CryptoStream s = new CryptoStream( ms, new ToBase64Transform(), CryptoStreamMode.Write ) ; 
			s.Write( input, index, count ) ; 
			s.Close();
			return ms.ToArray();
		}

		/// <summary>
		///     Decodifica un arreglo de bytes, que viene en base 64
		///     dejandolo en binario
		/// </summary>
		/// <param name="input" type="byte[]">
		///     <para>
		///         arreglo de bytes a codificar
		///     </para>
		/// </param>
		/// <param name="index" type="int">
		///     <para>
		///         indice desde donde empezar la codificacion
		///     </para>
		/// </param>
		/// <param name="count" type="int">
		///     <para>
		///         cantidad de bytes a decodificar
		///     </para>
		/// </param>
		/// <returns>
		///     un arreglo de bytes en binario, decodificados a partir de base 64
		/// </returns>
		public static byte[] Decode(byte[] input, int index, int count)
		{
			if (input == null)
				return null;
				
			MemoryStream msi = new MemoryStream(input, index, count);
			CryptoStream s = new CryptoStream( msi, new FromBase64Transform(), CryptoStreamMode.Read ) ; 
			MemoryStream mso = new MemoryStream();
			int nr = 0;
			byte[] buffer = new byte[1000];
			while ((nr = s.Read(buffer, 0, buffer.Length)) > 0)
			{
				mso.Write(buffer, 0, nr);
			}
			s.Close();
			mso.Close();
			return mso.ToArray();
		}


		/// <summary>
		///     Codifica un arreglo de bytes dejandolo en base64 codificado
		///     de acuerdo al Encoding
		/// </summary>
		/// <param name="encoding" type="System.Text.Encoding">
		///     <para>
		///         El encoding se usa para saber como quedaran
		///         los caracteres (ascii, o utf8)
		///     </para>
		/// </param>
		/// <param name="input" type="byte[]">
		///     <para>
		///         arreglo de bytes a codificar
		///     </para>
		/// </param>
		/// <param name="index" type="int">
		///     <para>
		///         posicion inicial desde la cual empezar las codificacion
		///     </para>
		/// </param>
		/// <param name="count" type="int">
		///     <para>
		///         cantidad de bytes a codificar
		///     </para>
		/// </param>
		/// <returns>
		///     string codificado
		/// </returns>
		public static string Encode(Encoding encoding, byte[] input, int index, int count)
		{
			if (input == null)
				return null;
			return encoding.GetString( Encode(input, index, count) );
		}

		/// <summary>
		///     Decodifica un string en base64 dejandolo en el
		///     encoding dado
		/// </summary>
		/// <param name="encoding" type="System.Text.Encoding">
		///     <para>
		///         encodign (utf8, o ascii) en que quedara el string
		///     </para>
		/// </param>
		/// <param name="input" type="string">
		///     <para>
		///         string en base 64 a decodificar
		///     </para>
		/// </param>
		/// <returns>
		///     Un string decodificado y escrito en el encoding dado.
		/// </returns>
		public static string Decode(Encoding encoding, string input)
		{
			if (input == null)
				return null;
			byte[] buf = encoding.GetBytes(input);
			buf = Decode(buf, 0, buf.Length);
			return encoding.GetString(buf, 0, buf.Length);
		}

	}
}
