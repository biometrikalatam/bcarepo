using System;
using System.Collections.Generic;
using System.Text;

namespace Bio.Core.Constant
{
    public class Errors
    {
        //1 - 100 - Generales
        static public int IERR_OK = 0;
        static public string SERR_OK = "Funcionamiento Correcto";

        static public int IERR_UNKNOWN = -1;
        static public string SERR_UNKNOWN = "Error Desconocido";
		
        static public int IERR_REINTENT = -2;
        static public string SERR_REINTENT = "Reintente";

        static public int IERR_LICENSE = -3;
        static public string SERR_LICENSE = "Licencia Erronea";

        static public int IERR_DATABASE = -4;
        static public string SERR_DATABASE = "Error de Base de Datos";

        static public int IERR_RUT_INVALID = -5;
        static public string SERR_RUT_INVALID = "RUT Invalido";

        static public int IERR_BAD_PARAMETER = -6;
        static public string SERR_BAD_PARAMETER = "Parametro Erroneo";

        static public int IERR_CONX_WS = -7;
        static public string SERR_CONX_WS = "Error en la conexón al web service o api";

        static public int IERR_NORMALIZING_DATA = -8;
        static public string SERR_NORMALIZING_DATA = "Error normalizando data";

        static public int IERR_SERIALIZING_DATA = -9;
        static public string SERR_SERIALIZING_DATA = "Error serializando data";

        static public int IERR_DESERIALIZING_DATA = -10;
        static public string SERR_DESERIALIZING_DATA = "Error deserializando data";

        static public int IERR_SAVING_TX = -11;
        static public string SERR_SAVING_TX = "Error grabando transaccion";

        static public int IERR_SAVING_VERFIED = -12;
        static public string SERR_SAVING_VERFIED = "Error grabando data verificada";

        static public int IERR_INCONSISTENT_ACTION = -13;
        static public string SERR_INCONSISTENT_ACTION = "Id Acción inconsistente para este servicio";

        static public int IERR_ACTION_NOT_SUPPORTED = -14;
        static public string SERR_ACTION_NOT_SUPPORTED = "Acción no soportada en esta version";

        static public int IERR_COMPANY_NOT_EXIST = -15;
        static public string SERR_COMPANY_NOT_EXIST = "Compañia no existente";

        static public int IERR_UNAUTHORIZED = -16;
        static public string SERR_UNAUTHORIZED = "User/Psw No Autorizado para uso del servicio";

        //30
        static public int IERR_NO_LOCAL_DATA = -30;
        static public string SERR_NO_LOCAL_DATA = "No hay datos locales";

        static public int IERR_IDENTITY_NOT_FOUND = -31;
        static public string SERR_IDENTITY_NOT_FOUND = "Identidad no encontrada";

        static public int IERR_BIR_NOT_FOUND = -32;
        static public string SERR_BIR_NOT_FOUND = "No hay datos para realizar la comparacion";

        static public int IERR_IDENTITY_EXIST = -33;
        static public string SERR_IDENTITY_EXIST = "Ya existe la identidad";

        //40
        static public int IERR_WSQ_INVALID = -40;
        static public string SERR_WSQ_INVALID = "WSQ Invalido";

        static public int IERR_WSQ_LOW_QUALITY = -41;
        static public string SERR_WSQ_LOW_QUALITY = "WSQ de baja calidad";

        static public int IERR_WSQ_DECOMPRESSING = -42;
        static public string SERR_WSQ_DECOMPRESSING = "Descomprimiendo WSQ";

        static public int IERR_WSQ_COMPRESSING = -43;
        static public string SERR_WSQ_COMPRESSING = "Comprimiendo WSQ";
        
        //50
        static public int IERR_MATCH_VALIDATION = -50;
        static public string SERR_MATCH_VALIDATION = "Error en proceso de matching";

        static public int IERR_VERIFY = -51;
        static public string SERR_VERIFY = "Error en proceso de verificacion";

        static public int IERR_IDENTIFY = -52;
        static public string SERR_NO_IDENTIFY = "Error en proceso de identifiacion";

        static public int IERR_MATCHER_NOT_AVAILABLE = -53;
        static public string SERR_MATCHER_NOT_AVAILABLE = "No existe Matcher disponible";

        static public int IERR_EXTRACTING = -54;
        static public string SERR_EXTRACTING = "Error extrayendo minucias";

        static public int IERR_INVALID_TEMPLATE = -55;
        static public string SERR_INVALID_TEMPLATE = "Template invalido";

        static public int IERR_NULL_TEMPLATE = -56;
        static public string SERR_NULL_TEMPLATE = "Template nulo";

        static public int IERR_INVALID_TOKEN = -57;
        static public string SERR_INVALID_TOKEN = "Token invalido";

        static public int IERR_NULL_TOKEN = -58;
        static public string SERR_NULL_TOKEN = "Token nulo";

        static public int IERR_UNKNOWN_TOKEN = -59;
        static public string SERR_UNKNOWN_TOKEN = "Token desconocido";

        //70 - 79 - Connectors
        static public int IERR_CONNECTOR_NOT_CONFIGURED = -70;
        static public string SERR_CONNECTOR_NOT_CONFIGURED = "Conector no configurado correctamente";

        static public int IERR_CONNECTOR_NOT_AVAILABLE = -71;
        static public string SERR_CONNECTOR_NOT_AVAILABLE = "Conector no disponible";
        
        static public int IERR_CONNECTOR_REMOTE_ERROR = -72;
        static public string SERR_CONNECTOR_REMOTE_ERROR = "Error en la ejecución remota del web services";

        static public int IERR_CONNECTOR_MALFORMED_ANSWER = -73;
        static public string SERR_CONNECTOR_MALFORMED_ANSWER = "Respuesta de conector mal formada";

        //80 - 90 - PKI
        static public int IERR_CERTIFICATE_EXPIRED = -80;
        static public string SERR_CERTIFICATE_EXPIRED = "Certificado vencido";

        static public int IERR_CERTIFICATE_NOT_EQUAL = -81;
        static public string SERR_CERTIFICATE_NOT_EQUAL = "Certificado No Igual";

        //100 - 110 - SRCeI
        static public int IERR_USER_INVALID_IN_SRCeI = -100;
        static public string SERR_USER_INVALID_IN_SRCeI = "Usuario invalido en SRCeI";
		
        static public int IERR_CONX_WS_SRCeI = -101;
        static public string SERR_CONX_WS_SRCeI = "Error en la conexion al web service del SRCeI";

        static public int IERR_XML_INVALID_IN_SRCeI = -102;
        static public string SERR_XML_INVALID_IN_SRCeI = "XML del SRCeI erroneo";

        static public int IERR_NO_DATA_IN_SRCeI = -103;
        static public string SERR_NO_DATA_IN_SRCeI = "No existen datos en SRCeI";

        static public int IERR_IMAGE_BAD_QUALITY_IN_SRCeI = -104;
        static public string SERR_IMAGE_BAD_QUALITY_IN_SRCeI = "Imagen de mala calidad para SRCeI";

         //200
        static public int IERR_BAD_SERIALIZER_CONFIG_FILE = -201;
        static public string SERR_BAD_SERIALIZER_CONFIG_FILE = "El paso de parámetros no permitió realizar la serialización del objeto correctamente";

        static public int IERR_SELECT_SERIALIZER_CONFIG_FILE = -202;
        static public string SERR_SELECT_SERIALIZER_CONFIG_FILE = "No se han seleccionado correctamente los datos de configuración";

        static public int IERR_SAVE_CONFIG_FILE = -203;
        static public string SERR_SAVE_CONFIG_FILE = "El archivo de configuración fue modificado con éxito";

        //300
        static public int IERR_BAD_SERIALIZER_COMPANYS = -301;
        static public string SERR_BAD_SERIALIZER_COMPANYS = "Imposible serializar la lista de companias";


        //400
        static public int IERR_DUPLICATE_ORIGIN = -401;
        static public string SERR_DUPLICATE_ORIGIN = "No se puede modificar el origen porque crearía valores duplicados";

        static public int IERR_DELETE_ORIGIN = -402;
        static public string SERR_DELETE_ORIGIN = "No se puede eliminar el origen.";

        //500
        static public int IERR_CLIENT_NOT_AUTHORIZED = -501;
        static public string SERR_CLIENT_NOT_AUTHORIZED = "Cliente No Autorizado en la plataforma";

        //600
        static public int IERR_TX_NOT_EXIST = -601;
        static public string SERR_TX_NOT_EXIST = "Transacción no existe";
        static public int IERR_TX_CONSUMED = -602;
        static public string SERR_TX_CONSUMED = "Transacción consumida";

        //700
        static public int IERR_3RO_FAILED_RESULT = -701;
        static public string SERR_3RO_FAILED_RESULT = "Respuesta FAILED de servicio de 3ero";

        static public int IERR_3RO_RESPONSE_NULL = -702;
        static public string SERR_3RO_RESPONSE_NULL = "Respuesta nula de servicio de 3ero";

        static public int IERR_3RO_DENIED_FRAUD = -703;
        static public string SERR_3RO_DENIED_FRAUD = "Fraude Detectado";

        static public int IERR_3RO_DENIED_UNSUPPORTED_ID_TYPE = -704;
        static public string SERR_3RO_DENIED_UNSUPPORTED_ID_TYPE = "Id Type No soportado";

        static public int IERR_3RO_DENIED_UNSUPPORTED_ID_COUNTRY = -705;
        static public string SERR_3RO_DENIED_UNSUPPORTED_ID_COUNTRY = "Pais No soportado";

        static public int IERR_3RO_ERROR_NOT_READABLE_ID = -706;
        static public string SERR_3RO_ERROR_NOT_READABLE_ID = "Document No Legible";

        static public int IERR_3RO_NO_ID_UPLOADED = -707;
        static public string SERR_3RO_NO_ID_UPLOADED = "Documento No cargado (caducado)";

        static public int IERR_3RO_NO_IN_DATABASE = -708;
        static public string SERR_3RO_NO_IN_DATABASE = "TrackId de tercero no existen en BD";

        static public int IERR_NO_ENROLLTYPE_ENABLED = -709;
        static public string SERR_NO_ENROLLTYPE_ENABLED = "Tipo de enrolamiento no soportado";

        static public int IERR_NO_MATCH_ENROLL_COMBINED = -710;
        static public string SERR_NO_MATCH_ENROLL_COMBINED = "No hubo match para enroll combinado";

        static public int IERR_ERROR_IN_MATCH_ENROLL_COMBINED = -711;
        static public string SERR_ERROR_IN_MATCH_ENROLL_COMBINED = "Error en proceso de match para enroll combinado";

        //800 - POSController
        static public int IERR_CREATING_PENDING_CN = -800;
        static public string SERR_CREATING_PENDING_CN = "Transaccion Pendiente de Proceso Cedula Nueva";

        static public int IERR_CREATING_PENDING_CA = -801;
        static public string SERR_CREATING_PENDING_CA = "Transaccion Pendiente de Proceso Cedula Antigua";

        static public int IERR_CREATING_TX = -802;
        static public string SERR_CREATING_TX = "Error creando transaccion";

        static public int IERR_TX_COMPLETED = -803;
        static public string SERR_TX_COMPLETED = "Error transaccion procesada con anterioridad";


        static public string GetDescription(int error)
        {
            switch (error)
            {
                //1 - 100 - Generales
                case 0: 
                    return SERR_OK;
                case -1: 
                    return SERR_UNKNOWN;
                case -2: 
                    return SERR_REINTENT;
                case -3:
                    return SERR_LICENSE;
                case -4: 
                    return SERR_DATABASE;
                case -5: 
                    return SERR_RUT_INVALID;
                case -6: 
                    return SERR_BAD_PARAMETER;
                case -7:
                    return SERR_CONX_WS;
                case -8:
                    return SERR_NORMALIZING_DATA;
                case -9:
                    return SERR_SERIALIZING_DATA;
                case -10:
                    return SERR_DESERIALIZING_DATA;
                case -11:
                    return SERR_SAVING_TX;
                case -12:
                    return SERR_SAVING_VERFIED;
                case -13:
                    return SERR_INCONSISTENT_ACTION;
                case -14:
                    return SERR_ACTION_NOT_SUPPORTED;
                case -15:
                    return SERR_COMPANY_NOT_EXIST;
                case -16:
                    return SERR_UNAUTHORIZED;

                //30
                case -30: 
                    return SERR_NO_LOCAL_DATA;

                case -31:
                    return SERR_IDENTITY_NOT_FOUND;

                case -32:
                    return SERR_BIR_NOT_FOUND;
                
                case -33:                
                    return SERR_IDENTITY_EXIST;
                    
                //40
                case -40:
                    return SERR_WSQ_INVALID;

                case -41:
                    return SERR_WSQ_LOW_QUALITY;

                case -42:
                    return SERR_WSQ_DECOMPRESSING;

                case -43:
                    return SERR_WSQ_COMPRESSING;

                //50
                case -50: 
                    return SERR_MATCH_VALIDATION;

                case -51: 
                    return SERR_VERIFY;

                case -52:
                    return SERR_NO_IDENTIFY;

                case -53:
                    return SERR_MATCHER_NOT_AVAILABLE;

                case -54:
                    return SERR_EXTRACTING;

                case -55:
                    return SERR_INVALID_TEMPLATE;

                 case -56:
                    return SERR_NULL_TEMPLATE;

                 case -57:
                    return SERR_INVALID_TOKEN;

                 case -58:
                    return SERR_NULL_TOKEN;

                 case -59:
                    return SERR_UNKNOWN_TOKEN;

                    //70-79 - Connectors
                case -70:
                    return SERR_CONNECTOR_NOT_CONFIGURED;

                case -71:
                    return SERR_CONNECTOR_NOT_AVAILABLE;

                case -72:
                    return SERR_CONNECTOR_REMOTE_ERROR;

                case -73:
                    return SERR_CONNECTOR_MALFORMED_ANSWER;

                     //80 - 90 - PKI
                case -80:
                    return SERR_CERTIFICATE_EXPIRED;

                case -81:
                    return SERR_CERTIFICATE_NOT_EQUAL;

                //100 - 110 - SRCeI
                case -100: 
                    return SERR_USER_INVALID_IN_SRCeI;
        		
                case -101: 
                    return SERR_CONX_WS_SRCeI;

                case -102: 
                    return SERR_XML_INVALID_IN_SRCeI;

                case -103: 
                    return SERR_NO_DATA_IN_SRCeI;

                case -104: 
                    return SERR_IMAGE_BAD_QUALITY_IN_SRCeI;

                //200
                case -201:
                    return SERR_BAD_SERIALIZER_CONFIG_FILE;

                case -202:
                    return SERR_SELECT_SERIALIZER_CONFIG_FILE;

                case -203:
                    return SERR_SAVE_CONFIG_FILE;

                case -401:
                    return SERR_DUPLICATE_ORIGIN;
                case -402:
                    return SERR_DELETE_ORIGIN;

                case -501:
                    return SERR_CLIENT_NOT_AUTHORIZED;

                case -601:
                    return SERR_TX_NOT_EXIST;
                case -602:
                    return SERR_TX_CONSUMED;

                case -701:
                    return SERR_3RO_FAILED_RESULT;

                case -702:
                    return SERR_3RO_RESPONSE_NULL; // = "Respuesta nula de servicio de 3ero";

                case -703:
                    return SERR_3RO_DENIED_FRAUD; // = "Fraude Detectado";

                case -704:
                    return SERR_3RO_DENIED_UNSUPPORTED_ID_TYPE; // = "Id Type No soportado";

                case -705:
                    return SERR_3RO_DENIED_UNSUPPORTED_ID_COUNTRY; // = "Pais No soportado";

                case -706:
                    return SERR_3RO_ERROR_NOT_READABLE_ID; // = "Document No Legible";

                case -707:
                    return SERR_3RO_NO_ID_UPLOADED; // = "Documento No cargado (caducado)";

                case -708:
                    return SERR_3RO_NO_IN_DATABASE; // = "TrackId de tercero no existen en BD";

                case -709:
                    return SERR_NO_ENROLLTYPE_ENABLED; // = "Tipo de enrolamiento no soportado";

                case -710:
                    return SERR_NO_MATCH_ENROLL_COMBINED; // = "No hubo match para enroll combinado";

                case -711:
                    return SERR_ERROR_IN_MATCH_ENROLL_COMBINED; // = "Error en proceso de match para enroll combinado";

                //800 - POSController
                case -800:
                    return SERR_CREATING_PENDING_CN; // = "Transaccion Pendiente de Proceso Cedula Nueva";

                case -801:
                    return SERR_CREATING_PENDING_CA; // = "Transaccion Pendiente de Proceso Cedula Antigua";

                case -802:
                    return SERR_CREATING_TX; // = "Error creando transaccion";

                case -803:
                    return SERR_TX_COMPLETED; // = "Error transaccion procesada con anterioridad";

                default:
                    return "Error No Documentado";
            }
        
        }

        
    }

}
