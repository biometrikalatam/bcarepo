﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bio.Core.QR
{
    public partial class FormQR : Form
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(FormQR));
        public FormQR()
        {
            InitializeComponent();
        }

        public string GetImgQRB64(string url)
        {
            string ret = "";
            try
            {
                this.qrCodeImgControl1.Text = url;
                byte[] byImgQR = null;
                using (System.IO.MemoryStream m = new System.IO.MemoryStream())
                {
                    qrCodeImgControl1.Image.Save(m, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byImgQR = m.ToArray();
                }

                ret = Convert.ToBase64String(byImgQR);
            }
            catch (Exception ex)
            {
                ret = "";
                LOG.Error("Error FormQR.GetImgQRB64", ex);
            }
            return ret;
        }

        private void FormQR_Load(object sender, EventArgs e)
        {

        }
    }
}
