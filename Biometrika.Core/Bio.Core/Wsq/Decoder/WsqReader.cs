using System.IO;

namespace Bio.Core.Wsq.Decoder
{
	/// <summary>
	/// La clase WsqReader implementa un lector de stream, especificco para archivos wsq.
	/// </summary>
	public sealed class WsqReader
	{
		const int SymbolStart = 0xFF;

		private BinaryReader reader; 
        private Stream stream;        		
		

		public WsqReader(Stream stream)
		{
			this.stream = stream;
			reader = new BinaryReader(stream);
		}
		/// <summary>
		/// Retorna un DecoderFrame que se lee desde el stream
		/// </summary>
		/// <returns>El decoder frame, o null si hay problemas</returns>
		public Frame ReadFrame()
		{
			if (!ReadSymbol(SOI))
				throw new DecoderException("El formato no es wsq");
			Frame frame = new Frame();
			frame.SetLength((int) stream.Length);
			ReadHeader(frame);
			while (frame.AddBlock(ReadEntropyBlock(frame)))
				;
			CheckSymbol(EOI);
			return frame;
		}


		private EntropyEncodedBlock ReadEntropyBlock (Frame frame)
		{
			EntropyEncodedBlock ecs = ReadBlockHeader(frame);
			if (ecs == null)
				return null;

			long startPos = stream.Position;
			byte sym = 0;
			while (true)
			{
				sym = reader.ReadByte ();
				if (sym == SymbolStart)
				{
					int data2 = reader.ReadByte ();
					if (data2 != 0)
					{
						lastSymbol = (ushort) ((sym << 8) | data2);
						break;
					}
				}
			}
			long endPos = stream.Position-2;
			//pos = startPos;
			stream.Position = startPos;
			byte[] data = reader.ReadBytes((int) (endPos-startPos));
			ecs.SetData(data, 0, data.Length);
			return ecs;
		}

		private EntropyEncodedBlock ReadBlockHeader(Frame frame)
		{
			while (ReadTable(frame))
				;
			if (lastSymbol != SOB)
				return null;
			ReadShort();
			EntropyEncodedBlock ecs = new EntropyEncodedBlock(frame);
			ecs.SetTableSelector(reader.ReadByte ());
			ecs.SetHuffmanTables(lastHuffmanTables);
			return ecs;
		}

		private void ReadHeader (Frame frame)
		{
			while (ReadTable(frame))
				;
			if (lastSymbol != SOF)
				throw new DecoderException("Se esperaba {0:x}, pero se encontro {1:x}", SOF, lastSymbol);
			int lf = ReadShort()-2;
			byte[] data = reader.ReadBytes (lf);
			frame.ReadHeader(data, 0, lf);
			//pos += lf;
		}

		private int lastSymbol;

		private bool ReadTable (Frame frame)
		{
			int symbol = ReadShort();
			switch (symbol)
			{
				case DTT: 
					ReadTransformTable(frame);
					return true;
				case DQT: 
					ReadQuantizationTable(frame);
					return true;
				case DHT: 
					ReadHufmanTables(frame);
					return true;
				case COM: 
					ReadComment(frame);
					return true;
			}
			lastSymbol = symbol;
			return false;
		}

		/// <summary>
		/// Comentarios seran ignorados esta vez
		/// </summary>
		private void ReadComment (Frame frame)
		{
			int lc = ReadShort()-2;
			byte[] data = reader.ReadBytes (lc);
			frame.AddComment(data, 0, lc);
		}

		private void ReadHufmanTables (Frame frame)
		{
			int lh = ReadShort()-2;
			byte[] data = reader.ReadBytes (lh);
			lastHuffmanTables = new HuffmanTables(data, 0, lh);
			frame.SetHuffmanTables(lastHuffmanTables);
		}

		private void ReadQuantizationTable (Frame frame)
		{
			int lq = ReadShort()-2;
			byte[] data = reader.ReadBytes(lq);
			frame.ParseQuantizationTable(data, 0, lq);
//			pos += lq;
		}

		private void ReadTransformTable (Frame frame)
		{
			int lt = ReadShort() - 2;
			byte[] data = reader.ReadBytes (lt);
			frame.ParseTransformationTables(data, 0);
		}


		private bool ReadSymbol(ushort symbol)
		{
			lastSymbol = ReadShort();
			return lastSymbol == symbol;
		}

		private int ReadShort()
		{
			ushort data = (ushort) reader.ReadInt16 ();
			return (data >> 8) | ((data & 0xff) << 8);
			//int result = (data[pos] << 8) | data[pos+1];
			//pos += 2;
			//return result;
		}

		
		private HuffmanTables lastHuffmanTables;
		
			
		private const ushort SOI = (ushort) 0xffa0u;
		private const ushort EOI = (ushort) 0xffa1u;
		private const ushort SOF = (ushort) 0xffa2u;
		private const ushort SOB = (ushort) 0xffa3u;
		private const ushort DTT = (ushort) 0xffa4u;
		private const ushort DQT = (ushort) 0xffa5u;
		private const ushort DHT = (ushort) 0xffa6u;
		private const ushort COM = (ushort) 0xffa8u;

		private void CheckSymbol (ushort symbol)
		{
			if (lastSymbol != symbol)
				throw new DecoderException("Se esperaba simbolo {0:x}, pero se encontro {1:x}", symbol, lastSymbol);
		}

		
	}
}
