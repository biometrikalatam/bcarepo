﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bio.Core.Utils
{
    public class Utils
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Utils));

        internal static double GetPorcentajeNEC(double score, double threshold)
        {
            double ret = 100;
            try
            {
                log.Debug("DEHelper.GetPorcentajeNEC - IN => score = " + score.ToString());
                if (score >= 15000 - threshold)
                {
                    ret = 100;
                }
                else
                {
                    double resto = 15000 - threshold;  //Resto de la diferencia entre 15000 y el umbral
                    double porcentajeResto = score * 100 / resto; //Calculo el porcentaje de lo que significa ese score para ese resto
                    double toSume = 20 * porcentajeResto / 100; //Saco el porcentaje sobre el 20% variable que uso
                    ret = 80 + toSume; //A un 80% fijo por ser positivo le sumo el variable obtenido
                    if (ret > 100) //Por si acaso se pasa (no deberia pero para estar seguro)
                        ret = 100;
                }

            }
            catch (Exception ex)
            {
                ret = 100;
                log.Error("DEHelper.GetPorcentajeNEC - Error: " + ex.Message);
            }
            log.Debug("DEHelper.GetPorcentajeNEC - OUT! - Porcentaje = " + ret.ToString());
            return ret;
        }

        /// <summary>
        /// Parsea una fecha con diferentes formatos. Sino devuelve MinValue
        /// Formatos de fecha procesados:
        ///     yyyy/MM/dd
        ///     yyyy/M/dd
        ///     dd/MM/yyyy
        ///     dd/M/yyyy
        ///     M/dd/yyyy
        ///     MM/dd/yyyy
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static DateTime Parse(string strDate)
        {
            DateTime dRet = new DateTime(1900, 1, 1);
            bool mustReturn = false;
            try
            {
                log.Debug("Utils.Parse IN - Fechav = " + (string.IsNullOrEmpty(strDate) ? "Null" : strDate));
                if (string.IsNullOrEmpty(strDate))
                    return dRet;

                //Reemplazo - por /
                strDate = strDate.Replace("-", "/");
                log.Debug("Utils.Parse IN - Fechav = " + strDate);
                try
                {
                    log.Debug("Utils.Parse try yyyy/MM/dd...");
                    dRet = DateTime.ParseExact(strDate, "yyyy/MM/dd", null);
                    log.Debug("Utils.Parse try yyyy/MM/dd OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;

                try
                {
                    log.Debug("Utils.Parse try yyyy/M/dd...");
                    dRet = DateTime.ParseExact(strDate, "yyyy/M/dd", null);
                    log.Debug("Utils.Parse try yyyy/M/dd OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;
                //------------------
                try
                {
                    log.Debug("Utils.Parse try dd/MM/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
                    log.Debug("Utils.Parse try dd/MM/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;

                try
                {
                    log.Debug("Utils.Parse try dd/M/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "dd/M/yyyy", null);
                    log.Debug("Utils.Parse try dd/M/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;

                try
                {
                    log.Debug("Utils.Parse try M/dd/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "M/dd/yyyy", null);
                    log.Debug("Utils.Parse try M/dd/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;

                try
                {
                    log.Debug("Utils.Parse try MM/dd/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "MM/dd/yyyy", null);
                    log.Debug("Utils.Parse try MM/dd/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;

                try
                {
                    log.Debug("Utils.Parse try M/d/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "M/d/yyyy", null);
                    log.Debug("Utils.Parse try M/d/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;

                try
                {
                    log.Debug("Utils.Parse try yyMMdd...");
                    dRet = DateTime.ParseExact(strDate, "yyMMdd", null);
                    log.Debug("Utils.Parse try yyMMdd OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }
            }
            catch (Exception ex)
            {
                log.Error("Utils.Pars Error: " + ex.Message);
            }
            return dRet;
        }

        internal static string EnmascaraMail(string destinataryMail)
        {
            string sRet = "NULL";
            try
            {
                log.Debug("Utils.EnmascaraMail IN...");
                if (string.IsNullOrEmpty(destinataryMail))
                {
                    log.Debug("Utils.EnmascaraMail - Mail Recibido nulo. Sale...");
                    sRet = "NULL";
                }
                else
                {
                    log.Debug("Utils.EnmascaraMail - Mail Recibido => " + destinataryMail);
                    string[] partes = destinataryMail.Split('@');
                    if (partes.Length != 2)
                        return sRet;

                    partes[0] = partes[0].Substring(0, 1) + "***";
                    sRet = partes[0] + "@" + partes[1];
                }
            }
            catch (Exception ex)
            {
                sRet = "NULL";
                log.Error("Utils.EnmascaraMail Error: " + ex.Message);
            }
            log.Debug("Utils.EnmascaraMail - OUT! => sale con valor => " + sRet);
            return sRet;
        }

        public static int CalcularEdad(DateTime fechaNacimiento)
        {
            // Obtiene la fecha actual:
            DateTime fechaActual = DateTime.Today;

            // Comprueba que la se haya introducido una fecha válida; si 
            // la fecha de nacimiento es mayor a la fecha actual se muestra mensaje 
            // de advertencia:
            if (fechaNacimiento > fechaActual)
            {
                Console.WriteLine("La fecha de nacimiento es mayor que la actual.");
                return -1;
            }
            else
            {
                int edad = fechaActual.Year - fechaNacimiento.Year;

                // Comprueba que el mes de la fecha de nacimiento es mayor 
                // que el mes de la fecha actual:
                if (fechaNacimiento.Month >= fechaActual.Month)
                {
                    if (fechaNacimiento.Day > fechaActual.Day)
                    {
                        --edad;
                    }
                }

                return edad;
            }
        }

        public static string ToStringFromProperty(object obj, int flag = 0)
        {
            string ret = "";
            bool isFirst = true;
            object oAux = null;
            try
            {
                log.Debug("Utils.ToStringFromProperty IN...");
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
                {
                    oAux = descriptor.GetValue(obj);
                    if (oAux == null) oAux = "";
                    if (isFirst)
                    {
                        isFirst = false;
                        if (flag == 0) ret = descriptor.Name + " = " + oAux.ToString() + Environment.NewLine;
                        else ret = descriptor.Name + " = " + oAux.ToString() + "|";
                    }
                    else
                    {
                        if (flag == 0) ret += descriptor.Name + " = " + oAux.ToString() + Environment.NewLine;
                        else ret += descriptor.Name + " = " + oAux.ToString() + "|";
                    }
                }
            }
            catch (Exception ex)
            {
                ret = "Utils.ToStringFromProperty Excp Error: " + ex.Message;
                log.Error("Utils.ToStringFromProperty Excp Error: ", ex);
            }
            log.Debug("Utils.ToStringFromProperty OUT!");
            return ret;
        }
    }
}
