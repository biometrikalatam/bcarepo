using System;
using System.Runtime.InteropServices;
using Bio.Core.Matcher.Constant;
using log4net;
//using stdole;


namespace Bio.Core.Utils
{
	/// <summary>
	/// Summary description for BFTWrapper.
	/// </summary>
	public class BioLibWrapper
	{
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BioLibWrapper));

		public BioLibWrapper() { }
		
		[DllImport("biolib.dll")]
		public static extern int bok_wsq_encode(int hraw_image, float factor);

		[DllImport("biolib.dll")]
		public static extern int bok_wsq_decode(int hraw_image);

		
		[DllImport("biolib.dll")]
		public static extern int bfc_resize(int sample, 
			int new_width,
			int new_height, 
			int back_color); 

		[DllImport("biolib.dll")]
		public static extern string bfc_sample_to_base64_str(int sample, 
			int include_extra_info); 
		
        //[DllImport("biolib.dll")]
        //public static extern int bfc_from_ipicture2(IPicture pic, int resX, 
        //    int resY, int resNewX, 
        //    int resNewY);

        //[DllImport("biolib.dll")]
        //public static extern int bfc_from_ipicture(IPicture pic, int resX, int resY);

		[DllImport("biolib.dll")]
		public static extern int bfc_new_sample(byte[] raw, int width, 
			int height, int depth, 
			int resolutionx, int resolutiony, 
			int stride);
        
		[DllImport("biolib.dll")]
		public static extern int bfc_delete_sample(int shandle);

		[DllImport("biolib.dll")]
		public static extern int bfc_sample_rawsize(int hsample);

		[DllImport("biolib.dll")]
		public static extern int bfc_copy_to_raw(int shandle, byte[] raw_dest, int raw_dest_size);

		[DllImport("biolib.dll")]
		public static extern int bfc_sample_width(int shandle);

		[DllImport("biolib.dll")]
		public static extern int bfc_sample_height(int shandle);

		[DllImport("biolib.dll")]
		public static extern int bfc_quality(int shandle);

		[DllImport("biolib.dll")]
		public static extern int bfc_quality_class(int shandle);

		[DllImport("biolib.dll")]
		public static extern void bok_bfc_startup();

		[DllImport("biolib.dll")]
		public static extern void bok_bfc_shutdown();

		[DllImport("biolib.dll")]
		public static extern int bft_sample_to_template(int handle, int format, 
			int threshold, out int score, 
			byte[] args);
        
		[DllImport("biolib.dll")]
		public static extern int bft_delete_template(int handle);
        
		[DllImport("biolib.dll")]
		public static extern int bft_load_from_file(byte[] path, int format);
        
		[DllImport("biolib.dll")]
		public static extern int bft_match(int template1, int template2, double threshold, int algorithm, out double score, byte[] args);
        
		[DllImport("biolib.dll")]
		public static extern int bfc_load_from_file(byte[] file, int format);

		[DllImport("biolib.dll")]
		public static extern int bft_load_template(byte[] byTemplate, int size);

		[DllImport("biolib.dll")]
		public static extern int bfc_sensor_find(int sensor_class, int model);

		[DllImport("biolib.dll")]
		public static extern int bft_sensor_open(int sensor_class, int sensor_model, byte[] args);

		[DllImport("biolib.dll")]
		public static extern int bft_sensor_close(int sensor_handle);

		[DllImport("biolib.dll")]
		public static extern int bft_get_sample(int sensor, int timeout, int normalize, int window);

		[DllImport("biolib.dll")]
		public static extern int bft_enroll(int sensor, int timeout, int normalize, int window);

		[DllImport("biolib.dll")]
		public static extern string bft_template_to_base64_str(int template);

	    [DllImport("biolib.dll")]
	    public static extern int bfc_resample(int shandle, int new_resolution_x,
	                                          int new_resolution_y, int mode); 

//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
//'' INICIO - Wrapper de WSQ
//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	    [DllImport("biolib.dll")]
	    public static extern int bok_wsq_encode(int hraw_image, int factor);

        [DllImport("biolib.dll")]
	    public static extern int bok_wsq_encode2(int hraw_image, int factor);

//        'Se usa para crear un handle a una imagen wsq que se tiene en memoria (probablemente cargada desde un archivo).
//            'wsq_image: puntero a un arreglo de bytes que contiene la imagen en formato wsq.
//            'length: largo del arreglo de bytes.
	    [DllImport("biolib.dll")]
	    public static extern int bok_new_wsq_sample(byte Image, int Size);
    
        //'Public Declare Function bft_wsq_from_base64_str Lib "biolib.dll" _
        //'    (ByVal wsq As String) As Long
    
//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
//'' FIN - Wrapper de WSQ
//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        public static string GetWSQFromArrayImage(byte[] byImage, 
                                                  int w, int h, int wd, int hd,
                                                  int umbralquality, int formatSample, 
                                                  int deviceid, out int quality) 
        {
            int lHandle = 0;
            int lHandleWSQ = 0;
            int iErr = 0;
            quality = 0;
            string ret = null;

            try {
                
                if (deviceid == Devices.DEVICE_DIGITALPERSONA)
                {
                    lHandle = bfc_new_sample(byImage, w, h, 8, 700, 700, 0);
                    iErr = bfc_resample(lHandle, 500, 500, 1);
                    if (iErr < 0)
                    {
                        LOG.Debug("Error BioLib Handle (2) [" + iErr + "]");
                        return null;
                    }
                }
                else 
                {
                    lHandle = bfc_new_sample(byImage, w, h, 8, 500, 500, 0);
                }
                if (lHandle <= 0)
                {
                    LOG.Debug("Error BioLib Handle (1) [" + lHandle + "]");
                    return null;
                }

                //Hago resize para dejarlo en 512x512 y relleno con blanco
                iErr = bfc_resize(lHandle, wd, hd, 255);
                if (iErr < 0)
                {
                    LOG.Debug("Error BioLib (3) [Resize] (" + iErr + ")");
                    return null;
                }

                //Chequeo calidad, sino salgo
                //  '0 a 199: Pobre
                //  '200 a 399: Regular
                //  '400 a 599: Buena
                //  '600 a 799: Muy Buena
                //  '800 a 1000: Excelente.
                quality = bfc_quality(lHandle);
                if (umbralquality > 0 && quality < umbralquality)
                {
                    //iErr = quality;
                    LOG.Debug("Error BioLib Handle (3.1) [quality poor = " + 
                        quality.ToString() + " - Umbral = " + umbralquality.ToString() + "]");
                    return null;
                }

                if (formatSample == 0)
                {
                    //SampleFormat.SAMPLE_FORMAT_WSQ) {
                    //Comprimo RAW a WSQ
                    lHandleWSQ = bok_wsq_encode2(lHandle, 53);
                    if (lHandleWSQ <= 0)
                    {
                        LOG.Debug("Error BioLib (4) [No WSQ] [" + lHandleWSQ + "]");
                        return null;
                    }
                    ret = bfc_sample_to_base64_str(lHandleWSQ, 0);
                } else
                {
                    //'Es RAW
                    ret = bfc_sample_to_base64_str(lHandle, 0);
                }

                //Libero memoria de ambas muestras RAW y WSQ
                iErr = bfc_delete_sample(lHandle);
                if (iErr < 0)
                {
                    LOG.Debug("Error BioLib (5) [" + iErr + "]");
                    return null;
                }

                if (formatSample == 0)
                {
                    //SampleFormat.SAMPLE_FORMAT_WSQ) Then
                    iErr = bfc_delete_sample(lHandleWSQ);
                    if (iErr < 0)
                    {
                        LOG.Debug("Error BioLib (6) [" + iErr + "]");
                        return null;
                    }
                }
            } catch (Exception ex)
            {
                LOG.Debug("BioLibWrapper.GetWSQFromArrayImage", ex); 
                ret = null;
            }
            return ret;
        }

	}
}
