using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Xml;
using Bio.Core.pki.utils;
using log4net;

namespace Bio.Core.pki.xmldsig
{
    /// <summary>
    /// Descripción breve de Class1.
    /// </summary>
    public class BKSignerXML
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(BKSignerXML));

        public BKSignerXML() { }

        // Sign an XML file. 
        // This document cannot be verified unless the verifying 
        // code has the key with which it was signed.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Doc"></param>
        /// <param name="Key"></param>
        /// <param name="ReferenceUri"></param>
        /// <param name="PathCert"></param>
        /// <returns></returns>
        public static bool SignXml(XmlDocument Doc, AsymmetricAlgorithm Key, string  ReferenceUri, string PathCert) //RSA Key)
        {
            try
            {
                // Check arguments.
                if (Doc == null) return false;
                //throw new ArgumentException("Doc NULL");
                if (Key == null) return false;
                //throw new ArgumentException("Key NULL");

                // Create a SignedXml object.
                SignedXml signedXml = new SignedXml(Doc);

                // Add the key to the SignedXml document.
                signedXml.SigningKey = Key;

                // Create a reference to be signed.
                if (!String.IsNullOrEmpty(ReferenceUri))
                {
                    Reference reference = new Reference();
                    reference.Uri = "#IDRDREF.v1";
                
                    // Add an enveloped transformation to the reference.
                    XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
                    reference.AddTransform(env);

                    // Add the reference to the SignedXml object.
                    signedXml.AddReference(reference);
                }

                // Add an RSAKeyValue KeyInfo (optional; helps recipient find key to validate).
                KeyInfo keyInfo = new KeyInfo();
                keyInfo.AddClause(new RSAKeyValue((RSA)Key));
                // Load the X509 certificate.
                X509Certificate MSCert = X509Certificate.CreateFromCertFile(PathCert);

                // Load the certificate into a KeyInfoX509Data object
                // and add it to the KeyInfo object.
                keyInfo.AddClause(new KeyInfoX509Data(MSCert));
                signedXml.KeyInfo = keyInfo;

                // Compute the signature.
                signedXml.ComputeSignature();

                // Get the XML representation of the signature and save
                // it to an XmlElement object.
                XmlElement xmlDigitalSignature = signedXml.GetXml();

                // Append the element to the XML document.
                Doc.DocumentElement.AppendChild(Doc.ImportNode(xmlDigitalSignature, true));

            }
            catch (Exception ex)
            {
                log.Error("BKSigner.SignXML Error", ex);
                return false;
            }

            return true;
        }


        // Sign an XML file. 
        // This document cannot be verified unless the verifying 
        // code has the key with which it was signed.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Doc"></param>
        /// <param name="Key"></param>
        /// <param name="ReferenceUri"></param>
        /// <param name="pMSCert"></param>
        /// <returns></returns>
        public static bool SignXml(XmlDocument Doc, AsymmetricAlgorithm Key, string ReferenceUri, X509Certificate pMSCert) //RSA Key)
        {
            try
            {
                log.Error("BKSigner.SignXML IN...");
                // Check arguments.
                if (Doc == null) return false;
                //throw new ArgumentException("Doc NULL");
                if (Key == null) return false;
                //throw new ArgumentException("Key NULL");

                // Create a SignedXml object.
                SignedXml signedXml = new SignedXml(Doc);

                // Add the key to the SignedXml document.
                signedXml.SigningKey = Key;

                // Create a reference to be signed.
                if (!String.IsNullOrEmpty(ReferenceUri))
                {
                    log.Error("BKSigner.SignXML - ReferenceUri != null => " + ReferenceUri);
                    Reference reference = new Reference();
                    reference.Uri = "#IDRDREF.v1";

                    // Add an enveloped transformation to the reference.
                    XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
                    reference.AddTransform(env);

                    // Add the reference to the SignedXml object.
                    signedXml.AddReference(reference);
                }

                // Add an RSAKeyValue KeyInfo (optional; helps recipient find key to validate).
                KeyInfo keyInfo = new KeyInfo();
                keyInfo.AddClause(new RSAKeyValue((RSA)Key));
                // Load the X509 certificate.
                X509Certificate MSCert = pMSCert; // X509Certificate.CreateFromCertFile(PathCert);

                // Load the certificate into a KeyInfoX509Data object
                // and add it to the KeyInfo object.
                keyInfo.AddClause(new KeyInfoX509Data(MSCert));
                signedXml.KeyInfo = keyInfo;

                // Compute the signature.
                signedXml.ComputeSignature();

                // Get the XML representation of the signature and save
                // it to an XmlElement object.
                XmlElement xmlDigitalSignature = signedXml.GetXml();

                // Append the element to the XML document.
                Doc.DocumentElement.AppendChild(Doc.ImportNode(xmlDigitalSignature, true));
                log.Error("BKSigner.SignXML AppendChild...");
            }
            catch (Exception ex)
            {
                log.Error("BKSigner.SignXML Error", ex);
                return false;
            }
            log.Error("BKSigner.SignXML OUT!");
            return true;
        }

        // Verify the signature of an XML file against an asymmetric 
        // algorithm and return the result.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Doc"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static Boolean VerifyXml(XmlDocument Doc, AsymmetricAlgorithm key) //RSA Key)
        {
            // Check arguments.
            if (Doc == null)
                throw new ArgumentException("Doc");
            //if (Key == null)
            //    throw new ArgumentException("Key");

            // Create a new SignedXml object and pass it
            // the XML document class.
            SignedXml signedXml = new SignedXml(Doc);

            // Find the "Signature" node and create a new
            // XmlNodeList object.
            XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");

            // Throw an exception if no signature was found.
            if (nodeList.Count <= 0)
            {
                throw new CryptographicException("Verification failed: No Signature was found in the document.");
            }

            // This example only supports one signature for
            // the entire XML document.  Throw an exception 
            // if more than one signature was found.
            if (nodeList.Count >= 2)
            {
                throw new CryptographicException("Verification failed: More that one signature was found for the document.");
            }

            // Load the first <signature> node.  
            signedXml.LoadXml((XmlElement)nodeList[0]);

            // Check the signature and return the result.
            return signedXml.CheckSignature();
        }


        //public BKSignerXML() { }

        //public BKSignerXML(Certificate certificado)
        //{
        //    oCertificate = certificado;
        //}

        //public BKSignerXML(string sPathPFXCertificate, string sPFXPassword)
        //{
        //    SetCertificatePFX(sPathPFXCertificate, sPFXPassword);
        //}

        ////Certificado para firmar
        //private Certificate oCertificate;
        //public Certificate Certificate
        //{
        //    get { return oCertificate; }
        //    set { oCertificate = value; }
        //}

        ////Seteo certificado en variable CAPICOM.Certificate. El
        ////certificado esta en formato PKCS12
        //public void SetCertificatePFX(string p_sPathPFXCertificate, string p_sPFXPassword)
        //{
        //    oCertificate = new CertificateClass();
        //    oCertificate.Load(p_sPathPFXCertificate, p_sPFXPassword, CAPICOM_KEY_STORAGE_FLAG.CAPICOM_KEY_STORAGE_EXPORTABLE, CAPICOM_KEY_LOCATION.CAPICOM_CURRENT_USER_KEY);
        //}


        //public XmlDocument GetSignatureXmlDsigEnveloped(XmlDocument p_DOM, string sID,
        //                                string XmlSigFileName,
        //                                RSA Key, string Certificate)
        //{
        //    try
        //    {
        //        // Create a SignedXml object.
        //        SignedXml signedXml = new SignedXml();

        //        PfxOpen oPfxOpen = new PfxOpen();
        //        string psw = "gsuhit";
        //        oPfxOpen.LoadPfx("gsuhit.p12", ref psw);
        //        CspParameters cp = new CspParameters();
        //        cp.KeyContainerName = oPfxOpen.container;
        //        RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(cp);

        //        // Assign the key to the SignedXml object.
        //        //RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        //        RSAParameters RSAKeyInfo = new RSAParameters();
        //        RSAKeyInfo.Modulus = oPfxOpen.keymodulus;
        //        RSAKeyInfo.Exponent = oPfxOpen.keyexponent;
        //        RSACryptoServiceProvider oRSA = new RSACryptoServiceProvider();
        //        oRSA.ImportParameters(RSAKeyInfo);

        //        signedXml.SigningKey = RSA;

        //        // setup the key for signing
        //        //				RSA signingKey = new RSACryptoServiceProvider();
        //        using (StreamWriter writer = new StreamWriter("signing-key.xml"))
        //            writer.WriteLine(RSA.ToXmlString(false));


        //        // Create a data object to hold the data to sign.
        //        DataObject dataObject = new DataObject();
        //        dataObject.Data = p_DOM.ChildNodes;
        //        dataObject.Id = sID.Trim();

        //        // Add the data object to the signature.
        //        signedXml.AddObject(dataObject);

        //        // Create a reference to be able to package everything into the
        //        // message.
        //        Reference reference = new Reference();
        //        // need to add our enveloped transform. And let's sign
        //        // the entire message, as is common
        //        reference.Uri = "#" + sID.Trim();
        //        reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());

        //        // Add it to the message.
        //        signedXml.AddReference(reference);


        //        // Create a new KeyInfo object.
        //        KeyInfo keyInfo = new KeyInfo();

        //        // Load the X509 certificate.
        //        //				X509Certificate MSCert = X509Certificate.CreateFromCertFile(Certificate);
        //        //MSCert = oCertificate.;
        //        // Load the certificate into a KeyInfoX509Data object
        //        // and add it to the KeyInfo object.
        //        keyInfo.AddClause(new KeyInfoX509Data(oPfxOpen.cert));

        //        // Add the KeyInfo object to the SignedXml object.
        //        signedXml.KeyInfo = keyInfo;

        //        signedXml.ComputeSignature();

        //        // Get the XML representation of the signature.
        //        XmlElement xmlSignature = signedXml.GetXml();

        //        // reposition the nodes so that we get an enveloped
        //        // signature
        //        XmlNode node = p_DOM.ImportNode(xmlSignature, true);
        //        XmlNode root = p_DOM.DocumentElement;
        //        root.InsertAfter(node, root.LastChild);

        //        p_DOM.PreserveWhitespace = true;

        //        return p_DOM;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        return null;
        //    }

        //}

        //public XmlDocument GetFirmaXML(XmlDocument p_DOM, string sID)
        //{
        //    // Create the SignedXml message.
        //    SignedXml signedXml = new SignedXml();

        //    //Genero RSA Key desde CAPICOM.PrivateKey	
        //    //			CspParameters csp = new CspParameters();
        //    //			csp.KeyContainerName = Certificate.PrivateKey.ContainerName;
        //    //			csp.ProviderName = Certificate.PrivateKey.ProviderName;
        //    //			csp.ProviderType = Convert.ToInt32(Certificate.PrivateKey.ProviderType);
        //    //			switch (Certificate.PrivateKey.KeySpec)
        //    //			{
        //    //				case CAPICOM_KEY_SPEC.CAPICOM_KEY_SPEC_KEYEXCHANGE:
        //    //					csp.KeyNumber = 1;
        //    //					break;
        //    //				case CAPICOM_KEY_SPEC.CAPICOM_KEY_SPEC_SIGNATURE:
        //    //					csp.KeyNumber = 2;
        //    //					break;
        //    //			}
        //    //			if (Certificate.PrivateKey.IsMachineKeyset())
        //    //				csp.Flags = CspProviderFlags.UseMachineKeyStore;
        //    RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

        //    signedXml.SigningKey = rsa;

        //    // Create a data object to hold the data to sign.
        //    DataObject dataObject = new DataObject();
        //    dataObject.Data = p_DOM.ChildNodes;
        //    dataObject.Id = sID.Trim();

        //    // Add the data object to the signature.
        //    signedXml.AddObject(dataObject);

        //    // Create a reference to be able to package everything into the
        //    // message.
        //    Reference reference = new Reference();
        //    // need to add our enveloped transform. And let's sign
        //    // the entire message, as is common
        //    reference.Uri = "#IDRDREF.v1";
        //    reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());

        //    // Add it to the message.
        //    signedXml.AddReference(reference);

        //    // Add a KeyInfo.
        //    KeyInfo keyInfo = new KeyInfo();
        //    keyInfo.AddClause(new RSAKeyValue(rsa));
        //    signedXml.KeyInfo = keyInfo;


        //    // Compute the signature.
        //    signedXml.ComputeSignature();

        //    // Get the XML representation of the signature.
        //    XmlElement xmlSignature = signedXml.GetXml();

        //    // reposition the nodes so that we get an enveloped
        //    // signature
        //    XmlNode node = p_DOM.ImportNode(xmlSignature, true);
        //    XmlNode root = p_DOM.DocumentElement;
        //    root.InsertAfter(node, root.LastChild);

        //    p_DOM.PreserveWhitespace = true;

        //    return p_DOM;
        //}

        //public bool IsFirmaValidaXML(string p_sPathXML, ref AsymmetricAlgorithm key)
        //{
        //    bool bResult = false;

        //    try
        //    {
        //        XmlDocument xmlDocument = new XmlDocument();
        //        xmlDocument.PreserveWhitespace = true;
        //        xmlDocument.Load(p_sPathXML);

        //        // read in the public key
        //        RSA signingKey = new RSACryptoServiceProvider();
        //        using (StreamReader reader = new StreamReader("signing-key.xml"))
        //            signingKey.FromXmlString(reader.ReadToEnd());

        //        //				MySignedXml signedXml = new MySignedXml(xmlDocument);
        //        SignedXml signedXml = new SignedXml(xmlDocument);

        //        XmlNodeList nodeList = xmlDocument.GetElementsByTagName("Signature");
        //        signedXml.LoadXml((XmlElement)nodeList[0]);

        //        key = null;
        //        if (signedXml.CheckSignature(signingKey))
        //        {
        //            bResult = true;
        //        }
        //    }
        //    catch (Exception exc)
        //    {
        //        //MessageBox.Show(exc.ToString(), Messages.ExceptionTitle, MessageBoxButtons.OK, MessageBoxIcon.Stop);
        //        Console.WriteLine(exc.ToString());
        //    }

        //    return bResult;
        //}

        //public bool IsFirmaValidaXML(XmlDocument p_DOM, ref AsymmetricAlgorithm key)
        //{
        //    bool bResult = false;

        //    try
        //    {
        //        XmlNodeList nodeList = p_DOM.GetElementsByTagName("Signature");

        //        // read in the public key
        //        RSA signingKey = new RSACryptoServiceProvider();
        //        using (StreamReader reader = new StreamReader("signing-key.xml"))
        //            signingKey.FromXmlString(reader.ReadToEnd());


        //        SignedXml signedXml = new SignedXml();
        //        signedXml.LoadXml((XmlElement)nodeList[0]);

        //        key = null;
        //        if (signedXml.CheckSignature(signingKey))
        //        {
        //            bResult = true;
        //        }
        //    }
        //    catch (Exception exc)
        //    {
        //        //MessageBox.Show(exc.ToString(), Messages.ExceptionTitle, MessageBoxButtons.OK, MessageBoxIcon.Stop);
        //        Console.WriteLine(exc.ToString());
        //    }

        //    return bResult;
        //}

    }
}
