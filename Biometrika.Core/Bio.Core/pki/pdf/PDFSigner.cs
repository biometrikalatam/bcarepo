﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Bio.Core.pki.utils;
using log4net;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Org.BouncyCastle.X509;
using SysX509 = System.Security.Cryptography.X509Certificates;
using System.Collections;

namespace Bio.Core.pki.pdf
{
    public class PDFSigner
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PDFSigner));

        public PDFSigner() { }

        /// <summary>
        /// Firma un documento
        /// </summary>
        /// <param name="Source">Documento origen en Base 64</param>
        /// <param name="PFXbase64">Certificado en Base64</param>
        /// <param name="PFXpsw">Clave certificado</param>
        /// <param name="Certificate">Certificado a utilizar</param>
        /// <param name="Reason">Razón de la firma</param>
        /// <param name="Location">Ubicación</param>
        /// <param name="AddVisibleSign">Establece si hay que agregar la firma visible al documento</param>
        /// <param name="out Target">Documento destino en Base64</param>
        //public static bool SignPDF(string SourceB64, SysX509.X509Certificate2 Certificate, string Reason, string Location, bool AddVisibleSign, out string Target)
        public static bool SignPDF(string SourceB64, string PFXbase64, string PFXpsw, string Reason, string Location, bool AddVisibleSign, out string Target)
        {
            bool ret = false;
            Target = null;
            SysX509.X509Certificate2 Certificate;
            try
            {
                Certificate = new SysX509.X509Certificate2();
                Certificate.Import(Convert.FromBase64String(PFXbase64), PFXpsw, SysX509.X509KeyStorageFlags.PersistKeySet);
                //Certificate.Import(@"D:\Biometrika\Desarrollo\biometrika_PKI\E-Cert\GustavoSuhitExportadoN.pfx", "biometrika", X509KeyStorageFlags.PersistKeySet);
               
                //Leo Archvio Origen
                byte[] bySource = Convert.FromBase64String(SourceB64);
                //Creo arraypara archivo destino con espacio para la firma
                byte[] byTarget = new byte[bySource.Length + 10240];

                X509CertificateParser objCP = new X509CertificateParser();
                X509Certificate[] objChain = new X509Certificate[] { objCP.ReadCertificate(Certificate.RawData) };

                PdfReader objReader = new PdfReader(bySource);

                //PdfStamper objStamper = PdfStamper.CreateSignature(objReader, new FileStream(Target, FileMode.Create), '\0');
                PdfStamper objStamper = PdfStamper.CreateSignature(objReader, new MemoryStream(byTarget, true), '\0');
                PdfSignatureAppearance objSA = objStamper.SignatureAppearance;

                if (AddVisibleSign)
                    objSA.SetVisibleSignature(new Rectangle(10, 10, 50, 400), 1, "MiFirma");
                objSA.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                objSA.SignDate = DateTime.Now;
                objSA.SetCrypto(null, objChain, null, null);
                objSA.Reason = Reason;
                objSA.Location = Location;
                objSA.Acro6Layers = true;
                objSA.Render = PdfSignatureAppearance.SignatureRender.NameAndDescription;
                PdfSignature objSignature = new PdfSignature(PdfName.ADOBE_PPKMS, PdfName.ADBE_PKCS7_SHA1);
                objSignature.Date = new PdfDate(objSA.SignDate);
                objSignature.Name = PdfPKCS7.GetSubjectFields(objChain[0]).GetField("CN");
                if (objSA.Reason != null)
                    objSignature.Reason = objSA.Reason;
                if (objSA.Location != null)
                    objSignature.Location = objSA.Location;
                objSA.CryptoDictionary = objSignature;
                int intCSize = 4000;
                Hashtable objTable = new Hashtable();
                objTable[PdfName.CONTENTS] = intCSize * 2 + 2;
                objSA.PreClose(objTable);

                HashAlgorithm objSHA1 = new SHA1CryptoServiceProvider();

                Stream objStream = objSA.RangeStream;
                int intRead = 0;
                byte[] bytBuffer = new byte[8192];
                while ((intRead = objStream.Read(bytBuffer, 0, 8192)) > 0)
                    objSHA1.TransformBlock(bytBuffer, 0, intRead, bytBuffer, 0);
                objSHA1.TransformFinalBlock(bytBuffer, 0, 0);

                byte[] bytPK = SignMsg(objSHA1.Hash, Certificate, false);
                byte[] bytOut = new byte[intCSize];

                PdfDictionary objDict = new PdfDictionary();

                Array.Copy(bytPK, 0, bytOut, 0, bytPK.Length);

                objDict.Put(PdfName.CONTENTS, new PdfString(bytOut).SetHexWriting(true));
                objSA.Close(objDict);

                //byte[] byTarget = File.ReadAllBytes(Target);
                Target = Convert.ToBase64String(byTarget);
                ret = true;
            }
            catch (Exception ex)
            {
                log.Error("PDFSigner Error - " + ex.Message);
                Target = null;
                ret = false;
            }
            return ret;
 
        }

        /// <summary>
        /// Crea la firma CMS/PKCS #7
        /// </summary>
        private static byte[] SignMsg(byte[] Message, SysX509.X509Certificate2 SignerCertificate, bool Detached)
        {
            SignedCms objSignedCms = null; ;
            try
            {
                //Creamos el contenedor
                ContentInfo contentInfo = new ContentInfo(Message);

                //Instanciamos el objeto SignedCms con el contenedor
                objSignedCms = new SignedCms(contentInfo, Detached);

                //Creamos el "firmante"
                CmsSigner objCmsSigner = new CmsSigner(SignerCertificate);

                // Include the following line if the top certificate in the
                // smartcard is not in the trusted list.
                objCmsSigner.IncludeOption = SysX509.X509IncludeOption.EndCertOnly;

                //  Sign the CMS/PKCS #7 message. The second argument is
                //  needed to ask for the pin.
                objSignedCms.ComputeSignature(objCmsSigner, false);

               
            }
            catch (Exception ex)
            {
                log.Error("PDFSigner.SigMsg Error - " + ex.Message);
                return null;
            }
            //Encodeamos el mensaje CMS/PKCS #7
            return objSignedCms.Encode();
           
        }

 
        /*
        public static void SignPdf(string fileName, string certThumbprint)
        {
            ////Get certificate
            ////Open the currently logged-in user certificate store
            //var store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            //store.Open(OpenFlags.ReadOnly);

            ////Select a certificate from the certificate store
            //var certs = store.Certificates.Find(X509FindType.FindByThumbprint, GetCertThumbprint(certThumbprint), true);
            //store.Close();

            ////Verify that a certificate exists 
            //if (certs.Count == 0)
            //{
            //    MessageBox.Show("No Current user certificate store!", "Sign PDF", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}

            ////Open Pdf document
            //byte[] pdfData = File.ReadAllBytes(fileName);

            ////Sign the PDF document
            //byte[] signedData = SignDocument(pdfData, certs[0]);

            //File.WriteAllBytes(fileName, signedData);
        }
        
        private static string GetCertThumbprint(string certThumbprint)
        {
            string thumbprint = certThumbprint.Replace(" ", "").ToUpperInvariant();
            if (thumbprint[0] == 8206)
            {
                thumbprint = thumbprint.Substring(1);
            }

            return thumbprint;
        }

        private static byte[] SignDocument(byte[] pdfData, X509Certificate2 cert)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                var reader = new PdfReader(pdfData);
                var stp = PdfStamper.CreateSignature(reader, stream, '\0');
                var sap = stp.SignatureAppearance;

                //Protect certain features of the document 
                stp.SetEncryption(null,
                    Guid.NewGuid().ToByteArray(), //random password 
                    PdfWriter.ALLOW_PRINTING | PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_SCREENREADERS,
                    PdfWriter.ENCRYPTION_AES_256);

                //Get certificate chain
                var cp = new Org.BouncyCastle.X509.X509CertificateParser();
                var certChain = new Org.BouncyCastle.X509.X509Certificate[] { cp.ReadCertificate(cert.RawData) };

                //sap.SetCrypto(null, certChain, null, PdfSignatureAppearance.WINCER_SIGNED);

                //Set signature appearance
                BaseFont helvetica = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
                Font font = new Font(helvetica, 12, iTextSharp.text.Font.NORMAL);
                sap.Layer2Font = font;
                sap.SetVisibleSignature(new iTextSharp.text.Rectangle(415, 100, 585, 40), 1, null);

                var dic = new PdfSignature(PdfName.ADOBE_PPKMS, PdfName.ADBE_PKCS7_SHA1);
                //Set some stuff in the signature dictionary.
                dic.Date = new PdfDate(sap.SignDate);
                dic.Name = cert.Subject;    //Certificate name 
                if (sap.Reason != null)
                {
                    dic.Reason = sap.Reason;
                }
                if (sap.Location != null)
                {
                    dic.Location = sap.Location;
                }

                //Set the crypto dictionary 
                sap.CryptoDictionary = dic;

                //Set the size of the certificates and signature. 
                int csize = 4096; //Size of the signature - 4K

                //Reserve some space for certs and signatures
                var reservedSpace = new Dictionary<PdfName, int>();
                reservedSpace[PdfName.CONTENTS] = csize * 2 + 2; //*2 because binary data is stored as hex strings. +2 for end of field
                sap.PreClose(reservedSpace);    //Actually reserve it 

                //Build the signature 
                HashAlgorithm sha = new SHA1CryptoServiceProvider();

                //var sapStream = sap.RangeStream;
                //int read = 0;
                //byte[] buff = new byte[8192];
                //while ((read = sapStream.Read(buff, 0, 8192)) > 0)
                //{
                //    sha.TransformBlock(buff, 0, read, buff, 0);
                //}
                //sha.TransformFinalBlock(buff, 0, 0);

                byte[] pk = SignMsg(sha.Hash, cert, false);

                //Put the certs and signature into the reserved buffer 
                byte[] outc = new byte[csize];
                Array.Copy(pk, 0, outc, 0, pk.Length);

                //Put the reserved buffer into the reserved space 
                PdfDictionary certificateDictionary = new PdfDictionary();
                certificateDictionary.Put(PdfName.CONTENTS, new PdfString(outc).SetHexWriting(true));

                //Write the signature 
                sap.Close(certificateDictionary);
                //Close the stamper and save it 
                stp.Close();

                reader.Close();

                //Return the saved pdf 
                return stream.GetBuffer();
            }
        }

        private static byte[] SignMsg(Byte[] msg, X509Certificate2 cert, bool detached)
        {
            //Place message in a ContentInfo object. This is required to build a SignedCms object. 
            ContentInfo contentInfo = new ContentInfo(msg);

            //Instantiate SignedCms object with the ContentInfo above. 
            //Has default SubjectIdentifierType IssuerAndSerialNumber. 
            SignedCms signedCms = new SignedCms(contentInfo, detached);

            //Formulate a CmsSigner object for the signer. 
            CmsSigner cmsSigner = new CmsSigner(cert);  //First cert in the chain is the signer cert

            //Do the whole certificate chain. This way intermediate certificates get sent across as well.
            cmsSigner.IncludeOption = X509IncludeOption.ExcludeRoot;

            //Sign the CMS/PKCS #7 message. The second argument is needed to ask for the pin. 
            signedCms.ComputeSignature(cmsSigner, false);

            //Encode the CMS/PKCS #7 message. 
            return signedCms.Encode();
        }
        */

        /*

        // Sign an XML file. 
        // This document cannot be verified unless the verifying 
        // code has the key with which it was signed.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Doc"></param>
        /// <param name="Key"></param>
        /// <param name="ReferenceUri"></param>
        /// <param name="PathCert"></param>
        /// <returns></returns>
        public static bool PDFSign(string Doc, AsymmetricAlgorithm Key, string  ReferenceUri, string PathCert) //RSA Key)
        {
            try
            {
                // Check arguments.
                if (Doc == null) return false;
                //throw new ArgumentException("Doc NULL");
                if (Key == null) return false;
                //throw new ArgumentException("Key NULL");

                // Create a SignedXml object.
                SignedXml signedXml = new SignedXml(Doc);

                // Add the key to the SignedXml document.
                signedXml.SigningKey = Key;

                // Create a reference to be signed.
                if (!String.IsNullOrEmpty(ReferenceUri))
                {
                    Reference reference = new Reference();
                    reference.Uri = "#IDRDREF.v1";
                
                    // Add an enveloped transformation to the reference.
                    XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
                    reference.AddTransform(env);

                    // Add the reference to the SignedXml object.
                    signedXml.AddReference(reference);
                }

                // Add an RSAKeyValue KeyInfo (optional; helps recipient find key to validate).
                KeyInfo keyInfo = new KeyInfo();
                keyInfo.AddClause(new RSAKeyValue((RSA)Key));
                // Load the X509 certificate.
                X509Certificate MSCert = X509Certificate.CreateFromCertFile(PathCert);

                // Load the certificate into a KeyInfoX509Data object
                // and add it to the KeyInfo object.
                keyInfo.AddClause(new KeyInfoX509Data(MSCert));
                signedXml.KeyInfo = keyInfo;

                // Compute the signature.
                signedXml.ComputeSignature();

                // Get the XML representation of the signature and save
                // it to an XmlElement object.
                XmlElement xmlDigitalSignature = signedXml.GetXml();

                // Append the element to the XML document.
                Doc.DocumentElement.AppendChild(Doc.ImportNode(xmlDigitalSignature, true));

            }
            catch (Exception ex)
            {
                log.Error("BKSigner.SignXML Error", ex);
                return false;
            }

            return true;
        }

        // Verify the signature of an XML file against an asymmetric 
        // algorithm and return the result.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Doc"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static Boolean PDFVerifySign(string Doc, AsymmetricAlgorithm key) //RSA Key)
        {
            //// Check arguments.
            //if (Doc == null)
            //    throw new ArgumentException("Doc");
            ////if (Key == null)
            ////    throw new ArgumentException("Key");

            //// Create a new SignedXml object and pass it
            //// the XML document class.
            //SignedXml signedXml = new SignedXml(Doc);

            //// Find the "Signature" node and create a new
            //// XmlNodeList object.
            //XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");

            //// Throw an exception if no signature was found.
            //if (nodeList.Count <= 0)
            //{
            //    throw new CryptographicException("Verification failed: No Signature was found in the document.");
            //}

            //// This example only supports one signature for
            //// the entire XML document.  Throw an exception 
            //// if more than one signature was found.
            //if (nodeList.Count >= 2)
            //{
            //    throw new CryptographicException("Verification failed: More that one signature was found for the document.");
            //}

            //// Load the first <signature> node.  
            //signedXml.LoadXml((XmlElement)nodeList[0]);

            //// Check the signature and return the result.
            //return signedXml.CheckSignature();
            return true;
        }
        */

        /*
         
 /// <summary>
 /// Signs a PDF document using iTextSharp library
 /// </summary>
 /// <param name="sourceDocument">The path of the source pdf document which is to be signed</param>
 /// <param name="destinationPath">The path at which the signed pdf document should be generated</param>
 /// <param name="privateKeyStream">A Stream containing the private/public key in .pfx format which would be used to sign the document</param>
 /// <param name="keyPassword">The password for the private key</param>
 /// <param name="reason">String describing the reason for signing, would be embedded as part of the signature</param>
 /// <param name="location">Location where the document was signed, would be embedded as part of the signature</param>    
 public static void signPdfFile(string sourceDocument, string destinationPath, Stream privateKeyStream, string keyPassword, string reason, string location)
 {
     Pkcs12Store pk12 = new Pkcs12Store(privateKeyStream, keyPassword.ToCharArray());
     privateKeyStream.Dispose();

     //then Iterate throught certificate entries to find the private key entry
     string alias = null;
     foreach (string tAlias in pk12.Aliases)
     {
         if (pk12.IsKeyEntry(tAlias))
         {
             alias = tAlias;
             break;
         }
     }
     var pk = pk12.GetKey(alias).Key;

     // reader and stamper
     PdfReader reader = new PdfReader(sourceDocument);
     using (FileStream fout = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
     {
         using (PdfStamper stamper = PdfStamper.CreateSignature(reader, fout, '\0'))
         {
             // appearance
             PdfSignatureAppearance appearance = stamper.SignatureAppearance;
             //appearance.Image = new iTextSharp.text.pdf.PdfImage();
             appearance.Reason = reason;
             appearance.Location = location;
             appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(20, 10, 170, 60), 1, "Icsi-Vendor");
             // digital signature
             IExternalSignature es = new PrivateKeySignature(pk, "SHA-256");
             MakeSignature.SignDetached(appearance, es, new X509Certificate[] { pk12.GetCertificate(alias).Certificate }, null, null, null, 0, CryptoStandard.CMS);

             stamper.Close();
         }
     }
 }


 /// <summary>
 /// Verifies the signature of a prevously signed PDF document using the specified public key
 /// </summary>
 /// <param name="pdfFile">a Previously signed pdf document</param>
 /// <param name="publicKeyStream">Public key to be used to verify the signature in .cer format</param>
 /// <exception cref="System.InvalidOperationException">Throw System.InvalidOperationException if the document is not signed or the signature could not be verified</exception>
 public static void verifyPdfSignature(string pdfFile, Stream publicKeyStream)
 {
     var parser = new X509CertificateParser();
     var certificate = parser.ReadCertificate(publicKeyStream);
     publicKeyStream.Dispose();

     PdfReader reader = new PdfReader(pdfFile);
     AcroFields af = reader.AcroFields;
     var names = af.GetSignatureNames();

     if (names.Count == 0)
     {
         throw new InvalidOperationException("No Signature present in pdf file.");
     }

     foreach (string name in names)
     {
         if (!af.SignatureCoversWholeDocument(name))
         {
             throw new InvalidOperationException(string.Format("The signature: {0} does not covers the whole document.", name));
         }

         PdfPKCS7 pk = af.VerifySignature(name);
         var cal = pk.SignDate;
         var pkc = pk.Certificates;

         if (!pk.Verify())
         {
             throw new InvalidOperationException("The signature could not be verified.");
         }
         if (!pk.VerifyTimestampImprint())
         {
             throw new InvalidOperationException("The signature timestamp could not be verified.");
         }

         IList<VerificationException>[] fails = CertificateVerification.VerifyCertificates(pkc, new X509Certificate[] { certificate }, null, cal);
         if (fails != null)
         {
             throw new InvalidOperationException("The file is not signed using the specified key-pair.");
         }
     }
 }


 /*
             /// <summary>
        /// Firma un documento
        /// </summary>
        /// <param name="Source">Documento origen</param>
        /// <param name="Target">Documento destino</param>
        /// <param name="Certificate">Certificado a utilizar</param>
        /// <param name="Reason">Razón de la firma</param>
        /// <param name="Location">Ubicación</param>
        /// <param name="AddVisibleSign">Establece si hay que agregar la firma visible al documento</param>
        public static void SignHashed(string Source, string Target, SysX509.X509Certificate2 Certificate, string Reason, string Location, bool AddVisibleSign)
        {
         X509CertificateParser objCP = new X509CertificateParser();
         X509Certificate[] objChain = new X509Certificate[] { objCP.ReadCertificate(Certificate.RawData) };
         PdfReader objReader = new PdfReader(Source);

         PdfStamper objStamper = PdfStamper.CreateSignature(objReader, new FileStream(Target, FileMode.Create), '\0');
         PdfSignatureAppearance objSA = objStamper.SignatureAppearance;
         if (AddVisibleSign)
             objSA.SetVisibleSignature(new Rectangle(100, 100, 300, 200), 1, null);
         objSA.SignDate = DateTime.Now;
         objSA.SetCrypto(null, objChain, null, null);
         objSA.Reason = Reason;
         objSA.Location = Location;
         objSA.Acro6Layers = true;
         objSA.Render = PdfSignatureAppearance.SignatureRender.NameAndDescription;
         PdfSignature objSignature = new PdfSignature(PdfName.ADOBE_PPKMS, PdfName.ADBE_PKCS7_SHA1);
         objSignature.Date = new PdfDate(objSA.SignDate);
         objSignature.Name = PdfPKCS7.GetSubjectFields(objChain[0]).GetField("CN");
         if (objSA.Reason != null)
             objSignature.Reason = objSA.Reason;
         if (objSA.Location != null)
             objSignature.Location = objSA.Location;
         objSA.CryptoDictionary = objSignature;
         int intCSize = 4000;
         Hashtable objTable = new Hashtable();
         objTable[PdfName.CONTENTS] = intCSize * 2 + 2;
         objSA.PreClose(objTable);
         HashAlgorithm objSHA1 = new SHA1CryptoServiceProvider();
         Stream objStream = objSA.RangeStream;
         int intRead = 0;
         byte[] bytBuffer = new byte[8192];
         while ((intRead = objStream.Read(bytBuffer, 0, 8192)) > 0)
             objSHA1.TransformBlock(bytBuffer, 0, intRead, bytBuffer, 0);
         objSHA1.TransformFinalBlock(bytBuffer, 0, 0);
         byte[] bytPK = SignMsg(objSHA1.Hash, Certificate, false);
         byte[] bytOut = new byte[intCSize];
         PdfDictionary objDict = new PdfDictionary();
         Array.Copy(bytPK, 0, bytOut, 0, bytPK.Length);
         objDict.Put(PdfName.CONTENTS, new PdfString(bytOut).SetHexWriting(true));
         objSA.Close(objDict);
        }

        /// <summary>
        /// Crea la firma CMS/PKCS #7
        /// </summary>
        private static byte[] SignMsg(byte[] Message, SysX509.X509Certificate2 SignerCertificate, bool Detached)
        {
         //Creamos el contenedor
         ContentInfo contentInfo = new ContentInfo(Message);
         //Instanciamos el objeto SignedCms con el contenedor
         SignedCms objSignedCms = new SignedCms(contentInfo, Detached);
         //Creamos el "firmante"
         CmsSigner objCmsSigner = new CmsSigner(SignerCertificate);
         // Include the following line if the top certificate in the
         // smartcard is not in the trusted list.
         objCmsSigner.IncludeOption = SysX509.X509IncludeOption.EndCertOnly;
         //  Sign the CMS/PKCS #7 message. The second argument is
         //  needed to ask for the pin.
         objSignedCms.ComputeSignature(objCmsSigner, false);
         //Encodeamos el mensaje CMS/PKCS #7
         return objSignedCms.Encode();
        }
        }
 */

    }
}
