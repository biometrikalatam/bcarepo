using System;
using System.Security.Cryptography;
using Org.Mentalis.Security.Certificates;
using log4net;

namespace Bio.Core.pki.mentalis
{
	public class Pkcs12 {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(Pkcs12));


		#region Constructores		
    		public Pkcs12() { }

    		public Pkcs12(string path, string psw) {
    			this.certificatestore = Pkcs12.OpenStorePFX(path,psw);
    			this.certificate = Pkcs12.OpenPFX(path,psw);
    		}
    		
    		#endregion Constructores		
    	
    	#region Public Methods		
    		public static CertificateStore OpenStorePFX(string path, string psw) {
    			try {
    				return CertificateStore.CreateFromPfxFile(path.Trim(),psw.Trim());
    			} catch (Exception ex) {
                    LOG.Error("Pkcs12.OpenStorePFX", ex);
    			}
    			return null;
    		}

    		public static Certificate OpenPFX(string path, string psw) {
    			Certificate cRet = null;
    			try {
    				Certificate[] cCol = 
    					(CertificateStore.CreateFromPfxFile(path,psw)).EnumCertificates();

    				for (int i=0; i<cCol.Length; i++) {
    					if (cCol[i].HasPrivateKey()) {
    						cRet = cCol[i];
    					}
    				}
    			} catch (Exception ex) {
                    LOG.Error("Pkcs12.OpenPFX", ex);
    			}
    			return cRet;
    		}

    		public static AsymmetricAlgorithm GetPrivateKeyFromPFX(string path, string psw) {
    			AsymmetricAlgorithm aRet = null;
    			try {
                    LOG.Debug("Pkcs12.GetPrivateKeyFromPFX Entrando...");
    				Certificate[] cCol = 
    					(CertificateStore.CreateFromPfxFile(path,psw)).EnumCertificates();
                    LOG.Debug("Pkcs12.GetPrivateKeyFromPFX Certificados[].Length = " + ((cCol != null)?cCol.Length.ToString():"null"));
    				Certificate cAux = null;
    				for (int i=0; i<cCol.Length; i++) {
                        LOG.Debug("Pkcs12.GetPrivateKeyFromPFX  ----> Certificados[i]=" + i + " - " + cCol[i].GetName());
                        LOG.Debug("Pkcs12.GetPrivateKeyFromPFX  ----> HasPrivateKey[i]=" + cCol[i].HasPrivateKey());
                        if (cCol[i].HasPrivateKey())
                        {
    						cAux = cCol[i];
    					}
    				}
    				aRet = cAux.PrivateKey; 
    			} catch (Exception ex) {
                    LOG.Error("Pkcs12.GetPrivateKeyFromPFX", ex);
    			}
    			return aRet;
    		}

    		public AsymmetricAlgorithm GetPrivateKeyFromPFX() {
    			AsymmetricAlgorithm aRet = null;
    			try {
    				Certificate[] cCol = this.certificatestore.EnumCertificates();

    				Certificate cAux = null;
    				for (int i=0; i<cCol.Length; i++) {
    					if (cCol[i].HasPrivateKey()) {
    						cAux = cCol[i];
    					}
    				}
    				aRet = cAux.PrivateKey; 
    			} catch (Exception ex) {
                    LOG.Error("Pkcs12.GetPrivateKeyFromPFX", ex);
    			}
    			return aRet;
    		}

    		public static AsymmetricAlgorithm GetPublicKeyFromPFX(string path, string psw) {
    			AsymmetricAlgorithm aRet = null;
    			try {
    				Certificate[] cCol = 
    					(CertificateStore.CreateFromPfxFile(path,psw)).EnumCertificates();

    				Certificate cAux = null;
    				for (int i=0; i<cCol.Length; i++) {
    					if (cCol[i].HasPrivateKey()) {
    						cAux = cCol[i];
    					}
    				}
    				aRet = cAux.PublicKey; 
    			} catch (Exception ex) {
                    LOG.Error("Pkcs12.GetPublicKeyFromPFX", ex);
    			}
    			return aRet;
    		}

    		public AsymmetricAlgorithm GetPublicKeyFromPFX() {
    			AsymmetricAlgorithm aRet = null;
    			try {
    				Certificate[] cCol = this.certificatestore.EnumCertificates();

    				Certificate cAux = null;
    				for (int i=0; i<cCol.Length; i++) {
    					if (cCol[i].HasPrivateKey()) {
    						cAux = cCol[i];
    					}
    				}
    				aRet = cAux.PublicKey; 
    			} catch (Exception ex) {
                    LOG.Error("Pkcs12.GetPublicKeyFromPFX", ex);
    			}
    			return aRet;
    		}

    		public Certificate GetCertificate() {
    			Certificate cRet = null;
    			try {
    				Certificate[] cCol = this.CertificateStore.EnumCertificates();

    				for (int i=0; i<cCol.Length; i++) {
    					if (cCol[i].HasPrivateKey()) {
    						cRet = cCol[i];
    					}
    				}
    			} catch (Exception ex) {
                    LOG.Error("Pkcs12.GetCertificate", ex);
    			}
    			return cRet;
    		}

		#endregion Public Methods		

		#region Public Properties
    		public CertificateStore CertificateStore {
    			get { return certificatestore; }
    			set { certificatestore = value; }
    		}
    		public Certificate Certificate {
    			get { return certificate; }
    			set { certificate = value; }
    		}
		#endregion Public Properties

		#region Private
    		CertificateStore certificatestore;
    		Certificate certificate;
		#endregion Private
	}
}
