// ==++==
// 
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class: System.IO.Ports.SerialEventArgs : EventArgs
**
** Purpose: SerialEventArgs class defined.
**
** Date:  August 2002
**
===========================================================*/

using System;


namespace System.IO.Ports
{
	public class SerialEventArgs : EventArgs 
	{
		
		internal SerialEvents eventType;

		public SerialEvents EventType
		{
			get 
			{
				return eventType;
			}
			set 
			{
				eventType = value;
			}
		}

		public SerialEventArgs(SerialEvents eventCode) 
		{
			this.EventType = eventCode;
		}
	}

}
