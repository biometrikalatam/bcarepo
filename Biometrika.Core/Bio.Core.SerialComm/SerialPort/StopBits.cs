// ==++==
// 
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Type: System.IO.Ports.StopBits
**
** Purpose: StopBits enum type defined
**
** Date:  August 2002
**
===========================================================*/


namespace System.IO.Ports
{

	
	public enum StopBits 
	{
		
		One = 1,
		Two = 2,
		OnePointFive = 3
		
	};	
}