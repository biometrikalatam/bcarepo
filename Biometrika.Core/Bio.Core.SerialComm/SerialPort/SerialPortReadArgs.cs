using System;

namespace BioCore.SerialComm.SerialPort
{
	/// <summary>
	/// Summary description for SerialPortReadArgs.
	/// </summary>
	public class SerialPortReadArgs : IArguments
	{
		public int timeout;

		public SerialPortReadArgs(int timeout)
		{
			this.timeout = timeout;
		}

		public int Timeout
		{
			get
			{
				return timeout;
			}
			set
			{
				timeout = value;
			}
		}

	}
}
