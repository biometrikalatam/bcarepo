using System;

namespace BioCore.SerialComm
{
	/// <summary>
	/// Summary description for IQuality.
	/// </summary>
	public interface IQuality
	{
		int CompareWith(IQuality quality);
	}
}
