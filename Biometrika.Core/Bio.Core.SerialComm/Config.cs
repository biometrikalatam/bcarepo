﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using BioCore.SerialComm.SerialPort;

namespace Bio.Core.SerialComm
{
    public class Config
    {

#region Private

#endregion Private


#region Public

        [XmlArrayItem("SerialPort", Type = typeof(SerialPortConfig))]
        public ArrayList Devices;

#endregion Public
    }
}
