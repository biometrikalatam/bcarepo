using System;

namespace BioCore.SerialComm
{
	/// <summary>
	/// Summary description for BioControlsException.
	/// </summary>
	public class BioControlsException : Exception
	{
		public BioControlsException()
		{
		}

		public BioControlsException(ErrorCode code) : base(code.ToString ())
		{
			
		}
	}
}
