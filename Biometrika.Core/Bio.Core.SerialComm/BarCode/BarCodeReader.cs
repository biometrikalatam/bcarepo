using System;
using System.IO;
using System.IO.Ports;
using BioCore.SerialComm.Feedback;
using BioCore.SerialComm.SerialPort;
using log4net;
using System.Threading;

namespace BioCore.SerialComm.BarCode
{
	/// <summary>
	/// Summary description for Pdf417.
	/// </summary>
	public class BarCodeReader : ISensor
	{

        private static readonly ILog LOG = LogManager.GetLogger(typeof(BarCodeReader));

		public string name;
		

		public BarCodeReader(string name, IConfig config, IFeedbackProvider feedback)
		{
			this.feedback = feedback;
			this.name = name;
			Configure(config);
		}




		public string Name
		{
			get { return name; }
		}

		public Type Driver
		{
			get { return typeof(Driver); }
		}


		public IResult Execute (ICommand cmd, IArguments args)
		{
			throw new NotImplementedException ();
		}

		SerialPortConfig scfg = null;
		

		public void Configure (IConfig config)
		{
			scfg = config as SerialPortConfig;
			
			
		}

	
		public void Open ()
		{
			try 
			{
                
                LOG.Info("Antes de abrir el puerto: Name=" + scfg.Name + " => Port =" + scfg.Port.ToString() +
                            "|Bauds=" + scfg.Bauds.ToString() + "|Parity=" + scfg.Parity.ToString() + 
                            "|Bits=" + scfg.Bits.ToString() + "|StopBits=" + scfg.StopBits.ToString());
				serialPort = new System.IO.Ports.SerialPort(scfg.Port,scfg.Bauds, scfg.Parity, scfg.Bits, scfg.StopBits);
                if (!serialPort.IsOpen)
                {
                    LOG.Info("El puerto no esta abierto");
                    Thread.Sleep(100);
                    LOG.Info("Se espera un milisegundo");
                    serialPort.Open();
                }
				serialPort.EventFilter = SerialEvents.All;
				serialPort.ReceivedEvent += new SerialEventHandler(serialPort_ReceivedEvent);
                LOG.Info("El puerto se abio con �xito!");
            }
            catch (Exception ex)
			{
				LOG.Error("BarCodeReader.Open",ex);
				feedback.ErrorMessage (this, "Error al abrir lector de codigo de barras! ["+ex.Message+"]" );
			}
		}



		public void Close ()
		{
			if (serialPort.IsOpen)
				serialPort.Close();
		}

		public IStatus Status ()
		{
			throw new NotImplementedException ();
		}


		public IFeedbackProvider FeedbackProvider
		{
			get 
			{
				if (feedback == null)
					feedback = new NullFeedbackProvider();
				return feedback;
			}
			set 
			{ 
				feedback = value;
			}
		}
		IFeedbackProvider feedback;

	

		public string Id
		{
			get { return String.Empty; }
		}

		public void SetActive()
		{
			//No hace nada, solo para que se implemente y no falle en otro lado
		}

		public SampleAvailableEvent OnSampleAvailableEventAvailable
		{
			get { return onSampleAvailableEvent; }
			set { onSampleAvailableEvent = value; }
		}

		public void DoEvents ()
		{
			
			
		}

		public System.IO.Ports.SerialPort serialPort;
		SampleAvailableEvent onSampleAvailableEvent;

		public ISample GetSample (IArguments arguments)
		{
            
			
			try 
			{
                LOG.Debug("Obteniendo la muestra");
                if (serialPort == null || !serialPort.IsOpen)
                    return null;

                LOG.Debug("TimeOut:" + (arguments as BarCodeArguments).Timeout);
                serialPort.ReadTimeout = (arguments as BarCodeArguments).Timeout;
			
					byte [] byteBuffer = new byte[1000];                  

				int numJustRead;
				int nread = 0;
				using (MemoryStream ms = new MemoryStream())
				{
					while ((numJustRead = serialPort.Read(byteBuffer, 0, 1000)) != 0)
					{
						ms.Write(byteBuffer, 0, numJustRead);
						nread += numJustRead;
					}
					ms.Close();
					return new SerialSample(this, ms.ToArray ());
				}
			}		
			catch (Exception ex)
			{
				FeedbackProvider.AddMessage (this, ex.Message);
				LOG.Error("BarCodeReader.GetSample",ex);				
			}
			return null;
		}

		private void serialPort_ReceivedEvent(object source, SerialEventArgs e)
		{
            try
            {
                if (onSampleAvailableEvent != null)
                    onSampleAvailableEvent(this, new SerialSample(this, null));
            } 
            catch (Exception ex)
            {
                LOG.Error("BarCodeReader.serialPort_ReceivedEvent", ex);
            }
		}
	}
}
