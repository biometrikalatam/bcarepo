using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
//using BioCore.SerialComm.Fingerprints;
using log4net;

namespace BioCore.SerialComm.Cedula
{
	/// <summary>
	/// Summary description for CedulaTemplate.
	/// </summary>
	public class CedulaTemplate //: FingerTemplate
	{
        private static readonly ILog LOG = LogManager.GetLogger(typeof(CedulaTemplate));

		public CedulaTemplate(byte[] pdf) : base()
		{
			isValid = false;
			try
			{
                //if (pdf[0] == 80)
                //{
                //    byte[] pdf1 = new byte[pdf.Length - 1];
                //    for (int i = 1; i < pdf.Length; i++)
                //    {
                //        pdf1[i - 1] = pdf[i];
                //    }
                //    pdf = null;
                //    pdf = pdf1;
                //}

                String type = "";
                type = Encoding.UTF7.GetString(pdf, 0, 1);
               
                if (type != "h")
                {
                    rut = Encoding.UTF7.GetString(pdf, 0, 9).Replace('\0', ' ').TrimEnd();
                    if (rut.StartsWith("0"))
                    {
                        rut = rut.Substring(1);
                    }
                    apellido = Encoding.UTF7.GetString(pdf, 19, 30).Replace('\0', ' ').TrimEnd();
                    pais = Encoding.UTF7.GetString(pdf, 49, 3).Replace('\0', ' ').TrimEnd();
                    numeroDeSerie = Encoding.UTF7.GetString(pdf, 58, 10).Replace('\0', ' ').TrimEnd();

                    string aa = Encoding.UTF7.GetString(pdf, 52, 2);
                    string mm = Encoding.UTF7.GetString(pdf, 54, 2);
                    string dd = Encoding.UTF7.GetString(pdf, 56, 2);
                    expiracion = new DateTime(Convert.ToInt32(aa) + 2000, Convert.ToInt32(mm), Convert.ToInt32(dd));

                    FingerId = (pdf[73] << 24) + (pdf[72] << 16) + (pdf[71] << 8) + (pdf[70]);
                    int pcLen = (pdf[77] << 24) + (pdf[76] << 16) + (pdf[75] << 8) + (pdf[74]);
                    byte[] pc1 = new Byte[400];
                    Buffer.BlockCopy(pdf, 78, pc1, 0, pcLen);
                    SetMinutiae(pc1, pcLen);
                    isValid = true;
                }
                else if (type == "h")
                {
                    rut = Encoding.UTF7.GetString(pdf).Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[0] + Encoding.UTF7.GetString(pdf).Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[1]; //Encoding.UTF7.GetString(pdf).Split('?')[1].Split('&')[0].Split('=')[1];
                    isValid = true;
                }
            }
			catch (Exception ex)
			{
				isValid = false;
				LOG.Error("CedulaTemplate.CedulaTemplate",ex);
			}
		}

		private void SetMinutiae (byte[] pc1, int len)
		{
			minu = pc1;
			minuLen = len;
		}

	    private int _fingerid;
	    public int FingerId
	    {
            get { return _fingerid; }
            set { _fingerid = value; }
	    }

		public string Rut 
		{
			get { return rut; }
			set { rut = value; }
		}
		private string rut;

		public string Apellido
		{
			get { return apellido; }
			set { apellido = value; }
		}
		private string apellido;

		public string Pais
		{
			get { return pais; }
			set { pais = value; }
		}
		private string pais;


		public string NumeroDeSerie
		{
			get { return numeroDeSerie; }
			set { numeroDeSerie = value; }
		}
		private string numeroDeSerie;

		
		public DateTime FechaExpiracion
		{
			get { return expiracion; }
			set { expiracion = value; }
		}
		private DateTime expiracion;
		private byte[] minu;
		private int minuLen;
		private bool isValid;


		public bool IsValid
		{
			get
			{
				return isValid;
			}
		}

		public int Quality
		{
			get { return isValid ? minu.Length : 0; }
		}

		/// <summary>
		/// Escribe a un stream la muestra
		/// </summary>
		/// <param name="stream">stream de salida</param>
		public void Serialize (StreamWriter stream)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(CedulaTemplate));
			serializer.Serialize(stream, this);
		}

		/// <summary>
		/// Lee desde un stream la muestra
		/// </summary>
		/// <param name="stream">stream de entrada</param>
		public void Deserialize (StreamReader stream)
		{
			
		}

		public Array Data
		{
			get { return minu; }
		}

		public int DataLen
		{
			get {  return minuLen; }
		}

        public string ToXML()
        {
            return "<Cedula>" +
                        "<Rut>" + Rut +"</Rut>" +
                        "<Apellido>" + Apellido +"</Apellido>" +
                        "<Pais>" + Pais +"</Pais>" +
                        "<NumeroDeSerie>" + NumeroDeSerie +"</NumeroDeSerie>" +
                        "<Expiracion>" + FechaExpiracion.ToString("dd/MM/yyyy") +"</Expiracion>" +
                        "<Finger>" + FingerId.ToString() +"</Finger>" +
                        "<BiometricData>" + Convert.ToBase64String(minu) +"</BiometricData>" +
                        "<LenBiometricData>" + minuLen.ToString() +"</LenBiometricData>" +
                   "</Cedula>";
        }

	}
}
