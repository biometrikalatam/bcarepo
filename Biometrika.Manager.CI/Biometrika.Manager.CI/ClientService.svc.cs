﻿using BCR.Bio.Domain.BCI;
using BCR.System.Log;
using BCR.System.Validation;
using System;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

namespace BCI
{
    /// <summary>
    ///
    /// </summary>
    public class ClientService : IClientService
    {
        public string DeployedAuditSite { get { return ConfigurationManager.AppSettings.Get("DeployedAuditSite"); } }

        public string UserPeticion { get { return ConfigurationManager.AppSettings.Get("UserPeticion"); } }

        public string PassPeticion { get { return ConfigurationManager.AppSettings.Get("PassPeticion"); } }

        public bool DebugXMLContent { get { return bool.Parse(ConfigurationManager.AppSettings.Get("DebugXMLContent")); } }

        static ClientService()
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="XMLclient"></param>
        /// <returns></returns>
        public string GetCCX(string XMLclient)
        {
            Log4Bio.Info("Peticion a GetCCX ");
            ValidationDictionary validationDictionary = new ValidationDictionary();

            // parse client request
            Transaction transaction = BCIWebService.ParseClient(XMLclient);

            if (DebugXMLContent)
                BCIWebService.PersistContent(XMLclient, "Terminal3en1", " WS->GetCCX", transaction.CodigoAutorizacion);

            if (transaction == null)
            {
                validationDictionary.AddError("86.1", "Error interno");
                Log4Bio.Info("No se generó la transaction esperada");

                string msjSalida = ResponseTerminal(validationDictionary);

                if (DebugXMLContent)
                    BCIWebService.PersistContent(msjSalida, "Terminal3en1", " WS->GetCCX", "");

                return msjSalida;
            }

            // validaciones
            if (BCIWebService.IsValidateRequest(transaction, validationDictionary))
            {
                transaction.UrlDocument = string.Concat(DeployedAuditSite, "/audit/", transaction.CodigoAutorizacion);

                TransactionDocument transactionDocument = CreateDocumentSignedWithTSA(transaction);

                // persistir
                int id = BCIWebService.Save(transaction, transactionDocument);

                // enroll
                BCIWebService.BioPortalEnroll(transaction);

                // Peticionar
                string xmlResponse = BCIWebService.GenerateProviderRequest(transaction, id, UserPeticion, PassPeticion);

                if (DebugXMLContent)
                    BCIWebService.PersistContent(xmlResponse, " WS->GetCCX", "Terminal3en1", transaction.CodigoAutorizacion);

                // respaldar el Response obtenido para futuros pedidos
                id = BCIWebService.UpdateTransaction(transaction, xmlResponse);

                return xmlResponse;
            }
            else
            {
                Log4Bio.Info("No se válido el user o empresa o point");

                string msjSalida = ResponseTerminal(validationDictionary);

                if (DebugXMLContent)
                    BCIWebService.PersistContent(msjSalida, " WS->GetCCX", "Terminal3en1", transaction.CodigoAutorizacion);

                return msjSalida;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="XMLclient"></param>
        /// <returns></returns>
        public string Persist(string XMLclient)
        {
            Log4Bio.Info("Peticion a Persist ");
            ValidationDictionary validationDictionary = new ValidationDictionary();
            // parse client request
            Transaction transaction = BCIWebService.ParseClient(XMLclient);
            if (transaction == null)
            {
                validationDictionary.AddError("86.1", "Error interno");
                Log4Bio.Info("No se generó la transaction esperada");

                string msjSalida = ResponseTerminal(validationDictionary);

                if (DebugXMLContent)
                    BCIWebService.PersistContent(msjSalida, "Terminal3en1", " WS->Persist", "");

                return msjSalida;
            }

            if (DebugXMLContent)
                BCIWebService.PersistContent(XMLclient, "Terminal3en1", " WS->Persist", transaction.CodigoAutorizacion);

            if (transaction == null)
            {
                validationDictionary.AddError("86.1", "Error interno");
                Log4Bio.Info("No se generó la transaction esperada");

                string msjSalida = ResponseTerminal(validationDictionary);

                if (DebugXMLContent)
                    BCIWebService.PersistContent(msjSalida, "Terminal3en1", " WS->Persist", transaction.CodigoAutorizacion);

                return msjSalida;
            }

            // validaciones
            if (BCIWebService.IsValidateRequest(transaction, validationDictionary))
            {
                transaction.UrlDocument = string.Concat(DeployedAuditSite, "/audit/", transaction.CodigoAutorizacion);

                TransactionDocument transactionDocument = CreateDocumentSignedWithTSA(transaction);

                // persistir
                int id = BCIWebService.Save(transaction, transactionDocument);

                // enroll
                BCIWebService.BioPortalEnroll(transaction);

                if (DebugXMLContent)
                    BCIWebService.PersistContent("Ok", " WS->Persist", "Terminal3en1", transaction.CodigoAutorizacion);

                return "Ok";
            }
            else
            {
                string msjSalida = ResponseTerminal(validationDictionary);

                if (DebugXMLContent)
                    BCIWebService.PersistContent(msjSalida, " WS->Persist", "Terminal3en1", transaction.CodigoAutorizacion);

                return msjSalida;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        private TransactionDocument CreateDocumentSignedWithTSA(Transaction transaction)
        {
            TransactionDocument transactionDocument = null;

            if (BCIWebService.HaveSignedTSA(transaction.Company))
            {
                Log4Bio.Info("Firmando el documento con safestamper");
                // prepare docfor sign
                transactionDocument = BCIWebService.CreateDocumentForSign(transaction);

                transactionDocument.SignerURL = "https://tsa.safestamper.com/";

                // claim tsa
                transactionDocument = BCIWebService.SignWithTSA(transactionDocument);
            }
            return transactionDocument;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="validationDictionary"></param>
        /// <returns></returns>
        private string ResponseTerminal(ValidationDictionary validationDictionary)
        {
            string code = string.Empty;
            string message = string.Empty;

            foreach (string key in validationDictionary.ErrorList.Keys)
            {
                code = key;
                break;
            }

            message = validationDictionary.ErrorList[code];

            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "RespuestaTerminal.xml");
            if (!System.IO.File.Exists(path))
                throw new Exception("No existe el archivo RespuestaTerminal");

            string xmlRespuesta = File.ReadAllText(path);

            xmlRespuesta = Regex.Replace(xmlRespuesta, "{codigo}", code);
            xmlRespuesta = Regex.Replace(xmlRespuesta, "{detalle}", message);
            return xmlRespuesta;
        }
    }
}