﻿using System.ServiceModel;

namespace BCI
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IClientService" in both code and config file together.
    [ServiceContract]
    public interface IClientService
    {
        [OperationContract]
        string GetCCX(string XMLclient);

        [OperationContract]
        string Persist(string XMLclient);
    }
}