﻿using System.ServiceModel;

namespace BCI
{
    [ServiceContract]
    public interface ICustomerService
    {
        [OperationContract]
        string GetCopyCertificate(string user, string password, string company, string rut, string format);

        [OperationContract]
        string GetAuthorizationInfo(string user, string password, string company, string rut);

        [OperationContract]
        string GetTxInfo(string user, string password, string company, string rut, long milisec, int flag);
 
    }
}