﻿using BCI.Libs;
using BCR.Bio.Domain;
using BCR.Bio.Domain.BCI;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Services.BCI;
using BCR.System.Configuration;
using BCR.System.Enum;
using BCR.System.Log;
using BCR.System.Validation;
using Microsoft.Reporting.WebForms;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Tsp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace BCI
{
    /// <summary>
    ///
    /// </summary>
    public static class BCIWebService
    {
        
        /// <summary>
        ///
        /// </summary>
        public static void Initialize()
        {
            string pathLog4Net = ConfigurationManager.AppSettings.Get("PathLog4Net");
            LogConfiguration logConfiguration = new LogConfiguration(LogProvider.Log4Net, new FileInfo(pathLog4Net));

            SystemConfiguration systemConfiguration = new SystemConfiguration(logConfiguration, false);

            UoW = new BCIUnitOfWork(systemConfiguration.HibernateParams, new ValidationDictionary());

            return;
        }

        public static BCIUnitOfWork UoW { get; private set; }

        private static bool HaveContext
        {
            get
            {
                return HttpContext.Current != null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="XMLclient"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static Transaction ParseClient(string XMLclient)
        {
            Transaction transaction = new Transaction();
            // generar codAuto
            Guid guid = Guid.NewGuid();

            try
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(XMLclient);
                XmlNodeList xnList = document.SelectNodes("/request");
                foreach (XmlNode xn in xnList)
                {
                    transaction.Result = xn["result"].InnerText == "T";
                    transaction.Rut = xn["rut"].InnerText;
                    transaction.EventTime = DateTime.Parse(xn["timestamp"].InnerText);
                    transaction.UserId = xn["id"].InnerText;
                    transaction.IdTerminal = xn["idterminal"].InnerText;
                    transaction.FingerPrint = xn["fingerprint"].InnerText;
                    transaction.BodyPart = int.Parse(xn["bodypart"].InnerText);
                    transaction.PatherLastName = xn["patherlastname"] == null ? "" : xn["patherlastname"].InnerText;
                    transaction.Name = xn["name"] == null ? "" : xn["name"].InnerText;
                    transaction.MotherLastName = xn["motherlastname"] == null ? "" : xn["motherlastname"].InnerText;

                    DateTime birthDate = DateTime.Parse(xn["birthdate"].InnerText);
                    if (birthDate == DateTime.MinValue)
                        transaction.BirthDate = null;
                    else
                        transaction.BirthDate = birthDate;

                    DateTime endDate = DateTime.Parse(xn["enddate"].InnerText);
                    if (endDate == DateTime.MinValue)
                        transaction.EndDate = null;
                    else
                        transaction.EndDate = endDate;

                    transaction.Serial = xn["serial"] == null ? "" : xn["serial"].InnerText;

                    transaction.Nacionality = xn["nacionality"] == null ? "" : xn["nacionality"].InnerText;
                    transaction.Sex = xn["sex"] == null ? "" : xn["sex"].InnerText;
                    transaction.Score = xn["score"] == null ? 0 : Double.Parse(xn["score"].InnerText);
                    transaction.Company = xn["company"] == null ? -1 : int.Parse(xn["company"].InnerText);

                    transaction.TimeStamp = transaction.EventTime.ToString("yyyy/MM/dd hh:mm:ss");
                }
            }
            catch (Exception e)
            {
                //ValidationDictionary.AddError("810.5", "Error al recuperar el xml desde la terminal");
                Log4Bio.Info(string.Concat("Error al recuperar el xml desde la terminal ", e.Message));
                return null;
            }
            //transaction.CodigoAutorizacion = guid.ToString();
            if(string.IsNullOrEmpty( GetDomain(transaction.Company)))
            {
                Log4Bio.Info(string.Concat("Error al recuperar la empresa del xml desde la terminal " ));
                return null;

            }
            transaction.CodigoAutorizacion = string.Concat(GetDomain(transaction.Company), "-", guid.ToString());

            return transaction;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static bool IsValidateRequest(Transaction transaction, ValidationDictionary validationDictionary)
        {
            Log4Bio.Info(string.Concat("Entrando a IsValidateRequest "));
            ICompany company = UoW.CompanyService.GetById(transaction.Company);
            if (company == null)
            {
                validationDictionary.AddError("810.2", "Empresa no autorizada o inexistente");
                Log4Bio.Info(string.Concat("Empresa ", transaction.Company, " no existe"));
                return false;
            }

            if (!PersistFalseVerification(company.Id) && !transaction.Result)
            {
                validationDictionary.AddError("810.1", "Cédula no válida");
                return false;
            }

            // determinar si el user es de la compañia
            bool validateUser = ValidateUserCompany(company, transaction.UserId);

            if (validateUser)
            {
                bool validatePoint = ValidatePoint(company, transaction.IdTerminal, validationDictionary);

                //if (!validatePoint)
                //{
                //    ValidationDictionary.AddError("810.3", "Punto no válido");
                //}

                return validatePoint;
            }

            validationDictionary.AddError("810.4", "Usuario no válido");
            return false;
        }

        public static bool HaveSignedTSA(int companyId)
        {
            CompanyAttrib companyAttrib = UoW.CompanyAttribService.GetByCompany(companyId);
            if (companyAttrib != null)
                return companyAttrib.SignWithTSA;
            else
                return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool PersistFalseVerification(int companyId)
        {
            CompanyAttrib companyAttrib = UoW.CompanyAttribService.GetByCompany(companyId);
            if (companyAttrib != null)
                return companyAttrib.PersistFalseVerification;
            else
                return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="company"></param>
        /// <param name="idTerminal"></param>
        /// <param name="ValidationDictionary"></param>
        /// <returns></returns>
        private static bool ValidatePoint(ICompany company, string idTerminal, ValidationDictionary validationDictionary)
        {
            Point point = UoW.PointService.GetBySensorId(company.Id, idTerminal);
            if (point == null)
            {
                validationDictionary.AddError("810.6", "Terminal inexistente");
                Log4Bio.Info(string.Concat("Terminal  ", idTerminal, " no existe"));

                return false;
            }
            else
            {
                if (point.HQ.Company.Id != company.Id)
                {
                    validationDictionary.AddError("810.6", "Terminal no autorizada o inexistente");
                    Log4Bio.Info(string.Concat("Terminal  ", idTerminal, " no existe"));

                    return false;
                }
            }
            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static bool ValidateUserCompany(ICompany company, string userId)
        {
            if (!company.Status)
            {
                Log4Bio.Info(string.Concat("Empresa ", company.Id, " no habilitada"));
                return false;
            }

            IUser user = UoW.UserService.GetByUsername(userId);
            if (user == null)
            {
                Log4Bio.Info(string.Concat("Usuario ", userId, " no existe"));
                return false;
            }

            if (user.Company.Id != company.Id)
            {
                Log4Bio.Info(string.Concat("Usuario ", userId, " no existe en Empresa ", company.Id));
                return false;
            }

            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="transactionDocument"></param>
        /// <returns></returns>
        public static TransactionDocument SignWithTSA(TransactionDocument transactionDocument)
        {
            transactionDocument.HashType = "SHA1";
            SHA1 sha1 = SHA1CryptoServiceProvider.Create();
            byte[] hash = sha1.ComputeHash(transactionDocument.Document);                    // generar un hash del documento

            transactionDocument.Hash = BitConverter.ToString(hash);                  // mantener el hash como control futuro

            TimeStampRequestGenerator reqGen = new TimeStampRequestGenerator();

            reqGen.SetCertReq(true);   // ??
            TimeStampRequest request = reqGen.Generate(TspAlgorithms.Sha1, hash, BigInteger.ValueOf(100));

            byte[] reqData = request.GetEncoded();

            try
            {
                HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(transactionDocument.SignerURL);
                httpReq.Method = "POST";
                httpReq.ContentType = "application/timestamp-query";
                httpReq.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes("gsuhit@biometrika.cl:B1ometrika")));
                httpReq.ContentLength = reqData.Length;

                // Write the request content
                Stream reqStream = httpReq.GetRequestStream();
                reqStream.Write(reqData, 0, reqData.Length);
                reqStream.Close();

                HttpWebResponse httpResp = (HttpWebResponse)httpReq.GetResponse();

                // Read the response
                Stream respStream = new BufferedStream(httpResp.GetResponseStream());
                TimeStampResponse response = new TimeStampResponse(respStream);
                respStream.Close();

                TimeStampToken t = response.TimeStampToken;

                string res1 = "Serial Number: " + t.TimeStampInfo.TstInfo.SerialNumber.ToString();
                string res2 = "Gen Time: " + t.TimeStampInfo.GenTime.ToString();

                string policy = t.TimeStampInfo.Policy.ToString();
                //string obj = Global.ByteToString(t.TimeStampInfo.GetEncoded());
                //obj = addlinebreaks(obj, 32, 32);
                //string res4 = "Encoded timestamp: " + obj;

                transactionDocument.Policy = policy;
                transactionDocument.DateTimeSign = t.TimeStampInfo.GenTime;
            }
            catch (Exception e)
            {
            }
            if (transactionDocument.DateTimeSign == null)
                transactionDocument.DateTimeSign = DateTime.Now;

            return transactionDocument;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="transaction"></param>
        public static void BioPortalEnroll(Transaction transaction)
        {
            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="transactionDocument"></param>
        /// <returns></returns>
        public static int Save(Transaction transaction, TransactionDocument transactionDocument)
        {
            if (transactionDocument != null)
            {
                transactionDocument.Transaction = transaction;
                transaction.TransactionDocument = transactionDocument;
            }

            int id = UoW.TransactionService.Create(transaction);

            return id;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="id"></param>
        public static string GenerateProviderRequest(Transaction transaction, int id, string userPeticion, string passPeticion)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "Peticion.xml");
            if (!System.IO.File.Exists(path))
                throw new Exception("No existe el archivo Peticion");

            string xmlPedido = File.ReadAllText(path);

            xmlPedido = Regex.Replace(xmlPedido, "{llave}", "");
            xmlPedido = Regex.Replace(xmlPedido, "{tipo}", "AUT");
            xmlPedido = Regex.Replace(xmlPedido, "{user}", userPeticion);
            xmlPedido = Regex.Replace(xmlPedido, "{password}", passPeticion);
            xmlPedido = Regex.Replace(xmlPedido, "{rut}", transaction.Rut);
            xmlPedido = Regex.Replace(xmlPedido, "{codautoriza}", transaction.CodigoAutorizacion);

            string xmlResponse = string.Empty;

            if (true)
            {
                if (bool.Parse(ConfigurationManager.AppSettings.Get("DebugXMLContent")))
                    BCIWebService.PersistContent(xmlPedido, "Biometrika", "Previred-CWSMensajeriaXMLClient", transaction.CodigoAutorizacion);

                PreviredWS.CWSMensajeriaXMLClient CWSMensajeriaXMLClient = new PreviredWS.CWSMensajeriaXMLClient();
                xmlResponse = CWSMensajeriaXMLClient.ejecuta(xmlPedido);

                if (bool.Parse(ConfigurationManager.AppSettings.Get("DebugXMLContent")))
                    BCIWebService.PersistContent(xmlResponse, "Previred-CWSMensajeriaXMLClient", "Biometrika", transaction.CodigoAutorizacion);
            }
            else
            {
                ServiceReference1.CWSMensajeriaXMLClient cws = new ServiceReference1.CWSMensajeriaXMLClient();
                xmlResponse = cws.ejecuta(xmlPedido);
            }

            return xmlResponse;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static TransactionDocument CreateDocumentForSign(Transaction transaction)
        {
            LocalReport localReport = new LocalReport();
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Document", "Autorizacion.rdlc");
            if (!System.IO.File.Exists(path))
                return null;

            localReport.ReportPath = path;
            localReport.EnableExternalImages = true;

            List<ReportParameter> parameters = new List<ReportParameter>();

            parameters.Add(new ReportParameter("RutCliente", transaction.Rut));
            parameters.Add(new ReportParameter("Nombre", transaction.PatherLastName == "" ? "-" : transaction.PatherLastName));
            parameters.Add(new ReportParameter("Fechahora", transaction.EventTime.ToString("dd/MM/yyyy hh:mm:ss")));
            parameters.Add(new ReportParameter("NumeroAuditoria", transaction.CodigoAutorizacion));

            parameters.Add(new ReportParameter("UrlVerificacion", transaction.UrlDocument));

            string image64 = Utils.ConvertWSQ64ToString64(transaction.FingerPrint);

            parameters.Add(new ReportParameter("Huella", image64));
            parameters.Add(new ReportParameter("TipoGrafico", "image/bmp"));

            localReport.SetParameters(parameters);

            string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + transaction.Id + "</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            //System.IO.File.WriteAllBytes(@"c:\render5.pdf", renderedBytes);
            TransactionDocument transactionDocument = new TransactionDocument();
            transactionDocument.Document = renderedBytes;
            return transactionDocument;
        }

        public static bool ValidateUser(string usuario, string password)
        {
            /// previred  / previred
            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="password"></param>
        /// <param name="codautoriza"></param>
        /// <returns></returns>
        public static string GetResponseForValidation(string usuario, string password, string codautoriza)
        {
            string xmlRespueta;

            Transaction transaction = UoW.TransactionService.GetByCodigoAutorizacion(codautoriza);

            if (transaction != null)
            {
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "RespuestaAutorizacion.xml");

                if (!System.IO.File.Exists(path))
                    throw new Exception("No existe el archivo RespuestaAutorizacion");
                xmlRespueta = File.ReadAllText(path);

                xmlRespueta = xmlRespueta.Replace("{codautoriza}", transaction.CodigoAutorizacion);
                xmlRespueta = xmlRespueta.Replace("{rut}", transaction.Rut);
                xmlRespueta = xmlRespueta.Replace("{fechahoraconsulta}", transaction.EventTime.ToString("dd/MM/yyyy hh:mm:ss"));

                xmlRespueta = xmlRespueta.Replace("{idcliente}", transaction.IdTerminal);  ///<<<< ver
                xmlRespueta = xmlRespueta.Replace("{resultado}", (transaction.Result ? 1 : 0).ToString());   // 1 para true
                xmlRespueta = xmlRespueta.Replace("{numeroserie}", transaction.Serial);
                xmlRespueta = xmlRespueta.Replace("{apellidopaterno}", transaction.PatherLastName);
                xmlRespueta = xmlRespueta.Replace("{timestamp}", transaction.TimeStamp);
                xmlRespueta = xmlRespueta.Replace("{urlauditoria}", transaction.UrlDocument);
                xmlRespueta = xmlRespueta.Replace("{codigo}", "9000");
            }
            else
            {
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "RespuestaAutorizacionNegativa.xml");

                if (!System.IO.File.Exists(path))
                    throw new Exception("No existe el archivo RespuestaAutorizacion");
                xmlRespueta = File.ReadAllText(path);
            }

            return xmlRespueta;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public static string UsuarioNoValido()
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "UsuarioNoValido.xml");

            if (!System.IO.File.Exists(path))
                throw new Exception("No existe el archivo UsuarioNoValido");

            return File.ReadAllText(path);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="XMLclient"></param>
        public static void PersistContent(string XMLclient, string source, string destination, string codigoAutorizacion)
        {
            //string codigoAutorizacion = string.Empty;

            Content content = new Content()
            {
                ContentXml = XMLclient,
                CreateDate = DateTime.Now,
                Source = source,
                Destination = destination,
                EventTime = DateTime.Now,
                CodigoAutorizacion = codigoAutorizacion
            };

            UoW.ContentService.Create(content);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static string GetDomain(int companyId)
        {
            ICompany company = UoW.CompanyService.GetById(companyId);
            if (company == null)
                return string.Empty;
            else
                return company.Domain;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="xmlResponse"></param>
        /// <param name="ValidationDictionary"></param>
        /// <returns></returns>
        public static int UpdateTransaction(Transaction transaction, string xmlResponse)
        {
            TransactionContent transactionContent = new TransactionContent();

            transactionContent.EventTime = DateTime.Now;
            transactionContent.Rut = transaction.Rut;
            transactionContent.Content = xmlResponse;
            transactionContent.Transaction = transaction;
            transactionContent.Company = transaction.Company;
            transaction.TransactionContent = transactionContent;

            transaction = UoW.TransactionService.Update(transaction);

            return transaction.Id;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="company"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool IsValidUser(string company, string user, string password)
        {
            ICompany companyC = UoW.CompanyService.GetById(int.Parse(company));
            if (ValidateUserCompany(companyC, user))
            {
                return UoW.ValidateUser(user, password);
            }
            return false;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="dateFrom"></param>
        /// <returns></returns>
        public static string GetContentByRut(string companyId, string rut, DateTime dateFrom)
        {
            TransactionContent transactionContent = UoW.TransactionContentService.GetByRutAndDate(companyId, rut, dateFrom);

            if (transactionContent == null)
                return string.Empty;
            else
                return transactionContent.Content;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="serviceRequest"></param>
        public static void GenerateRequestLog(ServiceRequest serviceRequest)
        {
            UoW.ServiceRequestService.Create(serviceRequest);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="dateFrom"></param>
        /// <param name="ValidationDictionary"></param>
        /// <returns></returns>
        public static Transaction GetTransactionInfoByRut(string companyId, string rut, DateTime dateFrom)
        {
            Paginacion paginacion = new Paginacion()
            {
                Pagina = 0,
                Cantidad = 1
            };

            TransactionSearch transactionSearch = new TransactionSearch()
            {
                EventTimeFrom = dateFrom,
                Rut = rut,
                Company = int.Parse(companyId)
            };

            Transaction transaction = UoW.TransactionService.ListByApellidoOrRut(paginacion, transactionSearch).ToList().FirstOrDefault();

            return transaction;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="dateFrom"></param>
        /// <param name="ValidationDictionary"></param>
        /// <returns></returns>
        public static Transaction GetTransactionInfoByRutDemo(string companyId, string rut, DateTime dateFrom)
        {
            Paginacion paginacion = new Paginacion()
            {
                Pagina = 0,
                Cantidad = 1
            };

            TransactionSearch transactionSearch = new TransactionSearch()
            {
                EventTimeFrom = dateFrom,
                Rut = rut,
                Company = int.Parse(companyId)
            };

            Transaction transaction = UoW.TransactionService.ListByApellidoOrRut(paginacion, transactionSearch).ToList().FirstOrDefault();

            return transaction;
        }

        private static string ParseXMLPrevired(String transactioncontentxml)
        {
            string xmlResponse = transactioncontentxml;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlResponse);
            string xml = "";
            string xmlpaso = "";
            // Loop through the child nodes and consider all but comments and declaration
            if (doc.HasChildNodes)
            {
                StringBuilder builder = new StringBuilder();

                foreach (XmlNode node in doc.ChildNodes)
                    if (node.NodeType != XmlNodeType.XmlDeclaration && node.NodeType != XmlNodeType.Comment)
                        builder.Append(node.OuterXml);

                xml = builder.ToString();
                xmlpaso = xml.Substring(11);
                xml = xmlpaso.Substring(0, xmlpaso.Length - 12);
            }
            return xml;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="codigoAutorizacion"></param>
        /// <param name="urlDocument"></param>
        /// <returns></returns>
        public static string GenerateTxtInfoResponse(Transaction transaction, int flag)
        {
            string path = String.Empty;
            string xmlRespuesta = String.Empty;

            if (flag == 0)
            {
                String xml = "";
                if (transaction.TransactionContent != null)
                {
                    xml = BCIWebService.ParseXMLPrevired(transaction.TransactionContent.Content);
                }
                path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "GetInfoTxtTxContent.xml");
                if (!System.IO.File.Exists(path))
                    throw new Exception("No existe el archivo GetInfoTxtTxContent");

                xmlRespuesta = File.ReadAllText(path);
                xmlRespuesta = xmlRespuesta.Replace("{codigo}", "9000");
                xmlRespuesta = xmlRespuesta.Replace("{codautoriza}", transaction.CodigoAutorizacion);
                xmlRespuesta = xmlRespuesta.Replace("{rut}", transaction.Rut);
                xmlRespuesta = xmlRespuesta.Replace("{fechahoraconsulta}", transaction.EventTime.ToString("dd/MM/yyyy hh:mm:ss"));
                xmlRespuesta = xmlRespuesta.Replace("{idcliente}", transaction.IdTerminal);
                xmlRespuesta = xmlRespuesta.Replace("{resultado}", transaction.Result.ToString());
                xmlRespuesta = xmlRespuesta.Replace("{numeroserie}", transaction.Serial);
                xmlRespuesta = xmlRespuesta.Replace("{apellidopaterno}", transaction.PatherLastName);
                xmlRespuesta = xmlRespuesta.Replace("{timestamp}", transaction.TimeStamp);
                xmlRespuesta = xmlRespuesta.Replace("{wsq}", transaction.FingerPrint);
                xmlRespuesta = xmlRespuesta.Replace("{urlauditoria}", transaction.UrlDocument);
                xmlRespuesta = xmlRespuesta.Replace("{previredcontent}", xml);
                xmlRespuesta = xmlRespuesta.Replace("{error}", "");
            }
            else if (flag == 1)
            {
                String xml = "";
                if (transaction.TransactionContent != null)
                {
                    xml = BCIWebService.ParseXMLPrevired(transaction.TransactionContent.Content);
                }
                path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "GetInfoTxtTxContentNoWsq.xml");
                if (!System.IO.File.Exists(path))
                    throw new Exception("No existe el archivo GetInfoTxtTxContent");

                xmlRespuesta = File.ReadAllText(path);
                xmlRespuesta = xmlRespuesta.Replace("{codigo}", "9000");
                xmlRespuesta = xmlRespuesta.Replace("{codautoriza}", transaction.CodigoAutorizacion);
                xmlRespuesta = xmlRespuesta.Replace("{rut}", transaction.Rut);
                xmlRespuesta = xmlRespuesta.Replace("{fechahoraconsulta}", transaction.EventTime.ToString("dd/MM/yyyy hh:mm:ss"));
                xmlRespuesta = xmlRespuesta.Replace("{idcliente}", transaction.IdTerminal);
                xmlRespuesta = xmlRespuesta.Replace("{resultado}", transaction.Result.ToString());
                xmlRespuesta = xmlRespuesta.Replace("{numeroserie}", transaction.Serial);
                xmlRespuesta = xmlRespuesta.Replace("{apellidopaterno}", transaction.PatherLastName);
                xmlRespuesta = xmlRespuesta.Replace("{timestamp}", transaction.TimeStamp);
                xmlRespuesta = xmlRespuesta.Replace("{urlauditoria}", transaction.UrlDocument);
                xmlRespuesta = xmlRespuesta.Replace("{previredcontent}", xml);
                xmlRespuesta = xmlRespuesta.Replace("{error}", "");
            }
            else if (flag == 2)
            {
                path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "GetInfoTxtNoTxContent.xml");
                if (!System.IO.File.Exists(path))
                    throw new Exception("No existe el archivo GetInfoTxtNoTxContent");

                xmlRespuesta = File.ReadAllText(path);
                xmlRespuesta = xmlRespuesta.Replace("{codigo}", "9000");
                xmlRespuesta = xmlRespuesta.Replace("{codautoriza}", transaction.CodigoAutorizacion);
                xmlRespuesta = xmlRespuesta.Replace("{rut}", transaction.Rut);
                xmlRespuesta = xmlRespuesta.Replace("{fechahoraconsulta}", transaction.EventTime.ToString("dd/MM/yyyy hh:mm:ss"));
                xmlRespuesta = xmlRespuesta.Replace("{idcliente}", transaction.IdTerminal);
                xmlRespuesta = xmlRespuesta.Replace("{resultado}", transaction.Result.ToString());
                xmlRespuesta = xmlRespuesta.Replace("{numeroserie}", transaction.Serial);
                xmlRespuesta = xmlRespuesta.Replace("{apellidopaterno}", transaction.PatherLastName);
                xmlRespuesta = xmlRespuesta.Replace("{timestamp}", transaction.TimeStamp);
                xmlRespuesta = xmlRespuesta.Replace("{wsq}", transaction.FingerPrint);
                xmlRespuesta = xmlRespuesta.Replace("{urlauditoria}", transaction.UrlDocument);
                xmlRespuesta = xmlRespuesta.Replace("{error}", "");
            }
            else if (flag == 3)
            {
                path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "GetInfoTxtNoWsq.xml");
                if (!System.IO.File.Exists(path))
                    throw new Exception("No existe el archivo GetInfoTxtNoWsq");

                xmlRespuesta = File.ReadAllText(path);
                xmlRespuesta = xmlRespuesta.Replace("{codigo}", "9000");
                xmlRespuesta = xmlRespuesta.Replace("{codautoriza}", transaction.CodigoAutorizacion);
                xmlRespuesta = xmlRespuesta.Replace("{rut}", transaction.Rut);
                xmlRespuesta = xmlRespuesta.Replace("{fechahoraconsulta}", transaction.EventTime.ToString("dd/MM/yyyy hh:mm:ss"));
                xmlRespuesta = xmlRespuesta.Replace("{idcliente}", transaction.IdTerminal);
                xmlRespuesta = xmlRespuesta.Replace("{resultado}", transaction.Result.ToString());
                xmlRespuesta = xmlRespuesta.Replace("{numeroserie}", transaction.Serial);
                xmlRespuesta = xmlRespuesta.Replace("{apellidopaterno}", transaction.PatherLastName);
                xmlRespuesta = xmlRespuesta.Replace("{timestamp}", transaction.TimeStamp);
                xmlRespuesta = xmlRespuesta.Replace("{urlauditoria}", transaction.UrlDocument);
                xmlRespuesta = xmlRespuesta.Replace("{error}", "");
            }
            return xmlRespuesta;
        }
    }
}