﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebNV.WebSite.Models;

namespace WebNV.Controllers
{
    [Authorize]
    //[InitializeSimpleMembership]
    public class AccountController : BaseController
    {
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (UoW.ValidateUser(model.UserName, model.Password))  //( )
                {
                    ICompany currentCompany;
                    if (CurrentUserCompany != null)
                        currentCompany = CurrentUserCompany;
                    else
                        currentCompany = UoW.UserService.GetByUsername(model.UserName).Company;



                    if (currentCompany.Status && currentCompany.EndDate == null)
                    {
                        FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                            && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Welcome", "Home");
                        }
                    }
                    else
                    {
                        model.ErrorMessage = "Empresa desactivada desde : " + currentCompany.EndDate;
                    }

                }
                else
                {
                    ModelState.AddModelError("", "Usuario o clave no válido");
                    model.ErrorMessage = "Usuario o clave incorrecto";
                    TempData["ViewData"] = ViewData;
                }
            }
            else
            {
                ModelState.AddModelError("", "Error al ingresar.");
                TempData["ViewData"] = ViewData;
            }

            // If we got this far, something failed, redisplay form
            return View("~/Views/Home/index.cshtml", model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogInBVI(string username, string password)
        {
            if (string.IsNullOrEmpty(username)) return null;
            if (string.IsNullOrEmpty(password)) return null;
            JsonResult result = null;
            bool loginSuccess = false;
            string Message = "";

            if (UoW.ValidateUser(username, password))
            {
                ICompany currentCompany;
                IUser user;
                if (CurrentUserCompany != null && CurrentUser != null)
                {
                    currentCompany = CurrentUserCompany;
                    user = CurrentUser;
                }
                else
                {
                    user = UoW.UserService.GetByUsername(username);
                    user.Password = null;
                    user.PasswordQuestion = null;
                    user.PasswordAnswer = null;
                    user.PasswordSalt = null;



                    currentCompany = user.Company;
                    currentCompany.HQs = null;
                    currentCompany.Sectors = null;
                    currentCompany.Users = null;
                    currentCompany.AccessName = null;
                    currentCompany.SecretKey = null;

                    if (user.Companies != null && user.Companies.Count > 0)
                    {

                        foreach(Company company in user.Companies)
                        {
                            company.HQs = null;
                            company.Sectors = null;
                            company.Users = null;
                            company.AccessName = null;
                            company.SecretKey = null;
                        }
                        //user.Companies = new List<Company>
                        //{
                        //    (Company) currentCompany
                        //};
                    }

                }

                if (currentCompany.Status && currentCompany.EndDate == null)
                {
                    loginSuccess = true;
                    Message = "Acceso Correcto";
                }
                else
                {
                    Message = "Empresa desactivada desde " + currentCompany.EndDate;
                }
                result = Json(new { loginSuccess, user, Message }, JsonRequestBehavior.DenyGet);
            }
            else
            {
                Message = "Usuario/Contraseña incorrecta";
                result = Json(new { loginSuccess, Message }, JsonRequestBehavior.DenyGet);
            }

            return result;
        }

        //
        // POST: /Account/LogOff

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);

            HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", "");
            cookie2.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie2);

            FormsAuthentication.RedirectToLoginPage();

            return RedirectToAction("Index", "Home");
        }

        #region Helpers

        //private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        //{
        //    // See http://go.microsoft.com/fwlink/?LinkID=177550 for
        //    // a full list of status codes.
        //    switch (createStatus)
        //    {
        //        case MembershipCreateStatus.DuplicateUserName:
        //            return "User name already exists. Please enter a different user name.";

        //        case MembershipCreateStatus.DuplicateEmail:
        //            return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

        //        case MembershipCreateStatus.InvalidPassword:
        //            return "The password provided is invalid. Please enter a valid password value.";

        //        case MembershipCreateStatus.InvalidEmail:
        //            return "The e-mail address provided is invalid. Please check the value and try again.";

        //        case MembershipCreateStatus.InvalidAnswer:
        //            return "The password retrieval answer provided is invalid. Please check the value and try again.";

        //        case MembershipCreateStatus.InvalidQuestion:
        //            return "The password retrieval question provided is invalid. Please check the value and try again.";

        //        case MembershipCreateStatus.InvalidUserName:
        //            return "The user name provided is invalid. Please check the value and try again.";

        //        case MembershipCreateStatus.ProviderError:
        //            return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

        //        case MembershipCreateStatus.UserRejected:
        //            return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

        //        default:
        //            return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
        //    }
        //}

        #endregion Helpers
    }
}