﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using System.Linq;
using System.Web.Mvc;
using WebNV.Models.Admin;

namespace WebNV.Controllers
{
    public class GeoController : BaseController
    {
        [Authorize(Roles = ("Super Admin"))]
        public ActionResult Index()
        {
            GeoModel geoModel = new GeoModel();
            geoModel.Countries = UoW.CountryService.List().ToList();

            return View(geoModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="cityName"></param>
        /// <returns></returns>
        public ActionResult AddCity(int parentId, string cityName)
        {
            IDept dept = UoW.DeptService.GetById(parentId);

            ICity city = new City()
            {
                Description = cityName,
                Dept = (Dept)dept
            };

            int idCity = UoW.CityService.Create(city);

            JsonResult res = Json(new { idCity }, JsonRequestBehavior.AllowGet);

            return res;
        }
    }
}