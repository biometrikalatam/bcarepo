﻿using BCR.Bio.Domain.NV;
using System.Collections.Generic;
using System.Web.Mvc;
using WebNV.Helpers;
using WebNV.Models.NV;

namespace WebNV.Controllers
{
    [Authorize]
    public class ExtensionController : BaseController
    {
        // GET: Extension
        public ActionResult Index()
        {
            IList<Extension> list = UoW.ExtensionService.ListByCriteria(CurrentUserCompany.Id);
            return View(list);
        }

        public ActionResult Create()
        {
            ExtensionModel extensionModel = new ExtensionModel();
            extensionModel.AplicacionList = Web.AplicacionList(UoW.AplicacionService.ListByCriteria(CurrentUserCompany.Id), "");

            return View(extensionModel);
        }

        [HttpPost]
        public ActionResult Create(ExtensionModel extensionModel)
        {
            Extension extension = new Extension();
            extension.Aplicacion = UoW.AplicacionService.GetById(extensionModel.Aplicacion);
            extension.Nombre = extensionModel.Extension.Nombre;
            extension.Tipo = extensionModel.Extension.Tipo;

            UoW.ExtensionService.Create(extension);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            ExtensionModel extensionModel = new ExtensionModel();
            extensionModel.Extension = UoW.ExtensionService.GetById(id);
            extensionModel.Aplicacion = extensionModel.Extension.Aplicacion.Id;

            extensionModel.AplicacionList = Web.AplicacionList(UoW.AplicacionService.List(), extensionModel.Aplicacion.ToString());

            return View(extensionModel);
        }

        [HttpPost]
        public ActionResult Edit(ExtensionModel extensionModel)
        {
            Extension extension = UoW.ExtensionService.GetById(extensionModel.Extension.Id);
            extension.Aplicacion = UoW.AplicacionService.GetById(extensionModel.Aplicacion);
            extension.Nombre = extensionModel.Extension.Nombre;
            extension.Tipo = extensionModel.Extension.Tipo;

            UoW.ExtensionService.Update(extension);

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            ExtensionModel extensionModel = new ExtensionModel();
            extensionModel.Extension = UoW.ExtensionService.GetById(id);
            extensionModel.Aplicacion = extensionModel.Extension.Aplicacion.Id;

            return View(extensionModel);
        }

        [HttpPost]
        public ActionResult Delete(ExtensionModel extensionModel)
        {
            Extension extension = UoW.ExtensionService.GetById(extensionModel.Extension.Id);

            //if (extension.Aplicacion.)

            UoW.ExtensionService.Delete<Extension>(extensionModel.Extension.Id);

            return RedirectToAction("Index");
        }
    }
}