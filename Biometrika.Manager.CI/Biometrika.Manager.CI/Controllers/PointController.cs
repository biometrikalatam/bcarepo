﻿using AutoMapper;
using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using WebNV.Helpers;
using WebNV.Models.Access;

namespace WebNV.Controllers
{
    [Authorize]
    public class PointController : BaseController
    {
        //
        // GET: /Point/

        public ActionResult IndexBase(int? companyId)
        {
            PointListModel pointListModel = new PointListModel();
            if (Roles.IsUserInRole(CurrentUser.Username, "Super Admin"))
            {
                pointListModel.Companies = Web.CompanyList(UoW.CompanyService.List());
                if (companyId == null)
                    pointListModel.Company = null; // UoW.CompanyService.List().FirstOrDefault();
                else
                    pointListModel.Company = UoW.CompanyService.GetById((int)companyId, withHQs: true, withSectors: true);

                pointListModel.CompanyValue = pointListModel.Company == null ? 0 : pointListModel.Company.Id;
            }
            else
            {
                pointListModel.Companies = new List<SelectListItem>();
                pointListModel.Company = (Company)UoW.CompanyService.GetById(CurrentUserCompany.Id, withHQs: true, withSectors: true);

                SelectListItem companyItem = new SelectListItem()
                {
                    Text = pointListModel.Company.Name,
                    Value = pointListModel.Company.Id.ToString()
                };

                pointListModel.Companies.Add(companyItem);
            }

            return View(pointListModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<PointModel> pointModelList = new List<PointModel>();

            int holdingId = UoW.GetHolding(User.Identity.Name);

            UoW.PointService
                .GetByHolding(holdingId)
                .ToList()
                .ForEach(m => pointModelList.Add(Mapper.Map<Point, PointModel>((Point)m)));

            return View(pointModelList);
        }

        /// <summary>
        /// Crea la compañia , Get
        /// </summary>
        /// <returns></returns>
        public ActionResult Create(string idCompany)
        {
            ICompany company = UoW.CompanyService.GetById(int.Parse(idCompany), withHQs: true, withSectors: true);

            PointModel pointModel = new PointModel();

            pointModel.Company = company;
            pointModel.HQList = Web.HQList(company.HQs, "");
            pointModel.SectorList = Web.SectorList(company.Sectors, "");

            //bpCompanyModel.Status = true;
            return View(pointModel);
        }

        /// <summary>
        /// Crea la compañia , post
        /// </summary>
        /// <param name="bpCompanyModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(PointModel pointModel)
        {
            try
            {
                if (UoW.PointService.GetBySensorName(pointModel.Name) == null)   //  agregar q busque por ocmpañia
                {
                    Point point = Mapper.Map<PointModel, Point>(pointModel);
                    point.HQ = (HQ)UoW.HQService.GetById(pointModel.HQId);
                    point.Sector = (Sector)UoW.SectorService.GetById(pointModel.SectorId);

                    int idPoint = UoW.PointService.Create(point);

                    point = (Point)UoW.PointService.GetById(idPoint);

                    if (!ModelState.IsValid)
                    {
                        ModelState.AddModelError("", "Error interno ");
                        return View(pointModel);
                    }

                    return RedirectToAction("IndexBase", new { companyId = point.HQ.Company.Id });
                }
                else
                {
                    ModelState.AddModelError("", "Existe otro punto con ese nombre");
                    return View(pointModel);
                }
            }
            catch
            {
                ModelState.AddModelError("", "Error interno ");
                return View(pointModel);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            PointModel pointModel = Mapper.Map<Point, PointModel>((Point)UoW.PointService.GetById(id));

            pointModel.Company = UoW.CompanyService.GetById(pointModel.HQ.Company.Id, withHQs: true, withSectors: true);
            pointModel.HQList = Web.HQList(pointModel.Company.HQs, "");
            pointModel.SectorList = Web.SectorList(pointModel.Company.Sectors, "");

            return View(pointModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="pointModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(PointModel pointModel)
        {
            try
            {
                IPoint pOrg = UoW.PointService.GetById(pointModel.Id);

                Point point = Mapper.Map<PointModel, Point>(pointModel);

                point.CreateDate = pOrg.CreateDate;
                point.HQ = (HQ)UoW.HQService.GetById(pointModel.HQId);
                point.Sector = (Sector)UoW.SectorService.GetById(pointModel.SectorId);

                IPoint pointupdated = UoW.PointService.Update(point);

                point = (Point)UoW.PointService.GetById(pointupdated.Id);

                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", "Error interno ");
                    return View(pointModel);
                }

                return RedirectToAction("IndexBase", new { companyId = point.HQ.Company.Id });
            }
            catch
            {
                ModelState.AddModelError("", "Error interno ");
                return View(pointModel);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Disable(int id, int idCompany)
        {
            UoW.PointService.Disable(id);
            return RedirectToAction("IndexBase", new { companyId = idCompany });
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Enable(int id, int idCompany)
        {
            UoW.PointService.Enable(id);

            return RedirectToAction("IndexBase", new { companyId = idCompany });
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            UoW.PointService.Delete<IPoint>(id);
            return RedirectToAction("Index");
        }
    }
}