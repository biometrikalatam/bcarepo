﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.NV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebNV.Helpers;
using WebNV.Models.NV;

namespace WebNV.Controllers
{
    [Authorize(Roles = ("Super Admin"))]
    public class AplicacionController : BaseController
    {
        // GET: Extension
        public ActionResult Index()
        {
            IList<Aplicacion> list = UoW.AplicacionService.ListByCriteria(CurrentUserCompany.Id);
            return View(list);
        }

        public ActionResult Create()
        {
            AplicacionModel aplicacionModel = new AplicacionModel();

            //aplicacionModel.CompanyList =Web.CompanyList(UoW.CompanyService.List(), true);
            aplicacionModel.Company = CurrentUserCompany.Id;
            aplicacionModel.CompanyName = UoW.CompanyService.GetById(CurrentUserCompany.Id).Name;
            return View(aplicacionModel);
        }

        [HttpPost]
        public ActionResult Create(AplicacionModel aplicacionModel)
        {
            Aplicacion aplicacion = new Aplicacion();
            aplicacion.Company = (Company)UoW.CompanyService.GetById(CurrentUserCompany.Id);
            aplicacion.Nombre = aplicacionModel.Aplicacion.Nombre;
            aplicacion.Checkingen = aplicacionModel.Aplicacion.Checkingen;

            aplicacion.Qextensiones = aplicacionModel.Aplicacion.Qextensiones;
            aplicacion.Signaturepsw = aplicacionModel.Aplicacion.Signaturepsw;
            aplicacion.Signaturetype = aplicacionModel.Aplicacion.Signaturetype;
            aplicacion.Urlfea = aplicacionModel.Aplicacion.Urlfea;
            aplicacion.Userfea = aplicacionModel.Aplicacion.Userfea;
            aplicacion.Appfea = aplicacionModel.Aplicacion.Appfea;
            aplicacion.Certificatecer = aplicacionModel.Aplicacion.Certificatecer;
            aplicacion.Certificatepxf = aplicacionModel.Aplicacion.Certificatepxf;


            UoW.AplicacionService.Create(aplicacion);
            
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            AplicacionModel aplicacionModel = new AplicacionModel();
            aplicacionModel.Aplicacion = UoW.AplicacionService.GetById(id);
            aplicacionModel.Company = CurrentUserCompany.Id;

            //aplicacionModel.CompanyList = Web.CompanyList(UoW.CompanyService.List(), true);

            return View(aplicacionModel);
        }

        [HttpPost]
        public ActionResult Edit(AplicacionModel aplicacionModel)
        {
            Aplicacion aplicacion = UoW.AplicacionService.GetById(aplicacionModel.Aplicacion.Id);
 
            aplicacion.Company = (Company)UoW.CompanyService.GetById(CurrentUserCompany.Id);

            aplicacion.Nombre = aplicacionModel.Aplicacion.Nombre;

            aplicacion.Checkingen = aplicacionModel.Aplicacion.Checkingen;
            aplicacion.Qextensiones = aplicacionModel.Aplicacion.Qextensiones;
            aplicacion.Signaturepsw = aplicacionModel.Aplicacion.Signaturepsw;
            aplicacion.Signaturetype = aplicacionModel.Aplicacion.Signaturetype;
            aplicacion.Urlfea = aplicacionModel.Aplicacion.Urlfea;
            aplicacion.Userfea = aplicacionModel.Aplicacion.Userfea;
            aplicacion.Appfea = aplicacionModel.Aplicacion.Appfea;
            aplicacion.Certificatecer = aplicacionModel.Aplicacion.Certificatecer;
            aplicacion.Certificatepxf = aplicacionModel.Aplicacion.Certificatepxf;

            UoW.AplicacionService.Update(aplicacion);


            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            AplicacionModel aplicacionModel = new AplicacionModel();
            aplicacionModel.Aplicacion = UoW.AplicacionService.GetById(id);
            aplicacionModel.Company = aplicacionModel.Aplicacion.Company.Id;


            return View(aplicacionModel);
        }

        [HttpPost]
        public ActionResult Delete(AplicacionModel aplicacionModel)
        {
            Aplicacion Aplicacion = UoW.AplicacionService.GetById(aplicacionModel.Aplicacion.Id);

            //if (Aplicacion.Aplicacion.)

            UoW.AplicacionService.Delete<Aplicacion>(aplicacionModel.Aplicacion.Id);


            return RedirectToAction("Index");
        }
    }
}