﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.NV;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace WebNV.Controllers
{
    [Authorize(Roles = ("Super Admin"))]
    public class CompanyWorkFlowController : BaseController
    {
        // GET: CompanyWorkFlow
        [HttpGet]
        public ActionResult Index()
        {
            IList<CompanyWorkFlow> workFlows = UoW.CompanyWorkFlowService.GetAll();
            return View(workFlows);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            CompanyWorkFlow companyWorkFlow = UoW.CompanyWorkFlowService.GetById(id);

            string[] parsedAuthorizationMessage = ParseHtml(companyWorkFlow.AuthorizationMessage);

            ViewBag.AuthorizationMessageTitle = parsedAuthorizationMessage[0];
            ViewBag.AuthorizationMessageBody = parsedAuthorizationMessage[1];
            ViewBag.AuthorizationMessageFooter = parsedAuthorizationMessage[2];

            List<SelectListItem> companySelectList = new List<SelectListItem>();

            var companyList = UoW.CompanyService.List();
            foreach (var company in companyList)
            {
                if (companyWorkFlow.Company.Id == company.Id)
                {
                    companySelectList.Add(new SelectListItem() { Value = company.Id.ToString(), Text = company.Name, Selected = true });
                } else
                {
                    companySelectList.Add(new SelectListItem() { Value = company.Id.ToString(), Text = company.Name, Selected = false });
                }
            }
            ViewBag.CompanySelectList = companySelectList;

            //string[] parsedVideoMessage = ParseHtml(companyWorkFlow.VideoMessage);

            //ViewBag.VideoMessageTitle = parsedVideoMessage[0];
            //ViewBag.VideoMessageBody = parsedVideoMessage[1];
            //ViewBag.VideoMessageFooter = parsedVideoMessage[2];


            return View(companyWorkFlow);
        }

        [HttpGet]
        public ActionResult Create()
        {
            CompanyWorkFlow companyWorkFlow = new CompanyWorkFlow();
            List<SelectListItem> companySelectList = new List<SelectListItem>();

            var companyList = UoW.CompanyService.List();
            foreach (var company in companyList)
            {
                companySelectList.Add(new SelectListItem() { Value = company.Id.ToString(), Text = company.Name });
            }
            ViewBag.CompanySelectList = companySelectList;
            return View(companyWorkFlow);
        }

        [HttpPost]
        public ActionResult Create(CompanyWorkFlow formWorkFlow, string AuthorizationMessageFooter, string AuthorizationMessageBody, string AuthorizationMessageTitle, int CompanySelectList)
        {
            string authorizationMessageHtml = GenerateHtml(AuthorizationMessageTitle, AuthorizationMessageBody, AuthorizationMessageFooter);

            try
            {
                Log4Bio.Info("Creando Workflow con id");
                formWorkFlow.AuthorizationMessage = authorizationMessageHtml;
                formWorkFlow.Company = (Company) UoW.CompanyService.GetById(CompanySelectList);
                int id = UoW.CompanyWorkFlowService.Create(formWorkFlow);
                if (id > 0)
                {
                    IList<CompanyWorkFlow> workFlows = UoW.CompanyWorkFlowService.GetAll();
                    return View("Index", workFlows);
                }
                else
                {
                    return View(formWorkFlow);
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("Error al crear Workflow");
                Log4Bio.Error(ex.ToString());
                return View(formWorkFlow);
            }
        }

        [HttpGet]
        public ActionResult Clone(int id)
        {
            CompanyWorkFlow baseCompanyWorkFlow = UoW.CompanyWorkFlowService.GetById(id);
            if (baseCompanyWorkFlow != null)
            {
                CompanyWorkFlow newCompanyWorkFlow = new CompanyWorkFlow
                {
                    Authorization = baseCompanyWorkFlow.Authorization,
                    AuthorizationMessage = baseCompanyWorkFlow.AuthorizationMessage,
                    BpWebTypeOnBoarding = baseCompanyWorkFlow.BpWebTypeOnBoarding,
                    BpWebTypeVerify = baseCompanyWorkFlow.BpWebTypeVerify,
                    CheckAdult = baseCompanyWorkFlow.CheckAdult,
                    CheckExpired = baseCompanyWorkFlow.CheckExpired,
                    CheckLockSrcei = baseCompanyWorkFlow.CheckLockSrcei,
                    Company = baseCompanyWorkFlow.Company,
                    GeoRefMandatory = baseCompanyWorkFlow.GeoRefMandatory,
                    Name = baseCompanyWorkFlow.Name + " copia",
                    OnBoardingMandatory = baseCompanyWorkFlow.OnBoardingMandatory,
                    ReceiverList = baseCompanyWorkFlow.ReceiverList,
                    RedirectUrl = baseCompanyWorkFlow.RedirectUrl,
                    SignerInclude = baseCompanyWorkFlow.SignerInclude,
                    Theme = baseCompanyWorkFlow.Theme,
                    Threshold = baseCompanyWorkFlow.Threshold,
                    VideoInclude = baseCompanyWorkFlow.VideoInclude,
                    VideoMessage = baseCompanyWorkFlow.VideoMessage
                };
                UoW.CompanyWorkFlowService.Create(newCompanyWorkFlow);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var res = UoW.CompanyWorkFlowService.Delete<CompanyWorkFlow>(id);
            return Json(new { res }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Edit(CompanyWorkFlow formWorkFlow, string AuthorizationMessageFooter, string AuthorizationMessageBody, string AuthorizationMessageTitle, int CompanySelectList)
        {
            CompanyWorkFlow companyWorkFlow = UoW.CompanyWorkFlowService.GetById(formWorkFlow.Id);

            string authorizationMessageHtml = GenerateHtml(AuthorizationMessageTitle, AuthorizationMessageBody, AuthorizationMessageFooter);
            //string videoMessageHtml = GenerateHtml(VideoMessageTitle, VideoMessageBody, VideoMessageFooter);

            try
            {
                Log4Bio.Info("Editando Workflow con id :" + companyWorkFlow.Id);
                companyWorkFlow.Name = formWorkFlow.Name;
                companyWorkFlow.Company = (Company) UoW.CompanyService.GetById(CompanySelectList);
                companyWorkFlow.OnBoardingMandatory = formWorkFlow.OnBoardingMandatory;
                companyWorkFlow.BpWebTypeOnBoarding = formWorkFlow.BpWebTypeOnBoarding;
                companyWorkFlow.BpWebTypeVerify = formWorkFlow.BpWebTypeVerify;
                companyWorkFlow.ReceiverList = formWorkFlow.ReceiverList;
                companyWorkFlow.Threshold = formWorkFlow.Threshold;
                companyWorkFlow.Authorization = formWorkFlow.Authorization;
                companyWorkFlow.AuthorizationMessage = authorizationMessageHtml;
                companyWorkFlow.SignerInclude = formWorkFlow.SignerInclude;
                companyWorkFlow.CheckLockSrcei = formWorkFlow.CheckLockSrcei;
                companyWorkFlow.VideoInclude = formWorkFlow.VideoInclude;
                companyWorkFlow.VideoMessage = formWorkFlow.VideoMessage;
                companyWorkFlow.Theme = formWorkFlow.Theme;
                companyWorkFlow.GeoRefMandatory = formWorkFlow.GeoRefMandatory;
                companyWorkFlow.RedirectUrl = formWorkFlow.RedirectUrl;
                companyWorkFlow = UoW.CompanyWorkFlowService.Update(companyWorkFlow);

            }
            catch (Exception ex)
            {
                Log4Bio.Error("Error al editar Workflow");
                Log4Bio.Error(ex.ToString());
            }
            List<SelectListItem> companySelectList = new List<SelectListItem>();

            var companyList = UoW.CompanyService.List();
            foreach (var company in companyList)
            {
                if (companyWorkFlow.Company.Id == company.Id)
                {
                    companySelectList.Add(new SelectListItem() { Value = company.Id.ToString(), Text = company.Name, Selected = true });
                }
                else
                {
                    companySelectList.Add(new SelectListItem() { Value = company.Id.ToString(), Text = company.Name, Selected = false });
                }
            }
            ViewBag.CompanySelectList = companySelectList;
            return View(companyWorkFlow);
        }

        private string GenerateHtml(string title, string body, string footer)
        {
            string separator = "<!-- * -->";
            string htmlGenerated = "<strong>" + title + "</strong> <br />" + separator;
            htmlGenerated += "<p>" + body + "</p> <hr />" + separator;
            htmlGenerated += "<i>" + footer + "</i>";
            return htmlGenerated;
        }


        private string[] ParseHtml(string html)
        {
            string[] separator = { "<!-- * -->" };
            string[] htmlParsed;
            try
            {
                htmlParsed = html.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                if (htmlParsed.Length == 3)
                {
                    string title = htmlParsed[0];
                    title = title.Replace("<strong>", "");
                    title = title.Replace("</strong>", "");
                    title = title.Replace("<br />", "");

                    string body = htmlParsed[1];
                    body = body.Replace("<p>", "");
                    body = body.Replace("</p>", "");
                    body = body.Replace("<hr />", "");

                    string footer = htmlParsed[2];
                    footer = footer.Replace("<i>", "");
                    footer = footer.Replace("</i>", "");

                    htmlParsed[0] = title;
                    htmlParsed[1] = body;
                    htmlParsed[2] = footer;

                }
                else
                {
                    htmlParsed = new string[3] { "", "", "" };
                }
            }
            catch (Exception ex)
            {
                htmlParsed = new string[3] { "", "", "" };
                ex.ToString();
            }

            return htmlParsed;
        }
    }
}