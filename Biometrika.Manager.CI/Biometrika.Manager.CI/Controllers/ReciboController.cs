﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.NV;
using BCR.Bio.Domain.NV.DTO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using WebNV.Models;

namespace WebNV.Controllers
{
    [Authorize]
    public class ReciboController : BaseController
    {
        public ActionResult Index(int? id, string ReciboIdSearch, int? AppSearch, string FechaSearch)
        {
            ReciboDTO param = new ReciboDTO
            {
                //param.id = id.HasValue ? id.Value : 0;
                ReciboIdSearch = ReciboIdSearch,
                AppSearch = AppSearch,
                FechaSearch = FechaSearch,
                paginacion = new Paginacion()
                {
                    Cantidad = 10,
                    Pagina = id ?? 0
                },

                Company = (Company)CurrentUserCompany
            };

            List<ReciboResultDTO> list = UoW.ReciboService.ListByCriteria(param);

            ViewBag.Paginacion = param.paginacion;


            return View(list);
        }

        [HttpPost]
        public ActionResult Document(int id)
        {
            Recibo recibo = UoW.ReciboService.GetById(id);
            recibo.Aplicacion = UoW.AplicacionService.GetById(recibo.Aplicacion.Id);

            //XDocument document = XDocument.Parse(recibo.ReciboXml);

            //var nodo = document.Root
            //      .Elements("ExtensionItem")

            //      .Select(x => new Student
            //      {
            //          Name = (string)x.Attribute("name"),
            //          Class = (string)x.Attribute("class")
            //      })
            //      .ToList();

            return View(recibo);
        }

        public ActionResult DocumentDetailed(int id)
        {
            ReceiptModel receiptModel = ReadXML(id);
            return View(receiptModel);
        }

        [HttpPost]
        public ActionResult ReceiptDetail(int id)
        {
            ExtensionsXMLModel model = ReadExtensionsXML(id);

            return View(model);
        }

        public FileResult DownloadRecibo(int id)
        {
            Recibo recibo = UoW.ReciboService.GetById(id);

            if (recibo != null)
            {
                var stream = new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(recibo.ReciboXml));
                return File(stream, "application/octet-stream", "Recibo_" + id + ".xml");
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public FileResult DownloadPDF(int id)
        {
            ReceiptModel receiptModel = ReadXML(id);

            if (!string.IsNullOrEmpty(receiptModel.PDFCICertify))
            {
                var stream = Convert.FromBase64String(receiptModel.PDFCICertify);
                return File(stream, "application/pdf", "Recibo_" + id + ".pdf");
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Lee las extensions del XML contenido en un Recibo y las devuelve en un Modelo con todos sus atributos
        /// </summary>
        /// <param name="id">ID del Recibo / ReceiptId</param>
        /// <returns></returns>
        public ReceiptModel ReadXML(int id)
        {

            Recibo recibo = UoW.ReciboService.GetById(id);
            XmlDocument document = new XmlDocument();
            ReceiptModel receiptModel = new ReceiptModel
            {
                Id = id,
                Ip = recibo.IPorigen
            };
            try
            {
                document.LoadXml(recibo.ReciboXml);
                XmlNodeList nodeList = document.SelectNodes("RDEnvelope/Rd/Extensiones");
                XmlNode root = nodeList[0];

                foreach (XmlNode xnode in root.ChildNodes)
                {
                    string extensionKey = "";
                    string extensionValue = "";
                    try
                    {
                        extensionKey = xnode.SelectSingleNode("key").InnerText;
                        extensionValue = xnode.SelectSingleNode("value").InnerText;
                    }
                    catch (Exception e)
                    {
                        extensionKey = "";
                        extensionValue = "";
                    }


                    switch (extensionKey)
                    {
                        case "TrackIdCI":
                            receiptModel.TrackIdCI = extensionValue;
                            break;
                        case "IDCardImage":
                            receiptModel.IDCardImage = extensionValue;
                            break;
                        case "IDCardPhotoImage":
                            receiptModel.IDCardPhotoImage = extensionValue;
                            break;
                        case "IDCardSignatureImage":
                            receiptModel.IDCardSignatureImage = extensionValue;
                            break;
                        case "Selfie":
                            receiptModel.Selfie = extensionValue;
                            break;
                        case "TypeId":
                            receiptModel.TypeId = extensionValue;
                            break;
                        case "ValueId":
                            receiptModel.ValueId = extensionValue;
                            break;
                        case "Name":
                            receiptModel.Name = extensionValue;
                            break;
                        case "BirthDate":
                            receiptModel.BirthDate = ParseDate(extensionValue);
                            break;
                        case "IssueDate":
                            receiptModel.IssueDate = ParseDate(extensionValue);
                            break;
                        case "Sex":
                            receiptModel.Sex = extensionValue;
                            break;
                        case "Nacionality":
                            receiptModel.Nacionality = extensionValue;
                            break;
                        case "Serial":
                            receiptModel.Serial = extensionValue;
                            break;
                        case "ExpirationDate":
                            receiptModel.ExpirationDate = ParseDate(extensionValue);
                            break;
                        case "GeoRef":
                            receiptModel.GeoRef = extensionValue;
                            break;
                        case "WorkStationID":
                            receiptModel.WorkStationID = extensionValue;
                            break;
                        case "ValidityType":
                            receiptModel.ValidityType = extensionValue;
                            break;
                        case "PDFCICertify":
                            receiptModel.PDFCICertify = extensionValue;
                            break;
                        case "QRCIVerify":
                            receiptModel.QRCIVerify = extensionValue;
                            break;
                        case "Score":
                            extensionValue = extensionValue.Trim(new Char[] { ' ', '%' });
                            receiptModel.Score = Double.Parse(extensionValue, CultureInfo.InvariantCulture);
                            break;
                        case "Pais":
                            receiptModel.Country = extensionValue;
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }

            return receiptModel;
        }

        public ExtensionsXMLModel ReadExtensionsXML(int id)
        {
            Dictionary<string, string> xmlExtensions = new Dictionary<string, string>();
            ExtensionsXMLModel extensionsXMLModel = new ExtensionsXMLModel
            {
                Extensions = xmlExtensions,
                ReceiptId = id
            };
            Recibo recibo = UoW.ReciboService.GetById(id);
            XmlDocument document = new XmlDocument();

            try
            {
                document.LoadXml(recibo.ReciboXml);
                XmlNodeList nodeList = document.SelectNodes("RDEnvelope/Rd/Extensiones");
                XmlNode root = nodeList[0];

                foreach (XmlNode xnode in root.ChildNodes)
                {
                    string extensionKey = "";
                    string extensionValue = "";
                    try
                    {
                        extensionKey = xnode.SelectSingleNode("key").InnerText;
                        extensionValue = xnode.SelectSingleNode("value").InnerText;
                    }
                    catch (Exception e)
                    {
                        e.ToString();
                        extensionKey = "";
                        extensionValue = "";
                    }

                    if (!string.IsNullOrEmpty(extensionKey) && !string.IsNullOrEmpty(extensionValue))
                    {
                        xmlExtensions.Add(extensionKey, extensionValue);
                        if (extensionKey.Contains("PDFCICertify")) extensionsXMLModel.HasPDF = true;
                    }
                       
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }


            return extensionsXMLModel;
        }

        DateTime ParseDate(String date)
        {
            try
            {
                return DateTime.ParseExact(date, "d/M/yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                try
                {
                    return DateTime.ParseExact(date, "M/d/yyyy", CultureInfo.InvariantCulture);
                }
                catch (Exception ex)
                {
                    return DateTime.MinValue;
                }

            }
        }


        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="ApellidoToSearch"></param>
        ///// <param name="NroRUTToSearch"></param>
        ///// <returns></returns>
        //internal IList<Recibo> GetList(int? id, string ApellidoToSearch, string NroRUTToSearch)
        //{
        //    return null;
        //    //if (!string.IsNullOrEmpty( ApellidoToSearch))
        //    //    ApellidoToSearch = ApellidoToSearch.Trim();

        //    //if (!string.IsNullOrEmpty(NroRUTToSearch))
        //    //    NroRUTToSearch = NroRUTToSearch.Trim();

        //    //Paginacion paginacion = new Paginacion() { Cantidad = 10, Pagina = id ?? 0 };

        //    //List<Recibo> list = null;

        //    //TransactionSearch transactionSearch = new TransactionSearch();
        //    //transactionSearch.PatherLastName = ApellidoToSearch ?? "";
        //    //transactionSearch.Rut = NroRUTToSearch ?? "";

        //    //if (User.IsInRole("Super Admin"))
        //    //{
        //    //    transactionSearch.Company = null;
        //    //    transactionSearch.UserId = "";
        //    //}

        //    //if (User.IsInRole("Administrador"))
        //    //{
        //    //    transactionSearch.Company = CurrentUserCompany.Id;
        //    //    transactionSearch.UserId = "";
        //    //}

        //    //if (User.IsInRole("Auditor"))
        //    //{
        //    //    transactionSearch.Company = CurrentUserCompany.Id;
        //    //    transactionSearch.UserId = "";
        //    //}

        //    //if (User.IsInRole("Supervisor"))
        //    //{
        //    //    transactionSearch.Company = CurrentUserCompany.Id;
        //    //    transactionSearch.UserId = "";
        //    //}

        //    //if (User.IsInRole("Operador"))
        //    //{
        //    //    transactionSearch.Company = CurrentUserCompany.Id;
        //    //    transactionSearch.UserId = User.Identity.Name;
        //    //}

        //    //list = UoW.TransactionService.ListByApellidoOrRut(paginacion, transactionSearch);

        //    //ViewBag.Paginacion = paginacion;
        //    //ViewBag.ApellidoToSearch = ApellidoToSearch;
        //    //ViewBag.NroRUTToSearch = NroRUTToSearch;

        //    //return list;
        //}

        //// GET: Transaction
        //public ActionResult Index(int? id, string ApellidoToSearch, string NroRUTToSearch)
        //{
        //    return View(GetList(id, ApellidoToSearch, NroRUTToSearch));
        //}

        //[HttpPost]
        //public ActionResult Index(FormCollection fc, int? id, string ApellidoToSearch, string NroRUTToSearch)
        //{
        //    return View(GetList(id, ApellidoToSearch, NroRUTToSearch));
        //}
    }
}