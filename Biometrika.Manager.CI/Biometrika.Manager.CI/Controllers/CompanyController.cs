﻿using AutoMapper;
using BCR.Bio.Domain;
using BCR.Bio.Domain.NV;
using BCR.Bio.Domain.Interface;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebNV.Models.Admin;

namespace WebNV.Controllers
{
    /// <summary>
    ///
    /// </summary>
    [Authorize(Roles = ("Super Admin,Administrador"))]
    public class CompanyController : BaseController
    {
        //
        // GET: /Company/
        [Authorize(Roles = ("Super Admin"))]
        public ActionResult Index(string RutToSearch, string RazonSocialToSearch, int? EstadoToSearch)
        {

            if (!string.IsNullOrEmpty(RazonSocialToSearch))
                RazonSocialToSearch = RazonSocialToSearch.Trim();
            if (!string.IsNullOrEmpty(RutToSearch))
                RutToSearch = RutToSearch.Trim();

            //Paginacion paginacion = new Paginacion() { Cantidad = 10, Pagina = id ?? 0 };
            List<BpCompanyModel> bpCompanyModelList = new List<BpCompanyModel>();

            if (CurrentUser.Companies.Count > 0)
            {
                int holdingId = UoW.GetHolding(User.Identity.Name);

                IList<ICompany> companylist;
                if (EstadoToSearch == null)
                    companylist = UoW.CompanyService.GetByRutAndName(RutToSearch ?? "", RazonSocialToSearch ?? "", holdingId);
                else
                    companylist = UoW.CompanyService.GetByRutAndStatusAndName(RutToSearch ?? "", (int)EstadoToSearch, RazonSocialToSearch ?? "", holdingId);


                companylist
                    .ToList()
                    .ForEach(m => bpCompanyModelList.Add(Transform(m)));
            }
            return View(bpCompanyModelList);
        }

        private BpCompanyModel Transform(ICompany m)
        {
            BpCompanyModel cm = new BpCompanyModel();
            cm.Id = m.Id;
            cm.Rut = m.Rut;
            cm.Address = m.Address;
            cm.Name = m.Name;
            cm.Domain = m.Domain;
            cm.Phone = m.Phone;
            cm.CreateDate = m.CreateDate;
            cm.Status = m.Status;
            cm.EndDate = m.EndDate;

            return cm;
        }


        [Authorize(Roles = ("Administrador"))]
        public ActionResult Info()
        {
            BpCompanyModel bpCompanyModel = Mapper.Map<Company, BpCompanyModel>((Company)UoW.CompanyService.GetById(CurrentUserCompany.Id, withHQs: true, withSectors: true));
            return View(bpCompanyModel);
        }

        /// <summary>
        /// Edita la comapñia, get
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = ("Super Admin"))]
        public ActionResult Edit(int id)
        {
            BpCompanyModel bpCompanyModel = Mapper.Map<Company, BpCompanyModel>((Company)UoW.CompanyService.GetById(id, withHQs: true, withSectors: true));
            bpCompanyModel.Status = bpCompanyModel.EndDate == null;
            bpCompanyModel.HoldingList = Helpers.Web.HoldingList(UoW.HoldingService.List(), bpCompanyModel.Holding.ToString());


            return View(bpCompanyModel);
        }

        /// <summary>
        /// Edita la compañia , post
        /// </summary>
        /// <param name="bpCompanyModel"></param>
        /// <returns></returns>
        [Authorize(Roles = ("Super Admin"))]
        [HttpPost]
        public ActionResult Edit(BpCompanyModel bpCompanyModel)
        {
            try
            {
                ICompany company = UoW.CompanyService.GetById(bpCompanyModel.Id);
                company.UpdateDate = DateTime.Now;
                company.Address = bpCompanyModel.Address;
                company.ContactName = bpCompanyModel.ContactName;
                company.Domain = bpCompanyModel.Domain;
                company.Fax = bpCompanyModel.Fax;
                //company.Holding = bpCompanyModel.Holding;
                company.Name = bpCompanyModel.Name;
                company.Phone = bpCompanyModel.Phone;
                company.Phone2 = bpCompanyModel.Phone2;
                company.Rut = bpCompanyModel.Rut;
                if (company.Status != bpCompanyModel.Status)
                {
                    if (!bpCompanyModel.Status)
                    {
                        company.EndDate = DateTime.Now;
                        company.Status = false;
                    }

                    else
                    {
                        company.EndDate = null;
                        company.Status = true;
                    }
                }


                Company bpCompany = (Company)UoW.CompanyService.Update(company);


                if (!ModelState.IsValid)
                {
                    bpCompanyModel.HoldingList = Helpers.Web.HoldingList(UoW.HoldingService.List(), bpCompanyModel.Holding.ToString());
                    return View(bpCompanyModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al actualizar una empresa", e);
                bpCompanyModel.HoldingList = Helpers.Web.HoldingList(UoW.HoldingService.List(), bpCompanyModel.Holding.ToString());
                return View(bpCompanyModel);
            }
        }

        /// <summary>
        /// Crea la compañia , Get
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = ("Super Admin"))]
        public ActionResult Create()
        {
            BpCompanyModel bpCompanyModel = new BpCompanyModel();
            bpCompanyModel.HoldingList = Helpers.Web.HoldingList(UoW.HoldingService.List(), bpCompanyModel.Holding.ToString());
            bpCompanyModel.Status = true;
            return View(bpCompanyModel);
        }

        /// <summary>
        /// Crea la compañia , post
        /// </summary>
        /// <param name="bpCompanyModel"></param>
        /// <returns></returns>
        [Authorize(Roles = ("Super Admin"))]
        [HttpPost]
        public ActionResult Create(BpCompanyModel bpCompanyModel)
        {

            try
            {
                Company bpCompany = UoW.CreateCompany(Mapper.Map<BpCompanyModel, Company>(bpCompanyModel));


                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", "Error interno ");
                    return View(bpCompanyModel);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = ("Super Admin"))]
        public ActionResult Disable(int id)
        {
            UoW.CompanyService.Disable(id);
            return RedirectToAction("Index");
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = ("Super Admin"))]
        public ActionResult Enable(int id)
        {
            UoW.CompanyService.Enable(id);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = ("Super Admin"))]
        public ActionResult DisableHQ(int id, string back)
        {
            UoW.HQService.Disable(id);
            return Redirect(back);
        }

        [Authorize(Roles = ("Super Admin"))]
        public ActionResult EnableHQ(int id, string back)
        {
            UoW.HQService.Enable(id);
            return Redirect(back);
        }

        [Authorize(Roles = ("Super Admin"))]
        public ActionResult DisableSector(int id, string back)
        {
            UoW.SectorService.Disable(id);
            return Redirect(back);
        }
        [Authorize(Roles = ("Super Admin"))]
        public ActionResult EnableSector(int id, string back)
        {
            UoW.SectorService.Enable(id);
            return Redirect(back);
        }

        /// <summary>
        /// Elimina la compañia, GET de la page
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = ("Super Admin"))]
        public ActionResult Delete(int id)
        {
            return View(Mapper.Map<Company, BpCompanyModel>((Company)UoW.CompanyService.GetById(id)));
        }

        /// <summary>
        /// Elimina la ocmpañia, post
        /// </summary>
        /// <param name="bpCompanyModel"></param>
        /// <returns></returns>
        [Authorize(Roles = ("Super Admin"))]
        [HttpPost]
        public ActionResult Delete(BpCompanyModel bpCompanyModel)
        {
            try
            {
                /// Limpia el Model State de validaciones innecesarias
                ModelState.Clear();

                UoW.CompanyService.Delete<Company>(bpCompanyModel.Id);

                if (!ModelState.IsValid)
                    return View(bpCompanyModel);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        #region HQ

        /// <summary>
        ///
        /// </summary>
        /// <param name="company"></param>
        /// <param name="back"></param>
        /// <returns></returns>
        public ActionResult AddHQ(int company, string back)
        {
            //UoW.TownService.List()

            ICompany companyObj = UoW.CompanyService.GetById(company);
            HQModel HQModel = new HQModel();
            HQModel.CompanyId = companyObj.Id;
            HQModel.CompanyName = companyObj.Name;
            HQModel.Countries = UoW.CountryService.List().ToList();
            HQModel.Back = back;
            return View(HQModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="HQModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddHQ(HQModel HQModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICompany companyObj = UoW.CompanyService.GetById(HQModel.CompanyId);
                    HQModel.Company = companyObj;

                    HQ hq = Mapper.Map<HQModel, HQ>((HQModel)HQModel);
                    hq.Town = UoW.TownService.GetById(HQModel.TownId);
                    int idhq = UoW.HQService.Create(hq);
                }
                return RedirectToAction("Edit", new { id = HQModel.CompanyId });
            }
            catch (Exception e)
            {
            }
            return View(HQModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="company"></param>
        /// <param name="hqId"></param>
        /// <param name="back"></param>
        /// <returns></returns>
        public ActionResult EditHQ(int company, int hqId, string back)
        {
            IHQ hq = UoW.HQService.GetById(hqId);
            ICompany companyObj = UoW.CompanyService.GetById(company);

            HQModel HQModel = new HQModel();

            HQModel.Description = hq.Description;
            HQModel.Address = hq.Address;
            HQModel.Contact = hq.Contact;
            HQModel.Phone = hq.Phone;
            HQModel.TownId = hq.Town == null ? 0 : hq.Town.Id;
            HQModel.Id = hq.Id;
            HQModel.Countries = UoW.CountryService.List().ToList();
            HQModel.Company = companyObj;
            HQModel.CompanyId = companyObj.Id;
            HQModel.CompanyName = companyObj.Name;
            HQModel.Town = hq.Town;
            HQModel.TownId = hq.Town == null ? -1 : hq.Town.Id;
            HQModel.Back = back;
            return View(HQModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="HQModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditHQ(HQModel HQModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Company companyObj = (Company)UoW.CompanyService.GetById(HQModel.CompanyId);
                    IHQ hq = UoW.HQService.GetById(HQModel.Id);

                    hq.Company = companyObj;

                    hq.Description = HQModel.Description;
                    hq.Address = HQModel.Address;
                    hq.Contact = HQModel.Contact;
                    hq.Phone = HQModel.Phone;
                    hq.Town = UoW.TownService.GetById(HQModel.TownId);
                    hq = UoW.HQService.Update(hq);
                }
                return RedirectToAction("Edit", new { id = HQModel.CompanyId });
            }
            catch (Exception e)
            {
            }
            return View(HQModel);
        }

        #endregion HQ

        #region Sectores

        /// <summary>
        ///
        /// </summary>
        /// <param name="company"></param>
        /// <param name="back"></param>
        /// <returns></returns>
        public ActionResult AddSector(int company, string back)
        {
            ICompany companyObj = UoW.CompanyService.GetById(company);
            SectorModel sectorModel = new SectorModel();
            sectorModel.CompanyId = companyObj.Id;
            sectorModel.CompanyName = companyObj.Name;

            sectorModel.Back = back;
            return View(sectorModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sectorModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddSector(SectorModel sectorModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Company companyObj = (Company)UoW.CompanyService.GetById(sectorModel.CompanyId);
                    sectorModel.Companies = new List<Company>();
                    sectorModel.Companies.Add(companyObj);
                    int idhq = UoW.SectorService.Create(Mapper.Map<SectorModel, Sector>((SectorModel)sectorModel));
                }
                return RedirectToAction("Edit", new { id = sectorModel.CompanyId });
            }
            catch (Exception e)
            {
            }
            return View(sectorModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="company"></param>
        /// <param name="sectorId"></param>
        /// <param name="back"></param>
        /// <returns></returns>
        public ActionResult EditSector(int company, int sectorId, string back)
        {
            ISector sector = UoW.SectorService.GetById(sectorId);
            ICompany companyObj = UoW.CompanyService.GetById(company);
            SectorModel sectorModel = new SectorModel();
            sectorModel.CompanyId = companyObj.Id;
            sectorModel.CompanyName = companyObj.Name;

            sectorModel.Id = sector.Id;
            sectorModel.Description = sector.Description;
            sectorModel.externalid = sector.externalid;
            sectorModel.ContactName = sector.ContactName;
            sectorModel.Back = back;
            return View(sectorModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sectorModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditSector(SectorModel sectorModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Company companyObj = (Company)UoW.CompanyService.GetById(sectorModel.CompanyId);
                    ISector sector = UoW.SectorService.GetById(sectorModel.Id);

                    if (sector.Companies.Count(m => m.Id == companyObj.Id) == 0)
                        sector.Companies.Add(companyObj);

                    sector.Id = sectorModel.Id;
                    sector.Description = sectorModel.Description;
                    sector.externalid = sectorModel.externalid;
                    sector.ContactName = sectorModel.ContactName;
                    sector = UoW.SectorService.Update(sector);
                }
                return RedirectToAction("Edit", new { id = sectorModel.CompanyId });
            }
            catch (Exception e)
            {
            }
            return View(sectorModel);
        }

        #endregion Sectores
    }
}