﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.NV;
using BCR.Bio.Domain.NV.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebNV.Models.NV;
using BCR.System.Log;
using System.Net.Http.Headers;

namespace WebNV.Controllers
{
    [Authorize]
    public class CertificationController : BaseController
    {

        public ActionResult Find(CITxModel cITxModel, int idPaginacion = 0, bool isCertification = true)
        {
            if (cITxModel.FilterParams is null)
            {
                cITxModel.FilterParams = new CITxDTO
                {
                    ReceiptId = 0,
                    TypeId = BCR.Bio.Domain.NV.DTO.TypeId.RUT,
                    ValueId = "",
                    TrackId = "",
                    Type = isCertification ? 0 : 1
                };
            }

            Log4Bio.Info("Cargando vista Certification/Find receiptId: " + cITxModel.FilterParams.ReceiptId + " typeId: " + cITxModel.FilterParams.TypeId + " valueId: " + cITxModel.FilterParams.ValueId + " trackId: " + cITxModel.FilterParams.TrackId);
            if (!cITxModel.FilterParams.EventTimeFrom.HasValue || cITxModel.FilterParams.EventTimeFrom == DateTime.MinValue) cITxModel.FilterParams.EventTimeFrom = DateTime.Now.AddDays(-3);
            if (!cITxModel.FilterParams.EventTimeTo.HasValue || cITxModel.FilterParams.EventTimeTo == DateTime.MinValue) cITxModel.FilterParams.EventTimeTo = DateTime.Now;

            int companyId;
            if (User.IsInRole("Super Admin") && cITxModel.CompanyId > 0)
            {
                Log4Bio.Info("Es Super Admin y revisa Company : " + cITxModel.CompanyId);
                companyId = cITxModel.CompanyId;
            }
            else
            {
                Log4Bio.Info("No es Super Admin, se selecciona la Company del usuario: " + CurrentUserCompany.Id);
                companyId = CurrentUserCompany.Id;
            }

            cITxModel.FilterParams.CompanyId = companyId;
            Log4Bio.Info("Buscando resultados desde " + cITxModel.FilterParams.EventTimeFrom + " hasta " + cITxModel.FilterParams.EventTimeTo);
            cITxModel.FilterParams.Paginacion = new Paginacion()
            {
                Cantidad = 10,
                Pagina = idPaginacion
            };

            cITxModel.List = UoW.CITxService.ListDTOByCriteria(cITxModel.FilterParams);

            ViewBag.CompanyList = new List<SelectListItem>();

            if (User.IsInRole("Super Admin"))
            {
                Log4Bio.Info("SuperAdmin: cargando lista de compañías");
                var companies = UoW.CompanyService.List();
                foreach (var icompany in companies)
                {
                    ViewBag.CompanyList.Add(new SelectListItem { Text = icompany.Name, Value = icompany.Id.ToString() });
                }
            }
            if (isCertification)
                return View(cITxModel);
            else
                return View("FindProcedures", cITxModel);
        }

        [HttpGet]
        public ActionResult FindProcedures()
        {
            return Find(new CITxModel(), 0, false);
        }

        [HttpPost]
        public ActionResult Detail(int certId)
        {
            Log4Bio.Info("Cargando vista detalle de Certificación para el ID: " + certId);
            CITx model = UoW.CITxService.GetById(certId);
            model.Score = ParseScore(model.Score);
            return View(model);
        }


        string ParseScore(string scoreString)
        {
            if (!string.IsNullOrEmpty(scoreString))
            {
                scoreString = scoreString.Replace('.', ',');
                double score = double.Parse(scoreString);
                Log4Bio.Info("Score: " + scoreString + " Formateado :" + score.ToString("F") + "%");
                return score.ToString("F") + "%";
            }
            Log4Bio.Info("Score vacío");
            return "";
        }

        [HttpPost]
        public FileResult DownloadPDF(int certId)
        {
            Log4Bio.Info("DownloadPDF con ID: " + certId);
            CITx certification = UoW.CITxService.GetById(certId);

            if (!string.IsNullOrEmpty(certification.CertifyPdf))
            {
                Log4Bio.Info("PDF encontrado se genera descarga.");
                var stream = Convert.FromBase64String(certification.CertifyPdf);
                return File(stream, "application/pdf", "Certificacion_" + certId + ".pdf");
            }
            else
            {
                Log4Bio.Error("PDF no encontrado. certId:" + certId);
                return null;
            }
        }

        [HttpPost]
        public FileResult DownloadRecibo(int receiptIdXml)
        {
            Log4Bio.Info("DownloadRecibo con receiptIdXml: " + receiptIdXml);
            Recibo recibo = UoW.ReciboService.GetById(receiptIdXml);

            if (!string.IsNullOrEmpty(recibo.ReciboXml))
            {
                Log4Bio.Info("'XML encontrado se genera descarga.");
                var stream = new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(recibo.ReciboXml));
                return File(stream, "application/octet-stream", "Recibo_" + receiptIdXml + ".xml");
            }
            else
            {
                Log4Bio.Error("XML no encontrado. receiptIdXml:" + receiptIdXml);
                return null;
            }
        }

        //[Authorize(Roles = ("Super Admin, Administrador, Operador"))]
        //public ActionResult New()
        //{
        //    Log4Bio.Info("Cargando vista New Certification");
        //    CertificationModel certificationModel = new CertificationModel();
        //    Config config = UoW.ConfigService.GetByCompanyId(CurrentUserCompany.Id);
        //    if (config is null)
        //    {
        //        Log4Bio.Info("No se encuentra Config de la compañía en la BBDD. Se procede a crear una nueva.");
        //        Company company = (Company)CurrentUserCompany;
        //        config = new Config(company);
        //        config.Id = UoW.ConfigService.Create(config);
        //    }
        //    config = UoW.ConfigService.GetById(config.Id);
        //    Log4Bio.Info("Cargamos la configuración de la compañía para lanzar la vista");
        //    certificationModel.ReceiverEmail = config.ReceiverEmails;
        //    //certificationModel.OnBoarding = config.ForceOnBoarding;
        //    return View(certificationModel);
        //}


        [Authorize(Roles = ("Super Admin, Administrador, Operador"))]
        public ActionResult New()
        {
            Log4Bio.Info("Cargando vista New Certification");
            CertificationModel certificationModel = new CertificationModel();
            CompanyWorkFlow companyWorkFlow = UoW.CompanyWorkFlowService.GetByCompanyId(CurrentUserCompany.Id);
            if (companyWorkFlow is null)
            {
                Company company = (Company)CurrentUserCompany;
                companyWorkFlow = new CompanyWorkFlow(company);
                companyWorkFlow.Id = UoW.CompanyWorkFlowService.Create(companyWorkFlow);

            }
            companyWorkFlow = UoW.CompanyWorkFlowService.GetById(companyWorkFlow.Id);
            Log4Bio.Info("Cargamos la configuración de la compañía para lanzar la vista");
            certificationModel.ReceiverEmail = companyWorkFlow.ReceiverList;
            return View(certificationModel);
        }



        [HttpPost]
        [Authorize(Roles = ("Super Admin, Administrador, Operador"))]
        public ActionResult New(CertificationModel certificationModel)
        {
            Log4Bio.Info("Post New Certification: " + certificationModel.TypeId + " " + certificationModel.ValueId);
            Log4Bio.Info("ReceiverEmail: " + certificationModel.ReceiverEmail + ", Email: " + certificationModel.Email + ", Phone: " + certificationModel.Phone);
            ResponseCreateTx response = CreateTx(certificationModel);
            if (!string.IsNullOrEmpty(response.Message) && response.Message.Contains("Rut Inválido"))
            {
                Log4Bio.Error("Error al crear Nueva Certificación Rut Inválido :" + certificationModel.ValueId);
                return View(certificationModel);
            }
            return View("Response", response);
        }


        //private ResponseCreateTx CreateTx(CertificationModel certificationModel)
        //{

        //    if (!ValidateRut(certificationModel.ValueId)) return new ResponseCreateTx
        //    {
        //        Message = "Rut Inválido"
        //    };
        //    Config config = UoW.ConfigService.GetByCompanyId(CurrentUserCompany.Id);
        //    int company = int.Parse(ConfigurationManager.AppSettings["Company"]);
        //    string guid = Guid.NewGuid().ToString("N");
        //    int application = int.Parse(ConfigurationManager.AppSettings["Application"]);
        //    string theme = ConfigurationManager.AppSettings["Theme"];
        //    string type = ConfigurationManager.AppSettings["Type"];
        //    int signerInclude = int.Parse(ConfigurationManager.AppSettings["SignerInclude"]);
        //    int checklockSrcei = int.Parse(ConfigurationManager.AppSettings["ChecklockSrcei"]);
        //    string[] listMailsDestinatary;
        //    string currentAction = "ciweb/ciCreateTx";
        //    string callbackUrl = ConfigurationManager.AppSettings["CallbackUrl"];
        //    string redirectUrl = ConfigurationManager.AppSettings["Redirecturl"];
        //    string uri = ConfigurationManager.AppSettings["ApiUrl"] + currentAction;

        //    try
        //    {
        //        listMailsDestinatary = certificationModel.ReceiverEmail.Split(',');
        //    }
        //    catch (Exception ex)
        //    {
        //        listMailsDestinatary = certificationModel.ReceiverEmail.Split(';');
        //        ex.ToString();
        //    }



        //    Certification certification = new Certification
        //    {
        //        Company = company,
        //        Application = application,
        //        TypeId = certificationModel.TypeId.ToString(),
        //        ValueId = certificationModel.ValueId,
        //        MailCertify = certificationModel.Email,
        //        Cellphone = certificationModel.Phone.ToString(),
        //        Theme = theme,
        //        CustomerTrackId = guid,
        //        Callbackurl = callbackUrl,
        //        Redirecturl = redirectUrl,
        //        Type = type,
        //        SignerInclude = signerInclude,
        //        ChecklockSrcei = checklockSrcei,
        //        Threshold = config.Score,
        //        ListMailsDestinatary = listMailsDestinatary
        //    };

        //    // Serialize our concrete class into a JSON String
        //    string stringCertification = JsonConvert.SerializeObject(certification);
        //    Log4Bio.Info("JSON request: " + stringCertification);
        //    // Wrap our JSON inside a StringContent which then can be used by the HttpClient class
        //    StringContent httpContent = new StringContent(stringCertification, Encoding.UTF8, "application/json");

        //    using (var httpClient = new HttpClient())
        //    {

        //        Log4Bio.Info("Iniciamos solicitud a la api: " + uri);
        //        Log4Bio.Info("Esperando respuesta...");
        //        var httpResponse = httpClient.PostAsync(uri, httpContent);

        //        var settingsJson = new JsonSerializerSettings
        //        {
        //            NullValueHandling = NullValueHandling.Ignore,
        //            MissingMemberHandling = MissingMemberHandling.Ignore
        //        };


        //        if (httpResponse.Result != null && httpResponse.Result.StatusCode.ToString().Equals("OK"))
        //        {
        //            Log4Bio.Info("Respuesta obtenida: 200 OK");
        //            Task<string> responseContent = httpResponse.Result.Content.ReadAsStringAsync();
        //            ResponseCreateTx responseDeserialized;
        //            try
        //            {
        //                Log4Bio.Info("Deserializamos respuesta :" + responseContent.Result.ToString());
        //                responseDeserialized = JsonConvert.DeserializeObject<ResponseCreateTx>(responseContent.Result, settingsJson);
        //                return responseDeserialized;
        //            }
        //            catch (Exception ex)
        //            {
        //                Log4Bio.Error("Error al deserializar respuesta de la api: " + ex.ToString());
        //                ex.ToString();
        //                return new ResponseCreateTx();
        //            }
        //        }
        //        else
        //        {
        //            // Error al enviar solicitud 
        //            Log4Bio.Error("Error en respuesta obtenida de la API: " + httpResponse.Result.StatusCode);
        //            Log4Bio.Error(httpResponse.Result.StatusCode.ToString());
        //            Log4Bio.Error(httpResponse.Result.ToString());
        //            return new ResponseCreateTx();
        //        }
        //    }

        //}

        private ResponseCreateTx CreateTx(CertificationModel certificationModel)
        {
            certificationModel.ValueId = certificationModel.ValueId.ToUpper();
            if (!ValidateRut(certificationModel.ValueId)) return new ResponseCreateTx
            {
                Message = "Rut Inválido"
            };
            CompanyWorkFlow companyWorkFlow = UoW.CompanyWorkFlowService.GetByCompanyId(CurrentUserCompany.Id);
            int company = CurrentUserCompany.Id;
            string guid = Guid.NewGuid().ToString("N");
            int application = int.Parse(ConfigurationManager.AppSettings["Application"]);
            string theme = companyWorkFlow.Theme;
            string[] listMailsDestinatary;

            string currentAction = "ciweb/ciCreateTx";
            string callbackUrl = ConfigurationManager.AppSettings["CallbackUrl"];
            //string redirectUrl = ConfigurationManager.AppSettings["Redirecturl"];
            string uri = ConfigurationManager.AppSettings["ApiUrl"] + currentAction;

            try
            {
                listMailsDestinatary = certificationModel.ReceiverEmail.Split(',');
            }
            catch (Exception ex)
            {
                listMailsDestinatary = certificationModel.ReceiverEmail.Split(';');
                ex.ToString();
            }



            Certification certification = new Certification
            {
                Company = company,
                Application = application,
                TypeId = certificationModel.TypeId.ToString(),
                ValueId = certificationModel.ValueId,
                MailCertify = certificationModel.Email,
                Cellphone = certificationModel.Phone.ToString(),
                Theme = theme,
                CustomerTrackId = guid,
                Callbackurl = callbackUrl,
                Redirecturl = companyWorkFlow.RedirectUrl,
                WorkFlowName = companyWorkFlow.Name,
                ListMailsDestinatary = listMailsDestinatary
            };

            // Serialize our concrete class into a JSON String
            string stringCertification = JsonConvert.SerializeObject(certification);
            Log4Bio.Info("JSON request: " + stringCertification);
            // Wrap our JSON inside a StringContent which then can be used by the HttpClient class
            StringContent httpContent = new StringContent(stringCertification, Encoding.UTF8, "application/json");

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GenerateAuthHeaders());

                Log4Bio.Info("Iniciamos solicitud a la api: " + uri);
                Log4Bio.Info("Esperando respuesta...");
                var httpResponse = httpClient.PostAsync(uri, httpContent);

                var settingsJson = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };


                if (httpResponse.Result != null && httpResponse.Result.StatusCode.ToString().Equals("OK"))
                {
                    Log4Bio.Info("Respuesta obtenida: 200 OK");
                    Task<string> responseContent = httpResponse.Result.Content.ReadAsStringAsync();
                    ResponseCreateTx responseDeserialized;
                    try
                    {
                        Log4Bio.Info("Deserializamos respuesta :" + responseContent.Result.ToString());
                        responseDeserialized = JsonConvert.DeserializeObject<ResponseCreateTx>(responseContent.Result, settingsJson);
                        return responseDeserialized;
                    }
                    catch (Exception ex)
                    {
                        Log4Bio.Error("Error al deserializar respuesta de la api: " + ex.ToString());
                        ex.ToString();
                        return new ResponseCreateTx();
                    }
                }
                else
                {
                    // Error al enviar solicitud 
                    Log4Bio.Error("Error en respuesta obtenida de la API: " + httpResponse.Result.StatusCode);
                    Log4Bio.Error(httpResponse.Result.StatusCode.ToString());
                    Log4Bio.Error(httpResponse.Result.ToString());
                    return new ResponseCreateTx();
                }
            }

        }

        [HttpPost]
        public ActionResult ReSendTx(string trackId)
        {
            int companyId = CurrentUserCompany.Id;
            string currentAction = "ciweb/ciReSendTx";
            string uri = ConfigurationManager.AppSettings["ApiUrl"] + currentAction;
            ReSendTx reSendTx = new ReSendTx
            {
                Company = companyId,
                TrackId = trackId
            };
            Log4Bio.Info("Post ReSendTx trackId: " + trackId + " companyId :" + companyId);
            string stringReSendTx = JsonConvert.SerializeObject(reSendTx);

            Log4Bio.Info("JSON request: " + stringReSendTx);

            StringContent httpContent = new StringContent(stringReSendTx, Encoding.UTF8, "application/json");

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GenerateAuthHeaders());

                Log4Bio.Info("Iniciamos solicitud a la api: " + uri);
                Log4Bio.Info("Esperando respuesta...");
                var httpResponse = httpClient.PostAsync(uri, httpContent);


                if (httpResponse.Result.StatusCode.ToString().Equals("OK"))
                {
                    Log4Bio.Info("Respuesta obtenida: 200 OK");
                    return Json(new { message = "Solicitud de certificación reenviada exitosamente!" });
                }
                else
                {
                    Log4Bio.Error("Error en respuesta obtenida de la API: " + httpResponse.Result.StatusCode);
                    Log4Bio.Error(httpResponse.Result.StatusCode.ToString());
                    Log4Bio.Error(httpResponse.Result.ToString());
                    return Json(new { message = "Hubo un error al re-enviar certificación. Favor intentar nuevamente" });
                }
            }

        }

        [HttpPost]
        public ActionResult TxGetStatus(string trackId, bool isCertification = true)
        {
            string apiUrl = ConfigurationManager.AppSettings["ApiUrl"];
            string currentAction;
            if (isCertification)
                currentAction = "ciweb/CIStatusTx";
            else
                currentAction = "v1/signer/txStatus";

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GenerateAuthHeaders());

                string uri = apiUrl + currentAction + "?trackid=" + trackId;
                Log4Bio.Info("Iniciamos solicitud a la api: " + uri);
                Log4Bio.Info("Esperando respuesta...");
                var httpResponse = httpClient.GetAsync(uri);
                if (httpResponse.Result.StatusCode.ToString().Equals("OK"))
                {
                    Log4Bio.Info("Respuesta obtenida: 200 OK");
                    Task<string> responseContent = httpResponse.Result.Content.ReadAsStringAsync();
                    Dictionary<string, string> jsonresp = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseContent.Result);
                    jsonresp.TryGetValue("status", out string status);
                    return Json(new { message = "Estado consultado: " + status });
                }
                else
                {
                    Log4Bio.Error("Error en respuesta obtenida de la API: " + httpResponse.Result.StatusCode);
                    Log4Bio.Error(httpResponse.Result.StatusCode.ToString());
                    Log4Bio.Error(httpResponse.Result.ToString());
                    return Json(new { message = "Hubo un error al re-enviar certificación. Favor intentar nuevamente" });
                }
            }

        }


        bool ValidateRut(string rut)
        {

            bool validacion = false;
            try
            {
                rut = rut.ToUpper();
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                if (rut.Length > 9 || rut.Length < 8) return false;
                int rutAux = int.Parse(rut.Substring(0, rut.Length - 1));

                char dv = char.Parse(rut.Substring(rut.Length - 1, 1));

                int m = 0, s = 1;
                for (; rutAux != 0; rutAux /= 10)
                {
                    s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                }
                if (dv == (char)(s != 0 ? s + 47 : 75))
                {
                    validacion = true;
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("ValidateRut: Error al intentar validar rut: " + rut + " // " + ex.ToString());
                ex.ToString();
            }
            return validacion;
        }

        private string GenerateAuthHeaders()
        {
            return Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(CurrentUserCompany.AccessName + ":" + CurrentUserCompany.SecretKey));
        }




    }
}