﻿using AutoMapper;
using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.System.Crypto;
using BCR.System.Log;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using WebNV.Helpers;
using WebNV.Models.Admin;

namespace WebNV.Controllers
{
    /// <summary>
    ///
    /// </summary>
    [Authorize]
    public class UserController : BaseController
    {
        /// <summary>
        /// Lista de usuarios para administradores
        /// </summary>
        /// <param name="userToSearch"></param>
        /// <param name="companyToSearch"></param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador")]
        public ActionResult Index(string userToSearch, int? companyToSearch)
        {
            UserSearchModel userSearchModel = new UserSearchModel();

            userSearchModel.UserModelList = IndexLocal(userToSearch, CurrentUserCompany.Id);
            userSearchModel.CompanyList = null;                                     // no se asigna compañia para seleccionar

            return View(userSearchModel);
        }

        /// <summary>
        /// Indice de usuarios para Super admins
        /// </summary>
        /// <param name="userToSearch"></param>
        /// <param name="companyToSearch"></param>
        /// <returns></returns>
        [Authorize(Roles = "Super Admin")]
        public ActionResult IndexSA(string userToSearch, int? companyToSearch)
        {
            UserSearchModel userSearchModel = new UserSearchModel();
            userSearchModel.UserModelList = IndexLocal(userToSearch, companyToSearch);

            List<SelectListItem> companyList = new List<SelectListItem>();
            companyList.Add(new SelectListItem() { Value = "-1", Text = "Todas", Selected = true });
            companyList.AddRange(Web.CompanyList(UoW.CompanyService.List()));

            userSearchModel.CompanyList = companyList;

            return View(userSearchModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int id)
        {
            User userToEdit = (User)UoW.UserService.GetById(id);                              // recuperar el usuario del sistema que se quiere editar

            UserEditModel userEditModel = Mapper.Map<User, UserEditModel>(userToEdit);      // armar el model y asignar las compañias posibles
            userEditModel.CompanyValue = userToEdit.Company.Id;                           // setear el ID compañia por que le corresponde

            //string[] userToEditRoles = Roles.GetRolesForUser(userEditModel.Username);       //Recuperar los roles del usuario, -------- solo sera 1
            //if (userToEditRoles.Length > 0)
            userEditModel.Rol = userToEdit.Roles[0].Id.ToString();

            if (userToEdit.Roles[0].Name == "Super Admin")                                // si el editado es super admin, bloqueaar
            {
                Log4Bio.Fatal("Se intenta modificar un usuario super admin sin permisos");
                ModelState.AddModelError("", "Cambio no autorizado");
                return View();
            }

            userEditModel.Password = "";                                                    // blanqear la pass

            userEditModel.RolList = Web.RoleList(UoW.RolService.List());                       // asignar los posibles roles a seleccionar
            userEditModel.CompanyList = null; // Web.CompanyList(UoW.CompanyService.List());                                    // asignar las compañias a seleccionar

            return View(userEditModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Super Admin")]
        public ActionResult EditSA(int id)
        {
            User userToEdit = (User)UoW.UserService.GetById(id);                              // recuperar el usuario del sistema que se quiere editar

            UserEditModel userEditModel = Mapper.Map<User, UserEditModel>(userToEdit);      // armar el model y asignar las compañias posibles
            if (userToEdit.Company != null)
                userEditModel.CompanyValue = userToEdit.Company.Id;                           // setear el ID compañia por que le corresponde
            else
                userEditModel.CompanyValue = -1;

            userEditModel.Rol = userToEdit.Roles[0].Id.ToString();

            userEditModel.Password = "";                                                    // blanqear la pass

            userEditModel.RolList = Web.RoleListSA(UoW.RolService.List());                        // asignar los posibles roles a seleccionar
            userEditModel.CompanyList = Web.CompanyList(UoW.CompanyService.List());                                  // asignar las compañias a seleccionar

            return View(userEditModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(UserEditModel userEditModel)
        {
            if (ModelState.IsValid)
            {
                userEditModel.CompanyValue = CurrentUserCompany.Id;                      // setear la empresa del admin al user
                EditUserLocal(userEditModel);
            }
            else
                ModelState.AddModelError("", "Error interno ");

            userEditModel.RolList = Web.RoleList(UoW.RolService.List());
            userEditModel.CompanyList = null;

            return View(userEditModel);
        }

        /// <summary>
        /// Post del edit para super admin
        /// </summary>
        /// <param name="userEditModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Super Admin")]
        public ActionResult EditSA(UserEditModel userEditModel)
        {
            if (ModelState.IsValid)
            {
                // recuperar el usuario original
                EditUserLocal(userEditModel);
            }
            else
                ModelState.AddModelError("", "Error interno ");

            userEditModel.RolList = Web.RoleListSA(UoW.RolService.List());                        // asignar los posibles roles a seleccionar
            userEditModel.CompanyList = Web.CompanyList(UoW.CompanyService.List());

            return View(userEditModel);
        }

        /// <summary>
        /// Crea la compañia , Get
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrador, Super Admin")]
        public ActionResult Create()
        {
            UserModel userModel = new UserModel();

            userModel.RolList = Web.RoleList(UoW.RolService.List());                                         // roles sin SA
            userModel.CompanyList = null;
            userModel.Domain = CurrentUserCompany.Domain;  // para los adm se toma el domain del admin

            return View(userModel);
        }

        /// <summary>
        /// Crea usuario, es Super admin
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Super Admin")]
        public ActionResult CreateSA()
        {
            UserModel userModel = new UserModel();

            userModel.RolList = Web.RoleList(UoW.RolService.List());   // RoleListSA se remueve para evitar cagadas
            userModel.CompanyList = Web.CompanyList(UoW.CompanyService.List());

            return View(userModel);
        }

        /// <summary>
        /// Crea la compañia , post
        /// </summary>
        /// <param name="bpCompanyModel"></param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Create(UserModel userModel)
        {
            userModel.CompanyValue = CurrentUserCompany.Id;
            userModel.Domain = CurrentUserCompany.Domain;
            if (CreateUserLocal(userModel))
            {
                userModel.IsReadOnly = true;
                userModel.Notification = "Usuario creado correctamente";

            } else
            {
                userModel.Notification = "Error al crear usuario";
            }


            userModel.RolList = Web.RoleList(UoW.RolService.List());                                        // roles sin SA
            userModel.CompanyList = null;

            return View(userModel);
        }

        /// <summary>
        /// Crea la user  , post  es Super admin
        /// </summary>
        /// <param name="bpCompanyModel"></param>
        /// <returns></returns>
        [Authorize(Roles = "Super Admin")]
        [HttpPost]
        public ActionResult CreateSA(UserModel userModel)
        {
            if (userModel.CompanyValue > 0)
                userModel.Domain = UoW.CompanyService.GetById(userModel.CompanyValue).Domain;

            if (CreateUserLocal(userModel))
            {
                userModel.IsReadOnly = true;
                userModel.Notification = "Usuario creado correctamente";

            }
            else
            {
                userModel.Notification = "Error al crear usuario";
            }

            userModel.RolList = Web.RoleListSA(UoW.RolService.List());
            userModel.CompanyList = Web.CompanyList(UoW.CompanyService.List());

            return View(userModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public virtual ActionResult Perfil()
        {
            UserModel userModel = new UserModel();

            userModel.Name = CurrentUser.Username;               // ro
            userModel.CreateDate = CurrentUser.CreateDate;       //ro
            userModel.Email = CurrentUser.Email;
            userModel.LastLoginDate = CurrentUser.LastLoginDate;

            TempData["currentUserToChange"] = CurrentUser.Username;
            return View(userModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult ChangePassword()
        {
            ChangePasswordModel changePasswordModel = new ChangePasswordModel();
            changePasswordModel.UserName = User.Identity.Name;
            return View(changePasswordModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult ChangePassword(ChangePasswordModel changePasswordModel)
        {
            IUser user = (User)UoW.UserService.GetByUsername(changePasswordModel.UserName);

            if (!PasswordProvider.ValidatePassword(changePasswordModel.OldPassword, user.Password))
                ModelState.AddModelError("InvalidOldPassword", "La clave anterior no es correcta");
            else
            {
                if (!string.IsNullOrEmpty(changePasswordModel.NewPassword))
                {
                    string encodedPassword = PasswordProvider.GeneratePasswordHashed(changePasswordModel.NewPassword);

                    if (encodedPassword.Length > 255)
                        ModelState.AddModelError("InvalidNewPassword", "La clave nueva no es válida");

                    user.Password = encodedPassword;

                    user = UoW.UserService.Update(user);
                }
            }
            //changePasswordModel.Notification = "Se cambió la clave con éxito.";

            return View(changePasswordModel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteUser(int id)
        {
            bool response = UoW.UserService.Delete<IUser>(id);

            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult UnblockUser(int id, string back)
        {
            bool response = UoW.UserService.UnLockUser(id);
            return Redirect(back);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult BlockUser(int id, string back)
        {
            bool response = UoW.UserService.LockUser(id);
            return Redirect(back);
        }

        public ActionResult EnableUser(int id, string back)
        {
            UoW.UserService.Enable(id);
            return Redirect(back);
        }

        public ActionResult DisableUser(int id, string back)
        {
            UoW.UserService.Disable(id);
            return Redirect(back);
        }

        public ActionResult Emular(int id, string urlBack)
        {
            IUser user = UoW.UserService.GetById(id);

            FormsAuthentication.SetAuthCookie(user.Username, true);
            return RedirectToAction("Welcome", "Home", new { Area = "", token = CurrentUser.Username, back = urlBack });
        }


        /// <summary>
        ///
        /// </summary>
        /// <param name="userToSearch"></param>
        /// <param name="companyToSearch"></param>
        /// <returns></returns>
        private List<UserModel> IndexLocal(string userToSearch, int? companyToSearch)
        {
            Paginacion paginacion = new Paginacion() { Pagina = 0, Cantidad = int.MaxValue };

            List<IUser> userList = UoW.UserService.GetUsersByCompanyUsername(paginacion, companyToSearch ?? -1, userToSearch).ToList();
            List<UserModel> bpUserModelList = new List<UserModel>();

            foreach (User user in userList)
            {
                UserModel usermodel = Mapper.Map<User, UserModel>(user);

                string[] roles_ = Roles.GetRolesForUser(user.Username);
                if (roles_.Count() == 0)
                    usermodel.Rol = "Operador";
                else
                    usermodel.Rol = roles_[0];

                if (usermodel.Rol != "Super Admin" || Roles.IsUserInRole(CurrentUser.Username, "Super Admin"))
                    bpUserModelList.Add(usermodel);
            }

            return bpUserModelList;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userEditModel"></param>
        /// <returns></returns>
        private bool EditUserLocal(UserEditModel userEditModel)
        {
            IUser user = (User)UoW.UserService.GetById(userEditModel.Id);

            user.Name = userEditModel.Name;
            user.Surname = userEditModel.Surname;
            user.Phone = userEditModel.Phone;
            user.PostalCode = userEditModel.PostalCode;
            user.IsApproved = true;

            if (!string.IsNullOrEmpty(userEditModel.Password))
            {
                string encodedPassword = PasswordProvider.GeneratePasswordHashed(userEditModel.Password);

                if (encodedPassword.Length > 255)
                    ModelState.AddModelError("InvalidencodedPassword", "InvalidencodedPassword");

                user.Password = encodedPassword;
            }

            ICompany company = null;
            if (userEditModel.CompanyValue > 0)
                company = UoW.CompanyService.GetById(userEditModel.CompanyValue);

            user.Companies.Clear();
            user.Companies.Add((Company)company);

            IRol roleSelected = UoW.RolService.GetById(userEditModel.Rol);
            user.Roles.Clear();
            user.Roles.Add((Rol)roleSelected);

            user = UoW.UserService.Update(user);

            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="usermodel"></param>
        /// <returns></returns>
        private bool CreateUserLocal(UserModel userModel)
        {
            if (UoW.UserService.GetUsersByEmail(userModel.Email).Count > 0)
            {
                ModelState.AddModelError("", "Existe otro usuario con ese Email");
                return false;
            }

            if (userModel.CompanyValue > 0)
                userModel.Username = string.Concat(userModel.Username.Trim(), "@", userModel.Domain.Trim());

            Paginacion paginacion = new Paginacion() { Pagina = 0, Cantidad = 1 };

            if (UoW.UserService.GetUsersByCompanyUsername(paginacion, userModel.CompanyValue, userModel.Username).Count > 0)
            {
                ModelState.AddModelError("", "Existe otro usuario con ese nombre en la empresa");
                return false;
            }

            IRol roleSelected = UoW.RolService.GetById(userModel.Rol);
            ICompany company = null;
            if (userModel.CompanyValue > 0)
                company = UoW.CompanyService.GetById(userModel.CompanyValue);

            IUser user = Mapper.Map<UserModel, User>(userModel);

            user.Companies.Add((Company)company);
            user.Roles.Add((Rol)roleSelected);

            int ret = UoW.UserService.Create(user);

            return true;
        }
    }
}