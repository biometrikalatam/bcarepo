﻿using BCR.System.Communication;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebNV.WebSite.Models;

namespace WebNV.Controllers
{

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewData = (ViewDataDictionary)TempData["ViewData"];
            LoginModel loginModel = new LoginModel();
            return View(loginModel);
        }
        [Authorize]
        public ActionResult Welcome()
        {
            return View();
        }

        public ActionResult Error()
        {
            string error = "Error interno";
            if (Session["Error_"]!= null)
                error = Session["Error_"].ToString();

            MailServer.SendSoporteMail("erroresbci@biometrika.cl", "Error en BCI", error);

            FormsAuthentication.SignOut();
            Session.Abandon();

            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);

            HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", "");
            cookie2.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie2);

            return View((object)error);
        }
    }
}