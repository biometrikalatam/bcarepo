﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.NV;
using System.Web.Mvc;

namespace WebNV.Controllers
{
    [Authorize(Roles = ("Super Admin, Administrador"))]
    public class ConfigController : BaseController
    {
        //public ActionResult Index()
        //{
        //    Config config = UoW.ConfigService.GetByCompanyId(CurrentUser.Company.Id);
        //    if (config is null)
        //    {
        //        Company company = (Company)CurrentUserCompany;
        //        config = new Config(company);
        //        config.Id = UoW.ConfigService.Create(config);

        //    } 

        //    return View(config);
        //}

        public ActionResult Index()
        {
            CompanyWorkFlow companyWorkFlow = UoW.CompanyWorkFlowService.GetByCompanyId(CurrentUser.Company.Id);
            if (companyWorkFlow is null)
            {
                Company company = (Company)CurrentUserCompany;
                companyWorkFlow = new CompanyWorkFlow(company);
                companyWorkFlow.Id = UoW.CompanyWorkFlowService.Create(companyWorkFlow);
            } 

            return View(companyWorkFlow);
        }

        //[HttpPost]
        //public ActionResult Index(Config configForm)
        //{
        //    Config currentConfig = UoW.ConfigService.GetByCompanyId(CurrentUserCompany.Id);
        //    if(!string.IsNullOrEmpty(configForm.ReceiverEmails)) currentConfig.ReceiverEmails = configForm.ReceiverEmails;
        //    if (configForm.Score > 0) currentConfig.Score = configForm.Score;
        //    currentConfig.ForceOnBoarding = configForm.ForceOnBoarding;

        //    Config configUpdated = UoW.ConfigService.Update(currentConfig);

        //    return View(configUpdated);
        //}


        [HttpPost]
        public ActionResult Index(CompanyWorkFlow formWorkFlow)
        {
            CompanyWorkFlow currentWorkFlow = UoW.CompanyWorkFlowService.GetByCompanyId(CurrentUserCompany.Id);
            if (!string.IsNullOrEmpty(formWorkFlow.ReceiverList)) currentWorkFlow.ReceiverList = formWorkFlow.ReceiverList;
            if (formWorkFlow.Threshold > 0) currentWorkFlow.Threshold = formWorkFlow.Threshold;

            CompanyWorkFlow workFlowUpdated = UoW.CompanyWorkFlowService.Update(currentWorkFlow);

            return View(workFlowUpdated);
        }
    }
}