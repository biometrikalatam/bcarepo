﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Services.NV;
using BCR.System.Validation;
using System.Web.Mvc;

namespace WebNV.Controllers
{
    /// <summary>
    /// Controller Base
    /// </summary>
    public abstract class BaseController : Controller
    {
        public BaseController()
        {
            //if (IPAddress != null)
            //    if (Request != null)
            //        IPAddress = Request.UserHostAddress;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
 
            if (!string.IsNullOrEmpty(User.Identity.Name))
            {
                CurrentUser = UoW.GetUserByName(User.Identity.Name);
                CurrentUserCompany = CurrentUser.Company;
            }
        }

        protected IUser CurrentUser
        {
            get;
            private set;
        }

        protected ICompany CurrentUserCompany
        {
            get;
            private set;
        }

        protected NVUnitOfWork UoW
        {
            get
            {
                if (!HaveContext) return null;

                return NVService.UoW;
            }
        }

        private bool HaveContext
        {
            get
            {
                return HttpContext != null;
            }
        }

        protected static string IPAddress { get; set; }
    }
}