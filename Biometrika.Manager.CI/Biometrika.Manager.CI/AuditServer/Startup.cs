﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AuditServer.Startup))]
namespace AuditServer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
