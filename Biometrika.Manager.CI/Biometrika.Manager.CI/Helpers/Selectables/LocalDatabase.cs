﻿using BCR.Bio.Domain;
using BCR.Bio.Enum;
using BCR.Bio.Enum.BioPortal;
using BCR.System.Enum;
using Bio.Manager.Services;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Bio.Manager.WebSite.Helpers.Selectables
{
    /// <summary>
    /// Selecciones
    /// </summary>
    internal  static class LocalDatabase
    {
        private static Hashtable factorList = new Hashtable();
        private static Hashtable algoritmoList = new Hashtable();

        /// <summary>
        ///
        /// </summary>
        public static void LoadFactorList()
        {
            if (factorList.Count == 0)
            {
                factorList.Add("1", "Clave");
                factorList.Add("2", "Huella Digital");
                factorList.Add("11", "Certificado Digital");
            }
        }

        /// <summary>
        ///
        /// </summary>
        public static void LoadAlgoritmoList()
        {
            if (algoritmoList.Count == 0)
            {
                algoritmoList.Add("4", "Digital Persona");
                algoritmoList.Add("7", "Verifinger");
                algoritmoList.Add("13", "ANSI INSITS 378-2004");
                algoritmoList.Add("14", "ISO/IEC 19794-2:2005");
            }
        }

        /// <summary>
        /// Core de creacion de Selects
        /// </summary>
        /// <param name="lista"></param>
        /// <returns></returns>
        private static List<SelectListItem> CreateListItem(Hashtable lista)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            foreach (var item in lista.Keys)
                list.Add(new SelectListItem() { Value = item.ToString(), Text = lista[item].ToString() });
            return list;
        }

        /// <summary>
        /// Retorna un valor
        /// </summary>
        /// <param name="list"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static string GetValue(Hashtable list, string key)
        {
            return (string)list[key];
        }

        /// <summary>
        /// Recuperar factor
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        internal static string GetFactorById(string key)
        {
            LoadFactorList();
            return GetValue(factorList, key);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        internal static string GetAlgoritmoById(string key)
        {
            LoadAlgoritmoList();
            return GetValue(algoritmoList, key);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        internal static string GetBodyPart(string key)
        {
            return EnumHelper.GetStringValue<BodyPartEnum>(int.Parse(key));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        internal static string GetBirType(string key)
        {
            return EnumHelper.GetStringValue<BirTypeEnum>(int.Parse(key));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        internal static string GetMinutiaeType(string key)
        {
            return EnumHelper.GetStringValue<MinutiaeTypeEnum>(int.Parse(key));
        }

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        //internal static List<SelectListItem> GetCompanys()
        //{


        //    return companys.Select(tipoBase =>
        //        new SelectListItem
        //        {
        //            Selected = false,
        //            Text = tipoBase.Name,
        //            Value = tipoBase.Id.ToString()
        //        }).ToList();
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="role"></param>
        ///// <returns></returns>
        //internal static List<SelectListItem> GetCompanysByRol(string role)
        //{
        //    return null;
        //    if (role == "Super Administrador")
        //    {
        //        List<SelectListItem> companyList = new List<SelectListItem>();
        //        companyList.Add(new SelectListItem() { Value = "0", Text = "Todas" });
        //        companyList.AddRange(GetCompanys());
        //        return companyList;
        //    }
        //    else
        //        return null;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        //internal static List<SelectListItem> GetCompanyList()
        //{
        //    if (Roles.GetRolesForUser()[0] == "Super Administrador")
        //    {
        //        return GetCompanys();
        //    }
        //    else
        //        return null;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        //internal static List<SelectListItem> GetCompanyListWithAll()
        //{
        //    if (Roles.GetRolesForUser()[0] == "Super Administrador")
        //    {
        //        List<SelectListItem> companyList = new List<SelectListItem>();
        //        companyList.Add(new SelectListItem() { Value = "0", Text = "Todas" });
        //        companyList.AddRange(GetCompanys());

        //        return companyList;
        //    }
        //    else
        //        return null;
        //}

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        internal static List<SelectListItem> GetLevels()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Value = "INFO", Text = "INFO" });
            list.Add(new SelectListItem() { Value = "DEBUG", Text = "DEBUG" });
            list.Add(new SelectListItem() { Value = "ERROR", Text = "ERROR" });
            list.Add(new SelectListItem() { Value = "FATAL", Text = "FATAL" });
            list.Add(new SelectListItem() { Value = "WARN", Text = "WARN" });
            return list;
        }

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="value"></param>
        ///// <returns></returns>
        //internal static Company GetCompanyById(int value)
        //{
        //    return BioManagerService.GetCompanyById(value);
        //}

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        internal static List<SelectListItem> GetAlgoritmoList()
        {
            LoadAlgoritmoList();
            return CreateListItem(algoritmoList);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        internal static List<SelectListItem> GetOrdenVerificacion()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Value = "1", Text = "Solo Local" });
            list.Add(new SelectListItem() { Value = "2", Text = "Primero Local" });
            list.Add(new SelectListItem() { Value = "3", Text = "Solo Remoto" });

            return list;
        }

        internal static List<SelectListItem> GetVerificadoList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Value = "NV", Text = "No Verificado" });
            list.Add(new SelectListItem() { Value = "VCI", Text = "Verificado con CI" });
            list.Add(new SelectListItem() { Value = "OTRO", Text = "Otro" });

            return list;
        }

        internal static List<SelectListItem> GetTipoId()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Value = "RUT", Text = "RUT" });
            list.Add(new SelectListItem() { Value = "UID", Text = "User ID" });
            list.Add(new SelectListItem() { Value = "CI", Text = "Cedula de Identidad" });
            list.Add(new SelectListItem() { Value = "PAS", Text = "Pasaporte" });
            list.Add(new SelectListItem() { Value = "DNI", Text = "Documento Nacional de Identidad" });
            list.Add(new SelectListItem() { Value = "CCC", Text = "Cedula de Identidad Duplicada" });
            return list;
        }

        internal static List<SelectListItem> GetSexo()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Value = "M", Text = "Masculino" });
            list.Add(new SelectListItem() { Value = "F", Text = "Femenino" });

            return list;
        }

        internal static List<SelectListItem> GetFactorList()
        {
            LoadFactorList();
            return CreateListItem(factorList);
        }

        internal static List<SelectListItem> GetDeviceList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Value = "1", Text = "Digital Persona", Selected = true });
            list.Add(new SelectListItem() { Value = "2", Text = "Secugen" });
            list.Add(new SelectListItem() { Value = "7", Text = "Upek" });

            return list;
        }

        internal static List<SelectListItem> GetModeloList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Value = "4000", Text = "U.are.U 4000", Selected = true });
            list.Add(new SelectListItem() { Value = "4500", Text = "U.are.U 4500" });
            list.Add(new SelectListItem() { Value = "FDU02", Text = "Hamster II / III" });
            list.Add(new SelectListItem() { Value = "FDU03", Text = "Hamster Plus" });
            list.Add(new SelectListItem() { Value = "FDU04", Text = "Hamster IV" });

            return list;
        }

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        //internal static List<SelectListItem> GetRolesWithSA()
        //{
        //    return GetRoles(true);
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        //internal static List<SelectListItem> GetRoles()
        //{
        //    return GetRoles(false);
        //}

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        //private static List<SelectListItem> GetRoles(bool includeSA)
        //{
        //    CustomRoleProvider customRoleProvider = new CustomRoleProvider();
        //    string[] rolesArray = customRoleProvider.GetAllRoles();

        //    List<string> roleList = new List<string>(rolesArray);

        //    if (!includeSA)
        //        roleList.Remove("Super Administrador");

        //    return roleList.Select(item =>
        //       new SelectListItem
        //       {
        //           Selected = false,
        //           Text = item,
        //           Value = item
        //       }).ToList();
        //}

        //internal static List<SelectListItem> GetOrigenList()
        //{
        //    var origens = BioManagerService.GetOrigens().ToList();

        //    return origens.Select(tipoBase =>
        //        new SelectListItem
        //        {
        //            Selected = false,
        //            Text = tipoBase.Description,
        //            Value = tipoBase.Id.ToString()
        //        }).ToList();
        //}

        internal static List<SelectListItem> GetActionList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            Hashtable actions = EnumHelper.List<ActionEnum>();
            foreach (var item in actions.Keys)
            {
                list.Add(new SelectListItem() { Value = (string)item.ToString(), Text = (string)actions[item].ToString() });
            }

            return list;
        }

        internal static List<SelectListItem> GetStateTypeList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Value = "0", Text = "ESTADO_OK" });
            list.Add(new SelectListItem() { Value = "1", Text = "ESTADO_USUARIO_NO_VALIDO" });
            list.Add(new SelectListItem() { Value = "2", Text = "ESTADO_XML_INVALIDO" });
            list.Add(new SelectListItem() { Value = "3", Text = "ESTADO_NO_EXISTE_INFO" });
            list.Add(new SelectListItem() { Value = "4", Text = "ESTADO_CALIDAD_DEFICIENTE" });

            return list;
        }

        internal static List<SelectListItem> GetResultadosTxList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Value = "1", Text = "RESULTADO_OK" });
            list.Add(new SelectListItem() { Value = "2", Text = "RESULTADO_NOOK" });
            return list;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        internal static string GetActionById(int p)
        {
            return EnumHelper.GetStringValue<ActionEnum>(p);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        internal static string GetResultById(int actionType, int result)
        {
            if (actionType == 1 || actionType == 3)
            {
                if (result == 0)
                    return "Error";
                if (result == 1)
                    return "Positivo";
                if (result == 2)
                    return "Negativo";
            }

            return "";
        }

        /// <summary>
        /// Retornar detalle del error
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        internal static string GetErrorDetailedById(int p)
        {
            return EnumHelper.GetStringValue<Errors>(p);
        }

        internal static string GetOperacionById(string p)
        {
            if (p == "0")
                return "Error";
            if (p == "1")
                return "Verificación";
            if (p == "2")
                return "Enroll";

            return "";
        }
    }
}