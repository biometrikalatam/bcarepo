﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebNV.Helpers.Extension
{
    public static  class HTMLExtension
    {
        public static IHtmlString SuccessInformation(this HtmlHelper htmlHelper, string message)
        {
            if (string.IsNullOrEmpty( message))
            {
                return null;
            }

            string messageSpan;
            
            TagBuilder spanTag = new TagBuilder("div");
            spanTag.MergeAttribute("class", "SuccessInformation");
            spanTag.SetInnerText(message);
            messageSpan = spanTag.ToString(TagRenderMode.Normal) + Environment.NewLine;

            return new HtmlString(messageSpan);
        }

        //public static Image Base64ToImage(string base64String)
        //{
        //    // Convert Base64 String to byte[]
        //    byte[] imageBytes = Convert.FromBase64String(base64String);
        //    MemoryStream ms = new MemoryStream(imageBytes, 0,
        //      imageBytes.Length);

        //    // Convert byte[] to Image
        //    ms.Write(imageBytes, 0, imageBytes.Length);
        //    Image image = Image.FromStream(ms, true);
        //    return image;
        //}
    }


}
