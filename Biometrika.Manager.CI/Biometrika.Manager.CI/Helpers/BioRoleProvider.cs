﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Services.NV;
using System.Linq;
using System.Web.Security;

namespace WebNV.Helpers
{
    public class BioRoleProvider : RoleProvider
    {
        private NVUnitOfWork service;

        public BioRoleProvider()
        {
            service =  NVService.UoW;
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            IUser user = service.UserService.GetByUsername(username);
            if (user == null)
                return false;
            return user.Roles != null && (user.Roles.Count(r => r.Name == roleName) == 1);
        }

        public override string[] GetRolesForUser(string username)
        {
            IUser user = service.UserService.GetByUsername(username);
            if (user == null)
                return new string[] { };
            return user.Roles == null ? new string[] { } :
                  user.Roles.Select(u => u.Name).ToArray();
        }

        // -- Snip --

        public override string[] GetAllRoles()
        {
            return service.RolService.List().Select(r => r.Name).ToArray();
        }

        // -- Snip --

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new System.NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new System.NotImplementedException();
        }
    }
}