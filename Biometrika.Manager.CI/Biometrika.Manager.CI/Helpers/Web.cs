﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Domain.NV;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebNV.Models;

namespace WebNV.Helpers
{
    public static class Web
    {
        public static List<SelectListItem> SectorList(IList<Sector> list, string selected = "")
        {
            return list.Select(item =>
               new SelectListItem
               {
                   Selected = (item.Id.ToString() == selected),
                   Text = item.Description,
                   Value = item.Id.ToString()
               }).ToList();
        }

        public static List<SelectListItem> AplicacionList(IList<Aplicacion> list, string selected)
        {
            return list.Select(item =>
               new SelectListItem
               {
                   Selected = (item.Id.ToString() == selected),
                   Text = string.Concat(item.Nombre),
                   Value = item.Id.ToString()
               }).ToList();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="list"></param>
        /// <param name="selected"></param>
        /// <returns></returns>
        public static List<SelectListItem> HQList(IList<HQ> list, string selected)
        {
            return list.Select(item =>
               new SelectListItem
               {
                   Selected = (item.Id.ToString() == selected),
                   Text = string.Concat(item.Description ),
                   Value = item.Id.ToString()
               }).ToList();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static List<SelectListItem> HoldingList(IList<IHolding> list, string selected)
        {
            return list.Select(item =>
               new SelectListItem
               {
                   Selected = (item.Id.ToString() == selected),
                   Text = item.Name,
                   Value = item.Id.ToString()
               }).ToList();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static List<SelectListItem> RoleListSA(IList<IRol> list)
        {
            return list.Select(item =>
               new SelectListItem
               {
                   Selected = false,
                   Text = item.Name,
                   Value = item.Id.ToString()
               }).ToList();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static List<SelectListItem> RoleList(IList<IRol> list)
        {
            return list.Where(item => item.Name != "Super Admin").Select(item =>
               new SelectListItem
               {
                   Selected = false,
                   Text = item.Name,
                   Value = item.Id.ToString()
               }).ToList();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> CompanyList(IList<ICompany> list, bool includeEmpty = true)
        {
            List<SelectListItem> companylist = new List<SelectListItem>();
            companylist.Add(new SelectListItem() { Value = "-1", Text = "" });

            list.ToList().ForEach(item => companylist.Add(
               new SelectListItem
               {
                   Selected = false,
                   Text = item.Name,
                   Value = item.Id.ToString()
               }));

            return companylist;
        }

        public static List<SelectListItem> OrigenList(IQueryable<IBpOrigin> list)
        {
            return list.Select(item =>
                new SelectListItem
                {
                    Selected = false,
                    Text = item.Description,
                    Value = item.Id.ToString()
                }).ToList();
        }

        public static List<CheckedViewModel> CompanyListCheck(List<Company> allCompanies, IList<Company> actualCompanies)
        {
            List<CheckedViewModel> companylist = new List<CheckedViewModel>();

            allCompanies.ToList().ForEach(m => companylist.Add(
                new CheckedViewModel()
                {
                    Id = m.Id,
                    Name = m.Name,
                    Checked = actualCompanies == null ? false : actualCompanies.Count(n => n.Id == m.Id) == 1
                }));

            return companylist;
        }
    }
}