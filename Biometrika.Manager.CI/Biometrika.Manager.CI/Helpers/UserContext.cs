﻿using BCR.Bio.Domain;
using BCR.Bio.Services;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;

namespace Bio.Manager.WebSite.Helpers
{
    public static class UserContext
    {
        public static string IPAddress { get; private set; }

        public static User User { get; private set; }

        //public static IDictionary<int, string> Companies { get; private set; }
   
        internal static void Initialize(BioUnitOfWork UoW, HttpRequestBase Request, IPrincipal UserIdentity)
        {
            if (IPAddress!=null)
                if (Request != null)
                    IPAddress = Request.UserHostAddress;

            if (User!=null)
                if (UserIdentity != null)
                    User = (User)UoW.UserService.GetByUsername(UserIdentity.Identity.Name);

            //if (User != null)
            //    Companies = User.Companies;
        }
    }
}