﻿using BCR.Bio.Domain.BCI;
using BCR.System.Log;
using BCR.System.Validation;
using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web;

namespace BCI
{
    /// <summary>
    ///
    /// </summary>
    public class CustomerService : ICustomerService
    {
        public bool DebugXMLContent { get { return bool.Parse(ConfigurationManager.AppSettings.Get("DebugXMLContent")); } }

        public int RetentionTime { get { return int.Parse(ConfigurationManager.AppSettings.Get("RetentionTime")); } }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="company"></param>
        /// <param name="rut"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public string GetAuthorizationInfo(string user, string password, string company, string rut)
        {
             Log4Bio.Info("Peticion GetAuthorizationInfo");

            if (DebugXMLContent)
                BCIWebService.PersistContent(string.Concat(user, "-", password, "-", company, "-", rut), "Client", " WS->GetAuthorizationInfo", rut);

            string response = string.Empty;

            ServiceRequest serviceRequest = new ServiceRequest()
            {
                EventTime = DateTime.Now,
                Source = HttpContext.Current.Request.UserHostAddress,
                User = user,
                Company = company,
                Rut = rut
            };

            if (BCIWebService.IsValidUser(company, user, password))
            {
                DateTime dateFrom = DateTime.Now;
                dateFrom = dateFrom.AddDays(-(RetentionTime));

                Transaction transaction = BCIWebService.GetTransactionInfoByRut(company, rut, dateFrom);
                if ((transaction == null) || (transaction.Id == 0))
                {
                    serviceRequest.Result = "-2";
                    response = ResponseError("RespuestaCacheNotFound.xml");
                }
                else
                {
                    serviceRequest.Result = "0";
                    response = GenerateAuthorizationInfo(transaction.CodigoAutorizacion, transaction.UrlDocument);
                }
            }
            else
            {
                serviceRequest.Result = "-1";
                response = ResponseError("RespuestaCacheInvalidUserPassCompany.xml");
            }

            if (DebugXMLContent)
                BCIWebService.PersistContent(response, " WS->GetAuthorizationInfo", "Client", rut);

            BCIWebService.GenerateRequestLog(serviceRequest);

            return response;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="company"></param>
        /// <param name="rut"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public string GetCopyCertificate(string user, string password, string company, string rut, string format)
        {
            string response = string.Empty;
             Log4Bio.Info("Peticion GetCopyCertificate");

            if (DebugXMLContent)
                BCIWebService.PersistContent(string.Concat(user, "-", password, "-", company, "-", rut), "Client", " WS->GetCopyCertificate", rut);

            ServiceRequest serviceRequest = new ServiceRequest()
            {
                EventTime = DateTime.Now,
                Format = format,
                Source = HttpContext.Current.Request.UserHostAddress,
                User = user,
                Company = company,
                Rut = rut
            };

            if (BCIWebService.IsValidUser(company, user, password))
            {
                DateTime dateFrom = DateTime.Now;
                dateFrom = dateFrom.AddDays(-(RetentionTime));

                string xmlResponse = BCIWebService.GetContentByRut(company, rut, dateFrom);

                if (string.IsNullOrEmpty(xmlResponse))
                {
                    serviceRequest.Result = "-2";
                    response = ResponseError("RespuestaCacheNotFound.xml");
                }
                else
                {
                    serviceRequest.Result = "0";
                    if (format.ToLower() == "pdf")
                        response = xmlResponse;
                    else
                        response = CerticateEncoded(xmlResponse);
                }
            }
            else
            {
                serviceRequest.Result = "-1";
                response = ResponseError("RespuestaCacheInvalidUserPassCompany.xml");
            }

            if (DebugXMLContent)
                BCIWebService.PersistContent(response, " WS->GetCopyCertificate", "Client", rut);

            BCIWebService.GenerateRequestLog(serviceRequest);

            return response;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="xmlResponse"></param>
        /// <returns></returns>
        private string CerticateEncoded(string xmlResponse)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(xmlResponse);
            string stringbase64 = Convert.ToBase64String(plainTextBytes);

            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "RespuestaCacheOk.xml");
            if (!System.IO.File.Exists(path))
                throw new Exception("No existe el archivo RespuestaTerminal");

            string xmlRespuesta = File.ReadAllText(path);
            xmlRespuesta = xmlRespuesta.Replace("{certificate}", stringbase64);

            return xmlRespuesta;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="codigoAutorizacion"></param>
        /// <param name="urlDocument"></param>
        /// <returns></returns>
        private string GenerateAuthorizationInfo(string codigoAutorizacion, string urlDocument)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "AuthorizationInfo.xml");
            if (!System.IO.File.Exists(path))
                throw new Exception("No existe el archivo RespuestaTerminal");

            string xmlRespuesta = File.ReadAllText(path);
            xmlRespuesta = xmlRespuesta.Replace("{codigoAutorizacion}", codigoAutorizacion);
            xmlRespuesta = xmlRespuesta.Replace("{urlDocument}", urlDocument);
            return xmlRespuesta;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="fileError"></param>
        /// <returns></returns>
        private string ResponseError(string fileError)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", fileError);
            if (!System.IO.File.Exists(path))
                throw new Exception("No existe el archivo RespuestaTerminal");

            string xmlRespuesta = File.ReadAllText(path);

            return xmlRespuesta;
        }

        public string GetTxInfo(string user, string password, string company, string rut, long milisec, int flag)
        {
            Log4Bio.Info("Peticion GetTxInfo");
 
            string response = string.Empty;
            string path = "";

            ServiceRequest serviceRequest = new ServiceRequest()
            {
                EventTime = DateTime.Now,
                Source = HttpContext.Current.Request.UserHostAddress,
                User = user,
                Company = company,
                Rut = rut
            };

            if (BCIWebService.IsValidUser(company, user, password))
            {
                int retentionTime = Convert.ToInt32(milisec);

                //La transacción tendrá un vigencia máxima de x cantidad de milisegundos.
                DateTime date = DateTime.Now;

                TimeSpan span = new TimeSpan(0, 0, 0, 0, retentionTime);
                DateTime dateFrom = date.Subtract(span);

                Transaction transaction = BCIWebService.GetTransactionInfoByRutDemo(company, rut, dateFrom);
                if ((transaction == null) || (transaction.Id == 0))
                {
                    serviceRequest.Result = "-2";
                    response = ResponseError("RespuestaCacheNotFound.xml");
                }
                else
                {
                    response = BCIWebService.GenerateTxtInfoResponse(transaction, flag);

                    serviceRequest.Result = "0";
                    //response = GenerateAuthorizationInfo(transaction.CodigoAutorizacion, transaction.UrlDocument);
                }
            }
            else
            {
                serviceRequest.Result = "-1";
                return ResponseError("RespuestaCacheInvalidUserPassCompany.xml");
            }

            return response;
        }
    }
}