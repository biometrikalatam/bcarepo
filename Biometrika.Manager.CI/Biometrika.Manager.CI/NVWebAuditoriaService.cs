﻿using BCR.Bio.Services.NV;
using BCR.System.Configuration;
using BCR.System.Enum;
using BCR.System.Log;
using BCR.System.Validation;
using System.Configuration;
using System.IO;

namespace WebNV
{
    /// <summary>
    /// Servicio de todo el framework
    /// </summary>
    public static class NVService
    {
        /// <summary>
        /// Inicializa el framework, con log, conexiones
        /// </summary>
        /// <returns></returns>
        public static void Initialize()
        {
            string logname = ConfigurationManager.AppSettings["LogName"];
            LogConfiguration logConfiguration = new LogConfiguration(LogProvider.Log4Net, new FileInfo(@"log\" + logname));

            SystemConfiguration systemConfiguration = new SystemConfiguration(logConfiguration, false);
            
            UoW = new NVUnitOfWork(systemConfiguration.HibernateParams, new ValidationDictionary());

            return;
        }

        public static NVUnitOfWork UoW { get; private set; }
    }
}