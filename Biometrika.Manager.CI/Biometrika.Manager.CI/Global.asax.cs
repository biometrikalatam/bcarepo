﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebNV.App_Start;

namespace WebNV
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutomapperConfig.RegisterMaps();

            NVService.Initialize();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //if (sender.ToString() != "ASP.global_asax")
            //if (Server.GetLastError() != null)
            //{
            //    Exception exception = Server.GetLastError();
            //    Response.Clear();

            //    HttpException httpException = exception as HttpException;

            //    Server.ClearError();
            //    Session["Error_"] =   string.Concat( exception.Message, Environment.NewLine , exception.StackTrace == null ? "": exception.StackTrace );

            //    Response.RedirectToRoute(
            //                   new RouteValueDictionary {
            //                        { "Controller", "Home" },
            //                        { "Action", "Error" }});
            //}
        }
    }
}