﻿using BCR.System.Log;
using System;
using System.Configuration;
using System.Xml;

namespace BCI
{
    /// <summary>
    /// Web Service que atiende las peticiones de Previred
    /// Es el paso intermedio en el flujo de control
    /// </summary>
    public class Previred : IPrevired
    {
        private string usuario;
        private string password;
        private string codautoriza;

        private string test = "";

        static Previred()
        {
        }

        /// <summary>
        /// Método que ejecuta la solicitud
        /// </summary>
        /// <param name="xmlIn">XML de entrada
        /// Ver ejemplo abajo
        /// </param>
        /// <returns>XML de salida, ver ejemplo abajo</returns>
        public string Ejecuta(string xmlIn)
        {
            string xmlResponse;

            Log4Bio.Info("Peticion Ejecuta");

            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlIn);
            XmlNodeList xnList = document.SelectNodes("/peticion");
            foreach (XmlNode xn in xnList)
            {
                usuario = xn["usuario"].InnerText;
                password = xn["password"].InnerText;
                codautoriza = xn["codautoriza"].InnerText;
            }

            // Dejar rastro
            if (bool.Parse(ConfigurationManager.AppSettings.Get("DebugXMLContent")))
                BCIWebService.PersistContent(xmlIn, "WS - Previred", " WS->Ejecuta", codautoriza);

            if (BCIWebService.ValidateUser(usuario, password))
                /// verifica la peticion  y responde
                xmlResponse = BCIWebService.GetResponseForValidation(usuario, password, codautoriza);
            else
                xmlResponse = BCIWebService.UsuarioNoValido();

            if (bool.Parse(ConfigurationManager.AppSettings.Get("DebugXMLContent")))
                BCIWebService.PersistContent(xmlResponse, " WS->Ejecuta", "WS - Previred", codautoriza);

            return xmlResponse;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public string Ping(string param)
        {
            Log4Bio.Info("Peticion Ping");

            return string.Concat(param, " at ", DateTime.Now);
        }
    }
}

/*
 *  XML de entrada de ejemplo
 *
<?xml version="1.0" encoding="iso-8859-1"?>
  <peticion>
  <usuario>previred</usuario>
  <password>078795f09789d84668e271ffbb7d5043</password>
  <codautoriza>b27f8c8a-5370-41f2-a280-6c02552734b9</codautoriza>
</peticion>
 *
 *
 *
 * XML de salida esperado de ejemplo
 *
 * <?xml version="1.0" encoding="ISO-8859-1" ?>
<respuesta>
  <codigo>9000</codigo>
  <codautoriza>b27f8c8a-5370-41f2-a280-6c02552734b9</codautoriza>
  <rut>13695203-K</rut>
  <fechahoraconsulta>21/12/2015 02:28:14</fechahoraconsulta >
  <idcliente>H39150402297</idcliente >
  <resultado>True</resultado >
  <numeroserie></numeroserie >
  <apellidopaterno>CERÓN</apellidopaterno >
  <timestamp></timestamp >
  <urlauditoria>https://bci.biometrika.cl/webaudit/audit/b27f8c8a-5370-41f2-a280-6c02552734b9</urlauditoria >
</respuesta>

 *
 *
 *
*/