﻿function BiometrikaClient(host) {
    this.BaseUrl = host;
    this.Src = "";
    this.TypeId = "";
    this.ValueId = "";
    this.TokenContent = "";
    this.Device = "";
    this.AuthenticationFactor = "";
    this.OperationType = "";
    this.MinutiaeType = "";
    this.TimeOut = "";
    this.Theme = "";
    this.Hash = Math.floor((Math.random() * 100) + 1);
    
    this.GetFinger = function () {
        var returnData = new BiometrikaClientResponse();

        if (this.TypeId == null || this.TypeId == '') {
            returnData.Status = false;
            returnData.Message = "Ingrese Tipo de Identificación";
            return returnData;
        }
        if (this.ValueId == null || this.ValueId == '') {
            returnData.Status = false;
            returnData.Message = "Ingrese Identificación";
            return returnData;
        }

        if (this.TokenContent == null || this.TokenContent == '') {
            returnData.Status = false;
            returnData.Message = "Ingrese TokenContent";
            return returnData;
        }
        if (this.AuthenticationFactor == null || this.AuthenticationFactor == '') {
            returnData.Status = false;
            returnData.Message = "Ingrese AuthenticationFactor";
            return returnData;
        }
        if (this.OperationType == null || this.OperationType == '') {
            returnData.Status = false;
            returnData.Message = "Ingrese OperationType";
            return returnData;
        }
        if (this.MinutiaeType == null || this.MinutiaeType == '') {
            returnData.Status = false;
            returnData.Message = "Ingrese MinutiaeType";
            return returnData;
        }
        if (this.Device == null || this.Device == '') {
            returnData.Status = false;
            returnData.Message = "Ingrese Device";
            return returnData;
        }
        if (this.TimeOut == null || this.TimeOut == '' || this.TimeOut == '0') {
            returnData.Status = false;
            returnData.Message = "Ingrese Timeout";
            return returnData;
        }

        this.Src = "bpc7";
        this.Theme = "theme_ligthgreen";

        var finalUrl = this.BaseUrl +
            "?src=" + this.Src +
            "&TypeId=" + this.TypeId +
            "&ValueId=" + this.ValueId +
            "&Tokencontent=" + this.TokenContent +
            "&Device=" + this.Device +
            "&AuthenticationFactor=" + this.AuthenticationFactor +
            "&OperationType=" + this.OperationType +
            "&MinutiaeType=" + this.MinutiaeType +
            "&Timeout=" + this.TimeOut +
            "&Theme=" + this.Theme +
            "&Hash=" + this.Hash;

        alert(finalUrl);

        var a = function (callback) {
            $.ajax({
                type: "GET",
                url: finalUrl,
                async: false,
                crossDomain: true,
                processData: false,
                error: function (error) {
                    var errMsg = "Error al llamar a BiometrikaClient, " + error.statusText;
                    returnData.Status = false;
                    returnData.Message = errMsg;
                    console.log(errMsg);
                },
                success: callback
            });
        };

        a(function (msg) {
            returnData.Status = true;
            returnData.Data = msg;
        });

        return returnData;
    };
}

function BiometrikaClientResponse() {
    this.Status = false;
    this.Message = "";
    this.Data = "";
}