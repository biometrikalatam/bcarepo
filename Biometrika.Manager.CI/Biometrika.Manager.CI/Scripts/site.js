﻿//$.validator.methods.date = function (value, element) {
//    return this.optional(element) || Globalize.parseDate(value, "d/M/y", "es");
//}
function AddValidationError(msg) {
    var contenedor = $(".validation-summary-errors").find("ul");

    if (contenedor.length > 0) {
        //contenedor.empty();
        contenedor.append("<li>" + msg + "</li>")
    }
    else {
        $(".validation-summary-valid").addClass("validation-summary-errors").removeClass("validation-summary-valid")
        contenedor.append("<li>" + msg + "</li>")
    }

}
// JS for Identity - Find

function ID_ID_Initialize() {
    formid.BPC5.Operationtype = $("#Operacion").val();
    $("#Device").change(function (e) { return ID_ID_onChangeDevice(); });
    $("#Algoritmo").change(function (e) { return ID_ID_onChangeAlgoritmo(); });
    $("input[name='ContenidoToken']").click(function (e) { return ID_ID_onClickContenidoToken(); });
    $("form").submit(function (event) {
        return ID_ID_onSubmit();
    });
}

function ID_ID_SetBpClient() {
    if (ID_ID_SensorOk()) {
        var Factor = $("#Factor").val();
        var Device = $("#Device").val();
        var Model = $("#Modelo").val();
        var TipoMinucia = $("#Algoritmo").val();

        if (Factor == "") {
            alert('Debe ingresar un Factor');
            return false;
        }
        if (Device == "") {
            alert('Debe ingresar un Device');
            return false;
        }
        if (Model == "") {
            alert('Debe ingresar un Model');
            return false;
        }
        if (TipoMinucia == "") {
            alert('Debe ingresar un Algoritmo');
            return false;
        }

        formid.BPC5.AuthenticationFactor = Factor;
        formid.BPC5.TypeId = "RUT";
        formid.BPC5.ValueId = "11111111";
        formid.BPC5.Minutiaetype = TipoMinucia;
        formid.BPC5.Device = Device;
        formid.BPC5.Model = Model;
    }
    return false;
}

function ID_ID_onHuellaDetect() {
    if (!IsFingerPressed) {
        if (formid.BPC5.GetToken().length != 0) {
            $("#SensorInfo").val(formid.BPC5.GetInfo());
            IsFingerPressed = true;

            $("#Data").val(formid.BPC5.GetToken());    // asigno al hidden para q persista en el model ...
            $("#SampleJPG").val(formid.BPC5.GetSampleJPG());
            $("#Dedo").val(formid.BPC5.Finger);
        }
        else
            timer = setTimeout('ID_ID_onHuellaDetect()', 3000);
    }
    else
        timer = setTimeout('ID_ID_onHuellaDetect()', 3000);
}

// JS for Identity - AddBir

function ID_AB_Initialize() {
    formid.BPC5.Operationtype = 2; //Enroll
    $("#Operacion").val("2");
    $("#Device").change(function (e) { return ID_AB_onChangeDevice(); });

    $("form").submit(function (event) {
        return ID_AB_onSubmit();
    });
}



function ID_AB_onChangeDevice() {
    switch ($("#Device").val()) {
        case "1":
            $("#Modelo").val("4000");
            break;
        case "2":
            $("#Modelo").val("FDU02");
            break;
        case "7":
            $("#Modelo").find("option:selected").removeAttr("selected");
            break;
        default:
    }
}


function ID_AB_SensorOk() {
    if (formid.BPC5) {
        var sensorInfoCheck = null;
        try {
            sensorInfoCheck = formid.BPC5.GetInfo()
        } catch (e) {
        }

        if (sensorInfoCheck != null) {
            if (formid.BPC5.GetInfo().split('|')[1].length > 0) {
                huboError = false;
                ID_AB_Cargar();
                return true;
            }
            else
                mensajeError = "No se detecta el sensor de huella conectado al ordenador.";
        }
        else
            mensajeError = "Error al intentar acceder al control de lectura.Si usa Explorer pruebe agregar este sitio a los de confianza";
    }
    else
        mensajeError = "El control de lectura de huella no está instalado.";

    if (huboError)
        return false;
    //ShowError(mensajeError);
}

function ID_AB_Cargar() {
    //SetBioPortalClient(); // simula el boton de carga
    timer = setTimeout('ID_AB_onHuellaDetect()', 300);
}

function ID_AB_onHuellaDetect() {
    if (!IsFingerPressed) {
        if (formid.BPC5.GetToken().length != 0) {
            $("#SensorInfo").val(formid.BPC5.GetInfo());
            IsFingerPressed = true;
            $("#Data").val(formid.BPC5.GetToken());    // asigno al hidden para q persista en el model ...
            $("#SampleJPG").val(formid.BPC5.GetSampleJPG());
            $("#Dedo").val(formid.BPC5.Finger);
        }
        else
            timer = setTimeout('ID_AB_onHuellaDetect()', 3000);
    }
    else
        timer = setTimeout('ID_AB_onHuellaDetect()', 3000);
}

// js for identity -  enroll

function ID_EN_Initialize() {
    $("#FechaVencimiento").mask("99/99/9999");
    $("#FechaNacimiento").mask("99/99/9999");

    formid.BPC5.Operationtype = $("#Operacion").val(); //Enroll

    ID_EN_FotoSetup();
    ID_EN_FirmaSetup();

    $("input[name='Verificado']").click(function (e) { return ID_EN_onClickVerificado(); });

    //$("#Verificado").change(function (e) { return onChangeVerificado(); });
    $("#Device").change(function (e) { return ID_EN_onChangeDevice(); });
    $("form").submit(function (event) {
        return ID_EN_onSubmit();
    });
}



function ID_EN_onClickVerificado() {
    if ($('input:radio[name=Verificado]:checked').val() == "NV") {
        $("#OtroVerificadoText").val("NV");
    }
    if ($('input:radio[name=Verificado]:checked').val() == "VCI") {
        $("#OtroVerificadoText").val("VCI");
    }
    if ($('input:radio[name=Verificado]:checked').val() == "OTRO") {
        $("#OtroVerificadoText").val("");
    }
}

function ID_EN_onChangeDevice() {
    switch ($("#Device").val()) {
        case "1":
            $("#Modelo").val("4000");
            break;
        case "2":
            $("#Modelo").val("FDU02");
            break;
        case "7":
            $("#Modelo").find("option:selected").removeAttr("selected");
            break;
        default:
    }
}



function ID_EN_SensorOk() {
    huboError = true;
    if (formid.BPC5) {
        var sensorInfoCheck = null;
        try {
            sensorInfoCheck = formid.BPC5.GetInfo()
        } catch (e) {
        }

        if (sensorInfoCheck != null) {
            if (formid.BPC5.GetInfo().split('|')[1].length > 0) {
                huboError = false;
                ID_EN_Cargar();
                return true;
            }
            else
                mensajeError = "No se detecta el sensor de huella conectado al ordenador.";
        }
        else
            mensajeError = "Error al intentar acceder al control de lectura.Si usa Explorer pruebe agregar este sitio a los de confianza";
    }
    else
        mensajeError = "El control de lectura de huella no está instalado.";

    if (huboError)
        return false;
    //ShowError(mensajeError);
}

function ID_EN_Cargar() {
    //SetBioPortalClient(); // simula el boton de carga
    timer = setTimeout('ID_EN_onHuellaDetect()', 300);
}

function ID_EN_onHuellaDetect() {
    if (!IsFingerPressed) {
        if (formid.BPC5.GetToken().length != 0) {
            $("#SensorInfo").val(formid.BPC5.GetInfo());
            IsFingerPressed = true;

            $("#Data").val(formid.BPC5.GetToken());    // asigno al hidden para q persista en el model ...
            $("#SampleJPG").val(formid.BPC5.GetSampleJPG());
            $("#Dedo").val(formid.BPC5.Finger);
        }
        else
            timer = setTimeout('ID_EN_onHuellaDetect()', 3000);
    }
    else
        timer = setTimeout('ID_EN_onHuellaDetect()', 3000);
}

function ID_EN_SetImages() {
    try {
        if (document.getElementById("AxFirma").GetImagenB64() != undefined) {
            $("#Firma").val(document.getElementById("AxFirma").GetImagenB64());
        }

        if (document.getElementById("AxFoto").GetImagenB64() != undefined) {
            $("#Foto").val(document.getElementById("AxFoto").GetImagenB64());
        }
    } catch (e) {

    }
    
}

function ID_EN_FotoSetup() {
    try {
        formid.AxFoto.Init(1);
        formid.AxFoto.SetSizeRet(140, 140);

        if (formid.FotoSaved.value == '1') {
            formid.AxFoto.SetImage($("#Foto").val());
        }
    } catch (e) {
    }
}

function ID_EN_FirmaSetup() {
    try {
        formid.AxFirma.Init(2);
        formid.AxFirma.SetSizeRet(140, 70);

        if (formid.FirmaSaved.value == '1') {
            formid.AxFirma.SetImage($("#Firma").val());
        }
    } catch (e) {
    }
}

//$(".datePicker").datepicker({
//    dateFormat: 'dd/mm/yy',
//    weekStart: 0,
//    language: "es",
//    changeMonth: true,
//    changeYear: true,
//});

//$(function () {
//    //Array para dar formato en español
//    $.datepicker.regional['ar'] =
//    {
//        closeText: 'Cerrar',
//        prevText: 'Previo',
//        nextText: 'Próximo',

//        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
//        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
//        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
//        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
//        monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
//        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
//        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
//        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
//        dateFormat: 'dd/mm/yy', firstDay: 0,
//        initStatus: 'Selecciona la fecha', isRTL: false
//    };
//    $.datepicker.regional['ar'];
//});

// js for identity - verify

function ID_VE_Initialize() {
    formid.BPC5.Operationtype = $("#Operacion").val();
    $("#Device").change(function (e) { return ID_VE_onChangeDevice(); });
    $("#Algoritmo").change(function (e) { return ID_VE_onChangeAlgoritmo(); });
    $("input[name='ContenidoToken']").click(function (e) { return ID_VE_onClickContenidoToken(); });
    $("form").submit(function (event) {
        return ID_VE_onSubmit();
    });
}




function ID_VE_onClickContenidoToken() {
    formid.BPC5.Tokencontent = $('input:radio[name=ContenidoToken]:checked').val();
}

function ID_VE_onChangeDevice() {
    switch ($("#Device").val()) {
        case "1":
            $("#Modelo").val("4000");
            break;
        case "2":
            $("#Modelo").val("FDU02");
            break;
        case "7":
            $("#Modelo").find("option:selected").removeAttr("selected");
            break;
        default:
    }
}

function ID_VE_onChangeAlgoritmo() {
    if ($("#Algoritmo").val() == "7") {
        $("#Umbral").val("63");
    }
    else {
        $("#Umbral").val("0");
    }
}

function ID_VE_SetBpClient() {
    if (ID_VE_SensorOk()) {
        var TipoId = $("#TipoId").val();
        var NumeroID = $("#NumeroID").val();

        if (TipoId == null || TipoId == "") {
            alert('Debe ingresar un Tipo ID y Nro ID para poder inicializar el componente');
            return false;
        }
        if (NumeroID == null || NumeroID == "") {
            alert('Debe ingresar un Tipo ID y Nro ID para poder inicializar el componente');
            return false;
        }
        var Minutiae = $("#Algoritmo").val();
        var Factor = $("#Factor").val();
        var Device = $("#Device").val();
        var Model = $("#Modelo").val();
    }

    formid.BPC5.AuthenticationFactor = Factor;
    formid.BPC5.Minutiaetype = Minutiae;
    formid.BPC5.TypeId = TipoId;
    formid.BPC5.ValueId = NumeroID;
    formid.BPC5.Device = Device;
    formid.BPC5.Model = Model;
    return false;
}

function ID_VE_SensorOk() {
    var huboError = false;
    if (formid.BPC5) {
        var sensorInfoCheck = null;
        try {
            sensorInfoCheck = formid.BPC5.GetInfo()
        } catch (e) {
        }

        if (sensorInfoCheck != null) {
            if (formid.BPC5.GetInfo().split('|')[1].length > 0) {
                huboError = false;
                ID_VE_Cargar();
                return true;
            }
            else
                mensajeError = "No se detecta el sensor de huella conectado al ordenador.";
        }
        else
            mensajeError = "Error al intentar acceder al control de lectura.Si usa Explorer pruebe agregar este sitio a los de confianza";
    }
    else
        mensajeError = "El control de lectura de huella no está instalado.";

    if (huboError)
        return false;
}

function ID_VE_Cargar() {
    timer = setTimeout('ID_VE_onHuellaDetect()', 300);
}

function ID_VE_onHuellaDetect() {
    if (!IsFingerPressed) {
        if (formid.BPC5.GetToken().length != 0) {
            $("#SensorInfo").val(formid.BPC5.GetInfo());
            IsFingerPressed = true;

            $("#Data").val(formid.BPC5.GetToken());    // asigno al hidden para q persista en el model ...
            $("#SampleJPG").val(formid.BPC5.GetSampleJPG());
            $("#Dedo").val(formid.BPC5.Finger);
        }
        else
            timer = setTimeout('ID_VE_onHuellaDetect()', 3000);
    }
    else
        timer = setTimeout('ID_VE_onHuellaDetect()', 3000);
}

/// identifiacion - identificacion





function ID_ID_onClickContenidoToken() {
    formid.BPC5.Tokencontent = $('input:radio[name=ContenidoToken]:checked').val();
}

function ID_ID_onChangeDevice() {
    switch ($("#Device").val()) {
        case "1":
            $("#Modelo").val("4000");
            break;
        case "2":
            $("#Modelo").val("FDU02");
            break;
        case "7":
            $("#Modelo").find("option:selected").removeAttr("selected");
            break;
        default:
    }
}

function ID_ID_onChangeAlgoritmo() {
    if ($("#Algoritmo").val() == "7") {
        $("#Umbral").val("63");
    }
    else {
        $("#Umbral").val("0");
    }
}



function ID_ID_SensorOk() {
    if (formid.BPC5) {
        var sensorInfoCheck = null;
        try {
            sensorInfoCheck = formid.BPC5.GetInfo()
        } catch (e) {
        }

        if (sensorInfoCheck != null) {
            if (formid.BPC5.GetInfo().split('|')[1].length > 0) {
                huboError = false;
                ID_ID_Cargar();
                return true;
            }
            else
                mensajeError = "No se detecta el sensor de huella conectado al ordenador.";
        }
        else
            mensajeError = "Error al intentar acceder al control de lectura.Si usa Explorer pruebe agregar este sitio a los de confianza";
    }
    else
        mensajeError = "El control de lectura de huella no está instalado.";

    if (huboError)
        return false;
    //ShowError(mensajeError);
}

function ID_ID_Cargar() {
    //SetBioPortalClient(); // simula el boton de carga
    timer = setTimeout('ID_ID_onHuellaDetect()', 300);
}



// js for edit enroll

function ID_EE_Initialize() {
    ID_EE_FotoSetup();
    ID_EE_FirmaSetup();

    $("input[name='Verificado']").click(function (e) { return ID_EE_onClickVerificado(); });

    $("input[name='Verificado']").each(
        function () {
            if ($(this).is(':checked')) {
                $("#OtroVerificadoText").val($(this).val());
            }
        });

    $("form").submit(function (event) {
        return ID_EE_onSubmit();
    });
}



function ID_EE_onClickVerificado() {
    if ($('input:radio[name=Verificado]:checked').val() == "NV") {
        $("#OtroVerificadoText").val("NV");
    }
    if ($('input:radio[name=Verificado]:checked').val() == "VCI") {
        $("#OtroVerificadoText").val("VCI");
    }
    if ($('input:radio[name=Verificado]:checked').val() == "OTRO") {
        $("#OtroVerificadoText").val("");
    }
}

function ID_EE_SetImages() {
    if (document.getElementById("AxFirma").GetImagenB64() != undefined) {
        $("#Firma").val(document.getElementById("AxFirma").GetImagenB64());
    }

    if (document.getElementById("AxFoto").GetImagenB64() != undefined) {
        $("#Foto").val(document.getElementById("AxFoto").GetImagenB64());
    }
}

function ID_EE_FotoSetup() {
    try {
        formid.AxFoto.Init(1);
        formid.AxFoto.SetSizeRet(140, 140);

        if ($("#Foto").val().length > 0) {
            formid.AxFoto.SetImage($("#Foto").val());
        }
    } catch (e) {
    }
}

function ID_EE_FirmaSetup() {
    try {
        formid.AxFirma.Init(2);
        formid.AxFirma.SetSizeRet(140, 70);

        if ($("#Firma").val().length > 0) {
            formid.AxFirma.SetImage($("#Firma").val());
        }
    } catch (e) {
    }
}





//  log de transacciones

function LO_TR_Initialize() {
    $("#FechaDesde").mask("99/99/9999");
    $("#FechaHasta").mask("99/99/9999");

    $("input[name='NotSelectDate']").click(function (e) { return LO_TR_onClickSelectDate(); });
}

function LO_TR_onClickSelectDate() {
    $("#FechaDesde").prop('disabled', !$("input[name='NotSelectDate']").prop('checked'));
    $("#FechaHasta").prop('disabled', !$("input[name='NotSelectDate']").prop('checked'));
}

function PopulateModal() { }

function LO_TR_SetPagina(pagina) {
    $("#Paginacion_Pagina").val(pagina);
}

//$(".datePicker").datepicker({
//    dateFormat: 'dd/mm/yy',
//    weekStart: 0,
//    language: "es",
//    changeMonth: true,
//    changeYear: true,
//});

//$(function () {
//    //Array para dar formato en español
//    $.datepicker.regional['ar'] =
//    {
//        closeText: 'Cerrar',
//        prevText: 'Previo',
//        nextText: 'Próximo',

//        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
//        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
//        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
//        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
//        monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
//        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
//        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
//        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
//        dateFormat: 'dd/mm/yy', firstDay: 0,
//        initStatus: 'Selecciona la fecha', isRTL: false
//    };
//    $.datepicker.regional['ar'];
//});

function LO_OP_Initialize() {
    $("#FechaDesde").mask("99/99/9999");
    $("#FechaHasta").mask("99/99/9999");
}


//$(".datePicker").datepicker({
//    dateFormat: 'dd/mm/yy',
//    weekStart: 0,
//    language: "es",
//    changeMonth: true,
//    changeYear: true,
//});

//$(function () {
//    //Array para dar formato en español
//    $.datepicker.regional['ar'] =
//    {
//        closeText: 'Cerrar',
//        prevText: 'Previo',
//        nextText: 'Próximo',

//        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
//        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
//        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
//        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
//        monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
//        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
//        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
//        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
//        dateFormat: 'dd/mm/yy', firstDay: 0,
//        initStatus: 'Selecciona la fecha', isRTL: false
//    };
//    $.datepicker.regional['ar'];
//});