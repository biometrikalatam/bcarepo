﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BCR.Bio.Domain.NV
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class EmailAddressListAttribute : DataTypeAttribute
    {
        #region privates
        private readonly EmailAddressAttribute _emailAddressAttribute = new EmailAddressAttribute();
        #endregion

        #region ctor
        public EmailAddressListAttribute() : base(DataType.EmailAddress) { }
        #endregion

        #region Overrides
        /// <summary>
        /// Verifica si el valor es válido
        /// </summary>
        /// <param name="value">Lista de emails a validar</param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            string emailAddr = Convert.ToString(value);
            if (string.IsNullOrWhiteSpace(emailAddr)) return false;

            //lets test for mulitple email addresses
            string[] emails = emailAddr.Split(new[] { ';', ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
            return emails.All(t => _emailAddressAttribute.IsValid(t));
        }
        #endregion

    }
}