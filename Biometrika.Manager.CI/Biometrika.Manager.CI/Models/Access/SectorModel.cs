﻿using BCR.Bio.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebNV.Models.Access
{
    public class SectorMetaData
    {
        [HiddenInput(DisplayValue = true)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Description")]
        [StringLength(100)]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Contacto ")]
        [StringLength(80)]
        public string ContactName { get; set; }

        [Required]
        [Display(Name = "External Id ")]
        [StringLength(80)]
        public string externalid { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha de creación")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateDate { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Baja")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> EndDate { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> UpdateDate { get; set; }
    }

    [MetadataType(typeof(SectorMetaData))]
    public class SectorModelx : Sector
    {
        public List<CheckedViewModel> CompanyList { get; set; }
    }
}