﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebNV.Models.Access
{
    public class PointModelMetaData
    {
        [Editable(false)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
      
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "IPaddress")]
        public string IPaddress { get; set; }

        [Required]
        [Display(Name = "SensorId")]
        public string SensorId { get; set; }

        [Required]
        [Display(Name = "MacAddress")]
        public string MacAddress { get; set; }

        [Required]
        [Display(Name = "Posee impresora")]
        public int HasPrinter { get; set; }

        [Editable(false)]
        public  DateTime? CreateDate { get; set; }
    }

    [MetadataType(typeof(PointModelMetaData))]
    public class PointModel : Point
    {
        //[Display(Name = "Compañia")]
        //public List<SelectListItem> CompanyList
        //{
        //    get;
        //    set;
        //}

        [Display(Name = "Sector")]
        public List<SelectListItem> SectorList
        {
            get;
            set;
        }

        [Display(Name = "Sedes")]
        public List<SelectListItem> HQList
        {
            get;
            set;
        }

        [HiddenInput()]
        public ICompany Company 
        {
            get;
            set;
        }

        public int HQId
        {
            get;
            set;
        }

        public int SectorId
        {
            get;
            set;
        }
    }
}
