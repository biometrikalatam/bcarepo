﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using System.Collections.Generic;
using System.Web.Mvc;
 
namespace WebNV.Models.Access
{
    public class PointListModel
    {
        public List<SelectListItem> Companies
        {
            get;
            set;
        }

        public ICompany Company { get; set; }

        public int CompanyValue
        {
            get;
            set;
        }

        
    }
}