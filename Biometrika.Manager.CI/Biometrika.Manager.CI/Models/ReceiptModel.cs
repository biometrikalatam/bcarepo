﻿using System;

namespace WebNV.Models
{
    public class ReceiptModel
    {
        public int Id { get; set; }
        public string TrackIdCI { get; set; }
        public string IDCardImage { get; set; }
        public string IDCardPhotoImage { get; set; }
        public string IDCardSignatureImage { get; set; }
        public string Selfie { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime IssueDate { get; set; }
        public string Sex { get; set; }
        public string Nacionality { get; set; }
        public string Serial { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string GeoRef { get; set; }
        public string WorkStationID { get; set; }
        public string ValidityType { get; set; }
        public string PDFCICertify { get; set; }
        public string QRCIVerify { get; set; }
        public double Score { get; set; }
        public string Country { get; set; }
        public string Ip { get; set; }
    }
}