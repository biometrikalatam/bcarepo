﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNV.Models
{
    public class ExtensionsXMLModel
    {
        public Dictionary<string, string> Extensions { get; set; }
        public int ReceiptId { get; set; }
        public bool HasPDF { get; set; }
    }
}