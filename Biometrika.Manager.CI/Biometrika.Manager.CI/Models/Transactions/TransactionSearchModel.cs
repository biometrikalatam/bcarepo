﻿using BCR.Bio.Domain.NV;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebNV.Models.Transactions
{
    public class TransactionSearchModel
    {
        private int companyValue = -1;

        [Display(Name = "Compañia")]
        public int CompanyValue
        {
            get
            {
                return companyValue;
            }
            set
            {
                this.companyValue = value;
            }
        }

        /// <summary>
        /// DDL de Compañias
        /// </summary>
        public List<SelectListItem> CompanyList
        {
            get;
            set;
        }

        public IList<Recibo> Recibos { get; set; }
    }
}