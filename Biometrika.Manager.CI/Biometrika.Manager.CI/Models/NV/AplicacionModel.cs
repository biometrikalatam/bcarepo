﻿using BCR.Bio.Domain.NV;
using System.Collections.Generic;
using System.Web.Mvc;

namespace WebNV.Models.NV
{
    public class AplicacionModel
    {
        public List<SelectListItem> CompanyList { get; set; }
        public int Company { get; set; }
        public string CompanyName { get; set; }
        public Aplicacion Aplicacion { get; set; }
 
    }
}