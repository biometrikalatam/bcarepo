﻿using BCR.Bio.Domain.NV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebNV.Models.NV
{
    public class ExtensionModel
    {
        public  Extension Extension { get; set; }

        public List<SelectListItem> AplicacionList { get; set; }

        public int Aplicacion { get; set; }
    }
}