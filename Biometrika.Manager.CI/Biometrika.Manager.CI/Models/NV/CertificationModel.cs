﻿using BCR.Bio.Domain.NV;
using System.ComponentModel.DataAnnotations;

namespace WebNV.Models.NV
{
    /// <summary>
    /// Modelo para iniciar nueva solicitud de certificación
    /// </summary>
    public class CertificationModel
    {
        [Required]
        public TypeId TypeId { get; set; }

        [Required]
        [ChileanRut(ErrorMessage = "Ingrese un rut válido")]
        [RegularExpression(@"^(\d{1,3}(\.?\d{3}){2})\-?([\dkK])$", ErrorMessage = "Ingrese un rut válido")]
        public string ValueId { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Range(900000000, 999999999)]
        public int? Phone { get; set; }

        [Required]
        [RegularExpression(@"^(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*(;|,)\s*|\s*$))*$",
         ErrorMessage = "Lista de emails incorrecta, favor revisar.")]
        public string ReceiverEmail { get; set; }

        [Required]
        public bool OnBoarding { get; set; }
    }


    public enum TypeId
    {
        RUT,
        PAS
    }
}