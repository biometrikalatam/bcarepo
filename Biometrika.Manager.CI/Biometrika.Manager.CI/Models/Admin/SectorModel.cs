﻿using BCR.Bio.Domain;
using System.ComponentModel.DataAnnotations;

namespace WebNV.Models.Admin
{
    public class SectorMetaData
    {
        [Editable(false)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Descripción")]
        public string Description { get; set; }

        [Display(Name = "Contacto")]
        public string ContactName { get; set; }
    }

    [MetadataType(typeof(SectorMetaData))]
    public class SectorModel : Sector
    {
        public string Back { get; set; }

        public int CompanyId { get; set; }

        public string CompanyName { get; set; }
    }
}