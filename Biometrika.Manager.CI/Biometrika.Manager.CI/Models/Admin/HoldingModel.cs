﻿using BCR.Bio.Domain;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebNV.Models.Admin
{
    public class HoldingMetaData
    {
        [HiddenInput(DisplayValue = true)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Dirección")]
        [StringLength(80)]
        public string Address { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha de creación")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateDate { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Baja")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> Endate { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> UpdateDate { get; set; }
    }

    [MetadataType(typeof(HoldingMetaData))]
    public class HoldingModel : Holding
    {
    }
}