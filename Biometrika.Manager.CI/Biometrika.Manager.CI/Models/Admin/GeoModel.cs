﻿using BCR.Bio.Domain.Interface;
using System.Collections.Generic;

namespace WebNV.Models.Admin
{
    public class GeoModel
    {
        public List<ICountry> Countries { get; set; }
    }
}