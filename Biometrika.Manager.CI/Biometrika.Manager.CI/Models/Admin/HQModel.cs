﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebNV.Models.Admin
{
    public class HQMetaData
    {
        [Editable(false)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Descripción")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Dirección")]
        public string Address { get; set; }

        [Display(Name = "Teléfono")]
        public string Phone { get; set; }

        [Display(Name = "Contacto")]
        public string Contact { get; set; }


    }

    [MetadataType(typeof(HQMetaData))]
    public class HQModel : HQ
    {

        public string Back { get; set; }

        public int CompanyId { get; set;  }

        public string CompanyName { get; set; }

        public int TownId { get; set; }

        public string TownName { get; set;  }

        public List<ICountry> Countries { get; set; }
    }
}