﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebNV.Models.Admin
{
    public class RolModel
    {
        [Required(ErrorMessage = "Ingresar rol")]
        [Display(Name = "Rol")]
        public string RolName { get; set; }

    }
}