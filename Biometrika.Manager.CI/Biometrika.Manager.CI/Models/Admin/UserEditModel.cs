﻿using BCR.Bio.Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebNV.Models.Admin
{
    /// <summary>
    ///
    /// </summary>
    public class UserEditModelMetaData
    {
        public int Id { get; set; }

        [Display(Name = "Nombre Usuario")]
        public string Username { get; set; }

        [StringLength(20, ErrorMessage = "Longitud mínima para {0} es de {2}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        //[Display(Name = "Empresa")]
        //public Company CompanyId { get; set; }

        //Seleccione la empresa a la cual pertenece el usuario
        public string Company { get; set; }

        //Ingrese datos opcionales

        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Apellidos")]
        public string Surname { get; set; }

        [Display(Name = "Teléfono")]
        public string Phone { get; set; }

        [Display(Name = "Código Postal")]
        public string PostalCode { get; set; }

        [ScaffoldColumn(false)]
        public string PasswordSalt { get; set; }
    }

    [MetadataType(typeof(UserEditModelMetaData))]
    public class UserEditModel : User
    {
        [DataType(DataType.Password)]
        [Display(Name = "Repita Password")]
        //[Compare("Password", ErrorMessage = "No coinciden las passwords.")]
        public string ConfirmPassword { get; set; }

        private int companyValue = -1;

        /// <summary>
        /// ID de seleccionado
        /// Se decora el identificador
        /// </summary>
        [Required(ErrorMessage = "Ingresar Compañia")]
        [Display(Name = "Compañia")]
        public int CompanyValue
        {
            get;
            set;
        }

        private string rol = string.Empty;

        /// <summary>
        ///
        /// </summary>
        [Required(ErrorMessage = "Ingresar rol")]
        [Display(Name = "Rol")]
        public string Rol
        {
            get
            {
                return rol;
            }
            set
            {
                this.rol = value;
                //this.Pais = SelectListPais.GetPaisByID(value);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public List<SelectListItem> RolList
        {
            get;
            set;
        }

        /// <summary>
        ///
        /// </summary>
        public string Notification { get; set; }

        public List<SelectListItem> CompanyList { get; set; }
    }
}