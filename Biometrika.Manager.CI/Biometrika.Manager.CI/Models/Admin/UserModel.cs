﻿using BCR.Bio.Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebNV.Models.Admin
{
    public class UserModelMetaData
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Nombre Usuario")]
        public string Username { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Longitud mínima para {0} es de {2}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Longitud mínima para {0} es de {2}.", MinimumLength = 6)]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail no válido")]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Longitud mínima para {0} es de {2}.", MinimumLength = 6)]
        [Display(Name = "Pregunta de Seguridad")]
        public string PasswordQuestion { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Longitud mínima para {0} es de {2}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Respuestas")]
        public string PasswordAnswer { get; set; }

        //Seleccione la empresa a la cual pertenece el usuario
        [Display(Name = "Empresa")]
        public string Company { get; set; }

        //Ingrese datos opcionales

        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Apellidos")]
        public string Surname { get; set; }

        [Display(Name = "Teléfono")]
        public string Phone { get; set; }

        [Display(Name = "Código Postal")]
        public string PostalCode { get; set; }

        [ScaffoldColumn(false)]
        public string PasswordSalt { get; set; }
    }

    [MetadataType(typeof(UserModelMetaData))]
    public class UserModel : User
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Repita Password")]
        //[Compare("Password", ErrorMessage = "No coinciden las passwords.")]
        public string ConfirmPassword { get; set; }

        private int companyValue = -1;

        /// <summary>
        /// ID de seleccionado
        /// Se decora el identificador
        /// </summary>
        [Required(ErrorMessage = "Ingresar Compañia")]
        [Display(Name = "Compañia")]
        public int CompanyValue
        {
            get
            {
                return companyValue;
                if (this.Company != null)
                    return this.Company.Id;
                else
                    return -1;
            }
            set
            {
                this.companyValue = value;
                //this.Company = LocalDatabase.GetCompanyById(value);
            }
        }

        /// <summary>
        /// DDL de Compañias
        /// </summary>
        public List<SelectListItem> CompanyList
        {
            get;
            set;
        }

        private string rol = string.Empty;

        /// <summary>
        ///
        /// </summary>
        [Required(ErrorMessage = "Ingresar rol")]
        [Display(Name = "Rol")]
        public string Rol
        {
            get
            {
                return rol;
            }
            set
            {
                this.rol = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public List<SelectListItem> RolList
        {
            get;
            set;
        }

        /// <summary>
        ///
        /// </summary>
        public string Notification { get; set; }

        /// <summary>
        ///
        /// </summary>
        public bool IsReadOnly { get; set; }

        /// <summary>
        ///
        /// </summary>
        public string Domain { get; set; }
    }
}