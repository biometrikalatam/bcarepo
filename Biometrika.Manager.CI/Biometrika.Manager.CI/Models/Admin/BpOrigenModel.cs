﻿using BCR.Bio.Domain;
using System.ComponentModel.DataAnnotations;

namespace Bio.Manager.WebSite.Models.Admin
{
    /// <summary>
    /// Metadata de Origen
    /// </summary>
    public class BpOrigenMetaData
    {
        [Required]
        [Range(50,5000)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Descripción")]
        public string Description { get; set; }
    }

    /// <summary>
    /// Model de Origen
    /// </summary>
    [MetadataType(typeof(BpOrigenMetaData))]
    public class BpOrigenModel : BpOrigin
    {
    }
}