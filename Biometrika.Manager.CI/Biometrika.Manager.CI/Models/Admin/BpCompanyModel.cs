﻿using BCR.Bio.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebNV.Models.Admin
{
    public class BpCompanyMetaData
    {
        [HiddenInput(DisplayValue = true)]
        public int Id { get; set; }

        [Required]
        [RegularExpression(@"^0*(\d{1,9})\-?([\dkK])$", ErrorMessage = "Error en el ingreso del RUT/RUN")]
        [StringLength(10)]
        public string Rut { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Dominio")]
        [StringLength(15)]
        public string Domain { get; set; }

        [Required]
        [Display(Name = "Dirección")]
        [StringLength(80)]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Teléfono")]
        [RegularExpression("[0-9]{1,}", ErrorMessage = "Ingrese números solamente")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(20)]
        public string Phone { get; set; }

        [Display(Name = "Teléfono 2")]
        [RegularExpression("[0-9]{1,}", ErrorMessage = "Ingrese números solamente")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(20)]
        public string Phone2 { get; set; }

        [Display(Name = "Fax")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("[0-9]{1,}", ErrorMessage = "Ingrese números solamente")]
        [StringLength(20)]
        public string Fax { get; set; }

        [Display(Name = "Nombre contacto")]
        [StringLength(50)]
        public string ContactName { get; set; }

        [Display(Name = "Estado")]
        [UIHint("ActivoSiNo")]
        public bool Status { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha de creación")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateDate { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Baja")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> EndDate { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> UpdateDate { get; set; }

        //[ScaffoldColumn(false)]
        //public string Additionaldata { get; set; }

        //[ScaffoldColumn(false)]
        //[DataType(DataType.DateTime)]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //public Nullable<DateTime> Laststatusdate { get; set; }

        //[ScaffoldColumn(false)]
        //public bool IsChanged { get; set; }

        //[ScaffoldColumn(false)]
        //public bool IsDeleted { get; set; }
    }

    [MetadataType(typeof(BpCompanyMetaData))]
    public class BpCompanyModel : Company
    {
        [Display(Name = "Holding")]
        public List<SelectListItem> HoldingList
        {
            get;
            set;
        }

        [Display(Name = "Persistir falsos")]
        public bool PersistFalseVerification { get; set; }

        [Display(Name = "Sign TSA")]
        public bool SignWithTSA { get; set; }
    }
}