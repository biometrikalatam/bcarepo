﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace WebNV.Models.Admin
{
    /// <summary>
    ///
    /// </summary>
    public class UserSearchModel
    {


        /// <summary>
        ///
        /// </summary>
        public IEnumerable<UserModel> UserModelList { get;  set; }

        /// <summary>
        /// DDL de Compañias
        /// </summary>
        public List<SelectListItem> CompanyList
        {
            get;
            set;
        }

        
    }
}