﻿using AutoMapper;
using BCR.Bio.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebNV.Models.Access;
using WebNV.Models.Admin;

namespace WebNV.App_Start
{
    public class AutomapperConfig
    {
        public static void RegisterMaps()
        {
            Mapper.CreateMap<Company, BpCompanyModel>();
            Mapper.CreateMap<BpCompanyModel, Company>();

            //Mapper.CreateMap<BpOrigin, BpOrigenModel>();
            //Mapper.CreateMap<BpOrigenModel, BpOrigin>();

            Mapper.CreateMap<User, UserModel>();
            Mapper.CreateMap<UserModel, User>();

            Mapper.CreateMap<User, UserEditModel>();
            Mapper.CreateMap<UserEditModel, User>();

            Mapper.CreateMap<Holding, HoldingModel>();
            Mapper.CreateMap<HoldingModel, Holding>();

            Mapper.CreateMap<HQ, HQModel>();
            Mapper.CreateMap<HQModel, HQ>();

            Mapper.CreateMap<Point, PointModel>();
            Mapper.CreateMap<PointModel, Point>();

            Mapper.CreateMap<Sector, SectorModel>();
            Mapper.CreateMap<SectorModel, Sector>();
        }
    }
}