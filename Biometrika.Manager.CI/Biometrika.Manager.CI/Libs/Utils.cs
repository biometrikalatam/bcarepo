﻿using BCR.Bio.Wsq.Decoder;
using BCR.System.Imaging;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace BCI.Libs
{
public static class Utils
{
public static DateTime? StringToDate(string strDate)
{
return new DateTime();
}

/// <summary>
///
/// </summary>
/// <param name="wsqBase64"></param>
/// <returns></returns>
public static string ConvertWSQ64ToString64(string wsqBase64)
{
byte[] wsq = Convert.FromBase64String(wsqBase64);   // pasar de base64 a wsq

WsqDecoder wsqDecoder = new WsqDecoder(); // transformar wsq a imagen
short w, h;
byte[] imageRaw;
wsqDecoder.DecodeMemory(wsq, out w, out h, out imageRaw);
Bitmap bitmap = ImageProcessor.RawToBitmap(imageRaw, w, h);

MemoryStream memoryStream = new MemoryStream();
bitmap.Save(memoryStream, ImageFormat.Bmp);
byte[] imageBytes = memoryStream.ToArray();
return Convert.ToBase64String(imageBytes);
}
}
}