﻿using System;
using BCR.Bio.Domain.NV;
using BCR.Bio.Infraestructura.NV;
using BCR.Bio.Services.Abstract;
using BCR.Bio.Domain.NV.DTO;
using System.Collections.Generic;

namespace BCR.Bio.Services.NV
{
    public class CITxService : Service<CITx>
    {
        private CITxRepository repositoryLocal;

        public CITxService(CITxRepository repository) : base(repository)
        {
            repositoryLocal = repository;
        }

        public List<CITxDTO> ListDTOByCriteria(CITxDTO param)
        {
            return repositoryLocal.ListDTOByCriteria(param);
        }

        protected override bool ValidateForCreate(CITx t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(CITx t)
        {
            return true;
        }
    }
}
