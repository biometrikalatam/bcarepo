﻿using System;
using BCR.Bio.Domain.NV;
using BCR.Bio.Infraestructura.NV;
using BCR.Bio.Services.Abstract;
using BCR.Bio.Domain.NV.DTO;
using System.Collections.Generic;

namespace BCR.Bio.Services.NV
{
    public class ReciboService : Service<Recibo>
    {
        private ReciboRepository repositoryLocal;

        public ReciboService(ReciboRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        public List<ReciboResultDTO> ListByCriteria(ReciboDTO param)
        {
             return repositoryLocal.ListByCriteria(param);
        }

        protected override bool ValidateForCreate(Recibo t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(Recibo t)
        {
            return true;
        }
    }
}