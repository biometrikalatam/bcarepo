﻿using BCR.Bio.Domain.NV;
using BCR.Bio.Infraestructura.NV;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services.NV
{
    public class AplicacionService : Service<Aplicacion>
    {
        private AplicacionRepository repositoryLocal;

        /// <summary>
        ///
        /// </summary>
        /// <param name="validationDictionary"></param>
        /// <param name="repository"></param>
        public AplicacionService(AplicacionRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        public IList<Aplicacion> ListByCriteria(int company)
        {
            return repositoryLocal.ListByCriteria(company);
        }

        protected override bool ValidateForCreate(Aplicacion t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(Aplicacion t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }
    }
}