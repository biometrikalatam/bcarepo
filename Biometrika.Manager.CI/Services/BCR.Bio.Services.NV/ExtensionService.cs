﻿using BCR.Bio.Domain.NV;
using BCR.Bio.Infraestructura.NV;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services.NV
{
    public class ExtensionService : Service<Extension>
    {
        private ExtensionRepository repositoryLocal;

        public ExtensionService(ExtensionRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        public IList<Extension> ListByCriteria(int id)
        {
            return repositoryLocal.ListByCriteria(id);
        }

        protected override bool ValidateForCreate(Extension t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(Extension t)
        {
            return true;
        }
    }
}