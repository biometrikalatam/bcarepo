﻿using System;
using System.Collections.Generic;
using BCR.Bio.Domain.NV;
using BCR.Bio.Infraestructura.NV;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services.NV
{
    public class CompanyWorkFlowService : Service<CompanyWorkFlow>
    {
        private readonly CompanyWorkFlowRepository repositoryLocal;

        public CompanyWorkFlowService(CompanyWorkFlowRepository repository) : base(repository)
        {
            repositoryLocal = repository;
        }

        public CompanyWorkFlow GetByCompanyId(int id)
        {
            return repositoryLocal.GetByCompanyId(id);
        }

        public IList<CompanyWorkFlow> GetAll()
        {
            return repositoryLocal.GetAll();
        }

        protected override bool ValidateForCreate(CompanyWorkFlow t)
        {
            t.CreateDate = DateTime.Now;
            t.UpdateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(CompanyWorkFlow t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

    }
}
