﻿using System;
using BCR.Bio.Domain.NV;
using BCR.Bio.Infraestructura.NV;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services.NV
{
   public class ConfigService : Service<Config>
    {
        private readonly ConfigRepository repositoryLocal;

        public ConfigService(ConfigRepository repository) : base(repository)
        {
            repositoryLocal = repository;
        }

        public Config GetByCompanyId(int id)
        {
            return repositoryLocal.GetByCompanyId(id);
        }

        protected override bool ValidateForCreate(Config t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(Config t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }
    }
}
