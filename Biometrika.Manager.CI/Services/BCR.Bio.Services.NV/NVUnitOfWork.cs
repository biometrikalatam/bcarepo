﻿using BCR.Bio.Infraestructura.NV;
using BCR.System.Configuration;
using BCR.System.Service;
using BCR.System.Validation;

namespace BCR.Bio.Services.NV
{
    public class NVUnitOfWork : BioUnitOfWork
    {
        public NVUnitOfWork(NhibernateParams nhibernateParams, ValidationDictionary validationDictionary)
            : base(nhibernateParams, validationDictionary)
        {
            FillServiceLocator();
        }

        private void FillServiceLocator()
        {
            ServiceLocator.Add<AplicacionRepository, AplicacionService>();
            ServiceLocator.Add<ReciboRepository, ReciboService>();
            ServiceLocator.Add<CITxRepository, CITxService>();
            ServiceLocator.Add<ConfigRepository, ConfigService>();
            ServiceLocator.Add<CompanyWorkFlowRepository, CompanyWorkFlowService>();
            ServiceLocator.Add<ExtensionRepository, ExtensionService>();
        }

        public ExtensionService ExtensionService
        {
            get
            {
                return ServiceLocator.GetService<ExtensionService>();
            }
        }

        public AplicacionService AplicacionService
        {
            get
            {
                return ServiceLocator.GetService<AplicacionService>();
            }
        }

        public ReciboService ReciboService
        {
            get
            {
                return ServiceLocator.GetService<ReciboService>();
            }
        }
        public CITxService CITxService
        {
            get
            {
                return ServiceLocator.GetService<CITxService>();
            }
        }
        public ConfigService ConfigService
        {
            get
            {
                return ServiceLocator.GetService<ConfigService>();
            }
        }

        public CompanyWorkFlowService CompanyWorkFlowService
        {
            get
            {
                return ServiceLocator.GetService<CompanyWorkFlowService>();
            }
        }
    }
}