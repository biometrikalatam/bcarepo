﻿using BCR.Bio.Domain.NV;
using BCR.Bio.Domain.NV.DTO;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace BCR.Bio.Infraestructura.NV
{
    public class ReciboRepository : Repository<Recibo>
    {
        public List<ReciboResultDTO> ListByCriteria(ReciboDTO param)
        {
            try
            {
                List<ReciboResultDTO> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Aplicacion aplicacionAlias = null;

                    var list1 = session.QueryOver<Recibo>()
                        .Left.JoinAlias(x => x.Aplicacion, () => aplicacionAlias);

                    if (string.IsNullOrEmpty(param.FechaSearch))
                    {
                        int days = int.Parse(ConfigurationManager.AppSettings["MaxDaysToSearchReceipt"]);
                        list1.And(x => x.Fecha > DateTime.Now.AddDays(-days));
                    }

                    //ICriteria crit = session.CreateCriteria(typeof(Recibo));
                    list1.Where(x => aplicacionAlias.Company.Id == param.Company.Id);

                    if (!String.IsNullOrEmpty(param.ReciboIdSearch))
                        list1.Where(x => x.Reciboid == param.ReciboIdSearch);
                    if (param.AppSearch.HasValue && param.AppSearch.Value > 0)
                        list1.Where(x => aplicacionAlias.Id == param.AppSearch.Value);

                    list1.Skip(param.paginacion.Pagina * param.paginacion.Cantidad);
                    list1.Take(param.paginacion.Cantidad);
                    param.paginacion.Total = list1.RowCount();

                    list1.OrderBy(x => x.Id).Desc();

                    List<Recibo> listRecibos = (List<Recibo>)list1.List<Recibo>();

                    list = new List<ReciboResultDTO>();
                    foreach (Recibo item in listRecibos)
                    {
                        ReciboResultDTO dto = new ReciboResultDTO();
                        dto.Id = item.Id;
                        dto.Fecha = item.Fecha;
                        dto.Descripcion = item.Descripcion;
                        dto.Reciboid = item.Reciboid;
                        dto.IPorigen = item.IPorigen;
                        dto.NombreAplicacion = item.Aplicacion.Nombre;
                        list.Add(dto);
                    }
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar ListByCriteria", e);
                return null;
            }
        }
    }
}