﻿using BCR.Bio.Domain.NV;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.NV.Mappings
{
    public class ReciboMap : ClassMap<Recibo>
    {
        public ReciboMap()
        {
            Table("[rd_recibos]");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Reciboid).Column("reciboid");
            Map(x => x.Fecha).Column("fecha");
            References(x => x.Aplicacion).Column("idaplicacion").Class(typeof(Aplicacion));
            Map(x => x.ReciboXml).Column("reciboXml").CustomSqlType("TEXT").Length(int.MaxValue);

            Map(x => x.Version).Column("version");
            Map(x => x.Descripcion).Column("descripcion");
            Map(x => x.IPorigen).Column("iporigen");
        }
    }
}