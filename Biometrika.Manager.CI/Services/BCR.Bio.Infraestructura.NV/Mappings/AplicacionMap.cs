﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.NV;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.NV.Mappings
{
    public class AplicacionMap : ClassMap<Aplicacion>
    {
        public AplicacionMap()
        {
            Table("[rd_aplicaciones]");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Nombre).Column("nombre");
            Map(x => x.Checkingen).Column("checkingen");

            Map(x => x.Appfea).Column("appfea");
            Map(x => x.Urlfea).Column("urlfea");

            Map(x => x.Qextensiones).Column("qextensiones");
            Map(x => x.Signaturetype).Column("signaturetype");

            Map(x => x.Certificatepxf).Column("certificatepxf");
            Map(x => x.Signaturepsw).Column("signaturepsw");

            Map(x => x.Certificatecer).Column("certificatecer");
            Map(x => x.Userfea).Column("userfea");

            Map(x => x.Deleted).Column("deleted");

            References(x => x.Company).Column("company").Class(typeof(Company)).Not.LazyLoad();

            HasMany(x => x.Recibos)
                .Cascade
                .None().Inverse()
                .Table("rd_recibos")
                .KeyColumn("idaplicacion");

            HasMany(x => x.Extensiones)
                .Cascade
                .None().Inverse()
                .Table("rd_extensiones")
                .KeyColumn("idaplicacion");
        }
    }
}