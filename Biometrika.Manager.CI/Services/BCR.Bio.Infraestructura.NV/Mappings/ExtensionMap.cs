﻿using BCR.Bio.Domain.NV;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.NV.Mappings
{
    public class ExtensionMap : ClassMap<Extension>
    {
        public ExtensionMap()
        {
            Table("[rd_extensiones]");
            Id(x => x.Id).GeneratedBy.Identity();

            References(x => x.Aplicacion).Column("idaplicacion").Class(typeof(Aplicacion)).Not.LazyLoad();

            Map(x => x.Nombre).Column("nombre");
            Map(x => x.Tipo).Column("tipo");
        }
    }
}