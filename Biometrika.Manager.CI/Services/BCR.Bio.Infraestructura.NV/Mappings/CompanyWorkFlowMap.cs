﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.NV;
using FluentNHibernate.Mapping;
namespace BCR.Bio.Infraestructura.NV.Mappings
{
    public class CompanyWorkFlowMap: ClassMap<CompanyWorkFlow>
    {
        public CompanyWorkFlowMap()
        {
            Table("[company_workflow]");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Column("[name]");
            Map(x => x.OnBoardingMandatory).Column("[onboardingmandatory]").CustomType<Status>();
            Map(x => x.BpWebTypeOnBoarding).Column("[bpwebtypeonboarding]");
            Map(x => x.BpWebTypeVerify).Column("[bpwebtypeverify]");
            Map(x => x.ReceiverList).Column("[receiverlist]").Nullable();
            Map(x => x.Threshold).Column("[threshold]").Nullable();
            Map(x => x.CreateDate).Column("[datecreate]");
            Map(x => x.UpdateDate).Column("[dateupdate]");
            Map(x => x.Authorization).Column("[autorization]").CustomType<Status>();
            Map(x => x.AuthorizationMessage).Column("[autorizationmessage]").Nullable();
            Map(x => x.SignerInclude).Column("[signerinclude]").CustomType<Status>();
            Map(x => x.CheckLockSrcei).Column("[checklocksrcei]").CustomType<Status>();
            Map(x => x.VideoInclude).Column("[videoinclude]").CustomType<Status>();
            Map(x => x.VideoMessage).Column("[videomessage]").Nullable();
            Map(x => x.Theme).Column("[theme]");
            Map(x => x.CheckAdult).Column("[checkadult]").CustomType<Status>();
            Map(x => x.CheckExpired).Column("[checkexpired]").CustomType<Status>();

            Map(x => x.GeoRefMandatory).Column("[georefmandatory]").CustomType<Status>();
            Map(x => x.RedirectUrl).Column("[redirecturl]").Nullable();


            References(x => x.Company).Column("[companyid]").Class(typeof(Company)).Not.LazyLoad();
        }

    }
}
