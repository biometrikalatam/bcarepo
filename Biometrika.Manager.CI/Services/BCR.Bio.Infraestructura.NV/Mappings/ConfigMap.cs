﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.NV;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.NV.Mappings
{
    public class ConfigMap : ClassMap<Config>
    {
        public ConfigMap()
        {
            Table("[Config]");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.ReceiverEmails).Column("[receiverList]");
            Map(x => x.Score).Column("[score]");
            Map(x => x.ForceOnBoarding).Column("[forceOB]");
            Map(x => x.CreateDate).Column("[createDate]");
            Map(x => x.UpdateDate).Column("[updateDate]");

            References(x => x.Company).Column("[company]").Class(typeof(Company)).Not.LazyLoad();


        }
    }
}
