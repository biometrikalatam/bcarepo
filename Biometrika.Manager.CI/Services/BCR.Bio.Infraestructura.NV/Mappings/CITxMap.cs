﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.NV;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.NV.Mappings
{
    public class CITxMap : ClassMap<CITx>
    {
        public CITxMap()
        {
            Table("[rd_CITx]");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.TrackId).Column("trackid");
            Map(x => x.ActionId).Column("actionid");
            Map(x => x.ReceiptId).Column("receiptid");
            Map(x => x.TrackIdbp).Column("trackidbp");
            Map(x => x.TrackIdde).Column("trackidde");
            Map(x => x.DateGen).Column("dategen");
            Map(x => x.ValidityType).Column("validitytype");
            Map(x => x.ValidityDays).Column("validitydays");
            Map(x => x.ResultCode).Column("resultcode");
            Map(x => x.CertifyPdf).Column("certifypdf").CustomSqlType("TEXT").Length(int.MaxValue);
            Map(x => x.QRGenerated).Column("qrgenerated").CustomSqlType("TEXT").Length(int.MaxValue);
            Map(x => x.QRValidity).Column("qrvalidity");
            Map(x => x.CompanyId).Column("companyid");
            Map(x => x.LastModify).Column("lastmodify");
            Map(x => x.DestinataryMail).Column("destinatarymail");
            Map(x => x.TypeId).Column("typeid");
            Map(x => x.ValueId).Column("valueid");
            Map(x => x.Name).Column("name");
            Map(x => x.PhaterLastname).Column("phaterlastname");
            Map(x => x.MotherLastname).Column("motherlastname");
            Map(x => x.IdCardImageFront).Column("idcardimagefront").CustomSqlType("TEXT").Length(int.MaxValue);
            Map(x => x.IDCardImageBack).Column("idcardimageback").CustomSqlType("TEXT").Length(int.MaxValue);
            Map(x => x.IdcardPhotoImage).Column("idcardphotoimage").CustomSqlType("TEXT").Length(int.MaxValue);
            Map(x => x.IdcardsSignatureImage).Column("idcardsignatureimage").CustomSqlType("TEXT").Length(int.MaxValue);
            Map(x => x.Selfie).Column("selfie").CustomSqlType("TEXT").Length(int.MaxValue);
            Map(x => x.WorkstationId).Column("workstationid");
            Map(x => x.Georef).Column("georef");
            Map(x => x.Sex).Column("sex");
            Map(x => x.BirthDate).Column("birthdate");
            Map(x => x.IssueDate).Column("issuedate");
            Map(x => x.ExprationDate).Column("exprationdate");
            Map(x => x.Serial).Column("serial");
            Map(x => x.Nacionality).Column("nacionality");
            Map(x => x.Score).Column("score");
            Map(x => x.Threshold).Column("threshold");
            Map(x => x.Map).Column("map").CustomSqlType("TEXT").Length(int.MaxValue);
            Map(x => x.UrlBpWeb).Column("urlbpweb");
            Map(x => x.ListMailsDistribution).Column("listmailsdistribution");
            Map(x => x.Cellphone).Column("cellphone");
            Map(x => x.ManualSignatureImage).Column("manualsignatureimage");
            Map(x => x.Type).Column("type");
            Map(x => x.CarRegisterImageFront).Column("carregisterimagefront");
            Map(x => x.CarRegisterImageBack).Column("carregisterimageback");
            Map(x => x.WritingImage).Column("writingimage");
            Map(x => x.Form).Column("form");
            Map(x => x.SecurityCode).Column("securitycode");
            Map(x => x.DinamicParam).Column("dinamicparam");
        }
    }
}
