﻿using BCR.Bio.Domain.NV;
using BCR.Bio.Domain.NV.DTO;
using BCR.Bio.Infraestructura.Abstract;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura.NV
{
    public class CITxRepository : Repository<CITx>
    {
        public List<CITxDTO> ListDTOByCriteria(CITxDTO param)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var list = session.QueryOver<CITx>()
                        .Where(x => x.CompanyId == param.CompanyId)
                        .And(x => x.Type == param.Type)
                        ;

                    if (!string.IsNullOrEmpty(param.TrackId))
                    {
                        list.And(x => x.TrackId == param.TrackId);
                    }
                    else
                    {
                        if (param.Id > 0)
                        {
                            list.And(x => x.Id == param.Id);
                        }
                        else
                        {
                            if (param.ReceiptId > 0)
                            {
                                list.And(x => x.ReceiptId == param.ReceiptId);
                            }
                            else
                            {
                                list.And(x => x.TypeId == param.TypeId);

                                if (!param.StatusCITx.Equals(StatusCITx.Todos))
                                {
                                    switch (param.StatusCITx)
                                    {
                                        case StatusCITx.Rechazado:
                                            list.And(x => x.ActionId < 0);
                                            break;
                                        case StatusCITx.Pendiente:
                                            list.And(x => x.ActionId.IsIn(new int[] { 1, 2, 22, 23 }));
                                            break;
                                        case StatusCITx.Listo:
                                            list.And(x => x.ActionId.IsIn(new int[] { 3, 33, 8, 88, 4, 44, 9, 99 }));
                                            break;
                                        case StatusCITx.Info:
                                            list.And(x => x.ActionId.IsIn(new int[] { 5, 6, 7 }));
                                            break;
                                    }
                                }

                                if (!string.IsNullOrEmpty(param.ValueId))
                                    list.And(x => x.ValueId == param.ValueId);
                                if (param.EventTimeFrom != null)
                                    list.And(x => x.DateGen >= param.EventTimeFrom);
                                if (param.EventTimeTo != null)
                                {
                                    DateTime dateTo = Convert.ToDateTime(param.EventTimeTo);
                                    list.And(x => x.DateGen <= dateTo.Date.AddDays(1));
                                }
                                if (!string.IsNullOrEmpty(param.Names))
                                    list.And(x => x.Name.IsLike("%" + param.Names + "%"));

                                if (!string.IsNullOrEmpty(param.LastNames))
                                    list.And(x => x.PhaterLastname.IsLike("%" + param.LastNames + "%"));

                            }

                        }
                    }



                    //list.Skip(param.Paginacion.Pagina * param.Paginacion.Cantidad);
                    //list.Take(param.Paginacion.Cantidad);
                    //param.Paginacion.Total = list.RowCount();

                    list.OrderBy(x => x.LastModify).Desc();

                    List<CITxDTO> list1 = new List<CITxDTO>();

                    List<CITx> listCITx = (List<CITx>)list.List<CITx>();

                    foreach (var item in listCITx)
                    {
                        CITxDTO dto = new CITxDTO
                        {
                            Id = item.Id,
                            ReceiptId = item.ReceiptId,
                            TypeId = item.TypeId,
                            ValueId = item.ValueId,
                            CompanyId = item.CompanyId,
                            ActionId = item.ActionId,
                            DateGen = item.DateGen,
                            FullName = item.Name + " " + item.PhaterLastname + " " + item.MotherLastname
                        };
                        if (!string.IsNullOrEmpty(item.Score))
                        {
                            string scoreString = "0";

                            if (!item.Score.Equals("0"))
                            {
                                try
                                {
                                    scoreString = item.Score.Length > 5 ? item.Score.Substring(0, 5) + "%" : item.Score + "%";
                                }
                                catch (Exception e)
                                {
                                    e.ToString();
                                }
                            }

                            dto.Score = scoreString.ToString();
                        }

                        dto.HasPdf = !string.IsNullOrEmpty(item.CertifyPdf);
                        dto.HasXml = item.ReceiptId > 0;
                        dto.CarRegisterImageFront = !string.IsNullOrEmpty(item.CarRegisterImageFront) ? item.CarRegisterImageFront : "";
                        dto.CarRegisterImageBack = !string.IsNullOrEmpty(item.CarRegisterImageBack) ? item.CarRegisterImageBack : "";
                        dto.WritingImage = !string.IsNullOrEmpty(item.WritingImage) ? item.WritingImage : "";
                        dto.Form = !string.IsNullOrEmpty(item.Form) ? item.Form : "";
                        dto.SecurityCode = !string.IsNullOrEmpty(item.SecurityCode) ? item.SecurityCode : "";
                        dto.TrackIdBp = !string.IsNullOrEmpty(item.TrackIdbp) ? item.TrackIdbp : "";
                        dto.Type = item.Type;
                        dto.TrackId = item.TrackId;
                        dto.Urlbpweb = item.UrlBpWeb;

                        switch (item.ResultCode)
                        {
                            case 1:
                                dto.StatusCITx = StatusCITx.Listo;
                                break;
                            case 2:
                                dto.StatusCITx = StatusCITx.Rechazado;
                                break;
                            default:
                                break;
                        }
                        

                        list1.Add(dto);
                    }

                    return list1;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return null;
        }
    }
}
