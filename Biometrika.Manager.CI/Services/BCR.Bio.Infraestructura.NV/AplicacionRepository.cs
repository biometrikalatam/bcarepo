﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.NV;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura.NV
{
    public class AplicacionRepository : Repository<Aplicacion>
    {

        public IList<Aplicacion> ListByCriteria(int company)
        {
            try
            {
                IList<Aplicacion> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria criteria = session.CreateCriteria(typeof(Aplicacion));
                    if (company > 0)
                        criteria.Add(Expression.Eq("Company",new Company(company)));
                    list = criteria.List<Aplicacion>(); 

                    foreach (Aplicacion aplicacion in list)
                    {
                        aplicacion.HasExtensiones = aplicacion.Extensiones.Count > 0;
                        aplicacion.HasRecibos = aplicacion.Recibos.Count > 0;
                    }
                    
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar ListByCriteria", e);
                return null;
            }
        }

        public static Aplicacion GetAplicacionById(int id)
        {
            try
            {
                Aplicacion appout;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria crit = session.CreateCriteria(typeof(Aplicacion))
                                            .Add(Expression.Eq("Id", id));
                    appout = crit.UniqueResult<Aplicacion>();
                }
                return appout;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return null;
            }
        }
    }
}