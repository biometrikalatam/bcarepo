﻿using BCR.Bio.Domain.NV;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura.NV
{
    public class CompanyWorkFlowRepository : Repository<CompanyWorkFlow>
    {
        public CompanyWorkFlow GetByCompanyId(int id)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var list = session.QueryOver<CompanyWorkFlow>()
                        .Where(x => x.Company.Id == id);
                    list.Take(1);
                    IList<CompanyWorkFlow> listConfig = list.List<CompanyWorkFlow>();
                    Log4Bio.Info("WorkFlow " + listConfig[0].Name + " encontrado correctamente");
                    return listConfig[0];
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("No se encuentra WorkFlow asociado a la compañía con Id: " + id);
                ex.ToString();
            }
            return null;
        }
        public IList<CompanyWorkFlow> GetAll()
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var list = session.QueryOver<CompanyWorkFlow>();
                    IList<CompanyWorkFlow> listConfig = list.List<CompanyWorkFlow>();
                    return listConfig;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return null;
        }
    }
}
