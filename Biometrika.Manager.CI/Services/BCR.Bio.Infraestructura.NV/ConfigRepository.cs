﻿using BCR.Bio.Domain.NV;
using BCR.Bio.Infraestructura.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura.NV
{
    public class ConfigRepository : Repository<Config>
    {

        public Config GetByCompanyId(int id)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var list = session.QueryOver<Config>()
                        .Where(x => x.Company.Id == id);
                    list.Take(1);
                    IList<Config> listConfig = list.List<Config>();
                    return listConfig[0];
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return null;
        }

    }
}
