﻿using BCR.Bio.Domain.NV;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura.NV
{
    public class ExtensionRepository : Repository<Extension>
    {
        public IList<Extension> ListByCriteria(int company)
        {
            try
            {
                IList<Extension> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Aplicacion aplicacionAlias = null;
                    
                    var list1 = session.QueryOver<Extension>()
                      .Left.JoinAlias(x => x.Aplicacion, () => aplicacionAlias);

                    if (company > 0)
                        list1.Where(x => aplicacionAlias.Company.Id == company);

                    list = list1.List<Extension>();

                    //foreach (Extension extension in list)
                    //{
                    //    aplicacion.HasExtensiones = aplicacion.Extensiones.Count > 0;
                    //    aplicacion.HasRecibos = aplicacion.Recibos.Count > 0;
                    //}
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar ListByCriteria", e);
                return null;
            }
        }
    }
}