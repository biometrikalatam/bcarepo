﻿using BCR.Bio.Domain.Abstract;
using System;
using System.ComponentModel.DataAnnotations;

namespace BCR.Bio.Domain.NV
{
    public class Config : AbstractEntity
    {
        [Required]
        [RegularExpression(@"^(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*(;|,)\s*|\s*$))*$",
         ErrorMessage = "Lista de emails incorrecta, favor revisar.")]
        public virtual string ReceiverEmails { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        [Required]
        [Range(0, 100)]
        public virtual decimal Score { get; set; }
        public virtual bool ForceOnBoarding { get; set; }
        public virtual Company Company { get; set; }

        /// <summary>
        /// Cuando no se encuentra registro de configuración asociado a la compañía se crea con datos por defecto
        /// </summary>
        /// <param name="company">Recibe un Company obtenido desde (Company)CurrentUserCompany</param>
        public Config(Company company)
        {
            ReceiverEmails = "ejemplo@empresa.com,ejemplo2@biometrika.cl";
            Score = (decimal)70.0;
            ForceOnBoarding = false;
            Company = company;
            CreateDate = DateTime.Now;
        }

        public Config() { }
    }
}
