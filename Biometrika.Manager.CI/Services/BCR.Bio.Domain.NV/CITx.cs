﻿using BCR.Bio.Domain.Abstract;
using System;

namespace BCR.Bio.Domain.NV
{
    public class CITx : AbstractEntity
    { 
        public virtual string TrackId { get; set; }
        public virtual int ActionId { get; set; }
        public virtual int ReceiptId { get; set; }
        public virtual string TrackIdbp { get; set; }
        public virtual string TrackIdde { get; set; }
        public virtual DateTime DateGen { get; set; }
        public virtual int ValidityType { get; set; }
        public virtual int ValidityDays { get; set; }
        public virtual int ResultCode { get; set; }
        public virtual string CertifyPdf { get; set; }
        public virtual string QRGenerated { get; set; }
        public virtual int QRValidity { get; set; }
        public virtual int CompanyId { get; set; }
        public virtual DateTime LastModify { get; set; }
        public virtual string DestinataryMail { get; set; }
        public virtual BCR.Bio.Domain.NV.DTO.TypeId TypeId { get; set; }
        public virtual string ValueId { get; set; }
        public virtual string Name { get; set; }
        public virtual string PhaterLastname { get; set; }
        public virtual string MotherLastname { get; set; }
        public virtual string IdCardImageFront { get; set; }
        public virtual string IDCardImageBack { get; set; }
        public virtual string IdcardPhotoImage { get; set; }
        public virtual string IdcardsSignatureImage { get; set; }
        public virtual string Selfie { get; set; }
        public virtual string WorkstationId { get; set; }
        public virtual string Georef { get; set; }
        public virtual char Sex { get; set; }
        public virtual string BirthDate { get; set; }
        public virtual string IssueDate { get; set; }
        public virtual string ExprationDate { get; set; }
        public virtual string Serial { get; set; }
        public virtual string Nacionality { get; set; }
        public virtual string Score { get; set; }
        public virtual string Threshold { get; set; }
        public virtual string Map { get; set; }
        public virtual string UrlBpWeb { get; set; }
        public virtual string ListMailsDistribution { get; set; }
        public virtual string Cellphone { get; set; }
        public virtual string ManualSignatureImage { get; set; }
        public virtual int Type { get; set; }
        public virtual string CarRegisterImageFront { get; set; }
        public virtual string CarRegisterImageBack { get; set; }
        public virtual string WritingImage { get; set; }
        public virtual string Form { get; set; }
        public virtual string SecurityCode { get; set; }
        public virtual string TaxIdCompany { get; set; }
        public virtual string DinamicParam { get; set; }
        //public virtual bool IsCertification { get; set; }
    }
}
