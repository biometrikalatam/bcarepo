﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BCR.Bio.Domain.NV.DTO
{
    public class CITxDTO
    {
        public Paginacion Paginacion { get; set; }

        /// <summary>
        /// Parametros filtro de búsqueda
        /// </summary>
        public int? Id { get; set; }
        public int? ReceiptId { get; set; }
        public TypeId TypeId { get; set; }
        public string ValueId { get; set; }
        public int CompanyId { get; set; }
        [Display(Name = "Desde")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EventTimeFrom { get; set; }
        [Display(Name = "Hasta")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EventTimeTo { get; set; }
        [MinLength(3, ErrorMessage = "Ingrese al menos 3 letras")]
        public string Names { get; set; }
        [MinLength(3, ErrorMessage = "Ingrese al menos 3 letras")]
        public string LastNames { get; set; }
        [StringLength(32, MinimumLength = 32, ErrorMessage = "Cantidad de caracteres invalida")]
        public string TrackId { get; set; }
        public StatusCITx StatusCITx { get; set; }


        /// <summary>
        /// Atributos para vista de busqueda
        /// </summary>
        public int ActionId { get; set; }
        public DateTime? DateGen { get; set; }
        public string FullName { get; set; }
        public string Score { get; set; }
        public bool HasPdf { get; set; }
        public bool HasXml { get; set; }
        public string TrackIdBp { get; set; }
        public string Urlbpweb { get; set; }
        public int Type { get; set; }
        public string Form { get; set; }
        public string CarRegisterImageFront { get; set; }
        public string CarRegisterImageBack { get; set; }
        public string WritingImage { get; set; }
        public string SecurityCode { get; set; }



    }
    public enum TypeId
    {
        RUT,
        PAS
    }

    public enum StatusCITx
    {
        Todos,
        Pendiente,
        Listo,
        Rechazado,
        Info
    }

}
