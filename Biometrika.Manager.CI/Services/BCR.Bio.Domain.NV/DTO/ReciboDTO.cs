﻿namespace BCR.Bio.Domain.NV.DTO
{
    // enviar aca los que serian los parametros de filtro
    public class ReciboDTO
    {
        public Paginacion paginacion { get; set; }

        public int? id;
        public string ReciboIdSearch;
        public int? AppSearch;
        public string FechaSearch;
        public Company Company;
    }
}