﻿using System;

namespace BCR.Bio.Domain.NV.DTO
{
    public class ReciboResultDTO
    {
        public int Id { get; set; }

        public DateTime Fecha { get; set; }

        public string Descripcion { get; set; }

        public string Reciboid { get; set; }

        public string IPorigen { get; set; }

        public string NombreAplicacion { get; set; }
    }
}