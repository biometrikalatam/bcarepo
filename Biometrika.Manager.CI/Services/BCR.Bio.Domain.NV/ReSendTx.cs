﻿using Newtonsoft.Json;

namespace BCR.Bio.Domain.NV
{
    public class ReSendTx
    {
        [JsonProperty("company")]
        public int Company { get; set; }

        [JsonProperty("trackid")]
        public string TrackId { get; set; }


    }
}
