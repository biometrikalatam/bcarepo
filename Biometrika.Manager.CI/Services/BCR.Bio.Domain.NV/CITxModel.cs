﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Domain.NV.DTO;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.NV
{
    public class CITxModel
    {
        public List<CITxDTO> List { get; set; }
        //public Nullable<DateTime> EventTimeFrom { get; set; }
        //public Nullable<DateTime> EventTimeTo { get; set; }
        /// <summary>
        ///  SuperAdmin puede seleccionar Company al buscar Certificaciones
        /// </summary>
        public int CompanyId { get; set; }
        public CITxDTO FilterParams { get; set; }
    }
}


