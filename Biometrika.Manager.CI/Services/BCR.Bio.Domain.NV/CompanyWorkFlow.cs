﻿using BCR.Bio.Domain.Abstract;
using System;
using System.ComponentModel.DataAnnotations;



namespace BCR.Bio.Domain.NV
{
    public class CompanyWorkFlow : AbstractEntity
    {
        [Required]
        public virtual Company Company { get; set; }
        [Required]
        public virtual string Name { get; set; }
        [Required]
        public virtual Status OnBoardingMandatory { get; set; }
        [Required]
        public virtual BpWebTypeOnBoardingEnum BpWebTypeOnBoarding { get; set; }
        [Required]
        public virtual BpWebTypeVerifyEnum BpWebTypeVerify { get; set; }
        [RegularExpression(@"^(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*(;|,)\s*|\s*$))*$",
        ErrorMessage = "Lista de emails incorrecta, favor revisar.")]
        public virtual string ReceiverList { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        [Required]
        [Range(0, 100)]
        public virtual decimal Threshold { get; set; }
        [Required]
        public virtual Status Authorization { get; set; }
        public virtual string AuthorizationMessage { get; set; }
        [Required]
        public virtual Status SignerInclude { get; set; }
        [Required]
        public virtual Status CheckLockSrcei { get; set; }
        [Required]
        public virtual Status VideoInclude { get; set; }
        public virtual string VideoMessage { get; set; }
        [Required]
        public virtual string Theme { get; set; }

        [Required]
        public virtual Status GeoRefMandatory { get; set; }
        public virtual string RedirectUrl { get; set; }

        public virtual Status CheckAdult { get; set; }
        public virtual Status CheckExpired { get; set; }

        
        public CompanyWorkFlow(Company company)
        {

            string authorizationMessage = "<strong>Autorización</strong><!-- * --><p>Al utilizar el servicio autorizo expresamente a Biometrika y Notario Virtual para encriptar y almacenar en sus registros mis datos personales:Nombre, RUT, fecha de nacimiento, género y patrón facial. A cotejar luego esos datos contra aquellos que se capturen en el futuro, con la finalidad de verificar mi identidad. Dichos datos no serán comunicados a terceros y podrá pedir que sean eliminados enviando mail a soporte@biometrikalatam.com</p> <hr /><!-- * --><i>*El patrón biométrico facial almacenado es una representación matemática de los puntos característicos de una imágen de la persona, que se almacena encriptada, y NO puede ser utulizado en un sistema ajeno al de Notario Virtual.</i>";
            Name = "default";
            Company = company;
            OnBoardingMandatory = Status.Desactivado;
            BpWebTypeOnBoarding = BpWebTypeOnBoardingEnum.tv3dplus;
            BpWebTypeVerify = BpWebTypeVerifyEnum.tv3d;
            Threshold = 50;
            Authorization = Status.Activado;
            AuthorizationMessage = authorizationMessage;
            SignerInclude = Status.Desactivado;
            CheckLockSrcei = Status.Desactivado;
            VideoInclude = Status.Desactivado;
            CreateDate = DateTime.Now;
            UpdateDate = DateTime.Now;
            Theme = "nv";
            GeoRefMandatory = Status.Activado;
            CheckAdult = Status.Activado;
            CheckExpired = Status.Activado;

        }

        public CompanyWorkFlow() { }
    }

    public enum BpWebTypeOnBoardingEnum
    {
        tv2d,
        tv3d,
        tv3dplus,
        tv3dstrong
    }

    public enum BpWebTypeVerifyEnum
    {
        tv2d,
        tv3d,
        tv3dplus
    }

    public enum Status
    {
        Desactivado = 0,
        Activado = 1
    }

}
