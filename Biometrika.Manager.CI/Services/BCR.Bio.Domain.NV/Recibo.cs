﻿using BCR.Bio.Domain.Abstract;
using System;

namespace BCR.Bio.Domain.NV
{
    public class Recibo : AbstractEntity
    {
        public virtual string Reciboid { get; set; }

        public virtual DateTime Fecha { get; set; }

        public virtual Aplicacion Aplicacion { get; set; }

        public virtual string ReciboXml { get; set; }

        public virtual string Version { get; set; }

        public virtual string Descripcion { get; set; }

        public virtual string IPorigen { get; set; }
    }
}