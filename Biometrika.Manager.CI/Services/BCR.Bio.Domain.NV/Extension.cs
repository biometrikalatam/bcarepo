﻿using BCR.Bio.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.NV
{
    public class Extension : AbstractEntity
    {
        public virtual Aplicacion Aplicacion { get; set; }

        public virtual string Nombre { get; set; }

        public virtual string Tipo { get; set; }
    }
}
