﻿using BCR.Bio.Domain.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.NV
{
    public class Aplicacion : AbstractEntity
    {
        public virtual string Nombre { get; set; }

        public virtual int Checkingen { get; set; }

        public virtual Company Company { get; set; }

        public virtual IList<Recibo> Recibos { get; set; }

        public virtual IList<Extension> Extensiones { get; set; }

        public virtual bool HasRecibos { get; set; }

        public virtual bool HasExtensiones { get; set; }

        public virtual int Qextensiones { get; set; }

        public virtual int Signaturetype { get; set; }

        public virtual string Certificatepxf { get; set; }

        public virtual string Signaturepsw { get; set; }

        public virtual string Certificatecer { get; set; }

        public virtual string Urlfea { get; set; }

        public virtual string Appfea { get; set; }

        public virtual string Userfea { get; set; }

        public virtual DateTime? Deleted { get; set; }
    }
}