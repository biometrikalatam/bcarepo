﻿

using Newtonsoft.Json;

namespace BCR.Bio.Domain.NV
{
    public class Certification
    {
        [JsonProperty("company")]
        public int Company { get; set; }

        [JsonProperty("application")]
        public int Application { get; set; }

        [JsonProperty("typeId")]
        public string TypeId { get; set; }

        [JsonProperty("valueId")]
        public string ValueId { get; set; }

        [JsonProperty("mailCertify")]
        public string MailCertify { get; set; }

        [JsonProperty("cellphone")]
        public string Cellphone { get; set; }

        [JsonProperty("theme")]
        public string Theme { get; set; }

        [JsonProperty("customerTrackId")]
        public string CustomerTrackId { get; set; }

        [JsonProperty("callbackurl")]
        public string Callbackurl { get; set; }

        [JsonProperty("redirecturl")]
        public string Redirecturl { get; set; }

         [JsonProperty("workflowname")]
        public string WorkFlowName { get; set; }



        //[JsonProperty("type")]
        //public string Type { get; set; }

        //[JsonProperty("signerInclude")]
        //public int SignerInclude { get; set; }

        //[JsonProperty("checklockSrcei")]
        //public int ChecklockSrcei { get; set; }

        //[JsonProperty("threshold")]
        //public decimal Threshold { get; set; }

        [JsonProperty("listMailsDestinatary")]
        public string[] ListMailsDestinatary { get; set; }

    }
}
