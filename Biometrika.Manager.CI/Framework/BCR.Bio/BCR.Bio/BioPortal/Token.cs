﻿using BCR.Bio.Enum;
using BCR.Bio.Enum.BioPortal;
using BCR.System.Crypto;
using System;

namespace BCR.Bio.BioPortal
{
    public class Token
    {
        public TokenContentEnum Content { get; set; }

        public string Wsq_Template { get; set; }

        public int Finger { get; set; }

        public string TypeId { get; set; }

        public string ValueId { get; set; }

        public string SerialSensor { get; set; }

        public string IP { get; set; }

        public MinutiaeTypeEnum MinutiaeType { get; set; }

        public DeviceEnum Device { get; set; }

        public AuthenticationFactorEnum AuthenticationFactor { get; set; }

        public OperationTypeEnum OperationType { get; set; }

        public int ErrorCode { get; set; }
        public string Token_Template { get; set; }
        public string Timestamp { get; set; }

        public string Generate(string header)
        {
            string token = null;
            ErrorCode = 0;
            try
            {
                //RUT | 21284415 - 2 | cncncncncncnc | 3 |/ 6D /| -septmpl--septmpl--septmpl -| 06092016160906 | 169.254.80.80~192.168.2.3 | 0 | 2 | 0 | 1 | 2 | 1

                string _plaintoken = TypeId + "|" +
                                     ValueId + "|" +
                                     SerialSensor + "|" +
                                     Finger + "|" +
                                     Wsq_Template + "|" +
                                     Token_Template + "|" +
                                     DateTime.Now.ToString("ddMMyyyyHHmmss") + "|" +
                                     IP + "|" +
                                     (int)Content + "|" +
                                     (int)MinutiaeType + "|0|" +
                                     (int)Device + "|" +
                                     (int)AuthenticationFactor + "|" +
                                     (int)OperationType;

                token = header + Crypto.EncryptWithSymmetricAid(Crypto.PUBLIC_KEY, _plaintoken);//Convert.ToBase64String(byencryptedtoken);
                //"AV050105"
            }
            catch (Exception ex)
            {
                ErrorCode = -1;
                token = null;
            }
            return token;
        }
    }
}