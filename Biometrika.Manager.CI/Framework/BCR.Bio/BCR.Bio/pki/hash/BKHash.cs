using System;
using System.Security.Cryptography;

namespace BCR.Bio.pki.hash
{
    /// <summary>
    /// Descripción breve de BOKhash.
    /// </summary>
    public class BKHash
    {
        public BKHash()
        {
        }

        public static Byte[] Hash(Byte[] dataToHash, string algotithm)
        {
            //Create hash value from String 1 using MD5 instance returned by Crypto Config system
            // algotithm = MD5 | ...
            byte[] hashvalue = ((HashAlgorithm)CryptoConfig.CreateFromName(algotithm)).ComputeHash(dataToHash);
            return hashvalue;
        }
    }
}