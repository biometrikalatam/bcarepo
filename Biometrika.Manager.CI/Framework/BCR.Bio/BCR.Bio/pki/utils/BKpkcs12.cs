//using System;
//using System.Collections;
//using System.Security.Cryptography.X509Certificates;
//using CAPICOM;
//using Org.Mentalis.Security.Certificates;
//using Certificate = Org.Mentalis.Security.Certificates.Certificate;
//
//namespace Bio.Core.pki.utils
//{
//	/// <summary>
//	/// Descripción breve de BOKpkcs12.
//	/// </summary>
//	public class BOKpkcs12
//	{
//
//		#region Constructores		
//		
//		public BOKpkcs12() { }
//
//		public BOKpkcs12(string path, string psw)
//		{
//			this.certificate = BOKpkcs12.BOKOpenPFX(path,psw);
//			this.cs = CertificateStore.CreateFromPfxFile(path,psw);
//		}
//		
//		#endregion Constructores		
//		
//		#region Public Methods		
//		public static ICertificate BOKOpenPFX(string path, string psw) {
//		ICertificate cRet = null;
//		ICertificate cAux = null;
//			try {
//				//Creo un storage de certificado
//				CAPICOM.StoreClass sc = new StoreClass();
//				//Lo abro en UBICACION = MEMORIA
//				sc.Open(CAPICOM.CAPICOM_STORE_LOCATION.CAPICOM_MEMORY_STORE, null ,CAPICOM.CAPICOM_STORE_OPEN_MODE.CAPICOM_STORE_OPEN_READ_ONLY);
//				//Cago en el Storage el certificado .pfx o .p12
//				sc.Load(path.Trim(),psw.Trim(),CAPICOM.CAPICOM_KEY_STORAGE_FLAG.CAPICOM_KEY_STORAGE_DEFAULT);
//
//				//Tomo el Enumerator de certificados del storage
//				IEnumerator e = sc.Certificates.GetEnumerator();
//				bool bSigue = true;
//				while (e.MoveNext() && bSigue) {
//					cAux = (ICertificate)e.Current;
//					if (cAux.HasPrivateKey())
//					{
//						cRet = cAux;
//						bSigue = false;
//					}
//				}
//				sc.Close();
//				sc = null;
//			} catch (Exception ex) {
//				Console.WriteLine(ex.Message);
//				cRet = null;
//			}
//			return cRet;
//		}
//
//		public static X509Certificate MentalisOpenPFX(string path, string psw) {
//			X509Certificate cRet = null;
//			X509Certificate cAux = null;
//			try {
////				//Creo un storage de certificado
////				CAPICOM.StoreClass sc = new StoreClass();
////				//Lo abro en UBICACION = MEMORIA
////				sc.Open(CAPICOM.CAPICOM_STORE_LOCATION.CAPICOM_MEMORY_STORE, null ,CAPICOM.CAPICOM_STORE_OPEN_MODE.CAPICOM_STORE_OPEN_READ_ONLY);
////				//Cago en el Storage el certificado .pfx o .p12
////				sc.Load(path.Trim(),psw.Trim(),CAPICOM.CAPICOM_KEY_STORAGE_FLAG.CAPICOM_KEY_STORAGE_EXPORTABLE);
////
////				//Tomo el Enumerator de certificados del storage
////				IEnumerator e = sc.Certificates.GetEnumerator();
////				bool bSigue = true;
////				while (e.MoveNext() && bSigue) {
////					cAux = (ICertificate)e.Current;
////					if (cAux.HasPrivateKey()) {
////						cRet = cAux;
////						bSigue = false;
////					}
////				}
////				sc.Close();
////				sc = null;
//				Org.Mentalis.Security.Certificates.Certificate[] col = 
//					(CertificateStore.CreateFromPfxFile(path,psw)).EnumCertificates();
//
//				for (int i=0; i<col.Length; i++) {
//					if (col[i].HasPrivateKey()) {
//						cRet = col[i].ToX509();
//					}
//				}
//				
//
//			} catch (Exception ex) {
//				Console.WriteLine(ex.Message);
//				cRet = null;
//			}
//			return cRet;
//		}
//		#endregion Public Methods		
//
//		#region Public Properties
//		public ICertificate Certificate
//		{
//			get { return certificate; }
//			set { certificate = value; }
//		}
//		#endregion Public Properties
//
//		#region Private
//		ICertificate certificate;
//		Org.Mentalis.Security.Certificates.CertificateStore cs;
//		#endregion Private
//	}
//}
