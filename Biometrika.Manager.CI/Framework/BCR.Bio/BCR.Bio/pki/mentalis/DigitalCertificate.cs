﻿using BCR.Bio.Enum.BioPortal;
using System;

//using Bio.Core.Constant;
//using log4net;

namespace BCR.Bio.pki.mentalis
{
    public class DigitalCertificate
    {
        //private static readonly ILog LOG = LogManager.GetLogger(typeof(Pkcs12));

        public DigitalCertificate()
        {
        }

        public DigitalCertificate(string certb64)
        {
            try
            {
                _certificate = Org.Mentalis.Security.Certificates.Certificate.CreateFromBase64String(certb64);
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.pki.mentalis.DigitalCertificate.DigitalCertificate Error", ex);
            }
        }

        private Org.Mentalis.Security.Certificates.Certificate _certificate;

        public Org.Mentalis.Security.Certificates.Certificate Certificate
        {
            set { _certificate = value; }
            get { return _certificate; }
        }

        public void LoadCertificateFromBase64(string certb64)
        {
            try
            {
                _certificate = Org.Mentalis.Security.Certificates.Certificate.CreateFromBase64String(certb64);
            }
            catch (Exception ex)
            {
                //LOG.Error("Error", ex);
            }
        }

        public static int Compare(DigitalCertificate cert1, DigitalCertificate cert2)
        {
            int ret = (int)Errors.IERR_OK;
            try
            {
                Errors ret_1 = cert2.Certificate.GetSerialNumberString().Equals(cert1.Certificate.GetSerialNumberString()) ?
                                    Errors.IERR_OK : Errors.IERR_CERTIFICATE_NOT_EQUAL;
                ret = (int)ret_1;
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.pki.mentalis.DigitalCertificate.Compare Error", ex);
                ret = (int)Errors.IERR_UNKNOWN;
            }
            return ret;
        }

        public static int IsNotExpired(string certb64)
        {
            int ret = (int)Errors.IERR_OK;
            try
            {
                Org.Mentalis.Security.Certificates.Certificate cert =
                    Org.Mentalis.Security.Certificates.Certificate.CreateFromBase64String(certb64);
                Errors ret_1 = cert.GetExpirationDate() >= DateTime.Now ? Errors.IERR_OK : Errors.IERR_CERTIFICATE_EXPIRED;
                ret = (int)ret_1;
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.pki.mentalis.DigitalCertificate.IsValid Error", ex);
                ret = (int)Errors.IERR_UNKNOWN;
            }

            return ret;
        }
    }
}