//using System;
//using System.Collections;
//using CAPICOM;
//
//namespace Bio.Core.pki.capicom
//{
//	public class BKpkcs12 {
//
//		#region Constructores		
//		
//		public BKpkcs12() { }
//
//		public BKpkcs12(string path, string psw) {
//			this.certificate = BKpkcs12.BKOpenPFX(path,psw);
//		}
//		
//		#endregion Constructores		
//		
//		#region Public Methods		
//		public static ICertificate BKOpenPFX(string path, string psw) {
//			ICertificate cRet = null;
//			ICertificate cAux = null;
//			try {
//				//Creo un storage de certificado
//				CAPICOM.StoreClass sc = new StoreClass();
//				//Lo abro en UBICACION = MEMORIA
//				sc.Open(CAPICOM.CAPICOM_STORE_LOCATION.CAPICOM_MEMORY_STORE, null ,CAPICOM.CAPICOM_STORE_OPEN_MODE.CAPICOM_STORE_OPEN_READ_ONLY);
//				//Cago en el Storage el certificado .pfx o .p12
//				sc.Load(path.Trim(),psw.Trim(),CAPICOM.CAPICOM_KEY_STORAGE_FLAG.CAPICOM_KEY_STORAGE_DEFAULT);
//
//				//Tomo el Enumerator de certificados del storage
//				IEnumerator e = sc.Certificates.GetEnumerator();
//				bool bSigue = true;
//				while (e.MoveNext() && bSigue) {
//					cAux = (ICertificate)e.Current;
//					if (cAux.HasPrivateKey()) {
//						cRet = cAux;
//						bSigue = false;
//					}
//				}
//				sc.Close();
//				sc = null;
//			} catch (Exception ex) {
//				Console.WriteLine(ex.Message);
//				cRet = null;
//			}
//			return cRet;
//		}
//
//
//		#endregion Public Methods		
//
//		#region Public Properties
//		public ICertificate Certificate {
//			get { return certificate; }
//			set { certificate = value; }
//		}
//		#endregion Public Properties
//
//		#region Private
//		ICertificate certificate;
//		#endregion Private
//	}
//}
