﻿using BCR.System.Attribute;

namespace BCR.Bio.Enum
{
    public enum BirTypeEnum
    {
        [StringValue("RAW")]
        RAW = 0x01,

        [StringValue("WSQ")]
        INTERMEDIATE_DATA = 0x02,

        [StringValue("Template")]
        PROCESSED_DATA = 0x04,

        [StringValue("Minucias Encriptadas")]
        ENCRYPTED = 0x08,

        [StringValue("SIGNED")]
        SIGNED = 0x10,

        [StringValue("Minucias UTF8")]
        ENCODING_UTF8 = 0x20,

        [StringValue("ENCODING_ASCII")]
        ENCODING_ASCII = 0x40
    }
}

//public const int RAW = 0x01;
//public const int INTERMEDIATE_DATA = 0x02;
//public const int PROCESSED_DATA = 0x04;
//public const int ENCRYPTED = 0x08;
//public const int SIGNED = 0x10;
//public const int ENCODING_UTF8 = 0x20;
//public const int ENCODING_ASCII = 0x40;

//public static string GetName(int type)
//{
//    string ret = null;
//    switch (type)
//    {
//        case BirType.RAW:  //Minucias
//            ret = "RAW";
//            break;
//        case BirType.PROCESSED_DATA:  //Minucias
//            ret = "Template";
//            break;
//        case BirType.INTERMEDIATE_DATA: //WSQ
//            ret = "WSQ";
//            break;
//        case BirType.PROCESSED_DATA | BirType.ENCODING_UTF8:
//            ret = "Minucias UTF8";
//            break;
//        case BirType.INTERMEDIATE_DATA | BirType.ENCODING_UTF8:
//            ret = "WSQ UTF8";
//            break;
//        case BirType.PROCESSED_DATA | BirType.ENCRYPTED:
//            ret = "Minucias Encriptadas";
//            break;
//        case BirType.INTERMEDIATE_DATA | BirType.ENCRYPTED:
//            ret = "WSQ Encriptadas";
//            break;
//        default:
//            ret = "Desconocido";
//            break;

//    }
//    return ret;