﻿using BCR.System.Attribute;

namespace BCR.Bio.Enum
{
    public enum DeviceEnum
    {
        [StringValue("Cualquiera")]
        DEVICE_ANY = -2,

        [StringValue("Todos")]
        DEVICE_ALL = -1,

        [StringValue("Ninguno")]
        DEVICE_NONE = 0,

        [StringValue("Digital Persona")]
        DEVICE_DIGITALPERSONA = 1,

        [StringValue("Secugen")]
        DEVICE_SECUGEN = 2,

        [StringValue("Crossmatch")]
        DEVICE_CROSSMATCH = 3,

        [StringValue("Testech")]
        DEVICE_TESTECH = 4,

        [StringValue("Sagem")]
        DEVICE_SAGEM = 5,

        [StringValue("Identix")]
        DEVICE_IDENTIX = 6,

        [StringValue("Upek")]
        DEVICE_UPEK = 7,

        [StringValue("Cogent")]
        DEVICE_COGENT = 8,

        [StringValue("Handkey")]
        DEVICE_HANDKEY = 30,

        [StringValue("Facial")]
        DEVICE_FACIAL = 40,

        [StringValue("Iris")]
        DEVICE_IRIS = 50,

        [StringValue("Retina")]
        DEVICE_RETINA = 60
    }
}