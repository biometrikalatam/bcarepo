﻿using BCR.Bio.Enum.BioPortal;
using BCR.System.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Enum
{
    public static partial class EnumExtension
    {
        public static string GetDescription(this MinutiaeTypeEnum enumObject)
        {
            return EnumHelper.GetDescription(enumObject);
        }

        public static string GetDescription(this AuthenticationFactorEnum enumObject)
        {
            return EnumHelper.GetDescription(enumObject);
        }
    }
}
