﻿using BCR.System.Attribute;

namespace BCR.Bio.Enum.BioPortal
{
    public enum ActionEnum
    {
        [StringValue("Cualquiera")]
        ACTION_ANY = -2,

        [StringValue("Todos")]
        ACTION_All = -1,

        [StringValue("Ninguno")]
        ACTION_NONE = 0,

        [StringValue("Verify")]
        ACTION_VERIFY = 1,

        [StringValue("Identify")]
        ACTION_IDENTIFY = 2,

        [StringValue("Enroll")]
        ACTION_ENROLL = 3,

        [StringValue("Get")]
        ACTION_GET = 4,

        [StringValue("Modify")]
        ACTION_MODIFY = 5,

        [StringValue("Delete")]
        ACTION_DELETE = 6,

        [StringValue("Add BIR")]
        ACTION_ADDBIR = 7,

        [StringValue("Delete BIR")]
        ACTION_DELETEBIR = 8,

        [StringValue("Verify y Get")]
        ACTION_VERIFYANDGET = 9,

        [StringValue("Get Solo Datos")]
        ACTION_GETDATA = 10,

        [StringValue("Get Solo Foto")]
        ACTION_GETPHOTO = 11,

        [StringValue("Get Solo Firma")]
        ACTION_GETSIGNATURE = 12,

        [StringValue("Get Actions")]
        ACTION_GETACTIONS = 20,

        [StringValue("Get Origin")]
        ACTION_GETORIGINS = 21,

        [StringValue("Get Matchers Availables")]
        ACTION_GETMATCHESRAVAILABLES = 22,

        [StringValue("Get Connectors Availables")]
        ACTION_GETCONNECTORSAVAILABLES = 23,

        [StringValue("Get Authentication Factor Availables")]
        ACTION_GETAUTHENTICATIONFACTORSAVAILABLES = 24,

        [StringValue("Get Minutiae Types Availables")]
        ACTION_GETMINUTIAETYPESAVAILABLES = 25,

        [StringValue("Get Birs For Sync")]
        ACTION_SYNC_GETBIRS = 100,

        [StringValue("Get Birs For Sync")]
        ACTION_SYNC_ADDBIRS = 101,

        [StringValue("ACTION_VERIFY_BIOSIGNATURE")]
        ACTION_VERIFY_BIOSIGNATURE = 102
    }
}