﻿using BCR.System.Attribute;

namespace BCR.Bio.Enum.BioPortal
{
    public enum AuthenticationFactorEnum
    {
        [StringValue("Cualquiera")]
        AUTHENTICATIONFACTOR_ANY = -2,

        [StringValue("Todos")]
        AUTHENTICATIONFACTOR_All = -1,

        [StringValue("Ninguno")]
        AUTHENTICATIONFACTOR_NONE = 0,

        [StringValue("Clave")]
        AUTHENTICATIONFACTOR_PASSWORD = 1,

        [StringValue("Huella Digital")]
        AUTHENTICATIONFACTOR_FINGERPRINT = 2,

        [StringValue("Geometria de la Mano")]
        AUTHENTICATIONFACTOR_HANDGEOMETRY = 3,

        [StringValue("Facial")]
        AUTHENTICATIONFACTOR_FACIAL = 4,

        [StringValue("Voz")]
        AUTHENTICATIONFACTOR_VOICE = 5,

        [StringValue("Iris")]
        AUTHENTICATIONFACTOR_IRIS = 6,

        [StringValue("Retina")]
        AUTHENTICATIONFACTOR_RETINA = 7,

        [StringValue("Imagen termal facial")]
        AUTHENTICATIONFACTOR_THERMALFACEIMAGE = 8,

        [StringValue("Imagen termal de mano")]
        AUTHENTICATIONFACTOR_THERMALHANDIMAGE = 9,

        [StringValue("Mapa de venas de manos")]
        AUTHENTICATIONFACTOR_HANDVEINSMAP = 10,

        [StringValue("PKI")]
        AUTHENTICATIONFACTOR_PKI = 11
    }
}