﻿using BCR.System.Attribute;

namespace BCR.Bio.Enum.BioPortal
{
    public enum OperationOrderEnum
    {
        [StringValue("Cualquiera")]
        OPERATIONORDER_ANY = -2,

        [StringValue("Todos")]
        OPERATIONORDER_All = -1,

        [StringValue("Default")]
        OPERATIONORDER_DEFAULT = 0,

        [StringValue("Local Solo")]
        OPERATIONORDER_LOCALONLY = 1,

        [StringValue("Local Primero")]
        OPERATIONORDER_LOCALFIRST = 2,

        [StringValue("Remoto Solo")]
        OPERATIONORDER_REMOTEONLY = 3
    }
}
