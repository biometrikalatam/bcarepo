﻿using BCR.System.Attribute;

namespace BCR.Bio.Enum.BioPortal
{
    /// <summary>
    /// Determina en que orden se hace
    /// </summary>
    public enum InsertOptionEnum
    {
        [StringValue("Default del Config")]
        Default_del_Config = 0,

        [StringValue("Enroll")]
        Enroll = 1,

        [StringValue("No Enroll")]
        No_Enroll = 2
    }
}