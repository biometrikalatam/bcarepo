using BCR.System.Attribute;

namespace BCR.Bio.Enum.BioPortal
{
    public enum Errors
    {
        [StringValue("Funcionamiento Correcto")]
        IERR_OK = 0,

        [StringValue("Error Desconocido")]
        IERR_UNKNOWN = -1,

        [StringValue("Reintente")]
        IERR_REINTENT = -2,

        [StringValue("Licencia Erronea")]
        IERR_LICENSE = -3,

        [StringValue("Error de Base de Datos")]
        IERR_DATABASE = -4,

        [StringValue("RUT Invalido")]
        IERR_RUT_INVALID = -5,

        [StringValue("Parametro Erroneo")]
        IERR_BAD_PARAMETER = -6,

        [StringValue("Error en la conexón al web service")]
        IERR_CONX_WS = -7,

        [StringValue("Error normalizando data")]
        IERR_NORMALIZING_DATA = -8,

        [StringValue("Error serializando data")]
        IERR_SERIALIZING_DATA = -9,

        [StringValue("Error deserializando data")]
        IERR_DESERIALIZING_DATA = -10,

        [StringValue("Error grabando transaccion")]
        IERR_SAVING_TX = -11,

        [StringValue("Error grabando data verificada")]
        IERR_SAVING_VERFIED = -12,

        [StringValue("Id Acción inconsistente para este servicio")]
        IERR_INCONSISTENT_ACTION = -13,

        [StringValue("Acción no soportada en esta version")]
        IERR_ACTION_NOT_SUPPORTED = -14,

        [StringValue("Compañia no existente")]
        IERR_COMPANY_NOT_EXIST = -15,

        //30
        [StringValue("No hay datos locales")]
        IERR_NO_LOCAL_DATA = -30,

        [StringValue("Identidad no encontrada")]
        IERR_IDENTITY_NOT_FOUND = -31,

        [StringValue("No hay datos para realizar la comparacion")]
        IERR_BIR_NOT_FOUND = -32,

        [StringValue("Ya existe la identidad")]
        IERR_IDENTITY_EXIST = -33,

        //40
        [StringValue("WSQ Invalido")]
        IERR_WSQ_INVALID = -40,

        [StringValue("WSQ de baja calidad")]
        IERR_WSQ_LOW_QUALITY = -41,

        [StringValue("Descomprimiendo WSQ")]
        IERR_WSQ_DECOMPRESSING = -42,

        [StringValue("Comprimiendo WSQ")]
        IERR_WSQ_COMPRESSING = -43,

        //50
        [StringValue("Error en proceso de matching")]
        IERR_MATCH_VALIDATION = -50,

        [StringValue("Error en proceso de verificacion")]
        IERR_VERIFY = -51,

        [StringValue("Error en proceso de identifiacion")]
        IERR_IDENTIFY = -52,

        [StringValue("No existe Matcher disponible")]
        IERR_MATCHER_NOT_AVAILABLE = -53,

        [StringValue("Error extrayendo minucias")]
        IERR_EXTRACTING = -54,

        [StringValue("Template invalido")]
        IERR_INVALID_TEMPLATE = -55,

        [StringValue("Template nulo")]
        IERR_NULL_TEMPLATE = -56,

        [StringValue("Token invalido")]
        IERR_INVALID_TOKEN = -57,

        [StringValue("Token nulo")]
        IERR_NULL_TOKEN = -58,

        [StringValue("Token desconocido")]
        IERR_UNKNOWN_TOKEN = -59,

        //70 - 79 - Connectors
        [StringValue("Conector no configurado correctamente")]
        IERR_CONNECTOR_NOT_CONFIGURED = -70,

        [StringValue("Conector no disponible")]
        IERR_CONNECTOR_NOT_AVAILABLE = -71,

        [StringValue("Error en la ejecución remota del web services")]
        IERR_CONNECTOR_REMOTE_ERROR = -72,

        [StringValue("Respuesta de conector mal formada")]
        IERR_CONNECTOR_MALFORMED_ANSWER = -73,

        //80 - 90 - PKI
        [StringValue("Certificado vencido")]
        IERR_CERTIFICATE_EXPIRED = -80,

        [StringValue("Certificado No Igual")]
        IERR_CERTIFICATE_NOT_EQUAL = -81,

        //100 - 110 - SRCeI
        [StringValue("Usuario invalido en SRCeI")]
        IERR_USER_INVALID_IN_SRCeI = -100,

        [StringValue("Error en la conexion al web service del SRCeI")]
        IERR_CONX_WS_SRCeI = -101,

        [StringValue("XML del SRCeI erroneo")]
        IERR_XML_INVALID_IN_SRCeI = -102,

        [StringValue("No existen datos en SRCeI")]
        IERR_NO_DATA_IN_SRCeI = -103,

        [StringValue("Imagen de mala calidad para SRCeI")]
        IERR_IMAGE_BAD_QUALITY_IN_SRCeI = -104,

        //200
        [StringValue("El paso de parámetros no permitió realizar la serialización del objeto correctamente")]
        IERR_BAD_SERIALIZER_CONFIG_FILE = -201,

        [StringValue("No se han seleccionado correctamente los datos de configuración")]
        IERR_SELECT_SERIALIZER_CONFIG_FILE = -202,

        [StringValue("El archivo de configuración fue modificado con éxito")]
        IERR_SAVE_CONFIG_FILE = -203,

        //300
        [StringValue("Imposible serializar la lista de companias")]
        IERR_BAD_SERIALIZER_COMPANYS = -301,

        //400
        [StringValue("No se puede modificar el origen porque crearía valores duplicados")]
        IERR_DUPLICATE_ORIGIN = -401,

        [StringValue("No se puede eliminar el origen.")]
        IERR_DELETE_ORIGIN = -402,

        [StringValue("Cliente deshabilitado")]
        IERR_CLIENT_DESHABILITADO = -501
    }
}