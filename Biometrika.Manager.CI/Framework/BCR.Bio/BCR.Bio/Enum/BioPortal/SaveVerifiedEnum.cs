﻿namespace BCR.Bio.Enum.BioPortal
{
    /// <summary>
    /// Determina si se graba en BD una copia de cada muestra verificada
    /// </summary>
    public enum SaveVerifiedEnum
    {
        Default_del_Config = 0,
        Save_Data = 1,
        No_Save_Data = 2
    }
}