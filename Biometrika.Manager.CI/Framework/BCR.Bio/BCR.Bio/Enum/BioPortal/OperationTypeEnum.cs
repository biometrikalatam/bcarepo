﻿namespace BCR.Bio.Enum.BioPortal
{
    public enum OperationTypeEnum
    {
        NONE,
        VERIFY,
        ENROLL
    }
}