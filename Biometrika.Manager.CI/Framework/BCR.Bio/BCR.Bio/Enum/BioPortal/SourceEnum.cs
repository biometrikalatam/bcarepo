﻿using BCR.System.Attribute;

namespace BCR.Bio.Enum.BioPortal
{
    public enum SourceEnum
    {
        [StringValue("Cualquiera")]
        SOURCE_ANY = -2,

        [StringValue("Todos")]
        SOURCE_All = -1,

        [StringValue("Default")]
        SOURCE_DEFAULT = 0,

        [StringValue("Local")]
        SOURCE_LOCAL = 1,

        [StringValue("Remoto")]
        SOURCE_REMOTE = 2
    }
}
