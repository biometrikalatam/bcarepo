﻿using BCR.System.Attribute;

namespace BCR.Bio.Enum.BioPortal
{
    public enum TemplateDestinationEnum
    {
        [StringValue("Cualquiera")]
        TEMPLATEDESTINATION_ANY = -2,

        [StringValue("Todos")]
        TEMPLATEDESTINATION_All = -1,

        [StringValue("Default")]
        TEMPLATEDESTINATION_DEFAULT = 0,

        [StringValue("TO_VERIFY")]
        TEMPLATEDESTINATION_TOVERIFY = 1,

        [StringValue("TO_ENROLL")]
        TEMPLATEDESTINATION_TOENROLL = 2,

        [StringValue("TO_IDENTIFY")]
        TEMPLATEDESTINATION_TOIDENTIFY = 3
    }
}

