﻿namespace BCR.Bio.Enum.BioPortal
{
    /// <summary>
    /// Indica que tipo de matching se hace en Identify o en Verify (cuando hay muchas muestras
    /// por parte de cuerpo o es con _bodypart = 0 o menor.
    /// </summary>
    public enum MatchingTypeEnum
    {
        Default = 0,
        First = 1,
        Best = 2
    }
}