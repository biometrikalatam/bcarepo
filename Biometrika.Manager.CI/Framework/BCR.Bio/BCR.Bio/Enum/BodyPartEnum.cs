﻿using BCR.System.Attribute;

namespace BCR.Bio.Enum
{
    public enum BodyPartEnum
    {
        [StringValue("Cualquiera")]
        BODYPART_ANY = -2,

        [StringValue("Todos")]
        BODYPART_ALL = -1,

        [StringValue("Ninguno")]
        BODYPART_NONE = 0,

        [StringValue("Dedo Pulgar Derecho")]
        BODYPART_DEDOPULGARDERECHO = 1,

        [StringValue("Dedo Indice Derecho")]
        BODYPART_DEDOINDICEDERECHO = 2,

        [StringValue("Dedo Mayor Derecho")]
        BODYPART_DEDOMAYORDERECHO = 3,

        [StringValue("Dedo Anular Derecho")]
        BODYPART_DEDOANULARDERECHO = 4,

        [StringValue("Dedo Menique Derecho")]
        BODYPART_DEDOMENIQUEDERECHO = 5,

        [StringValue("Dedo Pulgar Izquierdo")]
        BODYPART_DEDOPULGARIZQUIERDO = 6,

        [StringValue("Dedo Indice Izquierdo")]
        BODYPART_DEDOINDICEIZQUIERDO = 7,

        [StringValue("Dedo Mayor Izquierdo")]
        BODYPART_DEDOMAYORIZQUIERDO = 8,

        [StringValue("Dedo Anular Izquierdo")]
        BODYPART_DEDOANULARIZQUIERDO = 9,

        [StringValue("Dedo Menique Izquierdo")]
        BODYPART_DEDOMENIQUEIZQUIERDO = 10,

        //Manos
        [StringValue("Mano Derecha")]
        BODYPART_MANODERECHA = 11,

        [StringValue("Mano Izquierda")]
        BODYPART_MANOIZQUIERDA = 12,

        //Ojos
        [StringValue("Ojo Derecho")]
        BODYPART_OJODERECHO = 13,

        [StringValue("Ojo Izquierdo")]
        BODYPART_OJOIZQUIERDO = 14,

        //Voz

        [StringValue("Voz")]
        BODYPART_VOZ = 15,

        //Face
        [StringValue("Cara")]
        BODYPART_FACE = 16
    }
}