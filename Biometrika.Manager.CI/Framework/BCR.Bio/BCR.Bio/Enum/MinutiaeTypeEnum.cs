﻿using BCR.System.Attribute;

namespace BCR.Bio.Enum
{
    public enum MinutiaeTypeEnum
    {
        [StringValue("Cualquiera")]
        MINUTIAETYPE_ANY = -2,

        [StringValue("Todas")]
        MINUTIAETYPE_ALL = -1,

        [StringValue("Ninguna")]
        MINUTIAETYPE_NONE = 0,

        [StringValue("NEC")]
        MINUTIAETYPE_NEC = 1,

        [StringValue("Digital Persona")]
        MINUTIAETYPE_DIGITALPERSONA = 2,

        [StringValue("Digital Persona Gold")]
        MINUTIAETYPE_DIGITALPERSONAGOLD = 3,

        [StringValue("Digital Persona")]
        MINUTIAETYPE_DIGITALPERSONAOTW = 4,

        [StringValue("Secugen")]
        MINUTIAETYPE_SECUGEN = 5,

        [StringValue("Testech")]
        MINUTIAETYPE_TESTECH = 6,

        [StringValue("Verifinger")]
        MINUTIAETYPE_VERIFINGER = 7,

        [StringValue("Sagem")]
        MINUTIAETYPE_SAGEM = 8,

        [StringValue("Identix")]
        MINUTIAETYPE_IDENTIX = 9,

        [StringValue("Upek")]
        MINUTIAETYPE_UPEK = 10,

        [StringValue("Cogent")]
        MINUTIAETYPE_COGENT = 11,

        [StringValue("Crossmatch")]
        MINUTIAETYPE_CROSSMATCH = 12,

        [StringValue("ANSI INSITS 378-2004")]
        MINUTIAETYPE_ANSI_INSITS_378_2004 = 13,

        [StringValue("ISO/IEC 19794-2:2005")]
        MINUTIAETYPE_ISO_IEC_19794_2_2005 = 14,

        [StringValue("Clave")]
        MINUTIAETYPE_PASSWORD = 20,

        [StringValue("WSQ")]
        MINUTIAETYPE_WSQ = 21,

        [StringValue("RAW")]
        MINUTIAETYPE_RAW = 22,

        [StringValue("INCITS 381-2004")]
        MINUTIAETYPE_INCITS_381_2004 = 23,

        [StringValue("TOKEN")]
        MINUTIAETYPE_TOKEN = 24,

        [StringValue("Handkey")]
        MINUTIAETYPE_HANDKEY = 30,

        [StringValue("Facial")]
        MINUTIAETYPE_FACIAL = 40,

        [StringValue("JPG")]
        MINUTIAETYPE_JPG = 41,

        [StringValue("F7 Facial")]
        MINUTIAETYPE_FACIAL_F7 = 42,

        [StringValue("VeriLook Facial")]
        MINUTIAETYPE_FACIAL_VERILOOK = 43,

        [StringValue("Iris")]
        MINUTIAETYPE_IRIS = 50,

        [StringValue("Retina")]
        MINUTIAETYPE_RETINA = 60,

        [StringValue("Certificado Digital x509")]
        MINUTIAETYPE_X509 = 70
    }
}