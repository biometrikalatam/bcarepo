﻿namespace BCR.Bio.Message
{
    public static class MessageProvider
    {
        public static string Get(string tag)
        {
            return MessageBase.ResourceManager.GetString(tag);
        }
    }
}