using System.Collections;
using System.Text;

namespace BCR.Bio.Wsq.Decoder
{
	/// <summary>
	/// El frame segun la especificacion es lo que viene entre
	/// SOI y EOI
	/// </summary>
	public sealed class Frame : Table
	{
		const int NumSubBands = 60;
	
		private int length;


		private float[] hifilters;
		private float[] lofilters;
		private float[] neghifilters;


		internal HuffmanTable GetHuffmanTable(int pos)
		{
			if (huffTables == null)
				throw new DecoderException("Este frame no tiene tablas de huffman");

			return huffTables.GetTable(pos);
		}
	

		
		private ArrayList blocks = new ArrayList ();
		private HuffmanTables huffTables;
		
		
		/// <summary>
		/// Constructor
		/// </summary>
		public Frame () : base(null)
		{
		}

		
		internal bool AddBlock (EntropyEncodedBlock block)
		{
			if (block == null)
				return false;
			blocks.Add (block);
			block.BlockFrame = this;
			return true;
		}

		public int Length
		{
			get { return length; }
		}	


		internal void ReadHeader (byte[] data, int start, int length)
		{
			SetData(data, start, length);
			black = GetByte (0);
			white = GetByte (1);
			height = GetShort (2);
			width =  GetShort (4);
			m = GetFloat16 (6);
			r = GetFloat16 (9);
			algorithm = GetByte (11);
			softwareImplementation = GetShort (12);
		}

		private float[] ndata;
		internal bool Decode(byte[] data)
		{
			if (data == null)
				return false;
			
			QuantumTreeNode[] qtrees;
			WaveletTreeNode[] wtrees;

			short[] qdata = DecodeBinsIndexValues();

			TreeBuilder.BuildTrees(width, height, out wtrees, out qtrees);
			
			ndata = Unquantize(qdata, qtrees);
			if (ndata == null)
				return false;

			Reconstruct(wtrees);
			ConvertImage(ndata, data, width, height, r, m);
			return true;
		}

		private byte black;
		private byte white;
		private short height;
		private short width;
		private float m;
		private float r;
		private byte algorithm;
		private short softwareImplementation;

		public byte Black
		{
			get { return black; }
		}

		public byte White
		{
			get { return white; }
		}

		public short Height
		{
			get { return height; }
		}

		public short Width
		{
			get { return width; }
		}

		public float M
		{
			get { return m; }
		}

		public float R
		{
			get { return r; }
		}

		public byte Algorithm
		{
			get { return algorithm; }
		}

		public short SoftwareImplementation
		{
			get { return softwareImplementation; }
		}

		public short[] BinsIndexValues
		{
			get
			{
				return DecodeBinsIndexValues();
			}
		}

		public float[] Qbin
		{
			get
			{
				return qbin;
			}
		}

		public float[] Zbin
		{
			get
			{
				return zbin;
			}
		}

		public float C
		{
			get
			{
				return c;
			}
		}

		private float[] qbin;
		private float[] zbin;
		private float c;

		private float[] Unquantize (short[] binIndexValues, QuantumTreeNode[] qtree)
		{
			float[] result = new float[width*height];
			
			int sptr = 0;
			int fptr = 0;
			
			
			for (int k = 0; k < NumSubBands; k++)
			{
				// subbanda es 0
				if (qbin[k] == 0.0f)
					continue;

				fptr = (qtree[k].Y * width) + qtree[k].X;
				
				float zk2 = zbin[k]/2.0f;
				float qbk = qbin[k];
				int  stride = width - qtree[k].LenX;
				for (int row = 0; row < qtree[k].LenY; row++, fptr += stride)
					for (int col = 0; col < qtree[k].LenX; col++)
					{
						int sp = binIndexValues[sptr++];
						if (sp == 0)
							result[fptr++] = 0.0f;
						else if (sp > 0)
							result[fptr++] = (qbk*(sp-C)) + zk2;
						else
							result[fptr++] = (qbk*(sp+C)) - zk2;
					}
			}

			return result;
		}


		private short[] DecodeBinsIndexValues ()
		{
			short[] result = new short[ImageSize];
			int pos = 0;
			foreach (EntropyEncodedBlock ecs in blocks)
			{
				EntropyDecoder decoder = new EntropyDecoder (ecs);
				if (!decoder.Decode (result, ref pos))
					break;
				if (pos > result.Length)
					break;
			}
			return result;
		}

		/// <summary>
		/// Tama�o en bytes de la imagen
		/// </summary>
		public int ImageSize
		{
			get { return width*height; }
		}

		private void Reconstruct (WaveletTreeNode[] treeNodes)
		{
			/* Allocate temporary floating point pixmap. */
			float[] fdata1 = new float[width*height];

			InitJoinLetsParams();
			JoinLets00(fdata1, treeNodes[19]);
			JoinLets11(fdata1, treeNodes[18]);
			JoinLets10(fdata1, treeNodes[17]);
			JoinLets01(fdata1, treeNodes[16]);
			JoinLets00(fdata1, treeNodes[15]);
			JoinLets00(fdata1, treeNodes[14]);
			JoinLets11(fdata1, treeNodes[13]);
			JoinLets10(fdata1, treeNodes[12]);
			JoinLets01(fdata1, treeNodes[11]);
			JoinLets00(fdata1, treeNodes[10]);
			JoinLets11(fdata1, treeNodes[09]);
			JoinLets10(fdata1, treeNodes[08]);
			JoinLets01(fdata1, treeNodes[07]);
			JoinLets00(fdata1, treeNodes[06]);
			JoinLets10(fdata1, treeNodes[05]);
			JoinLets01(fdata1, treeNodes[04]);
			JoinLets10(fdata1, treeNodes[03]);
			JoinLets01(fdata1, treeNodes[02]);
			JoinLets00(fdata1, treeNodes[01]);
			JoinLets00(fdata1, treeNodes[00]);
		}

		private void JoinLets00(float[] fdata1, WaveletTreeNode wtree)
		{
			int offset = (wtree.Y*width) + wtree.X;
			JoinLets1W(fdata1, offset, wtree.LenX, wtree.LenY);
			JoinLetsW1(offset, fdata1, wtree.LenY, wtree.LenX);
		}

		private void JoinLets01(float[] fdata1, WaveletTreeNode wtree)
		{
			int offset = (wtree.Y*width) + wtree.X;	
			JoinLets1W(fdata1, offset, wtree.LenX, wtree.LenY);
			JoinLetsInvW1(fdata1, offset, wtree.LenY, wtree.LenX);
		}

		private void JoinLets10(float[] fdata1, WaveletTreeNode wtree)
		{
			int offset = (wtree.Y*width) + wtree.X;
			JoinLetsInv1W(fdata1, offset, wtree.LenX, wtree.LenY);
			JoinLetsW1(offset, fdata1, wtree.LenY, wtree.LenX);
		}

		private void JoinLets11(float[] fdata1, WaveletTreeNode wtree)
		{
			int offset = (wtree.Y*width) + wtree.X;
			JoinLetsInv1W(fdata1, offset, wtree.LenX, wtree.LenY);
			JoinLetsInvW1(fdata1, offset, wtree.LenY, wtree.LenX);
		}

		private void ConvertImage (float[] pixmap, byte[] raw, short width, short height, float r_scale, float m_shift)
		{
			int img = 0;
			int data = 0;
			for (int r = 0; r < height; r++) 
			{
				for (int c = 0; c < width; c++) 
				{
				
					float img_tmp = (pixmap[img] * r_scale) + m_shift;// + 0.5);
					if (img_tmp < 0.0f)
						raw[data] = 0; /* neg pix poss after quantization */
					else
					{
						if (img_tmp > 255.0f)
							raw[data] = 255;
						else
							raw[data] = (byte) img_tmp;
					}

					++img;
					++data;
				}
			}
		}

		
		private void JoinLets1W(float[] newData, int pold, int len1, int len2)
		{
			bool da_ev = InitJoinLetsVars(len2);
			bool asymdaev = asym && da_ev;
			
			for (int cl_rw = 0; cl_rw < len1; cl_rw++, pold++)
			{
				int fhre=0;

				newData[cl_rw] = newData[cl_rw+ width] = 0.0f;
				int lspx = pold + locwidth;
				int lspxstr = -width;
				int lstap = lotap;
				int lle2 = olle;
				
				int hp0 = pold + wllen;
				int hspx = hp0 + hocwidth; 
				int hspxstr = -width;
				int hstap = hotap;
				int hle2 = ohle;
				float osfac = ssfac;

				int limg = cl_rw;
				int himg = cl_rw;
				int hp1 = hp0 + hlen1width;
				int lp3 = pold + ll1width;
				
				for(int pix = 0; pix < hlen; pix++) 
				{
					for(int tap = lstap; tap >=0; tap--) 
					{
						int lle = lle2;
						int lre = olre;
						int lpx = lspx;
						int lpxstr = lspxstr;
						float sum = ndata[lpx] * lofilters[tap];
						for(int i = tap+2; i < losz; i += 2) 
						{
							if(lpx == pold) 
							{
								if(lle == 0) 
									lpxstr = width;
								else
									lpxstr = lle = 0;
							}
							if(lpx == lp3) 
							{
								if(lre == 0) 
									lpxstr = -width;
								else
									lpxstr = lre = 0;
							}
							lpx += lpxstr;
							sum += ndata[lpx] * lofilters[i];
						}
						newData[limg] = sum;
						limg += width;
					}
					if(lspx == pold) 
					{
						if(lle2 == 0) 
							lspxstr = width;
						else
							lspxstr = lle2 = 0;
					}
					lspx += lspxstr;
					lstap = 1;

					for(int tap = hstap; tap >=0; tap--) 
					{
						int hle = hle2;
						int hre = ohre;
						int hpx = hspx;
						int hpxstr = hspxstr;
						fhre = ofhre;
						float sfac = osfac;

						for(int i = tap; i < hisz; i += 2) 
						{
							if(hpx == hp0) 
							{
								if(hle == 0) 
								{
									hpxstr = width;
									sfac = 1.0f;
								}
								else 
									hpxstr = hle = 0;
							}
							if(hpx == hp1) 
							{
								if(hre != 0) 
								{
									hpxstr = hre = 0;
									if(asymdaev)
									{
										hre = 1;
										fhre--;
										sfac = (float)fhre;
										if(sfac == 0.0)
											hre = 0;
									}
								}
								else 
								{
									hpxstr = -width;
									if(asym)
										sfac = -1.0f;
								}
							}
							newData[himg] += ndata[hpx] * hifilt[i] * sfac;
							hpx += hpxstr;
						}
						himg += width;
					}
					if(hspx == hp0) 
					{
						if(hle2 == 0) 
						{
							hspxstr = width;
							osfac = 1.0f;
						}
						else 
							hspxstr = hle2 = 0;
					}
					hspx += hspxstr;
					hstap = 1;
				}

				if(da_ev)
					lstap = 1;
				else
					lstap = 1+lotap;
				

				for(int tap = 1; tap >= lstap; tap--) 
				{
					int lle = lle2;
					int lre = olre;
					int lpx = lspx;
					int lpxstr = lspxstr;

					newData[limg] = ndata[lpx] * lofilters[tap];
					for(int i = tap+2; i < losz; i += 2) 
					{
						if(lpx == pold) 
						{
							if(lle == 0) 
								lpxstr = width;
							else
								lpxstr = lle = 0;
						}
						if(lpx == lp3) 
						{
							if(lre == 0) 
								lpxstr = -width;
							else
								lpxstr = lre = 0;
						}
						lpx += lpxstr;
						newData[limg] += ndata[lpx] * lofilters[i];
					}
					limg += width;
				}


				if(da_ev)
				{
					hstap = hotap;
					
					if(hisz == 2) 
					{
						hspx -= hspxstr;
						fhre = 1;
					}
				}
				else
					hstap = 1+hotap;
				


				for(int tap = 1; tap >= hstap; tap--) 
				{
					int hle = hle2;
					int hre = ohre;
					int hpx = hspx;
					int hpxstr = hspxstr;
					float sfac = osfac;
					if(hisz != 2)
						fhre = ofhre;

					for(int i = tap; i < hisz; i += 2) 
					{
						if(hpx == hp0) 
						{
							if(hle == 0) 
							{
								hpxstr = width;
								sfac = 1.0f;
							}
							else 
								hpxstr = hle = 0;
						}
						if(hpx == hp1) 
						{
							if(hre == 0) 
							{
								hpxstr = -width;
								if(asym)
									sfac = -1.0f;
							}
							else 
							{
								hpxstr = hre = 0;
								if(asymdaev)
								{
									hre = 1;
									fhre--;
									sfac = (float)fhre;
									if(sfac == 0.0)
										hre = 0;
								}
							}
						}
						newData[himg] += ndata[hpx] * hifilt[i] * sfac;
						hpx += hpxstr;
					}
					himg += width;
				}
			}			

		}


		private void JoinLetsW1 (int pnew, float[] oldData, int len1, int len2)
		{
			bool da_ev = InitJoinLetsVars(len2);
			bool asymdaev = asym && da_ev;
			int fhre=0;
			for (int cl_rw = 0, lp0 = 0; cl_rw < len1; cl_rw++, pnew += width, lp0 += width)
			{
				int limg = pnew; 
				int himg = limg;
				ndata[himg] = ndata[himg + 1] = 0.0f;
				
				int lp1 = lp0 + llen1;
				int lspx = lp0 + loc;
				int lstap = lotap;
				int lle2 = olle;
				int lre2 = olre;

				int hp0 = lp0+llen;
				int hp1 = hp0 + (hlen-1);
				int hspx = hp0 + hoc;
				int hstap = hotap;
				int hle2 = ohle;
				int hre2 = ohre;
				float osfac = ssfac;
				int lspxstr = -1;
				int hspxstr = -1;
				
				for(int pix = 0; pix < hlen; pix++) 
				{
					
					for(int tap = lstap; tap >=0; tap--) 
					{
						int lle = lle2;
						int lre = lre2;
						int lpx = lspx;
						int lpxstr = lspxstr;

						ndata[limg] = oldData[lpx] * lofilters[tap];
						for(int i = tap+2; i < losz; i += 2) 
						{
							if(lpx == lp0) 
							{
								if(lle == 0) 
									lpxstr = 1;
								else
									lpxstr = lle = 0;
							}
							if(lpx == lp1) 
							{
								if(lre == 0) 
									lpxstr = -1;
								else
									lpxstr = lre = 0;
							}
							lpx += lpxstr;
							ndata[limg] += oldData[lpx] * lofilters[i];
						}
						limg++;
					}
					if(lspx == lp0) 
					{
						if (lle2 == 0) 
							lspxstr = 1;
						else
							lspxstr = lle2 = 0;
					}
					lspx += lspxstr;
					lstap = 1;

					for(int tap = hstap; tap >=0; tap--) 
					{
						int hle = hle2;
						int hre = hre2;
						int hpx = hspx;
						int hpxstr = hspxstr;
						fhre = ofhre;
						float sfac = osfac;

						for(int i = tap; i < hisz; i += 2) 
						{
							if(hpx == hp0) 
							{
								if(hle == 0) 
								{
									hpxstr = 1;
									sfac = 1.0f;
								}
								else 
									hpxstr = hle = 0;
							}
							if(hpx == hp1) 
							{
								if(hre == 0) 
								{
									hpxstr = -1;
									if(asym)
										sfac = -1.0f;
								}
								else 
								{
									hpxstr = hre = 0;
									if(asymdaev)
									{
										hre = 1;
										fhre--;
										sfac = (float)fhre;
										if(sfac == 0.0)
											hre = 0;
									}
								}
							}
							ndata[himg] += oldData[hpx] * hifilt[i] * sfac;
							hpx += hpxstr;
						}
						himg++;
					}
					if(hspx == hp0) 
					{
						if(hle2 == 0) 
						{
							hspxstr = 1;
							osfac = 1.0f;
						}
						else 
							hspxstr = hle2 = 0;
					}
					hspx += hspxstr;
					hstap = 1;
				}

				if(da_ev)
					lstap = 1;
				else
					lstap = 1+lotap;
				

				for(int tap = 1; tap >= lstap; tap--) 
				{
					int lle = lle2;
					int lre = lre2;
					int lpx = lspx;
					int lpxstr = lspxstr;

					ndata[limg] = oldData[lpx] * lofilters[tap];
					for(int i = tap+2; i < losz; i += 2) 
					{
						if(lpx == lp0) 
						{
							if(lle == 0) 
								lpxstr = 1;
							else
								lpxstr = lle = 0;
						}
						if(lpx == lp1) 
						{
							if(lre == 0) 
								lpxstr = -1;
							else
								lpxstr = lre = 0;
						}
						lpx += lpxstr;
						ndata[limg] += oldData[lpx] * lofilters[i];
					}
					limg++;
				}


				if(da_ev)
				{
					hstap = hotap;
					
					if(hisz == 2) 
					{
						hspx -= hspxstr;
						fhre = 1;
					}
				}
				else
					hstap = 1+hotap;
				


				for(int tap = 1; tap >= hstap; tap--) 
				{
					int hle = hle2;
					int hre = hre2;
					int hpx = hspx;
					int hpxstr = hspxstr;
					float sfac = osfac;
					if(hisz != 2)
						fhre = ofhre;

					for(int i = tap; i < hisz; i += 2) 
					{
						if(hpx == hp0) 
						{
							if(hle == 0) 
							{
								hpxstr = 1;
								sfac = 1.0f;
							}
							else 
								hpxstr = hle = 0;
						}
						if(hpx == hp1) 
						{
							if(hre == 0) 
							{
								hpxstr = -1;
								if(asym)
									sfac = -1.0f;
							}
							else 
							{
								hpxstr = 0;
								hre = 0;
								if(asymdaev) 
								{
									hre = 1;
									fhre--;
									sfac = (float)fhre;
									if(sfac == 0.0)
										hre = 0;
								}
							}
						}
						ndata[himg] += oldData[hpx] * hifilt[i] * sfac;
						hpx += hpxstr;
					}
					himg++;
				}
			}			

		}


		private void JoinLetsInv1W (float[] newData, int pold, int len1, int len2)
		{
			bool da_ev = InitJoinLetsVars(len2);
			int fhre=0;
			

			for (int cl_rw = 0; cl_rw < len1; cl_rw++)  
			{
				newData[cl_rw] = 0.0f;
				newData[cl_rw + width] = 0.0f;
				int hipass = pold + cl_rw;
				int lopass = hipass + whlen;
				
				int lp0 = lopass;
				int lp1 = lp0 + ll1width;
				int lspx = lp0 + locwidth;
				int lspxstr = -width;
				int lstap = lotap;
				int lle2 = olle;
				int lre2 = olre;

				int hp0 = hipass;
				int hp1 = hp0 + hlen1width; 
				int hspx = hp0 + hocwidth; 
				int hspxstr = -width;
				int hstap = hotap;
				int hle2 = ohle;
				int hre2 = ohre;
				float osfac = ssfac;

				int limg = cl_rw;
				int himg = cl_rw;
				
				bool asymdaev = asym && da_ev;
				for(int pix = 0; pix < hlen; pix++) 
				{
					for(int tap = lstap; tap >=0; tap--) 
					{
						int lle = lle2;
						int lre = lre2;
						int lpx = lspx;
						int lpxstr = lspxstr;

						newData[limg] = ndata[lpx] * lofilters[tap];
						for(int i = tap+2; i < losz; i += 2) 
						{
							if(lpx == lp0) 
							{
								if(lle == 0) 
									lpxstr = width;
								else
									lpxstr = lle = 0;
							}
							if(lpx == lp1) 
							{
								if(lre == 0) 
									lpxstr = -width;
								else
									lpxstr = lre = 0;
							}
							lpx += lpxstr;
							newData[limg] += ndata[lpx] * lofilters[i];
						}
						limg += width;
					}
					if(lspx == lp0) 
					{
						if(lle2 != 0) 
							lspxstr = lle2 = 0;
						else
							lspxstr = width;
					}
					lspx += lspxstr;
					lstap = 1;

					for(int tap = hstap; tap >=0; tap--) 
					{
						int hle = hle2;
						int hre = hre2;
						int hpx = hspx;
						int hpxstr = hspxstr;
						fhre = ofhre;
						float sfac = osfac;

						for(int i = tap; i < hisz; i += 2) 
						{
							if(hpx == hp0) 
							{
								if(hle == 0) 
								{
									hpxstr = width;
									sfac = 1.0f;
								}
								else 
									hpxstr = hle = 0;
							}
							if(hpx == hp1) 
							{
								if(hre == 0) 
								{
									hpxstr = -width;
									if(asym)
										sfac = -1.0f;
								}
								else 
								{
									hpxstr = hre = 0;
									if(asymdaev)
									{
										hre = 1;
										fhre--;
										sfac = (float)fhre;
										if(sfac == 0.0)
											hre = 0;
									}
								}
							}
							newData[himg] += ndata[hpx] * hifilt[i] * sfac;
							hpx += hpxstr;
						}
						himg += width;
					}
					if(hspx == hp0) 
					{
						if(hle2 == 0) 
						{
							hspxstr = width;
							osfac = 1.0f;
						}
						else 
							hspxstr = hle2 = 0;
					}
					hspx += hspxstr;
					hstap = 1;
				}

				if (da_ev)
					lstap = lotap;
				else
					lstap = lotap + 1;
				for(int tap = 1; tap >= lstap; tap--) 
				{
					int lle = lle2;
					int lre = lre2;
					int lpx = lspx;
					int lpxstr = lspxstr;

					newData[limg] = ndata[lpx] * lofilters[tap];
					for(int i = tap+2; i < losz; i += 2) 
					{
						if(lpx == lp0) 
						{
							if(lle == 0) 
								lpxstr = width;
							else
								lpxstr = lle = 0;
						}
						if(lpx == lp1) 
						{
							if(lre == 0) 
								lpxstr = -width;
							else
								lpxstr = lre = 0;
						}
						lpx += lpxstr;
						newData[limg] += ndata[lpx] * lofilters[i];
					}
					limg += width;
				}


				if(da_ev)
				{
					hstap = hotap;
					if(hisz == 2) 
					{
						hspx -= hspxstr;
						fhre = 1;
					}
				}
				else
					hstap = 1 + hotap;
				


				for(int tap = 1; tap >= hstap; tap--) 
				{
					int hle = hle2;
					int hre = hre2;
					int hpx = hspx;
					int hpxstr = hspxstr;
					float sfac = osfac;
					if(hisz != 2)
						fhre = ofhre;

					for(int i = tap; i < hisz; i += 2) 
					{
						if(hpx == hp0) 
						{
							if(hle == 0) 
							{
								hpxstr = width;
								sfac = 1.0f;
							}
							else 
								hpxstr = hle = 0;
						}
						if(hpx == hp1) 
						{
							if(hre == 0) 
							{
								hpxstr = -width;
								if(asym)
									sfac = -1.0f;
							}
							else 
							{
								hpxstr = hre = 0;
								if(asymdaev)
								{
									hre = 1;
									fhre--;
									sfac = (float)fhre;
									if(sfac == 0.0)
										hre = 0;
								}
							}
						}
						newData[himg] += ndata[hpx] * hifilt[i] * sfac;
						hpx += hpxstr;
					}
					himg += width;
				}
			}			

		}


		private void JoinLetsInvW1 (float[] oldData, int pnew, int len1, int len2)
		{
			bool da_ev = InitJoinLetsVars(len2);
			bool asymdaev = asym && da_ev;
			int fhre=0;
			for (int cl_rw = 0, hipass = 0; cl_rw < len1; cl_rw++, hipass += width)  
			{

				int limg = pnew + cl_rw * width;
				int himg = limg;
				ndata[himg] = ndata[himg + 1] = 0.0f;
				int lp0 = hipass + hlen;
				int lp1 = lp0 + llen1;
				int lspx = lp0 + loc;
				int lspxstr = -1;
				int lstap = lotap;
				int lle2 = olle;
				int lre2 = olre;
				int hp0 = hipass;
				int hp1 = hp0 + (hlen-1);
				int hspx = hp0 + hoc;
				int hspxstr = -1;
				int hstap = hotap;
				int hle2 = ohle;
				int hre2 = ohre;
				float osfac = ssfac;

			
				
				for(int pix = 0; pix < hlen; pix++) 
				{
					
					for(int tap = lstap; tap >=0; tap--) 
					{
						int lle = lle2;
						int lre = lre2;
						int lpx = lspx;
						int lpxstr = lspxstr;

						ndata[limg] = oldData[lpx] * lofilters[tap];
						for(int i = tap+2; i < losz; i += 2) 
						{
							if(lpx == lp0) 
							{
								if(lle != 0) 
									lpxstr = lle = 0;
								else
									lpxstr = 1;
							}
							if(lpx == lp1) 
							{
								if(lre != 0) 
									lpxstr = lre = 0;
								else
									lpxstr = -1;
							}
							lpx += lpxstr;
							ndata[limg] += oldData[lpx] * lofilters[i];
						}
						limg += 1;
					}
					if(lspx == lp0) 
					{
						if(lle2 != 0) 
							lspxstr = lle2 = 0;
						else
							lspxstr = 1;
					}
					lspx += lspxstr;
					lstap = 1;

					for(int tap = hstap; tap >=0; tap--) 
					{
						int hle = hle2;
						int hre = hre2;
						int hpx = hspx;
						int hpxstr = hspxstr;
						fhre = ofhre;
						float sfac = osfac;

						for(int i = tap; i < hisz; i += 2) 
						{
							if(hpx == hp0) 
							{
								if(hle != 0) 
									hpxstr = hle = 0;
								else 
								{
									hpxstr = 1;
									sfac = 1.0f;
								}
							}
							if(hpx == hp1) 
							{
								if (hre == 0) 
								{
									hpxstr = -1;
									if(asym)
										sfac = -1.0f;
								}
								else 
								{
									hpxstr = hre = 0;
									if(asymdaev)
									{
										hre = 1;
										fhre--;
										sfac = (float)fhre;
										if(sfac == 0.0)
											hre = 0;
									}
								}
							}
							ndata[himg] += oldData[hpx] * hifilt[i] * sfac;
							hpx += hpxstr;
						}
						himg++;
					}
					if(hspx == hp0) 
					{
						if(hle2 != 0) 
							hspxstr = hle2 = 0;
						else 
						{
							hspxstr = 1;
							osfac = 1.0f;
						}
					}
					hspx += hspxstr;
					hstap = 1;
				}


				if (da_ev)
					lstap = lotap;
				else
					lstap = lotap+1;

				for(int tap = 1; tap >= lstap; tap--) 
				{
					int lle = lle2;
					int lre = lre2;
					int lpx = lspx;
					int lpxstr = lspxstr;

					ndata[limg] = oldData[lpx] * lofilters[tap];
					for(int i = tap+2; i < losz; i += 2) 
					{
						if(lpx == lp0) 
						{
							if(lle == 0) 
								lpxstr = 1;
							else
								lpxstr = lle = 0;
						}
						if(lpx == lp1) 
						{
							if(lre == 0) 
								lpxstr = -1;
							else
								lpxstr = lre = 0;
						}
						lpx += lpxstr;
						ndata[limg] += oldData[lpx] * lofilters[i];
					}
					limg++;
				}


				if(da_ev)
				{
					hstap = hotap;
					if(hisz == 2) 
					{
						hspx -= hspxstr;
						fhre = 1;
					}
				}
				else
					hstap = 1 + hotap;
				


				for(int tap = 1; tap >= hstap; tap--) 
				{
					int hle = hle2;
					int hre = hre2;
					int hpx = hspx;
					int hpxstr = hspxstr;
					float sfac = osfac;
					if(hisz != 2)
						fhre = ofhre;

					for(int i = tap; i < hisz; i += 2) 
					{
						if(hpx == hp0) 
						{
							if(hle == 0) 
							{
								hpxstr = 1;
								sfac = 1.0f;
							}
							else 
								hpxstr = hle = 0;
						}
						if(hpx == hp1) 
						{
							if(hre == 0) 
							{
								hpxstr = -1;
								if(asym)
									sfac = -1.0f;
							}
							else 
							{
								hpxstr = hre = 0;
								if(asymdaev)
								{
									hre = 1;
									fhre--;
									sfac = (float)fhre;
									if(sfac == 0.0)
										hre = 0;
								}
							}
						}
						ndata[himg] += oldData[hpx] * hifilt[i] * sfac;
						hpx += hpxstr;
					}
					himg++;
				}
			}			

		}


		private bool InitJoinLetsVars(int len2)
		{
			int da_ev = len2 & 0x01;
			llen = (len2+da_ev) >> 1;
			hlen = llen - da_ev;
			ohle = 1;
			if(lofiev != 0)
			{
				olle =  0;
				ohre = da_ev;
				olre = 1 - da_ev;
			}
			else
			{
				olle = ohre = 1;
				olre = 1 - da_ev;
				if(loc == -1)
					loc = olle = 0;
				if(hoc == -1)
					hoc = ohle = 0;
			}
			whlen = width * hlen;
			wllen = width * llen;
			hocwidth = (hoc * width);
			llen1 = llen-1;
			ll1width = llen1 * width;
			locwidth = loc * width;
			hlen1 = hlen-1;
			hlen1width = hlen1 * width;
			return da_ev != 0;
		}

		private int losz;
		private int hisz;
		private int lofiev;
		private int hifiev;
		private int loc;
		private int hoc;
		private bool asym;
		private float ssfac;
		private int lotap;
		private int ofhre;
		private int hotap;
		private float[] hifilt;
		private int olle;
		private int ohre;
		private int ohle;
		private int olre;
		private int llen;
		private int hlen;
		private int whlen;
		private int hocwidth;
		private int ll1width;
		private int locwidth;
		private int hlen1width;
		private int llen1;
		private int hlen1;
		private int wllen;
		private ArrayList comments = new ArrayList();

		/// <summary>
		/// Lista de comentarios que vienen dentro del archivo wsq
		/// </summary>
		public ArrayList Comments
		{
			get 
			{
				return comments;
			}
		}


		/// <summary>
		/// Lee la tabla de filtros altos y bajos (tabla de transformacion)
		/// </summary>
		/// <param name="data">segmento de datos </param>
		internal void ParseTransformationTables (byte[] data, int start)
		{
			int pos = start;
			hisz = data[pos++];
			losz = data[pos++];
			lofiev = losz & 0x01;
			hifiev = hisz & 0x01;
			lofilters = new float[losz];
			hifilters = new float[hisz];
			neghifilters = new float[hisz];

			ParseHiFilter(data, ref pos);
			ParseLoFilter(data, pos);
			InitJoinLetsParams();
		}
		
		private void ParseHiFilter(byte[] data, ref int pos)
		{
			int asize = ((hisz+hifiev )>>1) - 1;
			if (hifiev == 0)
				ParseHiFilterEven(data, ref pos, asize);
			else 
				ParseHiFilterOdd(data, ref pos, asize);
		}

		private void ParseHiFilterEven(byte[] data, ref int pos, int asize)
		{
			for (int cnt = 0; cnt <= asize; cnt++)
			{
				float alofilt = GetSignedFloat32(data, ref pos);
				int index1 = cnt+asize+1;
				hifilters[index1] = IntSign(cnt) * alofilt;
				neghifilters[index1] = -hifilters[index1];
				int index2 = asize-cnt;
				hifilters[index2] = (float) (-1.0 * hifilters[index1]);
				neghifilters[index2] = -hifilters[index2];
			}
		}

		private void ParseHiFilterOdd(byte[] data, ref int pos, int asize)
		{
			for (int cnt = 0; cnt <= asize; cnt++)
			{
				float alofilt = GetSignedFloat32(data, ref pos);
				int index1 = cnt+asize;
				hifilters[index1] = IntSign(cnt) * alofilt;
				neghifilters[index1] = -hifilters[index1];
				if (cnt > 0)
				{
					int index2 = asize-cnt;
					hifilters[index2] = hifilters[index1];
					neghifilters[index2] = -hifilters[index1];
				}
			}
		}
		private void ParseLoFilter(byte[] data, int pos)
		{
			int asize = ((losz+lofiev)>>1)  - 1;
			if (lofiev == 0)
				ParseLoFilterEven(data, pos, asize);
			else
				ParseLoFilterOdd(data, pos, asize);
		}

		private void ParseLoFilterOdd(byte[] data, int pos, int asize)
		{
			for (int cnt = 0; cnt <= asize; cnt++)
			{
				int offset = cnt+1-lofiev;
				lofilters[asize+offset] = IntSign(offset)*GetSignedFloat32(data, ref pos);
				if (cnt > 0)
					lofilters[asize-cnt] = lofilters[cnt+asize];
			}
		}

		private void ParseLoFilterEven(byte[] data, int pos, int asize)
		{
			for (int cnt = 0; cnt <= asize; cnt++)
			{
				int offset = cnt+1-lofiev;
				lofilters[asize+offset] = IntSign(offset)*GetSignedFloat32(data, ref pos);
				lofilters[asize-cnt] = lofilters[cnt+asize+1];
			}
		}
		
		static int IntSign(int power)	
		{
			if (power == 0) 
				return 1;
			return (power & 0x01) == 0 ? 1 : -1;
		}

		internal void ParseQuantizationTable (byte[] data, int start, int length)
		{
			qbin = new float[64];
			zbin = new float[64];
			int n = ((/*data.L*/length/3)-1)>>1;
			int pos = start;
			c = GetFloat16(data, ref pos);
			for (int k = 0; k < n; k++)
			{
				qbin[k] = GetFloat16(data, ref pos);
				zbin[k] = GetFloat16(data, ref pos);
			}
		}

		private void InitJoinLetsParams ()
		{
			asym = lofiev == 0;
			if(lofiev == 0)
			{
				ssfac = -1.0f;
				ofhre = 2;
				loc = (losz>>2) - 1;
				hoc = (hisz>>2) - 1;
				lotap = (losz>>1) & 0x01;
				hotap = (hisz>>1) & 0x01;
				hifilt = neghifilters;
			}
			else 
			{
				ssfac = 1.0f;
				ofhre = 0;
				loc = (losz-1)>>2;
				hoc = ((hisz+1)>>2) - 1;
				lotap = ((losz-1)>>1) & 0x01;
				hotap = ((hisz+1)>>1) & 0x01;
				hifilt = hifilters;
			}
			

		}

		internal void AddComment (byte[] comment, int start, int length)
		{
			if (comment != null)
				comments.Add(Encoding.ASCII.GetString (comment, start, length));
			
		}

		internal void SetHuffmanTables (HuffmanTables htables)
		{
			huffTables = htables;
		}

		internal void SetLength (int length)
		{
			this.length = length;
		}
	}
}