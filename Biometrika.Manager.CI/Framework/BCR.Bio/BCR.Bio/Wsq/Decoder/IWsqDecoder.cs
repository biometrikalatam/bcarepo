using System.IO;

namespace BCR.Bio.Wsq.Decoder
{
	/// <summary>
	/// IWsqDecoder describa la interfaz de los decodificadores wsq.
	/// </summary>
	public interface IWsqDecoder
	{
		/// <summary>
		/// Decodifica un segmento de memoria codificado en formato wsq
		/// </summary>
		/// <param name="wsq">El arreglo en memoria que contiene la imagen codificada en formato wsq</param>
		/// <param name="width">ancho de la imagen reconstruida</param>
		/// <param name="height">alto de la imagen reconstruida</param>
		/// <param name="raw">imagen en formato raw</param>
		/// <returns>true si el decodificador pudo descomprimir la imagen, false en caso de falla</returns>
		bool DecodeMemory(byte[] wsq, out short width, out short height, out byte[] raw);

		/// <summary>
		/// Decodifica un stream que viene en formato wsq
		/// </summary>
		/// <param name="inStream">stream de entrada que contiene la imagen wsq</param>
		/// <param name="outStream">stream donde quedar� la imagen en formato raw</param>
		/// <param name="width">ancho de la imagen descomprimida</param>
		/// <param name="height">alto de la imagen descomprimida</param>
		/// <returns>true si pudo decodificar, false si hubo alg�n problema</returns>
		bool DecodeStream(Stream inStream, Stream outStream, out short width, out short height);
	}
}
