//#define PROF 
using System;

namespace BCR.Bio.Wsq.Encoder
{
	/// <summary>
	/// Esta clase representa la imagen normalizando los valores
	/// de los pixels de modo que la media sea cercana a 0.0
	/// </summary>
	public class NormalizedData
	{



		static readonly float[] hifilt = {
											  0.064538882628938f,
											 -0.040689417609558f,
											 -0.41809227322221f,
											  0.78848561640566f,
											 -0.41809227322221f,
											 -0.04068941760955f,
											 0.064538882628938f
										 };

		
		static readonly float[] lofilt = {
							   0.037828455506995f,
							  -0.023849465019380f,
							  -0.11062440441842f,
							   0.37740285561265f,
							   0.85269867900940f,
							   0.37740285561265f,
							  -0.11062440441842f,
							  -0.023849465019380f,
							   0.037828455506995f
						  };


		public NormalizedData(byte[] raw, int w, int h)
		{
			width = w;
			height = h;
			width2 = w*2;
			width4 = w*4;
			Normalize(raw);
		}



		/// <summary>
		/// factor m
		/// </summary>
		public float M
		{
			get { return mFactor; }
		}

		/// <summary>
		/// factor r
		/// </summary>
		public float R
		{
			get { return rFactor; }
		}

		byte[] raw;
		private void Normalize(byte[] raw)
		{
			this.raw = raw;
			int numPix = raw.Length;
			ndata = new float[numPix];
			fdata1 = new float[width*height];

			int sum = 0;
			byte imax = 0;
			byte imin = 255;

			for (int i = 0; i < numPix; i++)
			{
				byte pixel = raw[i];
				sum += pixel;
				if (pixel > imax)
					imax = pixel;
				if (pixel < imin)
					imin = pixel;
			}

			mFactor = ((float) sum/(float) numPix);

			rFactor = Math.Max(imax - mFactor, mFactor - imin) / 128.0f;
			
			for (int i = 0; i < 256; i++)
			{
				//byte val = (byte) i;
				float fval = (((float) i - mFactor)/rFactor);
				pval[i] = fval;
				for (int j = 0; j < 7; j++)
				{
					hival[j,i] = hifilt[j]*fval;
					loval[j,i] = lofilt[j]*fval;
				}
				loval[7,i] = lofilt[7]*fval;
				loval[8,i] = lofilt[8]*fval;
			}
			for (int i = 0; i < numPix; i++)
				ndata[i] = pval[raw[i]];
		}

		float[]  pval  = new float[256];
		float[,] loval = new float[9,256];
		float[,] hival = new float[7,256];
		
		internal void WsqDecompose(WaveletTreeNode[] treeNodes)
		{

		
			/* Compute the Wavelet image decomposition. */
			
			GetLetsRows00(); //0, height, width);//.OffSet, wtree.LenY, wtree.LenX);
			GetLetsCols00(); //0, width, height);//.OffSet, wtree.LenX, wtree.LenY);
			//GetLets00(treeNodes[0]);
			GetLets00(treeNodes[1]);
			GetLets10(treeNodes[2]);
			GetLets01(treeNodes[3]);
			GetLets10(treeNodes[4]);
			GetLets01(treeNodes[5]);
			GetLets00(treeNodes[6]);
			GetLets10(treeNodes[7]);
			GetLets01(treeNodes[8]);
			GetLets11(treeNodes[9]);
			GetLets00(treeNodes[10]);
			GetLets10(treeNodes[11]);
			GetLets01(treeNodes[12]);
			GetLets11(treeNodes[13]);
			GetLets00(treeNodes[14]);
			GetLets00(treeNodes[15]);
			GetLets10(treeNodes[16]);
			GetLets01(treeNodes[17]);
			GetLets11(treeNodes[18]);
			GetLets00(treeNodes[19]);
		}

		private void GetLets00(WaveletTreeNode wtree)
		{
			GetLetsRows(wtree.OffSet, wtree.LenY, wtree.LenX);
			GetLetsCols(wtree.OffSet, wtree.LenX, wtree.LenY);
		}

		private void GetLets01(WaveletTreeNode wtree)
		{
			GetLetsRows(wtree.OffSet, wtree.LenY, wtree.LenX);
			GetLetsColsInv(wtree.OffSet, wtree.LenX, wtree.LenY);
		}

		private void GetLets10(WaveletTreeNode wtree)
		{
			GetLetsRowsInv(wtree.OffSet, wtree.LenY, wtree.LenX);
			GetLetsCols(wtree.OffSet, wtree.LenX, wtree.LenY);
		}

		private void GetLets11(WaveletTreeNode wtree)
		{
			GetLetsRowsInv(wtree.OffSet, wtree.LenY, wtree.LenX);
			GetLetsColsInv(wtree.OffSet, wtree.LenX, wtree.LenY);
		}



		void GetLetsRows(int offset, int lenX, int lenY)
		{
			int da_ev = lenY & 0x01;
			int llen = (lenY+da_ev)>>1;
			int hlen = llen - da_ev;
			int len2stride = (lenY-1);
			int pend = offset + lenX*width;
			for(int p0 = 0; offset < pend; offset += width, p0 += width)
			{
				PixLoop1(da_ev, offset, offset + len2stride, hlen, p0, p0 + llen);
			}
		}

		void GetLetsRowsInv(int offset, int lenX, int lenY)
		{
			int da_ev = lenY & 0x01;
			int llen = (lenY+da_ev)>>1;
			int hlen = llen - da_ev;
			int len2stride = (lenY-1);
			int pend = offset+lenX*width;
			for(int p0 = 0; offset < pend; offset += width, p0 += width)
			{
				PixLoop1(da_ev, offset, offset + len2stride, hlen, p0 + hlen, p0);
			}

		}


		void GetLetsRows00()
		{
			int da_ev = width & 0x01;
			int llen = (width+da_ev)>>1;
			int hlen = llen - da_ev;
			int hlen3 = hlen - 3;
			int pend = height*width;
			for(int p0 = 0; p0 < pend;  p0 += width)
			{
				Pix00Loop1(da_ev, p0, p0 + (width-1), hlen3, p0, p0 + llen);
			}
		}

		private void Pix00Loop1(int da_ev, int p0, int p1, int hlen, int lopass, int hipass)
		{
			int hspx = p0+2;
			int lspx = p0+4;
			
			fdata1[lopass++] = ApplyFilter00M1L(lspx, p0);
			fdata1[hipass++] = ApplyFilter00M1H(hspx, p0);
			hspx = p0;
			lspx = p0+2;
			
			fdata1[lopass++] = ApplyFilter00M1L(lspx, p0);
			fdata1[hipass++] = ApplyFilter00P1H(hspx, p1);
			hspx = p0+2;
			lspx = p0;
			
			fdata1[lopass++] = ApplyFilter00P1L(lspx, p1);
			fdata1[hipass++] = ApplyFilter00P1H(hspx, p1);

			hspx = p0+4;
			lspx = p0+2;
			while (hlen > 0)
			{
				fdata1[lopass++] = ApplyFilter00P1L(lspx, p1);
				
				fdata1[hipass++] = ApplyFilter00P1H(hspx, p1);
				lspx++; lspx++;
				hspx++; hspx++;
				hlen--;
			}
			if (da_ev != 0)
				fdata1[lopass] = ApplyFilter00P1L(lspx, p1);
			
		}

		private float ApplyFilter00M1L (int lpx, int p0)
		{
			float sum = loval[0, raw[lpx]];
		    int i;
			for(i = 1; i < 9 && lpx != p0; i++) 
			{
				--lpx;// += lpxstr;
				sum += loval[i, raw[lpx]]; 
			}
			for (; i < 9; i++)
			{
				++lpx;
				sum += loval[i,raw[lpx]];
			}
			return sum;
		}
		private float ApplyFilter00P1L (int lpx, int p1)
		{
			float sum = loval[0, raw[lpx]];
		
			int i;
			for(i = 1; lpx != p1 && i < 9; i++) 
			{
				++lpx;
				sum += loval[i, raw[lpx]]; 
			}
			for (; i < 9; i++)
			{
				--lpx;
				sum += loval[i,raw[lpx]];
			}
			return sum;
		}

		private float ApplyFilter00P1H (int lpx, int p1)
		{
			float sum = hival[0, raw[lpx]]; //ndata[lpx] * hifilt[0];
			int i;
			for(i = 1; lpx != p1 && i < 7; i++) 
			{
				++lpx;
				sum += hival[i, raw[lpx]]; //ndata[lpx] * hifilt[i];
			}
			for (; i < 7 ; i++)
			{
				--lpx;
				sum += hival[i, raw[lpx]];

			}
			return sum;
		}

		private float ApplyFilter00M1H (int lpx, int p0)
		{
			float sum = hival[0, raw[lpx]]; //ndata[lpx] * hifilt[0];
			int i;
			for(i = 1; lpx != p0 && i < 7; i++) 
			{
				--lpx;
				sum += hival[i, raw[lpx]]; //ndata[lpx] * hifilt[i];
			}
			for (; i < 7; i++)
			{
				++lpx;
				sum += hival[i, raw[lpx]];
			}
			return sum;
		}




		void GetLetsCols00()
		{
			int da_ev = height & 0x01;
			int llen = (height+da_ev)>>1;
			int hlen = llen - da_ev;
			int llen_stride = llen * width;
			int len2stride = (height-1)*width;
			int pend = width;
			for(int  p0 = 0; p0 < pend; p0++)
			{
				PixLoopW(da_ev, p0, p0 + len2stride, hlen, p0, p0 + llen_stride);
			}
		}


		void GetLetsCols(int offset, int lenX, int lenY)
		{
			int da_ev = lenY & 0x01;
			int llen = (lenY+da_ev)>>1;
			int hlen = llen - da_ev;
			int llen_stride = llen * width;
			int len2stride = (lenY-1)*width;
			int pend = offset + lenX;
			for(int  p0 = 0; offset < pend; offset++, p0++)
			{
				PixLoopW(da_ev, p0, p0 + len2stride, hlen, offset, offset + llen_stride);
			}
		}

		void GetLetsColsInv(int offset, int lenX, int lenY)
		{
			int len2stride = (lenY-1)*width;		
			int pend = offset + lenX;
			int da_ev = (lenY & 0x01);
			int llen = (lenY+da_ev) >> 1;
			int hlen = llen - da_ev;
			int hlen_stride = hlen * width;
			for(int p0 = 0; offset < pend; offset++, p0++)
			{
				PixLoopW(da_ev, p0, p0 + len2stride, hlen, offset + hlen_stride, offset);
			}
		}

		private void PixLoop1(int da_ev, int p0, int p1, int hlen, int lopass, int hipass)
		{
			int hspx = p0+2;
			int lspx = p0+4;
			
			fdata1[lopass++] = ApplyFilter1L(lspx, p0, p1, -1);
			fdata1[hipass++] = ApplyFilter1H(hspx, p0, p1, -1);
			hspx = p0;
			lspx = p0+2;
			hlen--;
			
			fdata1[lopass++] = ApplyFilter1L(lspx, p0, p1, -1);
			fdata1[hipass++] = ApplyFilter1H(hspx, p0, p1, 1);
			hspx = p0+2;
			lspx = p0;
			hlen--;
			while (hlen > 0)
			{
				fdata1[lopass++] = ApplyFilter1L(lspx, p0, p1, 1);
				fdata1[hipass++] = ApplyFilter1H(hspx, p0, p1, 1);
				lspx++; lspx++;
				hspx++; hspx++;
				hlen--;
			}
			if (da_ev != 0)
				fdata1[lopass] = ApplyFilter1L(lspx, p0, p1, 1);
			
		}


		private void PixLoopW(int da_ev, int p0, int p1, int hlen, int lopass, int hipass)
		{
			int hspx = p0 + width2;
			int lspx = p0 + width4;
			ndata[lopass] = ApplyFilterWL(lspx, p0, p1, -width);
			lopass += width;
			ndata[hipass] = ApplyFilterWH(hspx, p0, p1, -width);
			hipass += width;
			hspx = p0;
			lspx = p0 + width2;
			hlen--;
			
			ndata[lopass] = ApplyFilterWL(lspx, p0, p1, -width);
			lopass += width;
			ndata[hipass] = ApplyFilterWH(hspx, p0, p1, width);
			hipass += width;
			hspx = p0 + width2;
			lspx = p0;
			hlen--;
			
			
			while (hlen > 0)
			{
				ndata[lopass] = ApplyFilterWL(lspx, p0, p1, width);
				lopass += width;
				ndata[hipass] = ApplyFilterWH(hspx, p0, p1, width);
				hipass += width;
				lspx += width2;
				hspx += width2;
				hlen--;
			}
			
			if (da_ev != 0)
				ndata[lopass] = ApplyFilterWL(lspx, p0, p1, width);
		}



		private float ApplyFilter1L (int lpx, int p0, int p1, int lpxstr)
		{
			float sum = ndata[lpx] * lofilt[0];
		
			for(int i = 1; i < 9; i++) 
			{
				if(lpx == p0) 
					lpxstr = 1;
				else if(lpx == p1) 
					lpxstr = -1;
				lpx += lpxstr;
				sum += ndata[lpx] * lofilt[i];
			}
			return sum;
		}

		private float ApplyFilter1H (int lpx, int p0, int p1, int lpxstr)
		{
			float sum = ndata[lpx] * hifilt[0];
			for(int i = 1; i < 7; i++) 
			{
				if(lpx == p0) 
					lpxstr = 1;
				else if(lpx == p1) 
					lpxstr = -1;
				lpx += lpxstr;
				sum += ndata[lpx] * hifilt[i];
			}
			return sum;
		}




		private float ApplyFilterWL (int lpx, int p0, int p1, int lpxstr)
		{
		
			float sum = fdata1[lpx] * lofilt[0];

			for(int i = 1; i < 9; i++) 
			{
				if(lpx == p0) 
					lpxstr = width;
				else if(lpx == p1) 
					lpxstr = -width;
				lpx += lpxstr;
				sum += fdata1[lpx] * lofilt[i];
			}
			return sum;
		}

		private float ApplyFilterWH (int lpx, int p0, int p1, int lpxstr)
		{
			float sum = fdata1[lpx] * hifilt[0];
			for(int i = 1; i < 7; i++) 
			{
				if(lpx == p0) 
					lpxstr = width;
				else if(lpx == p1) 
					lpxstr = -width;
				lpx += lpxstr;
				sum += fdata1[lpx] * hifilt[i];
			}
			return sum;
		}

		private float[] ndata;
		private float[] fdata1;
		private float mFactor;
		private float rFactor;
		private int width;
		private int height;
		private int width2;
		private int width4;


		/// <summary>
		/// datos
		/// </summary>
		public float[] Data
		{
			get { return ndata; }
		}


		/// <summary>
		/// Filtro banda baja
		/// </summary>
		public static float[] LoFilt
		{
			get { return lofilt; }
		}

		/// <summary>
		/// filtro banda alta
		/// </summary>
		public static float[] HiFilt
		{
			get { return hifilt; }
		}
	}
}