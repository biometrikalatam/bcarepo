﻿using BCR.Bio.Enum;
using BCR.Bio.Wsq.Decoder;
using BCR.System.Imaging;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace BCR.Bio.Wsq
{
    public class BioConvert
    {
        public static string GetJPGfromSample(string sample, int tipo)
        {
            byte[] byRaw;
            byte[] byJPG;
            short w, h;
            try
            {


                if (tipo == (int)MinutiaeTypeEnum.MINUTIAETYPE_WSQ)
                {
                    WsqDecoder dec = new WsqDecoder();
                    dec.DecodeMemory(Convert.FromBase64String(sample), out w, out h, out byRaw);
                }
                else
                {
                    byRaw = Convert.FromBase64String(sample);
                }

                Bitmap bmp = ImageProcessor.RawToBitmap(byRaw, 512, 512);
                byJPG = new byte[bmp.Width * bmp.Height];
                MemoryStream ms = new MemoryStream(byJPG);
                bmp.Save(ms, ImageFormat.Jpeg);

                return Convert.ToBase64String(byJPG);
            }
            catch (Exception ex)
            {
                //LOG.Error("IdentityController.GetJPGfromSample Error", ex);
                return null;
            }
        }
    }
}