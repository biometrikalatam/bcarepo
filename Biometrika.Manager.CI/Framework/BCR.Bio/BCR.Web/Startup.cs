﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BCR.Web.Startup))]
namespace BCR.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
