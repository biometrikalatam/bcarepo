﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BCR.System.Crypto
{
    public static class CryptoProvider
    {
        public static string Encrypt(string text)
        {

            UTF8Encoding UTF8_Encoding = new UTF8Encoding();
            MD5CryptoServiceProvider MD5CryptoServiceProvider = new MD5CryptoServiceProvider();
            Byte[] BioKey = MD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(Constants.System.BIOMETRIKA_KEY));
            TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider()
            {
                Key = BioKey,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
                     
            Byte[] textBytes = Encoding.UTF8.GetBytes(text);    

            ICryptoTransform cryptoTransform = tripleDESCryptoServiceProvider.CreateEncryptor();
            Byte[] encrypted = cryptoTransform.TransformFinalBlock(textBytes, 0, textBytes.Length);

            MD5CryptoServiceProvider.Clear();
            tripleDESCryptoServiceProvider.Clear();

            return Convert.ToBase64String(encrypted);

        }


        public static string Decrypt(string encrypted)
        {
            UTF8Encoding UTF8_Encoding = new UTF8Encoding();
            MD5CryptoServiceProvider MD5CryptoServiceProvider = new MD5CryptoServiceProvider();
            Byte[] BioKey = MD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(Constants.System.BIOMETRIKA_KEY));
            TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider()
            {
                Key = BioKey,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };

            Byte[] encryptedBuffer = Convert.FromBase64String(encrypted);

            ICryptoTransform cryptoTransform = tripleDESCryptoServiceProvider.CreateDecryptor();

            Byte[] decrypted = cryptoTransform.TransformFinalBlock(encryptedBuffer, 0, encryptedBuffer.Length);

            MD5CryptoServiceProvider.Clear();
            tripleDESCryptoServiceProvider.Clear();

            return Encoding.UTF8.GetString(decrypted);

        }
    }
}
