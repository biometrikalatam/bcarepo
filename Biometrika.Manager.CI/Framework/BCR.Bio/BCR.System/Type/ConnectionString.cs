﻿using System;
using System.Data.SqlClient;
using BCR.System.Enum;

namespace BCR.System.Type
{
    public class ConnectionString 
    {

        SqlConnectionStringBuilder connectionStringBuilder;

        public ConnectionString(string dataSource, string catalog, string userId, string password)
        {
            connectionStringBuilder = new SqlConnectionStringBuilder() 
            { 
                DataSource = dataSource, 
                InitialCatalog = catalog, 
                UserID = userId, 
                Password = password 
            };
        }

        public ConnectionString(string connectionString)
        {
            connectionStringBuilder = new SqlConnectionStringBuilder(connectionString);
        }

        //public string DataSource { get; set; }

        //public string Catalog { get; set; }

        //public string UserId { get; set; }

        //public String Password { get; set; }

        //public Database Database { get; set; }

        public String ToString()
        {
            return string.Format("data source={0};initial catalog={1};Integrated Security=false;user id={2};password={3};", 
                connectionStringBuilder.DataSource, 
                connectionStringBuilder.InitialCatalog, 
                connectionStringBuilder.UserID, 
                connectionStringBuilder.Password);
        }

        public string Key
        {
            get
            {
                return string.Format("s:{0}-c:{1}",connectionStringBuilder.DataSource,connectionStringBuilder.InitialCatalog);
            }
        }
    }
}