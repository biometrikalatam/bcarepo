﻿using System;

namespace BCR.System.Type
{
    public class Time
    {
        public Time(int hour, int minute, int second)
        {
            Hour = hour;
            Minute = minute;
            Second = second;
        }

        public int Hour { get; set; }

        public int Minute { get; set; }

        public int Second { get; set; }

        public DateTime ToDateTime()
        {
            return new DateTime(1970, 11, 2, Hour, Minute, Second);
        }

        public TimeSpan ToTimeSpan()
        {
            return new TimeSpan(Hour, Minute, Second);

        }
    }
}