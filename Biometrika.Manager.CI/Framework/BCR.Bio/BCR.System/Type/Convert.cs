﻿using System;

namespace BCR.System.Type
{
    public static class ConvertEx
    {
        public static bool ObjectToBoolean(object obj)
        {
            if (obj is bool)
                return (bool)obj;

            return false;
        }

        public static string ObjectToString(object obj)
        {
            if (obj is string)
                return (string)obj;

            return string.Empty;
        }

        public static DateTime? ObjectToDateTimeStrict(object obj)
        {
            return DateTime.Parse(obj.ToString());
        }

        public static DateTime? ObjectToDateTime(object obj)
        {
            if (obj is DateTime)
                if ((DateTime)obj != DateTime.MinValue)
                    return DateTime.Parse(obj.ToString());

            return null;
        }

        public static double ObjectToDouble(object obj)
        {
            if (obj is string)
                return double.Parse((string)obj);
            else
                if (obj is double)
                return (double)obj;

            return 0.0;
        }

        public static int ObjectToInt(object obj)
        {
            if (obj is string)
                return int.Parse((string)obj);
            else
            {
                if (obj is int)
                    return (int)obj;
                if (obj == null)
                    return 0;
            }

            return -1;
        }

        public static int? ObjectToIntNull(object obj)
        {
            if (obj == null)
                return null;
            else
                if (obj is string)
                return int.Parse((string)obj);
            else
                    if (obj is int)
                return (int)obj;

            return null;
        }
    }
}