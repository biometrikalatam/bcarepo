﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCR.System.Type
{
    public class RUT 
    {
        string rut = string.Empty;

        public RUT (string rut)
        {
            this.rut = rut;
        }

        public override string ToString ()
        {
            return this.rut;
        }

        public bool IsValid
        {
            get { return true; }
        }

        public string Number
        {
            get
            {
                return rut.Split('-')[0];
            }
        }

        public string Dv
        {
            get
            {
                return rut.Split('-')[1];
            }
        }
    }
}
