﻿using BCR.System.Enum;
using NHibernate;
using System.Collections.Generic;

namespace BCR.System.Type
{
    /// <summary>
    /// Collection of connections 
    /// </summary>
    public class ConnectionCollection
    {
        private static IDictionary<Database, ConnectionCollectionItem> connections = new Dictionary<Database, ConnectionCollectionItem>();

        public ConnectionCollection()
        {
            //if ( !connections.ContainsKey (Database.BCR_Key))
            //    connections.Add(Database.BCR_Key, null);

            //if (!connections.ContainsKey(Database.BCR_Log))
            //    connections.Add(Database.BCR_Log, null);
            
            //if (!connections.ContainsKey(Database.BCR_System))
            //    connections.Add(Database.BCR_System, null);

            if (!connections.ContainsKey(Database.BCR_Bio))
                connections.Add(Database.BCR_Bio, null);
        }


        /// <summary>
        /// Get an Item for connection collection
        /// </summary>
        /// <param name="databaseKey"></param>
        /// <returns></returns>
        public ConnectionCollectionItem GetConnectionCollectionItem(Database databaseKey)
        {
            return connections[databaseKey];
        }

        //public void AddSession(Database databaseKey, ConnectionCollectionItem connectionCollectionItem)
        //{
        //    connections[databaseKey] = connectionCollectionItem;
        //}

        /// <summary>
        /// Referesh a database with new Item
        /// </summary>
        /// <param name="databaseKey"></param>
        /// <param name="connectionCollectionItem"></param>
        public void Update(Database databaseKey, ConnectionCollectionItem connectionCollectionItem)
        {
            connections[databaseKey] = connectionCollectionItem;
        }

        /// <summary>
        /// Add a connectionstring to database
        /// </summary>
        /// <param name="databaseKey"></param>
        /// <param name="connectionString"></param>
        public void AddConnectionString(Database databaseKey, ConnectionString connectionString)
        {
            if (!connections.ContainsKey(databaseKey))
                connections.Add(databaseKey, null);

            connections[databaseKey] = new ConnectionCollectionItem(connectionString);
        }
    }
}