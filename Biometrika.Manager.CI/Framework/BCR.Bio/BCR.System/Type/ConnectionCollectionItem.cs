﻿using NHibernate;

namespace BCR.System.Type
{
    /// <summary>
    /// Pair of ConnectionString and Session status
    /// </summary>
    public class ConnectionCollectionItem
    {
        public ConnectionString ConnectionString { get; set; }

        public ISessionFactory SessionFactory { get; set; }

        public ConnectionCollectionItem(Type.ConnectionString connectionString)
        {
            this.ConnectionString = connectionString;
        }
    }
}