﻿using BCR.System.Attribute;
using BCR.System.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BCR.System.Type
{
    /// <summary>
    /// Enumerador de respuestas para la patente ingresada
    /// </summary>
    public enum PlateValidationResponse
    {
        ERROR,
        INVALID_PLATE,
        OK
    }

    /// <summary>
    /// Enumerador con los tipos de patron regex para usar en la validacion de la patente ingresada
    /// </summary>
    public enum ValidationPattern
    {
        [StringValue(@"(^0[a-zA-Z]{1}|(^[a-zA-Z]{2}))((\d{2})|(\w{2}))(\d{2})$")]
        CHL
    }

    /// <summary>
    /// Clase tipo de dato referente a patentes vehiculares
    /// </summary>
    public class VehiclePlate
    {
        private readonly Regex Regex;

        /// <summary>
        /// Patente vehicular
        /// </summary>
        public string PlateNumber { get; }
        /// <summary>
        /// Patron de validacion para la patente ingresada
        /// </summary>
        public ValidationPattern ValidationPattern { get; set; }

        /// <summary>
        /// Construye un objeto VehiclePlate con la patente y tipo de validacion ingresados
        /// </summary>
        /// <param name="plateNumber"></param>
        /// <param name="validationPattern"></param>
        public VehiclePlate(string plateNumber, ValidationPattern validationPattern)
        {
            if (string.IsNullOrEmpty(plateNumber))
                throw new ArgumentException("vacío o nulo ", nameof(plateNumber));

            ValidationPattern = validationPattern;
            PlateNumber = plateNumber;

            Regex = new Regex(EnumHelper.GetStringValue(ValidationPattern));
        }
        
        /// <summary>
        /// Método de validacion de la patente ingresada en base al tipo de validación seleccionada
        /// </summary>
        /// <returns></returns>
        public PlateValidationResponse Validate()
        {
            if (!Regex.Match(PlateNumber).Success)
                return PlateValidationResponse.INVALID_PLATE;
            return PlateValidationResponse.OK;
        }

        /// <summary>
        /// Método para obtener un arreglo con la patente separada en pares de caracteres
        /// </summary>
        /// <returns>string[] patente separada en pares de caracteres</returns>
        public string[] PairArray()
        {
            if (!Regex.Match(PlateNumber).Success)
                return null;

            IList<string> pairList = new List<string>();
            for (int i = 0; i < PlateNumber.Length; i+=2)
            {
                if (i < PlateNumber.Length)
                    pairList.Add(PlateNumber.Substring(i, 2));
                else
                    pairList.Add(PlateNumber.Substring(i, PlateNumber.Length - 1));
            }

            return pairList.ToArray();
        }
    }
}
