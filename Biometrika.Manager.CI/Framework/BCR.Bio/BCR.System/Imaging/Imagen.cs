using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace BCR.System.Imaging
{
	/// <summary>
	/// Clase para manejo de imagenes
	/// </summary>
	public class Imagen 
	{
		/// <summary>
		/// Constructor de una imagen
		/// </summary>
		public Imagen()
		{
			data = null;
		}

		/// <summary>
		/// Imagen a partir de un arreglo de bytes
		/// </summary>
		/// <param name="imagen">arreglo de bytes con la imagen</param>
		public Imagen(byte[] imagen)
		{
			FromArray(imagen);
		}

		/// <summary>
		/// Graba la imagen a un archivo
		/// </summary>
		/// <param name="filename">archivo en donde guardara la imagen</param>
		public void Grabar(string filename)
		{
			if (data == null)
				return;

			using (FileStream fs = new FileStream(filename, FileMode.Create))
			{
				fs.Write(data, 0, data.Length);
			}
		}

		/// <summary>
		/// Ajusta el ancho y alto de la imagen a los nuevos parametros
		/// </summary>
		/// <param name="width">ancho deseado</param>
		/// <param name="height">alto deseado</param>
		public void Ajustar(int width, int height)
		{
			if (data == null)
				return;

			using (MemoryStream ms = new MemoryStream(data))
			{
				Bitmap bmp = new Bitmap(ms);
				Bitmap newBmp = new Bitmap(width, height);
				Graphics gr = Graphics.FromImage (newBmp);
				gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
				gr.DrawImage (bmp, new Rectangle(0, 0, width, height), 0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel);
				gr.Dispose ();
				using (MemoryStream msb = new MemoryStream())
				{
					newBmp.Save(msb, ImageFormat.Jpeg);
					FromArray(msb.ToArray());
				}
			}
		}

		/// <summary>
		/// Este operador permite asignar a un picture object
		/// </summary>
		/// <param name="img"></param>
		/// <returns></returns>
		public static implicit operator Image(Imagen img)
		{
			return img.Image;
		}

		/// <summary>
		/// La imagen como un tipo Image de .Net
		/// </summary>
		public Image Image
		{
			get
			{
				Image imagen = null;
				using (MemoryStream ms = new MemoryStream(data))
				{
					imagen = new Bitmap(ms);
				}
				return imagen;
			}
		}

		/// <summary>
		/// Convierte la imagen a un formato binario
		/// </summary>
		/// <returns>Codificacion binaria de la imagen en Jpeg</returns>
		public byte[] ToArray()
		{
			return data;
			
		}

		/// <summary>
		/// Obtiene la foto de un formato binario, normalmente JPEG
		/// </summary>
		/// <param name="img"></param>
		public void FromArray(byte[] img)
		{
			data = img == null ? null : (byte[]) img.Clone();
			
		}

		/// <summary>
		/// Datos de la imagen
		/// </summary>
		public byte[] Data
		{
			get { return ToArray(); }
			set { FromArray(value); }
		}

		byte[] data;
	}
}
