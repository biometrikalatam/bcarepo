using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using Microsoft.VisualBasic.Compatibility.VB6;
//using log4net;

namespace BCR.System.Imaging
{
    /// <summary>
    /// Manipulaci�n de imagenes raw, usado principalmente por algoritmos,
    /// wsq, y afis.
    /// </summary>
    public sealed class ImageProcessor
    {
        //private static readonly ILog LOG = LogManager.GetLogger(typeof(ImageProcessor));

        private ImageProcessor() { }

        /// <summary>
        /// Convierte una imagen Raw de un ancho y alto en un bitmap de 24 bits por pixel
        /// del mismo tama�o.
        /// </summary>
        /// <param name="raw">La imagen raw, como escala de grises de 0(black)..255(white)</param>
        /// <param name="width">ancho de la imagen raw</param>
        /// <param name="height">altura de la imagen raw</param>
        /// <returns>Un Bitmap (.Net) con la imagen a 24bpp</returns>
        public static Bitmap RawToBitmap(byte[] raw, int width, int height)
        {
            Bitmap img = null;

            try
            {
                img = new Bitmap(width, height, PixelFormat.Format24bppRgb);
                BitmapData bmd = img.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, img.PixelFormat);

                unsafe
                {
                    byte* imgPtr = (byte*)bmd.Scan0.ToPointer();
                    int stride = bmd.Stride;
                    int imgPos = 0;
                    int rawPos = 0;
                    for (int row = 0; row < height; row++)
                    {
                        int rowPos = imgPos;
                        for (int col = 0; col < width; col++)
                        {
                            byte val = raw[rawPos++];
                            imgPtr[rowPos] = imgPtr[rowPos + 1] = imgPtr[rowPos + 2] = val;
                            rowPos += 3;
                        }
                        imgPos += stride;
                    }
                }
                img.UnlockBits(bmd);
            }
            catch (Exception ex)
            {
                //LOG.Error("ImageProcessor.RawToBitmap", ex);
            }
            return img;
        }

        /// <summary>
        /// Recibe un bitmap (en cualquier tipo de formato de pixel) y entrega
        /// la imagen raw en formato de arreglo de bytes en escala de grises
        /// 0(black)...255(white)
        /// </summary>
        /// <param name="img">bitmap con la imagen</param>
        /// <returns>El arreglo de img.Width*img.Height bytes</returns>
        public static byte[] BitmapToRaw(Bitmap img)
        {
            if (img == null)
                return null;

            int width = img.Width;
            int height = img.Height;
            byte[] result = new byte[width * height];
            try
            {
                BitmapData bmd = img.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, img.PixelFormat);

                int step = 1;
                switch (img.PixelFormat)
                {
                    case PixelFormat.Format8bppIndexed:
                        step = 1;
                        break;
                    case PixelFormat.Format24bppRgb:
                        step = 3;
                        break;
                    case PixelFormat.Format32bppArgb:
                    case PixelFormat.Format32bppPArgb:
                    case PixelFormat.Format32bppRgb:
                        step = 4;
                        break;
                }

                unsafe
                {
                    byte* ptr = (byte*)bmd.Scan0.ToPointer();
                    int stride = bmd.Stride;
                    if (stride < 0)
                    {
                        stride = -stride;
                        int dstOffset = result.Length - width;
                        int outOffset = 0;
                        for (int y = height - 1; y >= 0; y--)
                        {
                            dstOffset = width * y;
                            int offset = outOffset;
                            for (int x = 0; x < width; x++)
                            {
                                int luma = (int)(ptr[offset] * 0.3 + ptr[offset + 1] * 0.59 + ptr[offset + 2] * 0.11);
                                result[dstOffset++] = (byte)(luma);
                                offset += step;
                            }
                            outOffset += stride;
                        }
                    }
                    else
                    {
                        int dstOffset = 0;
                        int outOffset = 0;
                        for (int y = 0; y < height; y++)
                        {
                            int offset = outOffset;
                            for (int x = 0; x < width; x++)
                            {
                                int luma = (int)(ptr[offset] * 0.3 + ptr[offset + 1] * 0.59 + ptr[offset + 2] * 0.11);
                                result[dstOffset++] = (byte)(luma);
                                offset += step;
                            }
                            outOffset += stride;
                        }
                    }
                }
                img.UnlockBits(bmd);
            }
            catch (Exception ex)
            {
                //LOG.Error("ImageProcessor.BitmapToRaw", ex);
            }
            return result;
        }

        /// <summary>
        /// Ajusta el tama�o de una imagen raw, esto sirve para cambiar el
        /// tama�o, si la imagen resultante es m�s grande se rellena con negro (0)
        /// </summary>
        /// <param name="raw">imagen raw original</param>
        /// <param name="wOrig">ancho de raw</param>
        /// <param name="hOrig">alto de raw</param>
        /// <param name="wDst">ancho deseado</param>
        /// <param name="hDst">alto deseado</param>
        /// <returns>un arreglo de bytes de tama�o wDst*hDst</returns>
        public static byte[] Adjust(byte[] raw, int wOrig, int hOrig, int wDst, int hDst)
        {


            byte[] result = new byte[wDst * hDst];
            try
            {
                for (int i = 0; i < result.Length; i++)
                {
                    result[i] = 255;
                }

                int offset = 0;
                int doffset = 0;
                int top = Math.Min(hDst, hOrig);
                int width = Math.Min(wDst, wOrig);
                for (int i = 0; i < top; i++)
                {
                    Buffer.BlockCopy(raw, offset, result, doffset, width);
                    offset += wOrig;
                    doffset += wDst;
                    if (i == 270)
                    {
                        int a = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("ImageProcessor.Adjust", ex);
                result = null;
            }
            return result;
        }

        /// <summary>
        /// Retorna el negativo del raw pasado como parametro
        /// </summary>
        /// <param name="raw"></param>
        /// <returns></returns>
        public static byte[] GetNegativeRaw(byte[] raw)
        {
            byte[] rawnegative = null;
            try
            {
                rawnegative = new byte[raw.Length];
                for (int i = 0; i < raw.Length; i++)
                {
                    rawnegative[i] = (byte)(255 - raw[i]);
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("ImageProcessor.GetNegativeRaw", ex);
                rawnegative = null;
            }
            return rawnegative;
        }

        /// <summary>
        /// Toma desde un IPicture y genera una im�gen de 500dpi, de 512x512.
        /// </summary>
        /// <param name="picture">IPicture con l aimagen</param>
        /// <param name="witdhdest">ancho destino</param>
        /// <param name="heightdest">alto destino</param>
        /// <param name="dpixorig">dpiX originen</param>
        /// <param name="dpiyorig">dpiY origen</param>
        /// <param name="dpixdest">dpiX destino</param>
        /// <param name="dpiydest">dpiY destino</param>
        /// <param name="msgErr">Descripcion de error si existiera</param>
        /// <returns></returns>
        public static byte[] NormalizaFromPicture(object picture,
            int witdhdest, int heightdest, int dpixorig, int dpiyorig,
            int dpixdest, int dpiydest, out string msgErr)
        {
            byte[] raw_rc_512 = null;
            msgErr = "S/C";
            try
            {
                Bitmap img = (Bitmap)Support.IPictureDispToImage(picture);
                img.SetResolution(dpixorig, dpiyorig);

                Bitmap image = new Bitmap(witdhdest, heightdest, PixelFormat.Format24bppRgb);
                image.SetResolution(dpixdest, dpiydest);
                Graphics gr = Graphics.FromImage(image);
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.DrawImage(img, new Rectangle(0, 0, witdhdest, heightdest), 0, 0,
                    img.Width, img.Height, GraphicsUnit.Pixel);

                byte[] rawaux = ImageProcessor.BitmapToRaw(image);
                raw_rc_512 = ImageProcessor.Adjust(rawaux, witdhdest, heightdest, 512, 512);
                gr.Dispose();
                img.Dispose();
            }
            catch (Exception ex)
            {
                msgErr = "Error en NormalizaFromPicture [" + ex.Message + "]";
                //LOG.Error("ImageProcessor.NormalizaFromPicture", ex);
            }
            return raw_rc_512;
        }

        /// <summary>
        /// Extrae RAW desde IPicture. Desde DP imagen.
        /// </summary>
        /// <param name="picture">IPicture conteninedo la imagen d ela huella</param>
        /// <param name="width">Ancho destino</param>
        /// <param name="height">Alto destino</param>
        /// <param name="msgErr">Descripcion de error si existe</param>
        /// <returns></returns>
        public static byte[] ExtraeBytesFromPicture(object picture, int width, int height, out string msgErr)
        {
            byte[] ret = null;
            msgErr = "S/C";
            try
            {
                int dpix = 700;
                int dpiy = 700;
                Bitmap img = (Bitmap)Support.IPictureDispToImage(picture);
                img.SetResolution(700, 700);
                Bitmap image = new Bitmap(width, height, PixelFormat.Format24bppRgb);
                image.SetResolution(dpix, dpiy);
                Graphics gr = Graphics.FromImage(image);
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.DrawImage(img, new Rectangle(0, 0, width, height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel);
                ret = ImageProcessor.BitmapToRaw(image);
                img.Dispose();
            }
            catch (Exception ex)
            {
                msgErr = "Error en ExtraeBytesFromPicture [" + ex.Message + "]";
                //LOG.Error("ImageProcessor.ExtraeBytesFromPicture", ex);
            }
            return ret;
        }

        /// <summary>
        /// Da vuelta verticalmente una imagen
        /// </summary>
        /// <param name="byImg">Arreglo de bytes con la imagen</param>
        /// <param name="wP">Ancho de la imagen</param>
        /// <param name="hP">Alto de la imagen</param>
        /// <returns>Imagen flipeada</returns>
        static public byte[] FlipVerticalImage(byte[] byImg, long wP, long hP)
        {
            long size = wP * hP;
            byte[] bAux = new byte[size];
            long k = 0;

            long w = size;
            try
            {
                while (w > 0)
                {
                    for (long h = wP; h > 0; h--)
                    {
                        bAux[k] = byImg[w - h];
                        k++;
                    }
                    w = w - wP;
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("ImageProcessor.FlipVerticalImage", ex);
            }

            return bAux;
        }

        /// <summary>
        /// Rota una imagen 90 grados a la derecha
        /// </summary>
        /// <param name="byImg">Arreglo de bytes con la imagen original</param>
        /// <param name="wP">Ancho de la imagen</param>
        /// <param name="hP">Alto de la imagen</param>
        /// <returns>Imagen girada</returns>
        static public byte[] RotateImage90Right(byte[] byImg, long wP, long hP)
        {
            long size = wP * hP;
            byte[] bAux = new byte[size];
            long k = 0;

            try
            {
                for (long w = 0; w < wP; w++)
                {
                    for (long h = hP; h > 0; h--)
                    {
                        bAux[k] = byImg[(h - 1) * 352 + w];
                        k++;
                    }
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("ImageProcessor.RotateImage90Right", ex);
            }

            return bAux;
        }

        /// <summary>
        /// Rota una imagen 90 grados a la izquierda
        /// </summary>
        /// <param name="byImg">Arreglo de bytes con la imagen original</param>
        /// <param name="wP">Ancho de la imagen</param>
        /// <param name="hP">Alto de la imagen</param>
        /// <returns>Imagen girada</returns>
        static public byte[] RotateImgae90Left(byte[] byImg, long wP, long hP)
        {
            long size = wP * hP;
            byte[] bAux = new byte[size];

            byte[,] matrix = new byte[wP, hP];

            long k = 0;
            try
            {
                for (int y = 0; y <= hP - 1; y++)
                {
                    for (int x = 0; x <= wP - 1; x++)
                    {
                        matrix[x, y] = byImg[k];
                        k++;
                    }
                }

                //Lleno array desde matriz generando la rotacion
                k = 0;
                for (long x1 = (wP - 1); x1 >= 0; x1--)
                {
                    for (int y1 = 0; y1 <= hP - 1; y1++)
                    {
                        bAux[k] = matrix[x1, y1];
                        k++;
                    }
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("ImageProcessor.RotateImgae90Left", ex);
            }
            return bAux;
        }

        /// <summary>
        /// Convierte toda la imagen al negativo de su contenido
        /// </summary>
        /// <param name="byImg">Arreglo de bytes con la imagen original</param>
        /// <returns>Imagen en negativo</returns>
        static public byte[] ConvertNegative(byte[] byImg)
        {
            byte[] byAux = new byte[byImg.Length];
            // Cambia cada pixel de negativo a positivo
            for (int i = 0; i < byImg.Length; i++)
            {
                byAux[i] = (byte)~byImg[i]; //(255 - byImg[i]);
            }

            return byAux;
        }

        /// <summary>
        /// Dado un RAW, lo devuelve como BITMAP
        /// </summary>
        /// <param name="raw">Arreglo de bytes contenineod RAW</param>
        /// <returns>Imagen BITMAP</returns>
        static public Image ToImage(byte[] raw)
        {
            if (raw == null)
                return null;
            Bitmap bmp = null;
            try
            {
                using (MemoryStream ms = new MemoryStream(raw))
                {
                    bmp = new Bitmap(ms);
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("ImageProcessor.ToImage", ex);
            }
            return bmp;
        }

        static public byte[] FillRaw(byte[] raw, int wOrig, int hOrig, int wDst, int hDst)
        {
            byte[] result = new byte[wDst * hDst];
            int auxRead = 0;
            int auxWrite = 0;

            try
            {
                for (int i1 = 0; i1 < result.Length; i1++)
                {
                    result[i1] = 255;
                }

                for (int i = 0; i < hOrig - 1; i++)
                {
                    for (int j = 0; j < wOrig; j++)
                    {
                        result[auxWrite + j] = raw[auxRead + j];
                    }
                    auxRead += wOrig;
                    auxWrite += wDst;
                    if (i == 270)
                    {
                        int aa = 0;
                    }
                }


            }
            catch (Exception ex)
            {
                //LOG.Error("ImageProcessor.Adjust", ex);
                result = null;
            }
            return result;
        }
    }
}
