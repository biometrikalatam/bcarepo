﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCR.System
{
    public class BioMrz
    {
        private String documentnumber;
        private String country;
        private String mrz;
        private String dateofbirth;
        private String dateofexpiry;
        private String nacionality;
        private String sex;
        private String motherlastmame;
        private String patherlastmame;
        private String firstrow;
        private String secondrow;
        private String lastrow;
        private String name; 
        private String dni;
        private String photo;

        public BioMrz()
        {

        }

        public BioMrz(String MRZ)
        {
            mrz = MRZ;
            firstrow = mrz.Substring(1, 29);
            secondrow = mrz.Substring(31, 30);
            lastrow = mrz.Substring(62, 30);
        }

        /// <summary>
        ///  Permite recuperar el número de documento de la cédula desde el MRZ
        /// </summary>
        public String DocumentNumber
        {
            get
            {
                if (String.IsNullOrEmpty(mrz))
                {                   
                    documentnumber = "N/I";
                }
                else
                {
                    documentnumber = mrz.Substring(5, 9); ;
                }
                return documentnumber;
            }
            set
            {
                documentnumber = value;
            }
        }
        /// <summary>
        /// Permite recuperar el país de emisión de la cédula desde el MRZ.
        /// </summary>
        public String Country
        {
            get
            {
                if (String.IsNullOrEmpty(mrz))
                {
                    
                    country = "N/I";
                }
                else
                {
                    country = mrz.Substring(2, 3);
                }
                return country;
            }
            set
            {
                country = value;
            }
        }
        /// <summary>
        /// Permite recuperar la fecha de nacimiento de la persona desde el MRZ.
        /// </summary>
        public String DateOfBirth
        {
            get
            {
                if (String.IsNullOrEmpty(mrz))
                {                    
                    dateofbirth = "N/I";
                }
                else
                {
                    dateofbirth = mrz.Substring(30, 6);
                }
                return dateofbirth;
            }
            set
            {
                DateOfBirth = value;
            }
        }
        /// <summary>
        /// Permite recuperar el sexo de la persona, F o M, desde el MRZ
        /// </summary>
        public String Sex
        {
            get
            {
                if (String.IsNullOrEmpty(mrz))
                {                    
                    sex = "N/I";
                }
                else
                {
                    sex = secondrow.Substring(7, 1);
                }
                return sex;
            }
            set
            {
                Sex = value;
            }
        }
        /// <summary>
        /// Permite recuperar la fecha de expiración de la cédula desde el MRZ
        /// </summary>
        public String DateOfExpiry
        {
            get
            {
                if (String.IsNullOrEmpty(mrz))
                {
                    
                    dateofexpiry = "N/I";
                }
                else
                {
                    dateofexpiry = mrz.Substring(38, 6);
                }
                return dateofexpiry;
            }
            set
            {
                dateofexpiry = value;
            }
        }
        /// <summary>
        /// Permite recuperar la nacionalidad de la persona desde el MRZ.
        /// </summary>
        public String Nationality
        {
            get
            {
                if (String.IsNullOrEmpty(mrz))
                {                    
                    nacionality = "N/I";
                }
                else
                {
                    nacionality = mrz.Substring(45, 3);
                }
                return nacionality;
            }
            set
            {
                Nationality = value;
            }
        }
        /// <summary>
        /// Permite recuperar el apellido paterno de la persona desde el MRZ
        /// </summary>
        public String PatherLastName
        {
            get
            {
                if (String.IsNullOrEmpty(mrz))
                {
                   
                    patherlastmame = "N/I";
                }
                else
                {
                    int separator = lastrow.IndexOf("<<");
                    patherlastmame = lastrow.Substring(0, separator).Split('<')[0];
                }
                return patherlastmame.Trim();
            }
            set
            {
                patherlastmame = value;
            }
        }
        /// <summary>
        /// Permite recuperar el apellido materno de la persona desde el MRZ
        /// </summary>
        public String MotherLastName
        {
            get
            {
                if (String.IsNullOrEmpty(mrz))
                {
                    
                    motherlastmame = "N/I";
                }
                else
                {
                    int separator = lastrow.IndexOf("<<");
                    if (lastrow.Substring(0, separator).Contains('<'))
                    {
                        motherlastmame = lastrow.Substring(0, separator).Split('<')[1];
                    }
                    else
                    {
                        
                        motherlastmame = String.Empty;
                    }
                }
                return motherlastmame.Trim();
            }
            set
            {
                motherlastmame = value;
            }
        }
        /// <summary>
        /// Permite recuperar el nombre extraído del MRZ.
        /// </summary>
        public String Name
        {
            get
            {
                if (String.IsNullOrEmpty(mrz))
                {                   
                    name = "N/I";
                }
                else
                {
                    int separator = lastrow.IndexOf("<<");
                    name = lastrow.Substring(separator + 1).Replace('<', ' ');

                }
                return name.Trim();
            }
            set
            {
                name = value;
            }
        }
        public String DNI
        {
            get
            {
                if (String.IsNullOrEmpty(mrz))
                {                    
                    name = "N/I";
                }
                else
                {
                    dni = mrz.Substring(48, 11).Split('<')[0] + "-" + mrz.Substring(48, 11).Split('<')[1]; ;
                }
                return dni;
            }
            set
            {
                dni = value;
            }
        }
        public String Photo
        {
            get { return photo; }
            set
            {
                photo = value;
            }
        }
    }
}