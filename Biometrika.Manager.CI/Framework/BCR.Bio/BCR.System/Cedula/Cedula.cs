﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.System.Cedula
{
    public class Cedula
    {
        private String typeid;
        private String valueid;
        private String name;
        private String patherlastname;
        private String motherlastname;
        private String sex;
        private String documentnumber;
        private String documentexpiration;
        private String birthdate;
        private String nationality;
        private String photo;
        private String signature;

        public String TypeId
        {
            get { return typeid; }
            set
            {
                typeid = value;
            }
        }

        public String ValueId
        {
            get { return valueid; }
            set { valueid = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }
        
        public String Patherlastname
        {
            get { return patherlastname; }
            set { patherlastname = value; }
        }
        public String Motherlastname
        {
            get { return motherlastname; }
            set { motherlastname = value; }
        }
        public String Sex
        {
            get { return sex; }
            set { sex = value; }
        }
        public String Documentnumber
        {
            get { return documentnumber; }
            set { documentnumber = value; }
        }
        public String Documentexpiration
        {
            get { return documentexpiration; }
            set { documentexpiration = value; }
        }
        public String Birthdate
        {
            get { return birthdate; }
            set { birthdate = value; }
        }
        public String Nationality
        {
            get { return nationality; }
            set { nationality = value; }
        }
        public String Photo
        {
            get { return photo; }
            set { photo = value; }
        }
        public String Signature
        {
            get { return signature; }
            set { signature = value; }
        }

    }
}
