﻿using BCR.System.Type;
namespace BCR.System.Configuration
{
    public class NhibernateParams
    {
        public ConnectionString ConnectionString { get; set; }

        public string[] Mappings { get; set; }

        public bool EnableAudit { get; set; }

        
    }
}