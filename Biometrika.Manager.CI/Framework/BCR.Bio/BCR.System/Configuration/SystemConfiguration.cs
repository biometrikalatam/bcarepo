﻿using BCR.System.Crypto;
using BCR.System.Enum;
using BCR.System.Log;
using BCR.System.Log.Abstract;
using BCR.System.Type;
using System;
using System.Configuration;

namespace BCR.System.Configuration
{
    /// <summary>
    /// Configuración general del sistema
    /// </summary>
    public class SystemConfiguration
    {
        private static AbstractLogger abstractLogProvider { get; set; }

        public SystemConfiguration()
            : this(new LogConfiguration(LogProvider.Log4Net, ConnectionStringBio, LogType.All), true)
        {
        }

        public SystemConfiguration(LogConfiguration logConfiguration, bool enableAudit)
        {
            if (abstractLogProvider == null)
            {
                abstractLogProvider = LogProviderFactoy.GetLogger(logConfiguration);
                  
                Log4Bio.Initialize(abstractLogProvider);

                if (enableAudit)
                {
                }
            }
        }

        /// <summary>
        /// Get the current system logger
        /// </summary>
        /// <returns></returns>
        public AbstractLogger GetLogger()
        {
            if (abstractLogProvider == null)
                abstractLogProvider = LogProviderFactoy.GetLogger(new LogConfiguration(LogProvider.Log4Net, ConnectionStringBio, LogType.Error));

            return abstractLogProvider;
        }

        public void Log(string message, LogType logType)
        {
            abstractLogProvider.Log(message, logType);
        }

        public void Log(string message, LogType logType, string context)
        {
            abstractLogProvider.Log(message, logType, context);
        }

        public static ConnectionString ConnectionStringBio
        {
            get
            {
                return GetConnectionStringFromConfigKey("BCR_BIO");
            }
        }

        public NhibernateParams HibernateParams
        {
            get
            {
                NhibernateParams nhibernateParams = new NhibernateParams();
                nhibernateParams.ConnectionString = ConnectionStringBio;
                nhibernateParams.EnableAudit = bool.Parse(ConfigurationManager.AppSettings["EnableAudit"]);
                nhibernateParams.Mappings = ConfigurationManager.AppSettings["Mappings"].Split(',');

                return nhibernateParams;
            }
        }

        private static ConnectionString GetConnectionStringFromConfigKey(string key)
        {
            return new ConnectionString(CryptoProvider.Decrypt(ConfigurationManager.AppSettings[key]));
        }
    }
}