﻿using System.Collections.Generic;
using System.Linq;

namespace BCR.System.Validation
{
    public class ValidationDictionary
    {
        private  Dictionary<string, string> dictionary = new Dictionary<string, string>();

        public void AddError(string key, string errorMessage)
        {
            if (dictionary == null)
                dictionary = new Dictionary<string, string>();

            if (dictionary.Keys.Contains(key))
                dictionary[key] = string.Concat(dictionary[key], "-", errorMessage);
            else
                dictionary.Add(key, errorMessage);
        }

        public bool IsValid
        {
            get
            {
                return dictionary.Keys.Count == 0;
            }
        }

        public Dictionary<string, string> ErrorList
        {
            get
            {
                return dictionary;
            }
        }

        public void Clear()
        {
            dictionary = new Dictionary<string, string>();
        }

        public string FullStack
        {
            get
            {
                string stack = string.Empty;
                foreach (var item in dictionary.Keys)
                {
                    stack = string.Concat(stack, ":", item, ":", dictionary[item]);
                }
                return stack;
            }
        }
    }
}