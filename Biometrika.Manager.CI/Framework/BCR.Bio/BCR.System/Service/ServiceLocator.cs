﻿using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.System.Service
{
    public  class ServiceLocator
    {
        //private static Dictionary<Type, object> instantiadedServices = new Dictionary<Type, object>();
        private static Dictionary<string, object> instantiadedServices = new Dictionary<string, object>();
        private static Dictionary<global::System.Type, object> index = new Dictionary<global::System.Type, object>();



        public static void Add<R, S>()
        {
            if (!index.ContainsKey(typeof(S)))
            {
                //R repository = (R)Activator.CreateInstance(typeof(R));

                index.Add(typeof(S), (R)Activator.CreateInstance(typeof(R)));
            }
            return;
        }

        //public static async Task AddAsync<R, S>()
        //{
        //    if (!index.ContainsKey(typeof(S)))
        //    {
        //        R repository = (R)Activator.CreateInstance(typeof(R));

        //        index.Add(typeof(S), repository);
        //    }
        //    return;
        //}

        public static S GetService<S>()
        {
            try
            {
                if (!instantiadedServices.ContainsKey(typeof(S).Name))
                {
                    instantiadedServices.Add(typeof(S).Name, (S)Activator.CreateInstance(typeof(S), new object[] { index[typeof(S)] }));
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al cargar los servicios en la cache", e);
            }

            return (S)instantiadedServices[typeof(S).Name];
        }
    }
}
