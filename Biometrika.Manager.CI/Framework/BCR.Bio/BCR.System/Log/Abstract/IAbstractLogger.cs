﻿using BCR.System.Enum;
using System;


namespace BCR.System.Log.Abstract
{
    /// <summary>
    /// Interface for abstract logger
    /// </summary>
    public interface IAbstractLogger
    {
        void Log(string message, LogType logType);

        void Log(string message, LogType logType, string Context);

        void Error(string message);

        void Debug(string message);

        void Error(string message, Exception e);

        void Fatal(string message);

        void Warn(string message);

        void Info(string message);



        void Warn(string msg, Exception ex);
    }
}