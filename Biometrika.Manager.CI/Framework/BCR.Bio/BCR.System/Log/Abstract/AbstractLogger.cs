﻿using BCR.System.Enum;
using System;

namespace BCR.System.Log.Abstract
{
    /// <summary>
    /// Modelo abstracto de un  Logger
    /// </summary>
    public abstract class AbstractLogger : IAbstractLogger
    {
        protected bool active = false;

        public abstract void Log(string message, LogType logType);

        public abstract void Log(string message, LogType logType, string Context);

        public virtual void Error(string message)
        {
            Log(message, LogType.Error);
        }

        public virtual void Debug(string message)
        {
            Log(message, LogType.Debug);
        }

        public virtual void Error(string message, global::System.Exception e)
        {
            message = string.Concat(message, " Exception : ", e.Message);
            if (e.InnerException!=null)
            {
                message = string.Concat(message, " Exception : ", e.InnerException.Message);
                message = string.Concat(message, " Exception : ", e.InnerException.StackTrace);

            }

            Log(message, LogType.Error);
        }

        public void Fatal(string message)
        {
            Log(message, LogType.Fatal);
        }

        public void Warn(string message)
        {
            Log(message, LogType.Warning);
        }


        public void Info(string message)
        {
            Log(message, LogType.Info);
        }


        public void Warn(string msg, Exception ex)
        {
            Log(msg, LogType.Warning);
        }
    }
}