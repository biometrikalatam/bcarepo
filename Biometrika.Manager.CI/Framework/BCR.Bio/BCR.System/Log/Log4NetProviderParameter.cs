﻿using BCR.System.Configuration;
using BCR.System.Enum;
using BCR.System.Log.Interface;
using BCR.System.Type;
using log4net.Core;
using System.IO;

namespace BCR.System.Log
{
   /// <summary>
   /// Log 4 net paramter provider
   /// </summary>
    public class Log4NetProviderParameter : ILogProviderParameter
    {
        public Log4NetProviderParameter()
        {
            Level = LogType.Error;
        }

        public FileInfo LogFileName { get; set; }

        public LogRepositoryType LogRepositoryType { get; set; }

        public ConnectionString ConnectionString { get; set; }

        public LogType Level { get; set; }

        public LogType NHibernateLevel { get; set; }
    }
}