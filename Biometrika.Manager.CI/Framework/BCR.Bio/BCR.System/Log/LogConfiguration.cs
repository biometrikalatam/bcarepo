﻿using BCR.System.Enum;
using BCR.System.Type;
using System.IO;

namespace BCR.System.Log
{
    /// <summary>
    /// Configuración del Logger
    /// </summary>
    public class LogConfiguration
    {
        public LogProvider LogProvider { get; set; }

        public LogRepositoryType LogRepositoryType { get; set; }

        public FileInfo LogPath { get; set; }

        public string EventLabel { get; set; }

        public ConnectionString ConnectionString { get; set; }

        public LogType Level { get; set; }
        public LogType NHibernateLevel { get; set; }

        public LogConfiguration(FileInfo logPath)
        {
            this.LogProvider = LogProvider.Log4Net;
            this.LogPath = logPath;
            this.LogRepositoryType = Enum.LogRepositoryType.File;
            this.Level = LogType.Error;
        }

        public LogConfiguration(string eventLabel)
        {
            this.LogProvider = LogProvider.Log4Net;
            this.EventLabel = eventLabel;
            this.LogRepositoryType = Enum.LogRepositoryType.EventLog;
        }

        public LogConfiguration(ConnectionString connectionString, LogType level) :
            this(connectionString)
        {
            this.LogProvider = LogProvider.Log4Net;
            this.Level = level;
        }

        public LogConfiguration(ConnectionString connectionString)
        {
            this.LogProvider = LogProvider.Log4Net;
            this.ConnectionString = connectionString;
            this.LogRepositoryType = Enum.LogRepositoryType.Database;
        }

        public LogConfiguration(LogProvider logProvider, FileInfo logPath, LogType level):
            this(logProvider, logPath)
        {
            
            this.Level = level;
        }

        /// <summary>
        /// Constructor de configuración de Log, con opcion de nivel de log de NHibernate
        /// </summary>
        /// <param name="logProvider">Tipo de log a utilizar</param>
        /// <param name="logPath">Ruta donde se van a generar los archivos de log</param>
        /// <param name="level">Nivel de log a escribir</param>
        /// <param name="nHibernateLevel">Nivel de log de NHibernate a escribir</param>
        public LogConfiguration(LogProvider logProvider, FileInfo logPath, LogType level, LogType nHibernateLevel) :
            this(logProvider, logPath)
        {
            this.NHibernateLevel = nHibernateLevel;
            this.Level = level;
        }

        public LogConfiguration(LogProvider logProvider, FileInfo logPath)
        {
            this.LogProvider = logProvider;
            this.LogPath = logPath;
            this.LogRepositoryType = Enum.LogRepositoryType.File;
            this.Level = LogType.All;
        }

        public LogConfiguration(LogProvider logProvider, string eventLabel)
        {
            this.LogProvider = logProvider;
            this.EventLabel = eventLabel;
            this.LogRepositoryType = Enum.LogRepositoryType.EventLog;
        }

        public LogConfiguration(LogProvider logProvider, ConnectionString connectionString, LogType level) :
            this(logProvider, connectionString)
        {
            this.Level = level;
        }

        public LogConfiguration(LogProvider logProvider, ConnectionString connectionString)
        {
            this.LogProvider = logProvider;
            this.ConnectionString = connectionString;
            this.LogRepositoryType = Enum.LogRepositoryType.Database;
        }
    }
}