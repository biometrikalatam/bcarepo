﻿using BCR.System.Enum;
using BCR.System.Log.Abstract;
using BCR.System.Log.Interface;
using global::System.Data;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;

namespace BCR.System.Log
{
    /// <summary>
    /// Implementación de Log4NetLogger
    /// </summary>
    public sealed class Log4NetLogger : AbstractLogger
    {
        public Log4NetLogger(ILogProviderParameter log4NetProviderParameter)
        {
            Log4NetProviderParameter parameter = (Log4NetProviderParameter)log4NetProviderParameter;

            switch (parameter.LogRepositoryType)
            {
                case Enum.LogRepositoryType.File:
                    SetFileAppender(parameter);
                    break;

                case Enum.LogRepositoryType.Database:
                    SetDatabase(parameter);
                    break;

                case Enum.LogRepositoryType.EventLog:
                    // implementar
                    break;
            }

            active = true;
        }

        public override void Log(string message, LogType logType)
        {
            if (active)
                Log(message, logType, "LOG");
        }

        public override void Log(string message, LogType logType, string context)
        {
            if (active)
            {
                ILog log = LogManager.GetLogger(context);
                switch (logType)
                {
                    case LogType.Debug:
                        log.Debug(message);
                        break;

                    case LogType.Error:
                        log.Error(message);
                        break;

                    case LogType.Info:
                        log.Info(message);
                        break;

                    case LogType.Warning:
                        log.Warn(message);
                        break;
                }
            }
        }

        /// <summary>
        /// Initilize Database log provider for log4net
        /// </summary>
        /// <param name="parameter"></param>
        private void SetDatabase(Log4NetProviderParameter parameter)
        {
            AdoNetAppender adoNetAppender = new AdoNetAppender()
            {
                CommandText = "INSERT INTO LOG (log_date, log_level, log_logger, log_message,  log_user, log_ip, log_exception) VALUES (@log_date, @log_level, @log_logger, @log_message,  @log_user, @log_ip, @log_exception)",
                ConnectionString = parameter.ConnectionString.ToString(),
                ConnectionType = "System.Data.SqlClient.SqlConnection, System.Data, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089",
                CommandType = CommandType.Text,
                BufferSize = 1
            };

            AddDateTimeParameterToAppender(adoNetAppender, "log_date");
            AddStringParameterToAppender(adoNetAppender, "log_thread", 255, "%thread");
            AddStringParameterToAppender(adoNetAppender, "log_level", 50, "%level");
            AddStringParameterToAppender(adoNetAppender, "log_logger", 255, "%logger");
            AddStringParameterToAppender(adoNetAppender, "log_message", 4000, "%message");
            AddStringParameterToAppender(adoNetAppender, "log_user", 50, "%aspnet-request{AUTH_USER}");
            AddStringParameterToAppender(adoNetAppender, "log_ip", 15, "%aspnet-request{REMOTE_ADDR}");
            AddErrorParameterToAppender(adoNetAppender, "log_exception", 2000);

            adoNetAppender.ActivateOptions();

            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

            switch (parameter.Level)
            {
                case Enum.LogType.Info:
                    hierarchy.Root.Level = Level.Info;
                    break;

                case Enum.LogType.Debug:
                    hierarchy.Root.Level = Level.Debug;
                    break;

                case Enum.LogType.Error:
                    hierarchy.Root.Level = Level.Error;
                    break;

                case Enum.LogType.Warning:
                    hierarchy.Root.Level = Level.Warn;
                    break;

                case Enum.LogType.All:
                    hierarchy.Root.Level = Level.All;
                    break;
            }

            hierarchy.Root.AddAppender(adoNetAppender);

            hierarchy.Configured = true;

            BasicConfigurator.Configure(adoNetAppender);
        }

        /// <summary>
        /// Initialize Log appender for log4net
        /// </summary>
        /// <param name="parameter"></param>
        private void SetFileAppender(Log4NetProviderParameter parameter)
        {
            var layout = new PatternLayout("%datetime [%thread] %-5level %logger %ndc - %message%newline");
            FileAppender fileAppender = new RollingFileAppender
            {
                File = parameter.LogFileName.ToString(),
                Layout = layout,
                AppendToFile = true,
                RollingStyle = RollingFileAppender.RollingMode.Size,
                MaximumFileSize = "5000KB",
                MaxSizeRollBackups = 20
            };

            //var layout = new PatternLayout("%date{ABSOLUTE} [%thread] [%logger] %level - %message%newline%exception");
            //FileAppender fileAppender = new RollingFileAppender
            //{
            //    File = parameter.LogFileName.ToString(),
            //    Layout = layout,
            //    AppendToFile = true,
            //    RollingStyle = RollingFileAppender.RollingMode.Date,
            //    MaxSizeRollBackups = 10,
            //    StaticLogFileName = false,
            //    DatePattern = "yyyy'-'MM'-'dd'.log'",
            //    LockingModel = new FileAppender.MinimalLock()
            //};


            //layout.ActivateOptions();
            fileAppender.ActivateOptions();

            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

            #region NHibernateLog

            Logger loggerNHSQL = hierarchy.GetLogger("NHibernate.SQL") as Logger;
            loggerNHSQL.AddAppender(fileAppender);

            Logger loggerNH = hierarchy.GetLogger("NHibernate") as Logger;
            loggerNH.AddAppender(fileAppender);

            switch (parameter.NHibernateLevel)
            {
                case LogType.Info:
                    loggerNHSQL.Level = Level.Info;
                    loggerNH.Level = Level.Info;
                    break;
                case LogType.Debug:
                    loggerNHSQL.Level = Level.Debug;
                    loggerNH.Level = Level.Debug;
                    break;
                case LogType.Error:
                    loggerNHSQL.Level = Level.Error;
                    loggerNH.Level = Level.Error;
                    break;
                case LogType.Warning:
                    loggerNHSQL.Level = Level.Warn;
                    loggerNH.Level = Level.Warn;
                    break;
                case LogType.All:
                    loggerNHSQL.Level = Level.All;
                    loggerNH.Level = Level.All;
                    break;
                case LogType.Fatal:
                    loggerNHSQL.Level = Level.Fatal;
                    loggerNH.Level = Level.Fatal;
                    break;
                case LogType.None:
                case LogType.Off:
                default:
                    loggerNHSQL.Level = Level.Off;
                    loggerNH.Level = Level.Off;
                    break;
            }

            #endregion

            switch (parameter.Level)
            {
                case Enum.LogType.Info:
                    hierarchy.Root.Level = Level.Info;
                    break;

                case Enum.LogType.Debug:
                    hierarchy.Root.Level = Level.Debug;
                    break;

                case Enum.LogType.Error:
                    hierarchy.Root.Level = Level.Error;
                    break;

                case Enum.LogType.Warning:
                    hierarchy.Root.Level = Level.Warn;
                    break;

                case Enum.LogType.All:
                    hierarchy.Root.Level = Level.All;
                    break;

                case Enum.LogType.None:
                    hierarchy.Root.Level =  Level.Error;
                    break;
            }

            

            //hierarchy.Root.Level = Level.Info;


            hierarchy.Root.AddAppender(fileAppender);

            hierarchy.Configured = true;
            BasicConfigurator.Configure(fileAppender);
             
        }

        #region helpers

        private static void AddStringParameterToAppender(AdoNetAppender appender, string paramName, int size, string conversionPattern)
        {
            var param = new AdoNetAppenderParameter
            {
                ParameterName = paramName,
                DbType = global::System.Data.DbType.String,
                Size = size,
                Layout = new Layout2RawLayoutAdapter(new PatternLayout(conversionPattern))
            };
            appender.AddParameter(param);
        }

        private static void AddDateTimeParameterToAppender(AdoNetAppender appender, string paramName)
        {
            var param = new AdoNetAppenderParameter
            {
                ParameterName = paramName,
                DbType = global::System.Data.DbType.DateTime,
                Layout = new RawUtcTimeStampLayout()
            };
            appender.AddParameter(param);
        }

        private static void AddErrorParameterToAppender(AdoNetAppender appender, string paramName, int size)
        {
            var param = new AdoNetAppenderParameter
            {
                ParameterName = paramName,
                DbType = global::System.Data.DbType.String,
                Size = size,
                Layout = new Layout2RawLayoutAdapter(new ExceptionLayout())
            };
            appender.AddParameter(param);
        }

        #endregion helpers
    }
}