﻿using BCR.System.Log.Abstract;
using System;
using System.Threading.Tasks;

namespace BCR.System.Log
{
    public static class Log4Bio
    {
        private static AbstractLogger log;

        public static void Initialize(AbstractLogger logCreated)
        {
            log = logCreated;
        }

        public static void Fatal(string message)
        {
            Task task = Task.Factory.StartNew(() => log.Fatal(message));
        }

        public static void Error(string message)
        {
            //message = "#############   " + Environment.NewLine + message + Environment.NewLine + "  #############";
            Task task = Task.Factory.StartNew(() => log.Error(message));
        }

        public static void Error(string message, Exception e)
        {
            //message = "#############   " + Environment.NewLine + message + Environment.NewLine + "  #############";
            Task task = Task.Factory.StartNew(() => log.Error(message, e));

        }

        public static void Debug(string message)
        {
            Task task = Task.Factory.StartNew(() => log.Debug(message));
        }

        public static void Warn(string message)
        {
            Task task = Task.Factory.StartNew(() => log.Warn(message));
        }

        public static void Info(string message)
        {

            //if (message.ToLower().StartsWith("select"))
            //    message = message.Replace(",", "," + Environment.NewLine)
            //        .Replace("FROM", Environment.NewLine + "FROM")
            //        .Replace("WHERE", Environment.NewLine + "WHERE")

            //        ;

            //message = "************************ " + Environment.NewLine + message + Environment.NewLine + "************************ ";

            Task task = Task.Factory.StartNew(() => log.Info(message));
        }
    }
}