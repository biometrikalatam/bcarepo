using BCR.System.Log.Abstract;

namespace BCR.System.Log
{
    /// <summary>
    /// Factoria de Proveedores de Log
    /// </summary>
    public static class LogProviderFactoy
    {
        /// <summary>
        /// Retorna el log seg�n la config indicada
        /// </summary>
        /// <param name="logConfiguration"></param>
        /// <returns></returns>
        public static AbstractLogger GetLogger(LogConfiguration logConfiguration)
        {
            if (logConfiguration.LogProvider == Enum.LogProvider.Log4Net)
            {
                Log4NetProviderParameter log4NetProviderParameter = new Log4NetProviderParameter()
                {
                    LogRepositoryType = logConfiguration.LogRepositoryType,
                    LogFileName = logConfiguration.LogPath,
                    ConnectionString = logConfiguration.ConnectionString,
                    Level = logConfiguration.Level,
                    NHibernateLevel = logConfiguration.NHibernateLevel
                };

                return new Log4NetLogger(log4NetProviderParameter);
            }

            return null;
        }
    }
}