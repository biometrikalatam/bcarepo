using System;
using System.IO;
using System.Reflection;

using System.Runtime.InteropServices;

namespace BCR.System.Win32
{
    /// <summary>
    /// Descripción breve de Sounds.
    /// </summary>
    public class Sounds
    {
        private const int SND_SYNC = 0;
        private const int SND_ASYNC = 1;
        private const int SND_MEMORY = 4;
        private const int SND_FILENAME = 131072;

        //Provide access to PlaySound() in the Multi-media system API through	MMSYSTEM.dll.
        [DllImport("winmm")]
        public static extern bool PlaySound(string pszSound, IntPtr hFile, Int32 fdwSound);

        [DllImport("winmm.dll")]
        public static extern bool sndPlaySound(byte[] pbyte, Int32 param);

        /// <summary>
        /// Toca un archivo wav
        /// </summary>
        /// <param name="soundFile">nombre del archivo</param>
        public static void PlayFile(string soundFile)
        {
            // Stop current sound
            PlaySound(null, IntPtr.Zero, SND_ASYNC);
            // Play sound
            PlaySound(soundFile, IntPtr.Zero, SND_ASYNC | SND_FILENAME);
        }

        /// <summary>
        /// Toca un archivo wav que esta como recurso incrustado
        /// </summary>
        /// <param name="resourceName">nombre del recurso</param>
        public static void PlayResource(Assembly assembly, string resourceName)
        {
            Stream myStream = assembly.GetManifestResourceStream(resourceName);
            byte[] byteWavFile = new Byte[myStream.Length];
            // if it fails above, there is a problem with resource
            // make sure you have it embedded and the
            // name is correct ("project.Folder.Filename.Ext")
            // case sensative (i think)
            MemoryStream ms = new MemoryStream();
            int i = myStream.Read(byteWavFile, 0, byteWavFile.Length);
            ms.Write(byteWavFile, 0, i);

            myStream.Close();

            sndPlaySound(ms.ToArray(), SND_MEMORY);

            ms.Close();
        }
    }
}