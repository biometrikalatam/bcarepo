﻿using BCR.System.Communication.Domain;
using BCR.System.Log;
using System;
using System.Net;
using System.Net.Mail;

//using log4net;

namespace BCR.System.Communication
{

    public static class MailServer
    {
        public static void SendSoporteMail(string to, string subject, string body)
        {
            MailMessage mailmessage = new MailMessage()
            {
                From = new MailAddress("soporte@biometrika.cl"),
                Subject = subject,
                Body = body
            };

            mailmessage.To.Add(to);

            Server.Send(mailmessage);
        }

        public static void SendMail( string from, string to, string subject ,string body)
        {
            MailMessage mailmessage = new MailMessage()
            {
                From = new MailAddress(from),
                Subject = subject,
                Body = body
            };

            mailmessage.To.Add(to);
            mailmessage.IsBodyHtml = true;

            Server.Send(mailmessage);
        }

        static SmtpClient Server
        {
            get
            {
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new  NetworkCredential("soporte@biometrika.cl", "B1ometrika2014");
                SmtpServer.EnableSsl = true;
                return SmtpServer;
            }
        }

    }


    public class SmtpSenderSimple
    {
        //private static readonly ILog LOG = LogManager.GetLogger(typeof(SmtpSenderSimple));

        static public int SendMail(string server, int port, string to, string cc, string bcc, string from,
                                    string subject, string body, bool sslenable, string user, string psw,
                                    bool isHTML)
        {
            int ret = 0;
            string[] aux;

            try
            {
                if (String.IsNullOrEmpty(server) || String.IsNullOrEmpty(to) ||
                    String.IsNullOrEmpty(from))
                    return -2;

                MailMessage mail = new MailMessage();
                aux = to.Trim().Split(new string[] { ";" }, StringSplitOptions.None);
                foreach (string sTo in aux)
                {
                    mail.To.Add(new MailAddress(sTo));
                }

                if (!String.IsNullOrEmpty(cc))
                {
                    aux = cc.Trim().Split(new string[] { ";" }, StringSplitOptions.None);
                    foreach (string sCC in aux)
                    {
                        mail.CC.Add(new MailAddress(sCC));
                    }
                }
                if (!String.IsNullOrEmpty(bcc))
                {
                    aux = bcc.Trim().Split(new string[] { ";" }, StringSplitOptions.None);
                    foreach (string sBCC in aux)
                    {
                        mail.Bcc.Add(new MailAddress(sBCC));
                    }
                }
                mail.Sender = new MailAddress(from);
                mail.From = new MailAddress(from);
                mail.Subject = subject;
                mail.IsBodyHtml = isHTML;
                mail.Body = body;

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "mail.biometrika.cl";
                smtp.Port = 25;

                smtp.Credentials = new NetworkCredential(user, psw);
                smtp.EnableSsl = sslenable;
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                //LOG.Error("Error SmtpSenderSimple", ex);
                ret = -1;
            }
            return ret;
        }

        public static int SendMail(EmailConfigData mailConfig)
        {
            try
            {
                Log4Bio.Debug("Configuramos todos los parametros para el envio de correos");

                if (string.IsNullOrEmpty(mailConfig.Server) || mailConfig.To == null || mailConfig.To.Length == 0 ||
                    string.IsNullOrEmpty(mailConfig.From))
                    return -2;

                Log4Bio.Debug("Smtp config");
                Log4Bio.Debug("User: " + mailConfig.User);
                Log4Bio.Debug("Server: " + mailConfig.Server);
                Log4Bio.Debug("Puerto: " + mailConfig.Puerto);
                Log4Bio.Debug("EnableSsl: " + mailConfig.EnableSsl.ToString());
                SmtpClient smtp = new SmtpClient
                {
                    Credentials = new NetworkCredential(mailConfig.User, mailConfig.Password),
                    Host = mailConfig.Server,
                    Port = mailConfig.Puerto,
                    EnableSsl = mailConfig.EnableSsl,

                };

                Log4Bio.Debug("Email config");
                Log4Bio.Debug("From: " + mailConfig.From);
                Log4Bio.Debug("Subject: " + mailConfig.Subject);
                MailMessage correo = new MailMessage
                {
                    From = new MailAddress(mailConfig.From),
                    Subject = mailConfig.Subject,
                    IsBodyHtml = mailConfig.IsBodyHtml,
                    Priority = mailConfig.Priority,
                    Body = mailConfig.Body
                };

                foreach (var item in mailConfig.To)
                {
                    correo.To.Add(item);
                    Log4Bio.Debug("To: " + item);
                }

                Log4Bio.Debug("Enviando");
                smtp.Send(correo);
                Log4Bio.Debug("Correo enviado");
                return 0;
            }
            catch (Exception ex)
            {
                Log4Bio.Debug(ex.Message);
                return -1;
            }
        }
    }
}