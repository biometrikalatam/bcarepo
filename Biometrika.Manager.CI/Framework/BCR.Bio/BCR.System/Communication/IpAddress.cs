using System;
using System.Net;

namespace BCR.System.Communication
{
    /// <summary>
    /// Descripci�n breve de IpAddress.
    /// </summary>
    public class IpAddress
    {
        public IpAddress()
        {
            //
            // TODO: agregar aqu� la l�gica del constructor
            //
        }

        public static bool IsIPValid(string ip)
        {
            try
            {
                IPAddress address = IPAddress.Parse(ip);
                Console.WriteLine(address.ToString());
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }

        public static string getIPAddress()
        {
            string server;
            IPHostEntry heserver;

            server = Dns.GetHostName();

            // Get server related information.
            heserver = Dns.Resolve(server);
            return heserver.AddressList[0].ToString().Trim();
        }
    }
}