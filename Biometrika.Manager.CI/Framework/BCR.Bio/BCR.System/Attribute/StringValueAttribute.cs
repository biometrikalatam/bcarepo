﻿namespace BCR.System.Attribute
{
    public sealed class StringValueAttribute : global::System.Attribute
    {
        private string str;

        public StringValueAttribute(string value)
        {
            str = value;
        }

        public string Value
        {
            get { return str; }
        }
    }
}