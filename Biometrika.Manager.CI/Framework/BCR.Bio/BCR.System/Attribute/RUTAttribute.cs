﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace BCR.System.Attribute
{
    public class RUTAttribute : RegularExpressionAttribute
    {
        public RUTAttribute()
            : base(GetRegex())
        { }

        private static string GetRegex()
        {

            return @"^0*(\d{1,3}(\.?\d{3})*)\-?([\dkK])$";
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(ErrorMessage ?? "Inrgese una identificación custom");
            }
        }
    }
}
