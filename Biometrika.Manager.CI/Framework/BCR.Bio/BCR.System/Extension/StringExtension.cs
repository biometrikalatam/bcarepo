﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;

namespace BCR.System.Extension
{
    public static class StringExtension
    {
        public static string ToSQLLike(this string expression)
        {
            return string.Concat("%", expression, "%");
        }

        public static byte[] Base64DecodeString(this string inputStr)
        {
            byte[] decodedByteArray = Convert.FromBase64String(inputStr);
            return (decodedByteArray);
        }

        public static string RemoveDiacritics(this string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static byte[] ToByteArray(this string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public static bool RutValidation(this string rut)
        {
            bool isValid = false;

            if (string.IsNullOrEmpty(rut))
                return isValid;

            try
            {
                rut = rut.ToUpper();
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                int rutAux = int.Parse(rut.Substring(0, rut.Length - 1));

                char dv = char.Parse(rut.Substring(rut.Length - 1, 1));

                int m = 0, s = 1;
                for (; rutAux != 0; rutAux /= 10)
                {
                    s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                }
                if (dv == (char)(s != 0 ? s + 47 : 75))
                {
                    isValid = true;
                }
            }
            catch (Exception)
            {
            }

            return isValid;
        }

        /// <summary>
        /// Permite, en base a un tipo, devolver el valor enum correspondiente
        /// </summary>
        /// <typeparam name="T">Tipo de Enum</typeparam>
        /// <param name="value">Valor para convertir</param>
        /// <param name="defaultValue">Valor por defecto en el caso que no se logre igualar value con las opciones del enum sumistrado en el tipo</param>
        /// <returns>value convertido al valor que le corresponde del tipo de enum</returns>
        public static T ToEnum<T>(this string value, T defaultValue)
            where T : struct
        {
            if (string.IsNullOrEmpty(value))
                return defaultValue;

            T result;
            return global::System.Enum.TryParse<T>(value, true, out result) ? result : defaultValue;
        }

        /// <summary>
        /// Método que permite obtner un string con la primera letra en mayusculas
        /// </summary>
        /// <param name="value"></param>
        /// <param name="lang">Valor de lenguaje a utilizar
        /// - Español: es-ES
        /// - Ingles (USA): us-US
        /// </param>
        /// <returns></returns>
        public static string ToTitleCase(this string value, string lang)
        {
            TextInfo textInfo = new CultureInfo(lang, false).TextInfo;
            return textInfo.ToTitleCase(value);
        }

        public static DateTime ToDateTime(this string value, string format)
        {
            DateTime retorno = DateTime.MinValue;
            if (DateTime.TryParseExact(value, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out retorno))
                return retorno;
            return new DateTime().SqlMinValue();
        }

        /// <summary>
        /// Reemplaza texto structurado entre {} por el nuevo valor entregado
        /// </summary>
        /// <param name="value">Texto donde se realizara el reemplazo</param>
        /// <param name="parameter">parametro a reemplazar</param>
        /// <param name="newValue">Texto usado para reemplazar el valor de parameter</param>
        /// <returns>Texto con parametro reemplazado</returns>
        public static string SetParameter(this string value, string parameter, string newValue)
        {
            return value.Replace(parameter, newValue);
        }
    }
}