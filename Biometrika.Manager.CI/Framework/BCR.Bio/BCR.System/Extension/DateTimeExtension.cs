﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Text;

namespace BCR.System.Extension
{
    public static class DateTimeExtension
    {
        /// <summary>
        /// Metodo para transformar un DateTime al formato ISO8601
        /// "yyyy-MM-ddTHH:mm:ss.fff" o "yyyy-MM-ddTHH:mm:ss.fff"
        /// </summary>
        /// <param name="value">DateTime a convertir</param>
        /// <returns>string con la fecha en el formato ISO8601</returns>
        public static string FormatISO8601(this DateTime value)
        {
            // Evaluar si esto sirve de algo, en teoria no deberia nunca ser null
            if (value == null)
                return string.Empty;

            DateTimeOffset dto = value;
            //string format = dto.Offset == TimeSpan.Zero
            //    ? "yyyy-MM-ddTHH:mm:ss.fffZ"
            //    : "yyyy-MM-ddTHH:mm:ss.fffzzz";

            string format = "yyyy-MM-ddTHH:mm:ss.fff";

            return dto.ToString(format, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Por evaluar
        /// DateTime sqlMinDate = new DateTime().SqlMinValue();
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime SqlMinValue(this DateTime value)
        {
            return (DateTime)SqlDateTime.MinValue;
        }
    }
}
