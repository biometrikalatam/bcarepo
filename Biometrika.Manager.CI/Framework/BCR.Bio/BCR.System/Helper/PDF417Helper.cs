﻿using BCR.System.Log;
using Pdf417DecoderLibrary;
using System;
using System.Drawing;
using System.IO;

namespace BCR.System.Helper
{
    public static class PDF417Helper
    {
        public static string ProcesaPDF417Recognition(string imageBackCedula)
        {
            string strReturn = null;
            try
            {
                Log4Bio.Debug("PDF417Helper.ProcesaPDF417Recognition - IN...");

                if (string.IsNullOrEmpty(imageBackCedula))
                {
                    Log4Bio.Debug("PDF417Helper.ProcesaPDF417Recognition - Sale porque param entrada es nulo!");
                }
                else
                {
                    Log4Bio.Debug("PDF417Helper.ProcesaPDF417Recognition - Convierte string entrada en Image...");
                }

                MemoryStream ms = new MemoryStream(Convert.FromBase64String(imageBackCedula));
                Image img = Image.FromStream(ms);
                ms.Close();

                Log4Bio.Debug("PDF417Helper.ProcesaPDF417Recognition - Create Bitmap...");
                // load image to bitmap
                Bitmap Pdf417InputImage = new Bitmap((Image)img.Clone());

                // decode barcodes
                Pdf417Decoder oPdf417Decoder = new Pdf417Decoder();

                // open trace file
                Log4Bio.Debug("PDF417Helper.ProcesaPDF417Recognition - Pdf417Decoder.Decode in...");
                int BarcodesCount = oPdf417Decoder.Decode(Pdf417InputImage);

                // no barcodes were found
                if (BarcodesCount == 0)
                {
                    Log4Bio.Debug("PDF417Helper.ProcesaPDF417Recognition - No reconocio barcode PDF417!");
                }

                // one barcodes was found
                else if (BarcodesCount > 0)
                {
                    // decoding was successful
                    // convert binary data to text string
                    strReturn = Convert.ToBase64String(oPdf417Decoder.BarcodesInfo[0].BarcodeData);
                    Log4Bio.Debug("PDF417Helper.ProcesaPDF417Recognition - PDF417 Reconocido => Length=" +
                                (string.IsNullOrEmpty(strReturn) ? "Null" : strReturn.Length.ToString()));
                }
            }
            catch (Exception ex)
            {
                strReturn = null;
                Log4Bio.Error("PDF417Helper.ProcesaPDF417Recognition - Error: " + ex.Message);
            }
            Log4Bio.Debug("PDF417Helper.ProcesaPDF417Recognition - OUT!");
            return strReturn;
        }
    }
}
