﻿using System;
using System.Net;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using BCR.System.Log;

namespace BCR.System.Helper
{
    public class Email
    {
        public static bool SendMail(string[] mailto, string subject, string body,
                                    byte[] attach, string mimetypeattach,
                                    string nombreattach, EmailSettings emailSettings)
        {
            bool ret = false;
            MemoryStream ms = null;
            try
            {
                Log4Bio.Debug("Utils.SendMail IN...");
                //save the data to a memory stream
                //Envio de Correo
                
                Log4Bio.Debug("Utils.SendMail UserSMTP = " + emailSettings.SMTPUser + " / " + emailSettings.SMTPPassword);
                MailAddress SendFrom = new MailAddress(emailSettings.SMTPUser, emailSettings.SMTPDisplayName);
                Log4Bio.Debug("Utils.SendMail SendTo = " + mailto[0]);
                MailAddress SendTo = new MailAddress(mailto[0]);
                MailMessage MyMessage = null;
                try
                {
                    SmtpClient emailClient = new SmtpClient(emailSettings.SMTPServer);
                    emailClient.Port = emailSettings.SMTPPort;
                    emailClient.EnableSsl = true;
                    NetworkCredential _Credential =
                                new NetworkCredential(emailSettings.SMTPUser, emailSettings.SMTPPassword, null);
                    MyMessage = new MailMessage(SendFrom, SendTo);
                    if (mailto.Length > 1)
                    {
                        for (int i = 1; i < mailto.Length; i++)
                        {
                            MyMessage.To.Add(new MailAddress(mailto[i]));
                            Log4Bio.Debug("Utils.SendMail - Correo => Enviar a:" + mailto[i] + " desde : " + SendFrom);
                        }
                    }

                    if (attach != null && attach.Length > 0)
                    {
                        ms = new MemoryStream(attach);
                        ms.Position = 0;
                        string mt = MediaTypeNames.Application.Pdf;
                        if ((mimetypeattach == "docx") || (mimetypeattach == "doc"))
                            mt = MediaTypeNames.Application.Rtf;
                        else if ((mimetypeattach == "xls") || (mimetypeattach == "xlsx"))
                            mt = MediaTypeNames.Application.Octet;
                        else if ((mimetypeattach == "pdf"))
                            mt = MediaTypeNames.Application.Pdf;
                        else
                            mt = MediaTypeNames.Application.Octet;
                        MyMessage.Attachments.Add(new Attachment(ms, nombreattach, mt));
                    }

                    Log4Bio.Debug("Utils.SendMail Set Body HTML...");
                    MyMessage.IsBodyHtml = true;
                    Log4Bio.Debug("Utils.SendMail Subject = " + subject);
                    MyMessage.Subject = subject;
                    Log4Bio.Debug("Utils.SendMail - Body = " + body);
                    MyMessage.Body = body;
                    emailClient.Credentials = _Credential;
                    try
                    {
                        Log4Bio.Debug("Utils.SendMail Enviando...");
                        emailClient.Send(MyMessage);
                        Log4Bio.Debug("Utils.SendMail Enviado sin error!");
                        ret = true;
                    }
                    catch (Exception ex)
                    {
                        Log4Bio.Error("emailClient.Send(MyMessage)- Error", ex);
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    Log4Bio.Error("emailClient.Send [" + ex.Message + "]");
                    ret = false;
                }

                if (ms != null)
                {
                    ms.Close();
                    ms.Dispose();
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("Utils.SendMail Error SendMail", ex);
                ret = false;
            }
            Log4Bio.Debug("Utils.SendMail OUT!");
            return ret;

        }

        //public static bool SendMailHtml(string[] mailto, string subject, string body,
        //                            byte[] attach, string mimetypeattach,
        //                            string nombreattach)
        //{
        //    bool ret = false;
        //    MemoryStream ms = null;
        //    try
        //    {
        //        Log4Bio.Debug("Utils.SendMail IN...");
        //        //save the data to a memory stream


        //        //Envio de Correo
        //        Log4Bio.Debug("Utils.SendMail UserSMTP = " + Settings.Default.UserSMTP + " / " + Settings.Default.ClaveSMTP);
        //        MailAddress SendFrom = new MailAddress(Settings.Default.UserSMTP, Settings.Default.SMTPDisplayName);
        //        Log4Bio.Debug("Utils.SendMail SendTo = " + mailto[0]);
        //        MailAddress SendTo = new MailAddress(mailto[0]);
        //        MailMessage MyMessage = null;
        //        try
        //        {
        //            SmtpClient emailClient = new SmtpClient(Settings.Default.ServerSMTP);
        //            emailClient.Port = 587;
        //            emailClient.EnableSsl = true;
        //            NetworkCredential _Credential =
        //                        new NetworkCredential(Settings.Default.UserSMTP, Settings.Default.ClaveSMTP, null);
        //            MyMessage = new MailMessage(SendFrom, SendTo);
        //            if (mailto.Length > 1)
        //            {
        //                for (int i = 1; i < mailto.Length; i++)
        //                {
        //                    MyMessage.To.Add(new MailAddress(mailto[i]));
        //                    Log4Bio.Debug("Utils.SendMail - Correo => Enviar a:" + mailto[i] + " desde : " + SendFrom);
        //                }
        //                //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.cl"));
        //                //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.pe"));
        //                //MyMessage.To.Add(new MailAddress("gsuhit@yahoo.com"));
        //            }

        //            if (attach != null && attach.Length > 0)
        //            {
        //                ms = new MemoryStream(attach);
        //                ms.Position = 0;
        //                string mt = MediaTypeNames.Application.Pdf;
        //                if ((mimetypeattach == "docx") || (mimetypeattach == "doc"))
        //                    mt = MediaTypeNames.Application.Rtf;
        //                else if ((mimetypeattach == "xls") || (mimetypeattach == "xlsx"))
        //                    mt = MediaTypeNames.Application.Octet;
        //                else if ((mimetypeattach == "pdf"))
        //                    mt = MediaTypeNames.Application.Pdf;
        //                else
        //                    mt = MediaTypeNames.Application.Octet;
        //                MyMessage.Attachments.Add(new Attachment(ms, nombreattach, mt));
        //            }

        //            Log4Bio.Debug("Utils.SendMail Set Body HTML...");
        //            //MyMessage.IsBodyHtml = true;

        //            //Create two views, one text, one HTML.
        //            //string s = ".";
        //            //System.Net.Mail.AlternateView plainTextView =
        //            //    System.Net.Mail.AlternateView.CreateAlternateViewFromString(s, Encoding.UTF8, "text/html");
        //            System.Net.Mail.AlternateView htmlView =
        //                System.Net.Mail.AlternateView.CreateAlternateViewFromString(body, Encoding.UTF8, "text/html");

        //            //Add image to HTML version

        //            System.Net.Mail.LinkedResource imageResourceH =
        //                new System.Net.Mail.LinkedResource(Properties.Settings.Default.LogoMailH, MediaTypeNames.Image.Jpeg);
        //            imageResourceH.ContentId = "HDIImageH";
        //            htmlView.LinkedResources.Add(imageResourceH);

        //            System.Net.Mail.LinkedResource imageResourceF =
        //                new System.Net.Mail.LinkedResource(Properties.Settings.Default.LogoMailF, MediaTypeNames.Image.Jpeg);
        //            imageResourceF.ContentId = "HDIImageF";
        //            htmlView.LinkedResources.Add(imageResourceF);

        //            //Add two views to message.

        //            //MyMessage.AlternateViews.Add(plainTextView);
        //            MyMessage.AlternateViews.Add(htmlView);

        //            Log4Bio.Debug("Utils.SendMail Subject = " + subject);
        //            MyMessage.Subject = subject;
        //            Log4Bio.Debug("Utils.SendMail - Body = " + body);
        //            //MyMessage.Body = body;
        //            emailClient.Credentials = _Credential;
        //            try
        //            {
        //                Log4Bio.Debug("Utils.SendMail Enviando...");
        //                emailClient.Send(MyMessage);
        //                Log4Bio.Debug("Utils.SendMail Enviado sin error!");
        //                ret = true;
        //            }
        //            catch (Exception ex)
        //            {
        //                Log4Bio.Error("emailClient.Send(MyMessage)- Error", ex);
        //                ret = false;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Log4Bio.Error("emailClient.Send [" + ex.Message + "]");
        //            ret = false;
        //        }

        //        if (ms != null)
        //        {
        //            ms.Close();
        //            ms.Dispose();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log4Bio.Error("Utils.SendMail Error SendMail", ex);
        //        ret = false;
        //    }
        //    Log4Bio.Debug("Utils.SendMail OUT!");
        //    return ret;

        //}


    }
}
