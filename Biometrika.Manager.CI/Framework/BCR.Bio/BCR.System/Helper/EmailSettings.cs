﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.System.Helper
{
    public class EmailSettings
    {
        public string SMTPServer { get; set; }
        public string SMTPUser { get; set; }
        public string SMTPPassword { get; set; }
        public string SMTPDisplayName { get; set; }

        public int SMTPPort { get; set; }

        public EmailSettings()
        {
            SMTPServer = "smtp.google.com";
            SMTPUser = "";
            SMTPPassword = "";
            SMTPDisplayName = "";
            SMTPPort = 587;
        }
    }
}
