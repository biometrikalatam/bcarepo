﻿using BCR.System.Attribute;
using System.Collections;
using System.Linq;
using System.Reflection;

namespace BCR.System.Enum
{
    /// <summary>
    ///
    /// </summary>
    public static class EnumHelper
    {
        private static Hashtable EnumCached = new Hashtable();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(global::System.Enum value)
        {
            return GetStringValue(value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetStringValue(global::System.Enum value)
        {
            string valueDescription = null;
            global::System.Type type = value.GetType();

            string key = string.Format("{0}.{1}", type, value);
            if (EnumCached.ContainsKey(key))
                valueDescription = (EnumCached[key] as StringValueAttribute).Value;
            else
            {
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] attrs = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (attrs.Length > 0)
                {
                    key = string.Format("{0}.{1}", type, value);
                    EnumCached.Add(key, attrs[0]);
                    valueDescription = attrs[0].Value;
                }
            }

            return valueDescription;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetStringValue<T>(int value)
        {
            string valueDescription = null;
            global::System.Type type = typeof(T);  //value.GetType();

            string key = string.Format("{0}.{1}", type, value);
            if (EnumCached.ContainsKey(key))
                valueDescription = (EnumCached[key] as StringValueAttribute).Value;
            else
            {
                string valueEnum = global::System.Enum.GetName(type, value);

                FieldInfo fi = type.GetField(valueEnum);
                StringValueAttribute[] attrs = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (attrs.Length > 0)
                {
                    key = string.Format("{0}.{1}", type, value);
                    EnumCached.Add(key, attrs[0]);
                    valueDescription = attrs[0].Value;
                }
            }

            return valueDescription;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Hashtable List<T>()
        {
            Hashtable list = new Hashtable();
            foreach (T key in global::System.Enum.GetValues(typeof(T)).Cast<T>())
            {
                list.Add(key, "");
            }
            return list;
        }
    }
}