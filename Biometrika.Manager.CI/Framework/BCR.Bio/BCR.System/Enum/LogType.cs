﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.System.Enum
{
    public enum LogType
    {
        Info,
        Debug,
        Error,
        Warning,
        All,
        Fatal,
        None,
        Off
    }
}
