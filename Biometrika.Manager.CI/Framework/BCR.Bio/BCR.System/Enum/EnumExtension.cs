﻿namespace BCR.System.Enum
{
    public static partial class EnumExtension
    {
        public static string GetDescription(this AuditAction auditAction)
        {
            return EnumHelper.GetDescription(auditAction);
        }
    }
}