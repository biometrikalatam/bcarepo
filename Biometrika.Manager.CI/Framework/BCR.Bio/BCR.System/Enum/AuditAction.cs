﻿using BCR.System.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.System.Enum
{
    public enum AuditAction
    {
        [StringValue("Create")]
        Create,
        [StringValue("Update")]
        Update,
        [StringValue("Delete")]
        Delete,
        [StringValue("Read")]
        Read
    }
}
