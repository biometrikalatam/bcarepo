## Pasos para generar un nuevo proyecto en el framework
1. Descargar el framework a su propia ubicación
2. Compilar los proyectos propios del framework en el siguiente orden:
    * Services
	* Domain
	* Mappings (en cualquier orden)
	* Infraestructura
	* System
	* Al final todo el resto