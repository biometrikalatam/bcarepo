﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Enum;
using BCR.Bio.Services;
using BCR.System.Configuration;
using BCR.System.Crypto;
using BCR.System.Enum;
using BCR.System.Log;
using BCR.System.Type;
using BCR.System.Validation;
using NUnit.Framework;
using System;
using System.IO;

namespace Test
{
    internal class Program
    {
        private static BioUnitOfWork bioUoW;

        private static void Main(string[] args)
        {

            byte[] passwordDecoded = AES.Encrypt("basilio");
            var hexString = BitConverter.ToString(passwordDecoded);
            hexString = hexString.Replace("-", "");

            int key = 2;
            string datata = EnumHelper.GetStringValue<BodyPartEnum>(key);

            if (true)
                GenerateEncriptedDataSource();

            //LogConfiguration logConfiguration = new LogConfiguration(LogProvider.Log4Net, new FileInfo(@"C:\Log4net\test2.txt"));
            //logConfiguration.Level = LogType.All;

            //ValidationDictionary validationDictionary = new ValidationDictionary();
            //BioUnitOfWork service = new BioUnitOfWork(new SystemConfiguration(logConfiguration, false));

            //bioUoW = service.GetBioUnitOfWork();

            LogConfiguration logConfiguration = new LogConfiguration(LogProvider.Log4Net, new FileInfo(@"C:\Log4net\test2.txt"));

            //LogConfiguration logConfiguration = new LogConfiguration(LogProvider.Log4Net, new FileInfo(@"C:\Log4net\spsa.txt"));

            SystemConfiguration systemConfiguration = new SystemConfiguration(logConfiguration, false);

            bioUoW = new BioUnitOfWork(systemConfiguration.HibernateParams, new ValidationDictionary());

            if (true)
                testTimeRestriction();

            if (false)
                testAutorization();

            if (false)
                testcentroDeCostos();

            if (true)
                TestUser();

            if (false)
                TestSector();

            if (false)
                TestPoint();

            //ICompany cc = bioUoW.CompanyService.GetById(12);

            if (false)
                TestCityDeptCountry();

            if (false)
                TestRol();

            ////var rc = 1;

            //IUser p = bioUoW.UserService.GetByUsername("Basilio");

            ////IHolding holding = new Holding()
            ////{
            ////     Name= "Biometrika",
            ////     CreateDate = DateTime.Now
            ////};

            ////bioUoW.HoldingService.Create(holding);

            //IHolding holding = bioUoW.HoldingService.GetById(1);

            //ICompany company1 = new Company()
            //{
            //    Name = "Interna1",
            //    CreateDate = DateTime.Now,
            //    ContactName = "Romualdo",
            //    Domain = "Int1",
            //    Holding = holding.Id,
            //    Rut = "34343453455",
            //    Phone = "15 23233234",
            //    Address = "serv"
            //};

            //var v = bioUoW.CompanyService.Create(company1);

            //var sargs = 1;

            //return;

            if (false)
                TestOriginBPIdentityBPBIr();
        }

        private static void testTimeRestriction()
        {
            TimeRestriction tr3 = bioUoW.TimeRestrictionService.GetById(3);


            TimeRestriction timeRestriction = new TimeRestriction();

            timeRestriction.StartTime = DateTime.Now;
            timeRestriction.EndTime = DateTime.Now.AddDays(360);
            timeRestriction.Active = true;
            timeRestriction.Block = false;
            timeRestriction.DayStartTime = new Time(8, 30, 0).ToTimeSpan();
            timeRestriction.DayEndTime = new Time(22, 45, 0).ToTimeSpan();
            timeRestriction.Name = "Ingresos 8 a 23 L Mi Vi";
            timeRestriction.Saturday = false;
            timeRestriction.Sunday = false;
            timeRestriction.Monday = true;
            timeRestriction.Thursday = false;
            timeRestriction.Friday = true;
            timeRestriction.Tuesday = false;
            timeRestriction.Wednesday = true;

            bioUoW.TimeRestrictionService.Create(timeRestriction);

        }

        private static void TestRol()
        {
            //IRol rol = new Rol()
            //{
            //    Name = "Administrador"
            //};

            //int sd3 = bioUoW.RolService.Create(rol);

            //IUser s = bioUoW.UserService.GetById(15);
            ////IRol rol = new Rol()
            ////{
            ////    Name = "Administrador"
            ////};

            ////var r0 = bioUoW.RolService.Create(rol);

            //var r0 = bioUoW.RolService.GetById(1);

            ////IUser user = new User()
            ////{
            ////    Username = "basilio3",
            ////    Name = "Basilio Caffese3",
            ////    Password = "basilio3",
            ////    CreateDate = DateTime.Now,
            ////    Email = "bcaffese3@biometrika.cl",
            ////    Surname = "Basilio3",
            ////    PasswordQuestion = "una hormiga",
            ////    PasswordAnswer = "es chiquita"
            ////};

            ////List<UserRol> roles = new List<UserRol>();
            ////roles.Add(new UserRol() { RolId = 1 });

            ////var r = bioUoW.CreateUser(user, company, roles);
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public static void TestUser()
        {
            //ICompany companyBeta = bioUoW.CompanyService.GetById(16);

            ICompany companyBeta = new Company(12);

            IUser userbeta = bioUoW.UserService.GetById(24);

            userbeta.Companies.Clear();
            userbeta.Companies.Add((Company)companyBeta);

            IUser ret1 = bioUoW.UserService.Update(userbeta);

            //IUser userbeta2 = bioUoW.UpdateUser(userbeta, null, bioUoW.RolService.GetById(4));

            ICompany company = bioUoW.CompanyService.GetById(3);

            IUser user = new User()
            {
                Username = "basili0-63@Jumbo",
                Name = "Basilio Caffese66803",
                Password = "basilio9983",
                Email = "bcaffese3@biometrika.cl",
                Surname = "Basilio3",
                PasswordQuestion = "una hormiga",
                PasswordAnswer = "es chiquita"
            };

            IRol rol = bioUoW.RolService.GetById(1);

            user.Companies.Add((Company)company);
            user.Roles.Add((Rol)rol);

            int ret = bioUoW.UserService.Create(user);

            //IUser r = bioUoW.CreateUser(user, company, bioUoW.RolService.GetById(1));

            //IUser uu= bioUoW.UserService.GetByUsername("basilio");
        }

        /// <summary>
        ///
        /// </summary>
        private static void TestOriginBPIdentityBPBIr()
        {
            ////IAbstractLogger log = BioService.GetLogger();
            //ICompany companyff = bioUoW.CompanyService.GetById(10);

            //IBpOrigin bpOrigin = bioUoW.BpOriginService.GetById(2);

            //IBpOrigin bpOrigin2 = new BpOrigin()
            //{
            //    Description = "BP_MOVIL",
            //    CreateDate = DateTime.Now
            //};
            //int res = bioUoW.BpOriginService.Create(bpOrigin2);

            //return;

            ////bioUoW.BpIdentityService.GetIdentitiesByFiltro(company, "CI", "2123123123213", "", "");

            ////IBpBir bpBir = new BpBir();
            ////bpBir.Additionaldata = "SizeData=400";
            ////bpBir.Authenticationfactor = 2;
            ////bpBir.Bodypart = 7;
            ////bpBir.Companyidenroll = 10;
            //////bpBir.Creation = DateTime.Now;
            ////bpBir.Data = "/6D/pAA6CQcACTLTJc0ACuDzGZoBCkHv8ZoBC44nZM0AC+F5ozMACS7/VgABCvkz0zMBC/KHIZoACiZ32jP/pQGFAgAsAzBoAzoWAzBoAzoWAzBoAzoWAzBoAzoWAzKPAzysAzwaA0gfAz+ZA0xRAzRmAz7gAzI+AzxKAzoiA0XCAzhRA0OVA0qAA1lmA0KQA0/gA00QA1x5A0XHA1O7Az7eA0txAz3AA0oaA0fOA1YrA0a9A1TjAzWwA0BsAzu6A0etA0AtA00DA0hXA1bPAzxYA0hqA0AkA0z4A0hjA1bdA0yKA1vZA0e9A1YWA1D/A2EyA1MyA2PWA1buA2hQA1DoA2EWA1arA2gAA1eHA2kIA1tVA22aA1c9A2iwA1cNA2h2A2inA32VA2emA3xhA1ojA2wqA1hfA2oMA2CqA3QAA1/hA3MOA3PEA4rsA27uA4UdA495A6wrA47vA6uFA3FPA4f4A2a6A3tFA5RHA7HvA3nMA5IoA3QlA4tgA0FuA06EA1rPA2z4A2e7A3x6A5HBA67oA4+ZA6xRA470A6uLA+cZAhu7A+9jAhy6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/6IAEQD/AgACAAJQgAQ+5O0nv/+mAIwAAAACBQMIDBEQDRMRCQAAALO1AbGytrevsLgCEmWsra65ugMQERMUZqqru7y9vg0ODxUWpaanqKm/wMHCw8XIBAUGBwhpn6Cho6TExsfKyxdolpebnJ6iyczNz9EJDBkbe4qMjY6QlZianc7Q0tf+ChgdHyYreX2Cg4iSlJnT1eHU1tja3N7l6Pz/owADAO/rT6vm+f8AU/d/CxPsTbE6xOsTrE77vbu9X+36P8rff/CxPsTbE6xOsTrE77/hs+j6fz/Nq+H/ABYn2JtidYnWJ1id+Hwp/hpftN3picO/+difYm2J1idYnWJ38vD/AD05+nI0+PygI9pkPw/4sTrE2xOsTrE6xO+H8dMhp3dPD8oCL/KAi2mV7PwsT7E2xOsTrE6xO932aenTkadDTj6e3T1/R99ifYm2J1idYnWJ3f2aeD5QEO02mnu+UBENPD+n+VidYm2J1idYnWJ3bTT0abnTJafbTu6cf83/ADYm2JtidYnWJ1id1jT+aY/TU/KAhunQ06v5/wALE2xNsTrE6xOsTt89OJp19PJ8oCY6c3TS/T8bE6xNsTrE6xOsTt/VpYNPtp/dORpm9LQnhYn2JtidYnWJ1idjE9K3pkdKnpTPXv8ACxPsTbE6xOsTrE6xMsTbEuxOsT7E2xLsTLEyxNsTrE6xOsTrE6xOsTLEyxM/p/8Av6f0sTLE2xNsTrE6xOsTrE6xOsTrE6xOsTrE6xOsTrE6xOsTrE6xOsTrE6xOsToFEniWje3jJi47Hg9TeRxhrZYfc3jZu3X7/wBXxujc3jGz6Pg3q+j/AKXa3ju/+4+n/fut7p7m8er+f59v/jq/j/pPe3j1e31bOz/3H6cNrePUcbf4/b/j+j37G8fR6uX6+nr3Xdm1vHr9U3gfb9b728c+/wDN9eH2L0t1t5L5YdPbhgetvIQhCD8Y/LANHaKj5XCtza7SPMI+zd2648ww5+fqp9rHyAS6w/Z9IHkJpLvw64HmE8fo+Gu4jyyny/Z6fp/mfKUwp6u/0etvMZ/s/wCo3fZHlbDl3/vXuvceTA9fP7o27cI8mOzD7v8A193fvph5FWd2q66ar/cQ1JiYHmE0m2Efie992HNQeZe7ouly+w+UPTXGru2N5TgndcnLd5h083gPcs0Pl5eZNV5+pvKdtPrf3Yv+Ld/z9t90Aeb3z5ux9mI82tN3ZhqxI8rsNkmVB/UD/eENVDczVOwJ1jgdzUXDsQhyXDhUh5hHcxcQUuHAQkUm2MYrUQhuHCClFguEmhNVw4AhRkOGOHWlHKG8cBIGzY439PZ3sQhkOAI6fx/8/wDb4Ryr4QhBv4gjdnu2doTrg/coMhwAdX6+jt9k/V0f99vd1oyS4QKfN9g2/u5sd3t++RqvHAZc+uE7rrsI+HgvhMyHAML8PX29Vur7Ou744+Cm4cC9vsXl2dvPH2nZ7f371Mhwj57/AF/v+iUr49vbzdAN44F7ppv/AF6sPWi09+zpY3jgbr+vqq8O+TdAtXXil44F3dOFNplr17rts/nol44J/wAvTt93Trjd78Nc6eiEvHAvw59i+z1++Mcddr21XjgX79yp9m33xOMJdjBLhwBF/CXOUeV9Ofr2Mlw4serD2c6+u5039O4G4cAE9vTsTfLB+rYxCWj8YqGJw1ICdwcIbRmBZwYc+JfPouWEySKhxgwAzqjaClChAgh6hndhKMFgk6HmXZd2KjQUAMb2TWchrKU2pK3dA2zJrMyrm0Xk0XOz43dbyUpgXrCa4k9I2zOM7zmAoy7XnR7xhAzB8Ue9zjr34aqQMzUVlnzuatuqo8IKq91LRhGE1YoeJ5ugVsRuvh1aue1YxpfbLeC4rmeqV/Vt2teZMcz27bUZ+dOmGQX1xjhrWTw2OF0NW+3C6jUAEdrPWW27C8je78kwjTfbiygZRUEkm/ZgKYMMxc1Um2ISNZmUPDbdbddffd7Fl0cuqV111w/G7u7kmou5Lu7+fQLkvMIEI0OQhCgAHOSggUZYcIMwQgExDn5Ajh4d5o9QyhCRJscMcS+gO1wSJ34Ak5zdGG8BJIocVvuwF+DYs8QKxGyKmvYQQdDPMMm6SbdYrlbexkUxS2jhhldZsXSibcYVhlEYPseihCLkJzRDtDEMFeiPmo8kFIM7YnSDmo5uaj0beY2TGbF78YK0cxhKHzMrbNe0pgdkLPO8sE6TRpli9a9LYUtuqd0BzlHgzoVB2PoFEKWuRg/GYeUmaTtyMqCiGhPiHIfPsFDVMzYITUH4iSUE0VlFRB4DkIVDFCxyB+I1FDg0BMAajwlGalEkodHRkBynx+zsVnY1HKXfavNd3NeTUDwhFxxt9fza59UrShQ8TPy+/m5fp5tVs/YuQ8Aaro8N85dW3VT6bS4PCGv9l3h2yjqv9/0XIEPFb128+/sHrn7k59QdDwm9Xbq1D181+23BajwjuenN96yvnTb2T2gHhMukS9/vg+jw3P2YoDwlLdt973PyxR29oB4sO7q5un4vTt22yvYIeLX34d/b3emX3K+Lmo8XY/WG2+hNt7+6/EA8I68ftt5vTu3n17euaOh4uw7Pf36qNcO2ZkUPFsuGzfdc8+hOzpoEOZ+6OfolaIboyPmlQye3XAQVB80MpCPbd4g+YI6AhUHiPnzT4ymEM0N4n4iEYoQ5qGR+J0IcuhFI8RrcB8aXuVQo/CCgS6knmjsECPwipaYLgm3Wilqn4SSJ9l/VPw+nqYoyHKac1P2z9Pwb+XeIqfiEdHp1dP7vRiRj4jwBGB7X9cuW3pls9kqjwgRswK+Hs63TX0KUOXtp0v3/AFa/D4mdEAfhKfC7d7er9vx+Nt93T2VPwino559v0e/bv5j04aoB4hhSTf76/Ru9kHm6Aj8TYJKfqn6fjy4x6NWwHKu72dP1fd/rbq+Fv1XOH4l7D6vRH29f8H/ds+3cUfihxbsTsdMP2ev0rAPFSfdux1jtl9b/APz7+pkPFC9u9bmoO35+lvBkfhEcvdEOAmvVsv5iHzbX3N1822XYsp1PmEQ/Tr2UuBFT5i66ze7YOMj6AhJHjPnzigQBiHCFBmIAMTKMyMaz4nV3QHITWsIz0QxUcxyBCImhQHMchuyUvIOgVOrbiQ4rKNvbZSjItsNXCCazdqNRHog4gDf0YYThZnZK1GOU6mwpNn3TG6aDKDsTCjYJtxG25XOXYWZkGEpxbg+YYzDxS6Iwnc71i2kBsA1sNigyvPp2dG+3Cgdd95GV7rd13Zq7OmNuGx3zEdPx19+vAoTC1g826SQyYQmAOYr0+2nr9vZaz4yBrZdp+te284QinPEWrTBUpMFs5m0TYNsQDkVXpQoOQsIILAt/YfPqkOio7oEgaAcgeMhCDiCCoGSXti5HIwBYPNAh0Mj0KO8Vioo0+qhxvBQnMS6OghnoyHMQI1G971Z6RnUPGyZZCjtmCG5kxOx1SDNnylJxaZEbIWaBxleI1ErgwxN9TZY5ot1X9ijBo5blyh9XVEqa8JbzHRPPtw6mYdSyd2iMpT3aturrltVpRTvplMqdPbzdk6a8MNs2HEEFEw2Jt7neV1/tjMbu70eGrCLb3jd1HMMerpo8lPfPdLF65m6HlCbupp356NchWJeA9coGdRUyts53wOgYTUujlh+QkwG/KPPsA+Mo4jQMhQ3oHqPGDUSUZBnKMpbEOEYfIUiMVQHOEBMEU+RwjzSIJc8hyBHWUaAXBQjIaVlkdwEcEoc81ZqIKicHrFEDhGR4RyTlIclCHJqgHKSai4dHcF65o0QXqIpodhUShgvsasPUsR4hRq3pUaOC5qGYY1AmKnYlq4gUVYFTwhrFUIwRjBQVu4AYu6hBoILzmrtCDjLqWRiRmnNZzmt4lKU2Wc5zGc+fZKBAzIwCA8goS5JQVDKBkg4FHYMMwR3FTEhCahmKOHYzRyKyAt0gjPgjhBxFA7xipbC8uc77djUwkjQFrJQyvmyChbAZ9cnphDMjo6Cu9N01KA4Eg5SkNAVCk7b5Ct8WR2ejuKFDmdwUapnvJas3rkfEvVDnMt7lMKOk0hC+YvUzFpXVSkczKHMiAwWS5jbz3kjAFyzAZmtaA+IR0YMMz49aWjcTAqGhrYxRXAZ9AW+dWBgsvJDoS8N8hdGqIfxjz94IQoQpR9ACAkUUhuQA1ExJ+QIwQUfIOMVG/FgjE5yjI4QQx0FAACplA5AzvLCcyug+IPOIBzhHhkDgI4Y5wamZFqjOXowDiFKBjW4qIhmSjNnbIAUKB6CsioPUwAbkDVFCyucxyF0JQo5zuAjsSzBJZy6FSEcIHzqzhcjZCK6K7IGIqZ86oWZaMgRxxiHGQEnQWeSO1GhcwAAQEAKoYADgZmZmChmZQzMzefONRZmRgj/IEJd2eECAVnIKF6FBobIHBI5HSA2AYIdAQzZwSKgM5LvRWCkDQUZipcanOgskOCUhs4d6OsnCAwc7pMUwRRC3sK1VIWV5S/bjSsUhBQCKLJVGZ2AZnM0Lg1zaChEkCGkCssUYuQihayHFTKgQOeMuCEJIzgss0KEIYNZR8BMoGIY1sUV3OQACslaFJhgyNoOG6RcIan4zqZJwXQvxgRjeqCZP9Y8R8+oQQEKGo8ZCF0YIyRyOpCABxxuuEMUIPGWJCUm4NQzHJDUozzds5D0xfFnIDjO1MLblqFPEcrxbqTCEL1E1lpY9gUAl5rnbZ1Xh3QodhIyiMUEEkoVyDLS2HV2QLsugjKUuxZppRodZvmN2DkNRgjEtXPtkVLhMEVGGU3vLBSWcEsDlN9EZ5C9yAi13Njq2IWYhXL5n29fs3bkCKVR632z59++V8TAJzymmyd2++Jw2ciIZ9qqb2B0EwXJZ4PGXqARkPn8ygQoENRGhxkISPyBwqAoEBzDIJo7FkcZyHgkPe1T6IUXEF1R0c5gwdqK4eFQZwxJaKmlUMzSdGhmADo+YglandCS5rOGEqJJDUADlBt7n1MCjrstIOV5RsiQRUwvRnykGWzZNwpxtY1zfl2UhUXBV29VYwu6m2ycG+VN5ymfVrN+27BRIkDM262J0aEZGmrZgJPfSWxWdgZ5ijOjTKuoJNaqUeey5KGENYSmspF0tqoM4eGqXHXsk44xFTsR+QhHAB8/mULIUI+QIDkNUI50FA7qKgoOcF2Rwzhn0EOGktTFjoIBQkuyCsFyZmpg+gpO96iFcaCkQ9EYITFZQkTmXKM0xW5VsaOgcYbdAdGCQRUrZwKjeGdnSKy5YnFHDkjMFwQsgJLIc6kMKjUEbOUZGdqgRGAzPCEJAQEI1bA1QUdCig5nLo80YKUbO0puXUFzxxg6zZ8GepjWAkY4quAKHjdrY6jNA3IGQ0oF/tHn1wj1FxUDnBQISxIPyEukICKjnLBUcICz6GdjOohH44UlSFdGIrIZCXBRhnLqrA5A7GsVBGVDUq6IJKuDVSo1saiCzqyEZSzAJrVCzukZ4ZkwxDksJmsVUiRaovjWWeqbhgGGcGqiLUWcZwyNMEhyjq9a0Qh4KFUfO5YFZhGImuYqsXgOkGAgrKTQq4BV85Y0qpOjMyMcwDtfRqKjwOMkKi0XBuQeIZB+U+fW//6YAngEAAgECAgMFCg8OFhURGQAAs7UBsrYCAwSxtwUGabC4BwgREhMUFRavuQkKCw0ODxAXGBkaaq66uwwbHB0eHz5AUFGsrby9IDc8PT9BSEtMTU5SU1RVXKmqq76/wCIjJCo4O0JDR0lKT1laXWFjZKWnqCElMjU2OUZWV1heX2BipKbDJicoLzA0OkRFW4iYmZucnqChwcLExsjJzv+jAAMB0qMb0c35FDBRVTbJflYRNeMHdflpqpdVBds30BEcDhl+YMcdu9+pyy/Nqzu9093L2/osjL82zyudz9H7f1cgxflMdz+n2ft/u930ft+lxfleOtd3H+7639n+H7P0d+L8p3nL+/2fp9vL9n09/wD6bEvyE7j9n939/wCn2fr9z7v8LJflOU91bf8AD6v/AL931XrUHyot+76f/wB917JWL8u1R/b9X+H7Pr/uP+Xs/bi/I6/VOW05Vt7fb9f0akvzfSctf0fX7Ppf0fTc2L830E+j/wB/28p/6/r5HsuX5r9vI/X7vp7v/D/x/wDObMvy37OPsr2hXs/8v/b9H+GL8tH6agE923/H/wDu/F+U1/VRrtq7Svq/+Kl+YfbfdVWd+3e4vp5awO6/p1vN9N44/RdG6+wMG54aaUxrQegCewqGg+ZMU+B8jEcbQrbQfKYctZWD5HDjlaUuR8hGDx/We7+/aXgfIxnGVx9nu/RsOB8pX0fTt9Hs4/VrP1OXyC/Tfu7uPt1e76f2kHyAd3ff1Wd07u7lUHyHLl/fr9fKv033fTx73D5eLrt+z2ba61r7u/bD5dtivbr3/wDL2z9pdwfLes9u3eV+nlXf9Hcj5L1Nvd3/ALLOMH2YHyEv6Jt+zbu9xy119wPkONcVv3d31/8A5Zt3wfKvdPbx7+WPZqbQfL3+zvvXb6H3fTy/b3MHyPH27NPGPH2d3HA+UScXbkz9XL6+5HykfqhO/wBpt3OB6OV1x12muzkerWcWa7h7WHDYcVssY+m5e0vtu8bRaowdJdbS7s6zN91kqre2rdbbtt6qDXLySU9VS2u+6DlF8xU9nHvlTWDcfMQ2qsiPF6S6u9mjjDVOhnf3tzjxJq3r0KzlVwur79XykNe7a6m3HuneL0O05W61O7bv48ep7tr2OU4/X7u7j0Ps7+Ow1O47/wBP/Czorl3Sj2ak2v8A+vd0uCuPL9eqmvHqO/vNg2o9k/Q9TKlbQv8AXyvpSuLsXr37V3djcrLQdhxYXDaHaO4eHuwtlZPRZLHtYFTXD2IrE86zaDuDqqJyJqnZQFyttartKqVU122OxNU2m0K1671BbJrtt1G0qMadtX0XZq/TA6i4Xtri2J0GqtTvNZevZdtBTLo6TvhW22sLw9F8eVVXKVrfLjddOvLHu4uBYdO3G+7v42XcTqJfEly+O07k6RmuqcuKw6jaJtZeuvbbRaHoqU1iz0OXJw9SWxGDpXlcMNE0ryuTZdkhgrymKLlXjZcV5VaGq1sBxXmDY11myjmvKw1R5FStc15Wd93bd/p1hGV0VPZ9H6TWzu7s15CXx9nL6C67vd+r9MZXTq3r9RV695ivIXx9kv28vby15TXWFeRxrO+57H9XdrtqSvKUNuuvu7r7/dmvKX3e36/03+v9lfo5e3NebXunLWfX36/TsXivIa998T9Xu/bP75RmvLx+mv8Aj9G31ns7u7X9Wa821/TtXf7fd7p393HFeVvXW6+u6r6X/wCdiV5r225Oxt7vb3cTFeUqfV9epdXyXbFeZ40a1PqUTFdDSzV5QM15iW3DAaa/GcNAGxYbzpYkBo87y4msulyeQhFqcSamh5gSo63HrJdy12wwgeUTbWD34OswbHI5e6nrSu+Xx4921e2noSiu728e59tfV1r9P6zv/Vtyns16r4vJrXXba+WvI6djO31fT7dXl3HTyo43ew17e4wea9r4/o47X9H03rs+Zl8tWuPfAb2enayuXK36uN+3i9BrVe3vfp1h7Ll9Op3bfRPY+3bY5dZdFzXXv2+jVh0HLY1D2gmDoJ3VUXus1yeaqqPF40RwdDyu6g2QwHQRrYd4dQYeGu4ImX0EXNdiyoFxI9aYKu/fSUVhOqoarrQ31EqGKYB1gGAxVL0LfIvFbbFsPMLSiUvedNVnYjdN9VtW8aDaavLpKLxVURh1UWaza0OVvULrNgxt39OvEnKXcutkOiyvYeyivZrsPVd3OW0r24dulO7C7VNbQ6Aq7uXt37XS+YwX366jtTHsRboxfoKiD2kTDh85DJHQ4ebh+EjTfqJcbm3Yl0Xh9NVWjDqZYYqN9jGWJLT03V0TXqXLKiPf12kqlyX1AYcUMOlbZrAUOPVtLl7N4SHSE2ItF7WdLC25epCnpCcSVeuDbqVG9tuW1bV1F3eocTjxlvUQpccb1HrrFCmzfVRZgi1DpHRYhXnvBi07TCOX0BocPpi+odZTK89l4JXo12q0ival2LaZPMAjqkU6jblsub7XN5YUddRuFmE6S9blEJVL1UuvJhihh0U7OOTON/TXXq+4uMU2roJxOWLlXXH2dOqJtKmtXqPaOnffLp1hq6xS0XoHGrdWlzbqumlraI7dL3lzVZ3PE6mE1Y936gTpRtm2u3svFei4x2fQ7n1GDh9A5O0zesPguVZCHYBY4ewQpYMfPbgwx6ld1QO0XeHUYTAwrrbhHNB1NtQY+iiI3LU87GUQ1p8zFl4qjYOlq6YktYPSbELik2HoadbNqGWR6a2jghGa9VYBmzZg6bwcjWXaJ0uK2SGCjqosBxSdgy5TGvifWAjrl7CCstDzsumHoGDq0HWwcawjg6QeWEsexhCA012DEt1j2OEMj2WurTo9dYuXEqV17U7NZTY6UZxp0JT27KMOti4MjLOhYzUiXRHoM3dYaIHTVOW4rZ06oxAwdixcnaRl0x7RG5WKo84iVLTtclkv1OHh+L8SWWdrGHvoro9iFFQPU4XB2GDcR7BiQcPoTFYOocJEwvSuHDDD1mVawdlwAwLLeoNsrGr6Q3oRDpVipFI9TBvDHB0rsreVh1IUDgXqY1d7ZYddESBhetDFwXB1NYRy9rCNYT4Th8uHAdZoEMvoQzth7XChg61KuVQeghD0N3vNewgDow6hGCtr1hLDDth7KjhA61xUCGoR6AhW0DBDpuqhKAY9WyIxinWCti7SiHRdVWzTTbVdRtjljUjDpqkCtaUQ6GKRJtcb6V2msc0+cikIw6g3EfQw+w4eHsfujDAvv1o9lK3ufQsB7HKLAHrMIlnaQg7jqYxgLF7CEW44OsGOAlvSVLWIVF67Lg6C9pEY1b0Nw0cWV0jG45Ki9RhhdN1Z0hCFrg6jJKJqIvWIY1IPYK6B1iYH3wgYH0p+48Pd56Ot0KpJfa4Vow+dclg+c1BYD0sPhdHQOolrDIdimig9IKVKqEU7KgAw6xoFsI+hdCjqKGGAg3007S3cHWBKYxs6lIBAj1hBwKsOmqYpAO1gVLTzrgixh1kVWN9rhj9k3vrASFlnpMDH0qlsb7EYYPfYuF63AJo9bFCCJ2OXL5yNUYcPW5cAx6TcxiB1bGXKkOqxsMuHpMC5InVdI5GuymO5hXU04blQj1O6jIdRG2PqVwZE6kIuEYdiYU98y/CQ4fjUHzu7UfQRrYy9ZAjtx0PUI9pKWu/ZHtBMWqdTgzcJXaYSOAOynFKw6SasIAYOsiwvWCQ6L32xGuxl3hydYjgwp0lxNxh6UgZWU9ThyxGHZQYQ7QY4cvUkLywDroMOFDralQv1VtLFj0gQqr4ooBDh6uH4G25UexwYt9F4pgEewxVL76xA7VBC8HUYUEuu1pjsRexyURWL0MWK3CUQ6aFxVDtb0hCyowYdVxtW3L02uGAmDpbLy5IHU5cGTqrckAj1kcOFesA3nXVUlkbh2MuF4e0jamF7bePf7eQSnz1exWtHD7PsVaecwuB89xBIe+4PQuEHzkIQcX77uOh3uHtN5o9ZhwYIdTvcPofA9Sb31L8Dg3v+U5x6nxHUaG57SO885hiemxlkfSDh6gAAlAAAcPQ0X1GD0o5Yecy/EsPUaPqXBh7WHwhHcPWRcvqQ3EfUYTrfCnWYMHoNF9953qXBDcfEdYQw++wjuDqbjvXsNGMPPUMK++sA98jw+bjCPxF375cPQVDFR84l7IEe1l5fQ869lRI4Q6zeRj2DLyMrsV3XHqFwZKu4vQsAcUD0uFfVeXBhwdpExSj2Lky9LDcZXqYsFhD0DpXWi5I4rqEgbJg7abuo+lu9nuKa9ASptfD2MoeowwwX52LLX0WkpcHntivoLbxRD0uEU6xYMGKdjAlsOx0rcB13Lw4XrSXKgbjoMUXnVw9JBYQe2tEYwh2MIQgnUQdzDqqGKcmHprCXoh13BMsTrqFmGPWJGoQB7KwXCNnpDFbemxiesBGWXg9JE99CJg9ToelwmDted7QDcdhG34LSHqYLuetw5IAdZoRw9ZEg4fPcfhcDzHURHcHWw5h6jRwGTqYb07SXKyPYRol6+li4v3wlDH331gI8/A5GGDsGLhw9jFGHaRrcec3HqPgR8DDpdDc9pGODsRwMEYx6l0GmPSjzvYfCRhDIR6Rh4D0iZOoEhvXrYZcPaLEwdhFuEPUxYbfAj6wE9MHqID8CZPUnwsI/xP8AlP33g6Pheo/dfgf3nD9kXmfOaPpIZfjD31+JY8Nwj9hX1MfSZXB8CHqMOHtOZfsvxv2HR9BvcnnYep8T/A7z/I+8f5gYuX0OH0rgDzmU+MI+sBgX1gNqcPwAAAAAA4e58b+4/wDY+sCG3D6wE9P3j4CGH0GA+Eh8Y/uvY/dfjfjcOH3z4yPpY7jsHD+ceYidQ4cnpIwh6Af3HJ2u5IfCcPoj/iMHpIQh8R6CHxMdH7Z+8druV7XevodBI9NQ5zrPjRwQPQYdzE7BMmHqcpD0P2HnOwsd52pcdD0mTD2m4fiPWAjj6wKhfWA0D6wEDYfEf5D+13H/AFvOfZMvvsIfunwD6wGBOs4P7DyHDweZ4RLwa2OHncHBuOD6r4w0Q4M9bjg6GjlImDR3Ufnew0TDHC/0sPERYcxvY4fzrzuHePiP9qc6uXg1OlHwmjg/3rvfA/7DmOZiaG4ODM6Jl8b/AGvMc54Xg0MpwZcHBwU5zg7PwJwcX+twfYDmODU+M3HB4I735nmOk4P7vOEWf2PYZGH9DznQc5/YcH94OTzvWf0Gjveh6X5jtfOH5X99/IbzB/iPzO8fCcHY3HxHzuH0Hjf7X0P9jH338b4CJCG53PMw3PzuhvdB3GHBuOZ5nC/gckdD4353c+V7X8yvvPO6OhuIR/G864cnQ7xPxHQZcvkeZ/K7nc4fE7mBwZzwmDeYf6XyPYbjCfOb3Q4N7ucO93GDsPmPG8zHymjg++870jDqX8CZcHid7vPC6HBnTxPhT750mjk4M54R0MvQ8x+MjuMMYw4NBhw4F3Gj5n87DQ8D4Txm8/CgsY+Rh0Oh+FhHxmTL/ax3jzLhy8GUec5jxGXyH85zG80PO/mYHjfgY/O4MngODcZJbk3PoPym4IeR6WHzG93HifG6H+oyYI7jK+Ay+Q/CbzBzsPefznMmRND1P5nnNzk0Nzuf6XDueg3GT8xoc5hwbjB4H+l8Tou895/qOYykSPgP7HyGCMHymD85Dc5cP+83ODcZI73oPwnmTQ3nBqec3OHD4ze/lNDnHc8G953g7GGJ8LwZh8ZwamPUcH9wcHsy8HA9YEAvrDln/6MAAwHoPCeQwL6T+U5jfrH3je+Y/lMLuSW+My+BfCfadDzEImTDuvDHsPwmho7aGDevM+Q/hd5vPAVHJl3Li2PUfcNzzu5y1HJkwRwcz4j5DqYwDAo7nCh1H84ZtN6MWLHB1n8K+Iyy8kWOUTBhF0ek+YqG5wtQpysYPvO4+Q30YY6FQMmB0Q1YS7dwniP4DyFLgNxlllMW5WDcErynyu4VcM13rSZcWLgjWXzH3nc5IJDRhRAVlYDe+E/jHN5NRy5eZrRc2S9H5h3sVlGCKFwluXB4q8R/oY5TIZNhgRYQcXF6z+Ug7jIOAzUcC7xfIfIbiGQXQwQMajzBbmjzB/GYM8Y7mLqMSXaVl0vceQ/CsHTUm14vk0E1Og5j/tHRgRhSwJyvA8o2LDex8h8plXeVlYwb0Llt1WTRyaGTJ/E73QMUDBXARsDiENB8p/Acy7gXRqglhG3I3F7qhhxeTJznyEHDDC4tqDhCFLAlpZUq10PIfaPeSLK2MEoy2mdqt1mt3toeQ+67h2aMVywECDa4ZZLIS0ieU/hMG4wW3ClY2QCnaXh0WBdu4w+A+2cxe4qMtvcLLuMXR1djI6H4qixZWKWViobEbw4UiL3Fw8h8imho4olNtOGtitnBCGTOuDD4j+MYQjkojKZTVtXmlyQWa0eU+0ZqEIFOzLCK1cq5cLogGCsbYCoYPCfIbgrWlvIQYwTYIYDJrjlk3vgr+E3qOKu3AwMWy5co0ohktIVut5z7LkYumsINxY41i3cLAy2po2Ud0PGfxJQQl7maxu7c1WW3cYq2GTwnyJlQhWSDNcbLbGUI3o3BpluHxH+cw77QhBxdQCWza8hCXoSowGzxn23chgKuKBrNtrwhKqiGaMVmo4MBDcfaIZqoEKpmxYVt37MKhNeNambYAwrmOc/7lxVSrdoF7LQGDUqtEMhisvhPtGlcYEQqFt2atsoooKqgCMobKq7733j7a4NYxCU2tqS3BhM1mpyuarNeJFhHnPtGUCpcNri41vUaYQS2E2hG8WFdwW73cfIZKrGtbXi81rNq0dTk0WyoioTUw+8faMoRgu0WFStTYWN0KDaECXF140NnhPuuHIsY1HjLu3OpEiuEp0qjK+A/mvCimrbe20qsVLtCFEvGuuzk8B8vK4GHNVe0u0drvbYNYmrttvbtheE8J8rdQhu1W3bZbwbbQgDLwuDNlvjP4li4Ylxb2TDq7ENTVnLFoLhLQXwHyEIylCDlY6MbDUsuXhsyuGFvhPlWuOsWiOxNVATDtLKwm61hi2VUPAfbY4CotVHaJjVq2ibMqoyisOyoF7Tup8J9y8DToPJwmuNsAx11iccrcc0s1PEfdcEAMd4ZqhcNMrXBqGKIupV7Hd7x9za4YLcUZYVKULlYrFIYDZaKjO73j5BlM2u4w1WLglla4pNXk8iiLtCiAh4j5bjraCld+GXdRKobhUZdVxYLZGNwPCfbZVtk2lQx3o4ZtcKWsEblTVcXcvNuHwHyGNrzUC1wS7AhQ0NazaUVxIbJFRu78B8pi4QDF2VsNUW8S9daobvXWsG7YlRic59xmpmoEDYq9bKVI66lE2vUG1zVwlEfAfaMIF5JRUYbQrC23KcEvXFwYTbGpQeE+4wLLmtOjjY1eSXcKZd7TUNk1CWOsOPiPkdL0u2FsIDyt2Zqay7toBYBsPGFeI+4Xeto1d3hsGoa0S7IReV65aEFhgPCfdvUoajZLcbYoze2LZt7TWUBRrzr4D7g2ax2w5vK60RdrIjt7aJrWs1oi04fEfcIwiwYu5XYGFZt5YrWgKqN4t94+Ug4spgwJTG8WGm13RqMrBNrivvHyrLARmrKxqty7aS9KNdYyrYXe48J8hjbcFlQIURdoMMUUOuuGlTDuXnPlGXo7JgUJaIYMtY1ckeNTaHjPlAucsLCl2Y5qGLhGBtThw68ZcA8R9w1Ixluly7LlWwl6AVsDhdnWIB4j5TTaXtQ3sXtLTDB0GUYtiY1GB7x9xYy7jDF4WXAHRwRxcWGL0qPiPlAIxsVxyZcqEYLDLLwwYtvlPlI0hLJaaWmHnFu8Mppzae8fdMmadkbYYd4Zu4kYQjKj4z5DeYcbYcmXDgjuQWOLmsD3j5DcRc3gwuFMpi8KwwRblW+M/iEjmjCZIaF3BiuWGhUfGfxsI4NeYhubw4YQI2whHxn+hIwOYIEBgsQysSPmPuXeVwMrDko8FN5Mrg6T+QtgwjkIGDBGEG/QfeXQyEMFFRGBBeZfKfecLDFAZcBBMJhXL0H8rHQN7kyZMPnP5A0awR0cJzuA7D7y7yokVgx53cvQfeXeEV3LHR3r1H2TrNCLb4X4T+Y5l6Fh2n5X989YdtnB1dx7z/rP+c4Oa7nJ/S/A5dDI8GlwYSOHKcGxjmzDWXDuIfgPfN6ZTmODU+8YvJwanmGJghB6D8omHwMeZpjvf5nwJuPCkIaO4f+85jwuElLwbmOHnd5/sOd3ujvTI5N5+U8CZMEpl5dHxH4zceFeY8r/qOg3EMENwwdz85k0MGXnNzk/ocnNWBeYTBveDMe8WWf7ne87zjkMJucH5jmd6GFgwg+NP62COlxlWbky4P6HRrBzsuF04dEcn4TLzGlYrwpkGiB/rMJZCDBhg0dElSwwflcuB3vNUOZIEIRw/O71DDHC5DCczHwOT5iMMsRYuHBDRwOhof6UiYIZTDgNzDRlGH8R4k0NDF4IZMGjg/M7jmNDCxhvMGCB85gym4WXmmOUvmcuQh99jk8A4vJeGPhSLG/yMNHDhLMJaYcHhTDDRPvHvOGMI1HAbghuIQj8xuMVZgwRw7zAbzc/hNA0INQMESBgyQ3U5RMH9FQSESNaMMG8Yn9VEEhuI4cm5w4E++eBy5RMVo6OA3MEhZzH40cENDcBK0MmESHzGTQdxkcWaEGOTDggYP5DmMJh3BvNwYLwhoQ+Y0ZTETQjg3GCEI1EuCfymR33ojUcGHc+C5QmBj+Eg5HDkykXeMTNWGVIn4je4cOEgtljhcGaSKyz8BoMEyx0CURjLwujDLE5ZT76R52OQhhLEjEHc4cv85HnJeAwJCU8zk3VH8ZDJgyEEgAXkSMQwQCEX5zcYajEgYGCNOhkMEH8Q7nAQgmGEKQYi5Gt4P4Fgx3ppUc3Tis1HIhuoh99hFIc6EYy9DnawZJQn87zroXpe4YxyYI1Aj8ybnSs3hjo4Y8xElRPwEMVvXAlpEwwvDHQYYIfMYqnCWuBltbjDhyb2EE/AIiO5AjLhkhlywTIw/I0xy1AwlmTAGGETLhhow/E7yzBhg0sodCWNRNFw/fRB8AwjDcAJCWJljh/nMuQjgZWS44IRyomC9F+ZblYKcVgjBjE5rg4GWQjg/A4dCJZgiRLIIxhdENw/OxSEcJfMYuVlzTrgwwifMKaORN7lrCJCVg0d9fiQFyxMXgYErcbjIF05H8BkcruZSaGjEcGDAiJ86Oi5WU1kcuAgYcMSD+JNHJAcMHDeCOXBzp+R0QjCMIub3scDlwkMnzkQIx0cDHncLByYPysYGSzLB3kMMeYynzIOjlrBoxOYl6CaWfiPCQ0IYdDclx/wBZhgOgGhh3G4R0PzGEgmCG40MODmfzsI+AhoaOHneY/Geh3uDwH5B/5j8xh8ieo/I4fGeZ/wBzBPAZE4Nb4zDo+E/014DQy+MeDKc54XQ0P6zyG44Opzu84Nrl4PbwhXwvB0eo4Nx4Dh2nrD4NfWAgBuOEK73h1HB4Oh4ODHc8HY8LHmODU+E5jg5uHJwaHzPUcGs3nBnPMrwfg87wZXBg3hwanc+N4NR6wF4Mr2PBpPsHB0Oc4Py5eGwfGf1PCSfeOD+bz1gOi8IF/wCp4O7D+p4PB5jh1H7z+U/znB2Pyu54fJ/mf5j/ABv+06Xg3mD4j8Zo+E4dzufM9bvODK8IU63hmn/SffMD4Dg5nCuXh7vB/ODMaPBuPMeY0f8AY4ODmeZ4OL7wHBsec6T+18hpeHg0Bh8Tl4Nh4jhEmhwaX0v9bwfyO8eEQ8IBeDgdBwZn3jhDvCGcv+184cGw4RJ5j+k/eeHE+V4dz/0PrAWV9YrHvrA3w9YCGHrBAM4ep6wMFfWFAxw/z1gm+esBaj1gQI+sBf31g8QesYE3h7HCDPWJC56wpgfWCe56wYKeHs+sDZXLF3uHnYqsVY4VVy+sTcjxnB5fWAhB6xEHfXUH/wD/oQ==";
            ////bpBir.Minutiaetype = 14;
            ////bpBir.Type = 4;
            ////bpBir.Useridenroll = 17;
            ////bpBir.CreateDate = DateTime.Now;
            ////bpBir.Timestamp = DateTime.Now;

            ////IList<BpBir> listbir =  new List<BpBir>();
            ////listbir.Add((BpBir)bpBir);

            ////BpIdentity bpIdentity = new BpIdentity();
            ////bpIdentity.Birthdate = DateTime.Now;
            ////bpIdentity.Birthplace = "qwertyuio";
            ////bpIdentity.BpBir = listbir;
            ////bpIdentity.Companyidenroll = company;
            ////bpIdentity.Creation = DateTime.Now;
            ////bpIdentity.Documentseriesnumber = "dddddd";
            ////bpIdentity.Motherlastname = "dddddddxxxx";
            ////bpIdentity.Name = "ddssczxc";
            ////bpIdentity.Nationality = "asdaddc";
            ////bpIdentity.Patherlastname = "dcdcsvv";
            ////bpIdentity.Profession = "ddadscxx";
            ////bpIdentity.Sex = "M";
            ////bpIdentity.Typeid = "CI";
            ////bpIdentity.Valueid = "45644455";
            ////bpIdentity.Useridenroll = 17;
            ////bpIdentity.Verificationsource = "VCI";
            ////bpIdentity.Visatype = "dsfsgbfdg";

            ////bpBir.BpIdentity = bpIdentity;

            ////var rr = bioUoW.BpIdentityService.Create(bpIdentity);

            //var a = 19;
        }

        private static void TestCityDeptCountry()
        {
            //ICountry country1 = bioUoW.CountryService.GetById(5);

            //IList<Dept> deps = country1.Depts;

            //ICity cityc = bioUoW.CityService.GetById(2);
            //ITown townx = new Town()
            //{
            //    City = (City)cityc,
            //    Description = "Centro"
            //};
            //int tox = bioUoW.TownService.Create(townx);

            //IDept Deptc = bioUoW.DeptService.GetById(1);
            //ICity city = new City()
            //{
            //    Description = "Mendoza",
            //    Dept = (Dept)Deptc
            //};

            //int cid = bioUoW.CityService.Create(city);

            //ICountry country = bioUoW.CountryService.GetById(5);
            //IDept Dept = new Dept()
            //{
            //    Description = "Capital Federal",
            //    Country = (Country)country
            //};

            //int did = bioUoW.DeptService.Create(Dept);
        }

        private static void TestPoint()
        {
            //Point point = new Point()
            //{
            //    Address = "dir-punto11",
            //    Code = "XYZ-34213",
            //    ContactName = "Baasee1",
            //    CreateDate = DateTime.Now,
            //    IPaddress = "212.212.212.212",
            //    MacAddress = "HF33d331",
            //    Name = "Punto-uno11",
            //    SensorId = "eeed2221",
            //    Sector = sect1,
            //    HQ = hq1,
            //    Is_Dev = true,
            //    Dev_comport = 3,
            //    Dev_display12 = 12,
            //    Dev_dooropen = 2,
            //    Dev_serialnumber = "11-e3334dede3c"
            //};

            //int p1 = bioUoW.PointService.Create(point);
        }

        private static void TestSector()
        {
            //bioUoW.SectorService.Delete<Sector>(14);

            //ISector sector22 = new Sector()
            //{
            //    Description = "Sector tgtg3434",
            //    CreateDate = DateTime.Now,
            //    ContactName = "Pepe pompin",
            //    externalid = "kwehuehuiew"
            //};

            //ISector sector22 = bioUoW.SectorService.GetById(15);
            //sector22.Companies.Clear();
            //sector22.Companies.Add((Company)bioUoW.CompanyService.GetById(16));

            //ISector sector23 = bioUoW.SectorService.Update(sector22);
            //int ids = bioUoW.SectorService.Create(sector22);

            //Holding honding22 = (Holding)bioUoW.HoldingService.GetById(1);
            //// listar sectores del holding
            //IQueryable<Sector> sectores = bioUoW.SectorService.GetByHolding(honding22.Id);

            //HQ hq1 = (HQ)bioUoW.HQService.GetById(4);
            //Sector sect1 = (Sector)bioUoW.SectorService.GetById(1);

            //Sector sector = new Sector()
            //{
            //    Description = "Empleados - Cat B",
            //};

            //int s1 = bioUoW.SectorService.Create(sector);
        }

        private static void testAutorization()
        {
            //IAutorization autorizationTest = bioUoW.UserService.Create()

            //try
            //{
            //    IAutorization autorization = new Autorization();
            //    autorization.StartDate = DateTime.Now;
            //    autorization.EndDate = DateTime.Now;
            //    autorization.CreateDate = DateTime.Now;
            //    autorization.Motivo = "Ninguno en Particular";

            //    int idRegistrado = bioUoW.AutorizationService.Create(autorization);
            //}
            //catch (Exception ex)
            //{
            //    String hola = ex.ToString();
            //    return;
            //}
        }

        private static void testcentroDeCostos()
        {
            ICentroDeCostos centroDeCostos = new CentroDeCostos()
            {
                Description = "Test10",
                Valor = 5900,
                CreateDate = DateTime.Now
            };

            int resp = bioUoW.CentroDeCostosService.Create(centroDeCostos);

            ICentroDeCostos tempCC = bioUoW.CentroDeCostosService.GetById(1);

            tempCC.Description = "testUpdate";

            ICentroDeCostos tempCCU = bioUoW.CentroDeCostosService.Update(tempCC);

            ICentroDeCostos tempCC2 = bioUoW.CentroDeCostosService.GetById(1);
        }

        private static void GenerateEncriptedDataSource()
        {
            //Data Source=FIDES.cloudapp.net,8105;Initial Catalog=BCI;Persist Security Info=True;User ID=biometrika
            //var dataSource=   "data source=;initial catalog=BCR_Log;Integrated Security=false;user id=biometrika;password=biometrika;

            //string dataSource = @"demos.biometrika.cl,8105";
            //string dataSource = @"EDGARDO-WINDOWS\BIOMETRIKA01";
            //string user = "sa";
            //string pass = "sa.admin";
            string dataSource = @".\sqlexpress2014";
            string user = "biometrika";
            string pass = "biometrika";
            ConnectionString csKey = new ConnectionString(dataSource, "BVE", user, pass);

            string enc = CryptoProvider.Encrypt(csKey.ToString());

            Console.Read();
        }
    }
}