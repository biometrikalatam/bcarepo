﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class HQMap : ClassMap<HQ>
    {
        public HQMap()
        {
            Table("HQ");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");
            Map(x => x.Address).Column("address");
            Map(x => x.Phone).Column("phone");
            Map(x => x.Contact).Column("Contact");
            Map(x => x.EndDate).Column("endate");
            Map(x => x.UpdateDate).Column("updateDate");
            Map(x => x.CreateDate).Column("createDate");

            HasMany(x => x.Points)
            .Cascade
            .None()
            .Table("Point")
            .KeyColumn("HQ").Not.LazyLoad();

            References(x => x.Company, "company").Unique().Class(typeof(Company)).Fetch.Join();
            References(x => x.Town, "town").Unique().Class(typeof(Town)).Fetch.Join();
        }
    }
}