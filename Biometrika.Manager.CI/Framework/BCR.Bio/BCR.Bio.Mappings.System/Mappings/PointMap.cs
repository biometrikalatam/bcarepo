﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class PointMap : ClassMap<Point>
    {
        public PointMap()
        {
            Table("Point");
            DynamicUpdate();
            DynamicInsert();
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Code).Column("code");
            Map(x => x.Name).Column("Name");

            Map(x => x.IPaddress).Column("IPaddress");
            Map(x => x.SensorId).Column("SensorId");
            Map(x => x.MacAddress).Column("MacAddress");

            Map(x => x.Address).Column("Address");
            Map(x => x.Phone).Column("Phone");
            Map(x => x.OtherPhone).Column("OtherPhone");

            Map(x => x.ContactName).Column("ContactName");
            
            Map(x => x.UpdateDate).Column("updateDate");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.EndDate).Column("EndDate");
            Map(x => x.HasPrinter).Column("HasPrinter");

            Map(x => x.HasMail).Column("HasMail");
            Map(x => x.Is_Dev).Column("Is_Dev");
            Map(x => x.Dev_Id).Column("Dev_Id");

            Map(x => x.Dev_baudrates).Column("Dev_baudrates");
            Map(x => x.Dev_serialnumber).Column("Dev_serialnumber");
            Map(x => x.Dev_number).Column("Dev_number");

            Map(x => x.Dev_umbral).Column("Dev_umbral");
            Map(x => x.Dev_attendancemode).Column("Dev_attendancemode");
            Map(x => x.Dev_password).Column("Dev_password");

            Map(x => x.Dev_comport).Column("Dev_comport");
            Map(x => x.Dev_modeautentication).Column("Dev_modeautentication");
            Map(x => x.Dev_dooropen).Column("Dev_dooropen");

            Map(x => x.Dev_ipserver).Column("Dev_ipserver");
            Map(x => x.Dev_gateway).Column("Dev_gateway");
            Map(x => x.Dev_userlength).Column("Dev_userlength");

            Map(x => x.Dev_display12).Column("Dev_display12");
            Map(x => x.Dev_refresh).Column("Dev_refresh");
            Map(x => x.Dev_daterefresh).Column("Dev_daterefresh");

            
            References(x => x.Sector, "Sector").Unique().Class(typeof(Sector)).Not.LazyLoad().NotFound.Ignore();
            References(x => x.HQ, "HQ").Unique().Class(typeof(HQ)).Not.LazyLoad();

        }
    }
}