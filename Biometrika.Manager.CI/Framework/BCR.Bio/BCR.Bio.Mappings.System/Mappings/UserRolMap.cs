﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class UserRolMap : ClassMap<UserRol>
    {
        public UserRolMap()
        {
            Table("[UserRol]");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");

            //HasMany(x => x.Roles).KeyColumn("intID").LazyLoad().Inverse().Cascade.All();

            //Map(x => x.RolId).Column("rolId");
            //Map(x => x.UserId).Column("userId");

            //References(x => x.UserId, "userId").Class(typeof(User));
            //References(x => x.RolId, "roleId").Class(typeof(Rol));
        }
    }
}
