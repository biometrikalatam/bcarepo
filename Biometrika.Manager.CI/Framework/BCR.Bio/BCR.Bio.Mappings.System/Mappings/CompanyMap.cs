﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class CompanyMap : ClassMap<Company>
    {
        public CompanyMap()
        {
            //Table("bp_company");
            Table("company");
            DynamicUpdate();
            DynamicInsert();
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Address).Column("address");
            Map(x => x.AdditionalData).Column("additionaldata");
            Map(x => x.ContactName).Column("contactname");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.Domain).Column("domain");
            Map(x => x.EndDate).Column("endate");
            Map(x => x.Fax).Column("fax");

            Map(x => x.Name).Column("name");
            Map(x => x.Phone).Column("phone");
            Map(x => x.Phone2).Column("phone2");
            Map(x => x.Rut).Column("rut");
            Map(x => x.Status).Column("status");
            Map(x => x.UpdateDate).Column("updateDate");
            Map(x => x.Holding).Column("holding");
            Map(x => x.AccessName).Column("accessname");
            Map(x => x.SecretKey).Column("secretkey");
            

            HasMany(x => x.HQs)
                .Cascade
                .None().Inverse()
                .Table("HQ")
                .KeyColumn("company") ;

            HasManyToMany(x => x.Sectors)
                 .Cascade
                 .None()
                 .Inverse()
                 .ParentKeyColumn("company")
                 .Table("CompanySector")
                 .ChildKeyColumn("sector") ;

            HasManyToMany(x => x.Users)
                .Cascade.None()
                .Inverse()
                .Table("CompanyUser")
                .ParentKeyColumn("company")
                .ChildKeyColumn("userid"); 
        }
    }
}