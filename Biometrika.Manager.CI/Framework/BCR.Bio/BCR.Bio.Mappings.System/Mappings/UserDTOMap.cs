﻿using BCR.Bio.Domain.DTO;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.System.Mappings
{
    public class UserDTOMap : ClassMap<UserDTO>
    {
        public UserDTOMap()
        {
            Table("[User]");

            Id(x => x.Id).Column("Id");
            Map(x => x.Username).Column("username");

            HasManyToMany(x => x.Companies)
                .Cascade.None()
                .Table("CompanyUser")
                .ParentKeyColumn("userid")
                .ChildKeyColumn("company").Not.LazyLoad();
        }
    }
}