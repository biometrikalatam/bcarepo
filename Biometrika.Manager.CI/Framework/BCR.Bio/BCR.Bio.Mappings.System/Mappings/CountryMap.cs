﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class CountryMap : ClassMap<Country>
    {
        public CountryMap()
        {
            Table("Country");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Alpha2).Column("alpha2");
            Map(x => x.Alpha3).Column("alpha3");
            Map(x => x.NumericCode).Column("numericCode");
            Map(x => x.CountryRawName).Column("countryRawName");

            Map(x => x.Description).Column("description");

            HasMany(x => x.Depts)
                .Cascade
                .None()
                .Table("Dept")
                .KeyColumn("idCountry").Not.LazyLoad();   // esto esta mal, se deberia hacer un control q cargue por clase
        }
    }
}