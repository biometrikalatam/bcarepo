﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("[User]");
            DynamicUpdate();
            DynamicInsert();
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Username).Column("username");
            Map(x => x.Email).Column("email");
            Map(x => x.Password).Column("password");
            Map(x => x.IsApproved).Column("isApproved");
            Map(x => x.IsLockedOut).Column("isLockedOut");
            Map(x => x.PasswordSalt).Column("passwordSalt");
            Map(x => x.PasswordFormat).Column("passwordFormat");
            Map(x => x.PasswordQuestion).Column("passwordQuestion");
            Map(x => x.PasswordAnswer).Column("passwordAnswer");
            Map(x => x.FailedPasswordAttemptCount).Column("failedPasswordAttemptCount");
            Map(x => x.FailedPasswordAttemptWindowStart).Column("failedPasswordAttemptWindowStart");
            Map(x => x.FailedPasswordAnswerAttemptCount).Column("failedPasswordAnswerAttemptCount");
            Map(x => x.FailedPasswordAnswerAttemptWindowStart).Column("failedPasswordAnswerAttemptWindowStart");
            Map(x => x.LastPasswordChangedDate).Column("lastPasswordChangedDate");
            Map(x => x.LastActivityDate).Column("lastActivityDate");
            Map(x => x.LastLockOutDate).Column("lastLockOutDate");
            Map(x => x.LastLoginDate).Column("lastLoginDate");
            Map(x => x.Comments).Column("comments");
            Map(x => x.Name).Column("name");
            Map(x => x.Surname).Column("surname");
            Map(x => x.Phone).Column("phone");
            Map(x => x.PostalCode).Column("postalCode");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
            //References(x => x.Employee).Column("employee").Class<Employee>();

            HasManyToMany(x => x.Roles)
                    .Cascade.None()
                    .Table("UserRol")
                    .ParentKeyColumn("userId")
                    .ChildKeyColumn("rolId").Not.LazyLoad();

            HasManyToMany(x => x.Companies)
                    .Cascade.None()
                    .Table("CompanyUser")
                    .ParentKeyColumn("userid")
                    .ChildKeyColumn("company").Not.LazyLoad();
        }
    }
}