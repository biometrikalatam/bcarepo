﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class CityMap : ClassMap<City>//   ClassMapping<City>
    {
        public CityMap()
        {
            Table("City");
            Id(x => x.Id).GeneratedBy.Identity();

            //Id(x => x.Id,
            //    m =>
            //    {
            //        m.Column("Id");
            //        m.Generator(Generators.Native);
            //    });

            Map(x => x.Description).Column("description");
            //Property(x => x.Description,
            //    m =>
            //    {
            //        m.Column("description");
            //    }
            //    );

            References(x => x.Dept, "idDept").Unique();
            //ManyToOne(x => x.Dept,
            //    m=>
            //    {
            //        m.Column("idDept");
            //        m.Unique(true);
            //    }
            //    );

            HasMany(x => x.Towns)
                .Cascade
                .None()
                .Table("Town")
                .KeyColumn("idCity").Not.LazyLoad();   // esto esta mal, se deberia hacer un control q cargue por clase;

            //List<Town>(x => x.Towns,
            //    m=>
            //    {
            //        m.Cascade(Cascade.None);
            //        m.Table("Town");
            //        m.Key(c => c.Column("idCity"));
            //        m.Lazy(CollectionLazy.NoLazy);
            //    });
        }
    }
}