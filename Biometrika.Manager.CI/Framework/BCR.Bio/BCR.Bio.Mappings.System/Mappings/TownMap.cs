﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings
{
    class TownMap: ClassMap<Town>
    {
        public TownMap()
        {

            Table("Town");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");

            References(x => x.City, "idCity").Unique();
        }
    }
}
