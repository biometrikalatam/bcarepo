﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using BCR.System.Type;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using NHibernate.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class VisitorTrackingRepository : Repository<IVisitorTracking>
    {
        public VehicleVisitorTrackingResultDTO GetVehicleBy(VisitorTrackingDTO param)
        {
            IList<VisitorTrackingResultDTO> list = GetBy(param)
                                            .ToList();

            VisitorTrackingResultDTO firstVisitor = list.OrderByDescending(i => i.EventTime)
                                                    .FirstOrDefault();

            if (firstVisitor != null)
            {
                VehicleVisitorTrackingResultDTO resp = new VehicleVisitorTrackingResultDTO
                {
                    Patente = firstVisitor.PlateNumber,
                    Marca = firstVisitor.VMarca,
                    Modelo = firstVisitor.VModelo,
                    Tipo = firstVisitor.VTipo,
                    EventTime = firstVisitor.EventTime,
                    Grupo = firstVisitor.TrackingGuid,
                    ActionDescription = firstVisitor.ActionDescription,
                    ResultDescription = firstVisitor.ActionResult,
                    Color = firstVisitor.VColor,
                    Detalles = firstVisitor.VDetalles
                };

                resp.Integrantes = GetBy(new VisitorTrackingDTO() { Company = param.Company, TrakingGuid = firstVisitor.TrackingGuid, ActionId = 1, PlateNumber = param.PlateNumber }).ToList();
                resp.Integrantes.ForEach(x => x.Incluir = true);

                return resp;
            }

            return null;
        }

        public IList<VisitorTracking> GetByPlateNumber(VisitorTrackingFilter filter)
        {
            if (filter == null || string.IsNullOrEmpty(filter.PlateNumber))
                return null;

            try
            {
                IList<VisitorTracking> list = null;

                Vehicle vehicleAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var subQuery = QueryOver.Of<VisitorTracking>()
                                    .Left.JoinAlias(x => x.Vehicle, () => vehicleAlias)
                                    .Where(x => vehicleAlias.PlateNumber == filter.PlateNumber)
                                    .And(x => x.Company.Id == filter.CompanyId)
                                    .Select(u => u.TrackingGuid);

                    var criteria = session.QueryOver<VisitorTracking>()
                                   .WithSubquery.WhereProperty(x => x.TrackingGuid).In(subQuery)
                                   .OrderBy(x => x.EventTime).Desc;


                    #region Filtros

                    if (filter.TrackingAction != null)
                        criteria.Where(x => x.Action.Id == filter.TrackingAction.Id);

                    #endregion


                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<VisitorTracking>();

                    if (filter.WithTrackingAction)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Action));
                    if (filter.WithEventClass)
                        list.ForEach(x => NHibernateUtil.Initialize(x.EventClass));
                    if (filter.WithUnity)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Unity));
                    if (filter.WithHQ)
                        list.ForEach(x => NHibernateUtil.Initialize(x.HQ));
                    if (filter.WithPoint)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Point));
                    if (filter.WithProvider)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Provider));
                    if (filter.WithResident)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Resident));
                    if (filter.WithResult)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Result));
                    if (filter.WithVisitor)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Visitor));
                    if (filter.WithUser)
                        list.ForEach(x => NHibernateUtil.Initialize(x.User));
                    if (filter.WithEmployee)
                        list.ForEach(x => {
                            if (x.Employee != null)
                            {
                                NHibernateUtil.Initialize(x.Employee);
                                NHibernateUtil.Initialize(x.Employee.Identity);
                            }
                        });

                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByFilter", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IList<VisitorTrackingResultDTO> GetTrackings(VisitorTrackingDTO param)
        {
            if (param.Paginacion == null)
                param.Paginacion = new Paginacion() { Pagina = 0, Cantidad = 1 };
            try
            {
                IList<VisitorTrackingResultDTO> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    //VisitorTracking visitorTracking = null;

                    Visitor visitorAlias = null;
                    BpIdentity identityVisitorAlias = null;
                    Point pointInAlias = null;
                    TrackingAction trackingActionAlias = null;
                    TrackingResult trackingResultAlias = null;
                    //Provider providerAlias = null;
                    //Sector sectorAlias = null;
                    HQ hqAlias = null;
                    Event eventAlias = null;
                    Vehicle vehicleAlias = null;
                    Unity unityAlias = null;
                    EventClass eventClassAlias = null;
                    Resident residentAlias = null;
                    BpIdentity identityResidentAlias = null;
                    Provider providerVisitorTrackingAlias = null;
                    User userAlias = null;

                    var results = session.QueryOver<VisitorTracking>()
                       .Inner.JoinAlias(x => x.Visitor, () => visitorAlias)
                       //.Left.JoinAlias(x => x.Sector, () => sectorAlias)
                       .Left.JoinAlias(x => x.Action, () => trackingActionAlias)
                       .Left.JoinAlias(x => x.Result, () => trackingResultAlias)
                       .Left.JoinAlias(x => x.Event, () => eventAlias)
                       .Left.JoinAlias(x => x.Vehicle, () => vehicleAlias)
                       .Left.JoinAlias(x => x.Unity, () => unityAlias)
                       .Left.JoinAlias(x => x.EventClass, () => eventClassAlias)
                       .Left.JoinAlias(x => x.Resident, () => residentAlias)
                       .Left.JoinAlias(x => x.Provider, () => providerVisitorTrackingAlias)
                       .Left.JoinAlias(x => x.User, () => userAlias)
                       .Left.JoinAlias(() => residentAlias.Identity, () => identityResidentAlias)
                       .Inner.JoinAlias(() => visitorAlias.Identity, () => identityVisitorAlias)
                       .Inner.JoinAlias(x => x.Point, () => pointInAlias)
                       //.Inner.JoinAlias(() => visitorAlias.Provider, () => providerAlias)
                       .Left.JoinAlias(x => x.HQ, () => hqAlias)
                       .Where(x => x.Company.Id == param.Company)
                       .OrderBy(x => x.EventTime).Desc
                       .Select(
                           x => x.Id,
                           x => pointInAlias.Name,
                           x => x.EventTime,
                           x => identityVisitorAlias.TypeId,
                           x => identityVisitorAlias.ValueId,
                           x => visitorAlias.Id,
                           x => identityVisitorAlias.Name,
                           x => identityVisitorAlias.PatherLastname,
                           x => identityVisitorAlias.MotherLastname,
                           x => trackingActionAlias.Description,
                           x => trackingResultAlias.Description,
                           x => x.TrackingGuid,
                           x => trackingResultAlias.Id,
                           x => trackingActionAlias.Id,
                           x => vehicleAlias.PlateNumber,
                           x => unityAlias.Address,
                           x => eventClassAlias.Description,
                           x => identityResidentAlias.Name,
                           x => identityResidentAlias.PatherLastname,
                           x => identityResidentAlias.MotherLastname,
                           x => providerVisitorTrackingAlias.Name,
                           x => userAlias.Username,
                           x => residentAlias.Id,
                           x => unityAlias.Id,
                           x => eventClassAlias.Id,
                           x => identityResidentAlias.TypeId,
                           x => identityResidentAlias.ValueId,
                           x => identityResidentAlias.CompanyIdEnroll.Id,
                           x => x.Company.Id,
                           x => identityVisitorAlias.CompanyIdEnroll.Id,
                           x => identityResidentAlias.BirthDate,
                           x => identityVisitorAlias.BirthDate,
                           x => identityResidentAlias.Sex,
                           x => identityVisitorAlias.Sex,
                           x => vehicleAlias.Id,
                           x => identityResidentAlias.Nationality,
                           x => identityVisitorAlias.Nationality,
                           x => providerVisitorTrackingAlias.Id,
                           x => pointInAlias.Id,
                           x => pointInAlias.Code,
                           x => x.Comment,
                           x => userAlias.Id,
                           x => eventAlias.Description,
                           x => hqAlias.Description,
                           x => eventAlias.Id
                        );

                    results.ReadOnly();
                    if (param.Id != 0)
                        results.And(x => x.Id == param.Id);
                    if (param.VisitorId != null)
                        results.And(x => x.Visitor.Id == param.VisitorId);
                    if (param.VisitorTypeId != null)
                        results.And(x => identityVisitorAlias.TypeId == param.VisitorTypeId);
                    if (param.VisitorValueId != null)
                        results.And(x => identityVisitorAlias.ValueId == param.VisitorValueId.ToString().Trim());
                    if (param.EventTimeFrom != null)
                        results.And(x => x.EventTime >= param.EventTimeFrom);
                    if (param.EventTimeTo != null)
                    {
                        DateTime dateTo = Convert.ToDateTime(param.EventTimeTo);
                        results.And(x => x.EventTime <= dateTo.Date.AddDays(1));
                    }
                    if (!string.IsNullOrEmpty(param.PlateNumber))
                        results.And(x => vehicleAlias.PlateNumber == param.PlateNumber);
                    if (param.Provider != null)
                        results.And(x => providerVisitorTrackingAlias.Id == param.Provider.Id);
                    if (!string.IsNullOrEmpty(param.VisitorName))
                        results.And(x => identityVisitorAlias.Name.IsLike('%' + param.VisitorName.Trim() +'%' ));
                    if (!string.IsNullOrEmpty(param.VisitorLastName))
                        results.And(x => identityVisitorAlias.PatherLastname.IsLike ('%' +  param.VisitorLastName.Trim() +'%' ));

                    if (param.HQId > 0)
                        results.And(x => hqAlias.Id == param.HQId);

                    if (param.HQIdList != null && param.HQIdList.Any())
                        results.WhereRestrictionOn(x => hqAlias.Id).IsIn(param.HQIdList.ToList());

                    if (param.ActionId != 0)
                        results.And(x => trackingActionAlias.Id == param.ActionId);
                    if (param.ResultId > 0)
                        results.And(x => trackingResultAlias.Id == param.ResultId);
                    if (!string.IsNullOrEmpty(param.TrakingGuid))
                        results.And(x => x.TrackingGuid == param.TrakingGuid);
                    if (param.EventId > 0)
                        results.And(x => eventAlias.Id == param.EventId);
                    if (param.EventClassId > 0)
                        results.And(x => eventClassAlias.Id == param.EventClassId);
                    if (param.UserId > 0)
                        results.And(x => userAlias.Id == param.UserId);
                    if (param.UnityId > 0)
                        results.And(x => unityAlias.Id == param.UnityId);
                    if (param.ResidentId > 0)
                        results.And(x => residentAlias.Id == param.ResidentId);
                    if (param.Point > 0)
                        results.And(x => pointInAlias.Id == param.Point);

                    results.Skip(param.Paginacion.Pagina * param.Paginacion.Cantidad);
                    results.Take(param.Paginacion.Cantidad);

                    IList<VisitorTrackingResultDTO> retorno = results.List<object[]>()
                       .Select(p => new VisitorTrackingResultDTO()
                       {
                           Id = ConvertEx.ObjectToInt(p[0]),
                           PointDescription = ConvertEx.ObjectToString(p[1]),
                           EventTime = (DateTime)p[2],
                           VisitorTypeId = ConvertEx.ObjectToString(p[3]),
                           VisitorValueId = ConvertEx.ObjectToString(p[4]),
                           VisitorId = ConvertEx.ObjectToInt(p[5]),
                           VisitorName = ConvertEx.ObjectToString(p[6]),
                           VisitorPatherLastname = ConvertEx.ObjectToString(p[7]),
                           VisitorMotherLastname = ConvertEx.ObjectToString(p[8]),
                           ActionDescription = ConvertEx.ObjectToString(p[9]),
                           ActionResult = ConvertEx.ObjectToString(p[10]),
                           TrackingGuid = ConvertEx.ObjectToString(p[11]),
                           ResultId = ConvertEx.ObjectToInt(p[12]),
                           TrackingActionId = ConvertEx.ObjectToInt(p[13]),
                           PlateNumber = ConvertEx.ObjectToString(p[14]),
                           Destination = ConvertEx.ObjectToString(p[15]),
                           EventClassDescription = ConvertEx.ObjectToString(p[16]),
                           ResidentName = ConvertEx.ObjectToString(p[17]),
                           ResidentPatherLastname = ConvertEx.ObjectToString(p[18]),
                           ResidentMotherLastname = ConvertEx.ObjectToString(p[19]),
                           ProviderVisitorTrackingName = ConvertEx.ObjectToString(p[20]),
                           EmployeeDescription = ConvertEx.ObjectToString(p[21]),
                           ResidentId = ConvertEx.ObjectToInt(p[22]),
                           UnityId = ConvertEx.ObjectToInt(p[23]),
                           EventClassId = ConvertEx.ObjectToInt(p[24]),
                           ResidentTypeId = ConvertEx.ObjectToString(p[25]),
                           ResidentValueId = ConvertEx.ObjectToString(p[26]),
                           ResidentCompanyId = ConvertEx.ObjectToInt(p[27]),
                           CompanyId = ConvertEx.ObjectToInt(p[28]),
                           VisitorCompanyId = ConvertEx.ObjectToInt(p[29]),
                           ResidentBirthDate = ConvertEx.ObjectToDateTime(p[30]),
                           VisitorBirthDate = ConvertEx.ObjectToDateTime(p[31]),
                           ResidentSex = ConvertEx.ObjectToString(p[32]),
                           VisitorSex = ConvertEx.ObjectToString(p[33]),
                           VehicleId = ConvertEx.ObjectToInt(p[34]),
                           ResidentNationality = ConvertEx.ObjectToString(p[35]),
                           VisitorNationality = ConvertEx.ObjectToString(p[36]),
                           ProviderId = ConvertEx.ObjectToInt(p[37]),
                           PointId = ConvertEx.ObjectToInt(p[38]),
                           PointCode = ConvertEx.ObjectToString(p[39]),
                           Comentario = ConvertEx.ObjectToString(p[40]),
                           EmployeeId = ConvertEx.ObjectToInt(p[41]),
                           Event = new Event
                           {
                               Description = ConvertEx.ObjectToString(p[42]),
                               Id = ConvertEx.ObjectToInt(p[44])
                           },
                           HQDescription = ConvertEx.ObjectToString(p[43])

                       }).ToList();

                    param.Paginacion.Total = results.RowCount();
                    list = retorno;
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetUsersByCompany", e);
                return null;
            }
        }

        public IList<VisitorTrackingResultDTO> GetOpenTrackings(VisitorTrackingDTO param)
        {
            if (param.Paginacion == null)
                param.Paginacion = new Paginacion() { Pagina = 0, Cantidad = 1 };

            try
            {
                IList<VisitorTrackingResultDTO> list = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Visitor visitorAlias = null;
                    BpIdentity identityVisitorAlias = null;
                    Point pointInAlias = null;
                    TrackingAction trackingActionAlias = null;
                    TrackingResult trackingResultAlias = null;
                    //Provider providerAlias = null;
                    //Sector sectorAlias = null;
                    //HQ hqAlias = null;
                    Event eventAlias = null;
                    Vehicle vehicleAlias = null;
                    Unity unityAlias = null;
                    EventClass eventClassAlias = null;
                    Resident residentAlias = null;
                    BpIdentity identityResidentAlias = null;
                    Provider providerVisitorTrackingAlias = null;
                    User userAlias = null;
                    VisitorTracking visitorTrackingAlias = null;
                    Employee employeeAlias = null;
                    BpIdentity identityEmployeeAlias = null;
                    CounterTracking result = null;

                   
                    VisitorTracking vt=null;

                    //Query que trae todos aquellos tracking que no tienen salida
                    //IList<CounterTracking>tracktotal = session.QueryOver<VisitorTracking>(() => vt)
                    //   .SelectList(lista => lista
                    //        .Select(() => vt.TrackingGuid).WithAlias(() => result.trackingGuid)
                    //        .Select(
                    //                 Projections.GroupProperty(
                    //                     Projections.Property<VisitorTracking>(t => t.TrackingGuid)
                    //                 )
                    //               )
                    //   )

                    //.Where(Restrictions.Eq(Projections.Count<VisitorTracking>(t => t.TrackingGuid), 1))
                    //.TransformUsing((Transformers.AliasToBean<CounterTracking>()))
                    //.List<CounterTracking>();

                    if (param.EventTimeFrom == null) param.EventTimeFrom = DateTime.Now.AddDays(-180);

                    var total =
                       session.QueryOver<VisitorTracking>(() => vt)
                       .Where(x => x.Company.Id == 7)
                       .And(x => x.EventTime >= param.EventTimeFrom)
                       .WithSubquery.WhereProperty(x => x.TrackingGuid).In(
                           QueryOver.Of<VisitorTracking>(() => vt)
                         .SelectList(lista => lista
                              //.Select(() => vt.TrackingGuid).WithAlias(() => result.trackingGuid)
                              .Select(
                                       Projections.GroupProperty(
                                           Projections.Property<VisitorTracking>(t => t.TrackingGuid)
                                       )
                                     )
                         )

                      .Where(Restrictions.Eq(Projections.Count<VisitorTracking>(t => t.TrackingGuid), 1))

                           ).List<VisitorTracking>();



                    var results = session.QueryOver<VisitorTracking>(() => visitorTrackingAlias)
                        .WithSubquery.WhereProperty(x => x.TrackingGuid).NotIn
                        (
                            QueryOver.Of<VisitorTracking>()
                            .Where(vt2 => vt2.Action.Id == 2)
                            .And(vt2 => vt2.Company.Id == param.Company)
                            .Select(vt2 => vt2.TrackingGuid)
                        )

                       .Inner.JoinAlias(x => x.Visitor, () => visitorAlias)
                        .Left.JoinAlias(x => x.Action, () => trackingActionAlias)
                       .Left.JoinAlias(x => x.Result, () => trackingResultAlias)
                       .Left.JoinAlias(x => x.Event, () => eventAlias)
                       .Left.JoinAlias(x => x.Vehicle, () => vehicleAlias)
                       .Left.JoinAlias(x => x.Unity, () => unityAlias)
                       .Left.JoinAlias(x => x.EventClass, () => eventClassAlias)

                       //.Left.JoinAlias(x => x.Resident, () => residentAlias)
                       .Left.JoinAlias(x => x.Employee, () => employeeAlias)

                       .Left.JoinAlias(x => x.Provider, () => providerVisitorTrackingAlias)
                       .Left.JoinAlias(x => x.User, () => userAlias)

                       //.Inner.JoinAlias(() => residentAlias.Identity, () => identityResidentAlias)
                       .Left.JoinAlias(() => employeeAlias.Identity, () => identityEmployeeAlias)

                       .Inner.JoinAlias(() => visitorAlias.Identity, () => identityVisitorAlias)
                       .Inner.JoinAlias(x => x.Point, () => pointInAlias)
                        .Where(x => x.Company.Id == param.Company)
                       .And(x => x.Action.Id == 1)
                       //.And( () => VisitorTrackingAlias == null)
                       .OrderBy(x => x.EventTime).Desc
                       .Select(
                           x => x.Id,
                           x => pointInAlias.Name,
                           x => x.EventTime,
                           x => identityVisitorAlias.TypeId,
                           x => identityVisitorAlias.ValueId,
                           x => visitorAlias.Id,
                           x => identityVisitorAlias.Name,
                           x => identityVisitorAlias.PatherLastname,
                           x => identityVisitorAlias.MotherLastname,
                           x => trackingActionAlias.Description,
                           x => trackingResultAlias.Description,
                           x => x.TrackingGuid,
                           x => trackingResultAlias.Id,
                           x => trackingActionAlias.Id,
                           x => vehicleAlias.PlateNumber,
                           x => unityAlias.Address,
                           x => eventClassAlias.Description,
                           //x => identityResidentAlias.Name,
                           //x => identityResidentAlias.PatherLastname,
                           //x => identityResidentAlias.MotherLastname,
                           x => identityEmployeeAlias.Name,
                           x => identityEmployeeAlias.PatherLastname,
                           x => identityEmployeeAlias.MotherLastname,
                           x => providerVisitorTrackingAlias.Name,
                           x => userAlias.Username,
                           x => x.Comment
                        );
                    results.ReadOnly();

                    if (param.Id != 0)
                        results.And(x => x.Id == param.Id);
                    if (param.VisitorId != null)
                        results.And(x => x.Visitor.Id == param.VisitorId);
                    if (param.VisitorTypeId != null)
                        results.And(x => identityVisitorAlias.TypeId == param.VisitorTypeId);
                    if (param.VisitorValueId != null)
                        results.And(x => identityVisitorAlias.ValueId == param.VisitorValueId.ToString().Trim());
                    if (param.EventTimeFrom != null)
                        results.And(x => x.EventTime >= param.EventTimeFrom);
                    if (param.EventTimeTo != null)
                    {
                        DateTime dateTo = Convert.ToDateTime(param.EventTimeTo);
                        results.And(x => x.EventTime <= dateTo.Date.AddDays(1));
                    }
                    if (!string.IsNullOrEmpty(param.PlateNumber))
                        results.And(x => vehicleAlias.PlateNumber == param.PlateNumber);
                    if (param.Provider != null)
                        results.And(x => providerVisitorTrackingAlias.Id == param.Provider.Id);
                    if (!string.IsNullOrEmpty(param.VisitorName))
                        results.And(x => identityVisitorAlias.Name == param.VisitorName.Trim());
                    if (!string.IsNullOrEmpty(param.VisitorLastName))
                        results.And(x => identityVisitorAlias.PatherLastname == param.VisitorLastName.Trim());

                    if (param.ActionId != 0)
                        results.And(x => trackingActionAlias.Id == param.ActionId);
                    if (param.ResultId > 0)
                        results.And(x => trackingResultAlias.Id == param.ResultId);
                    if (!string.IsNullOrEmpty(param.TrakingGuid))
                        results.And(x => x.TrackingGuid == param.TrakingGuid);
                    if (param.EventId > 0)
                        results.And(x => eventAlias.Id == param.EventId);
                    if (param.EventClassId > 0)
                        results.And(x => eventClassAlias.Id == param.EventClassId);
                    if (param.UserId > 0)
                        results.And(x => userAlias.Id == param.UserId);

                    if (param.HQIdList != null && param.HQIdList.Any())
                        results.WhereRestrictionOn(x => x.HQ).IsIn(param.HQIdList.ToList());

                    results.Skip(param.Paginacion.Pagina * param.Paginacion.Cantidad);
                    results.Take(param.Paginacion.Cantidad);

                    results.UnderlyingCriteria.SetTimeout(120000); // TODO: Hay que ver bien esto, esto aprcha el problema, pero mientras mayor sea la cantidad de datos mayor sera el timeout necesario

                    IList<VisitorTrackingResultDTO> retorno = results.List<object[]>()
                       .Select(p => new VisitorTrackingResultDTO()
                       {
                           Id = ConvertEx.ObjectToInt(p[0]),
                           PointDescription = ConvertEx.ObjectToString(p[1]),
                           EventTime = (DateTime)p[2],
                           VisitorTypeId = ConvertEx.ObjectToString(p[3]),
                           VisitorValueId = ConvertEx.ObjectToString(p[4]),
                           VisitorId = ConvertEx.ObjectToInt(p[5]),
                           VisitorName = ConvertEx.ObjectToString(p[6]),
                           VisitorPatherLastname = ConvertEx.ObjectToString(p[7]),
                           VisitorMotherLastname = ConvertEx.ObjectToString(p[8]),
                           ActionDescription = ConvertEx.ObjectToString(p[9]),
                           ActionResult = ConvertEx.ObjectToString(p[10]),
                           TrackingGuid = ConvertEx.ObjectToString(p[11]),
                           ResultId = ConvertEx.ObjectToInt(p[12]),
                           TrackingActionId = ConvertEx.ObjectToInt(p[13]),
                           PlateNumber = ConvertEx.ObjectToString(p[14]),
                           Destination = ConvertEx.ObjectToString(p[15]),
                           EventClassDescription = ConvertEx.ObjectToString(p[16]),
                           ResidentName = ConvertEx.ObjectToString(p[17]),
                           ResidentPatherLastname = ConvertEx.ObjectToString(p[18]),
                           ResidentMotherLastname = ConvertEx.ObjectToString(p[19]),
                           ProviderVisitorTrackingName = ConvertEx.ObjectToString(p[20]),
                           EmployeeDescription = ConvertEx.ObjectToString(p[21]),
                           Comentario = ConvertEx.ObjectToString(p[22])
                       }).ToList();

                    try
                    {
                        //param.Paginacion.Total = results.RowCount();
                        param.Paginacion.Total = total.Count();
                    }
                    catch (Exception e)
                    {
                        param.Paginacion.Total = 400;
                    }
                    
                    list = retorno;
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetUsersByCompany", e);
                return null;
            }
        }

        public IList<VisitorTrackingResultDTO> GetBy(VisitorTrackingDTO param) // es absurdo tener dos metodos que hacen lo mismo, nos quedamos con GetTrackings?
        {
            try
            {
                IList<VisitorTrackingResultDTO> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    //VisitorTracking visitorTracking = null;

                    Visitor visitorAlias = null;
                    BpIdentity identityVisitorAlias = null;
                    Point pointInAlias = null;
                    TrackingAction trackingActionAlias = null;
                    TrackingResult trackingResultAlias = null;
                    //Provider providerAlias = null;
                    //Sector sectorAlias = null;
                    //HQ hqAlias = null;
                    Event eventAlias = null;
                    Vehicle vehicleAlias = null;
                    Unity unityAlias = null;
                    EventClass eventClassAlias = null;
                    Resident residentAlias = null;
                    BpIdentity identityResidentAlias = null;
                    Provider providerVisitorTrackingAlias = null;
                    User userAlias = null;

                    var results = session.QueryOver<VisitorTracking>()
                       .Inner.JoinAlias(x => x.Visitor, () => visitorAlias)
                       //.Left.JoinAlias(x => x.Sector, () => sectorAlias)
                       .Left.JoinAlias(x => x.Action, () => trackingActionAlias)
                       .Left.JoinAlias(x => x.Result, () => trackingResultAlias)
                       .Left.JoinAlias(x => x.Event, () => eventAlias)
                       .Left.JoinAlias(x => x.Vehicle, () => vehicleAlias)
                       .Left.JoinAlias(x => x.Unity, () => unityAlias)
                       .Left.JoinAlias(x => x.EventClass, () => eventClassAlias)
                       .Left.JoinAlias(x => x.Resident, () => residentAlias)
                       .Left.JoinAlias(x => x.Provider, () => providerVisitorTrackingAlias)
                       .Left.JoinAlias(x => x.User, () => userAlias)
                       .Inner.JoinAlias(() => residentAlias.Identity, () => identityResidentAlias)
                       .Inner.JoinAlias(() => visitorAlias.Identity, () => identityVisitorAlias)
                       .Inner.JoinAlias(x => x.Point, () => pointInAlias)
                       //.Inner.JoinAlias(() => visitorAlias.Provider, () => providerAlias)
                       //.Inner.JoinAlias(x => x.HQ, () => hqAlias)
                       .Where(x => x.Company.Id == param.Company)
                       .OrderBy(x => x.EventTime).Desc
                       .Select(
                           x => x.Id,
                           x => pointInAlias.Name,
                           x => x.EventTime,
                           x => identityVisitorAlias.TypeId,
                           x => identityVisitorAlias.ValueId,
                           x => visitorAlias.Id,
                           x => identityVisitorAlias.Name,
                           x => identityVisitorAlias.PatherLastname,
                           x => identityVisitorAlias.MotherLastname,
                           x => trackingActionAlias.Description,
                           x => trackingResultAlias.Description,
                           x => x.TrackingGuid,
                           x => trackingResultAlias.Id,
                           //x => providerAlias.Name,
                           //x => sectorAlias.Description,
                           x => trackingActionAlias.Id,
                           //x => hqAlias.Id,
                           //x => hqAlias.Description
                           x => vehicleAlias.PlateNumber,
                           x => unityAlias.Address,
                           x => eventClassAlias.Description,
                           x => identityResidentAlias.Name,
                           x => identityResidentAlias.PatherLastname,
                           x => identityResidentAlias.MotherLastname,
                           x => providerVisitorTrackingAlias.Name,
                           x => userAlias.Username
                        );
                    results.ReadOnly();
                    if (param.Id != 0)
                        results.And(x => x.Id == param.Id);
                    if (param.VisitorId != null)
                        results.And(x => x.Visitor.Id == param.VisitorId);
                    if (param.VisitorTypeId != null)
                        results.And(x => identityVisitorAlias.TypeId == param.VisitorTypeId);
                    if (param.VisitorValueId != null)
                        results.And(x => identityVisitorAlias.ValueId == param.VisitorValueId.ToString().Trim());
                    if (param.EventTimeFrom != null)
                        results.And(x => x.EventTime >= param.EventTimeFrom);
                    if (param.EventTimeTo != null)
                    {
                        DateTime dateTo = Convert.ToDateTime(param.EventTimeTo);
                        results.And(x => x.EventTime <= dateTo.Date.AddDays(1));
                    }
                    if (!string.IsNullOrEmpty(param.PlateNumber))
                        results.And(x => vehicleAlias.PlateNumber == param.PlateNumber);
                    if (param.Provider != null)
                        results.And(x => providerVisitorTrackingAlias.Id == param.Provider.Id);
                    if (!string.IsNullOrEmpty(param.VisitorName))
                        results.And(x => identityVisitorAlias.Name == param.VisitorName.Trim());
                    if (!string.IsNullOrEmpty(param.VisitorLastName))
                        results.And(x => identityVisitorAlias.PatherLastname == param.VisitorLastName.Trim());

                    //if (param.HQId > 0)
                    //    results.And(x => hqAlias.Id == param.HQId);
                    if (param.ActionId != 0)
                        results.And(x => trackingActionAlias.Id == param.ActionId);
                    if (param.ResultId > 0)
                        results.And(x => trackingResultAlias.Id == param.ResultId);
                    if (!string.IsNullOrEmpty(param.TrakingGuid))
                        results.And(x => x.TrackingGuid == param.TrakingGuid);
                    if (param.EventId > 0)
                        results.And(x => eventAlias.Id == param.EventId);
                    if (param.EventClassId > 0)
                        results.And(x => eventClassAlias.Id == param.EventClassId);
                    if (param.UserId > 0)
                        results.And(x => userAlias.Id == param.UserId);

                    IList<VisitorTrackingResultDTO> retorno = results.List<object[]>()
                       .Select(p => new VisitorTrackingResultDTO()
                       {
                           Id = ConvertEx.ObjectToInt(p[0]),
                           PointDescription = ConvertEx.ObjectToString(p[1]),
                           EventTime = (DateTime)p[2],
                           VisitorTypeId = ConvertEx.ObjectToString(p[3]),
                           VisitorValueId = ConvertEx.ObjectToString(p[4]),
                           VisitorId = ConvertEx.ObjectToInt(p[5]),
                           VisitorName = ConvertEx.ObjectToString(p[6]),
                           VisitorPatherLastname = ConvertEx.ObjectToString(p[7]),
                           VisitorMotherLastname = ConvertEx.ObjectToString(p[8]),
                           ActionDescription = ConvertEx.ObjectToString(p[9]),
                           ActionResult = ConvertEx.ObjectToString(p[10]),
                           TrackingGuid = ConvertEx.ObjectToString(p[11]),
                           ResultId = ConvertEx.ObjectToInt(p[12]),
                           //ProviderName = ConvertEx.ObjectToString(p[13]),

                           //SectorName = ConvertEx.ObjectToString(p[14]),
                           TrackingActionId = ConvertEx.ObjectToInt(p[13]),  //,
                           //HQId = ConvertEx.ObjectToInt(p[16]),
                           //HQDescription = ConvertEx.ObjectToString(p[17])
                           PlateNumber = ConvertEx.ObjectToString(p[14]),
                           Destination = ConvertEx.ObjectToString(p[15]),
                           EventClassDescription = ConvertEx.ObjectToString(p[16]),
                           ResidentName = ConvertEx.ObjectToString(p[17]),
                           ResidentPatherLastname = ConvertEx.ObjectToString(p[18]),
                           ResidentMotherLastname = ConvertEx.ObjectToString(p[19]),
                           ProviderVisitorTrackingName = ConvertEx.ObjectToString(p[20]),
                           EmployeeDescription = ConvertEx.ObjectToString(p[21])
                       }).ToList();

                    list = retorno;
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetUsersByCompany", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IList<VisitorTrackingExtDTO> GetLastTracks(BaseFilterParam param)
        {
            try
            {
                IList<VisitorTrackingExtDTO> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Visitor visitorAlias = null;
                    BpIdentity identityVisitorAlias = null;
                    Point pointInAlias = null;
                    TrackingAction trackingActionAlias = null;
                    TrackingResult trackingResultAlias = null;

                    var results = session.QueryOver<VisitorTracking>()
                       .Inner.JoinAlias(x => x.Visitor, () => visitorAlias)
                        .Left.JoinAlias(x => x.Action, () => trackingActionAlias)
                       .Left.JoinAlias(x => x.Result, () => trackingResultAlias)
                       .Inner.JoinAlias(() => visitorAlias.Identity, () => identityVisitorAlias)
                       .Inner.JoinAlias(x => x.Point, () => pointInAlias)
                       .Where(x => x.Company.Id == param.Company.Id)
                       .And(() => trackingResultAlias.Description == "Anomalía" || trackingResultAlias.Description == "Autorizado" || trackingResultAlias.Description == "Carnet Vencido")
                       .Select(
                           x => x.Id,
                           x => pointInAlias.Name,
                           x => x.EventTime,

                           x => identityVisitorAlias.TypeId,
                           x => identityVisitorAlias.ValueId,
                           x => visitorAlias.Id,

                           x => identityVisitorAlias.Name,
                           x => identityVisitorAlias.PatherLastname,

                           x => trackingActionAlias.Description,
                           x => trackingActionAlias.Label,

                           x => trackingResultAlias.Description
                        );

                    results.ReadOnly();

                    if (param.Id > 0)
                        results.And(x => x.Id != param.Id);

                    if (param.TypeID != null)
                        results.And(x => identityVisitorAlias.TypeId == param.TypeID);
                    if (param.ValueID != null)
                        results.And(x => identityVisitorAlias.ValueId == param.ValueID.ToString());
                    //if (param.EventTimeFrom != null)
                    //    results.And(x => x.EventTime >= param.EventTimeFrom);
                    //if (param.EventTimeTo != null)
                    //    results.And(x => x.EventTime <= param.EventTimeTo);
                    //if (param.Provider != null)
                    //    results.And(x => visitorAlias.Provider.Id == param.Provider.Id);

                    results.OrderBy(x => x.EventTime).Desc();
                    results.Take(5);

                    IList<VisitorTrackingExtDTO> retorno = results.List<object[]>()
                       .Select(p => new VisitorTrackingExtDTO()
                       {
                           Id = ConvertEx.ObjectToInt(p[0]),
                           PointDescription = ConvertEx.ObjectToString(p[1]),
                           EventTime = (DateTime)(p[2]),

                           VisitorTypeId = ConvertEx.ObjectToString(p[3]),
                           VisitorValueId = ConvertEx.ObjectToString(p[4]),
                           VisitorId = ConvertEx.ObjectToInt(p[5]),

                           VisitorName = ConvertEx.ObjectToString(p[6]),
                           VisitorPatherLastname = ConvertEx.ObjectToString(p[7]),

                           ActionDescription = ConvertEx.ObjectToString(p[8]),
                           ActionLabel = ConvertEx.ObjectToString(p[9]),

                           ActionResult = ConvertEx.ObjectToString(p[10])
                       }
                       ).ToList();

                    list = retorno;
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetLastTracks", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IList<VisitorTrackingExtDTO> GetByFilter(BaseFilterParam param)
        {
            try
            {
                IList<VisitorTrackingExtDTO> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Visitor visitorAlias = null;
                    BpIdentity identityVisitorAlias = null;
                    Point pointInAlias = null;
                    TrackingAction trackingActionAlias = null;
                    TrackingResult trackingResultAlias = null;
                    //Sector sectorAlias = null;

                    var results = session.QueryOver<VisitorTracking>()
                       .Inner.JoinAlias(x => x.Visitor, () => visitorAlias)
                       //.Left.JoinAlias(x => x.Sector, () => sectorAlias)
                       .Left.JoinAlias(x => x.Action, () => trackingActionAlias)
                       .Left.JoinAlias(x => x.Result, () => trackingResultAlias)
                       .Inner.JoinAlias(() => visitorAlias.Identity, () => identityVisitorAlias)
                       .Inner.JoinAlias(x => x.Point, () => pointInAlias)
                       .Where(x => x.Company.Id == param.Company.Id)
                       .Select(
                           x => x.Id,
                           x => pointInAlias.Name,
                           x => x.EventTime,

                           x => identityVisitorAlias.TypeId,
                           x => identityVisitorAlias.ValueId,
                           x => visitorAlias.Id,

                           x => identityVisitorAlias.Name,
                           x => identityVisitorAlias.PatherLastname,

                           x => trackingActionAlias.Description,
                           x => trackingActionAlias.Label,

                           x => trackingResultAlias.Description
                           //x => sectorAlias.Description
                       );

                    if (param.TypeID != null)
                        results.And(x => identityVisitorAlias.TypeId == param.TypeID);
                    if (param.ValueID != null)
                        results.And(x => identityVisitorAlias.ValueId == param.ValueID.ToString());
                    if (param.EventTimeFrom != null)
                        results.And(x => x.EventTime >= param.EventTimeFrom);
                    if (param.EventTimeTo != null)
                        results.And(x => x.EventTime <= param.EventTimeTo);
                    //if (param.Provider != null)
                    //    results.And(x => visitorAlias.Provider.Id == param.Provider.Id);
                    //if (param.ActionId != 0)
                    //    results.And(x => trackingActionAlias.Id == param.ActionId);
                    //if (param.ResultId != 0)
                    //    results.And(x => trackingResultAlias.Id == param.ResultId);
                    //if (!string.IsNullOrEmpty(param.TrakingGuid))
                    //    results.And(x => x.TrackingGuid == param.TrakingGuid);

                    results.ReadOnly();

                    IList<VisitorTrackingExtDTO> retorno = results.List<object[]>()
                       .Select(p => new VisitorTrackingExtDTO()
                       {
                           Id = ConvertEx.ObjectToInt(p[0]),
                           PointDescription = ConvertEx.ObjectToString(p[1]),
                           EventTime = (DateTime)(p[2]),

                           VisitorTypeId = ConvertEx.ObjectToString(p[3]),
                           VisitorValueId = ConvertEx.ObjectToString(p[4]),
                           VisitorId = ConvertEx.ObjectToInt(p[5]),

                           VisitorName = ConvertEx.ObjectToString(p[6]),
                           VisitorPatherLastname = ConvertEx.ObjectToString(p[7]),

                           ActionDescription = ConvertEx.ObjectToString(p[8]),
                           ActionLabel = ConvertEx.ObjectToString(p[9]),

                           ActionResult = ConvertEx.ObjectToString(p[10]),

                           SectorName = ConvertEx.ObjectToString(p[11])
                       }
                       ).ToList();

                    list = retorno;
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByFilter", e);
                return null;
            }
        }

        public IList<VisitorPerHour> VisitorPerHour(DateTime dia, int companyId)
        {
            IList<VisitorPerHour> visitorPerHour = null;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    visitorPerHour = session.CreateSQLQuery("exec [p_VisitorPerHour] :Day, :CompanyId")
                    .AddEntity(typeof(VisitorPerHour))
                    .SetParameter("Day", dia)
                    .SetParameter("CompanyId", companyId)
                    .List<VisitorPerHour>();
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar VisitorPerHour", e);
                return null;
            }

            return visitorPerHour;
        }

        public bool IsDuplicateTracking(string valueId, DateTime fecha)
        {
            if (string.IsNullOrEmpty(valueId))
                return true;

            try
            {
                Visitor visitorAlias = null;
                BpIdentity identityAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<VisitorTracking>()
                                    .Left.JoinAlias(x => x.Visitor, () => visitorAlias)
                                        .Left.JoinAlias(x => visitorAlias.Identity, () => identityAlias);

                    criteria.Where(x => identityAlias.ValueId == valueId)
                        .And(x => x.EventTime == fecha);

                    VisitorTracking visitorTracking = criteria.List<VisitorTracking>().FirstOrDefault();

                    if (visitorTracking == null)
                        return false;
                    return true;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EmployeeTrackingRepository.IsDuplicateTracking", e);
                return true;
            }
        }

        public IList<VisitorTracking> GetByFilter(VisitorTrackingFilter filter)
        {
            if (filter == null)
                return null;

            try
            {
                IList<VisitorTracking> list = null;

                Vehicle vehicleAlias = null;
                Visitor visitorAlias = null;
                BpIdentity bpIdentityAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var subQuery = QueryOver.Of<VisitorTracking>()
                                    .Inner.JoinAlias(x => x.Visitor, () => visitorAlias)
                                        .Inner.JoinAlias(x => visitorAlias.Identity, () => bpIdentityAlias)
                                    .Left.JoinAlias(x => x.Vehicle, () => vehicleAlias)
                                    .And(x => x.Company.Id == filter.CompanyId)
                                    .Select(x => x.Id);

                    if (filter.Desde != null)
                        subQuery.Where(x => x.EventTime > filter.Desde.Value.Date);
                    if (filter.Hasta != null)
                        subQuery.Where(x => x.EventTime < filter.Hasta.Value.Date.AddDays(1));

                    if (filter.Unity != null)
                    {
                        if (filter.Unity.Id > 0)
                            subQuery.Where(x => x.Unity.Id == filter.Unity.Id);
                    }

                    if (filter.ResidentAutorizador != null)
                    {
                        if (filter.ResidentAutorizador.Id > 0)
                            subQuery.Where(x => x.Resident.Id == filter.ResidentAutorizador.Id);
                    }

                    if (filter.EmployeeAutorizador != null)
                    {
                        if (filter.EmployeeAutorizador.Id > 0)
                            subQuery.Where(x => x.Employee.Id == filter.EmployeeAutorizador.Id);
                    }

                    if (!string.IsNullOrEmpty(filter.TypeId))
                        subQuery.Where(x => bpIdentityAlias.TypeId == filter.TypeId);
                    if (!string.IsNullOrEmpty(filter.ValueId))
                        subQuery.Where(x => bpIdentityAlias.ValueId == filter.ValueId);
                    if (!string.IsNullOrEmpty(filter.NameLike))
                        subQuery.WhereRestrictionOn(x => bpIdentityAlias.Name).IsLike(filter.NameLike + "%");
                    if (!string.IsNullOrEmpty((filter.FatherLastnameLike)))
                        subQuery.WhereRestrictionOn(x => bpIdentityAlias.PatherLastname).IsLike(filter.FatherLastnameLike + "%");
                    if (!string.IsNullOrEmpty(filter.PlateNumber))
                        subQuery.Where(x => vehicleAlias.PlateNumber == filter.PlateNumber);

                    if (filter.TrackingAction != null)
                    {
                        if (filter.TrackingAction.Id > 0)
                            subQuery.Where(x => x.Action.Id == filter.TrackingAction.Id);
                    }

                    if (filter.EventClass != null)
                    {
                        if (filter.EventClass.Id > 0)
                            subQuery.Where(x => x.EventClass.Id == filter.EventClass.Id);
                    }

                    if (filter.TrackingResult != null)
                    {
                        if (filter.TrackingResult.Id > 0)
                            subQuery.Where(x => x.Result.Id == filter.TrackingResult.Id);
                    }

                    if (filter.Provider != null)
                    {
                        if (filter.Provider.Id > 0)
                            subQuery.Where(x => x.Provider.Id == filter.Provider.Id);
                    }

                    if (filter.User != null)
                    {
                        if (filter.User.Id > 0)
                            subQuery.Where(x => x.User.Id == filter.User.Id);
                    }

                    if (filter.Event != null)
                    {
                        if (filter.Event.Id > 0)
                            subQuery.Where(x => x.Event.Id == filter.Event.Id);
                    }

                    if (filter.HqList != null && filter.HqList.Any())
                    {
                        subQuery.WhereRestrictionOn(x => x.HQ.Id).IsIn(filter.HqList.Select(h => h.Id).ToList());
                    }
                    else
                    {
                        if (filter.Hq != null)
                        {
                            if (filter.Hq.Id > 0)
                                subQuery.Where(x => x.HQ.Id == filter.Hq.Id);
                        }
                    }


                    var criteria = session.QueryOver<VisitorTracking>()
                                   .WithSubquery.WhereProperty(x => x.Id).In(subQuery)
                                   .OrderBy(x => x.EventTime).Desc;

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<VisitorTracking>();

                    list.ForEach(x => 
                    {
                        if (filter.WithTrackingAction)
                            NHibernateUtil.Initialize(x.Action);
                        if (filter.WithEventClass)
                            NHibernateUtil.Initialize(x.EventClass);
                        if (filter.WithHQ)
                            NHibernateUtil.Initialize(x.HQ);
                        if (filter.WithPoint)
                            NHibernateUtil.Initialize(x.Point);
                        if (filter.WithProvider)
                            NHibernateUtil.Initialize(x.Provider);
                        if (filter.WithResident)
                        {
                            if (x.Resident != null)
                            {
                                NHibernateUtil.Initialize(x.Resident);
                                NHibernateUtil.Initialize(x.Resident.Identity);
                            }
                        }
                        if (filter.WithResult)
                            NHibernateUtil.Initialize(x.Result);
                        if (filter.WithVisitor)
                            NHibernateUtil.Initialize(x.Visitor);
                        if (filter.WithUser)
                            NHibernateUtil.Initialize(x.User);
                        if (filter.WithVehicle)
                            NHibernateUtil.Initialize(x.Vehicle);
                        if (filter.WithEmployee)
                        {
                            if (x.Employee != null)
                            {
                                NHibernateUtil.Initialize(x.Employee);
                                NHibernateUtil.Initialize(x.Employee.Identity);
                            }
                        }
                        if (filter.WithEvent)
                            NHibernateUtil.Initialize(x.Event);
                        if (filter.WithUnity)
                            NHibernateUtil.Initialize(x.Unity);
                    });
                    //if (filter.WithTrackingAction)
                    //    list.ForEach(x => NHibernateUtil.Initialize(x.Action));
                    //if (filter.WithEventClass)
                    //    list.ForEach(x => NHibernateUtil.Initialize(x.EventClass));
                    //if (filter.WithHQ)
                    //    list.ForEach(x => NHibernateUtil.Initialize(x.HQ));
                    //if (filter.WithPoint)
                    //    list.ForEach(x => NHibernateUtil.Initialize(x.Point));
                    //if (filter.WithProvider)
                    //    list.ForEach(x => NHibernateUtil.Initialize(x.Provider));
                    //if (filter.WithResident)
                    //    list.ForEach(x => NHibernateUtil.Initialize(x.Resident));
                    //if (filter.WithResult)
                    //    list.ForEach(x => NHibernateUtil.Initialize(x.Result));
                    //if (filter.WithVisitor)
                    //    list.ForEach(x => NHibernateUtil.Initialize(x.Visitor));
                    //if (filter.WithUser)
                    //    list.ForEach(x => NHibernateUtil.Initialize(x.User));
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByFilter", e);
                return null;
            }
        }
    }
}