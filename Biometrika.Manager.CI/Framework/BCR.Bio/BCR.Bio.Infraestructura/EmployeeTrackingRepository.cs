﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCR.Bio.Domain.DTO.Filter;
using NHibernate.Util;
using NHibernate;
using BCR.Bio.Domain.DTO;
using NHibernate.Criterion;
using BCR.System.Type;

namespace BCR.Bio.Infraestructura
{
    public class EmployeeTrackingRepository : Repository<EmployeeTracking>
    {
        public IList<EmployeeTrackingResultDTO> GetOpenTrackings(EmployeeTrackingFilter param, bool getCasinoMarks = false)
        {
            try
            {
                IList<EmployeeTrackingResultDTO> list = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Employee employeeAlias = null;
                    BpIdentity identityEmployeeAlias = null;
                    Point pointInAlias = null;
                    TrackingAction trackingActionAlias = null;
                    TrackingResult trackingResultAlias = null;
                    Vehicle vehicleAlias = null;
                    Provider providerEmployeeTrackingAlias = null;
                    User userAlias = null;
                    EmployeeTracking employeeTrackingAlias = null;

                    if (param.InitDate == null) param.InitDate = DateTime.Now.AddDays(-180);


                    var results = session.QueryOver<EmployeeTracking>(() => employeeTrackingAlias)
                      .WithSubquery.WhereProperty(x => x.TrackingGuid).NotIn
                        (
                            QueryOver.Of<EmployeeTracking>()
                            .Where(et2 => et2.TrackingAction.Id == 2)
                            .And(et2 => et2.Company.Id == param.CompanyId)
                            .Select(et2 => et2.TrackingGuid)
                        )
                    .Inner.JoinAlias(x => x.Employee, () => employeeAlias)
                    .Left.JoinAlias(x => x.TrackingAction, () => trackingActionAlias)
                    .Left.JoinAlias(x => x.TrackingResult, () => trackingResultAlias)
                    .Left.JoinAlias(x => x.Vehicle, () => vehicleAlias)
                    .Left.JoinAlias(x => x.Provider, () => providerEmployeeTrackingAlias)
                    .Left.JoinAlias(x => x.User, () => userAlias)
                    .Left.JoinAlias(() => employeeAlias.Identity, () => identityEmployeeAlias)

                    .Inner.JoinAlias(x => x.Point, () => pointInAlias)
                        .Where(x => x.Company.Id == param.CompanyId)

                        .OrderBy(x => x.EventTime).Desc
                    ;

                    if (getCasinoMarks)
                        results.And(x => x.TrackingAction.Id == 1 || x.TrackingAction.Id == 6);
                    else
                        results.And(x => x.TrackingAction.Id == 1);

                    results.ReadOnly();

                    IList<EmployeeTrackingResultDTO> retorno = new List<EmployeeTrackingResultDTO>();

                    if (param.Id > 0)
                        results.And(x => x.Id == param.Id);
                    if (param.EmployeeId > 0)
                        results.And(x => x.Employee.Id == param.EmployeeId);
                    if (param.InitDate != null)
                        results.And(x => x.EventTime >= param.InitDate);
                    if (param.EndDate != null)
                    {
                        DateTime dateTo = Convert.ToDateTime(param.EndDate);
                        results.And(x => x.EventTime <= dateTo.Date.AddDays(1));
                    }
                    if (!string.IsNullOrEmpty(param.TypeId))
                        results.And(x => identityEmployeeAlias.TypeId == param.TypeId);
                    if (!string.IsNullOrEmpty(param.ValueId))
                        results.And(x => identityEmployeeAlias.ValueId == param.ValueId);
                    if (!string.IsNullOrEmpty(param.EmployeeName))
                        results.And(x => identityEmployeeAlias.Name == param.EmployeeName.Trim());
                    if (!string.IsNullOrEmpty(param.EmployeePatherlastname))
                        results.And(x => identityEmployeeAlias.PatherLastname == param.EmployeePatherlastname.Trim());
                    if (!string.IsNullOrEmpty(param.EmployeeFullName))
                        results.And(x => identityEmployeeAlias.FullName == param.EmployeeFullName.Trim());
                    if (param.UserId > 0)
                        results.And(x => userAlias.Id == param.UserId);

                    results.UnderlyingCriteria.SetTimeout(120000);
                    results.List().ForEach(el =>
                    {
                        EmployeeTrackingResultDTO empTrackAux = new EmployeeTrackingResultDTO
                        {
                            Id = el.Id,
                            ActionDescription = el.TrackingAction.Description,
                            ActionResult = el.TrackingResult.Description,
                            CompanyId = el.Company.Id,
                            Comentario = el.Comment,
                            EmployeeId = el.Employee.Id,
                            EmployeeName = el.Employee.Identity.Name,
                            EmployeePatherLastname = el.Employee.Identity.PatherLastname,
                            EmployeeMotherLastname = el.Employee.Identity.MotherLastname,
                            EmployeeTypeId = el.Employee.Identity.TypeId,
                            EmployeeValueId = el.Employee.Identity.ValueId,
                            EventTime = el.EventTime,
                            PointCode = el.Point.Code,
                            PointDescription = el.Point.Name,
                            PointId = el.Point.Id,
                            TrackingGuid = el.TrackingGuid,
                            TrackingActionId = el.TrackingAction.Id,
                            ResultId = el.TrackingResult.Id,
                            Temperature = el.Temperature,
                        };

                        if (el.Vehicle != null)
                        {
                            empTrackAux.VehicleId = el.Vehicle.Id;
                            empTrackAux.PlateNumber = el.Vehicle.PlateNumber;
                        }
                        if (el.Provider != null)
                        {
                            empTrackAux.ProviderId = el.Provider.Id;
                            empTrackAux.ProviderName = el.Provider.Name;
                        }

                        retorno.Add(empTrackAux);
                    });

                    list = retorno;
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetOpenTrackings", e);
                return null;
            }
        }

        public bool IsDuplicateTracking(string valueId, DateTime fecha)
        {
            if (string.IsNullOrEmpty(valueId))
                return true;

            try
            {
                Employee employeeAlias = null;
                BpIdentity identityAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<EmployeeTracking>()
                                    .Left.JoinAlias(x => x.Employee, () => employeeAlias)
                                        .Left.JoinAlias(x => employeeAlias.Identity, () => identityAlias);

                    criteria.Where(x => identityAlias.ValueId == valueId)
                        .And(x => x.EventTime == fecha);

                    EmployeeTracking employeeTracking = criteria.List<EmployeeTracking>()
                                                        .FirstOrDefault();

                    if (employeeTracking != null)
                        return true;

                    return false;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EmployeeTrackingRepository.IsDuplicateTracking", e);
                return true;
            }
        }

        public IList<EmployeeTracking> GetByFilter(EmployeeTrackingFilter filter)
        {
            if (filter == null)
                return null;

            try
            {
                IList<EmployeeTracking> list = null;
                TrackingAction trackingActionAlias = null;
                Employee employeeAlias = null;
                CentroDeCostos centroDeCostosAlias = null;
                BpIdentity identityAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    if (filter.EndDate == null)
                        filter.EndDate = DateTime.Now;

                    var criteria = session.QueryOver<EmployeeTracking>()
                                    .Left.JoinAlias(x => x.Employee, () => employeeAlias)
                                        .Left.JoinAlias(x => employeeAlias.Identity, () => identityAlias)
                                    .Where(x => x.Company.Id == filter.CompanyId)
                                    .OrderBy(x => x.EventTime).Desc;


                    if (filter.Id > 0)
                        criteria.Where(x => x.Id == filter.Id);
                    if (filter.InitDate != null)
                        criteria.Where(x => x.EventTime >= filter.InitDate);
                    if (filter.EndDate != null)
                        criteria.Where(x => x.EventTime <= filter.EndDate);
                    if (filter.EmployeeId > 0)
                        criteria.Where(x => x.Employee.Id == filter.EmployeeId);
                    if (!string.IsNullOrEmpty(filter.ValueId))
                        criteria.Where(x => identityAlias.ValueId == filter.ValueId);
                    if (filter.UserId > 0)
                        criteria.Where(x => x.User.Id == filter.UserId);
                    if (filter.OnlyAssistance)
                    {
                        criteria.Left.JoinAlias(x => x.TrackingAction, () => trackingActionAlias)
                            .Where(x => trackingActionAlias.Value > 0);
                    }

                    if (filter.CentroDeCostos != null)
                    {
                        criteria.Left.JoinAlias(x => employeeAlias.CentroCosto, () => centroDeCostosAlias);

                        if (filter.CentroDeCostos.Id > 0)
                            criteria.Where(x => centroDeCostosAlias.Id == filter.CentroDeCostos.Id);
                    }

                    if (filter.HqId > 0)
                        criteria.Where(x => x.HQ.Id == filter.HqId);
                    if (filter.HqIds != null && filter.HqIds.Any())
                        criteria.WhereRestrictionOn(x => x.HQ.Id).IsIn(filter.HqIds);
                    if (!string.IsNullOrEmpty(filter.EmployeeName))
                        criteria.WhereRestrictionOn(x => identityAlias.Name).IsLike(filter.EmployeeName + "%");
                    if (!string.IsNullOrEmpty(filter.EmployeePatherlastname))
                        criteria.WhereRestrictionOn(x => identityAlias.PatherLastname).IsLike(filter.EmployeePatherlastname + "%");

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<EmployeeTracking>();

                    if (filter.WithAction)
                        list.ForEach(x => NHibernateUtil.Initialize(x.TrackingAction));
                    if (filter.WithCompany)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Company));
                    if (filter.WithEmployee)
                    {
                        list.ForEach(x => NHibernateUtil.Initialize(x.Employee));
                        list.ForEach(x => NHibernateUtil.Initialize(x.Employee.Identity));
                        list.ForEach(x => NHibernateUtil.Initialize(x.Employee.CentroCosto));
                    }
                    if (filter.WithHQ)
                        list.ForEach(x => NHibernateUtil.Initialize(x.HQ));
                    if (filter.WithPoint)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Point));
                    if (filter.WithProvider)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Provider));
                    if (filter.WithResult)
                        list.ForEach(x => NHibernateUtil.Initialize(x.TrackingResult));
                    if (filter.WithSector)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Sector));
                    if (filter.WithVehicle)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Vehicle));
                    if (filter.WithUser)
                        list.ForEach(x => NHibernateUtil.Initialize(x.User));
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error WAISEmployeeTrackingRepository.GetByFilter", e);
                return null;
            }
        }
    }
}
