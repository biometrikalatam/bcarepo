﻿using System;
using System.Collections.Generic;
using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate.Criterion;


namespace BCR.Bio.Infraestructura
{
    public class HolidayRepository : Repository<Holiday>
    {

        public IList<Holiday> GetByCompanyId(int companyId)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var list = session.QueryOver<Holiday>()
                       .Where(x => x.Company.Id == companyId);
                    return list.List<Holiday>();
                }

            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetHolidaysByCompany", e);
                return null;
            }
        }

        public IList<Holiday> GetByFilter(HolidayFilterDTO filter)//todo filter
        {
            if (filter is null) return null;

            try
            {
                Unity unityAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var list = session.QueryOver<Holiday>()
                            .Inner.JoinAlias(x => x.Unity, () => unityAlias)
                            .Where(x => x.Company.Id == filter.CompanyId);

                    if (filter.UnityId != 0)
                        list.And(x => x.Unity.Id == filter.UnityId);

                    list.OrderBy(x => x.CreateDate).Desc();

                    var results = list.List<Holiday>();

                    return results;
                }

            }
            catch (Exception ex)
            {
                Log4Bio.Error("Error al recuperar Holiday by Filter" + ex.ToString());
                return null;
            }
        }
    }
}
