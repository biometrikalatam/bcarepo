﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class BpIdentityRepository : Repository<IBpIdentity>
    {
        public BpIdentity GetById(int id, bool withBirs = false)
        {
            try
            {
                BpIdentity identity;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria criteria = session.CreateCriteria<BpIdentity>().Add(Restrictions.Eq("id", id));
                    criteria.SetReadOnly(true);
                    identity = criteria.UniqueResult<BpIdentity>();

                    NHibernateUtil.Initialize(identity.CompanyIdEnroll);

                    if (identity != null)
                        if (withBirs)
                            NHibernateUtil.Initialize(identity.BpBir);
                }
                return identity;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar BpIdentityRepository.GetById", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="company"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="name"></param>
        /// <param name="lastname"></param>
        /// <returns></returns>
        public IList<BpIdentity> GetIdentitiesByFiltro(Paginacion paginacion, ICompany company,
                                                     string typeid, string valueid,
                                                     string name, string lastname, bool withBirs = false)
        {
            IList<BpIdentity> l = null;
            string msg;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria crit = session.CreateCriteria(typeof(BpIdentity));
                    crit.SetReadOnly(true);
                    if (company.Id > 0)
                    {
                        crit.Add(Restrictions.Eq("CompanyIdEnroll", company));
                    }

                    if (typeid != null && typeid.Trim().Length > 0)
                    {
                        crit.Add(Restrictions.Eq("TypeId", typeid));
                    }

                    if (valueid != null && valueid.Trim().Length > 0)
                    {
                        crit.Add(Expression.Eq("ValueId", valueid));
                    }

                    if (name != null && name.Trim().Length > 0)
                    {
                        crit.Add(Expression.InsensitiveLike("Name", name));
                    }

                    if (lastname != null && lastname.Trim().Length > 0)
                    {
                        crit.Add(Expression.Or(Expression.InsensitiveLike("PatherLastName", lastname),
                                               Expression.InsensitiveLike("MotherLastName", lastname)));
                    }

                    paginacion.Total = RowCount(crit);          // obtener la cantidad de filas para el paginado d

                    crit.AddOrder(Order.Desc("Id"));

                    crit.SetFirstResult(paginacion.Pagina * paginacion.Cantidad).SetMaxResults(paginacion.Cantidad);

                    l = crit.List<BpIdentity>();
                    if (withBirs)
                    {
                        foreach (BpIdentity identity in l)
                        {
                            identity.BpBir.ToList();
                            NHibernateUtil.Initialize(identity.CompanyIdEnroll);
                        }
                    }
                    else
                    {
                        //Elimino los Birs para que no salgan de la plataforma
                        foreach (BpIdentity identity in l)
                        {
                            identity.BpBir = null;
                            NHibernateUtil.Initialize(identity.CompanyIdEnroll);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("AdminBpIdentity.ListAll", ex);
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return l;
        }

        public IBpIdentity GetIdentityByCompanyIdentity(int companyId, string typeId, string valueId)
        {
            IBpIdentity identity = null;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    identity = session.QueryOver<IBpIdentity>()
                               .Where(x => x.CompanyIdEnroll.Id == companyId)
                               .And(x => x.TypeId == typeId)
                               .And(x => x.ValueId == valueId)
                               .ReadOnly()
                               .List<IBpIdentity>()
                               .FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("AdminBpIdentity.GetIdentityByCompanyIdentity", e);
            }

            return identity;
        }

        public IList<BpIdentity> GetByFilter(IdentityFilterDTO filter)
        {
            if (filter == null)
                return new List<BpIdentity>();

            try
            {
                IList<BpIdentity> list = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<BpIdentity>()
                                   .Where(x => x.CompanyIdEnroll.Id == filter.CompanyId)
                                   .OrderBy(x => x.PatherLastname).Asc;
                    if(filter.ValueId!=null)
                        criteria.Where(x => x.ValueId == filter.ValueId);
                    if (filter.Id > 0)
                        criteria.Where(x => x.Id == filter.Id);

                    if (filter.Paginacion != null)
                    {
                        filter.Paginacion.Total = criteria.RowCount();
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                    }

                    list = criteria.List<BpIdentity>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar BpIdentityRepository.GetByFilter", e);
                return new List<BpIdentity>();
            }
        }
    }
}