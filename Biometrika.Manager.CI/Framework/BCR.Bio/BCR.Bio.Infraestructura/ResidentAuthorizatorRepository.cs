﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class ResidentAuthorizatorRepository : Repository<ResidentAuthorizator>
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        public void DeleteByResident(int id)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    int deletedEntities = session.CreateQuery("DELETE ResidentAuthorizator r WHERE r.Resident = :idList")
                                        .SetParameter("idList", id)
                                        .ExecuteUpdate();

                    session.Flush();
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar DeleteResident -> ResidentAuthorizatorRepository", e);
            }

            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="filter"></param>
        /// <param name="unityId"></param>
        /// <returns></returns>
        public IList<Resident> GetAutorizatorListByFilter(int companyId, BpIdentity filter, int unityId = 0)
        {
            try
            {
                IList<Resident> list;
                Resident residentAlias = null;
                BpIdentity bpIdentityAlias = null;
                Unity unityAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<ResidentAuthorizator>()
                                   .Left.JoinAlias(x => x.Resident, () => residentAlias)
                                        .Left.JoinAlias(x => residentAlias.Unitys, () => unityAlias)
                                        .Left.JoinAlias(x => residentAlias.Identity, () => bpIdentityAlias)
                                   .Where(x => bpIdentityAlias.CompanyIdEnroll.Id == companyId);

                    criteria.ReadOnly();

                    if (unityId > 0)
                        criteria.And(x => unityAlias.Id == unityId);

                    if (filter != null)
                    {
                        if (!string.IsNullOrEmpty(filter.Name))
                            criteria.And(x => bpIdentityAlias.Name == filter.Name);
                        if (!string.IsNullOrEmpty(filter.PatherLastname))
                            criteria.And(x => bpIdentityAlias.PatherLastname == filter.PatherLastname);
                    }

                    list = criteria.List<ResidentAuthorizator>()
                            .Select(x => x.Resident).ToList();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByFilter", e);
                return null;
            }
        }
    }
}