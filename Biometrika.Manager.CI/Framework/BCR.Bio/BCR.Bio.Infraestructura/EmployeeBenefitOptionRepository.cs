﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura
{
    public class EmployeeBenefitOptionRepository : Repository<EmployeeOptionBenefit>    
    {

        public IList<EmployeeOptionBenefit> GetByEmployee(int employee)
        {
            IList<EmployeeOptionBenefit> EmployeeOptionBenefit=null;
            Employee EmployeeAlias = null;
            try
            {

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    EmployeeOptionBenefit = session.QueryOver<EmployeeOptionBenefit>()
                                                   .Inner.JoinAlias(x => x.employee, () => EmployeeAlias)
                                                   .Where(x => EmployeeAlias.Id==employee)
                                                   .List<EmployeeOptionBenefit>();
                }
            }
            catch(Exception e)
            {

            }
            return EmployeeOptionBenefit;
        }

        public bool DeleteAll(int employee)
        {
            bool res = false;
            try
            {

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    //session.Delete("from EmployeeOptionBenefit o where employee > :employee");
                    session.CreateQuery("delete EmployeeOptionBenefit  where employee = :employee")
                    .SetParameter("employee", employee)
                    .ExecuteUpdate();
                    session.Flush();
                    //Log4Bio.Info("Se elimina el Beneficio:" + EmployeeBenefitOption.Id + " asociado al empleado:" + EmployeeBenefitOption.employee.Id);

                    res = true;
                }
                
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al eliminar EmployeeBenefitOption", e);
            }
            return res;
        }


       
    }
}
