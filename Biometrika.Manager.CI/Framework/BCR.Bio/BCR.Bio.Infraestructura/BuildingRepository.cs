﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Infraestructura
{
    public class BuildingRepository : Repository<Building>
    {
        public IList<Building> GetByZone(int zoneId, Paginacion paginacion = null)
        {
            if (paginacion == null)
                paginacion = new Paginacion() { Pagina = 0, Cantidad = 1 };

            try
            {
                IList<Building> list;

                using (var session = BioNHibernateHelper.OpenSession())
                {

                    var criteria = session.QueryOver<Building>()
                      .Where(x => x.Zone.Id == zoneId);

                    criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                    criteria.Take(paginacion.Cantidad);

                    list = criteria.List<Building>();

                    paginacion.Total = criteria.RowCount();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar ZoneRepository -> GetByCompany", e);
                return null;
            }
        }

        public IList<Building> GetByCompany(int companyId)
        {
            try
            {
                IList<Building> list;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Zone zoneAlias = null;
                    HQ hqAlias = null;

                    list = session.QueryOver<Building>()
                      .Inner.JoinAlias(x => x.Zone, () => zoneAlias)
                      .Inner.JoinAlias(x => zoneAlias.HQ, () => hqAlias)
                      .Where(() => hqAlias.Company.Id == companyId)
                      .List<Building>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar BuildingRepository -> GetByCompany", e);
                return null;
            }
        }

        /// <summary>
        /// GetByFilter es un filtrado en base a el nuevo tipado de fltros
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public BuildingFilterDTO GetByFilter(BuildingFilterDTO filter)
        {
            IList<Building> list = new List<Building>();

            try
            {
                Zone zoneAlias = null;
                HQ hqAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Building>()
                                    .Left.JoinAlias(x => x.Zone, () => zoneAlias)
                                        .Left.JoinAlias(x => zoneAlias.HQ, () => hqAlias)
                                    .Where(x => hqAlias.Company.Id == filter.CompanyId)
                                    .OrderBy(x => x.Description).Asc;

                    if (filter.Id > 0)
                        criteria.Where(x => x.Id == filter.Id);
                    if (!string.IsNullOrEmpty(filter.Address))
                        criteria.Where(x => x.Address == filter.Address);
                    if (!string.IsNullOrEmpty(filter.AddressLike))
                        criteria.WhereRestrictionOn(x => x.Address).IsLike(filter.AddressLike + "%");
                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.Where(x => x.Description == filter.Description);
                    if (!string.IsNullOrEmpty(filter.DescriptionLike))
                        criteria.WhereRestrictionOn(x => x.Description).IsLike(filter.DescriptionLike + "%");

                    // filtro por zona
                    if (filter.ZoneId > 0)
                    {
                        criteria.And(x => zoneAlias.Id == filter.ZoneId);
                    }

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                    }

                    list = criteria.List<Building>();

                    if (filter.Paginacion != null)
                    {
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    filter.ResultList = list;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar BuildingRepository -> GetByFilter", e);
                return null;
            }

            return filter;
        }
    }
}