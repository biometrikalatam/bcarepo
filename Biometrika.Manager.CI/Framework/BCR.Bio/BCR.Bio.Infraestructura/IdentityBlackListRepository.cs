﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.Bio.Infraestructura.Configuration;
using BCR.System.Log;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Infraestructura
{
    public class IdentityBlackListRepository : Repository<IIdentityBlackList>
    {
       

        public IList<IIdentityBlackList> GetByCompanyTypeIdAndValueId(int companyId, string typeId, string valueId, bool active = false)
        {
            try
            {
                IList<IIdentityBlackList> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<IIdentityBlackList>()
                        .Where(k => k.Company.Id == companyId)
                        .And(k => k.TypeId == typeId)
                        .And(k => k.ValueId == valueId);

                    if (active)
                        criteria.And(k => k.EndDate == null);
                    criteria.ReadOnly();
                    list = criteria.List<IIdentityBlackList>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByCompanyTypeIdAndValueId", e);
                return null;
            }
        }

        public IList<IIdentityBlackList> GetByCompanyId(int companyId)
        {
            try
            {
                IList<IIdentityBlackList> list;
                User userInAlias = null;
                User userOutAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    IQueryOver<IIdentityBlackList> HQQuery =
                        session.QueryOver<IIdentityBlackList>()
                        .Left.JoinAlias(x => x.UserIn, () => userInAlias)
                        .Left.JoinAlias(x => x.UserOut, () => userOutAlias)
                        .Where(k => k.Company.Id == companyId);

                    HQQuery.ReadOnly();
                    list = HQQuery.List<IIdentityBlackList>();

                    if (list != null)
                    {
                        foreach (var item in list)
                        {
                            NHibernateUtil.Initialize(item.UserIn);
                            NHibernateUtil.Initialize(item.UserOut);
                        }
                    }
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByCompanyId", e);
                return null;
            }
        }

        public IdentityBlackListFilterDTO GetByFilter(IdentityBlackListFilterDTO filter)
        {
            IList<IdentityBlackList> list = new List<IdentityBlackList>();

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<IdentityBlackList>()
                        .Where(x => x.Company.Id == filter.CompanyId)
                        .OrderBy(x => x.ValueId).Asc;

                    if (filter.Active > 0)
                    {
                        if (filter.Active == 1)
                            criteria.And(x => x.EndDate == null);
                        else if (filter.Active == 2)
                            criteria.And(x => x.EndDate != null);
                    }
                        
                    criteria.ReadOnly();

                    if (!string.IsNullOrEmpty(filter.TypeId))
                        criteria.And(x => x.TypeId == filter.TypeId);
                    if (!string.IsNullOrEmpty(filter.ValueId))
                        criteria.And(x => x.ValueId == filter.ValueId);
                    if (filter.UserIn > 0)
                        criteria.And(x => x.UserIn.Id == filter.UserIn);
                    if (filter.UserOut > 0)
                        criteria.And(x => x.UserOut.Id == filter.UserOut);

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                    }

                    list = criteria.List<IdentityBlackList>();
                    if (list != null)
                    {
                        foreach (var item in list)
                        {
                            NHibernateUtil.Initialize(item.UserIn);
                            NHibernateUtil.Initialize(item.UserOut);
                        }
                    }

                    if (filter.Paginacion != null)
                    {
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    filter.ResultList = list;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar IdentityBlackListRepository -> GetByFilter", e);
                return null;
            }

            return filter;
        }
    }
}