﻿using System;
using System.Collections.Generic;
using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;


namespace BCR.Bio.Infraestructura
{
    public class VisitScheduleRepository : Repository<VisitSchedule>
    {
        public IList<VisitSchedule> GetByCompanyId(int companyId)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var list = session.QueryOver<VisitSchedule>()
                       .Where(x => x.Company.Id == companyId);
                    return list.List<VisitSchedule>();
                }

            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetHolidaysByCompany", e);
                return null;
            }
        }

    }
}
