﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.Bio.Infraestructura.Configuration;
using BCR.System.Log;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class CompanyRepository : Repository<ICompany>
    {
       

        public ICompany GetById(int id, bool withHQs = false, bool withSectors = false, bool withUsers =false)
        {
            try
            {
                ICompany item;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria criteria = session.CreateCriteria<ICompany>()
                        .Add(Restrictions.Eq("id", id));
                    criteria.SetReadOnly(true);
                    item = criteria.UniqueResult<ICompany>();
                    item.HaveHQs = item.HQs.Count > 0;
                    item.HaveSectors = item.Sectors.Count > 0;

                    if (withHQs)
                    {
                        NHibernateUtil.Initialize(item.HQs);
                        //item.HQs.ToList();
                        //foreach (IHQ hq in item.HQs)
                        //{
                        //    var dumy = hq.Town.Id;
                        //}
                    }
                    else
                        item.HQs = null;

                    if (withSectors)
                      NHibernateUtil.Initialize(item.Sectors);                    
                                //item.Sectors.ToList();
                    else
                        item.Sectors = null;

                    if (withUsers)
                        NHibernateUtil.Initialize(item.Users);
                    else
                        item.Users = null;
                    
                }
                return item;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar Compañia", e);
                return null;
            }
        }
    }
}