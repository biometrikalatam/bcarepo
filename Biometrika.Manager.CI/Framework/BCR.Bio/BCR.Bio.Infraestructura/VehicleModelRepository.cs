﻿using System;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.Bio.Infraestructura.Configuration;
using System.Linq;
using BCR.System.Log;

namespace BCR.Bio.Infraestructura
{
    public class VehicleModelRepository : Repository<IVehicleModel>
    {
        public IVehicleModel GetByDescription(string description)
        {
            try
            {
                IVehicleModel item;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    item = session.QueryOver<IVehicleModel>()
                            .WhereRestrictionOn(x => x.Description).IsLike("%" + description + "%")
                            .List<IVehicleModel>().FirstOrDefault();
                }
                return item;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar VehicleModelRepository.GetByDescription", e);
                return null;
            }
        }
    }
}