﻿using System;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.Bio.Infraestructura.Configuration;
using System.Linq;
using BCR.System.Log;

namespace BCR.Bio.Infraestructura
{
    public class VehicleMakeRepository : Repository<IVehicleMake>
    {
        public IVehicleMake GetByDescription(string vehicleMakeDescription)
        {
            try
            {
                IVehicleMake item;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    item = session.QueryOver<IVehicleMake>()
                            .WhereRestrictionOn(x => x.Description).IsLike("%" + vehicleMakeDescription + "%")
                            .List<IVehicleMake>().FirstOrDefault();
                }
                return item;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar VehicleMakeRepository.GetByDescription", e);
                return null;
            }
        }
    }
}