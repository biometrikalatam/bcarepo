﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate.Transform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura
{
    public class BenefitRepository: Repository<Benefit>
    {

        public IList<Benefit> GetAllBenefitByCompany(int companyid)
        {
            IList<Benefit> benefit=null;
            BenefitOption benefitoptionalias = null;
            

            try
            {

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    benefit = session.QueryOver<Benefit>()                        
                        .Where(x => x.Company.Id==companyid)
                        .List<Benefit>();

                }
            }
            catch (Exception e)
            {

            }
            return benefit;

        }

        /// <summary>
        /// Retorna todos los registros de la tabla RegisterMethod
        /// </summary>
        /// <returns></returns>
        public IList<BenefitDTO> GetByCompany(int companyid)
        {
            IList<BenefitDTO> listBenefitDTO = null;
            Benefit benefitalias = null;
            BenefitDTO result = null;
            try
            {

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    listBenefitDTO = session.QueryOver<Benefit>(() => benefitalias)
                            .SelectList(list => list
                             .Select(() => benefitalias.Id).WithAlias(() => result.Id)
                             .Select(() => benefitalias.Description).WithAlias(() => result.Description)
                             )
                             .Where(() => benefitalias.Company.Id == companyid)
                        .TransformUsing(Transformers.AliasToBean<BenefitDTO>())
                        .List<BenefitDTO>();
                    

                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar los beneficios asociados a una compañia", e);
            }
            return listBenefitDTO;
        }
    }
}
