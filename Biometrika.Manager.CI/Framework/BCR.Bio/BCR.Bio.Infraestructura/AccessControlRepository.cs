﻿using System;
using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using BCR.Bio.Domain.DTO.Filter;
using NHibernate.Util;
using NHibernate;

namespace BCR.Bio.Infraestructura
{
    public class AccessControlRepository : Repository<AccessControl>
    {
        public AccessControl GetByCompanyId(int companyId)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var item = session.QueryOver<AccessControl>()
                                .Where(x => x.Company.Id == companyId)
                                .List<AccessControl>()
                                .FirstOrDefault();

                    return item;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error AccessControl.GetByCompanyId", e);
                return null;
            }
        }

        public IList<AccessControl> GetByFilter(AccessControlFilter filter)
        {
            if (filter == null)
                return null;

            try
            {
                IList<AccessControl> list = null;
                Point pointAlias = null;
                PointRule pointRuleAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<AccessControl>()
                                    //.Where(x => x.Company.Id == filter.CompanyId)
                                    .OrderBy(x => x.Description).Asc;

                    if (filter.Id > 0)
                        criteria.Where(x => x.Id == filter.Id);
                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.Where(x => x.Description == filter.Description);
                    if (!string.IsNullOrEmpty(filter.DescriptionLike))
                        criteria.WhereRestrictionOn(x => x.Description).IsLike(filter.DescriptionLike + "%");

                    if (filter.PointId > 0)
                    {
                        criteria.Inner.JoinAlias(x => x.Points, () => pointAlias)
                            .Where(x => pointAlias.Id == filter.PointId);
                    }

                    if (!string.IsNullOrEmpty(filter.PointRuleLabel))
                        criteria.Inner.JoinAlias(x => x.PointRule, () => pointRuleAlias)
                            .Where(x => pointRuleAlias.Label == filter.PointRuleLabel);
                    else if (filter.PointRule != null)
                    {
                        criteria.Inner.JoinAlias(x => x.PointRule, () => pointRuleAlias);
                        if (filter.PointRule.Id > 0)
                            criteria.Where(x => pointRuleAlias.Id == filter.PointRule.Id);
                    }

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<AccessControl>();

                    if (filter.WithCompany)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Company));
                    if (filter.WithPointrule)
                        list.ForEach(x => NHibernateUtil.Initialize(x.PointRule));
                    if (filter.WithRuleScope)
                        list.ForEach(x => NHibernateUtil.Initialize(x.RuleScope));
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error AccessControlRepository.GetByFilter", e);
                return null;
            }
        }
    }
}