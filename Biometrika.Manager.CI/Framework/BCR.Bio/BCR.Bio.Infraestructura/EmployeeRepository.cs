﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using BCR.System.Type;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using NHibernate.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class EmployeeRepository : Repository<IEmployee>
    {
        public IList<Employee> GetEmployee(int company)
        {
            IList<Employee> list = null;
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    list = session.QueryOver<Employee>()
                        .List<Employee>();
                }
            }
            catch (Exception e)
            {
            }
            return list;
        }

        public bool GetEmployeeTracking(string datefrom, string dateto, int consolidado)
        {
            bool res = false;
            try
            { 
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    IQuery query = session.CreateSQLQuery("exec sp_calcularTiempos @dateto=:param1,@datefrom=:param2,@consolidado=:param3");
                    query.SetString("param1", dateto);
                    query.SetString("param2", datefrom);
                    query.SetInt32("param3", consolidado);
                    query.ExecuteUpdate();
                    res = true;
                }
            }
            catch(Exception exe)
            {
                res = false;
                Log4Bio.Error("Error en obtener el GetEmployeeTracking", exe);
            }
            return res;
        }

        public IList<EmployeeDTO> ListByCompanyId(int companyId)
        {
            IList<EmployeeDTO> list;
            using (var session = BioNHibernateHelper.OpenSession())
            {
                BpIdentity identityAlias = null;
                User userAlias = null;

                list = session.QueryOver<IEmployee>()
                  .Inner.JoinAlias(x => x.Identity, () => identityAlias)
                  .Left.JoinAlias(x => x.User, () => userAlias)
                  .Where(() => identityAlias.CompanyIdEnroll.Id == companyId)
                  .Select(
                    x => x.Id,
                    x => identityAlias.Name,
                    x => identityAlias.PatherLastname,
                    x => identityAlias.MotherLastname,
                    x => userAlias.Username
                    )
                    .List<object[]>()
                    .Select(p => new EmployeeDTO()
                    {
                        Id = ConvertEx.ObjectToInt(p[0]),
                        FullName = string.Concat(ConvertEx.ObjectToString(p[1]), ' ', ConvertEx.ObjectToString(p[2]), ' ', ConvertEx.ObjectToString(p[2])),
                        UserName = ConvertEx.ObjectToString(p[4])
                    }).ToList();
            }
            return list;
        }

        public EmployeeDTO GetEmployeeDto(int id)
        {
            EmployeeDTO employee = null;
            BpIdentity identityAlias = null;
            Employee employeeAlias = null;
            Department departmentAlias = null;
            Company companyAlias = null;
            CentroDeCostos centrocostoAlias = null;
            Work workAlias = null;
            EmployeeDTO result = null;
            Unity unityAlias = null;
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    employee = session.QueryOver<Employee>(() => employeeAlias)
                         .Left.JoinAlias(x => x.CentroCosto, () => centrocostoAlias)
                         .Left.JoinAlias(x => x.Identity, () => identityAlias)
                         .Left.JoinAlias(x => x.Identity.CompanyIdEnroll, () => companyAlias)
                         .Left.JoinAlias(x => x.Department, () => departmentAlias)
                        .Left.JoinAlias(x => x.Work, () => workAlias)
                        .Left.JoinAlias(x => x.Unitys, () => unityAlias)
                        .Where(x => x.Id == id)
                        .SelectList(list => list
                              .Select(() => employeeAlias.Id).WithAlias(() => result.Id)
                              .Select(() => identityAlias.Name).WithAlias(() => result.Name)
                              .Select(() => identityAlias.TypeId).WithAlias(() => result.TypeId)
                              .Select(() => identityAlias.ValueId).WithAlias(() => result.ValueId)
                              .Select(() => identityAlias.PatherLastname).WithAlias(() => result.PatherLastname)
                              .Select(() => identityAlias.MotherLastname).WithAlias(() => result.MotherLastname)
                              .Select(() => identityAlias.Sex).WithAlias(() => result.Sex)
                              .Select(() => identityAlias.BirthDate).WithAlias(() => result.BirthDate)
                              .Select(() => identityAlias.CompanyIdEnroll.Id).WithAlias(() => result.CompanyIdEnroll)
                              .Select(() => identityAlias.Id).WithAlias(() => result.Identity)
                              .Select(() => identityAlias.Cellphone).WithAlias(() => result.Cellphone)
                              .Select(() => identityAlias.Phone).WithAlias(() => result.Phone)
                              .Select(() => identityAlias.Phone2).WithAlias(() => result.Phone2)
                              .Select(() => employeeAlias.EndDate).WithAlias(() => result.Endate)
                              .Select(() => employeeAlias.TimeAssistance).WithAlias(() => result.TimeAssistance)
                              .Select(() => employeeAlias.CentroCosto.Id).WithAlias(() => result.CentrodeCostoId)
                              .Select(() => centrocostoAlias.Description).WithAlias(() => result.CentrodeCostoNombre)
                              .Select(() => employeeAlias.Department.Id).WithAlias(() => result.DepartamentId)
                              .Select(() => departmentAlias.description).WithAlias(() => result.DepartamentNombre)
                               .Select(() => employeeAlias.Work.Id).WithAlias(() => result.WorkId)
                              .Select(() => workAlias.Description).WithAlias(() => result.WorkNombre)
                              .Select(() => unityAlias.Id).WithAlias(() => result.UnityId)
                              .Select(() => unityAlias.Description).WithAlias(() => result.UnityName)
                              .Select(() => unityAlias.Address).WithAlias(() => result.UnityAddress)
                              .Select(() => employeeAlias.Autorizador).WithAlias(() => result.Autorizador)
                              )
                        .TransformUsing(Transformers.AliasToBean<EmployeeDTO>())
                        //.SingleOrDefault<EmployeeDTO>(); // Falla cuando lo que sea tenga mas de un Unity asociado
                        .List<EmployeeDTO>().FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar el empleado en GetEmployeeDto", e);
            }
            return employee;
        }

        public IList<Employee> GetByFilter(EmployeeFilterDTO filter)
        {
            try
            {
                IList<Employee> list = null;
                BpIdentity identityAlias = null;
                CentroDeCostos centroDeCostoAlias = null;
                Department departmentAlias = null;
                Work workAlias = null;
                RegisterMethod registerMethodAlias = null;
                Unity unityAlias = null;
                Zone zoneAlias = null;
                Employee employeeAlias = null;
                

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    #region SubQuery

                    var subQuery = QueryOver.Of<Employee>()
                                   .Inner.JoinAlias(x => x.Identity, () => identityAlias)
                                   .Where(x => identityAlias.CompanyIdEnroll.Id == filter.CompanyId);

                    if (filter.EmployeeClass == EmployeeClass.Authorizator)
                        subQuery.Where(x => x.Autorizador == true);
                    else if (filter.EmployeeClass == EmployeeClass.NotAuthorizator)
                        subQuery.Where(x => x.Autorizador == false || x.Autorizador == null);

                    if (filter.Id > 0)
                        subQuery.Where(x => x.Id == filter.Id);
                    if (!string.IsNullOrEmpty(filter.TypeId))
                        subQuery.Where(x => identityAlias.TypeId == filter.TypeId);
                    if (!string.IsNullOrEmpty(filter.ValueId))
                        subQuery.Where(x => identityAlias.ValueId == filter.ValueId);
                    if (!string.IsNullOrEmpty(filter.FatherLastName)) // TODO: Sabemos que nos da cagaso tremendo hacer busquedas por like a los dos lados...esto no lo hice yo
                        subQuery.Where(x => identityAlias.PatherLastname.IsLike("%" + filter.FatherLastName + "%") || identityAlias.MotherLastname.IsLike("%" + filter.FatherLastName + "%"));
                    if (!string.IsNullOrEmpty(filter.FullName))
                        subQuery.WhereRestrictionOn(x => identityAlias.FullName).IsLike("%" + filter.FullName + "%");
                    if (!string.IsNullOrEmpty(filter.FullApellidosLike))
                        subQuery.WhereRestrictionOn(x => identityAlias.FullApellidos).IsLike("%" + filter.FullApellidosLike + "%");
                    if (filter.IdentityId > 0)
                        subQuery.Where(x => x.Identity.Id == filter.IdentityId);
                    if (filter.IsActive == true)
                        subQuery.WhereRestrictionOn(x => x.EndDate).IsNull();
                    if (filter.Active > 0)
                    {
                        if (filter.Active == 1)
                            subQuery.WhereRestrictionOn(x => x.EndDate).IsNull();
                        if (filter.Active == 2)
                            subQuery.WhereRestrictionOn(x => x.EndDate).IsNotNull();
                    }
                    if (filter.OnlyAutorizator)
                        subQuery.Where(x => x.Autorizador == true);

                    if (filter.UserId != null)
                    {
                        if (filter.UserId.Id > 0)
                            subQuery.Where(x => x.User.Id == filter.UserId.Id);
                    }

                    if (filter.CentroDeCosto != null)
                    {
                        subQuery.Left.JoinAlias(x => x.CentroCosto, () => centroDeCostoAlias);
                        if (filter.CentroDeCosto.Id > 0)
                            subQuery.Where(x => centroDeCostoAlias.Id == filter.CentroDeCosto.Id);
                    }

                    if (filter.Unity != null)
                    {
                        subQuery.Left.JoinAlias(x => x.Unitys, () => unityAlias);
                        if (filter.Unity.Id > 0)
                            subQuery.Where(x => unityAlias.Id == filter.Unity.Id);
                        if (!string.IsNullOrEmpty(filter.Unity.Address))
                            subQuery.Where(x => unityAlias.Address == filter.Unity.Address);
                    }

                    if (filter.HqList != null && filter.HqList.Any())
                    {
                        if (filter.Unity == null)
                            subQuery.Left.JoinAlias(x => x.Unitys, () => unityAlias);

                        subQuery.Left.JoinAlias(x => unityAlias.Zone, () => zoneAlias)
                            .WhereRestrictionOn(x => zoneAlias.HQ.Id).IsIn(filter.HqList.Select(h => h.Id).ToList());
                    }

                    subQuery.Select(x => x.Id);

                    #endregion

                    identityAlias = null;
                    var criteria = session.QueryOver<Employee>(() => employeeAlias)
                        .Inner.JoinAlias(x => x.Identity, () => identityAlias)
                        .WithSubquery.WhereProperty(x => x.Id).In(subQuery)
                        .OrderBy(x => identityAlias.FullName).Asc
                        .ReadOnly();

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<Employee>();

                    if (filter.WithContactForm)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Identity.ContactFormList));
                    if (filter.WithCentroDeCosto)
                        list.ForEach(x => NHibernateUtil.Initialize(x.CentroCosto));
                    if (filter.WithUser)
                        list.ForEach(x => NHibernateUtil.Initialize(x.User));
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EmployeRepository.GetByFilter", e);
                return null;
            }
        }

        public Employee GetById(int id, bool withIdentity = false)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Employee employee = session.QueryOver<Employee>()
                           .Where(x => x.Id == id)
                           .List<Employee>()
                           .FirstOrDefault();

                    if (withIdentity)
                        NHibernateUtil.Initialize(employee.Identity);

                    return employee;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EmployeeRespository.GetById", e);
                return null;
            }
        }

        public string GetHASHByCompanyId(int companyId)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.CreateSQLQuery("select CHECKSUM_AGG(BINARY_CHECKSUM([endDate], [autorizador], BPI.[full_name]))"
                    + " FROM [Employee] EMP"
                    + " INNER JOIN [bp_identity] BPI"
                    + " ON EMP.[identity] = BPI.id"
                    + " WHERE BPI.[companyidenroll] = :CompanyId")
                    .SetParameter("CompanyId", companyId);

                    var checksum = criteria.UniqueResult();
                    return checksum?.ToString() ?? "";
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EmployeeRepository -> GetHASHByCompanyId", e);
                return null;
            }
        }
    }
}