﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate;
using NHibernate.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura
{
    public class VisitorTrackingObjetadosRepository : Repository<VisitorTrackingObjetados>
    {
        public IList<VisitorTrackingObjetados> GetByFilter(VisitorTrackingObjetadosFilter filter)
        {
            BpIdentity identityAlias = null;
            IList<VisitorTrackingObjetados> list = new List<VisitorTrackingObjetados>();

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<VisitorTrackingObjetados>()
                                    .Inner.JoinAlias(x => x.Identity, () => identityAlias)
                                    .Where(x => x.Company.Id == filter.CompanyId)
                                    .And(x => x.TimeStamp > filter.Desde)
                                    .And(x => x.TimeStamp < filter.Hasta.Date.AddDays(1))
                                    .OrderBy(x => x.TimeStamp).Desc;

                    if (!string.IsNullOrEmpty(filter.TypeId))
                        criteria.And(x => identityAlias.TypeId == filter.TypeId);
                    if (!string.IsNullOrEmpty(filter.ValueId))
                        criteria.And(x => identityAlias.ValueId == filter.ValueId);
                    if (filter.ActionId > 0)
                        criteria.And(x => x.Action.Id == filter.ActionId);
                    if (filter.ResultId > 0)
                        criteria.And(x => x.Result.Id == filter.ResultId);

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<VisitorTrackingObjetados>();

                    list.ForEach(x =>
                    {
                        if (filter.WithIdentity)
                            NHibernateUtil.Initialize(x.Identity);
                        if (filter.WithAction)
                            NHibernateUtil.Initialize(x.Action);
                        if (filter.WithResult)
                            NHibernateUtil.Initialize(x.Result);
                        if (filter.WithPoint)
                            NHibernateUtil.Initialize(x.Point);
                    });
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar VisitorTrackingObjetadosRepository -> GetByFilter", e);
                return null;
            }
        }
    }
}
