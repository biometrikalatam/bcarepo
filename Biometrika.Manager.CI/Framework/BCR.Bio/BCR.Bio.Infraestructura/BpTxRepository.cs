﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using BCR.System.Validation;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura
{
    public class BpTxRepository : Repository<BpTx>
    {
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public int GetQCurrentClient()
        {
            String msgErr;
            int res = 0;
            String strsql = "";
            string msgerr;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    strsql = "SELECT COUNT(*) AS QCantidad " +
                         "FROM (SELECT DISTINCT ipenduser " +
                         "      FROM bp_tx) AS derivedtbl_1";

                    IList<int> result = session.CreateSQLQuery(strsql)
                                                .AddScalar("QCantidad", NHibernateUtil.Int32)
                                            .SetResultTransformer(new NHibernate.Transform.AliasToBeanResultTransformer(typeof(int)))
                                            .List<int>();
                    res = (result == null || result.Count == 0) ? 0 : result[0];
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("AdminBpTx.GetQCurrentClient", ex);
                msgErr = ex.Message;
                res = -1;
            }

            return res;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="idtrack"></param>
        /// <returns></returns>
        public BpTx GetByTrackId(string idtrack)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    IList list = session.CreateCriteria(typeof(BpTx)).
                    Add(Expression.Eq("Trackid", idtrack)).List();
                    if (list != null && list.Count > 0) return (BpTx)list[0];
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("AdminBpTx.Retrieve (by typeid,valueid)", ex);
                //ValidationDictionary.AddError("", ex.Message);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bpTx"></param>
        /// <param name="conxAux"></param>
        public void AddTxConx(BpTx tx, BpTxConx conx)
        {
            BpTx oRes = null;
            try
            {
                conx.BpTx = tx;
                if (tx.BpTxConx == null)
                    tx.BpTxConx = new List<BpTxConx>();

                tx.BpTxConx.Add(conx);
                oRes = tx;
            }
            catch (Exception ex)
            {
                Log4Bio.Error("BpTx.AddTxConx", ex);
            }
            return;
        }
    }
}