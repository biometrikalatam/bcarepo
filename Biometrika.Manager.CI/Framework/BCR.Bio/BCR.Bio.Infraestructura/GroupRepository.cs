﻿using System;
using System.Collections.Generic;
using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;

namespace BCR.Bio.Infraestructura
{
    public class GroupRepository : Repository<Group>
    {
        public IList<Group> GetByVisitorId(int visitorId)
        {
            try
            {
                IList<Group> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Visitor visitorAlias = null;

                    list = session.QueryOver<Group>()
                           .Left.JoinAlias(x => x.Visitors, () => visitorAlias)
                           .Where(x => visitorAlias.Id == visitorId)
                           .List<Group>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GroupsRepository -> GetByVisitorId", e);
                return null;
            }
        }
    }
}