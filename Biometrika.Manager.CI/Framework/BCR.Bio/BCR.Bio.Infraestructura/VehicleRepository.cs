﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using BCR.System.Type;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class VehicleRepository : Repository<IVehicle>
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <param name="companyId"></param>
        /// <param name="withResidents"></param>
        /// <param name="withVisitors"></param>
        /// <returns></returns>
        public IVehicle GetById(int id, int companyId, bool withResidents = false, bool withVisitors = false, bool withProvider = false)
        {
            try
            {
                Vehicle item;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    item = session.QueryOver<Vehicle>()
                            .Where(x => x.Id == id)
                            .List<Vehicle>().FirstOrDefault();

                    if (withResidents)
                        NHibernateUtil.Initialize(item.Residents);
                    if (withVisitors)
                        NHibernateUtil.Initialize(item.Visitors);
                    if (withProvider)
                        NHibernateUtil.Initialize(item.ProviderList);
                }
                return item;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar vehículo", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="plateNumber"></param>
        /// <returns></returns>
        public IList<IVehicle> GetByCompanyAndPlateNumber(int companyId, string plateNumber)
        {
            try
            {
                IList<IVehicle> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    IQueryOver<IVehicle> HQQuery =
                session.QueryOver<IVehicle>()
                        .Where(k => k.Company.Id == companyId)
                        .And(k => k.PlateNumber == plateNumber);

                    HQQuery.ReadOnly();
                    list = HQQuery.List<IVehicle>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByCompanyAndPlateNumber", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="paginacion"></param>
        /// <param name="companyId"></param>
        /// <param name="platenumber"></param>
        /// <param name="apellido"></param>
        /// <returns></returns>
        public IList<Vehicle> GetByFilter(IPaginacion paginacion, int companyId, Vehicle filter, bool withResidents = false, bool withVisitors = false, bool withProvider = false)
        {
            if (filter == null)
                return null;

            try
            {
                IList<Vehicle> list;
                Provider providerAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Vehicle>()
                                   .Left.JoinAlias(x => x.ProviderList, () => providerAlias)
                                   .Where(x => x.Company.Id == companyId)
                                   .OrderBy(x => x.PlateNumber).Asc;

                    if (!string.IsNullOrEmpty(filter.PlateNumber))
                        criteria.WhereRestrictionOn(x => x.PlateNumber).IsLike("%" + filter.PlateNumber + "%");

                    if (filter.ProviderList != null && filter.ProviderList.Any())
                        criteria.Where(x => providerAlias.Id == filter.ProviderList.First().Id);

                    if (filter.IdentityState != null && filter.IdentityState.Id > 0)
                        criteria.Where(x => x.IdentityState.Id == filter.IdentityState.Id);

                    if (paginacion != null)
                    {
                        criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                        criteria.Take(paginacion.Cantidad);
                        paginacion.Total = criteria.RowCount();
                    }

                    criteria.ReadOnly();
                    list = criteria.List<Vehicle>();

                    if (withResidents)
                        list.ForEach(x => 
                        {
                            NHibernateUtil.Initialize(x.Residents);
                            x.Residents.ForEach(r => NHibernateUtil.Initialize(r.Unitys));
                        });
                    if (withVisitors)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Visitors));
                    if (withProvider)
                        list.ForEach(x => NHibernateUtil.Initialize(x.ProviderList));
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByFilter -> VehicleRepository", e);
                return null;
            }
        }

        /// <summary>
        /// Método para retornar una lista de vehículos de acuerdo a un filtro.
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public IList<Vehicle> GetByCustomFilter(VehicleFilter filter, bool withResidents = false)
        {
            if (filter == null)
                return null;
            try
            {
                IList<Vehicle> list;
                Provider providerAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Vehicle>()
                                   .Left.JoinAlias(x => x.ProviderList, () => providerAlias)
                                   .Where(x => x.Company.Id == filter.CompanyId);

                    if (filter.Id > 0)
                        criteria.Where(x => x.Id == filter.Id);
                    if (!string.IsNullOrEmpty(filter.PlateNumber))
                        criteria.Where(x => x.PlateNumber == filter.PlateNumber);
                    if (!string.IsNullOrEmpty(filter.PlateNumberLike))
                        criteria.WhereRestrictionOn(x => x.PlateNumber).IsLike(filter.PlateNumberLike + "%");
                    if (filter.OnlyActive)
                        criteria.Where(x => x.EndDate == null);

                    if (filter.ProviderList != null && filter.ProviderList.Any())
                    {
                        Provider provider = filter.ProviderList.First();

                        if (provider.Id > 0)
                            criteria.Where(x => providerAlias.Id == provider.Id);
                        if (!string.IsNullOrEmpty(provider.Name))
                            criteria.Where(x => providerAlias.Name == provider.Name);
                    }

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    criteria.ReadOnly();
                    list = criteria.List<Vehicle>();

                    if (withResidents)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Residents));

                    return list;
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("Error al recuperar GetByCustomFilter -> VehicleRepository", ex);
                return null;
            }
        }

        public IList<Vehicle> GetByIdentityAndCompany(IPaginacion paginacion, int companyId, string plateNumber, string typeId, string valueId, bool withResidents = false, bool withVisitors = false, bool withProvider = false)
        {
            try
            {
                IList<Vehicle> list;
                Resident residentAlias = null;
                BpIdentity bpidentityAlias = null;
                Visitor visitorAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    List<int> vehicleIdList = new List<int>();

                    #region CriteriaResident

                    var criteriaResident = session.QueryOver<IVehicle>()
                                            .Left.JoinAlias(x => x.Residents, () => residentAlias)
                                            .Left.JoinAlias(x => residentAlias.Identity, () => bpidentityAlias)
                                            .Where(x => x.Company.Id == companyId);

                    if (!string.IsNullOrEmpty(typeId))
                        criteriaResident.Where(x => bpidentityAlias.TypeId == typeId);
                    if (!string.IsNullOrEmpty(valueId))
                        criteriaResident.Where(x => bpidentityAlias.ValueId == valueId);
                    if (!string.IsNullOrEmpty(plateNumber))
                        criteriaResident.Where(x => x.PlateNumber == plateNumber);

                    List<int> residentVehicleIdList = criteriaResident.List<IVehicle>()
                                            .Select(x => x.Id)
                                            .ToList();

                    if (residentVehicleIdList != null && residentVehicleIdList.Any())
                        vehicleIdList.AddRange(residentVehicleIdList);

                    #endregion

                    bpidentityAlias = null;

                    #region CriteriaVisitor

                    var criteriaVisitor = session.QueryOver<Vehicle>()
                                            .Left.JoinAlias(x => x.Visitors, () => visitorAlias)
                                            .Left.JoinAlias(x => visitorAlias.Identity, () => bpidentityAlias)
                                            .Where(x => x.Company.Id == companyId);

                    if (!string.IsNullOrEmpty(typeId))
                        criteriaVisitor.Where(x => bpidentityAlias.TypeId == typeId);
                    if (!string.IsNullOrEmpty(valueId))
                        criteriaVisitor.Where(x => bpidentityAlias.ValueId == valueId);
                    if (!string.IsNullOrEmpty(plateNumber))
                        criteriaVisitor.Where(x => x.PlateNumber == plateNumber);

                    List<int> visitorVehicleIdList = criteriaVisitor.List<Vehicle>()
                                            .Select(x => x.Id)
                                            .ToList();

                    if (visitorVehicleIdList != null && visitorVehicleIdList.Any())
                        vehicleIdList.AddRange(visitorVehicleIdList);

                    #endregion

                    var criteria = session.QueryOver<Vehicle>()
                                   .Where(x => x.Company.Id == companyId)
                                   .WhereRestrictionOn(x => x.Id).IsIn(vehicleIdList);

                    if (paginacion != null)
                    {
                        criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                        criteria.Take(paginacion.Cantidad);
                    }

                    list = criteria.List<Vehicle>();

                    if (withResidents)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Residents));
                    if (withVisitors)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Visitors));
                    if (withProvider)
                        list.ForEach(x => NHibernateUtil.Initialize(x.ProviderList));

                    if (paginacion != null)
                    {
                        paginacion.Total = criteria.RowCount();
                    }
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByIdentityAndCompany -> VehicleRepository", e);
                return null;
            }
        }


        /// <summary>
        /// Provisorio, analizando funcionamiento
        /// Recupera los vehiculos para un reidente determinado o para un Unity
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IList<Vehicle> GetByFilter(VehicleFilter filter, bool withResidents = false, bool withVisitors = false, bool withProvider = false)
        {
            if (filter == null)
                return null;

            try
            {
                IList<Vehicle> list;
                Resident residentAlias = null;
                Unity unityAlias = null;
                Visitor visitorAlias = null;
                Vehicle vehicleAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    List<int> vehicleIdList = new List<int>();

                    #region SubQueryResident

                    var subQuery = QueryOver.Of<Vehicle>()
                                    .Left.JoinAlias(x => x.Residents, () => residentAlias)
                                        .Left.JoinAlias(x => residentAlias.Unitys, () => unityAlias)
                                    .Where(x => x.Company.Id == filter.CompanyId);

                    if (filter.ResidentId > 0)
                        subQuery.Where(x => residentAlias.Id == filter.ResidentId);
                    if (filter.UnityId > 0)
                        subQuery.Where(x => unityAlias.Id == filter.UnityId);
                    if (!string.IsNullOrEmpty(filter.PlateNumberLike))
                        subQuery.WhereRestrictionOn(x => x.PlateNumber).IsLike("%" + filter.PlateNumberLike + "%");
                    if (filter.IdentityStateId > 0)
                        subQuery.Where(x => x.IdentityState.Id == filter.IdentityStateId);

                    subQuery.Select(x => x.Id);

                    #endregion

                    unityAlias = null;
                    residentAlias = null;

                    #region CriteriaVisitor

                    //var subQueryV = QueryOver.Of<Vehicle>()
                    //                .Left.JoinAlias(x => x.Visitors, () => visitorAlias)
                    //                    .Left.JoinAlias(x => visitorAlias.Unitys, () => unityAlias)
                    //                .Where(x => x.Company.Id == filter.CompanyId);

                    ////if (filter.ResidentId > 0)
                    ////    subQuery.Where(x => residentAlias.Id == filter.ResidentId);
                    //if (filter.UnityId > 0)
                    //    subQueryV.Where(x => unityAlias.Id == filter.UnityId);
                    //if (!string.IsNullOrEmpty(filter.PlateNumberLike))
                    //    subQueryV.WhereRestrictionOn(x => x.PlateNumber).IsLike("%" + filter.PlateNumberLike + "%");
                    //if (filter.IdentityStateId > 0)
                    //    subQueryV.Where(x => x.IdentityState.Id == filter.IdentityStateId);

                    //IList<Vehicle> idVisitorVehicle = subQueryV.Select(x => x.Id);

                    #endregion

                    var criteria = session.QueryOver<Vehicle>(() => vehicleAlias)
                        .WithSubquery.WhereProperty(x => x.Id).In(subQuery);

                    if (filter.ForResident)
                        criteria.Inner.JoinAlias(x => x.Residents, () => residentAlias);
                        
                    criteria.ReadOnly();

                    //var criteria = session.QueryOver<Vehicle>()
                    //               .Where(x => x.Company.Id == filter.CompanyId)
                    //               .WhereRestrictionOn(x => x.Id).IsIn(vehicleIdList);

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<Vehicle>();

                    if (withResidents)
                        list.ForEach(x =>
                        {
                            NHibernateUtil.Initialize(x.Residents);
                            x.Residents.ForEach(r => NHibernateUtil.Initialize(r.Unitys));
                        });
                    if (withVisitors)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Visitors));
                    if (withProvider)
                        list.ForEach(x => NHibernateUtil.Initialize(x.ProviderList));
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByUnityAndCompany -> VehicleRepository", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="onlyResidents"></param>
        /// <param name="onlyVisitors"></param>
        /// <returns></returns>
        public IList<IVehicle> GetByCompanyId(int companyId, bool onlyResidents = false, bool onlyVisitors = false)
        {
            try
            {
                IList<IVehicle> list = new List<IVehicle>();
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var HQQuery = session.QueryOver<IVehicle>()
                        .Where(k => k.Company.Id == companyId);
                    HQQuery.ReadOnly();
                    if (onlyResidents)
                    {
                        HQQuery = HQQuery.Fetch(k => k.Residents).Eager;
                        //HQQuery.And(k => k.Residents.Count > 0);
                    }

                    if (onlyVisitors)
                    {
                        //HQQuery.And(k => k.Visitors.Count > 0);
                        HQQuery = HQQuery.Fetch(k => k.Visitors).Eager;
                    }

                    list = HQQuery.List<IVehicle>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByCompanyId", e);
                return null;
            }
        }

        public IList<Vehicle> GetResidentVehiclesByCompanyId(int companyId, bool withResident = false, bool onlyActive = false, bool onlyResidentActive = false)
        {
            try
            {
                IList<Vehicle> list = null;
                Resident residentAlias = null;
                BpIdentity identityAlias = null;
                IdentityState residentIdentityStatAlias = null;
                IdentityState vehicleIdentityStateAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Vehicle>()
                           .Inner.JoinAlias(x => x.Residents, () => residentAlias)
                           .Inner.JoinAlias(x => residentAlias.Identity, () => identityAlias)
                           .Left.JoinAlias(x => x.IdentityState, () => vehicleIdentityStateAlias)
                           .Where(x => x.Company.Id == companyId);

                    if (onlyActive)
                        criteria.Where(x => vehicleIdentityStateAlias.Class == "VEHICLE")
                            .And(x => vehicleIdentityStateAlias.Tag == "ACTIVE")
                            .And(x => vehicleIdentityStateAlias.Company.Id == companyId);

                    if (onlyResidentActive)
                        criteria.Left.JoinAlias(x => identityAlias.IdentityStateList, () => residentIdentityStatAlias)
                            .Where(x => residentIdentityStatAlias.Class == "IDENTITY")
                            .And(x => residentIdentityStatAlias.Tag == "ACTIVE")
                            .And(x => residentIdentityStatAlias.Company.Id == companyId);

                    list = criteria.List<Vehicle>();

                    if (withResident)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Residents));
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetForResidentByCompanyId", e);
                return null;
            }
        }

        public IList<Vehicle> GetByResidentAndCompanyId(VehicleFilter filter, bool withResidents = false, bool withVisitors = false, bool withProvider = false)
        {
            try
            {
                IList<Vehicle> list = null;
                Resident residentAlias = null;
                Provider providerAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Vehicle>()
                           .Inner.JoinAlias(x => x.Residents, () => residentAlias)
                           .Left.JoinAlias(x => x.ProviderList, () => providerAlias)
                           .Where(x => x.Company.Id == filter.CompanyId)
                           .And(x => residentAlias.Id == filter.ResidentId)
                           .OrderBy(x => x.PlateNumber).Asc;

                    if (!string.IsNullOrEmpty(filter.PlateNumberLike))
                        criteria.WhereRestrictionOn(x => x.PlateNumber).IsLike("%" + filter.PlateNumberLike + "%");

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<Vehicle>();

                    if (withResidents)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Residents));
                    if (withVisitors)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Visitors));
                    if (withProvider)
                        list.ForEach(x => NHibernateUtil.Initialize(x.ProviderList));
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetForResidentByCompanyId", e);
                return null;
            }
        }
    }
}