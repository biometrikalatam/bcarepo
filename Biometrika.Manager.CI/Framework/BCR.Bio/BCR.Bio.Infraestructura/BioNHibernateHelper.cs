﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Audit;
using BCR.System.Configuration;
using BCR.System.Log;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using System.Linq;
using System.Reflection;

namespace BCR.Bio.Infraestructura
{
    /// <summary>
    ///
    /// </summary>
    public class BioNHibernateHelper
    {
        private static ISessionFactory sessionFactory = null;
        private static NhibernateParams NhibernateParams = null;
        private static AuditInterceptor auditInterceptor = new AuditInterceptor();

        public static void Initilize(NhibernateParams nhibernateParams)
        {
            if (NhibernateParams == null)
                NhibernateParams = nhibernateParams;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="mappings"></param>
        /// <returns></returns>
        private static ISessionFactory SessionFactory()
        {
            if (sessionFactory == null)
            {
                Log4Bio.Info("Iniciando Fluent config");
                ForceLoad();

                sessionFactory = Fluently.Configure()
                    .Database(
                        MsSqlConfiguration.MsSql2008.ConnectionString(NhibernateParams.ConnectionString.ToString())
                    )
                    .Mappings(AddAssemblies)
                    //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<User>().AddFromAssemblyOf<BpIdentity>())
                    .ExposeConfiguration(AddConfiguration)
                    .BuildSessionFactory();

                Log4Bio.Info("Inicialización Fluent config exitosa");
            }

            return sessionFactory;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="configuration"></param>
        private static void AddConfiguration(NHibernate.Cfg.Configuration configuration)
        {
            if (NhibernateParams.EnableAudit)
                configuration.SetInterceptor(auditInterceptor);
        }

        private static void ForceLoad()
        {
            for (int i = 0; i < NhibernateParams.Mappings.Length; i++)
            {
                try
                {
                    AppDomain.CurrentDomain.Load(NhibernateParams.Mappings[i].Trim());
                }
                catch (Exception e)
                {
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="mappingConfiguration"></param>
        private static void AddAssemblies(MappingConfiguration mappingConfiguration)
        {
            ////var events = new List<ManualResetEvent>();

            //for (int i = 0; i < NhibernateParams.Mappings.Count(); i++)
            //{
            //    //var resetEvent = new ManualResetEvent(false);

            //    try
            //    {
            //        //ThreadPool.QueueUserWorkItem(new WaitCallback(Callback), NhibernateParams.Mappings[i].Trim());
            //        //events.Add(resetEvent);
            //        AppDomain.CurrentDomain.Load(NhibernateParams.Mappings[i].Trim());
            //    }
            //    catch (Exception e)
            //    {
            //    }
            //    //finally
            //    //{
            //    //    resetEvent.Set();
            //    //}
            //}

            ////WaitHandle.WaitAll(events.ToArray());

            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

            if (NhibernateParams.Mappings.Length == 0)
                throw new Exception("Mappings no configurado");

            for (int i = 0; i < NhibernateParams.Mappings.Length; i++)
            {
                Assembly assembly = assemblies.Where(t => t.FullName.Contains(NhibernateParams.Mappings[i].Trim())).First();
                mappingConfiguration.FluentMappings.AddFromAssembly(assembly).Conventions.Add(new DateTimeMapsToTimestampConvention());
            }
        }

        //private static void Callback(object param)
        //{
        //    string mappingLibrary = (string)param;
        //    AppDomain.CurrentDomain.Load(mappingLibrary);
        //}

        /// <summary>
        /// Open a session for specific Key
        /// </summary>
        /// <param name="connectionStringKey"></param>
        /// <returns></returns>
        public static ISession OpenSession()
        {
        sessionFactory = SessionFactory();
            ISession session = sessionFactory.OpenSession();
            session.FlushMode = FlushMode.Always;

            if (NhibernateParams.EnableAudit)
                auditInterceptor.SetSessionForAuditInterceptor(sessionFactory.OpenSession());

            return session;
        }

        /// <summary>
        ///
        /// </summary>
        public static void StartHibernate()
        {
            sessionFactory = SessionFactory();
            ISession session = sessionFactory.OpenSession();
            session.FlushMode = FlushMode.Always;

            if (NhibernateParams.EnableAudit)
                auditInterceptor.SetSessionForAuditInterceptor(sessionFactory.OpenSession());

            return;
        }
    }
}