﻿using NHibernate.Criterion;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura.Interface
{
    /// <summary>
    /// Repository base for all. Generic T must be a class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : class
    {
        int Create(T t);

        IList<T> GetByCriteria(DetachedCriteria criteria);

        IList<T> List();

        T GetById(int id);

        bool Delete<T>(int id);

        T Update(T t);
    }
}