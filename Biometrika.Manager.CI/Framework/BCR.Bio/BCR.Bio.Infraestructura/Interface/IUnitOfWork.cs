﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Interface
{
    public interface IUnitOfWork : IDisposable
    {
        TransactionWrapper BeginTransaction();
        void Commit();
        void Rollback();
    }
}
