﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura
{
    public class IdentityTypeRepository : Repository<IdentityType>
    {
        public IList<IdentityType> GetByFilter(IdentityTypeFilterDTO filter)
        {
            if (filter == null)
                return null;

            try
            {
                IList<IdentityType> list = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<IdentityType>()
                                   .Where(x => x.Company.Id == filter.CompanyId)
                                   .OrderBy(x => x.Class).Asc
                                   .OrderBy(x => x.Description).Asc;

                    if (filter.Id > 0)
                        criteria.Where(x => x.Id == filter.Id);
                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.Where(x => x.Description == filter.Description);
                    if (!string.IsNullOrEmpty(filter.DescriptionLike))
                        criteria.WhereRestrictionOn(x => x.Description).IsLike(filter.DescriptionLike + "%");

                    if (!string.IsNullOrEmpty(filter.Class))
                        criteria.Where(x => x.Class == filter.Class);

                    criteria.ReadOnly();
                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<IdentityType>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar IdentityTypeRepository.GetByFilter", e);
                return null;
            }
        }
    }
}
