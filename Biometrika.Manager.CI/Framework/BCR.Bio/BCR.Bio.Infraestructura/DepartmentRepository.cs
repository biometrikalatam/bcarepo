﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura
{
    public class DepartmentRepository : Repository<Department>
    {
        public IList<Department> GetByFilter(DepartmentDTO filter)
        {
            try
            {
                IList<Department> list = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Department>()
                                   .Where(x => x.company.Id == filter.CompanyId)
                                   .OrderBy(x => x.description).Asc;

                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.Where(x => x.description == filter.Description);
                    if (!string.IsNullOrEmpty(filter.DescriptionLike))
                        criteria.WhereRestrictionOn(x => x.description).IsLike(filter.DescriptionLike + "%");

                    criteria.ReadOnly();
                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<Department>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar CentroDeCostosRepository.GetByFilter", e);
                return null;
            }
        }
    }
}
