﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura
{
    public class ContactFormRepository : Repository<ContactForm>
    {
        public IList<ContactForm> GetByFilter(ContactFormFilter filter)
        {
            if (filter == null)
                return null;

            try
            {
                IList<ContactForm> list = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<ContactForm>()
                                    .OrderBy(x => x.Description).Asc;

                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.Where(x => x.Description == filter.Description);
                    if (!string.IsNullOrEmpty(filter.DescriptionLike))
                        criteria.WhereRestrictionOn(x => x.Description).IsLike(filter.DescriptionLike + "%");

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<ContactForm>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar ContactFormRepository.GetByFilter", e);
                return null;
            }
        }
    }
}
