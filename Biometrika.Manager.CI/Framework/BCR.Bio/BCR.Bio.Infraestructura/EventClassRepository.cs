﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.EventClass;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class EventClassRepository : Repository<EventClass>
    {
        public IList<EventClass> GetList(int companyId)
        {
            try
            {
                IList<EventClass> list;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    list = session.QueryOver<EventClass>()
                        .Where(x => x.Company.Id == companyId)
                        .ReadOnly()
                        .List<EventClass>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EventClassRepository -> GetList", e);
                return null;
            }
        }

        public EventClassFilterDTO GetByFilter(EventClassFilterDTO filter)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<EventClass>()
                        .Where(x => x.Company.Id == filter.CompanyId)
                        .OrderBy(x => x.Description).Asc;

                    criteria.ReadOnly();

                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.Where(x => x.Description == filter.Description);
                    if (!string.IsNullOrEmpty(filter.DescriptionLike))
                        criteria.WhereRestrictionOn(x => x.Description).IsLike(filter.DescriptionLike + "%");

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    filter.ResultList = criteria.List<EventClass>()
                                       .Select(x => new EventClassDTO()
                                       {
                                           Id = x.Id,
                                           Description = x.Description
                                       }).ToList();
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EventClassRepository -> GetByFilter", e);
            }

            return filter;
        }

        public string GetHASHByCompanyId(int companyId)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var checksum = session.CreateSQLQuery("select CHECKSUM_AGG(CHECKSUM([description]))"
                                   + " from EventClass"
                                   + " where [company] =" + companyId).UniqueResult();

                    return checksum?.ToString() ?? "";
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar ResidentRepository -> GetHASHByCompanyId", e);
                return null;
            }
        }
    }
}