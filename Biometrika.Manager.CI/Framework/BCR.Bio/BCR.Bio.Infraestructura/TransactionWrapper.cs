﻿using NHibernate;

namespace BCR.Bio.Infraestructura
{
    public class TransactionWrapper
    {
        public ITransaction Transaction { get; set; }
    }
}