﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.Bio.Infraestructura.Configuration;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    /// <summary>
    ///
    /// </summary>
    public class LogRepository : Repository<IBPLog>
    {
       
        /// <summary>
        ///
        /// </summary>
        /// <param name="paginacion"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="level"></param>
        /// <param name="loggerLike"></param>
        /// <param name="messageLike"></param>
        /// <param name="exceptionLike"></param>
        /// <returns></returns>
        public IQueryable<BPLog> ListByFilters(
            IPaginacion paginacion,
            DateTime dateFrom,
            DateTime dateTo,
            string level,
            string loggerLike,
            string messageLike,
            string exceptionLike)
        {
            IQueryable<BPLog> list;
            using (var session = BioNHibernateHelper.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria(typeof(BPLog));

                criteria.Add(Restrictions.Between("datel", dateFrom, dateTo));

                if (!string.IsNullOrEmpty(level))
                    criteria.Add(Restrictions.Eq("levell", level));

                if (!string.IsNullOrEmpty(loggerLike))
                    criteria.Add(Restrictions.Eq("loggerl", loggerLike));

                if (!string.IsNullOrEmpty(messageLike))
                    criteria.Add(Restrictions.Eq("messagel", messageLike));

                if (!string.IsNullOrEmpty(exceptionLike))
                    criteria.Add(Restrictions.Eq("exceptionl", exceptionLike));

                paginacion.Total = RowCount(criteria);          // obtener la cantidad de filas para el paginado d

                criteria.AddOrder(Order.Desc("datel"));

                criteria.SetFirstResult(paginacion.Pagina * paginacion.Cantidad).SetMaxResults(paginacion.Cantidad);

                list = (IQueryable<BPLog>)criteria.List<BPLog>().AsQueryable<BPLog>();
            }
            return list;
        }

       
    }
}