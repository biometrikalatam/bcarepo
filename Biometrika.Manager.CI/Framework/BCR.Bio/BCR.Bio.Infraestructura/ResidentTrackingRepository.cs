﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura
{
    public class ResidentTrackingRepository : Repository<ResidentTracking>
    {
        public IList<ResidentTracking> GetByFilter(ResidentTrackingFilter filter)
        {
            if (filter == null)
                return null;

            try
            {
                Resident residentAlias = null;
                BpIdentity identityAlias = null;
                IList<ResidentTracking> list = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<ResidentTracking>()
                                    .Inner.JoinAlias(x => x.Resident, () => residentAlias)
                                        .Inner.JoinAlias(x => residentAlias.Identity, () => identityAlias)
                                    .Where(x => identityAlias.CompanyIdEnroll.Id == filter.CompanyId)
                                    .And(x => x.EventTime > filter.InitDate)
                                    .And(x => x.EventTime < filter.EndDate.AddDays(1))
                                    .OrderBy(x => x.EventTime).Desc;

                    if (filter.ResidentId > 0)
                        criteria.Where(x => residentAlias.Id == filter.ResidentId);
                    if (!string.IsNullOrEmpty(filter.TypeId))
                        criteria.Where(x => identityAlias.TypeId == filter.TypeId);
                    if (!string.IsNullOrEmpty(filter.ValueId))
                        criteria.WhereRestrictionOn(x => identityAlias.ValueId).IsLike(filter.ValueId + "%");

                    if (filter.PointId > 0)
                        criteria.Where(x => x.Point.Id == filter.PointId);

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<ResidentTracking>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar ResidentTrackingRepository.GetByFilter", e);
                return null;
            }
        }
    }
}
