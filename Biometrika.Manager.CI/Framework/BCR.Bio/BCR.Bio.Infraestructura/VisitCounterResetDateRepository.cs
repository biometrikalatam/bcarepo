﻿using System;
using System.Collections.Generic;
using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate;

namespace BCR.Bio.Infraestructura
{
    public class VisitCounterResetDateRepository : Repository<IVisitCounterResetDate>
    {
        public IList<IVisitCounterResetDate> GetByCompanyId(int companyId)
        {
            try
            {
                IList<IVisitCounterResetDate> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    IQueryOver<IVisitCounterResetDate> HQQuery =
                session.QueryOver<IVisitCounterResetDate>()
                        .Where(k => k.Company.Id == companyId);
                    HQQuery.ReadOnly();
                    list = HQQuery.List<IVisitCounterResetDate>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetVouchersByCompany", e);
                return null;
            }
        }

    }
}
