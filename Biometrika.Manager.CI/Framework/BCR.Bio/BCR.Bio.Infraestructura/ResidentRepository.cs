﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.Bio.Infraestructura.Configuration;
using BCR.System.Log;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.DTO.Filter;
using NHibernate.Transform;
using NHibernate.Util;

namespace BCR.Bio.Infraestructura
{
    

    public class ResidentRepository : Repository<IResident>
    {
        public IResident GetById(int id, bool withVehicles = false, bool withUnity = false, bool withIdentity = false)
        {
            try
            {
                IResident resident;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria criteria = session.CreateCriteria<IResident>().Add(Restrictions.Eq("id", id));
                    criteria.SetReadOnly(true);

                    resident = criteria.UniqueResult<IResident>();

                    if (withVehicles)
                        NHibernateUtil.Initialize(resident.Vehicles);
                        NHibernateUtil.Initialize(resident.HaveVehicles);
                    if (withUnity)
                        NHibernateUtil.Initialize(resident.Unitys);
                    if (withIdentity)
                        NHibernateUtil.Initialize(resident.Identity);
                }
                return resident;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al obtener ResidentRepository => GetById", e);
                return null;
            }
        }

        public IList<IResident> GetByCompanyId(int companyId)
        {
            try
            {
                IList<IResident> list;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    IQueryOver<IResident, BpIdentity> HQQuery =
                    session.QueryOver<IResident>()
                    .JoinQueryOver(c => c.Identity)
                        .Where(k => k.CompanyIdEnroll.Id == companyId);

                    HQQuery.ReadOnly();

                    list = HQQuery.List<IResident>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar [ResidentRepository] GetByCompanyId", e);
                return null;
            }
        }

        public IList<IResident> GetByFilter(Paginacion paginacion, ResidentSearchDTO residentSearchDTO)
        {
            try
            {
                IList<IResident> list;
                BpIdentity bpIdentityAlias = null;                
                Unity unityAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Resident>()
                                   .Left.JoinAlias(x => x.Identity, () => bpIdentityAlias)
                                   .Left.JoinAlias(x => x.Unitys, () => unityAlias)
                                   .Where(x => bpIdentityAlias.CompanyIdEnroll.Id == residentSearchDTO.Company);
                    criteria.ReadOnly();

                    if (!string.IsNullOrEmpty(residentSearchDTO.TypeId))
                        criteria.And(x => bpIdentityAlias.TypeId == residentSearchDTO.TypeId);
                    if (!string.IsNullOrEmpty(residentSearchDTO.ValueId))
                        criteria.And(x => bpIdentityAlias.ValueId == residentSearchDTO.ValueId);
                    if (!string.IsNullOrEmpty(residentSearchDTO.Name))
                        criteria.And(x => bpIdentityAlias.Name.IsLike( residentSearchDTO.Name.Trim()));
                    if (!string.IsNullOrEmpty(residentSearchDTO.LastName))
                        criteria.And(() => bpIdentityAlias.PatherLastname.IsLike('%'+residentSearchDTO.LastName.Trim() +'%') || bpIdentityAlias.MotherLastname.IsLike('%'+ residentSearchDTO.LastName.Trim() + '%'));
                    
                    if (!string.IsNullOrEmpty(residentSearchDTO.Address))
                        criteria.And(x => unityAlias.Address.IsLike('%' + residentSearchDTO.Address.Trim() + '%' ));

                    paginacion.Total = criteria.RowCount();
                    criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                    criteria.Take(paginacion.Cantidad);


                    list = criteria.List<IResident>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByFilter", e);
                return null;
            }
        }

        public IList<Resident> GetByUnity(int unityId, Paginacion paginacion)
        {
            if (paginacion == null)
                paginacion = new Paginacion() { Pagina = 0, Cantidad = 1 };

            try
            {
                IList<Resident> list;
                BpIdentity bpIdentityAlias = null;
                Unity unityAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Resident>()
                                    .Left.JoinAlias(x => x.Identity, () => bpIdentityAlias)
                                    .Left.JoinAlias(x => x.Unitys, () => unityAlias)
                                    .Where(x => unityAlias.Id == unityId);

                    criteria.ReadOnly();

                    criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                    criteria.Take(paginacion.Cantidad);

                    list = criteria.List<Resident>();

                    paginacion.Total = criteria.RowCount();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByUnity", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="apellidoPaterno"></param>
        /// <param name="nombre"></param>
        /// <param name="typeId"></param>
        /// <param name="numberId"></param>
        /// <returns></returns>
        public IList<IResident> GetByCompanyAndIdentity(int companyId, string apellidoPaterno = "", string nombre = "", string typeId = "", string numberId = null)
        {
            try
            {
                IList<IResident> list;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    IQueryOver<IResident, BpIdentity> HQQuery = session.QueryOver<IResident>()
                                                                .JoinQueryOver(c => c.Identity)
                                                                .Where(k => k.CompanyIdEnroll.Id == companyId);
                    HQQuery.ReadOnly();

                    if (!string.IsNullOrEmpty(apellidoPaterno))
                        HQQuery.And(k => k.PatherLastname == apellidoPaterno);

                    if (!string.IsNullOrEmpty(nombre))
                        HQQuery.And(k => k.Name == nombre);

                    if (!string.IsNullOrEmpty(typeId) && !string.IsNullOrEmpty(numberId))
                    {
                        HQQuery.And(k => k.TypeId == typeId);
                        HQQuery.And(k => k.ValueId == numberId);
                    }

                    //HQQuery.c.Add(Expression.  .Eq("companyid", idCompany));

                    list = HQQuery.List<IResident>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByCompanyAndIdentity", e);
                return null;
            }
        }

        public ResidentFilterDTO GetByCustomFilter(ResidentFilterDTO filter)
        {
            IList<Resident> list = new List<Resident>();

            try
            {
                BpIdentity bpIdentityAlias = null;
                Unity unityAlias = null;
                Building buildingAlias = null;
                Zone zoneAlias = null;
                HQ hqAlias = null;
                ResidentAuthorizator residentAuthorizationAlias = null;
                Resident residentAlias = null;
                CentroDeCostos centroDeCostoAlias = null;
                IdentityType identityTypeAlias = null;
                IdentityState identityStateAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    #region SubQuery

                    var subQuery = QueryOver.Of<Resident>()
                                .Left.JoinAlias(x => x.Identity, () => bpIdentityAlias)
                                    .Left.JoinAlias(x => bpIdentityAlias.CentrosDeCostoList, () => centroDeCostoAlias)
                                    .Left.JoinAlias(x => bpIdentityAlias.IdentityTypeList, () => identityTypeAlias)
                                       .Left.JoinAlias(x => x.Unitys, () => unityAlias)
                                           .Left.JoinAlias(x => unityAlias.Building, () => buildingAlias)
                                           .Left.JoinAlias(x => unityAlias.Zone, () => zoneAlias)
                                       .Where(x => bpIdentityAlias.CompanyIdEnroll.Id == filter.CompanyId);

                    if (filter.Id > 0)
                        subQuery.And(x => x.Id == filter.Id);
                    if (!string.IsNullOrEmpty(filter.TypeId))
                        subQuery.And(x => bpIdentityAlias.TypeId == filter.TypeId);
                    if (!string.IsNullOrEmpty(filter.ValueId))
                        subQuery.WhereRestrictionOn(x => bpIdentityAlias.ValueId).IsLike(filter.ValueId + "%");
                    if (!string.IsNullOrEmpty(filter.ValueIdLike))
                        subQuery.WhereRestrictionOn(x => bpIdentityAlias.ValueId).IsLike(filter.ValueIdLike + "%");
                    if (!string.IsNullOrEmpty(filter.Name))
                        subQuery.And(x => bpIdentityAlias.Name.IsLike('%' + filter.Name + '%'));
                    if (!string.IsNullOrEmpty(filter.FatherLastName))
                        subQuery.And(x => bpIdentityAlias.PatherLastname.IsLike('%' + filter.FatherLastName + '%'));
                    if (filter.IdentityId > 0)
                        subQuery.Where(x => x.Identity.Id == filter.IdentityId);

                    if (filter.IsAuthorizator)
                    {
                        subQuery.JoinAlias(x => x.ResidentAuthorizators, () => residentAuthorizationAlias);
                    }

                    if (!string.IsNullOrEmpty(filter.FullName)) // TODO: Sabemos que nos da cagaso tremendo hacer busquedas por like a los dos lados, este si lo hcie yo para mantener al consistencia, no esoty de acuerdo
                        subQuery.WhereRestrictionOn(x => bpIdentityAlias.FullName).IsLike("%" + filter.FullName + "%");
                    if (!string.IsNullOrEmpty(filter.FatherLastNameLike))
                        subQuery.WhereRestrictionOn(x => bpIdentityAlias.PatherLastname).IsLike(filter.FatherLastNameLike + "%");
                    if (!string.IsNullOrEmpty(filter.FullApellidosLike))
                        subQuery.WhereRestrictionOn(x => bpIdentityAlias.FullApellidos).IsLike("%" + filter.FullApellidosLike + "%");

                    // filtrado por Unity
                    if (filter.UnityId > 0)
                    {
                        subQuery.And(x => unityAlias.Id == filter.UnityId);
                    }
                    else if (!string.IsNullOrEmpty(filter.UnityFullAddress))
                    {
                        subQuery.And(x => unityAlias.Address == filter.UnityFullAddress);
                    }

                    // filtrado por Building
                    if (filter.BuildingId > 0)
                        subQuery.And(x => buildingAlias.Id == filter.BuildingId);

                    // filtrado por Zone
                    if (filter.ZoneId > 0)
                        subQuery.And(x => zoneAlias.Id == filter.ZoneId);

                    if (filter.HQList != null && filter.HQList.Any())
                        subQuery.Inner.JoinAlias(x => zoneAlias.HQ, () => hqAlias)
                            .WhereRestrictionOn(x => hqAlias.Id).IsIn(filter.HQList.Select(i => i.Id).ToList());

                    // filtrado por CentroDeCostos
                    if (filter.CentroDeCosto != null)
                    {
                        if (filter.CentroDeCosto.Id > 0)
                            subQuery.Where(x => centroDeCostoAlias.Id == filter.CentroDeCosto.Id);
                    }

                    // filtrado por IdentityType
                    if (filter.IdentityType != null)
                    {
                        if (filter.IdentityType.Id > 0)
                            subQuery.Where(x => identityTypeAlias.Id == filter.IdentityType.Id);
                    }

                    // filtrado por IdentityState
                    if (filter.IdentityState != null)
                    {
                        subQuery.Inner.JoinAlias(x => bpIdentityAlias.IdentityStateList, () => identityStateAlias);

                        if (filter.IdentityState.Id > 0)
                            subQuery.Where(x => identityStateAlias.Id == filter.IdentityState.Id);
                        else
                        {
                            subQuery.Where(x => identityStateAlias.Class == filter.IdentityState.Class)
                            .Where(x => identityStateAlias.Company.Id == filter.CompanyId)
                            .Where(x => identityStateAlias.Tag == filter.IdentityState.Tag);
                        }
                    }

                    subQuery.Select(x => x.Id);

                    #endregion

                    bpIdentityAlias = null;
                    var criteria = session.QueryOver<Resident>(() => residentAlias)
                        .Inner.JoinAlias(x => x.Identity, () => bpIdentityAlias)
                        .WithSubquery.WhereProperty(x => x.Id).In(subQuery)
                        .OrderBy(x => bpIdentityAlias.FullName).Asc
                        .ReadOnly();

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<Resident>();

                    if (filter.WithUnity)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Unitys));
                    if (filter.WithVehicle)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Vehicles));

                    filter.ResultList = list;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar ResidentRepository -> GetByCustomFilter", e);
                return null;
            }

            return filter;
        }

        public string GetHASHByCompanyId(int companyId)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var checksum = session.CreateSQLQuery("select CHECKSUM_AGG(CHECKSUM(RES.id, IDE.typeid, IDE.valueid, IDE.name, IDE.patherlastname, IDE.motherlastname, IDE.companyidenroll))"
                    + " FROM Resident RES"
                    + " left join bp_identity IDE"
                    + " on RES.[identity] = IDE.id"
                    + " inner join ResidentAuthorizator RESA"
                    + " on RESA.resident = RES.id"
                    + " where IDE.companyidenroll = " + companyId).UniqueResult();

                    return checksum?.ToString() ?? "";
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar ResidentRepository -> GetHASHByCompanyId", e);
                return null;
            }
        }
    }
}