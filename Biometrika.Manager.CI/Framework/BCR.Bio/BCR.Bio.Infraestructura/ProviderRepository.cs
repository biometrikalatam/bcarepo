﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class ProviderRepository : Repository<Provider>
    {
        /// <summary>
        /// Repositorio con los métodos 
        /// </summary>
        /// <param name="paginacion"></param>
        /// <param name="company"></param>
        /// <param name="name"></param>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public IList<Provider> GetProvidersBy(Paginacion paginacion, ICompany company, string name, string identifier, bool withVehicles = false, bool withVisitors = false, bool onlyActive = false)
        {
            IList<Provider> list = null;
            string msg;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    //Provider providerAlias = null;
                    //BpIdentity identityVisitorAlias = null;

                    var results = session.QueryOver<Provider>();
                    //.Inner.JoinAlias(x => x.Provider, () => providerAlias)
                    //.Inner.JoinAlias(x => x.Identity, () => identityVisitorAlias)
                    //.Where(x => identityVisitorAlias.CompanyIdEnroll.Id == company.Id);
                    results.ReadOnly();
                    results.And(x => x.Company == company);
                    if (!string.IsNullOrEmpty(name))
                        results.And(x => x.Name.IsLike(name));

                    if (!string.IsNullOrEmpty(identifier))
                        results.And(x => x.IdentifierCode == identifier);

                    if (onlyActive)
                        results.Where(x => x.EndDate == null);

                    results.OrderBy(x => x.Name).Asc();

                    if (paginacion != null)
                    {
                        results.Skip(paginacion.Pagina * paginacion.Cantidad);
                        results.Take(paginacion.Cantidad);
                        paginacion.Total = results.RowCount();
                    }                    

                    list = results.List<Provider>().ToList();

                    if (withVehicles)
                        list.ForEach(x => NHibernateUtil.Initialize(x.VehicleList));
                    if (withVisitors)
                        list.ForEach(x => NHibernateUtil.Initialize(x.VisitorList));
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("GetProvidersBy", ex);
            }

            return list;
        }

        /// <summary>
        /// Retorna todos los proveedores asociados a una compañía
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public IList<Provider> GetByCompany(ICompany company)
        {
            IList<Provider> list = null;
            string msg;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var results = session.QueryOver<Provider>();
                    results.ReadOnly();
                    results.And(x => x.Company == company);

                    results.OrderBy(x => x.Name).Asc();

                    list = results.List<Provider>().ToList();
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("GetByCompanyId", ex);
            }

            return list;
        }

        public Provider GetById(int id, bool withVisitor = false, bool withVehicle = false)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Provider>()
                                .Where(x => x.Id == id);

                    var result = criteria.List<Provider>().First();

                    if (withVisitor)
                        NHibernateUtil.Initialize(result.VisitorList);
                    if (withVehicle)
                        NHibernateUtil.Initialize(result.VehicleList);

                    return result;
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("GetByCompanyId", ex);
                return null;
            }
        }

        public IList<Provider> GetByCustomFilter(ProviderFilterDTO filter)
        {
            IList<Provider> list = new List<Provider>();

            try
            {
                Provider providerAlias = null;
                BpIdentity bpIdentityAlias = null;
                Visitor visitorAlias = null;
                Vehicle vehicle = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var subQuery = QueryOver.Of<Provider>()
                        .Left.JoinAlias(x => x.VehicleList, () => vehicle)
                        .Left.JoinAlias(x => x.VisitorList, () => visitorAlias)
                        .Left.JoinAlias(x => visitorAlias.Identity, () => bpIdentityAlias)
                        .Where(c => c.Company.Id == filter.CompanyId);

                    if (filter.OnlyActive)
                        subQuery.Where(x => x.EndDate == null);
                    if (!String.IsNullOrEmpty(filter.PlateNumber))
                        subQuery.Where(x => vehicle.PlateNumber == filter.PlateNumber);
                    if (!string.IsNullOrEmpty(filter.TypeId))
                        subQuery.Where(x => bpIdentityAlias.TypeId == filter.TypeId);
                    if (!string.IsNullOrEmpty(filter.ValueId))
                        subQuery.Where(x => bpIdentityAlias.ValueId == filter.ValueId);
                    if (!string.IsNullOrEmpty(filter.Name))
                        subQuery.Where(x => x.Name == filter.Name);
                    if (!string.IsNullOrEmpty(filter.NameLike))
                        subQuery.WhereRestrictionOn(x => x.Name).IsLike(filter.NameLike + "%");
                    if (!string.IsNullOrEmpty(filter.IdentifierCode))
                        subQuery.Where(x => x.IdentifierCode == filter.IdentifierCode);

                    subQuery.Select(x => x.Id);

                    var criteria = session.QueryOver<Provider>(() => providerAlias)
                        .WithSubquery.WhereProperty(x => x.Id).In(subQuery)
                        .ReadOnly();

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<Provider>();
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar ResidentRepository -> GetByCustomFilter", e);
                return null;
            }

            return list;
        }

        public string GetHASHByCompanyId(int companyId)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var checksum = session.CreateSQLQuery("SELECT CHECKSUM_AGG(CHECKSUM(PRO.id, PRO.name)) "
                    + "FROM Provider PRO "
                    + "where PRO.company =  ?")
                    .SetInt32(0, companyId)
                    .UniqueResult();

                    return checksum?.ToString() ?? "";
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar ResidentRepository -> GetHASHByCompanyId", e);
                return null;
            }
        }

    }
}