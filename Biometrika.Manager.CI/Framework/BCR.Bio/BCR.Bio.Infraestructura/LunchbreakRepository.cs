﻿using System;
using System.Collections.Generic;
using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate.Criterion;

namespace BCR.Bio.Infraestructura
{
    public class LunchbreakRepository : Repository<ILunchbreak>
    {
        public IList<ILunchbreak> GetByCompanyId(int companyId)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var list = session.QueryOver<ILunchbreak>()
                       .Where(x => x.Company.Id == companyId);
                    return list.List<ILunchbreak>();
                }

            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetLunchbreaksByCompany", e);
                return null;
            }
        }
    }
}
