﻿using BCR.Bio.Infraestructura.Audit;
using BCR.Bio.Infraestructura.Mappings;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace BCR.Bio.Infraestructura
{
    /// <summary>
    /// NHibernate entry point
    /// </summary>
    public class NHibernateHelper
    {
        private static ISessionFactory sessionFactory = null;
        //private static AuditInterceptor auditInterceptor = new AuditInterceptor();

        /// <summary>
        /// Get a session for specific connection string key
        /// </summary>
        /// <param name="connectionStringKey"></param>
        /// <returns></returns>
        private static ISessionFactory SessionFactory(string connectionString)
        {
            if (sessionFactory == null)
            {
                sessionFactory = Fluently.Configure()
                    .Database(MsSqlConfiguration.MsSql2008.ConnectionString(connectionString))
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<CompanyMap>())
                    //.ExposeConfiguration(m => { m.SetInterceptor(auditInterceptor); })
                    .BuildSessionFactory();
            }

            return sessionFactory;
        }

        /// <summary>
        /// Open a session for specific Key
        /// </summary>
        /// <param name="connectionStringKey"></param>
        /// <returns></returns>
        public static ISession OpenSession(string connectionString)
        {
            ISession session = SessionFactory(connectionString).OpenSession();
            //auditInterceptor.SetSessionForAuditInterceptor(session);
            return session;
        }
    }

}