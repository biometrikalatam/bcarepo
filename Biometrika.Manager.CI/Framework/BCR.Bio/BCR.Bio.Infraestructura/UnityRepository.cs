﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Extension;
using BCR.System.Log;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class UnityRepository : Repository<Unity>
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <param name="withResidents"></param>
        /// <returns></returns>
        //public Unity GetById(int id, bool withResidents = false, bool withVisitor = false)
        public Unity GetById(int id, bool withResidents = false)
        {
            try
            {
                Unity item;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria criteria = session.CreateCriteria<Unity>()
                        .Add(Restrictions.Eq("id", id));

                    criteria.SetReadOnly(true);
                    item = criteria.UniqueResult<Unity>();

                    if (withResidents)
                        NHibernateUtil.Initialize(item.Residents);
                    else
                        item.Residents = null;

                    //if (withVisitor)
                    //    NHibernateUtil.Initialize(item.Visitors);
                    //else
                    //    item.Visitors = null;
                }
                return item;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar UnityRepository -> GetById", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public IList<Unity> GetByZone(int zoneId, Paginacion paginacion = null)
        {
            if (paginacion == null)
                paginacion = new Paginacion() { Pagina = 0, Cantidad = 1 };

            try
            {
                IList<Unity> list;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Unity>()
                      .Where(x => x.Zone.Id == zoneId)
                      .And(x => x.Building == null);
                    criteria.ReadOnly();

                    criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                    criteria.Take(paginacion.Cantidad);

                    list = criteria.List<Unity>();

                    paginacion.Total = criteria.RowCount();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar UnityRepository -> GetByZone", e);
                return null;
            }
        }

        public string GetHASHByCompanyId(int companyId)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var checksum = session.CreateSQLQuery("select CHECKSUM_AGG(CHECKSUM(UNI.[address])) from Unity UNI"
                                                          + " left join Zone ZO"
                                                          + " on UNI.zone = ZO.id"
                                                          + " left join HQ HQ"
                                                          + " on HQ.id = ZO.HQ"
                                                          + " left join bp_company COM"
                                                          + " on HQ.company = COM.id"
                                                          + " where COM.id = " + companyId).UniqueResult();

                    return checksum?.ToString() ?? "";
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar UnityRepository -> GetHASHByCompanyId", e);
                return null;
            }
        }

        public string GetHASHByCompanyIdOnlyAuthorized(int companyId)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var checksum = session.CreateSQLQuery("select CHECKSUM_AGG(CHECKSUM(UNI.[address]))"
                                                          + " from Unity UNI"
                                                          + " where [id] in (select UNI.[id]"
                                                          + " from Unity UNI"
                                                          + " left join Zone ZO"
                                                          + " on UNI.zone = ZO.id"
                                                          + " left join HQ HQ"
                                                          + " on HQ.id = ZO.HQ"
                                                          + " left join bp_company COM"
                                                          + " on HQ.company = COM.id"
                                                          + " inner join ResidentUnity RU"
                                                          + " on RU.unity = UNI.id"
                                                          + " inner join ResidentAuthorizator RA"
                                                          + " on RA.resident = RU.resident"
                                                          + " where COM.id = " + companyId
                                                          + " group by UNI.[id])").UniqueResult();

                    return checksum?.ToString() ?? "";
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar UnityRepository -> GetHASHByCompanyId", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="buildingId"></param>
        /// <returns></returns>
        public IList<Unity> GetByBuilding(int buildingId, Paginacion paginacion = null)
        {
            if (paginacion == null)
                paginacion = new Paginacion() { Pagina = 0, Cantidad = 1 };

            try
            {
                IList<Unity> list;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Unity>()
                      .Where(x => x.Building.Id == buildingId);
                    criteria.ReadOnly();

                    criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                    criteria.Take(paginacion.Cantidad);

                    list = criteria.List<Unity>();

                    paginacion.Total = criteria.RowCount();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar UnityRepository -> GetByBuilding", e);
                return null;
            }
        }

        /// <summary>
        /// Busqueda de unity por filtro, siempre hay que enviar el Unity.Zone.HQ.Company.Id, es un filtro por defecto
        /// </summary>
        /// <param name="filter"></param>
        /// <returns>Listado de Unity: IList<Unity></Unity></returns>
        public IList<Unity> GetByFilter(Unity filter)
        {
            IList<Unity> list;

            try
            {
                Zone zoneAlias = null;
                HQ hqAlias = null;
                Building buildingAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Unity>()
                                    .Left.JoinAlias(x => x.Zone, () => zoneAlias)
                                        .Left.JoinAlias(x => zoneAlias.HQ, () => hqAlias)
                                    .Left.JoinAlias(x => x.Building, () => buildingAlias)
                                    .Where(x => hqAlias.Company.Id == filter.Zone.HQ.Company.Id);

                    criteria.ReadOnly();
                    // filtrado por Building
                    if (filter.Building != null)
                    {
                        if (filter.Building.Id > 0)
                        {
                            criteria.Where(x => buildingAlias.Id == filter.Building.Id);
                        }
                    }

                    // filtrado por Zone
                    if (filter.Zone != null)
                    {
                        if (filter.Zone.Id > 0)
                        {
                            criteria.Where(x => zoneAlias.Id == filter.Zone.Id);
                        }
                    }

                    list = criteria.List<Unity>();
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar UnityRepository -> GetByBuilding", e);
                return null;
            }

            return list;
        }

        public UnityFilterDTO GetByCustomFilter(UnityFilterDTO filter)
        {
            IList<Unity> list = new List<Unity>();

            try
            {
                Zone zoneAlias = null;
                HQ hqAlias = null;
                Building buildingAlias = null;
                Resident residentAlias = null;
                Employee employeeAlias = null;
                BpIdentity identityAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Unity>()
                                    .Left.JoinAlias(x => x.Zone, () => zoneAlias)
                                        .Left.JoinAlias(x => zoneAlias.HQ, () => hqAlias)
                                    .Left.JoinAlias(x => x.Building, () => buildingAlias)
                                    .Where(x => hqAlias.Company.Id == filter.CompanyId)
                                    .OrderBy(x => x.Address).Asc;

                    criteria.ReadOnly();

                    if (!string.IsNullOrEmpty(filter.Address))
                        criteria.Where(x => x.Address == filter.Address);
                    if (!string.IsNullOrEmpty(filter.AddressLike))
                        criteria.WhereRestrictionOn(x => x.Address).IsLike("%" + filter.AddressLike + "%");
                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.Where(x => x.Description == filter.Description);

                    // filtrado por Building
                    if (filter.BuildingId > 0)
                        criteria.And(x => buildingAlias.Id == filter.BuildingId);
                    if (!string.IsNullOrEmpty(filter.BuildingDescription))
                        criteria.Where(x => buildingAlias.Description == filter.BuildingDescription);

                    // filtrado por Zone
                    if (filter.ZoneId > 0)
                        criteria.And(x => zoneAlias.Id == filter.ZoneId);
                    if (!string.IsNullOrEmpty(filter.ZoneDescription))
                        criteria.Where(x => zoneAlias.Description == filter.ZoneDescription);

                    // filtrado por Resident
                    if (filter.ResidentId > 0)
                    {
                        criteria.Left.JoinAlias(x => x.Residents, () => residentAlias)
                                .And(x => residentAlias.Id == filter.ResidentId);
                    }

                    // filtrado por EmployeeId
                    if (filter.EmployeeId > 0)
                    {
                        criteria.Left.JoinAlias(x => x.Employees, () => employeeAlias)
                                .And(x => employeeAlias.Id == filter.EmployeeId);
                    }

                    // Filtrado por EmployeeFullName
                    if (!string.IsNullOrEmpty(filter.EmployeeFullName))
                        criteria.Left.JoinAlias(x => x.Employees, () => employeeAlias)
                            .Inner.JoinAlias(x => employeeAlias.Identity, () => identityAlias)
                            .Where(x => identityAlias.FullName == filter.EmployeeFullName);

                    // filtrado por HQ
                    if (filter.HQList != null && filter.HQList.Any())
                        criteria.WhereRestrictionOn(x => hqAlias.Id).IsIn(filter.HQList.Select(p => p.Id).ToList());

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                    }

                    list = criteria.List<Unity>();

                    if (filter.Paginacion != null)
                    {
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    filter.ResultList = list;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar UnityRepository -> GetByCustomFilter", e);
                return null;
            }

            return filter;
        }
    }
}