﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class UserRepository : Repository<IUser>
    {
       
        /// <summary>
        ///
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        
        public IUser GetByusername(string username)
        {
            IUser user;
            Log4Bio.Info(string.Format("IUser  ->  GetByusername({0})", username));

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria crit = session.CreateCriteria<User>();
                    crit.SetReadOnly(true);
                    if (!string.IsNullOrEmpty(username))
                    {
                        crit.Add(Expression.Eq("Username", username));
                        user = crit.List<IUser>().FirstOrDefault();
                    }
                    else
                        user = null;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar el Username", e);
                Log4Bio.Error("Error al recuperar el Username", e.InnerException);
                user = null;
            }
            return user;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="idCompany"></param>
        /// <returns></returns>
        public IList<IUser> GetUsersByCompany(int idCompany)
        {
            Log4Bio.Info(string.Format("IList<IUser>  ->  GetUsersByCompany({0})", idCompany));

            try
            {
                IList<IUser> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria crit = session.CreateCriteria<User>();
                    crit.SetReadOnly(true);
                    crit.CreateAlias("Companies", "companies");
                    crit.Add(Restrictions.Eq("companies.id", idCompany));

                    list = crit.List<IUser>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetUsersByCompany", e);
                return null;
            }
        }

        public IList<UserDTO> GetUsersByCompanyLigth(int idCompany)
        {
            Log4Bio.Info(string.Format("IList<UserDTO>  ->  GetUsersByCompanyLigth({0})", idCompany));

            try
            {
                IList<UserDTO> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria crit = session.CreateCriteria<UserDTO>();
                    crit.SetReadOnly(true);
                    crit.CreateAlias("Companies", "companies");
                    crit.Add(Restrictions.Eq("companies.id", idCompany));

                    list = crit.List<UserDTO>().OrderBy(m => m.Username).ToList();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetUsersByCompanyLigth", e);
                return null;
            }
        }

        public IList<IUser> GetUsersWthoutEmployeeAssignedByCompany(int idCompany)
        {
            Log4Bio.Info(string.Format("IList<UserDTO>  ->  GetUsersWthoutEmployeeAssignedByCompany({0})", idCompany));

            try
            {
                IList<IUser> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria crit = session.CreateCriteria<User>();
                    crit.SetReadOnly(true);
                    crit.CreateAlias("Companies", "companies");
                    crit.Add(Restrictions.Eq("companies.id", idCompany));
                    crit.Add(Restrictions.IsNull("Employee"));
                    list = crit.List<IUser>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetUsersByCompany", e);
                return null;
            }
        }

        /// <summary>
        /// Método de consulta de Usuarios en base a su username e id de company
        /// </summary>
        /// <param name="paginacion">Objeto de paginado</param>
        /// <param name="companyId">Id de company a buscar</param>
        /// <param name="username">Nombre de usuario a buscar</param>
        /// <returns>Listado de user encontrados o nulo</returns>
        public IList<IUser> GetUsersByCompanyUsername(Paginacion paginacion, int idCompany, string username)
        {
            Log4Bio.Info(string.Format("IList<IUser>  ->  GetUsersByCompanyUsername(p,{0},{1})", idCompany, username));

            try
            {
                IList<IUser> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria crit = session.CreateCriteria(typeof(User));
                    crit.SetReadOnly(true);
                    if (idCompany > 0)
                    {
                        crit.CreateAlias("Companies", "companies");
                        crit.Add(Restrictions.Eq("companies.id", idCompany));
                    }

                    if (!string.IsNullOrEmpty(username))
                    {
                        //crit.Add(Expression.Like("Username", username + "%"));
                        crit.Add(Expression.Eq("Username", username ));
                    }

                    paginacion.Total = RowCount(crit);          // obtener la cantidad de filas para el paginado d

                    crit.AddOrder(Order.Desc("id"));

                    if (paginacion.Cantidad != int.MaxValue)
                        crit.SetFirstResult(paginacion.Pagina * paginacion.Cantidad).SetMaxResults(paginacion.Cantidad);

                    list = crit.List<IUser>();
                }
                return list;
            }
            catch (Exception e)
            {

                Log4Bio.Error("Error al recuperar GetUsersByCompanyUsername", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public IList<IUser> GetUsersByEmail(string email)
        {
            Log4Bio.Info(string.Format("IList<IUser>  ->  GetUsersByEmail({0})", email));

            try
            {
                IList<IUser> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria crit = session.CreateCriteria(typeof(User));
                    crit.SetReadOnly(true);
                    if (!string.IsNullOrEmpty(email))
                    {
                        crit.Add(Expression.Eq("Email", email));
                    }

                    list = crit.List<IUser>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar el GetUsersByEmail", e);
                return null;
            }
        }

        public IUser GetUsersByUsernameAndEmail(string username, string email)
        {
            Log4Bio.Info(string.Format("IList<IUser>  ->  GetUsersByUsernameAndEmail({0}, {1})", username, email));

            IUser user = null;
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    user = session.QueryOver<User>()
                            .Where(x => x.Username == username)
                            .And(x => x.Email == email)
                            .List<User>()
                            .FirstOrDefault();
                }
                return user;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar el GetUsersByUsernameAndEmail", e);
                return user;
            }
        }

        // TODO: Solo para etapa 1 Lomas la Dehesa, quitar al pasar a la Etapa 2
        public int CreateExtendedData(int userId, string decryptedPassword)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    User user = session.QueryOver<User>()
                                .Where(x => x.Id == userId)
                                .List<User>()
                                .FirstOrDefault();

                    if (user == null)
                        return -1;

                    UserExtended userExtended = new UserExtended
                    {
                        User = user,
                        DecryptedPassword = decryptedPassword
                    };

                    int res = (int)session.Save(userExtended);
                    session.Flush();
                    return res;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error CreateExtendedData", e);
                return -1;
            }
        }

        // TODO: Solo para etapa 1 Lomas la Dehesa, quitar al pasar a la Etapa 2
        public int UpdateExtendedData(int userId, string decryptedPassword)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    UserExtended userExtended = session.QueryOver<UserExtended>()
                                                .Where(x => x.User.Id == userId)
                                                .List<UserExtended>()
                                                .FirstOrDefault();

                    if (userExtended == null)
                        return -1;

                    userExtended.DecryptedPassword = decryptedPassword;

                    session.Update(userExtended);
                    session.Flush();
                    return userExtended.Id;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error UpdateExtendedData", e);
                return -1;
            }
        }

        // TODO: Solo para etapa 1 Lomas la Dehesa, quitar al pasar a la Etapa 2
        public IList<User> GetOperadoresByCompanyId(int companyId)
        {
            try
            {
                IList<User> list;
                Company companyAlias = null;
                Rol rolAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    list = session.QueryOver<User>()
                           .Inner.JoinAlias(x => x.Companies, () => companyAlias)
                           .Inner.JoinAlias(x => x.Roles, () => rolAlias)
                           .Where(x => companyAlias.Id == companyId)
                           .And(x => rolAlias.Name == "Operador")
                           .List<User>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar el GetUsersByEmail", e);
                return null;
            }
        }

        // TODO: Solo para etapa 1 Lomas la Dehesa, quitar al pasar a la Etapa 2
        public UserExtended GetUserExtended(int userId)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    UserExtended userExtended = session.QueryOver<UserExtended>()
                                                .Where(x => x.User.Id == userId)
                                                .List<UserExtended>()
                                                .FirstOrDefault();

                    return userExtended;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar el GetUserExtended", e);
                return null;
            }
        }

        public IList<User> GetUsersByRolAndCompany(int rolId, int companyId)
        {
            try
            {
                IList<User> list;
                Company companyAlias = null;
                Rol rolAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    list = session.QueryOver<User>()
                           .Inner.JoinAlias(x => x.Companies, () => companyAlias)
                           .Inner.JoinAlias(x => x.Roles, () => rolAlias)
                           .Where(x => companyAlias.Id == companyId)
                           .And(x => rolAlias.Id == rolId)
                           .List<User>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar el GetUsersByEmail", e);
                return null;
            }
        }
    }
}