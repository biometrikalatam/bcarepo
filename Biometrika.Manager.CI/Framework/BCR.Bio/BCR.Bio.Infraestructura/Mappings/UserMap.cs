﻿using BCR.Bio.Domain;
using FluentNHibernate.Conventions.Inspections;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("[User]");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Username).Column("username");
            Map(x => x.Email).Column("email");
            Map(x => x.Password).Column("password");
            Map(x => x.IsApproved).Column("isApproved");
            Map(x => x.IsLockedOut).Column("isLockedOut");
            Map(x => x.PasswordSalt).Column("passwordSalt");
            Map(x => x.PasswordFormat).Column("passwordFormat");
            Map(x => x.PasswordQuestion).Column("passwordQuestion");
            Map(x => x.PasswordAnswer).Column("passwordAnswer");
            Map(x => x.FailedPasswordAttemptCount).Column("failedPasswordAttemptCount");
            Map(x => x.FailedPasswordAttemptWindowStart).Column("failedPasswordAttemptWindowStart");
            Map(x => x.FailedPasswordAnswerAttemptCount).Column("failedPasswordAnswerAttemptCount");
            Map(x => x.FailedPasswordAnswerAttemptWindowStart).Column("failedPasswordAnswerAttemptWindowStart");
            Map(x => x.LastPasswordChangedDate).Column("lastPasswordChangedDate");
            Map(x => x.LastActivityDate).Column("lastActivityDate");
            Map(x => x.LastLockOutDate).Column("lastLockOutDate");
            Map(x => x.LastLoginDate).Column("lastLoginDate");
            Map(x => x.Comments).Column("comments");
            Map(x => x.Name).Column("name");
            Map(x => x.Surname).Column("surname");
            Map(x => x.Phone).Column("phone");
            Map(x => x.PostalCode).Column("postalCode");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");

            //HasMany(x => x.Roles).Cascade.All();

            HasManyToMany(x => x.Roles)
                    .Cascade.All()
                    .Table("UserRol")
                    .ParentKeyColumn("userId")
                    .ChildKeyColumn("rolId");

            HasManyToMany(x => x.Companies)
                    .Cascade.All()
                    .Table("CompanyUser")
                    .ParentKeyColumn("userid")
                    .ChildKeyColumn("company");

            //References(x => x.Company, "CompanyId").Class(typeof(BCR.Bio.Domain.Company));
        }
    }
}