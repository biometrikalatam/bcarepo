﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class DeptMap : ClassMap<Dept>
    {
        public DeptMap()
        {
            Table("Dept");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");

            References(x => x.Country, "idCountry").Unique();

            HasMany(x => x.Cities)
                .Cascade
                .None()
                .Table("City")
                .KeyColumn("idDept"); 
        }
    }
}