﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class BpIdentityMap : ClassMap<BpIdentity>
    {
        public BpIdentityMap()
        {
            Table("bp_identity");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Typeid).Column("typeid");
            Map(x => x.Valueid).Column("valueid");
            Map(x => x.Nick).Column("nick");
            Map(x => x.Name).Column("name");
            Map(x => x.Patherlastname).Column("patherlastname");
            Map(x => x.Motherlastname).Column("motherlastname");
            Map(x => x.Sex).Column("sex");
            Map(x => x.Documentseriesnumber).Column("documentseriesnumber");
            Map(x => x.Documentexpirationdate).Column("documentexpirationdate");
            Map(x => x.Visatype).Column("visatype");
            Map(x => x.Birthdate).Column("birthdate");
            
            Map(x => x.Birthplace).Column("birthplace");
            Map(x => x.Nationality).Column("nationality");
            Map(x => x.Photography).Column("photography");
            Map(x => x.Signatureimage).Column("signatureimage");
            Map(x => x.Profession).Column("profession");
            Map(x => x.Dynamicdata).Column("dynamicdata");
            Map(x => x.Enrollinfo).Column("enrollinfo");
            Map(x => x.Creation).Column("creation");
            Map(x => x.Verificationsource).Column("verificationsource");
            Map(x => x.Useridenroll).Column("useridenroll");

            References(x => x.Companyidenroll).Column("companyidenroll").Class(typeof(BCR.Bio.Domain.Company)); 

            HasMany(x => x.BpBir).Cascade.All();
            HasMany(x => x.BpIdentityDynamicdata).Cascade.All();
        }
    }
}

  
