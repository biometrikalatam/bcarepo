﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class CityMap: ClassMap<City>
    {
        public CityMap()
        {
            
            Table("City");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");

            References(x => x.Dept, "idDept").Unique();

            HasMany(x => x.Towns)
                .Cascade
                .None()
                .Table("Town")
                .KeyColumn("idCity"); 
        }
    }
}
