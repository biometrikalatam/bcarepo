﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class CountryMap : ClassMap<Country>
    {
        public CountryMap()
        {
            Table("Country");
            Id(x => x.Id).GeneratedBy.Identity();
            
            Map(x => x.Description).Column("description");

            HasMany(x => x.Depts)
                .Cascade
                .None()
                .Table("Dept")
                .KeyColumn("idCountry"); 
        }
    }
}