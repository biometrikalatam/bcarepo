﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class RolMap : ClassMap<Rol>
    {
        public RolMap()
        {
            Table("Rol");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Name).Column("name");

            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");

            //HasManyToMany(x => x.UsersInRole)
            //           .Cascade.All()
            //           .Inverse()
            //           .Table("UserRol")
            //           .ParentKeyColumn("Id")
            //           .ChildKeyColumn("rolId");
        }
    }
}