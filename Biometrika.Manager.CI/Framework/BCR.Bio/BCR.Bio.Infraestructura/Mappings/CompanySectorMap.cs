﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class CompanySectorMap : ClassMap<CompanySector>
    {
        public CompanySectorMap()
        {
            Table("CompanySector");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");

            References(x => x.Sector).Column("sector");

            References(x => x.Company).Column("company");

            //Map(x => x.Sector).Column("Sector");
            //Map(x => x.Company).Column("Company");
        }
    }
}