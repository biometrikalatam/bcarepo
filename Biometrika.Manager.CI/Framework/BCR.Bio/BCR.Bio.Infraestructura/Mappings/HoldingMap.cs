﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class HoldingMap : ClassMap<Holding>
    {
        public HoldingMap()
        {
            Table("Holding");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.CreateDate).Column("CreateDate");
            Map(x => x.Endate).Column("Endate");
            Map(x => x.Name).Column("Name");
            Map(x => x.UpdateDate).Column("UpdateDate");
            Map(x => x.Address).Column("Address");

            HasMany(x => x.Companies)
            .Cascade
            .None()
            .Table("Company")
            .KeyColumn("holding");
        }
    }
}
