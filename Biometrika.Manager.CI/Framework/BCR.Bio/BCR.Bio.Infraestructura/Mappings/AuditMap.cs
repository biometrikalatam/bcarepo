﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCR.Bio.Domain;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class AuditMap: ClassMap<Domain.Audit>
    {
        public AuditMap()
        {
            Table("Audit");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Action).Column("Action");
            Map(x => x.AuditDate).Column("AuditDate");
            Map(x => x.Data).Column("Data");
            Map(x => x.ObjectId).Column("Object_Id");
            Map(x => x.ObjectName).Column("Object");
            //References(x => x.Dept, "idDept").Unique();
        }
    }

}
