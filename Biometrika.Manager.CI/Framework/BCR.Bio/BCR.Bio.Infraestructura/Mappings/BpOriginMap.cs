﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class BpOriginMap : ClassMap<BpOrigin>
    {
        public BpOriginMap()
        {
            Table("bp_origin");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");

            //References(x => x.BpIdentity, "identid").Unique();
        }
    }
}