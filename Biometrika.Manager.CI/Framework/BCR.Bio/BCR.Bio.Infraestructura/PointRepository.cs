﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate.Criterion;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class PointRepository : Repository<IPoint>
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="idtrack"></param>
        /// <returns></returns>
        public Point GetBySensorId(int companyId, string sensorId)
        {
            Log4Bio.Info(string.Format("Point->  GetBySensorId({0},{1})", companyId, sensorId));

            try
            {
                Point item = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    HQ hqAlias = null;

                    item = session.QueryOver<Point>()
                      .Inner.JoinAlias(x => x.HQ, () => hqAlias)
                      .Where(x => x.SensorId == sensorId)
                      .And(() => hqAlias.Company.Id == companyId)
                      .ReadOnly()
                      .SingleOrDefault();
                }
                return item;
            }
            catch (Exception ex)
            {
                Log4Bio.Error("GetBySensorId(sensorIds)", ex);
                //ValidationDictionary.AddError("", ex.Message);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<Point> GetByCompanyId(int companyId)
        {
            Log4Bio.Info(string.Format("List<Point> ->  GetByCompanyId({0})", companyId));
            try
            {
                List<Point> list = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    HQ hqAlias = null;

                    list = session.QueryOver<Point>()
                      .Inner.JoinAlias(x => x.HQ, () => hqAlias)
                      .Where(() => hqAlias.Company.Id == companyId)
                      .ReadOnly()
                      .List<Point>().ToList();
                }
                return list;
            }
            catch (Exception ex)
            {
                Log4Bio.Error("GetByCompanyId(companyId)", ex);
                //ValidationDictionary.AddError("", ex.Message);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Point GetBySensorName(string name)
        {
            Log4Bio.Info(string.Format("Point ->  GetBySensorName({0})", name));

            try
            {
                Point item;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    IList list = session.CreateCriteria<Point>().
                    Add(Expression.Eq("Name", name))
                    .SetReadOnly(true)
                    .List();
                    if (list != null && list.Count > 0)
                        item = (Point)list[0];
                    else
                        item = null;
                }
                return item;
            }
            catch (Exception ex)
            {
                Log4Bio.Error("GetBySensorName(name)", ex);
                //ValidationDictionary.AddError("", ex.Message);
                return null;
            }
        }

        public bool SyncPointById(int id)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Point point = session.QueryOver<Point>()
                                    .Where(x => x.Id == id)
                                    .List<Point>()
                                    .FirstOrDefault();

                    if (point == null)
                        return false;

                    point.Dev_refresh = 1;
                    session.Update(point);
                    session.Flush();
                    return true;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error en SyncPointById()", e);
                return false;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public IList<AccessControl> GetAttachedAccessControl(Point point)
        {
            try
            {
                IList<AccessControl> list;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var hql = @"select s
                            from AccessControl s
                            inner join s.Points as c
                            where s.RuleScope = 2
                            and c.Id = :pointId";

                    IList<AccessControl> accessControls = session.CreateQuery(hql)
                    .SetString("pointId", point.Id.ToString())
                    .SetReadOnly(true)
                    .List<AccessControl>();

                    hql = @"select s
                            from  AccessControl s
                            where s.EndDate = null
                            and s.Company = :pointCompanyId
                            and s.RuleScope = 1";
                     
                    IList<AccessControl> accessControlsFull = session.CreateQuery(hql)
                    .SetString("pointCompanyId", point.HQ.Company.Id.ToString())
                    .SetReadOnly(true)
                    .List<AccessControl>();

                    list = accessControls.Union(accessControlsFull).ToList();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetAttachedAccessControl", e);
                return null;
            }
        }

        public Point GetByCompanyIdAndCode(int companyId, string code)
        {
            Log4Bio.Info(string.Format("Point ->  GetByCompanyIdAndCode({0}, {1})", companyId, code));
            try
            {
                Point punto = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    HQ hqAlias = null;
                    punto = session.QueryOver<Point>()
                      .Inner.JoinAlias(x => x.HQ, () => hqAlias)
                      .Where(() => hqAlias.Company.Id == companyId)
                      .And(x => x.Code == code)
                      .ReadOnly()
                      .List<Point>().FirstOrDefault();
                }
                return punto;
            }
            catch (Exception ex)
            {
                Log4Bio.Error("GetByCompanyIdAndCode(companyId, code)", ex);
                //ValidationDictionary.AddError("", ex.Message);
                return null;
            }
        }

        public IList<Point> GetByFilter(PointFilterDTO filter)
        {
            Log4Bio.Debug("PointRepository.GetByFilter");

            try
            {
                IList<Point> list = null;
                HQ hqAlias = null;
                Sector sectorAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Point>()
                                   .Left.JoinAlias(x => x.HQ, () => hqAlias)
                                   .Where(x => hqAlias.Company.Id == filter.CompanyId)
                                   .OrderBy(x => hqAlias.Description).Asc
                                   .OrderBy(x => x.Code).Asc;

                    if (filter.Id > 0)
                        criteria.Where(x => x.Id == filter.Id);

                    if (filter.HQ != null)
                    {
                        if (filter.HQ.Id > 0)
                            criteria.And(x => hqAlias.Id == filter.HQ.Id);
                    }

                    if (filter.Sector != null)
                    {
                        if (filter.Sector.Id > 0)
                            criteria.Left.JoinAlias(x => x.Sector, () => sectorAlias)
                            .And(x => sectorAlias.Id == filter.Sector.Id);
                    }

                    if (!string.IsNullOrEmpty(filter.NameLike))
                        criteria.WhereRestrictionOn(x => x.Name).IsLike(filter.NameLike + "%");
                    if (!string.IsNullOrEmpty(filter.Code))
                        criteria.Where(x => x.Code == filter.Code);
                    if (!string.IsNullOrEmpty(filter.Name))
                        criteria.Where(x => x.Name == filter.Name);

                    criteria.ReadOnly();
                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<Point>();
                }
                return list;
            }
            catch (Exception ex)
            {
                Log4Bio.Error("Error -> PointRepository.GetByFilter", ex);
                return null;
            }
        }

        public Point GetByCompanyAndCode(int companyId, string code)
        {
            if (companyId <= 0)
                return null;
            if (string.IsNullOrEmpty(code))
                return null;

            try
            {
                HQ hqAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Point>()
                                    .Inner.JoinAlias(x => x.HQ, () => hqAlias)
                                    .Where(x => hqAlias.Company.Id == companyId)
                                    .Where(x => x.Code == code);

                    return criteria.List<Point>().FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error -> PointRepository.GetByCompanyAndCode", e);
                return null;
            }
        }

        public bool SyncAccessPoints()
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var checksum = session.CreateSQLQuery("EXEC [dbo].[sp_SyncEmployeeAndBirs]").UniqueResult();
                    return true;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar UnityRepository -> GetHASHByCompanyId", e);
                return false;
            }
        }
    }
}