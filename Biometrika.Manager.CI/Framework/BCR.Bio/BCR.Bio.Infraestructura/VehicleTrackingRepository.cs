﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCR.Bio.Domain.DTO.Filter;
using BCR.System.Log;
using BCR.Bio.Infraestructura;
using NHibernate;

namespace BCR.Bio.Infraestructura
{
    public class VehicleTrackingRepository : Repository<VehicleTracking>
    {
        public List<VehicleTracking> GetByFilter(VehicleTrackingFilter filter)
        {
            if (filter == null)
                return null;

            try
            {
                List<VehicleTracking> list = null;
                Vehicle vehicleAlias = null;
                Resident residentAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<VehicleTracking>()
                                   .Left.JoinAlias(x => x.Vehicle, () => vehicleAlias)
                                   .Where(x => x.Company.Id == filter.CompanyId)
                                   .And(x => x.RecognitionTime > filter.InitDate)
                                   .And(x => x.RecognitionTime < filter.EndDate.AddDays(1))
                                   .OrderBy(x => x.RecognitionTime).Desc;

                    if (!string.IsNullOrEmpty(filter.NumberPlateLike))
                        criteria.WhereRestrictionOn(x => x.NumberPlate).IsLike("%" + filter.NumberPlateLike + "%");
                    if (filter.PointId > 0)
                        criteria.Where(x => x.Point.Id == filter.PointId);

                    if (filter.Tipo == 0)
                        criteria.Left.JoinAlias(x => vehicleAlias.Residents, () => residentAlias);
                    if (filter.Tipo == 1)
                        criteria.Inner.JoinAlias(x => vehicleAlias.Residents, () => residentAlias)
                            .Where(x => vehicleAlias.Residents != null);
                    else if (filter.Tipo == 2)
                        criteria.Left.JoinAlias(x => vehicleAlias.Residents, () => residentAlias)
                            .Where(x => vehicleAlias.Residents == null);

                    if (filter.ResidentId > 0)
                        criteria.Where(x => residentAlias.Id == filter.ResidentId);

                    if (filter.AperturaAutomatica == AperturaAutomaticaEnum.Si)
                        criteria.Where(x => x.IsOpen == true);
                    if (filter.AperturaAutomatica == AperturaAutomaticaEnum.No)
                        criteria.Where(x => x.IsOpen == null || x.IsOpen == false);

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<VehicleTracking>().ToList();

                    list.ForEach(x =>
                    {
                        NHibernateUtil.Initialize(x.Vehicle);
                        if (x.Vehicle != null)
                            NHibernateUtil.Initialize(x.Vehicle.Residents);
                    });
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error VehicleTrackingRepository.GetByFilter ", e);
                return null;
            }
        }
    }
}
