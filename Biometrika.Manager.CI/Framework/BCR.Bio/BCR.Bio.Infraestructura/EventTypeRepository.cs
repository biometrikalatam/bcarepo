﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura
{
    public class EventTypeRepository : Repository<EventType>
    {
        public IList<EventType> GetList()
        {
            try
            {
                IList<EventType> list;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    list = session.QueryOver<EventType>()
                        .ReadOnly()
                        .List<EventType>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EventTypeRepository -> GetList", e);
                return null;
            }
        }
    }
}