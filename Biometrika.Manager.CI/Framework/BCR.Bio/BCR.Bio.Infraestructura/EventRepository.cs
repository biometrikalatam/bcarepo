﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate;
using NHibernate.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class EventRepository : Repository<Event>
    {
        public IList<Event> GetByCompany(Paginacion paginacion, int companyId)
        {
            IList<Event> list;
            TimeRestriction timeRestrictionAlias = null;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Event>()
                                   .Left.JoinAlias(x => x.TimeRestrictions, () => timeRestrictionAlias)
                                   .Where(x => x.Company.Id == companyId)
                                   .OrderBy(x => timeRestrictionAlias.StartTime).Desc;

                    criteria.ReadOnly();

                    if (paginacion != null)
                    {
                        criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                        if (paginacion.Cantidad > 0)
                            criteria.Take(paginacion.Cantidad);
                        paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<Event>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EventRepository -> GetByCompany", e);
                return null;
            }
        }

        public IList<Event> GetByCompanyIdAndMonthInterval(int companyId, DateTime startDate, DateTime? endDate = null)
        {
            IList<Event> list;
            TimeRestriction timeRestrictionAlias = null;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Event>()
                                   .Left.JoinAlias(x => x.TimeRestrictions, () => timeRestrictionAlias)
                                   .Where(x => x.Company.Id == companyId)
                                   .And(x => timeRestrictionAlias.StartTime >= startDate || timeRestrictionAlias.EndTime <= endDate)
                                   .OrderBy(x => timeRestrictionAlias.StartTime).Desc;

                    criteria.ReadOnly();

                    if (endDate != null)
                    {
                        criteria.And(x => timeRestrictionAlias.EndTime <= endDate);
                    }

                    //if (paginacion != null)
                    //{
                    //    criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                    //    if (paginacion.Cantidad > 0)
                    //        criteria.Take(paginacion.Cantidad);
                    //    paginacion.Total = criteria.RowCount();
                    //}

                    list = criteria.List<Event>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EventRepository -> GetByCompany", e);
                return null;
            }
        }

        public IList<Event> GetByIdentityAndCompany(int companyId, string typeId, string valueId)
        {
            try
            {
                IList<Event> list;
                Group groupAlias = null;
                Visitor visitorAlias = null;
                BpIdentity bpIdentityAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Event>()
                        .Left.JoinAlias(x => x.GroupVisitor, () => groupAlias)
                            .Left.JoinAlias(x => groupAlias.Visitors, () => visitorAlias)
                                .Left.JoinAlias(x => visitorAlias.Identity, () => bpIdentityAlias)
                                .Where(x => bpIdentityAlias.CompanyIdEnroll.Id == companyId);

                    if (!string.IsNullOrEmpty(typeId))
                        criteria.Where(x => bpIdentityAlias.TypeId == typeId);
                    if (!string.IsNullOrEmpty(valueId))
                        criteria.Where(x => bpIdentityAlias.ValueId == valueId);

                    criteria.ReadOnly();

                    list = criteria.List<Event>();

                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EventRepository -> GetByIdentityAndCompany", e);
                return null;
            }
        }

        public Event GetByDescription(Paginacion paginacion, int companyId, string description)
        {
            IList<Event> list;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Event>()
                                   .Where(x => x.Company.Id == companyId)
                                   .And(x => x.Description == description);

                    criteria.ReadOnly();

                    if (paginacion != null)
                    {
                        criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                        if (paginacion.Cantidad > 0)
                            criteria.Take(paginacion.Cantidad);
                        paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<Event>();
                }
                return list.FirstOrDefault();
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EventRepository -> GetByDescription", e);
                return null;
            }
        }

        public Event GetById(int id, bool withResident = false, bool withVisitor = false, bool withTracking = false)
        {
            Event evento = null;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Event>()
                                   .Where(x => x.Id == id);

                    criteria.ReadOnly();
                    evento = criteria.List<Event>().FirstOrDefault();

                    if (withResident)
                        NHibernateUtil.Initialize(evento.GroupResident);
                    if (withVisitor)
                    {
                        NHibernateUtil.Initialize(evento.GroupVisitor);

                        if (evento.GroupVisitor != null)
                        {
                            NHibernateUtil.Initialize(evento.GroupVisitor.Visitors);
                        }
                    }

                    if (withTracking)
                        NHibernateUtil.Initialize(evento.TrackingList);
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EventRepository -> GetById", e);
                return null;
            }

            return evento;
        }

        public IList<Event> GetByCompanyAndUnity(int companyId, int unityId)
        {
            IList<Event> list;

            try
            {
                Unity unityAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Event>()
                                   .Left.JoinAlias(x => x.Unity, () => unityAlias)
                                   .Where(x => x.Company.Id == companyId)
                                   .And(x => unityAlias.Id == unityId);

                    criteria.ReadOnly();

                    list = criteria.List<Event>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EventRepository -> GetByCompany", e);
                return null;
            }
        }

        public EventFilterDTO GetByFilter(EventFilterDTO filter)
        {
            try
            {
                TimeRestriction timeRestrictionAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Event>()
                                   .Left.JoinAlias(x => x.TimeRestrictions, () => timeRestrictionAlias)
                                   .Where(x => x.Company.Id == filter.CompanyId)
                                   .OrderBy(x => x.CreateDate).Desc;

                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.And(x => x.Description == filter.Description);

                    if (!string.IsNullOrEmpty(filter.DescriptionLike))
                        criteria.WhereRestrictionOn(x => x.Description).IsLike(filter.Description + '%');

                    if (filter.ProviderId > 0)
                        criteria.Where(x => x.Provider.Id == filter.ProviderId);

                    if (filter.StartDate != null)
                        criteria.Where(x => timeRestrictionAlias.StartTime <= filter.StartDate)
                                .And(x => timeRestrictionAlias.EndTime > filter.StartDate);

                    criteria.ReadOnly();

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        if (filter.Paginacion.Cantidad > 0)
                            criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    filter.ResultList = criteria.List<Event>().ToList();

                    if (filter.WithVisitors)
                        filter.ResultList.ForEach(x =>
                        {
                            NHibernateUtil.Initialize(x.GroupVisitor);
                            if (x.GroupVisitor != null)
                            {
                                NHibernateUtil.Initialize(x.GroupVisitor.Visitors);
                            }
                        });
                }
                return filter;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar EventRepository -> GetByFilter", e);
                return null;
            }
        }
    }
}