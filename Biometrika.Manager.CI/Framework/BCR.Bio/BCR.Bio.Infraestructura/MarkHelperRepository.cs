﻿using System;
using System.Collections.Generic;
using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate.Criterion;
namespace BCR.Bio.Infraestructura
{
    public class MarkHelperRepository : Repository<MarkHelper>
    {
        public IList<MarkHelper> GetByFilter(MarkHelperFilterDTO filter, bool filterProcessed = false)
        {
            if (filter == null)
                return null;
            try
            {
                Point pointAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var list = session.QueryOver<MarkHelper>()
                        .Inner.JoinAlias(x => x.Point, () => pointAlias)
                        .Where(x => x.Company.Id == filter.CompanyId);

                    if (filterProcessed)
                        list.And(x => x.Processed == filter.Processed);
                   
                    if (filter.TimeStamp > 0)
                        list.And(x => x.TimeStamp == filter.TimeStamp);
                    if (!string.IsNullOrEmpty(filter.PersonId))
                        list.And(x => x.PersonId == filter.PersonId);
                    if (filter.PointId > 0)
                        list.And(x => x.Point.Id == filter.PointId);

                    list.OrderBy(x => x.TimeStamp).Desc();

                    var results = list.List<MarkHelper>();

                    return results;

                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("Error al recuperar MarkHelper by Filter" + ex.ToString());
                return null;
            }
        }

        public MarkHelper GetLastMarkHelperByPersonId(string personId, int companyId)
        {
            using (var session = BioNHibernateHelper.OpenSession())
            { 
                try
                {
                    var list = session.QueryOver<MarkHelper>()
                       .Where(x => x.Company.Id == companyId)
                       .And(x => x.PersonId == personId)
                       //.And(x => x.Processed == false)
                       .OrderBy(x => x.DateTimeStamp).Desc()
                       .Take(1)
                       ;
                    if (list != null)
                    {
                        var results = list.List<MarkHelper>();
                        if (results != null && results.Count > 0)
                            return results[0];
                    }
                }
                catch(Exception ex)
                {
                    Log4Bio.Error("Error al recuperar MarkHelper by GetLastMarkHelperByPersonId " + ex.ToString());
                }
            }
            return null;
        }
        //public MarkHelper GetMaxTimeStampByPersonId(string personId)
        //{

        //    using (var session = BioNHibernateHelper.OpenSession())
        //    {
        //        try
        //        {
        //            var criteria = session.CreateCriteria<MarkHelper>();
        //            criteria.Add(Restrictions.Eq("PersonId", personId));
        //            criteria.Add(Restrictions.Eq("Processed", false));
        //            criteria.SetProjection(Projections.Max("TimeStamp"));
        //            var result = criteria.UniqueResult();
        //            if (result != null)
        //            {
        //                long maxTimeStamp = long.Parse(result.ToString());

        //                Point pointAlias = null;
        //                var query = session.QueryOver<MarkHelper>()
        //                    .Inner.JoinAlias(x => x.Point, () => pointAlias)
        //                    .Where(x => x.PersonId == personId)
        //                    .And(x => x.TimeStamp == maxTimeStamp)
        //                    ;

        //                query.Take(1);
        //                var results = query.List<MarkHelper>();

        //                return results[0];
        //            }
        //            else
        //            {
        //                return null;
        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            Log4Bio.Error("Error al recuperar MarkHelper by GetMaxTimeStampByPersonId " + ex.ToString());
        //            return null;
        //        }

        //    }
        //}

    }
}
