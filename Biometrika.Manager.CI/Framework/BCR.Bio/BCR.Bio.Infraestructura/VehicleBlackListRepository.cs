﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.Bio.Infraestructura.Configuration;
using BCR.System.Log;
using NHibernate;
using NHibernate.Util;
using System;
using System.Collections.Generic;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Infraestructura
{
    public class VehicleBlackListRepository : Repository<IVehicleBlackList>
    {
      

        public IList<IVehicleBlackList> GetByCompanyAndPlateNumber(int companyId, string plateNumber, bool active = false)
        {
            try
            {
                IList<IVehicleBlackList> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<IVehicleBlackList>()
                        .Where(k => k.Company.Id == companyId)
                        .And(k => k.PlateNumber == plateNumber);
                    criteria.ReadOnly();
                    if (active)
                        criteria.And(x => x.EndDate == null);

                    list = criteria.List<IVehicleBlackList>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByCompanyAndPlateNumber", e);
                return null;
            }
        }

        public IList<IVehicleBlackList> GetByCompanyId(int companyId)
        {
            try
            {
                IList<IVehicleBlackList> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    IQueryOver<IVehicleBlackList> HQQuery =
                session.QueryOver<IVehicleBlackList>()
                        .Where(k => k.Company.Id == companyId);

                    HQQuery.ReadOnly();
                    list = HQQuery.List<IVehicleBlackList>();

                    if (list != null)
                    {
                        foreach (var item in list)
                        {
                            NHibernateUtil.Initialize(item.UserIn);
                            NHibernateUtil.Initialize(item.UserOut);
                        }
                    }
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByCompanyId", e);
                return null;
            }
        }

        public IList<VehicleBlackList> GetByFilter(Paginacion paginacion, VehicleBlackList filter)
        {
            try
            {
                IList<VehicleBlackList> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<VehicleBlackList>()
                                   .Where(x => x.Company.Id == filter.Company.Id)
                                   .OrderBy(x => x.PlateNumber).Asc;

                    if (!string.IsNullOrEmpty(filter.PlateNumber))
                        criteria.And(x => x.PlateNumber == filter.PlateNumber);

                    if (paginacion != null)
                    {
                        criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                        criteria.Take(paginacion.Cantidad);
                        paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<VehicleBlackList>();

                    list.ForEach(x => 
                    {
                        NHibernateUtil.Initialize(x.UserIn);
                        NHibernateUtil.Initialize(x.UserOut);
                    });
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByFilter", e);
                return null;
            }
        }

        public IList<VehicleBlackList> GetByFilter(VehicleBlackListFilter filter)
        {
            try
            {
                IList<VehicleBlackList> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<VehicleBlackList>()
                                   .Where(x => x.Company.Id == filter.CompanyId)
                                   .OrderBy(x => x.PlateNumber).Asc;

                    if (filter.Active > 0)
                    {
                        if (filter.Active == 1)
                            criteria.And(x => x.EndDate == null);
                        else if (filter.Active == 2)
                            criteria.And(x => x.EndDate != null);
                    }

                    if (!string.IsNullOrEmpty(filter.PlateNumber))
                        criteria.And(x => x.PlateNumber == filter.PlateNumber);
                    if (filter.UserIn > 0)
                        criteria.And(x => x.UserIn.Id == filter.UserIn);
                    if (filter.UserOut > 0)
                        criteria.And(x => x.UserOut.Id == filter.UserOut);

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<VehicleBlackList>();

                    list.ForEach(x =>
                    {
                        NHibernateUtil.Initialize(x.UserIn);
                        NHibernateUtil.Initialize(x.UserOut);
                    });
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByFilter", e);
                return null;
            }
        }
    }
}