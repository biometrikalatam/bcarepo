﻿using System;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.Bio.Infraestructura.Configuration;
using System.Linq;
using BCR.System.Log;

namespace BCR.Bio.Infraestructura
{
    public class VehicleTypeRepository : Repository<IVehicleType>
    {
        public IVehicleType GetByDescription(string vehicleTypeDescription)
        {
            try
            {
                IVehicleType item;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    item = session.QueryOver<IVehicleType>()
                            .WhereRestrictionOn(x => x.Description).IsLike("%" + vehicleTypeDescription + "%")
                            .List<IVehicleType>().FirstOrDefault();
                }
                return item;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar VehicleTypeRepository.GetByDescription", e);
                return null;
            }
        }
    }
}