﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.Bio.Infraestructura.Configuration;
using BCR.System.Log;
using NHibernate;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura
{
    public class HQRepository : Repository<IHQ>
    {
        

        public IList<IHQ> GetByHolding(int holdingId)
        {
            try
            {
                IList<IHQ> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    IQueryOver<HQ, ICompany> HQQuery =
                session.QueryOver<HQ>()
                    .JoinQueryOver(c => c.Company)
                        .Where(k => k.Holding == holdingId);
                    //ICriteria crit = session.QueryOver .CreateCriteria(typeof(HQ));
                    //crit.Add(Expression.  .Eq("companyid", idCompany));

                    HQQuery.ReadOnly();
                    list = HQQuery.List<IHQ>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetUsersByCompany", e);
                return null;
            }
        }

        public IList<HQ> GetByFilter(HQFilterDTO filter, Paginacion paginacion)
        {
            Log4Bio.Info("HQRepository.GetByFilter");

            try
            {
                IList<HQ> list = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<HQ>()
                                   .Where(x => x.Company.Id == filter.Company.Id)
                                   .OrderBy(x => x.Description).Asc;

                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.Where(x => x.Description == filter.Description);
                    if (!string.IsNullOrEmpty(filter.DescriptionLike))
                        criteria.WhereRestrictionOn(x => x.Description).IsLike(filter.DescriptionLike + "%");

                    if (filter.HQIds != null && filter.HQIds.Count > 0)
                    {
                        criteria.WhereRestrictionOn(x => x.Id).IsIn(filter.HQIds);
                    }

                    if (paginacion != null)
                    {
                        paginacion.Total = criteria.RowCount();
                        criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                        criteria.Take(paginacion.Cantidad);
                    }

                    criteria.ReadOnly();
                    list = criteria.List<HQ>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByFilter", e);
                return null;
            }
        }
    }
}