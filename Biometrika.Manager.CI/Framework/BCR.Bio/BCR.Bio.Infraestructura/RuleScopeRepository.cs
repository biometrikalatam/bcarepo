﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using BCR.Bio.Domain.DTO.Filter;
using System.Collections.Generic;
using BCR.System.Log;
using System;

namespace BCR.Bio.Infraestructura
{
    public class RuleScopeRepository : Repository<RuleScope>
    {
        public IList<RuleScope> GetByFilter(RuleScopeFilter filter)
        {
            if (filter == null)
                return null;

            try
            {
                IList<RuleScope> list = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<RuleScope>()
                                    .OrderBy(x => x.Description).Asc;

                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.Where(x => x.Description == filter.Description);
                    if (!string.IsNullOrEmpty(filter.DescriptionLike))
                        criteria.WhereRestrictionOn(x => x.Description).IsLike(filter.DescriptionLike + "%");

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<RuleScope>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error RuleScopeRepository.GetByFilter", e);
                return null;
            }
        }
    }
}