﻿using System;
using System.Collections.Generic;
using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate.Criterion;


namespace BCR.Bio.Infraestructura
{
    public class HolidayAuthorizedRepository : Repository<HolidayAuthorized>
    {

        public IList<HolidayAuthorized> GetByHolidayId(int holidayId)
        {
            if (holidayId < 0) return null;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var list = session.QueryOver<HolidayAuthorized>()
                            .Where(x => x.Holiday.Id == holidayId);

                    list.OrderBy(x => x.CreateDate).Desc();

                    var results = list.List<HolidayAuthorized>();

                    return results;
                }

            }
            catch (Exception ex)
            {
                Log4Bio.Error("Error al recuperar HolidayAuthorized by Holiday id "+ holidayId + ex.ToString());
                return null;
            }
        }

        public IList<HolidayAuthorized> GetByValueIdAndHoliday(string valueId, int holidayId)
        {
            if (holidayId < 0) return null;
            if (string.IsNullOrEmpty(valueId)) return null;

            try
            {
                BpIdentity bpIdentityAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var list = session.QueryOver<HolidayAuthorized>()
                            .Inner.JoinAlias(x => x.Identity, () => bpIdentityAlias)
                            .Where(x => x.Holiday.Id == holidayId)
                            .And(x => bpIdentityAlias.ValueId == valueId);

                    list.OrderBy(x => x.CreateDate).Desc();
                    return list.List<HolidayAuthorized>();
                    
                }
            } catch (Exception ex)
            {
                Log4Bio.Error("GetByValueIdAndHoliday: Error al recuperar HolidayAuthorized. " + ex.ToString());
                return null;
            }
        }
    }
}
