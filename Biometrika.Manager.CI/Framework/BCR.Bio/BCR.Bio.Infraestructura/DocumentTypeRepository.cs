﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura
{
    public class DocumentTypeRepository : RepositoryLocal<DocumentType, string>
    {
        private static List<DocumentType> documentTypeList;

        private DocumentTypeRepository()
        {
            if (documentTypeList == null)
            {
                documentTypeList = new List<DocumentType>();
                documentTypeList.Add(new DocumentType() { Id = "RUT", Description = "RUT" });
                documentTypeList.Add(new DocumentType() { Id = "PAS", Description = "Pasaporte" });
                documentTypeList.Add(new DocumentType() { Id = "CI", Description = "Cédula de Identidad" });
            }
        }
    }
}