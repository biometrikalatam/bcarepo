﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura
{
    public class CentroDeCostosRepository : Repository<ICentroDeCostos>
    {
        public IList<CentroDeCostos> GetByFilter(CostCenterFilterDTO filter)
        {
            try
            {
                IList<CentroDeCostos> list = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<CentroDeCostos>()
                                   .Where(x => x.Company.Id == filter.CompanyId)
                                   .OrderBy(x => x.Description).Asc;

                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.Where(x => x.Description == filter.Description);
                    if (!string.IsNullOrEmpty(filter.DescriptionLike))
                        criteria.WhereRestrictionOn(x => x.Description).IsLike(filter.DescriptionLike + "%");

                    criteria.ReadOnly();
                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<CentroDeCostos>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar CentroDeCostosRepository.GetByFilter", e);
                return null;
            }
        }
    }
}