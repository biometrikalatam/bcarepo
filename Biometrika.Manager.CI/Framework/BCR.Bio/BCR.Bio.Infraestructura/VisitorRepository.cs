﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.DTO.Filter;
using NHibernate.Util;
using System.Diagnostics;

namespace BCR.Bio.Infraestructura
{
    public class VisitorRepository : Repository<IVisitor>
    {
        public IList<IVisitor> GetByCompanyId(int companyId)
        {
            try
            {
                IList<IVisitor> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    IQueryOver<IVisitor, BpIdentity> HQQuery =
                session.QueryOver<IVisitor>()
                    .JoinQueryOver(c => c.Identity)
                        .Where(k => k.CompanyIdEnroll.Id == companyId);
                    HQQuery.ReadOnly();
                    list = HQQuery.List<IVisitor>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetUsersByCompany", e);
                return null;
            }
        }

        public Visitor GetById(int id, bool withIdentity = false, bool withProvider = false, bool withUnity = false, bool withVehicles = false)
        {
            IList<Visitor> list = null;
            string msg;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    //Provider providerAlias = null;
                    BpIdentity identityVisitorAlias = null;

                    var results = session.QueryOver<Visitor>();
                    //if (withProvider)
                    //    results.Inner.JoinAlias(x => x.Provider, () => providerAlias);

                    results.ReadOnly();
                    if (withIdentity)
                        results.Inner.JoinAlias(x => x.Identity, () => identityVisitorAlias);

                    results = results.Where(x => x.Id == id);

                    //if (providerId != -1)
                    //    results.And(x => x.Provider.Id == providerId);
                    //if (!string.IsNullOrEmpty(typeid))
                    //    results.And(x => identityVisitorAlias.TypeId == typeid);

                    //if (!string.IsNullOrEmpty(valueid))
                    //    results.And(x => identityVisitorAlias.ValueId == valueid);
                    //if (!string.IsNullOrEmpty(name))
                    //    results.And(x => identityVisitorAlias.Name.IsLike(name));
                    //if (!string.IsNullOrEmpty(lastname))
                    //    results.And(x => identityVisitorAlias.PatherLastname.IsLike(lastname));

                    list = results.List<Visitor>();

                    if (withIdentity)
                    {
                        foreach (Visitor item in list)
                        {
                            item.Identity.BpBir.ToList();
                        }
                    }

                    if (withUnity)
                    {
                        list.ToList().ForEach(x => NHibernateUtil.Initialize(x.Unitys));
                    }

                    if (withVehicles)
                    {
                        list.ToList().ForEach(x => NHibernateUtil.Initialize(x.Vehicles));
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("VisitorExtRepository.GetVisitorsByFiltro", ex);
            }

            return list.FirstOrDefault();
        }

        public bool PromoteToResident(string typeId, string valueId, int companyId)
        {
            if (string.IsNullOrEmpty(typeId))
                return false;
            if (string.IsNullOrEmpty(valueId))
                return false;
            if (companyId <= 0)
                return false;


            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.CreateSQLQuery("EXEC [p_ChangeVisitorResident] :companyId, :typeId, :valueId");
                    criteria.SetParameter("companyId", companyId);
                    criteria.SetParameter("typeId", typeId);
                    criteria.SetParameter("valueId", valueId);

                    var res = criteria.List();
                    if (res != null)
                        return true;
                    return false;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("VisitorExtRepository.PromoteToResident", e);
                return false;
            }
        }

        public bool PromoteToEmployee(BpIdentity identity)
        {
            Visitor visitor = GetByIdentityId(identity.Id);

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var newVisitor = session.Load<Visitor>(visitor.Id);
                    newVisitor.EndDate = DateTime.Now;
                    newVisitor.UpdateDate = DateTime.Now;
                    session.Flush();
                    return true;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("VisitorExtRepository.PromoteToResident", e);
                return false;
            }
            
        }

        public IList<Visitor> GetByUnity(int unityId, Paginacion paginacion)
        {
            if (paginacion == null)
                paginacion = new Paginacion() { Pagina = 0, Cantidad = 1 };

            try
            {
                IList<Visitor> list;
                BpIdentity bpIdentityAlias = null;
                Unity unityAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Visitor>()
                                    .Left.JoinAlias(x => x.Identity, () => bpIdentityAlias)
                                    .Left.JoinAlias(x => x.Unitys, () => unityAlias)
                                    .Where(x => unityAlias.Id == unityId);

                    criteria.ReadOnly();

                    criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                    criteria.Take(paginacion.Cantidad);

                    list = criteria.List<Visitor>();

                    paginacion.Total = criteria.RowCount();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByUnity", e);
                return null;
            }
        }

        public List<IVisitor> GetByFilter(Paginacion paginacion, VisitorSearchDTO visitorSearchDTO)
        {
            try
            {
                IList<IVisitor> list;
                BpIdentity bpIdentityAlias = null;
                Unity unityAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Visitor>()
                                   .Left.JoinAlias(x => x.Identity, () => bpIdentityAlias)
                                   .Left.JoinAlias(x => x.Unitys, () => unityAlias)
                                   .Where(x => bpIdentityAlias.CompanyIdEnroll.Id == visitorSearchDTO.Company);

                    criteria.ReadOnly();
                    if (!string.IsNullOrEmpty(visitorSearchDTO.TypeId))
                        criteria.And(x => bpIdentityAlias.TypeId == visitorSearchDTO.TypeId);
                    if (!string.IsNullOrEmpty(visitorSearchDTO.ValueId))
                        criteria.And(x => bpIdentityAlias.ValueId == visitorSearchDTO.ValueId);
                    if (!string.IsNullOrEmpty(visitorSearchDTO.Name))
                        criteria.And(x => bpIdentityAlias.Name.IsLike(visitorSearchDTO.Name.Trim()));
                    if (!string.IsNullOrEmpty(visitorSearchDTO.LastName))
                        criteria.And(() => bpIdentityAlias.PatherLastname.IsLike('%' + visitorSearchDTO.LastName.Trim() + '%') || bpIdentityAlias.MotherLastname.IsLike('%' + visitorSearchDTO.LastName.Trim() + '%'));

                    if (!string.IsNullOrEmpty(visitorSearchDTO.Address))
                        criteria.And(x => unityAlias.Address.IsLike('%' + visitorSearchDTO.Address.Trim() + '%'));

                    paginacion.Total = criteria.RowCount();
                    criteria.Skip(paginacion.Pagina * paginacion.Cantidad);
                    criteria.Take(paginacion.Cantidad);


                    list = criteria.List<IVisitor>();
                }
                return list.ToList();
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByFilter", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="apellidoPaterno"></param>
        /// <param name="nombre"></param>
        /// <param name="typeId"></param>
        /// <param name="numberId"></param>
        /// <returns></returns>
        public IList<IVisitor> GetByCompanyAndIdentity(int companyId, string apellidoPaterno = "", string nombre = "", string typeId = "", string valueId = "", bool withUnitys = false)
        {
            try
            {
                IList<IVisitor> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    IQueryOver<IVisitor, BpIdentity> HQQuery =
                    session.QueryOver<IVisitor>()
                        .JoinQueryOver(c => c.Identity)
                        .Where(k => k.CompanyIdEnroll.Id == companyId);

                    HQQuery.ReadOnly();

                    if (!string.IsNullOrEmpty(apellidoPaterno))
                        HQQuery.And(k => k.PatherLastname == apellidoPaterno);

                    if (!string.IsNullOrEmpty(nombre))
                        HQQuery.And(k => k.Name == nombre);

                    if (typeId != "" && !string.IsNullOrEmpty(valueId))
                    {
                        HQQuery.And(k => k.TypeId == typeId);
                        HQQuery.And(k => k.ValueId == valueId);
                    }

                    list = HQQuery.List<IVisitor>();

                    if (withUnitys)
                        list.ForEach(x => NHibernateUtil.Initialize(x.Unitys));
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByCompanyAndIdentity", e);
                return null;
            }
        }

        public IList<Visitor> GetVisitorsByFiltro(Paginacion paginacion, ICompany company,
                                                    string typeid, string valueid,
                                                    string name, string lastname, int providerId)
        {
            IList<Visitor> list = null;
            string msg;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Provider providerAlias = null;
                    BpIdentity identityAlias = null;

                    var results = session.QueryOver<Visitor>()
                     //.Inner.JoinAlias(x => x.Provider, () => providerAlias)
                     .Inner.JoinAlias(x => x.Identity, () => identityAlias)
                     .Where(x => identityAlias.CompanyIdEnroll.Id == company.Id);

                    results.ReadOnly();

                    //if (providerId != -1)
                    //    results.And(x => x.Provider.Id == providerId);
                    if (!string.IsNullOrEmpty(typeid))
                        results.And(x => identityAlias.TypeId == typeid);

                    if (!string.IsNullOrEmpty(valueid))
                        results.And(x => identityAlias.ValueId == valueid);
                    if (!string.IsNullOrEmpty(name))
                        results.And(x => identityAlias.Name.IsLike(name));
                    if (!string.IsNullOrEmpty(lastname))
                        results.And(x => identityAlias.PatherLastname.IsLike(lastname));

                    results.OrderBy(x => x.Identity).Desc();
                    results.Skip(paginacion.Pagina * paginacion.Cantidad);
                    results.Take(paginacion.Cantidad);

                    list = results.List<Visitor>().ToList();

                    foreach (Visitor visitor in list)
                    {
                        visitor.IsEnrolled = visitor.Identity.BpBir.Count() > 0;
                    }

                    paginacion.Total = results.RowCount();
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("VisitorExtRepository.GetVisitorsByFiltro", ex);
            }

            return list;
        }

        public int RowCount(ICriteria criteria)
        {
            ICriteria rowCountCriteria = (ICriteria)criteria.Clone();

            rowCountCriteria.SetProjection(Projections.RowCount());
            return int.Parse(rowCountCriteria.UniqueResult().ToString());
        }

        public VisitorFilterDTO GetByFilter(VisitorFilterDTO filter)
        {
            try
            {
                BpIdentity bpIdentityAlias = null;
                Unity unityAlias = null;
                Building buildingAlias = null;
                Zone zoneAlias = null;
                IdentityType identityTypeAlias = null;
                IdentityState identityStateAlias = null;
                Visitor visitorAlias = null;
                Provider providerAlias = null;
                Vehicle providerVehicleAlias = null;
                Resident residentAlias = null;
                Vehicle vehicleAlias = null;
                Resident residentVehicleAlias = null;
                HQ hqAlias = null;
                BpIdentity identityAlias = null;
                Provider ProviderAlias = null;
                Vehicle ProviderVehicleAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    #region SubQuery




                    var subQuery = QueryOver.Of<Visitor>()
                                   .Left.JoinAlias(x => x.Identity, () => bpIdentityAlias)
                                       .Left.JoinAlias(x => bpIdentityAlias.IdentityTypeList, () => identityTypeAlias)
                                    .Left.JoinAlias(x => x.Provider, () => providerAlias)
                                        .Left.JoinAlias(x => x.Vehicles, () => providerVehicleAlias)
                                   .Where(x => bpIdentityAlias.CompanyIdEnroll.Id == filter.CompanyId);
                                     //.And(x => zoneAlias.Id == 6);

                                    // subQuery.WhereRestrictionOn(x => zoneAlias.Id == 6);


                    if (filter.VisitorClass == VisitorClassEnum.All)
                    {
                        subQuery.Left.JoinAlias(x => x.Unitys, () => unityAlias)
                                .Left.JoinAlias(x => unityAlias.Building, () => buildingAlias)
                                .Left.JoinAlias(x => unityAlias.Zone, () => zoneAlias);
                    }
                    else if (filter.VisitorClass == VisitorClassEnum.Authorized)
                    {
                        subQuery.Inner.JoinAlias(x => x.Unitys, () => unityAlias)
                                .Left.JoinAlias(x => unityAlias.Building, () => buildingAlias)
                                .Left.JoinAlias(x => unityAlias.Zone, () => zoneAlias);
                    }
                    else if (filter.VisitorClass == VisitorClassEnum.Registered)
                    {
                        subQuery.Left.JoinQueryOver(x => x.Unitys, () => unityAlias)
                            .WhereRestrictionOn(x => unityAlias.Id).IsNull();
                    }

                    if (!string.IsNullOrEmpty(filter.TypeId))
                        subQuery.And(x => bpIdentityAlias.TypeId == filter.TypeId);
                    if (!string.IsNullOrEmpty(filter.ValueId))
                        subQuery.WhereRestrictionOn(x => bpIdentityAlias.ValueId).IsLike(filter.ValueId + "%");
                    if (!string.IsNullOrEmpty(filter.Name))
                        subQuery.And(x => bpIdentityAlias.Name.IsLike('%' + filter.Name + '%'));
                    if (!string.IsNullOrEmpty(filter.FatherLastName))
                        subQuery.And(x => bpIdentityAlias.PatherLastname.IsLike('%' + filter.FatherLastName + '%'));

                    if (!string.IsNullOrEmpty(filter.FullName))
                        subQuery.WhereRestrictionOn(x => bpIdentityAlias.FullName).IsLike(filter.FullName + "%");

                    // filtrado por Unity
                    if (filter.UnityId > 0)
                    {
                        subQuery.And(x => unityAlias.Id == filter.UnityId);
                    }

                    //// filtrado por Building
                    if (filter.BuildingId > 0)
                        subQuery.And(x => buildingAlias.Id == filter.BuildingId);

                    // filtrado por Zone
                    if (filter.ZoneId > 0)
                        subQuery.And(x => zoneAlias.Id == filter.ZoneId);

                    // filtrado por Provider
                    if (filter.Provider != null)
                    {
                        if (filter.Provider.Id > 0)
                            subQuery.Where(x => x.Provider.Id == filter.Provider.Id);
                    }

                    // filtrado por IdentityType
                    if (filter.IdentityType != null)
                    {
                        if (filter.IdentityType.Id > 0)
                            subQuery.Where(x => identityTypeAlias.Id == filter.IdentityType.Id);
                    }

                    // filtrado por IdentityState
                    if (filter.IdentityState != null)
                    {
                        subQuery.Inner.JoinAlias(x => bpIdentityAlias.IdentityStateList, () => identityStateAlias);

                        if (filter.IdentityState.Id > 0)
                            subQuery.Where(x => identityStateAlias.Id == filter.IdentityState.Id);
                        else
                        {
                            subQuery.Where(x => identityStateAlias.Class == filter.IdentityState.Class)
                            .Where(x => identityStateAlias.Company.Id == filter.CompanyId)
                            .Where(x => identityStateAlias.Tag == filter.IdentityState.Tag);
                        }
                    }

                    if (filter.HqList != null && filter.HqList.Any())
                    {
                        subQuery.WhereRestrictionOn(x => zoneAlias.HQ.Id).IsIn(filter.HqList.Select(h => h.Id).ToList());

                       
                    }

                    subQuery.Select(x => x.Id);

                    #endregion
                    var criteria = session.QueryOver<Visitor>(() => visitorAlias)
                                    .WithSubquery.WhereProperty(x => x.Id).In(subQuery)
                                    .Where(x => x.EndDate == null);                    

                    criteria.ReadOnly();
                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    NHibernateUtil.Initialize(filter.ResultList);
                    //var lista = criteria.List<Visitor>();
                    filter.ResultList = (List<Visitor>)session
                        .GetSessionImplementation()
                        .PersistenceContext
                        .Unproxy(criteria.List<Visitor>());
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar VisitorRepository -> GetByFilter", e);
                return null;
            }

            return filter;
        }

        public Visitor GetByIdentityId(int identityId)
        {
            if (identityId <= 0)
                return null;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Visitor>()
                           .Where(x => x.Identity.Id == identityId);

                    return criteria.List<Visitor>().FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar VisitorRepository -> GetByIdentityId", e);
                return null;
            }
        }

        /// <summary>
        /// Trae las visitas asociadas a un HQ. Excepto para el SuperAdmin
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public VisitorFilterDTO GetByVisitorFromVisitorTracking(VisitorFilterDTO filter)
        {
            BpIdentity bpIdentityAlias = null;
            Unity unityAlias = null;
            Building buildingAlias = null;
            Zone zoneAlias = null;
            IdentityType identityTypeAlias = null;
            IdentityState identityStateAlias = null;
            Visitor visitorAlias = null;
            Provider providerAlias = null;
            Vehicle providerVehicleAlias = null;
            Resident residentAlias = null;
            Vehicle vehicleAlias = null;
            Resident residentVehicleAlias = null;
            HQ hqAlias = null;
            BpIdentity identityAlias = null;
            Provider ProviderAlias = null;
            Vehicle ProviderVehicleAlias = null;
            try
            { 
                using (var session = BioNHibernateHelper.OpenSession())
                {

                    Log4Bio.Info("Visitor: Filtramos el tracking Id por criterio de búsqueda de HQ");


                    var test = QueryOver.Of<VisitorTracking>()
                         .Left.JoinAlias(x => x.HQ, () => hqAlias);
                    if (filter.IsSuperAdmin)
                    {
                        Log4Bio.Info("Visitor: Es super admin, ve todos los HQ");
                        test.Where(x => x.Company.Id == filter.CompanyId);
                    } else
                    {
                        Log4Bio.Info("Visitor: Entra en filtro por HQ para administrador de sitio");
                        test.WhereRestrictionOn(x => x.HQ.Id).IsIn(filter.HqList.Select(h => h.Id).ToList());
                    };

                  
                                           
                    test.SelectList(list => list
                    .Select(Projections.Distinct(Projections.Property<VisitorTracking>(x => x.Visitor)))
                    );
   

                    test.Select(x => x.Visitor.Id);

                    var cri = session.QueryOver<Visitor>()
                        .Left.JoinAlias(x => x.Identity, () => identityAlias);
                    if (!string.IsNullOrEmpty(filter.TypeId))
                        cri.Where(() => identityAlias.TypeId == filter.TypeId);
                    if (!string.IsNullOrEmpty(filter.ValueId))
                        cri.WhereRestrictionOn(() => identityAlias.ValueId).IsLike(filter.ValueId + "%");

                    // Se eliminan de los resultados las visitas (tabla: Visitor) con endDate.
                    // (Promovidos a Employee)
                    cri.Where(x => x.EndDate == null);

                    cri.WithSubquery.WhereProperty(x => x.Id).In(test);
                   

                    cri.ReadOnly();
                    if (filter.Paginacion != null)
                    {
                        cri.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        cri.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = cri.RowCount();
                    }

                    NHibernateUtil.Initialize(filter.ResultList);
                    //var lista = criteria.List<Visitor>();
                    filter.ResultList = (List<Visitor>)session
                        .GetSessionImplementation()
                        .PersistenceContext
                        .Unproxy(cri.List<Visitor>());


                    var cria = session.QueryOver<Visitor>()
                               .Left.JoinAlias(x => x.Unitys, () => unityAlias)
                               .Left.JoinAlias(x => unityAlias.Building, () => buildingAlias)
                               .Left.JoinAlias(x => unityAlias.Zone, () => zoneAlias)
                               .Left.JoinAlias(x => zoneAlias.HQ, () => hqAlias)
                               .WhereRestrictionOn(() => hqAlias.Id).IsIn(filter.HqList.Select(h => h.Id).ToList());

                    IList<Visitor> a = cria.List<Visitor>();

                }
            }
            catch(Exception exe)
            {
                Log4Bio.Error("Se produjo un error al buscar por los criterios");
                Log4Bio.Error("error:" + exe.Message, exe);
            }
            return filter;
        }
    }
}