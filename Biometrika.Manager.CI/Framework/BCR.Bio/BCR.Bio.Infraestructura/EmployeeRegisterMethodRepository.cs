﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura
{
    public class EmployeeRegisterMethodRepository : Repository<EmployeeRegisterMethod>
    {
        public IList<EmployeeRegisterMethod> GetByEmployee(int employee)
        {
            IList<EmployeeRegisterMethod> EmployeeRegisterMethod = null;
            Employee EmployeeAlias = null;
            try
            {

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    EmployeeRegisterMethod = session.QueryOver<EmployeeRegisterMethod>()
                                                   .Inner.JoinAlias(x => x.employee, () => EmployeeAlias)
                                                   .Where(x => EmployeeAlias.Id == employee)
                                                   .List<EmployeeRegisterMethod>();
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Se produjo un error en Obtener RegisterMethodEmployee", e);
            }
            return EmployeeRegisterMethod;
        }

        public bool DeleteAll(int employee)
        {
            bool res = false;
            try
            {

                using (var session = BioNHibernateHelper.OpenSession())
                {

                    //session.Delete("from EmployeeRegisterMethod o");
                    session.CreateQuery("delete EmployeeRegisterMethod  where employee = :employee")
                  .SetParameter("employee", employee)
                  .ExecuteUpdate();
                    session.Flush();
                    //Log4Bio.Info("Se elimina el Beneficio:" + EmployeeBenefitOption.Id + " asociado al empleado:" + EmployeeBenefitOption.employee.Id);

                    res = true;
                }

            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al eliminar EmployeeRegisterMethod", e);
            }
            return res;
        }


    }
}
