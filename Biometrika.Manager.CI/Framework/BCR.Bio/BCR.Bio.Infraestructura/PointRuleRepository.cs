﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura
{
    public class PointRuleRepository : Repository<PointRule>
    {
        public IList<PointRule> GetByFilter(PointRuleFilter filter)
        {
            if (filter == null)
                return null;

            try
            {
                IList<PointRule> list = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<PointRule>()
                                    .OrderBy(x => x.Description).Asc;

                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.Where(x => x.Description == filter.Description);
                    if (!string.IsNullOrEmpty(filter.DescriptionLike))
                        criteria.WhereRestrictionOn(x => x.Description).IsLike(filter.DescriptionLike + "%");

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<PointRule>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error PointRuleRepository.GetByFilter", e);
                return null;
            }
        }
    }
}