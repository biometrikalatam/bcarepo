﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class SectorRepository : Repository<ISector>
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public IQueryable<Sector> GetByCompany(int companyId)
        {
            try
            {
                IQueryable<Sector> list;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var hql = @"select distinct  s
                            from Sector s
                            inner join s.Companies as c
                            where c.Id = :companyId";

                    //session.CacheMode = NHibernate.CacheMode.Ignore;
                    //session.Flush();

                    IList<Sector> sectors = session.CreateQuery(hql)
                    .SetString("companyId", companyId.ToString())
                    .SetReadOnly(true)
                    .List<Sector>();

                    //sectors.ToList();

                    list = (IQueryable<Sector>)sectors.AsQueryable();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByCompany", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="holdingId"></param>
        /// <returns></returns>
        public IQueryable<Sector> GetByHolding(int holdingId)
        {
            try
            {
                IQueryable<Sector> list;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var hql = @"select distinct  s
                            from Sector s
                            inner join s.Companies as c
                            where c.Holding = :holdingid";

                    IList<Sector> sectors = session.CreateQuery(hql)
                    .SetString("holdingid", holdingId.ToString())
                    .SetReadOnly(true)
                    .List<Sector>();

                    list = (IQueryable<Sector>)sectors.AsQueryable();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetByHolding", e);
                return null;
            }
        }

        public IList<Sector> GetByFilter(SectorFilterDTO filter)
        {
            if (filter == null)
                return null;

            try
            {
                IList<Sector> list = null;
                Company companyAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Sector>()
                                    .Inner.JoinAlias(x => x.Companies, () => companyAlias)
                                    .Where(x => companyAlias.Id == filter.CompanyId)
                                    .OrderBy(x => x.Description).Asc;

                    if (!string.IsNullOrEmpty(filter.DescriptionLike))
                        criteria.WhereRestrictionOn(x => x.Description).IsLike(filter.DescriptionLike + "%");

                    if (filter.Paginacion != null)
                    {
                        filter.Paginacion.Total = criteria.RowCount();
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                    }

                    criteria.ReadOnly();
                    list = criteria.List<Sector>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error SectorRepository.GetByFilter", e);
                return null;
            }
        }
    }
}