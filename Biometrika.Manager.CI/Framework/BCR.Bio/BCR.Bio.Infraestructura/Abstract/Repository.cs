﻿using BCR.Bio.Infraestructura.Interface;
using BCR.System.Log;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura.Abstract
{
    /// <summary>
    /// Abstract implementation for repository
    /// </summary>
    /// <typeparam name="T">Domain entitys</typeparam>
    public class Repository<T> : IRepository<T> where T : class  //, new()
    {
        //protected IAbstractLogger log;
        //protected IValidationDictionary validationDictionary;

        /// <summary>
        ///
        /// </summary>
        /// <param name="repositoryParams"></param>
        //public Repository(RepositoryParams repositoryParams)
        //{
        //    validationDictionary = repositoryParams.ValidationDictionary;
        //    //log = repositoryParams.Logger;
        //}

        /// <summary>
        ///
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public virtual int Create(T t)
        {
            object id = null;
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Log4Bio.Info(string.Format("{0}  ->  Create(t)", typeof(T).ToString()));

                    id = session.Save(t);
                    session.Flush();
                }

                return (int)id;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error", e);
                return -1;
            }
            
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        public virtual bool Delete<T>(int id)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    session.Delete(GetById(id));
                    session.Flush();
                }

                return true;
            }
            catch (Exception e)
            {
                Log4Bio.Error(e.Message);
                return false;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="t"></param>
        public virtual T Update(T t)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Log4Bio.Info(string.Format("{0}  ->  Update(t)", typeof(T).ToString()));

                    session.Update(t);
                    session.Flush();
                }

                //T t1 = session.Merge(typeof(T).ToString(), t);
                //session.Clear();
                //session.Flush();

                //session.Update(t);
                //session.Flush();
                return t;
            }
            catch (Exception e)
            {
                Log4Bio.Error(e.Message);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public virtual IList<T> GetByCriteria(DetachedCriteria criteria)
        {
            IList<T> list;
            using (var session = BioNHibernateHelper.OpenSession())
            {
                list = criteria.GetExecutableCriteria(session).List<T>();
            }

            return list;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual T GetById(int id)
        {
            try
            {
                T result;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Log4Bio.Info(string.Format("{0}  ->  GetById({1})", typeof(T).ToString(), id));
                    result = session.CreateCriteria<T>()
                            .Add(Restrictions.Eq("id", id))
                            .List<T>().FirstOrDefault();
                }
                return result;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error GetById", e);
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public virtual IList<T> List()
        {
            IList<T> list = null;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    Log4Bio.Info(string.Format("{0}  ->  List()", typeof(T).ToString()));

                    ICriteria criteria = session.CreateCriteria(typeof(T));
                    list = criteria.List<T>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar lista", e);
                return list;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public int RowCount(ICriteria criteria)
        {
            ICriteria rowCountCriteria = (ICriteria)criteria.Clone();

            rowCountCriteria.SetProjection(Projections.RowCount());
            return int.Parse(rowCountCriteria.UniqueResult().ToString());
        }
    }
}