﻿using BCR.Bio.Infraestructura.Interface;
using BCR.System.Configuration;
using NHibernate;
using System;
using System.Data;

namespace BCR.Bio.Infraestructura.Abstract
{
    public abstract class AbstractUnitOfWork : IUnitOfWork
    {
        protected TransactionWrapper transaction = new TransactionWrapper();

        //protected static NhibernateParams nhibernateParams;

        protected static ISession Session { get; private set; }

        //1-6
        public AbstractUnitOfWork(NhibernateParams nhibernateParams)
        {
            BioNHibernateHelper.Initilize(nhibernateParams);

            //if (Session == null)
            //    Session = OpenSession();
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        //public IRepository<T> GetRepository<T>() where T : class
        //{
        //    if (repositories != null)
        //    {
        //        foreach (var key in repositories.Keys)
        //        {
        //            if (key == typeof(T))
        //            {
        //                return repositories[typeof(T)] as Repository<T>;
        //            }
        //        }
        //    }

        //    IRepository<T> repository = new Repository<T>();
        //    repositories.Add(typeof(T), repository);
        //    return repository;
        //}

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public virtual TransactionWrapper BeginTransaction()
        {
            Session = BioNHibernateHelper.OpenSession();            
            transaction.Transaction = Session.BeginTransaction(IsolationLevel.ReadCommitted);
            return transaction;
        }
       

        /// <summary>
        ///
        /// </summary>
        public virtual void Commit()
        {
            if (!transaction.Transaction.IsActive)
            {
                throw new InvalidOperationException("Cannot commit to inactive transaction.");
            }
            transaction.Transaction.Commit();
            
        }

        public virtual void Rollback()
        {
            if (transaction.Transaction.IsActive)
            {
                transaction.Transaction.Rollback();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public virtual void Dispose()
        {
            if (Session != null)
            {
                Session.Dispose();
            }
            if (transaction != null)
            {
                transaction.Transaction.Dispose();
            }
        }
    }
}