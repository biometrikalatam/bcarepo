﻿using BCR.Bio.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Abstract
{
    public abstract class RepositoryLocal<T, Tkey> where T : AbstractEntityItem<Tkey>
    {
        private static List<T> list = null;


        public IList<T> GetByDescription(string match)
        {
            return (IList<T>)list.Select(m => m.Description.Equals(match));
        }

        public IList<T> List()
        {
            return list;
        }

        public T GetById(Tkey id)
        {
            return list.Single(m => m.Id.Equals(id));
        }
    }
}
