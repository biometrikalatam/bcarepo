﻿using System;
using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;

namespace BCR.Bio.Infraestructura
{
    public class AutoClubStatusSociosRepository : Repository<AutoClubStatusSocios>
    {
        public AutoClubStatusSocios GetByValueId(string valueId)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var list = session.QueryOver<AutoClubStatusSocios>()
                        .Where(x => x.Rut == valueId);

                    return list.List<AutoClubStatusSocios>()[0];
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetHolidaysByCompany", e);
                return null;
            }
        }
    }
}
