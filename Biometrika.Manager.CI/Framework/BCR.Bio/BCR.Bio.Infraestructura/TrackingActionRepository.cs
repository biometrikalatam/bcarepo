﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;

namespace BCR.Bio.Infraestructura
{
    public class TrackingActionRepository : Repository<ITrackingAction>
    {
        public ITrackingAction GetByName(string description)
        {
            try
            {
                ITrackingAction item;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    item = session.QueryOver<TrackingAction>()
                        .Where(x => x.Description == description)
                        .ReadOnly()
                        .SingleOrDefault();
                }
                return item;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar TrackingActionRepository -> GetByName", e);
                return null;
            }
        }

        public TrackingAction GetByValue(int value)
        {
            try
            {
                TrackingAction item;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    item = session.QueryOver<TrackingAction>()
                        .Where(x => x.Value == value)
                        .ReadOnly()
                        .SingleOrDefault();
                }
                return item;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar TrackingActionRepository -> GetByName", e);
                return null;
            }
        }
    }
}