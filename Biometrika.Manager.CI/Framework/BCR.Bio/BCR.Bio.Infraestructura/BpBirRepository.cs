﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.Bio.Infraestructura.Configuration;
using BCR.System.Log;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura
{
    public class BpBirRepository : Repository<IBpBir>
    {
        public IList<BpBir> GetByFilter(BpBirFilter filter)
        {
            if (filter == null)
                return null;

            IList<BpBir> list = null;
            BpIdentity identityAlias = null;
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<BpBir>()
                                    .Inner.JoinAlias(x => x.BpIdentity, () => identityAlias)
                                    .Where(x => x.Companyidenroll == filter.CompanyId);

                    if (filter.BpIdentity != null)
                    {
                        if (filter.BpIdentity.Id > 0)
                            criteria.Where(x => identityAlias.Id == filter.BpIdentity.Id);
                        if (!string.IsNullOrEmpty(filter.BpIdentity.TypeId))
                            criteria.Where(x => identityAlias.TypeId == filter.BpIdentity.TypeId);
                        if (!string.IsNullOrEmpty(filter.BpIdentity.ValueId))
                            criteria.Where(x => identityAlias.ValueId == filter.BpIdentity.ValueId);
                    }

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<BpBir>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar BpBirRepository.GetByFitler", e);
                return null;
            }
        }
    }
}