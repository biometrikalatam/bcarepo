﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.Bio.Infraestructura.Configuration;
using System.Collections.Generic;
using BCR.Bio.Domain;
using System;
using BCR.System.Log;

namespace BCR.Bio.Infraestructura
{
    public class TrackingResultRepository : Repository<ITrackingResult>
    {
        public ITrackingResult GetByName(string description)
        {
            try
            {
                ITrackingResult item;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    item = session.QueryOver<TrackingResult>()
                        .Where(x => x.Description == description)
                        .ReadOnly()
                        .SingleOrDefault();
                }
                return item;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar TrackingResultRepository -> GetByName", e);
                return null;
            }
        }
    }
}