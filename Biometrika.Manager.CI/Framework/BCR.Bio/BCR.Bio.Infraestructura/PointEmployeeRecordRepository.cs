﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using BCR.System.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using BCR.Bio.Domain.DTO.Filter;
using NHibernate.Criterion;

namespace BCR.Bio.Infraestructura
{
    public class PointEmployeeRecordRepository : Repository<IPointEmployeeRecord>
    {
        public IList<PointEmployeeRecordDTO> ListByCompanyId(int companyId)
        {
            IList<PointEmployeeRecordDTO> list;
            using (var session = BioNHibernateHelper.OpenSession())
            {
                Point pointAlias = null;
                User employeeAlias = null;
                //BpIdentity identityAlias = null;
                //User userAlias = null;

                list = session.QueryOver<IPointEmployeeRecord>()
                  .Inner.JoinAlias(x => x.Point, () => pointAlias)
                  .Inner.JoinAlias(x => x.User, () => employeeAlias)
                  //.Inner.JoinAlias(() => employeeAlias.Identity, () => identityAlias)
                  //.Left.JoinAlias(() => employeeAlias.User, () => userAlias)
                  .Where(x => x.Company.Id == companyId)
                  .Select(
                    x => x.Id,
                    x => x.StartActivity,
                    x => x.EndActivity,
                    x => pointAlias.Name,
                    x => employeeAlias.Name,
                    x => employeeAlias.Username
                    )
                    .List<object[]>()
                    .Select(p => new PointEmployeeRecordDTO()
                    {
                        Id = ConvertEx.ObjectToInt(p[0]),
                        StartActivity = (DateTime)ConvertEx.ObjectToDateTime(p[1]),
                        EndActivity = ConvertEx.ObjectToDateTime(p[2]),
                        PointName = ConvertEx.ObjectToString(p[3]),
                        EmployeeName = ConvertEx.ObjectToString(p[4]),
                        Username = ConvertEx.ObjectToString(p[5])
                    }).ToList();
            }
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IList<IPointEmployeeRecord> GetByUserId(int id, DateTime moment)
        {
            IList<IPointEmployeeRecord> list = null;
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    list = session.QueryOver<IPointEmployeeRecord>()
                       .Where(x => x.User.Id == id)
                       .And(x => moment >= x.StartActivity && (x.EndActivity == null || moment <= x.EndActivity))
                       .ReadOnly()
                       .List<IPointEmployeeRecord>()
                       .ToList();

                    return list;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar PointEmployeeRecordRepository.GetByUserId", e);
                return list;
            }
        }

        public IList<PointEmployeeRecord> GetByFilter(PointEmployeeRecordFilterDTO filter)
        {
            if (filter == null)
                return null;

            try
            {
                IList<PointEmployeeRecord> list = null;
                User userAlias = null;
                Point pointAlias = null;
                PointEmployeeRecord pointEmployeeRecordAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var subQuery = QueryOver.Of<PointEmployeeRecord>()
                                    .Left.JoinAlias(x => x.User, () => userAlias)
                                    .Left.JoinAlias(x => x.Point, () => pointAlias)
                                    .Where(x => x.Company.Id == filter.CompanyId);

                    if (filter.User != null)
                    {
                        if (filter.User.Id > 0)
                            subQuery.Where(x => userAlias.Id == filter.User.Id);
                    }

                    if (filter.Point != null)
                    {
                        if (filter.Point.Id > 0)
                            subQuery.Where(x => pointAlias.Id == filter.Point.Id);
                    }

                    subQuery.Select(x => x.Id);

                    var criteria = session.QueryOver<PointEmployeeRecord>(() => pointEmployeeRecordAlias)
                                    .WithSubquery.WhereProperty(x => x.Id).In(subQuery)
                                    .ReadOnly();

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<PointEmployeeRecord>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar PointEmployeeRecordRepository.GetByFilter", e);
                return null;
            }
        }

        public IList<IPointEmployeeRecord> GetByUserIdAndCodePoint(int id, DateTime moment, string code, bool onlyActive = true)
        {
            try
            {
                IList<IPointEmployeeRecord> list;
                Point pointAlias = null;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<IPointEmployeeRecord>()
                        .Inner.JoinAlias(x => x.Point, () => pointAlias)
                        .Where(x => x.User.Id == id);
                        

                    if (onlyActive)
                        criteria.Where(x => moment >= x.StartActivity && (x.EndActivity == null || moment <= x.EndActivity));
                    if (!string.IsNullOrEmpty(code))
                        criteria.Where(x => pointAlias.Code == code);

                    list = criteria.ReadOnly().List<IPointEmployeeRecord>().ToList();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error -> PointEmployeeRecordRepository.GetByUserIdAndCodePoint", e);
                return null;
            }
        }

        public IList<IPointEmployeeRecord> GetByPointId(int id)
        {
            IList<IPointEmployeeRecord> list;
            using (var session = BioNHibernateHelper.OpenSession())
            {
                list = session.QueryOver<IPointEmployeeRecord>()
                   .Where(x => x.Point.Id == id)
                   .ReadOnly()
                   .List<IPointEmployeeRecord>()
                   .ToList();
            }
            return list;
        }
    }
}