﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Infraestructura
{
    public class ResidentPermissionRepository : Repository<IResidentPermission>
    {

        public IList<IResidentPermission> GetByResidentId(int residentId, bool withResident = false, bool withUnity = false)
        {
            try
            {
                IList<IResidentPermission> residentPermissions;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria criteria = session.CreateCriteria<IResidentPermission>().Add(Restrictions.Eq("resident", residentId));
                    criteria.SetReadOnly(true);

                    residentPermissions = criteria.List<IResidentPermission>();

                    if (withResident)
                    {
                        foreach (var residentPermission in residentPermissions)
                        {
                            NHibernateUtil.Initialize(residentPermission.Resident);
                        }
                    }
                    if (withUnity)
                    {
                        foreach (var residentPermission in residentPermissions)
                        {
                            NHibernateUtil.Initialize(residentPermission.Unity);
                        }
                    }
                }
                return residentPermissions;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al obtener ResidentPermissionRepository => GetByResidentId", e);
                return null;
            }
        }
        public IList<IResidentPermission> GetByUnityId(int unityId, bool withResident = false, bool withUnity = false)
        {
            try
            {
                IList<IResidentPermission> residentPermissions;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria criteria = session.CreateCriteria<IResidentPermission>().Add(Restrictions.Eq("unity", unityId));
                    criteria.SetReadOnly(true);

                    residentPermissions = criteria.List<IResidentPermission>();

                    if (withResident)
                    {
                        foreach (var residentPermission in residentPermissions)
                        {
                            NHibernateUtil.Initialize(residentPermission.Resident);
                        }
                    }
                    if (withUnity)
                    {
                        foreach (var residentPermission in residentPermissions)
                        {
                            NHibernateUtil.Initialize(residentPermission.Unity);
                        }
                    }

                }
                return residentPermissions;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al obtener ResidentPermissionRepository => GetByUnityId", e);
                return null;
            }
        }
        public IResidentPermission GetByUnityIdAndResidentId(int unityId, int residentId, bool withResident = false, bool withUnity = false)
        {
            try
            {
                IResidentPermission residentPermission;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    ICriteria criteria = session.CreateCriteria<IResidentPermission>()
                        .Add(Restrictions.Eq("unity", unityId))
                        .Add(Restrictions.Eq("resident", residentId))
                        ;

                    criteria.SetReadOnly(true);

                    residentPermission = criteria.UniqueResult<IResidentPermission>();

                    if (withResident)
                        NHibernateUtil.Initialize(residentPermission.Resident);
                    if (withUnity)
                        NHibernateUtil.Initialize(residentPermission.Unity);
                }
                return residentPermission;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al obtener ResidentPermissionRepository => GetByUnityId", e);
                return null;
            }
        }

    }
}
