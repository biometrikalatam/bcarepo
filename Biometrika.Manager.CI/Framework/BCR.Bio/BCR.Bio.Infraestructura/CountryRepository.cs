﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class CountryRepository : Repository<ICountry>
    {
        public IList<Country> GetList()
        {
            try
            {
                IList<Country> list;
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    list = session.QueryOver<Country>()
                           .List<Country>();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar CountryRepository -> GetList", e);
                return null;
            }
        }

        /// <summary>
        /// GetByFilter es un filtrado en base a el nuevo tipado de fltros
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public CountryFilterDTO GetByFilter(CountryFilterDTO filter)
        {
            IList<Country> list = new List<Country>();

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Country>()
                                   .OrderBy(x => x.Description).Asc;

                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.WhereRestrictionOn(x => x.Description).IsLike(filter.Description + "%");

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                    }

                    list = criteria.List<Country>();

                    if (filter.Paginacion != null)
                    {
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    filter.ResultList = list;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar CountryRepository -> GetByFilter", e);
                return null;
            }

            return filter;
        }

        /// <summary>
        /// Busqueda de nacionalidad por acronimo Alpha3 ISO3166
        /// </summary>
        /// <param name="Alpha3">Codigo ISO3166-Alpha3</param>
        /// <returns></returns>
        public Country GetByAlpha3(string Alpha3)
        {
            Country item = null;

            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                    item = session.QueryOver<Country>()
                           .Where(x => x.Alpha3 == Alpha3)
                           .List<Country>()
                           .FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar CountryRepository -> GetByFilter", e);
                return null;
            }

            return item;
        }
    }
}