﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura
{
    public class IdentityContactFormRepository : Repository<IdentityContactForm>
    {

        /// <summary>
        /// Permite recuperar los contactos asociados a una identidad
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IList<IdentityContactForm> GetByIdentity(int id)
        {
            IList<IdentityContactForm> listIdentityContact = null;
            ContactForm contactformAlias=null;

            using (var session = BioNHibernateHelper.OpenSession())
            {
                listIdentityContact = session.QueryOver<IdentityContactForm>()
                                .Inner.JoinAlias(x => x.ContactForm, () => contactformAlias)
                                 .Where(x =>x.Identity.Id == id)
                                 .List<IdentityContactForm>();

            }
            return listIdentityContact;
        }

        public IList<IdentityContactForm> GetByFilter(IdentityContactFormFilter filter)
        {
            if (filter == null)
                return null;

            try
            {
                IList<IdentityContactForm> list = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<IdentityContactForm>();

                    if (filter.IdentityId > 0)
                        criteria.Where(x => x.Identity.Id == filter.IdentityId);

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    list = criteria.List<IdentityContactForm>();
                }

                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar IdentityContactFormRepository.GetByFilter", e);
                return null;
            }
        }
    }
}
