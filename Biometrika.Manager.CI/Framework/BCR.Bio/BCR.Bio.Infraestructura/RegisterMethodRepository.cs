﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate.Transform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura
{
    public class RegisterMethodRepository : Repository<RegisterMethod>
    {

        

        /// <summary>
        /// Retorna todos los registros de la tabla RegisterMethod
        /// </summary>
        /// <returns></returns>
        public IList<RegisterMethodDTO> GetAll()
        {
            IList<RegisterMethodDTO> listRegisterMethod = null;
            RegisterMethod registerMethodalias = null;
            RegisterMethodDTO result = null;
            try
            {

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    listRegisterMethod = session.QueryOver<RegisterMethod>(()=> registerMethodalias)
                            .SelectList(list => list
                             .Select(() => registerMethodalias.Id).WithAlias(() => result.Id)
                             .Select(() => registerMethodalias.Name).WithAlias(() => result.description)
                             )
                        .TransformUsing(Transformers.AliasToBean<RegisterMethodDTO>())
                        .List<RegisterMethodDTO>();

                   

                       


                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar los métodos de registros", e);
            }
            return listRegisterMethod;
        }
    }
}
