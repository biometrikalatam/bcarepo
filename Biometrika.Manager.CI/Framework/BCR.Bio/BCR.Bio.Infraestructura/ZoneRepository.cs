﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Infraestructura
{
    public class ZoneRepository : Repository<Zone>
    {
        public IList<Zone> GetByCompany(int companyId)
        {
            try
            {
                IList<Zone> list;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    HQ hqAlias = null;
                    Building buildingAlias = null; // parala query unica

                    list = session.QueryOver<Zone>()
                      .Inner.JoinAlias(x => x.HQ, () => hqAlias)
                      .Where(() => hqAlias.Company.Id == companyId)
                      .ReadOnly()
                      .List<Zone>().ToList();
                }
                return list;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar ZoneRepository -> GetByCompany", e);
                return null;
            }
        }

        /// <summary>
        /// GetByFilter es un filtrado en base a el nuevo tipado de fltros
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ZoneFilterDTO GetByFilter(ZoneFilterDTO filter)
        {
            IList<Zone> list = new List<Zone>();

            try
            {
                HQ hqAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    var criteria = session.QueryOver<Zone>()
                                    .Left.JoinAlias(x => x.HQ, () => hqAlias)
                                    .Where(x => hqAlias.Company.Id == filter.CompanyId)
                                    .OrderBy(x => x.Description).Asc;

                    if (filter.Id > 0)
                        criteria.Where(x => x.Id == filter.Id);
                    if (!string.IsNullOrEmpty(filter.Description))
                        criteria.Where(x => x.Description == filter.Description);
                    if (!string.IsNullOrEmpty(filter.DescriptionLike))
                        criteria.WhereRestrictionOn(x => x.Description).IsLike(filter.DescriptionLike + "%");

                    // filtro por HQ
                    if (filter.HQId > 0)
                        criteria.And(x => hqAlias.Id == filter.HQId);

                    if (filter.Paginacion != null)
                    {
                        criteria.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        criteria.Take(filter.Paginacion.Cantidad);
                    }

                    list = criteria.List<Zone>();

                    if (filter.Paginacion != null)
                    {
                        filter.Paginacion.Total = criteria.RowCount();
                    }

                    filter.ResultList = list;
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar ZoneRepository -> GetByFilter", e);
                return null;
            }

            return filter;
        }
    }
}