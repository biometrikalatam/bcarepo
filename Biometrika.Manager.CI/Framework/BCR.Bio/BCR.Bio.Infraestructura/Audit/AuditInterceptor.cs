﻿using BCR.Bio.Infraestructura.Configuration;
using BCR.System.Configuration;
using BCR.System.Enum;
using BCR.System.Log;
using NHibernate;
using NHibernate.SqlCommand;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Audit
{
    /// <summary>
    /// Nhibernate Interceptor implementation for audit
    /// Note : audit must be run in parallel task
    /// </summary>
    public class AuditInterceptor : EmptyInterceptor
    {
        private AuditRepository auditRepository;

        /// <summary>
        /// Interceptor ctor.
        /// </summary>
        /// <param name="connectionCollection">Connection list</param>
        //public AuditInterceptor(ConnectionCollection connectionCollection)
        //{
        //    auditRepository = new AuditRepository(connectionCollection);
        //}

        public AuditInterceptor()
        {
            //if (session != null)
            //    auditRepository = new AuditRepository(session, null);
        }

        public void SetSessionForAuditInterceptor(ISession session)
        {
            //if (session != null)
                //auditRepository = new AuditRepository();
        }

        public override SqlString OnPrepareStatement(SqlString sql)
        {
            //Log4Bio.Info(sql.ToString());
            //Trace.WriteLine(sql.ToString());
            return sql;
        }

 
        //public override void OnDelete(object entity, object id, object[] state, string[] propertyNames, NHibernate.Type.IType[] types)
        //{
        //    var task1 = Task.Run(() => AuditObjectModificationAsync(entity, id, AuditAction.Delete, propertyNames, state));

        //    base.OnDelete(entity, id, state, propertyNames, types);
        //}

        //public override bool OnFlushDirty(object entity, object id, object[] currentState, object[] previousState, string[] propertyNames, NHibernate.Type.IType[] types)
        //{
        //    var task1 = Task.Run(() => AuditObjectModificationAsync(entity, id, AuditAction.Update, propertyNames, previousState));

        //    return base.OnFlushDirty(entity, id, currentState, previousState, propertyNames, types);
        //}

        //public override bool OnSave(object entity, object id, object[] state, string[] propertyNames, NHibernate.Type.IType[] types)
        //{
        //    var task1 = Task.Run(() => AuditObjectModificationAsync(entity, id, AuditAction.Create, propertyNames, state));

        //    return base.OnSave(entity, id, state, propertyNames, types);
        //}

        private void AuditObjectModificationAsync(object entity, object id, AuditAction auditAction, string[] propertyNames, object[] state)
        {
            /// TODO : Implement USERD ID here

            //if (entity.GetType().Name == "Audit") return;

            //Domain.Audit audit = new Domain.Audit
            //{
            //    AuditDate = DateTime.Now,
            //    ObjectName = entity.GetType().Name,
            //    ObjectId = id == null ? null : id.ToString(),
            //    Action = auditAction
            //    //Username = UserAuth.CurrentUserName
            //};

            //auditRepository.Create(audit);
        }
    }
}