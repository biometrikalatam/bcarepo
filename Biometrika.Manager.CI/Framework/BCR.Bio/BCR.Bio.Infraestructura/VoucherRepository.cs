﻿using System;
using System.Collections.Generic;
using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Log;
using NHibernate.Criterion;

namespace BCR.Bio.Infraestructura
{
    public class VoucherRepository : Repository<IVoucher>
    {
        public IList<IVoucher> GetByCompanyId(int companyId)
        {
            try
            {
                using (var session = BioNHibernateHelper.OpenSession())
                {
                     var list = session.QueryOver<IVoucher>()
                        .Where(x => x.Company.Id == companyId);
                        return list.List<IVoucher>();
                }

            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar GetVouchersByCompany", e);
                return null;
            }
        }
        public IList<IVoucher> GetByFilter(VoucherFilterDTO filter)
        {
            if (filter == null)
                return null;
            try
            {
                BpIdentity bpIdentityAlias = null;
                CentroDeCostos centroDeCostosAlias = null;

                using (var session = BioNHibernateHelper.OpenSession())
                {
                    //IList<IVoucher> list = null;

                    var list = session.QueryOver<IVoucher>()
                         .Inner.JoinAlias(x => x.Identity, () => bpIdentityAlias)
                         .Inner.JoinAlias(x => x.CentroDeCostos, () => centroDeCostosAlias)
                         .Where(x => x.Company.Id == filter.CompanyId);

                    if (filter.Desde != null)
                        list.And(x => x.CreateDate > filter.Desde.Value.Date);
                    if (filter.Hasta != null)
                        list.And(x => x.CreateDate < filter.Hasta.Value.Date.AddDays(1));

                    if (!string.IsNullOrEmpty(filter.TypeId))
                        list.And(x => bpIdentityAlias.TypeId == filter.TypeId);
                    if (!string.IsNullOrEmpty(filter.ValueId))
                        list.And(x => bpIdentityAlias.ValueId == filter.ValueId);
                    if (!string.IsNullOrEmpty(filter.NameLike))
                        list.WhereRestrictionOn(x => bpIdentityAlias.Name).IsLike("%" + filter.NameLike + "%");
                    if (!string.IsNullOrEmpty((filter.FatherLastnameLike)))
                        list.WhereRestrictionOn(x => bpIdentityAlias.PatherLastname).IsLike("%" + filter.FatherLastnameLike + "%");
                    if (filter.CentroDeCosto != null)
                        list.And(x => centroDeCostosAlias.Id == filter.CentroDeCosto.Id);

                    if (filter.Paginacion != null)
                    {
                        list.Skip(filter.Paginacion.Pagina * filter.Paginacion.Cantidad);
                        list.Take(filter.Paginacion.Cantidad);
                        filter.Paginacion.Total = list.RowCount();
                    }

                    list.OrderBy(x => x.CreateDate).Desc();

                    var results  = list.List<IVoucher>();

                    return results;
                }

            }
            catch (Exception ex)
            {
                Log4Bio.Error("Error al recuperar Voucher by Filter" + ex.ToString());
                return null;
            }
        }
    }
}
