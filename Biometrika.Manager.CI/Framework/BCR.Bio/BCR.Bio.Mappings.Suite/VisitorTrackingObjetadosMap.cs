﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite
{
    public class VisitorTrackingObjetadosMap : ClassMap<VisitorTrackingObjetados>
    {
        public VisitorTrackingObjetadosMap()
        {
            Table("[VisitorTrackingObjetados]");
            Id(x => x.Id).Column("[id]").GeneratedBy.Identity();
            References(x => x.Identity).Column("[identity]");
            Map(x => x.TimeStamp).Column("[timestamp]");
            References(x => x.Action).Column("[action]");
            References(x => x.Result).Column("[result]");
            References(x => x.Point).Column("[point]");
            References(x => x.Company).Column("[company]");
        }
    }
}
