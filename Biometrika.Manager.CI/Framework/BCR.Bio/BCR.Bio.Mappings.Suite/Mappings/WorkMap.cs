﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    class WorkMap : ClassMap<Work>
    {
        public WorkMap()
        {
            Table("Work");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
            References(x => x.Company).Column("[company]").Class<Company>();

        }
    
    }
}
