﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class HolidayMap: ClassMap<Holiday>
    {

        public HolidayMap()
        {
            Table("Holiday");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.StartDate).Column("startDate").Not.Nullable();
            Map(x => x.EndDate).Column("endDate").Not.Nullable();
            Map(x => x.CreateDate).Column("createDate").Not.Nullable();
            Map(x => x.UpdateDate).Column("updateDate");
            References(x => x.Unity).Column("[unity]").Class<Unity>().Not.LazyLoad();
            References(x => x.Company).Column("[company]").Class<Company>().Not.LazyLoad().Not.Nullable();
        }
    }
}
