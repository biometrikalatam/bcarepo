﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    

    public class BuildingMap : ClassMap<Building>
    {
        public BuildingMap()
        {
            Table("Building");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");
            Map(x => x.Address).Column("address");


            References(x => x.Zone).Column("zone").Class(typeof(Zone)).Not.LazyLoad();

            HasMany(x => x.Unitys)
                .Cascade
                .None().Inverse()
                .Table("Unity")
                .KeyColumn("building").Not.LazyLoad();
        }
    }
}
