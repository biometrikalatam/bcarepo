﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class GroupMap : ClassMap<Group>
    {
        public GroupMap()
        {
            Table("[Group]");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");

            Map(x => x.EndDate).Column("endDate");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");

            HasManyToMany(x => x.Visitors)
              .Cascade
              .SaveUpdate()
              .Table("VisitorGroup")
              .ParentKeyColumn("[group]")
              .ChildKeyColumn("visitor");//.Not.LazyLoad();
        }
    }
}