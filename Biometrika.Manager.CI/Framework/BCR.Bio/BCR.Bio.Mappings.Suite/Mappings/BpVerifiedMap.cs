﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class BpVerifiedMap : ClassMap<BpVerified>
    {
        public BpVerifiedMap()
        {
            Table("bp_verified");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Datatype).Column("datatype");
            Map(x => x.Data).Column("data");
            Map(x => x.Additionaldata).Column("additionaldata");
            Map(x => x.Timestamp).Column("timestamp");
            Map(x => x.Insertoption).Column("insertoption");
            Map(x => x.Matchingtype).Column("matchingtype");

            References(x => x.BpTx, "txid").Unique();
        }
    }
}