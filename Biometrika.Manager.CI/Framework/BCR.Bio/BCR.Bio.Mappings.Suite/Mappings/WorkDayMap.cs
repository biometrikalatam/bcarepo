﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class WorkDayMap : ClassMap<WorkDay>
    {
        public WorkDayMap()
        {
            Table("workDay");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Name).Column("name");
            Map(x => x.StartTime).Column("startTime");
            Map(x => x.EndTime).Column("endTime");
            Map(x => x.WorkTime).Column("workTime");
            Map(x => x.StartDate).Column("startDate");
            Map(x => x.EndDate).Column("endDate");
        }
    }
}