﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class BenefitOptionMap : ClassMap<BenefitOption>
    {
        public BenefitOptionMap()
        {
            Table("BenefitOption");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Description).Column("description");
            Map(x => x.Activo).Column("active");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
            References(x => x.Benefit).Column("benefit");
        }
    }
}
