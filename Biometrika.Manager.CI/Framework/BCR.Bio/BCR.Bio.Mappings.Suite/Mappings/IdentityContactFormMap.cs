﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class IdentityContactFormMap : ClassMap<IdentityContactForm>
    {
        public IdentityContactFormMap()
        {
            Table("[IdentityContactForm]");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Identity).Column("[identity]").Class<BpIdentity>();
            References(x => x.ContactForm).Column("[contactForm]").Class<ContactForm>();
            Map(x => x.ValueForm).Column("[valueForm]");
        }
    }
}
