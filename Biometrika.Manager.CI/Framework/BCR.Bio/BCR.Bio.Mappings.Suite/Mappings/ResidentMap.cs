﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class ResidentMap : ClassMap<Resident>
    {
        public ResidentMap()
        {
            Table("Resident");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Identity).Column("[identity]").Class(typeof(BpIdentity)).Not.LazyLoad();
            Map(x => x.EndDate).Column("endDate");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
 
            HasMany(x => x.ResidentAuthorizators)
              .Cascade
              .All() 
              .Table("ResidentAuthorizator")
              .KeyColumn("resident").Not.LazyLoad();

            HasManyToMany(x => x.Vehicles)
            .Cascade
            .Merge()
            .Table("ResidentVehicle")
            .ParentKeyColumn("resident")
            .ChildKeyColumn("vehicle");
            //.Not.LazyLoad();

            HasManyToMany(x => x.Unitys)
            .Cascade
            .SaveUpdate()
            .Table("ResidentUnity")
            .ParentKeyColumn("resident")
            .ChildKeyColumn("unity");
            //.Not.LazyLoad();
        }
    }
}