﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class ResidentPermissionMap : ClassMap<ResidentPermission>
    {
        public ResidentPermissionMap()
        {
            Table("ResidentPermission");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.CanInvite).Column("canInvite");
            Map(x => x.CanEditPermissions).Column("canEditPermissions");
            Map(x => x.CanAddFamilyMembers).Column("canAddFamilyMembers");
            Map(x => x.IsLeader).Column("isLeader");

            References(x => x.Unity).Column("[unity]").Class(typeof(Unity)).Not.LazyLoad();
            References(x => x.Resident).Column("[resident]").Class(typeof(Resident)).Not.LazyLoad();

            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
        }
    }
}
