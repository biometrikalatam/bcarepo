﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    internal class ResidentAuthorizatorMap : ClassMap<ResidentAuthorizator>
    {
        public ResidentAuthorizatorMap()
        {
            Table("[ResidentAuthorizator]");

            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.EndDate).Column("enddate");

            Map(x => x.Category).Column("category");

            References(x => x.Resident).Column("resident").Class(typeof(Resident));
        }
    }
}