﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class VehicleBlackListMap : ClassMap<VehicleBlackList>
    {
        public VehicleBlackListMap()
        {
            Table("VehicleBlackList");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Color).Column("color");
            Map(x => x.RazonIn).Column("razonIn").Length(100000);
            Map(x => x.RazonOut).Column("razonOut").Length(100000);
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");

            Map(x => x.EndDate).Column("endDate");
            Map(x => x.PlateNumber).Column("plateNumber");

            References(x => x.Make).Column("make").Class(typeof(VehicleMake));
            References(x => x.Model).Column("model").Class(typeof(VehicleModel));
            References(x => x.Type).Column("type").Class(typeof(VehicleType));

            References(x => x.UserIn).Column("userIn").Class(typeof(User));
            References(x => x.UserOut).Column("userOut").Class(typeof(User));

            References(x => x.Company).Column("company").Class(typeof(Company));
            References(x => x.Vehicle).Column("[vehicle]").Class<Vehicle>();

            HasManyToMany(x => x.Residents)
             .Cascade
             .All()
             .Table("ResidentVehicle")
             .ParentKeyColumn("vehicle")
             .ChildKeyColumn("resident");

            HasManyToMany(x => x.Visitors)
             .Cascade
             .All()
             .Table("VisitorVehicle")
             .ParentKeyColumn("vehicle")
             .ChildKeyColumn("visitor");
        }
    }
}