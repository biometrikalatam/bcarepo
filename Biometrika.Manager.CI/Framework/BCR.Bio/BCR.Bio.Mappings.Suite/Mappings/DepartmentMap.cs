﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    class DepartmentMap : ClassMap<Department>
    {
        public DepartmentMap()
        {
            Table("Department");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.description).Column("description");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
            References(x => x.company).Column("[company]").Class<Company>();

            HasMany(x => x.Employees)
               .KeyColumn("id");               

        }
    }
}
