﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class EmployeeMap : ClassMap<Employee>
    {
        public EmployeeMap()
        {
            Table("Employee");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Identity).Column("[identity]").Class(typeof(BpIdentity)).Unique().Cascade.All();             
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
            Map(x => x.EndDate).Column("endDate");
            Map(x => x.TimeAssistance).Column("timeassistance");
            Map(x => x.TemporalDateTicket).Column("[temporaldateticket]");
            Map(x => x.Autorizador).Column("[autorizador]");

            References(x => x.CentroCosto).Column("CentroCosto").Class(typeof(CentroDeCostos));
            References(x => x.User).Column("[User]").Class<User>();
            References(x => x.Work).Column("[work]").Class<Work>();
            References(x => x.Department).Column("[department]").Class(typeof(Department));

            HasMany(x => x.EmployeeOptionBenefit).Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.EmployeeRegisterMethod).Cascade.AllDeleteOrphan().Inverse();

            //Traemos la colección de opciones de beneficios asociados a un empleado.
            HasManyToMany(x => x.BenefitOption)
                .Table("EmployeeOptionBenefit")
                .ParentKeyColumn("employee")
                .ChildKeyColumn("benefitoption")
                .Cascade.All();

            //Traemos la colección de opciones de registros biométricos de un empleado.
            HasManyToMany(x => x.RegisterMethod)
                 .Table("EmployeeRegisterMethod")
                 .ParentKeyColumn("employee")
                 .ChildKeyColumn("registermethod")
                 .Cascade.None()
                 .Not.LazyLoad();

            HasManyToMany(x => x.Unitys)
                .Cascade
                .SaveUpdate()
                .Table("[EmployeeUnity]")
                .ParentKeyColumn("[employee]")
                .ChildKeyColumn("[unity]")
                .Not.LazyLoad();

        }
    }
}
