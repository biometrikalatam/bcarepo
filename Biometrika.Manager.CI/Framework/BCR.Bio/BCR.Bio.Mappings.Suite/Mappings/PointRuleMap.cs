﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class PointRuleMap : ClassMap<PointRule>
    {
        public PointRuleMap()
        {
            Table("[PointRule]");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");

            Map(x => x.Label).Column("label");
        }
    }
}