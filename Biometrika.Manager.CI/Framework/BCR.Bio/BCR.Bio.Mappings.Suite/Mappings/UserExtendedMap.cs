﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    // TODO: Solo para etapa 1 Lomas la Dehesa, quitar al pasar a la Etapa 2
    public class UserExtendedMap : ClassMap<UserExtended>
    {
        public UserExtendedMap()
        {
            Table("[UserExtended]");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.User).Column("[user]").Class<User>();
            Map(x => x.DecryptedPassword).Column("[decryptedPassword]");
        }
    }
}
