﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class VehicleMap : ClassMap<Vehicle>
    {
        public VehicleMap()
        {
            Table("Vehicle");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Color).Column("color");
            Map(x => x.Detail).Column("detail");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");

            Map(x => x.EndDate).Column("endDate");
            Map(x => x.PlateNumber).Column("plateNumber");

            References(x => x.Make).Column("make").Class(typeof(VehicleMake)).Not.LazyLoad();
            References(x => x.Model).Column("model").Class(typeof(VehicleModel)).Not.LazyLoad();
            References(x => x.Type).Column("type").Class(typeof(VehicleType)).Not.LazyLoad();
            References(x => x.IdentityState).Column("[identityState]").Class<IdentityState>().Not.LazyLoad();

            References(x => x.Company).Column("company").Class(typeof(Company));

            HasManyToMany(x => x.Residents)
             .Cascade
             .SaveUpdate()
             .Table("ResidentVehicle")
             .ParentKeyColumn("vehicle")
             .ChildKeyColumn("resident");

            HasManyToMany(x => x.Visitors)
             .Cascade
             .SaveUpdate()
             .Table("VisitorVehicle")
             .ParentKeyColumn("vehicle")
             .ChildKeyColumn("visitor");

            HasMany(x => x.VehicleBlackList)
                .Cascade
                .None()
                .Table("[VehicleBlackList]")
                .KeyColumn("[vehicle]").Not.LazyLoad();

            HasManyToMany(x => x.ProviderList)
             .Cascade
             .SaveUpdate()
             .Table("[VehicleProvider]")
             .ParentKeyColumn("[vehicle]")
             .ChildKeyColumn("[provider]");
        }
    }
}