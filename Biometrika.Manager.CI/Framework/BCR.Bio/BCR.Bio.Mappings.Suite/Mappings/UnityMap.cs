﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class UnityMap : ClassMap<Unity>
    {
        public UnityMap()
        {
            Table("Unity");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");
            Map(x => x.Address).Column("address");

            References(x => x.Building).Column("building").Class<Building>();//.Not.LazyLoad();

            References(x => x.Zone).Column("zone").Class<Zone>().Not.LazyLoad();

            HasManyToMany(x => x.Residents)
              .Table("ResidentUnity")
              .ParentKeyColumn("unity")
              .ChildKeyColumn("resident");//.Not.LazyLoad();

            //HasManyToMany(x => x.Visitors)
            //  .Table("VisitorUnity")
            //  .ParentKeyColumn("unity")
            //  .ChildKeyColumn("visitor");//.Not.LazyLoad();


            HasManyToMany(x => x.Employees)
              .Table("EmployeeUnity")
              .ParentKeyColumn("unity")
              .ChildKeyColumn("employee");//.Not.LazyLoad();
        }
    }
}