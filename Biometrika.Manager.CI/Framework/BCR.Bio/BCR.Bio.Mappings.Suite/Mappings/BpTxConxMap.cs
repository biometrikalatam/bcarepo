﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class BpTxConxMap: ClassMap<BpTxConx>
    {
        public BpTxConxMap()
        {
            Table("bp_tx_conx");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Consultationtype).Column("consultationtype");
            Map(x => x.Connectorid).Column("connectorid");
            Map(x => x.Trackid).Column("trackid");
            Map(x => x.Status).Column("status");
            Map(x => x.Result).Column("result");
            Map(x => x.Score).Column("score");
            Map(x => x.Threshold).Column("threshold");
            Map(x => x.Timestamp).Column("timestamp");

            References(x => x.BpTx, "txid").Unique();
        }
    }
}
