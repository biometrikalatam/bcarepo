﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class VehicleTrackingMap : ClassMap<VehicleTracking>
    {
        public VehicleTrackingMap()
        {
            Table("[VehicleTracking]");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.NumberPlate).Column("[numberPlate]");
            References(x => x.Vehicle).Column("[vehicle]").Class<Vehicle>().Not.LazyLoad();
            References(x => x.Company).Column("[company]").Class<Company>();
            Map(x => x.CreateDate).Column("[createDate]").CustomType("timestamp").CustomSqlType("TIMESTAMP(6) WITH TIME ZONE");
            Map(x => x.UpdateDate).Column("[updateDate]").CustomType("timestamp").CustomSqlType("TIMESTAMP(6) WITH TIME ZONE");
            Map(x => x.RecognitionTime).Column("[recognitionTime]").CustomType("timestamp").CustomSqlType("TIMESTAMP(6) WITH TIME ZONE");
            Map(x => x.Confidence).Column("[confidence]");
            Map(x => x.InBlacklist).Column("[inBlacklist]");
            Map(x => x.FileTime).Column("[fileTime]").CustomType("timestamp").CustomSqlType("TIMESTAMP(6) WITH TIME ZONE");
            Map(x => x.IsOpen).Column("[isOpen]");
            Map(x => x.OpenTime).Column("[openTime]").CustomType("timestamp").CustomSqlType("TIMESTAMP(6) WITH TIME ZONE");
            References(x => x.Point).Column("[point]").Class<Point>().Not.LazyLoad();
        }
    }
}
