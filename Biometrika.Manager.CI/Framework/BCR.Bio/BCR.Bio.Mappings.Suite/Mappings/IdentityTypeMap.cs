﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class IdentityTypeMap : ClassMap<IdentityType>
    {
        public IdentityTypeMap()
        {
            Table("[IdentityType]");
            Id(x => x.Id).Column("[id]").GeneratedBy.Identity();
            Map(x => x.Description).Column("[description]");
            Map(x => x.Class).Column("[class]");
            References(x => x.Company).Column("[company]").Class<Company>();
        }
    }
}
