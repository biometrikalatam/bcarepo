﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class VisitCounterResetDateMap : ClassMap<VisitCounterResetDate>
    {
        public VisitCounterResetDateMap()
        {
            Table("VisitCounterResetDate");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.CreateDate).Column("createDate").Not.Nullable();
            Map(x => x.UpdateDate).Column("updateDate");
            References(x => x.Company).Column("[companyId]").Class<Company>().Not.Nullable();
            Map(x => x.ResetDate).Column("resetDate").Not.Nullable();
        }
    }
}
