﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class TrackingActionMap : ClassMap<TrackingAction>
    {
        public TrackingActionMap()
        {
            Table("TrackingAction");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");

            Map(x => x.Label).Column("label");
            Map(x => x.Value).Column("[value]");
        }
    }
}