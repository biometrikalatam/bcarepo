﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    class EmployeeManagerDepartmentMap : ClassMap<EmployeeManagerDepartment>
    {
        public EmployeeManagerDepartmentMap()
        {
            Table("EmployeeManagerDepartment");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
            References(x => x.employee).Column("[employee]").Class<Employee>();
            References(x => x.department).Column("[department]").Class<Department>();


        }
    }
}
