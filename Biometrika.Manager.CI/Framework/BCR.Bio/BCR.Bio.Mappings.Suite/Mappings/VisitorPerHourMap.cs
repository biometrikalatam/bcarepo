﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class VisitorPerHourMap : ClassMap<VisitorPerHour>
    {
        public VisitorPerHourMap()
        {
            ReadOnly();
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Fecha).Column("Fecha");
            Map(x => x.Hora).Column("Hora");
            Map(x => x.Total).Column("Total");
        }
    }
}
