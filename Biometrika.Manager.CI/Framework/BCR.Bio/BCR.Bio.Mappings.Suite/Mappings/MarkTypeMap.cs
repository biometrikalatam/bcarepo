﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class MarkTypeMap : ClassMap<MarkType>
    {
        public MarkTypeMap()
        {
            Table("MarkType");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");
        }
    }
}