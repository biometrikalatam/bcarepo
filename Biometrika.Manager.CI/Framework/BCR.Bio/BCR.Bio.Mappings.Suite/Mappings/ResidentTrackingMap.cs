﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class ResidentTrackingMap : ClassMap<ResidentTracking>
    {
        public ResidentTrackingMap()
        {
            Table("[ResidentTracking]");
            Id(x => x.Id).Column("[id]").GeneratedBy.Identity();
            Map(x => x.EventTime).Column("[eventTime]");
            References(x => x.Point).Column("[point]").Class<Point>().Not.LazyLoad();
            References(x => x.TrackingAction).Column("[action]").Class<TrackingAction>().Not.LazyLoad();
            References(x => x.Resident).Column("[resident]").Class<Resident>().Not.LazyLoad();
            References(x => x.TrackingResult).Column("[result]").Class<TrackingResult>().Not.LazyLoad();
            Map(x => x.Comment).Column("[comment]");
        }
    }
}
