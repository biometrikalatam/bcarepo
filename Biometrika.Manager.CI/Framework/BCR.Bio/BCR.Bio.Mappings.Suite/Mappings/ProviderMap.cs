﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class ProviderMap : ClassMap<Provider>
    {
        public ProviderMap()
        {
            Table("[Provider]");

            DynamicUpdate();
            DynamicInsert();
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.IdentifierCode).Column("identifierCode");

            Map(x => x.Address).Column("address");

            Map(x => x.CreateDate).Column("createDate");

            Map(x => x.ContactName).Column("contactName");

            Map(x => x.EndDate).Column("enddate");

            Map(x => x.Name).Column("name");
            Map(x => x.Phone).Column("phone");

            Map(x => x.Status).Column("status");
            Map(x => x.Outsourcing).Column("outsourcing");

            Map(x => x.UpdateDate).Column("updateDate");

            References(x => x.Company).Column("company").Class<Company>();

            HasMany(x => x.VisitorList)
                .Cascade
                .None()
                .Table("[Visitor]")
                .KeyColumn("[provider]");

            HasManyToMany(x => x.VehicleList)
              .Table("[VehicleProvider]")
              .ParentKeyColumn("[provider]")
              .ChildKeyColumn("[vehicle]");
        }
    }
}