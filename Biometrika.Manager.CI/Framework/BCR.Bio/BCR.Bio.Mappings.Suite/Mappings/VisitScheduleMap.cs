﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class VisitScheduleMap: ClassMap<VisitSchedule>
    {
        public VisitScheduleMap()
        {
            Table("VisitSchedule");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.CreateDate).Column("createDate").Not.Nullable();
            Map(x => x.UpdateDate).Column("updateDate");
            Map(x => x.Used).Column("used");
            Map(x => x.EventDate).Column("eventDate").Not.Nullable();
            Map(x => x.StartTime).Column("startTime").Not.Nullable();
            Map(x => x.EndTime).Column("endTime");


            References(x => x.Unity).Column("[unity]").Class<Unity>().Not.LazyLoad();
            References(x => x.Visitor).Column("[visitor]").Class<Visitor>().Not.LazyLoad().Not.Nullable();
            References(x => x.Employee).Column("[employee]").Class<Employee>().Not.LazyLoad();
            References(x => x.Company).Column("[company]").Class<Company>().Not.LazyLoad().Not.Nullable();
        }

    }
}
