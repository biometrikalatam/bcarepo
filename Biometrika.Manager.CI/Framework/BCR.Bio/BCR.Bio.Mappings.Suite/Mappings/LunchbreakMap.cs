﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class LunchbreakMap : ClassMap<Lunchbreak>
    {
        public LunchbreakMap()
        {
            Table("EmployeeLunchBreak");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Minutes).Column("minutes");
            Map(x => x.Name).Column("name");
            Map(x => x.WorkHours).Column("workhours");

            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");

            References(x => x.Company).Column("company").Class<Company>();

        }
    }
}
