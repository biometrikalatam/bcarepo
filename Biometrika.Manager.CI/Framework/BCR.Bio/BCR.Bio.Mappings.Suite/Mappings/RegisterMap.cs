﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite
{
    class RegisterMethodMap: ClassMap<RegisterMethod>    
    {
        public RegisterMethodMap()
        {
            Table("RegisterMethod");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Column("name");

        }

    }
}
