﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class PointEmployeeRecordMap : ClassMap<PointEmployeeRecord>
    {
        public PointEmployeeRecordMap()
        {
            Table("PointEmployeeRecord");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.StartActivity).Column("startActivity");
            Map(x => x.EndActivity).Column("endActivity");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");

            References(x => x.Company).Column("[company]").Class(typeof(Company)).Not.LazyLoad();
            References(x => x.User).Column("[employee]").Class(typeof(User)).Not.LazyLoad();
            References(x => x.Point).Column("[point]").Class(typeof(Point)).Not.LazyLoad();

        }
    }
}