﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class SectorMap : ClassMap<Sector>
    {
        public SectorMap()
        {
            Table("Sector");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("Description");
            Map(x => x.ContactName).Column("ContactName");
            Map(x => x.EndDate).Column("EndDate");
            Map(x => x.CreateDate).Column("CreateDate");
            Map(x => x.UpdateDate).Column("UpdateDate");
            Map(x => x.externalid).Column("externalid");

            HasManyToMany(x => x.Companies)
                     .Cascade
                     .All()
                     .Table("CompanySector")
                     .ParentKeyColumn("sector")
                     .ChildKeyColumn("company");

            HasMany(x => x.Points)
               .Cascade
               .None()
               .Table("Point")
               .KeyColumn("Sector");
        }
    }
}