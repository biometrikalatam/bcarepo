﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BCR.Bio.Domain;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class AutorizationMap : ClassMap<Autorization>
    {
        public AutorizationMap()
        {
            Table("Autorization");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Motivo).Column("motivo");
            Map(x => x.StartDate).Column("startDate");
            Map(x => x.EndDate).Column("endDate");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
        }
    }
}
