﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class AutoClubStatusSociosMap : ClassMap<AutoClubStatusSocios>
    {
        public AutoClubStatusSociosMap()
        {
            Table("AutoClubStatusSocios");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
            Map(x => x.Rut).Column("rut");
            Map(x => x.Activo).Column("activo");
            Map(x => x.DebeCuotas).Column("debeCuotas");
        }
    }
}
