﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class ContactFormMap : ClassMap<ContactForm>
    {
        public ContactFormMap()
        {
            Table("[ContactForm]");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Description).Column("[description]");
            // linea de test para volver a subir
        }
    }
}
