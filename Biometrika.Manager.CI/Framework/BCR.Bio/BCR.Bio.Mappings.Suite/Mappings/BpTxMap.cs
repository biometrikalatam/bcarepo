﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class BpTxMap : ClassMap<BpTx>
    {
        public BpTxMap()
        {
            Table("bp_tx");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Trackid).Column("trackid");
            Map(x => x.Operationcode).Column("operationcode");
            Map(x => x.Typeid).Column("typeid");
            Map(x => x.Valueid).Column("valueid");
            Map(x => x.Result).Column("result");
            Map(x => x.Score).Column("score");
            Map(x => x.Threshold).Column("threshold");
            Map(x => x.Timestampstart).Column("timestampstart");
            Map(x => x.Timestampend).Column("timestampend");
            Map(x => x.Authenticationfactor).Column("authenticationfactor");
            Map(x => x.Minutiaetype).Column("minutiaetype");
            Map(x => x.Bodypart).Column("bodypart");
            Map(x => x.Actiontype).Column("actiontype");
           
            Map(x => x.Timestampclient).Column("timestampclient");
            Map(x => x.Clientid).Column("clientid");
            Map(x => x.Ipenduser).Column("ipenduser");

            Map(x => x.Enduser).Column("enduser");
            Map(x => x.Dynamicdata).Column("dynamicdata");
            Map(x => x.Companyidtx).Column("companyidtx");
            Map(x => x.Useridtx).Column("useridtx");
            Map(x => x.Operationsource).Column("operationsource");
            Map(x => x.Abs).Column("abs");

            References(x => x.BpOrigin).Column("originid").Class(typeof(BCR.Bio.Domain.BpOrigin)); 

            HasMany(x => x.BpTxConx);
            HasMany(x => x.BpVerified);
        }
    }
}