﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class PointToPointDependencyMap : ClassMap<PointToPointDependency>
    {
        public PointToPointDependencyMap()
        {
            Table("PointToPointDependency");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");

            Map(x => x.Latency).Column("latency");

            References(x => x.PointMaster).Column("pointMaster").Class(typeof(Point)).Not.LazyLoad();
            References(x => x.PointSlave).Column("pointSlave").Class(typeof(Point)).Not.LazyLoad();
        }
    }
}