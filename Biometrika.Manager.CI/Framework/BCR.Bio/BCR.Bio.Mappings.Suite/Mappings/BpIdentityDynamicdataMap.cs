﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class BpIdentityDynamicdataMap : ClassMap<BpIdentityDynamicdata>
    {
        public BpIdentityDynamicdataMap()
        {
            Table("bp_identity_dynamicdata");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.DDKey).Column("ddkey");
            Map(x => x.DDValue).Column("ddvalue");
            Map(x => x.Creation).Column("creation");
            Map(x => x.LastUpdate).Column("lastupdate");

            References(x => x.BpIdentity, "identid").Unique();
        }
    }
}