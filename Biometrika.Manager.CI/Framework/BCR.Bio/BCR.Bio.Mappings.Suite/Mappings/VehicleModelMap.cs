﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class VehicleModelMap : ClassMap<VehicleModel>
    {
        public VehicleModelMap()
        {
            Table("VehicleModel");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");
        }
    }
}