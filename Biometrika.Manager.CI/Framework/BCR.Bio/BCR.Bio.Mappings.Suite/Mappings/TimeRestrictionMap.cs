﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class TimeRestrictionMap : ClassMap<TimeRestriction>
    {
        public TimeRestrictionMap()
        {
            Table("TimeRestriction");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Name).Column("name");

            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
            Map(x => x.EndDate).Column("endDate");

            Map(x => x.StartTime).Column("startTime");
            Map(x => x.EndTime).Column("endTime");

            Map(x => x.DayStartTime).Column("dayStartTime").CustomType("TimeAsTimeSpan");
            Map(x => x.DayEndTime).Column("dayEndTime").CustomType("TimeAsTimeSpan");

            Map(x => x.Active).Column("active");
            Map(x => x.Block).Column("block");

            Map(x => x.Sunday).Column("sunday");
            Map(x => x.Monday).Column("monday");
            Map(x => x.Tuesday).Column("tuesday");
            Map(x => x.Wednesday).Column("wednesday");
            Map(x => x.Thursday).Column("thursday");
            Map(x => x.Friday).Column("friday");
            Map(x => x.Saturday).Column("saturday");
        }
    }
}