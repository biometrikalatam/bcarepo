﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class EventClassMap : ClassMap<EventClass>
    {
        public EventClassMap()
        {
            Table("EventClass");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");
            References(x => x.Company, "company").Unique().Class(typeof(Company)).Fetch.Join();
        }
    }
}