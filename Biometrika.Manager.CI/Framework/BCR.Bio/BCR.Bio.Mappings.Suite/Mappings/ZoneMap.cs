﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
//using NHibernate.Mapping.ByCode;
//using NHibernate.Mapping.ByCode.Conformist;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class ZoneMap : ClassMap<Zone>
    {
        public ZoneMap()
        {
            Table("Zone");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");

            References(x => x.HQ).Column("hq").Class(typeof(HQ)).Not.LazyLoad();

            HasMany(x => x.Buildings)
                .Cascade
                .None()
                .Table("Building")
                .KeyColumn("zone").Not.LazyLoad();

            HasMany(x => x.Unitys)
                .Cascade
                .None()
                .Table("Unity")
                .KeyColumn("zone").Not.LazyLoad();
        }
    }
    //public class ZoneMap : ClassMapping<Zone>
    //{
    //    public ZoneMap()
    //    {
    //        Id(x => x.Id, m => m.Generator(Generators.Identity));

    //        Property(x => x.Description, m => m.Column("description"));

    //        ManyToOne(x => x.HQ, m => m.Column("hq"));
    //    }
    //}
}