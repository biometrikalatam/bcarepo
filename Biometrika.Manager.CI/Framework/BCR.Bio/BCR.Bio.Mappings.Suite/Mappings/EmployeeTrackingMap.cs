﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class EmployeeTrackingMap : ClassMap<EmployeeTracking>
    {
        public EmployeeTrackingMap()
        {
            Table("[EmployeeTracking]");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Company).Column("[company]").Class<Company>();
            Map(x => x.EventTime).Column("[eventTime]");
            References(x => x.Point).Column("[point]").Class<Point>();
            References(x => x.Employee).Column("[employee]").Class<Employee>();
            References(x => x.Vehicle).Column("[vehicle]").Class<Vehicle>();
            References(x => x.TrackingAction).Column("[action]").Class<TrackingAction>();
            References(x => x.TrackingResult).Column("[result]").Class<TrackingResult>();
            Map(x => x.Comment).Column("[comment]");
            Map(x => x.CreateDate).Column("[createDate]");
            Map(x => x.UpdateDate).Column("[updateDate]");
            Map(x => x.TrackingGuid).Column("[trackingGuid]");
            References(x => x.Sector).Column("[sector]").Class<Sector>();
            References(x => x.HQ).Column("[hq]").Class<HQ>();
            References(x => x.Provider).Column("[provider]").Class<Provider>();
            References(x => x.User).Column("[user]").Class<User>();
            Map(x => x.Signature).Column("[signature]").CustomType("StringClob").CustomSqlType("VARCHAR(MAX)");
        }
    }
}
