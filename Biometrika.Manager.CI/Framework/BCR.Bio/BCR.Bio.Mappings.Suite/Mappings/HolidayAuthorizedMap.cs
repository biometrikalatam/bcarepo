﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;


namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class HolidayAuthorizedMap : ClassMap<HolidayAuthorized>
    {
        public HolidayAuthorizedMap()
        {
            Table("HolidayAuthorized");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.StartDate).Column("startDate").Not.Nullable();
            Map(x => x.EndDate).Column("endDate").Not.Nullable();
            Map(x => x.StartTime).Column("startTime").Not.Nullable();
            Map(x => x.EndTime).Column("endTime").Not.Nullable();
            Map(x => x.CreateDate).Column("createDate").Not.Nullable();
            Map(x => x.UpdateDate).Column("updateDate");
            References(x => x.Identity).Column("[identity]").Class<BpIdentity>().Not.LazyLoad().Not.Nullable();
            References(x => x.Holiday).Column("[holiday]").Class<Holiday>().Not.LazyLoad().Not.Nullable();
        }
    }
}
