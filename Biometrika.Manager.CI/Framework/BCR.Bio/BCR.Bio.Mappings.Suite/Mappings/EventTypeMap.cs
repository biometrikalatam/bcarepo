﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class EventTypeMap : ClassMap<EventType>
    {
        public EventTypeMap()
        {
            Table("EventType");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");
        }
    }
}