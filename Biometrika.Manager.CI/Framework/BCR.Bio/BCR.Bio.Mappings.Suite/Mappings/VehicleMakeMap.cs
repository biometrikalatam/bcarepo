﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class VehicleMakeMap : ClassMap<VehicleMake>
    {
        public VehicleMakeMap()
        {
            Table("VehicleMake");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");
        }
    }
}