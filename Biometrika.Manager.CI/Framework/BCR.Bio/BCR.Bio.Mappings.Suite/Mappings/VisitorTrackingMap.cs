﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class VisitorTrackingMap : ClassMap<VisitorTracking>
    {
        public VisitorTrackingMap()
        {
            Table("VisitorTracking");

            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.EventTime).Column("eventTime");

            References(x => x.Company).Column("company").Class(typeof(Company));
            References(x => x.Visitor).Column("visitor").Class(typeof(Visitor));
            References(x => x.Vehicle).Column("vehicle").Class(typeof(Vehicle));

            References(x => x.Point).Column("point").Class(typeof(Point));

            References(x => x.Resident).Column("resident").Class(typeof(Resident));

            References(x => x.Action).Column("action").Class(typeof(TrackingAction));

            References(x => x.Result).Column("result").Class(typeof(TrackingResult));

            References(x => x.User).Column("[user]").Class(typeof(User));

            References(x => x.EventType).Column("eventType").Class(typeof(EventType));

            References(x => x.Provider).Column("provider").Class(typeof(Provider));

            References(x => x.EventClass).Column("EventClass").Class(typeof(EventClass));

            References(x => x.Employee).Column("[employee]").Class<Employee>();


            References(x => x.HQ).Column("hq").Class<HQ>();

            References(x => x.Zone).Column("zone").Class(typeof(Zone));
            References(x => x.Building).Column("building").Class(typeof(Building));
            References(x => x.Unity).Column("unity").Class(typeof(Unity));

            References(x => x.Event).Column("event").Class(typeof(Event));

            //Map(x => x.EndDate).Column("endDate");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");

            Map(x => x.Comment).Column("comment");

            Map(x => x.TrackingGuid).Column("trackingGuid");

            //HasManyToMany(x => x.Vehicles)
            //.Cascade
            //.All()
            //.Table("VisitorVehicle")
            //.ParentKeyColumn("visitor")
            //.ChildKeyColumn("vehicle");
        }
    }
}