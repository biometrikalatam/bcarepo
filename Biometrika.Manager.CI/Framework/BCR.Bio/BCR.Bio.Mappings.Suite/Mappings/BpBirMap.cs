﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class BpBirMap : ClassMap<BpBir>
    {
        public BpBirMap()
        {
            Table("bp_bir");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Type).Column("type");
            Map(x => x.Authenticationfactor).Column("authenticationfactor");
            Map(x => x.Minutiaetype).Column("minutiaetype");
            Map(x => x.Bodypart).Column("bodypart");
            Map(x => x.Data).Column("data").CustomType("StringClob");
            Map(x => x.Additionaldata).Column("additionaldata");
            Map(x => x.Timestamp).Column("timestamp");
            //Map(x => x.CreateDate).Column("createDate");
            Map(x => x.Signature).Column("signature");
            Map(x => x.Companyidenroll).Column("companyidenroll");
            Map(x => x.Useridenroll).Column("useridenroll");
            //Map(x => x.UpdateDate).Column("updateDate");
            References(x => x.BpIdentity, "identid").Unique();
        }
    }
}