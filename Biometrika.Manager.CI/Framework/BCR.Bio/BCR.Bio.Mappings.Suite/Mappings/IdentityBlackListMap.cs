﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class IdentityBlackListMap : ClassMap<IdentityBlackList>
    {
        public IdentityBlackListMap()
        {
            Table("IdentityBlackList");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.TypeId).Column("typeId").Not.Nullable();
            Map(x => x.ValueId).Column("valueId").Not.Nullable();

            Map(x => x.RazonIn).Column("razonIn").Length(100000);
            Map(x => x.RazonOut).Column("razonOut").Length(100000);
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");

            Map(x => x.EndDate).Column("endDate");

            References(x => x.UserIn).Column("userIn").Class(typeof(User));
            References(x => x.UserOut).Column("userOut").Class(typeof(User));

            References(x => x.Company).Column("company").Class(typeof(Company));
            References(x => x.Identity).Column("[identity]").Class<BpIdentity>().Not.LazyLoad();
        }
    }
}