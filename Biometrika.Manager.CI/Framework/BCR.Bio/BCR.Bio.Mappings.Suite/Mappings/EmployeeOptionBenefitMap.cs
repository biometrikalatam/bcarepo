﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    class EmployeeOptionBenefitMap : ClassMap<EmployeeOptionBenefit>
    {

        public EmployeeOptionBenefitMap()
        {
            Table("EmployeeOptionBenefit");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.employee, "employee").Unique().Class(typeof(Employee));
            References(x => x.benefitoption, "benefitoption").Unique().Class(typeof(BenefitOption));
        }
    }
}
