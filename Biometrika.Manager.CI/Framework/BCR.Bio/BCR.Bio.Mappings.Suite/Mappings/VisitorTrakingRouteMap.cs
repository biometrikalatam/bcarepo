﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class VisitorTrakingRouteMap : ClassMap<VisitorTrakingRoute>
    {
        public VisitorTrakingRouteMap()
        {
            Table("[VisitorTrakingRoute]");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");
        }
    }
}