﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class VoucherMap : ClassMap<Voucher>
    {
        public VoucherMap()
        {
            Table("Voucher");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Identity).Column("[identity_id]").Class<BpIdentity>().Not.LazyLoad();
            References(x => x.CentroDeCostos).Column("[centrocosto]").Class<CentroDeCostos>().Not.LazyLoad().Not.Nullable();            //References(x => x.TrakingRoute).Column("[trackingRoute]").Class(typeof(VisitorTrakingRoute)).Not.LazyLoad();
            Map(x => x.Horas).Column("[horas]").Not.Nullable();
            References(x => x.Autorizador).Column("[autorizador_id]").Class<BpIdentity>().Not.LazyLoad().Not.Nullable();
            References(x => x.Employee).Column("[employee_id]").Class<Employee>().Not.LazyLoad();
            Map(x => x.CreateDate).Column("createDate").Not.Nullable();
            Map(x => x.UpdateDate).Column("updateDate");
            Map(x => x.Total).Column("total");
            References(x => x.Company).Column("[company]").Class<Company>().Not.LazyLoad().Not.Nullable();
        }
    }
}