﻿using System;
using FluentNHibernate.Mapping;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCR.Bio.Domain;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class BenefitMap: ClassMap<Benefit>
    {
        public BenefitMap()
        {
            Table("Benefit");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Description).Column("description");
            Map(x => x.Modulo).Column("modulo");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
            References(x => x.Company, "company").Unique().Class(typeof(Company));

            HasMany(x => x.BenefitOption)
                   .Cascade.All()
                   .KeyColumn("Benefit")
                   .Not.LazyLoad() ;
                              

        }
    }
}
