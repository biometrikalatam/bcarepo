﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class BCR_AppsMap : ClassMap<BCR_Apps>
    {
        public BCR_AppsMap()
        {
            Table("BCR_Apps");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Key).Column("[key]");
            Map(x => x.Application).Column("application");

            Map(x => x.Menu).Column("menu");

            HasManyToMany(x => x.Companies)
              .Cascade
              .All()
              .Table("BCR_AppCompany")
              .ParentKeyColumn("application")
              .ChildKeyColumn("company");
        }
    }
}