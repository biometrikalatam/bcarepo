﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class MarkHelperMap : ClassMap<MarkHelper>
    {
        public MarkHelperMap()
        {
            Table("MarkHelper");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.TimeStamp).Column("[timestamp]");
            Map(x => x.PersonId).Column("personId");
            Map(x => x.Processed).Column("processed");
            Map(x => x.HasEmployeeTracking).Column("hasEmployeeTracking");
            Map(x => x.Direction).Column("direction");

            References(x => x.Point).Column("[point]").Class<Point>().Not.LazyLoad();
            References(x => x.Company).Column("[company]").Class<Company>().Not.LazyLoad();

            Map(x => x.DateTimeStamp).Column("dateTimeStamp");
            Map(x => x.Temperature).Column("temperature");
            Map(x => x.PhotoMarkBase64).Column("photoMarkBase64").CustomType("StringClob"); ;
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
        }

    }
}
