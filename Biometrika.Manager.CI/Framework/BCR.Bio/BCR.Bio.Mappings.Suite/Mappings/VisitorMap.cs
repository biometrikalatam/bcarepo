﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class VisitorMap : ClassMap<Visitor>
    {
        public VisitorMap()
        {
            Table("Visitor");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Identity).Column("[identity]").Class<BpIdentity>().Not.LazyLoad();
            References(x => x.Provider).Column("[provider]").Class<Provider>().Not.LazyLoad();

            //References(x => x.TrakingRoute).Column("[trackingRoute]").Class(typeof(VisitorTrakingRoute)).Not.LazyLoad();


            Map(x => x.EndDate).Column("endDate");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
            Map(x => x.VisitCounter).Column("visitCounter");

            HasManyToMany(x => x.Vehicles)
            .Cascade
            .SaveUpdate()
            .Table("VisitorVehicle")
            .ParentKeyColumn("visitor")
            .ChildKeyColumn("vehicle")
            .Not.LazyLoad();

            HasManyToMany(x => x.Unitys)
            .Cascade
            .SaveUpdate()
            .Table("VisitorUnity")
            .ParentKeyColumn("visitor")
            .ChildKeyColumn("unity")
            .Not.LazyLoad();
        }
    }
}