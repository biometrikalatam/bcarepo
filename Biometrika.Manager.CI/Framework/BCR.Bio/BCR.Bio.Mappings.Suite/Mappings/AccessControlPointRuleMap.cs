﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class AccessControlPointRuleMap : ClassMap<AccessControlPointRule>
    {
        public AccessControlPointRuleMap()
        {
            Table("AccessControlPointRule");
            Id(x => x.Id).GeneratedBy.Identity();

            References(x => x.AccessControl).Column("accessControl").Class(typeof(AccessControlPointRule));

            References(x => x.PointRule).Column("pointRule").Class(typeof(PointRule));

            References(x => x.RuleScope).Column("ruleScope").Class(typeof(RuleScope));

            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
        }
    }
}