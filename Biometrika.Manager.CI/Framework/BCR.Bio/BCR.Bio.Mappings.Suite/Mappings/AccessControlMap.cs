﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class AccessControlMap : ClassMap<AccessControl>
    {
        public AccessControlMap()
        {
            Table("AccessControl");
            Id(x => x.Id).GeneratedBy.Identity();

            References(x => x.Company).Column("company").Class(typeof(Company));

            Map(x => x.Description).Column("description");
            Map(x => x.EndDate).Column("endDate");
            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");

            References(x => x.PointRule).Column("pointRule").Class(typeof(PointRule));

            References(x => x.RuleScope).Column("ruleScope").Class(typeof(RuleScope));

            Map(x => x.MinLimit).Column("[minLimit]");
            Map(x => x.MaxLimit).Column("[maxLimit]");
            Map(x => x.ControlValue).Column("[controlValue]");

            HasManyToMany(x => x.Points)
            .Cascade
            .None()
            .Table("AccessControlPoint")
            .ParentKeyColumn("accessControl")
            .ChildKeyColumn("point")
            .Not.LazyLoad();
        }
    }
}