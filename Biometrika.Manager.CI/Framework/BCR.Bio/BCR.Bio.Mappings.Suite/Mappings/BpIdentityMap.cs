﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings
{
    public class BpIdentityMap : ClassMap<BpIdentity>
    {
        public BpIdentityMap()
        {
            Table("bp_identity");            
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.TypeId).Column("typeid");
            Map(x => x.ValueId).Column("valueid");
            Map(x => x.Nick).Column("nick");
            Map(x => x.Name).Column("name");
            Map(x => x.PatherLastname).Column("patherlastname");
            Map(x => x.MotherLastname).Column("motherlastname");
            Map(x => x.Sex).Column("sex");
            Map(x => x.DocumentSeriesNumber).Column("documentseriesnumber");
            Map(x => x.DocumentExpirationDate).Column("documentexpirationdate");
            Map(x => x.VisaType).Column("visatype");
            Map(x => x.BirthDate).Column("birthdate");

            Map(x => x.BirthPlace).Column("birthplace");
            Map(x => x.Nationality).Column("nationality");
            Map(x => x.Photography).Column("photography").CustomType("StringClob");
            Map(x => x.Signatureimage).Column("signatureimage").CustomType("StringClob");
            Map(x => x.Profession).Column("profession");
            Map(x => x.Dynamicdata).Column("dynamicdata");
            Map(x => x.Enrollinfo).Column("enrollinfo");
            Map(x => x.Creation).Column("creation");
            Map(x => x.VerificationSource).Column("verificationsource");
            Map(x => x.UserIdEnroll).Column("useridenroll");
            Map(x => x.FullName).Column("full_name").Generated.Always();
            Map(x => x.FullApellidos).Column("[full_apellidos]").Generated.Always();
            //Map(x => x.Address).Column("address");

            //Map(x => x.Number).Column("number");
            //Map(x => x.Address2).Column("address2");
            Map(x => x.Phone).Column("phone");

            Map(x => x.Cellphone).Column("cellphone");
            Map(x => x.Phone2).Column("phone2");
            //Map(x => x.PostalCode).Column("postalCode");

            References(x => x.CompanyIdEnroll).Column("companyidenroll").Class<Company>();

            HasMany(x => x.BpBir).Cascade.All();
            HasMany(x => x.BpIdentityDynamicdata).Cascade.All();

            // TODO: ver bien esto, no esta funcionando como yo quiero pero debe ser porque lo hago mal o me falta algo
            //HasOne(x => x.Resident).PropertyRef("Identity");
            //HasOne(x => x.Visitor).PropertyRef("Identity");
            //HasOne(x => x.Employee).PropertyRef("Identity");
            HasMany(x => x.IdentityBlackList)
                .Cascade
                .None()
                .Table("[IdentityBlackList]")
                .KeyColumn("[identity]").Not.LazyLoad();

            HasManyToMany(x => x.IdentityTypeList)
                .Cascade
                .SaveUpdate()
                .Table("[IdentityIdentityType]")
                .ParentKeyColumn("[identity]")
                .ChildKeyColumn("[identityType]")
                .Not.LazyLoad();

            HasManyToMany(x => x.CentrosDeCostoList)
                .Cascade
                .SaveUpdate()
                .Table("[IdentityCentroDeCostos]")
                .ParentKeyColumn("[identity]")
                .ChildKeyColumn("[centroDeCostos]")
                .Not.LazyLoad();

            HasManyToMany(x => x.IdentityStateList)
                .Cascade
                .SaveUpdate()
                .Table("[IdentityIdentityState]")
                .ParentKeyColumn("[identity]")
                .ChildKeyColumn("[identityState]")
                .Not.LazyLoad();

            HasManyToMany(x => x.ContactFormList)
                .Cascade
                .Delete()
                .Table("[IdentityContactForm]")
                .ParentKeyColumn("[identity]")
                .ChildKeyColumn("[contactForm]");
                
        }
    }
}