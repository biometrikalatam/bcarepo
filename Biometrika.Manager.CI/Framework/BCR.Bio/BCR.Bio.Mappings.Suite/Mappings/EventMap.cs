﻿using BCR.Bio.Domain;
using FluentNHibernate.Mapping;

namespace BCR.Bio.Infraestructura.Mappings.Suite.Mappings
{
    public class EventMap : ClassMap<Event>
    {
        public EventMap()
        {
            Table("Event");
            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Description).Column("description");

            Map(x => x.CreateDate).Column("createDate");
            Map(x => x.UpdateDate).Column("updateDate");
            Map(x => x.EndDate).Column("endDate");

            Map(x => x.Comment).Column("comment");

            References(x => x.Company).Column("company").Class(typeof(Company)).Not.LazyLoad();
            References(x => x.HQ).Column("HQ").Class(typeof(HQ)).Not.LazyLoad();

            References(x => x.GroupResident).Column("groupResident").Class(typeof(Group)).Not.LazyLoad();
            References(x => x.GroupVisitor).Column("groupVisitor").Class(typeof(Group)).Not.LazyLoad();

            References(x => x.EventClass).Column("eventClass").Class(typeof(EventClass)).Not.LazyLoad();
            References(x => x.EventType).Column("eventType").Class(typeof(EventType)).Not.LazyLoad();

            References(x => x.Provider).Column("provider").Class(typeof(Provider)).Not.LazyLoad();

            References(x => x.ResidentAuthorizator).Column("residentAuthorizator").Class(typeof(Resident)).Not.LazyLoad();
            References(x => x.Unity).Column("unity").Class(typeof(Unity)).Not.LazyLoad();

            HasManyToMany(x => x.TimeRestrictions)
              .Cascade
              .All()
              .Table("EventTimeRestriction")
              .ParentKeyColumn("event")
              .ChildKeyColumn("timeRestriction").Not.LazyLoad();

            HasMany(x => x.TrackingList)
                .Cascade
                .None()
                .Table("VisitorTracking")
                .KeyColumn("event");
        }
    }
}