﻿namespace BCR.Bio.Domain
{
    public class BpConfig
    {



        /// <summary>
        /// Determina si se hace enroll ante un matching positivo de una fuente externa
        /// </summary>
        private int _insertOption = 0;

        /// <summary>
        /// Determina si se graba en BD una copia de cada muestra verificada
        ///     0 - Default
        ///     1 - Se graba
        ///     2 - No se graba
        /// </summary>
        private int _saveVerified = 0;

        /// <summary>
        /// Minucia utilizada por default en matching
        /// </summary>
        private int _minutiaetypeDefault = 2; //Digital Persona

        /// <summary>
        /// Timeout default a cualqueir WS que se conecte
        /// </summary>
        private int _timeoutWSdefault = 30000;

        /// <summary>
        ///Arreglo con las minucias que se deban generar en un enroll a aprtir de un WSQ o RAW
        /// </summary>
        private int[] _minutiaetypeToEnroll;

        /// <summary>
        /// Determina pordefault que tipod e matching se hace.
        /// Posibles valores:
        ///     1-First     => Primer matching positivo
        ///     2-Best      => Mejor matching
        /// </summary>
        private int _matchingtype;

        /// <summary>
        /// Determina en que orden se hacen las operaciones:
        ///     1 - Solo Local
        ///     2 - Primero Local
        ///     3 - Solo Remoto/s
        /// </summary>
        private int _operationOrder = 0;

        /// <summary>
        /// Indica si se debe chequear la compañia, o la compañia y el user en cada transaccion
        /// en el web services. Para deshabilitar en caso que una compañía compre el producto
        /// y queira hacerlo mas eficiente.
        /// Valores Posibles:
        ///             0 - No Chequea
        ///             1 - Chequea Compañia
        ///             2 - Chequea Usuario
        ///             3 - Chequea Compañia/Usuario
        /// </summary>
        private int _checkAccess = 0;

        /// <summary>
        /// ConnectorID default para accesos remotos
        /// </summary>
        private string _connectorDefault = null;

        /// <summary>
        /// Existen 3 tipos:
        ///     0 - Sin Firma
        ///     1 - Simple
        ///     2 - Advanced
        /// </summary>
        private int _bioSignatureType = 0;



        /// <summary>
        /// Determina si se hace enroll ante un matching positivo de una fuente externa
        /// </summary>
        public int InsertOption
        {
            get { return _insertOption; }
            set { _insertOption = value; }
        }

        /// <summary>
        /// Determina si se graba en BD una copia de cada muestra verificada
        /// </summary>
        public int SaveVerified
        {
            get { return _saveVerified; }
            set { _saveVerified = value; }
        }

        /// <summary>
        /// Minucia utilizada por default en matching
        /// </summary>
        public int MinutiaetypeDefault
        {
            get { return _minutiaetypeDefault; }
            set { _minutiaetypeDefault = value; }
        }

        /// <summary>
        /// Timeout default a cualqueir WS que se conecte
        /// </summary>
        public int TimeoutWSdefault
        {
            get { return _timeoutWSdefault; }
            set { _timeoutWSdefault = value; }
        }

        /// <summary>
        ///Arreglo con las minucias que se deban generar en un enroll a aprtir de un WSQ o RAW
        /// </summary>
        public int[] MinutiaetypeToEnroll
        {
            get { return _minutiaetypeToEnroll; }
            set { _minutiaetypeToEnroll = value; }
        }

        /// <summary>
        /// Determina pordefault que tipod e matching se hace.
        /// Posibles valores:
        ///     1-First     => Primer matching positivo
        ///     2-Best      => Mejor matching
        /// </summary>
        public int Matchingtype
        {
            get { return _matchingtype; }
            set { _matchingtype = value; }
        }

        /// <summary>
        /// Determina en que orden se hacen las operaciones:
        ///     static public int OPERATIONORDER_LOCALONLY = 1;
        ///     static public int OPERATIONORDER_LOCALFIRST = 2;
        ///     static public int OPERATIONORDER_REMOTEONLY = 3;
        /// </summary>
        public int OperationOrder
        {
            get { return _operationOrder; }
            set { _operationOrder = value; }
        }

        /// <summary>
        /// Indica si se debe chequear la compañia, o la compañia y el user en cada transaccion
        /// en el web services. Para deshabilitar en caso que una compañía compre el producto
        /// y queira hacerlo mas eficiente.
        /// Valores Posibles:
        ///             0 - No Chequea
        ///             1 - Chequea Compañia
        ///             2 - Chequea Usuario
        ///             3 - Chequea Compañia/Usuario
        /// </summary>
        public int CheckAccess
        {
            get { return _checkAccess; }
            set { _checkAccess = value; }
        }

        /// <summary>
        /// ConnectorID default para accesos remotos
        /// </summary>
        public string ConnectorDefault
        {
            get { return _connectorDefault; }
            set { _connectorDefault = value; }
        }

        /// <summary>
        /// Existen 3 tipos:
        ///     0 - Sin Firma
        ///     1 - Simple
        ///     2 - Advanced
        /// </summary>
        public int BioSignatureType
        {
            get { return _bioSignatureType; }
            set { _bioSignatureType = value; }
        } 




    }
}


