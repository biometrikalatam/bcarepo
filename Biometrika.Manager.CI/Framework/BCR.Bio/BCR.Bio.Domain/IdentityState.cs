﻿using BCR.Bio.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class IdentityState : AbstractEntityItem<int>
    {
        public virtual string Class { get; set; }
        public virtual string Tag { get; set; }
        public virtual Company Company { get; set; }
    }
}
