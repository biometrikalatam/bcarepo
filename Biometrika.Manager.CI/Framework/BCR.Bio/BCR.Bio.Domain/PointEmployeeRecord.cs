﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;

namespace BCR.Bio.Domain
{
    public class PointEmployeeRecord : AbstractEntity, IPointEmployeeRecord
    {
        public virtual IUser User { get; set; }

        public virtual DateTime? EndActivity { get; set; }

        public virtual IPoint Point { get; set; }

        public virtual DateTime StartActivity { get; set; }

        public virtual ICompany Company { get; set; }
    }
}