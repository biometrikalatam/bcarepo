﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    // TODO: Solo para etapa 1 Lomas la Dehesa, quitar al pasar a la Etapa 2
    public class UserExtended
    {
        public virtual int Id { get; set; }
        public virtual User User { get; set; }
        public virtual string DecryptedPassword { get; set; }
    }
}
