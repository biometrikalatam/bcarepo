﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class EmployeeTracking
    {
        public virtual int Id { get; set; }
        public virtual Company Company { get; set; }
        public virtual DateTime EventTime { get; set; }
        public virtual Point Point { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual Vehicle Vehicle { get; set; }
        public virtual TrackingAction TrackingAction { get; set; }
        public virtual TrackingResult TrackingResult { get; set; }
        public virtual string Comment { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string TrackingGuid { get; set; }
        public virtual Sector Sector { get; set; }
        public virtual HQ HQ { get; set; }
        public virtual Provider Provider { get; set; }
        public virtual User User { get; set; }
        public virtual string Signature { get; set; }
        public virtual string HashMark { get; set; }
        public virtual decimal Temperature { get; set; }
        public virtual bool HasPhoto { get; set; }
    }
}
