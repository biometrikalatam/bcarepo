﻿using BCR.Bio.Domain.Abstract;

namespace BCR.Bio.Domain
{
    public class PointToPointDependency : AbstractEntityItem<int>
    {
        public virtual Point PointMaster { get; set; }
        public virtual Point PointSlave { get; set; }

        public virtual int Latency { get; set; }
    }
}