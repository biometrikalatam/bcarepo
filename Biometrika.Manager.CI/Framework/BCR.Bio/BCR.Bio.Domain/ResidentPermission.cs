﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using NHibernate;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class ResidentPermission : AbstractEntity, IResidentPermission
    {
        public virtual bool CanInvite { get; set; }
        public virtual bool CanEditPermissions { get; set; }
        public virtual bool CanAddFamilyMembers { get; set; }
        public virtual bool IsLeader { get; set; }
        public virtual Resident Resident { get; set; }
        public virtual Unity Unity { get; set; }
    }
}
