using BCR.Bio.Domain.Abstract;

/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/

using System;
using System.ComponentModel;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// BpTxConx object for NHibernate mapped table 'bp_tx_conx'.
    /// </summary>
    [Serializable]
    public class BpTxConx : AbstractEntity
    {
        #region Member Variables

        //protected int _id;
        protected BpTx _bptx;

        protected string _consultationtype;
        protected string _connectorid;
        protected string _trackid;
        protected int? _status;
        protected int? _result;
        protected double? _score;
        protected double? _threshold;
        protected string _timestamp;


        #endregion Member Variables

        #region Constructors

        public BpTxConx()
        {
        }

        public BpTxConx(BpTx bptx, string consultationtype, string connectorid, string trackid, int? status, int? result, double? score, double? threshold, string timestamp)
        {
            this._bptx = bptx;
            this._consultationtype = consultationtype;
            this._connectorid = connectorid;
            this._trackid = trackid;
            this._status = status;
            this._result = result;
            this._score = score;
            this._threshold = threshold;
            this._timestamp = timestamp;
        }

        public BpTxConx(BpTx bptx, string consultationtype)
        {
            this._bptx = bptx;
            this._consultationtype = consultationtype;
        }

        public BpTxConx(BpTx bptx, string consultationtype, string connectorid)
        {
            this._bptx = bptx;
            this._consultationtype = consultationtype;
            this._connectorid = connectorid;
        }

        #endregion Constructors

        #region Public Properties

        //public  virtual int Id
        //{
        //    get { return _id; }
        //    set {if (value != this._id){_id= value;}}
        //}
        public virtual BpTx BpTx
        {
            get { return _bptx; }
            set { _bptx = value; }
        }

        public virtual string Consultationtype
        {
            get { return _consultationtype; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Consultationtype cannot contain more than 5 characters");
                if (value != this._consultationtype) { _consultationtype = value; }
            }
        }

        public virtual string Connectorid
        {
            get { return _connectorid; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Connectorid cannot contain more than 5 characters");
                if (value != this._connectorid) { _connectorid = value; }
            }
        }

        public virtual string Trackid
        {
            get { return _trackid; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Trackid cannot contain more than 100 characters");
                if (value != this._trackid) { _trackid = value; }
            }
        }

        public virtual int? Status
        {
            get { return _status; }
            set { if (value != this._status) { _status = value; } }
        }

        public virtual int? Result
        {
            get { return _result; }
            set { if (value != this._result) { _result = value; } }
        }

        public virtual double? Score
        {
            get { return _score; }
            set { if (value != this._score) { _score = value; } }
        }

        public virtual double? Threshold
        {
            get { return _threshold; }
            set { if (value != this._threshold) { _threshold = value; } }
        }

        public virtual string Timestamp
        {
            get { return _timestamp; }
            set
            {
                if (value != null && value.Length > 25)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Timestamp cannot contain more than 25 characters");
                if (value != this._timestamp) { _timestamp = value; }
            }
        }

        #endregion Public Properties
    }
}