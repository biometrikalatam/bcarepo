/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/

using BCR.Bio.Domain.Abstract;
using System;
using System.ComponentModel;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// BpVerified object for NHibernate mapped table 'bp_verified'.
    /// </summary>
    [Serializable]
    public class BpVerified : AbstractEntity
    {
        #region Member Variables

        //protected int _id;
        protected int _datatype;

        protected string _data;
        protected string _additionaldata;
        protected DateTime _timestamp;
        protected BpTx _bptx;
        protected int? _insertoption;
        protected int? _matchingtype;


        #endregion Member Variables

        #region Constructors

        public BpVerified()
        {
        }

        public BpVerified(int datatype, string data, string additionaldata, DateTime timestamp, BpTx bptx,
                            int? insertoption, int? matchingtype)
        {
            this._datatype = datatype;
            this._data = data;
            this._additionaldata = additionaldata;
            this._timestamp = timestamp;
            this._bptx = bptx;
            this._insertoption = insertoption;
            this._matchingtype = matchingtype;
        }

        public BpVerified(int datatype, string data, DateTime timestamp, BpTx bptx)
        {
            this._datatype = datatype;
            this._data = data;
            this._timestamp = timestamp;
            this._bptx = bptx;
        }

        #endregion Constructors

        #region Public Properties

        public virtual int Datatype
        {
            get { return _datatype; }
            set { if (value != this._datatype) { _datatype = value; } }
        }

        public virtual string Data
        {
            get { return _data; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 2147483647 characters");
                if (value != this._data) { _data = value; }
            }
        }

        public virtual string Additionaldata
        {
            get { return _additionaldata; }
            set
            {
                if (value != null && value.Length > 2048)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 2048 characters");
                if (value != this._additionaldata) { _data = value; }
            }
        }

        public virtual DateTime Timestamp
        {
            get { return _timestamp; }
            set { if (value != this._timestamp) { _timestamp = value; } }
        }

        public virtual BpTx BpTx
        {
            get { return _bptx; }
            set { _bptx = value; }
        }

        public virtual int? Insertoption
        {
            get { return _insertoption; }
            set { if (value != this._insertoption) { _insertoption = value; } }
        }

        public virtual int? Matchingtype
        {
            get { return _matchingtype; }
            set { if (value != this._matchingtype) { _matchingtype = value; } }
        }

        #endregion Public Properties
    }
}