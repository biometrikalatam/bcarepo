﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Clase de dominio que apunta a la tabla User
    /// </summary>
    public class User : AbstractEntity, IUser
    {
        public User()
        {
            Companies = new List<Company>();
            Roles = new List<Rol>();
        }

        /// <summary>
        /// Nombre de usuario, automaticamente al crear un nuevo usuario se le suma @DomainName
        /// </summary>
        public virtual string Username { get; set; }
        /// <summary>
        /// Correo de la cuenta de usuario
        /// </summary>
        public virtual string Email { get; set; }
        /// <summary>
        /// Clave, se debe encriptar antes de enviar guardar
        /// </summary>
        public virtual string Password { get; set; }

        public virtual bool IsApproved { get; set; }

        public virtual bool IsLockedOut { get; set; }

        public virtual string PasswordSalt { get; set; }

        public virtual int PasswordFormat { get; set; }

        public virtual string PasswordQuestion { get; set; }

        public virtual string PasswordAnswer { get; set; }

        public virtual int FailedPasswordAttemptCount { get; set; }

        public virtual DateTime? FailedPasswordAttemptWindowStart { get; set; }

        public virtual int FailedPasswordAnswerAttemptCount { get; set; }

        public virtual DateTime? FailedPasswordAnswerAttemptWindowStart { get; set; }

        public virtual DateTime? LastPasswordChangedDate { get; set; }

        public virtual DateTime? LastActivityDate { get; set; }

        public virtual DateTime? LastLockOutDate { get; set; }

        public virtual DateTime? LastLoginDate { get; set; }

        public virtual string Comments { get; set; }

        public virtual string Name { get; set; }

        public virtual string Surname { get; set; }

        public virtual string Phone { get; set; }

        public virtual string PostalCode { get; set; }
        /// <summary>
        /// Compañias a las que pertenece el usuario, hace referencia a la tabla CompanyUser en base al UserId y desde acá se 
        /// relaciona con la compañia por medio del campo CompanyUser.company
        /// </summary>
        public virtual IList<Company> Companies { get; set; }
        /// <summary>
        /// Listado de roles de usuario, generalmente se utilia solo uno, el primero
        /// </summary>
        public virtual IList<Rol> Roles { get; set; }

        public virtual Employee Employee { get; set; }

        //public virtual bool IsEmployee
        //{
        //    get
        //    {
        //        return Employee != null;
        //    }
        //}

        public virtual ICompany Company
        {
            get
            {
                if (Companies.Count >= 1)
                    return (ICompany)Companies[0];
                else
                    return null;
            }
        }
    }
}