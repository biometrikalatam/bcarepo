﻿using BCR.Bio.Domain.Abstract;

namespace BCR.Bio.Domain
{
    public class AccessControlPointRule : AbstractEntity
    {
        public virtual AccessControl AccessControl { get; set; }

        public virtual PointRule PointRule { get; set; }

        public virtual RuleScope RuleScope { get; set; }
    }
}