﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class Rol : AbstractEntity, IRol
    {
        public virtual string Name { get; set; }
        public virtual int Lvl { get; set; }

        //public virtual List<User> UsersInRole { get; set; }

        //public Rol()
        //{
        //    UsersInRole = new List<User>();
        //}
    }
}