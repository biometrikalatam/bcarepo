﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class City : AbstractEntityItem<int>, ICity
    {
        public virtual Dept Dept { get; set; }

        public virtual IList<Town> Towns { get; set; }

        
    }
}