﻿using BCR.Bio.Domain.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class TimeRestriction : AbstractEntity
    {
        public virtual string Name { get; set; }

        public virtual DateTime StartTime { get; set; }

        public virtual DateTime EndTime { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual TimeSpan DayStartTime { get; set; }

        public virtual TimeSpan DayEndTime { get; set; }

        public virtual bool Active { get; set; }

        public virtual bool Block { get; set; }

        public virtual bool Sunday { get; set; }

        public virtual bool Monday { get; set; }

        public virtual bool Tuesday { get; set; }

        public virtual bool Wednesday { get; set; }

        public virtual bool Thursday { get; set; }

        public virtual bool Friday { get; set; }

        public virtual bool Saturday { get; set; }

        public virtual List<int> DayNumbers
        {
            get
            {
                List<int> days = new List<int>();
                if (Sunday)
                    days.Add(0);
                if (Monday)
                    days.Add(1);
                if (Tuesday)
                    days.Add(2);
                if (Wednesday)
                    days.Add(3);
                if (Thursday)
                    days.Add(4);
                if (Friday)
                    days.Add(5);
                if (Saturday)
                    days.Add(6);

                return days;
            }
        }

        public virtual List<DayOfWeek> SelectedDays
        {
            get
            {
                List<DayOfWeek> list = new List<DayOfWeek>();

                if (Sunday)
                    list.Add(DayOfWeek.Sunday);
                if (Monday)
                    list.Add(DayOfWeek.Monday);
                if (Tuesday)
                    list.Add(DayOfWeek.Tuesday);
                if (Wednesday)
                    list.Add(DayOfWeek.Wednesday);
                if (Thursday)
                    list.Add(DayOfWeek.Thursday);
                if (Friday)
                    list.Add(DayOfWeek.Friday);
                if (Saturday)
                    list.Add(DayOfWeek.Saturday);

                return list;
            }            
        }
    }
}