﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.HFSecury.Helper;
using System;

namespace BCR.Bio.Domain
{
    public class MarkHelper : AbstractEntity
    {
        private long _timeStamp;
        public virtual long TimeStamp
        {
            get
            {
                return _timeStamp;
            }
            set
            {
                _timeStamp = value;
                this.DateTimeStamp = HelperTime.UnixTimeToDateTime(_timeStamp);
            }
        }
        //public virtual long TimeStamp { get; set; }
        public virtual string PersonId { get; set; }
        public virtual bool Processed { get; set; }
        public virtual bool HasEmployeeTracking { get; set; }
        public virtual Point Point { get; set; }
        public virtual Company Company { get; set; }

        public virtual DateTime DateTimeStamp { get; set; }

        public virtual string PhotoMarkBase64 { get; set; }

        public virtual decimal Temperature { get; set; }

        public virtual int Direction { get; set; }
    }
}
