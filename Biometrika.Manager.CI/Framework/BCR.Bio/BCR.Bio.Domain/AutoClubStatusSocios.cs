﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using NHibernate;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class AutoClubStatusSocios : AbstractEntity
    {
        public virtual string Rut { get; set; }
        public virtual bool Activo { get; set; }
        public virtual bool DebeCuotas { get; set; }
    }
}
