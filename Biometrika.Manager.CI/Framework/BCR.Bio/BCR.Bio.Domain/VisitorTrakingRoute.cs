﻿using BCR.Bio.Domain.Abstract;

namespace BCR.Bio.Domain
{
    public class VisitorTrakingRoute : AbstractEntityItem<int>
    {
        public VisitorTrakingRoute() { }

        public VisitorTrakingRoute(int idTemp)
        {
            Id = idTemp;
        }

    }
}