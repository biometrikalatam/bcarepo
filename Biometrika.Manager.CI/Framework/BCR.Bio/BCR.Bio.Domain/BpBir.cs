using BCR.Bio.Domain.Interface;
//using BCR.Bio.pki.mentalis;
using BCR.Bio.pki.symmetric;
using BCR.Bio.Domain.Abstract;
using System;



namespace BCR.Bio.Domain
{
    /// <summary>
    /// BpBir object for NHibernate mapped table 'bp_bir'.
    /// </summary>
    [Serializable]
    public class BpBir :   IBpBir
    {
        //private static readonly ILog LOG = LogManager.GetLogger(typeof(BpBir));

        #region Constructors

        public BpBir()
        {
        }

        public BpBir(int bpidentity, int authenticationfactor, int minutiaetype,
           int bodypart, string data, string additionaldata, DateTime timestamp,
           DateTime createDate, int companyidenroll)
        {
            BpIdentity.Id = bpidentity;
            Authenticationfactor = authenticationfactor;
            Minutiaetype = minutiaetype;
            Bodypart = bodypart;
            Data = data;
            Additionaldata = additionaldata;
            Timestamp = timestamp;
            //Creation = creation;
            Companyidenroll = companyidenroll;
            //CreateDate = createDate;
        }

        public BpBir(
            BpIdentity bpidentity,
            int type,
            int authenticationfactor,
            int minutiaetype,
            int bodypart,
            string data,
            string additionaldata,
            DateTime timestamp,
            DateTime createDate,
            string signature,
            int companyidenroll,
            int useridenroll)
        {
            BpIdentity = bpidentity;
            Type = type;
            Authenticationfactor = authenticationfactor;
            Minutiaetype = minutiaetype;
            Bodypart = bodypart;
            Data = data;
            Additionaldata = additionaldata;
            Timestamp = timestamp;
            //Creation = creation;
            Signature = signature;
            Companyidenroll = companyidenroll;
            Useridenroll = useridenroll;
            //CreateDate = createDate;
        }

        public BpBir(
            BpIdentity bpidentity,
            int type,
            int authenticationfactor,
            int minutiaetype,
            int bodypart,
            string data,
            string additionaldata)
        {
            BpIdentity = bpidentity;
            Type = type;
            Authenticationfactor = authenticationfactor;
            Minutiaetype = minutiaetype;
            Bodypart = bodypart;
            Data = data;
            Additionaldata = additionaldata;
        }

        /*public BpBir(int type, int authenticationfactor, int minutiaetype, int bodypart,
                    string data, string additionaldata, object bpidentity, DateTime timestamp, DateTime creation,
                    string signature, int companyidenroll, int useridenroll,
                    Signer signer, Encryptor encryptor)
        {
            //this.Id = id;

            this.Signer = signer;
            this.Encryptor = encryptor;

            this._type = type;
            this._authenticationfactor = authenticationfactor;
            this._bodypart = bodypart;
            this._minutiaetype = minutiaetype;

            //Added 10/2007
            AddSuFixForOracle(ref data);

            try
            {
                if (encryptor != null)
                {
                    object dataEncrypted = null;
                    if (encryptor.Encrypt(data, true, ref dataEncrypted, true))
                    {
                        this._data = (string)dataEncrypted;
                        this._type = type | Bio.Core.Matcher.Constant.BirType.ENCRYPTED;
                    }
                    else
                    {
                        this._data = data;
                    }
                }
                else
                {
                    this._data = data;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BpBir.BpBir - Encrypt", ex);
                this._data = data;
            }
            this._additionaldata = additionaldata;

            try
            {
                if (this._signature == null && this.Signer != null)
                {
                    object sSignature = null;
                    signer.GetSignature(ref sSignature, data, "SHA1", true);
                    this._signature = (string)sSignature;
                    this._type = type | Bio.Core.Matcher.Constant.BirType.SIGNED;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BpBir.BpBir - Signature", ex);
            }

            this._timestamp = timestamp;
            this._creation = creation;
            string msg = null;

            try
            {
                if (bpidentity.GetType().Equals(typeof(BpIdentity)))
                {
                    this._bpidentity = (BpIdentity)bpidentity;
                }
                else
                {
                    int ierr = AdminBpIdentity.Retrieve((int)bpidentity, out this._bpidentity, out msg);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BpBir.BpBir - Asign BpIdentity", ex);
            }
        }*/

        #endregion Constructors

        #region Public Properties

        public virtual int Id
        {
            get;
            set;
        }

        public virtual BpIdentity BpIdentity
        {
            get;
            set;
        }

        public virtual int Type
        {
            get;
            set;
        }

        public virtual int Authenticationfactor
        {
            get;
            set;
        }

        public virtual int Minutiaetype
        {
            get;
            set;
        }

        public virtual int Bodypart
        {
            get;
            set;
        }

        public virtual string Data
        {
            get;
            set;
        }

        public virtual string Additionaldata
        {
            get;
            set;
        }

        public virtual DateTime Timestamp
        {
            get;
            set;
        }

        public virtual DateTime Creation
        {
            get;
            set;
        }

        public virtual string Signature
        {
            get;
            set;
        }

        public virtual int Companyidenroll
        {
            get;
            set;
        }

        public virtual int Useridenroll
        {
            get;
            set;
        }

        //Agregados para firmar y encriptar si fuera necesario.

        //Comentados para probar quitar libreria mentalis
        //public virtual Signer Signer
        //{
        //    get;
        //    set;
        //}

        //public virtual Encryptor Encryptor
        //{
        //    get;
        //    set;
        //}

        #endregion Public Properties

        //Agregado por tema de CLob en Oracle que rango entre 2000 y 4000 caracteres da error:
        //ORA-01461: s�lo puede enlazar un valor LONG para insertarlo en una columna LONG
        private void AddSuFixForOracle(ref string pdata)
        {
            try
            {
                if (pdata == null || pdata.Length == 0) return;

                int size = pdata.Length;

                string sufix = "NA";

                if (size >= 1996 && size < 4000)
                {
                    sufix = new string('A', 4010 - size);
                }
                pdata = pdata + "|" + sufix;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    ////Definicion de BioApi.org
    //public class BIR_TYPE
    //{
    //    public const int RAW = 0x01;
    //    public const int INTERMEDIATE_DATA = 0x02;
    //    public const int PROCESSED_DATA = 0x04;
    //    public const int ENCRYPTED = 0x08;
    //    public const int SIGNED = 0x10;
    //    public const int ENCODING_UTF8 = 0x20;
    //    public const int ENCODING_ASCII = 0x40;

    //    public static string GetName(int type)
    //    {
    //        string ret = null;
    //        switch (type)
    //        {
    //            case BIR_TYPE.RAW:  //Minucias
    //                ret = "RAW";
    //                break;
    //            case BIR_TYPE.PROCESSED_DATA:  //Minucias
    //                ret = "Template";
    //                break;
    //            case BIR_TYPE.INTERMEDIATE_DATA: //WSQ
    //                ret = "WSQ";
    //                break;
    //            case BIR_TYPE.PROCESSED_DATA | BIR_TYPE.ENCODING_UTF8:
    //                ret = "Minucias UTF8";
    //                break;
    //            case BIR_TYPE.INTERMEDIATE_DATA | BIR_TYPE.ENCODING_UTF8:
    //                ret = "WSQ UTF8";
    //                break;
    //            case BIR_TYPE.PROCESSED_DATA | BIR_TYPE.ENCRYPTED:
    //                ret = "Minucias Encriptadas";
    //                break;
    //            case BIR_TYPE.INTERMEDIATE_DATA | BIR_TYPE.ENCRYPTED:
    //                ret = "WSQ Encriptadas";
    //                break;
    //            default:
    //                ret = "Desconocido";
    //                break;

    //        }
    //        return ret;
    //    }
    //}
}