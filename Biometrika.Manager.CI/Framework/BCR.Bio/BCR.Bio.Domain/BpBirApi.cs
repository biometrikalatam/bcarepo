﻿/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bio.Core.Api
{
    /// <summary>
    /// BpBir object for NHibernate mapped table 'bp_bir'.
    /// </summary>
    [Serializable]
    public class BpBirApi : INotifyPropertyChanged
    {
        //private static readonly ILog LOG = LogManager.GetLogger(typeof(BpBirApi));

        #region Member Variables
        protected int _id;
        protected int _bpidentity;
        protected int _authenticationfactor;
        protected int _minutiaetype;
        protected int _bodypart;
        protected string _data;
        protected string _additionaldata;
        protected DateTime _timestamp;
        protected DateTime _creation;
        protected int _companyidenroll;

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
        #region Constructors

        public BpBirApi() { }

        public BpBirApi(int bpidentity, int authenticationfactor, int minutiaetype,
            int bodypart, string data, string additionaldata, DateTime timestamp,
            DateTime creation, int companyidenroll)
        {
            this._bpidentity = bpidentity;
            this._authenticationfactor = authenticationfactor;
            this._minutiaetype = minutiaetype;
            this._bodypart = bodypart;
            this._data = data;
            this._additionaldata = additionaldata;
            this._timestamp = timestamp;
            this._creation = creation;
            this._companyidenroll = companyidenroll;
        }


        #endregion
        #region Public Properties
        public virtual int Id
        {
            get { return _id; }
            set { if (value != this._id) { _id = value; NotifyPropertyChanged("Id"); } }
        }
        public virtual int BpIdentity
        {
            get { return _bpidentity; }
            set { _bpidentity = value; }
        }
        public virtual int Authenticationfactor
        {
            get { return _authenticationfactor; }
            set { if (value != this._authenticationfactor) { _authenticationfactor = value; NotifyPropertyChanged("Authenticationfactor"); } }
        }
        public virtual int Minutiaetype
        {
            get { return _minutiaetype; }
            set { if (value != this._minutiaetype) { _minutiaetype = value; NotifyPropertyChanged("Minutiaetype"); } }
        }
        public virtual int Bodypart
        {
            get { return _bodypart; }
            set { if (value != this._bodypart) { _bodypart = value; NotifyPropertyChanged("Bodypart"); } }
        }
        public virtual string Data
        {
            get { return _data; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 2147483647 characters");
                if (value != this._data) { _data = value; NotifyPropertyChanged("Data"); }
            }
        }
        public virtual string Additionaldata
        {
            get { return _additionaldata; }
            set
            {
                if (value != null && value.Length > 2048)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 2048 characters");
                if (value != this._additionaldata) { _additionaldata = value; NotifyPropertyChanged("Additionaldata"); }
            }
        }
        public virtual DateTime Timestamp
        {
            get { return _timestamp; }
            set { if (value != this._timestamp) { _timestamp = value; NotifyPropertyChanged("Timestamp"); } }
        }
        public virtual DateTime Creation
        {
            get { return _creation; }
            set { if (value != this._creation) { _creation = value; NotifyPropertyChanged("Creation"); } }
        }
        public virtual int Companyidenroll
        {
            get { return _companyidenroll; }
            set { if (value != this._companyidenroll) { _companyidenroll = value; NotifyPropertyChanged("Companyidenroll"); } }
        }
        protected virtual void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            if ((obj == null) || (obj.GetType() != this.GetType())) return false;
            BpBirApi castObj = (BpBirApi)obj;
            return (castObj != null) &&
            this._id == castObj.Id;
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }


}
