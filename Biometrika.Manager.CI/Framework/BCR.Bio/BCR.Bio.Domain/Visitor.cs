﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class Visitor : AbstractEntity, IVisitor
    {
        public virtual BpIdentity Identity { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual IList<Vehicle> Vehicles { get; set; }

        public virtual Provider Provider { get; set; }

        public virtual VisitorTrakingRoute TrakingRoute { get; set; }

        public virtual bool IsEnrolled { get; set; }

        public virtual IList<Unity> Unitys { get; set; }

        public virtual int VisitCounter { get; set; }
    }

}