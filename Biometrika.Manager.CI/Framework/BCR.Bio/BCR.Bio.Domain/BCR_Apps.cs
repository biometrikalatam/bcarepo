﻿using BCR.Bio.Domain.Abstract;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class BCR_Apps : AbstractEntity
    {
        public virtual string Application { get; set; }

        public virtual string Menu { get; set; }

        public virtual string Key { get; set; }

        public virtual IList<Company> Companies { get; set; }
    }
}