﻿
using Newtonsoft.Json;

namespace BCR.Bio.Domain
{
    public class ResponseCreateTx
    {
        [JsonProperty("trackid")]
        public string Trackid { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("ciwebUrl")]
        public string CiwebUrl { get; set; }
    }
}
