﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class Dept :  AbstractEntityItem<int>, IDept
    {
        public virtual Country Country { get; set; }

        public virtual IList<City> Cities { get; set; }

        public Dept()
        {
            Cities = new List<City>();
        }
    }
}