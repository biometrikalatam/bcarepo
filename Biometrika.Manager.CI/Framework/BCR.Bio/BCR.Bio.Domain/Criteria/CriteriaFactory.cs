using BCR.Bio.Domain.Interface;
using BCR.System.Extension;
using NHibernate.Criterion;

namespace BCR.Bio.Domain.Criteria
{
    /// <summary>
    ///
    /// </summary>
    public static class CriteriaFactory
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="companyName"></param>
        /// <param name="exactMatch"></param>
        /// <param name="insensitive"></param>
        /// <returns></returns>
        public static DetachedCriteria CompanyByName(string companyName, int holdingId, bool exactMatch = true, bool insensitive = true)
        {
            if (exactMatch)
                return DetachedCriteria.For<ICompany>()
                    .Add(Expression.Eq("Name", companyName))
                    .Add(Expression.Eq("holding", holdingId));
            else
                if (insensitive)
                return DetachedCriteria.For<ICompany>()
                    .Add(Expression.InsensitiveLike("Name", companyName.ToSQLLike()))
                    .Add(Expression.Eq("holding", holdingId));
            else
                return DetachedCriteria.For<ICompany>()
                    .Add(Expression.Like("Name", companyName.ToSQLLike()))
                    .Add(Expression.Eq("holding", holdingId));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        public static DetachedCriteria CompanyByRut(string rut, int holdingId)
        {
            return DetachedCriteria.For<ICompany>()
                .Add(Expression.Eq("Rut", rut))
                .Add(Expression.Eq("holding", holdingId));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static DetachedCriteria CompanyByRutAndDomain(string rut, string domain, int holdingId)
        {
            DetachedCriteria criteria;

            criteria = DetachedCriteria.For<ICompany>().Add(Expression.Eq("Holding", holdingId));
            if (!string.IsNullOrEmpty(rut))
                criteria.Add(Expression.Eq("Rut", rut));

            if (!string.IsNullOrEmpty(domain))
                criteria.Add(Expression.InsensitiveLike("Domain", domain));

            return criteria;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="status"></param>
        /// <param name="companyName"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public static DetachedCriteria CompanyByRutAndStatusAndName(string rut, int status, string companyName, int holdingId, bool exactMatch = true)
        {
            DetachedCriteria criteria;

            criteria = DetachedCriteria.For<ICompany>().Add(Expression.Eq("Holding", holdingId));
            if (!string.IsNullOrEmpty(rut))
                criteria.Add(Expression.Eq("Rut", rut));

            if (status == 0)
                criteria.Add(Expression.Eq("Status", true));

            if (status == 1)
                criteria.Add(Expression.Eq("Status", false));

            if (!string.IsNullOrEmpty(companyName))
                criteria.Add(Expression.InsensitiveLike("Name", companyName));

            return criteria;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="companyName"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public static DetachedCriteria CompanyByRutAndName(string rut, string companyName, int holdingId, bool exactMatch = true)
        {
            DetachedCriteria detachedCriteria = DetachedCriteria.For<ICompany>();
            if (!string.IsNullOrEmpty(rut))
                detachedCriteria
                    .Add(Expression.Eq("Rut", rut));
            if (!string.IsNullOrEmpty(companyName))
                detachedCriteria
                    .Add(Expression.Eq("Name", companyName));

            //detachedCriteria.Add(Expression.Eq("Holding", holdingId));
            return detachedCriteria;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static DetachedCriteria CompanyByRutAndStatus(string rut, bool status, int holdingId)
        {
            DetachedCriteria criteria;

            criteria = DetachedCriteria.For<ICompany>()
                .Add(Expression.Eq("Holding", holdingId))
                .Add(Expression.Eq("Status", status));

            if (!string.IsNullOrEmpty(rut))
                criteria.Add(Expression.Eq("Rut", rut));

            return criteria;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stringToMatch"></param>
        /// <param name="exactMatch"></param>
        /// <param name="insensitive"></param>
        /// <returns></returns>
        public static DetachedCriteria ItemByDescription<T>(string stringToMatch, bool exactMatch = true, bool insensitive = true)
        {
            if (exactMatch)
                return DetachedCriteria.For<T>().Add(Expression.Eq("Description", stringToMatch));
            else
                if (insensitive)
                return DetachedCriteria.For<T>().Add(Expression.InsensitiveLike("Description", stringToMatch.ToSQLLike()));
            else
                return DetachedCriteria.For<T>().Add(Expression.Like("Description", stringToMatch.ToSQLLike()));
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="holdingId"></param>
        /// <returns></returns>
        public static DetachedCriteria GetByHolding<T>(int holdingId)
        {
            return DetachedCriteria.For<T>().Add(Expression.Eq("holding", holdingId));
        }
    }
}