﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;

namespace BCR.Bio.Domain
{
    public class UserRol : AbstractEntity, IUserRol
    {
        public virtual User UserId
        {
            get;
            set;
        }

        public virtual Rol RolId
        {
            get;
            set;
        }
    }
}