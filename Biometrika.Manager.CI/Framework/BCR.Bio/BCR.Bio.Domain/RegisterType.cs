﻿/* Clase que define el origen del tipo de marcas.
 * Si fue realizado usando su huella, cédula o cualquier otro tipo
 * 
*/
 
using BCR.Bio.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class RegisterType: AbstractEntity
    {
        public virtual string Tipo { get; set; }
        public virtual string Description { get; set; }
    }
}
