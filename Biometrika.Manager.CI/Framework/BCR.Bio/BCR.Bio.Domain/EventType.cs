﻿using BCR.Bio.Domain.Abstract;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Clase de dominio que hace referencia a la tabla EventType, es utilizada para contener el tipo de acceso a realizar que no es lo mismo que el tipo de marca
    /// </summary>
    public class EventType : AbstractEntityItem<int>
    {
        //public virtual string Description { get; set; }
    }
}