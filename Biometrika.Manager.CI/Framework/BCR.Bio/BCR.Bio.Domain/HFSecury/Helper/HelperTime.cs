﻿using System;

namespace BCR.Bio.Domain.HFSecury.Helper
{
    public static class HelperTime
    {
        /// <summary>
        /// Get a 13-bit timestamp
        /// </summary>
        /// <returns></returns>
        public static string GetTimeStamp()
        {
            DateTime time = DateTime.Now;
            long ts = ConvertDateTimeToInt(time);
            return ts.ToString();
        }
        /// <summary>  
        /// Convert c# DateTime time format to Unix timestamp format
                /// </summary>  
        /// <param name="time">time</param>
                /// <returns>long</returns>  
        private static long ConvertDateTimeToInt(DateTime time)
        {
            //new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc); // 
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1, 0, 0, 0, 0));
            long t = (time.Ticks - startTime.Ticks) / 10000; // except 10000 is adjusted to 13 bits
            return t;
        }

        public static string epoch2string(int epoch)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).
                        AddSeconds(epoch).ToShortDateString();
        }



        //public static DateTime UnixTimeStampToDateTime2(long unixTimeStamp)
        //{
        //    // Unix timestamp is seconds past epoch
        //    DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        //    dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        //    return dtDateTime;
        //}

        //public static DateTime UnixTimestampToDateTime(double unixTime)
        //{
        //    DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        //    long unixTimeStampInTicks = (long)(unixTime * TimeSpan.TicksPerSecond);
        //    //DateTime d1 = new DateTime(unixStart.Ticks + unixTimeStampInTicks, DateTimeKind.Utc);
        //    //DateTime d2 = new DateTime(unixStart.Ticks + unixTimeStampInTicks, DateTimeKind.Local);
        //    //DateTime d3 = new DateTime(unixStart.Ticks + unixTimeStampInTicks, DateTimeKind.Unspecified);
        //    return new DateTime(unixStart.Ticks + unixTimeStampInTicks, DateTimeKind.Local);
        //}

        /// <summary>
        /// Convert Unix time value to a DateTime object.
        /// </summary>
        /// <param name="unixtime">The Unix time stamp you want to convert to DateTime.</param>
        /// <returns>Returns a DateTime object that represents value of the Unix time.</returns>

        public static DateTime UnixTimeToDateTime(long unixtime)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixtime).ToLocalTime();
            return dtDateTime;
        }

        /// <summary>
        /// Convert a DateTime to a unix timestamp
        /// </summary>
        /// <param name="MyDateTime">The DateTime object to convert into a Unix Time</param>
        /// <returns></returns>
        public static long DateTimeToUnix(DateTime MyDateTime)
        {
            TimeSpan timeSpan = MyDateTime - new DateTime(1970, 1, 1, 0, 0, 0);

            return (long)timeSpan.TotalSeconds;
        }

        ////////
        ////// <summary>
        /// Converts the given date value to epoch time.
        /// </summary>
        public static long ToEpochTime(this DateTime dateTime)
        {
            var date = dateTime.ToUniversalTime();
            var ticks = date.Ticks - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).Ticks;
            var ts = ticks / TimeSpan.TicksPerSecond;
            return ts;
        }

        /// <summary>
        /// Converts the given date value to epoch time.
        /// </summary>
        public static long ToEpochTime(this DateTimeOffset dateTime)
        {
            var date = dateTime.ToUniversalTime();
            var ticks = date.Ticks - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero).Ticks;
            var ts = ticks / TimeSpan.TicksPerSecond;
            return ts;
        }

        /// <summary>
        /// Converts the given epoch time to a <see cref="DateTime"/> with <see cref="DateTimeKind.Utc"/> kind.
        /// </summary>
        public static DateTime ToDateTimeFromEpoch(this long intDate)
        {
            var timeInTicks = intDate * TimeSpan.TicksPerSecond;
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddTicks(timeInTicks);
        }

        /// <summary>
        /// Converts the given epoch time to a UTC <see cref="DateTimeOffset"/>.
        /// </summary>
        public static DateTimeOffset ToDateTimeOffsetFromEpoch(this long intDate)
        {
            var timeInTicks = intDate * TimeSpan.TicksPerSecond;
            return new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero).AddTicks(timeInTicks);
        }
    }
}
