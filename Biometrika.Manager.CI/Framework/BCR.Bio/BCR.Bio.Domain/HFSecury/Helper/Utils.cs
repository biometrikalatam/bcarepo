﻿using System;
using System.ComponentModel;

namespace BCR.Bio.Domain.HFSecury.Helper
{
    public class Utils
    {
        public static string ToString(object obj)
        {
            string ret = "";
            string name;
            object value;
            try
            {
                bool isFirst = true;
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
                {
                    name = descriptor.Name;
                    value = descriptor.GetValue(obj);

                    if (isFirst)
                    {
                        isFirst = false;
                        ret = name + "=" + value.ToString();
                    }
                    else
                    {
                        ret = ret + "|" + name + "=" + (value != null ? value.ToString() : "null");
                    }
                }
            }
            catch (Exception ex)
            {
                ret = "";
                //Log4Bio.Error(" Error: " + ex.Message);
            }
            return ret;
        }
    }
}
