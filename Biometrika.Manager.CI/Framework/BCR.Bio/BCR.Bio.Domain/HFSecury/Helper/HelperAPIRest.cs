﻿using BCR.Bio.Domain.HFSecury.Models;
using BCR.System.Log;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.HFSecury.Helper
{
    public class HelperAPIRest
    {

        public static bool DebugExtendido { get; set; } = true; //Graba salidas desde response, pueden ser grandes
                                                                //por eso se pude deshabilitar
        public static int Timeout { get; set; } = 30000;

        public static Hashtable DevicesList;

        /// <summary>
        /// Llamada al API Rest del device generica. 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="method"></param>
        /// <param name="result">PArsea resultado. Luego en data segun llamado se procesa en cada 
        /// llamante</param>
        /// <returns>codigp de error => 0-OK y negativo error</returns>
        public static int HRequest(string url, Method method, out Result result)
        {
            int ret = 0;
            result = null;
            try
            {
                Log4Bio.Debug("HRequest IN...");

                Log4Bio.Debug("HRequest URL => " + url + " - Method= " + method.ToString() +
                            " - Timeout= " + Timeout.ToString());
                var client = new RestClient(url);
                client.Timeout = Timeout;
                var request = new RestRequest(method);
                Log4Bio.Debug("HRequest Calling...");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == HttpStatusCode.OK)
                {
                    if (DebugExtendido) Log4Bio.Debug("HRequest response.Content = " + response.Content);
                    else Log4Bio.Debug("HRequest response.Content!=null => " + (!string.IsNullOrEmpty(response.Content)));
                    result = JsonConvert.DeserializeObject<Result>(response.Content);
                }
                else
                {
                    Log4Bio.Debug("HRequest response.Content = NULL");
                    ret = -2;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                Log4Bio.Error("HRequest Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("HRequest OUT! => ret = " + ret.ToString());
            return ret;
        }

        internal static int HRequestForm(string url, Dictionary<string, object> parametros, Method method,
                                         out Result result)
        {
            int ret = 0;
            result = null;
            try
            {
                Log4Bio.Debug("HRequestForm IN...");

                Log4Bio.Debug("HRequestForm URL => " + url + " - Method= " + method.ToString() +
                            " - Timeout= " + Timeout.ToString());

                //if (parametros.ContainsKey("imgBase64") && url.Contains("/face/create"))
                //{
                //    Face face = new Face
                //    {
                //        pass = parametros["pass"].ToString(),
                //        personId = parametros["personId"].ToString(),
                //        imgBase64 = parametros["imgBase64"].ToString()
                //    };
                //    string faceJson = JsonConvert.SerializeObject(face);
                //    //StringContent httpContent = new StringContent(faceJson, Encoding.UTF8, "application/json");
                //    Dictionary<string, string> parametetrosDictStr = new Dictionary<string, string>();
                //    foreach(var item in parametros)
                //    {
                //        parametetrosDictStr.Add(item.Key, item.Value.ToString());
                //    }
                //    var encodedContent = new FormUrlEncodedContent(parametetrosDictStr);
                //    using (var httpClient = new HttpClient())
                //    {
                //        var httpResponse = httpClient.PostAsync(url, encodedContent);
                //        var settingsJson = new JsonSerializerSettings
                //        {
                //            NullValueHandling = NullValueHandling.Ignore,
                //            MissingMemberHandling = MissingMemberHandling.Ignore
                //        };


                //        if (httpResponse.Result != null && httpResponse.Result.StatusCode.ToString().Equals("OK"))
                //        {
                //            Log4Bio.Info("Respuesta obtenida: 200 OK");
                //            Task<string> responseContent = httpResponse.Result.Content.ReadAsStringAsync();
                //            Result resultApi;
                //            try
                //            {
                //                Log4Bio.Info("Deserializamos respuesta :" + responseContent.Result.ToString());
                //                resultApi = JsonConvert.DeserializeObject<Result>(responseContent.Result, settingsJson);
                //                Log4Bio.Info(resultApi.ToString());
                //                return 1;
                //            }
                //            catch (Exception ex)
                //            {
                //                Log4Bio.Error("Error al deserializar respuesta de la api: " + ex.ToString());
                //                ex.ToString();
                //                return -1;
                //            }
                //        }
                //        else
                //        {
                //            // Error al enviar solicitud 
                //            Log4Bio.Error("Error en respuesta obtenida de la API: " + httpResponse.Result.StatusCode);
                //            Log4Bio.Error(httpResponse.Result.StatusCode.ToString());
                //            Log4Bio.Error(httpResponse.Result.ToString());
                //            return -1;
                //        }

                //    }


                //}
                //else
                //{
                    var client = new RestClient(url);
                    client.Timeout = Timeout;
                    var request = new RestRequest(method);
                    request.AddHeader("Content-Type", "multipart/form-data");
                    foreach (var item in parametros)
                    {
                        request.AddParameter(item.Key, item.Value);
                    }
                    Log4Bio.Debug("HRequestForm Calling...");
                    IRestResponse response = client.Execute(request);

                    if (response != null && response.StatusCode == HttpStatusCode.OK)
                    {
                        if (DebugExtendido) Log4Bio.Debug("HRequestForm response.Content = " + response.Content);
                        else Log4Bio.Debug("HRequestForm response.Content!=null => " + (!string.IsNullOrEmpty(response.Content)));
                        result = JsonConvert.DeserializeObject<Result>(response.Content);
                    }
                    else
                    {
                        Log4Bio.Debug("HRequestForm response.Content = NULL");
                        ret = -2;
                    }
                //}

                //request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                //request.AddHeader("Content-Type", "undefined");

                //request.AddHeader("Content-Type", "text/plain;charset=UTF-8");
                //if (parametros.ContainsKey("imgBase64") && url.Contains("/face/create"))
                //{
                //    Face face = new Face
                //    {
                //        personId = parametros["personId"].ToString(),
                //        pass = parametros["pass"].ToString(),
                //        imgBase64 = parametros["imgBase64"].ToString(),
                //    };
                //    request.AddBody(face);

                //} else
                //{
                //    foreach (var item in parametros)
                //    {
                //        request.AddParameter(item.Key, item.Value);
                //    }
                //}





            }
            catch (Exception ex)
            {
                ret = -1;
                Log4Bio.Error("HRequestForm Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("HRequestForm OUT! => ret = " + ret.ToString());
            return ret;
        }
    }
}
