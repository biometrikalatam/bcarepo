﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.HFSecury.Models
{
    public class Code
    {
        /*
        appendix
            Code    Explanation
            000     Request successful
            400     Request parameter validation fails
            401     Signature verification fails
            403     Permissions reason for rejecting the request
            500     The system is busy, please try again later (Unified system exception return 500)
            1010    Equipment failure to restart, Un root.
            2000    Face create abnormal
            2010    Delete Face fail
        */

        public static string CODE_OK_000 = "000";
        public static string CODE_ERR_PARAMETER_FAIL_400 = "400";
        public static string CODE_ERR_SIGNATURE_FAIL_401 = "401";
        public static string CODE_ERR_PERMISSION_FAIL_403 = "403";
        public static string CODE_ERR_SYSTEM_EXCP_500 = "500";
        public static string CODE_ERR_RESTART_FAIL_1010 = "1010";
        public static string CODE_ERR_FACE_CREATE_FAIL_2000 = "2000";
        public static string CODE_ERR_DELETE_FACE_FAIL = "2010";
    }
}
