﻿
namespace BCR.Bio.Domain.HFSecury.Models
{
    public class Face
    {
        public string pass { get; set; }
        public string personId { get; set; }
        public string faceId { get; set; }
        public string imgBase64 { get; set; }
        public string imgUrl { get; set; }

    }
}
