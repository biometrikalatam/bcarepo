﻿
namespace BCR.Bio.Domain.HFSecury.Models
{

    public class Result
    {
        public string code { get; set; } //Return code, described in Appendix
        public object data { get; set; } //Return data objects
        public string msg { get; set; } //returned messages
        public int result { get; set; } //It is transferred through the interface,1 success, 0 failure
        public bool success { get; set; } //Whether the operation is successful, the success of true and Failure of false
    }

}
