﻿
namespace BCR.Bio.Domain.HFSecury.Models
{
    /*
     {
         "comModContent": "hello", // String, Serial output mode customizations
         "comModType": 100, // Int, Serial output mode
         "companyName": "My test", // String, Company name, location of the display device screen See
         "delayTimeForCloseDoor": 500, // Int, Close to the door opening relay control interval, ms
         "displayModContent": "{Name}Welcome", // String Identifying the text display mode custom content
         "displayModType": 100, // Int Identifying text display mode
         "identifyDistance": 1, // Int Identify the distance, 0: Unlimited, 1:0.5 Meters, 2:1 Meters, 3:1.5 Meters, 4: 3 Meters
         "saveIdentifyTime": 0, // Int, The same face recognition camera is not spaced out(100ms)
         "identifyScores": 80, // Int, Recognition score
         "multiplayerDetection": 1, // Int, And more personal face detection settings
         "recRank": 2, // Int Identify grade- grade 1: Not open live identification; grade 2: reject some photos; grade 3: reject videos and photos, As identified farthest 1.5 Meters
         "recStrangerTimesThreshold": 3, // Int Stranger judgment
         "recStrangerType": 2, // Int, Switch stranger (stranger whether identification)
         "ttsModContent": "welcome{name} ", // String, Voice broadcast mode custom content
         "rtsModStrangerContent": "hello stranger ", // String The stranger within the custom voice broadcast mode Allow
         "rtsModStrangerType": 100, // Int Stranger voice broadcast mode
         "rtsModType": 100, // Int, Voice broadcast mode
         "wg": "#WG {id} #", // String Wiegand type and output
         "whitelist": 1, // Int, ID than the whitelist switch,1:turn off,2:open
         "saveIdentifyMode": 1, // Int, Callback cognizant recording mode,0: Not resume 1: Resume
         "onLightStartTime": 0, // Start Time On fill light, fill light lit control
         "onLightEndTime": 0, // Start and end time of the fill light, End time must be greater than start time, if 0, The lights turned off Steady
         }
    */
    public class Config
    {
        public string comModContent { get; set; } = "hello";
        public string comModType { get; set; } = "1";
        public string companyName { get; set; } = "Biometrika";
        public string delayTimeForCloseDoor { get; set; } = "1000";
        public string displayModContent { get; set; } = "{name}";
        public string displayModType { get; set; } = "1";
        public string identifyDistance { get; set; } = "1";
        public string saveIdentifyTime { get; set; } = "1";
        public string identifyScores { get; set; } = "65";
        public string multiplayerDetection { get; set; }
        public string recRank { get; set; } = "2";
        public string recStrangerTimesThreshold { get; set; } = "1";
        public string recStrangerType { get; set; } = "1";
        public string ttsModContent { get; set; } = "\n{name}";
        public string rtsModStrangerContent { get; set; } = "Extraño";
        public string rtsModStrangerType { get; set; } = "2";
        public string rtsModType { get; set; } = "2";
        public string wg { get; set; } = "#WG{id}#";
        public string whitelist { get; set; } = "1";
        public string saveIdentifyMode { get; set; } = "1";
        public string onLightStartTime { get; set; } = "0";
        public string onLightEndTime { get; set; } = "0";

        public string ToString()
        {
            return "";
            //return Helpers.Utils.ToString(this);
        }
    }
}
