﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.HFSecury.Models
{
    class Callbacks
    {
    }

    public class CallbackModel
    {
        public double standard { get; set; }
        public double searchScore { get; set; }
        public string data { get; set; } //viene lectura de codigo de barras
        public string ip { get; set; }
        public string deviceKey { get; set; }
        public string direction { get; set; }
        public string type { get; set; }
        public string personName { get; set; }
        public string path { get; set; }
        public string imgBase64 { get; set; }
        public string temperature { get; set; }
        public string personId { get; set; }
        public string time { get; set; }
        public string temperatureState { get; set; }
        public string livenessScore { get; set; }
        public string mask { get; set; }

    }

    public class CallbackHeartBeat
    {
        public string deviceKey { get; set; }
        public string time { get; set; }
        public string personCount { get; set; }
        public string faceCount { get; set; }
        public string ip { get; set; }
        public string version { get; set; }
    }

    public class CallbackQRModel
    {
        public string deviceKey { get; set; }
        public string time { get; set; }
        public string qrcode { get; set; }
    }
}
