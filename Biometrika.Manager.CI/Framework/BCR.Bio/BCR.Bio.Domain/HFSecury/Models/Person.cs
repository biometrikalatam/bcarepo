﻿namespace BCR.Bio.Domain.HFSecury.Models
{
    public class Person
    {
        public string id { get; set; }
        public string name { get; set; }
        public string idcardNum { get; set; }
    }
}