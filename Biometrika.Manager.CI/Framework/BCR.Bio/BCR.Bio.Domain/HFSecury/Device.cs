﻿using BCR.Bio.Domain.HFSecury.Helper;
using BCR.Bio.Domain.HFSecury.Models;
using BCR.System.Log;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace BCR.Bio.Domain.HFSecury
{
    public class Device
    {
        public string SN { get; set; } = "SNlocalhost";
        public string Name { get; set; } = "DeviceNameDefault";
        public string Protocol { get; set; } = "http";
        public string IP { get; set; } = "localhost";
        public string Port { get; set; } = "8090";
        public string Password { get; set; }
        public string Config { get; set; }
        public string CallbackIdentifyUrl { get; set; }
        //public string CallbackQREventUrl { get; set; }
        public string CallbackHeart { get; set; }

        public static bool DebugExtendido { get; set; } = false; //Graba salidas desde response, pueden ser grandes
                                                                //por eso se pude deshabilitar
        public Device() { }

        public int Initialize(string name, string ip, string port, string password) {
            int ret = 0;
            try
            {
                Name = name;
                IP = ip;
                Port = port;
                Password = password;
                string strSN;
                ret = GetSN(out strSN);
                SN = strSN;
            }
            catch (Exception ex)
            {

                Log4Bio.Error(" Error: " + ex.Message);
            }

            return ret;
        }

#region Admin Gral

        
        /// <summary>
        /// Devuelve el Serial Device Key o SN<IP> sino
        /// </summary>
        /// <param name="serialDeviceKey"></param>
        /// <returns></returns>
        public int GetSN(out string serialDeviceKey)
        {
            int ret = 0;
            Result result = null;
            serialDeviceKey = null;
            try
            {
                Log4Bio.Debug("GetSN IN...");
                HelperAPIRest.HRequest(Protocol + "://" + IP + ":" + Port + "/getDeviceKey", 
                                               Method.GET, out result);

                if (result != null)
                {
                    serialDeviceKey = (string)result.data;
                } else
                {
                    serialDeviceKey = "SN" + IP;
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                Log4Bio.Error("GEtSN Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("GetSN OUT! SN => " + (serialDeviceKey));
            return ret;
        }

        /// <summary>
        /// Setea logo que se muestra en el inicio y en la pantalla inicial
        /// </summary>
        /// <param name="base64logo"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        public int SetLogo(string base64logo, out string msgerr)
        {
            int ret = 0;
            Result result = null;
            msgerr = "";
            try
            {
                Log4Bio.Debug("SetLogo IN...");
                ret = HelperAPIRest.HRequest(Protocol + "://" + IP + ":" + Port + "/changeLogo?pass=" +
                                               Password + "&imgBase64=" + base64logo,
                                               Method.POST, out result);

                if (result != null && result.code.Equals(Code.CODE_OK_000))
                {
                    ret = 0;
                }
                else
                {
                    msgerr = "Error desde Device [Code = " + result.code + "|msg=" + result.msg + "]";
                    Log4Bio.Error("SetLogo - Error=" + msgerr);
                    ret = -2;
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("SetLogo Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("SetLogo OUT! ret => " + ret.ToString());
            return ret;
        }

        public int SetTime(string currenttime, out string msgerr)
        {
            int ret = 0;
            Result result = null;
            msgerr = "";
            try
            {
                Log4Bio.Debug("SetTime IN...");
                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("pass", Password);
                parametros.Add("timestamp", currenttime);

                ret = HelperAPIRest.HRequestForm(Protocol + "://" + IP + ":" + Port +
                                            "/setTime", parametros,
                                               Method.POST, out result);

                if (result != null && result.code.Equals(Code.CODE_OK_000))
                {
                    ret = 0;
                }
                else
                {
                    msgerr = "Error desde Device [Code = " + result.code + "|msg=" + result.msg + "]";
                    Log4Bio.Error("SetTime - Error=" + msgerr);
                    ret = -2;
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("SetTime Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("SetTime OUT! ret => " + ret.ToString());
            return ret;
        }


        /// <summary>
        /// Reinicia el device completo
        /// </summary>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        public int Restart(out string msgerr)
        {
            int ret = 0;
            Result result = null;
            msgerr = "";
            try
            {
                Log4Bio.Debug("Restart IN...");
                ret = HelperAPIRest.HRequest(Protocol + "://" + IP + ":" + Port + "/restartDevice?pass="
                                               + Password,
                                               Method.POST, out result);

                //if (result != null && result.result == 1)
                //{
                //    ret = 0;
                //}
                //else
                //{
                //    msgerr = "Error desde Device [result = " + result.code + "|msg=" + result.msg + "]";
                //    Log4Bio.Error("SetLogo - Error=" + msgerr);
                //    ret = -2;
                //}
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("Restart Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("Restart OUT! ret => " + ret.ToString());
            return ret;
        }

        public int SetCallbackIdentifyUrl(string url, out string msgerr)
        {
            int ret = 0;
            Result result = null;
            msgerr = "";
            try
            {
                Log4Bio.Debug("SetCallbackIdentifyUrl IN...");
                ret = HelperAPIRest.HRequest(Protocol + "://" + IP + ":" + Port + "/setIdentifyCallBack?pass="
                                               + Password + "&callbackUrl= " + url,
                                               Method.POST, out result);

                if (result != null && result.code.Equals(Code.CODE_OK_000))
                {
                    ret = 0;
                }
                else
                {
                    msgerr = "Error desde Device [result = " + result.code + "|msg=" + result.msg + "]";
                    Log4Bio.Error("SetCallbackIdentifyUrl - Error=" + msgerr);
                    ret = -2;
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("SetCallbackIdentifyUrl Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("SetCallbackIdentifyUrl OUT! ret => " + ret.ToString());
            return ret;
        }

        public int SetDeviceHeartBeatUrl(string url, out string msgerr)
        {
            int ret = 0;
            Result result = null;
            msgerr = "";
            try
            {
                Log4Bio.Debug("SetDeviceHeartBeatUrl IN...");
                string urlToSend = Protocol + "://" + IP + ":" + Port + "/setDeviceHeartBeat?pass=" + Password + "&url= " + url;
                ret = HelperAPIRest.HRequest(urlToSend, Method.POST, out result);

                if (result != null && result.code.Equals(Code.CODE_OK_000))
                {
                    ret = 0;
                }
                else
                {
                    msgerr = "Error desde Device [result = " + result.code + "|msg=" + result.msg + "]";
                    Log4Bio.Error("SetDeviceHeartBeatUrl - Error=" + msgerr);
                    ret = -2;
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("SetDeviceHeartBeatUrl Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("SetDeviceHeartBeatUrl OUT! ret => " + ret.ToString());
            return ret;
        }

#endregion Admin Gral

#region Admin Personas

        public int AddPersona(Person person, out string msgerr)
        {
            int ret = 0;
            Result result = null;
            msgerr = "";
            try
            {
                Log4Bio.Debug("AddPersona IN...");

                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("pass", Password);
                parametros.Add("person", JsonConvert.SerializeObject(person));

                ret = HelperAPIRest.HRequestForm(Protocol + "://" + IP + ":" + Port +
                                            "/person/create", parametros,
                                               Method.POST, out result);

                if (result != null && result.code.Equals(Code.CODE_OK_000))
                {
                    ret = 0;
                }
                else
                {
                    msgerr = "Error desde Device [result = " + result.code + "|msg=" + result.msg + "]";
                    Log4Bio.Error("AddPersona - Error=" + msgerr);
                    ret = -2;
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("AddPersona Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("AddPersona OUT! ret => " + ret.ToString());
            return ret;
        }

        public int UpdatePersona(Person person, out string msgerr)
        {
            int ret = 0;
            Result result = null;
            msgerr = "";
            try
            {
                Log4Bio.Debug("UpdatePersona IN...");

                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("pass", Password);
                parametros.Add("person", Newtonsoft.Json.JsonConvert.SerializeObject(person));

                ret = HelperAPIRest.HRequestForm(Protocol + "://" + IP + ":" + Port +
                                            "/person/update", parametros,
                                               Method.POST, out result);

                if (result != null && result.code.Equals(Code.CODE_OK_000))
                {
                    ret = 0;
                }
                else
                {
                    msgerr = "Error desde Device [result = " + result.code + "|msg=" + result.msg + "]";
                    Log4Bio.Error("UpdatePersona - Error=" + msgerr);
                    ret = -2;
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("UpdatePersona Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("UpdatePersona OUT! ret => " + ret.ToString());
            return ret;
        }

        public int DeletePersona(string id, out string msgerr)
        {
            int ret = 0;
            Result result = null;
            msgerr = "";
            try
            {
                Log4Bio.Debug("DeletePersona IN...");

                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("pass", Password);
                parametros.Add("id", id);

                ret = HelperAPIRest.HRequestForm(Protocol + "://" + IP + ":" + Port +
                                            "/person/delete", parametros,
                                               Method.POST, out result);

                if (result != null && result.code.Equals(Code.CODE_OK_000))
                {
                    ret = 0;
                }
                else
                {
                    msgerr = "Error desde Device [result = " + result.code + "|msg=" + result.msg + "]";
                    Log4Bio.Error("DeletePersona - Error=" + msgerr);
                    ret = -2;
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("DeletePersona Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("DeletePersona OUT! ret => " + ret.ToString());
            return ret;
        }

        public int AddPhotoByUrl(string personId, string faceId, string imgUrl, out string msgerr)
        {
            Face face = new Face
            {
                pass = Password,
                personId = personId,
                faceId = faceId,
                imgUrl = imgUrl
            };
            int ret = 0;
            Result result = null;
            msgerr = "";

            try
            {
                Log4Bio.Debug("AddPhotoByUrl IN...");
                Log4Bio.Info("Parametros: ");
                Log4Bio.Info("personId: " + face.personId);
                Log4Bio.Info("faceId: " + face.faceId);
                Log4Bio.Info("imgUrl: " + face.imgUrl);

                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("pass", face.pass);
                parametros.Add("personId", face.personId);
                parametros.Add("faceId", face.faceId);
                parametros.Add("imgUrl", face.imgUrl);
                ret = HelperAPIRest.HRequestForm(Protocol + "://" + IP + ":" + Port +
                                            "/face/createByUrl", parametros,
                                               Method.POST, out result);

                if (result != null && result.code.Equals(Code.CODE_OK_000))
                {
                    ret = 0;
                }
                else
                {
                    msgerr = "Error desde Device [result = " + result.code + "|msg=" + result.msg + "]";
                    Log4Bio.Error("AddPhoto - Error=" + msgerr);
                    ret = -2;
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("AddPhoto Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("AddPhoto OUT! ret => " + ret.ToString());
            return ret;
        }

        public int AddPhoto(string personId, string faceId, string photoB64, out string msgerr)
        {
            int ret = 0;
            Result result = null;
            msgerr = "";
            try
            {
                Log4Bio.Debug("AddPhoto IN...");

                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("pass", Password);
                parametros.Add("personId", personId);
                parametros.Add("faceId", faceId);
                parametros.Add("imgBase64", photoB64);

                ret = HelperAPIRest.HRequestForm(Protocol + "://" + IP + ":" + Port +
                                            "/face/create", parametros,
                                               Method.POST, out result);

                if (result != null && result.code.Equals(Code.CODE_OK_000))
                {
                    ret = 0;
                }
                else
                {
                    msgerr = "Error desde Device [result = " + result.code + "|msg=" + result.msg + "]";
                    Log4Bio.Error("AddPhoto - Error=" + msgerr);
                    ret = -2;
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("AddPhoto Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("AddPhoto OUT! ret => " + ret.ToString());
            return ret;
        }

        public int DeletePhoto(string faceId, out string msgerr)
        {
            int ret = 0;
            Result result = null;
            msgerr = "";
            try
            {
                Log4Bio.Debug("DeletePhoto IN...");

                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("pass", Password);
                parametros.Add("faceId", faceId);

                ret = HelperAPIRest.HRequestForm(Protocol + "://" + IP + ":" + Port +
                                            "/face/delete", parametros,
                                               Method.POST, out result);

                if (result != null && result.code.Equals(Code.CODE_OK_000))
                {
                    ret = 0;
                }
                else
                {
                    msgerr = "Error desde Device [result = " + result.code + "|msg=" + result.msg + "]";
                    Log4Bio.Error("DeletePhoto - Error=" + msgerr);
                    ret = -2;
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("DeletePhoto Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("DeletePhoto OUT! ret => " + ret.ToString());
            return ret;
        }

        public int EnrollPersona(Person person, string faceId, string photoB64, out string msgerr)
        {
            int ret = 0;
            Result result = null;
            msgerr = "";
            try
            {
                Log4Bio.Debug("EnrollPersona IN...");

                Log4Bio.Debug("EnrollPersona - AddPersona => person.id = " + person.id + "|name = " + person.name + "...");
                ret = AddPersona(person, out msgerr);
                Log4Bio.Debug("EnrollPersona - AddPersona - ret = " + ret.ToString());

                if (ret == 0)
                {
                    if (DebugExtendido)
                        Log4Bio.Debug("EnrollPersona - Addhoto => faceId = " + faceId + "|face=" + photoB64);
                    else
                        Log4Bio.Debug("EnrollPersona - Addhoto => faceId = " + faceId);
                    ret = AddPhoto(person.id, faceId, photoB64, out msgerr);
                    Log4Bio.Debug("EnrollPersona - Addhoto - ret = " + ret.ToString());
                } 
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("EnrollPersona Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("EnrollPersona OUT! ret => " + ret.ToString());
            return ret;
        }

#endregion Admin Personas

#region Admin Mark

        public int GetMarks(MarkSearchModelIn param, out IList<MarkSearchModelOut> markList, out string msgerr)
        {
            int ret = 0;
            Result result = null;
            markList = null;
            msgerr = "";
            try
            {
                Log4Bio.Debug("GetMarks IN...");
                Log4Bio.Debug("GetMarks params = " + param.ToString());
                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("pass", Password);
                parametros.Add("personId", param.personId);
                parametros.Add("length", param.length);
                parametros.Add("index", param.index);
                parametros.Add("startTime", param.startTime);
                parametros.Add("endTime", param.endTime);
                parametros.Add("model", param.model);
                parametros.Add("order", param.order);

                Log4Bio.Debug("GetMarks - Call HRequestForm..:");
                ret = HelperAPIRest.HRequestForm(Protocol + "://" + IP + ":" + Port +
                                            "/newFindRecords", parametros,
                                               Method.POST, out result);

                if (result != null && result.code.Equals(Code.CODE_OK_000))
                {
                    Log4Bio.Debug("GetMarks - Parseando lista de marcas...");
                    if (result.data != null)
                    {
                        //if ()
                        //foreach (var item in (IList)result.data)
                        //{

                        //}
                        markList = JsonConvert.DeserializeObject<IList<MarkSearchModelOut>>(result.data.ToString()); 
                        if (markList != null)
                        {
                            Log4Bio.Debug("GetMarks - Cantidad de marcas => " + markList.Count.ToString());
                        } else
                        {
                            Log4Bio.Debug("GetMarks - Parseo marcas con resultado = null...");
                        }
                    } else
                    {
                        Log4Bio.Debug("GetMarks - Lista recuperada nula...");
                    }
                    ret = 0;
                }
                else
                {
                    msgerr = "Error desde Device [result = " + result.code + "|msg=" + result.msg + "]";
                    Log4Bio.Error("GetMarks - Error=" + msgerr);
                    ret = -2;
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("GetMarks Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("GetMarks OUT! ret => " + ret.ToString());
            return ret;
        }

        public int DeleteMarks(MarkDeleteModelIn param, out int qdeleted, out string msgerr)
        {
            int ret = 0;
            Result result = null;
            qdeleted = 0;
            msgerr = "";
            try
            {
                Log4Bio.Debug("DeleteMarks IN...");
                Log4Bio.Debug("DeleteMarks params = " + param.ToString());
                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("pass", Password);
                parametros.Add("personId", param.personId);
                parametros.Add("startTime", param.startTime);
                parametros.Add("endTime", param.endTime);
                parametros.Add("model", param.model);

                Log4Bio.Debug("GetMarks - Call HRequestForm..:");
                ret = HelperAPIRest.HRequestForm(Protocol + "://" + IP + ":" + Port +
                                            "/newDeleteRecords", parametros,
                                               Method.POST, out result);

                if (result != null && result.code.Equals(Code.CODE_OK_000))
                {
                    Log4Bio.Debug("DeleteMarks - Parseando cantidad de marcas eliminadas...");
                    if (result.data != null)
                    {
                        //string[] adata = ((string)result.data).Split(':');
                        //if (adata != null && adata.Length>1)
                        //{
                            qdeleted = GetNumberFromChineseString((string)result.data);
                        //}
                        Log4Bio.Debug("DeleteMarks - Cantidad marcas eliminadas => " + qdeleted.ToString());
                    }
                    else
                    {
                        Log4Bio.Debug("DeleteMarks - result nulo...");
                    }
                    ret = 0;
                }
                else
                {
                    //TODO CHECK NULL 
                    if (result != null && !string.IsNullOrEmpty(result.code) && !string.IsNullOrEmpty(result.msg)) msgerr = "Error desde Device [result = " + result.code + "|msg=" + result.msg + "]";
                    else msgerr = "Error al eliminar marca";
                    Log4Bio.Error("DeleteMarks - Error=" + msgerr);
                    ret = -2;
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Excp Error desde Device [" + ex.Message + "]";
                Log4Bio.Error("DeleteMarks Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("DeleteMarks OUT! ret => " + ret.ToString());
            return ret;
        }

        

#endregion Admin Mark


#region Operacion

        /// <summary>
        /// Manda a abrir el rele desde remoto. 
        /// </summary>
        /// <returns></returns>
        public int OpenRele()
        {
            int ret = 0;
            Result result = null;
            try
            {
                Log4Bio.Debug("OpenRele IN...");
                HelperAPIRest.HRequest(Protocol + "://" + IP + ":" + Port + "/device/openDoorControl?pass=" +
                                               Password + "&type=1&content=1",
                                               Method.POST, out result);

                if (result != null)
                {
                    ret = (result.success && result.result == 1) ? 0 : -2;
                }
                else
                {
                    ret = -3;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                Log4Bio.Error("OpenRele Excp Error: " + ex.Message);
            }
            Log4Bio.Debug("OpenRele OUT! ret = " + ret.ToString());
            return ret;
        }

        //public int SetConfig(Config config, out string msgerr)
        //{
        //    int ret = 0;
        //    Result result = null;
        //    qdeleted = 0;
        //    msgerr = "";
        //    try
        //    {
        //        Log4Bio.Debug("SetConfig IN...");
        //        Log4Bio.Debug("SetConfig params = " + param.ToString());
        //        Dictionary<string, object> parametros = new Dictionary<string, object>();
        //        parametros.Add("pass", Password);
        //        parametros.Add("personId", param.personId);
        //        parametros.Add("startTime", param.startTime);
        //        parametros.Add("endTime", param.endTime);
        //        parametros.Add("model", param.model);

        //        Log4Bio.Debug("GetMarks - Call HRequestForm..:");
        //        ret = HelperAPIRest.HRequestForm(Protocol + "://" + IP + ":" + Port +
        //                                    "/newDeleteRecords", parametros,
        //                                       Method.POST, out result);

        //        if (result != null && result.code.Equals(Code.CODE_OK_000))
        //        {
        //            Log4Bio.Debug("DeleteMarks - Parseando cantidad de marcas eliminadas...");
        //            if (result.data != null)
        //            {
        //                //string[] adata = ((string)result.data).Split(':');
        //                //if (adata != null && adata.Length>1)
        //                //{
        //                qdeleted = GetNumberFromChineseString((string)result.data);
        //                //}
        //                Log4Bio.Debug("DeleteMarks - Cantidad marcas eliminadas => " + qdeleted.ToString());
        //            }
        //            else
        //            {
        //                Log4Bio.Debug("DeleteMarks - result nulo...");
        //            }
        //            ret = 0;
        //        }
        //        else
        //        {
        //            msgerr = "Error desde Device [result = " + result.code + "|msg=" + result.msg + "]";
        //            Log4Bio.Error("DeleteMarks - Error=" + msgerr);
        //            ret = -2;
        //        }
        //        ret = 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = -1;
        //        msgerr = "Excp Error desde Device [" + ex.Message + "]";
        //        Log4Bio.Error("DeleteMarks Excp Error: " + ex.Message);
        //    }
        //    Log4Bio.Debug("DeleteMarks OUT! ret => " + ret.ToString());
        //    return ret;
        //}

        #endregion Operacion

        private int GetNumberFromChineseString(string sparamin)
        {
            int ret = 0;
            try
            {
                Log4Bio.Debug("GetNumberFromChineseString IN => sparimin = " + sparamin);
                string s = Encoding.UTF8.GetString(ASCIIEncoding.ASCII.GetBytes(sparamin));
                Log4Bio.Debug("GetNumberFromChineseString - UTF8 => " + s);
                bool isDigit = false;
                string sout = "";
                string sc;
                int auxi = -1;
                foreach (char item in s)
                {
                    auxi = -1;
                    isDigit = false;
                    sc = item.ToString();
                    try
                    {
                        auxi = Convert.ToInt16(sc);
                    }
                    catch (Exception ex)
                    {
                        auxi = -1;
                    }
                    if (auxi > 0)
                    {
                        isDigit = true;
                    }
                    if (isDigit)
                    {
                        sout = sout + sc.ToString();
                    }
                }
                sout = sout.Trim();
                ret = Convert.ToInt32(sout);
            }
            catch (Exception ex)
            {
                Log4Bio.Error("GetNumberFromChineseString Excp Error: " + ex.Message);
                ret = 0;
            }
            Log4Bio.Debug("GetNumberFromChineseString OUT! => ret = " + ret.ToString());
            return ret;
        }
    }
}
