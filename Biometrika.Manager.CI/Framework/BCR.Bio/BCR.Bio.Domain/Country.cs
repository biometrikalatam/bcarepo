﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Tabla de dominio que hace referencia a la tabla Country
    /// </summary>
    public class Country : AbstractEntityItem<int>, ICountry
    {
        public virtual IList<Dept> Depts { get; set; }

        public Country()
        {
            Depts = new List<Dept>();
        }

        public virtual string Alpha2 { get; set; }
        /// <summary>
        /// Alpha3 es la sigla que le corresponde a cada pais bajo la norma ISO3166-1
        /// </summary>
        public virtual string Alpha3 { get; set; }
        /// <summary>
        /// Codigo numerico de cada pais en base a ISO3166-1
        /// </summary>
        public virtual string NumericCode { get; set; }
        public virtual string CountryRawName { get; set; }
    }
    
}
