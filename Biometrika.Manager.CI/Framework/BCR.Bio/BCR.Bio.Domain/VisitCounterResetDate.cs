﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class VisitCounterResetDate : AbstractEntity, IVisitCounterResetDate
    {
        public virtual DateTime ResetDate { get; set; }
        public virtual Company Company { get; set; }
    }
}
