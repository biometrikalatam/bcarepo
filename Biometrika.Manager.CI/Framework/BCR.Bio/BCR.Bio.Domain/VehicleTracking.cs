﻿using BCR.Bio.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class VehicleTracking : AbstractEntity
    {
        public virtual string NumberPlate { get; set; }
        public virtual DateTime RecognitionTime { get; set; }
        public virtual float? Confidence { get; set; }
        public virtual Vehicle Vehicle { get; set; }
        public virtual bool InBlacklist { get; set; }
        public virtual DateTime? FileTime { get; set; }
        public virtual bool? IsOpen { get; set; }
        public virtual DateTime? OpenTime { get; set; }
        public virtual Point Point { get; set; }
        public virtual Company Company { get; set; }

        public virtual string Tipo
        {
            get
            {
                string res = "Visita";
                if (Vehicle != null && Vehicle.Residents != null && Vehicle.Residents.Any())
                    res = "Residente";
                return res;
            }
        }
    }
}
