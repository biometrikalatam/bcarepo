﻿using BCR.Bio.Domain.Abstract;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class Zone : AbstractEntity
    {

        public Zone() { }

        public Zone(int id)
        {
            Id = id;
        }

        public virtual string Description { get; set; }

        public virtual HQ HQ { get; set; }

        public virtual IList<Building> Buildings { get; set; }
        public virtual bool HaveBuildings
        {
            get
            {
                return Buildings != null ? Buildings.Count > 0 : false;
            }
        }

        public virtual IList<Unity> Unitys { get; set; }
        public virtual bool HaveUnitys
        {
            get
            {
                //return Unitys ?? Unitys.Count > 0;
                return Unitys != null ? Unitys.Count > 0 : false;
            }
        }
    }
}