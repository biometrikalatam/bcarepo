﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;

namespace BCR.Bio.Domain
{
    public class TrackingAction : AbstractEntityItem<int>, ITrackingAction
    {
        private ITrackingAction action;
        public TrackingAction( )
        {
           
        }
        public TrackingAction(int actionId)
        {
            Id = actionId;
        }

        public virtual string Label { get; set; }
        public virtual int Value { get; set; }
    }
}