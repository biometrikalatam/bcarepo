﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class Department : AbstractEntity, IDepartmet
    {
        public virtual Company company { get; set; }
        
        public virtual string description { get; set; }

        public virtual IList<Employee> Employees { get; set; }

    }
}
