﻿using Bio.Core.Api;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BCR.Bio.Domain
{
    [XmlRoot("ArrayOfBpBirApi")]
    public class BpBirApiList
    {
        public BpBirApiList() { Items = new List<BpBirApi>(); }
        [XmlElement("BpBirApi")]
        public List<BpBirApi> Items { get; set; }
    }
}
