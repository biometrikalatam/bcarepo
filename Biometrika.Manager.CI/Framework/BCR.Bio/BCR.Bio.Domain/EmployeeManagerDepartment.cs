﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class EmployeeManagerDepartment : AbstractEntity, IEmployeeManagerDepartment
    {
        public virtual Department department { get; set; }
        

        public virtual Employee employee { get; set; }
        
    }
}
