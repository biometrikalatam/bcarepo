﻿using BCR.Bio.Domain.Abstract;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BCR.Bio.Domain
{
    public class Unity : AbstractEntity
    {
        [Display(Name = "Descripción")]
        public virtual string Description { get; set; }

        [Display(Name = "Dirección")]
        public virtual string Address { get; set; }

        public virtual Building Building { get; set; }

        public virtual Zone Zone { get; set; }

        public virtual IList<Resident> Residents { get; set; }

        public virtual bool HaveResidents
        {
            get
            {
                return Residents != null ? Residents.Count > 0 : false;
            }
        }

        //public virtual IList<Visitor> Visitors { get; set; }

        //public virtual bool HaveVisitors
        //{
        //    get
        //    {
        //        return Visitors != null ? Visitors.Count > 0 : false;
        //    }
        //}

        public virtual IList<Employee> Employees { get; set; }

        //public 
    }
}