﻿using BCR.Bio.Domain.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class AccessControl : AbstractEntity
    {
        public AccessControl()
        {
        }

        public AccessControl(int idAccessControl)
        {
            this.Id = idAccessControl;
        }

        public virtual PointRule PointRule { get; set; }

        public virtual RuleScope RuleScope { get; set; }

        public virtual string Description { get; set; }

        public virtual Company Company { get; set; }

        public virtual IList<Point> Points { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual int MinLimit { get; set; }
        public virtual int MaxLimit { get; set; }
        public virtual int ControlValue { get; set; }
    }
}