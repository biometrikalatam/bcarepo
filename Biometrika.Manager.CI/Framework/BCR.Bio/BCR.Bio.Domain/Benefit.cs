﻿/***
 *Clase que permite manejar los beneficios asociados a una empresa.
 * Ejemplo de beneficio es el derecho a casino.
 * Ese benficio dependerá si la empresa tiene ese beneficio o no. 
 ***/
using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;

namespace BCR.Bio.Domain
{
    public class Benefit : AbstractEntity, IBenefit
    {
        public virtual ICompany Company { get; set; }
        
        public virtual string Description { get; set; }

        public virtual int Modulo { get; set; }

        public virtual IList<BenefitOption> BenefitOption { get; set; }

    }
}
