﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;

namespace BCR.Bio.Domain
{
    public class TrackingResult : AbstractEntityItem<int>, ITrackingResult
    {
        public TrackingResult()
        {
        }

        public TrackingResult(int id)
        {
            Id = id;
        }
    }
}