﻿using BCR.Bio.Domain.Abstract;
using System;

namespace BCR.Bio.Domain
{
    public class VisitSchedule : AbstractEntity
    {
        public virtual Visitor Visitor { get; set; }
        public virtual DateTime EventDate { get; set; }
        public virtual DateTime StartTime { get; set; }
        public virtual DateTime EndTime { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual bool Used { get; set; }
        public virtual Unity Unity { get; set; }
        public virtual Company Company { get; set; }
    }
}
