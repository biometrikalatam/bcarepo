﻿using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Abstract
{
    public abstract class AbstractEntityItem<T> : IEntityItem<T>
    {
        public virtual string Description
        {
            get;
            set;
        }

        public virtual  T Id
        {
            get;
            set;
        }


    }
}
