﻿using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCR.Bio.Domain.Abstract
{
    /// <summary>
    /// Contiene las propiedades basicas minimas de el 99% de las tablas del modelo
    /// </summary>
    public abstract class AbstractEntity :IEntity
    {
        /// <summary>
        /// Id de la tabla referenciada
        /// </summary>
        public virtual int Id
        {
            get;
            set;
        }

        public virtual DateTime? CreateDate    // este no debería ser opcional,  ver explicacion (*)
        {
            get;
            set;
        }

        public virtual DateTime? UpdateDate
        {
            get;
            set;
        }

        
         
    }
}


/*
 (*)
 Si el campo datetime es obligatorio, cuando nhibernate construye la clase, la popula con una fecha tomada de .net Datetime.MinValue, 
 * esa fecha es  01/01/1001 que para SQL es un error. 
 * Si el campo no sufre cambios, nhibernate no produce error, pero si se pasa el campo a un model  , aunque este null, nhibernate lo 
 * completa con 01/01/1001, luego cuando lo va a persistir entiende que la fecha de creación cambió , y da error
 * 
 * 
 * 
 * 
 */