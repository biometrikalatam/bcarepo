﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class BenefitOption : AbstractEntity, IBenefitOption
    {
        public virtual int Activo { get; set; }
        
        public virtual Benefit Benefit { get; set; }
        
        public virtual String Description { get; set; }
        
    }
}
