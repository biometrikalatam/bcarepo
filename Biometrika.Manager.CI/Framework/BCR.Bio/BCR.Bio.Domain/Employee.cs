﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using NHibernate;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class Employee : AbstractEntity, IEmployee
    {
        public virtual BpIdentity Identity { get; set; }

        public virtual IUser User { get; set; }

        public virtual CentroDeCostos CentroCosto { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual IList<BenefitOption> BenefitOption { get; set; }

        public virtual IList<EmployeeOptionBenefit> EmployeeOptionBenefit { get; set; }

        public virtual Work Work { get; set; }
        
        public virtual Department Department {get;set;}

        public virtual IList<RegisterMethod> RegisterMethod { get; set; }

        public virtual IList<EmployeeRegisterMethod> EmployeeRegisterMethod { get; set; }

        public virtual int TimeAssistance { get; set; }

        public virtual DateTime? TemporalDateTicket { get; set; }
        public virtual bool Autorizador { get; set; }
        public virtual IList<Unity> Unitys { get; set; }
    }
}