﻿using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class EmployeeRegisterMethod
    {
        public EmployeeRegisterMethod()
        {

        }

        public EmployeeRegisterMethod(IEmployee Employee, IRegisterMethod RegisterMethod)
        {
            employee = Employee;
            registermethod = RegisterMethod;
        }
       

        public virtual int Id { get; set; }

        public virtual IEmployee employee { get; set; }

        public virtual IRegisterMethod registermethod { get; set; }

    }
}
