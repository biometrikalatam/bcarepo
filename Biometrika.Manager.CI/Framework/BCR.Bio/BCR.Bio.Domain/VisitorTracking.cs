﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;

namespace BCR.Bio.Domain
{
    public class VisitorTracking : AbstractEntity, IVisitorTracking
    {
        public VisitorTracking() { }
        public VisitorTracking(int id)
        {
            Id = id;
        }

        public VisitorTracking(string trackingGuid)
        {
            TrackingGuid = trackingGuid;
        }

        public virtual ICompany Company { get; set; }

        public virtual ITrackingAction Action { get; set; }

        public virtual string Comment { get; set; }

        public virtual DateTime EventTime { get; set; }

        public virtual IPoint Point { get; set; }

        public virtual IResident Resident { get; set; }

        public virtual ITrackingResult Result { get; set; }

        public virtual IVisitor Visitor { get; set; }

        public virtual IVehicle Vehicle { get; set; }

        public virtual IEmployee Employee { get; set; }
        public virtual IUser User { get; set; }

        public virtual string TrackingGuid { get; set; }

        public virtual Sector Sector { get; set; }

        public virtual MarkType MarkType { get; set; }

        public virtual HQ HQ { get; set; }

        public virtual Zone Zone { get; set; }

        public virtual Building Building { get; set; }

        public virtual Unity Unity { get; set; }

        public virtual Event Event { get; set; }

        public virtual Provider Provider { get; set; }

        public virtual EventType EventType { get; set; }

        public virtual EventClass EventClass { get; set; }
    }
}