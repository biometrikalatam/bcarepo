﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Clase de dominio que hace referencia a la tabla bp_company
    /// </summary>
    public class Company : AbstractEntity, ICompany
    {
        public Company() { }

        public Company(int idTemp)
        {
            Id = idTemp;
        }
        /// <summary>
        /// Nombre
        /// </summary>
        public virtual string Name { get; set; }
        /// <summary>
        /// Nombre de dominio
        /// </summary>
        public virtual string Domain { get; set; }
        /// <summary>
        /// Identificador fiscal
        /// </summary>
        public virtual string Rut { get; set; }
        /// <summary>
        /// Dirección
        /// </summary>
        public virtual string Address { get; set; }

        public virtual string Phone { get; set; }

        public virtual string Phone2 { get; set; }

        public virtual string Fax { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual string AdditionalData { get; set; }

        public virtual string ContactName { get; set; }

        public virtual bool Status { get; set; }

        public virtual int Holding { get; set; }
        /// <summary>
        /// Hace referencia a los HQ asociados de la compañia, se referencia por medio de HQ.company
        /// </summary>
        public virtual IList<HQ> HQs { get; set; }
        /// <summary>
        /// Hace referencia a los Sector asociados a la compañia, se referencia por medio de la tabla intermedia CompanySector
        /// por su campo company (CompanySector.company)
        /// </summary>
        public virtual IList<Sector> Sectors { get; set; }
        /// <summary>
        /// Hace referencia a los User asociaddos a la compañia, se referencia por medio de la tabla intermedia CompanyUser
        /// por su campo company (CompanyUser.company)
        /// </summary>
        public virtual IList<User> Users { get; set; }

        public virtual bool HaveSectors { get; set; }

        public virtual bool HaveHQs { get; set; }

        public virtual Boolean IsActive
        {
            get
            {
                return EndDate == null;
            }
        }

        public virtual bool IsDisable
        {
            get
            {
                return EndDate != null;
            }
        }

        public virtual string AccessName { get; set; }
        public virtual string SecretKey { get; set; }
    }
}