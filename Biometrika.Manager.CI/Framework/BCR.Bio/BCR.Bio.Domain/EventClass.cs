﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Clase de dominio que hace referencia a la tabla EventClass, 
    /// es utilizada mayormente para contener los motivos de un acceso de visitas
    /// </summary>
    public class EventClass : AbstractEntityItem<int>
    {
        //public virtual string Description { get; set; }
        public virtual ICompany Company { get; set; }
    }
}