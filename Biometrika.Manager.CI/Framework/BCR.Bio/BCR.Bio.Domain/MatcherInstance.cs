﻿using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Define una estrucura para levantar los matcher configurados y crea las clases
    /// para los posteriores clones cuando se deba usar.
    /// </summary>
    [Serializable]
    public class MatcherInstance
    {
        public MatcherInstance(string name, int authenticationfactor, int minutiaetype,
                                 double thresholdextract, double thresholdmatching,
                                 string assembly, string parameters)
        {
            Name = name;
            Authenticationfactor = authenticationfactor;
            Minutiaetype = minutiaetype;
            Thresholdextract = thresholdextract;
            Thresholdmatching = thresholdmatching;
            Assembly = assembly;
            Parameters = parameters;
        }

     

        ///<summary>
        ///</summary>
        public string Name
        {
            get;
            set;
        }

        ///<summary>
        ///</summary>
        public int Authenticationfactor
        {
            get;
            set;
        }

        ///<summary>
        ///</summary>
        public int Minutiaetype
        {
            get;
            set;
        }

        ///<summary>
        ///</summary>
        public double Thresholdextract
        {
            get;
            set;
        }

        ///<summary>
        ///</summary>
        public double Thresholdmatching
        {
            get;
            set;
        }

        ///<summary>
        ///</summary>
        public string Assembly
        {
            get;
            set;
        }

        ///<summary>
        ///</summary>
        public string Parameters
        {
            get;
            set;
        }

        ///<summary>
        ///</summary>
        public IMatcher Matcher
        {
            get;
            set;
        }

        ///<summary>
        ///</summary>
        public IExtractor Extractor
        {
            get;
            set;
        }

        ///<summary>
        ///</summary>
        public ITemplate Template
        {
            get;
            set;
        }
    }
}
