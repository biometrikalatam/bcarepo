﻿using System;
namespace BCR.Bio.Domain
{
    ///<summary>
    /// Items de DynamicData
    ///</summary>
    public class DynamicDataItem
    {
        //private string _key;
        //private string _value;
        ///// <summary>
        /////  Ctor for serialization 
        ///// </summary>
        //public DynamicDataItem()
        //{

        //}

        //public DynamicDataItem(string pkey, string pvalue)
        //{
        //    _key = pkey;
        //    _value = pvalue;
        //}

        /////<summary>
        /////</summary>
        //public string Key
        //{
        //    get { return _key; }
        //    set { _key = value; }
        //}

        /////<summary>
        /////</summary>
        //public string Value
        //{
        //    get { return _value; }
        //    set { _value = value; }
        //}
        public DynamicDataItem()
        {
        }


        public DynamicDataItem(string pkey, string pvalue)
        {
            _key = pkey;
            _value = pvalue;
        }

        #region Private

        string _key;
        string _value;


        #endregion Private

        #region Public

        ///<summary>
        ///</summary>
        public string key
        {
            get { return _key; }
            set { _key = value; }
        }
        ///<summary>
        ///</summary>
        public string value
        {
            get { return _value; }
            set { _value = value; }
        }

        #endregion Public
    }
}