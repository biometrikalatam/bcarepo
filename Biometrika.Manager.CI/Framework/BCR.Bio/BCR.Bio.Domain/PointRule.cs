﻿using BCR.Bio.Domain.Abstract;

namespace BCR.Bio.Domain
{
    public class PointRule : AbstractEntityItem<int>
    {
        public PointRule()
        {
        }

        public PointRule(int id)
        {
            this.Id = id;
        }

        public virtual string Label { get; set; }
    }
}