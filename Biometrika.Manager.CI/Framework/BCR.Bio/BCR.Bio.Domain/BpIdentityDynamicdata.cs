using BCR.Bio.Domain.Abstract;
using System;
using System.ComponentModel;

//using log4net;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// BpBir object for NHibernate mapped table 'bp_bir'.
    /// </summary>
    [Serializable]
    public class BpIdentityDynamicdata : AbstractEntity
    {
        //private static readonly ILog LOG = LogManager.GetLogger(typeof(BpIdentityDynamicdata));

        #region Member Variables

        //protected int _id;
        protected BpIdentity _bpidentity;

        protected string _ddkey;
        protected string _ddvalue;
        protected DateTime _creation;
        protected DateTime _lastupdate;


        #endregion Member Variables

        #region Constructors

        public BpIdentityDynamicdata()
        {
        }

        public BpIdentityDynamicdata(BpIdentity bpidentity, string ddkey, string ddvalue)
        {
            this._bpidentity = bpidentity;
            this._ddkey = ddkey;
            this._ddvalue = ddvalue;
        }

        public BpIdentityDynamicdata(BpIdentity bpidentity, string ddkey, string ddvalue, DateTime creation, DateTime update)
        {
            this._bpidentity = bpidentity;
            this._ddkey = ddkey;
            this._ddvalue = ddvalue;
            this._creation = creation;
            this._lastupdate = update;
        }

        #endregion Constructors

        #region Public Properties

        //public virtual int Id
        //{
        //    get { return _id; }
        //    set { if (value != this._id) { _id = value;  } }
        //}

        public virtual BpIdentity BpIdentity
        {
            get { return _bpidentity; }
            set { _bpidentity = value; }
        }

        public virtual string DDKey
        {
            get { return _ddkey; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 50 characters");
                if (value != this._ddkey) { _ddkey = value; }
            }
        }

        public virtual string DDValue
        {
            get { return _ddvalue; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 2147483647 characters");
                if (value != this._ddvalue) { _ddvalue = value; }
            }
        }

        public virtual DateTime Creation
        {
            get { return _creation; }
            set { if (value != this._creation) { _creation = value; } }
        }

        public virtual DateTime LastUpdate
        {
            get { return _lastupdate; }
            set { if (value != this._lastupdate) { _lastupdate = value; } }
        }

        #endregion Public Properties
    }
}