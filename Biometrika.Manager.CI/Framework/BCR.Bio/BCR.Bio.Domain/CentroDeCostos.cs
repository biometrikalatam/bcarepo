﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class CentroDeCostos : AbstractEntity, ICentroDeCostos
    {
        public virtual ICompany Company { get; set; }

        public virtual string Description { get; set; }
        public virtual int Valor { get; set; }
    }
}
