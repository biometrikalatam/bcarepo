﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace BCR.Bio.Domain
{
    //public class DynamicData
    //{
    //    //private static readonly ILog LOG = LogManager.GetLogger(typeof(DynamicData));

    //    public List<DynamicDataItem> DynamicDataItems;

    //    #region Operations

    //    /// <summary>
    //    /// Retorna la lista de items de key/value
    //    /// </summary>
    //    /// <returns>Lista de datos dinamicos</returns>
    //    public List<DynamicDataItem> GetItems()
    //    {
    //        return DynamicDataItems;
    //    }

    //    /// <summary>
    //    /// Dada una clave, retorna el valor si existe la clave. sino nulll
    //    /// </summary>
    //    /// <param name="key">Clave para buscar</param>
    //    /// <returns>Objeto con el valor del parametro</returns>
    //    public object GetValue(string key)
    //    {
    //        object oRes = null;

    //        try
    //        {
    //            if (DynamicDataItems == null || DynamicDataItems.Count == 0)
    //                return null;

    //            for (var i = 0; i < DynamicDataItems.Count; i++)
    //            {
    //                if (key == DynamicDataItems[i].Key)
    //                {
    //                    oRes = DynamicDataItems[i].Value;
    //                    break;
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            //LOG.Error("Bio.Core.Api.DynamicData.GetValue", ex);
    //        }

    //        return oRes;
    //    }

    //    /// <summary>
    //    /// Setea un valor en una clave dada
    //    /// </summary>
    //    /// <param name="key">Clave para actualizar</param>
    //    /// <param name="value">Valor del item a actualizar</param>
    //    /// <returns>True o false si actualizo bien o mal respectivamente</returns>
    //    public bool SetValue(string key, string value)
    //    {
    //        bool bRes = false;
    //        try
    //        {
    //            if (DynamicDataItems == null || DynamicDataItems.Count == 0)
    //                return false;

    //            for (int i = 0; i < DynamicDataItems.Count; i++)
    //            {
    //                if (key == (DynamicDataItems[i]).Key)
    //                {
    //                    DynamicDataItems[i].Value = value;
    //                    bRes = true;
    //                    break;
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            //LOG.Error("Bio.Core.Api.DynamicData.SetValue", ex);
    //        }

    //        return bRes;
    //    }

    //    /// <summary>
    //    /// Agrega un valor en una clave dada
    //    /// </summary>
    //    /// <param name="key">Clave para agregar</param>
    //    /// <param name="value">Valor del item a agregar</param>
    //    /// <returns>True o false si agrego bien o mal respectivamente</returns>
    //    public bool AddValue(string key, string value)
    //    {
    //        bool bRes = false;
    //        try
    //        {
    //            if (DynamicDataItems == null)
    //                DynamicDataItems = new List<DynamicDataItem>();

    //            DynamicDataItems.Add(new DynamicDataItem(key, value));
    //            bRes = true;
    //        }
    //        catch (Exception ex)
    //        {
    //            //LOG.Error("Bio.Core.Api.DynamicData.AddValue", ex);
    //        }

    //        return bRes;
    //    }

    //    #endregion Operations

    //    #region Serialize

    //    /// <summary>
    //    /// Serializa un objeto DynamicData a disco
    //    /// </summary>
    //    /// <param name="ddata">DynamicData a serializar</param>
    //    /// <param name="path">Path absoluto donde serializar</param>
    //    /// <returns>true o false en exito y fracaso respectivamente</returns>
    //    public static bool SerializeToFile(DynamicData ddata, string path)
    //    {
    //        bool bRes = false;

    //        try
    //        {
    //            XmlSerializer serializer = new XmlSerializer(typeof(DynamicData));
    //            TextWriter writer = new StreamWriter(path);
    //            serializer.Serialize(writer, ddata);
    //            writer.Close();
    //            bRes = true;
    //        }
    //        catch (Exception ex)
    //        {
    //            //LOG.Error("Bio.Core.Api.DynamicData.SaveToFile", ex);
    //        }
    //        return bRes;
    //    }

    //    /// <summary>
    //    /// Deserializa un objeto Parameters desde disco
    //    /// </summary>
    //    /// <param name="path">Path absoluto donde serializar</param>
    //    /// <returns>Objeto deserializado o null de haber error</returns>
    //    public static DynamicData DeserializeFromFile(string path)
    //    {
    //        try
    //        {
    //            XmlSerializer serializer = new XmlSerializer(typeof(DynamicData));
    //            TextReader reader = new StreamReader(path);
    //            DynamicData cf = (DynamicData)serializer.Deserialize(reader);
    //            reader.Close();
    //            return cf;
    //        }
    //        catch (Exception ex)
    //        {
    //            //LOG.Error("Bio.Core.Api.DynamicData.SaveToFile", ex);
    //        }
    //        return null;
    //    }

    //    /// <summary>
    //    /// Serializa un objeto DynamicData y lo devuelve en un string
    //    /// </summary>
    //    /// <param name="ddata">DynamicData a serializar</param>
    //    /// <returns>true o false en exito y fracaso respectivamente</returns>
    //    public static string SerializeToXml(DynamicData ddata)
    //    {
    //        string xmlRes = null;

    //        try
    //        {
    //            MemoryStream memoryStream = new MemoryStream();
    //            XmlSerializer xs = new XmlSerializer(ddata.GetType()); //typeof(T));
    //            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
    //            xs.Serialize(xmlTextWriter, ddata);
    //            memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
    //            xmlRes = UTF8ByteArrayToString(memoryStream.ToArray());
    //        }
    //        catch (Exception ex)
    //        {
    //            //LOG.Error("Bio.Core.Api.DynamicData.SerializeToXml", ex);
    //        }
    //        return xmlRes;
    //    }

    //    /// <summary>
    //    /// Serializa un objeto Parameters y lo devuelve en un string
    //    /// </summary>
    //    /// <param name="xmlddata">Xml del objeto a deserializar</param>
    //    /// <returns>true o false en exito y fracaso respectivamente</returns>
    //    public static DynamicData DeserializeFromXml(string xmlddata)
    //    {
    //        DynamicData olRes = null;
    //        XmlSerializer xs;
    //        try
    //        {
    //            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
    //            ns.Add("", "");
    //            xs = new XmlSerializer(typeof(DynamicData), "");
    //            MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(xmlddata));
    //            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
    //            olRes = (DynamicData)xs.Deserialize(memoryStream);
    //        }
    //        catch (Exception ex)
    //        {
    //            //LOG.Error("Bio.Core.Api.DynamicData.DeserializeFromXml", ex);
    //        }
    //        return olRes;
    //    }

    //    #endregion Serialize

    //    private static Byte[] StringToUTF8ByteArray(string pXmlString)
    //    {
    //        byte[] byteArray = null;
    //        try
    //        {
    //            UTF8Encoding encoding = new UTF8Encoding();
    //            byteArray = encoding.GetBytes(pXmlString);
    //        }
    //        catch (Exception ex)
    //        {
    //            //LOG.Error("Bio.Core.Api.DynamicData.StringToUTF8ByteArray", ex);
    //        }
    //        return byteArray;
    //    }

    //    private static string UTF8ByteArrayToString(byte[] characters)
    //    {
    //        string constructedString = null;
    //        try
    //        {
    //            UTF8Encoding encoding = new UTF8Encoding();
    //            constructedString = encoding.GetString(characters);
    //        }
    //        catch (Exception ex)
    //        {
    //            //LOG.Error("Bio.Core.Api.DynamicData.UTF8ByteArrayToString", ex);
    //        }
    //        return constructedString;
    //    }
    //}
    [Serializable]
    public class DynamicData
    {
        //private static readonly ILog LOG = LogManager.GetLogger(typeof(DynamicData));

        public List<DynamicDataItem> DynamicDataItems;

        #region Operations
        /// <summary>
        /// Retorna la lista de items de key/value
        /// </summary>
        /// <returns>Lista de datos dinamicos</returns>
        public List<DynamicDataItem> GetItems()
        {
            return DynamicDataItems;
        }

        /// <summary>
        /// Dada una clave, retorna el valor si existe la clave. sino nulll
        /// </summary>
        /// <param name="key">Clave para buscar</param>
        /// <returns>Objeto con el valor del parametro</returns>
        public object GetValue(string key)
        {
            object oRes = null;

            try
            {
                if (DynamicDataItems == null || DynamicDataItems.Count == 0)
                    return null;

                for (var i = 0; i < DynamicDataItems.Count; i++)
                {
                    if (key == DynamicDataItems[i].key)
                    {
                        oRes = DynamicDataItems[i].value;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Api.DynamicData.GetValue", ex);
            }

            return oRes;
        }

        /// <summary>
        /// Setea un valor en una clave dada
        /// </summary>
        /// <param name="key">Clave para actualizar</param>
        /// <param name="value">Valor del item a actualizar</param>
        /// <returns>True o false si actualizo bien o mal respectivamente</returns>
        public bool SetValue(string key, string value)
        {
            bool bRes = false;
            try
            {
                if (DynamicDataItems == null || DynamicDataItems.Count == 0)
                    return false;

                for (int i = 0; i < DynamicDataItems.Count; i++)
                {
                    if (key == (DynamicDataItems[i]).key)
                    {
                        DynamicDataItems[i].value = value;
                        bRes = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Api.DynamicData.SetValue", ex);
            }

            return bRes;
        }

        /// <summary>
        /// Agrega un valor en una clave dada
        /// </summary>
        /// <param name="key">Clave para agregar</param>
        /// <param name="value">Valor del item a agregar</param>
        /// <returns>True o false si agrego bien o mal respectivamente</returns>
        public bool AddValue(string key, string value)
        {
            bool bRes = false;
            try
            {
                if (DynamicDataItems == null)
                    DynamicDataItems = new List<DynamicDataItem>();

                DynamicDataItems.Add(new DynamicDataItem(key, value));
                bRes = true;
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Api.DynamicData.AddValue", ex);
            }

            return bRes;
        }

        #endregion Operations

        #region Serialize

        /// <summary>
        /// Serializa un objeto DynamicData a disco
        /// </summary>
        /// <param name="ddata">DynamicData a serializar</param>
        /// <param name="path">Path absoluto donde serializar</param>
        /// <returns>true o false en exito y fracaso respectivamente</returns>
        public static bool SerializeToFile(DynamicData ddata, string path)
        {
            bool bRes = false;

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(DynamicData));
                TextWriter writer = new StreamWriter(path);
                serializer.Serialize(writer, ddata);
                writer.Close();
                bRes = true;
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Api.DynamicData.SaveToFile", ex);
            }
            return bRes;
        }

        /// <summary>
        /// Deserializa un objeto Parameters desde disco
        /// </summary>
        /// <param name="path">Path absoluto donde serializar</param>
        /// <returns>Objeto deserializado o null de haber error</returns>
        public static DynamicData DeserializeFromFile(string path)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(DynamicData));
                TextReader reader = new StreamReader(path);
                DynamicData cf = (DynamicData)serializer.Deserialize(reader);
                reader.Close();
                return cf;
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Api.DynamicData.SaveToFile", ex);
            }
            return null;
        }

        /// <summary>
        /// Serializa un objeto DynamicData y lo devuelve en un string
        /// </summary>
        /// <param name="ddata">DynamicData a serializar</param>
        /// <returns>true o false en exito y fracaso respectivamente</returns>
        public static string SerializeToXml(DynamicData ddata)
        {
            string xmlRes = null;

            try
            {
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(ddata.GetType()); //typeof(T));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xs.Serialize(xmlTextWriter, ddata);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                xmlRes = UTF8ByteArrayToString(memoryStream.ToArray());
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Api.DynamicData.SerializeToXml", ex);
            }
            return xmlRes;
        }

        /// <summary>
        /// Serializa un objeto Parameters y lo devuelve en un string
        /// </summary>
        /// <param name="xmlddata">Xml del objeto a deserializar</param>
        /// <returns>true o false en exito y fracaso respectivamente</returns>
        public static DynamicData DeserializeFromXml(string xmlddata)
        {
            DynamicData olRes = null;
            XmlSerializer xs;
            try
            {
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");
                xs = new XmlSerializer(typeof(DynamicData), "");
                MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(xmlddata));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                olRes = (DynamicData)xs.Deserialize(memoryStream);
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Api.DynamicData.DeserializeFromXml", ex);
            }
            return olRes;
        }

        #endregion Serialize

        private static Byte[] StringToUTF8ByteArray(string pXmlString)
        {
            byte[] byteArray = null;
            try
            {
                UTF8Encoding encoding = new UTF8Encoding();
                byteArray = encoding.GetBytes(pXmlString);
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Api.DynamicData.StringToUTF8ByteArray", ex);
            }
            return byteArray;
        }

        private static string UTF8ByteArrayToString(byte[] characters)
        {
            string constructedString = null;
            try
            {
                UTF8Encoding encoding = new UTF8Encoding();
                constructedString = encoding.GetString(characters);
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Api.DynamicData.UTF8ByteArrayToString", ex);
            }
            return constructedString;
        }
    }
}