﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class Voucher : AbstractEntity, IVoucher
    {
        public virtual BpIdentity Identity { get; set; }
        public virtual CentroDeCostos CentroDeCostos { get; set; }
        public virtual int Horas { get; set; }
        public virtual BpIdentity Autorizador { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual int Total { get; set; }
        public virtual Company Company { get; set; }
    }

}