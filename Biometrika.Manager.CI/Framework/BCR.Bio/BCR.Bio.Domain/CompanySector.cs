﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Clase de dominio que hace referencia a la tabla CompanySector, que es donde se relaciona Company y Sector
    /// </summary>
    public class CompanySector : AbstractEntity, ICompanySector
    {
        public virtual Sector Sector
        {
            get;
            set;
        }

        public virtual Company Company
        {
            get;
            set;
        }
    }
}
