﻿using BCR.Bio.Domain.Abstract;
using System;

namespace BCR.Bio.Domain
{
    public class HolidayAuthorized : AbstractEntity
    {
        public virtual Holiday Holiday { get; set; }
        public virtual BpIdentity Identity { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public virtual int StartTime { get; set; }
        public virtual int EndTime { get; set; }

    }
}
