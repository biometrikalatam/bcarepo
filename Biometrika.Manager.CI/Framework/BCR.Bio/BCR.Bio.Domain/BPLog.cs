﻿using BCR.Bio.Domain.Interface;

using System;

namespace BCR.Bio.Domain
{
    public class BPLog :  IBPLog
    {
        public int id { get; set; }

        public DateTime datel { get; set; }

        public string threadl { get; set; }

        public string levell { get; set; }

        public string loggerl { get; set; }

        public string messagel { get; set; }

        public string exceptionl { get; set; }
    }
}