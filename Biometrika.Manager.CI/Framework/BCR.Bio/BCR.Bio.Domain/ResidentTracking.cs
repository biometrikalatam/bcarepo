﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class ResidentTracking
    {
        public virtual int Id { get; set; }
        /// <summary>
        /// Fecha y hora de la marca
        /// </summary>
        public virtual DateTime EventTime { get; set; }
        /// <summary>
        /// Punto donde se genera la marca
        /// </summary>
        public virtual Point Point { get; set; }
        /// <summary>
        /// Tipo de marca
        /// </summary>
        public virtual TrackingAction TrackingAction { get; set; }
        /// <summary>
        /// Resident que genera la marca
        /// </summary>
        public virtual Resident Resident { get; set; }
        /// <summary>
        /// Resultado de la marca
        /// </summary>
        public virtual TrackingResult TrackingResult { get; set; }
        /// <summary>
        /// Comentario asociado a la marca
        /// </summary>
        public virtual string Comment { get; set; }
    }
}
