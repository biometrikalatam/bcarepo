﻿using BCR.Bio.Enum.API;
using BCR.Bio.Message;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BCR.Bio.Domain.API
{
    /// <summary>
    /// Clase encargada de contener las respuestas para las diferentes APIs
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ApiResponse<T>
    {
        /// <summary>
        /// Status de funcionamiento interno, indiferente de la cantidad de resultados,
        /// esto apunta a la ejecución de un proceso completo
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ApiResponseEnum Status { get; set; }

        /// <summary>
        /// De tipo genérico determinado en la instanciación, encargado de almacenar datos de respuesta
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Mensaje de ser necesario, asociado a la Ejecución/No Ejecución del proceso
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Mensaje   legible
        /// </summary>
        public string ReadableMessage
        {
            get
            {
                if (!string.IsNullOrEmpty(Message))
                    return MessageProvider.Get(Message);
                return null;
            }
        }
    }
}