﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Email
{
    public class EmailConfigData
    {
        public string From { get; set; }
        public string[] To { get; set; }
        public string Subject { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Server { get; set; }
        public int Puerto { get; set; }
        public bool EnableSsl { get; set; }
        public string Body { get; set; }
    }
}
