﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO
{
    public class VehicleVisitorTrackingResultDTO
    {
        public string Patente { get; set; }

        public string Marca { get; set; }

        public string Modelo { get; set; }

        public string Tipo { get; set; }

        public string Color { get; set; }

        public string Detalles { get; set; }

        public DateTime? EventTime { get; set; }

        public string Grupo { get; set; }

        public string ActionDescription { get; set; }

        public string ResultDescription { get; set; }

        public List<VisitorTrackingResultDTO> Integrantes { get; set; }
    }
}
