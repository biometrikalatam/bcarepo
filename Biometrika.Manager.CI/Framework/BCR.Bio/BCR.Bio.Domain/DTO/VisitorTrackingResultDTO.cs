﻿using System;

namespace BCR.Bio.Domain.DTO
{
    public class VisitorTrackingResultDTO
    {
        public int Id { get; set; }

        public string ActionDescription { get; set; }

        public string ActionResult { get; set; }

        public int ResultId { get; set; }

        public DateTime EventTime { get; set; }

        public int PointId { get; set; }
        public string PointDescription { get; set; }
        public string PointCode { get; set; }

        public string ResidentTypeId { get; set; }

        public string ResidentValueId { get; set; }

        public int ResidentId { get; set; }
        public int ResidentCompanyId { get; set; }
        public string ResidentNationality { get; set; }

        public string VisitorTypeId { get; set; }

        public string VisitorValueId { get; set; }

        public int VisitorId { get; set; }
        public int VisitorCompanyId { get; set; }
        public DateTime? VisitorBirthDate { get; set; }
        public string VisitorSex { get; set; }
        public string VisitorNationality { get; set; }

        public int EmployeeId { get; set; }
        public string EmployeeDescription { get; set; }

        public string PlateNumber { get; set; }
        public int VehicleId { get; set; }
        public string VMarca { get; set; }
        public string VModelo { get; set; }
        public string VTipo { get; set; }
        public string VColor { get; set; }
        public string VDetalles { get; set; }

        public string ResidentName { get; set; }

        public string ResidentPatherLastname { get; set; }

        public string ResidentMotherLastname { get; set; }

        public string ResidentContact { get; set; }
        public DateTime? ResidentBirthDate { get; set; }
        public string ResidentSex { get; set; }

        public string VisitorName { get; set; }

        public string VisitorPatherLastname { get; set; }

        public string VisitorMotherLastname { get; set; }

        public string ResidentFullName
        {
            get
            {
                return string.Concat(ResidentName, ' ', ResidentPatherLastname, ' ', ResidentMotherLastname);
            }
        }

        public string VisitorFullName
        {
            get
            {
                return string.Concat(VisitorName, ' ', VisitorPatherLastname, ' ', VisitorMotherLastname);
            }
        }

        public TimeSpan Permanencia { get; set; }

        public string TrackingGuid { get; set; }

        public bool Incluir { get; set; }

        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
        public string ProviderVisitorTrackingName { get; set; }

        public string SectorName { get; set; }
        public int TrackingActionId { get; set; }

        public string HQDescription { get; set; }
        public int HQId { get; set; }

        public Unity Unity { get; set; }

        public string EventClassDescription { get; set; }
        public int EventClassId { get; set; }
        public string Destination { get; set; }
        public int UnityId { get; set; }
        public int CompanyId { get; set; }
        public string Comentario { get; set; }
        public Event Event { get; set; }
    }
}