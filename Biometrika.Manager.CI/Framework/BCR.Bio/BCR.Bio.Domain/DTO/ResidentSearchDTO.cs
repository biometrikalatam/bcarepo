﻿using System.ComponentModel.DataAnnotations;

namespace BCR.Bio.Domain.DTO
{
    public class ResidentSearchDTO
    {
        [Display(Name = "Apellido ( Paterno o Materno)")]
        public string LastName { get; set; }

        [Display(Name = "RUT")]
        public string ValueId { get; set; }

        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Dirección")]
        [MaxLength(50), MinLength(3, ErrorMessage = "Ingrese al menos 3 letras")]
        public string Address { get; set; }

        public int Company { get; set; }

        public string TypeId { get; set; }
    }
}