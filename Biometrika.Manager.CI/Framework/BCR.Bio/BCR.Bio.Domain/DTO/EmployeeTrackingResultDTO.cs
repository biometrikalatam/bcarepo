﻿using System;

namespace BCR.Bio.Domain.DTO
{
    public class EmployeeTrackingResultDTO
    {
        public int Id { get; set; }

        public string ActionDescription { get; set; }

        public string ActionResult { get; set; }

        public int ResultId { get; set; }

        public DateTime EventTime { get; set; }

        public int PointId { get; set; }
        public string PointDescription { get; set; }
        public string PointCode { get; set; }

        public int EmployeeId { get; set; }

        public string EmployeeTypeId { get; set; }
        public string EmployeeValueId { get; set; }
        public string EmployeeName { get; set; }

        public string EmployeePatherLastname { get; set; }

        public string EmployeeMotherLastname { get; set; }

        public string EmployeeFullName
        {
            get
            {
                return string.Concat(EmployeeName, ' ', EmployeePatherLastname, ' ', EmployeeMotherLastname);
            }
        }

        public string PlateNumber { get; set; }
        public int VehicleId { get; set; }
        public string VMarca { get; set; }
        public string VModelo { get; set; }
        public string VTipo { get; set; }
        public string VColor { get; set; }
        public string VDetalles { get; set; }

        public TimeSpan Permanencia { get; set; }

        public string TrackingGuid { get; set; }

        public bool Incluir { get; set; }

        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
        public string ProviderEmployeeTrackingName { get; set; }

        public string SectorName { get; set; }
        public int TrackingActionId { get; set; }

        public string HQDescription { get; set; }
        public int HQId { get; set; }

        public int CompanyId { get; set; }
        public string Comentario { get; set; }
        public decimal Temperature { get; set; }
        public bool HasPhoto { get; set; }
    }
}
