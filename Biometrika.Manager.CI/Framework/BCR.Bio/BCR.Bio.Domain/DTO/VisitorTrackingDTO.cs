﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BCR.Bio.Domain.DTO
{
    public class VisitorTrackingDTO
    {
        public int Id { get; set; }

        public int Company { get; set; }

        public int ActionId { get; set; }

        public DateTime? EventTimeFrom { get; set; }

        public DateTime? EventTimeTo { get; set; }

        public int Point { get; set; }

        public string ResidentTypeId { get; set; }

        public string ResidentValueId { get; set; }

        public int ResultId { get; set; }

        public string VisitorTypeId { get; set; }

        public string VisitorValueId { get; set; }

        public int Employee { get; set; }

        public string PlateNumber { get; set; }

        public string TrakingGuid { get; set; }

        public Provider Provider { get; set; }

        public Paginacion Paginacion { get; set; }

        public string VisitorName { get; set; }

        public string VisitorLastName { get; set; }

        public int? VisitorId { get; set; }

        public int HQId { get; set; }
        public int EventId { get; set; }

        public int EventClassId { get; set; }
        public int UserId { get; set; }
        public int UnityId { get; set; }
        public int ResidentId { get; set; }

        public IList<int> HQIdList { get; set; }
    }
}