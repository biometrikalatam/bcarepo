﻿using BCR.Bio.Domain.DTO.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.EventClass
{
    public class EventClassFilterDTO : BaseFilter
    {
        public string Description { get; set; }
        public string DescriptionLike { get; set; }
        public IList<EventClassDTO> ResultList { get; set; }
    }
}
