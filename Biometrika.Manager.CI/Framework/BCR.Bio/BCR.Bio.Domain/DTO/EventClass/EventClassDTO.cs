﻿using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.EventClass
{
    public class EventClassDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
