﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO
{
    public class IdentityBirDTO
    {
        /// <summary>
        /// Corresponde al valor del id bir
        /// </summary>
        public Int64 Id { get; set; }


        public int EmployeeId { get; set; }

        public int IdentityId { get; set; }

        public String TypeId { get; set; }

        public String ValueId { get; set; }

        public String Name { get; set; }

        public String Patherlastname { get; set; }

        public String Motherlastname { get; set; }

        public String Template { get; set; }

        public int MinutiaeType { get; set; }

        public int AuthenticationFactor { get; set; }

        private DateTime? enddate;
        public DateTime? Enddate {
            get
            {
                return enddate;
            }
            set {
                enddate = value;
                if (enddate==DateTime.MinValue)
                {
                    enddate = null;
                }
                

            }
        }
    }
}
