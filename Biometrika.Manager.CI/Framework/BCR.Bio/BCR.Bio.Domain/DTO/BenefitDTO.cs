﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO
{
    public class BenefitDTO
    {
        public int Id { get; set; }

        public String Description { get; set; }
        
    }
}
