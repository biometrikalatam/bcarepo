﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO
{
    public class VisitorTrackingExtDTO
    {
        public string ActionDescription { get; set; }
        public string ActionLabel { get; set; }
        public string ActionResult { get; set; }
        public DateTime EventTime { get; set; }
        public int Id { get; set; }
        public string PointDescription { get; set; }
        public int VisitorId { get; set; }
        public string VisitorName { get; set; }
        public string VisitorPatherLastname { get; set; }
        public string VisitorTypeId { get; set; }
        public string VisitorValueId { get; set; }

        public string SectorName { get; set; } 
    }
}
