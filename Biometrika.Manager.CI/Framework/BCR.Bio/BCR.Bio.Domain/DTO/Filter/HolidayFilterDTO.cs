﻿
using System.Collections.Generic;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class HolidayFilterDTO
    {
        public int CompanyId { get; set; }
        public int UnityId { get; set; }
        public IList<Holiday> ResultList { get; set; }
    }
}
