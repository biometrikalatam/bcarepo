﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class ZoneFilterDTO : BaseFilter
    {
        public string Description { get; set; }
        public string DescriptionLike { get; set; }
        public string Address { get; set; }
        public string AddressLike { get; set; }
        public int HQId { get; set; }
        public IList<Zone> ResultList { get; set; }
        public bool Export { get; set; }
    }
}
