﻿using BCR.Bio.Domain.Interface;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class IdentityFilterDTO : BaseFilter
    {
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string ValueIdLike { get; set; }
        public string Name { get; set; }
        public string FatherLastName { get; set; }
        public string FatherLastNameLike { get; set; }
        public string FullApellidosLike { get; set; }
        public string MotherLastName { get; set; }
        public string FullName { get; set; }

        public int UnityId { get; set; }
        public string UnityFullAddress { get; set; }

        public int BuildingId { get; set; }
        public string BuildingDescription { get; set; }

        public int ZoneId { get; set; }
        public string ZoneDescription { get; set; }

        public CentroDeCostos CentroDeCosto { get; set; }
        public IdentityType IdentityType { get; set; }
        /// <summary>
        /// Corresponde al filtro de estado, es importante pasar el Class y el Tag a buscar, el company se asigna automaticamente en base a la misma busqueda
        /// </summary>
        public IdentityState IdentityState { get; set; }
        public int IdentityId { get; set; }
    }
}
