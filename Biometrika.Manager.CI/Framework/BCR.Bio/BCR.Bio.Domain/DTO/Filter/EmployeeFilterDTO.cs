﻿using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public enum EmployeeClass
    {
        All,
        Authorizator,
        NotAuthorizator
    }

    public class EmployeeFilterDTO : IdentityFilterDTO
    {
        public IList<Employee> ResultList { get; set; }
        public bool WithContactForm { get; set; }
        public bool WithRegisterMethod { get; set; }
        public bool WithCentroDeCosto { get; set; }
        public bool WithUser { get; set; }
        public IUser UserId { get; set; }
        public bool IsActive { get; set; }
        public int Active { get; set; }
        public bool OnlyAutorizator { get; set; }
        public Unity Unity { get; set; }
        public List<HQ> HqList { get; set; }
        public EmployeeClass EmployeeClass { get; set; }
        public bool Export { get; set; }
    }
}
