﻿using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class SectorFilterDTO : BaseFilter, ISector
    {
        public IList<Company> Companies { get; set; }

        public string ContactName { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Description { get; set; }
        public string DescriptionLike { get; set; }

        public DateTime? EndDate { get; set; }

        public string externalid { get; set; }

        public IList<Point> Points { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}
