﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class VisitorTrackingFilter : BaseFilter
    {
        public DateTime? Desde { get; set; }
        public DateTime? Hasta { get; set; }
        public Unity Unity { get; set; }
        public Employee EmployeeAutorizador { get; set; }
        public Resident ResidentAutorizador { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string NameLike { get; set; }
        public string FatherLastnameLike { get; set; }
        public string PlateNumber { get; set; }
        public TrackingAction TrackingAction { get; set; }
        public Domain.EventClass  EventClass { get; set; }
        public TrackingResult TrackingResult { get; set; }
        public Provider Provider { get; set; }
        public User User { get; set; }
        public Event Event { get; set; }
        public HQ Hq { get; set; }
        public List<HQ> HqList { get; set; }

        public bool WithTrackingAction { get; set; }
        public bool WithEventClass { get; set; }
        public bool WithHQ { get; set; }
        public bool WithPoint { get; set; }
        public bool WithProvider { get; set; }
        public bool WithResident { get; set; }
        public bool WithResult { get; set; }
        public bool WithVisitor { get; set; }
        public bool WithUser { get; set; }
        public bool WithVehicle { get; set; }
        public bool WithEmployee { get; set; }
        public bool WithEvent { get; set; }
        public bool WithUnity { get; set; }

        public bool Export { get; set; }
    }
}
