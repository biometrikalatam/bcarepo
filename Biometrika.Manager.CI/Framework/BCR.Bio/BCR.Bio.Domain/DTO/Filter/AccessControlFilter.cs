﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class AccessControlFilter : BaseFilter
    {
        public string Description { get; set; }
        public string DescriptionLike { get; set; }
        public int PointId { get; set; }
        public bool WithCompany { get; set; }
        public bool WithPointrule { get; set; }
        public bool WithRuleScope { get; set; }
        public string PointRuleLabel { get; set; }
        public PointRule PointRule { get; set; }
    }
}
