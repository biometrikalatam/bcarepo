﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class EventFilterDTO : BaseFilter
    {
        public EventFilterDTO(int companyId)
        {
            CompanyId = companyId;
        }

        public string Description { get; set; }
        public string DescriptionLike { get; set; }
        public int ProviderId { get; set; }
        public DateTime? StartDate { get; set; }
        public List<Event> ResultList { get; set; }

        public bool WithVisitors { get; set; }
    }
}
