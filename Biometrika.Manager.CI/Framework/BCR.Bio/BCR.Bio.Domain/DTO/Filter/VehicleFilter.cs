﻿using BCR.Bio.Domain.DTO.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class VehicleFilter : BaseFilter
    {
        public string PlateNumber { get; set; }
        public string PlateNumberLike { get; set; }
        public bool OnlyActive { get; set; }
        public IList<Provider> ProviderList { get; set; }
        public bool ForResident { get; set; }
        public bool ForVisitor { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public int ResidentId { get; set; }
        public int UnityId { get; set; }
        public int IdentityStateId { get; set; }
    }
}
