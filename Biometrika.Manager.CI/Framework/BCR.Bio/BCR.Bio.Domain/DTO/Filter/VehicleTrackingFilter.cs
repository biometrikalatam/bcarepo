﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public enum AperturaAutomaticaEnum
    {
        All,
        Si,
        No
    }

    public class VehicleTrackingFilter : BaseFilter
    {
        public DateTime InitDate { get; set; }
        public DateTime EndDate { get; set; }
        public string NumberPlateLike { get; set; }

        public int PointId { get; set; }
        public string PointName { get; set; }

        public int Tipo { get; set; }
        public string TipoName { get; set; }

        public int ResidentId { get; set; }
        public string ResidentFullName { get; set; }
        public AperturaAutomaticaEnum AperturaAutomatica { get; set; }
    }
}
