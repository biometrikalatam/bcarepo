﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class VehicleBlackListFilter : BaseFilter
    {
        public string PlateNumber { get; set; }
        public int Active { get; set; }
        public int UserIn { get; set; }
        public int UserOut { get; set; }
    }
}
