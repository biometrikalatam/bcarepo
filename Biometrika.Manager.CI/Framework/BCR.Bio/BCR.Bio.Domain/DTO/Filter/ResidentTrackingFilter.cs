﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class ResidentTrackingFilter : BaseFilter
    {
        public DateTime InitDate { get; set; }
        public DateTime EndDate { get; set; }

        public int PointId { get; set; }
        public string PointName { get; set; }

        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public int ResidentId { get; set; }
    }
}
