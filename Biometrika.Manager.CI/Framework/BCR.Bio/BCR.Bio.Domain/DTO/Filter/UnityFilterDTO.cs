﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class UnityFilterDTO : BaseFilter
    {
        public string Description { get; set; }
        public string Address { get; set; }
        public string AddressLike { get; set; }
        public int BuildingId { get; set; }
        public string BuildingDescription { get; set; }
        public int ZoneId { get; set; }
        public string ZoneDescription { get; set; }
        public int ResidentId { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeFullName { get; set; }

        public IList<Unity> ResultList { get; set; }
        public string Back { get; set; }
        public IList<HQ> HQList { get; set; }
        public bool Export { get; set; }
    }
}
