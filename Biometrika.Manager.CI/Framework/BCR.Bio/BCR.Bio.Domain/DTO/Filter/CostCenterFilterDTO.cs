﻿using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class CostCenterFilterDTO : BaseFilter, ICentroDeCostos
    {
        public ICompany Company { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Description { get; set; }
        public string DescriptionLike { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int Valor { get; set; }
    }
}
