﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public enum VisitorClassEnum
    {
        All,
        Authorized,
        Registered
    }

    public class VisitorFilterDTO : IdentityFilterDTO
    {
        public IList<Visitor> ResultList { get; set; }
        public Provider Provider { get; set; }
        public VisitorClassEnum VisitorClass { get; set; }

        public bool EagerLoading { get; set; }
        public List<HQ> HqList { get; set; }
        public bool Export { get; set; }
        public bool IsSuperAdmin { get; set; }
    }
}
