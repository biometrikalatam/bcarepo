﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class IdentityTypeFilterDTO : BaseFilter
    {
        public string Description { get; set; }
        public string DescriptionLike { get; set; }
        public string Class { get; set; }
    }
}
