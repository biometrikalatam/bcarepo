﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class ResidentFilterDTO : IdentityFilterDTO
    {
        public IList<Resident> ResultList { get; set; }
        public bool IsAuthorizator { get; set; }
        public bool WithUnity { get; set; }
        public bool WithVehicle { get; set; }
        public IList<HQ> HQList { get; set; }
        public bool Export { get; set; }
    }
}
