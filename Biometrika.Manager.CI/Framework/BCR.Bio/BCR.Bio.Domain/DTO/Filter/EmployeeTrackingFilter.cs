﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class EmployeeTrackingFilter : BaseFilter
    {
        public bool WithCompany { get; set; }
        public bool WithPoint { get; set; }
        public bool WithEmployee { get; set; }
        public bool WithVehicle { get; set; }
        public bool WithAction { get; set; }
        public bool WithResult { get; set; }
        public bool WithSector { get; set; }
        public bool WithHQ { get; set; }
        public bool WithProvider { get; set; }
        public bool WithUser { get; set; }
        public bool OnlyAssistance { get; set; }

        public DateTime? InitDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeFullName { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeePatherlastname { get; set; }
        public CentroDeCostos CentroDeCostos { get; set; }
        public string ValueId { get; set; }
        public string TypeId { get; set; }
        public int UserId { get; set; }
        public int HqId { get; set; }
        public int[] HqIds { get; set; }
        public string HqDescription { get; set; }

        public IList<HQ> HqList { get; set; }
        //public TrackingAction TrackingAction { get; set; }
    }
}
