﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class HQFilterDTO : HQ
    {
        public string DescriptionLike { get; set; }
        public List<int> HQIds { get; set; }
    }
}
