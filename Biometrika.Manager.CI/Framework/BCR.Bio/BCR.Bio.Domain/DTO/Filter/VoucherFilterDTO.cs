﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class VoucherFilterDTO : BaseFilter
    {
        public DateTime? Desde { get; set; }
        public DateTime? Hasta { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string NameLike { get; set; }
        public string FatherLastnameLike { get; set; }
        public CentroDeCostos CentroDeCosto { get; set; }
        public bool Export { get; set; }
    }
}
