﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class WorkDTO : BaseFilter
    {
        public int Company { get; set; }
        public string Description { get; set; }
        public string DescriptionLike { get; set; }

    }
}
