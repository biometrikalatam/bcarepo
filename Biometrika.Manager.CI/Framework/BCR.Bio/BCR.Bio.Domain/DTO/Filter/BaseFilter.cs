﻿using System;
using BCR.Bio.Domain.Interface;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class BaseFilter
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public Paginacion Paginacion { get; set; }
    }
}
