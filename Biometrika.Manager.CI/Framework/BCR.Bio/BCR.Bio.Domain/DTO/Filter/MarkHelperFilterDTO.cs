﻿using System;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class MarkHelperFilterDTO : BaseFilter
    {
        public long TimeStamp { get; set; }
        public string PersonId { get; set; }
        public bool Processed { get; set; }
        public int PointId { get; set; }

    }
}
