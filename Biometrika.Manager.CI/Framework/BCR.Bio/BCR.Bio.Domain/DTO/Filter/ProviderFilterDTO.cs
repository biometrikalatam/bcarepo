﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class ProviderFilterDTO : BaseFilter
    {
        public string Name { get; set; }
        public string NameLike { get; set; }
        public string IdentifierCode { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string PlateNumber { get; set; }
        public DateTime? EndDate { get; set; }
        public bool Outsourcing { get; set; }
        public Company Company { get; set; }
        public IList<Visitor> VisitorList { get; set; }
        public IList<Vehicle> VehicleList { get; set; }
        public bool OnlyActive { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
    }
}
