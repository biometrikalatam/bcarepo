﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class CountryFilterDTO : BaseFilter
    {
        public string Description { get; set; }
        public string Alpha3 { get; set; }

        public IList<Country> ResultList { get; set; }
    }
}
