﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class VisitorTrackingObjetadosFilter : BaseFilter
    {
        public DateTime Desde { get; set; }
        public DateTime Hasta { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public int ActionId { get; set; }
        public int ResultId { get; set; }

        public bool WithIdentity { get; set; }
        public bool WithAction { get; set; }
        public bool WithResult { get; set; }
        public bool WithPoint { get; set; }
    }
}
