﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class IdentityContactFormFilter : BaseFilter
    {
        public int IdentityId { get; set; }
    }
}
