﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class IdentityBlackListFilterDTO : IdentityFilterDTO
    {
        public IList<IdentityBlackList> ResultList { get; set; }
        /// <summary>
        /// 0 todos, 1 bloqueados, 2 desbloqueados
        /// </summary>
        public int Active { get; set; }
        public int UserIn { get; set; }
        public int UserOut { get; set; }
        public bool Export { get; set; }
    }
}
