﻿using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class PointFilterDTO : BaseFilter, IPoint
    {
        public string Address { get; set; }

        public string Code { get; set; }

        public string ContactName { get; set; }

        public DateTime? CreateDate { get; set; }

        public int Dev_attendancemode { get; set; }

        public int Dev_baudrates { get; set; }

        public int Dev_comport { get; set; }

        public DateTime? Dev_daterefresh { get; set; }

        public int Dev_display12 { get; set; }

        public int Dev_dooropen { get; set; }

        public string Dev_gateway { get; set; }

        public int Dev_Id { get; set; }

        public string Dev_ipserver { get; set; }

        public int Dev_modeautentication { get; set; }

        public int Dev_number { get; set; }

        public string Dev_password { get; set; }

        public int Dev_refresh { get; set; }

        public string Dev_serialnumber { get; set; }

        public int Dev_umbral { get; set; }

        public int Dev_userlength { get; set; }

        public DateTime? EndDate { get; set; }

        public int HasMail { get; set; }

        public int HasPrinter { get; set; }

        public HQ HQ { get; set; }

        public string IPaddress { get; set; }

        public bool Is_Dev { get; set; }

        public string MacAddress { get; set; }

        public string Name { get; set; }
        public string NameLike { get; set; }

        public string OtherPhone { get; set; }

        public string Phone { get; set; }

        public Sector Sector { get; set; }

        public string SensorId { get; set; }

        public DateTime? UpdateDate { get; set; }

        public void Disable()
        {
            throw new NotImplementedException();
        }

        public void Enable()
        {
            throw new NotImplementedException();
        }
    }
}
