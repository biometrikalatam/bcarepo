﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class BuildingFilterDTO : BaseFilter
    {
        public string Description { get; set; }
        public string DescriptionLike { get; set; }
        public string Address { get; set; }
        public string AddressLike { get; set; }
        public int ZoneId { get; set; }
        public string ZoneDescription { get; set; }
        public IList<Building> ResultList { get; set; }
    }
}
