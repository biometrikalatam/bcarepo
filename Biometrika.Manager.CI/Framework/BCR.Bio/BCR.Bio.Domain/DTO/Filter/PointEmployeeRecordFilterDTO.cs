﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO.Filter
{
    public class PointEmployeeRecordFilterDTO : BaseFilter
    {
        public Point Point { get; set; }
        public User User { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaTermino { get; set; }
    }
}
