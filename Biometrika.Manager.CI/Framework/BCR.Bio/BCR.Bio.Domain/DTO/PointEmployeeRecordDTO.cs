﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO
{
    public class PointEmployeeRecordDTO
    {
        public int  Id { get; set; }

        public string EmployeeName { get; set; }

        public string Username { get; set; }

        public DateTime? EndActivity { get; set; }

        public string PointName { get; set; }

        public DateTime StartActivity { get; set; }

    }
}
