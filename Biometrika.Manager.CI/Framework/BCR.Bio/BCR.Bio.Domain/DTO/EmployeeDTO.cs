﻿using BCR.Bio.Domain.Interface;
using System;

namespace BCR.Bio.Domain.DTO
{
    public class EmployeeDTO
    {
        public virtual int Id { get; set; }

        public virtual string TypeId { get; set; }

        public virtual string ValueId { get; set; }

        public virtual string Name { get; set; }

        public virtual string PatherLastname { get; set; }

        public virtual string MotherLastname { get; set; }

        public virtual string Sex { get; set; }

        public virtual DateTime? BirthDate { get; set; }

        public virtual int CompanyIdEnroll { get; set; }

        public virtual int CentrodeCostoId { get; set; }

        public virtual String CentrodeCostoNombre { get; set; }

        public virtual int DepartamentId { get; set; }

        public virtual String DepartamentNombre { get; set; }

        public virtual int WorkId { get; set; }

        public virtual String WorkNombre { get; set; }


        public string FullName { get; set; }
       
        public virtual string UserName { get; set; }

        public virtual int Identity { get; set; }

        public virtual String Phone { get; set; }

        public virtual String Phone2 { get; set; }

        public virtual String Cellphone { get; set; }

        public virtual DateTime Endate { get; set; }

        public virtual int TimeAssistance { get; set; }

        public bool IsTimeAssistance
        {
            get
            {
                if (TimeAssistance == 0)
                    return false;
                else
                    return true;
            }
        }
        
        public bool IsActive
        {
            get
            {
                if (Endate==DateTime.MinValue)
                    return true;
                else
                    return false;
            }
        }

        public int UnityId { get; set; }

        public String UnityName { get; set; }

        public string UnityAddress { get; set; }
        public bool Autorizador { get; set; }
    }
}