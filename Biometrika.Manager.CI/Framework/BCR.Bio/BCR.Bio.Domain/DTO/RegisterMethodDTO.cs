﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO
{
    public class RegisterMethodDTO
    {
        public int Id { get; set; }

        public String description { get; set; }

        public bool IsSelected { get; set; }

        public object Tags { get; set; }


    }
}
