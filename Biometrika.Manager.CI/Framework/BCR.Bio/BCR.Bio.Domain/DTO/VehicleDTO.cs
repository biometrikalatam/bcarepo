﻿using System;

namespace BCR.Bio.Domain.DTO
{
    public class VehicleDTO
    {
        public int Id { get; set; }

        public string PlateNumber { get; set; }

        public string Source { get; set; }

        public int IdentityId { get; set; }

        public string Name { get; set; }

        public string FatherLastName { get; set; }

        public string MotherLastName { get; set; }

        public string SourceDetail
        {
            get
            {
                return Resident != null ? "Residente" : "Visitante"; 
            }
        }

        public Provider Provider { get; set; }
        public Resident Resident { get; set; }
        public Visitor Visitor { get; set; }
        public IdentityState IdentityState { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}