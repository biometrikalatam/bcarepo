﻿using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.DTO
{
    public class BaseFilterParam
    {
        public ICompany Company { get; set; }
        public string TypeID { get; set; }

        public string ValueID { get; set; }

        public Provider Provider { get; set; }
        public DateTime EventTimeFrom { get; set; } = DateTime.MinValue;
        public DateTime EventTimeTo { get; set; } = DateTime.MaxValue;
        public ITrackingAction ClaimAction { get; set; }
        public int Id { get; set; }
    }
}
