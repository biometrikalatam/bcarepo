﻿using BCR.Bio.Domain.Interface;
using System.Collections.Generic;

namespace BCR.Bio.Domain.DTO
{
    public class UserDTO
    {
        public virtual int Id { get; set; }
        public virtual string Username { get; set; }

        public virtual IList<Company> Companies { get; set; }

        public virtual ICompany Company
        {
            get
            {
                if (Companies.Count >= 1)
                    return (ICompany)Companies[0];
                else
                    return null;
            }
        }
    }
}