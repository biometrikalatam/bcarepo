﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class HQ : AbstractEntity, IHQ
    {
        public HQ ()
        { }

        public HQ(int IdTemp)
        {
            Id = IdTemp;
        }

        public virtual string Description { get; set; }

        public virtual string Address { get; set; }

        public virtual string Phone { get; set; }

        public virtual string Contact { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual ICompany Company { get; set; }

        public virtual ITown Town { get; set; }

        public virtual IList<Point> Points { get; set; }
    }
}