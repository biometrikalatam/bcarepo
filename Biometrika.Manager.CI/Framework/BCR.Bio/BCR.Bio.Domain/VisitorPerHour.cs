﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class VisitorPerHour
    {
        public virtual int Id { get; set; }
        public virtual DateTime Fecha { get; set; }
        public virtual int Hora { get; set; }
        public virtual int Total { get; set; }
    }
}
