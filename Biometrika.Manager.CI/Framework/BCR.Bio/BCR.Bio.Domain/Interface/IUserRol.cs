﻿namespace BCR.Bio.Domain.Interface
{
    public interface IUserRol : IEntity
    {
        User UserId { get; set; }

        Rol RolId { get; set; }
    }
}