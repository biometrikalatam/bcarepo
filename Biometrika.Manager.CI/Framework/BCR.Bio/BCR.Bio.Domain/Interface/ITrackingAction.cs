﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Interface
{
    public interface ITrackingAction : IEntityItem<int>
    {
        string Label { get; set; }
        int Value { get; set; }
    }
}
