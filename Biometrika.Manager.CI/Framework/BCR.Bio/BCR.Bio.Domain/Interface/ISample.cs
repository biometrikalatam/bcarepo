﻿namespace BCR.Bio.Domain.Interface
{
    internal interface ISample
    {
        string Additionaldata { get; set; }

        string Data { get; set; }

        int Device { get; set; }

        int Minutiaetype { get; set; }
    }
}