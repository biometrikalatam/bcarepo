﻿using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IVoucher: IEntity
    {
        BpIdentity Identity { get; set; }
        CentroDeCostos CentroDeCostos { get; set; }
        int Horas { get; set; }
        BpIdentity Autorizador { get; set; }
        Employee Employee { get; set; }
        int Total { get; set; }
        Company Company { get; set; }
    }
}
