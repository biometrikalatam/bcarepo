﻿using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface ICompany : IEntity
    {
        string Name { get; set; }

        string Domain { get; set; }

        string Rut { get; set; }

        string Address { get; set; }

        string Phone { get; set; }

        string Phone2 { get; set; }

        string Fax { get; set; }

        DateTime? EndDate { get; set; }

        String AdditionalData { get; set; }

        string ContactName { get; set; }

        bool Status { get; set; }

        int Holding { get; set; }

        IList<HQ> HQs { get; set; }

        IList<Sector> Sectors { get; set; }

        IList<User> Users { get; set; }

        bool IsActive { get; }

        bool HaveSectors { get; set; }

        bool HaveHQs { get; set; }

        string AccessName { get; set; }

        string SecretKey { get; set; }
    }
}