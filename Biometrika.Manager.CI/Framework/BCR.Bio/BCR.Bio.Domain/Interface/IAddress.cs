﻿namespace BCR.Bio.Domain.Interface
{
    public interface IAddress
    {
        int Number { get; set; }

        string Street { get; set; }

        string PostCode { get; set; }

        ICity Ciudad { get; set; }
    }
}