﻿using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IDept : IEntityItem<int>
    {
        Country Country { get; set; }

        IList<City> Cities { get; set; }
    }
}