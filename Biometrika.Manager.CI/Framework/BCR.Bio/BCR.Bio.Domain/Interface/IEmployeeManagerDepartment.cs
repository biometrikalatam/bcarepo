﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Interface
{
    interface IEmployeeManagerDepartment: IEntity
    {
        Employee employee { get; set; }

        Department department { get; set; }

    }
}
