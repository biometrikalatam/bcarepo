﻿using System;

namespace BCR.Bio.Domain.Interface
{
    public interface IVisitorTracking : IEntity
    {
        ICompany Company { get; set; }
        DateTime EventTime { get; set; }
        IPoint Point { get; set; }
        ITrackingAction Action { get; set; }
        ITrackingResult Result { get; set; }
        IVisitor Visitor { get; set; }
        IVehicle Vehicle { get; set; }
        IResident Resident { get; set; }
        IEmployee Employee { get; set; }
        IUser User { get; set; }
        string Comment { get; set; }
        string TrackingGuid { get; set; }
    }
}