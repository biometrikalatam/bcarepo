﻿using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IBpIdentity
    {
        /// <summary>
        /// Id de la tabla referenciada
        /// </summary>
        int Id { get; set; }

        DateTime? BirthDate { get; set; }

        string BirthPlace { get; set; }

        IList<BpBir> BpBir { get; set; }

        IList<BpIdentityDynamicdata> BpIdentityDynamicdata { get; set; }

        ICompany CompanyIdEnroll { get; set; }

        DateTime Creation { get; set; }

        DateTime? DocumentExpirationDate { get; set; }

        string DocumentSeriesNumber { get; set; }

        string Dynamicdata { get; set; }

        string Enrollinfo { get; set; }

        string MotherLastname { get; set; }

        string Name { get; set; }

        string Nationality { get; set; }

        string Nick { get; set; }

        string PatherLastname { get; set; }

        string Photography { get; set; }

        string Profession { get; set; }

        string Sex { get; set; }

        string Signatureimage { get; set; }

        string TypeId { get; set; }

        int UserIdEnroll { get; set; }

        string ValueId { get; set; }

        string VerificationSource { get; set; }

        string VisaType { get; set; }

        int VisitCounter { get; set; }

        ITown Town { get; set; }

        string Phone { get; set; }
        string Phone2 { get; set; }
        string Cellphone { get; set; }
        IList<IdentityBlackList> IdentityBlackList { get; set; }
        IList<IdentityType> IdentityTypeList { get; set; }
        IList<CentroDeCostos> CentrosDeCostoList { get; set; }
        IList<IdentityState> IdentityStateList { get; set; }
    }
}