﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Interface
{
    public interface IAutorization : IEntity
    {
        string Motivo { get; set; }

        DateTime StartDate { get; set; }
        
        DateTime EndDate { get; set; }
    }
}