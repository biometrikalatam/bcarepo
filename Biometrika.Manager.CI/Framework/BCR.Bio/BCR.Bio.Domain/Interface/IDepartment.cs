﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Interface
{
    interface IDepartmet: IEntity
    {
        string description { get; set; }

        Company company { get; set; }

        IList<Employee> Employees { get; set; }
    }
}
