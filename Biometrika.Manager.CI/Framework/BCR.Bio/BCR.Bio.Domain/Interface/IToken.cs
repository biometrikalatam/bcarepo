﻿using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IToken : IDisposable
    {
        /// <summary>
        /// 1-Verify | 2-Enroll
        /// </summary>
        int OperationType { get; set; }

        /// <summary>
        /// Tecnologia a la que corresponde el template
        /// </summary>
        int AuthenticationFactor { get; set; }

        /// <summary>
        /// Tipo de minucia que contiene Data, incluidos WSQ, RAW, etc.
        /// </summary>
        int MinutiaeType { get; set; }

        ///// <summary>
        ///// Tipo de informacion (data) que contendra el template (desde BIR_TYPE)
        ///// </summary>
        //int Type { get; set; }

        /// <summary>
        /// Parte del cuerpo si es tecnologia biometrica
        /// </summary>
        int BodyPart { get; set; }

        /// <summary>
        /// [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
        /// </summary>
        int DataTypeToken { get; set; }

        /// <summary>
        /// Data propia del template o sample
        /// </summary>
        List<Sample> Samples { get; set; }

        /// <summary>
        /// Datos adicionales segun sea la data. Por ejemplo si es WSQ,
        /// se puede colocar las dimensiones de la forma w=512|h=512, o si
        /// es template NEC, el coeficiente coef=xxx. Si hay mas de uno separado
        /// por pipe "|"
        /// </summary>
        string AdditionalData { get; set; }

        /// <summary>
        /// Obtiene la Data en base 64
        /// </summary>
        string GetData { get; }

        /// <summary>
        /// Setea la data desde base 64
        /// </summary>
        string SetData { set; }

        // <summary>
        /// Tipo ID de la persona (RUT, DNI, etc)
        /// </summary>
        string Typeid { get; set; }

        // <summary>
        /// Valor ID de la persona (RUT, DNI, etc)
        /// </summary>
        string Valueid { get; set; }

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist
        /// </summary>
        string Clientid { get; set; }

        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        string Ipenduser { get; set; }

        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        string Enduser { get; set; }

        /// <summary>
        /// Extrae el contenido del token y llena las propiedades del objeto
        /// </summary>
        /// <param name="token">Token a extraer</param>
        /// <returns></returns>
        int Extract(string token);

        /// <summary>
        /// Extrae el contenido del token y llena las propiedades del objeto y ademas de llenar las prop del bjeto,
        /// setea las variable que corresponden a un objeto XmlParamIn
        /// </summary>
        /// <param name="token">token en base 64</param>
        /// <param name="paramin">objeto XmlParamIn inicial si existe sino null</param>
        /// <param name="newparamin">Objeto XmlParamIn con datos extraidos desde token</param>
        /// <returns></returns>
        int ExtractToParameters(string token, out string newparamin);
    }
}