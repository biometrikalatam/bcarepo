﻿using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IVisitor : IEntity
    {
        DateTime? EndDate { get; set; }

        BpIdentity Identity { get; set; }

        IList<Vehicle> Vehicles { get; set; }

        Provider Provider { get; set; }

        VisitorTrakingRoute TrakingRoute { get; set; }
        IList<Unity> Unitys { get; set; }

        int VisitCounter { get; set; }
    }
}