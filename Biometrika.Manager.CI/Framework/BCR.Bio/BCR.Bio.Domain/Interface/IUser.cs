﻿using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IUser : IEntity
    {
        string Username { get; set; }

        string Email { get; set; }

        string Password { get; set; }

        bool IsApproved { get; set; }

        bool IsLockedOut { get; set; }

        string PasswordSalt { get; set; }

        int PasswordFormat { get; set; }

        string PasswordQuestion { get; set; }

        string PasswordAnswer { get; set; }

        int FailedPasswordAttemptCount { get; set; }

        DateTime? FailedPasswordAttemptWindowStart { get; set; }

        int FailedPasswordAnswerAttemptCount { get; set; }

        DateTime? FailedPasswordAnswerAttemptWindowStart { get; set; }

        DateTime? LastPasswordChangedDate { get; set; }

        DateTime? LastActivityDate { get; set; }

        DateTime? LastLockOutDate { get; set; }

        DateTime? LastLoginDate { get; set; }

        string Comments { get; set; }

        string Name { get; set; }

        string Surname { get; set; }

        string Phone { get; set; }

        string PostalCode { get; set; }

        IList<Company> Companies { get; set; }

        IList<Rol> Roles { get; set; }

        ICompany Company { get; }

        Employee Employee { get; set; }
    }
}