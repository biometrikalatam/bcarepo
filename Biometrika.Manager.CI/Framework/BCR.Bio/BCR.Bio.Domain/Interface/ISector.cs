﻿using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface ISector : IEntity
    {
        string Description { get; set; }

        string externalid { get; set; }

        string ContactName { get; set; }

        DateTime? EndDate { get; set; }

        IList<Point> Points { get; set; }

        IList<Company> Companies { get; set; }
    }
}