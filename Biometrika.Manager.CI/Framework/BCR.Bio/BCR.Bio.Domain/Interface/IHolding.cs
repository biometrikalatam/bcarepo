﻿using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IHolding : IEntity
    {
        string Name { get; set; }

        string Address { get; set; }

        DateTime? Endate { get; set; }

        String AdditionalData { get; set; }

        IList<Company> Companies { get; set; }
    }
}