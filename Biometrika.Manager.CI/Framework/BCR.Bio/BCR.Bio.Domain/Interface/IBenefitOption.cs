﻿using BCR.Bio.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Interface
{
    public interface IBenefitOption : IEntity
    {
        string Description { get; set; }

        int Activo { get; set; }

        Benefit Benefit { get; set; }

    }
}
