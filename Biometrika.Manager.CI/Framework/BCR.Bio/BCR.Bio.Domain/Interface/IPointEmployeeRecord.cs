﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Interface
{
    public interface IPointEmployeeRecord : IEntity
    {
        IPoint Point { get; set; }

        IUser User { get; set; }

        DateTime StartActivity { get; set; }

        DateTime? EndActivity { get; set; }

        ICompany Company { get; set; }
    }
}
