﻿using System;

namespace BCR.Bio.Domain.Interface
{
    public interface IIdentityBlackList : IEntity
    {
        string TypeId { get; set; }

        string ValueId { get; set; }

        ICompany Company { get; set; }

        string RazonIn { get; set; }

        IUser UserIn { get; set; }

        string RazonOut { get; set; }

        IUser UserOut { get; set; }

        DateTime? EndDate { get; set; }
        BpIdentity Identity { get; set; }
    }
}