﻿using System;

namespace BCR.Bio.Domain.Interface
{
    public interface IEntity : IEntityBase<int>
    {
        //Guid Key { get; set; }

        DateTime? CreateDate { get; set; }

        DateTime? UpdateDate { get; set; }
    }
}