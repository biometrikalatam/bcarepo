﻿using BCR.System.Enum;
using System;

namespace BCR.Bio.Domain.Interface
{
    public interface IAudit : IEntityBase<int>
    {
        User User { get; set; }

        AuditAction Action { get; set; }

        DateTime AuditDate { get; set; }

        String ObjectName { get; set; }

        String ObjectId { get; set; }

        String Data { get; set; }
    }
}