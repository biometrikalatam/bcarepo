﻿using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IRol : IEntity
    {
        string Name { get; set; }

        //List<User> UsersInRole { get; set; }
        int Lvl { get; set; }
    }
}