﻿using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IResident : IEntity
    {
        DateTime? EndDate { get; set; }

        BpIdentity Identity { get; set; }

        IList<Vehicle> Vehicles { get; set; }

        IList<Unity> Unitys { get; set; }

        IList<ResidentAuthorizator> ResidentAuthorizators { get; set; }

        bool HaveVehicles { get; }

        bool IsAuthorizator { get; }
    }
}