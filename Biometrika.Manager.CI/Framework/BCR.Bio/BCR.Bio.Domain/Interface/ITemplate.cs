﻿using System;

namespace BCR.Bio.Domain.Interface
{
    /// <summary>
    /// Interface destinada a definir los datos necesarios para templates.
    /// Dentro de los templates se consideran los samples (caso que sea WSQ o RAW por ejemplo) 
    /// o template de algun algoritmo.
    /// </summary>
    public interface ITemplate : IDisposable
    {
        /// <summary>
        /// Tecnologia a la que corresponde el template
        /// </summary>
        int AuthenticationFactor { get; set; }

        /// <summary>
        /// Tipo de minucia que contiene Data, incluidos WSQ, RAW, etc.
        /// </summary>
        int MinutiaeType { get; set; }

        /// <summary>
        /// Tipo de informacion (data) que contendra el template (desde BIR_TYPE)
        /// </summary>
        int Type { get; set; }

        /// <summary>
        /// Parte del cuerpo si es tecnologia biometrica
        /// </summary>
        int BodyPart { get; set; }

        /// <summary>
        /// Data propia del template o sample
        /// </summary>
        byte[] Data { get; set; }

        /// <summary>
        /// Datos adicionales segun sea la data. Por ejemplo si es WSQ, 
        /// se puede colocar las dimensiones de la forma w=512|h=512, o si
        /// es template NEC, el coeficiente coef=xxx. Si hay mas de uno separado 
        /// por pipe "|"
        /// </summary>
        string AdditionalData { get; set; }

        /// <summary>
        /// Obtiene la Data en base 64
        /// </summary>
        string GetData { get; }

        /// <summary>
        /// Setea la data desde base 64
        /// </summary>
        string SetData { set; }

    }
}
