﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCR.Bio.Domain.Interface
{
    public interface IPersona :IEntity
    {

        string TypeId { get; set; }

        string ValueId { get; set; }

        string Name { get; set; }

        string PatherLastName { get; set; }

        string MotherLastName { get; set; }

        int Sex { get; set; }

        DateTime DateBirth { get; set; }

        DateTime StarDate { get; set; }

        DateTime EndDate { get; set; }

        string Mail { get; set; }

        string Photography { get; set; }

        IAddress Address { get; set; }
    }
}
