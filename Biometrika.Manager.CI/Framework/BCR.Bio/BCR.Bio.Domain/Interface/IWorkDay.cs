﻿using System;

namespace BCR.Bio.Domain.Interface
{
    public interface IWorkDay : IEntity
    {
        int Id { get; set; }
        String Name { get; set; }
        DateTime StartTime { get; set; }
        DateTime EndTime { get; set; }
        DateTime WorkTime { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
    }
}