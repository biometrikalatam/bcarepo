﻿using System;

namespace BCR.Bio.Domain.Interface
{
    public interface IPoint : IEntity, ICanBeDisable
    {
        string Code { get; set; }

        string Name { get; set; }

        string IPaddress { get; set; }

        string SensorId { get; set; }

        string MacAddress { get; set; }

        string Address { get; set; }

        string Phone { get; set; }

        string OtherPhone { get; set; }

        string ContactName { get; set; }

        DateTime? EndDate { get; set; }

        int HasPrinter { get; set; }

        int HasMail { get; set; }

        bool Is_Dev { get; set; }

        int Dev_Id { get; set; }

        int Dev_baudrates { get; set; }

        string Dev_serialnumber { get; set; }

        int Dev_number { get; set; }

        int Dev_umbral { get; set; }

        int Dev_attendancemode { get; set; }

        string Dev_password { get; set; }

        int Dev_comport { get; set; }

        int Dev_modeautentication { get; set; }

        int Dev_dooropen { get; set; }

        string Dev_ipserver { get; set; }

        string Dev_gateway { get; set; }

        int Dev_userlength { get; set; }

        int Dev_display12 { get; set; }

        int Dev_refresh { get; set; }

        DateTime? Dev_daterefresh { get; set; }

        Sector Sector { get; set; }

        HQ HQ { get; set; }
    }
}