﻿using System;

namespace BCR.Bio.Domain.Interface
{
    public interface IBPLog
    {
        int id { get; set; }

        DateTime datel { get; set; }

        string exceptionl { get; set; }

        string levell { get; set; }

        string loggerl { get; set; }

        string messagel { get; set; }

        string threadl { get; set; }
    }
}