﻿namespace BCR.Bio.Domain.Interface
{
    public interface IEntityItem<T> : IEntityBase<T>
    {
        string Description { get; set; }
    }
}