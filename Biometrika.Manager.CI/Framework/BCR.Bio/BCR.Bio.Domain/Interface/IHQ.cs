﻿using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IHQ : IEntity
    {
        string Description { get; set; }

        string Address { get; set; }

        string Phone { get; set; }

        string Contact { get; set; }

        DateTime? EndDate { get; set; }

        ICompany Company { get; set; }

        ITown Town { get; set; }

        IList<Point> Points { get; set; }
    }
}