﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Interface
{
    interface IBenefit: IEntity
    {
        string Description { get; set; }

        int Modulo { get; set; }

        ICompany Company { get; set; }

    }
}
