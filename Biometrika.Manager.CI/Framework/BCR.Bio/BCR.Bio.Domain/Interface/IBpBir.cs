﻿using BCR.Bio.Domain.Interface;
using System;

namespace BCR.Bio.Domain.Interface
{
    public interface IBpBir 
    {
        int Id { get; set; }

        BpIdentity BpIdentity { get; set; }

        int Authenticationfactor { get; set; }

        int Minutiaetype { get; set; }

        int Bodypart { get; set; }

        string Data { get; set; }

        string Additionaldata { get; set; }

        DateTime Timestamp { get; set; }

        //DateTime Creation { get; set; }

        string Signature { get; set; }

        int Companyidenroll { get; set; }

        int Useridenroll { get; set; }

        int Type { get; set; }
    }
}