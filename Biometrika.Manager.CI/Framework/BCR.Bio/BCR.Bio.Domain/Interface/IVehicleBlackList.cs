﻿using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IVehicleBlackList : IEntity
    {
        string PlateNumber { get; set; }

        IVehicleMake Make { get; set; }

        IVehicleModel Model { get; set; }

        IVehicleType Type { get; set; }

        string Color { get; set; }

        string RazonIn { get; set; }

        IUser UserIn { get; set; }

        string RazonOut { get; set; }

        IUser UserOut { get; set; }

        DateTime? EndDate { get; set; }

        ICompany Company { get; set; }

        IList<Resident> Residents { get; set; }

        IList<Visitor> Visitors { get; set; }
        Vehicle Vehicle { get; set; }
    }
}