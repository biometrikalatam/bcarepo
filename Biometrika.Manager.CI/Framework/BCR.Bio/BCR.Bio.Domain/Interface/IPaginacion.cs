﻿namespace BCR.Bio.Domain.Interface
{
    public interface IPaginacion
    {
        int Cantidad { get; set; }

        int Pagina { get; set; }

        int Total { get; set; }
    }
}