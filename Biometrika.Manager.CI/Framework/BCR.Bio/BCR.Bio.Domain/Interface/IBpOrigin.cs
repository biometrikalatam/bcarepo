﻿using BCR.Bio.Domain.Interface;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IBpOrigin : IEntity
    {
        //IList<BpTx> BpTx { get; set; }

        string Description { get; set; }

        //int Id { get; set; }
    }
}