﻿
namespace BCR.Bio.Domain.Interface
{
    public interface ILunchbreak : IEntity
    {
        Company Company { get; set; }
        string Name { get; set; }
        int Minutes { get; set; }
        double WorkHours { get; set; }
    }
}
