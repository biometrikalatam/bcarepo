﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Interface
{
    interface IEmployeeOptionBenefit:IEntity
    {
        IBenefitOption benefitoption { get; set; }

        IEmployee employee { get; set; }
    }
}
