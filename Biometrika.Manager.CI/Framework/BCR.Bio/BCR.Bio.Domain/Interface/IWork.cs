﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Interface
{
    interface IWork : IEntity
    {
        string Description { get; set; }        

        ICompany Company { get; set; }
        
    }
}
