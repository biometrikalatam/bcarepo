﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Interface
{
    public interface ICity : IEntityItem<int> 
    {
        Dept Dept { get; set; }

        IList<Town> Towns { get; set; }
    }
}
