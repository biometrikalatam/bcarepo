﻿using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IResidentPermission : IEntity
    {
        bool CanInvite { get; set; }
        bool CanEditPermissions { get; set; }
        bool CanAddFamilyMembers { get; set; }
        bool IsLeader { get; set; }
        Resident Resident { get; set; }
        Unity Unity { get; set; }
    }
}
