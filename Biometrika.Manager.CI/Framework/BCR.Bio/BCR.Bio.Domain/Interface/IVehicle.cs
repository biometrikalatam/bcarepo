﻿using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain.Interface
{
    public interface IVehicle : IEntity
    {
        string PlateNumber { get; set; }

        IVehicleMake Make { get; set; }

        IVehicleModel Model { get; set; }

        IVehicleType Type { get; set; }

        string Color { get; set; }

        string Detail { get; set; }

        DateTime? EndDate { get; set; }

        ICompany Company { get; set; }

        IList<Resident> Residents { get; set; }

        IList<Visitor> Visitors { get; set; }

        IList<VehicleBlackList> VehicleBlackList { get; set; }

        IdentityState IdentityState { get; set; }
    }
}