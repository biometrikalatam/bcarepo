﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCR.Bio.Domain.Interface
{
   
    public interface ICar:IEntity
    {
        string NumberPlate { get; set; }

        string Color { get; set; }

        int Year { get; set; }

        DateTime StartDate { get; set; }

        DateTime EndDate { get; set; }
    }
}
