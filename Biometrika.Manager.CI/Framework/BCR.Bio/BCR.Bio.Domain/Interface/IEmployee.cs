﻿namespace BCR.Bio.Domain.Interface
{
    public interface IEmployee : IEntity
    {
        BpIdentity Identity { get; set; }

        IUser User { get; set; }

        Work Work { get; set; }
    }
}