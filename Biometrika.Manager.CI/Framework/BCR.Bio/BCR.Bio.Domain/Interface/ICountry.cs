﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Interface
{
    public interface ICountry :  IEntityItem<int> 
    {
        IList<Dept> Depts { get; set; }
        string Alpha2 { get; set; }
        string Alpha3 { get; set; }
        string NumericCode { get; set; }
        string CountryRawName { get; set; }
    }
}
