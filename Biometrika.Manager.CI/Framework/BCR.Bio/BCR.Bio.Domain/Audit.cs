﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;

namespace BCR.Bio.Domain
{
    //[Serializable]
    /// <summary>
    /// Audit domain entity
    /// </summary>
    public class Audit :  IAudit
    {
        public virtual int Id { get; set; }

        public virtual User User { get; set; }

        public virtual BCR.System.Enum.AuditAction Action { get; set; }

        //public virtual DateTime CreateDate { get; set; }

        //public virtual DateTime? UpdateDate { get; set; }

        public virtual DateTime AuditDate { get; set; }

        public virtual String ObjectName { get; set; }

        public virtual String ObjectId { get; set; }

        public virtual string Data { get; set; }
        
    }
}