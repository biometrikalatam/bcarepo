﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Define una estrucura para levantar los matcher configurados desde
    /// XML (Matcher.cfg), para luego inicializar en Availables.
    /// </summary>
    [Serializable]
    public class ConnectorDefinition
    {
        private string _connectorid;
        private string _name;
        private string _assembly;
        private DynamicData _config;

        ///<summary>
        ///</summary>
        public string ConnectorId
        {
            get { return _connectorid; }
            set { _connectorid = value; }
        }

        ///<summary>
        ///</summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        ///<summary>
        ///</summary>
        public string Assembly
        {
            get { return _assembly; }
            set { _assembly = value; }
        }

        ///<summary>
        ///</summary>
        public DynamicData Config
        {
            get { return _config; }
            set { _config = value; }
        }

    }
}
