﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class EmployeeOptionBenefit : AbstractEntity, IEmployeeOptionBenefit
    {
        public EmployeeOptionBenefit()
        {

        }
    
        public EmployeeOptionBenefit(IEmployee Employee, IBenefitOption BenefitOption)
        {
            employee = Employee;
            benefitoption = BenefitOption;
        }
        public EmployeeOptionBenefit(int id, IEmployee Employee, IBenefitOption BenefitOption)
        {
            this.Id = id;
            employee = Employee;
            benefitoption = BenefitOption;
        }



        public virtual IBenefitOption benefitoption { get; set; }
        
        public virtual IEmployee employee { get; set; }
        
    }
}
