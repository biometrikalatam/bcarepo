﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Domain
{
    public class Sector : AbstractEntity, ISector
    {
        public virtual string Description { get; set; }

        public virtual string ContactName { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual string externalid { get; set; }

        public virtual IList<Point> Points { get; set; }

        public virtual IList<Company> Companies { get; set; }

        public Sector()
        {
            Companies = new List<Company>();
        }

        public Sector(int IdTemp)
        {
            Id = IdTemp;
        }


        public virtual bool IsDisable
        {
            get
            {
                return EndDate != null;
            }
        }

        //public virtual bool CanBeEnable
        //{
        //    get
        //    {
        //        bool canBe = false;
        //        Companies.ToList().ForEach(c => canBe = c.IsActive || canBe);
        //        return canBe;
        //    }
        //}
    }
}