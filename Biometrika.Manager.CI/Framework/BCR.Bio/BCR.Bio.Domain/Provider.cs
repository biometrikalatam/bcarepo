﻿using BCR.Bio.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BCR.Bio.Domain
{
    public class Provider : AbstractEntity
    {
        public Provider()
        {
        }

        public Provider(int idTemp)
        {
            Id = idTemp;
        }

        [Required(ErrorMessage = "Nombre obligatorio")]
        [Display(Name = "Nombre *")]
        public virtual string Name { get; set; }

        [Required(ErrorMessage = "Identificador obligatorio")]
        [Display(Name = "Identificador fiscal *")]
        public virtual string IdentifierCode { get; set; }

        [Display(Name = "Dirección")]
        public virtual string Address { get; set; }

        [Display(Name = "Teléfono")]
        public virtual string Phone { get; set; }

        public virtual DateTime? EndDate { get; set; }

        [Display(Name = "Contacto")]
        public virtual string ContactName { get; set; }

        public virtual bool Status { get; set; }

        public virtual bool Outsourcing { get; set; }

        public virtual Company Company { get; set; }

        public virtual IList<Visitor> VisitorList { get; set; }
        public virtual IList<Vehicle> VehicleList { get; set; }
    }
}