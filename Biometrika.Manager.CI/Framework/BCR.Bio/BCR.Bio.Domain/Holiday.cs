﻿using BCR.Bio.Domain.Abstract;
using System;

namespace BCR.Bio.Domain
{
    public class Holiday : AbstractEntity
    {
        public virtual Unity Unity { get; set; }
        public virtual Company Company { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
    }
}
