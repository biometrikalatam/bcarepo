﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using BCR.System.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class Vehicle : AbstractEntity, IVehicle
    {
        public virtual string Color { get; set; }

        [JsonIgnore]
        public virtual ICompany Company { get; set; }

        public virtual string Detail { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual IVehicleMake Make { get; set; }

        public virtual IVehicleModel Model { get; set; }

        public virtual string PlateNumber { get; set; }

        public virtual IVehicleType Type { get; set; }

        public virtual IList<Resident> Residents { get; set; }

        public virtual IList<Visitor> Visitors { get; set; }

        public virtual IList<VehicleBlackList> VehicleBlackList { get; set; }

        //Saqué ésto porque no tengo idea qué proposito cumplía. Chocaba con Nhibernate
        //public override Boolean Equals(Object obj)
        //{
        //    Vehicle vehicle = (Vehicle)obj;
        //    return this.Id == vehicle.Id;
        //}

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        //public virtual Boolean Equals(Vehicle other)
        //{
        //    return this.Id == other.Id;
        //}

        //public Vehicle Create()
        //{
        //    ServiceLocator.GetService<VehicleService>
        //}

        public virtual IList<Provider> ProviderList { get; set; }
        public virtual IdentityState IdentityState { get; set; }
    }
}