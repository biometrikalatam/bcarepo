﻿using BCR.Bio.Domain.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Tabla de dominio que hace referencia a la tabla Group
    /// </summary>
    public class Group : AbstractEntity
    {
        public virtual string Description { get; set; }

        public virtual DateTime? EndDate { get; set; }
        /// <summary>
        /// Hace referencia a la tabla VisitorGroup, que es el listado de visitas agrupadas
        /// </summary>
        public virtual IList<Visitor> Visitors { get; set; }

        public virtual bool HasVisitors
        {
            get
            {
                return Visitors != null ? Visitors.Count > 0 : false;
            }
        }
    }
}