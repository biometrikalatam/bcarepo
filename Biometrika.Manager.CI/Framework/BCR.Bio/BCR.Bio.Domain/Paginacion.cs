﻿using BCR.Bio.Domain.Interface;
using System;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Clase de configuracion para paginado de busquedas
    /// </summary>
    public class Paginacion : IPaginacion
    {
        /// <summary>
        /// Cantidad de resultados a recuperar por pagina
        /// </summary>
        public int Cantidad { get; set; }
        /// <summary>
        /// Que pagina estamos buscando, si es la inicial debe ser 0
        /// </summary>
        public int Pagina { get; set; }
        /// <summary>
        /// Total de resultados sin paginar devueltos por la consulta
        /// </summary>
        public int Total { get; set; }
        public int PageNumBotLimit
        {
            get
            {
                if (Total == 0)
                    return 0;

                return Pagina == 0 ? 0 :
                    (Pagina - 4 < 0 ? 0 : Pagina - 4);
            }
        }
        public int PageNumTopLimit
        {
            get
            {
                if (Total == 0)
                    return 0;

                float divTotal = Total / 10;
                return Pagina == (int)Math.Floor(divTotal) ? Pagina :
                    (Pagina + 4 > (int)Math.Floor(divTotal) ? (int)Math.Floor(divTotal) : Pagina + 4);
            }
        }

    }
}