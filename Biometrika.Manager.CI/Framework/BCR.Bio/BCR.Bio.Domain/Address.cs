﻿using BCR.Bio.Domain.Interface;

namespace BCR.Bio.Domain
{
    public class Address : IAddress
    {
        public int Number { get; set; }

        public string Street { get; set; }

        public string PostCode { get; set; }

        public ICity Ciudad { get; set; }
    }
}