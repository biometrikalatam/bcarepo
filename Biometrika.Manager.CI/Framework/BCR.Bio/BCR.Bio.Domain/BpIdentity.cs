using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using Bio.Core.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Clase de dominio que hace referencia a la tabla bp_identity
    /// Esta clase implementa un AbstractEntity y la interfaz IBpIdentity
    /// </summary>
    public class BpIdentity : AbstractEntity, IBpIdentity
    {
        public BpIdentity()
        {
        }

        /// <summary>
        /// Constructor que recibe un int para la construccion de la clase con un id determinado, solo se asigna la propiedad Id
        /// </summary>
        /// <param name="IdTemp"></param>
        public BpIdentity(int IdTemp)
        {
            Id = IdTemp;
        }

        public BpIdentity(ICompany companyidenroll, string typeid, string valueid)
        {
            TypeId = typeid;
            ValueId = valueid;

            CompanyIdEnroll = companyidenroll;
            VerificationSource = "NV";
            DocumentExpirationDate = DateTimeHelper.CheckDateTimeMinimal(null);
            BirthDate = DateTimeHelper.CheckDateTimeMinimal(null);
        }

        public BpIdentity(string nick)
        {
            TypeId = "UID";
            ValueId = nick;

            CompanyIdEnroll = null;
            VerificationSource = "NV";
            DocumentExpirationDate = DateTimeHelper.CheckDateTimeMinimal(null);
            BirthDate = DateTimeHelper.CheckDateTimeMinimal(null);
        }

        //public virtual int Id
        //{
        //    get { return _id; }
        //    set { if (value != this._id) { _id = value; } }
        //}

        public virtual string Nick { get; set; }

        /// <summary>
        /// Hace referencia a la columna typeid
        /// </summary>
        public virtual string TypeId { get; set; }
        /// <summary>
        /// Hace referencia a la columna valueid y debe corresponder con el typeid
        /// </summary>
        public virtual string ValueId { get; set; }

        public virtual string Name { get; set; }

        public virtual string PatherLastname { get; set; }

        public virtual string MotherLastname { get; set; }

        /// <summary>
        /// Sexo de la persona, valores posibles M, F
        /// </summary>
        public virtual string Sex { get; set; }

        public virtual string DocumentSeriesNumber { get; set; }

        public virtual DateTime? DocumentExpirationDate { get; set; }

        public virtual string VisaType { get; set; }

        public virtual DateTime? BirthDate { get; set; }

        public virtual string BirthPlace { get; set; }

        public virtual string Nationality { get; set; }

        public virtual string Photography { get; set; }

        public virtual string Signatureimage { get; set; }

        public virtual string Profession { get; set; }

        public virtual string Dynamicdata { get; set; }

        public virtual string Enrollinfo { get; set; }

        public virtual DateTime Creation { get; set; }

        public virtual string VerificationSource { get; set; }

        /// <summary>
        /// Propiedad que hace referencia al campo company, este campo tiene relaci�n con la tabla bp_company
        /// </summary>
        public virtual ICompany CompanyIdEnroll { get; set; }

        public virtual int UserIdEnroll { get; set; }

        public virtual IList<BpBir> BpBir { get; set; }

        public virtual IList<BpIdentityDynamicdata> BpIdentityDynamicdata { get; set; }

        public virtual ITown Town { get; set; }

        public virtual string FullName { get; set; }
        public virtual string FullApellidos { get; set; }

        public virtual string Phone { get; set; }
        public virtual string Phone2 { get; set; }
        public virtual string Cellphone { get; set; }

        public virtual IList<IdentityBlackList> IdentityBlackList { get; set; }

        public virtual IList<IdentityType> IdentityTypeList { get; set; }

        public virtual IList<CentroDeCostos> CentrosDeCostoList { get; set; }
        public virtual IList<IdentityState> IdentityStateList { get; set; }
        public virtual IList<ContactForm> ContactFormList { get; set; }
        public virtual int VisitCounter { get; set; }
    }
}