﻿using BCR.Bio.Domain.Abstract;

namespace BCR.Bio.Domain
{
    public class RuleScope : AbstractEntityItem<int>
    {
        public RuleScope() { }

        public RuleScope(int id)
        {
            this.Id = id;
        }
    }
}