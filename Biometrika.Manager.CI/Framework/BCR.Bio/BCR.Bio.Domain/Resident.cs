﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using NHibernate;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class Resident : AbstractEntity, IResident
    {
        public virtual BpIdentity Identity { get; set; }
        public virtual DateTime? EndDate { get; set; }

        public virtual IList<Vehicle> Vehicles { get; set; }

        public virtual IList<ResidentAuthorizator> ResidentAuthorizators { get; set; }

        public virtual IList<Unity> Unitys { get; set; }

        public virtual bool HaveVehicles
        {
            get
            {
                return Vehicles != null ? Vehicles.Count > 0 : false;
            }
        }

        public virtual bool IsAuthorizator
        {
            get
            {
                if (ResidentAuthorizators != null)
                    return ResidentAuthorizators.Count > 0;

                return false;
            }
        }

        public virtual bool IsUnityLoaded()
        {
            return NHibernateUtil.IsInitialized(Unitys) ? true : false;
        }
    }
}