﻿using BCR.Bio.Domain.Abstract;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Clase de dominio que hace referencia a la tabla Building, un Building siempre depende de un Zone
    /// </summary>
    public class Building : AbstractEntity
    {
        public Building() { }


        public Building(int idBuilding)
        {
            Id = idBuilding;
        }
        /// <summary>
        /// Descriocion o nombre de Building
        /// </summary>
        public virtual string Description { get; set; }
        /// <summary>
        /// Direccion de Building
        /// </summary>
        public virtual string Address { get; set; }
        /// <summary>
        /// Zone a la que pertence el Building
        /// </summary>
        public virtual Zone Zone { get; set; }
        /// <summary>
        /// Listado de Unity que pertencen al Building, en referencia a Unity.building
        /// </summary>
        public virtual IList<Unity> Unitys { get; set; }
        /// <summary>
        /// Booleano que dice si el Building tiene Unity asociados
        /// </summary>
        public virtual bool HaveUnitys
        {
            get
            {
                return Unitys != null ? Unitys.Count > 0 : false;
            }
        }
    }
}