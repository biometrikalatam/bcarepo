﻿using Bio.Core.Api;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BCR.Bio.Domain
{
    [XmlTypeAttribute(AnonymousType = true)]
    public class BirsData
    {
        [XmlElement("BpBirApi")]
        public List<BpBirApi> BpBirs { get; set; }

        public BirsData()
        {
            BpBirs = new List<BpBirApi>();
        }
    }
}
