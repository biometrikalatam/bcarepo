﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Define una estrucura para levantar los matcher configurados desde
    /// XML (Matcher.cfg), para luego inicializar en Availables.
    /// </summary>
    [Serializable]
    public class MatcherDefinition
    {
        private string _name;
        private int _authenticationfactor;
        private int _minutiaetype;
        private double _thresholdextract;
        private double _thresholdmatching;
        private string _assembly;
        private string _parameters;

        ///<summary>
        ///</summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        ///<summary>
        ///</summary>
        public int Authenticationfactor
        {
            get { return _authenticationfactor; }
            set { _authenticationfactor = value; }
        }

        ///<summary>
        ///</summary>
        public int Minutiaetype
        {
            get { return _minutiaetype; }
            set { _minutiaetype = value; }
        }

        ///<summary>
        ///</summary>
        public double Thresholdextract
        {
            get { return _thresholdextract; }
            set { _thresholdextract = value; }
        }

        ///<summary>
        ///</summary>
        public double Thresholdmatching
        {
            get { return _thresholdmatching; }
            set { _thresholdmatching = value; }
        }

        ///<summary>
        ///</summary>
        public string Assembly
        {
            get { return _assembly; }
            set { _assembly = value; }
        }

        ///<summary>
        ///</summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }
    }

}
