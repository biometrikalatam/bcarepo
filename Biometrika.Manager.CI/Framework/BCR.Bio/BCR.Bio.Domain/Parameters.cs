﻿using BCR.System.IO;
//using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace BCR.Bio.Domain
{
    [Serializable]
    public class Parameters
    {
        //private static readonly ILog LOG = LogManager.GetLogger(typeof(Parameters));

        public Parameters()
        {
        }

        #region properties

        ///<summary>
        /// Lista de parametros
        ///</summary>
        public List<ParametersItem> Items;

        #endregion properties

        #region Operations

        /// <summary>
        /// Retorna la lista de items de parametros, con formato key/value
        /// </summary>
        /// <returns>Lista de parametros</returns>
        public List<ParametersItem> GetConfigValues()
        {
            return Items;
        }

        /// <summary>
        /// Dada una clave, retorna el valor si existe la clave. sino nulll
        /// </summary>
        /// <param name="key">Clave para buscar</param>
        /// <returns>Objeto con el valor del parametro</returns>
        public object GetValue(string key)
        {
            object oRes = null;

            try
            {
                if (Items == null || Items.Count == 0)
                    return null;

                for (var i = 0; i < Items.Count; i++)
                {
                    if (key == Items[i].Key)
                    {
                        oRes = Items[i].Value;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Serialize.Parameters.GetValue", ex);
            }

            return oRes;
        }

        /// <summary>
        /// Setea un valor en una clave dada
        /// </summary>
        /// <param name="key">Clave para actualizar</param>
        /// <param name="value">Valor del parametro a actualizar</param>
        /// <returns>True o false si actualizo bien o mal respectivamente</returns>
        public bool SetValue(string key, object value)
        {
            bool bRes = false;
            try
            {
                if (Items == null || Items.Count == 0)
                    return false;

                for (int i = 0; i < Items.Count; i++)
                {
                    if (key == (Items[i]).Key)
                    {
                        Items[i].Value = value;
                        bRes = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Serialize.Parameters.SetValue", ex);
            }

            return bRes;
        }

        /// <summary>
        /// Agrega un valor en una clave dada
        /// </summary>
        /// <param name="key">Clave para agregar</param>
        /// <param name="value">Valor del parametro a agregar</param>
        /// <returns>True o false si agrego bien o mal respectivamente</returns>
        public bool AddValue(string key, object value)
        {
            bool bRes = false;
            try
            {
                if (Items == null)
                    Items = new List<ParametersItem>();

                Items.Add(new ParametersItem(key, value));
                bRes = true;
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Serialize.Parameters.AddValue", ex);
            }

            return bRes;
        }

        #endregion Operations

        #region Serialize

        /// <summary>
        /// Serializa un objeto Parameters a disco
        /// </summary>
        /// <param name="param">Parametros a serializar</param>
        /// <param name="path">Path absoluto donde serializar</param>
        /// <returns>true o false en exito y fracaso respectivamente</returns>
        public static bool SerializeToFile(Parameters param, string path)
        {
            bool bRes = false;

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Parameters));
                TextWriter writer = new StreamWriter(path);
                serializer.Serialize(writer, param);
                writer.Close();
                bRes = true;
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Serialize.SaveToFile", ex);
            }
            return bRes;
        }

        /// <summary>
        /// Deserializa un objeto Parameters desde disco
        /// </summary>
        /// <param name="path">Path absoluto donde serializar</param>
        /// <returns>Objeto deserializado o null de haber error</returns>
        public static Parameters DeserializeFromFile(string path)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Parameters));
                TextReader reader = new StreamReader(path);
                Parameters cf = (Parameters)serializer.Deserialize(reader);
                reader.Close();
                return cf;
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Serialize.SaveToFile", ex);
            }
            return null;
        }

        /// <summary>
        /// Serializa un objeto Parameters y lo devuelve en un string
        /// </summary>
        /// <param name="param">Parametros a serializar</param>
        /// <returns>true o false en exito y fracaso respectivamente</returns>
        public static string SerializeToXml(Parameters param)
        {
            string xmlRes = null;

            try
            {
                xmlRes = XmlUtils.SerializeObject(param);
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Serialize.SerializeToXml", ex);
            }
            return xmlRes;
        }

        /// <summary>
        /// Serializa un objeto Parameters y lo devuelve en un string
        /// </summary>
        /// <param name="xmlParam">Xml del objeto a deserializar</param>
        /// <returns>true o false en exito y fracaso respectivamente</returns>
        public static Parameters DeserializeFromXml(string xmlParam)
        {
            Parameters olRes = null;

            try
            {
                olRes = XmlUtils.DeserializeObject<Parameters>(xmlParam);
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Serialize.DeserializeFromXml", ex);
            }
            return olRes;
        }

        #endregion Serialize
    }

    ///<summary>
    /// Items de la lista de parametros
    ///</summary>
    [Serializable]
    public class ParametersItem
    {
        public ParametersItem()
        {
        }

        /// <summary>
        /// Constructor basico
        /// </summary>
        /// <param name="key">Clave</param>
        /// <param name="value">Valor</param>
        public ParametersItem(string key, object value)
        {
            _key = key;
            _value = value;
        }

        private string _key;

        /// <summary>
        /// Clave del parametro
        /// </summary>
        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }

        private object _value;

        /// <summary>
        /// Valor del parametro
        /// </summary>
        public object Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}