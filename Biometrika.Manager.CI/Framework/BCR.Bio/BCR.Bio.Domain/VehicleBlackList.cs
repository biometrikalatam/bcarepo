﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class VehicleBlackList : AbstractEntity, IVehicleBlackList
    {
        public virtual string Color { get; set; }

        public virtual ICompany Company { get; set; }

        public virtual string RazonIn { get; set; }

        public virtual IUser UserIn { get; set; }

        public virtual string RazonOut { get; set; }

        public virtual IUser UserOut { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual IVehicleMake Make { get; set; }

        public virtual IVehicleModel Model { get; set; }

        public virtual string PlateNumber { get; set; }

        public virtual IVehicleType Type { get; set; }

        public virtual IList<Resident> Residents { get; set; }

        public virtual IList<Visitor> Visitors { get; set; }

        public virtual Vehicle Vehicle { get; set; }
        public virtual bool Export { get; set; }
    }
}