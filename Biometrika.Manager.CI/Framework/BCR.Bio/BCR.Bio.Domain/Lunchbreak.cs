﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;

namespace BCR.Bio.Domain
{
    public class Lunchbreak : AbstractEntity, ILunchbreak
    {
        public virtual Company Company { get; set; }
        public virtual string Name { get; set; }
        public virtual int Minutes { get; set; }
        public virtual double WorkHours { get; set; }
    }
}
