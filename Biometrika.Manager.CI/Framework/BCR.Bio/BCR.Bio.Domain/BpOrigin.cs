using BCR.Bio.Domain.Interface;
using BCR.Bio.Domain.Abstract;

using System;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// BpOrigin object for NHibernate mapped table 'bp_origin'.
    /// </summary>
    [Serializable]
    public class BpOrigin : AbstractEntity, IBpOrigin
    {
        public virtual string Description
        {
            get;
            set;
        }
    }
}