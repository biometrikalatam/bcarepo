﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;

namespace BCR.Bio.Domain
{
    public class WorkDay : AbstractEntity, IWorkDay
    {
        //public virtual int Id { get; set; }
        public virtual String Name { get; set; }
        public virtual DateTime StartTime { get; set; }
        public virtual DateTime EndTime { get; set; }
        public virtual DateTime WorkTime { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
    }
}