﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
/// <summary>
/// Register Method corresponde a todos aquellos tipos de formas que una identidad puede generar un evento en la plataforma.
/// Por ejemplo, los valores posible son:
/// 1. RUT
/// 2. HUELLA.
/// 3. PIN
/// 4. TARJETA
/// Se asocia tanto a un Empleado, así como un Punto o Dispositivo. Ya que la combinatoria de registermethod determinará
/// las posibilidades de esa persona de registrar el evento.
///</summary>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class RegisterMethod : IRegisterMethod
    {
        public virtual DateTime? CreateDate { get; set; }    
        
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual DateTime? UpdateDate { get; set; }
        
    }
}
