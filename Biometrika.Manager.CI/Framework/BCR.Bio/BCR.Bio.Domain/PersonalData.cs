﻿using System;

namespace BCR.Bio.Domain
{
    public class PersonalData
    {
        public int Id
        {
            get;
            set;
        }

        public string Typeid
        {
            get;
            set;
        }

        public string Valueid
        {
            get;
            set;
        }

        public string Nick
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Patherlastname
        {
            get;
            set;
        }

        public string Motherlastname
        {
            get;
            set;
        }

        public string Sex
        {
            get;
            set;
        }

        public string Documentseriesnumber
        {
            get;
            set;
        }

        public DateTime Documentexpirationdate
        {
            get;
            set;
        }

        public string Visatype
        {
            get;
            set;
        }

        public DateTime Birthdate
        {
            get;
            set;
        }

        public string Birthplace
        {
            get;
            set;
        }

        public string Nationality
        {
            get;
            set;
        }

        public string Photography
        {
            get;
            set;
        }

        public string Signatureimage
        {
            get;
            set;
        }

        public string Profession
        {
            get;
            set;
        }

        public DynamicDataItem[] Dynamicdata
        {
            get;
            set;
        }

        public string Enrollinfo
        {
            get;
            set;
        }

        public DateTime Creation
        {
            get;
            set;
        }

        public string Verificationsource
        {
            get;
            set;
        }

        public int Companyidenroll
        {
            get;
            set;
        }

        public int Useridenroll
        {
            get;
            set;
        }
        public string DocImageBack { get; set; }
        public string DocImageFront { get; set; }
    }
}