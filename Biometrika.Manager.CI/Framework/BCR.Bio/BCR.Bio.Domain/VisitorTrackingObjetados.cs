﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class VisitorTrackingObjetados
    {
        public virtual int Id { get; set; }
        public virtual BpIdentity Identity { get; set; }
        public virtual DateTime TimeStamp { get; set; }
        public virtual TrackingAction Action { get; set; }
        public virtual TrackingResult Result { get; set; }
        public virtual Point Point { get; set; }
        public virtual Company Company { get; set; }
    }
}
