﻿using BCR.Bio.Domain.Abstract;
using System;

namespace BCR.Bio.Domain
{
    public class ResidentAuthorizator : AbstractEntity
    {
        public virtual Resident Resident { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual int Category { get; set; }   // must be a type : AuthirizatorCategory
    }
}