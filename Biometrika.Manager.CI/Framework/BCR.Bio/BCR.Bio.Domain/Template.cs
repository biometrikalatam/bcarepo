﻿using BCR.Bio.Domain.Interface;
using System;
using System.Text;

namespace BCR.Bio.Domain
{
    [Serializable]
    public class Template : ITemplate
    {
        //private static readonly ILog LOG = LogManager.GetLogger(typeof(Template));

        /// <summary>
        /// Datos adicionales segun sea la data. Por ejemplo si es WSQ,
        /// se puede colocar las dimensiones de la forma w=512|h=512, o si
        /// es template NEC, el coeficiente coef=xxx. Si hay mas de uno separado
        /// por pipe "|"
        /// </summary>
        public string AdditionalData
        {
            get;
            set;
        }

        /// <summary>
        /// Tecnologia a la que corresponde el template
        /// </summary>
        public int AuthenticationFactor
        {
            get;
            set;
        }

        /// <summary>
        /// Parte del cuerpo si es tecnologia biometrica
        /// </summary>
        public int BodyPart
        {
            get;
            set;
        }

        /// <summary>
        /// Id Company que enrolo el template utilizado
        /// </summary>
        public int Companyidenroll
        {
            get;
            set;
        }

        /// <summary>
        /// Data propia del template o sample
        /// </summary>
        public byte[] Data
        {
            get;
            set;
        }

        /// <summary>
        /// Obtiene la Data en base 64
        /// </summary>
        public string GetData
        {
            get { return (Data != null && Data.Length > 0) ? Convert.ToBase64String(Data) : null; }
        }

        /// <summary>
        /// Agregado apra que identifique el bir y con eso saber la
        /// Identity para los casos de Identify
        /// </summary>
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// Tipo de minucia que contiene Data, incluidos WSQ, RAW, etc.
        /// </summary>
        public int MinutiaeType
        {
            get;
            set;
        }

        /// <summary>
        /// Tipo de informacion (data) que contendra el template (desde BIR_TYPE)
        /// </summary>
        public int Type
        {
            get;
            set;
        }

        /// <summary>
        /// Id user que enrolo el template utilizado
        /// </summary>
        public int Useridenroll
        {
            get;
            set;
        }

        /// <summary>
        /// Fuente de verificacion del enrolamiento (Ej: SRCeI)
        /// </summary>
        public string Verificationsource
        {
            get;
            set;
        }

        /// <summary>
        /// Setea la data desde base 64
        /// </summary>
        public string SetData
        {
            set
            {
                try
                {
                    //Hago esto para hacer compatible con versiones anteriores. Luego sacarlo
                    //y hacer conversor para que sea mas rápido
                    string[] part = value.Split('|');

                    if (part.Length >= 2)
                    {
                        byte[] aux = Convert.FromBase64String(part.Length == 2 ? part[1] : part[0]);

                        if (Encoding.ASCII.GetString(aux, 0, 3).Equals("VF5"))
                        {
                            Data = new byte[aux.Length - 8];
                            for (int i = 8; i < aux.Length; i++)
                            {
                                Data[i - 8] = aux[i];
                            }
                        }
                        else
                        {
                            Data = aux;
                        }

                        if (part.Length > 2) AdditionalData = part[0];
                    }
                    else
                    {
                        Data = Convert.FromBase64String(value);
                    }
                }
                catch (Exception ex)
                {
                    //LOG.Error("Bio.Core.Matcher.Template SetError", ex);
                    Data = null;
                }
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}