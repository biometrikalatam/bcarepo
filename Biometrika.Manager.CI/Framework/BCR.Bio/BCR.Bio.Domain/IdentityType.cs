﻿using BCR.Bio.Domain.Abstract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class IdentityType : AbstractEntityItem<int>
    {
        public virtual string Class { get; set; }
        [JsonIgnore]
        public virtual Company Company { get; set; }
    }
}
