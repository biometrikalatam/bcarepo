﻿using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain.Matcher
{
    /// <summary>
    /// Define una estrucura para levantar los matcher configurados y crea las clases
    /// para los posteriores clones cuando se deba usar.
    /// </summary>
    [Serializable]
    public class TokenInstance
    {
        internal TokenInstance(string name, string prefix, string assembly, string parameters)
        {
            _name = name;
            _prefix = prefix;
            _assembly = assembly;
            _parameters = parameters;
        }

        private string _name;
        private string _prefix;
        private string _assembly;
        private string _parameters;
        private IToken _token;

        ///<summary>
        ///</summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        ///<summary>
        ///</summary>
        public string Prefix
        {
            get { return _prefix; }
            set { _prefix = value; }
        }

        ///<summary>
        ///</summary>
        public string Assembly
        {
            get { return _assembly; }
            set { _assembly = value; }
        }

        ///<summary>
        ///</summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        ///<summary>
        ///</summary>
        public IToken Token
        {
            get { return _token; }
            set { _token = value; }
        }
    }
}
