﻿using System;

namespace BCR.Bio.Domain.Matcher
{
    /// <summary>
    /// Define una estrucura para levantar los token configurados desde
    /// XML (Token.cfg), para luego inicializar en Availables.
    /// </summary>
    [Serializable]
    public class TokenDefinition
    {
        private string _name;
        private string _prefix;
        private string _assembly;
        private string _parameters;

        ///<summary>
        ///</summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        ///<summary>
        ///</summary>
        public string Prefix
        {
            get { return _prefix; }
            set { _prefix = value; }
        }

        ///<summary>
        ///</summary>
        public string Assembly
        {
            get { return _assembly; }
            set { _assembly = value; }
        }

        ///<summary>
        ///</summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }
    }
}