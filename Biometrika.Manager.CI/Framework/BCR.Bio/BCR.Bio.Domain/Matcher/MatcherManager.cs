﻿using BCR.Bio.Domain.Interface;
using BCR.System.Log.Abstract;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace BCR.Bio.Domain.Matcher
{
    /// <summary>
    /// Esta clase esta destinada a levantar los tipos de matcher existentes
    /// y crear las clases base, para clonar cuando sea neceario usarlas.
    /// </summary>
    [Serializable]
    public class MatcherManager
    {
        static IAbstractLogger log;

        private List<MatcherDefinition> _matchersConfigured;

        ///<summary>
        /// Lista de Matchers configurados en config XML.
        ///</summary>
        public List<MatcherDefinition> MatchersConfigured
        {
            get { return _matchersConfigured; }
            set { _matchersConfigured = value; }
        }

        private Hashtable _matchersAvailables = new Hashtable();

        ///<summary>
        /// Tabla indexada por MinutiaeType, con instancias creadas para trabajar.
        ///</summary>
        internal Hashtable MatchersAvailables
        {
            get { return _matchersAvailables; }
            set { _matchersAvailables = value; }
        }

        ///<summary>
        /// Recorre la estructura de Matchers Configured, y va generando las
        /// instancias en MatcherAvailables.
        ///</summary>
        ///<returns>0-OK | -1-Error</returns>
        public int Initialization(IAbstractLogger logger)
        {
            log = logger;
            int ret = 0;
            MatcherInstance mi;

            lock (_matchersAvailables)
            {
                try
                {
                    _matchersAvailables = new Hashtable();
                    bool matcherInstanced;
                    foreach (MatcherDefinition oMatcher in _matchersConfigured)
                    {
                        Assembly asmLogin = Assembly.Load(oMatcher.Assembly);
                        mi = new MatcherInstance(oMatcher.Name,
                                                 oMatcher.Authenticationfactor,
                                                 oMatcher.Minutiaetype,
                                                 oMatcher.Thresholdextract,
                                                 oMatcher.Thresholdmatching,
                                                 oMatcher.Assembly,
                                                 oMatcher.Parameters);
                        matcherInstanced = true;
                        //Type currenttype = null;
                        object instance;

                        Type[] types = null;
                        try
                        {
                            types = asmLogin.GetTypes();
                        }
                        catch (Exception exloader)
                        {
                            log.Warn("MatcherManager.Initialization.Loader Warn - Activator.CreateInstance(t)", exloader);
                        }

                        foreach (Type t in types)
                        {
                            try
                            {
                                instance = null;
                                try
                                {
                                    instance = Activator.CreateInstance(t);
                                }
                                catch (Exception exC)
                                {
                                    log.Warn("MatcherManager.Initialization Warn - Activator.CreateInstance(t)", exC);
                                    instance = null;
                                }

                                if (instance != null)
                                {
                                    if (instance is IMatcher) //(t.Name.IndexOf("Matcher") > -1)
                                    {
                                        mi.Matcher = (IMatcher)instance;
                                        mi.Matcher.AuthenticationFactor = oMatcher.Authenticationfactor;
                                        mi.Matcher.MinutiaeType = oMatcher.Minutiaetype;
                                        mi.Matcher.Threshold = oMatcher.Thresholdmatching;
                                        mi.Matcher.Parameters = oMatcher.Parameters;
                                    }
                                    if (instance is IExtractor) //(t.Name.IndexOf("Extractor") > -1)
                                    {
                                        mi.Extractor = (IExtractor)instance;
                                        mi.Extractor.AuthenticationFactor = oMatcher.Authenticationfactor;
                                        mi.Extractor.MinutiaeType = oMatcher.Minutiaetype;
                                        mi.Extractor.Threshold = oMatcher.Thresholdextract;
                                        mi.Extractor.Parameters = oMatcher.Parameters;
                                    }
                                    if (instance is ITemplate) //(t.Name.IndexOf("Template") > -1)
                                    {
                                        mi.Template = (ITemplate)instance;
                                        mi.Template.AuthenticationFactor = oMatcher.Authenticationfactor;
                                        mi.Template.MinutiaeType = oMatcher.Minutiaetype;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error("MatcherManager.Initialization - Creando instancias de AF=" +
                                          oMatcher.Authenticationfactor + "-MT=" + oMatcher.Minutiaetype, ex);
                                matcherInstanced = false;
                            }
                        }

                        //Agrego la minucia con las instancias creadas, si se instancio lo necesario
                        if (matcherInstanced)
                        {
                            _matchersAvailables.Add(mi.Minutiaetype, mi);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ret = -1;
                    log.Error("MatcherManager.Initialization", ex);
                }
            }
            return ret;
        }

        /// <summary>
        /// Metodo para ejecutar el Dispose o Release de cada Matcher de ser necesario
        /// liberar temas en cada tecnologia (Ej. Relaese License en VF6).
        /// </summary>
        ///<returns>0-OK | -1-Error</returns>
        public int Release()
        {
            int ret = 0;

            lock (_matchersAvailables)
            {
                try
                {
                    foreach (MatcherInstance oMatcher in _matchersAvailables)
                    {
                        if (oMatcher.Matcher is IDisposable) oMatcher.Matcher.Dispose();
                        if (oMatcher.Extractor is IDisposable) oMatcher.Extractor.Dispose();
                        if (oMatcher.Template is IDisposable) oMatcher.Template.Dispose();
                    }
                    _matchersAvailables = null;
                }
                catch (Exception ex)
                {
                    ret = -1;
                    log.Error("MatcherManager.Release", ex);
                }
            }
            return ret;
        }

        ///<summary>
        /// Dado un AF y una MT, devuelve un clon del MinutiaeType, si es que existe en
        /// los MatcherAvailables, sino null.
        ///</summary>
        ///<param name="authenticationfactor">Tipo de tecnología a utilizar</param>
        ///<param name="minutiaetype">Tipo d eminucia a utilizar</param>
        ///<returns>Un clon del objeto encontrado o null</returns>
        public MatcherInstance GetMatcherInstance(int authenticationfactor, int minutiaetype)
        {
            MatcherInstance objRet = null;
            try
            {
                if (_matchersAvailables != null)
                {
                    if (_matchersAvailables.ContainsKey(minutiaetype))
                    {
                        objRet = (MatcherInstance)_matchersAvailables[minutiaetype];
                        if (objRet == null || objRet.Authenticationfactor != authenticationfactor)
                        {
                            log.Warn("MatcherManager.GetMatcherInstance - MatcherInstance con MinutiaeType=" +
                                minutiaetype.ToString() + " es nulo o con Authenticationfactor != " +
                                authenticationfactor.ToString() +
                                ((objRet != null) ? "[" + objRet.Authenticationfactor.ToString() + "]" : "[null]"));
                            objRet = null;
                        }
                    }
                }
                else
                {
                    log.Warn("MatcherManager.GetMatcherInstance - MatchersAvailables = null");
                }
            }
            catch (Exception ex)
            {
                log.Error("MatcherManager.GetMatcherInstance Exception", ex);
            }
            return objRet;
        }

        /// <summary>
        /// Retorna la cantidad de instancias existentes en _matchersAvailables
        /// </summary>
        /// <returns></returns>
        public int QuantityMatchersAvailables()
        {
            return _matchersAvailables == null ? 0 : _matchersAvailables.Count;
        }

        /// <summary>
        /// Devuelve el Hashtable completo de Matchers Availables
        /// </summary>
        /// <returns></returns>
        public Hashtable GetMatchersAvailables()
        {
            return _matchersAvailables;
        }

        /// <summary>
        /// Formatea info de Matchers Availables
        /// </summary>
        /// <returns>String conteniendo los valores de los Matchers instanciados</returns>
        public string MatchersAvailablesToString()
        {
            if (_matchersAvailables == null)
            {
                return null;
            }
            string aux = null;
            bool first = true;

            //for (int i = 0; i < _matchersAvailables.Count; i++)
            MatcherInstance ma;
            foreach (object o in _matchersAvailables)
            {
                //MatcherInstance ma = (MatcherInstance)_matchersAvailables[i];
                ma = (MatcherInstance)((DictionaryEntry)o).Value;
                if (first)
                {
                    aux = "Name=" + ma.Name + "-" +
                          "AF=" + ma.Authenticationfactor.ToString() + "-" +
                          "MT=" + ma.Minutiaetype.ToString() + "-" +
                          "THExt=" + ma.Thresholdextract.ToString() + "-" +
                          "THMat=" + ma.Thresholdmatching.ToString() + "-" +
                          "Asm=" + ma.Assembly + "-" +
                          "Mat=" + (ma.Matcher != null ? "OK" : "NULL") + "-" +
                          "Ext=" + (ma.Extractor != null ? "OK" : "NULL") + "-" +
                          "Tpl=" + (ma.Template != null ? "OK" : "NULL");
                    first = false;
                }
                else
                {
                    aux = aux + "|" +
                          "Name=" + ma.Name + "-" +
                          "AF=" + ma.Authenticationfactor.ToString() + "-" +
                          "MT=" + ma.Minutiaetype.ToString() + "-" +
                          "THExt=" + ma.Thresholdextract.ToString() + "-" +
                          "THMat=" + ma.Thresholdmatching.ToString() + "-" +
                          "Asm=" + ma.Assembly + "-" +
                          "Mat=" + (ma.Matcher != null ? "OK" : "NULL") + "-" +
                          "Ext=" + (ma.Extractor != null ? "OK" : "NULL") + "-" +
                          "Tpl=" + (ma.Template != null ? "OK" : "NULL");
                }
            }
            return aux;
        }
    }
}