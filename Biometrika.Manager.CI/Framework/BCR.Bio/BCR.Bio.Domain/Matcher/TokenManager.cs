﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Enum.BioPortal;
using BCR.System.Log.Abstract;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace BCR.Bio.Domain.Matcher
{
    /// <summary>
    /// Esta clase esta destinada a levantar los tipos de token existentes
    /// y crear las clases base, para clonar cuando sea neceario usarlas.
    /// </summary>
    [Serializable]
    public class TokenManager
    {
        private static IAbstractLogger log;

        /// <summary>
        /// De acuerdo al encabezamiento, usa una clase u otra para llenar un token.
        /// </summary>
        /// <param name="token">Token en base 64</param>
        /// <param name="oToken">Objeto Token devuelto</param>
        /// <returns>Codigo de ejecucion</returns>
        public int HandleToken(string token, out IToken oToken)
        {
            oToken = null;
            int ret = (int)Errors.IERR_OK;
            try
            {
                if (token == null)
                {
                    log.Debug("HandleToken - parametro nulo");
                    return (int)Errors.IERR_NULL_TOKEN;
                }

                TokenInstance tokeninstance = (TokenInstance)tokensAvailables[token.Trim().Substring(0, 8)];

                if (tokeninstance != null)
                {
                    ret = tokeninstance.Token.Extract(token);
                    if (ret == (int)Errors.IERR_OK)
                    {
                        oToken = tokeninstance.Token;
                    }
                }
                else
                {
                    ret = (int)Errors.IERR_UNKNOWN_TOKEN;
                }

                //if (token.Trim().Substring(0, 8).Equals("JV010501"))
                //{
                //    //oToken = new TokenJV010501(token);
                //    return ret;
                //}

                //if (token.Trim().Substring(0, 6).Equals("AV0305") ||
                //    token.Trim().Substring(0, 6).Equals("AE0305"))
                //{
                //    //oToken = new TokenAV0305(token);
                //    return ret;
                //}

                //if (token.Trim().Substring(0, 8).Equals("AV040101") ||
                //    token.Trim().Substring(0, 8).Equals("AV030901"))
                //{
                //    //oToken = new TokenAV040101(token);
                //    return ret;
                //}
            }
            catch (Exception ex)
            {
                ret = (int)Errors.IERR_UNKNOWN;
                log.Error("TokenManager.HandleToken", ex);
            }
            return ret;
        }

        #region Serializable

        private List<TokenDefinition> tokensConfigured;

        ///<summary>
        /// Lista de Matchers configurados en config XML.
        ///</summary>
        public List<TokenDefinition> TokensConfigured
        {
            get { return tokensConfigured; }
            set { tokensConfigured = value; }
        }

        #endregion Serializable

        #region Internal Operations

        private Hashtable tokensAvailables = new Hashtable();

        ///<summary>
        /// Tabla indexada por Prefix, con instancias creadas para trabajar.
        ///</summary>
        internal Hashtable TokensAvailables
        {
            get { return tokensAvailables; }
            set { tokensAvailables = value; }
        }

        ///<summary>
        /// Recorre la estructura de Matchers Configured, y va generando las
        /// instancias en MatcherAvailables.
        ///</summary>
        ///<returns>0-OK | -1-Error</returns>
        public int Initialization(IAbstractLogger logger)
        {
            log = logger;
            int ret = 0;
            TokenInstance tokenInstance;

            lock (tokensAvailables)
            {
                try
                {
                    tokensAvailables = new Hashtable();
                    bool tokenInstanced;
                    foreach (TokenDefinition oToken in tokensConfigured)
                    {
                        Assembly asmLogin = Assembly.Load(oToken.Assembly);
                        tokenInstance = new TokenInstance(oToken.Name,
                                                 oToken.Prefix,
                                                 oToken.Assembly,
                                                 oToken.Parameters);
                        tokenInstanced = true;
                        //Type currenttype = null;
                        object instance;

                        Type[] types = null;
                        try
                        {
                            types = asmLogin.GetTypes();
                        }
                        catch (Exception exloader)
                        {
                            log.Warn("TokenManager.Initialization.Loader Warn - Activator.CreateInstance(t)", exloader);
                        }

                        foreach (Type type in types)
                        {
                            try
                            {
                                instance = null;
                                try
                                {
                                    instance = Activator.CreateInstance(type);
                                }
                                catch (Exception exC)
                                {
                                    log.Warn("TokenManager.Initialization Warn - Activator.CreateInstance(t)", exC);
                                    instance = null;
                                }

                                if (instance != null)
                                {
                                    if (instance is IToken)
                                    {
                                        tokenInstance.Token = (IToken)instance;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error("TokenManager.Initialization - Creando instancias de Prefix=" + tokenInstance.Prefix, ex);
                                tokenInstanced = false;
                            }
                        }

                        //Agrego la minucia con las instancias creadas, si se instancio lo necesario
                        if (tokenInstanced)
                        {
                            tokensAvailables.Add(tokenInstance.Prefix, tokenInstance);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ret = -1;
                    log.Error("TokenManager.Initialization", ex);
                }
            }
            return ret;
        }

        /// <summary>
        /// Metodo para ejecutar el Dispose o Release de cada Matcher de ser necesario
        /// liberar temas en cada tecnologia (Ej. Relaese License en VF6).
        /// </summary>
        ///<returns>0-OK | -1-Error</returns>
        public int Release()
        {
            int ret = 0;

            lock (tokensAvailables)
            {
                try
                {
                    //foreach (TokenInstance oToken in _tokensAvailables)
                    //{
                    //    if (oToken.Token is IDisposable) oToken.Token.Dispose();
                    //}
                    tokensAvailables = null;
                }
                catch (Exception ex)
                {
                    ret = -1;
                    log.Error("TokenManager.Release", ex);
                }
            }
            return ret;
        }

        ///<summary>
        /// Dado un prefix, devuelve un clon del token, si es que existe en
        /// los TokenAvailables, sino null.
        ///</summary>
        ///<param name="prefix">Tipo token a procesar</param>
        ///<returns>Un clon del objeto encontrado o null</returns>
        public MatcherInstance GetTokenInstance(string prefix)
        {
            MatcherInstance objRet = null;
            try
            {
                if (tokensAvailables != null)
                {
                    if (tokensAvailables.ContainsKey(prefix))
                    {
                        objRet = (MatcherInstance)tokensAvailables[prefix];
                        if (objRet == null)
                        {
                            log.Warn("TokenManager.GetMatcherInstance - TokenInstance con Prefix=" +
                                prefix + " - " + ((objRet != null) ? "[OK]" : "[null]"));
                            objRet = null;
                        }
                    }
                }
                else
                {
                    log.Warn("TokenManager.GetMatcherInstance - TokensrsAvailables = null");
                }
            }
            catch (Exception ex)
            {
                log.Error("TokenManager.GetTokenInstance Exception", ex);
            }
            return objRet;
        }

        /// <summary>
        /// Retorna la cantidad de instancias existentes en _matchersAvailables
        /// </summary>
        /// <returns></returns>
        public int QuantityTokensAvailables()
        {
            return tokensAvailables == null ? 0 : tokensAvailables.Count;
        }

        /// <summary>
        /// Devuelve el Hashtable completo de Matchers Availables
        /// </summary>
        /// <returns></returns>
        public Hashtable GetTokensAvailables()
        {
            return tokensAvailables;
        }

        /// <summary>
        /// Formatea info de Matchers Availables
        /// </summary>
        /// <returns>String conteniendo los valores de los Matchers instanciados</returns>
        public string TokensAvailablesToString()
        {
            if (tokensAvailables == null)
            {
                return null;
            }
            string aux = null;
            bool first = true;

            //for (int i = 0; i < _matchersAvailables.Count; i++)
            TokenInstance ma;
            foreach (object o in tokensAvailables)
            {
                //MatcherInstance ma = (MatcherInstance)_matchersAvailables[i];
                ma = (TokenInstance)((DictionaryEntry)o).Value;
                if (first)
                {
                    aux = "Name=" + ma.Name + "-" +
                          "Prefix=" + ma.Prefix.ToString() + "-" +
                          "Asm=" + ma.Assembly + "-" +
                          "Tok=" + (ma.Token != null ? "OK" : "NULL");
                    first = false;
                }
                else
                {
                    aux = aux + "|" +
                         "Name=" + ma.Name + "-" +
                          "Prefix=" + ma.Prefix.ToString() + "-" +
                          "Asm=" + ma.Assembly + "-" +
                          "Tok=" + (ma.Token != null ? "OK" : "NULL");
                }
            }
            return aux;
        }
    }

        #endregion Internal Operations
}