﻿using BCR.Bio.Domain.Interface;
using BCR.System.Log.Abstract;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace BCR.Bio.Domain.Matcher
{
    /// <summary>
    /// Esta clase esta destinada a levantar los tipos de matcher existentes
    /// y crear las clases base, para clonar cuando sea neceario usarlas.
    /// </summary>
    [Serializable]
    public class ConnectorManager
    {
        private static IAbstractLogger log;

        #region Serializable

        private List<ConnectorDefinition> _connectorsConfigured;

        ///<summary>
        /// Lista de Connectors configurados en config XML.
        ///</summary>
        public List<ConnectorDefinition> ConnectorsConfigured
        {
            get { return _connectorsConfigured; }
            set { _connectorsConfigured = value; }
        }

        #endregion Serializable

        #region Internal Operations

        private Hashtable _connectorsAvailables = new Hashtable();

        ///<summary>
        /// Tabla indexada por MinutiaeType, con instancias creadas para trabajar.
        ///</summary>
        internal Hashtable ConnectorsAvailables
        {
            get { return _connectorsAvailables; }
            set { _connectorsAvailables = value; }
        }

        ///<summary>
        /// Recorre la estructura de Matchers Configured, y va generando las
        /// instancias en MatcherAvailables.
        ///</summary>
        ///<returns>0-OK | -1-Error</returns>
        public int Initialization(IAbstractLogger abstractLogger)
        {
            log = abstractLogger;
            int ret = 0;
            ConnectorInstance ci;

            lock (_connectorsAvailables)
            {
                try
                {
                    _connectorsAvailables = new Hashtable();
                    bool connectorInstanced;
                    foreach (ConnectorDefinition oConnector in _connectorsConfigured)
                    {
                        Assembly asmLogin = Assembly.Load(oConnector.Assembly);
                        ci = new ConnectorInstance(oConnector.ConnectorId,
                                                   oConnector.Name,
                                                   oConnector.Assembly,
                                                   oConnector.Config);
                        connectorInstanced = true;
                        //Type currenttype = null;
                        object instance;
                        foreach (Type t in asmLogin.GetTypes())
                        {
                            try
                            {
                                if ((typeof(IConnector)).IsAssignableFrom(t))
                                {
                                    instance = Activator.CreateInstance(t);

                                    if (instance is IConnector) //(t.Name.IndexOf("Matcher") > -1)
                                    {
                                        ci.Connector = (IConnector)instance;
                                        ci.Connector.ConnectorId = oConnector.ConnectorId;
                                        ci.Connector.Config = oConnector.Config;
                                    }
                                    instance = null;
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error("ConnectorManager.Initialization - Creando instancias de Connector = " +
                                          oConnector.ConnectorId + " - " + oConnector.Name, ex);
                                connectorInstanced = false;
                            }
                        }

                        //Agrego la minucia con la sinstancias creadas, si se instancio lo necesario
                        if (connectorInstanced)
                        {
                            _connectorsAvailables.Add(ci.ConnectorId, ci);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ret = -1;
                    log.Error("ConnectorManager.Initialization", ex);
                }
            }
            return ret;
        }

        /// <summary>
        /// Metodo para ejecutar el Dispose o Release de cada Connector de ser necesario
        /// liberar temas en cada tecnologia.
        /// </summary>
        ///<returns>0-OK | -1-Error</returns>
        public int Release()
        {
            int ret = 0;

            lock (_connectorsAvailables)
            {
                try
                {
                    foreach (ConnectorInstance oConnector in _connectorsAvailables)
                    {
                        if (oConnector.Connector is IDisposable) oConnector.Connector.Dispose();
                    }
                    _connectorsAvailables = null;
                }
                catch (Exception ex)
                {
                    ret = -1;
                    log.Error("ConnectorManager.Release", ex);
                }
            }
            return ret;
        }

        ///<summary>
        /// Dado un ConnectorId, si es que existe en los MatcherAvailables, devuelve una instancia, sino null.
        ///</summary>
        ///<param name="connectorid">Identificador del conector</param>
        ///<returns>Un clon del objeto encontrado o null</returns>
        public ConnectorInstance GetConnectorInstance(string connectorid)
        {
            ConnectorInstance objRet = null;
            try
            {
                if (_connectorsAvailables != null)
                {
                    if (_connectorsAvailables.ContainsKey(connectorid))
                    {
                        objRet = (ConnectorInstance)_connectorsAvailables[connectorid];
                        if (objRet == null)
                        {
                            log.Warn("ConnectorManager.GetConnectorInstance - ConnectorInstance con ConnectorId=" +
                                     connectorid + " es nulo");
                        }
                    }
                }
                else
                {
                    log.Warn("ConnectorManager.GetConnectorInstance - ConnectorsAvailables = null");
                }
            }
            catch (Exception ex)
            {
                log.Error("ConnectorManager.GetConnectorInstance Exception", ex);
            }
            return objRet;
        }

        /// <summary>
        /// Retorna la cantidad de instancias existentes en _connectorsAvailables
        /// </summary>
        /// <returns></returns>
        public int QuantityConnectorsAvailables()
        {
            return _connectorsAvailables == null ? 0 : _connectorsAvailables.Count;
        }

        /// <summary>
        /// Devuelve el Hashtable completo de Connector Availables
        /// </summary>
        /// <returns></returns>
        public Hashtable GetConnectorsAvailables()
        {
            return _connectorsAvailables;
        }

        /// <summary>
        /// Formatea info de Connectors Availables
        /// </summary>
        /// <returns>String conteniendo los valores de los Connectors instanciados</returns>
        public string ConnecotorsAvailablesToString()
        {
            if (_connectorsAvailables == null)
            {
                return null;
            }
            string aux = null;
            bool first = true;

            //for (int i = 0; i < _matchersAvailables.Count; i++)
            ConnectorInstance ci;
            foreach (object o in _connectorsAvailables)
            {
                //MatcherInstance ma = (MatcherInstance)_matchersAvailables[i];
                ci = (ConnectorInstance)((DictionaryEntry)o).Value;
                if (first)
                {
                    aux = "ConnectorId=" + ci.ConnectorId + "-" +
                          "Name=" + ci.Name + "-" +
                          "Asm=" + ci.Assembly + "-" +
                          "Connector=" + (ci.Connector != null ? "OK" : "NULL");
                    first = false;
                }
                else
                {
                    aux = aux + "|" +
                          "|ConnectorId=" + ci.ConnectorId + "-" +
                          "|Name=" + ci.Name + "-" +
                          "|Asm=" + ci.Assembly + "-" +
                          "|Connector=" + (ci.Connector != null ? "OK" : "NULL");
                }
            }
            return aux;
        }


    }

        #endregion Internal Operations
}