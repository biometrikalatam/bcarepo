﻿using System;

namespace BCR.Bio.Domain
{
    public class AnomalyResult
    {
        public bool IsAnomaly { get; set; }

        public string Message { get; set; }

        public string ClaimExpected { get; set; }

        public DateTime? LastDate { get; set; }

        public string LastClaim { get; set; }
    }
}