﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    public class Holding : AbstractEntity, IHolding
    {
        public virtual string Name { get; set; }

        public virtual  string Address { get; set; }

        public virtual DateTime? Endate { get; set; }

        public virtual  string AdditionalData { get; set; }

        public virtual IList<Company> Companies { get; set; }
    }
}