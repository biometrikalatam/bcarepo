﻿using BCR.Bio.Domain.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Clase de dominio que apunta a la tabla Event, sirve para almacenar eventos con horario y sin horario definido
    /// </summary>
    public class Event : AbstractEntity
    {
        public virtual string Description { get; set; }

        public virtual string Comment { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual Company Company { get; set; }

        public virtual HQ HQ { get; set; }
        /// <summary>
        /// Grupo de residentes asociados
        /// </summary>
        public virtual Group GroupResident { get; set; }
        /// <summary>
        /// Grupo de visitas asociadas
        /// </summary>
        public virtual Group GroupVisitor { get; set; }
        /// <summary>
        /// Tipo de ingreso del evento
        /// </summary>
        public virtual EventType EventType { get; set; }
        /// <summary>
        /// Motivo del evento
        /// </summary>
        public virtual EventClass EventClass { get; set; }
        /// <summary>
        /// Proveedor asociado
        /// </summary>
        public virtual Provider Provider { get; set; }
        /// <summary>
        /// Lista de restricciones de tiempo
        /// </summary>
        public virtual IList<TimeRestriction> TimeRestrictions { get; set; }
        /// <summary>
        /// Residente autorizador
        /// </summary>
        public virtual Resident ResidentAuthorizator { get; set; }
        /// <summary>
        /// Lugar donde se realizara el evento
        /// </summary>
        public virtual Unity Unity { get; set; }

        public virtual bool IsList // EventType no es para esto?
        {
            get
            {
                bool isList = false;

                if (GroupResident != null || GroupVisitor != null)
                {
                    isList = true;
                }

                return isList;
            }
        }

        public virtual IList<VisitorTracking> TrackingList { get; set; }

        public virtual bool CanDelete
        {
            get
            {
                if (TimeRestrictions != null && TimeRestrictions.Count > 0)
                {
                    if (DateTime.Now > TimeRestrictions[0].StartTime.Add(TimeRestrictions[0].DayStartTime))
                        return false;
                }

                return true;
            }
        }

        public virtual bool CanEdit
        {
            get
            {
                if (TimeRestrictions != null && TimeRestrictions.Count > 0)
                {
                    if (DateTime.Now > TimeRestrictions[0].StartTime.Add(TimeRestrictions[0].DayStartTime))
                        return false;
                }

                return true;
            }
        }

        public virtual bool CanOperate
        {
            get
            {
                bool res = false;
                if (TimeRestrictions != null && TimeRestrictions.Count > 0)
                {
                    if (DateTime.Now > TimeRestrictions[0].StartTime.Add(TimeRestrictions[0].DayStartTime)
                        && DateTime.Now < TimeRestrictions[0].EndTime.Add(TimeRestrictions[0].DayEndTime))
                    {
                        if (DateTime.Now.TimeOfDay > TimeRestrictions[0].DayStartTime)
                        {
                            foreach (var item in TimeRestrictions)
                            {
                                if (item.Monday)
                                {
                                    if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                                    {
                                        DateTime controlDate = DateTime.Now.Date.Add(TimeRestrictions[0].DayEndTime);
                                        if (DateTime.Now < controlDate)
                                        {
                                            res = true;
                                            break;
                                        }
                                        else
                                        {
                                            res = false;
                                            break;
                                        }
                                    }
                                }

                                if (item.Tuesday)
                                {
                                    if (DateTime.Now.DayOfWeek == DayOfWeek.Tuesday)
                                    {
                                        DateTime controlDate = DateTime.Now.Date.Add(TimeRestrictions[0].DayEndTime);
                                        if (DateTime.Now < controlDate)
                                        {
                                            res = true;
                                            break;
                                        }
                                        else
                                        {
                                            res = false;
                                            break;
                                        }
                                    }
                                }

                                if (item.Wednesday)
                                {
                                    if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
                                    {
                                        DateTime controlDate = DateTime.Now.Date.Add(TimeRestrictions[0].DayEndTime);
                                        if (DateTime.Now < controlDate)
                                        {
                                            res = true;
                                            break;
                                        }
                                        else
                                        {
                                            res = false;
                                            break;
                                        }
                                    }
                                }

                                if (item.Thursday)
                                {
                                    if (DateTime.Now.DayOfWeek == DayOfWeek.Thursday)
                                    {
                                        DateTime controlDate = DateTime.Now.Date.Add(TimeRestrictions[0].DayEndTime);
                                        if (DateTime.Now < controlDate)
                                        {
                                            res = true;
                                            break;
                                        }
                                        else
                                        {
                                            res = false;
                                            break;
                                        }
                                    }
                                }

                                if (item.Friday)
                                {
                                    if (DateTime.Now.DayOfWeek == DayOfWeek.Friday)
                                    {
                                        DateTime controlDate = DateTime.Now.Date.Add(TimeRestrictions[0].DayEndTime);
                                        if (DateTime.Now < controlDate)
                                        {
                                            res = true;
                                            break;
                                        }
                                        else
                                        {
                                            res = false;
                                            break;
                                        }
                                    }
                                }

                                if (item.Saturday)
                                {
                                    if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
                                    {
                                        DateTime controlDate = DateTime.Now.Date.Add(TimeRestrictions[0].DayEndTime);
                                        if (DateTime.Now < controlDate)
                                        {
                                            res = true;
                                            break;
                                        }
                                        else
                                        {
                                            res = false;
                                            break;
                                        }
                                    }
                                }

                                if (item.Sunday)
                                {
                                    if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        DateTime controlDate = DateTime.Now.Date.Add(TimeRestrictions[0].DayEndTime);
                                        if (DateTime.Now < controlDate)
                                        {
                                            res = true;
                                            break;
                                        }
                                        else
                                        {
                                            res = false;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return res;
            }
        }

        public virtual bool IsEnded
        {
            get
            {
                bool res = false;

                if (TimeRestrictions != null && TimeRestrictions.Count > 0)
                {
                    if (DateTime.Now > TimeRestrictions[0].EndTime.Add(TimeRestrictions[0].DayEndTime))
                    {
                        res = true;
                    }
                }
                return res;
            }
        }
    }
}