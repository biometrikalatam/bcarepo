﻿using BCR.Bio.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Define una estrucura para levantar los connectors configurados y crea las clases
    /// para los posteriores clones cuando se deba usar.
    /// </summary>
    [Serializable]
    public class ConnectorInstance
    {
        internal ConnectorInstance(string connectorid, string name, string assembly, DynamicData config)
        {
            _connectorid = connectorid;
            _name = name;
            _assembly = assembly;
            _config = config;
        }

        private string _connectorid;
        private string _name;
        private string _assembly;
        private DynamicData _config;
        private IConnector _connector;

        ///<summary>
        ///</summary>
        public string ConnectorId
        {
            get { return _connectorid; }
            set { _connectorid = value; }
        }

        ///<summary>
        ///</summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        ///<summary>
        ///</summary>
        public DynamicData Config
        {
            get { return _config; }
            set { _config = value; }
        }

        ///<summary>
        ///</summary>
        public string Assembly
        {
            get { return _assembly; }
            set { _assembly = value; }
        }

        ///<summary>
        ///</summary>
        public IConnector Connector
        {
            get { return _connector; }
            set { _connector = value; }
        }

    }
}
