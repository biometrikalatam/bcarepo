﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;

namespace BCR.Bio.Domain
{
    /// <summary>
    /// Clase de dominio que hace referencia a la tabla IdentityBlacklist que contiene los datos de Identificaciones con bloqueo de acceso,
    /// la interpretacion y funcionamiento de los datos son determinados por la capa de presentacion que es donde se decide si se bloquea un acceso o
    /// solo s da una alerta
    /// </summary>
    public class IdentityBlackList : AbstractEntity, IIdentityBlackList
    {
        public virtual string TypeId { get; set; }

        public virtual string ValueId { get; set; }
        /// <summary>
        /// Compañia a la que pertenece la identificacion bloqueada
        /// </summary>
        public virtual ICompany Company { get; set; }
        /// <summary>
        /// Razón de entrada a lista negra
        /// </summary>
        public virtual string RazonIn { get; set; }
        /// <summary>
        /// Usuario que hace el ingreso
        /// </summary>
        public virtual IUser UserIn { get; set; }
        /// <summary>
        /// Razón por la que la indetificacion fue quitada de lista negra
        /// </summary>
        public virtual string RazonOut { get; set; }
        /// <summary>
        /// Usuario que quita de lista negra
        /// </summary>
        public virtual IUser UserOut { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual BpIdentity Identity { get; set; }
    }
}