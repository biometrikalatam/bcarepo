﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Domain
{
    public class IdentityContactForm
    {
        public virtual int Id { get; set; }
        public virtual BpIdentity Identity { get; set; }
        public virtual ContactForm ContactForm { get; set; }
        public virtual string ValueForm { get; set; }
    }
}
