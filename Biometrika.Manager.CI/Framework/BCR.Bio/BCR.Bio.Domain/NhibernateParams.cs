﻿namespace BCR.Bio.Domain
{
    public class NhibernateParams
    {
        public string ConnectionString { get; set; }

        public string[] Mappings { get; set; }

        public bool EnableAudit { get; set;  }
    }
}