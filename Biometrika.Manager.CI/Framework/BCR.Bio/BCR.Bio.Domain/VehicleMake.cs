﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;

namespace BCR.Bio.Domain
{
    public class VehicleMake : AbstractEntityItem<int>, IVehicleMake
    {
    }
}