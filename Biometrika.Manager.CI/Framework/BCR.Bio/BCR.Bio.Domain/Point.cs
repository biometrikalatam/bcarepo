﻿using BCR.Bio.Domain.Abstract;
using BCR.Bio.Domain.Interface;
using System;

namespace BCR.Bio.Domain
{
    // TODO : separar Point de Point de autónomos
    /// <summary>
    /// Clase e dominio que hace referencia a la tabla Point
    /// </summary>
    public class Point : AbstractEntity, IPoint 
    {
        
        public Point()
        {
         
        }
        public Point(int pointId)
        {
            Id = pointId;
        }

        public virtual string Code { get; set; }

        public virtual string Name { get; set; }

        public virtual string IPaddress { get; set; }

        public virtual string SensorId { get; set; }

        public virtual string MacAddress { get; set; }

        public virtual string Address { get; set; }

        public virtual string Phone { get; set; }

        public virtual string OtherPhone { get; set; }

        public virtual string ContactName { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual int HasPrinter { get; set; }

        public virtual int HasMail { get; set; }

        public virtual bool Is_Dev { get; set; }

        public virtual int Dev_Id { get; set; }

        public virtual int Dev_baudrates { get; set; }

        public virtual string Dev_serialnumber { get; set; }

        public virtual int Dev_number { get; set; }

        public virtual int Dev_umbral { get; set; }

        public virtual int Dev_attendancemode { get; set; }

        public virtual string Dev_password { get; set; }

        public virtual int Dev_comport { get; set; }

        public virtual int Dev_modeautentication { get; set; }

        public virtual int Dev_dooropen { get; set; }

        public virtual string Dev_ipserver { get; set; }

        public virtual string Dev_gateway { get; set; }

        public virtual int Dev_userlength { get; set; }

        public virtual int Dev_display12 { get; set; }

        public virtual int Dev_refresh { get; set; }

        public virtual DateTime? Dev_daterefresh { get; set; }
        /// <summary>
        /// Sector al que pertenece el Point
        /// </summary>
        public virtual Sector Sector { get; set; }
        /// <summary>
        /// HQ al que pertenece el Point
        /// </summary>
        public virtual HQ HQ { get; set; }

        public virtual void Disable()
        {
            EndDate = DateTime.Now;
        }

        public virtual void Enable()
        {
            EndDate = null;
        }
    }
}