﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class TownService : Service<ITown>
    {
        public TownService(TownRepository repository)
            : base(repository)
        {
        }

        protected override bool ValidateForCreate(ITown t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(ITown t)
        {
            return true;
        }
    }
}