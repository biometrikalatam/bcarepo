﻿using System;
using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class TrackingActionService : Service<ITrackingAction>
    {
        TrackingActionRepository localRepo;

        public TrackingActionService(TrackingActionRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(ITrackingAction t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(ITrackingAction t)
        {
            throw new global::System.NotImplementedException();
        }

        public ITrackingAction GetByName(string description)
        {
            return localRepo.GetByName(description);
        }

        public TrackingAction GetByValue(int value)
        {
            return localRepo.GetByValue(value);
        }
    }
}