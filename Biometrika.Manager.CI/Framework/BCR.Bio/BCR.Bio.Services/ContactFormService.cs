﻿using BCR.Bio.Domain;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCR.Bio.Infraestructura.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class ContactFormService : Service<ContactForm>
    {
        ContactFormRepository localRepo;

        public ContactFormService(ContactFormRepository repository) : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(ContactForm t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(ContactForm t)
        {
            return true;
        }

        public IList<ContactForm> GetByFilter(ContactFormFilter filter)
        {
            return localRepo.GetByFilter(filter);
        }
    }
}
