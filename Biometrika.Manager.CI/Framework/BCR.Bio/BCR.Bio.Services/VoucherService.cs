﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BCR.Bio.Services.Abstract;
using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class VoucherService : Service<IVoucher>
    {
        private VoucherRepository repositoryLocal;

        public VoucherService(VoucherRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IVoucher t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(IVoucher t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<IVoucher> GetByCompanyId(int companyId)
        {
            return repositoryLocal.GetByCompanyId(companyId);
        }

        public IList<IVoucher> GetByFilter(VoucherFilterDTO filter)
        {
            return repositoryLocal.GetByFilter(filter);
        }

    }


}
