﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Services
{
    public class RegisterMethodService: Service<RegisterMethod>
    {
        RegisterMethodRepository localRepo;

        public RegisterMethodService(RegisterMethodRepository repositoryParams) : base(repositoryParams)
        {
            localRepo = repositoryParams;
        }

        public IList<RegisterMethodDTO> GetAll()
        {
            return localRepo.GetAll();


        }

        protected override Boolean ValidateForCreate(RegisterMethod t)
        {            
            return true;
        }

        protected override Boolean ValidateForUpdate(RegisterMethod t)
        {            
            return true;
        }

    }
}
