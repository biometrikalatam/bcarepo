﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;

namespace BCR.Bio.Services
{
    public class MarkTypeService : Service<MarkType>
    {
        public MarkTypeService(MarkTypeRepository repository)
            : base(repository)
        {
        }

        protected override bool ValidateForCreate(MarkType t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(MarkType t)
        {
            throw new NotImplementedException();
        }
    }
}