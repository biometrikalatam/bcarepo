﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class RuleScopeService : Service<RuleScope>
    {
        RuleScopeRepository localRepository;
        public RuleScopeService(RuleScopeRepository repository)
            : base(repository)
        {
            localRepository = repository;
        }

        protected override bool ValidateForCreate(RuleScope t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(RuleScope t)
        {
            return true;
        }

        public IList<RuleScope> GetByFilter(RuleScopeFilter filter)
        {
            return localRepository.GetByFilter(filter);
        }
    }
}