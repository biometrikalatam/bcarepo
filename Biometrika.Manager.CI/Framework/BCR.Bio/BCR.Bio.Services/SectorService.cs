﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Services
{
    public class SectorService : Service<ISector>
    {
        private SectorRepository repositoryLocal;

        public SectorService(SectorRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(ISector t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(ISector t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        protected override bool IsEndDatableThenDo(ISector t, bool activate)
        {
            if (!activate)
                t.EndDate = DateTime.Now;
            else
                t.EndDate = null;

            return true;
        }

        public IQueryable<Sector> GetByHolding(int holdingId)
        {
            return repositoryLocal.GetByHolding(holdingId);
        }

        public IQueryable<Sector> GetByCompany(int companyId)
        {
            return repositoryLocal.GetByCompany(companyId);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="t"></param>
        public override void CascadeDisable(ISector t)
        {
            // disable HQs

            foreach (IPoint point in t.Points)
                point.Disable();
            //serviceAccess.PointService.Disable(point);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public override Boolean CanBeEnabled(ISector t)
        {
            bool canBeEnabled = true;

            foreach (ICompany company in t.Companies)
            {
                canBeEnabled = canBeEnabled && (company.EndDate == null);
            }

            return canBeEnabled;
        }

        public IList<Sector> GetByFilter(SectorFilterDTO filter)
        {
            return repositoryLocal.GetByFilter(filter);
        }
    }
}