﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class BpTxService : Service<BpTx>
    {
        private BpTxRepository repositoryLocal;

        /// <summary>
        ///
        /// </summary>
        /// <param name="validationDictionary"></param>
        /// <param name="repository"></param>
        public BpTxService(BpTxRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(BpTx t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(BpTx t)
        {
            return true;
        }

        public int GetQCurrentClient()
        {
            return repositoryLocal.GetQCurrentClient();
        }

        public BpTx GetByTrackId(string idtrack)
        {
            return repositoryLocal.GetByTrackId(idtrack);
        }

        public int CreateExtended(BpTx bpTx, List<BpTxConx> conxList)
        {
            if (conxList != null)
            {
                foreach (BpTxConx conxAux in conxList)
                {
                    //conxAux.BpTx = transaccion;
                    AddTxConx(bpTx, conxAux);
                }
            }
            //if (!AdminBpTx.Write(transaccion, out transaccionsaved, out msgErr))
            //{
            //    transaccionsaved = null;
            //}
            return bpTx.Id;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bpTx"></param>
        /// <param name="conxAux"></param>
        private void AddTxConx(BpTx bpTx, BpTxConx conxAux)
        {
            repositoryLocal.AddTxConx(bpTx, conxAux);
        }
    }
}