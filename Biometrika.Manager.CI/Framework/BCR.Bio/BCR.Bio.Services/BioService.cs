﻿using BCR.System.Configuration;
using BCR.System.Log.Abstract;

namespace BCR.Bio.Services
{
    /// <summary>
    /// Servicio inicializador del framework base
    /// </summary>
    public class BioServicex
    {
        private static SystemConfiguration sysConfiguration;

        //1-3
        public BioServicex(SystemConfiguration systemConfiguration)
        {
            sysConfiguration = systemConfiguration;
        }
 
        public BioUnitOfWork GetBioUnitOfWork()
        {
            return new BioUnitOfWork(sysConfiguration.HibernateParams);
        }
    }
}