﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BCR.Bio.Services.Abstract;
using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class LunchbreakService : Service<ILunchbreak>
    {
        private LunchbreakRepository repositoryLocal;

        public LunchbreakService(LunchbreakRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(ILunchbreak t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(ILunchbreak t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<ILunchbreak> GetByCompanyId(int companyId)
        {
            return repositoryLocal.GetByCompanyId(companyId);
        }
    }
}
