﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class EventTypeService : Service<EventType>
    {
        EventTypeRepository localRepo;
        public EventTypeService(EventTypeRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(EventType t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(EventType t)
        {
            throw new NotImplementedException();
        }

        public IList<EventType> GetList()
        {
            return localRepo.GetList();
        }
    }
}