﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;

namespace BCR.Bio.Services
{
    public class BCR_AppsService : Service<BCR_Apps>
    {
        private BCR_AppsRepository repositoryLocal;

        public BCR_AppsService(BCR_AppsRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(BCR_Apps t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(BCR_Apps t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }
    }
}