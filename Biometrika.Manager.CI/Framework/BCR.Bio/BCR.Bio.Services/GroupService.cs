﻿using BCR.Bio.Domain;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCR.Bio.Infraestructura.Interface;
using BCR.Bio.Infraestructura;

namespace BCR.Bio.Services
{
    public class GroupService : Service<Group>
    {
        GroupRepository localRepo;

        public GroupService(GroupRepository repository) : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(Group t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(Group t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<Group> GetByVisitorId(int visitorId)
        {
            return localRepo.GetByVisitorId(visitorId);
        }
    }
}
