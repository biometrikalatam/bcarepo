﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class VehicleService : Service<IVehicle>
    {
        private VehicleRepository repositoryLocal;

        public VehicleService(VehicleRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IVehicle t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(IVehicle t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IVehicle GetById(int id, int companyId, bool withResidents = false, bool withVisitors = false, bool withProvider = false)
        {
            return repositoryLocal.GetById(id, companyId, withResidents, withVisitors, withProvider);
        }

        public IList<IVehicle> GetByCompanyAndPlateNumber(int companyId, string plateNumber)
        {
            return repositoryLocal.GetByCompanyAndPlateNumber(companyId, plateNumber);
        }

        public IList<IVehicle> GetByCompanyId(int companyId, bool onlyResidents = false, bool onlyVisitors = false)
        {
            return repositoryLocal.GetByCompanyId(companyId, onlyResidents, onlyVisitors);
        }

        public IList<IVehicle> GetResidentsVehiclesByCompanyId(int companyId)
        {
            return repositoryLocal.GetByCompanyId(companyId, onlyResidents: true);
        }

        public IList<Vehicle> GetByFilter(IPaginacion paginacion, int companyId, Vehicle filter, bool withResidents = false, bool withVisitors = false, bool withProvider = false)
        {
            return repositoryLocal.GetByFilter( paginacion, companyId, filter, withResidents, withVisitors, withProvider);
        }

        public IList<Vehicle> GetByFilter(VehicleFilter filter, bool withResidents = false, bool withVisitors = false, bool withProvider = false)
        {
            return repositoryLocal.GetByFilter(filter, withResidents, withVisitors, withProvider);
        }

        public IList<Vehicle> GetByIdentityAndCompany(IPaginacion paginacion, int companyId,string plateNumber, string typeId, string valueId, bool withResidents = false, bool withVisitors = false, bool withProvider = false)
        {
            return repositoryLocal.GetByIdentityAndCompany(paginacion, companyId, plateNumber, typeId, valueId, withResidents, withVisitors, withProvider);
        }

        public IList<Vehicle> GetByCustomFilter(VehicleFilter filter, bool withResidents = false)
        {
            return repositoryLocal.GetByCustomFilter(filter, withResidents);
        }

        public IList<Vehicle> GetResidentVehiclesByCompanyId(int companyId, bool withResident = false, bool onlyActive = false, bool onlyResidentActive = false)
        {
            return repositoryLocal.GetResidentVehiclesByCompanyId(companyId, withResident, onlyActive, onlyResidentActive);
        }

        public IList<Vehicle> GetByResidentAndCompanyId(VehicleFilter filter, bool withResidents = false, bool withVisitors = false, bool withProvider = false)
        {
            return repositoryLocal.GetByResidentAndCompanyId(filter, withResidents, withVisitors, withProvider);
        }
    }
}