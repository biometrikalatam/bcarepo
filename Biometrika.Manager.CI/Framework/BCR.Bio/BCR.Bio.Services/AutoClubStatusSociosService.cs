﻿using System;
using System.Collections.Generic;
using BCR.Bio.Services.Abstract;
using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;

namespace BCR.Bio.Services
{
    public class AutoClubStatusSociosService : Service<AutoClubStatusSocios>
    {
        private AutoClubStatusSociosRepository repositoryLocal;

        public AutoClubStatusSociosService(AutoClubStatusSociosRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        public AutoClubStatusSocios GetByValueId(string valueId)
        {
            return repositoryLocal.GetByValueId(valueId);
        }

        protected override bool ValidateForCreate(AutoClubStatusSocios t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(AutoClubStatusSocios t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

    }
}
