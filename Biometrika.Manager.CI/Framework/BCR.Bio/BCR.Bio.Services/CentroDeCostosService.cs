﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class CentroDeCostosService : Service<ICentroDeCostos>
    {
        private CentroDeCostosRepository localRepo;

        public CentroDeCostosService(CentroDeCostosRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(ICentroDeCostos t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(ICentroDeCostos t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<CentroDeCostos> GetByFilter(CostCenterFilterDTO filter)
        {
            return localRepo.GetByFilter(filter);
        }
    }
}