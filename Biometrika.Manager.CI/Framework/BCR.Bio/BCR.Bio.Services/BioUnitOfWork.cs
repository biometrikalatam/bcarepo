﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Enum.BioPortal;
using BCR.Bio.Infraestructura;
using BCR.Bio.Infraestructura.Abstract;
using BCR.System.Configuration;
using BCR.System.Log;
using BCR.System.Service;
using BCR.System.Validation;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Services
{
    /// <summary>
    ///
    /// </summary>
    public class BioUnitOfWork : AbstractUnitOfWork
    {
        public ValidationDictionary ValidationDictionary { get; private set; }

        /// <summary>
        ///
        /// </summary>
        /// <param name="nhibernateParams"></param>
        public BioUnitOfWork(NhibernateParams nhibernateParams, ValidationDictionary validationDictionary)
            : base(nhibernateParams)
        {
            ValidationDictionary = validationDictionary;
            FillServiceLocator();
        }

        private void FillServiceLocator()
        {
            ServiceLocator.Add<PointEmployeeRecordRepository, PointEmployeeRecordService>();
            ServiceLocator.Add<TrackingResultRepository, TrackingResultService>();
            ServiceLocator.Add<TrackingActionRepository, TrackingActionService>();
            ServiceLocator.Add<IdentityBlackListRepository, IdentityBlackListService>();
            ServiceLocator.Add<VehicleBlackListRepository, VehicleBlackListService>();
            ServiceLocator.Add<EmployeeRepository, EmployeeService>();
            ServiceLocator.Add<RegisterMethodRepository, RegisterMethodService>();
            ServiceLocator.Add<DepartmentRepository, DepartmentService>();
            ServiceLocator.Add<WorkRepository, WorkService>();
            ServiceLocator.Add<BenefitRepository, BenefitService>();
            ServiceLocator.Add<BenefitOptionRepository, BenefitOptionService>();
            ServiceLocator.Add<EmployeeBenefitOptionRepository, EmployeeBenefitOptionService>();
            ServiceLocator.Add<EmployeeRegisterMethodRepository, EmployeeRegisterMethodService>();

            ServiceLocator.Add<VehicleModelRepository, VehicleModelService>();
            ServiceLocator.Add<VisitorTrackingRepository, VisitorTrackingService>();
            ServiceLocator.Add<VehicleMakeRepository, VehicleMakeService>();
            ServiceLocator.Add<VehicleModelRepository, VehicleModelService>();
            ServiceLocator.Add<VehicleTypeRepository, VehicleTypeService>();

            ServiceLocator.Add<CentroDeCostosRepository, CentroDeCostosService>();
            ServiceLocator.Add<VisitorRepository, VisitorService>();
            ServiceLocator.Add<VehicleRepository, VehicleService>();
            ServiceLocator.Add<ResidentRepository, ResidentService>();
            ServiceLocator.Add<PointRepository, PointService>();
            ServiceLocator.Add<ResidentAuthorizatorRepository, ResidentAuthorizatorService>();

            ServiceLocator.Add<SectorRepository, SectorService>();
            ServiceLocator.Add<HQRepository, HQService>();
            ServiceLocator.Add<TownRepository, TownService>();

            ServiceLocator.Add<CityRepository, CityService>();
            ServiceLocator.Add<HoldingRepository, HoldingService>();

            ServiceLocator.Add<DeptRepository, DeptService>();
            ServiceLocator.Add<BpOriginRepository, BpOriginService>();
            ServiceLocator.Add<BpIdentityRepository, BpIdentityService>();

            ServiceLocator.Add<BpBirRepository, BpBirService>();
            ServiceLocator.Add<BpTxRepository, BpTxService>();

            ServiceLocator.Add<BpVerifiedRepository, BpVerifiedService>();
            ServiceLocator.Add<RolRepository, RolService>();
            ServiceLocator.Add<WorkDayRepository, WorkDayService>();
            ServiceLocator.Add<UserRepository, UserService>();

            ServiceLocator.Add<CompanyRepository, CompanyService>();
            ServiceLocator.Add<CountryRepository, CountryService>();
            ServiceLocator.Add<LogRepository, LogService>();
            ServiceLocator.Add<ProviderRepository, ProviderService>();
            ServiceLocator.Add<VisitorTrakingRouteRepository, VisitorTrakingRouteService>();
            ServiceLocator.Add<MarkTypeRepository, MarkTypeService>();

            ServiceLocator.Add<ZoneRepository, ZoneService>();
            ServiceLocator.Add<BuildingRepository, BuildingService>();
            ServiceLocator.Add<UnityRepository, UnityService>();

            ServiceLocator.Add<EventTypeRepository, EventTypeService>();
            ServiceLocator.Add<EventClassRepository, EventClassService>();
            ServiceLocator.Add<BCR_AppsRepository, BCR_AppsService>();

            ServiceLocator.Add<EventRepository, EventService>();
            ServiceLocator.Add<TimeRestrictionRepository, TimeRestrictionService>();
            ServiceLocator.Add<GroupRepository, GroupService>();

            ServiceLocator.Add<PointRuleRepository, PointRuleService>();
            ServiceLocator.Add<RuleScopeRepository, RuleScopeService>();
         
            ServiceLocator.Add<AccessControlRepository, AccessControlService>();

            ServiceLocator.Add<PointToPointDependencyRepository, PointToPointDependencyService>();

            ServiceLocator.Add<IdentityTypeRepository, IdentityTypeService>();
            ServiceLocator.Add<IdentityStateRepository, IdentityStateService>();
            ServiceLocator.Add<ResidentTrackingRepository, ResidentTrackingService>();
            ServiceLocator.Add<VehicleTrackingRepository, VehicleTrackingService>();
            ServiceLocator.Add<ContactFormRepository, ContactFormService>();
            ServiceLocator.Add<IdentityContactFormRepository, IdentityContactFormService>();
            ServiceLocator.Add<EmployeeTrackingRepository, EmployeeTrackingService>();

            // ServiceLocator.Add<PointEmployeeRecordRepository, PointEmployeeRecordService>());
            ServiceLocator.Add<VisitorTrackingObjetadosRepository, VisitorTrackingObjetadosService>();
            ServiceLocator.Add<VoucherRepository, VoucherService>();
            ServiceLocator.Add<VisitCounterResetDateRepository, VisitCounterResetDateService>();
            ServiceLocator.Add<HolidayRepository, HolidayService>();
            ServiceLocator.Add<HolidayAuthorizedRepository, HolidayAuthorizedService>();

        }

        public ResidentTrackingService ResidentTrackingService => ServiceLocator.GetService<ResidentTrackingService>();
        public IdentityStateService IdentityStateService => ServiceLocator.GetService<IdentityStateService>();
        public IdentityTypeService IdentityTypeService => ServiceLocator.GetService<IdentityTypeService>();
        public EmployeeService EmployeeService => ServiceLocator.GetService<EmployeeService>();
        public RegisterMethodService RegisterMethodService => ServiceLocator.GetService<RegisterMethodService>();
        public DepartmentService DepartmentService => ServiceLocator.GetService<DepartmentService>();
        public WorkService WorkService => ServiceLocator.GetService<WorkService>();
        public BenefitService BenefitService => ServiceLocator.GetService<BenefitService>();
        public BenefitOptionService BenefitOptionService => ServiceLocator.GetService<BenefitOptionService>();
        public EmployeeBenefitOptionService EmployeeBenefitOptionService => ServiceLocator.GetService<EmployeeBenefitOptionService>();
        public EmployeeRegisterMethodService EmployeeRegisterMethodService => ServiceLocator.GetService<EmployeeRegisterMethodService>();

        public VehicleTrackingService VehicleTrackingService => ServiceLocator.GetService<VehicleTrackingService>();

        public AccessControlService AccessControlService => ServiceLocator.GetService<AccessControlService>();

        public PointToPointDependencyService PointToPointDependencyService => ServiceLocator.GetService<PointToPointDependencyService>();
        public ContactFormService ContactFormService => ServiceLocator.GetService<ContactFormService>();
        public IdentityContactFormService IdentityContactFormService => ServiceLocator.GetService<IdentityContactFormService>();
        public EmployeeTrackingService EmployeeTrackingService => ServiceLocator.GetService<EmployeeTrackingService>();
        public VisitorTrackingObjetadosService VisitorTrackingObjetadosService => ServiceLocator.GetService<VisitorTrackingObjetadosService>();
        public VoucherService VoucherService => ServiceLocator.GetService<VoucherService>();
        public HolidayService HolidayService => ServiceLocator.GetService<HolidayService>();
        public HolidayAuthorizedService HolidayAuthorizedService => ServiceLocator.GetService<HolidayAuthorizedService>();
        public VisitCounterResetDateService VisitCounterResetDateService => ServiceLocator.GetService<VisitCounterResetDateService>();

        public RuleScopeService RuleScopeService
        {
            get
            {
                return ServiceLocator.GetService<RuleScopeService>();
            }
        }

        public PointRuleService PointRuleService
        {
            get
            {
                return ServiceLocator.GetService<PointRuleService>();
            }
        }

        public GroupService GroupService // Esto es un wrapper
        {
            get
            {
                return ServiceLocator.GetService<GroupService>();
            }
        }

        public TimeRestrictionService TimeRestrictionService
        {
            get
            {
                return ServiceLocator.GetService<TimeRestrictionService>();
            }
        }

        public EventService EventService
        {
            get
            {
                return ServiceLocator.GetService<EventService>();
            }
        }

        public BCR_AppsService BCR_AppsService
        {
            get
            {
                return ServiceLocator.GetService<BCR_AppsService>();
            }
        }

        public EventTypeService EventTypeService
        {
            get
            {
                return ServiceLocator.GetService<EventTypeService>();
            }
        }

        public EventClassService EventClassService
        {
            get
            {
                return ServiceLocator.GetService<EventClassService>();
            }
        }

        public UnityService UnityService
        {
            get
            {
                return ServiceLocator.GetService<UnityService>();
            }
        }

        public BuildingService BuildingService
        {
            get
            {
                return ServiceLocator.GetService<BuildingService>();
            }
        }

        public ZoneService ZoneService
        {
            get
            {
                return ServiceLocator.GetService<ZoneService>();
            }
        }

        public MarkTypeService MarkTypeService
        {
            get
            {
                return ServiceLocator.GetService<MarkTypeService>();
            }
        }

        public VisitorTrakingRouteService VisitorTrakingRouteService
        {
            get
            {
                return ServiceLocator.GetService<VisitorTrakingRouteService>();
            }
        }

        public ProviderService ProviderService
        {
            get
            {
                return ServiceLocator.GetService<ProviderService>();
            }
        }

        public PointEmployeeRecordService PointEmployeeRecordService
        {
            get
            {
                return ServiceLocator.GetService<PointEmployeeRecordService>();
            }
        }

        public TrackingResultService TrackingResultService
        {
            get
            {
                return ServiceLocator.GetService<TrackingResultService>();
            }
        }

        public TrackingActionService TrackingActionService
        {
            get
            {
                return ServiceLocator.GetService<TrackingActionService>();
            }
        }

        public IdentityBlackListService IdentityBlackListService
        {
            get
            {
                return ServiceLocator.GetService<IdentityBlackListService>();
            }
        }

        public VehicleBlackListService VehicleBlackListService
        {
            get
            {
                return ServiceLocator.GetService<VehicleBlackListService>();
            }
        }

        //public EmployeeService EmployeeService
        //{
        //    get
        //    {
        //        return ServiceLocator.GetService<EmployeeService>();
        //    }
        //}

        public VehicleModelService VehicleModelServices
        {
            get
            {
                return ServiceLocator.GetService<VehicleModelService>();
            }
        }

        public VisitorTrackingService VisitorTrackingService
        {
            get
            {
                return ServiceLocator.GetService<VisitorTrackingService>();
            }
        }

        public VehicleMakeService VehicleMakeService
        {
            get
            {
                return ServiceLocator.GetService<VehicleMakeService>();
            }
        }

        public VehicleModelService VehicleModelService
        {
            get
            {
                return ServiceLocator.GetService<VehicleModelService>();
            }
        }

        public VehicleTypeService VehicleTypeService
        {
            get
            {
                return ServiceLocator.GetService<VehicleTypeService>();
            }
        }

        public CentroDeCostosService CentroDeCostosService
        {
            get
            {
                return ServiceLocator.GetService<CentroDeCostosService>();
            }
        }

        public VisitorService VisitorService
        {
            get
            {
                return ServiceLocator.GetService<VisitorService>();
            }
        }

        public VehicleService VehicleService
        {
            get
            {
                return ServiceLocator.GetService<VehicleService>();
            }
        }

        public ResidentService ResidentService
        {
            get
            {
                return ServiceLocator.GetService<ResidentService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public PointService PointService
        {
            get
            {
                return ServiceLocator.GetService<PointService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public ResidentAuthorizatorService ResidentAuthorizatorService
        {
            get
            {
                return ServiceLocator.GetService<ResidentAuthorizatorService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public SectorService SectorService
        {
            get
            {
                return ServiceLocator.GetService<SectorService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public HQService HQService
        {
            get
            {
                return ServiceLocator.GetService<HQService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public TownService TownService
        {
            get
            {
                return ServiceLocator.GetService<TownService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public CityService CityService
        {
            get
            {
                return ServiceLocator.GetService<CityService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public HoldingService HoldingService
        {
            get
            {
                return ServiceLocator.GetService<HoldingService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public DeptService DeptService
        {
            get
            {
                return ServiceLocator.GetService<DeptService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public BpOriginService BpOriginService
        {
            get
            {
                return ServiceLocator.GetService<BpOriginService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public BpIdentityService BpIdentityService
        {
            get
            {
                return ServiceLocator.GetService<BpIdentityService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public BpBirService BpBirService
        {
            get
            {
                return ServiceLocator.GetService<BpBirService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public BpTxService BpTxService
        {
            get
            {
                return ServiceLocator.GetService<BpTxService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public BpVerifiedService BpVerifiedService
        {
            get
            {
                return ServiceLocator.GetService<BpVerifiedService>();
            }
        }

        public RolService RolService
        {
            get
            {
                return ServiceLocator.GetService<RolService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public WorkDayService WorkDayService
        {
            get
            {
                return ServiceLocator.GetService<WorkDayService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public UserService UserService
        {
            /*
             Importante: cuando se necesita que el repository sea específico se debe consutruir  como tal, como en este caso
             */
            get
            {
                return ServiceLocator.GetService<UserService>();
            }
        }

        public CompanyService CompanyService
        {
            get
            {
                return ServiceLocator.GetService<CompanyService>();
            }
        }

        public CountryService CountryService
        {
            get
            {
                return ServiceLocator.GetService<CountryService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        public LogService LogService
        {
            get
            {
                return ServiceLocator.GetService<LogService>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <param name="company"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="authenticationfactor"></param>
        /// <param name="minutiaetype"></param>
        /// <param name="bodypart"></param>
        /// <param name="listBirs"></param>
        /// <returns></returns>
        public Errors RetrieveWithFilter(int id, ICompany company,
                                                 string typeid, string valueid,
                                                 int authenticationfactor, int minutiaetype, int bodypart,
                                                 out List<BpBir> listBirs)
        {
            Errors ret;
            List<BpBir> birs = new List<BpBir>();
            listBirs = null;
            string msg;
            BpIdentity identity;
            IList<BpIdentity> listIdentities = null;

            try
            {
                Log4Bio.Debug("AdminBpBir.RetrieveWithFilter in...");
                Log4Bio.Debug("AdminBpBir.RetrieveWithFilter companyidenroll = " + company.Id.ToString() +
                          " - typeid = " + typeid + " - valueid = " + valueid);
                //Busco que exista la identidad, si viene typeid/valueid != null (es Verify) sino retorno con error
                if (typeid != null && valueid != null)
                {
                    Paginacion paginacion = new Paginacion() { Pagina = 0, Cantidad = int.MaxValue };
                    listIdentities = BpIdentityService.GetIdentitiesByFiltro(paginacion, company, typeid.Trim(), valueid.Trim(), null, null);
                }

                identity = listIdentities != null && listIdentities.Count > 0 ? (BpIdentity)listIdentities[0] : null;

                //Si es Verify y no encotnro identidad retorno error, sino si es Identify sigo
                if (identity == null && (typeid != null && valueid != null))
                {
                    Log4Bio.Debug("No se encontro la identidad con valores typeid=" + typeid + "/valueid=" + valueid);
                    listBirs = null;
                    return Errors.IERR_IDENTITY_NOT_FOUND;
                }

                Log4Bio.Debug("AdminBpBir.RetrieveWithFilter id = " + id.ToString());
                //Si viende id de BIR busco por id, porque es unico y mas rapido
                if (id > 0)
                {
                    birs.Add((BpBir)BpBirService.GetById(id));
                }
                else  //Genero filtro
                {
                    Log4Bio.Debug("AdminBpBir.RetrieveWithFilter crea criteria...");
                    //Creo criteria
                    ICriteria crit = Session.CreateCriteria(typeof(BpBir));

                    //Filtro por Identity primero siemrpe que sea verificacion
                    if (identity != null)
                    {
                        crit.Add(Expression.Eq("BpIdentity", identity));
                    }

                    //Si es >0 => no es Any/All/None
                    if (authenticationfactor > 0)
                    {
                        crit.Add(Expression.Eq("Authenticationfactor", authenticationfactor));
                    }

                    if (minutiaetype > 0)
                    {
                        crit.Add(Expression.Eq("Minutiaetype", minutiaetype));
                    }

                    if (bodypart > 0)
                    {
                        crit.Add(Expression.Eq("Bodypart", bodypart));
                    }

                    if (company != null)
                    {
                        crit.Add(Expression.Eq("Companyidenroll", company.Id));
                    }

                    Log4Bio.Debug("AdminBpBir.RetrieveWithFilter Criteria = " + crit.ToString());
                    //Recupero los elementos con filtro
                    birs = (List<BpBir>)crit.AddOrder(new Order("Authenticationfactor", true)).
                                                      AddOrder(new Order("Type", true)).
                                                      AddOrder(new Order("Bodypart", true)).
                                                      AddOrder(new Order("Minutiaetype", true)).
                                                      List<BpBir>();
                    listBirs = birs;
                    Log4Bio.Debug("AdminBpBir.RetrieveWithFilter listBirs.count = ");
                    Log4Bio.Debug(listBirs == null ? "null" : listBirs.Count.ToString());
                }
                ret = Errors.IERR_OK;
            }
            catch (Exception ex)
            {
                Log4Bio.Error("AdminBpBir.RetrieveWithFilter", ex);
                birs = null;
                ret = Errors.IERR_UNKNOWN;
            }

            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return ret;
        }

        private bool IsValiduserFormat(string username)
        {
            return username.All(c => IsValidChar(c));
        }

        private bool IsValidChar(char c)
        {
            if (Char.IsLetterOrDigit(c)) return true;
            if (c == '@') return true;

            return false;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool ValidateUser(IUser user, string password)
        {
            bool validateUser = false;
            string username = user.Username.Trim();
            if (user == null)
            {
                ValidationDictionary.AddError("", "Usuario no válido");
                Log4Bio.Info(string.Format("Intento de autenticación fallido para {0}  pass {1}", username, password));
                return false;
            }

            if (user.IsApproved)
            {
                if (user != null)
                    if (UserService.ValidatePassword(user, password))
                        validateUser = true;
                    else
                    {
                        ValidationDictionary.AddError("", "Clave no válida");
                        Log4Bio.Info(string.Format("Intento de autenticación fallido para {0}  pass {1}", username, password));
                    }
                else
                {
                    ValidationDictionary.AddError("", "Usuario no válido");
                    Log4Bio.Info(string.Concat("Intento de autenticación fallido para ", username));
                }
            }
            else
            {
                ValidationDictionary.AddError("", "Usuario no habilitado");
                Log4Bio.Info(string.Concat("Intento de autenticación fallido de usuario deshabilitdo para ", username));
            }

            return validateUser;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool ValidateUser(string username, string password)
        {
            IUser user = UserService.GetByUsername(username);

            if (user != null)
                return ValidateUser(user, password);
            else
                return false;
        }

        public int InsertEmployee(Employee employee, IList<IdentityContactForm> IdentityContactForm)
        {
            int id = 0;
            int p = 0;
            int res = -1;
            if (employee.Identity.Id>0)
            {
                Log4Bio.Info("Se trata de una modificación de identidad");
                //Hay que modificar la identidad.
                BpIdentityService.Update(employee.Identity);
            }
            else
            {
                Log4Bio.Info("Se trata de una nueva identidad");
                //Hay que agregar la identidad.
                id = BpIdentityService.Create(employee.Identity);
                if(id>0)
                {
                    Log4Bio.Info("Recuperamos la identidad");
                    //Recuperamos la identidad.
                    employee.Identity = BpIdentityService.GetById(id);
                }
                else
                {
                    Log4Bio.Error("No se pudo crear la identidad");
                }

            }
            id=EmployeeService.Create(employee);
            
            if (id>0)
            {
                res = id;
                foreach (IdentityContactForm identitycontactform in IdentityContactForm)
                {
                    identitycontactform.Identity = employee.Identity; 
                    id=IdentityContactFormService.Create(identitycontactform);
                    if(id>0)
                    {
                        Log4Bio.Info("Se agrega el identity contact a la identidad");
                    }
                }
                Log4Bio.Info("Se creo correctamente el trabajador");
              
            }
            else
            {
                Log4Bio.Error("Se produjo un error al momento de guardar el empleado");
            }
            return res;
            
        }

        public Employee UpdateEmployee(Employee employee, IList<IdentityContactForm> identitycontactforms)
        {
            
            //BeginTransaction();
            try
            {

                Log4Bio.Info("Eliminamos los benefinicios de la persona");
                EmployeeBenefitOptionService.DeleteAll(employee.Id);
                Log4Bio.Info("Eliminamos los registermethod asociados");
                EmployeeRegisterMethodService.DeleteAll(employee.Id);
                EmployeeService.Update(employee);
                Log4Bio.Info("Grabamos los datos de contacto de un empleado");
                foreach (IdentityContactForm identitycontactform in identitycontactforms)
                    IdentityContactFormService.Update(identitycontactform);


              //  Commit();
                                
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al crea empresa ", e);
                //Rollback();
            }
            return null;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public Company CreateCompany(Company company)
        {
            BeginTransaction();
            try
            {
                int idCompany = CompanyService.Create(company);
                Company companyNew = (Company)CompanyService.GetById(idCompany);
                //User.Companies.Add(company);
                //UserService.Update(User);

                Commit();
                return companyNew;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al crea empresa ", e);
                Rollback();
            }
            return null;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public int GetHolding(string username)
        {
            IUser user = UserService.GetByUsername(username);
            if (user.Companies.Count > 0)
                return (int)user.Companies[0].Holding;

            return -1;
        }

        public User GetUserByName(string username)
        {
            Paginacion paginacion = new Paginacion() { Cantidad = 1, Pagina = 0 };
            IList<IUser> users = UserService.GetUsersByCompanyUsername(paginacion, -1, username);

            if (users != null)
                if (users.Count > 0)
                    return (User)users.First();
                else
                    return null;
            else
                return null;
        }
    }
}