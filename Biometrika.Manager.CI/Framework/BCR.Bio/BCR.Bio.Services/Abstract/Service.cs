﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Interface;
using BCR.Bio.Services.Interface;
using BCR.System.Log;
using BCR.System.Validation;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services.Abstract
{
    public abstract class Service<T> : IService<T> where T : class  //, new()
    {
        protected IRepository<T> repository;
        
        protected abstract bool ValidateForCreate(T t);

        protected abstract bool ValidateForUpdate(T t);

        public ValidationDictionary ValidationDictionary { get; private set; }
        /// <summary>
        ///
        /// </summary>
        /// <param name="repositoryParams"></param>
        public Service(IRepository<T> repositoryParams)
        {
            ValidationDictionary = new ValidationDictionary();
            repository = repositoryParams;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public virtual int Create(T t)
        {
            if (!ValidateForCreate(t))
            {
                return 0;
            }
            int id = repository.Create(t);
            OnCreated(id);
            return id;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        public virtual void OnCreated(int id)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="t"></param>
        public virtual T Update(T t)
        {
            if (!ValidateForUpdate(t))
            {
                return t;
            }
            t = repository.Update(t);

            OnUpdated(t);

            return t;
        }

        public virtual void OnUpdated(T t)
        {

        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        public virtual bool Delete<T>(int id)
        {
            //if (!ValidateForDelete(t))
            //{
            //    return -1;
            //}
            try
            {
                bool result =  repository.Delete<T>(id);
                OnDeleted<T>(id);
                return result;
            }
            catch (Exception e)
            {
                 
                Log4Bio.Error(string.Concat("Delete:", typeof(T), ":", id), e);

                return false;
            }
        }

        public virtual void OnDeleted<T>(int id)
        {

        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual T GetById(int id)
        {
            return repository.GetById(id);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public virtual IList<T> List()
        {
            return repository.List();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public virtual IList<T> GetByCriteria(DetachedCriteria criteria)
        {
            return repository.GetByCriteria(criteria);
        }

        /// <summary>
        /// Must be override in endatables
        /// </summary>
        /// <param name="t"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        protected virtual bool IsEndDatableThenDo(T t, bool status)
        {
            return false;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public virtual T Disable(T t)
        {
            CascadeDisable(t);
            return ChangeStatus(t, false);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public virtual T Enable(T t)
        {
            if (CanBeEnabled(t))
                return ChangeStatus(t, true);
            else
                return null;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual T Disable(int id)
        {
            CascadeDisable(GetById(id));
            return ChangeStatus(GetById(id), false);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual T Enable(int id)
        {
            if (CanBeEnabled(GetById(id)))
                return ChangeStatus(GetById(id), true);
            else
            {
                Log4Bio.Error("No se pudo habilitar");  
                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="t"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        private T ChangeStatus(T t, bool status)
        {
            if (!IsEndDatableThenDo(t, status))             // is End Date is not present , do nothing
                return t;

            t = repository.Update(t);

            return t;
        }

        /// <summary>
        /// Override for disable cascade in dependencies
        /// </summary>
        /// <param name="t"></param>
        public virtual void CascadeDisable(T t)
        {
            return;
        }

        /// <summary>
        /// Override when parent class is disabled
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public virtual bool CanBeEnabled(T t)
        {
            return true;
        }
    }
}