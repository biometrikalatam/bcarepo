﻿using BCR.Bio.Domain;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCR.Bio.Infraestructura.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class ResidentTrackingService : Service<ResidentTracking>
    {
        ResidentTrackingRepository localRepo = null;
        public ResidentTrackingService(ResidentTrackingRepository repository) 
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(ResidentTracking t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(ResidentTracking t)
        {
            return true;
        }

        public IList<ResidentTracking> GetByFilter(ResidentTrackingFilter filter)
        {
            return localRepo.GetByFilter(filter);
        }
    }
}
