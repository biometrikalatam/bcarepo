﻿using NHibernate.Criterion;
using System.Collections.Generic;

namespace BCR.Bio.Services.Interface
{
    public interface IService<T> where T : class
    {
        int Create(T t);

        IList<T> GetByCriteria(DetachedCriteria criteria);

        IList<T> List();

        T GetById(int id);

        bool Delete<T>(int id);

        T Update(T t);

      
    }
}