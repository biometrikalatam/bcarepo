﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using BCR.System.Log;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class UnityService : Service<Unity>
    {
        private UnityRepository localRepo;

        public UnityService(UnityRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(Unity t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(Unity t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public override void OnCreated(int id)
        {
            Log4Bio.Info("Se creo la unidad");
            // agregar aca el call al proceso post
            base.OnCreated(id);
        }

        public override void OnDeleted<T>(int id)
        {
            Log4Bio.Info("Se borro la unidad");

            // agregar aca el call al proceso
            base.OnDeleted<T>(id);  
        }
        public override void OnUpdated(Unity t)
        {
            Log4Bio.Info("Se actulizaod la unidad");

            // agregar aca lo necesario
            base.OnUpdated(t);
        }

        public IList<Unity> GetByBuilding(int buildingId, Paginacion paginacion = null)
        {
            return localRepo.GetByBuilding(buildingId, paginacion);
        }

        public IList<Unity> GetByZone(int zoneId, Paginacion paginacion = null)
        {
            return localRepo.GetByZone(zoneId, paginacion);
        }

        //public Unity GetById(int id, bool withResidents = false, bool withVisitor = false)
        public Unity GetById(int id, bool withResidents = false)
        {
            //return localRepo.GetById(id, withResidents, withVisitor);
            return localRepo.GetById(id, withResidents);
        }

        public IList<Unity> GetByFilter(Unity filter)
        {
            return localRepo.GetByFilter(filter);
        }

        /// <summary>
        /// GetByCustomFilter es un filtrado en base a el nuevo tipado de fltros
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public UnityFilterDTO GetByCustomFilter(UnityFilterDTO filter)
        {
            return localRepo.GetByCustomFilter(filter);
        }

        public string GetHASHByCompanyId(int companyId)
        {
            return localRepo.GetHASHByCompanyId(companyId);
        }

        public string GetHASHByCompanyIdOnlyAuthorized(int companyId)
        {
            return localRepo.GetHASHByCompanyIdOnlyAuthorized(companyId);
        }
    }
}