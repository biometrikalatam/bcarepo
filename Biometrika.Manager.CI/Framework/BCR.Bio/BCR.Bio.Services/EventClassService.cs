﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.EventClass;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class EventClassService : Service<EventClass>
    {
        EventClassRepository localRepo;
        public EventClassService(EventClassRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(EventClass t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(EventClass t)
        {
            return true;
        }

        public IList<EventClass> GetList(int companyId)
        {
            return localRepo.GetList(companyId);
        }

        public string GetHASHByCompanyId(int companyId)
        {
            return localRepo.GetHASHByCompanyId(companyId);
        }

        public EventClassFilterDTO GetByFilter(EventClassFilterDTO filter)
        {
            return localRepo.GetByFilter(filter);
        }
    }
}