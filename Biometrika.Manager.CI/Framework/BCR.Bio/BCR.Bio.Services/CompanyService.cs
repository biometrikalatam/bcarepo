﻿using BCR.Bio.Domain.Criteria;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using BCR.System.Service;
using System;
using System.Collections;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class CompanyService : Service<ICompany>
    {
        private static Hashtable companyNameCache = new Hashtable();

        private CompanyRepository repositoryLocal;

        public CompanyService(CompanyRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(ICompany t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(ICompany t)
        {
            //ICompany org = GetById(t.Id);
            //t.CreateDate = org.CreateDate;
            //t.Endate = org.Endate;

            //t.UpdateDate = DateTime.Now;
            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="t"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        protected override bool IsEndDatableThenDo(ICompany t, bool activate)
        {
            if (!activate)
                t.EndDate = DateTime.Now;
            else
                t.EndDate = null;

            return true;
        }

        public ICompany GetById(int id, bool withHQs = false, bool withSectors = false, bool withUsers = false)
        {
            return repositoryLocal.GetById(id, withHQs, withSectors, withUsers);
        }

        public ICompany GetByIdStrict(int id)
        {
            if (!companyNameCache.ContainsKey(id))
                companyNameCache.Add(id, repositoryLocal.GetById(id));

            return (ICompany)companyNameCache[id];
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        public override void OnCreated(int id)
        {
        }

        public override int Create(ICompany company)
        {
            if (!ValidateForCreate(company))
            {
                return 0;
            }

            return repositoryLocal.Create(company);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="t"></param>
        public override void CascadeDisable(ICompany t)
        {
            // disable HQs

            foreach (IHQ hq in t.HQs)
                ServiceLocator.GetService<HQService>().Disable(hq);

            foreach (ISector sector in t.Sectors)
                ServiceLocator.GetService<SectorService>().Disable(sector);

            foreach (IUser user in t.Users)
                ServiceLocator.GetService<UserService>().Disable(user);
        }

        public IList<ICompany> GetByCriteria(string companyLegalId, int holdingId)
        {
            return GetByCriteria(CriteriaFactory.CompanyByRut(companyLegalId, holdingId));
        }

        public IList<ICompany> GetByRutAndStatus(string companyLegalId, bool status, int holdingId)
        {
            return GetByCriteria(CriteriaFactory.CompanyByRutAndStatus(companyLegalId, status, holdingId));
        }

        public IList<ICompany> GetByRutAndDomain(string companyLegalId, string domain, int holdingId)
        {
            return GetByCriteria(CriteriaFactory.CompanyByRutAndDomain(companyLegalId, domain, holdingId));
        }

        public IList<ICompany> GetByRutAndStatusAndName(string rut, int status, string name, int holdingId)
        {
            return GetByCriteria(CriteriaFactory.CompanyByRutAndStatusAndName(rut, status, name, holdingId));
        }

        public IList<ICompany> GetByRutAndName(string rut, string name, int holdingId)
        {
            return GetByCriteria(CriteriaFactory.CompanyByRutAndName(rut, name, holdingId));
        }
    }
}