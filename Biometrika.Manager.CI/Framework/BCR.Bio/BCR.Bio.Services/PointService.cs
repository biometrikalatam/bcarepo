﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Criteria;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Services
{
    public class PointService : Service<IPoint>
    {
        private static Hashtable pointCache = new Hashtable();

        private PointRepository repositoryLocal;

        public PointService(PointRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IPoint t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(IPoint t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        protected override bool IsEndDatableThenDo(IPoint t, bool activate)
        {
            if (!activate)
                t.EndDate = DateTime.Now;
            else
                t.EndDate = null;

            return true;
        }

        public override Boolean CanBeEnabled(IPoint t)
        {
            return (t.HQ.EndDate == null) && (t.Sector.EndDate == null);
        }

        public IQueryable<Point> GetByHolding(int holdingId)
        {
            return (IQueryable<Point>)GetByCriteria(CriteriaFactory.GetByHolding<Point>(holdingId));
        }

        public Point GetBySensorId(int companyId, string sensorId)
        {
            return repositoryLocal.GetBySensorId(companyId, sensorId);
        }

        public Point GetBySensorName(string name)
        {
            return repositoryLocal.GetBySensorName(name);
        }

        public List<Point> GetByCompanyId(int companyId)
        {
            return repositoryLocal.GetByCompanyId(companyId);
        }

        public IList<AccessControl> GetAttachedAccessControl(Point point)
        {
            return repositoryLocal.GetAttachedAccessControl(point);
        }

        public Point GetByCompanyIdAndCode(int companyId, string code)
        {
            return repositoryLocal.GetByCompanyIdAndCode(companyId, code);
        }

        public IList<Point> GetByFilter(PointFilterDTO filter)
        {
            return repositoryLocal.GetByFilter(filter);
        }

        public Point GetByCompanyAndCode(int companyId, string code)
        {
            return repositoryLocal.GetByCompanyAndCode(companyId, code);
        }

        public bool SyncAccessPoints()
        {
            return repositoryLocal.SyncAccessPoints();
        }

        public bool SyncPointById(int id)
        {
            return repositoryLocal.SyncPointById(id);
        }
    }
}