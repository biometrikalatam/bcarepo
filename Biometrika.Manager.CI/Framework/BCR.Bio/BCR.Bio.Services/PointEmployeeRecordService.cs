﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Services
{
    public class PointEmployeeRecordService : Service<IPointEmployeeRecord>
    {
        private PointEmployeeRecordRepository repositoryLocal;

        public PointEmployeeRecordService(PointEmployeeRecordRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IPointEmployeeRecord t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(IPointEmployeeRecord t)
        {
            return true;
        }

        public IList<PointEmployeeRecordDTO> ListByCompanyId(int companyId)
        {
            return repositoryLocal.ListByCompanyId(companyId);
        }

        public IPoint GetByUserId(int id, DateTime moment)
        {
            IList<IPointEmployeeRecord>  list= repositoryLocal.GetByUserId(id, moment);
            if (list.Count > 0)
                return list[0].Point;
            else
                return null;
        }

        public IPoint GetByUserIdAndCodePoint(int id, DateTime moment, string code, bool onlyActive = true)
        {
            IList<IPointEmployeeRecord> list = repositoryLocal.GetByUserIdAndCodePoint(id, moment, code, onlyActive);
            if (list.Count > 0)
                return list[0].Point;
            else
                return null;
        }

        public IPointEmployeeRecord GetPointEmployeeRecordByUserIdAndCodePoint(int id, DateTime moment, string code, bool onlyActive = true)
        {
            return repositoryLocal.GetByUserIdAndCodePoint(id, moment, code, onlyActive).FirstOrDefault();
        }

        public IList<IPointEmployeeRecord> GetByPointId(int id)
        {
            return repositoryLocal.GetByPointId(id);
        }

        public IList<PointEmployeeRecord> GetByFilter(PointEmployeeRecordFilterDTO filter)
        {
            return repositoryLocal.GetByFilter(filter);
        }
    }
}