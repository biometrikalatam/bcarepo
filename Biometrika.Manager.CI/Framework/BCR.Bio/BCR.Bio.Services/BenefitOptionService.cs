﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Services
{
    public class BenefitOptionService : Service<BenefitOption>
    {

        BenefitOptionRepository localRepo = null;
        /// <summary>
        ///
        /// </summary>
        /// <param name="validationDictionary"></param>
        /// <param name="repository"></param>
        public BenefitOptionService(BenefitOptionRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }
        protected override bool ValidateForCreate(BenefitOption t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(BenefitOption t)
        {
            return true;
        }
    }
}
