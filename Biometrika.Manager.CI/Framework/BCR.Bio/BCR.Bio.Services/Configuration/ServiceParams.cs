﻿using BCR.Bio.Infraestructura.Interface;

namespace BCR.Bio.Services.Configuration
{
    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ServiceParams<T> where T : class
    {
        public IRepository<T> Repository { get; set; }

        
    }
}