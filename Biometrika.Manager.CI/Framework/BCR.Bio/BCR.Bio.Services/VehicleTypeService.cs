﻿using System;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class VehicleTypeService : Service<IVehicleType>
    {
        VehicleTypeRepository RepositoryLocal = null;
        public VehicleTypeService(VehicleTypeRepository repository)
            : base(repository)
        {
            RepositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IVehicleType t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(IVehicleType t)
        {
            return true;
        }

        public IVehicleType GetByDescription(string vehicleTypeDescription)
        {
            return RepositoryLocal.GetByDescription(vehicleTypeDescription);
        }
    }
}