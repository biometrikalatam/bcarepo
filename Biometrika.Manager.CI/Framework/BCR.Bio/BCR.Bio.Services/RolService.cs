﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Services
{
    public class RolService : Service<IRol>
    {
        public RolService(RolRepository repository)
            : base(repository)
        {
        }

        protected override bool ValidateForCreate(IRol t)
        {
            t.CreateDate = DateTime.Now;
            t.UpdateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(IRol t)
        {
            throw new global::System.NotImplementedException();
        }

        public IRol GetById(string id)
        {
            return GetById(int.Parse(id));
        }

        /// <summary>
        /// Convert a role names array to role id array
        /// </summary>
        /// <param name="rolesName"></param>
        /// <returns></returns>
        public string[] GetRoleIdFromRoleNames(string[] rolesName)
        {
            List<IRol> roles = (List<IRol>)List().ToList();

            string[] rolesId = new string[rolesName.Length];

            for (int i = 0; i < rolesName.Length; i++)
            {
                int idRol = roles.Single(r => r.Name == rolesName[i].ToString()).Id;
                rolesId[i] = idRol.ToString();
            }

            return rolesId;
        }
    }
}