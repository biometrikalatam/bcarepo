﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class ResidentAuthorizatorService : Service<ResidentAuthorizator>
    {
        ResidentAuthorizatorRepository localRepository;

        public ResidentAuthorizatorService(ResidentAuthorizatorRepository repository)
            : base(repository)
        {
            localRepository = repository;
        }

        protected override bool ValidateForCreate(ResidentAuthorizator t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(ResidentAuthorizator t)
        {
            return true;
        }

        public void DeleteByResident(int id)
        {
            localRepository.DeleteByResident(id);
        }

        public IList<Resident> GetAutorizatorListByFilter(int companyId, BpIdentity filter, int unityId = 0)
        {
            return localRepository.GetAutorizatorListByFilter(companyId, filter, unityId);
        }
    }
}