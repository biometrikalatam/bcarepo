﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;

namespace BCR.Bio.Services
{
    public class TimeRestrictionService : Service<TimeRestriction>
    {
        public TimeRestrictionService(TimeRestrictionRepository repository)
            : base(repository)
        {
        }

        protected override bool ValidateForCreate(TimeRestriction t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(TimeRestriction t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }
    }
}