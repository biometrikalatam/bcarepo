﻿using BCR.Bio.Domain;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCR.Bio.Infraestructura.Interface;
using BCR.Bio.Infraestructura;

namespace BCR.Bio.Services
{
    public class IdentityContactFormService : Service<IdentityContactForm>
    {
        IdentityContactFormRepository localRepo;

        public IdentityContactFormService(IdentityContactFormRepository repository) : base(repository)
        {
            localRepo = repository;
        }

        public IList<IdentityContactForm> GetByIdentity(int id)
        {
            return localRepo.GetByIdentity(id);
        }

        protected override bool ValidateForCreate(IdentityContactForm t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(IdentityContactForm t)
        {
            return true;
        }
    }
}
