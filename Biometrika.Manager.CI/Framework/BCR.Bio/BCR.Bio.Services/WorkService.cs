﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCR.Bio.Infraestructura.Interface;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class WorkService : Service<Work>
    {
        private WorkRepository localRepo;

        public WorkService(WorkRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(Work t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(Work t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }
        public IList<Work> GetByFilter(WorkDTO filter)
        {
            return localRepo.GetByFilter(filter);
        }

    }
}
