﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using BCR.System.Crypto;
using BCR.System.Validation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BCR.Bio.Services
{
    public class UserService : Service<IUser>
    {
        private UserRepository repositoryLocal;

        public UserService(UserRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        protected override bool ValidateForCreate(IUser user)
        {
            //if (user.Company == null)
            //    ValidationDictionary.AddError("USER_SIN_COMPANY", "No se ingresó la compañia");

            if (string.IsNullOrEmpty(user.Username))
                ValidationDictionary.AddError("USER_SIN_USERNAME", "No se ingresó Username");

            if (string.IsNullOrEmpty(user.Email))
                ValidationDictionary.AddError("USER_SIN_EMAIL", "No se ingresó Email");

            if (string.IsNullOrEmpty(user.Email))
                ValidationDictionary.AddError("USER_SIN_EMAIL", "No se ingresó Email");

            // check if the new password passes all required conditions
            if (!CheckPasswordPolicy(user.Password))
                ValidationDictionary.AddError("InvalidPassword", "InvalidPassword");

            string encodedPassword = PasswordProvider.GeneratePasswordHashed(user.Password);

            if (encodedPassword.Length > 255)
                ValidationDictionary.AddError("InvalidencodedPassword", "InvalidencodedPassword");

            user.Password = encodedPassword;

            string encodedAnswer = PasswordProvider.GeneratePasswordHashed(user.PasswordAnswer);

            if (encodedPassword.Length > 255)
                ValidationDictionary.AddError("InvalidencodedAnswer", "InvalidencodedAnswer");

            user.PasswordAnswer = encodedAnswer;
            user.CreateDate = DateTime.Now;
            user.IsApproved = true;
            return true;
            //return ValidationDictionary.IsValid;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="correctHash"></param>
        /// <returns></returns>
        public bool ValidatePassword(IUser user, string correctHash)
        {
            if (!user.IsApproved)
            {
                ValidationDictionary.AddError("", "Usuario inhabilitado");
                return false;
            }

            bool validPassword = PasswordProvider.ValidatePassword(correctHash, user.Password);
            if (validPassword)
            {
                user.IsLockedOut = false;
                user.LastLoginDate = DateTime.Now;
                user.FailedPasswordAttemptCount = 0;
            }
            else
            {
                if (user.FailedPasswordAttemptCount == 5)
                    user.IsLockedOut = true;

                user.FailedPasswordAttemptCount = user.FailedPasswordAttemptCount + 1;
                //////////////7
                ValidationDictionary.AddError("IntentosFallidos", string.Concat("IntentosFallidos", user.FailedPasswordAttemptCount));
            }

            user.LastActivityDate = DateTime.Now;
            Task<IUser> task = Task.Factory.StartNew<IUser>(() => Update(user));

            return validPassword;
        }

        protected override bool IsEndDatableThenDo(IUser t, bool activate)
        {
            if (!activate)
            {
                t.IsApproved = false;
                t.Comments = "Disable by Admin";
            }
            else
            {
                t.IsApproved = true;
            }
            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool LockUser(IUser user)
        {
            user.IsLockedOut = true;
            Update(user);
            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool LockUser(int userId)
        {
            IUser user = GetById(userId);
            user.IsLockedOut = true;
            Update(user);
            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool UnLockUser(IUser user)
        {
            user.IsLockedOut = false;
            Update(user);
            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool UnLockUser(int userId)
        {
            IUser user = GetById(userId);
            user.IsLockedOut = false;
            Update(user);
            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="idCompany"></param>
        /// <returns></returns>
        public IList<IUser> GetUsersByEmail(string email)
        {
            return repositoryLocal.GetUsersByEmail(email);
        }

        public IUser GetUsersByUsernameAndEmail(string username, string email)
        {
            return repositoryLocal.GetUsersByUsernameAndEmail(username, email);
        }

        /// <summary>
        /// Método de consulta de Usuarios en base a su username e id de company
        /// </summary>
        /// <param name="paginacion">Objeto de paginado</param>
        /// <param name="companyId">Id de company a buscar</param>
        /// <param name="username">Nombre de usuario a buscar</param>
        /// <returns>Listado de user encontrados o nulo</returns>
        public IList<IUser> GetUsersByCompanyUsername(Paginacion paginacion, int idCompany, string username)
        {
            return repositoryLocal.GetUsersByCompanyUsername(paginacion, idCompany, username);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="idCompany"></param>
        /// <returns></returns>
        public IList<IUser> GetUsersByCompany(int idCompany)
        {
            return repositoryLocal.GetUsersByCompany(idCompany);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCompany"></param>
        /// <returns></returns>
        public IList<UserDTO> GetUsersByCompanyLigth(int idCompany)
        {
            return repositoryLocal.GetUsersByCompanyLigth(idCompany);
        }

        public IList<IUser> GetUsersWthoutEmployeeAssignedByCompany(int idCompany)
        {
            //IList<IUser> list = new List<IUser>();
            //list = repositoryLocal.GetUsersWthoutEmployeeAssignedByCompany(idCompany);
            //list.Add(GetById(userId));
            return repositoryLocal.GetUsersWthoutEmployeeAssignedByCompany(idCompany);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        protected override bool ValidateForUpdate(IUser t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        private bool CheckPasswordPolicy(string password)
        {
            //// password length check
            //if (password.Length < MinRequiredPasswordLength)
            //{
            //    return false;
            //}

            int nonAlphanumericCharacters = 0;

            foreach (char c in password)
            {
                if (!Char.IsLetterOrDigit(c))
                {
                    nonAlphanumericCharacters++;
                }
            }

            //// non alphanumerics check
            //if (nonAlphanumericCharacters < MinRequiredNonAlphanumericCharacters)
            //{
            //    return false;
            //}

            // regular expression check
            //if (!String.IsNullOrEmpty(PasswordStrengthRegularExpression))
            //{
            //    if (!Regex.IsMatch(password, PasswordStrengthRegularExpression))
            //    {
            //        return false;
            //    }
            //}

            return true;
        }

        public IUser GetByUsername(string username)
        {
            

            return repositoryLocal.GetByusername(username);
        }

        // TODO: Solo para etapa 1 Lomas la Dehesa, quitar al pasar a la Etapa 2
        public int CreateExtendedData(int userId, string decryptedPassword)
        {
            return repositoryLocal.CreateExtendedData(userId, decryptedPassword);
        }

        // TODO: Solo para etapa 1 Lomas la Dehesa, quitar al pasar a la Etapa 2
        public int UpdateExtendedData(int userId, string decryptedPassword)
        {
            return repositoryLocal.UpdateExtendedData(userId, decryptedPassword);
        }

        // TODO: Solo para etapa 1 Lomas la Dehesa, quitar al pasar a la Etapa 2
        public IList<User> GetOperadoresByCompanyId(int companyId)
        {
            return repositoryLocal.GetOperadoresByCompanyId(companyId);
        }

        // TODO: Solo para etapa 1 Lomas la Dehesa, quitar al pasar a la Etapa 2
        public UserExtended GetUserExtended(int userId)
        {
            return repositoryLocal.GetUserExtended(userId);
        }

        public IList<User> GetUsersByRolAndCompany(int rolId, int companyId)
        {
            return repositoryLocal.GetUsersByRolAndCompany(rolId, companyId);
        }
    }
}