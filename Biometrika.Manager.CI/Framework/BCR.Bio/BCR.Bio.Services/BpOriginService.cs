﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class BpOriginService : Service<IBpOrigin>
    {
        public BpOriginService(BpOriginRepository repository)
            : base(repository)
        {
        }

        protected override bool ValidateForCreate(IBpOrigin t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(IBpOrigin t)
        {
            return true;
        }
    }
}