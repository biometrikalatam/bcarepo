﻿using BCR.Bio.Domain;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCR.Bio.Infraestructura.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class IdentityStateService : Service<IdentityState>
    {
        IdentityStateRepository localRepo;
        public IdentityStateService(IdentityStateRepository repository) 
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(IdentityState t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(IdentityState t)
        {
            return true;
        }

        public IList<IdentityState> GetByFilter(IdentityStateFilterDTO filter)
        {
            return localRepo.GetByFilter(filter);
        }
    }
}
