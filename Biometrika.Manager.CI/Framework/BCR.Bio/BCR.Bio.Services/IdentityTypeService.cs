﻿using BCR.Bio.Domain;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCR.Bio.Infraestructura.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class IdentityTypeService : Service<IdentityType>
    {
        IdentityTypeRepository localRepo;

        public IdentityTypeService(IdentityTypeRepository repository) 
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(IdentityType t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(IdentityType t)
        {
            return true;
        }

        public IList<IdentityType> GetByFilter(IdentityTypeFilterDTO filter)
        {
            return localRepo.GetByFilter(filter);
        }
    }
}
