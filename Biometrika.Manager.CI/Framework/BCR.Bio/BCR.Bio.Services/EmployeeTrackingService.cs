﻿using BCR.Bio.Domain;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCR.Bio.Infraestructura.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class EmployeeTrackingService : Service<EmployeeTracking>
    {
        EmployeeTrackingRepository localRepo;
        public EmployeeTrackingService(EmployeeTrackingRepository repository) : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(EmployeeTracking t)
        {
            if (t == null)
                return false;

            return true;
        }

        protected override bool ValidateForUpdate(EmployeeTracking t)
        {
            if (t == null)
                return false;

            return true;
        }

        public bool IsDuplicateTracking(string valueId, DateTime fecha)
        {
            return localRepo.IsDuplicateTracking(valueId, fecha);
        }

        public IList<EmployeeTracking> GetByFilter(EmployeeTrackingFilter filter)
        {
            return localRepo.GetByFilter(filter);
        }
    }
}
