﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class DeptService : Service<IDept>
    {
        public DeptService(DeptRepository repository)
            : base(repository)
        {
        }

        protected override bool ValidateForCreate(IDept t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(IDept t)
        {
            return true;
        }

         
    }
}