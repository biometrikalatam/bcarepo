﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Services
{
    public class BenefitService : Service<Benefit>
    {
        BenefitRepository localRepo;

        public BenefitService(BenefitRepository repositoryParams) : base(repositoryParams)
        {
            localRepo = repositoryParams;
        }

        public IList<BenefitDTO> GetByCompany(int companyId)
        {
            return localRepo.GetByCompany(companyId);


        }
        public IList<Benefit> GetAllBenefitByCompany(int companyid)
        {
            return localRepo.GetAllBenefitByCompany(companyid);
        }

        protected override Boolean ValidateForCreate(Benefit t)
        {
            return true;
        }

        protected override Boolean ValidateForUpdate(Benefit t)
        {
            return true;
        }
    }
}