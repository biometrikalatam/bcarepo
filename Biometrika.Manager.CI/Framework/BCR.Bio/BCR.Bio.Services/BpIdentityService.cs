﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Enum.BioPortal;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using BCR.System.Enum;
using BCR.System.Log;
using BCR.System.Validation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Services
{
    public class BpIdentityService : Service<IBpIdentity>
    {
        private BpIdentityRepository repositoryLocal;

        public BpIdentityService(BpIdentityRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IBpIdentity t)
        {
            Paginacion paginacion = new Paginacion() { Cantidad = 1, Pagina= 0 };
            IList<BpIdentity> list = GetIdentitiesByFiltro(paginacion , t.CompanyIdEnroll, t.TypeId, t.ValueId);
            if (list == null || list.Count == 0)
            {
                t.Creation = DateTime.Now;
                return true;
            }
            else
            {
                Log4Bio.Error( "Identidad Existente");
                return false;
            }
        }

        protected override bool ValidateForUpdate(IBpIdentity t)
        {
            return true;
        }

        public BpIdentity GetById(int id, bool withBirs = false)
        {
            return repositoryLocal.GetById(id, withBirs);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="company"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <returns></returns>
        public IList<BpIdentity> GetIdentitiesByFiltro(Paginacion paginacion , ICompany company, string typeid, string valueid, bool withBirs = false)
        {
            return GetIdentitiesByFiltro(paginacion, company, typeid, valueid, "", "", withBirs);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="company"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="name"></param>
        /// <param name="lastname"></param>
        /// <returns></returns>
        public IList<BpIdentity> GetIdentitiesByFiltro(Paginacion paginacion , ICompany company,
                                                     string typeid, string valueid,
                                                     string name, string lastname, bool withBirs = false)
        {
            return repositoryLocal.GetIdentitiesByFiltro(paginacion, company, typeid, valueid, name, lastname, withBirs);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="company"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        public void DeleteIdentityByFiltro(Paginacion paginacion,  ICompany company, string typeid, string valueid)
        {
            BpIdentity bpIdentity = GetIdentitiesByFiltro(paginacion , company, typeid, valueid).ToList().SingleOrDefault();
            Delete<BpIdentity>(bpIdentity.Id);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="pdata"></param>
        /// <param name="oDBBpIdentity"></param>
        /// <returns></returns>
        public BpIdentity MergeIdentity(PersonalData pdata, BpIdentity oDBBpIdentity)
        {
            //msg = "S/C";
            //Errors ret = Errors.IERR_OK;
            BpIdentity oNewBpIdentity = oDBBpIdentity;

            try
            {
                if (oDBBpIdentity == null)
                {
                    Log4Bio.Error( EnumHelper.GetDescription(Errors.IERR_IDENTITY_NOT_FOUND));
                    return null;
                }
                oNewBpIdentity.Nick = pdata.Nick ?? oDBBpIdentity.Nick;

                oNewBpIdentity.VerificationSource = pdata.Verificationsource;

                oNewBpIdentity.Name = pdata.Name ?? oNewBpIdentity.Name;
                oNewBpIdentity.PatherLastname = pdata.Patherlastname ?? oNewBpIdentity.PatherLastname;
                oNewBpIdentity.MotherLastname = pdata.Motherlastname ?? oNewBpIdentity.MotherLastname;
                oNewBpIdentity.Sex = pdata.Sex ?? oNewBpIdentity.Sex;
                oNewBpIdentity.DocumentSeriesNumber = pdata.Documentseriesnumber ?? oNewBpIdentity.DocumentSeriesNumber;
                oNewBpIdentity.DocumentExpirationDate = pdata.Documentexpirationdate.Year < 1960
                                                        ? oNewBpIdentity.DocumentExpirationDate
                                                        : pdata.Documentexpirationdate;
                oNewBpIdentity.VisaType = pdata.Visatype ?? oNewBpIdentity.VisaType;
                oNewBpIdentity.BirthDate = pdata.Birthdate.Year < 1850
                                           ? oNewBpIdentity.BirthDate
                                           : pdata.Birthdate;
                oNewBpIdentity.BirthPlace = pdata.Birthplace ?? oNewBpIdentity.BirthPlace;
                oNewBpIdentity.Nationality = pdata.Nationality != null ? pdata.Nationality : null;
                oNewBpIdentity.Photography = String.IsNullOrEmpty(pdata.Photography) ?
                    oNewBpIdentity.Photography : pdata.Photography;
                oNewBpIdentity.Signatureimage = String.IsNullOrEmpty(pdata.Signatureimage) ?
                    oNewBpIdentity.Signatureimage : pdata.Signatureimage;
                oNewBpIdentity.Profession = pdata.Profession ?? oNewBpIdentity.Profession;
                //Si hay DinamicData = debo mergear con la nueva
                //string ddataaux = oNewBpIdentity.Dynamicdata;
                //if (!String.IsNullOrEmpty(ddataaux) && ddataaux.IndexOf("<ArrayOfDynamicDataItem") > 0)
                //    ddataaux = ddataaux.Substring(ddataaux.IndexOf("<ArrayOfDynamicDataItem"));
                //List<DynamicDataItem> dd = XmlUtils.DeserializeObject<List<DynamicDataItem>>(ddataaux);
                //if (dd == null && (pdata.Dynamicdata != null && pdata.Dynamicdata.Length > 0))
                //{
                //    dd = new List<DynamicDataItem>();
                //}
                //if (pdata.Dynamicdata != null)
                //{
                //    foreach (DynamicDataItem ddi in pdata.Dynamicdata)
                //    {
                //        dd.Add(ddi);
                //    }
                //}
                //string ddata = XmlUtils.SerializeObject(dd);
                //ddata = ddata.Replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
                //oNewBpIdentity.Dynamicdata = ddata;

                //ADDED 11-2014 - Separado a BD
                BpIdentityDynamicdata bpiDD;
                if (pdata.Dynamicdata != null && pdata.Dynamicdata.Length > 0)
                {
                    if (oDBBpIdentity.BpIdentityDynamicdata == null)
                    {
                        oNewBpIdentity.BpIdentityDynamicdata = new List<BpIdentityDynamicdata>();
                    }
                    else
                    {
                        oNewBpIdentity.BpIdentityDynamicdata = oDBBpIdentity.BpIdentityDynamicdata;
                    }
                    foreach (DynamicDataItem item in pdata.Dynamicdata)
                    {
                        if (!ExisteKey(oNewBpIdentity.BpIdentityDynamicdata, item.key, item.value))
                        {
                            bpiDD = new BpIdentityDynamicdata(oNewBpIdentity, item.key, item.value);
                            oNewBpIdentity.BpIdentityDynamicdata.Add(bpiDD);
                        }
                    }
                }
                oNewBpIdentity.Enrollinfo = pdata.Enrollinfo;
            }
            catch (Exception ex)
            {
                Log4Bio.Error("AdminBpIdentity.MergeIdentity", ex);
                //msg = ex.Message;
                oNewBpIdentity = null;
                //ret = Errors.IERR_UNKNOWN;
                //ValidationDictionary.AddError("", string.Concat(EnumHelper.GetDescription(Errors.IERR_UNKNOWN), ex.Message));
            }
            return oNewBpIdentity;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="list"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private bool ExisteKey(IList<BpIdentityDynamicdata> list, string key, string value)
        {
            bool ret = false;
            try
            {
                foreach (BpIdentityDynamicdata item in list)
                {
                    if (item.DDKey.Trim().Equals(key.Trim()))
                    {
                        item.DDValue = value;
                        item.LastUpdate = DateTime.Now;
                        ret = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Bio.Error("AdminBpIdentity.ExisteKey", ex);
                ret = false;
            }
            return ret;
        }

        public BpIdentity FillIdentity(ICompany company, PersonalData pdata)
        {
            //msg = "S/C";
            BpIdentity identitycreated = null;
            //int ret = Errors.IERR_OK;

            try
            {
                //string ddata = XmlUtils.SerializeObject(pdata.Dynamicdata);
                //Se tendrá que armar de nuevo
                identitycreated = new BpIdentity(company, pdata.Typeid, pdata.Valueid);
                identitycreated.Nick = pdata.Nick;
                identitycreated.Name = pdata.Name;
                identitycreated.PatherLastname = pdata.Patherlastname;
                identitycreated.MotherLastname = pdata.Motherlastname;
                identitycreated.Sex = pdata.Sex;
                identitycreated.DocumentSeriesNumber = pdata.Documentseriesnumber;
                identitycreated.DocumentExpirationDate = pdata.Documentexpirationdate.Year < 1960
                                                    ? DateTime.Parse("01/01/1900")
                                                    : pdata.Documentexpirationdate;
                identitycreated.VisaType = pdata.Visatype;
                identitycreated.BirthDate = pdata.Birthdate.Year < 1960
                                                    ? DateTime.Parse("01/01/1900")
                                                    : pdata.Birthdate;
                identitycreated.BirthPlace = pdata.Birthplace;
                identitycreated.Nationality = string.IsNullOrEmpty(pdata.Nationality) ? null : pdata.Nationality;
                identitycreated.Photography = pdata.Photography;
                identitycreated.Signatureimage = pdata.Signatureimage;
                identitycreated.Profession = pdata.Profession;

                //ADDED 11-2014 - Separado a BD
                //identitycreated.Dynamicdata = ddata;
                BpIdentityDynamicdata bpiDD;
                if (pdata.Dynamicdata != null && pdata.Dynamicdata.Length > 0)
                {
                    identitycreated.BpIdentityDynamicdata = new List<BpIdentityDynamicdata>();
                    foreach (DynamicDataItem item in pdata.Dynamicdata)
                    {
                        bpiDD = new BpIdentityDynamicdata(identitycreated, item.key, item.value);
                        identitycreated.BpIdentityDynamicdata.Add(bpiDD);
                    }
                }

                identitycreated.Enrollinfo = pdata.Enrollinfo;
                identitycreated.Creation = DateTime.Now;
                identitycreated.VerificationSource = pdata.Verificationsource;
                identitycreated.UserIdEnroll = pdata.Useridenroll;
                /*pdata.Nick, pdata.Typeid, pdata.Valueid,
                    pdata.Name, pdata.Patherlastname, pdata.Motherlastname, pdata.Sex,
                    pdata.Documentseriesnumber, pdata.Documentexpirationdate, pdata.Visatype,
                    pdata.Birthdate, pdata.Birthplace, pdata.Nationality, pdata.Photography,
                    pdata.Signatureimage, pdata.Profession, ddata, pdata.Enrollinfo,
                    pdata.Creation, pdata.Verificationsource,
                    pdata.Companyidenroll == 0 ? companyid : pdata.Companyidenroll,
                    pdata.Useridenroll); // == 0 ? pdata.Useridenroll : pdata.Useridenroll);*/
            }
            catch (Exception ex)
            {
                Log4Bio.Error("AdminBpIdentity.FillIdentity", ex);
                //msg = ex.Message;
                identitycreated = null;

                //ret = Errors.IERR_UNKNOWN;
                //ValidationDictionary.AddError("", string.Concat(EnumHelper.GetDescription(Errors.IERR_UNKNOWN), ex.Message));
            }
            return identitycreated;
        }

        public IBpIdentity GetIdentityByCompanyIdentity(int companyId, string typeId, string valueId)
        {
            Log4Bio.Info("Consultando..");
            return repositoryLocal.GetIdentityByCompanyIdentity(companyId, typeId, valueId);
        }

        public IList<BpIdentity> GetByFilter(IdentityFilterDTO filter)
        {
            return repositoryLocal.GetByFilter(filter);
        }
    }
}