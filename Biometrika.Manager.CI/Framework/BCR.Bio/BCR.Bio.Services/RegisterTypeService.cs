﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Services
{
    public class RegisterTypeService : Service<RegisterType>
    {
        RegisterTypeRepository localRepo;

        public RegisterTypeService(RegisterTypeRepository repositoryParams) : base(repositoryParams)
        {
            localRepo = repositoryParams;
        }


        protected override Boolean ValidateForCreate(RegisterType t)
        {
            return true;
        }

        protected override Boolean ValidateForUpdate(RegisterType t)
        {
            return true;
        }
    }
}
