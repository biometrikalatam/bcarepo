﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Services
{
    public class VisitorTrackingObjetadosService : Service<VisitorTrackingObjetados>
    {
        VisitorTrackingObjetadosRepository localRepo;

        public VisitorTrackingObjetadosService(VisitorTrackingObjetadosRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(VisitorTrackingObjetados t)
        {
            t.TimeStamp = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(VisitorTrackingObjetados t)
        {
            //t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<VisitorTrackingObjetados> GetByFilter(VisitorTrackingObjetadosFilter filter)
        {
            return localRepo.GetByFilter(filter);
        }
    }
}
