﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class PointRuleService : Service<PointRule>
    {
        protected PointRuleRepository localRepo;
        public PointRuleService(PointRuleRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(PointRule t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(PointRule t)
        {
            return true; 
        }

        public IList<PointRule> GetByFilter(PointRuleFilter filter)
        {
            return localRepo.GetByFilter(filter);
        }
    }
}