﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Linq;

namespace BCR.Bio.Services
{
    public class LogService : Service<IBPLog>
    {
        private LogRepository repositoryLocal;

        public LogService(LogRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        public IQueryable<BPLog> ListByFilters(
           IPaginacion paginacion,
           DateTime dateFrom,
           DateTime dateTo,
           string level,
           string loggerLike,
           string messageLike,
           string exceptionLike)
        {
            return repositoryLocal.ListByFilters(paginacion, dateFrom, dateTo, level, loggerLike, messageLike, exceptionLike);
        }

        protected override bool ValidateForCreate(IBPLog t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(IBPLog t)
        {
            return true;
        }
    }
}