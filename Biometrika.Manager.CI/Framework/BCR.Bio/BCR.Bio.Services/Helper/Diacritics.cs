﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Services.Helper
{
    public static class Diacritics
    {
        /// <summary>
        /// Remueve acentuaciones de un string entregado
        /// De ser static no me gusta acá, preguntar si es util ver donde lo dejamos
        /// </summary>
        /// <param name="text">Texto para procesar</param>
        /// <returns>Texto procesado sin acentuaciones</returns>
        public static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
    }
}
