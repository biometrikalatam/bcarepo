﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class MarkHelperService : Service<MarkHelper>
    {
        private MarkHelperRepository repositoryLocal;

        public MarkHelperService(MarkHelperRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }
        protected override bool ValidateForCreate(MarkHelper t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(MarkHelper t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }
        public IList<MarkHelper> GetByFilter(MarkHelperFilterDTO filter, bool filterProcessed = false)
        {
            return repositoryLocal.GetByFilter(filter, filterProcessed);
        }

        //public MarkHelper GetMaxTimeStampByPersonId(string personId)
        //{
        //    return repositoryLocal.GetMaxTimeStampByPersonId(personId);
        //}
        
        public MarkHelper GetLastMarkHelperByPersonId(string personId, int companyId)
        {
            return repositoryLocal.GetLastMarkHelperByPersonId(personId, companyId);
        }
    }
}
