﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Services
{
    public class EmployeeBenefitOptionService : Service<EmployeeOptionBenefit>
    {

        private EmployeeBenefitOptionRepository repositoryLocal;

        public EmployeeBenefitOptionService(EmployeeBenefitOptionRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;

        }

     

        public bool DeleteAll(int employee)
        {
            return repositoryLocal.DeleteAll(employee);
        }

        public IList<EmployeeOptionBenefit> GetBenefitByEmployee(int employee)
        {
            return repositoryLocal.GetByEmployee(employee);
        }
      

        protected override bool ValidateForCreate(EmployeeOptionBenefit t)
        {
            throw new NotImplementedException();
        }

        protected override bool ValidateForUpdate(EmployeeOptionBenefit t)
        {
            throw new NotImplementedException();
        }
    }
}
