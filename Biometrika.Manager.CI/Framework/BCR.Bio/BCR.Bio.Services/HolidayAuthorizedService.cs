﻿using System;
using System.Collections.Generic;
using BCR.Bio.Services.Abstract;
using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class HolidayAuthorizedService : Service<HolidayAuthorized>
    {
        private HolidayAuthorizedRepository repositoryLocal;

        public HolidayAuthorizedService(HolidayAuthorizedRepository repository)
           : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(HolidayAuthorized t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(HolidayAuthorized t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<HolidayAuthorized> GetByHolidayId(int holidayId)
        {
            return repositoryLocal.GetByHolidayId(holidayId);
            //return repositoryLocal.GetByFilter(filter);
        }

        public IList<HolidayAuthorized> GetByValueIdAndHoliday(string valueId, int holidayId)
        {
            return repositoryLocal.GetByValueIdAndHoliday(valueId, holidayId);
            //return repositoryLocal.GetByFilter(filter);
        }
    }
}
