﻿using BCR.System.Configuration;
using BCR.System.Log.Abstract;
using BCR.System.Validation;
using System;

namespace BCR.Bio.Services
{
    /// <summary>
    /// Application entry point
    /// </summary>
    public static class SystemService
    {
        private static SystemConfiguration sysConfiguration;

        /// <summary>
        /// Initilization
        /// </summary>
        /// <param name="systemConfiguration">Aplication parameters </param>
        public static void Initilize(SystemConfiguration systemConfiguration)
        {
            sysConfiguration = systemConfiguration;
        }

        /// <summary>
        /// Get the current Logger
        /// </summary>
        /// <returns></returns>
        public static IAbstractLogger GetLogger()
        {
            //AppDomain domain = AppDomain.CurrentDomain;

            if (sysConfiguration == null)
                sysConfiguration = new SystemConfiguration();
            return sysConfiguration.GetLogger();
        }

        public static SystemUnitOfWork GetSystemUnitOfWork(ValidationDictionary validationDictionary)
        {
            return new SystemUnitOfWork(validationDictionary, SystemConfiguration.ConnectionStringBio, sysConfiguration.GetLogger());
        }
    }
}