﻿using System;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class VehicleMakeService : Service<IVehicleMake>
    {
        VehicleMakeRepository RepositoryLocal = null;
        public VehicleMakeService(VehicleMakeRepository repository)
            : base(repository)
        {
            RepositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IVehicleMake t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(IVehicleMake t)
        {
            return true;
        }

        public IVehicleMake GetByDescription(string vehicleMakeDescription)
        {
            return RepositoryLocal.GetByDescription(vehicleMakeDescription);
        }
    }
}