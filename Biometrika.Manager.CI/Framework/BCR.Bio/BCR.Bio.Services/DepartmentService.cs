﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Services
{
    public class DepartmentService : Service<Department>
    {
        private DepartmentRepository localRepo;

        public DepartmentService(DepartmentRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(Department t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(Department t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }
        public IList<Department> GetByFilter(DepartmentDTO filter)
        {
            return localRepo.GetByFilter(filter);
        }

    }
}
