﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using BCR.System.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class ResidentPermissionService : Service<IResidentPermission>
    {
        private ResidentPermissionRepository repositoryLocal;

        public ResidentPermissionService(ResidentPermissionRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IResidentPermission t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(IResidentPermission t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<IResidentPermission> GetByResidentId(int residentId, bool withResident = false, bool withUnity = false)
        {
            return repositoryLocal.GetByResidentId(residentId, withResident, withUnity);
        }

        public IList<IResidentPermission> GetByUnityId(int unityId, bool withResident = false, bool withUnity = false)
        {
            return repositoryLocal.GetByUnityId(unityId, withResident, withUnity);
        }
        public IResidentPermission GetByUnityIdAndResidentId(int unityId, int residentId, bool withResident = false, bool withUnity = false)
        {
            return repositoryLocal.GetByUnityIdAndResidentId(unityId, residentId, withResident, withUnity);
        }

    }
}
