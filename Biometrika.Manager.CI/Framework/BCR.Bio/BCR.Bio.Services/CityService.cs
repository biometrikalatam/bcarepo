﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;

namespace BCR.Bio.Services
{
    public class CityService : Service<ICity>
    {
        public CityService(CityRepository repository)
            : base(repository)
        {
        }

        protected override bool ValidateForCreate(ICity t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(ICity t)
        {
            throw new NotImplementedException();
        }
    }
}