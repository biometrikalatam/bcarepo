﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class VisitorService : Service<IVisitor>
    {
        private VisitorRepository repositoryLocal;

        public VisitorService(VisitorRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IVisitor t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(IVisitor t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public Visitor GetById(int id, bool withIdentity = false, bool withProvider = false, bool withUnity = false, bool withVehicles = false)
        {
            return repositoryLocal.GetById(id, withIdentity, withProvider, withUnity, withVehicles);
        }

        public IList<IVisitor> GetByCompanyId(int companyId)
        {
            return repositoryLocal.GetByCompanyId(companyId);
        }

        public IList<IVisitor> GetByCompanyAndIdentity(int companyId, string apellidoPaterno = "", string nombre = "", string typeId = "", string valueId = "", bool withUnitys = false)
        {
            return repositoryLocal.GetByCompanyAndIdentity(companyId, apellidoPaterno, nombre, typeId, valueId, withUnitys);
        }

        public IList<Visitor> GetIdentitiesByFiltro(Paginacion paginacion, ICompany company,
                                           string typeid, string valueid,
                                           string name, string lastname, int providerId)
        {
            return repositoryLocal.GetVisitorsByFiltro(paginacion, company, typeid, valueid, name, lastname, providerId);
        }

        public List<IVisitor> GetByFilter(Paginacion paginacion, VisitorSearchDTO visitorSearchDTO)
        {
            return repositoryLocal.GetByFilter(paginacion, visitorSearchDTO);
        }

        public IVisitor AddVehicle(int visitorId, IVehicle vehicle)
        {
            IVisitor visitor = GetById(visitorId, withVehicles: true);

            if (visitor.Vehicles.Count == 0)
                visitor.Vehicles = new List<Vehicle>();

            if (visitor.Vehicles.Where(m => m.PlateNumber == vehicle.PlateNumber).Count() == 0)
            {
                visitor.Vehicles.Add((Vehicle)vehicle);
                Update(visitor);
                return visitor;
            }
            else
            {
                ValidationDictionary.AddError("", "El vehículo existe para el residente");
                return visitor;
            }
        }

        public VisitorFilterDTO GetByFilter(VisitorFilterDTO filter)
        {
            return repositoryLocal.GetByFilter(filter);
        }

        public IList<Visitor> GetByUnity(int unityId, Paginacion paginacion)
        {
            return repositoryLocal.GetByUnity(unityId, paginacion);
        }

        public bool PromoteToResident(string typeId, string valueId, int companyId)
        {
            return repositoryLocal.PromoteToResident(typeId, valueId, companyId);
        }

        public bool PromoteToEmployee(BpIdentity identity)
        {
            return repositoryLocal.PromoteToEmployee(identity);
        }

        public Visitor GetByIdentityId(int identityId)
        {
            return repositoryLocal.GetByIdentityId(identityId);
        }

        public VisitorFilterDTO GetVisitorforVisitorTracking(VisitorFilterDTO filter)
        {
            return repositoryLocal.GetByVisitorFromVisitorTracking(filter);
        }
    }
}