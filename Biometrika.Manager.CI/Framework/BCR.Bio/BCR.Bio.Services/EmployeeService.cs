﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class EmployeeService : Service<IEmployee>
    {
        private EmployeeRepository repositoryLocal;

        public EmployeeService(EmployeeRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IEmployee t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(IEmployee t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<Employee> GetEmployee(int company)
        {
            return repositoryLocal.GetEmployee(company);
        }

        public bool GetEmployeeTracking(string dateto, string datefrom,int consolidado)
        {
             return repositoryLocal.GetEmployeeTracking(dateto, datefrom,consolidado);
        }

        public IList<EmployeeDTO> ListByCompanyId(int companyId)
        {
            return repositoryLocal.ListByCompanyId(companyId);
        }

        public IList<Employee> GetByFilter(EmployeeFilterDTO filter)
        {
            return repositoryLocal.GetByFilter(filter);
        }
        public EmployeeDTO GetEmployeeDto(int id)
        {
            return repositoryLocal.GetEmployeeDto(id);
        }
       
        public Employee GetById(int id, bool withIdentity = false)
        {
            return repositoryLocal.GetById(id, withIdentity);
        }

        public string GetHASHByCompanyId(int companyId)
        {
            return repositoryLocal.GetHASHByCompanyId(companyId);
        }
    }
}