﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class BuildingService : Service<Building>
    {
        private BuildingRepository localRepo;

        public BuildingService(BuildingRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(Building t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(Building t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<Building> GetByZone(int zoneId, Paginacion paginacion = null)
        {
            return localRepo.GetByZone(zoneId, paginacion);
        }

        public IList<Building> GetByCompany(int companyId)
        {
            return localRepo.GetByCompany(companyId);
        }

        /// <summary>
        /// GetByCustomFilter es un filtrado en base a el nuevo tipado de fltros
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public BuildingFilterDTO GetByFilter(BuildingFilterDTO filter)
        {
            return localRepo.GetByFilter(filter);
        }
    }
}