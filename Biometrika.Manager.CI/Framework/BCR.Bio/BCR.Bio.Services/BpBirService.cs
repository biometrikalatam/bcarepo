﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Enum.BioPortal;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using BCR.System.IO;
using BCR.System.Log;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class BpBirService : Service<IBpBir>
    {
        BpBirRepository localRepo = null;
        /// <summary>
        ///
        /// </summary>
        /// <param name="validationDictionary"></param>
        /// <param name="repository"></param>
        public BpBirService(BpBirRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="listBirs"></param>
        /// <param name="xmllistBirs"></param>
        /// <returns></returns>
        public Errors SerializeListBirsToITemplate(List<BpBir> listBirs, out string xmllistBirs)
        {
            Errors ret;
            xmllistBirs = null;
            List<Template> Templates = new List<Template>();
            try
            {
                Template temp;
                foreach (BpBir bir in listBirs)
                {
                    temp = new Template();
                    temp.Id = bir.Id;
                    temp.AuthenticationFactor = bir.Authenticationfactor;
                    temp.MinutiaeType = bir.Minutiaetype;
                    temp.BodyPart = bir.Bodypart;
                    //string[] aux = bir.Data.Split('|');
                    //string sdata = "";
                    //if (aux.Length == 1) sdata = aux[0];
                    //if (aux.Length >= 2) sdata = aux[1];
                    temp.SetData = bir.Data;
                    temp.Companyidenroll = bir.Companyidenroll;
                    temp.Useridenroll = bir.Useridenroll;
                    //if (bir.BpIdentity.Equals(null))
                    //{
                    //    bir.BpIdentity
                    //}
                    temp.Verificationsource = bir.BpIdentity.VerificationSource;
                    Templates.Add(temp);
                }

                xmllistBirs = SerializeHelper.SerializeToXml(Templates);
                temp = null;

                ret = Errors.IERR_OK;
            }
            catch (Exception ex)
            {
                Log4Bio.Error("AdminBpBir.RetrieveWithFilter", ex);
                xmllistBirs = null;
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }

        protected override bool ValidateForCreate(IBpBir t)
        {
            throw new NotImplementedException();
        }

        protected override bool ValidateForUpdate(IBpBir t)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="template"></param>
        /// <param name="companyidenroll"></param>
        /// <param name="useridenroll"></param>
        /// <returns></returns>
        public BpBir ConvertTemplateToBir(ITemplate template, int companyidenroll, int useridenroll)
        {
            //int ret = Errors.IERR_OK;
            BpBir birgenerated = null;
            BpIdentity oIdentity = null;
            try
            {
                birgenerated = new BpBir(oIdentity, template.Type, template.AuthenticationFactor,
                                         template.MinutiaeType, template.BodyPart, template.GetData,
                                         template.AdditionalData, DateTime.Now, DateTime.Now, null,
                                         companyidenroll, useridenroll);
                //ret = Errors.IERR_OK;
            }
            catch (Exception ex)
            {
                Log4Bio.Error("AdminBpBir.ConvertTemplateToBir", ex);

                birgenerated = null;
                //ret = Errors.IERR_UNKNOWN;
            }
            return birgenerated;
        }

        public IList<BpBir> GetByFilter(BpBirFilter filter)
        {
            return localRepo.GetByFilter(filter);
        }
    }
}