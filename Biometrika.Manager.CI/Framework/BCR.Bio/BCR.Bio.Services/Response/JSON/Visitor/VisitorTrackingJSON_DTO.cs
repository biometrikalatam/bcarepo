﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Services.Response.JSON.Visitor
{
    public class VisitorTrackingJSON_DTO
    {
        public int Id { get; set; }
        public string PlateNumber { get; set; }
        public string FechaIngreso { get; set; }
        public string PuntoIngreso { get; set; }
        public string Permanencia { get; set; }
        public string FechaSalida { get; set; }
        public string GUID { get; set; }
        public int TrackingResultId { get; set; }
        public int TrackingActionId { get; set; }
        public string ActionDescription { get; set; }
        public bool AntipassBack { get; set; }
        public string AditionalMessage { get; set; }
    }
}
