﻿using BCR.Bio.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Services.Response.JSON.Visitor
{
    public class VisitDataJSON_DTO
    {
        public int Id { get; set; }
        public int IdentityId { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string FullName { get; set; }
        public string Nombres { get; set; }
        public string ApePat { get; set; }
        public string ApeMat { get; set; }
        public int UnityId { get; set; }
        public string UnityAddress { get; set; }
        public int ResidentId { get; set; }
        public string ResidentFullName { get; set; }
        public VisitorTrackingJSON_DTO LastVisit { get; set; }
        public bool CanEnter { get; set; }
        public Provider Provider { get; set; }
        public string Observacion { get; set; }
    }
}
