﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Services.Response.JSON.TimeRestriction
{
    public class TimeRestrictionJSON_DTO
    {
        public string FechaInicio { get; set; }
        public string FechaTermino { get; set; }
        public List<string> Days { get; set; }
        public string InitTime { get; set; }
        public string EndTime { get; set; }
    }
}
