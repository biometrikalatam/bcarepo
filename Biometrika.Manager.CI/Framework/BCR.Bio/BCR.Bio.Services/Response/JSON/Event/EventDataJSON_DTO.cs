﻿using BCR.Bio.Services.Response.JSON.TimeRestriction;
using BCR.Bio.Services.Response.JSON.Visitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Services.Response.JSON.Event
{
    public class EventDataJSON_DTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public List<TimeRestrictionJSON_DTO> Times { get; set; }
        public string Responsable { get; set; }
        public string Lugar { get; set; }
        public string Direccion { get; set; }
        public string Clase { get; set; }
        public string Tipo { get; set; }
        public List<VisitDataJSON_DTO> VisitorList { get; set; }
        public bool IsList { get; set; }
        public bool CanOperate { get; set; }
        public string Proveedor { get; set; }
    }
}
