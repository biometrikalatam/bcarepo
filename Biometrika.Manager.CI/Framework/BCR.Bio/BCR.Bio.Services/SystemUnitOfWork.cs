﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura.Abstract;
using BCR.Bio.Services.Configuration;
using BCR.System.Log.Abstract;
using BCR.System.Type;
using BCR.System.Validation;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class SystemUnitOfWork : AbstractUnitOfWork
    {
        private static UserService userService;
        private static CompanyService companyService;
        private static CountryService countryService;
        private static UserRolService userRolService;
        private static RolService rolService;
        private static LogService logService;

        private ConnectionString connectionString;

        public SystemUnitOfWork(ValidationDictionary validationDictionary, ConnectionString connectionString, IAbstractLogger abstractLogger)
            : base(validationDictionary, connectionString, abstractLogger)
        {
        }

        public RolService RolService
        {
            get
            {
                if (rolService == null)
                    rolService = new RolService(new ServiceParams<IRol>()
                    {
                        ValidationDictionary = validationDictionary,
                        Repository = GetRepository<IRol>(),
                        Logger = log
                    });
                return rolService;
            }
        }

        public UserRolService UserRolService
        {
            get
            {
                if (userRolService == null)
                    userRolService = new UserRolService(new ServiceParams<UserRol>()
                    {
                        ValidationDictionary = validationDictionary,
                        Repository = GetRepository<UserRol>(),
                        Logger = log
                    });
                return userRolService;
            }
        }

        public UserService UserService
        {
            get
            {
                if (userService == null)
                    userService = new UserService(new ServiceParams<IUser>()
                    {
                        ValidationDictionary = validationDictionary,
                        Repository = GetRepository<IUser>(),
                        Logger = log
                    });

                return userService;
            }
        }

        public CompanyService CompanyService
        {
            get
            {
                if (companyService == null)
                    companyService = new CompanyService(new ServiceParams<ICompany>()
                    {
                        ValidationDictionary = validationDictionary,
                        Repository = GetRepository<ICompany>(),
                        Logger = log
                    });
                return companyService;
            }
        }

        public CountryService CountryService
        {
            get
            {
                if (countryService == null)
                    countryService = new CountryService(new ServiceParams<ICountry>()
                    {
                        ValidationDictionary = validationDictionary,
                        Repository = GetRepository<ICountry>(),
                        Logger = log
                    });
                return countryService;
            }
        }

        public LogService LogService
        {
            get
            {
                if (logService == null)
                    logService = new LogService(new ServiceParams<IBPLog>()
                    {
                        ValidationDictionary = validationDictionary,
                        Repository = GetRepository<IBPLog>(),
                        Logger = log
                    });
                return logService;
            }
        }

        public int CreateUser(IUser user, ICompany company, List<UserRol> roles)
        {
            BeginTransaction();
            user.Company = company;
            int userCreated = UserService.Create(user);

            foreach (var item in roles)
            {
                item.UserId = userCreated;
                UserRolService.Create(item);
            }

            Commit();

            return userCreated;
        }
    }
}