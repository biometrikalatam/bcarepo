﻿using System;
using System.Collections.Generic;
using BCR.Bio.Services.Abstract;
using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class VisitScheduleService : Service<VisitSchedule>
    {
        private VisitScheduleRepository repositoryLocal;

        public VisitScheduleService(VisitScheduleRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(VisitSchedule t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(VisitSchedule t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<VisitSchedule> GetByCompanyId(int companyId)
        {
            return repositoryLocal.GetByCompanyId(companyId);
        }
    }
}
