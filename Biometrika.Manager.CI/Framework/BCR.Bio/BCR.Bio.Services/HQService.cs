﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class HQService : Service<IHQ>
    {
        private HQRepository repositoryLocal;

        public HQService(HQRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IHQ t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(IHQ t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        protected override bool IsEndDatableThenDo(IHQ t, bool activate)
        {
            if (!activate)
                t.EndDate = DateTime.Now;
            else
                t.EndDate = null;

            return true;
        }

        public IList<IHQ> GetByHolding(int holdingId)
        {
            return repositoryLocal.GetByHolding(holdingId);
        }

        public override void CascadeDisable(IHQ t)
        {
            // disable HQs

            foreach (IPoint point in t.Points)
                point.Disable();
            //serviceAccess.PointService.Disable(point);
        }

        /// <summary>
        /// Only enable when company are
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public override Boolean CanBeEnabled(IHQ t)
        {
            return t.Company.EndDate == null;
        }

        public IList<HQ> GetByFilter(HQFilterDTO filter, Paginacion paginacion)
        {
            return repositoryLocal.GetByFilter(filter, paginacion);
        }
    }
}