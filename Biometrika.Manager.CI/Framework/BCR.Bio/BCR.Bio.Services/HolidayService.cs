﻿using System;
using System.Collections.Generic;
using BCR.Bio.Services.Abstract;
using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class HolidayService : Service<Holiday>
    {
        private HolidayRepository repositoryLocal;

        public HolidayService(HolidayRepository repository)
           : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(Holiday t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(Holiday t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<Holiday> GetByCompanyId(int companyId)
        {
            return repositoryLocal.GetByCompanyId(companyId);
        }

        public IList<Holiday> GetByFilter(HolidayFilterDTO filter)
        {
            return repositoryLocal.GetByFilter(filter);
        }
    }
}
