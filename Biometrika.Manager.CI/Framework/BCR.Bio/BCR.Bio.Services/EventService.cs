﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class EventService : Service<Event>
    {
        EventRepository localRepo;

        public EventService(EventRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(Event t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(Event t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<Event> GetByCompany(Paginacion paginacion, int companyId)
        {
            return localRepo.GetByCompany(paginacion, companyId);
        }

        public Event GetByDescription(Paginacion paginacion, int companyId, string description)
        {
            return localRepo.GetByDescription(paginacion, companyId, description);
        }

        public Event GetById(int id, bool withResident = false, bool withVisitor = false, bool withTracking = false)
        {
            return localRepo.GetById(id, withResident, withVisitor, withTracking);
        }

        public IList<Event> GetByCompanyIdAndMonthInterval(int companyId, DateTime startDate, DateTime? endDate = null)
        {
            return localRepo.GetByCompanyIdAndMonthInterval(companyId, startDate, endDate);
        }

        public IList<Event> GetByCompanyAndUnity(int companyId, int unityId)
        {
            return localRepo.GetByCompanyAndUnity(companyId, unityId);
        }

        public IList<Event> GetByIdentityAndCompany(int companyId, string typeId, string valueId)
        {
            return localRepo.GetByIdentityAndCompany(companyId, typeId, valueId);
        }

        public EventFilterDTO GetByFilter(EventFilterDTO filter)
        {
            return localRepo.GetByFilter(filter);
        }
    }
}