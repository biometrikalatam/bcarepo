﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class BpVerifiedService : Service<BpVerified>
    {
        public BpVerifiedService(BpVerifiedRepository repository)
            : base(repository)
        {
        }

        protected override bool ValidateForCreate(BpVerified t)
        {
            throw new global::System.NotImplementedException();
        }

        protected override bool ValidateForUpdate(BpVerified t)
        {
            throw new global::System.NotImplementedException();
        }
    }
}