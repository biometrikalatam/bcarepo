﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class WorkDayService : Service<IWorkDay>
    {
        public WorkDayService(WorkDayRepository repository)
            : base(repository)
        {
        }

        protected override bool ValidateForCreate(IWorkDay t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(IWorkDay t)
        {
            return true;
        }
    }
}