﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCR.Bio.Services
{
    public class EmployeeRegisterMethodService : Service<EmployeeRegisterMethod>
    {
        private EmployeeRegisterMethodRepository repositoryLocal;

        public EmployeeRegisterMethodService(EmployeeRegisterMethodRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;

        }

        public IList<EmployeeRegisterMethod> GetByEmployee(int id)
        {
            return repositoryLocal.GetByEmployee(id);
        }

        public bool DeleteAll(int employee)
        {
            return repositoryLocal.DeleteAll(employee);
        }

        protected override bool ValidateForCreate(EmployeeRegisterMethod t)
        {
            throw new NotImplementedException();
        }

        protected override bool ValidateForUpdate(EmployeeRegisterMethod t)
        {
            throw new NotImplementedException();
        }
    }
}
