﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class ProviderService : Service<Provider>

    {
        ProviderRepository localRepo;

        public ProviderService(ProviderRepository repositoryParams) : base(repositoryParams)
        {
            localRepo = repositoryParams;
        }

        protected override Boolean ValidateForCreate(Provider t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override Boolean ValidateForUpdate(Provider t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<Provider> GetProvidersBy(Paginacion paginacion, ICompany company, string name, string identifier, bool withVehicles = false, bool withVisitors = false, bool onlyActive = false)
        {
            return localRepo.GetProvidersBy(paginacion, company, name, identifier, withVehicles, withVisitors, onlyActive);
        }

        public IList<Provider> GetByCompany(ICompany company)
        {
            return localRepo.GetByCompany(company);
        }

        public Provider GetById(int id, bool withVisitor = false, bool withVehicle = false)
        {
            return localRepo.GetById(id, withVisitor, withVehicle);
        }

        /// <summary>
        /// GetByCustomFilter es un filtrado en base a el nuevo tipado de fltros
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IList<Provider> GetByCustomFilter(ProviderFilterDTO filter)
        {
            return localRepo.GetByCustomFilter(filter);
        }

        /// <summary>
        /// Retorna el hash de la tabla proveedores de acuerdo al companyId
        /// </summary>
        /// <param name="companyId">Id de la tabla company</param>
        /// <returns>Hash del nombre e IDs de proveedores</returns>
        public string GetHASHByCompanyId(int companyId)
        {
            return localRepo.GetHASHByCompanyId(companyId);
        }
    }
}