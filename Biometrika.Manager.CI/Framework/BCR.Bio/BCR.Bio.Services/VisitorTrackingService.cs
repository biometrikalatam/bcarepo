﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BCR.Bio.Services
{
    //public class TrackingEventArgs : EventArgs
    //{
    //    public int TrackingId { get; set; }
    //}  

    //public delegate void NewTrackingEventHandler(object sender, TrackingEventArgs e);

    public class VisitorTrackingService : Service<IVisitorTracking>
    {

        //public static event NewTrackingEventHandler NewTracking;

        private VisitorTrackingRepository repositoryLocal;

        public VisitorTrackingService(VisitorTrackingRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IVisitorTracking t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(IVisitorTracking t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        //public override void OnCreated(int id)
        //{
        //    //IVisitorTracking visitorTracking  = GetById(id);
        //    if (NewTracking != null)
        //        NewTracking(this, new TrackingEventArgs() { TrackingId = id });
        //}

        public IList<VisitorTrackingResultDTO> GetBy(VisitorTrackingDTO param)
        {
            return repositoryLocal.GetBy(param);
        }

        public VehicleVisitorTrackingResultDTO GetVehicleBy(VisitorTrackingDTO param)
        {
            return repositoryLocal.GetVehicleBy(param);
        }

        public IList<VisitorTrackingResultDTO> GetTrackings(VisitorTrackingDTO param)
        {
            return repositoryLocal.GetTrackings(param);
        }

        public IList<VisitorTrackingExtDTO> GetByFilter(BaseFilterParam param)
        {
            return repositoryLocal.GetByFilter(param);
        }

        public IList<VisitorTrackingExtDTO> GetLastTracks(BaseFilterParam param)
        {
            return repositoryLocal.GetLastTracks(param);
        }

        public IList<VisitorTrackingResultDTO> GetOpenTrackings( VisitorTrackingDTO param)
        {
            return repositoryLocal.GetOpenTrackings( param);
        }

        public VisitorTrackingResultDTO GetLastOpenedTracking(int companiId, int visitorId) // hay que meterle mas filtros, pero eso es determinado por la regla de antipassback
        {                                                                                 // deberia tener algo por defecto?
            VisitorTrackingResultDTO tracking = null;                                     // mas trackings mas lento

            List<VisitorTrackingResultDTO> list = GetBy(new VisitorTrackingDTO()
                                                        {
                                                            Company = companiId,
                                                            VisitorId = visitorId
                                                        })
                                                       .ToList();

            var groupByGUID = list.GroupBy(x => x.TrackingGuid);

            foreach (var itemByGUID in groupByGUID)
            {
                foreach (var visitorTracking in itemByGUID)
                {
                    if (visitorTracking.ActionDescription.Contains("Egreso"))
                    {
                        list.RemoveAll(x => x.TrackingGuid.Contains(visitorTracking.TrackingGuid) &&
                                        x.VisitorValueId.Contains(visitorTracking.VisitorValueId));
                        break;
                    }

                    if (visitorTracking.ActionDescription.Contains("Ingreso"))
                    {
                        visitorTracking.Permanencia = DateTime.Now - visitorTracking.EventTime;
                    }

                    if (visitorTracking.ActionDescription.Contains("Control"))
                    {
                        list.Remove(visitorTracking);
                    }
                }
            }

            tracking = list.FirstOrDefault();
            return tracking;
        }

        public IList<VisitorPerHour> VisitorPerHour(DateTime dia, int companyId)
        {
            return repositoryLocal.VisitorPerHour(dia, companyId);
        }

        public IList<VisitorTracking> GetByPlateNumber(VisitorTrackingFilter filter)
        {
            return repositoryLocal.GetByPlateNumber(filter);
        }

        public bool IsDuplicateTracking(string valueId, DateTime fecha)
        {
            return repositoryLocal.IsDuplicateTracking(valueId, fecha);
        }

        public IList<VisitorTracking> GetByFilter(VisitorTrackingFilter filter)
        {
            return repositoryLocal.GetByFilter(filter);
        }
    }
}