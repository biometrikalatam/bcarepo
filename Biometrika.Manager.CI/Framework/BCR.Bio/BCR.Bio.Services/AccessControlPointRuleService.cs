﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class AccessControlPointRuleService : Service<AccessControlPointRule>
    {
        public AccessControlPointRuleService(AccessControlPointRuleRepository repository)
            : base(repository)
        {
        }

        protected override bool ValidateForCreate(AccessControlPointRule t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(AccessControlPointRule t)
        {
            return true;
        }
    }
}