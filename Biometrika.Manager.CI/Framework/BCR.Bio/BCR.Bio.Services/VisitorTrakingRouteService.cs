﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura.Interface;
using BCR.Bio.Services.Abstract;
using System;

namespace BCR.Bio.Services
{
    public class VisitorTrakingRouteService : Service<VisitorTrakingRoute>
    {
        public VisitorTrakingRouteService(IRepository<VisitorTrakingRoute> repositoryParams) : base(repositoryParams)
        {
        }

        protected override bool ValidateForCreate(VisitorTrakingRoute t)
        {
            throw new NotImplementedException();
        }

        protected override bool ValidateForUpdate(VisitorTrakingRoute t)
        {
            throw new NotImplementedException();
        }
    }
}