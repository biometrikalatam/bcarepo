﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BCR.Bio.Services.Abstract;
using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;

namespace BCR.Bio.Services
{
    public class VisitCounterResetDateService : Service<IVisitCounterResetDate>
    {
        private VisitCounterResetDateRepository repositoryLocal;

        public VisitCounterResetDateService(VisitCounterResetDateRepository repository)
            : base(repository)

        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IVisitCounterResetDate t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(IVisitCounterResetDate t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<IVisitCounterResetDate> GetByCompanyId(int companyId)
        {
            return repositoryLocal.GetByCompanyId(companyId);
        }

    }
}
