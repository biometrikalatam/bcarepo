﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using BCR.System.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using BCR.Bio.Domain.DTO;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class ResidentService : Service<IResident>
    {
        private ResidentRepository repositoryLocal;

        public ResidentService(ResidentRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IResident t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        public string GetHASHByCompanyId(int companyId)
        {
            return repositoryLocal.GetHASHByCompanyId(companyId);
        }

        protected override bool ValidateForUpdate(IResident t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IResident GetById(int id, bool withVehicles = false, bool withUnity = false, bool withIdentity = false)
        {
            return repositoryLocal.GetById(id, withVehicles, withUnity, withIdentity);
        }

        public IList<IResident> GetByCompanyId(int companyId)
        {
            return repositoryLocal.GetByCompanyId(companyId);
        }

        public IList<IResident> GetByCompanyAndIdentity(int companyId, string apellidoPaterno = "", string nombre = "", string typeId = "", string numberId = null)
        {
            return repositoryLocal.GetByCompanyAndIdentity(companyId, apellidoPaterno, nombre, typeId, numberId);
        }

        public IList<IResident> GetByFilter(Paginacion paginacion, ResidentSearchDTO residentSearchDTO)
        {
            return repositoryLocal.GetByFilter(paginacion, residentSearchDTO);
        }

        public IResident AddVehicle(int residentId, IVehicle vehicle)
        {
            IResident resident = GetById(residentId,withVehicles:true);

            if (resident.Vehicles.Count == 0)
                resident.Vehicles = new List<Vehicle>();

            if (resident.Vehicles.Where(m => m.PlateNumber == vehicle.PlateNumber).Count() == 0)
            {
                resident.Vehicles.Add((Vehicle)vehicle);
                Update(resident);
                return resident;
            }
            else
            {
                ValidationDictionary.AddError("", "El vehículo existe para el residente");
                return null;
            }
        }

        /// <summary>
        /// GetByCustomFilter es un filtrado en base a el nuevo tipado de fltros
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ResidentFilterDTO GetByCustomFilter(ResidentFilterDTO filter)
        {
            return repositoryLocal.GetByCustomFilter(filter);
        }

        public IList<Resident> GetByUnity(int unityId, Paginacion paginacion = null)
        {
            return repositoryLocal.GetByUnity(unityId, paginacion);
        }
    }
}