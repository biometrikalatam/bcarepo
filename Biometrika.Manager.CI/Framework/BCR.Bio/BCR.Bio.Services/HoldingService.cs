﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class HoldingService : Service<IHolding>
    {
        public HoldingService(HoldingRepository repository)
            : base(repository)
        {
        }

        protected override bool ValidateForCreate(IHolding t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(IHolding t)
        {
            return true;
        }
    }
}