﻿using System;
using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System.Collections.Generic;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class AccessControlService : Service<AccessControl>
    {
        AccessControlRepository AccessControlRepository = null;
        public AccessControlService(AccessControlRepository repository)
            : base(repository)
        {
            AccessControlRepository = repository;
        }

        protected override bool ValidateForCreate(AccessControl t)
        {
            if (t == null)
                return false;

            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(AccessControl t)
        {
            return true;
        }

        public AccessControl GetByCompanyId(int companyId)
        {
            return AccessControlRepository.GetByCompanyId(companyId);
        }

        public IList<AccessControl> GetByFilter(AccessControlFilter filter)
        {
            return AccessControlRepository.GetByFilter(filter);
        }
    }
}