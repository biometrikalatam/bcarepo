﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class AutorizationService : Service<IAutorization>
    {
        public AutorizationService(AutorizationRepository repository)
            : base(repository)
        {
        }

        protected override bool ValidateForCreate(IAutorization t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(IAutorization t)
        {
            return true;
        }
    }
}