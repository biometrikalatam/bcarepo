﻿using BCR.Bio.Domain;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class PointToPointDependencyService : Service<PointToPointDependency>
    {
        private PointToPointDependencyRepository localRepo;

        public PointToPointDependencyService(PointToPointDependencyRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(PointToPointDependency t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(PointToPointDependency t)
        {
            return true;
        }
    }
}