﻿using BCR.Bio.Domain;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCR.Bio.Infraestructura.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class VehicleTrackingService : Service<VehicleTracking>
    {
        VehicleTrackingRepository Repository = null;

        public VehicleTrackingService(VehicleTrackingRepository repository) 
            : base(repository)
        {
            Repository = repository;
        }

        protected override bool ValidateForCreate(VehicleTracking t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(VehicleTracking t)
        {
            return true;
        }

        public List<VehicleTracking> GetByFilter(VehicleTrackingFilter filter)
        {
            return Repository.GetByFilter(filter);
        }
    }
}
