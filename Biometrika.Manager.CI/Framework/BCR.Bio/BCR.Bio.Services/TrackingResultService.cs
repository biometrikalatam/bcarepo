﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class TrackingResultService : Service<ITrackingResult>
    {
        TrackingResultRepository localRepo;

        public TrackingResultService(TrackingResultRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(ITrackingResult t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(ITrackingResult t)
        {
            return true;
        }

        public ITrackingResult GetByName(string description)
        {
            return localRepo.GetByName(description);
        }
    }
}