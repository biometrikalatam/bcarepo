﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using BCR.System.Log;
using NISOCountries.Core;
using NISOCountries.GeoNames;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BCR.System.Extension;

namespace BCR.Bio.Services
{
    public class CountryService : Service<ICountry>
    {
        private CountryRepository localRepo;

        public CountryService(CountryRepository repository)
            : base(repository)
        {
            localRepo = repository; ;
        }

        protected override bool ValidateForCreate(ICountry t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(ICountry t)
        {
            return true;
        }


        /// <summary>
        /// Permite actualizar los datos de la tabla Country en base a los datos ISO3166 de GeoNames.org
        /// Solo es necesario la primera vez para alimentar con datos, luego si se necesita actualizar los datos
        /// se puede volver a ejecutar manualmente.
        /// Por ahora solo Crea y Actualiza datos
        /// </summary>
        public void RefreshData()
        {
            try
            {
                List<GeonamesCountry> isoCountryList = new GeonamesISOCountryReader().GetDefault().ToList();
                if (isoCountryList != null && isoCountryList.Count > 0)
                {
                    IList<Country> list = localRepo.GetList();
                    list.ToList().ForEach(x => UpdateFromISO3611(x, isoCountryList));
                    // Se reccore el listado de GeoCountry para insertar en la base de datos
                    isoCountryList.ForEach(x => SaveFromISO3611(x));
                }
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error al recuperar CountryService -> ReLoadFromWeb", e);
            }
        }

        private int SaveFromISO3611(GeonamesCountry geoNamesCountry)
        {
            Country newCountry = new Country
            {
                Description = geoNamesCountry.CountryName.ToUpper(),
                Alpha2 = geoNamesCountry.Alpha2,
                Alpha3 = geoNamesCountry.Alpha3,
                NumericCode = geoNamesCountry.Numeric,
                CountryRawName = geoNamesCountry.CountryName.ToUpper()
            };

            return Create(newCountry);
        }

        /// <summary>
        /// Esto solo es para poder aceptar datos de una tabla Country ya poblada, donde los Country.Description
        /// fueron ingresados con acentuación. Impide duplicidad de Country. La busqueda inicial es en base al 
        /// Country.Description pero luego de aceptado el formato de carga y procesado lo viejo se debe modificar a 
        /// buscar por Alpha2, Alpha3 para mantener un orden en los codigos
        /// </summary>
        /// <param name="country"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        private int UpdateFromISO3611(Country country, List<GeonamesCountry> algo)
        {
            int res = 0;
            string countryName = country.Description.RemoveDiacritics();

            GeonamesCountry isoCountry = algo
                                         .Where(x => x.CountryName.ToUpper().CompareTo(countryName.ToUpper()) == 0)
                                         .FirstOrDefault();

            if (isoCountry != null)
            {
                // existe, update
                country.Alpha2 = isoCountry.Alpha2;
                country.Alpha3 = isoCountry.Alpha3;
                country.NumericCode = isoCountry.Numeric;
                country.CountryRawName = isoCountry.CountryName.ToUpper();
                country.Description = isoCountry.CountryName.ToUpper();

                ICountry tempCountry = Update(country);
                res = tempCountry != null ? tempCountry.Id : -1;
                algo.Remove(isoCountry);
            }
            return res;
        }

        /// <summary>
        /// GetByFilter es un filtrado en base a el nuevo tipado de fltros
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public CountryFilterDTO GetByFilter(CountryFilterDTO filter)
        {
            return localRepo.GetByFilter(filter);
        }

        /// <summary>
        /// Busqueda de nacionalidad por acronimo Alpha3 ISO3166
        /// </summary>
        /// <param name="Alpha3">Codigo ISO3166-Alpha3</param>
        /// <returns></returns>
        public Country GetByAlpha3(string Alpha3)
        {
            return localRepo.GetByAlpha3(Alpha3);
        }
    }
}