﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class VehicleBlackListService : Service<IVehicleBlackList>
    {
        private VehicleBlackListRepository repositoryLocal;

        public VehicleBlackListService(VehicleBlackListRepository repository)
            : base(repository)
        {
            repositoryLocal = (VehicleBlackListRepository)base.repository;
        }

        protected override bool ValidateForCreate(IVehicleBlackList t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(IVehicleBlackList t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<IVehicleBlackList> GetByCompanyAndPlateNumber(int companyId, string plateNumber, bool active = false)
        {
            return repositoryLocal.GetByCompanyAndPlateNumber(companyId, plateNumber, active);
        }

        public IList<IVehicleBlackList> GetByCompanyId(int companyId)
        {
            return repositoryLocal.GetByCompanyId(companyId);
        }

        public bool IsInBlackList(int companyId, string patente)
        {
            return repositoryLocal.GetByCompanyAndPlateNumber(companyId, patente, true).Count != 0 ? true : false;
        }

        public IList<VehicleBlackList> GetByFilter(Paginacion paginacion, VehicleBlackList filter)
        {
            return repositoryLocal.GetByFilter(paginacion, filter);
        }

        public IList<VehicleBlackList> GetByFilter(VehicleBlackListFilter filter)
        {
            return repositoryLocal.GetByFilter(filter);
        }
    }
}