﻿using BCR.Bio.Domain;
using BCR.Bio.Domain.DTO.Filter;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;

namespace BCR.Bio.Services
{
    public class ZoneService : Service<Zone>
    {
        ZoneRepository localRepo;

        public ZoneService(ZoneRepository repository)
            : base(repository)
        {
            localRepo = repository;
        }

        protected override bool ValidateForCreate(Zone t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(Zone t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<Zone> GetByCompany(int companyId)
        {
            return localRepo.GetByCompany(companyId);
        }

        /// <summary>
        /// GetByCustomFilter es un filtrado en base a el nuevo tipado de fltros
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ZoneFilterDTO GetByFilter(ZoneFilterDTO filter)
        {
            return localRepo.GetByFilter(filter);
        }
    }
}