﻿using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;
using System;
using System.Collections.Generic;
using BCR.Bio.Domain.DTO.Filter;

namespace BCR.Bio.Services
{
    public class IdentityBlackListService : Service<IIdentityBlackList>
    {
        private IdentityBlackListRepository repositoryLocal;

        public IdentityBlackListService(IdentityBlackListRepository repository)
            : base(repository)
        {
            repositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IIdentityBlackList t)
        {
            t.CreateDate = DateTime.Now;
            return true;
        }

        protected override bool ValidateForUpdate(IIdentityBlackList t)
        {
            t.UpdateDate = DateTime.Now;
            return true;
        }

        public IList<IIdentityBlackList> GetByCompanyTypeIdAndValueId(int companyId, string typeId, string valueId, bool active = false)
        {
            return repositoryLocal.GetByCompanyTypeIdAndValueId(companyId, typeId, valueId, active);
        }

        public bool IsInBlackList(int companyId, string typeId, string valueId)
        {
            return repositoryLocal.GetByCompanyTypeIdAndValueId(companyId, typeId, valueId, true).Count != 0 ? true : false;
        }

        public IList<IIdentityBlackList> GetByCompanyId(int companyId)
        {
            return repositoryLocal.GetByCompanyId(companyId);
        }

        public IdentityBlackListFilterDTO GetByFilter(IdentityBlackListFilterDTO filter)
        {
            return repositoryLocal.GetByFilter(filter);
        }
    }
}