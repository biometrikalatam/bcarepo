﻿using System;
using BCR.Bio.Domain.Interface;
using BCR.Bio.Infraestructura;
using BCR.Bio.Services.Abstract;

namespace BCR.Bio.Services
{
    public class VehicleModelService : Service<IVehicleModel>
    {
        VehicleModelRepository RepositoryLocal = null;
        public VehicleModelService(VehicleModelRepository repository)
            : base(repository)
        {
            RepositoryLocal = repository;
        }

        protected override bool ValidateForCreate(IVehicleModel t)
        {
            return true;
        }

        protected override bool ValidateForUpdate(IVehicleModel t)
        {
            return true;
        }

        public IVehicleModel GetByDescription(string vehicleMakeDescription)
        {
            return RepositoryLocal.GetByDescription(vehicleMakeDescription);
        }
    }
}