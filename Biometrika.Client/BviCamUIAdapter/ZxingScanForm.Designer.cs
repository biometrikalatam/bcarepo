﻿namespace BviCamUIAdapter
{
    partial class ZxingScanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZxingScanForm));
            this.txtBarcodeFormat = new System.Windows.Forms.TextBox();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.cmbDevice = new System.Windows.Forms.ComboBox();
            this.labTitleRut = new System.Windows.Forms.Label();
            this.lblRut = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.labTitle = new System.Windows.Forms.Label();
            this.labBordeLogo = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBarcodeFormat
            // 
            this.txtBarcodeFormat.Location = new System.Drawing.Point(12, 625);
            this.txtBarcodeFormat.Name = "txtBarcodeFormat";
            this.txtBarcodeFormat.Size = new System.Drawing.Size(100, 20);
            this.txtBarcodeFormat.TabIndex = 1;
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(12, 623);
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(489, 20);
            this.txtContent.TabIndex = 2;
            // 
            // cmbDevice
            // 
            this.cmbDevice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDevice.FormattingEnabled = true;
            this.cmbDevice.Location = new System.Drawing.Point(12, 630);
            this.cmbDevice.Name = "cmbDevice";
            this.cmbDevice.Size = new System.Drawing.Size(270, 21);
            this.cmbDevice.TabIndex = 3;
            this.cmbDevice.SelectedIndexChanged += new System.EventHandler(this.cmbDevice_SelectedIndexChanged);
            // 
            // labTitleRut
            // 
            this.labTitleRut.AutoSize = true;
            this.labTitleRut.BackColor = System.Drawing.Color.OrangeRed;
            this.labTitleRut.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTitleRut.ForeColor = System.Drawing.Color.White;
            this.labTitleRut.Location = new System.Drawing.Point(161, 19);
            this.labTitleRut.Name = "labTitleRut";
            this.labTitleRut.Size = new System.Drawing.Size(224, 37);
            this.labTitleRut.TabIndex = 4;
            this.labTitleRut.Text = "Verificando RUT";
            // 
            // lblRut
            // 
            this.lblRut.AutoSize = true;
            this.lblRut.BackColor = System.Drawing.Color.OrangeRed;
            this.lblRut.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRut.ForeColor = System.Drawing.Color.White;
            this.lblRut.Location = new System.Drawing.Point(378, 19);
            this.lblRut.Name = "lblRut";
            this.lblRut.Size = new System.Drawing.Size(172, 37);
            this.lblRut.TabIndex = 5;
            this.lblRut.Text = "11111111-1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 578);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(260, 22);
            this.label2.TabIndex = 7;
            this.label2.Text = "Escanee el código de Barra...";
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.Color.OrangeRed;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.ForeColor = System.Drawing.Color.White;
            this.btnSalir.Location = new System.Drawing.Point(524, 565);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(99, 30);
            this.btnSalir.TabIndex = 8;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.Location = new System.Drawing.Point(180, 539);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(45, 16);
            this.lblMessage.TabIndex = 9;
            this.lblMessage.Text = "label3";
            // 
            // labTitle
            // 
            this.labTitle.BackColor = System.Drawing.Color.OrangeRed;
            this.labTitle.Location = new System.Drawing.Point(-1, 11);
            this.labTitle.Name = "labTitle";
            this.labTitle.Size = new System.Drawing.Size(661, 54);
            this.labTitle.TabIndex = 10;
            // 
            // labBordeLogo
            // 
            this.labBordeLogo.BackColor = System.Drawing.Color.OrangeRed;
            this.labBordeLogo.Location = new System.Drawing.Point(16, 5);
            this.labBordeLogo.Name = "labBordeLogo";
            this.labBordeLogo.Size = new System.Drawing.Size(74, 73);
            this.labBordeLogo.TabIndex = 11;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::BviCamUIAdapter.Properties.Resources.icono_36x36;
            this.pictureBox2.Location = new System.Drawing.Point(30, 19);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(48, 48);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 83);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(661, 444);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(26, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 55);
            this.label4.TabIndex = 12;
            // 
            // ZxingScanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(660, 609);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labBordeLogo);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblRut);
            this.Controls.Add(this.labTitleRut);
            this.Controls.Add(this.cmbDevice);
            this.Controls.Add(this.txtContent);
            this.Controls.Add(this.txtBarcodeFormat);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ZxingScanForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Biometrika Identity Verification v2...";
            this.Load += new System.EventHandler(this.ZxingScanForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtBarcodeFormat;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.ComboBox cmbDevice;
        private System.Windows.Forms.Label labTitleRut;
        private System.Windows.Forms.Label lblRut;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label labTitle;
        private System.Windows.Forms.Label labBordeLogo;
        private System.Windows.Forms.Label label4;
    }
}