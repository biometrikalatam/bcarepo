﻿using BiometrikaComponentServiceDomain.Model;
using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BviCamUIAdapter
{
    public class Configuracion
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(Configuracion));

        public  BioPacket Packet { get; set; }

        public Solicitud solicitud { get; set; }

        public App app;

        internal Configuration appConfig;
        

        Mutex _instanceMutex = null;
        static string appGuid = "08b06c0f-25b9-4d2d-9dce-1f1ac0e3e1de";
        
        public Configuracion()
        {
            //if(app==null)
            //    app = new App();
        }

        public void SetPacket(NameValueCollection queryString)
        {
            Packet = new BioPacket();
            Packet.PacketParamIn = queryString;
            Packet.PacketParamOut = new Dictionary<string, object>();
            
        }
       
        public void OpenWindow()
        {
            int ret = 0;
            ret = CheckParameters(Packet);
            try
            {
                logger.Info("Validado los parámetros se construye la solicitud");
                Thread thread = new Thread(() =>
                {
                    logger.Info("Biometrika BVICam - Se encuentra activada la verificación de webcam");
                    ScanBarCode scanbarcode = new ScanBarCode();
                    scanbarcode.Channel(Packet);
                    logger.Info("Biometrika BVICam.. Se definen los parámetros");
                    scanbarcode.Activate();
                    scanbarcode.ValueId = dictParamIn["VALUEID"];
                    logger.Info("Biometrika BVICam.. Se define el Rut");
                    logger.Info("solicitud:" + solicitud.ToString());
                    scanbarcode.solicitud = solicitud;
                    logger.Info("Se definen solicitud");
                    if (ret == 0)
                        scanbarcode.Statusparameter = true;
                    else
                        scanbarcode.Statusparameter = false;
                    logger.Info(scanbarcode.Statusparameter);
                    scanbarcode.ShowDialog();
                    scanbarcode.Activate();
                    
                    
                });
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
                thread.Join();

            }
            catch (Exception exe)
            {
                logger.Error(exe.Message);
            }

        }

        private void doStuff()
        {
            MainWindow objMain = new MainWindow(solicitud);
            objMain.bioPacketLocal = Packet;
            //objMain.barcodeWebCam = scanbarcode.Barcodewebcam;
            objMain.ShowDialog();
        }

        private Solicitud BuildSolicitud()
        {
            try
            {
                solicitud = new Solicitud();
                solicitud.Persona = new Persona();
                logger.Info("Biometrika BVICAM.. se construye el objeto persona");
                solicitud.Persona.Rut = dictParamIn["VALUEID"];
                logger.Info(dictParamIn["VALUEID"]);
                solicitud.Configuracion = new BiometrikaComponentServiceDomain.Configuracion();
                solicitud.Configuracion.ExtraerDatosPersonales = Convert.ToBoolean(appConfig.AppSettings.Settings["ExtraerDatosPersonales"].Value);
                solicitud.Configuracion.ExtraerFirma = Convert.ToBoolean(appConfig.AppSettings.Settings["ExtraerFirma"].Value);
                solicitud.Configuracion.extraerFoto = Convert.ToBoolean(appConfig.AppSettings.Settings["ExtraerFoto"].Value); 
                solicitud.Configuracion.bloqueoCedula = Convert.ToBoolean(appConfig.AppSettings.Settings["BloquearDocumentoVencido"].Value); 
                solicitud.Configuracion.iso = Convert.ToBoolean(appConfig.AppSettings.Settings["iso"].Value); 
                solicitud.Configuracion.ansi = Convert.ToBoolean(appConfig.AppSettings.Settings["ansi"].Value);
                solicitud.Configuracion.raw = Convert.ToBoolean(appConfig.AppSettings.Settings["raw"].Value); 
                solicitud.Configuracion.vigenciaCedula = Convert.ToBoolean(appConfig.AppSettings.Settings["vigenciaCedula"].Value); ;
                solicitud.Configuracion.wsq = Convert.ToBoolean(appConfig.AppSettings.Settings["wsq"].Value); ;
                solicitud.Configuracion.jpeg = Convert.ToBoolean(appConfig.AppSettings.Settings["jpeg"].Value); 
            }
            catch (Exception ex)
            {
                logger.Error("aaaaa:" + ex.Message);
            }
            logger.Info("Termine de construir la solicitud");
            return solicitud;

        }
        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        public int CheckParameters(BioPacket bioPacketLocal)
        {

            int ret = 0;
            try
            {
                logger.Info("Biometrika BioCam.CheckParameters In...");
                if (bioPacketLocal == null) ret = -2;  //Lista de parametros no puede ser nulo

                logger.Debug("Biometrika BioCam.CheckParameters filling dictParamin...");
                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    logger.Debug("Biometrika BVICAM.CheckParameters     Adding => " + k + "=" + (string)bioPacketLocal.PacketParamIn[k] + "...");
                    dictParamIn.Add(k.ToUpper(), bioPacketLocal.PacketParamIn[k]);

                }
                logger.Debug("Biometrika BVICAM.CheckParameters dictParamin filled [length = " + dictParamIn.Count + "]");
                if ((!dictParamIn.ContainsKey("SRC") || String.IsNullOrEmpty(dictParamIn["SRC"]))
                    && ((!dictParamIn.ContainsKey("VALUEID") || String.IsNullOrEmpty(dictParamIn["VALUEID"]))))
                {
                    ret = -3;  //src/TypeId/ValueId deben ser enviados como parámetros
                    logger.Debug("Biometrika BVICAM.CheckParameters src deben ser enviados como parámetros!");
                }
                else
                {

                    logger.Info("Biometrika BVICAM ..Seteamos el rut definido");


                    logger.Info("Biometrika BVICAM ..Definimos si viene el parámetro solicitud");
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    try
                    {

                        if (dictParamIn["SOLICITUD"] != null)
                        {
                            solicitud = jsonSerializer.Deserialize<Solicitud>(dictParamIn["SOLICITUD"]);
                            if (solicitud != null)
                            {
                                logger.Info("Biometrika BVICAM ..La solicitud viene de acuerdo a los parámetros solicitados");

                            }
                            else
                            {

                                logger.Error("Biometrika BVICAM .. se asume una verificación por defecto con los mínimos de valores configurados en parámetros");
                                solicitud = BuildSolicitud();
                            }
                        }
                        else
                        {
                            logger.Error("Biometrika BVICAM .. se asume una verificación por defecto con los mínimos de valores configurados en parámetros");
                            solicitud = BuildSolicitud();
                        }

                    }
                    catch (Exception exe)
                    {
                        logger.Error("Biometrika BVICAM .. se asume una verificación por defecto con los mínimos de valores configurados en parámetros", exe);
                        logger.Info("Construye la solicitud");
                        solicitud = BuildSolicitud();
                    }
                    ret = 0;

                }


            }
            catch (Exception ex)
            {
                logger.Error("Error Biometrika BviCam.CheckParameters [" + ex.Message + "]");
                ret = -1;
            }
            logger.Info("Biometrika BviCam.CheckParameters Ount [ret=" + ret + "]!");
            return ret;
        }

    }
}

