﻿using Bio.Core.CLR;
using BiometrikaComponentServiceDomain.Model;
using BiometrikaComponentServiceDomain.Model.Enum;
using BviCamUIAdapter.Libs;
using BviCamUIAdapter.Model;
using BviCamUIAdapter.Model.Service;
using BviCamUIAdapter.ViewModel;
using LibBiometrikaVerification;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LibBiometrikaVerification;
using BCR_RegistroCivil_Wrapper;
using BCR_RegistroCivil_Wrapper.Response;
using BCR_RegistroCivil_Wrapper.Enum;
using CSJ2K;
using System.Drawing.Imaging;
using CSJ2K.Util;
using System.Drawing;
using System.IO;
using System.Threading;
using System.ServiceModel;
using log4net;
using Domain;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Reflection;

namespace BviCamUIAdapter
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BCR LectorCodigoDeBarras;
        FPR LectorHuellaDigital;

        EstadoVerificacion Paso;
        Solicitud SolicitudVerificacion;

        public BioPacket bioPacketLocal;

        public BarcodeWebCam barcodeWebCam;

        public int Timeout;

        BVIResponse respuesta;

        public MainWindowViewModel ViewModel;
        private static readonly ILog logger = LogManager.GetLogger(typeof(MainWindow));

        internal Configuration appConfig;

        /// <summary>
        /// Constructor de la ventana
        /// </summary>

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            Closing += MainWindowClosing;

            appConfig = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);

        }

        /// <summary>
        /// Constructor de la ventana con parámetro
        /// </summary>
        /// <param name="solicitud"></param>
        public MainWindow(Solicitud solicitud)
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            Closing += MainWindowClosing;
            appConfig = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);
            SetupProxy();            
            SolicitudVerificacion = solicitud;
            
        }
        /// <summary>
        /// Constructor de la ventana con parámetro
        /// </summary>
        /// <param name="solicitud"></param>
        public MainWindow(Solicitud solicitud,int timeout)
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            Closing += MainWindowClosing;
            if(timeout<0)
                Timeout = 3;
            else
                Timeout = timeout;
            appConfig = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);
            SetupProxy();
            SolicitudVerificacion = solicitud;
            
        }

        void SetupProxy()
        {
            bool usarProxy = false;
            var parseoExitoso = bool.TryParse(
                appConfig.AppSettings.Settings["UsarProxyPorDefecto"].Value,
                out usarProxy);
            if (parseoExitoso && usarProxy)
            {
                System.Net.WebRequest.DefaultWebProxy.Credentials
                    = System.Net.CredentialCache.DefaultNetworkCredentials;
            }
        }
        /// <summary>
        /// Método que se llama cuando la ventana principal ya se ha cargado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Paso = EstadoVerificacion.EsperandoLecturaCodigoBarras;

            Siguiente.Click += Boton_Click;
            Cerrar.Click += (o, i) =>
            {
                ViewModel.Resultado.Estado = Estado.Cancelado;
                ViewModel.Persona.TipoIdentificacion = TipoIdentificacion.Indeterminado;
                //MainWindowClosing(o, i);
                this.Close();
            };
            BotonOculto.Click += (o, i) =>
            {
                var helper = new DiagnosticHelper();
                helper.EnviarCorreo();
            };

            Titulo.Text = Titulo.Text.Replace(
                "$RUT$",
                SolicitudVerificacion.Persona.Rut);

            ViewModel = new MainWindowViewModel();
            DataContext = ViewModel;

            PersonalizarInterfaz();
            if (Convert.ToBoolean(appConfig.AppSettings.Settings["WebCamActive"].Value) == true)
            {
                logger.Info("El componente se inicializa usando webcam");
                
                InitWithWebCam();
            }
            else
            {
                logger.Info("El componente se inicializa usando COM");
                InitBCR();
            }
            if (Convert.ToBoolean(appConfig.AppSettings.Settings["ShowMail"].Value) == false)
            {
                
                this.EmailPanel.Visibility = Visibility.Hidden;
            }
            if (Convert.ToBoolean(appConfig.AppSettings.Settings["ShowPassword"].Value) == false)
            {
                this.PasswordPanel.Visibility = Visibility.Hidden;
                this.PasswordConfirmPanel.Visibility = Visibility.Hidden;
                
            }

        }

        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        /// <summary>
        /// Método que se llama cuando se está cerrando la aplicación (previo al cerrado)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MainWindowClosing(object sender, EventArgs e)
        {
            if (LectorCodigoDeBarras != null)
            {
                Thread.Sleep(100);
                LectorCodigoDeBarras.Close();
            }
            if(respuesta!=null)
            {
                if((Paso==EstadoVerificacion.VerificacionNegativa) || (Paso==EstadoVerificacion.VerificacionPositiva))
                {
                    //BuildRespuesta();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    BVIResponse result = BuildRespuesta();
                    SetPackageResponse(0, "", result);
                }
                else
                {
                    SetPackageResponse(-1, "No se finalizó el proceso de verificación de identidad", BuildRespuesta(-1, "No se finalizó el proceso de verificación de identidad"));                    
                    //logger.Info("Biometrika BviCAM.... Se cierra la ventana sin terminar el proceso de respuesta");
                    //this.Close();
                }
            }
            else
            {
                SetPackageResponse(-1, "No se finalizó el proceso de verificación de identidad", BuildRespuesta(-1, "No se finalizó el proceso de verificación de identidad"));
                logger.Info("Biometrika BviCAM.... Se cierra la ventana sin terminar el proceso de respuesta");
                //this.Close();
            }
        }

        /// <summary>
        /// Personalización de la interfaz
        /// </summary>
        void PersonalizarInterfaz()
        {
            CargarIcono();
            //SetBackgroundForControls(ConfigurationManager.AppSettings["Color"]);
            SetBackgroundForControls(appConfig.AppSettings.Settings["Color"].Value);
            CargarImagenes();
        }

        /// <summary>
        /// Retorna mensajes de respuesta del componente
        /// </summary>
        /// <param name="error"></param>
        /// <param name="msg"></param>
        /// <param name="result"></param>
        public void SetPackageResponse(int error, String msg, BVIResponse result)
        {

            bioPacketLocal.PacketParamOut = null;
            bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            logger.Info("Biometrika BVICam.. se arma el mensaje de retorno ");                        
            bioPacketLocal.PacketParamOut.Add("Data", result);
        }

        /// <summary>
        /// Asocia las imágenes bindeadas al viewmodel
        /// </summary>
        private void CargarImagenes()
        {
            ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaCompletado"];
            ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaInactivo"];
            ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessInactivo"];
            ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoInactivo"];
        }

        /// <summary>
        /// Inicialización del código de barras
        /// </summary>
        /// <returns>true si es que se inicializó, false en caso contrario</returns>
        bool InitBCR()
        {
            try
            {
                //var comPort = ConfigurationManager.AppSettings["COMPort"].ToUpper();
                var comPort = appConfig.AppSettings.Settings["COMPort"].Value.ToUpper();
                logger.Info($"Abriendo el lector de código de barras configurado en {comPort}");
                LectorCodigoDeBarras = new BCR(comPort);
                LectorCodigoDeBarras.DataCaptured += ProcesarCedulaLeida;
                var bcrOpened = LectorCodigoDeBarras.Open();
                if (!bcrOpened)
                {
                    logger.Info("Error al abrir el lector de código de barras");
                    MostrarMensajeError(null, "Error al abrir el lector de código de barras");
                }
                else
                {
                    logger.Info("Lector de Código de barras abierto correctamente");
                }
                return bcrOpened;
            }
            catch (Exception ex)
            {
                logger.Error(
                    
                    "Error al abrir el lector de código de barras. Puerto " +
                        appConfig.AppSettings.Settings["COMPort"].Value.ToUpper(), ex);
                //ConfigurationManager.AppSettings["COMPort"].ToUpper(),ex);
                return false;
            }
        }

        void InitWithWebCam()
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                ResetControls();
                logger.Info("Cédula leída corresponde a rut de la solicitud");
                DesbloquearSiguiente();
                ViewModel.Persona.Rut = barcodeWebCam.ValueId;
                ViewModel.Persona.ApellidoPaterno = barcodeWebCam.LastName;
                ViewModel.Persona.Serie =barcodeWebCam.DocumentNumber;
                if (SolicitudVerificacion.Configuracion.ExtraerDatosPersonales)
                {
                    ViewModel.Persona.FechaExpiracion = SolicitudVerificacion.Persona.FechaExpiracion;
                    ViewModel.Persona.FechaNacimiento = SolicitudVerificacion.Persona.FechaNacimiento;
                }
                else
                {
                    ViewModel.Persona.FechaExpiracion = Convert.ToDateTime("0001-01-01T00:00:00");
                    ViewModel.Persona.FechaNacimiento = Convert.ToDateTime("0001-01-01T00:00:00");
                }

                //Bloqueo de los controles
                ViewModel.Controles.RutIsEnabled = false;
                ViewModel.Controles.TipoDocumentoIsEnabled = false;

                Paso = EstadoVerificacion.EsperandoHuellaEscaneadaCedulaNueva;
                ViewModel.Persona.TipoIdentificacion = TipoIdentificacion.CedulaChilenaNueva;
                ViewModel.Persona.TipoDocumento = "Cédula Nueva";

                if (barcodeWebCam.Pdf417)
                {
                    Paso = EstadoVerificacion.EsperandoHuellaEscaneadaCedulaVieja;
                    ViewModel.Persona.TipoIdentificacion = TipoIdentificacion.CedulaChilenaAntigua;
                    ViewModel.Persona.TipoDocumento = "Cédula Antigua";

                    ViewModel.Controles.ApellidoPaternoIsEnabled = false;
                }
                else if (barcodeWebCam.Pdf417 == false)
                {
                    if (SolicitudVerificacion.Configuracion.BloqueoMenoresDeEdad == TipoBloqueo.Bloqueo)
                    {
                        if (IsMoreThan18((DateTime)LectorCodigoDeBarras.BirthDate) == true)
                        {
                            MostrarMensajeError(null, "Menor de Edad");
                        }
                    }
                }
                TextoHuella.Visibility = Visibility.Visible;
                UpdateButtonsImages();
            }));
        }

        public String FormatRutFormateado(String rut)
        {
            return rut.Substring(0, rut.Length - 1) + "-" + rut.Substring(rut.Length - 1);

        }

        /// <summary>
        /// Método que se llama al leer una cédula
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ProcesarCedulaLeida(object sender, EventArgs e)
        {
            logger.Info("Captura de lectura de cédula detectada. Antes del control de excepciones");
            try
            {
                logger.Info("Captura de lectura de cédula detectada");
                this.Dispatcher.Invoke(new Action(() =>
                {
                    ResetControls();
                    logger.Info("Comenzando procesamiento de lectura");
                    if (LectorCodigoDeBarras.RutFormateado !=  FormatRutFormateado(SolicitudVerificacion.Persona.Rut))
                    {
                        logger.Info("Cédula leída no corresponde a cédula de la solicitud");
                        MostrarMensajeError(null, "La cédula leída no corresponde al rut de la solicitud");
                        Paso = EstadoVerificacion.EsperandoLecturaCodigoBarras;
                        UpdateButtonsImages();
                    }
                    else
                    {
                        logger.Info("Cédula leída corresponde a rut de la solicitud");
                        DesbloquearSiguiente();
                        ViewModel.Persona.Rut = LectorCodigoDeBarras.RutFormateado;
                        ViewModel.Persona.ApellidoPaterno = LectorCodigoDeBarras.LastName;
                        ViewModel.Persona.Serie = LectorCodigoDeBarras.DocumentNumber;
                        if (SolicitudVerificacion.Configuracion.ExtraerDatosPersonales)
                        {
                            ViewModel.Persona.FechaExpiracion = LectorCodigoDeBarras.ExpirationDate;
                            ViewModel.Persona.FechaNacimiento = LectorCodigoDeBarras.BirthDate;
                        }
                        else
                        {
                            ViewModel.Persona.FechaExpiracion = Convert.ToDateTime("0001-01-01T00:00:00");
                            ViewModel.Persona.FechaNacimiento = Convert.ToDateTime("0001-01-01T00:00:00");
                        }

                        //Bloqueo de los controles
                        ViewModel.Controles.RutIsEnabled = false;
                        ViewModel.Controles.TipoDocumentoIsEnabled = false;

                        Paso = EstadoVerificacion.EsperandoHuellaEscaneadaCedulaNueva;
                        ViewModel.Persona.TipoIdentificacion = TipoIdentificacion.CedulaChilenaNueva;
                        ViewModel.Persona.TipoDocumento = "Cédula Nueva";

                        if (LectorCodigoDeBarras.Pdf417)
                        {
                            Paso = EstadoVerificacion.EsperandoHuellaEscaneadaCedulaVieja;
                            ViewModel.Persona.TipoIdentificacion = TipoIdentificacion.CedulaChilenaAntigua;
                            ViewModel.Persona.TipoDocumento = "Cédula Antigua";

                            ViewModel.Controles.ApellidoPaternoIsEnabled = false;
                        }
                        else if (LectorCodigoDeBarras.Pdf417 == false)
                        {
                            if (SolicitudVerificacion.Configuracion.BloqueoMenoresDeEdad == TipoBloqueo.Bloqueo)
                            {
                                if (IsMoreThan18((DateTime)LectorCodigoDeBarras.BirthDate) == true)
                                {
                                    MostrarMensajeError(null, "Menor de Edad");
                                }
                            }
                        }
                        TextoHuella.Visibility = Visibility.Visible;
                        UpdateButtonsImages();
                    }
                }));
            }
            catch (Exception ex)
            {
                logger.Error("Error al capturar los datos",ex);
            }
        }

        /// <summary>
        /// Llamada de métodos cuando se presiona el botón siguiente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Boton_Click(object sender, RoutedEventArgs e)
        {
            switch (Paso)
            {
                case EstadoVerificacion.EsperandoLecturaCodigoBarras:
                    ResetControls();
                    break;
                case EstadoVerificacion.EsperandoHuellaEscaneadaCedulaNueva:
                case EstadoVerificacion.EsperandoHuellaEscaneadaCedulaVieja:
                    CapturarHuella();
                    break;
                case EstadoVerificacion.EsperandoLecturaContactless:
                    //SetupClr();
                    VerificarContraCedula();
                    break;
                case EstadoVerificacion.VerificacionNegativa:
                case EstadoVerificacion.VerificacionPositiva:

                    //BuildRespuesta();
                    string result=JsonConvert.SerializeObject(BuildRespuesta());                    
                    JavaScriptSerializer js = new JavaScriptSerializer();                    
                    String json;                    
                    SetPackageResponse(0, "", BuildRespuesta());
                    this.Close();
                    break;
            }
        }

        private BVIResponse BuildRespuesta(int err, String msg)
        {
            respuesta = new BVIResponse();
            respuesta.ErrorNumber = err;
            respuesta.Mensaje = msg;
            return respuesta;
        }

        private BVIResponse BuildRespuesta()
        {

            respuesta = new BVIResponse();
            respuesta.Persona = new Persona();
            respuesta.Persona.Nombre = ViewModel.Persona.Nombre;
            respuesta.Persona.Rut = ViewModel.Persona.Rut;
            respuesta.Persona.ApellidoMaterno = ViewModel.Persona.ApellidoMaterno;
            respuesta.Persona.ApellidoPaterno = ViewModel.Persona.ApellidoPaterno;
            respuesta.Persona.FechaExpiracion = ViewModel.Persona.FechaExpiracion;
            respuesta.Persona.FechaNacimiento = ViewModel.Persona.FechaNacimiento;
            respuesta.Persona.Sexo = ViewModel.Persona.Sexo;
            respuesta.Persona.TipoIdentificacion = ViewModel.Persona.TipoIdentificacion;
            respuesta.Persona.Foto = ViewModel.Persona.Foto;
            respuesta.Persona.FotoFirma = ViewModel.Persona.FotoFirma;
            respuesta.Persona.Serie = ViewModel.Persona.Serie;
            respuesta.Persona.Wsq = ViewModel.Persona.Wsq;
            respuesta.Persona.Raw = ViewModel.Persona.Raw;
            respuesta.Persona.Iso = ViewModel.Persona.Iso;
            respuesta.Persona.Jpeg = ViewModel.Persona.Jpg;
            respuesta.ResultadoVerificacion = new ResultadoVerificacion();
            respuesta.ResultadoVerificacion.CedulaBloqueada = ViewModel.Resultado.CedulaBloqueada;
            respuesta.ResultadoVerificacion.CedulaVencida = ViewModel.Resultado.CedulaVencida;
            respuesta.ResultadoVerificacion.DatosPersonalesExtraidos = ViewModel.Resultado.DatosPersonalesExtraidos; 
            if(ViewModel.Persona.TipoIdentificacion==TipoIdentificacion.CedulaChilenaAntigua)
            {
                logger.Info("Enviamos los datos ingresados en las cajas del formulario");
                respuesta.Persona.Nombre = this.Nombres.Text;               
                respuesta.Persona.ApellidoMaterno = this.ApellidoMaterno.Text;
                respuesta.Persona.ApellidoPaterno = this.ApellidoPaterno.Text;
            }

            if(ViewModel.Resultado.Estado==Estado.Cancelado)
            {
                if (Paso == EstadoVerificacion.VerificacionNegativa)
                    respuesta.ResultadoVerificacion.Estado = Estado.VerificacionNegativa;
                else if (Paso == EstadoVerificacion.VerificacionPositiva)
                    respuesta.ResultadoVerificacion.Estado = Estado.VerificacionPositiva;
                
            }
            else
                respuesta.ResultadoVerificacion.Estado = ViewModel.Resultado.Estado;
            respuesta.ResultadoVerificacion.FotoExtraida = ViewModel.Resultado.FotoExtraida;
            respuesta.ResultadoVerificacion.FotoFirmaExtraida = ViewModel.Resultado.FotoFirmaExtraida;
            respuesta.ResultadoVerificacion.MenorDeEdad = ViewModel.Resultado.MenorDeEdad;
            respuesta.ErrorNumber = 0;
            respuesta.Mensaje = "No hay error";
            return respuesta;
        }

        /// <summary>
        /// Método que llama la captura de la huella
        /// </summary>
        void CapturarHuella()
        {
            logger.Info("Capturando Huella Dactilar");
            LectorHuellaDigital = new FPR();

            int timeoutLector = AppConfigHelper.GetTimeoutLector();
            //Solo para sacar el serial del secugen
            LectorHuellaDigital.SimpleOpen();

            int respuesta = LectorHuellaDigital.LongCaptureWithTimeOut(Timeout, 50);
            logger.Info("respuesta huella dactilar:" + respuesta);
            
            if (respuesta == FPR.NoError)
            {
                logger.Info("Huella Dactilar Capturada");
                ProcesarHuellaCapturada();
            }
            else if (respuesta == FPR.CaptureTimeout)
            {
                MostrarMensajeError(CapturarHuella, "Ha tomado mucho tiempo en capturar la huella");
            }
        }

        /// <summary>
        /// Procesamiento de los datos obtenidos por la huealla
        /// </summary>
        void ProcesarHuellaCapturada()
        {
            logger.Info("Procesando Huella Capturada");
            if (String.IsNullOrEmpty(LectorHuellaDigital.ISO))
            {
                logger.Error("Error al capturar la huella");
                CapturarHuella();
            }

            var fingerprint = WpfImageHelper.Base64ToBitmap(LectorHuellaDigital.BMP);
            ViewModel.Controles.ImagenCapturaHuella = WpfImageHelper.BitmapToBitmapImage(fingerprint);
            if (SolicitudVerificacion.Configuracion.ansi)
            {
                ViewModel.Persona.Ansi = LectorHuellaDigital.ANSI;
            }
            else
            {
                ViewModel.Persona.Ansi = "";
            }
            if (SolicitudVerificacion.Configuracion.raw)
            {
                ViewModel.Persona.Raw = LectorHuellaDigital.RAW;
            }
            else
            {
                ViewModel.Persona.Raw = "";
            }
            if (SolicitudVerificacion.Configuracion.iso)
            {
                ViewModel.Persona.Iso = LectorHuellaDigital.ISO;
            }
            else
            {
                ViewModel.Persona.Iso = "";
            }
            if (SolicitudVerificacion.Configuracion.wsq)
            {
                ViewModel.Persona.Wsq = LectorHuellaDigital.WSQ;
            }
            else
            {
                ViewModel.Persona.Wsq = "";
            }
            if (SolicitudVerificacion.Configuracion.jpeg)
            {
                ViewModel.Persona.Jpg = LectorHuellaDigital.BMP.ToString();
            }
            else
            {
                ViewModel.Persona.Jpg = "";
            }

            TextoHuella.Visibility = Visibility.Collapsed;

            if(Convert.ToBoolean(appConfig.AppSettings.Settings["WebCamActive"].Value) == true)
            { 
                if (barcodeWebCam.Pdf417)
                {
                    logger.Info("Realizando Verificación de cédula antigua por webcam");
                    VerificarCedulaAntiguaAsync();
                }
                else
                {
                    logger.Info("Realizando verificación de cédula nueva");
                    ConvertirIsoToIsoCCAsync(LectorHuellaDigital.ISO);
                }
            }
            else
            {
                if (LectorCodigoDeBarras.Pdf417)
                {
                    logger.Info("Realizando Verificación de cédula antigua por lector");
                    VerificarCedulaAntiguaAsync();
                }
                else
                {
                    logger.Info("Realizando verificación de cédula nueva");
                    ConvertirIsoToIsoCCAsync(LectorHuellaDigital.ISO);
                }
            }
        }

        /// <summary>
        /// Creación del worker que se encarga de la verificación con cédula antigua
        /// </summary>
        void VerificarCedulaAntiguaAsync()
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Visible;

            BackgroundWorker verificacionWorker = new BackgroundWorker();
            verificacionWorker.DoWork += VerificacionWorker_DoWork;
            verificacionWorker.RunWorkerCompleted += VerificacionWorker_RunWorkerCompleted;
            verificacionWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Método del worker de verificación de cédula antigua
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void VerificacionWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                bcr.bviproxy.BVIProxy bviproxy = new bcr.bviproxy.BVIProxy();
                bviproxy.Url = appConfig.AppSettings.Settings["WsProxy"].Value;
                logger.Info("BviProxy:" + bviproxy.Url);
                Model.Service.ApiResponse<BP_VerifyResponse> respuesta;
                logger.Info("BviCamUIAdapter.Properties.Settings.Default.UseProxy:" + appConfig.AppSettings.Settings["UseProxy"].Value);
                if (Convert.ToBoolean(appConfig.AppSettings.Settings["UseProxy"].Value) == true)
                {
                    logger.Info("Se verifica con NEC usando Proxy API");
                    NecDataToCompare necData;
                    ProxyCaller proxy = new ProxyCaller();
                    if (Convert.ToBoolean(appConfig.AppSettings.Settings["WebCamActive"].Value) == true)
                    {
                        necData = new NecDataToCompare()
                        {

                            necTemplate = Convert.ToBase64String(barcodeWebCam.NecTemplate),
                            rawData = Convert.ToBase64String(LectorHuellaDigital.NEC),
                            rut = barcodeWebCam.RutFormateado

                        };
                    }
                    else
                    {
                        necData = new NecDataToCompare()
                        {

                            necTemplate = Convert.ToBase64String(LectorCodigoDeBarras.NEC),
                            rawData = Convert.ToBase64String(LectorHuellaDigital.NEC),
                            rut = LectorCodigoDeBarras.RutFormateado

                        };
                    }
                    var respuestapro = proxy.VerifyNec(
                        necData,
                        LectorHuellaDigital.SERIALNUMBER);
                    e.Result = respuestapro;
                }
                else
                {
                    int result;
                    double score;
                    String msgErr = "";
                    logger.Info("Voy a actualizar sin pasar por proxy");
                    if (Convert.ToBoolean(appConfig.AppSettings.Settings["WebCamActive"].Value) == true)
                    {
                        logger.Info("Ingresa con webcam habilitado");

                        bviproxy.Verify(Convert.ToInt32(appConfig.AppSettings.Settings["CompanyId"].Value ),
                                       "RUT",
                                       barcodeWebCam.RutFormateado,
                                       LectorHuellaDigital.SERIALNUMBER,
                                       Convert.ToBase64String(barcodeWebCam._binaryData),
                                       LectorHuellaDigital.RAW,
                                       Convert.ToInt32(appConfig.AppSettings.Settings["Minutiaetype"].Value),
                                       Convert.ToInt32(appConfig.AppSettings.Settings["score"].Value),
                                       out result,
                                       out score,
                                       out msgErr);
                        respuesta = new Model.Service.ApiResponse<BP_VerifyResponse>();
                        respuesta.Status = ApiResponseEnum.Success;
                        respuesta.Message = msgErr;
                        respuesta.Data = new BP_VerifyResponse();
                        respuesta.Data.Code = 0;
                        respuesta.Data.Data = score.ToString();
                        logger.Info("Serial del dispositivo:" + LectorHuellaDigital.SERIALNUMBER);
                        logger.Info("Resultado de verificación ws:" + msgErr);
                        if (score >= Convert.ToInt32(appConfig.AppSettings.Settings["score"].Value))
                        {
                            respuesta.Data.VerificationResult = true;
                        }
                        else
                        {
                            respuesta.Data.VerificationResult = false;
                        }
                    }
                    else
                    {
                        logger.Info("Viene con verificación desde lector de código de barra");
                        bviproxy.Verify(Convert.ToInt32(appConfig.AppSettings.Settings["CompanyId"].Value),
                                       "RUT",
                                       LectorCodigoDeBarras.RutFormateado,
                                       LectorHuellaDigital.SERIALNUMBER,
                                       LectorCodigoDeBarras.FullPdf417,
                                       LectorHuellaDigital.RAW,
                                       Convert.ToInt32(appConfig.AppSettings.Settings["Minutiaetype"].Value),
                                       Convert.ToInt32(appConfig.AppSettings.Settings["score"].Value),
                                       out result,
                                       out score,
                                       out msgErr);
                        respuesta = new Model.Service.ApiResponse<BP_VerifyResponse>();
                        respuesta.Status = ApiResponseEnum.Success;
                        respuesta.Message = msgErr;
                        respuesta.Data = new BP_VerifyResponse();
                        respuesta.Data.Code = 0;
                        respuesta.Data.Data = score.ToString();
                        if (score >= Convert.ToInt32(appConfig.AppSettings.Settings["score"].Value))
                        {
                            respuesta.Data.VerificationResult = true;
                        }
                        else
                        {
                            respuesta.Data.VerificationResult = false;
                        }
                        logger.Info("Serial del dispositivo:" + LectorHuellaDigital.SERIALNUMBER);
                        logger.Info("Resultado de verificación ws:" + msgErr);

                    }
                    e.Result = respuesta;
                }
            }
            catch (TimeoutException ex)
            {
                logger.Error("El servicio de verificación ha tomado mucho tiempo en responder",ex);
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.Timeout,
                    Message = "El servicio de verificación ha tomado mucho tiempo en responder"
                };
                e.Result = ApiResponseEnum.Timeout;
            }
            catch (EndpointNotFoundException ex)
            {
                logger.Error("El servicio de verificación no se ha encontrado",ex);
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.EndPointNotFound,
                    Message = "El servicio de verificación de identidad no se ha encontrado. Verifique su conexión"
                };
            }
            catch (UriFormatException ex)
            {
                logger.Error("La URL ingresada no tiene un formato válido",ex);
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.UriFormatError,
                    Message = "La URL ingresada no tiene un formato válido. Comuníquese con soporte@biometrika.cl"
                };
            }
            catch (Exception ex)
            {
                logger.Error("Error indeterminado",ex);
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.Error,
                    Message = "Ha ocurrido un error con el servicio de verificación. Comuníquese con soporte@biometrik.cl"
                };
            }
        }

        private bool IsMoreThan18(DateTime from)
        {
            if (from.AddYears(18) < DateTime.Now)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Método que se ejecuta al terminar la verificación de identidad remota
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void VerificacionWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Collapsed;
            //Verificamos si la cédula se encuentra vencida.
            if (Convert.ToBoolean(appConfig.AppSettings.Settings["WebCamActive"].Value) == true)
            {
                logger.Info("la fecha de expiración de la cédula es:" + barcodeWebCam.ExpirationDate.ToString());
                if (barcodeWebCam.ExpirationDate < DateTime.Now)
                {
                    ViewModel.Resultado.CedulaVencida = true;
                }
                if (ViewModel.Persona.TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
                {
                    if (IsMoreThan18((DateTime)barcodeWebCam.BirthDate) == true)
                    {
                        ViewModel.Resultado.MenorDeEdad = true;
                    }
                    else
                    {
                        ViewModel.Resultado.MenorDeEdad = false;
                    }

                }
            }
            else
            {
                logger.Info("la fecha de expiración de la cédula es:" + LectorCodigoDeBarras.ExpirationDate.ToString());
                if (LectorCodigoDeBarras.ExpirationDate < DateTime.Now)
                {
                    ViewModel.Resultado.CedulaVencida = true;
                }
                if (ViewModel.Persona.TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
                {
                    if (IsMoreThan18((DateTime)LectorCodigoDeBarras.BirthDate) == true)
                    {
                        ViewModel.Resultado.MenorDeEdad = true;
                    }
                    else
                    {
                        ViewModel.Resultado.MenorDeEdad = false;
                    }

                }
            }
            if (SolicitudVerificacion.Configuracion.bloqueoCedula)
            {
                if (RegistroCivil())
                {
                    ViewModel.Resultado.CedulaBloqueada = false;
                }
                else
                {
                    ViewModel.Resultado.CedulaBloqueada = true;
                }
            }
            else
            {
                ViewModel.Resultado.CedulaBloqueada = false;
            }

            var resultado = (Model.Service.ApiResponse<BP_VerifyResponse>)e.Result;
            switch (resultado.Status)
            {
                case ApiResponseEnum.Success:
                    if (resultado.Data.VerificationResult)
                    {
                        Paso = EstadoVerificacion.VerificacionPositiva;
                        ViewModel.Resultado.Estado = Estado.VerificacionPositiva;
                    }
                    else
                    {
                        Paso = EstadoVerificacion.VerificacionNegativa;
                        ViewModel.Resultado.Estado = Estado.VerificacionNegativa;
                    }
                    UpdateButtonsImages();
                    break;
                case ApiResponseEnum.Timeout:
                    MostrarMensajeError(
                        VerificarCedulaAntiguaAsync,
                        resultado.Message);
                    break;
                case ApiResponseEnum.EndPointNotFound:
                case ApiResponseEnum.UriFormatError:
                case ApiResponseEnum.Error:
                    MostrarMensajeError(
                        null,
                        resultado.Message);
                    break;
            }
        }

        public Boolean RegistroCivil()
        {
            bool resp = false;
            //RC_Wrapper rcWrapper = new RC_Wrapper(ConfigurationManager.AppSettings["UrlRC"]);
            RC_Wrapper rcWrapper = new RC_Wrapper(appConfig.AppSettings.Settings["UrlRC"].Value);
            ResponseDTO response = null;
            try
            {
                response = rcWrapper.Verify(DocType.CEDULA, LectorCodigoDeBarras.RutFormateado, LectorCodigoDeBarras.DocumentNumber);
                if (response.ResponseMessage.Equals("Vigente"))
                {
                    resp = true;
                }
                else
                {
                    resp = false;
                }
            }
            catch (Exception ex)
            {

            }
            return resp;
        }


        /// <summary>
        /// Creación del Worker de conversión de Iso a Iso CC
        /// </summary>
        /// <param name="iso"></param>
        void ConvertirIsoToIsoCCAsync(string iso)
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Visible;

            BackgroundWorker conversionWorker = new BackgroundWorker();
            conversionWorker.DoWork += ConversionWorker_DoWork;
            conversionWorker.RunWorkerCompleted += ConversionWorker_Completed;
            conversionWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Trabajo que se ejecuta en el worker de conversion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ConversionWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                logger.Info("Se ingresa a Conversion DoWork");
                Model.Service.ApiResponse<BP_VerifyResponse> respuesta = null;
                if (Convert.ToBoolean(appConfig.AppSettings.Settings["UseProxy"].Value) == true)
                {
                    
                    ProxyCaller proxy = new ProxyCaller();
                    if (barcodeWebCam != null)
                    {
                        logger.Info("Verificación a través del Proxy, con uso de webcam activo");
                        respuesta = proxy.IsoToIsoCC(
                        barcodeWebCam.RutFormateado,
                        LectorHuellaDigital.ISO,
                        LectorHuellaDigital.SERIALNUMBER);
                    }
                    else
                    {
                        logger.Info("Verificación a través del Proxy, con uso de 3 en 1");
                        respuesta = proxy.IsoToIsoCC(
                        LectorCodigoDeBarras.Rut,
                        LectorHuellaDigital.ISO,
                        LectorHuellaDigital.SERIALNUMBER);
                    }
                    if (respuesta.Status == ApiResponseEnum.Success)
                    {
                        logger.Info("Se obtiene respuesta de la Api:" + ApiResponseEnum.Success);
                        LectorHuellaDigital.ISOCC = respuesta.Data.Data.Split('-');
                        e.Result = respuesta;
                    }
                    else
                    {
                        logger.Info("No se obtuvo respuesta success del endpoint");
                        logger.Info("respuesta:" + respuesta);
                        e.Result = respuesta;
                    }
                }
                else
                {
                    bcr.bviproxy.BVIProxy bviproxy = new bcr.bviproxy.BVIProxy();
                    bviproxy.Url = appConfig.AppSettings.Settings["WsProxy"].Value;
                    logger.Info("Url ws:" + bviproxy.Url);
                    respuesta = new Model.Service.ApiResponse<BP_VerifyResponse>();
                    String isocompact = null;
                    String msgErr = "";
                    if (barcodeWebCam != null)
                    {
                        logger.Info("Se realiza la verificación de identidad a través del web sevices, lectura desde la webcam");
                        logger.Info("Web:" + bviproxy.Url);
                        logger.Info("ISO:" + LectorHuellaDigital.ISO);
                        logger.Info("Serial Id:" + LectorHuellaDigital.SERIALNUMBER);
                        logger.Info("CompanyId:" + Convert.ToInt32(appConfig.AppSettings.Settings["CompanyId"].Value));
                        bviproxy.ConvertISOtoISOCP(Convert.ToInt32(appConfig.AppSettings.Settings["CompanyId"].Value),
                                                   "RUT",
                                                   barcodeWebCam.RutFormateado,
                                                   LectorHuellaDigital.SERIALNUMBER,
                                                   LectorHuellaDigital.ISO,
                                                   out isocompact,
                                                   out msgErr);
                        logger.Info("Se obtiene respuesta del webservices:" + msgErr);
                        respuesta = new Model.Service.ApiResponse<BP_VerifyResponse>();
                        logger.Info("Isocompact:" + isocompact);
                        respuesta.Status = ApiResponseEnum.Success;
                        respuesta.Message = msgErr;
                        respuesta.Data = new BP_VerifyResponse();
                        respuesta.Data.Code = 0;
                        respuesta.Data.Data = isocompact;

                    }
                    else
                    {
                        logger.Info("Se realiza la verificación de identidad a través del web sevices");
                        logger.Info("Web:" + bviproxy.Url);
                        logger.Info("ISO:" + LectorHuellaDigital.ISO);
                        bviproxy.ConvertISOtoISOCP(Convert.ToInt32(appConfig.AppSettings.Settings["CompanyId"].Value),
                                                   "RUT",
                                                   LectorCodigoDeBarras.RutFormateado,
                                                   LectorHuellaDigital.SERIALNUMBER,
                                                   LectorHuellaDigital.ISO,
                                                   out isocompact,
                                                   out msgErr);

                        logger.Info("Se obtiene respuesta del webservices:" + msgErr);
                        respuesta = new Model.Service.ApiResponse<BP_VerifyResponse>();
                        logger.Info("Isocompact:" + isocompact);
                        respuesta.Status = ApiResponseEnum.Success;
                        respuesta.Message = msgErr;
                        respuesta.Data = new BP_VerifyResponse();
                        respuesta.Data.Code = 0;
                        respuesta.Data.Data = isocompact;


                    }
                    if (respuesta.Status == ApiResponseEnum.Success)
                    {
                        logger.Info("Se obtiene respuesta de la Api:" + ApiResponseEnum.Success);
                        LectorHuellaDigital.ISOCC = respuesta.Data.Data.Split('-');
                        e.Result = respuesta;
                    }
                    else
                    {
                        logger.Info("No se obtuvo respuesta success del endpoint");
                        logger.Info("respuesta:" + respuesta);
                        e.Result = respuesta;
                    }
                }
            }
            catch (TimeoutException ex)
            {
                logger.Error("El servicio de verificación ha tomado mucho tiempo en responder", ex);
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.Timeout,
                    Message = "El servicio de verificación ha tomado mucho tiempo en responder"
                };
                e.Result = ApiResponseEnum.Timeout;
            }
            catch (EndpointNotFoundException ex)
            {
                logger.Error("El servicio de verificación de identidad no se ha encontrado. Verifique su conexión", ex);
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.EndPointNotFound,
                    Message = "El servicio de verificación de identidad no se ha encontrado. Verifique su conexión"
                };
            }
            catch (Exception ex)
            {
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.Error,
                    Message = "Se ha producido un error con el servicio de verificación"
                };
                logger.Error("Error no controlado en el timeout", ex);
            }
        }

        /// <summary>
        /// Método a ejecutar cuando se termina el proceso del background worker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ConversionWorker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Collapsed;

            var resultado = (Model.Service.ApiResponse<BP_VerifyResponse>)e.Result;
            switch (resultado.Status)
            {
                case ApiResponseEnum.Success:
                    Paso = EstadoVerificacion.EsperandoLecturaContactless;
                    UpdateButtonsImages();
                    break;
                case ApiResponseEnum.Timeout:
                    MostrarMensajeErrorConParametro<String>(
                        ConvertirIsoToIsoCCAsync,
                        LectorHuellaDigital.ISO,
                        resultado.Message);
                    break;
                case ApiResponseEnum.EndPointNotFound:
                case ApiResponseEnum.UriFormatError:
                case ApiResponseEnum.Error:
                    MostrarMensajeError(
                        null,
                        resultado.Message);
                    break;
            }
        }

        /// <summary>
        /// Worker que se encarga de realizar el match on card
        /// </summary>
        void VerificarContraCedula()
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Visible;
            BackgroundWorker matchOnCardWorker = new BackgroundWorker();
            matchOnCardWorker.DoWork += VerificarConCedula_DoWork;
            matchOnCardWorker.RunWorkerCompleted += VerificacionConCedula_Completed;
            matchOnCardWorker.RunWorkerAsync();
        }

        void VerificarConCedula_DoWork(object sender, DoWorkEventArgs args)
        {
            SetupClr();
        }

        void VerificacionConCedula_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Hidden;

            if(Convert.ToBoolean(appConfig.AppSettings.Settings["WebCamActive"].Value) == true)
            { 
                if (barcodeWebCam.ExpirationDate < DateTime.Now)
                {
                    ViewModel.Resultado.CedulaVencida = true;
                }
            }
            else
            {
                if (LectorCodigoDeBarras.ExpirationDate < DateTime.Now)
                {
                    ViewModel.Resultado.CedulaVencida = true;
                }
            }
            
            if (ViewModel.Persona.TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
            {
                if(Convert.ToBoolean(appConfig.AppSettings.Settings["WebCamActive"].Value) == true)
                { 
                    if (IsMoreThan18((DateTime)barcodeWebCam.BirthDate) == false)
                    {
                        ViewModel.Resultado.MenorDeEdad = false;
                    }
                    else
                    {
                        ViewModel.Resultado.MenorDeEdad = true;
                    }
                }
                else
                {
                    if (IsMoreThan18((DateTime)LectorCodigoDeBarras.BirthDate) == false)
                    {
                        ViewModel.Resultado.MenorDeEdad = false;
                    }
                    else
                    {
                        ViewModel.Resultado.MenorDeEdad = true;
                    }
                }
            }


            UpdateButtonsImages();
        }

        /// <summary>
        /// Se llama al método que consume la API de comparación de datos
        /// </summary>
        /// <param name="necData"></param>
        void VerificarCedulaAntiguaConProxy(NecDataToCompare necData)
        {
            ProxyCaller proxy = new ProxyCaller();
            Model.Service.ApiResponse<BP_VerifyResponse> respuesta = proxy.VerifyNec(
                necData,
                LectorHuellaDigital.SERIALNUMBER);
            if (respuesta.Status == ApiResponseEnum.Success)
            {
                if (respuesta.Data.VerificationResult)
                {
                    Paso = EstadoVerificacion.VerificacionPositiva;
                    ViewModel.Resultado.Estado = Estado.VerificacionPositiva;
                }
                else
                {
                    Paso = EstadoVerificacion.VerificacionNegativa;
                    ViewModel.Resultado.Estado = Estado.VerificacionNegativa;
                }
            }
            else
            {
                logger.Error($"Error en la verificación en BCRProxy {respuesta.Message}");
            }
        }

        /// <summary>
        /// Se limpia el viewmodel y se resetea el estado del paso
        /// </summary>
        void ResetControls()
        {
            logger.Info("Reestableciendo controles a estado por defecto");
            ViewModel.Persona.Nombre = String.Empty;
            ViewModel.Persona.ApellidoPaterno = String.Empty;
            ViewModel.Persona.ApellidoMaterno = String.Empty;
            ViewModel.Controles.ImagenCapturaHuella = null;

            //Bloqueo de los controles
            ViewModel.Controles.RutIsEnabled = true;
            ViewModel.Controles.NombresIsEnabled = true;
            ViewModel.Controles.ApellidoPaternoIsEnabled = true;
            ViewModel.Controles.ApellidoMaternoIsEnabled = true;
            ViewModel.Controles.TipoDocumentoIsEnabled = true;
            ViewModel.Controles.TextoSiguiente = "Siguiente";
            //ViewModel.Resultado = new Resultado();

            Paso = EstadoVerificacion.EsperandoLecturaCodigoBarras;
        }

        /// <summary>
        /// Abre el lector contactless, extrae los datos del MRZ y realiza la verificación de
        /// identidad en el caso de cédula nueva
        /// </summary>
        void SetupClr()
        {
            Int32 finger;
            try
            {
                var clr = new Bio.Core.CLR.CLR();

                //clr.ReaderName = ConfigurationManager.AppSettings["CLRReaderName"];
                logger.Info("Se inicializa con el lector:" + appConfig.AppSettings.Settings["CLRReaderName"].Value);
                clr.ReaderName = appConfig.AppSettings.Settings["CLRReaderName"].Value;
                clr.CLR_OPEN(10);
                Thread.Sleep(100);
                logger.Debug("Entrando a CLR_CONNECT");

                clr.CLR_CONNECT(Convert.ToUInt32(5000));

                if (clr.GetLasError() == 0)
                {
                    ExtractMrzData(clr);


                    Thread.Sleep(100);

                    logger.Debug("Extracción de datos terminada");
                    logger.Debug("Comenzando Verificación contra cédula nueva");
                    bool match = clr.Match(LectorHuellaDigital.ISOCC, out finger);
                    if (match)
                    {
                        ViewModel.Resultado.Estado = Estado.VerificacionPositiva;
                    }
                    else
                    {
                        ViewModel.Resultado.Estado = Estado.VerificacionNegativa;
                    }
                    logger.Info($"SetupClr: Resultado Match: {ViewModel.Resultado.Estado}");

                    Paso = (match) ? EstadoVerificacion.VerificacionPositiva : EstadoVerificacion.VerificacionNegativa;

                    //UpdateButtonsImages();
                }
                else if (clr.GetLasError() == -1)
                {
                    MostrarMensajeError(VerificarContraCedula, "Error al detectar la cédula. Acérquela al dispositivo");
                }
                clr.CLR_DISCONNECT();
                clr.CLR_CLOSE();
            }
            catch (EndOfStreamException ex)
            {
                MostrarMensajeError(VerificarContraCedula, "Error al detectar la cédula. Acérquela al dispositivo");
                logger.Error("La cédula leída no corresponde a la huella capturada",ex);
            }
            catch (Exception ex)
            {
                //MostrarMensajeError(SetupClr, "Error al detectar la cédula. Acérquela al dispositivo");
                MostrarMensajeError(VerificarContraCedula, "Error al detectar la cédula. Acérquela al dispositivo");
                ViewModel.Controles.BarraProgresoVisibility = Visibility.Hidden;
                logger.Error( "Error con el lector Contactless",ex);
            }
        }

        /// <summary>
        /// Se extraen los datos de la cédula y se actualiza el viewmodel
        /// </summary>
        /// <param name="clr">Lector Contactless</param>
        void ExtractMrzData(CLR clr)
        {
         
            if(Convert.ToBoolean(appConfig.AppSettings.Settings["WebCamActive"].Value) == true)
            {
                logger.Info("SetupClr: Entrando a CLR_SECURITY con número de documento: " +
                            barcodeWebCam.DocumentNumber);

                clr.CLR_SECURITY(
                    barcodeWebCam.DocumentNumber,
                    barcodeWebCam.DOB,
                    barcodeWebCam.DOE);
            }
            else
            {
                logger.Info("SetupClr: Entrando a CLR_SECURITY con número de documento: " +
                 LectorCodigoDeBarras.DocumentNumber);

                clr.CLR_SECURITY(
                    LectorCodigoDeBarras.DocumentNumber,
                    LectorCodigoDeBarras.DOB,
                    LectorCodigoDeBarras.DOE);
            }

            logger.Info("SetupClr: Entrando a CLR_GETMRZ");

            clr.CLR_GETMRZ();

            if (SolicitudVerificacion.Configuracion.ExtraerDatosPersonales)
            {
                ViewModel.Persona.ApellidoPaterno = clr.PatherLastName;
                ViewModel.Persona.Nombre = clr.Name;
                ViewModel.Persona.ApellidoMaterno = clr.MotherLastName;
                ViewModel.Persona.Serie = clr.DocumentNumber;
                ViewModel.Persona.Sexo = clr.Sex;
                //Bloque de los controles para que no puedan ser modificados
                ViewModel.Controles.NombresIsEnabled = false;
                ViewModel.Controles.ApellidoPaternoIsEnabled = false;
                ViewModel.Controles.ApellidoMaternoIsEnabled = false;
                
                ViewModel.Persona.FechaNacimiento = DateTime.ParseExact(clr.DateOfBirth, "yyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                ViewModel.Persona.FechaExpiracion = DateTime.ParseExact(clr.DateOfExpiry, "yyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                ViewModel.Resultado.DatosPersonalesExtraidos = true;



            }
            else
            {
                ViewModel.Persona.ApellidoPaterno = "";
                ViewModel.Persona.Nombre = "";
                ViewModel.Persona.ApellidoMaterno = "";
                ViewModel.Persona.Serie = "";
                ViewModel.Persona.Sexo = "";
                //Bloque de los controles para que no puedan ser modificados
                ViewModel.Controles.NombresIsEnabled = false;
                ViewModel.Controles.ApellidoPaternoIsEnabled = false;
                ViewModel.Controles.ApellidoMaternoIsEnabled = false;

                ViewModel.Resultado.DatosPersonalesExtraidos = false;
            }

            //EXTRACCIÓN FOTOS
            if (SolicitudVerificacion.Configuracion.extraerFoto)
            {
                byte[] fotoCed = clr.CLR_GETFOTO();
                if (fotoCed != null)
                {
                    try
                    {
                        logger.Info("Test");
                        //var foto = Convert.ToBase64String(fotoCed);
                        BitmapImageCreator.Register();
                        var por = J2kImage.FromBytes(fotoCed);
                        //Bitmap img = por.As<Bitmap>();//ACA SE CAE
                        int sdf = 0;
                        Bitmap img = por.As<Bitmap>();

                        ImageFormat format = ImageFormat.Jpeg;

                        using (MemoryStream ms = new MemoryStream())
                        {
                            img.Save(ms, format);
                            ViewModel.Persona.Foto = Convert.ToBase64String(ms.ToArray());
                        }

                        ViewModel.Resultado.FotoExtraida = true;
                        this.Dispatcher.Invoke(new Action(() =>
                        {
                            Foto.Source = BitmapToImageSource(img);
                        }));
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    ViewModel.Persona.Foto = "Null";
                    logger.Error("No se ha podido extraer la foto");
                }
            }
            else
            {
                ViewModel.Persona.Foto = "";
            }

            if (SolicitudVerificacion.Configuracion.ExtraerFirma)
            {
                byte[] fotoFir = clr.GET_CLRFIRMA();
                if (fotoFir != null)
                {
                    BitmapImageCreator.Register();
                    //var fotoF = Convert.ToBase64String(fotoFir);
                    var porF = J2kImage.FromBytes(fotoFir);
                    Bitmap imgF = porF.As<Bitmap>();

                    ImageFormat format = ImageFormat.Jpeg;

                    using (MemoryStream ms = new MemoryStream())
                    {
                        imgF.Save(ms, format);
                        ViewModel.Persona.FotoFirma = Convert.ToBase64String(ms.ToArray());
                    }

                    ViewModel.Resultado.FotoFirmaExtraida = true;
                }
                else
                {
                    ViewModel.Persona.FotoFirma = "Null";
                    logger.Error("No se ha podido extraer la foto firma");
                }
            }
            else
            {
                ViewModel.Persona.FotoFirma = "";
            }

        }

        /// <summary>
        /// Actualiza los botones de acuerdo al paso en el que se encuentra actualmente la aplicación
        /// </summary>
        void UpdateButtonsImages()
        {
            logger.Info("Actualizando controles");
            switch (Paso)
            {
                case EstadoVerificacion.EsperandoLecturaCodigoBarras:
                    ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaCompletado"];
                    ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaInactivo"];
                    ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessInactivo"];
                    //ImagenCapturaHuella.Source = new BitmapImage();
                    ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoInactivo"];
                    break;
                case EstadoVerificacion.EsperandoHuellaEscaneadaCedulaNueva:
                    ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaVerde"];
                    ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaCompletado"];
                    ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessInactivo"];
                    ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoInactivo"];
                    break;
                case EstadoVerificacion.EsperandoHuellaEscaneadaCedulaVieja:
                    ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaVerde"];
                    ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaCompletado"];
                    ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessInactivo"];
                    ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoInactivo"];
                    break;
                case EstadoVerificacion.EsperandoLecturaContactless:
                    ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaVerde"];
                    ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaVerde"];
                    ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessCompletado"];
                    ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoInactivo"];
                    break;
                case EstadoVerificacion.VerificacionPositiva:
                    ViewModel.Controles.TextoResultado = "Verificación Positiva";
                    ViewModel.Controles.TextoSiguiente = "Finalizar";
                    ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaVerde"];
                    ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaVerde"];
                    ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessVerde"];
                    ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoPositivo"];
                    break;
                case EstadoVerificacion.VerificacionNegativa:
                    ViewModel.Controles.TextoResultado = "Verificación Negativa";
                    ViewModel.Controles.TextoSiguiente = "Finalizar";
                    ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaVerde"];
                    ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaVerde"];
                    ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessVerde"];
                    ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoNegativo"];
                    break;
            }
        }

        /// <summary>
        /// Se cambia el ícono de la aplicación
        /// </summary>
        void CargarIcono()
        {
            //if (File.Exists(ConfigurationManager.AppSettings["Logo"]))
            if (File.Exists(appConfig.AppSettings.Settings["Logo"].Value))
            {
                //Bitmap bitmapImage = (Bitmap)System.Drawing.Image.FromFile(ConfigurationManager.AppSettings["Logo"]);
                Bitmap bitmapImage = (Bitmap)System.Drawing.Image.FromFile(appConfig.AppSettings.Settings["Logo"].Value);
                BitmapImage image = WpfImageHelper.PngToBitmapImage(bitmapImage);
                Logo.Source = image;
            }
        }

        /// <summary>
        /// Muestra mensaje de Error y asocia método sin parámetros al botón
        /// </summary>
        /// <param name="metodo"></param>
        /// <param name="mensaje"></param>
        void MostrarMensajeError(Action metodo, String mensaje)
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Hidden;
            ViewModel.Controles.PanelErrorVisibility = Visibility.Visible;
            ViewModel.Controles.TextoError = mensaje;
            ViewModel.Controles.SiguienteVisibility = Visibility.Hidden;
            
            if (metodo != null)
            {
                //Grid.SetColumnSpan(ErrorWrapper, 1);
                ViewModel.Controles.ErrorWrapperColumnSpan = 1;
                ViewModel.Controles.BotonErrorVisibility = Visibility.Visible;
                BotonError.Click += (sender, e) =>
                {
                    ViewModel.Controles.PanelErrorVisibility = Visibility.Hidden;
                    ViewModel.Controles.TextoError = String.Empty;
                    ViewModel.Controles.SiguienteVisibility = Visibility.Visible;
                    metodo();
                };
            }
            else
            {
                ViewModel.Controles.BotonErrorVisibility = Visibility.Collapsed;
                //Grid.SetColumnSpan(ErrorWrapper, 2);
                ViewModel.Controles.ErrorWrapperColumnSpan = 2;
            }
        }

        /// <summary>
        /// Muestra mensaje de Error y asocia método al botón
        /// </summary>
        /// <typeparam name="T">Genérico</typeparam>
        /// <param name="metodo">Método a asociar al botón de error</param>
        /// <param name="parametro">Parámetro que se le pasará a 'metodo'</param>
        /// <param name="mensaje">Mensaje a ser desplegado en el campo de error</param>
        void MostrarMensajeErrorConParametro<T>(
            Action<T> metodo,
            T parametro,
            String mensaje)
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Hidden;
            ViewModel.Controles.PanelErrorVisibility = Visibility.Visible;
            ViewModel.Controles.TextoError = mensaje;
            ViewModel.Controles.SiguienteVisibility = Visibility.Hidden;
            if (metodo != null)
            {
                //Grid.SetColumnSpan(ErrorWrapper, 1);
                ViewModel.Controles.ErrorWrapperColumnSpan = 1;
                ViewModel.Controles.BotonErrorVisibility = Visibility.Visible;
                BotonError.Click += (sender, e) =>
                {
                    //GridError.Visibility = Visibility.Hidden;
                    ViewModel.Controles.PanelErrorVisibility = Visibility.Hidden;
                    //Siguiente.Visibility = Visibility.Visible;
                    ViewModel.Controles.TextoError = String.Empty;
                    ViewModel.Controles.SiguienteVisibility = Visibility.Visible;
                    metodo(parametro);
                };
            }
            else
            {
                ViewModel.Controles.BotonErrorVisibility = Visibility.Collapsed;
                //Grid.SetColumnSpan(ErrorWrapper, 2);
                ViewModel.Controles.ErrorWrapperColumnSpan = 2;
            }
        }

        /// <summary>
        /// Cambia el fondo de los controles
        /// </summary>
        /// <param name="hexColor">Color eh formato hex. Ejemplo: #00FF00</param>
        void SetBackgroundForControls(string hexColor)
        {
            if (!String.IsNullOrEmpty(hexColor))
            {
                SolidColorBrush solidColorBrushDefault = (SolidColorBrush)GridPadre.FindResource("PrimaryBrush");
                SolidColorBrush solidColorBrushCustom = solidColorBrushDefault;
                try
                {
                    solidColorBrushCustom = (SolidColorBrush)(new BrushConverter().ConvertFrom(hexColor));
                }
                catch (FormatException ex)
                {
                    logger.Error("Error",ex);
                }
                System.Windows.Media.Color colorResource = (System.Windows.Media.Color)GridPadre.FindResource("PrimaryColor");
                colorResource.A = 100;
                solidColorBrushDefault.Color = solidColorBrushCustom.Color;
                logger.Info("Se modificarán los controles de acuerdo al color configurado");
            }
        }

        /// <summary>
        /// Bloquea el botón de siguiente
        /// </summary>
        void BloquearSiguiente()
        {
            Siguiente.IsEnabled = false;
        }

        /// <summary>
        /// Desbloquea los controles para poder seguir con el funcionamiento
        /// </summary>
        void DesbloquearSiguiente()
        {
            //GridError.Visibility = Visibility.Hidden;
            ViewModel.Controles.PanelErrorVisibility = Visibility.Hidden;
            Siguiente.IsEnabled = true;
            ViewModel.Controles.SiguienteVisibility = Visibility.Visible;
        }

        private void Cerrar_Click(object sender, RoutedEventArgs e)
        {
            if (LectorCodigoDeBarras != null)
            {
                Thread.Sleep(100);
                LectorCodigoDeBarras.Close();
            }
            if (respuesta == null)
            {
                if ((Paso == EstadoVerificacion.VerificacionNegativa) || (Paso == EstadoVerificacion.VerificacionPositiva))
                {
                    BuildRespuesta();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    BVIResponse result = BuildRespuesta();
                    SetPackageResponse(0, "", result);
                }
                else
                {
                    SetPackageResponse(-1, "No se finalizó el proceso de verificación de identidad", BuildRespuesta(-1, "No se finalizó el proceso de verificación de identidad"));
                    logger.Info("Biometrika BviCAM.... Se cierra la ventana sin terminar el proceso de respuesta");
                    //this.Close();
                }
            }
        }
    }
}
