﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BviCamUIAdapter.Libs
{
    public static class AppConfigHelper
    {
        public static int GetTimeoutLector()
        {
            int timeoutDefault = 5;
            bool conversion = Int32.TryParse(ConfigurationManager.AppSettings["TiempoEsperaLectorHuella"], out timeoutDefault);
            return timeoutDefault * 1000;
        }
    }
}
