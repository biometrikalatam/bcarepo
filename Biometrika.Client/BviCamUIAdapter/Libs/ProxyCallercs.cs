﻿
using BviCamUIAdapter.Model.Service;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;

namespace BviCamUIAdapter.Libs
{
    public class ProxyCaller
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ProxyCaller));

        string HostUrl;
        //Company Id en Duro del bioportal, todavía no está claro cómo se va a abordar
        const int CompanyId = 7;

        public Configuration appConfig;

        public ProxyCaller()
        {
            //HostUrl = ConfigurationManager.AppSettings["UrlProxy"];
            HostUrl = appConfig.AppSettings.Settings["UrlProxy"].Value;
            logger.Debug("Host:" + HostUrl);
        }

        public ApiResponse<BP_VerifyResponse> VerifyNec(NecDataToCompare necDataToCompare, string serialSecugen)
        {
            const string VerifyVerb = "api/Utils/Verify";

            logger.Debug("Host:" + appConfig.AppSettings.Settings["UrlProxy"].Value);
            logger.Debug("Verb:" + VerifyVerb);

            var client = new RestClient(HostUrl);
            var request = new RestRequest(VerifyVerb, Method.POST);

            BPVerify verify = new BPVerify()
            {
                Action = BP_VerifyActionType.Verify,
                ClientId = serialSecugen,
                TypeId = "RUT",
                ValueId = necDataToCompare.rut,
                Umbral = 1,
                Flag = BP_VerifyFlag.RAW,
                WSQoRAW = necDataToCompare.rawData,
                MinuciaNEC = necDataToCompare.necTemplate,
                CompanyId = CompanyId,
                EndUser = "EnUserTest",
                IpEndUser = "127.0.0.1",
                Origin = 1
            };

            request.RequestFormat = DataFormat.Json;
            request.AddBody(verify);

            var respuesta = client.Execute(request);
            logger.Info($"Respuesta: {respuesta.Content}");
            if (respuesta.ResponseStatus == ResponseStatus.Completed &&
                respuesta.StatusCode == HttpStatusCode.OK)
            {
                var apiResponse = JsonConvert.DeserializeObject<ApiResponse<BP_VerifyResponse>>(respuesta.Content);
                return apiResponse;
            }
            if (respuesta.ResponseStatus == ResponseStatus.TimedOut)
            {
                throw new TimeoutException();
            }
            else if (respuesta.ResponseStatus == ResponseStatus.Error)
            {
                throw new EndpointNotFoundException();
            }
            else if (respuesta.StatusCode == HttpStatusCode.NotFound)
            {
                throw new EndpointNotFoundException();
            }
            else
            {
                throw new Exception();
            }

        }

        public ApiResponse<BP_VerifyResponse> IsoToIsoCC(string valueid,string iso, String serialSecugen)
        {
            const string IsoToIsoCcVerb = "api/Utils/ConvertISOtoISOCP/{valueid}/{companyID}/{serial}";

            logger.Info("URL Servicio:" + HostUrl + IsoToIsoCcVerb);

            var client = new RestClient(HostUrl);
            var request = new RestRequest("api/Utils/ConvertISOtoISOCP/{valueid}/{companyID}/{serial}", Method.POST);
            //Todo: Reemplazar ésto por algo válido
            request.AddParameter("valueid", valueid, ParameterType.UrlSegment);
            request.AddParameter("companyID", CompanyId, ParameterType.UrlSegment);
            request.AddParameter("serial", serialSecugen, ParameterType.UrlSegment);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(iso);
            var respuesta = client.Execute(request);

            if (respuesta.ResponseStatus == ResponseStatus.Completed &&
                respuesta.StatusCode == HttpStatusCode.OK)
            {
                var apiResponse = JsonConvert.DeserializeObject<Model.Service.ApiResponse<BP_VerifyResponse>>(respuesta.Content);
                return apiResponse;
            }
            else if (respuesta.ResponseStatus == ResponseStatus.TimedOut)
            {
                throw new TimeoutException();
            }
            else if (respuesta.ResponseStatus == ResponseStatus.Error)
            {
                throw new EndpointNotFoundException();
            }
            else if (respuesta.StatusCode == HttpStatusCode.NotFound)
            {
                throw new EndpointNotFoundException();
            }
            else
            {
                throw new Exception();
            }
        }

        public ApiResponse<BP_VerifyResponse> IsoToIsoCC(string iso, String serialSecugen)
        {
            const string IsoToIsoCcVerb = "api/Utils/ConvertISOtoISOCP/{valueid}/{companyID}/{serial}";

            logger.Info("URL Servicio:" + HostUrl + IsoToIsoCcVerb);
            
            var client = new RestClient(HostUrl);
            var request = new RestRequest("api/Utils/ConvertISOtoISOCP/{valueid}/{companyID}/{serial}", Method.POST);
            //Todo: Reemplazar ésto por algo válido
            request.AddParameter("valueid", CompanyId, ParameterType.UrlSegment);
            request.AddParameter("companyID", CompanyId, ParameterType.UrlSegment);
            request.AddParameter("serial", serialSecugen, ParameterType.UrlSegment);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(iso);
            var respuesta = client.Execute(request);

            if (respuesta.ResponseStatus == ResponseStatus.Completed &&
                respuesta.StatusCode == HttpStatusCode.OK)
            {
                var apiResponse = JsonConvert.DeserializeObject<Model.Service.ApiResponse<BP_VerifyResponse>>(respuesta.Content);
                return apiResponse;
            }
            else if (respuesta.ResponseStatus == ResponseStatus.TimedOut)
            {
                throw new TimeoutException();
            }
            else if (respuesta.ResponseStatus == ResponseStatus.Error)
            {
                throw new EndpointNotFoundException();
            }
            else if (respuesta.StatusCode == HttpStatusCode.NotFound)
            {
                throw new EndpointNotFoundException();
            }
            else
            {
                throw new Exception();
            }
        }
    }
}
