﻿using BviCamUIAdapter.Model.Service;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BviCamUIAdapter.Libs
{
    public static class NecServiceCaller
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(NecServiceCaller));

        public static int CallNecService(NecDataToCompare necData)
        {
            return CallServiceRestSharp(necData);
        }

        static int CallServiceHttpClient(NecDataToCompare necDataToCompare)
        {
            var url = ConfigurationManager.AppSettings["localNECServiceURL"];

            var aEnviar = JsonConvert.SerializeObject(necDataToCompare);
            var content = new StringContent(aEnviar, Encoding.UTF8, "application/json");
            using (var client = new HttpClient())
            {
                HttpResponseMessage respuesta = client.PostAsync(url, content).Result;
                var responseContent = respuesta.Content;
                string responseString = responseContent.ReadAsStringAsync().Result;
                logger.Info($"Respuesta obtenida: {responseString}");
                int score = Int32.Parse(responseString);
                logger.Info($"Score de respuesta del servico NEC: {score}");
                return score;

            }
        }

        static int CallServiceRestSharp(NecDataToCompare necDataToCompare)
        {
            var url = ConfigurationManager.AppSettings["localNECServiceURL"];
            var uri = new Uri(url);
            var host = uri.GetLeftPart(System.UriPartial.Authority);
            var verb = uri.ToString().Replace(host.ToString(), String.Empty);

            var client = new RestClient(host);
            var request = new RestRequest(verb, Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(necDataToCompare);
            var respuesta = client.Execute(request);
            logger.Info($"Respuesta: {respuesta.Content}");
            return Int32.Parse(respuesta.Content);
        }
    }
}
