﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BviCamUIAdapter.Model
{
    public enum AlertType
    {
        Error,
        Positive,
        Alert,
        Network,
        Card,
        None
    }
}
