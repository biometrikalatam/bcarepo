﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BviCamUIAdapter.Model.Service
{
    public enum BP_VerifyFlag
    {
        None,
        WSQ = 21,
        RAW = 22
    }
}
