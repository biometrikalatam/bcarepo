﻿using BiometrikaComponentServiceDomain.Model.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using BviCamUIAdapter.Model;
using BiometrikaComponentServiceDomain.Model;

namespace BviCamUIAdapter.Model
{
    /// <summary>
    /// ViewModel de Persona
    /// </summary>
    public class Person : INotifyPropertyChanged
    {
        public Person()
        {
            Rut = "";
            Nombre = "";
            ApellidoPaterno = "";
            ApellidoMaterno = "";
            Sexo = "";
            FotoFirma = "";
            Foto = "";
            FechaNacimiento = new DateTime();
            FechaExpiracion = new DateTime();
            Serie = "";
            Raw = "";
            Iso = "";
            Ansi = "";
            Wsq = "";
            Jpg = "";
        }
        TipoIdentificacion tipoIdentificacion;
        public TipoIdentificacion TipoIdentificacion
        {
            get { return tipoIdentificacion; }
            set
            {
                tipoIdentificacion = value;
                NotifyPropertyChanged();
            }
        }

        string tipoDocumento;
        public string TipoDocumento
        {
            get { return tipoDocumento; }
            set
            {
                tipoDocumento = value;
                NotifyPropertyChanged();
            }
        }

        string rut;
        public string Rut
        {
            get { return rut; }
            set
            {
                rut = value;
                NotifyPropertyChanged();
            }
        }
        string nombre;
        public string Nombre
        {
            get { return nombre; }
            set
            {
                nombre = value;
                NotifyPropertyChanged();
            }
        }
        string apellidoPaterno;
        public string ApellidoPaterno
        {
            get { return apellidoPaterno; }
            set
            {
                apellidoPaterno = value;
                NotifyPropertyChanged();
            }
        }
        string apellidoMaterno;
        public string ApellidoMaterno
        {
            get
            {
                return apellidoMaterno;
            }
            set
            {
                apellidoMaterno = value;
                NotifyPropertyChanged();
            }
        }
        string fotoFirma;
        public string FotoFirma
        {
            get { return fotoFirma; }
            set
            {
                fotoFirma = value;
                NotifyPropertyChanged();
            }
        }
        string foto;
        public string Foto
        {
            get { return foto; }
            set
            {
                foto = value;
                NotifyPropertyChanged();
            }
        }
        DateTime? fechaNacimiento;
        public DateTime? FechaNacimiento
        {
            get
            {
                return fechaNacimiento;
            }
            set
            {
                fechaNacimiento = value;
                NotifyPropertyChanged();
            }
        }
        DateTime? fechaExpiracion;
        //Fecha de expiración de la cédula de identidad
        public DateTime? FechaExpiracion
        {
            get { return fechaExpiracion; }
            set
            {
                fechaExpiracion = value;
                NotifyPropertyChanged();
            }
        }

        String serie;
        /// <summary>
        /// número de serie del documento.
        /// </summary>
        /// <returns></returns>
        public String Serie
        {
            get { return serie; }
            set
            {
                serie = value;
                NotifyPropertyChanged();
            }
        }

        private String raw;

        public String Raw
        {
            get { return raw; }
            set
            {
                raw = value;
                NotifyPropertyChanged();
            }
        }

        private String ansi;

        public String Ansi
        {
            get { return ansi; }
            set
            {
                ansi = value;
                NotifyPropertyChanged();
            }
        }

        private String iso;

        public String Iso
        {
            get { return iso; }
            set
            {
                iso = value;
                NotifyPropertyChanged();
            }
        }

        private String wsq;

        public String Wsq
        {
            get { return wsq; }
            set
            {
                wsq = value;
                NotifyPropertyChanged();
            }
        }

        private String jpg;

        public String Jpg
        {
            get { return jpg; }
            set
            {
                jpg = value;
                NotifyPropertyChanged();
            }
        }

        private String sexo;
        public string Sexo
        {
            get { return sexo; }
            set
            {
                sexo = value;
                NotifyPropertyChanged();
            }
        }

        public Persona CreatePersona()
        {
            return new Persona()
            {
                Nombre = this.Nombre,
                ApellidoMaterno = this.ApellidoMaterno,
                ApellidoPaterno = this.ApellidoPaterno,
                FechaExpiracion = this.FechaExpiracion,
                FechaNacimiento = this.FechaNacimiento,
                Sexo = this.Sexo,
                Foto = this.Foto,
                FotoFirma = this.FotoFirma,
                TipoIdentificacion = this.TipoIdentificacion,
                Rut = this.Rut,
                Serie = this.Serie,
                //agregado
                Raw = this.Raw,
                Iso = this.Iso,
                Ansi = this.Ansi,
                Wsq = this.Wsq,
                Jpeg = this.jpg

            };

        }

        public event PropertyChangedEventHandler PropertyChanged;
        void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
