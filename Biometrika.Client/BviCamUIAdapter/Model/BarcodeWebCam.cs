﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BviCamUIAdapter.Model
{
    public class BarcodeWebCam
    {
        public bool Pdf417 { get; set; }

        public bool Qr { get; set; }

        public string FullPdf417 { get; set; }

        public Byte[] _binaryData { get; set; }

        public string DocumentNumber { get; set; }

        public DateTime BirthDate { get; set; }

        public DateTime ExpirationDate { get; set; }

        public string DOB { get; set; }

        public string DOE { get; set; }

        public string LastName { get; set; }

        public string ValueId { get; set; }

        public int length { get; set; }

        private byte[] temp;

        public byte[] NecTemplate;

        public void ProcessNec()
        {
            for (int z = 0; z < _binaryData.Length; z++)
            {
                if (_binaryData[z] == 32)
                {
                    _binaryData[z] = 0;
                }
            }
            FullPdf417 = Convert.ToBase64String(_binaryData);
            length = (_binaryData[77] << 24) + (_binaryData[76] << 16) + (_binaryData[75] << 8) + (_binaryData[74]);
            temp = new Byte[400];
            Buffer.BlockCopy(_binaryData, 78, temp, 0, length);

            Byte[] _necTemplate = new byte[length];
            Byte[] _aux = (Byte[])temp;

            for (Int32 i = 0; i < length; i++)
            {
                _necTemplate[i] = _aux[i];
            }
            NecTemplate = _necTemplate;


        }
        public String RutFormateado
        {
            get
            { 
                return this.ValueId.Substring(0, ValueId.Length - 1) + "-" + this.ValueId.Substring(this.ValueId.Length - 1);
            }
        }




    }
}
