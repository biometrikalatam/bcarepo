﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BviCamUIAdapter.Model
{
    public static class WebCam
    {
        public enum WebCamError
        {
            [Description("No error")]
            None = 0,
            [Description("No se encuentra la cámara conectada o no coincide con el nombre configurado")]
            NotFound = -1000,
            [Description("Se detuvo la captura por salida del usuario")]
            Stop =-1001,
            [Description("Parámetros faltantes para iniciar componente, revise log")]
            BadParameter =-2000
        }
        public static string GetDescription(Enum value)
        {
            return
                value
                    .GetType()
                    .GetMember(value.ToString())
                    .FirstOrDefault()
                    ?.GetCustomAttribute<DescriptionAttribute>()
                    ?.Description;
        }

    }
}
