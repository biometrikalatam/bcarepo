﻿using BiometrikaComponentServiceDomain.Model;
using BiometrikaComponentServiceDomain.Model.Enum;
using BviCamUIAdapter.Model;
using BviCamUIAdapter.Model.Service;
using Domain;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;

namespace BviCamUIAdapter
{
    
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    //public partial class App : Application
    //{
    //    private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(App));

    //    //public static BioPacket Packet { get; set; }

    //    //public Solicitud solicitud { get; set; }

    //    Mutex _instanceMutex = null;
    //    static string appGuid = "08b06c0f-25b9-4d2d-9dce-1f1ac0e3e1de";

    //    //public Application applicationaux;

    //    //public static void SetPacket(NameValueCollection queryString)
    //    //{
    //    //    Packet = new BioPacket();
    //    //    Packet.PacketParamIn = queryString;
    //    //Packet.PacketParamOut = new Dictionary<string, object>();

    //    //}
        
    //    protected override void OnExit(ExitEventArgs e)
    //    {
    //        logger.Info("Biometrika BVICamManager.. se sale de BVICamUIAdaptar");
    //        if (_instanceMutex != null)
    //            _instanceMutex.ReleaseMutex();
                       
    //        base.OnExit(e);
    //    }

    //    protected override void OnStartup(StartupEventArgs e)
    //    {
    //        bool createdNew;
    //        _instanceMutex = new Mutex(true, appGuid, out createdNew);
    //        if (!createdNew)
    //        {
    //            _instanceMutex = null;
    //            Application.Current.Shutdown();
    //            //dictParamIn = null;
    //            //return;
    //        }
            
    //        //WpfSingleInstance.Make();
    //        int ret = 0;
    //        logger.Info("Biometrika BVICamManager iniciando la aplicación, procedemos a tomar los valores");
    //        base.OnStartup(e);
    //        ////Tomamos el Package.
    //        //ret = CheckParameters(Packet);
    //        //    try
    //        //{ 
    //        //    logger.Info("Validado los parámetros se construye la solicitud");
                    
    //        //        logger.Info("Biometrika BVICam - Se encuentra activada la verificación de webcam");
    //        //        ScanBarCode scanbarcode = new ScanBarCode();
    //        //        scanbarcode.Channel(Packet);
    //        //        logger.Info("Biometrika BVICam.. Se definen los parámetros");
    //        //        scanbarcode.Activate();
    //        //        scanbarcode.ValueId = dictParamIn["VALUEID"];
    //        //        logger.Info("Biometrika BVICam.. Se define el Rut");
    //        //        logger.Info("solicitud:" + solicitud.ToString());
    //        //        scanbarcode.solicitud = solicitud;
    //        //        logger.Info("Se definen solicitud");
    //        //        if (ret == 0)
    //        //            scanbarcode.Statusparameter = true;
    //        //        else
    //        //            scanbarcode.Statusparameter = false;
    //        //        logger.Info(scanbarcode.Statusparameter);
    //        //        scanbarcode.ShowDialog();
    //        //        scanbarcode.Activate();
    //        //}
    //        //catch(Exception exe)
    //        //{
    //        //    logger.Error(exe.Message);
    //        //}


    //    }

    //    private void Application_Startup(object sender, StartupEventArgs e)
    //    {

    //    }

    //    private void Application_Exit(object sender, ExitEventArgs e)
    //    {

    //    }

    //    //private Solicitud BuildSolicitud()
    //    //{
    //    //    try
    //    //    { 
    //    //    solicitud = new Solicitud();
    //    //    solicitud.Persona = new Persona();
    //    //    logger.Info("Biometrika BVICAM.. se construye el objeto persona");
    //    //    solicitud.Persona.Rut = dictParamIn["VALUEID"];
    //    //    logger.Info(dictParamIn["VALUEID"]);
    //    //    solicitud.Configuracion = new BiometrikaComponentServiceDomain.Configuracion();
    //    //    solicitud.Configuracion.ExtraerDatosPersonales = BviCamUIAdapter.Properties.Settings.Default.ExtraerDatosPersonales;
    //    //    solicitud.Configuracion.ExtraerFirma =BviCamUIAdapter.Properties.Settings.Default.ExtraerFirma;
    //    //    solicitud.Configuracion.extraerFoto = BviCamUIAdapter.Properties.Settings.Default.ExtraerFoto;
    //    //    solicitud.Configuracion.bloqueoCedula =BviCamUIAdapter.Properties.Settings.Default.BloquearDocumentoVencido;
    //    //    solicitud.Configuracion.iso = BviCamUIAdapter.Properties.Settings.Default.iso;
    //    //    solicitud.Configuracion.ansi = BviCamUIAdapter.Properties.Settings.Default.ansi;
    //    //    solicitud.Configuracion.raw = BviCamUIAdapter.Properties.Settings.Default.raw;
    //    //    solicitud.Configuracion.vigenciaCedula = BviCamUIAdapter.Properties.Settings.Default.vigenciaCedula;
    //    //    solicitud.Configuracion.wsq = BviCamUIAdapter.Properties.Settings.Default.wsq;
    //    //    solicitud.Configuracion.jpeg = BviCamUIAdapter.Properties.Settings.Default.jpeg;
    //    //    }
    //    //    catch(Exception ex)
    //    //    {
    //    //        logger.Error("aaaaa:" + ex.Message);
    //    //    }
    //    //    logger.Info("Termine de construir la solicitud");
    //    //    return solicitud;

    //    //}
    //    //IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
    //    //private int CheckParameters(BioPacket bioPacketLocal)
    //    //{

    //    //    int ret = 0;
    //    //    try
    //    //    {
    //    //        logger.Info("Biometrika BioCam.CheckParameters In...");
    //    //        if (bioPacketLocal == null) ret = -2;  //Lista de parametros no puede ser nulo

    //    //        logger.Debug("Biometrika BioCam.CheckParameters filling dictParamin...");
    //    //        foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
    //    //        {
    //    //            logger.Debug("Biometrika BVICAM.CheckParameters     Adding => " + k + "=" + (string)bioPacketLocal.PacketParamIn[k] + "...");
    //    //            dictParamIn.Add(k.ToUpper(), bioPacketLocal.PacketParamIn[k]);

    //    //        }
    //    //        logger.Debug("Biometrika BVICAM.CheckParameters dictParamin filled [length = " + dictParamIn.Count + "]");
    //    //        if ((!dictParamIn.ContainsKey("SRC") || String.IsNullOrEmpty(dictParamIn["SRC"]))
    //    //            && ((!dictParamIn.ContainsKey("VALUEID") || String.IsNullOrEmpty(dictParamIn["VALUEID"]))))
    //    //        {
    //    //            ret = -3;  //src/TypeId/ValueId deben ser enviados como parámetros
    //    //            logger.Debug("Biometrika BVICAM.CheckParameters src deben ser enviados como parámetros!");
    //    //        }
    //    //        else
    //    //        {

    //    //            logger.Info("Biometrika BVICAM ..Seteamos el rut definido");


    //    //            logger.Info("Biometrika BVICAM ..Definimos si viene el parámetro solicitud");
    //    //            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
    //    //            try
    //    //            {

    //    //                if(dictParamIn["SOLICITUD"]!=null)
    //    //                {
    //    //                    solicitud = jsonSerializer.Deserialize<Solicitud>(dictParamIn["SOLICITUD"]);
    //    //                    if (solicitud != null)
    //    //                    {
    //    //                        logger.Info("Biometrika BVICAM ..La solicitud viene de acuerdo a los parámetros solicitados");

    //    //                    }
    //    //                    else
    //    //                    {

    //    //                        logger.Error("Biometrika BVICAM .. se asume una verificación por defecto con los mínimos de valores configurados en parámetros");
    //    //                        solicitud = BuildSolicitud();
    //    //                    }
    //    //                }
    //    //                else
    //    //                {
    //    //                    logger.Error("Biometrika BVICAM .. se asume una verificación por defecto con los mínimos de valores configurados en parámetros");
    //    //                    solicitud = BuildSolicitud();
    //    //                }

    //    //            }
    //    //            catch(Exception exe)                    
    //    //            {
    //    //                logger.Error("Biometrika BVICAM .. se asume una verificación por defecto con los mínimos de valores configurados en parámetros",exe);
    //    //                logger.Info("Construye la solicitud");
    //    //                solicitud=BuildSolicitud();
    //    //            }
    //    //            ret = 0;

    //    //        }


    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        logger.Error("Error Biometrika BviCam.CheckParameters [" + ex.Message + "]");
    //    //        ret = -1;
    //    //    }
    //    //    logger.Info("Biometrika BviCam.CheckParameters Ount [ret=" + ret + "]!");
    //    //    return ret;
    //    //}

    //}
}
