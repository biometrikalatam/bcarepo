﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace BviCamUIAdapter.ViewModel
{
    public class Imagenes
    {
        public Imagenes()
        {
            ImagenCedula = new BitmapImage();
            ImagenHuella = new BitmapImage();
            ImagenContactless = new BitmapImage();
            ImagenResultado = new BitmapImage();
        }

        ImageSource imagenCedula;
        public ImageSource ImagenCedula
        {
            get { return imagenCedula; }
            set
            {
                imagenCedula = value;
                NotifyPropertyChanged();
            }
        }

        ImageSource imagenHuella;
        public ImageSource ImagenHuella
        {
            get { return imagenHuella; }
            set
            {
                imagenHuella = value;
                NotifyPropertyChanged();
            }
        }

        ImageSource imagenContactless;
        public ImageSource ImagenContactless
        {
            get { return imagenContactless; }
            set
            {
                imagenContactless = value;
                NotifyPropertyChanged();
            }
        }

        ImageSource imagenResultado;
        public ImageSource ImagenResultado
        {
            get { return imagenResultado; }
            set
            {
                imagenResultado = value;
                NotifyPropertyChanged();
            }
        }

        //ImagenCedula.Source = (ImageSource) GridPadre.Resources["CedulaCompletado"];
        //ImagenHuella.Source = (ImageSource) GridPadre.Resources["HuellaInactivo"];
        //ImagenContactless.Source = (ImageSource) GridPadre.Resources["ContactlessInactivo"];
        //TextoResultado.Visibility = Visibility.Visible;
        //ImagenResultado.Source = (ImageSource) GridPadre.Resources["ResultadoInactivo"];

        public event PropertyChangedEventHandler PropertyChanged;
        void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
