﻿using BiometrikaComponentServiceDomain.Model;
using BviCamUIAdapter.Model;
using Bytescout.BarCodeReader;
using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using TouchlessLib;

namespace BviCamUIAdapter
{
    /// <summary>
    /// Lógica de interacción para ScanBarCode.xaml
    /// Esta pantalla permite interactuar con la web.
    /// Recibirá como parámetro base el rut y lo comparará con el rut leído desde la cédula.
    /// Si este no coincide entonces procederá a mostrar un mensaje en pantalla y cerrar la aplicación.
    /// Además deberá controlar que la cámara este disponible, sino el control de cerrará y validará todos los parámetros
    /// de entrada necesarios para que el control funcione correctamente.
    /// Importante: Toda la lógica de apertura de la aplicación se llevó a esta ventana.
    /// No se realiza a través de un método main, para evitar error de creación por duplicidad del dominio.
    /// </summary>
    public partial class ScanBarCode : Window
    {
        //Variable de log.
        private static readonly ILog logger = LogManager.GetLogger(typeof(ScanBarCode));

        // Touchless lib manager object (to use it you should have TouchlessLib.dll and WebCamLib.dll)
        private TouchlessMgr _touchlessMgr;

        // USED IN POPUP MODE ONLY (see ShowScanPopup() method)
        // Close or not on the first barcode found
        // (results are saved in _foundBarcodes)
        public bool CloseOnFirstBarcodeFound { get; set; }

        // Indicates if the form is closed
        public bool IsClosed { get; set; }

        // Background processing object
        BackgroundWorker _backgroundWorker = new BackgroundWorker();

        // Synchronization event
        readonly AutoResetEvent _synchronizationEvent = new AutoResetEvent(false);

        // Barcode type to scan
        private BarcodeTypeSelector _barcodeTypeToFind = new BarcodeTypeSelector();

        // Array with decoded barcodes from the last scanning session
        public FoundBarcode[] FoundBarcodes { get; set; }

        // Scanning delay (ms); default is to scan every 800 ms.
        const int ScanDelay = 500;

        // Internal varaible to indicate the status.
        public static bool Status = true;

        public delegate void SimpleDelegate();

        public bool HaveData { get; set; }

        private String Photo { get; set; }

        public BioPacket bioPacketLocal;

        private String BarCodeContent { get; set; }

        public String ValueId { get; set; }

        public Solicitud solicitud { get; set; }

        public BarcodeWebCam Barcodewebcam { get; set; }

        private bool cerrar = false;

        private bool CameraError = false;

        public  BioPacket Packet { get; set; }
        
        public bool ShowBvi { get; set; }

        BVIResponse respuesta;

        Mutex _instanceMutex = null;
        static string appGuid = "08b06c0f-25b9-4d2d-9dce-1f1ac0e3e1de";

        public Application applicationaux;


        /// <summary>
        /// Define si el chequeo de parámetros cumple con lo solicitado.
        /// </summary>
        public bool Statusparameter{get;set;}

        /// <summary>
        /// Creates the form.
        /// </summary>
        public ScanBarCode()
        {
            logger.Info("Biometrika BVICam... se inicializa ScanBarCode");
            InitializeComponent();
            //Channel(bioPacketLocal);
            
                lblScanning.Visibility = Visibility.Collapsed;
                CloseOnFirstBarcodeFound = true;
                _backgroundWorker.WorkerSupportsCancellation = true;
                _backgroundWorker.DoWork += BackgroundWorker_DoWork;
                _backgroundWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
                logger.Info("Biometrika BVICam... fin de inicialización ScanBarCode");
            
        }

        internal int Channel(BioPacket bioPacket)
        {
            int ret = 0;
            logger.Info("Biometrika BVICam In...");
            bioPacketLocal = bioPacket;

            //ret = CheckParameters(bioPacketLocal);

            if (ret == 0)
            {
                logger.Info("Biometrika BviCam Out - Parámetros configurados correctamente");
                if (bioPacketLocal.PacketParamOut == null)
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            }
            logger.Info("Biometrika BviCam Out [ret=" + ret + "]...");
            return ret;
        }

        private Solicitud BuildSolicitud()
        {
            try
            {
                solicitud = new Solicitud();
                solicitud.Persona = new Persona();
                logger.Info("Biometrika BVICAM.. se construye el objeto persona");
                solicitud.Persona.Rut = dictParamIn["VALUEID"];
                logger.Info(dictParamIn["VALUEID"]);
                solicitud.Configuracion = new BiometrikaComponentServiceDomain.Configuracion();
                //solicitud.Configuracion.ExtraerDatosPersonales = BviCamUIAdapter.Properties.Settings.Default.ExtraerDatosPersonales;
                //solicitud.Configuracion.ExtraerFirma = BviCamUIAdapter.Properties.Settings.Default.ExtraerFirma;
                //solicitud.Configuracion.extraerFoto = BviCamUIAdapter.Properties.Settings.Default.ExtraerFoto;
                //solicitud.Configuracion.bloqueoCedula = BviCamUIAdapter.Properties.Settings.Default.BloquearDocumentoVencido;
                //solicitud.Configuracion.iso = BviCamUIAdapter.Properties.Settings.Default.iso;
                //solicitud.Configuracion.ansi = BviCamUIAdapter.Properties.Settings.Default.ansi;
                //solicitud.Configuracion.raw = BviCamUIAdapter.Properties.Settings.Default.raw;
                //solicitud.Configuracion.vigenciaCedula = BviCamUIAdapter.Properties.Settings.Default.vigenciaCedula;
                //solicitud.Configuracion.wsq = BviCamUIAdapter.Properties.Settings.Default.wsq;
                //solicitud.Configuracion.jpeg = BviCamUIAdapter.Properties.Settings.Default.jpeg;
            }
            catch (Exception ex)
            {
                logger.Error("Biometrika BviCAM..Error al construir la solicitud:" + ex.Message);
                logger.Error("Biometrika BviCAM..stacktrace:" + ex.StackTrace);
            }
            logger.Info("Biometrika BviCAM..Termine de construir la solicitud");
            return solicitud;

        }
        public void SetPacket(NameValueCollection queryString)
        {
            Packet = new BioPacket();
            Packet.PacketParamIn = queryString;
            Packet.PacketParamOut = new Dictionary<string, object>();
            bioPacketLocal = Packet;
        }

        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        public int CheckParameters(BioPacket bioPacketLocal)
        {

            int ret = 0;
            try
            {
                logger.Info("Biometrika BviCAM..CheckParameters In...");
                if (bioPacketLocal == null) ret = -2;  //Lista de parametros no puede ser nulo

                logger.Debug("Biometrika BviCAM...CheckParameters filling dictParamin...");
                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    logger.Debug("Biometrika BVICAM.CheckParameters     Adding => " + k + "=" + (string)bioPacketLocal.PacketParamIn[k] + "...");
                    dictParamIn.Add(k.ToUpper(), bioPacketLocal.PacketParamIn[k]);

                }
                logger.Debug("Biometrika BVICAM.CheckParameters dictParamin filled [length = " + dictParamIn.Count + "]");
                if ((!dictParamIn.ContainsKey("SRC") || String.IsNullOrEmpty(dictParamIn["SRC"]))
                    && ((!dictParamIn.ContainsKey("VALUEID") || String.IsNullOrEmpty(dictParamIn["VALUEID"]))))
                {
                    ret = -3;  //src/TypeId/ValufeId deben ser enviados como parámetros
                    logger.Debug("Biometrika BviCAM..CheckParameters src deben ser enviados como parámetros!");
                }
                else
                {

                    logger.Info("Biometrika BVICAM ..Seteamos el rut definido");


                    logger.Info("Biometrika BVICAM ..Definimos si viene el parámetro solicitud");
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    try
                    {

                        if (dictParamIn["SOLICITUD"] != null)
                        {
                            solicitud = jsonSerializer.Deserialize<Solicitud>(dictParamIn["SOLICITUD"]);
                            if (solicitud != null)
                            {
                                logger.Info("Biometrika BVICAM ..La solicitud viene de acuerdo a los parámetros solicitados");

                            }
                            else
                            {

                                logger.Error("Biometrika BVICAM .. se asume una verificación por defecto con los mínimos de valores configurados en parámetros");
                                solicitud = BuildSolicitud();
                            }
                        }
                        else
                        {
                            logger.Error("Biometrika BVICAM .. se asume una verificación por defecto con los mínimos de valores configurados en parámetros");
                            solicitud = BuildSolicitud();
                        }

                    }
                    catch (Exception exe)
                    {
                        logger.Error("Biometrika BviCAM.. se asume una verificación por defecto con los mínimos de valores configurados en parámetros", exe);
                        logger.Info("Construye la solicitud");
                        solicitud = BuildSolicitud();
                    }
                    ret = 0;

                }
                //bioPacketLocal = bioPacketLocal;

            }
            catch (Exception ex)
            {
                logger.Error("Biometrika BviCAM..Error Biometrika BviCam.CheckParameters [" + ex.Message + "]");
                ret = -1;
            }
            logger.Info("Biometrika BviCAM..CheckParameters Ount [ret=" + ret + "]!");
            return ret;
        }

    


    // Searches for barcodes in bitmap object
    private FoundBarcode[] FindBarcodes(Bitmap bitmap)
        {
            Reader reader = new Reader();

            try
            {
                reader.RegistrationName = "demo";
                reader.RegistrationKey = "demo";

                this.Dispatcher.Invoke(DispatcherPriority.Normal, (SimpleDelegate)UpdateBarcodeTypeToFindFromCombobox);

                
                reader.BarcodeTypesToFind.PDF417 = true;
                reader.BarcodeTypesToFind.QRCode = true;
                reader.TextEncoding = Encoding.Default; // default ANSI encoding
                //reader.MaxNumberOfBarcodesPerPage = 1;
                FoundBarcode[] result = reader.ReadFrom(bitmap);
                String timeNow = string.Format("{0:HH:mm:ss:tt}", DateTime.Now);

                this.Dispatcher.Invoke(DispatcherPriority.Normal, (SimpleDelegate)delegate
                {
                    if (result != null && result.Length > 0)
                    {

                        //textAreaBarcodes.SelectAll();
                        //textAreaBarcodes.Selection.Text = "\nTime: " + timeNow + "\n";

                        // insert barcodes into text box
                        foreach (FoundBarcode barcode in result)
                        {
                            // make a sound that we found the barcode
                            Console.Beep();
                            // form the string with barcode value
                            String barcodeValue = String.Format("Found: {0} {1}\n", barcode.Type, barcode.Value);

                            // add barcode to the text area output
                            //textAreaBarcodes.AppendText(barcodeValue + "\n");
                            // add barcode to the list of saved barcodes
                            lblFoundBarcodes.Content = String.Format("Found {0} barcodes:", result.Length);
                            if (barcode.Type == Bytescout.BarCodeReader.SymbologyType.PDF417)
                            {
                                logger.Info("Biometrika BviCAM.. retorna el contenido del PDF417");
                                logger.Debug("Biometrika BviCAM.. Código PDF:" + barcode.Value);
                                logger.Debug("Biometrika BviCAM.. Código PDF:" + barcode.Value.Length);
                                BarCodeContent = barcode.Value.Substring(0, 40);

                                Barcodewebcam = new BarcodeWebCam();
                                Barcodewebcam.ValueId = barcode.Value.Substring(0, 9).Replace('\0', ' ').TrimEnd();
                                Barcodewebcam.LastName = barcode.Value.Substring(19, 30).Replace('\0', ' ').TrimEnd();
                                Barcodewebcam.DocumentNumber = barcode.Value.Substring(58, 10).Replace('\0', ' ').TrimEnd();
                                Barcodewebcam.Pdf417 = true;
                                //Barcodewebcam.FullPdf417= Convert.ToBase64String(_binaryData);
                                String _year = barcode.Value.Substring(52, 2);
                                String _month = barcode.Value.Substring(54, 2);
                                String _day = barcode.Value.Substring(56, 2);
                                Barcodewebcam.ExpirationDate = new DateTime(Convert.ToInt32(_year) + 2000, Convert.ToInt32(_month), Convert.ToInt32(_day));
                                String recorte = barcode.Value;
                                int indice = 0;
                                indice = barcode.Value.IndexOf(">TRIAL VERSION EXPIRES 90 DAYS AFTER INSTALLATION<");
                                String total;
                                total = barcode.Value.Substring(0, 420);
                                Barcodewebcam._binaryData = Encoding.Default.GetBytes(total);
                                Barcodewebcam.ProcessNec();

                            }
                            else if (barcode.Type == Bytescout.BarCodeReader.SymbologyType.QRCode)
                            {
                                logger.Info("Biometrika BviCAM..- retorna el contenido del qr");
                                logger.Debug("Biometrika BviCAM.. Código QR:" + barcode.Value);
                                int largo = (">TRIAL VERSION EXPIRES 90 DAYS AFTER INSTALLATION<").Length;
                                Barcodewebcam = new BarcodeWebCam();
                                Barcodewebcam.Qr = true;
                                BarCodeContent = barcode.Value.Substring(0, barcode.Value.Length - largo);
                                Barcodewebcam.ValueId = barcode.Value.Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[0] + barcode.Value.Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[1];


                                Int32 _pos = barcode.Value.LastIndexOf('=');
                                String _mrz = barcode.Value.Substring(_pos + 1, 24);

                                Barcodewebcam.DocumentNumber = _mrz.Substring(0, 9);
                                Barcodewebcam.DOB = _mrz.Substring(10, 6);
                                Barcodewebcam.DOE = _mrz.Substring(17, 6);

                                string _exdate = _mrz.Substring(17, 6);
                                string _year = _exdate.Substring(0, 2);
                                string _month = _exdate.Substring(2, 2);
                                string _day = _exdate.Substring(4, 2);

                                Barcodewebcam.ExpirationDate = new DateTime(Convert.ToInt32(_year) + 2000, Convert.ToInt32(_month), Convert.ToInt32(_day));

                            }



                        }
                    }

                    // make "Scanning..." label flicker
                    lblScanning.Visibility = lblScanning.Visibility == Visibility.Collapsed
                        ? Visibility.Visible
                        : Visibility.Collapsed;
                    lblScanning.UpdateLayout();
                    
                });

                // return found barcodes
                return result;

            }

            finally
            {
                reader.Dispose();
            }
        }
        public String FormatRutFormateado(String rut)
        {
                return rut.Substring(0, rut.Length - 1) + "-" + rut.Substring(rut.Length - 1);
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            
            this.Focus();

            // Populate barcode types into the combobox
            PopulateBarcodeTypesCombobox();
            int ret = 0;
            ret = CheckParameters(Packet);
            //if (Convert.ToBoolean(appConfig.AppSettings.Settings["WebCamActive"].Value) == false)
            //{
            //    if (ret == 0)
            //    {

            //        logger.Info("Biometrika BVICam..Se abre la aplicación con soporte Serial");
            //        MainWindow objMain = new MainWindow(solicitud);
            //        objMain.bioPacketLocal = this.bioPacketLocal;
            //        objMain.barcodeWebCam = Barcodewebcam;
            //        objMain.ShowDialog();
            //        this.Close();
            //    }
            //    else
            //    {
            //        logger.Error("Biometrika BVICam..No vienen los parámetros necesarios");
            //    }

            //}
            //else
            //{ 
            //    if (ret==0)
            //    {
            //        Statusparameter = true;
            //        InitCamera();
            //        logger.Info("Biometrika BVICam.. previo a inicia la cámara");
            //        StartDecoding();
            //        logger.Info("Biometrika BVICam.. después del inicioo cámara");
            //        lblRut.Content = dictParamIn["VALUEID"];
            //    }
            //    else
            //    {
            //        Statusparameter = false;
            //        StopDecoding();
            //    }
            //}
        }

        

        private void InitCamera()
        {
            try
            {
                logger.Info("Biometrika BviCAM..Se realiza el Inicio de la Cámara");
                // Create Touchless lib manager to work with video camera
                _touchlessMgr = new TouchlessMgr();

                // Iterate through available video camera devices
                foreach (Camera camera in _touchlessMgr.Cameras)
                {
                    // Add to list of available camera devices
                    cbCamera.Items.Add(camera);
                }

                // Select first available camera
                cbCamera.SelectedItem = _touchlessMgr.Cameras[0];


                // Setting default image dimensions; see also camera selection event.
                _touchlessMgr.Cameras[0].CaptureWidth = 640;//int.Parse(tbCameraWidth.Text);
                _touchlessMgr.Cameras[0].CaptureHeight = 480;// int.Parse(tbCameraHeight.Text);
                logger.Info("Biometrika BviCAM..Se  termina el Inicio de la Cámara");
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Se produjo una excepción al abrir el componente. \n Detalle técnico:" + ex.Message + "\n Comuníquese con soporte@biometrika.cl", "Biometrika");
                logger.Error("Biometrika BviCAM..Se produjo una excepción al iniciar la cámara:" + ex.Message, ex);
                CameraError = false;
            }
        }

        public void StartDecoding()
        {
            UpdateCameraSelection();

            // Clear the text box output
            //TextRange txt = new TextRange(textAreaBarcodes.Document.ContentStart, textAreaBarcodes.Document.ContentEnd);
            //txt.Text = "";

            // Clean list of barcodes
            FoundBarcodes = null;

            // Check camera selected
            if (cbCamera.SelectedIndex != -1)
            {
                lblMessage.Visibility = Visibility.Hidden;
                // Set status
                Status = true;
               
                cbCamera.IsEnabled = false;
                
                lblScanning.Content = "Scanning...";

                // Start the decoding thread
                _backgroundWorker.RunWorkerAsync(CloseOnFirstBarcodeFound);
            }
            else
            {
                logger.Error("Biometrika BviCAM.. no pudo subir la cámara");
                lblMessage.Content = "No se ha inicialiado la cámara";
                CameraError = true;
                lblFoundBarcodes.Visibility = Visibility.Hidden;
                lblScanning.Visibility = Visibility.Hidden;
                lblMessage.Foreground = new SolidColorBrush(Colors.Red);
                lblMessage.FontSize = 20;

                
            }
        }

        // Update picture box with the latest frame from video camera
        void CurrentCamera_OnImageCaptured(object sender, CameraEventArgs e)
        {
            // You can change image dimensions if needed
            _touchlessMgr.CurrentCamera.CaptureWidth = 640;
            _touchlessMgr.CurrentCamera.CaptureHeight = 480;
            Dispatcher.Invoke(DispatcherPriority.Normal, (SimpleDelegate)delegate
            {
                if (_touchlessMgr != null)
                {
                    pictureVideoPreview.BeginInit();
                    BitmapImage imageSource = BitmapToImageSource(_touchlessMgr.CurrentCamera.GetCurrentImage(), ImageFormat.Png);

                    ScaleTransform st = new ScaleTransform();
                    st.ScaleX = (double)320 / (double)imageSource.PixelWidth;
                    st.ScaleY = (double)240 / (double)imageSource.PixelHeight;
                    TransformedBitmap tb = new TransformedBitmap(imageSource, st);

                    pictureVideoPreview.Source = tb;
                    pictureVideoPreview.EndInit();
                    pictureVideoPreview.UpdateLayout();
                }
            });

        }

        // Convert System.Drawing.Bitmap to System.Windows.Media.Imaging.BitmapImage
        BitmapImage BitmapToImageSource(Bitmap bitmap, ImageFormat imageFormat)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                bitmap.Save(memoryStream, imageFormat);
                memoryStream.Position = 0;
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memoryStream;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                return bitmapImage;
            }
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            StartDecoding();
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            StopDecoding();
        }

        private void StopDecoding()
        {
            try {


            _backgroundWorker.CancelAsync();
              
                // Update UI elements
            lblScanning.Visibility = Visibility.Collapsed;

            // Change working status
            Status = false;

            //btnStart.IsEnabled = true;
            //btnStop.IsEnabled = false;

            //cbBarCodeType.IsEnabled = true;
            cbCamera.IsEnabled = true;

                //tbCameraHeight.IsEnabled = true;
                //tbCameraWidth.IsEnabled = true;

                if (CloseOnFirstBarcodeFound && FoundBarcodes != null && FoundBarcodes.Length > 0)
                {
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        if(Barcodewebcam.ValueId==lblRut.Content.ToString())
                        {
                            logger.Info("Biometrika BviCAM.. coincide el rut informado con el Rut Leído");
                            DeinitCamera();

                                  this.Hide();
                                  MainWindow objMain = new MainWindow(solicitud);
                                  objMain.bioPacketLocal = this.bioPacketLocal;
                                  objMain.barcodeWebCam = Barcodewebcam;
                                  objMain.ShowDialog();
                                  ShowBvi = true;
                            


                        }
                        else
                        {
                            logger.Error("Biometrika BviCAM.. no coincide el rut leído");
                            //MessageBox.Show("El rut configurado no coincide con el rut leído en la cédula");                            
                            BVIResponse bvi = BuildRespuesta(-3000, "El rut no coincide con la lectura");
                            SetPackageResponse(-3000,"El rut no coincide con la lectura", bvi);
                            DeinitCamera();
                            this.Close();


                        }
                        
                    }));
                }
                else if(Statusparameter==false)
                {
                    
                    logger.Info("Biometrika BVICam.. se cierra la aplicación por solicitud del usuario");
                    cerrar = false;
                    BVIResponse bvi = BuildRespuesta((int)WebCam.WebCamError.Stop, (String)WebCam.GetDescription(WebCam.WebCamError.Stop));
                    SetPackageResponse((int)WebCam.WebCamError.BadParameter, WebCam.GetDescription(WebCam.WebCamError.BadParameter), bvi);
                    DeinitCamera();
                    this.Close();
                    
                }
                else if(cerrar==true)
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {

                        //StopDecoding();
                        logger.Info("Biometrika BVICam.. se cierra la aplicación por solicitud del usuario");
                        cerrar = false;
                        BVIResponse bvi = BuildRespuesta((int)WebCam.WebCamError.Stop, (String)WebCam.GetDescription(WebCam.WebCamError.Stop));
                        SetPackageResponse(0,"a", bvi);
                        DeinitCamera();
                        Thread.Sleep(500);
                        this.Close();
                        //StopDecoding();
                    }));
                    
                }
                
             
            }
            catch (Exception ex)
            {
                //MessageBox.Show("exe:" + ex.Message);
                logger.Error("Biometrika BviCAM..Se produjo un error al detener el stream");
            }
        }

        private BVIResponse BuildRespuesta(int err, String msg)
        {
            respuesta = new BVIResponse();
            respuesta.Persona = new Persona();
            respuesta.ResultadoVerificacion = new ResultadoVerificacion();
            respuesta.ErrorNumber = err;
            respuesta.Mensaje = msg;
            return respuesta;
        }

        public void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            bool closeOnFirstBarcode = (bool)e.Argument;

            while (true)
            {
                // Work till user canceled the scan
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    _synchronizationEvent.Set();
                    return;
                }

                // Get current frame bitmap from camera using Touchless lib
                Bitmap bitmap = _touchlessMgr.CurrentCamera.GetCurrentImage();

                // Search barcodes
                FoundBarcode[] result = null;

                if (bitmap != null)
                    result = FindBarcodes(bitmap);

                // Check if we need to stop on first barcode found
                if (closeOnFirstBarcode && result != null && result.Length > 0)
                {
                    e.Result = result;
                    return; // end processing
                }

                // Wait a little to lower CPU load
                Thread.Sleep(ScanDelay);
            }
        }

        private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Clear last results
            FoundBarcodes = null;

            if (e.Cancelled)
            {
                lblScanning.Content = "Canceled";
            }
            else if (e.Error != null)
            {
                lblScanning.Content = "Error: " + e.Error.Message;
            }
            else
            {
                lblScanning.Content = "Done.";
                FoundBarcodes = (FoundBarcode[])e.Result;

                StopDecoding();
            }

            //StopDecoding();
        }

        private void cbCamera_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateCameraSelection();
        }

        private void UpdateCameraSelection()
        {
            if (cbCamera.Items.Count > 0 && cbCamera.SelectedIndex > -1)
            {
                if (_touchlessMgr.CurrentCamera != null)
                    _touchlessMgr.CurrentCamera.OnImageCaptured -= CurrentCamera_OnImageCaptured;

                _touchlessMgr.CurrentCamera = null;

                Camera currentCamera = _touchlessMgr.Cameras[cbCamera.SelectedIndex];

                // Setting camera output image dimensions
                currentCamera.CaptureWidth = 640;
                currentCamera.CaptureHeight = 480;

                _touchlessMgr.CurrentCamera = currentCamera;
                currentCamera.OnImageCaptured += CurrentCamera_OnImageCaptured;
            }
        }

        // Updates barcode type filter according with combobox selection
        private void UpdateBarcodeTypeToFindFromCombobox()
        {
            ////string selectedItemText = cbBarCodeType.Text;

            //if (string.IsNullOrEmpty(selectedItemText))
            //    throw new Exception("Empty barcode type selection.");

            _barcodeTypeToFind.Reset();

            //// Iterate through BarcodeTypeSelector bool properties 
            //// and enable property by barcode name selected in the combobox
            //foreach (PropertyInfo propertyInfo in typeof(BarcodeTypeSelector).GetProperties())
            //{
            //    // Skip readonly properties
            //    if (!propertyInfo.CanWrite)
            //        continue;

            //    if (propertyInfo.Name == selectedItemText)
            //        propertyInfo.SetValue(_barcodeTypeToFind, true, null);
            //}
        }

        protected void PopulateBarcodeTypesCombobox()
        {
           
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {

            //Deinitialize();
            //_touchlessMgr.Dispose();

            //base.OnClosing(e);

        }

        private void Deinitialize()
        {
            _backgroundWorker.CancelAsync();

            // Wait until BackgroundWorker finished
            if (_backgroundWorker.IsBusy)
                _synchronizationEvent.WaitOne();

            // Deinit camera
            //DeinitCamera();

            // Mark as closed
            IsClosed = true;
            

        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            
            Close();
        }

        private void DeinitCamera()
        {
            if (_touchlessMgr != null)
            {
                if(_touchlessMgr.CurrentCamera!=null)
                    _touchlessMgr.CurrentCamera.OnImageCaptured -= CurrentCamera_OnImageCaptured;
                _touchlessMgr.CurrentCamera = null;
            }

            if (cbCamera.SelectedItem != null)
                cbCamera.SelectedItem = null;

            cbCamera.Items.Clear();
            _touchlessMgr = null;

            //Thread.Sleep(500);
        }

        private void btnTryPopup_Click(object sender, RoutedEventArgs e)
        {
            // Stop scan if any
            StopDecoding();

            // Deinit the current camera
            DeinitCamera();

            ShowScanPopup();

            // Reinit current camera
            InitCamera();
        }

        private void ShowScanPopup()
        {
           
        }
        /// <summary>
        /// Define el paquete de retorno de datos.
        /// </summary>
        /// <param name="error"></param>
        /// <param name="msg"></param>
        /// <param name="result"></param>
        public void SetPackageResponse(int error, String msg,BVIResponse result)
        {
            try
            { 
                Packet.PacketParamOut = null;
                Packet.PacketParamOut = new Dictionary<string, object>();
                logger.Info("Biometrika BVICam.. se arma el mensaje de retorno ");            
                Packet.PacketParamOut.Add("Data", result);
            }
            catch(Exception exe)
            {
                MessageBox.Show(exe.Message);
            }
        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Biometrika BviCAM..Se presiona botón cerrar");
            cerrar = true;
            if (CameraError == false)
            {
                logger.Info("Biometrika BviCAM.. se cierra la aplicación");
                BVIResponse bvi = BuildRespuesta((int)WebCam.WebCamError.NotFound, WebCam.GetDescription(WebCam.WebCamError.NotFound));
                SetPackageResponse((int)WebCam.WebCamError.NotFound, WebCam.GetDescription(WebCam.WebCamError.NotFound), bvi);
                StopDecoding();
            }
            else
            {
                
                    logger.Error("Biometrika BviCAM.. se cierra la aplicación porque la webcam no estaba conectada");
                    BVIResponse bvi = BuildRespuesta((int)WebCam.WebCamError.NotFound, WebCam.GetDescription(WebCam.WebCamError.NotFound));
                    SetPackageResponse((int)WebCam.WebCamError.NotFound, WebCam.GetDescription(WebCam.WebCamError.NotFound),bvi);
                    StopDecoding();
                    
                
            }
        }
    }
}
