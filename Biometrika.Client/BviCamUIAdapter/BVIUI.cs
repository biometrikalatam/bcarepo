﻿using Biometrika.BioApi20;
using Biometrika.BioApi20.BSP;
using Biometrika.BioApi20.Interfaces;
using Domain;
using LibBiometrikaVerification;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BviCamUIAdapter
{
    public partial class BVIUI : Form
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BVIUI));

        internal Configuration appConfig;
        BCR LectorCodigoDeBarras;

        Biometrika.BioApi20.BSP.BSPBiometrika BSP;
        List<Biometrika.BioApi20.Sample> _SamplesCaptured;
        byte[] _Sample_Standard;

        public string SerialSensor { get; set; }
        public bool SensorConnected; //=> ReaderFactory.ReaderConnected;
        //private AbstractReader reader;

        //public int Tokencontent { get; set; }
        public bool HaveData { get; set; }
        public static BioPacket Packet { get; set; }
        private BioPacket bioPacketLocal;


        #region General

        public BVIUI()
        {
            InitializeComponent();

            LOG.Info("BVIUI.BVIUI Creating...");
            //System.Drawing.Drawing2D.GraphicsPath objDraw = new System.Drawing.Drawing2D.GraphicsPath();
            //objDraw.AddEllipse(0, 0, this.picSample.Width, this.picSample.Height);
            //this.picSample.Region = new Region(objDraw);

            ///// Timer del control general, se cierra en 10 segundos
            //timerControlGeneral = new System.Timers.Timer(10000);
            //timerControlGeneral.Elapsed += TimerControlGeneral_Elapsed;

            //// timer de esperar la huella
            //timer = new System.Timers.Timer(1000);
            //timer.Elapsed += Timer_Elapsed;
            //Tokencontent = TokenContentEnum.ALL;

            //timer.Enabled = false;

            //InitBSP();

            //timerShowRestToTimeout.Enabled = false;
            //CloseControl(false);
            LOG.Info("BVIUI.BVIUI Created!");
        }

        private void BVIUI_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            this.TopMost = true;
            this.TopLevel = true;

            //ProcessBodyPartShow();

            LOG.Debug("BVIUI.BVIUI_Load OK!");
        }

        internal int Channel(BioPacket bioPacket)
        {
            int ret = 0;
            LOG.Info("BioPortalClient7UI.Channel In...");
            bioPacketLocal = bioPacket;

            ret = CheckParameters(bioPacketLocal);

            if (ret == 0)
            {
                if (bioPacketLocal.PacketParamOut == null)
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            }
            LOG.Info("BioPortalClient7UI.Channel Out [ret=" + ret + "]...");
            return ret;
        }

        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        private int CheckParameters(BioPacket bioPacketLocal)
        {
            /*
                 src=cam&
                 typeId=RUT&
                 valueId=21284415-2&
                 Tokencontent=1&
                 Device=1&
                 AuthenticationFactor=2&
                 OperationType=1&
                 MinutiaeType=7&
                 Timeout=5000&
                 Theme=theme_blue&
                 Hash=4jf4jf532f23f4#
            */
            int ret = 0;
            try
            {
                LOG.Info("BioPortalClient7UI.CheckParameters In...");
                if (bioPacketLocal == null)
                    ret = -2;  //Lista de parametros no puede ser nulo

                //Paso a Dictionary

                LOG.Debug("BioPortalClient7UI.CheckParameters filling dictParamin...");
                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    LOG.Debug("BioPortalClient7UI.CheckParameters     Adding => " + k + "=" + (string)bioPacketLocal.PacketParamIn[k] + "...");
                    dictParamIn.Add(k.ToUpper(), bioPacketLocal.PacketParamIn[k]);
                }
                LOG.Debug("BioPortalClient7UI.CheckParameters dictParamin filled [length = " + dictParamIn.Count + "]");

                if (!dictParamIn.ContainsKey("SRC") || String.IsNullOrEmpty(dictParamIn["SRC"]) ||
                    !dictParamIn.ContainsKey("TYPEID") || String.IsNullOrEmpty(dictParamIn["TYPEID"]) ||
                    !dictParamIn.ContainsKey("VALUEID") || String.IsNullOrEmpty(dictParamIn["VALUEID"]))
                {
                    ret = -3;  //src/TypeId/ValueId deben ser enviados como parámetros
                    LOG.Debug("BioPortalClient7UI.CheckParameters src/TypeId/ValueId deben ser enviados como parámetros!");
                }
                else
                {
                    LOG.Debug("BioPortalClient7UI.CheckParameters evaluando cada parámetro...");
                    if (!dictParamIn.ContainsKey("TOKENCONTENT") || String.IsNullOrEmpty(dictParamIn["TOKENCONTENT"]))
                    {
                        if (!dictParamIn.ContainsKey("TOKENCONTENT"))
                        {
                            dictParamIn.Add("TOKENCONTENT", "0");
                        }
                        else
                        {
                            dictParamIn["TOKENCONTENT"] = "0";  //Ambos Minucia + WSQ
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters Tokencontent = " + dictParamIn["TOKENCONTENT"]);
                    if (!dictParamIn.ContainsKey("DEVICE") || String.IsNullOrEmpty(dictParamIn["DEVICE"]))
                    {
                        if (!dictParamIn.ContainsKey("DEVICE"))
                        {
                            dictParamIn.Add("DEVICE", "1");
                        }
                        else
                        {
                            dictParamIn["DEVICE"] = "1"; //DP - no imposta ahora
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters Device = " + dictParamIn["DEVICE"]);
                    if (!dictParamIn.ContainsKey("AUTHENTICATIONFACTOR") || String.IsNullOrEmpty(dictParamIn["AUTHENTICATIONFACTOR"]))
                    {
                        if (!dictParamIn.ContainsKey("AUTHENTICATIONFACTOR"))
                        {
                            dictParamIn.Add("AUTHENTICATIONFACTOR", "2");
                        }
                        else
                        {
                            dictParamIn["AUTHENTICATIONFACTOR"] = "2"; //Fingerprint
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters AuthenticationFactor = " + dictParamIn["AUTHENTICATIONFACTOR"]);
                    if (!dictParamIn.ContainsKey("OPERATIONTYPE") || String.IsNullOrEmpty(dictParamIn["OPERATIONTYPE"]))
                    {
                        if (!dictParamIn.ContainsKey("OPERATIONTYPE"))
                        {
                            dictParamIn.Add("OPERATIONTYPE", "1");
                        }
                        else
                        {
                            dictParamIn["OPERATIONTYPE"] = "1";
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters OperationType = " + dictParamIn["OPERATIONTYPE"]);
                    if (!dictParamIn.ContainsKey("MINUTIAETYPE") || String.IsNullOrEmpty(dictParamIn["MINUTIAETYPE"]))
                    {
                        if (!dictParamIn.ContainsKey("MINUTIAETYPE"))
                        {
                            dictParamIn.Add("MINUTIAETYPE", "14");
                        }
                        else
                        {
                            dictParamIn["MINUTIAETYPE"] = "14"; //ISO
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters MinutiaeType = " + dictParamIn["MINUTIAETYPE"]);
                    if (!dictParamIn.ContainsKey("TIMEOUT") || String.IsNullOrEmpty(dictParamIn["TIMEOUT"]))
                    {
                        if (!dictParamIn.ContainsKey("TIMEOUT"))
                        {
                            dictParamIn.Add("TIMEOUT", "10000");
                        }
                        else
                        {
                            dictParamIn["TIMEOUT"] = "10000";
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters Timeout = " + dictParamIn["TIMEOUT"]);
                    if (!dictParamIn.ContainsKey("THEME") || String.IsNullOrEmpty(dictParamIn["THEME"]))
                    {
                        if (!dictParamIn.ContainsKey("THEME"))
                        {
                            dictParamIn.Add("THEME", "theme_ligthgreen");
                        }
                        else
                        {
                            dictParamIn["THEME"] = "theme_green";
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters Theme = " + dictParamIn["THEME"]);
                    //Add 10-12-2018 - x AFEX - Para habilitar solo aquellos dedos que estan enrolados
                    if (!dictParamIn.ContainsKey("BODYPARTSHOW") || String.IsNullOrEmpty(dictParamIn["BODYPARTSHOW"]))
                    {
                        if (!dictParamIn.ContainsKey("BODYPARTSHOW"))
                        {
                            dictParamIn.Add("BODYPARTSHOW", "-1");  //Todos
                        }
                        else
                        {
                            dictParamIn["BODYPARTSHOW"] = "-1";
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters BodypartShow = " + dictParamIn["BODYPARTSHOW"]);


                    if (!dictParamIn.ContainsKey("HASH") || String.IsNullOrEmpty(dictParamIn["HASH"]))
                    {
                        if (!dictParamIn.ContainsKey("HASH"))
                        {
                            dictParamIn.Add("HASH", Guid.NewGuid().ToString("N"));
                        }
                        else
                        {
                            dictParamIn["HASH"] = Guid.NewGuid().ToString("N");
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters Hash = " + dictParamIn["HASH"]);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Error BioPortalClient7UIAdapter.CheckParameters [" + ex.Message + "]");
                ret = -1;
            }
            LOG.Info("BioPortalClient7UI.CheckParameters Ount [ret=" + ret + "]!");
            return ret;
        }

        private void MostrarMensajeError(string v)
        {
            throw new NotImplementedException();
        }

        public String FormatRutFormateado(String rut)
        {
            return rut.Substring(0, rut.Length - 1) + "-" + rut.Substring(rut.Length - 1);

        }



        #endregion General

        #region BioAPI.2.0

        internal void SetBSP(BSPBiometrika bsp)
        {
            try
            {
                BSP = bsp;
                BSP.OnCapturedEvent += OnCaptureEvent;
                BSP.OnTimeoutEvent += OnTimeoutEvent;
            }
            catch (Exception ex)
            {
                LOG.Error("BioPortalClient7UI.SetBSP Error = " + ex.Message);
            }
        }

        internal void FreeBSP()
        {
            BSP.OnCapturedEvent -= OnCaptureEvent;
            BSP.OnTimeoutEvent -= OnTimeoutEvent;
        }

        private void InitBSP()
        {
            //BSP = new Biometrika.BioApi20.BSP.BSPBiometrika();

            //BSP.BSPAttach("2.0", true);

            //GetInfoFromBSP();

            BSP.OnCapturedEvent += OnCaptureEvent;
            BSP.OnTimeoutEvent += OnTimeoutEvent;
        }

        private void OnCaptureEvent(int errCode, string errMessage, List<Sample> samplesCaptured)
        {
            string _WSQ = null;
            string _TemplateEnroll = null;
            string _TemplateVerify = null;
            string _TemplateISO = null;
            string _TemplateANSI = null;
            try
            {
                _SamplesCaptured = samplesCaptured;

                //Sample s;
                //try
                //{
                //    if (samplesCaptured != null && samplesCaptured.Count > 0)
                //    {
                //        //if (chkOrigianal.Checked)
                //        //{
                //        //    s = samplesCaptured[samplesCaptured.Count - 2];
                //        //}
                //        //else
                //        //{
                //            s = samplesCaptured[samplesCaptured.Count - 1];
                //        //}
                //        if (s.MinutiaeType == 41) picSample.Image = (Bitmap)s.Data;
                //    }
                //}
                //catch (Exception ex)
                //{
                //    //richTextBox1.Text = "Error obteniendo imagen!" + Environment.NewLine;
                //}

                //richTextBox1.Text += "Samples Obtenidos:" + Environment.NewLine;
                //richTextBox1.Text += "----------------------------------------------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;
                //foreach (Sample item in samplesCaptured)
                //{
                //    string saux = item.SampleWith + "x" + item.SampleHeight;
                //    //richTextBox1.Text += "AFactor=" + item.AuthenticationFactor.ToString() +
                //    //                     " - MType=" + item.MinutiaeType.ToString() +
                //    //                     " - BodyPart=" + item.BodyPart.ToString() +
                //    //                     " - Operacion=" + (item.Operation == 0 ? "Verify" : "Enroll") +
                //    //                     " - WxH=" + saux +
                //    //                     " - Sample=" + (item.MinutiaeType == 41 ? "Image" : Convert.ToBase64String((byte[])item.Data).Substring(0, 10) + "...")
                //    //                     + Environment.NewLine;
                //}
                //richTextBox1.Text += "----------------------------------------------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;

                string aux = "";


                int idx = 0;
                foreach (Sample item in samplesCaptured)
                {
                    //if (item.MinutiaeType == 41)
                    //{
                    //    System.IO.File.WriteAllBytes(@"c:\std\sample_mt_41_" + (idx++) + ".bmp", (byte[])item.Data);
                    //    Bitmap b = (Bitmap)item.Data;
                    //    b.Save(@"c:\std\sample_mt_41_" + (idx++) + ".jpg", ImageFormat.Jpeg);
                    //}
                    //else
                    //{
                    if (item.MinutiaeType == 13 && item.Operation == 0)  //Solo proceso los de verify
                    {
                        _TemplateANSI = Convert.ToBase64String((byte[])item.Data);
                    }
                    if (item.MinutiaeType == 14 && item.Operation == 0)  //Solo proceso los de verify
                    {
                        _TemplateISO = Convert.ToBase64String((byte[])item.Data);
                    }

                    if (item.MinutiaeType == 21)  //Solo proceso los de verify
                    {
                        _WSQ = Convert.ToBase64String((byte[])item.Data);
                    }

                    if (item.MinutiaeType != 41 && item.MinutiaeType != 13 && item.MinutiaeType != 14 &&
                        item.MinutiaeType != 22 && item.MinutiaeType != 21)  //Solo proceso los de verify
                    {
                        if (item.Operation == 0) //Es verify
                        {
                            _TemplateVerify = Convert.ToBase64String((byte[])item.Data);
                        }
                        else //es Enroll
                        {
                            _TemplateEnroll = Convert.ToBase64String((byte[])item.Data);
                        }
                    }
                    //}
                }

                TokenResponse = new TokenResponse();
                bool hasCapture = false;
                //reader.OnCapturedEvent -= OnCapturedEvent;
                try
                {
                    //if (fingerCaptured.CaptureQuality == ReaderFactory.CaptureQuality.QUALITY_GOOD)
                    if (errCode == 0 && samplesCaptured != null)
                    {
                        LOG.Debug("BioPortalClient7UI.CaptureFinger Huella No Null...");
                        TokenResponse.Huella = GetImageB64FromImage((Bitmap)(samplesCaptured[samplesCaptured.Count - 1]).Data);
                        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.Huella = " + TokenResponse.Huella);
                        TokenResponse.Width = samplesCaptured[samplesCaptured.Count - 1].SampleWith; // fingerCaptured.Image.Width;
                        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.Width = " + TokenResponse.Width);
                        TokenResponse.Height = samplesCaptured[samplesCaptured.Count - 1].SampleHeight; //fingerCaptured.Image.Height;
                        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.Height = " + TokenResponse.Height);
                        TokenResponse.BodyPart = Finger;
                        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.BodyPart = " + TokenResponse.BodyPart);
                        picSample.Image = (Bitmap)(samplesCaptured[samplesCaptured.Count - 2]).Data; // fingerCaptured.Image; // BioRouterInterpreter.Base64ToImage(serverResponse.Huella);
                        picSample.Refresh();
                        LOG.Debug("BioPortalClient7UI.CaptureFinger picSample.Image filled (picSample.Image != null) => " +
                            (picSample.Image != null).ToString());
                        this.Refresh();

                        //hasCapture = GenerateToken(fingerCaptured.WSQ64, fingerCaptured.TemplateRegistry64,
                        //                            fingerCaptured.TemplateVerify64,
                        //                            fingerCaptured.TemplateANSI64, fingerCaptured.TemplateISO64);
                        hasCapture = GenerateToken(_WSQ, _TemplateEnroll, _TemplateVerify, _TemplateANSI, _TemplateISO);
                        LOG.Debug("BioPortalClient7UI.CaptureFinger GenerateToken = " + hasCapture.ToString());
                        if (hasCapture)
                            SetColorLabF(Color.Green);
                        else
                            SetColorLabF(Color.Red);
                        Thread.Sleep(1000);

                        //bioPacketLocal.PacketParamOut.Add("TemplateVerify64", fingerCaptured.TemplateVerify64);
                        //bioPacketLocal.PacketParamOut.Add("TemplateRegistry64", fingerCaptured.TemplateRegistry64);
                        //bioPacketLocal.PacketParamOut.Add("TemplateANSI64", fingerCaptured.TemplateANSI64);
                        //bioPacketLocal.PacketParamOut.Add("TemplateISO64", fingerCaptured.TemplateISO64);
                        //bioPacketLocal.PacketParamOut.Add("WSQ", fingerCaptured.WSQ64);

                        //bioPacketLocal.PacketParamOut.Add("Huella", fingerCaptured.Image);
                        //bioPacketLocal.PacketParamOut.Add("Width", fingerCaptured.Image.Width);
                        //bioPacketLocal.PacketParamOut.Add("Height", fingerCaptured.Image.Height);

                        //bioPacketLocal.PacketParamOut.Add("Error", "0|Capture OK");
                        //bioPacketLocal.PacketParamOut.Add("BodyPart", dictParamIn["Finger"]);
                        ////bioPacketLocal.PacketParamOut.Add("BodyPartName", );
                        //bioPacketLocal.PacketParamOut.Add("AuthenticationFactor", 2);  //AF =  Fingerprint
                        ////bioPacketLocal.PacketParamOut.Add("AuthenticationFactorName", );
                        //bioPacketLocal.PacketParamOut.Add("SerialDevice", SerialSensor);

                        //picSample.Image = fingerCaptured.Image;
                        //picSample.Refresh();
                        hasCapture = true;
                    }
                    //else if (fingerCaptured.CaptureQuality == ReaderFactory.CaptureQuality.QUALITY_POOR)
                    //{
                    //    bioPacketLocal.PacketParamOut.Add("Error", "-41|Mala Calidad Captura"); //Mala Calidad => SERR_WSQ_LOW_QUALITY
                    //    picSample.Refresh();
                    //}
                }
                catch (Exception ex)
                {
                    bioPacketLocal.PacketParamOut.Add("Error", "-1|Error Procesando Captura [" + ex.Message + "]");
                    //WriteLog("ERROR - BeginCapture Error - " + ex.Message);
                }
                CloseControl(hasCapture);
            }
            catch (Exception ex)
            {

            }
            //timer1.Enabled = true;
        }

        private void OnTimeoutEvent(int errCode, string errMessage, ISensor sensor)
        {
            //picSample.Image = null;
            //MessageBox.Show(errCode + " - " + errMessage);
        }

        #endregion BioaAPI.2.0

        #region NFC



        #endregion NFC

        #region LCB

        /// <summary>
        /// Inicialización del código de barras
        /// </summary>
        /// <returns>true si es que se inicializó, false en caso contrario</returns>
        bool InitBCR()
        {
            try
            {
                //var comPort = ConfigurationManager.AppSettings["COMPort"].ToUpper();
                var comPort = appConfig.AppSettings.Settings["COMPort"].Value.ToUpper();
                LOG.Info($"BVIUI.InitBCR - Abriendo el lector de código de barras configurado en {comPort}");
                LectorCodigoDeBarras = new BCR(comPort);
                LectorCodigoDeBarras.DataCaptured += ProcesarCedulaLeida;
                var bcrOpened = LectorCodigoDeBarras.Open();
                if (!bcrOpened)
                {
                    LOG.Info("Error al abrir el lector de código de barras");
                    MostrarMensajeError("Error al abrir el lector de código de barras");
                }
                else
                {
                    LOG.Info("Lector de Código de barras abierto correctamente");
                }
                return bcrOpened;
            }
            catch (Exception ex)
            {
                LOG.Error(
                    "Error al abrir el lector de código de barras. Puerto " +
                        appConfig.AppSettings.Settings["COMPort"].Value.ToUpper(), ex);
                //ConfigurationManager.AppSettings["COMPort"].ToUpper(),ex);
                return false;
            }
        }

       

        void InitWithWebCam()
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                ResetControls();
                logger.Info("Cédula leída corresponde a rut de la solicitud");
                DesbloquearSiguiente();
                ViewModel.Persona.Rut = barcodeWebCam.ValueId;
                ViewModel.Persona.ApellidoPaterno = barcodeWebCam.LastName;
                ViewModel.Persona.Serie = barcodeWebCam.DocumentNumber;
                if (SolicitudVerificacion.Configuracion.ExtraerDatosPersonales)
                {
                    ViewModel.Persona.FechaExpiracion = SolicitudVerificacion.Persona.FechaExpiracion;
                    ViewModel.Persona.FechaNacimiento = SolicitudVerificacion.Persona.FechaNacimiento;
                }
                else
                {
                    ViewModel.Persona.FechaExpiracion = Convert.ToDateTime("0001-01-01T00:00:00");
                    ViewModel.Persona.FechaNacimiento = Convert.ToDateTime("0001-01-01T00:00:00");
                }

                //Bloqueo de los controles
                ViewModel.Controles.RutIsEnabled = false;
                ViewModel.Controles.TipoDocumentoIsEnabled = false;

                Paso = EstadoVerificacion.EsperandoHuellaEscaneadaCedulaNueva;
                ViewModel.Persona.TipoIdentificacion = TipoIdentificacion.CedulaChilenaNueva;
                ViewModel.Persona.TipoDocumento = "Cédula Nueva";

                if (barcodeWebCam.Pdf417)
                {
                    Paso = EstadoVerificacion.EsperandoHuellaEscaneadaCedulaVieja;
                    ViewModel.Persona.TipoIdentificacion = TipoIdentificacion.CedulaChilenaAntigua;
                    ViewModel.Persona.TipoDocumento = "Cédula Antigua";

                    ViewModel.Controles.ApellidoPaternoIsEnabled = false;
                }
                else if (barcodeWebCam.Pdf417 == false)
                {
                    if (SolicitudVerificacion.Configuracion.BloqueoMenoresDeEdad == TipoBloqueo.Bloqueo)
                    {
                        if (IsMoreThan18((DateTime)LectorCodigoDeBarras.BirthDate) == true)
                        {
                            MostrarMensajeError(null, "Menor de Edad");
                        }
                    }
                }
                TextoHuella.Visibility = Visibility.Visible;
                UpdateButtonsImages();
            }));
        }



        #endregion LCB




       
    }
}
