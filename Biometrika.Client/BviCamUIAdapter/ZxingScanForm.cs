﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

using AForge.Video;
using ZXing;
using System.IO;
using System.Text;
using BiometrikaComponentServiceDomain.Model;
using BviCamUIAdapter.Model;
using Domain;
using log4net;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Reflection;

namespace BviCamUIAdapter
{
    public partial class ZxingScanForm : Form
    {
        #region "VariablesDeAforge"
        private readonly CameraDevices camDevices;
        private Bitmap currentBitmapForDecoding;
        private readonly Thread decodingThread;
        private Result currentResult;
        private readonly Pen resultRectPen;
        #endregion "VariablesDeAforge"

        public BioPacket Packet { get; set; }

        public BioPacket bioPacketLocal;

        public bool ShowBvi { get; set; }

        public BarcodeWebCam Barcodewebcam { get; set; }

        private String BarCodeContent { get; set; }

        public Solicitud solicitud { get; set; }

        private IList<Device> devices = new List<Device>();

        //Variable de log.
        private static readonly ILog logger = LogManager.GetLogger(typeof(ScanBarCode));

        //Variable que indica si existe un error en la cámara.
        private bool CameraError = false;

        private bool cerrar = false;

        public int Timeout;

        /// <summary>
        /// Define si el chequeo de parámetros cumple con lo solicitado.
        /// </summary>
        public bool Statusparameter { get; set; }

        private byte[] resultAsBinary { get; set; }

        BVIResponse respuesta;

        internal Configuration appConfig;

        /// <summary>
        /// Estructura para almacenar información de obtenido del código de barra
        /// </summary>
        private struct Device
        {
            public int Index;
            public string Name;
            public override string ToString()
            {
                return Name;
            }
        }

        public ZxingScanForm()
        {
            InitializeComponent();

            logger.Info("Se inicializa el formulario ZXingScanForm");
            camDevices = new CameraDevices();

            decodingThread = new Thread(DecodeBarcode);
            decodingThread.Start();
                           
            pictureBox1.Paint += pictureBox1_Paint;
            resultRectPen = new Pen(Color.Green, 10);
        }

        /// <summary>
        /// Cambia el fondo de los controles
        /// </summary>
        /// <param name="hexColor">Color eh formato hex. Ejemplo: #00FF00</param>
        void SetBackgroundForControls(string hexColor)
        {
            if (!String.IsNullOrEmpty(hexColor))
            {
                Color _color = System.Drawing.ColorTranslator.FromHtml(hexColor);
                try
                {
                    labBordeLogo.BackColor = _color;
                    labTitle.BackColor = _color;
                    labTitleRut.BackColor = _color;
                    lblRut.BackColor = _color;
                    btnSalir.BackColor = _color;
                }
                catch (Exception ex)
                {
                    logger.Error("ZxingScanForm.SetBackgroundForControls Error", ex);
                }
                logger.Info("Se modificarán los controles de acuerdo al color configurado");
            }
        }

        private void ZxingScanForm_Load(object sender, EventArgs e)
        {
            int ret = 0;
            lblMessage.Visible = false;
            
            appConfig = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);

            try
            {
                if (File.Exists(appConfig.AppSettings.Settings["Logo"].Value))
                {
                    //Bitmap bitmapImage = (Bitmap)System.Drawing.Image.FromFile(ConfigurationManager.AppSettings["Logo"]);
                    Bitmap bitmapImage = (Bitmap)System.Drawing.Image.FromFile(appConfig.AppSettings.Settings["Logo"].Value);
                    pictureBox2.Image = bitmapImage;

                    //pictureBox1.Width = 661;
                    //pictureBox1.Height = 444;
                    //pictureBox1.Left = 0;
                    //pictureBox1.Top = 83;

                }
                SetBackgroundForControls(appConfig.AppSettings.Settings["Color"].Value);


            }
            catch (Exception ex)
            {
                logger.Info("ZxingScanForm_Load Error cargando logo personalizado [" + ex.Message + "]");
            }

            ret = CheckParameters(Packet);
            if (Convert.ToBoolean(appConfig.AppSettings.Settings["WebCamActive"].Value) == false)
            {
                if (ret == 0)
                {

                    logger.Info("Biometrika BVICam..Se abre la aplicación con soporte Serial");
                    MainWindow objMain = new MainWindow(solicitud);
                    objMain.bioPacketLocal = this.bioPacketLocal;
                    objMain.barcodeWebCam = Barcodewebcam;
                    objMain.ShowDialog();
                    this.Close();
                }
                else
                {
                    logger.Error("Biometrika BVICam..No vienen los parámetros necesarios");
                }

            }
            else
            {
                if (ret == 0)
                {
                    
                    InitCamera();
                    logger.Info("Biometrika BVICam.. previo a inicia la cámara");
                    
                    lblRut.Text = dictParamIn["VALUEID"];
                }
                else
                {
                    Statusparameter = false;
                    //StopDecoding();
                }
            }
        }
        /// <summary>
        /// Rutina que inicia la cámara con Aforge.
        /// </summary>
        private void InitCamera()
        {
            int indexselected = -1;

            try
            {
                logger.Info("Biometrika BviCAM..Se realiza el Inicio de la Cámara");
                
               
                //MessageBox.Show(dllConfigData);
                //Recorremos el arreglo de cámaras
                logger.Info("Cámara a buscar:" + appConfig.AppSettings.Settings["CAMCameraSelected"].Value);
                for (var index = 0; index < camDevices.Devices.Count; index++)
                {
                    
                    if (camDevices.Devices[index].Name == appConfig.AppSettings.Settings["CAMCameraSelected"].Value)
                    {
                        logger.Info("Se seleccionó la cámara:" + appConfig.AppSettings.Settings["CAMCameraSelected"].Value);
                        indexselected = index;
                        logger.Info("Número de cámara seleccionada:" + index);
                    }
                    Device deviceaux = new Device();
                    deviceaux.Index = index;
                    deviceaux.Name = camDevices.Devices[index].Name;
                    devices.Add(deviceaux);
                    logger.Info("Nombre de cámara encontrado:" + deviceaux.Name);

                    
                }
                if(indexselected<-1)
                {
                    logger.Error("Biometrika BviCAM.. no pudo subir la cámara");

                    CameraError = true;
                    lblMessage.ForeColor = Color.Red;
                    lblMessage.Text = "No se ha inicialiado la cámara";
                    lblMessage.Visible = true;
                }
                else
                { 
                    camDevices.SelectCamera(indexselected);
                    if (camDevices.Current != null)
                    {
                        camDevices.Current.NewFrame -= Current_NewFrame;
                        if (camDevices.Current.IsRunning)
                        {
                            camDevices.Current.SignalToStop();
                        }
                        camDevices.SelectCamera(devices[indexselected].Index);
                        camDevices.Current.NewFrame += Current_NewFrame;
                        camDevices.Current.Start();
                        logger.Info("Biometrika BviCAM..Se  termina el Inicio de la Cámara");
                    }
                    else
                    {
                        logger.Error("Biometrika BviCAM.. no pudo subir la cámara");
                    
                        CameraError = true;
                        lblMessage.ForeColor = Color.Red;
                        lblMessage.Text = "No se ha inicialiado la cámara";
                        lblMessage.Visible = true;
                    

                    }
                }
            }
            catch (Exception ex)
            {                
                logger.Error("Biometrika BviCAM..Se produjo una excepción al iniciar la cámara:" + ex.Message, ex);
                CameraError = false;
            }
        }


        internal int Channel(BioPacket bioPacket)
        {
            int ret = 0;
            logger.Info("Biometrika BVICam In...");
            bioPacketLocal = bioPacket;

            //ret = CheckParameters(bioPacketLocal);

            if (ret == 0)
            {
                logger.Info("Biometrika BviCam Out - Parámetros configurados correctamente");
                if (bioPacketLocal.PacketParamOut == null)
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            }
            logger.Info("Biometrika BviCam Out [ret=" + ret + "]...");
            return ret;
        }
        /// <summary>
        /// Construimos la salida de la solicitud que será devuelta al usuario final.
        /// </summary>
        /// <returns></returns>
        private Solicitud BuildSolicitud()
        {
            try
            {
                solicitud = new Solicitud();
                solicitud.Persona = new Persona();
                logger.Info("Biometrika BVICAM.. se construye el objeto persona");
                solicitud.Persona.Rut = dictParamIn["VALUEID"];
               
                logger.Info(dictParamIn["VALUEID"]);
                solicitud.Configuracion = new BiometrikaComponentServiceDomain.Configuracion();
                solicitud.Configuracion.ExtraerDatosPersonales = Convert.ToBoolean(appConfig.AppSettings.Settings["ExtraerDatosPersonales"].Value);
                solicitud.Configuracion.ExtraerFirma = Convert.ToBoolean(appConfig.AppSettings.Settings["ExtraerFirma"].Value);
                solicitud.Configuracion.extraerFoto = Convert.ToBoolean(appConfig.AppSettings.Settings["ExtraerFoto"].Value);
                solicitud.Configuracion.bloqueoCedula = Convert.ToBoolean(appConfig.AppSettings.Settings["BloquearDocumentoVencido"].Value);
                solicitud.Configuracion.iso = Convert.ToBoolean(appConfig.AppSettings.Settings["iso"].Value);
                solicitud.Configuracion.ansi = Convert.ToBoolean(appConfig.AppSettings.Settings["ansi"].Value);
                solicitud.Configuracion.raw = Convert.ToBoolean(appConfig.AppSettings.Settings["raw"].Value);
                solicitud.Configuracion.vigenciaCedula = Convert.ToBoolean(appConfig.AppSettings.Settings["vigenciaCedula"].Value);
                solicitud.Configuracion.wsq = Convert.ToBoolean(appConfig.AppSettings.Settings["wsq"].Value);
                solicitud.Configuracion.jpeg = Convert.ToBoolean(appConfig.AppSettings.Settings["jpeg"].Value);
            }
            catch (Exception ex)
            {
                logger.Error("Biometrika BviCAM..Error al construir la solicitud:" + ex.Message);
                logger.Error("Biometrika BviCAM..stacktrace:" + ex.StackTrace);
            }
            logger.Info("Biometrika BviCAM..Termine de construir la solicitud");
            return solicitud;

        }
        public void SetPacket(NameValueCollection queryString)
        {
            Packet = new BioPacket();
            Packet.PacketParamIn = queryString;
            Packet.PacketParamOut = new Dictionary<string, object>();
            bioPacketLocal = Packet;
        }

        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        public int CheckParameters(BioPacket bioPacketLocal)
        {

            int ret = 0;
            try
            {
                logger.Info("Biometrika BviCAM..CheckParameters In...");
                if (bioPacketLocal == null) ret = -2;  //Lista de parametros no puede ser nulo

                logger.Debug("Biometrika BviCAM...CheckParameters filling dictParamin...");
                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    logger.Debug("Biometrika BVICAM.CheckParameters     Adding => " + k + "=" + (string)bioPacketLocal.PacketParamIn[k] + "...");
                    dictParamIn.Add(k.ToUpper(), bioPacketLocal.PacketParamIn[k]);

                }
                logger.Debug("Biometrika BVICAM.CheckParameters dictParamin filled [length = " + dictParamIn.Count + "]");
                if ((!dictParamIn.ContainsKey("SRC") || String.IsNullOrEmpty(dictParamIn["SRC"]))
                    && ((!dictParamIn.ContainsKey("VALUEID") || String.IsNullOrEmpty(dictParamIn["VALUEID"]))))
                {
                    ret = -3;  //src/TypeId/ValufeId deben ser enviados como parámetros
                    logger.Debug("Biometrika BviCAM..CheckParameters src deben ser enviados como parámetros!");
                }
                else
                {

                    logger.Info("Biometrika BVICAM ..Seteamos el rut definido");


                    logger.Info("Biometrika BVICAM ..Definimos si viene el parámetro solicitud");
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    try
                    {

                        if (dictParamIn["SOLICITUD"] != null)
                        {
                            solicitud = jsonSerializer.Deserialize<Solicitud>(dictParamIn["SOLICITUD"]);
                            if (solicitud != null)
                            {
                                logger.Info("Biometrika BVICAM ..La solicitud viene de acuerdo a los parámetros solicitados");

                            }
                            else
                            {

                                logger.Error("Biometrika BVICAM .. se asume una verificación por defecto con los mínimos de valores configurados en parámetros");
                                solicitud = BuildSolicitud();
                            }
                        }
                        else
                        {
                            logger.Error("Biometrika BVICAM .. se asume una verificación por defecto con los mínimos de valores configurados en parámetros");
                            solicitud = BuildSolicitud();
                        }

                    }
                    catch (Exception exe)
                    {
                        logger.Error("Biometrika BviCAM.. se asume una verificación por defecto con los mínimos de valores configurados en parámetros", exe);
                        logger.Info("Construye la solicitud");
                        solicitud = BuildSolicitud();
                    }
                    ret = 0;

                }
                //bioPacketLocal = bioPacketLocal;

            }
            catch (Exception ex)
            {
                logger.Error("Biometrika BviCAM..Error Biometrika BviCam.CheckParameters [" + ex.Message + "]");
                ret = -1;
            }
            logger.Info("Biometrika BviCAM..CheckParameters Ount [ret=" + ret + "]!");
            return ret;
        }




        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
           
        }
        void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (currentResult == null)
                return;           
        }

       
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            if (!e.Cancel)
            {
                decodingThread.Abort();
                if (camDevices.Current != null)
                {
                    camDevices.Current.NewFrame -= Current_NewFrame;
                    if (camDevices.Current.IsRunning)
                    {
                        camDevices.Current.SignalToStop();
                    }
                }
            }
        }

        
        private void cmbDevice_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void Current_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            if (IsDisposed)
            {
                return;
            }

            try
            {
                if (currentBitmapForDecoding == null)
                {
                    currentBitmapForDecoding = (Bitmap)eventArgs.Frame.Clone();
                }
                if (pictureBox1.Image != null)
                {
                    pictureBox1.Image.Dispose();
                }
                Invoke(new Action<Bitmap>(ShowFrame), eventArgs.Frame.Clone());
            }
            catch (ObjectDisposedException)
            {
                // not sure, why....
            }
        }

        private void ShowFrame(Bitmap frame)
        {
            if (pictureBox1.Width < frame.Width)
            {
                pictureBox1.Width = frame.Width;
            }
            if (pictureBox1.Height < frame.Height)
            {
                pictureBox1.Height = frame.Height;
            }
            pictureBox1.Image = frame;
        }

        /// <summary>
        /// Método que realiza la decofificación de la imagen, usando la librería ZXING.
        /// Importante no cambiar la configuración del encoding, ya que el PDF417 es una imagen binaria.
        /// </summary>
        private void DecodeBarcode()
        {
            var reader = new BarcodeReader();
            while (true)
            {
                if (currentBitmapForDecoding != null)
                {
                    var result = reader.Decode(currentBitmapForDecoding);
                    if (result != null)
                    {
                        if (result.BarcodeFormat.ToString() == "PDF_417" || result.BarcodeFormat.ToString() == "QR_CODE")
                        {

                            logger.Info("Se detectó un código PDF417 o QR");
                            logger.Info("Código de barra leído:" + result.BarcodeFormat.ToString());
                            resultAsBinary = System.Text.Encoding.GetEncoding("ISO8859-1").GetBytes(result.Text);
                            logger.Debug("Contenido código de barra:" + result.Text);

                            Invoke(new Action<Result>(ShowResult), result);



                        }
                    }
                    currentBitmapForDecoding.Dispose();
                    currentBitmapForDecoding = null;
                }
                Thread.Sleep(200);
            }
        }
        /// <summary>
        /// Una vez identificado un código de barra, procedemos a decodificar la información.
        /// </summary>
        /// <param name="result"></param>
        private void ShowResult(Result result)
        {
            String rut = "";
            currentResult = result;
            txtBarcodeFormat.Text = result.BarcodeFormat.ToString();
            txtContent.Text = result.Text;
            Barcodewebcam = new BarcodeWebCam();
            if (result.BarcodeFormat.ToString() == "PDF_417")
            {
                logger.Info("Biometrika BviCAM.. retorna el contenido del PDF417");
            //    logger.Debug("Biometrika BviCAM.. Código PDF:" + barcode.Value);
            //    logger.Debug("Biometrika BviCAM.. Código PDF:" + barcode.Value.Length);
                BarCodeContent = result.Text.Substring(0, 40);

                
                Barcodewebcam.ValueId = result.Text.Substring(0, 9).Replace('\0', ' ').TrimEnd();
                
                Barcodewebcam.LastName = result.Text.Substring(19, 30).Replace('\0', ' ').TrimEnd();
                Barcodewebcam.DocumentNumber = result.Text.Substring(58, 10).Replace('\0', ' ').TrimEnd();
                Barcodewebcam.Pdf417 = true;
                ////Barcodewebcam.FullPdf417= Convert.ToBase64String(_binaryData);
                String _year = result.Text.Substring(52, 2);
                String _month = result.Text.Substring(54, 2);
                String _day = result.Text.Substring(56, 2);
                Barcodewebcam.ExpirationDate = new DateTime(Convert.ToInt32(_year) + 2000, Convert.ToInt32(_month), Convert.ToInt32(_day));
                //String recorte = barcode.Value;
                //int indice = 0;
                //indice = barcode.Value.IndexOf(">TRIAL VERSION EXPIRES 90 DAYS AFTER INSTALLATION<");
                //String total;
                //total = barcode.Value.Substring(0, 420);
                Barcodewebcam._binaryData = resultAsBinary;
                Barcodewebcam.ProcessNec();
            }
            else if (result.BarcodeFormat.ToString() == "QR_CODE")
            {
                logger.Info("Biometrika BviCAM..- retorna el contenido del qr");
                //Barcodewebcam = new BarcodeWebCam();
                Barcodewebcam.Qr = true;
                //BarCodeContent = result.Text.Substring(0, result.Text.Length - largo);
                Barcodewebcam.ValueId = result.Text.Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[0] + result.Text.Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[1];


                Int32 _pos = result.Text.LastIndexOf('=');
                String _mrz = result.Text.Substring(_pos + 1, 24);

                Barcodewebcam.DocumentNumber = _mrz.Substring(0, 9);
                Barcodewebcam.DOB = _mrz.Substring(10, 6);
                Barcodewebcam.DOE = _mrz.Substring(17, 6);

                string _exdate = _mrz.Substring(17, 6);
                string _year = _exdate.Substring(0, 2);
                string _month = _exdate.Substring(2, 2);
                string _day = _exdate.Substring(4, 2);

                Barcodewebcam.ExpirationDate = new DateTime(Convert.ToInt32(_year) + 2000, Convert.ToInt32(_month), Convert.ToInt32(_day));

            }
            
            decodingThread.Abort();
            logger.Info("Se obtiene el rut");
            rut = Barcodewebcam.ValueId;
            if (rut == lblRut.Text)
            { 
                this.Hide();
                int Timeout = 0;
                if (dictParamIn.ContainsKey("TIMEOUT"))
                { 
                    if (dictParamIn["TIMEOUT"] != null)
                    Timeout = Convert.ToInt32(dictParamIn["TIMEOUT"]);
                }
                else
                    Timeout = Convert.ToInt32(appConfig.AppSettings.Settings["Timeout"].Value);
                MainWindow objMain = new MainWindow(solicitud,Timeout);
                objMain.appConfig = this.appConfig;
                objMain.bioPacketLocal = this.bioPacketLocal;
                objMain.barcodeWebCam = Barcodewebcam;
                objMain.ShowDialog();
                ShowBvi = true;
                    //camDevices.Current.Stop();
            }
            else
            {
                logger.Error("Biometrika BviCAM.. no coincide el rut leído");
                //MessageBox.Show("El rut configurado no coincide con el rut leído en la cédula");                            
                BVIResponse bvi = BuildRespuesta(-3000, "El rut no coincide con la lectura");
                SetPackageResponse(-3000, "El rut no coincide con la lectura", bvi);
                
                this.Close();
            }

        }

        public String FormatRutFormateado(String rut)
        {
            return rut.Substring(0, rut.Length - 1) + "-" + rut.Substring(rut.Length - 1);

        }
        /// <summary>
        /// Define el paquete de retorno de datos.
        /// </summary>
        /// <param name="error"></param>
        /// <param name="msg"></param>
        /// <param name="result"></param>
        public void SetPackageResponse(int error, String msg, BVIResponse result)
        {
            try
            {
                Packet.PacketParamOut = null;
                Packet.PacketParamOut = new Dictionary<string, object>();
                logger.Info("Biometrika BVICam.. se arma el mensaje de retorno ");
                Packet.PacketParamOut.Add("Data", result);
            }
            catch (Exception exe)
            {
                MessageBox.Show(exe.Message);
            }
        }
        private BVIResponse BuildRespuesta(int err, String msg)
        {
            respuesta = new BVIResponse();
            respuesta.Persona = new Persona();
            respuesta.ResultadoVerificacion = new ResultadoVerificacion();
            respuesta.ErrorNumber = err;
            respuesta.Mensaje = msg;
            return respuesta;
        }


        private void btnSalir_Click(object sender, EventArgs e)
        {
            logger.Info("Biometrika BviCAM..Se presiona botón cerrar");
            cerrar = true;
            if (CameraError == false)
            {
                logger.Info("Biometrika BviCAM.. se cierra la aplicación");
                BVIResponse bvi = BuildRespuesta((int)WebCam.WebCamError.NotFound, WebCam.GetDescription(WebCam.WebCamError.NotFound));
                SetPackageResponse((int)WebCam.WebCamError.NotFound, WebCam.GetDescription(WebCam.WebCamError.NotFound), bvi);
                this.Close();
            }
            else
            {

                logger.Error("Biometrika BviCAM.. se cierra la aplicación porque la webcam no estaba conectada");
                BVIResponse bvi = BuildRespuesta((int)WebCam.WebCamError.NotFound, WebCam.GetDescription(WebCam.WebCamError.NotFound));
                SetPackageResponse((int)WebCam.WebCamError.NotFound, WebCam.GetDescription(WebCam.WebCamError.NotFound), bvi);
                this.Close();


            }
        }
    }
}
