﻿using Biometrika.BioApi20.BSP;
using Domain;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Forms;

namespace BviCamUIAdapter
{
    //static class Program
    //{
    //    /// <summary>
    //    /// The main entry point for the application.
    //    /// </summary>
    //    [STAThread]
    //    static void Main()
    //    {
    //        Application.EnableVisualStyles();
    //        Application.SetCompatibleTextRenderingDefault(false);
    //        Application.Run(new Form1());
    //    }
    //}
    public static class Program
    {
        public static bool HaveData
        {
            get {
                return bviUI == null ? false : bviUI.HaveData;
            }
        }
        public static BioPacket Packet { get; set; }

        private static BVIUI bviUI;

        private static BSPBiometrika _BSP;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            bviUI = new BVIUI();
            bviUI.SetBSP(_BSP);
            int ret = bviUI.Channel(Packet);
            if (ret == 0)
            {
                //bviUI.ShowCapture(null);
                Application.Run(bviUI);
            }
            else
            {
                Packet.PacketParamOut.Add("Error", GetError(ret));
            }
        }

        public static void SetBSP(BSPBiometrika bsp)
        {
            try
            {
                _BSP = bsp;
            }
            catch (Exception ex)
            {
                Packet.PacketParamOut.Add("Error", GetError(-4));
            }
        }

        private static string GetError(int ret)
        {
            string sret = null;
            switch (ret)
            {
                case -1:
                    sret = "-1|Error desconocido. Ver log";
                    break;
                case -2:
                    sret = "-2|Parametros nulos";
                    break;
                case -3:
                    sret = "-3|Parametros src/TypeId/ValueId denben existir y no pueden ser nulos";
                    break;
                case -4:
                    sret = "-4|Error seteando BSP";
                    break;
                default:
                    sret = "-1|Error desconocido. Ver log";
                    break;
            }
            return sret;
        }

        public static void SetPacket(NameValueCollection queryString)
        {
            Packet = new BioPacket();
            Packet.PacketParamIn = queryString;
            Packet.PacketParamOut = new Dictionary<string, object>();
        }

        public static void Close()
        {
            //bioPortalClient7UI.Close();
            //bioPortalClient7UI = null;
        }
    }
}