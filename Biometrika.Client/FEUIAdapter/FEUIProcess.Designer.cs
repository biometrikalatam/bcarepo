﻿namespace FEUIAdapter
{
    partial class FEUIProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ctlProgress = new System.Windows.Forms.ProgressBar();
            this.labMsg = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ctlProgress
            // 
            this.ctlProgress.Location = new System.Drawing.Point(79, 74);
            this.ctlProgress.Name = "ctlProgress";
            this.ctlProgress.Size = new System.Drawing.Size(441, 23);
            this.ctlProgress.TabIndex = 0;
            // 
            // labMsg
            // 
            this.labMsg.Location = new System.Drawing.Point(70, 126);
            this.labMsg.Name = "labMsg";
            this.labMsg.Size = new System.Drawing.Size(450, 70);
            this.labMsg.TabIndex = 1;
            this.labMsg.Text = "label1";
            // 
            // FEUIProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 219);
            this.Controls.Add(this.labMsg);
            this.Controls.Add(this.ctlProgress);
            this.Name = "FEUIProcess";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FEUIProcess";
            this.Load += new System.EventHandler(this.FEUIProcess_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar ctlProgress;
        private System.Windows.Forms.Label labMsg;
    }
}