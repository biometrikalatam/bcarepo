﻿namespace FEUIAdapter
{
    partial class frmPlay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPlay));
            this.playCtrl1 = new Leadtools.Multimedia.PlayCtrl();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.labPlay = new System.Windows.Forms.Label();
            this.labPause = new System.Windows.Forms.Label();
            this.labSalir = new System.Windows.Forms.Label();
            this.labMsg = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.playCtrl1)).BeginInit();
            this.SuspendLayout();
            // 
            // playCtrl1
            // 
            this.playCtrl1.Location = new System.Drawing.Point(92, 105);
            this.playCtrl1.Name = "playCtrl1";
            this.playCtrl1.OcxState = null;
            this.playCtrl1.Size = new System.Drawing.Size(351, 229);
            this.playCtrl1.SourceFile = null;
            this.playCtrl1.TabIndex = 0;
            this.playCtrl1.Text = "playCtrl1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(51, 379);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(142, 391);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // labPlay
            // 
            this.labPlay.BackColor = System.Drawing.Color.Transparent;
            this.labPlay.Location = new System.Drawing.Point(93, 337);
            this.labPlay.Name = "labPlay";
            this.labPlay.Size = new System.Drawing.Size(19, 18);
            this.labPlay.TabIndex = 3;
            this.labPlay.Click += new System.EventHandler(this.labPlay_Click);
            // 
            // labPause
            // 
            this.labPause.BackColor = System.Drawing.Color.Transparent;
            this.labPause.Location = new System.Drawing.Point(113, 337);
            this.labPause.Name = "labPause";
            this.labPause.Size = new System.Drawing.Size(19, 18);
            this.labPause.TabIndex = 4;
            this.labPause.Click += new System.EventHandler(this.labPause_Click);
            // 
            // labSalir
            // 
            this.labSalir.BackColor = System.Drawing.Color.Transparent;
            this.labSalir.Location = new System.Drawing.Point(460, 53);
            this.labSalir.Name = "labSalir";
            this.labSalir.Size = new System.Drawing.Size(26, 28);
            this.labSalir.TabIndex = 5;
            this.labSalir.Click += new System.EventHandler(this.labSalir_Click);
            // 
            // labMsg
            // 
            this.labMsg.BackColor = System.Drawing.Color.Transparent;
            this.labMsg.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMsg.ForeColor = System.Drawing.Color.DimGray;
            this.labMsg.Location = new System.Drawing.Point(201, 54);
            this.labMsg.Name = "labMsg";
            this.labMsg.Size = new System.Drawing.Size(256, 22);
            this.labMsg.TabIndex = 21;
            this.labMsg.Text = "Revisando Grabación...";
            this.labMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmPlay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FEUIAdapter.Properties.Resources.FE3D_Fondo_Play_v11;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(537, 466);
            this.Controls.Add(this.labMsg);
            this.Controls.Add(this.labSalir);
            this.Controls.Add(this.labPause);
            this.Controls.Add(this.labPlay);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.playCtrl1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPlay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmPlay";
            this.Load += new System.EventHandler(this.frmPlay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.playCtrl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Leadtools.Multimedia.PlayCtrl playCtrl1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label labPlay;
        private System.Windows.Forms.Label labPause;
        private System.Windows.Forms.Label labSalir;
        private System.Windows.Forms.Label labMsg;
    }
}