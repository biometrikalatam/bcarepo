﻿using Domain;
//using Emgu.CV;
//using Emgu.CV.Structure;
using Leadtools.Multimedia;
using LMVIOvLyLib;
using LMVTextOverlayLib;
using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace FEUIAdapter
{
    public partial class FEUI : Form
    {
        [DllImport("kernel32.dll", EntryPoint = "SetProcessWorkingSetSize", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        private static extern int SetProcessWorkingSetSize(IntPtr process, int minimumWorkingSetSize, int maximumWorkingSetSize);

        //UnlockSupport unlocksprt = new UnlockSupport();
        internal static object objLock = new object();

        private static readonly ILog LOG = LogManager.GetLogger(typeof(FEUI));
        internal ConvertCtrl ConvertCtrl1; // = new ConvertCtrl();
        internal PlayCtrl playCtrl1; // = new ConvertCtrl();

        internal int _CurrentError = 0;

        internal bool _recording;
        internal bool _hayImage = false;
        internal bool _hayVideo = false;
        internal bool _notarizing = false;
        internal int _CurrentStep = 0;
        public ErrorCode ConversionResult;
        internal bool Converting = false;
        internal string _tmpDirectory;
        //private Capture captureCam;

        internal string _LastError = null;
        internal string DeviceId { get; set; }
        internal string GeoRef { get; set; }
        public string DocumentId { get; set; }
        public string Mail { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string Company { get; set; }


        public string RDEnvelope { get; set; }
        public bool HaveData { get; set; }
        public static BioPacket Packet { get; set; }
        private BioPacket bioPacketLocal;

        private RUT RUT;

        private string json;
        private int timeLeft;
        private System.Timers.Timer timer;
        private System.Timers.Timer timerControlGeneral;

        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();

        public FEUI()
        {
            InitializeComponent();

            System.Drawing.Drawing2D.GraphicsPath objDraw = new System.Drawing.Drawing2D.GraphicsPath();
            objDraw.AddEllipse(0, 0, this.imgCapture.Width, this.imgCapture.Height);
            this.imgCapture.Region = new Region(objDraw);
        }

        private void FEUI_Load(object sender, EventArgs e)
        {

            //UnlockSupport unlocksprt = new UnlockSupport();
            //unlocksprt.Unlock(null);

            CheckForIllegalCrossThreadCalls = false;

            CheckTmpDirectory();

            GetDeviceId();

            GetGeoRef();

            try
            {
                if (!dictParamIn["THEME"].Equals("Default")) //Si no es fondo default => Cargo fondo 
                {
                    this.BackgroundImage = Image.FromFile(dictParamIn["THEME"]);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("FEUI.FEUI_Load Error cargando fondo = " + dictParamIn["THEME"]);
            }

            labMsg.Text = "Firmante >> " + dictParamIn["TYPEID"] + " " + dictParamIn["VALUEID"];
            labMsgToRead.Text = dictParamIn["MSGTOREAD"];

            try
            {
                picLogo3ro.Image = Image.FromFile(Properties.Settings.Default.FELogo3ro);
                picLogo3ro.Visible = true;
            }
            catch (Exception ex)
            {
                LOG.Error("FEUI.FEUI_Load Error Cargando logo externo...");
                picLogo3ro.Visible = false;
            }

            timerToSetCaptureOnLoad.Enabled = true;
        }

        private void GetGeoRef()
        {
            try
            {
                GeoRef = Properties.Settings.Default.FEGeoRef;
            }
            catch (Exception ex)
            {
                LOG.Error("FEUI.GetDeviceId Error [" + ex.Message + "]");
            }
        }

        private void GetDeviceId()
        {
            try
            {
                DeviceId = GetMACAddress();
            }
            catch (Exception ex)
            {
                LOG.Error("FEUI.GetDeviceId Error [" + ex.Message + "]");
            }
        }

        public string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card
                {
                    if (adapter.OperationalStatus == OperationalStatus.Up && (!adapter.Description.Contains("Virtual")
                        && !adapter.Description.Contains("Pseudo")))
                    {
                        if (adapter.GetPhysicalAddress().ToString() != "")
                        {
                            IPInterfaceProperties properties = adapter.GetIPProperties();
                            sMacAddress = adapter.GetPhysicalAddress().ToString();
                        }
                    }
                }
            }
            if (sMacAddress == string.Empty) sMacAddress = Properties.Settings.Default.FEDeviceId;
            else
            {
                string sAux = sMacAddress.Substring(0, 2);
                int i = 2;
                while (i < sMacAddress.Length)
                {
                    sAux = sAux + "-" + sMacAddress.Substring(1, 2);
                    i = i + 2; 
                }
            }
            return sMacAddress;
        }

        /// <summary>
        /// Chequea si existe el directorio de proceso temporal si FETargetVideoType = 0, sino, si es 1, 
        /// es Stream el destino.
        /// </summary>
        private void CheckTmpDirectory()
        {
            try
            {
                if (Properties.Settings.Default.FETargetVideoType == 0)
                {
                    if (!Directory.Exists(Properties.Settings.Default.FETargetVideoDirTemp))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(Properties.Settings.Default.FETargetVideoDirTemp);
                        if (di != null)
                        {
                            LOG.Info("FEUI.CheckTmpDirectory Se creo directorio temporal " + Properties.Settings.Default.FETargetVideoDirTemp);
                        } else
                        {
                            LOG.Warn("FEUI.CheckTmpDirectory Atencion!! Probelmas en la creación del directorio temporal " + Properties.Settings.Default.FETargetVideoDirTemp);
                        }
                    } else
                    {
                        LOG.Info("FEUI.CheckTmpDirectory Directorio temporal OK - " + Properties.Settings.Default.FETargetVideoDirTemp);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("FEUI.CheckTmpDirectory Error [" + ex.Message + "]");
            }
        }

        private void timerToSetCaptureOnLoad_Tick(object sender, EventArgs e)
        {
            timerToSetCaptureOnLoad.Enabled = false;
            CaptureCtrl1.EnterEdit();
            SetDefaultFormat();
            CaptureCtrl1.LeaveEdit();

            UpdateWizardStatus(0, 0);
        }

        private void UpdateWizardStatus(int step, int resultback)
        {
            switch (step)
            {
                case 0: //Init Component
                    if (resultback == 0) //Paso anterior OK => Seteo componente de captura.
                    {
                        picStep0.Image = Properties.Resources.capturar_video_celeste;
                        _CurrentStep++;
                    } else
                    {
                        picStep0.Image = Properties.Resources.capturar_video_rojo;
                    }
                    break;
                case 1: //Grabar video
                    if (resultback == 0) //Paso anterior OK => Seteo compoentne de captura.
                    {
                        picStep0.Image = Properties.Resources.capturar_video_verde;
                        _hayImage = true;
                        labStep1Aux.Visible = false;
                        picStep1.Image = Properties.Resources.grabar_video_celeste;
                        _CurrentStep++;
                    }
                    else
                    {
                        picStep0.Image = Properties.Resources.capturar_video_rojo;
                    }
                    break;
                case 2: //Comprimir Video
                    if (resultback == 0) //Paso anterior OK => Seteo notarizacion.
                    {
                        picStep1.Image = Properties.Resources.grabar_video_verde;
                        _hayVideo = true;
                        picStep2.Image = Properties.Resources.procesar_video_celeste;
                        labStep1Aux.Visible = false;
                        ctlProgress.Visible = false;
                        _CurrentStep++;
                    }
                    else
                    {
                        picStep1.Image = Properties.Resources.capturar_video_rojo;
                    }
                    break;
                case 3: //Notarizar
                    if (resultback == 0) //Paso anterior OK => Seteo notarizacion.
                    {
                        picStep2.Image = Properties.Resources.procesar_video_verde;
                        picStep3.Image = Properties.Resources.notarizar_celeste;
                        labStep2Aux.Visible = false;
                        ctlProgress.Visible = false;
                        _CurrentStep++;
                        //if (Properties.Settings.Default.FEStep4Automatic)
                        //{
                        //    timerNotarizar.Enabled = true;
                        //}
                    }
                    else
                    {
                        picStep2.Image = Properties.Resources.procesar_video_rojo;
                    }
                    //labStep3.Enabled = false;
                    break;
                case 4: //Salir
                    if (resultback == 0) //Paso anterior OK => Seteo notarizacion.
                    {
                        picStep3.Image = Properties.Resources.notarizar_verde;
                        picStep4.Image = Properties.Resources.salir_verde;
                        _notarizing = false;
                        //labStep3.ForeColor = Color.YellowGreen;
                        //picStep3OK.Visible = true;
                        labStep3Aux.Visible = false;
                        //labStep4.ForeColor = Color.YellowGreen;
                        //picStep4OK.Visible = true;
                        //labStep4.Enabled = false;
                        _CurrentStep++;
                        if (Properties.Settings.Default.FEStep4Automatic)
                        {
                            timerNotarizar.Enabled = true;
                        }
                    }
                    else
                    {
                        picStep3.Image = Properties.Resources.notarizar_rojo;
                    }
                    //labStep3.Enabled = false;
                    break;
                default:
                    break;
            }
            Refresh();
        }

        private void ResetWizardStatus()
        {
            _CurrentStep = 0;
            //labStep1.ForeColor = Color.DarkGray;
            //labStep2.ForeColor = Color.DarkGray;
            //labStep3.ForeColor = Color.DarkGray;
            //labStep4.ForeColor = Color.DarkGray;
            //picStep1OK.Visible = false;
            //picStep2OK.Visible = false;
            //picStep3OK.Visible = false;
            //picStep4OK.Visible = false;
        }



        internal int Channel(BioPacket bioPacket)
        {
            int ret = 0;
            LOG.Info("FEUI.Channel In...");
            bioPacketLocal = bioPacket;

            ret = CheckParameters(bioPacketLocal);

            if (ret == 0)
            {
                if (bioPacketLocal != null &&  bioPacketLocal.PacketParamOut == null)
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            }
            LOG.Info("FEUI.Channel Out [ret=" + ret + "]...");
            return ret;
        }

       
        private int CheckParameters(BioPacket bioPacketLocal)
        {
            /*
                 src=FEM&
                 typeId=RUT&
                 valueId=21284415-2&
                 documentId=123456NN&
                 msg=Yo, Gustavo Suhit, RUT 21284415-2, acepoto contrato 123456NN
                 
            */

            //return 0;

            int ret = 0;
            try
            {
                LOG.Info("FEUI.CheckParameters In...");
                if (bioPacketLocal == null) ret = -2;  //Lista de parametros no puede ser nulo

                //Paso a Dictionary

                LOG.Debug("FEUI.CheckParameters filling dictParamin...");
                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    LOG.Debug("FEUI.CheckParameters     Adding => " + k + "=" + (string)bioPacketLocal.PacketParamIn[k] + "...");
                    dictParamIn.Add(k.ToUpper(), bioPacketLocal.PacketParamIn[k]);
                }
                LOG.Debug("FEUI.CheckParameters dictParamin filled [length = " + dictParamIn.Count + "]");

                //if (!dictParamIn.ContainsKey("SRC") || String.IsNullOrEmpty(dictParamIn["src"]) ||
                //    !dictParamIn.ContainsKey("TYPEID") || String.IsNullOrEmpty(dictParamIn["TypeId"]) ||
                //    !dictParamIn.ContainsKey("VALUEID") || String.IsNullOrEmpty(dictParamIn["ValueId"]) ||
                //    !dictParamIn.ContainsKey("MAIL") || String.IsNullOrEmpty(dictParamIn["Mail"]) ||
                //    !dictParamIn.ContainsKey("DOCUMENTID") || String.IsNullOrEmpty(dictParamIn["DocumentId"]))
                    if (!dictParamIn.ContainsKey("SRC") ||
                    !dictParamIn.ContainsKey("TYPEID") || !dictParamIn.ContainsKey("VALUEID") || 
                    !dictParamIn.ContainsKey("MAIL") || !dictParamIn.ContainsKey("DOCUMENTID"))
                {
                    ret = -3;  //src/TypeId/ValueId deben ser enviados como parámetros
                    LOG.Debug("FEUI.CheckParameters src/TypeId/ValueId/DocumentId/Mail deben ser enviados como parámetros!");
                }
                else
                {
                    TypeId = dictParamIn["TYPEID"];
                    ValueId = dictParamIn["VALUEID"];
                    Mail = dictParamIn["MAIL"];
                    DocumentId = dictParamIn["DOCUMENTID"];
                    LOG.Debug("FEUI.CheckParameters evaluando cada parámetro...");
                    if (!dictParamIn.ContainsKey("MSGTOREAD") || String.IsNullOrEmpty(dictParamIn["MSGTOREAD"]))
                    {
                        if (!dictParamIn.ContainsKey("MSGTOREAD"))
                        {
                            dictParamIn.Add("MSGTOREAD", "Acepto las Condiciones");
                        }
                        else
                        {
                            dictParamIn["MSGTOREAD"] = "Acepto las Condiciones";  //Ambos Minucia + WSQ
                        }
                    }
                    LOG.Debug("FEUI.CheckParameters MsgToRead = " + dictParamIn["MSGTOREAD"]);
                    if (!dictParamIn.ContainsKey("THEME") || String.IsNullOrEmpty(dictParamIn["THEME"]))
                    {
                        if (!dictParamIn.ContainsKey("THEME"))
                        {
                            dictParamIn.Add("THEME", "Default");
                        }
                        else
                        {
                            dictParamIn["THEME"] = "Default";
                        }
                    }
                    LOG.Debug("FEUI.CheckParameters Theme = " + dictParamIn["THEME"]);
                    if (!dictParamIn.ContainsKey("COMPANY") || String.IsNullOrEmpty(dictParamIn["COMPANY"]))
                    {
                        if (!dictParamIn.ContainsKey("COMPANY"))
                        {
                            dictParamIn.Add("COMPANY", "");
                        }
                    }
                    Company = dictParamIn["COMPANY"];
                    LOG.Debug("FEUI.CheckParameters Company = " + dictParamIn["COMPANY"]);
                    //if (!dictParamIn.ContainsKey("Hash") || String.IsNullOrEmpty(dictParamIn["Hash"]))
                    //{
                    //    if (!dictParamIn.ContainsKey("Hash"))
                    //    {
                    //        dictParamIn.Add("Hash", Guid.NewGuid().ToString("N"));
                    //    }
                    //    else
                    //    {
                    //        dictParamIn["Hash"] = Guid.NewGuid().ToString("N");
                    //    }
                    //}
                    //LOG.Debug("FEUI.CheckParameters Hash = " + dictParamIn["Hash"]);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Error FEUI.CheckParameters [" + ex.Message + "]");
                ret = -1;
            }
            LOG.Info("FEUI.CheckParameters Ount [ret=" + ret + "]!");
            return ret;
        }

        private void labContorno_Click(object sender, EventArgs e)
        {

        }

       

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            if (bioPacketLocal.PacketParamOut == null)
                return; // throw new Exception("Este proceso se debe invocar asi el packet esta inicializado. USar el test o invocar");
            try
            { 
                //Convert video
                try
                {
                    Converting = true;
                    ConvertCtrl1.SourceFile = CaptureCtrl1.TargetFile;
                    ConvertCtrl1.TargetFile = CaptureCtrl1.TargetFile + ".compressed";
                    ConversionResult = ErrorCode.S_OK;
                    ConvertCtrl1.ErrorAbort += this.ConvertCtrl1_ErrorAbort;
                    ConvertCtrl1.StartConvert();
                }
                catch (System.Exception except)
                {
                    MessageBox.Show(except.Message);
                    
                }
            }
            catch (Exception ex)
            {
                bioPacketLocal.PacketParamOut.Add("Video", null);
                bioPacketLocal.PacketParamOut.Add("Photo", null);
                bioPacketLocal.PacketParamOut.Add("Error", "-2|Error devolviendo imagen: " + ex.Message);
            }

            //StopCamStream();
            //HaveData = true;
            //this.Close();
        }

        private void StopCamStream()
        {
            //Application.Idle -= ShowCamStream;
            //captureCam.Stop();
            //captureCam.Dispose();
        }

        private void ShowCamStream(object sender, EventArgs e)

        {
            //Mat mat = new Mat();
            //captureCam.Retrieve(mat);
            //Image<Bgr, Byte> img = mat.ToImage<Bgr, Byte>();
            
            //Image<Gray, Byte> grayImage = img.Convert<Gray, Byte>();
            //var faces = grayImage.DetectHaarCascade(
            //                                   haar, 1.4, 4,
            //                                   HAAR_DETECTION_TYPE.DO_CANNY_PRUNING,
            //                                   new Size(nextFrame.Width / 8, nextFrame.Height / 8)
            //                                   )[0];

            //imageCam.Image = img.ToBitmap();
        }

        

        #region LeadTools

        private void btnStartVideo_Click(object sender, EventArgs e)
        {
            //captureCam = new Capture();
            //Application.Idle += ShowCamStream;
            DoCapture(CaptureMode.VideoAndAudio);
        }

        private void btnFoto_Click(object sender, EventArgs e)
        {
            //imgCapture.Image = imageCam.Image;
            //imgCapture.Image = CaptureCtrl1.ge
            //CaptureCtrl1.VideoDevices.Selection = 0;
            ////System.Threading.Thread.Sleep(1000);
            //CaptureCtrl1.AudioDevices.Selection = 0;
            ////System.Threading.Thread.Sleep(1000);
            //CaptureCtrl1.Preview = true;
            
            //CaptureCtrl1.PreviewSource = CapturePreview.Video;
            ////captureCtrl2.TargetFormat = TargetFormatType.AVI;
            //CaptureCtrl1.Show();
        }

        private void SetDefaultFormat()
        {
            string streamType = null;
            string bstrFile = null;
            int extensionIndex = 0;

            //CaptureCtrl1 = new CaptureCtrl();
            //CaptureCtrl1.ErrorAbort += this.CaptureCtrl1_ErrorAbort;
            //CaptureCtrl1.Complete += this.CaptureCtrl1_Complete;
            //CaptureCtrl1.Progress += this.CaptureCtrl1_Progress;


            CaptureCtrl1.EnterEdit();
            CaptureCtrl1.VideoDevices.Selection = Properties.Settings.Default.FEVideoDeviceId;
            CaptureCtrl1.AudioDevices.Selection = Properties.Settings.Default.FEAudioDeviceId;

            //System.Threading.Thread.Sleep(1000);
      

            CaptureCtrl1.VideoWindowSizeMode = SizeMode.Stretch;
            CaptureCtrl1.PreviewSource = CapturePreview.Video;
            CaptureCtrl1.FrameRate = System.Convert.ToDouble(Properties.Settings.Default.FEFrameRate);
            CaptureCtrl1.TimeLimit = System.Convert.ToDouble(Properties.Settings.Default.FETimeLimit);
            CaptureCtrl1.UseTimeLimit = true;
            CaptureCtrl1.UseFrameRate = true;

            CaptureCtrl1.PreferredVideoRenderer = VideoRendererType.VMR7;
            CaptureCtrl1.PreviewTap = CapturePreviewTap.Compressor;
            CaptureCtrl1.TargetFormat = Leadtools.Multimedia.TargetFormatType.ISO;
            CaptureCtrl1.VideoCompressors.H264.Selected = true;
            CaptureCtrl1.AudioCompressors.AAC.Selected = true;
            //CaptureCtrl1.PreviewTap = CapturePreviewTap.Processors;
            CaptureCtrl1.Preview = true;
            //CaptureCtrl1.MasterStream = Leadtools.Multimedia.CaptureMasterStream.Video;


            //CaptureCtrl1.VideoWindowSizeMode = SizeMode.Fit;
            //CaptureCtrl1.Preview = true;

            //streamType = CaptureCtrl1.VideoCaptureStreamType;
            //if (streamType.Equals(Leadtools.Multimedia.Constants.MEDIATYPE_Stream, StringComparison.InvariantCultureIgnoreCase))
            //{
            //    bstrFile = CaptureCtrl1.TargetFile;
            //    extensionIndex = (bstrFile.LastIndexOf(".") + 1);
            //    CaptureCtrl1.TargetFile = bstrFile.Remove(extensionIndex) + "mpg";
            //    CaptureCtrl1.TargetFormat = TargetFormatType.Stream;
            //    CaptureCtrl1.AudioCompressors.Selection = -1;
            //    CaptureCtrl1.VideoCompressors.Selection = -1;
            //    CaptureCtrl1.SelectedVideoProcessors.Clear();
            //    CaptureCtrl1.SelectedAudioProcessors.Clear();
            //    //CaptureCtrl1.AudioDevices.Selection = -1;
            //}
            //else
            //{
                string sem = ValueId;
                CaptureCtrl1.TargetFile = Properties.Settings.Default.FETargetVideoDirTemp + (!string.IsNullOrEmpty(sem)?sem:"Video") + "_FE3d_tmp.mp4";
                //bstrFile = CaptureCtrl1.TargetFile;
                //extensionIndex = (bstrFile.LastIndexOf(".") + 1);
                //CaptureCtrl1.TargetFile = bstrFile.Remove(extensionIndex) + "avi";
                //CaptureCtrl1.TargetFormat = TargetFormatType.AVI;
            //}

            //CaptureCtrl1.MasterStream = CaptureMasterStream.Video;
            //CaptureCtrl1.AudioRenderers.Selection = 0;
            //CaptureCtrl1.AudioCompressors.Selection = 15;
            //CaptureCtrl1.AudioCaptureFormats.Selection = 7;

            CaptureCtrl1.LeaveEdit();

            IncludeWaterMark();
        }

        private void IncludeWaterMark()
        {
            try
            {
                CaptureCtrl1.Preview = false;

                //Adding Image OverLay
                CaptureCtrl1.SelectedVideoProcessors.Add(CaptureCtrl1.VideoProcessors.ImageOverlay);
                // get the the image overlay filter object
                LMVIOvLy _ImageOverLay = (LMVIOvLy)CaptureCtrl1.GetSubObject(CaptureObject.SelVideoProcessor);
                _ImageOverLay.OverlayImageFileName = Properties.Settings.Default.FELogoWaterMark; // @".\disclaimer_big.bmp";
                _ImageOverLay.set_XPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);
                _ImageOverLay.set_YPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);

                //Adding Text OverLay
                CaptureCtrl1.SelectedVideoProcessors.Add(CaptureCtrl1.VideoProcessors.TextOverlay);

                // get the the text overlay filter object
                LMVTextOverlay _overlay = (LMVTextOverlay)CaptureCtrl1.GetSubObject(CaptureObject.SelVideoProcessor + 1);
                _overlay.OverlayText = Company;
                _overlay.ViewRectLeft = 0;
                _overlay.ViewRectTop = 0;
                _overlay.ViewRectRight = 800;
                _overlay.ViewRectBottom = 600;
                _overlay.EnableXYPosition = true;
                _overlay.XPos = 80;
                _overlay.YPos = 130;
                _overlay.FontSize = 12;
                _overlay.Bold = true;
                _overlay.AutoReposToViewRect = true;

                CaptureCtrl1.SelectedVideoProcessors.Add(CaptureCtrl1.VideoProcessors.TextOverlay);
                LMVTextOverlay _overlay1 = (LMVTextOverlay)CaptureCtrl1.GetSubObject(CaptureObject.SelVideoProcessor + 2);
                _overlay1.OverlayText = TypeId + " " + ValueId;
                _overlay1.ViewRectLeft = 0;
                _overlay1.ViewRectTop = 0;
                _overlay1.ViewRectRight = 300;
                _overlay1.ViewRectBottom = 600;
                _overlay.WordWrap = true;
                _overlay1.EnableXYPosition = true;
                _overlay1.XPos = 80;
                _overlay1.YPos = 170;
                _overlay1.FontSize = 10;
                //_overlay1.FontColor = 16777215;
                _overlay1.AutoReposToViewRect = true;

                CaptureCtrl1.SelectedVideoProcessors.Add(CaptureCtrl1.VideoProcessors.TextOverlay);
                LMVTextOverlay _overlay2 = (LMVTextOverlay)CaptureCtrl1.GetSubObject(CaptureObject.SelVideoProcessor + 3);
                _overlay2.OverlayText = DocumentId;
                _overlay2.ViewRectLeft = 0;
                _overlay2.ViewRectTop = 0;
                _overlay2.ViewRectRight = 800;
                _overlay2.ViewRectBottom = 600;
                _overlay2.EnableXYPosition = true;
                _overlay2.XPos = 80;
                _overlay2.YPos = 210;
                _overlay2.FontSize = 10;
                //_overlay1.FontColor = 16777215;
                _overlay2.AutoReposToViewRect = true;

                CaptureCtrl1.Preview = true;
            }
            catch (Exception ex)
            {
                _CurrentError = -15;
                _LastError = "Error ingresando Marca de Agua [" + ex.Message + "]";
                LOG.Error("FEUI.IncludeWaterMark Error " + ex.Message);
            }
        }

        public void DoCapture(CaptureMode nMode)
        {
            try
            {
                CaptureCtrl1.ReadyCapture(nMode);
                labStep1Aux.Visible = true;
                //ctlProgress.Visible = true;
                //ctlProgress.Value = 0;
                _recording = true;
                CaptureCtrl1.StartCapture(nMode);
                return;
            }
            catch (Exception ex)
            {
                _CurrentError = -11;
                _LastError = "Error capturando video [" + ex.Message + "]";
                LOG.Error("FEUI.DoCapture Error [" + ex.Message + "]");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CaptureCtrl1.StopCapture();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CaptureCtrl1.PauseCapture();
        }

        private void CaptureCtrl1_Progress(object sender, ProgressEventArgs e)
        {
            if (_recording) //(_CurrentStep == 1)
            { 
                //Si es stop por fin de video, sino es stop por fin de captura de foto
                WriteCaptureStatus(false);
                int i = e.time;
                //ctlProgress.Value = (i > Properties.Settings.Default.FETimeLimit + 1) ? Properties.Settings.Default.FETimeLimit + 1 : i;
                ctlProgress.Value = (i > ctlProgress.Maximum) ? ctlProgress.Maximum : i;
                
            }
        }

        public void WriteCaptureStatus(bool bCompleted)
        {
            string s = null;
            int lDropped = 0;
            int lNotDropped = 0;
            double Time = 0;
            double fps = 0;

            lDropped = CaptureCtrl1.DroppedFrames;
            lNotDropped = CaptureCtrl1.DeliveredFrames;
            Time = CaptureCtrl1.CaptureTime;

            if (!bCompleted)
            {
                s = "Capturados " + System.Convert.ToString(lNotDropped) + " frames en " + Time.ToString("0.000") + " segundos!";
                labStep1Aux.Text = s;
            }
            else
            {
                fps = (lNotDropped / Time);
                s = "Capturados " + System.Convert.ToString(lNotDropped) + " frames en " + Time.ToString("0.000") + " segundos...";
                labStep1Aux.Text = s;
                LOG.Debug("FEUI.WriteCaptureStatus [" + s + "]");
            }
        }

        private void CaptureCtrl1_Complete(object sender, EventArgs e)
        {
            //_recording = false;
            if (_recording) //_CurrentStep == 1) //Si es stop por fin de video, sino es stop por fin de captura de foto
            {
                _recording = false;
                UpdateWizardStatus(2, 0);
                if (Properties.Settings.Default.FEStep2Automatic)
                {
                    timerStep2.Enabled = true;
                }
            }
        }

        private void CaptureCtrl1_ErrorAbort(object sender, ErrorAbortEventArgs e)
        {
            if (_recording) //if (_CurrentStep == 1)
            { //Si es stop por fin de video, sino es stop por fin de captura de foto
                _recording = false;
                UpdateWizardStatus(2, 1);
                _CurrentError = -11;
                _LastError = "Error capturando video ErrorAbort [" + e.errorMessage + "]";
            } else
            {
                _CurrentError = -10;
                _LastError = "Error capturando foto ErrorAbort [" + e.errorMessage + "]";

            }
            LOG.Error("FEUI.CaptureCtrl1_ErrorAbort [" + _LastError + "]");

            System.Threading.Thread.Sleep(500);
            timerToRestartRecord.Enabled = true;
            //CloseAdapter(0);
        }

        #endregion LeadTools

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            _recording = false;
            CaptureCtrl1.StopCapture();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            DoCapture(CaptureMode.VideoAndAudio);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            CaptureCtrl1.PauseCapture();
        }

        private void picSalir_Click(object sender, EventArgs e)
        {
            CloseAdapter(0);
        }

        private void CloseAdapter(int flag)
        {
            try
            {
                if (bioPacketLocal == null)
                {
                    bioPacketLocal = new BioPacket();
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
                }

                if (_CurrentError != 0 || _CurrentStep < 4)  //Si hay error o no completo todos los datos
                {
                    bioPacketLocal.PacketParamOut.Add("Video", null);
                    bioPacketLocal.PacketParamOut.Add("Photo", null);
                    bioPacketLocal.PacketParamOut.Add("FEM", null);
                    bioPacketLocal.PacketParamOut.Add("DeviceId", DeviceId);
                    bioPacketLocal.PacketParamOut.Add("Error", _CurrentError.ToString() + "|" + _LastError);
                }
                else
                {
                    bioPacketLocal.PacketParamOut.Add("Video", null);
                    bioPacketLocal.PacketParamOut.Add("Photo", null);
                    bioPacketLocal.PacketParamOut.Add("FEM", RDEnvelope);
                    bioPacketLocal.PacketParamOut.Add("DeviceId", DeviceId);
                    bioPacketLocal.PacketParamOut.Add("Error", "0|No Error");
                }
                HaveData = true;
                this.Close();
            }
            catch (Exception ex)
            {
                LOG.Error("FEUI.CloseAdapter Error Closing [" + ex.Message + "]");

            }
           
        }


        private void picAcercade_Click(object sender, EventArgs e)
        {
            frmHelp frmH = new frmHelp();
            frmH.Show(this);
        }

        private void picHelp_Click(object sender, EventArgs e)
        {
            frmHelp frmH = new frmHelp();
            frmH.Show(this);
        }

        private void picDelete_Click(object sender, EventArgs e)
        {
            ResetAdapter();
        }

        private void timerFinalizar_Tick(object sender, EventArgs e)
        {
            
        }

        private void CaptureCtrl1_Click(object sender, EventArgs e)
        {

        }

        private void labStep2_Click(object sender, EventArgs e)
        {
            try
            {
                //ctlProgress.Visible = true;
                //labStep2Aux.Visible = true;
                //Converting = true;
                //ConvertCtrl1 = new ConvertCtrl();
                //ConvertCtrl1.TargetFormat = TargetFormatType.AVI;
                //ConvertCtrl1.AudioCompressors.Selection = 15;
                //ConvertCtrl1.VideoCompressors.Selection = 4;
                //ConvertCtrl1.SourceFile = CaptureCtrl1.TargetFile;
                //ConvertCtrl1.TargetFile = CaptureCtrl1.TargetFile + ".compressed";
                //ConvertCtrl1.ErrorAbort += this.ConvertCtrl1_ErrorAbort;
                //ConvertCtrl1.Complete += this.ConvertCtrl1_Complete;
                //ConvertCtrl1.Progress += this.ConvertCtrl1_Progress;
                //ConvertCtrl1.StartConvert();
            }
            catch (Exception ex)
            {
                LOG.Error("FEUI.labStep2_Click Error StartConvert [" + ex.Message + "]");
                UpdateWizardStatus(3, 1);
            }
        }

        private void ConvertCtrl1_ErrorAbort(object sender, ErrorAbortEventArgs e)
        {
            Converting = false;
            UpdateWizardStatus(3, 1);
            _CurrentError = -12;
            _LastError = "Error procesando video ErrorAbort [" + e.errorMessage + "]";
            LOG.Error(_LastError);
        }

        private void ConvertCtrl1_Progress(object sender, ProgressEventArgs e)
        {
            labStep2Aux.Text = System.Convert.ToString(e.time) + "% complete";
            ctlProgress.Value = e.time;
        }

        private void ConvertCtrl1_Complete(object sender, EventArgs e)
        {
            labStep2Aux.Text = "100% complete";
            ctlProgress.Value = ctlProgress.Maximum;
            System.Threading.Thread.Sleep(500);
            Converting = false;
            UpdateWizardStatus(3, 0);
            if (Properties.Settings.Default.FEStep3Automatic)
            {
                timerNotarizar.Enabled = true;
            }
        }

        private void timerStep2_Tick(object sender, EventArgs e)
        {
            timerStep2.Enabled = false;
            picStep2_Click_New(null, null);
            //labStep2_Click(null, null);
        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {

        }

        private void picStep0_Click(object sender, EventArgs e)
        {
            if (!_hayImage)
            {
                try
                {
                    //get Image and copy it to the clipboard
                    Image imageCaptured = null;
                    CaptureCtrl1.StartCapture(CaptureMode.Still);
                    imageCaptured = CaptureCtrl1.GetStillImage(5000);
                    CaptureCtrl1.StopCapture();
                    Image ImageRsized = Helpers.ImageHelper.Resize(imageCaptured,25); //50);
                    ////Clipboard.SetDataObject(Image1, true);
                    imgCapture.Image = ImageRsized;
                    //imgCapture.Image = imageCaptured;
                    UpdateWizardStatus(1, 0);
                }
                catch (Exception ex)
                {
                    _CurrentError = -10;
                    _LastError = "Error capturando foto [" + ex.Message + "]";
                    LOG.Error("FEUI.picStep0_Click Get Image Error - " + ex.Message);
                    UpdateWizardStatus(1, 1);
                }
            }
            
        }

        private void picStep1_Click(object sender, EventArgs e)
        {
            if (!_recording && _hayImage)
            {
                if (_CurrentStep > 1)  //Deve vovler ara atras seteando lo necesario
                {
                    _CurrentStep = 1;
                    picStep1.Image = Properties.Resources.grabar_video_celeste;
                    picStep2.Image = Properties.Resources.procesar_video_gris;
                    picStep3.Image = Properties.Resources.notarizar_gris;
                    picStep4.Image = Properties.Resources.salir_gris;
                    ctlProgress.Visible = true;
                    ctlProgress.Maximum = Properties.Settings.Default.FETimeLimit * 1000;
                    ctlProgress.Value = 0;
                    RDEnvelope = null;
                    Refresh();
                }
                DoCapture(CaptureMode.VideoAndAudio);
            }
        }

        private void picStep2_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    //System.Diagnostics.Process oProc = new System.Diagnostics.Process();
            //    //oProc.StartInfo = new System.Diagnostics.ProcessStartInfo();
            //    //oProc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //    //oProc.StartInfo.CreateNoWindow = true;
            //    //oProc.StartInfo.UseShellExecute = false;
            //    //oProc.StartInfo.FileName = Application.StartupPath + "\\ConvertTool.exe";
            //    //oProc.StartInfo.Arguments = CaptureCtrl1.TargetFile;
            //    //oProc.Start();
            //    //oProc.WaitForExit();
            //    //oProc.Close();

            //    //FEUIProcess FEUIProc = new FEUIProcess();
            //    //FEUIProc.path = CaptureCtrl1.TargetFile;
            //    //FEUIProc.labFeedback = labStep2Aux;
            //    //FEUIProc.Show(this);

            //    //ctlProgress.Visible = true;
            //    //ctlProgress.Value = 0;
            //    //ctlProgress.Maximum = 100;
            //    //labStep2Aux.Visible = true;
            //    //Converting = true;

            //    ////if (ConvertCtrl1 != null)
            //    ////{
            //    ////    ConvertCtrl1.ErrorAbort -= this.ConvertCtrl1_ErrorAbort;
            //    ////    ConvertCtrl1.Complete -= this.ConvertCtrl1_Complete;
            //    ////    ConvertCtrl1.Progress -= this.ConvertCtrl1_Progress;
            //    ////    ConvertCtrl1.Dispose();
            //    ////    ConvertCtrl1 = null;
            //    ////}

            //    //ConvertCtrl1 = new ConvertCtrl();

            //    //ConvertCtrl1.TargetFormat = Leadtools.Multimedia.TargetFormatType.ISO;
            //    ////ConvertCtrl1.VideoCompressors.H264.Selected = true;
            //    ////ConvertCtrl1.AudioCompressors.AAC.Selected = true;
            //    ////ConvertCtrl1.TargetFormat = TargetFormatType.ISO;
            //    //ConvertCtrl1.AudioCompressors.Selection = Properties.Settings.Default.FEAudioCompressorId;
            //    //ConvertCtrl1.VideoCompressors.Selection = Properties.Settings.Default.FEVideoCompressorId;
            //    //ConvertCtrl1.SourceFile = CaptureCtrl1.TargetFile;
            //    //ConvertCtrl1.TargetFile = CaptureCtrl1.TargetFile + ".compressed";
            //    //ConvertCtrl1.ErrorAbort += this.ConvertCtrl1_ErrorAbort;
            //    //ConvertCtrl1.Complete += this.ConvertCtrl1_Complete;
            //    //ConvertCtrl1.Progress += this.ConvertCtrl1_Progress;
            //    //ConvertCtrl1.StartConvert();

            //    //Converting = false;
            //    //UpdateWizardStatus(3, 0);
            //    //if (Properties.Settings.Default.FEStep3Automatic)
            //    //{
            //    //    timerNotarizar.Enabled = true;
            //    //}
            //}   
            //catch (Exception ex)
            //{
            //    _CurrentError = -12;
            //    _LastError = "Error procesando video [" + ex.Message + "]";
            //    LOG.Error("FEUI.labStep2_Click Error StartConvert [" + ex.Message + "]");
            //    UpdateWizardStatus(3, 1);
            //}
        }

        private void picStep2_Click_New(object sender, EventArgs e)
        {
            try
            {
                Converting = false;
                UpdateWizardStatus(3, 0);
                if (Properties.Settings.Default.FEStep3Automatic)
                {
                    timerNotarizar.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                _CurrentError = -12;
                _LastError = "Error procesando video [" + ex.Message + "]";
                LOG.Error("FEUI.labStep2_Click Error StartConvert [" + ex.Message + "]");
                UpdateWizardStatus(3, 1);
            }
        }

        private void picStep3_Click(object sender, EventArgs e)
        {
            if (_hayVideo)
            {
                try
                {
                    string video = GetVideoInBase64();
                    if (String.IsNullOrEmpty(video))
                    {
                        UpdateWizardStatus(4, 1);
                        return;
                    }
                    _notarizing = true;
                    string image = GetImageInBase64();
                    string typeid = TypeId;
                    string valueid = ValueId;
                    string documentid = DocumentId;
                    string deviceid = DeviceId;
                    string georef = GeoRef;
                    string mail = Mail;
                    string xmlRDEnvelope;
                    string msgerrNV;
                    int ret = Helpers.NVHelper.Notarize(video, image, typeid, valueid, documentid, deviceid, georef, mail,
                                                        this.labStep3Aux, out xmlRDEnvelope, out msgerrNV);
                    if (ret == 0)
                    {
                        RDEnvelope = xmlRDEnvelope;
                        UpdateWizardStatus(4, 0);
                    }
                    else
                    {
                        _CurrentError = -14;
                        _LastError = "Error notarizando [ret=" + ret.ToString() + "-" + msgerrNV + "]";
                        UpdateWizardStatus(4, 1);
                    }
                }
                catch (Exception ex)
                {
                    _CurrentError = -14;
                    _LastError = "Error notarizando [" + ex.Message + "]";
                    LOG.Error("FEUI.labStep3_Click Error Notarize [" + ex.Message + "]");
                    UpdateWizardStatus(4, 1);
                }
            }
        }

        private string GetVideoInBase64()
        {
            string ret = null;
            try
            {
                byte[] byVideo = System.IO.File.ReadAllBytes(CaptureCtrl1.TargetFile); // + ".compressed");
                ret = Convert.ToBase64String(byVideo);
            }
            catch (Exception ex)
            {
                LOG.Error("FEUI.GetImageInBase64 Error [" + ex.Message + "]");
            }
            return ret;
        }

        private string GetImageInBase64()
        {
            string ret = null;
            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    imgCapture.Image.Save(stream, ImageFormat.Bmp);
                    ret = Convert.ToBase64String(stream.ToArray());
                }
            }
            catch (Exception ex)
            {
                LOG.Error("FEUI.GetVideoInBase64 Error [" + ex.Message + "]");
            }
            return ret;
        }

        private void picStep4_Click(object sender, EventArgs e)
        {
            if (!_recording && !_notarizing)
            {
                picSalir_Click(null, null);
            }
        }

        private void timerNotarizar_Tick(object sender, EventArgs e)
        {
            timerNotarizar.Enabled = false;
            picStep3_Click(null, null);
        }


        /// <summary>
        /// Reinicia el proceso desde 0
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labDelete_Click(object sender, EventArgs e)
        {
            ResetAdapter();
        }

        private void ResetAdapter()
        {
            _CurrentError = 0;
            _LastError = null;
            _CurrentStep = 0;
            picStep0.Image = Properties.Resources.capturar_video_celeste;
            picStep1.Image = Properties.Resources.grabar_video_gris;
            picStep2.Image = Properties.Resources.procesar_video_gris;
            picStep3.Image = Properties.Resources.notarizar_gris;
            picStep4.Image = Properties.Resources.salir_gris;
            labStep1Aux.Visible = false;
            labStep2Aux.Visible = false;
            labStep3Aux.Visible = false;
            _hayImage = false;
            _hayVideo = false;
            _recording = false;
            _notarizing = false;
            ctlProgress.Visible = false;
            ctlProgress.Value = 0;
            RDEnvelope = null;
            imgCapture.Image = Properties.Resources.no_picture;
            _hayImage = false;
            DeleteTmpDir();
            timerToSetCaptureOnLoad.Enabled = true;
        }

        private void picDeletePhoto_Click(object sender, EventArgs e)
        {
            ResetAdapter();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            ViewVideoSaved();
        }

        private void ViewVideoSaved()
        {
            try
            {

            }
            catch (Exception)
            {

            }
        }

        private void FEUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            DeleteTmpDir();

            FreeResources();

            //unlocksprt.Lock(null);
        }

        private void FreeResources()
        {
            try
            {
                CaptureCtrl1.Dispose();
                CaptureCtrl1 = null;
                ConvertCtrl1.Dispose();
                ConvertCtrl1 = null;
                playCtrl1.Dispose();
                playCtrl1 = null;

                alzheimer();
            }
            catch (Exception ex)
            {

            }
        }

        private void DeleteTmpDir()
        {
            try
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(Properties.Settings.Default.FETargetVideoDirTemp);
                foreach (FileInfo item in di.GetFiles())
                {
                    System.IO.File.Delete(item.FullName);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("FEUI.DeleteTmpDir Error [" + ex.Message + "]");
            }
        }

        private void labPlayVideo_Click(object sender, EventArgs e)
        {
            try
            {
                frmPlay play = new frmPlay();
                play.path = CaptureCtrl1.TargetFile;
                play.Show(this);
            }
            catch (Exception ex)
            {

            }
        }

        private void picSalir_MouseHover(object sender, EventArgs e)
        {
            labMsgMenu.TextAlign = ContentAlignment.TopRight;
            labMsgMenu.Text = "Salir...";
        }

        private void picSalir_MouseLeave(object sender, EventArgs e)
        {
            labMsgMenu.Text = "";
        }

        private void picAcercade_MouseHover(object sender, EventArgs e)
        {
            labMsgMenu.TextAlign = ContentAlignment.TopCenter;
            labMsgMenu.Text = "Acerca De...";
        }

        private void picAcercade_MouseLeave(object sender, EventArgs e)
        {
            labMsgMenu.Text = "";
        }

        private void picHelp_MouseHover(object sender, EventArgs e)
        {
            labMsgMenu.TextAlign = ContentAlignment.TopCenter;
            labMsgMenu.Text = "Ayuda...";
        }

        private void picDelete_MouseHover(object sender, EventArgs e)
        {
            labMsgMenu.TextAlign = ContentAlignment.TopLeft;    
            labMsgMenu.Text = "Reiniciar...";
        }

        private void picHelp_MouseLeave(object sender, EventArgs e)
        {
            labMsgMenu.Text = "";
        }

        private void picDelete_MouseLeave(object sender, EventArgs e)
        {
            labMsgMenu.Text = "";
        }

        private void timerToRestartRecord_Tick(object sender, EventArgs e)
        {
            timerToRestartRecord.Enabled = false;
            //_CurrentStep = _CurrentStep - 1;
            //UpdateWizardStatus(1, 0);
            CloseAdapter(0);
        }

        public static void alzheimer()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            SetProcessWorkingSetSize(System.Diagnostics.Process.GetCurrentProcess().Handle, -1, -1);
        }

        private void timerStep3_Tick(object sender, EventArgs e)
        {

        }
    }
}
