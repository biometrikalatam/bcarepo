﻿using Leadtools.Multimedia;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FEUIAdapter
{
    public partial class FEUIProcess : Form
    {
        internal ConvertCtrl ConvertCtrl1; // = new ConvertCtrl();
        internal Label labFeedback;
        internal string path;

        public FEUIProcess()
        {
            InitializeComponent();
        }

        private void FEUIProcess_Load(object sender, EventArgs e)
        {
            try
            {

                System.Diagnostics.Process.Start(Application.StartupPath + "\\ConvertTool.exe", path);


                //ctlProgress.Visible = true;
                //ctlProgress.Value = 0;
                //ctlProgress.Maximum = 100;
                //labFeedback.Visible = true;
                ////Converting = true;

                ////if (ConvertCtrl1 != null)
                ////{
                ////    ConvertCtrl1.ErrorAbort -= this.ConvertCtrl1_ErrorAbort;
                ////    ConvertCtrl1.Complete -= this.ConvertCtrl1_Complete;
                ////    ConvertCtrl1.Progress -= this.ConvertCtrl1_Progress;
                ////    ConvertCtrl1.Dispose();
                ////    ConvertCtrl1 = null;
                ////}

                //ConvertCtrl1 = new ConvertCtrl();

                //ConvertCtrl1.TargetFormat = Leadtools.Multimedia.TargetFormatType.ISO;
                //ConvertCtrl1.VideoCompressors.H264.Selected = true;
                //ConvertCtrl1.AudioCompressors.AAC.Selected = true;
                ////ConvertCtrl1.TargetFormat = TargetFormatType.ISO;
                ////ConvertCtrl1.AudioCompressors.Selection = Properties.Settings.Default.FEAudioCompressorId;
                ////ConvertCtrl1.VideoCompressors.Selection = Properties.Settings.Default.FEVideoCompressorId;
                //ConvertCtrl1.SourceFile = path; // CaptureCtrl1.TargetFile;
                //ConvertCtrl1.TargetFile = path + ".compressed"; // CaptureCtrl1.TargetFile + ".compressed";
                //ConvertCtrl1.ErrorAbort += this.ConvertCtrl1_ErrorAbort;
                //ConvertCtrl1.Complete += this.ConvertCtrl1_Complete;
                //ConvertCtrl1.Progress += this.ConvertCtrl1_Progress;
                //ConvertCtrl1.StartConvert();
            }
            catch (Exception ex)
            {
                labMsg.Text = "Error Load - " + ex.Message; 
                //_CurrentError = -12;
                //_LastError = "Error procesando video [" + ex.Message + "]";
                //LOG.Error("FEUI.labStep2_Click Error StartConvert [" + ex.Message + "]");
                //UpdateWizardStatus(3, 1);
            }
        }

        //private void ConvertCtrl1_ErrorAbort(object sender, ErrorAbortEventArgs e)
        //{
        //    //Converting = false;
        //    //UpdateWizardStatus(3, 1);
        //    //_CurrentError = -12;
        //    //_LastError = "Error proceanso video ErrorAbort [" + e.errorMessage + "]";
        //    labMsg.Text = "Error ConvertCtrl1_ErrorAbort - " + e.errorMessage;


        //}

        //private void ConvertCtrl1_Progress(object sender, ProgressEventArgs e)
        //{
        //    labFeedback.Text = System.Convert.ToString(e.time) + "% complete";
        //    ctlProgress.Value = e.time;
        //}

        //private void ConvertCtrl1_Complete(object sender, EventArgs e)
        //{
        //    labFeedback.Text = "100% complete";
        //    ctlProgress.Value = ctlProgress.Maximum;
        //    System.Threading.Thread.Sleep(500);
        //    //Converting = false;
        //    //UpdateWizardStatus(3, 0);
        //    //if (Properties.Settings.Default.FEStep3Automatic)
        //    //{
        //    //    timerNotarizar.Enabled = true;
        //    //}
        //}
    }
}
