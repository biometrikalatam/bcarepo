﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FEUIAdapter
{
    public partial class frmPlay : Form
    {

        internal string path;
        public frmPlay()
        {
            InitializeComponent();
        }

        private void frmPlay_Load(object sender, EventArgs e)
        {
            try
            {
                this.playCtrl1.SourceFile = path;
            }
            catch (Exception)
            {
                this.Close();
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            playCtrl1.Run();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            playCtrl1.Stop();
        }

        private void labPlay_Click(object sender, EventArgs e)
        {
            playCtrl1.Run();
        }

        private void labPause_Click(object sender, EventArgs e)
        {
            playCtrl1.Pause();
        }

        private void labSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
