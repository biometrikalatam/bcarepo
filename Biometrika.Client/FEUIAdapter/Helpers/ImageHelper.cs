﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace FEUIAdapter.Helpers
{
    internal class ImageHelper
    {
        /// <summary>
        /// Escala la imagen capturada al % especificado
        /// </summary>
        /// <param name="foto"> Imagen que se quiere escalar </param>
        /// <param name="TantoPorCiento"> Porcentaje de reducción</param>
        /// <returns>Imagen escalada</returns>
        static internal Image Resize(Image foto, int TantoPorCiento)
        {

            float Tporciento = ((float)TantoPorCiento / 100); // Obtengo el coeficiente de dimension                    
            int ImgOrAncho = foto.Width;
            int ImgOrAlto = foto.Height; // Obtengo las dimensiones originales de la foto

            int OrigX = 0;
            int OrigY = 0;
            int ResX = 0;  // Variables referencia para saber donde empiezo a contar px
            int ResY = 0;

            int ResAncho = (int)(ImgOrAncho * Tporciento);
            int ResAlto = (int)(ImgOrAlto * Tporciento); // Obtengo las dimensiones al % especificado    

            Bitmap RszIm = new Bitmap(ResAncho, ResAlto, PixelFormat.Format24bppRgb); // Creo una imagen con esas dimensiones y bpp
            RszIm.SetResolution(foto.HorizontalResolution, foto.VerticalResolution); // Le doy la misma res. que la original

            Graphics Gfoto = Graphics.FromImage(RszIm); // Creo una intancia de Graphics para manejar la imagen nueva
            Gfoto.InterpolationMode = InterpolationMode.HighQualityBicubic; // Especifico la calidad del algoritmo de sampleo
            // De la foto original, obtengo la redimensionada (mediante un rectángulo)
            Gfoto.DrawImage(foto, new Rectangle(ResX, ResY, ResAncho, ResAlto), new Rectangle(OrigX, OrigY, ImgOrAncho, ImgOrAlto), GraphicsUnit.Pixel);
            Gfoto.Dispose(); // Ya no me hace falta esto, asi que lo descargo

            return (RszIm); // Devuelvo la imagen redimensionada
        }

        /// <summary>
        /// Redimensiona la imagen en pixeles
        /// </summary>
        /// <param name="foto"> Imagen a redimensionar</param>
        /// <param name="ancho">Ancho de la imagen</param>
        /// <param name="alto">Alto de la imagen</param>
        /// <returns>Imagen redimensionada</returns>
        static internal Image Resize(Image foto, int ancho, int alto)
        {

            int ImgORAncho = foto.Width;
            int ImgOrAlto = foto.Height; // Obtengo las dimensiones de la foto

            int OrigX = 0;
            int OrigY = 0;
            int ResX = 0;  // Varables referencia para saber donde contar px
            int ResY = 0;

            float Porciento = 0;
            float PorcientoAncho = 0; // Porcentajes de sampleo
            float PorcientoAlto = 0;

            PorcientoAncho = ((float)ancho / (float)ImgORAncho);
            PorcientoAlto = ((float)alto / (float)ImgOrAlto); //Calculo el % que puedo resamplear

            if (PorcientoAlto < PorcientoAncho)
            {
                Porciento = PorcientoAlto;
            }
            else
            { // Para resamplear bien                
                Porciento = PorcientoAncho;
            }


            int AnchuraFinal = (int)(ImgORAncho * Porciento);
            int AlturaFinal;  // Calculo las nuevas dimensiones                

            if (ancho > alto)
            {
                AlturaFinal = (int)(ImgOrAlto * Porciento);
            }
            else
            {
                AlturaFinal = AnchuraFinal;
            } // Para proporcionar la imagen

            Bitmap RszIm = new Bitmap(ancho, alto, PixelFormat.Format24bppRgb);
            RszIm.SetResolution(foto.HorizontalResolution, foto.VerticalResolution);

            Graphics Gfoto = Graphics.FromImage(RszIm);
            Gfoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
            Gfoto.DrawImage(foto, new Rectangle(ResX, ResY, AnchuraFinal, AlturaFinal), new Rectangle(OrigX, OrigY, ImgORAncho, ImgOrAlto), GraphicsUnit.Pixel);
            Gfoto.Dispose();
            return (RszIm);

        }
    }
}

