﻿using Domain;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FEUIAdapter
{
    //public static class Program
    //{
        ///// <summary>
        ///// The main entry point for the application.
        ///// </summary>
        //[STAThread]
        //static void Main()
        //{
        //    Application.EnableVisualStyles();
        //    Application.SetCompatibleTextRenderingDefault(false);
        //    Application.Run(new FEUI());
        //}
        public static class Program
        {
            public static bool HaveData
            { 
                get
                {
                    return feUI == null ? false : feUI.HaveData;
                }
            }
            public static BioPacket Packet { get; set; }

            private static FEUI feUI;

            /// <summary>
            /// The main entry point for the application.
            /// </summary>
            [STAThread]
            public static void Main()
            {
                Application.EnableVisualStyles();
                //Application.SetCompatibleTextRenderingDefault(false);
                UnlockSupport unlocksprt = new UnlockSupport();
                unlocksprt.Unlock(null);

                //FEUI2 feUI = new FEUI2();
                //feUI.Show();
                //Application.Run(feUI);

                feUI = new FEUI();
                int ret = feUI.Channel(Packet);
                if (ret == 0)
                {
                    //feUI.ShowCapture(null);
                    feUI.ShowDialog();
                    try
                    {
                    //Application.Run(feUI);
                    //feUI.ShowDialog();
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        if (feUI != null) feUI = null;
                    }
                    catch (Exception)
                    {
                    
                    }
                }
                else
                {
                    Packet.PacketParamOut.Add("Error", GetError(ret));
                }
                unlocksprt.Lock(null);
            }

            private static string GetError(int ret)
            {
                string sret = null;
                switch (ret)
                {
                    case -1:
                        sret = "-1|Error desconocido. Ver log";
                        break;
                    case -2:
                        sret = "-2|Parametros nulos";
                        break;
                    case -3:
                        sret = "-3|Parametros src/TypeId/ValueId denben existir y no pueden ser nulos";
                        break;
                    default:
                        sret = "-1|Error desconocido. Ver log";
                        break;
                }
                return sret;
            }

            public static void SetPacket(NameValueCollection queryString)
            {
                Packet = new BioPacket();
                Packet.PacketParamIn = queryString;
                Packet.PacketParamOut = new Dictionary<string, object>();
            }

            public static void Close()
            {
                feUI.Close();
                feUI = null;
            }
        }
    //}
}
