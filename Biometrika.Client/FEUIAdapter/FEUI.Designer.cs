﻿using System;

namespace FEUIAdapter
{
    partial class FEUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        //[STAThread]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FEUI));
            this.labStep1Aux = new System.Windows.Forms.Label();
            this.timerToSetCaptureOnLoad = new System.Windows.Forms.Timer(this.components);
            this.imgCapture = new System.Windows.Forms.PictureBox();
            this.picSalir = new System.Windows.Forms.PictureBox();
            this.picAcercade = new System.Windows.Forms.PictureBox();
            this.picHelp = new System.Windows.Forms.PictureBox();
            this.picDelete = new System.Windows.Forms.PictureBox();
            this.labMsg = new System.Windows.Forms.Label();
            this.ctlProgress = new System.Windows.Forms.ProgressBar();
            this.timerNotarizar = new System.Windows.Forms.Timer(this.components);
            this.CaptureCtrl1 = new Leadtools.Multimedia.CaptureCtrl();
            this.labMsgToRead = new System.Windows.Forms.Label();
            this.picStep0 = new System.Windows.Forms.PictureBox();
            this.picDeletePhoto = new System.Windows.Forms.PictureBox();
            this.timerStep2 = new System.Windows.Forms.Timer(this.components);
            this.timerStep3 = new System.Windows.Forms.Timer(this.components);
            this.labTakeFoto = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labStep2Aux = new System.Windows.Forms.Label();
            this.labStep3Aux = new System.Windows.Forms.Label();
            this.picLogo3ro = new System.Windows.Forms.PictureBox();
            this.picStep1 = new System.Windows.Forms.PictureBox();
            this.picStep2 = new System.Windows.Forms.PictureBox();
            this.picStep3 = new System.Windows.Forms.PictureBox();
            this.picStep4 = new System.Windows.Forms.PictureBox();
            this.labPlayVideo = new System.Windows.Forms.Label();
            this.labMsgMenu = new System.Windows.Forms.Label();
            this.timerToRestartRecord = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAcercade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CaptureCtrl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDeletePhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo3ro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep4)).BeginInit();
            this.SuspendLayout();
            // 
            // labStep1Aux
            // 
            this.labStep1Aux.AutoSize = true;
            this.labStep1Aux.BackColor = System.Drawing.Color.Transparent;
            this.labStep1Aux.ForeColor = System.Drawing.Color.Gray;
            this.labStep1Aux.Location = new System.Drawing.Point(515, 206);
            this.labStep1Aux.Name = "labStep1Aux";
            this.labStep1Aux.Size = new System.Drawing.Size(63, 13);
            this.labStep1Aux.TabIndex = 9;
            this.labStep1Aux.Text = "Grabando...";
            this.labStep1Aux.Visible = false;
            // 
            // timerToSetCaptureOnLoad
            // 
            this.timerToSetCaptureOnLoad.Tick += new System.EventHandler(this.timerToSetCaptureOnLoad_Tick);
            // 
            // imgCapture
            // 
            this.imgCapture.Image = global::FEUIAdapter.Properties.Resources.no_picture;
            this.imgCapture.Location = new System.Drawing.Point(673, 134);
            this.imgCapture.Margin = new System.Windows.Forms.Padding(2);
            this.imgCapture.Name = "imgCapture";
            this.imgCapture.Size = new System.Drawing.Size(100, 59);
            this.imgCapture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgCapture.TabIndex = 5;
            this.imgCapture.TabStop = false;
            // 
            // picSalir
            // 
            this.picSalir.BackColor = System.Drawing.Color.White;
            this.picSalir.Image = global::FEUIAdapter.Properties.Resources.menu_salir;
            this.picSalir.Location = new System.Drawing.Point(734, 22);
            this.picSalir.Name = "picSalir";
            this.picSalir.Size = new System.Drawing.Size(44, 41);
            this.picSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSalir.TabIndex = 25;
            this.picSalir.TabStop = false;
            this.picSalir.Click += new System.EventHandler(this.picSalir_Click);
            this.picSalir.MouseLeave += new System.EventHandler(this.picSalir_MouseLeave);
            this.picSalir.MouseHover += new System.EventHandler(this.picSalir_MouseHover);
            // 
            // picAcercade
            // 
            this.picAcercade.BackColor = System.Drawing.Color.White;
            this.picAcercade.Image = global::FEUIAdapter.Properties.Resources.menu_biometrika;
            this.picAcercade.Location = new System.Drawing.Point(689, 22);
            this.picAcercade.Name = "picAcercade";
            this.picAcercade.Size = new System.Drawing.Size(51, 41);
            this.picAcercade.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picAcercade.TabIndex = 23;
            this.picAcercade.TabStop = false;
            this.picAcercade.Click += new System.EventHandler(this.picAcercade_Click);
            this.picAcercade.MouseLeave += new System.EventHandler(this.picAcercade_MouseLeave);
            this.picAcercade.MouseHover += new System.EventHandler(this.picAcercade_MouseHover);
            // 
            // picHelp
            // 
            this.picHelp.BackColor = System.Drawing.Color.White;
            this.picHelp.Image = global::FEUIAdapter.Properties.Resources.menu_ayuda;
            this.picHelp.Location = new System.Drawing.Point(639, 22);
            this.picHelp.Name = "picHelp";
            this.picHelp.Size = new System.Drawing.Size(51, 41);
            this.picHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picHelp.TabIndex = 22;
            this.picHelp.TabStop = false;
            this.picHelp.Click += new System.EventHandler(this.picHelp_Click);
            this.picHelp.MouseLeave += new System.EventHandler(this.picHelp_MouseLeave);
            this.picHelp.MouseHover += new System.EventHandler(this.picHelp_MouseHover);
            // 
            // picDelete
            // 
            this.picDelete.BackColor = System.Drawing.Color.White;
            this.picDelete.Image = global::FEUIAdapter.Properties.Resources.menu_reinicio;
            this.picDelete.Location = new System.Drawing.Point(589, 22);
            this.picDelete.Name = "picDelete";
            this.picDelete.Size = new System.Drawing.Size(51, 41);
            this.picDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picDelete.TabIndex = 21;
            this.picDelete.TabStop = false;
            this.picDelete.Click += new System.EventHandler(this.picDelete_Click);
            this.picDelete.MouseLeave += new System.EventHandler(this.picDelete_MouseLeave);
            this.picDelete.MouseHover += new System.EventHandler(this.picDelete_MouseHover);
            // 
            // labMsg
            // 
            this.labMsg.BackColor = System.Drawing.Color.Transparent;
            this.labMsg.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMsg.ForeColor = System.Drawing.Color.DimGray;
            this.labMsg.Location = new System.Drawing.Point(203, 54);
            this.labMsg.Name = "labMsg";
            this.labMsg.Size = new System.Drawing.Size(334, 22);
            this.labMsg.TabIndex = 20;
            this.labMsg.Text = "Firmante >> RUT 21284415-2";
            this.labMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ctlProgress
            // 
            this.ctlProgress.BackColor = System.Drawing.Color.SteelBlue;
            this.ctlProgress.Location = new System.Drawing.Point(154, 342);
            this.ctlProgress.Name = "ctlProgress";
            this.ctlProgress.Size = new System.Drawing.Size(195, 10);
            this.ctlProgress.TabIndex = 34;
            this.ctlProgress.Visible = false;
            // 
            // timerNotarizar
            // 
            this.timerNotarizar.Interval = 200;
            this.timerNotarizar.Tick += new System.EventHandler(this.timerNotarizar_Tick);
            // 
            // CaptureCtrl1
            // 
            this.CaptureCtrl1.AudioCompressors.Selection = -1;
            this.CaptureCtrl1.AudioDevices.Selection = -1;
            this.CaptureCtrl1.Location = new System.Drawing.Point(91, 125);
            this.CaptureCtrl1.Name = "CaptureCtrl1";
            this.CaptureCtrl1.OcxState = null;
            this.CaptureCtrl1.Size = new System.Drawing.Size(352, 208);
            this.CaptureCtrl1.TabIndex = 33;
            this.CaptureCtrl1.TargetDevices.Selection = -1;
            this.CaptureCtrl1.TargetFile = "c:\\tmp\\FE3D_tmp.avi";
            this.CaptureCtrl1.Text = "captureCtrl1";
            this.CaptureCtrl1.VideoCompressors.Selection = -1;
            this.CaptureCtrl1.VideoDevices.Selection = -1;
            this.CaptureCtrl1.WMProfile.Description = "";
            this.CaptureCtrl1.WMProfile.Name = "";
            this.CaptureCtrl1.Click += new System.EventHandler(this.CaptureCtrl1_Click);
            this.CaptureCtrl1.ErrorAbort += new Leadtools.Multimedia.ErrorAbortEventHandler(this.CaptureCtrl1_ErrorAbort);
            this.CaptureCtrl1.Complete += new System.EventHandler(this.CaptureCtrl1_Complete);
            this.CaptureCtrl1.Progress += new Leadtools.Multimedia.ProgressEventHandler(this.CaptureCtrl1_Progress);
            // 
            // labMsgToRead
            // 
            this.labMsgToRead.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMsgToRead.ForeColor = System.Drawing.Color.DimGray;
            this.labMsgToRead.Location = new System.Drawing.Point(139, 368);
            this.labMsgToRead.Name = "labMsgToRead";
            this.labMsgToRead.Size = new System.Drawing.Size(304, 78);
            this.labMsgToRead.TabIndex = 34;
            this.labMsgToRead.Text = "Yo, Gustavo Suhit, con RUT 21284415-2, acepto las condiciones del contrato de Com" +
    "pañía Nro 123456789NN";
            this.labMsgToRead.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // picStep0
            // 
            this.picStep0.Image = global::FEUIAdapter.Properties.Resources.capturar_video_gris;
            this.picStep0.Location = new System.Drawing.Point(458, 134);
            this.picStep0.Name = "picStep0";
            this.picStep0.Size = new System.Drawing.Size(203, 36);
            this.picStep0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picStep0.TabIndex = 35;
            this.picStep0.TabStop = false;
            this.picStep0.Click += new System.EventHandler(this.picStep0_Click);
            // 
            // picDeletePhoto
            // 
            this.picDeletePhoto.Image = global::FEUIAdapter.Properties.Resources.Menos;
            this.picDeletePhoto.Location = new System.Drawing.Point(757, 125);
            this.picDeletePhoto.Name = "picDeletePhoto";
            this.picDeletePhoto.Size = new System.Drawing.Size(16, 9);
            this.picDeletePhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picDeletePhoto.TabIndex = 36;
            this.picDeletePhoto.TabStop = false;
            this.picDeletePhoto.Click += new System.EventHandler(this.picDeletePhoto_Click);
            // 
            // timerStep2
            // 
            this.timerStep2.Tick += new System.EventHandler(this.timerStep2_Tick);
            // 
            // timerStep3
            // 
            this.timerStep3.Tick += new System.EventHandler(this.timerStep3_Tick);
            // 
            // labTakeFoto
            // 
            this.labTakeFoto.BackColor = System.Drawing.Color.Transparent;
            this.labTakeFoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTakeFoto.ForeColor = System.Drawing.Color.Black;
            this.labTakeFoto.Location = new System.Drawing.Point(466, 145);
            this.labTakeFoto.Name = "labTakeFoto";
            this.labTakeFoto.Size = new System.Drawing.Size(25, 25);
            this.labTakeFoto.TabIndex = 40;
            this.labTakeFoto.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.SteelBlue;
            this.pictureBox1.Image = global::FEUIAdapter.Properties.Resources.logo_uncolor;
            this.pictureBox1.Location = new System.Drawing.Point(734, 482);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(75, 36);
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // labStep2Aux
            // 
            this.labStep2Aux.AutoSize = true;
            this.labStep2Aux.ForeColor = System.Drawing.Color.Gray;
            this.labStep2Aux.Location = new System.Drawing.Point(515, 247);
            this.labStep2Aux.Name = "labStep2Aux";
            this.labStep2Aux.Size = new System.Drawing.Size(73, 13);
            this.labStep2Aux.TabIndex = 42;
            this.labStep2Aux.Text = "Procesando...";
            this.labStep2Aux.Visible = false;
            // 
            // labStep3Aux
            // 
            this.labStep3Aux.AutoSize = true;
            this.labStep3Aux.ForeColor = System.Drawing.Color.Gray;
            this.labStep3Aux.Location = new System.Drawing.Point(515, 290);
            this.labStep3Aux.Name = "labStep3Aux";
            this.labStep3Aux.Size = new System.Drawing.Size(73, 13);
            this.labStep3Aux.TabIndex = 43;
            this.labStep3Aux.Text = "Notarizando...";
            this.labStep3Aux.Visible = false;
            // 
            // picLogo3ro
            // 
            this.picLogo3ro.Image = global::FEUIAdapter.Properties.Resources.logo3ro;
            this.picLogo3ro.Location = new System.Drawing.Point(33, 437);
            this.picLogo3ro.Margin = new System.Windows.Forms.Padding(2);
            this.picLogo3ro.Name = "picLogo3ro";
            this.picLogo3ro.Size = new System.Drawing.Size(89, 51);
            this.picLogo3ro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo3ro.TabIndex = 44;
            this.picLogo3ro.TabStop = false;
            // 
            // picStep1
            // 
            this.picStep1.Image = global::FEUIAdapter.Properties.Resources.grabar_video_gris;
            this.picStep1.Location = new System.Drawing.Point(458, 176);
            this.picStep1.Name = "picStep1";
            this.picStep1.Size = new System.Drawing.Size(203, 36);
            this.picStep1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picStep1.TabIndex = 45;
            this.picStep1.TabStop = false;
            this.picStep1.Click += new System.EventHandler(this.picStep1_Click);
            // 
            // picStep2
            // 
            this.picStep2.Image = global::FEUIAdapter.Properties.Resources.procesar_video_gris;
            this.picStep2.Location = new System.Drawing.Point(458, 217);
            this.picStep2.Name = "picStep2";
            this.picStep2.Size = new System.Drawing.Size(203, 36);
            this.picStep2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picStep2.TabIndex = 46;
            this.picStep2.TabStop = false;
            this.picStep2.Click += new System.EventHandler(this.picStep2_Click);
            // 
            // picStep3
            // 
            this.picStep3.Image = global::FEUIAdapter.Properties.Resources.notarizar_gris;
            this.picStep3.Location = new System.Drawing.Point(458, 258);
            this.picStep3.Name = "picStep3";
            this.picStep3.Size = new System.Drawing.Size(203, 36);
            this.picStep3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picStep3.TabIndex = 47;
            this.picStep3.TabStop = false;
            this.picStep3.Click += new System.EventHandler(this.picStep3_Click);
            // 
            // picStep4
            // 
            this.picStep4.Image = global::FEUIAdapter.Properties.Resources.salir_gris;
            this.picStep4.Location = new System.Drawing.Point(458, 299);
            this.picStep4.Name = "picStep4";
            this.picStep4.Size = new System.Drawing.Size(203, 36);
            this.picStep4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picStep4.TabIndex = 48;
            this.picStep4.TabStop = false;
            this.picStep4.Click += new System.EventHandler(this.picStep4_Click);
            // 
            // labPlayVideo
            // 
            this.labPlayVideo.BackColor = System.Drawing.Color.Transparent;
            this.labPlayVideo.ForeColor = System.Drawing.Color.Gray;
            this.labPlayVideo.Location = new System.Drawing.Point(375, 107);
            this.labPlayVideo.Name = "labPlayVideo";
            this.labPlayVideo.Size = new System.Drawing.Size(68, 15);
            this.labPlayVideo.TabIndex = 49;
            this.labPlayVideo.Click += new System.EventHandler(this.labPlayVideo_Click);
            // 
            // labMsgMenu
            // 
            this.labMsgMenu.ForeColor = System.Drawing.Color.Gray;
            this.labMsgMenu.Location = new System.Drawing.Point(589, 64);
            this.labMsgMenu.Name = "labMsgMenu";
            this.labMsgMenu.Size = new System.Drawing.Size(189, 12);
            this.labMsgMenu.TabIndex = 50;
            this.labMsgMenu.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // timerToRestartRecord
            // 
            this.timerToRestartRecord.Tick += new System.EventHandler(this.timerToRestartRecord_Tick);
            // 
            // FEUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::FEUIAdapter.Properties.Resources.FE3D_Fondo_v3;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(810, 518);
            this.Controls.Add(this.labMsgMenu);
            this.Controls.Add(this.labPlayVideo);
            this.Controls.Add(this.picSalir);
            this.Controls.Add(this.picAcercade);
            this.Controls.Add(this.picHelp);
            this.Controls.Add(this.picDelete);
            this.Controls.Add(this.labStep3Aux);
            this.Controls.Add(this.picStep4);
            this.Controls.Add(this.picStep3);
            this.Controls.Add(this.labStep2Aux);
            this.Controls.Add(this.picStep2);
            this.Controls.Add(this.labStep1Aux);
            this.Controls.Add(this.picStep1);
            this.Controls.Add(this.picLogo3ro);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.picStep0);
            this.Controls.Add(this.labTakeFoto);
            this.Controls.Add(this.picDeletePhoto);
            this.Controls.Add(this.labMsgToRead);
            this.Controls.Add(this.CaptureCtrl1);
            this.Controls.Add(this.ctlProgress);
            this.Controls.Add(this.labMsg);
            this.Controls.Add(this.imgCapture);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FEUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FEUI_FormClosing);
            this.Load += new System.EventHandler(this.FEUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAcercade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CaptureCtrl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDeletePhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo3ro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox imgCapture;
        private System.Windows.Forms.Label labStep1Aux;
        private System.Windows.Forms.Timer timerToSetCaptureOnLoad;
        private System.Windows.Forms.PictureBox picSalir;
        private System.Windows.Forms.PictureBox picAcercade;
        private System.Windows.Forms.PictureBox picHelp;
        private System.Windows.Forms.PictureBox picDelete;
        private System.Windows.Forms.Label labMsg;
        private System.Windows.Forms.ProgressBar ctlProgress;
        private System.Windows.Forms.Timer timerNotarizar;
        private Leadtools.Multimedia.CaptureCtrl CaptureCtrl1;
        private System.Windows.Forms.Label labMsgToRead;
        private System.Windows.Forms.PictureBox picStep0;
        private System.Windows.Forms.PictureBox picDeletePhoto;
        private System.Windows.Forms.Timer timerStep2;
        private System.Windows.Forms.Timer timerStep3;
        private System.Windows.Forms.Label labTakeFoto;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labStep2Aux;
        private System.Windows.Forms.Label labStep3Aux;
        private System.Windows.Forms.PictureBox picLogo3ro;
        private System.Windows.Forms.PictureBox picStep1;
        private System.Windows.Forms.PictureBox picStep2;
        private System.Windows.Forms.PictureBox picStep3;
        private System.Windows.Forms.PictureBox picStep4;
        private System.Windows.Forms.Label labPlayVideo;
        private System.Windows.Forms.Label labMsgMenu;
        private System.Windows.Forms.Timer timerToRestartRecord;
    }
}

