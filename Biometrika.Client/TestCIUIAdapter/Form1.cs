﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestCIUIAdapter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Collections.Specialized.NameValueCollection NVC = new System.Collections.Specialized.NameValueCollection();
            /*
             src=ci&
                 typeId=RUT&
                 valueId=21284415-2&
                 Mail=gsuhit@gmail.com&
                 Theme=theme_blue&
                 Hash=4jf4jf532f23f4# 
            
            */
            NVC.Add("SRC", "CI");
            NVC.Add("TYPEID", "RUT");
            NVC.Add("VALUEID", "21284415-2");
            NVC.Add("MAIL", "gsuhit@gmail.com");
            NVC.Add("THEME", "theme_blue");
            NVC.Add("HASH", "4jf4jf532f23f4");
            CIUIAdapter.Program.SetPacket(NVC);
            CIUIAdapter.Program.Main();
            //CIUIAdapter.CIUIAdapter frm = new CIUIAdapter.CIUIAdapter();
            //frm.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //System.Drawing.Drawing2D.GraphicsPath objDraw = new System.Drawing.Drawing2D.GraphicsPath();
            //objDraw.AddEllipse(0, 0, this.pictureBox1.Width, this.pictureBox1.Height);
            //this.pictureBox1.Region = new Region(objDraw);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //timerProcess.Enabled = !timerProcess.Enabled;


        }

        int q = 0;
        private void timerProcess_Tick(object sender, EventArgs e)
        {
            if (label1.Text.Equals("\\"))
            {
                label1.Text = "|";
            } else if (label1.Text.Equals("|"))
            {
                label1.Text = "/";
            }
            else if (label1.Text.Equals("/"))
            {
                label1.Text = "--";
            }
            else if (label1.Text.Equals("--"))
            {
                label1.Text = "\\";
            }


            if (label2.Text.Equals(">>             "))
            {
                label2.Text = ">>>            ";
            }
            else if (label2.Text.Equals(">>>            "))
            {
                label2.Text = " >>>           ";
            }
            else if (label2.Text.Equals(" >>>           "))
            {
                label2.Text = "  >>>          ";
            }
            else if (label2.Text.Equals("  >>>          "))
            {
                label2.Text = "   >>>         ";
            }
            else if (label2.Text.Equals("   >>>         "))
            {
                label2.Text = "    >>>        ";
            }
            else if (label2.Text.Equals("    >>>        "))
            {
                label2.Text = "     >>>       ";
            }
            else if (label2.Text.Equals("     >>>       "))
            {
                label2.Text = ">>>            ";
            }

            
            try
            {

                if (pbProcess.Value == 200)
                {
                    pbProcess.Value = 0;
                    pbProcess.Refresh();
                }
                pbProcess.Value += 1;
                pbProcess.Refresh();
            }
            catch (Exception ex)
            {

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(textBox1.Text);
                textBox2.Text = (addr.Address == textBox1.Text).ToString();
            }
            catch
            {
                textBox2.Text = "false";
            }
        }
    }
}
