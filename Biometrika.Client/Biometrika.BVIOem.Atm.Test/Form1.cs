﻿using Biometrika.BVIOem.Atm.libs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.BVIOem.Atm.Test
{
    public partial class Form1 : Form
    {
        BviAtm _ATM_COMPONENT;
        
        public Form1()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
           InitComponent();
           
        }

        private void InitComponent()
        {

            string msgerr;
            try
            {
                //"\"ReaderProximityName\":  \"OMNIKEY CardMan 5x21-CL 0\"," +
                string jsonconfig = "{" +
                                        "\"PathLicense\":  \"BVIOEMATMLicense.lic\"," +
                                        "\"SensorType\":  \"1\"," +
                                        "\"SerialSensor\":  \"{6AAA4C29-B7C5-9846-9772-E1789F5286C5}\"," +
                                        "\"FingerTimeout\":  \"5000\"," +
                                        "\"QualityThreshold\":  \"50\"," +
                                        "\"PathLogConfig\":  \"Logger.cfg\"," +
                                        "\"COMPort\":  \"COM10\"," +
                                        "\"ReaderProximityName\":  \"HID Global OMNIKEY 5022 Smart Card Reader 0\"," +
                                        "\"pathlic3Pty\":  \"D:\\\\ACODE\\\\bcarepo\\\\Biometrika.Client\\\\Biometrika.BVIOem.Atm.Test\\\\bin\\\\Debug\\\\iengine.lic\"," +
                                        "\"IntegrityCheckEnabled\": \"true\"" +
                                    "}";
                //"\"IntegrityCheckEnabled\": \"true\"," +
                //"\"BloquedExpirationEnabled\": \"false\"" +

                _ATM_COMPONENT = new BviAtm();
                int ret = _ATM_COMPONENT.Initialize(out msgerr, true, jsonconfig);

                _ATM_COMPONENT._OnCapturedEvent += OnCaptureSampleEvent;
                _ATM_COMPONENT._OnTimeoutEvent += OnTimeoutSampleEvent;
                _ATM_COMPONENT._OnErrorEvent += OnErrorEvent;
                _ATM_COMPONENT._OnReadBarcodeEvent += OnReadBarcodeEvent;
                _ATM_COMPONENT._OnVerifytEvent += OnVerifyEvent;
                AddLog("InitComponent => Componetne inicializado OK!");
            }
            catch (Exception ex)
            {
                AddLog("InitComponent Excp => " + ex.Message);
            }
        }

        private void OnVerifyEvent(int errCode, string errMessage, bool resultverify)
        {
            AddLog("OnVerifyEvent => " + errCode.ToString() + " - " + errMessage + 
                    " - Result Verify=" + resultverify.ToString());
        }

        private void OnReadBarcodeEvent(int errCode, string errMessage, string data)
        {
            AddLog("OnReadBarcodeEvent => " + errCode.ToString() + " - " + errMessage + " - RUT Leido=" + data);
        }

        private void OnErrorEvent(int errCode, string errMessage)
        {
            AddLog("OnErrorEvent => " + errCode.ToString() + " - " + errMessage);
        }

        private void OnTimeoutSampleEvent(int errCode, string errMessage)
        {
            AddLog("OnTimeoutSampleEvent => " + errCode.ToString() + " - " + errMessage);
        }

        private void OnCaptureSampleEvent(int errCode, string errMessage, PersonData pdata) //List<Biometrika.BioApi20.Sample> samplesCaptured)
        {
            int i = 0;
            string msgerr;
            string data;
            i = _ATM_COMPONENT.GetData(out data, out msgerr);
            AddLog(data);
        }

        private void btnGetSample_Click(object sender, EventArgs e)
        {
            string msgerr;
            int ret = _ATM_COMPONENT.GetSample(out msgerr);
            AddLog("GetSample ret = " + ret.ToString());
        }

        private void AddLog(string msg)
        {
            if (string.IsNullOrEmpty(rtbLog.Text))
            {
                rtbLog.Text = msg;
            } else {
                rtbLog.Text += Environment.NewLine + msg;
            }
        }

        private void btnReadQR_Click(object sender, EventArgs e)
        {
            string msgerr;
            int ret = _ATM_COMPONENT.ReadBarcode(out msgerr);
            AddLog("ReadBarcode ret = " + ret.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int ret = _ATM_COMPONENT.SetInitVerifyProcess(textBox1.Text);
            AddLog("SetInitVerifyProcess ret = " + ret.ToString());
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            string msgerr;
            string data;
            int ret = _ATM_COMPONENT.GetData(out data, out msgerr, Convert.ToInt32(textBox2.Text));
            AddLog("GetData ret = " + ret.ToString() + " | msgerr = " + msgerr + " | Data = " + data);
        }

        private void btnVerify_Click(object sender, EventArgs e)
        {
            string msgerr;
            string data;
            int ret = _ATM_COMPONENT.Verify(out msgerr);
            AddLog("Verify ret = " + ret.ToString() + " | msgerr = " + msgerr);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            rtbLog.Text = "";
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            int ret = _ATM_COMPONENT.SetEndVerifyProcess();
            AddLog("End SetEndVerifyProcess ret = " + ret.ToString());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int ret = _ATM_COMPONENT.SetEndVerifyProcess();
            AddLog("End SetEndVerifyProcess ret = " + ret.ToString());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            InitComponent();
        }
    }
}
