﻿using Bio.Digital.Receipt.Api;
using Domain;
using Emgu.CV;
using Emgu.CV.Structure;
using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net.NetworkInformation;
using System.Threading;
using System.Windows.Forms;

namespace CIUIAdapter
{
    public partial class CIUIAdapter : Form
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(CIUIAdapter));

        //public int Tokencontent { get; set; }
        public bool HaveData { get; set; }
        public static BioPacket Packet { get; set; }
        private BioPacket bioPacketLocal;

        internal int _CurrentError = 0;
        internal string _sCurrentError = null;

        private string _pathPDF = null;
        private string _pathXML = null;
        private bool _MUSTRUN = false;
        internal string DeviceId { get; set; }
        internal string GeoRef { get; set; }
        public string DocumentId { get; set; }
        public string Mail { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string Company { get; set; }
        
        public string RDEnvelope { get; set; }
        public string TrackIdCI { get; set; }

        private Capture captureCam;
        private RUT RUT;

        private string json;
        private bool isMenuOpen = false;
        private int timeLeft;
        private System.Timers.Timer timer;
        private System.Timers.Timer timerControlGeneral;

        private int Finger { get; set; }
        //private bool MustStop { get; set; } = false;

        private int iTinmerToShow = 10;
        private bool isStart = true;

        private int _CAPTURING = 0; //1 - Cedula | 2 - Selfie

        private int _CURRENT_STEP = 0;

        private string _ImageCedulaB64;
        private string _ImageSelfieB64;
        CIParam _CIPARAM = null;

        public CIUIAdapter()
        {
            InitializeComponent();
        }

        private void CIUIAdapter_Load(object sender, System.EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            //this.TopMost = true;
            //this.TopLevel = true;

            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;

  
            Init();

            CheckTmpDirectory();

            GetDeviceId();

            GetGeoRef();

            try
            {
                if (!dictParamIn["THEME"].Equals("Default")) //Si no es fondo default => Cargo fondo 
                {
                    this.BackgroundImage = Image.FromFile(dictParamIn["THEME"]);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("FEUI.FEUI_Load Error cargando fondo = " + dictParamIn["THEME"]);
            }

            LabRutTitle.Text = dictParamIn["VALUEID"];

            try
            {
                picLogo3ro.Image = Image.FromFile(Properties.Settings.Default.CILogo3ro);
                picLogo3ro.Width = Properties.Settings.Default.CILogo3roW;
                picLogo3ro.Height = Properties.Settings.Default.CILogo3roH;
                picLogo3ro.Left = Properties.Settings.Default.CILogo3roX;
                picLogo3ro.Top = Properties.Settings.Default.CILogo3roY;
                picLogo3ro.Visible = true;
            }
            catch (Exception ex)
            {
                LOG.Error("FEUI.FEUI_Load Error Cargando logo externo...");
                picLogo3ro.Visible = false;
            }

            LOG.Debug("CIUIAdapter.CIUIAdapter_Load OK!");
        }

        private System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return null
            ;
        }

        internal int Channel(BioPacket bioPacket)
        {
            int ret = 0;
            LOG.Info("CIUIAdapter.Channel In...");
            bioPacketLocal = bioPacket;

            ret = CheckParameters(bioPacketLocal);

            if (ret == 0)
            {
                if (bioPacketLocal.PacketParamOut == null)
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            }
            LOG.Info("CIUIAdapter.Channel Out [ret=" + ret + "]...");
            return ret;
        }

        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        private int CheckParameters(BioPacket bioPacketLocal)
        {
            /*
                 src=ci&
                 typeId=RUT&
                 valueId=21284415-2&
                 Mail=gsuhit@gmail.com&
                 Theme=theme_blue&
                 Hash=4jf4jf532f23f4#
            */
            int ret = 0;
            try
            {
                LOG.Info("CIUIAdapter.CheckParameters In...");
                if (bioPacketLocal == null) ret = -2;  //Lista de parametros no puede ser nulo

                //Paso a Dictionary

                LOG.Debug("CIUIAdapter.CheckParameters filling dictParamin...");
                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    LOG.Debug("CIUIAdapter.CheckParameters     Adding => " + k + "=" + (string)bioPacketLocal.PacketParamIn[k] + "...");
                    dictParamIn.Add(k.ToUpper(), bioPacketLocal.PacketParamIn[k]);
                }
                LOG.Debug("CIUIAdapter.CheckParameters dictParamin filled [length = " + dictParamIn.Count + "]");

                // ||
                //!dictParamIn.ContainsKey("TYPEID") || String.IsNullOrEmpty(dictParamIn["TYPEID"]) ||
                //!dictParamIn.ContainsKey("VALUEID") || String.IsNullOrEmpty(dictParamIn["VALUEID"])
                if (!dictParamIn.ContainsKey("SRC") || String.IsNullOrEmpty(dictParamIn["SRC"])
                    || !dictParamIn.ContainsKey("TYPEID") || String.IsNullOrEmpty(dictParamIn["TYPEID"]) ||
                       !dictParamIn.ContainsKey("VALUEID") || String.IsNullOrEmpty(dictParamIn["VALUEID"]))
                {
                    ret = -3;  //src/TypeId/ValueId deben ser enviados como parámetros
                    LOG.Debug("CIUIAdapter.CheckParameters src/TypeId/ValueId deben ser enviados como parámetros!");
                }
                else
                {
                    LOG.Debug("CIUIAdapter.CheckParameters evaluando cada parámetro...");
                    if (!dictParamIn.ContainsKey("MAIL") || String.IsNullOrEmpty(dictParamIn["MAIL"]))
                    {
                        if (!dictParamIn.ContainsKey("MAIL"))
                        {
                            dictParamIn.Add("MAIL", "info@notariovirtual.cl");
                        }
                        else
                        {
                            dictParamIn["TOKENCONTENT"] = "info@notariovirtual.cl";  //Ambos Minucia + WSQ
                        }
                    }
                    //LOG.Debug("CIUIAdapter.CheckParameters Tokencontent = " + dictParamIn["TOKENCONTENT"]);
                    //if (!dictParamIn.ContainsKey("DEVICE") || String.IsNullOrEmpty(dictParamIn["DEVICE"]))
                    //{
                    //    if (!dictParamIn.ContainsKey("DEVICE"))
                    //    {
                    //        dictParamIn.Add("DEVICE", "1");
                    //    }
                    //    else
                    //    {
                    //        dictParamIn["DEVICE"] = "1"; //DP - no imposta ahora
                    //    }
                    //}
                    //LOG.Debug("CIUIAdapter.CheckParameters Device = " + dictParamIn["DEVICE"]);
                    //if (!dictParamIn.ContainsKey("AUTHENTICATIONFACTOR") || String.IsNullOrEmpty(dictParamIn["AUTHENTICATIONFACTOR"]))
                    //{
                    //    if (!dictParamIn.ContainsKey("AUTHENTICATIONFACTOR"))
                    //    {
                    //        dictParamIn.Add("AUTHENTICATIONFACTOR", "2");
                    //    }
                    //    else
                    //    {
                    //        dictParamIn["AUTHENTICATIONFACTOR"] = "2"; //Fingerprint
                    //    }
                    //}
                    //LOG.Debug("CIUIAdapter.CheckParameters AuthenticationFactor = " + dictParamIn["AUTHENTICATIONFACTOR"]);
                    //if (!dictParamIn.ContainsKey("OPERATIONTYPE") || String.IsNullOrEmpty(dictParamIn["OPERATIONTYPE"]))
                    //{
                    //    if (!dictParamIn.ContainsKey("OPERATIONTYPE"))
                    //    {
                    //        dictParamIn.Add("OPERATIONTYPE", "1");
                    //    }
                    //    else
                    //    {
                    //        dictParamIn["OPERATIONTYPE"] = "1";
                    //    }
                    //}
                    //LOG.Debug("CIUIAdapter.CheckParameters OperationType = " + dictParamIn["OPERATIONTYPE"]);
                    //if (!dictParamIn.ContainsKey("MINUTIAETYPE") || String.IsNullOrEmpty(dictParamIn["MINUTIAETYPE"]))
                    //{
                    //    if (!dictParamIn.ContainsKey("MINUTIAETYPE"))
                    //    {
                    //        dictParamIn.Add("MINUTIAETYPE", "14");
                    //    }
                    //    else
                    //    {
                    //        dictParamIn["MINUTIAETYPE"] = "14"; //ISO
                    //    }
                    //}
                    //LOG.Debug("CIUIAdapter.CheckParameters MinutiaeType = " + dictParamIn["MINUTIAETYPE"]);
                    if (!dictParamIn.ContainsKey("TIMEOUT") || String.IsNullOrEmpty(dictParamIn["TIMEOUT"]))
                    {
                        if (!dictParamIn.ContainsKey("TIMEOUT"))
                        {
                            dictParamIn.Add("TIMEOUT", "10000");
                        }
                        else
                        {
                            dictParamIn["TIMEOUT"] = "10000";
                        }
                    }
                    LOG.Debug("CIUIAdapter.CheckParameters Timeout = " + dictParamIn["TIMEOUT"]);
                    if (!dictParamIn.ContainsKey("THEME") || String.IsNullOrEmpty(dictParamIn["THEME"]))
                    {
                        if (!dictParamIn.ContainsKey("THEME"))
                        {
                            dictParamIn.Add("THEME", "theme_ligthgreen");
                        }
                        else
                        {
                            dictParamIn["THEME"] = "theme_green";
                        }
                    }
                    LOG.Debug("CIUIAdapter.CheckParameters Theme = " + dictParamIn["THEME"]);
                    
                    if (!dictParamIn.ContainsKey("HASH") || String.IsNullOrEmpty(dictParamIn["HASH"]))
                    {
                        if (!dictParamIn.ContainsKey("HASH"))
                        {
                            dictParamIn.Add("HASH", Guid.NewGuid().ToString("N"));
                        }
                        else
                        {
                            dictParamIn["HASH"] = Guid.NewGuid().ToString("N");
                        }
                    }
                    LOG.Debug("CIUIAdapter.CheckParameters Hash = " + dictParamIn["HASH"]);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Error CIUIAdapter.CheckParameters [" + ex.Message + "]");
                ret = -1;
            }
            LOG.Info("CIUIAdapter.CheckParameters Ount [ret=" + ret + "]!");
            return ret;
        }


        private void StopCamStream()
        {
            LOG.Info("CIUIAdapter.StopCamStream IN...");
            try
            {
                Application.Idle -= ShowCamStream;
                captureCam.Stop();
                captureCam.Dispose();
                labStopPreview.Enabled = false;
                labStartPreview.Enabled = true;
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.StopCamStream Error = " + ex.Message, ex);
            }
            LOG.Info("CIUIAdapter.StopCamStream OUT!");
        }

        private void ShowCamStream(object sender, EventArgs e)

        {
            try
            {
                Mat mat = new Mat();
                captureCam.Retrieve(mat);
                Image<Bgr, Byte> img = mat.ToImage<Bgr, Byte>();

                //Image<Gray, Byte> grayImage = img.Convert<Gray, Byte>();
                //var faces = grayImage.DetectHaarCascade(
                //                                   haar, 1.4, 4,
                //                                   HAAR_DETECTION_TYPE.DO_CANNY_PRUNING,
                //                                   new Size(nextFrame.Width / 8, nextFrame.Height / 8)
                //                                   )[0];

                imageCam.Image = img.ToBitmap();

            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.ShowCamStream Error = " + ex.Message, ex);
            }
            
        }

        private void Init()
        {
            try
            {
                StopCamStream();
                this.BackgroundImage = Properties.Resources.FondoCI_Init_0;
                LOG.Info("CIUIAdapter.Init IN...");

                labMsgGuia.ForeColor = Color.Gray;
                labMsgGuia.Text = "Inicie el proceso de Certificación de Idendtidad siguiendo los pasos propuestos...";
                HaveData = false;
                labMail.Text = dictParamIn["MAIL"];
                LabRutTitle.Text = dictParamIn["VALUEID"];
                LabRutTitle.ForeColor = Color.Gray;
                imageCedula.Visible = false;
                imageSelfie.Visible = false;
                picQR.Visible = false;
                picPDF.Visible = false;
                _ImageCedulaB64 = null;
                _ImageSelfieB64 = null;
                _CURRENT_STEP = 0;
                _CAPTURING = 0;
                labRUT.Text = "?";
                labNombre.Text = "?";
                labFNac.Text = "?";
                labNacionalidad.Text = "?";
                labSexo.Text = "?";
                //labVigencia.Visible = false;
                labFVto.ForeColor = Color.Gray;
                labFVto.Text = "?";
                labCapturaSelfie.Enabled = false;
                labCaturaCedula.Enabled = false;
                labStartPreview.Enabled = true;
                labStopPreview.Enabled = false;
                labCertificaIdentidad.Enabled = false;
                picQR.Visible = false;
                picPDF.Visible = false;
                _pathPDF = null;
                picXML.Visible = false;
                _pathXML = null;
                picCIOK.Visible = false;
                labTrackId.Visible = false;
                label5.Visible = false;

                this.Refresh();
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.Init Error = " + ex.Message, ex);
            }
            LOG.Info("CIUIAdapter.Init OUT!");
        }


        private void ShowStepStatus(string error = "")
        {
            try
            {
                LOG.Info("CIUIAdapter.ShowStepStatus IN =>  _CURRENT_STEP = " + _CURRENT_STEP);
                switch (_CURRENT_STEP)
                {
                    case 1:  //Capturando cedula
                        labMsgGuia.ForeColor = Color.Gray;
                        labMsgGuia.Text = "Paso 1 - Capturando Cédula. Presione CAPTURAR CÉDULA cuando tenga una imágen clara...";
                        labMsgGuia.Refresh();
                        this.BackgroundImage = Properties.Resources.FondoCI_P1_Init;
                        this.Refresh();
                        _CurrentError = 0;
                        _sCurrentError = "";
                        break;
                    case 21:  //Cedula Capturada OK
                        this.BackgroundImage = Properties.Resources.FondoCI_P1_OK;
                        this.Refresh();
                        labMsgGuia.ForeColor = Color.Gray;
                        labMsgGuia.Text = "Cédula Caturada!" + Environment.NewLine + "Paso 2 -  Capturando Selfie. Presione CAPTURAR SELFIE cuando tenga una imágen clara...";
                        labMsgGuia.Refresh();
                        labCapturaSelfie.Enabled = true;
                        _CurrentError = 0;
                        _sCurrentError = "";
                        break;
                    case 22:  //Cedula Capturada NO OK
                        labMsgGuia.Text = "Paso 1 - Error Capturando Cédula [" + error + "]";
                        labMsgGuia.ForeColor = Color.Red;
                        labMsgGuia.Refresh();
                        this.BackgroundImage = Properties.Resources.FondoCI_P1_NOOK;
                        this.Refresh();

                        StopCamStream();
                        labCapturaSelfie.Enabled = false;

                        _CurrentError = -22;
                        _sCurrentError = "Paso 1 - Error Capturando Cédula [" + error + "]";

                        break;
                    case 3:  //Capturando selfie
                        labMsgGuia.ForeColor = Color.Gray;
                        labMsgGuia.Text = "Cédula Capturada!" + Environment.NewLine + "Paso 2 -  Capturando Selfie. Presione CAPTURAR SELFIE cuando tenga una imágen clara...";
                        labMsgGuia.Refresh();

                        this.BackgroundImage = Properties.Resources.FondoCI_P1_OK;
                        this.Refresh();

                        _CurrentError = 0;
                        _sCurrentError = "";

                        labCapturaSelfie.Enabled = true;

                        break;
                    case 31:  //Cedula Capturada OK
                        labMsgGuia.ForeColor = Color.Gray;
                        labMsgGuia.Text = "Selfie Caturada!" + Environment.NewLine + "Paso 3 -  Certificando Identidad con Notario Virtual. Espere...";
                        labMsgGuia.Refresh();

                        _CURRENT_STEP = 4;

                        this.BackgroundImage = Properties.Resources.FondoCI_P2_OK;
                        this.Refresh();
                        _CurrentError = 0;
                        _sCurrentError = "";
                        break;
                    case 32:  //Cedula Capturada NO OK
                        labMsgGuia.Text = "Paso 2 - Error Capturando Selfie [" + error + "]";
                        labMsgGuia.ForeColor = Color.Red;
                        labMsgGuia.Refresh();

                        this.BackgroundImage = Properties.Resources.FondoCI_P2_NOOK;
                        this.Refresh();

                        _CurrentError = -32;
                        _sCurrentError = "Paso 2 - Error Capturando Selfie [" + error + "]";

                        break;
                    default:
                        break;
                }
                
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.ShowStepStatus Error = " + ex.Message, ex);
            }
            LOG.Info("CIUIAdapter.ShowStepStatus OUT!");
        }

        private void btnStartCedula_Click(object sender, EventArgs e)
        {
            LOG.Info("CIUIAdapter.btnStartCedula_Click IN...");
            try
            {
                _CAPTURING = 1;
                _CURRENT_STEP = 1;
                ShowStepStatus();
                captureCam = new Capture();
                Application.Idle += ShowCamStream;
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.btnStartCedula_Click Error = "+ ex.Message, ex);
            }
            LOG.Info("CIUIAdapter.btnStartCedula_Click OUT!");
        }

        

        private void btnCapturaCedula_Click(object sender, EventArgs e)
        {

            LOG.Info("CIUIAdapter.btnCapturaCedula_Click IN...");
            try
            {
                _CAPTURING = 0;
                //_CURRENT_STEP = 21;
                imageCedula.Image = imageCam.Image;
                _ImageCedulaB64 = GetB64FromPictureBox(imageCedula);
                LOG.Debug("CIUIAdapter.btnCapturaCedula_Click _ImageCedulaB64 = " + _ImageCedulaB64);

                //btnCapturaCedula.Enabled = false;
               
                timer_VerifyCedula.Enabled = true;
            }
            catch (Exception ex)
            {
                _CURRENT_STEP = 22;
                LOG.Error("CIUIAdapter.btnCapturaCedula_Click Error = " + ex.Message, ex);
            }
            LOG.Info("CIUIAdapter.btnCapturaCedula_Click OUT!");
            //StopCamStream();
            //ShowStepStatus();

        }

        private void btnStartSelfie_Click(object sender, EventArgs e)
        {
            LOG.Info("CIUIAdapter.btnStartSelfie_Click IN...");
            try
            {
                _CAPTURING = 2;
                _CURRENT_STEP = 3;
                ShowStepStatus();
                captureCam = new Capture();
                Application.Idle += ShowCamStream;
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.btnStartSelfie_Click Error = " + ex.Message, ex);
            }
            LOG.Info("CIUIAdapter.btnStartSelfie_Click OUT!");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LOG.Info("CIUIAdapter.button1_Click IN...");
            try
            {
                _CAPTURING = 0;
                //_CURRENT_STEP = 31;
                imageSelfie.Image = imageCam.Image;
                _ImageSelfieB64 = GetB64FromPictureBox(imageSelfie);
                LOG.Debug("CIUIAdapter.button1_Click _ImageSelfieB64 = " + _ImageSelfieB64);
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.button1_Click Error = " + ex.Message, ex);
                _CURRENT_STEP = 32;

            }
            StopCamStream();
            //ShowStepStatus();
            LOG.Info("CIUIAdapter.button1_Click OUT!");
            timer_GeneratingCI.Enabled = true;
        }

        private void timer_VerifyCedula_Tick(object sender, EventArgs e)
        {
            string err = "";
            try
            {
                timer_VerifyCedula.Enabled = false;
                LOG.Info("CIUIAdapter.timer_VerifyCedula_Tick IN...");
                //labRUT.Text = "21284415-2";
                //labNombre.Text = "Gustavo Gerardo Suhit GAllucci";
                //labFNac.Text = "28-09-2169";
                //labNacionalidad.Text = "ARG";
                //labSexo.Text = "M";
                //_CURRENT_STEP = 21;

                string xmlparamin, xmlparamout;
                using (WSCI.WSCI ws = new WSCI.WSCI())
                {
                    ws.Timeout = Properties.Settings.Default.CIWSTimeout;
                    Bio.Digital.Receipt.Api.XmlParamIn oParamin = new Bio.Digital.Receipt.Api.XmlParamIn();
                    oParamin.Actionid = 2;
                    oParamin.Origin = 1;
                    oParamin.Companyid = 1;
                    oParamin.ApplicationId = Properties.Settings.Default.NVCIAppId;
                    oParamin.CIParam = new Bio.Digital.Receipt.Api.CIParam();
                    oParamin.CIParam.TrackId = null;
                    oParamin.CIParam.TypeId = "RUT";
                    oParamin.CIParam.ValueId = dictParamIn["VALUEID"];
                    oParamin.CIParam.IDCardImage = _ImageCedulaB64;
                    xmlparamin = Bio.Digital.Receipt.Api.XmlUtils.SerializeAnObject(oParamin);
                    LOG.Debug("CIUIAdapter.timer_VerifyCedula_Tick xmlparamin = " + xmlparamin);
                    LOG.Debug("CIUIAdapter.timer_VerifyCedula_Tick WSCI.WSCI URL = " + ws.Url);
                    //pbProcess.Value = 0;
                    //pbProcess.Visible = true;
                    //pbProcess.Refresh();
                    //timer_ShowProgress.Enabled = true;
                    //_MUSTRUN = true;
                    //Thread tShowProgress = new Thread(new ThreadStart(ShowProgress));
                    //tShowProgress.Start();
                    //for (int i = 0; i < 10000; i++)
                    //{
                    //    var a = 0;
                    //}
                    labProcesando.Visible = true;
                    labProcesando.Refresh();
                    int ret = ws.Process(xmlparamin, out xmlparamout);
                    labProcesando.Visible = false;
                    //_MUSTRUN = false;
                    //tShowProgress.Abort();
                    //tShowProgress = null;

                    timer_ShowProgress.Enabled = false;
                    pbProcess.Visible = false;
                    LOG.Debug("CIUIAdapter.timer_VerifyCedula_Tick ret = " + ret + " => xmlparamout = " + (string.IsNullOrEmpty(xmlparamout) ? "NULL" : xmlparamout));
                    if (ret == 0)
                    {
                        if (!string.IsNullOrEmpty(xmlparamout))
                        {
                            XmlParamOut oParamout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                            if (oParamout.CIParam != null)
                            {
                                if (!string.IsNullOrEmpty(oParamout.CIParam.TypeId) && !string.IsNullOrEmpty(oParamout.CIParam.ValueId))
                                {
                                    _CIPARAM = oParamout.CIParam;

                                    labRUT.Text = _CIPARAM.ValueId;
                                    labNombre.Text = _CIPARAM.Name;
                                    labFNac.Text = _CIPARAM.BirthDate;
                                    labNacionalidad.Text = _CIPARAM.Nacionality;
                                    labSexo.Text = _CIPARAM.Sex;
                                    labFVto.Text = _CIPARAM.ExpirationDate;
                                    LOG.Debug("CIUIAdapter.timer_VerifyCedula_Tick Cedula leida => " +
                                        _CIPARAM.ValueId + "-" +
                                        _CIPARAM.Name + "-" +
                                        _CIPARAM.BirthDate + "-" +
                                        _CIPARAM.Nacionality + "-" +
                                        _CIPARAM.Sex + "-" +
                                        _CIPARAM.ExpirationDate + "-" +
                                        _CIPARAM.Serial);

                                    bool _Vencida = false;
                                    if (IsCedulaVigente(_CIPARAM.ExpirationDate))
                                    {
                                        LOG.Debug("CIUIAdapter.timer_VerifyCedula_Tick Cedula Vidente!");
                                        labVigencia.Text = "Cedula VIGENTE";
                                        labVigencia.ForeColor = Color.GreenYellow;
                                    } else
                                    {
                                        LOG.Debug("CIUIAdapter.timer_VerifyCedula_Tick Cedula NO Vigente!");
                                        labVigencia.Text = "Cedula NO VIGENTE";
                                        labVigencia.ForeColor = Color.Red;
                                        labFVto.ForeColor = Color.Red;
                                        _Vencida = true;
                                        if (Properties.Settings.Default.CIBlockCedulaNoVigente)
                                        {
                                            _CURRENT_STEP = 22;
                                            LabRutTitle.ForeColor = Color.Red;
                                            labRUT.ForeColor = Color.Red;
                                            LOG.Debug("CIUIAdapter.timer_VerifyCedula_Tick Cédula No Viegente. FVto = " + _CIPARAM.ExpirationDate);
                                            err = "-CI108 - Cédula No Viegente. FVto = " + _CIPARAM.ExpirationDate;
                                        }
                                    }
                                    labVigencia.Visible = true;

                                    //Solo sigo si no bloquea ante cedula vencida este o no vencida 
                                    if (!Properties.Settings.Default.CIBlockCedulaNoVigente)
                                    {
                                        //Chequeo que la cedula leida es de la persona que se pidio validad x el rut
                                        if (!_CIPARAM.ValueId.Equals(dictParamIn["VALUEID"]))
                                        {
                                            _CURRENT_STEP = 22;
                                            LabRutTitle.ForeColor = Color.Red;
                                            labRUT.ForeColor = Color.Red;
                                            LOG.Debug("CIUIAdapter.timer_VerifyCedula_Tick RUT Pedido <> RUT Leido => " + dictParamIn["VALUEID"] + "!=" + _CIPARAM.ValueId);
                                            err = "-CI101 - RUT informado <> RUT Leido";
                                        }
                                        else
                                        {
                                            LabRutTitle.ForeColor = Color.Gray;
                                            labRUT.ForeColor = Color.Gray;
                                            _CURRENT_STEP = 21;
                                        }
                                    }
                                }
                                else
                                {
                                    err = "-CI102 - ValueId Null";
                                    _CURRENT_STEP = 22;
                                }
                            }
                            else
                            {
                                err = "-CI103 - Retorno CIPARAM Nulo";
                                _CURRENT_STEP = 22;
                            }
                        }
                        else
                        {
                            err = "-CI104 - Retorno WS Nulo";
                            _CURRENT_STEP = 22;
                        }
                    }
                    else
                    {
                        err = "-CI105 - Retorno Error = " + ret.ToString();
                        _CURRENT_STEP = 22;
                    }
                }

            }
            catch (Exception ex)
            {
                err = "-CI106 - Desconocido"; 
                _CURRENT_STEP = 22;
                LOG.Error("CIUIAdapter.timer_VerifyCedula_Tick Error = " + ex.Message, ex);
            }
            ShowStepStatus(err);
            LOG.Info("CIUIAdapter.timer_VerifyCedula_Tick OUT!");
            labCaturaCedula.Enabled = true;
        }

        private void ShowProgress()
        {
            try
            {
                pbProcess.Visible = true;
                pbProcess.Value = 0;
                pbProcess.Refresh();
                while (_MUSTRUN)
                {
                    pbProcess.Value += 1;
                    pbProcess.Refresh();
                }
                pbProcess.Visible = false;
            }
            catch (Exception ex)
            {

            }
        }

        private bool IsCedulaVigente(string expirationDate)
        {
            bool ret;
            try
            {
                LOG.Debug("CIUIAdapter.IsCedulaVigente IN - expirationDate = " + expirationDate);
                DateTime dt = DateTime.Parse(expirationDate);
                LOG.Debug("CIUIAdapter.IsCedulaVigente Now = " + DateTime.Now.ToString("dd/MM/yyyy"));
                ret = (DateTime.Now < dt);
                LOG.Debug("CIUIAdapter.IsCedulaVigente => " + ret.ToString());
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("CIUIAdapter.IsCedulaVigente Error = " + ex.Message, ex);
            }
            return ret;
        }

        private void timer_GeneratingCI_Tick(object sender, EventArgs e)
        {
            try
            {
                timer_GeneratingCI.Enabled = false;
                LOG.Info("CIUIAdapter.timer_GeneratingCI_Tick IN...");

                //picQR.Image = Properties.Resources.QR_Example;
                //labTrackId.Text = "2gf32hg32f2xGDo88687ass876sda87a8d78";
                //lab3_Info.Text = "Identidad Certificada!!";
                //lab3_Info.Refresh();
                //picPDF.Visible = true;

                CertificaIDentidad();
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.timer_GeneratingCI_Tick Error = " + ex.Message, ex);
            }
            LOG.Info("CIUIAdapter.timer_GeneratingCI_Tick OUT!");

            //labCapturaSelfie.Enabled = true;
        }

        private void CertificaIDentidad()
        {
            string err = "";
            try
            {
                LOG.Info("CIUIAdapter.timer_GeneratingCI_Tick IN...");
                if (string.IsNullOrEmpty(_ImageCedulaB64) || string.IsNullOrEmpty(_ImageSelfieB64))
                {
                    LOG.Debug("CIUIAdapter.timer_GeneratingCI_Tick _ImageCedulaB64 = " + (string.IsNullOrEmpty(_ImageCedulaB64) ? "NULL" : _ImageCedulaB64));
                    LOG.Debug("CIUIAdapter.timer_GeneratingCI_Tick _ImageSelfieB64 = " + (string.IsNullOrEmpty(_ImageSelfieB64) ? "NULL" : _ImageSelfieB64));
                    LOG.Warn("CIUIAdapter.timer_GeneratingCI_Tick Retorno => _ImageCedulaB64 y/o Selfie nulas");
                    return;
                }

                labCertificaIdentidad.Enabled = false;
                string xmlparamin, xmlparamout;

                using (WSCI.WSCI ws = new WSCI.WSCI())
                {
                    ws.Url = Properties.Settings.Default.CIUIAdapter_WSCI_WSCI;
                    LOG.Debug("CIUIAdapter.timer_GeneratingCI_Tick WSCI Url = " + ws.Url);
                    ws.Timeout = Properties.Settings.Default.CIWSTimeout;
                    LOG.Debug("CIUIAdapter.timer_GeneratingCI_Tick WSCI Timeout = " + ws.Timeout);
                    XmlParamIn oParamin = new XmlParamIn();
                    oParamin.Actionid = 3;
                    oParamin.Origin = 1;
                    oParamin.Companyid = 1;
                    oParamin.ApplicationId = Properties.Settings.Default.NVCIAppId;
                    oParamin.Clientid = "3D-4F-5G-7E-9U";
                    if (_CIPARAM != null)
                    {
                        oParamin.CIParam = _CIPARAM;
                        //oParamin.CIParam.GeoRef = "-33.3513061452878,-70.5044428392026";
                        oParamin.CIParam.IDCardImage = _ImageCedulaB64;
                        oParamin.CIParam.Selfie = _ImageSelfieB64;
                        oParamin.CIParam.DestinaryMail = labMail.Text; // string.IsNullOrEmpty(txtMail.Text) ? "gsuhit@gmail.com" : txtMail.Text;
                    }

                    xmlparamin = Bio.Digital.Receipt.Api.XmlUtils.SerializeAnObject(oParamin);
                    LOG.Debug("CIUIAdapter.timer_GeneratingCI_Tick WSCI xmlparamin = " + xmlparamin);
                    LOG.Info("CIUIAdapter.timer_GeneratingCI_Tick Calling ws => " + ws.Url);

                    labProcesando.Visible = true;
                    labProcesando.Refresh();
                    int ret = ws.Process(xmlparamin, out xmlparamout);
                    labProcesando.Visible = false;

                    LOG.Info("CIUIAdapter.timer_GeneratingCI_Tick ret = " + ret);
                    LOG.Debug("CIUIAdapter.timer_GeneratingCI_Tick ret = " + ret + " => xmlparamout = " + (string.IsNullOrEmpty(xmlparamout) ? "NULL" : xmlparamout));
                    if (ret == 0)
                    {
                        if (!string.IsNullOrEmpty(xmlparamout))
                        {
                            Bio.Digital.Receipt.Api.XmlParamOut oParamout = Bio.Digital.Receipt.Api.XmlUtils.DeserializeObject<Bio.Digital.Receipt.Api.XmlParamOut>(xmlparamout);
                            if (!string.IsNullOrEmpty(oParamout.QR))
                            {
                                if (Properties.Settings.Default.CISaveDE)
                                {
                                    try
                                    {
                                        string _SUFIJO = DateTime.Now.ToString("yyyyMMddHHmmss");
                                        string path = Properties.Settings.Default.CIDirTemp + "CI_" + dictParamIn["VALUEID"] + "_" + _SUFIJO + ".pdf";
                                        _pathPDF = path;
                                        LOG.Info("CIUIAdapter.timer_GeneratingCI_Tick Salvando PDF en = " + path); 
                                        System.IO.File.WriteAllBytes(path, Convert.FromBase64String(oParamout.CIParam.CertificatePDF));

                                        path = Properties.Settings.Default.CIDirTemp + "CI_" + dictParamIn["VALUEID"] + "_" + _SUFIJO + ".xml";
                                        _pathXML = path;
                                        LOG.Info("CIUIAdapter.timer_GeneratingCI_Tick Salvando Recibo en = " + path);
                                        System.IO.File.WriteAllText(path,oParamout.Receipt);

                                        picPDF.Visible = true;
                                        picXML.Visible = true;
                                    }
                                    catch (Exception ex)
                                    {
                                        LOG.Error("CIUIAdapter.timer_GeneratingCI_Tick Error grabando PDF en disto = " + ex.Message, ex);
                                    }
                                }
                                Bitmap QRbmp = null;
                                byte[] byImgQR = Convert.FromBase64String(oParamout.QR);
                                System.IO.MemoryStream m = new System.IO.MemoryStream(byImgQR);
                                QRbmp = new Bitmap(System.Drawing.Image.FromStream(m));
                                m.Close();
                                picQR.Image = QRbmp;
                                picQR.Visible = true;
                                picCIOK.Visible = true;
                                labTrackId.Text = oParamout.CIParam.TrackId;
                                labTrackId.Visible = true;
                                label5.Visible = true;
                                RDEnvelope = oParamout.Receipt;
                                TrackIdCI = oParamout.CIParam.TrackId;
                                LOG.Debug("CIUIAdapter.timer_GeneratingCI_Tick TrackIdCI = " + TrackIdCI + " - RDEnvelope = " + RDEnvelope);
                                HaveData = true;

                                labMsgGuia.ForeColor = Color.Gray;
                                labMsgGuia.Text = "Paso 3 - Certificación de Identidad Realizada con Éxito. Felicidades!!";
                                labMsgGuia.Refresh();

                                this.BackgroundImage = Properties.Resources.FondoCI_P3_OK;
                                this.Refresh();

                                _CurrentError = 0;
                                _sCurrentError = "";

                                //Si es 0 => No cierra automatico
                                if (Properties.Settings.Default.CloseUITime != 0)
                                {
                                    //Si es -1 => pongo 100 milisegundos para que sea casi inmediato
                                    if (Properties.Settings.Default.CloseUITime == -1)
                                    {
                                        timer_Close.Interval = 100;
                                    }
                                    else //Sino pongo el tiempo seteado en milisegundos
                                    {
                                        timer_Close.Interval = Properties.Settings.Default.CloseUITime;
                                    }
                                    timer_Close.Enabled = true;
                                }
                            } else
                            {
                                err = "CI110 - QR Auditoria Nulo";
                                labMsgGuia.ForeColor = Color.Red;
                                labMsgGuia.Text = "Paso 3 - Error Certificando Identidad [" + err + "]";
                                labMsgGuia.Refresh();

                                _CurrentError = -42;
                                _sCurrentError = "Paso 3 - Error Certificando Identidad [" + err + "]";

                                labCertificaIdentidad.Enabled = true;
                            }
                        } else
                        {
                            err = "-CI111 - Param Retorno nulo";
                            labMsgGuia.ForeColor = Color.Red;
                            labMsgGuia.Text = "Paso 3 - Error Certificando Identidad [" + err + "]";
                            labMsgGuia.Refresh();
                            _CurrentError = -42;
                            _sCurrentError = "Paso 3 - Error Certificando Identidad [" + err + "]";
                            labCertificaIdentidad.Enabled = true;
                        }
                    }
                    else
                    {
                        err = "-CI112 - Retorno Error [" + ret.ToString() + "]";
                        labMsgGuia.ForeColor = Color.Red;
                        labMsgGuia.Text = "Paso 3 - Error Certificando Identidad [" + err + "]";
                        labMsgGuia.Refresh();
                        _CurrentError = -42;
                        _sCurrentError = "Paso 3 - Error Certificando Identidad [" + err + "]";
                        labCertificaIdentidad.Enabled = true;
                        //rtbResultado.Text = "GenerateCI WS => Error = " + ret.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                err = "-CI106 - Desconocido";
                labMsgGuia.ForeColor = Color.Red;
                labMsgGuia.Text = "Paso 3 - Error Certigficando Identidad [" + err + "]";
                labMsgGuia.Refresh();

                _CurrentError = -42;
                _sCurrentError = "Error Certificando Identidad [" + err + " - " + ex.Message + "]";

                LOG.Error("CIUIAdapter.timer_GeneratingCI_Tick Error = " + ex.Message, ex);
                labCertificaIdentidad.Enabled = true;
            }
            LOG.Info("CIUIAdapter.timer_GeneratingCI_TickOUT!");
        }



        #region Utils

        private string GetB64FromPictureBox(PictureBox pic)
        {
            return ImageToBase64(pic.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        public string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            string base64String = null;
            LOG.Debug("CIUIAdapter.ImageToBase64 IN...");
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    image.Save(ms, format);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    base64String = Convert.ToBase64String(imageBytes);
                    LOG.Debug("CIUIAdapter.ImageToBase64 base64String = " + base64String);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.ImageToBase64 Error = " + ex.Message, ex);
                base64String = null;
            }
            LOG.Debug("CIUIAdapter.ImageToBase64 OUT!");
            return base64String;
        }


        #endregion Utils

        private void labClose_Click(object sender, EventArgs e)
        {
            LOG.Info("CIUIAdapter.labClose_Click Clicked");
            bool statusClose = (!string.IsNullOrEmpty(RDEnvelope)); 
            CloseControl(statusClose);
        }

        private void labCaturaCedula_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("CIUIAdapter.labCaturaCedula_Click IN...");
                labCaturaCedula.Enabled = false;
                _CAPTURING = 0;
                //_CURRENT_STEP = 21;
                imageCedula.Image = imageCam.Image;
                imageCedula.Visible = true;
                _ImageCedulaB64 = GetB64FromPictureBox(imageCedula);
                LOG.Debug("CIUIAdapter.labCaturaCedula_Click _ImageCedulaB64 = " + _ImageCedulaB64);
                //btnCapturaCedula.Enabled = false;
                labMsgGuia.Text = "Verificando Cédula en línea! Espere...";
                labMsgGuia.Refresh();

                timer_VerifyCedula.Enabled = true;
            }
            catch (Exception ex)
            {
                _CURRENT_STEP = 22;
                _CurrentError = -22;
                _sCurrentError = "Error Capturando Cedula [" + ex.Message + "]";
                LOG.Error("CIUIAdapter.labCaturaCedula_Click Error = " + ex.Message, ex);
            }
            LOG.Info("CIUIAdapter.labCaturaCedula_Click OUT!");
            //StopCamStream();
            //ShowStepStatus();
        }

        private void labCapturaSelfie_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("CIUIAdapter.labCapturaSelfie_Click IN...");
                //labCapturaSelfie.Enabled = false;
                _CAPTURING = 0;
                imageSelfie.Image = imageCam.Image;
                imageSelfie.Visible = true;
                _ImageSelfieB64 = GetB64FromPictureBox(imageSelfie);
                _CURRENT_STEP = 31;
                LOG.Debug("CIUIAdapter.labCapturaSelfie_Click _ImageSelfieB64 = " + _ImageSelfieB64);
                StopCamStream();
                ShowStepStatus();
                if (Properties.Settings.Default.AutomaticCertify)
                {
                    timer_GeneratingCI.Enabled = true;
                }
                else
                {
                    labCertificaIdentidad.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                _CURRENT_STEP = 32;
                _CurrentError = -32;
                _sCurrentError = "Error Capturando Selfie [" + ex.Message + "]";
                LOG.Error("CIUIAdapter.labCapturaSelfie_Click Error = " + ex.Message, ex);
            }
            LOG.Info("CIUIAdapter.labCapturaSelfie_Click OUT!");
        }

        private void labStartPreview_Click(object sender, EventArgs e)
        {
            LOG.Info("CIUIAdapter.labStartPreview_Click IN...");
            try
            {

                if (!MailValid())
                {
                    return;
                }

                _CAPTURING = 1;
                //_CURRENT_STEP = 1;
                //ShowStepStatus();
                captureCam = new Capture();
                Application.Idle += ShowCamStream;

                if (_CURRENT_STEP == 0 || _CURRENT_STEP == 21 || _CURRENT_STEP == 22) //Si viene desde paso inicial o hubo error o no en lectura de cedula => Reinicio desde ahi
                {
                    _CURRENT_STEP = 1;
                    ShowStepStatus();
                    _ImageCedulaB64 = null;
                    _ImageSelfieB64 = null;
                    RDEnvelope = null;
                    TrackIdCI = null;
                    labCaturaCedula.Enabled = true;
                } 

                if (_CURRENT_STEP == 31 || _CURRENT_STEP == 31) //Si viene de error o no de lectura de selfie => Reinicio desde ahi
                {
                    _CURRENT_STEP = 21;
                    _ImageSelfieB64 = null;
                    RDEnvelope = null;
                    TrackIdCI = null;
                    ShowStepStatus();
                    labCapturaSelfie.Enabled = true;
                }

                if (_CURRENT_STEP > 32 || _CURRENT_STEP == 4) //Si retomo desde final con o sin error => Reinicio captura de selfie
                {
                    _CURRENT_STEP = 21;
                    _ImageSelfieB64 = null;
                    RDEnvelope = null;
                    TrackIdCI = null;
                    ShowStepStatus();
                    labCapturaSelfie.Enabled = true;
                }
                labStopPreview.Enabled = true;
                labStartPreview.Enabled = false;
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.labStartPreview_Click Error = " + ex.Message, ex);
                labCaturaCedula.Enabled = false;
            }
            LOG.Info("CIUIAdapter.labStartPreview_Click OUT!");
        }

        private bool MailValid()
        {
            bool ret = false;
            LOG.Debug("CIUIAdapter.MailNotValid IN...");
            try
            {
                var addr = new System.Net.Mail.MailAddress(labMail.Text);
                ret = (addr.Address == labMail.Text);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("CIUIAdapter.MailNotValid Error = " + ex.Message, ex);
            }
            LOG.Debug("CIUIAdapter.MailNotValid OUT!");
            return ret;
        }

        private void labStopPreview_Click(object sender, EventArgs e)
        {
            //_CAPTURING = 1;
            //_CURRENT_STEP = 1;
            //ShowStepStatus();
            StopCamStream();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void labVigencia_Click(object sender, EventArgs e)
        {

        }

        private void labCertificaIdentidad_Click(object sender, EventArgs e)
        {
            LOG.Info("CIUIAdapter.labCertificaIdentidad_Click Clicked");
            labMsgGuia.ForeColor = Color.Gray;
            labMsgGuia.Text = "Selfie Caturada!" + Environment.NewLine + "Paso 3 -  Certificando Identidad con Notario Virtual. Espere...";
            labMsgGuia.Refresh();
            CertificaIDentidad();
        }

        private void labReInit_Click(object sender, EventArgs e)
        {
            LOG.Info("CIUIAdapter.labReInit_Click Clicked");
            Init();
        }

        private void CloseControl(bool hasCapture)
        {
            LOG.Info("CIUIAdapter.CloseControl In...");
            try
            {
                if (bioPacketLocal == null)
                {
                    LOG.Debug("CIUIAdapter.CloseControl Creando bioPacketLocal...");
                    bioPacketLocal = new BioPacket();
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
                }

                if (_CurrentError != 0 || !hasCapture)  //Si hay error o no completo todos los datos
                {
                    bioPacketLocal.PacketParamOut.Add("TrackIdCI", null);
                    bioPacketLocal.PacketParamOut.Add("CI", null);
                    bioPacketLocal.PacketParamOut.Add("DeviceId", DeviceId);
                    bioPacketLocal.PacketParamOut.Add("Error", _CurrentError.ToString() + "|" + _sCurrentError);
                    LOG.Debug("CIUIAdapter.CloseControl VAlores devueltos => TrackIdCI = null | CI = null | DeviceId = " + DeviceId + " | Error = " + _CurrentError.ToString() + "|" + _sCurrentError);
                }
                else
                {
                    bioPacketLocal.PacketParamOut.Add("TrackIdCI", TrackIdCI);
                    bioPacketLocal.PacketParamOut.Add("CI", RDEnvelope);
                    bioPacketLocal.PacketParamOut.Add("DeviceId", DeviceId);
                    bioPacketLocal.PacketParamOut.Add("Error", "0|No Error");
                    LOG.Debug("CIUIAdapter.CloseControl VAlores devueltos => TrackIdCI = " + TrackIdCI + "| CI = " + RDEnvelope + "| DeviceId = " + DeviceId + " |0|No Error");
                }
                HaveData = true;
                this.Close();
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.CloseControl Error Closing [" + ex.Message + "]");

            }

            LOG.Info("CIUIAdapter.CloseControl OUT!");

        }

        private void GetGeoRef()
        {
            try
            {
                GeoRef = Properties.Settings.Default.CIGeoRef;
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.GetGeoRef Error [" + ex.Message + "]");
            }
        }

        private void GetDeviceId()
        {
            try
            {
                DeviceId = GetMACAddress();
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.GetDeviceId Error [" + ex.Message + "]");
            }
        }

        public string GetMACAddress()
        {
            string sMacAddress = null;
            try
            {
                LOG.Debug("CIUIAdapter.GetMACAddress IN...");
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                sMacAddress = string.Empty;
                foreach (NetworkInterface adapter in nics)
                {
                    if (sMacAddress == String.Empty)// only return MAC Address from first card
                    {
                        if (adapter.OperationalStatus == OperationalStatus.Up && (!adapter.Description.Contains("Virtual")
                            && !adapter.Description.Contains("Pseudo")))
                        {
                            if (adapter.GetPhysicalAddress().ToString() != "")
                            {
                                IPInterfaceProperties properties = adapter.GetIPProperties();
                                sMacAddress = adapter.GetPhysicalAddress().ToString();
                                LOG.Debug("CIUIAdapter.GetMACAddress MAC Tomada = " + sMacAddress);
                            }
                        }
                    }
                }
                if (sMacAddress == string.Empty)
                {
                    sMacAddress = Properties.Settings.Default.CIDeviceId;
                    LOG.Debug("CIUIAdapter.GetMACAddress MAC Tomada desde CIDeviceId = " + sMacAddress);
                }
                else
                {
                    string sAux = sMacAddress.Substring(0, 2);
                    int i = 2;
                    while (i < sMacAddress.Length)
                    {
                        sAux = sAux + "-" + sMacAddress.Substring(1, 2);
                        i = i + 2;
                    }
                    sMacAddress = sAux;
                    LOG.Debug("CIUIAdapter.GetMACAddress MAC formateada = " + sAux);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.GetMACAddress Error [" + ex.Message + "]");
            }
            
            return sMacAddress;
        }

        /// <summary>
        /// Chequea si existe el directorio de proceso temporal si FETargetVideoType = 0, sino, si es 1, 
        /// es Stream el destino.
        /// </summary>
        private void CheckTmpDirectory()
        {
            try
            {

                if (!Directory.Exists(Properties.Settings.Default.CIDirTemp))
                {
                    DirectoryInfo di = Directory.CreateDirectory(Properties.Settings.Default.CIDirTemp);
                    if (di != null)
                    {
                        LOG.Info("CIUIAdapter.CheckTmpDirectory Se creo directorio temporal " + Properties.Settings.Default.CIDirTemp);
                    }
                    else
                    {
                        LOG.Warn("CIUIAdapter.CheckTmpDirectory Atencion!! Problemas en la creación del directorio temporal " + Properties.Settings.Default.CIDirTemp);
                    }
                }
                else
                {
                    LOG.Info("CIUIAdapter.CheckTmpDirectory Directorio temporal OK - " + Properties.Settings.Default.CIDirTemp);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.CheckTmpDirectory Error [" + ex.Message + "]");
            }
        }

        private void timer_Close_Tick(object sender, EventArgs e)
        {
            LOG.Info("CIUIAdapter.timer_Close_Tick In...");
            timer_Close.Enabled = false;
            CloseControl(HaveData);
        }

        private void LabRutTitle_Click(object sender, EventArgs e)
        {

        }

        private void picPDF_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Debug("CIUIAdapter.picPDF_Click IN... Path PDF = " + _pathPDF);

                if (!string.IsNullOrEmpty(_pathPDF))
                {
                    System.Diagnostics.Process.Start(_pathPDF);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.picPDF_Click Error [" + ex.Message + "]");
            }
            LOG.Debug("CIUIAdapter.picPDF_Click Out!");
        }

        private void picXML_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Debug("CIUIAdapter.picXML_Click IN... Path PDF = " + _pathXML);

                if (!string.IsNullOrEmpty(_pathXML))
                {
                    System.Diagnostics.Process.Start(_pathXML);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.picXML_Click Error [" + ex.Message + "]");
            }
            LOG.Debug("CIUIAdapter.picXML_Click Out!");
        }

        private void process1_Exited(object sender, EventArgs e)
        {

        }

        private void timer_ShowProgress_Tick(object sender, EventArgs e)
        {
            try
            {
                if (pbProcess.Value == 100)
                {
                    pbProcess.Value = 0;
                    pbProcess.Refresh();
                }
                pbProcess.Value += 1;
                pbProcess.Refresh();
            }
            catch (Exception ex)
            {

            }
        }

        private void labMail_Leave(object sender, EventArgs e)
        {
            if (!MailValid())
            {
                labMail.ForeColor = Color.White;
                labMail.BackColor = Color.Red;
                labMail.Focus();
            } else
            {
                labMail.ForeColor = Color.Black;
                labMail.BackColor = Color.WhiteSmoke;
            }


        }
    }
}
