﻿namespace CIUIAdapter
{
    partial class CIUIAdapter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CIUIAdapter));
            this.imageCam = new System.Windows.Forms.PictureBox();
            this.labSexo = new System.Windows.Forms.Label();
            this.labNacionalidad = new System.Windows.Forms.Label();
            this.labFNac = new System.Windows.Forms.Label();
            this.labNombre = new System.Windows.Forms.Label();
            this.labRUT = new System.Windows.Forms.Label();
            this.imageCedula = new System.Windows.Forms.PictureBox();
            this.imageSelfie = new System.Windows.Forms.PictureBox();
            this.timer_VerifyCedula = new System.Windows.Forms.Timer(this.components);
            this.picQR = new System.Windows.Forms.PictureBox();
            this.timer_GeneratingCI = new System.Windows.Forms.Timer(this.components);
            this.labTrackId = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labClose = new System.Windows.Forms.Label();
            this.labCaturaCedula = new System.Windows.Forms.Label();
            this.labCapturaSelfie = new System.Windows.Forms.Label();
            this.labStartPreview = new System.Windows.Forms.Label();
            this.labStopPreview = new System.Windows.Forms.Label();
            this.picPDF = new System.Windows.Forms.PictureBox();
            this.labVigencia = new System.Windows.Forms.Label();
            this.labFVto = new System.Windows.Forms.Label();
            this.labCertificaIdentidad = new System.Windows.Forms.Label();
            this.LabRutTitle = new System.Windows.Forms.Label();
            this.labReInit = new System.Windows.Forms.Label();
            this.labMail = new System.Windows.Forms.TextBox();
            this.picLogo3ro = new System.Windows.Forms.PictureBox();
            this.timer_Close = new System.Windows.Forms.Timer(this.components);
            this.labMsgGuia = new System.Windows.Forms.Label();
            this.picXML = new System.Windows.Forms.PictureBox();
            this.pbProcess = new System.Windows.Forms.ProgressBar();
            this.timer_ShowProgress = new System.Windows.Forms.Timer(this.components);
            this.labProcesando = new System.Windows.Forms.Label();
            this.labHelp = new System.Windows.Forms.Label();
            this.labAbout = new System.Windows.Forms.Label();
            this.picCIOK = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCedula)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageSelfie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPDF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo3ro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picXML)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCIOK)).BeginInit();
            this.SuspendLayout();
            // 
            // imageCam
            // 
            this.imageCam.BackColor = System.Drawing.Color.Transparent;
            this.imageCam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageCam.Location = new System.Drawing.Point(149, 203);
            this.imageCam.Name = "imageCam";
            this.imageCam.Size = new System.Drawing.Size(556, 307);
            this.imageCam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageCam.TabIndex = 0;
            this.imageCam.TabStop = false;
            // 
            // labSexo
            // 
            this.labSexo.AutoSize = true;
            this.labSexo.BackColor = System.Drawing.Color.Transparent;
            this.labSexo.ForeColor = System.Drawing.Color.DimGray;
            this.labSexo.Location = new System.Drawing.Point(1079, 275);
            this.labSexo.Name = "labSexo";
            this.labSexo.Size = new System.Drawing.Size(16, 13);
            this.labSexo.TabIndex = 21;
            this.labSexo.Text = "M";
            // 
            // labNacionalidad
            // 
            this.labNacionalidad.AutoSize = true;
            this.labNacionalidad.BackColor = System.Drawing.Color.Transparent;
            this.labNacionalidad.ForeColor = System.Drawing.Color.DimGray;
            this.labNacionalidad.Location = new System.Drawing.Point(1079, 259);
            this.labNacionalidad.Name = "labNacionalidad";
            this.labNacionalidad.Size = new System.Drawing.Size(30, 13);
            this.labNacionalidad.TabIndex = 19;
            this.labNacionalidad.Text = "ARG";
            // 
            // labFNac
            // 
            this.labFNac.AutoSize = true;
            this.labFNac.BackColor = System.Drawing.Color.Transparent;
            this.labFNac.ForeColor = System.Drawing.Color.DimGray;
            this.labFNac.Location = new System.Drawing.Point(1078, 244);
            this.labFNac.Name = "labFNac";
            this.labFNac.Size = new System.Drawing.Size(65, 13);
            this.labFNac.TabIndex = 17;
            this.labFNac.Text = "28/09/1969";
            // 
            // labNombre
            // 
            this.labNombre.BackColor = System.Drawing.Color.Transparent;
            this.labNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNombre.ForeColor = System.Drawing.Color.DimGray;
            this.labNombre.Location = new System.Drawing.Point(1076, 226);
            this.labNombre.Name = "labNombre";
            this.labNombre.Size = new System.Drawing.Size(266, 17);
            this.labNombre.TabIndex = 15;
            this.labNombre.Text = "Gustavo Gerardo Suhit Gallucci";
            // 
            // labRUT
            // 
            this.labRUT.AutoSize = true;
            this.labRUT.BackColor = System.Drawing.Color.Transparent;
            this.labRUT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labRUT.ForeColor = System.Drawing.Color.DimGray;
            this.labRUT.Location = new System.Drawing.Point(1079, 212);
            this.labRUT.Name = "labRUT";
            this.labRUT.Size = new System.Drawing.Size(64, 13);
            this.labRUT.TabIndex = 13;
            this.labRUT.Text = "21284415-2";
            // 
            // imageCedula
            // 
            this.imageCedula.BackColor = System.Drawing.Color.Transparent;
            this.imageCedula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageCedula.Image = global::CIUIAdapter.Properties.Resources.dibujoCI_2_BN;
            this.imageCedula.Location = new System.Drawing.Point(818, 209);
            this.imageCedula.Name = "imageCedula";
            this.imageCedula.Size = new System.Drawing.Size(115, 80);
            this.imageCedula.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageCedula.TabIndex = 1;
            this.imageCedula.TabStop = false;
            this.imageCedula.Visible = false;
            // 
            // imageSelfie
            // 
            this.imageSelfie.BackColor = System.Drawing.Color.Transparent;
            this.imageSelfie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageSelfie.Image = global::CIUIAdapter.Properties.Resources.selfie1_2;
            this.imageSelfie.Location = new System.Drawing.Point(818, 333);
            this.imageSelfie.Name = "imageSelfie";
            this.imageSelfie.Size = new System.Drawing.Size(115, 80);
            this.imageSelfie.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageSelfie.TabIndex = 2;
            this.imageSelfie.TabStop = false;
            this.imageSelfie.Visible = false;
            // 
            // timer_VerifyCedula
            // 
            this.timer_VerifyCedula.Tick += new System.EventHandler(this.timer_VerifyCedula_Tick);
            // 
            // picQR
            // 
            this.picQR.BackColor = System.Drawing.Color.Transparent;
            this.picQR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picQR.Image = global::CIUIAdapter.Properties.Resources.QR;
            this.picQR.Location = new System.Drawing.Point(1208, 440);
            this.picQR.Name = "picQR";
            this.picQR.Size = new System.Drawing.Size(93, 96);
            this.picQR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picQR.TabIndex = 3;
            this.picQR.TabStop = false;
            this.picQR.Visible = false;
            // 
            // timer_GeneratingCI
            // 
            this.timer_GeneratingCI.Interval = 200;
            this.timer_GeneratingCI.Tick += new System.EventHandler(this.timer_GeneratingCI_Tick);
            // 
            // labTrackId
            // 
            this.labTrackId.BackColor = System.Drawing.Color.Transparent;
            this.labTrackId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTrackId.ForeColor = System.Drawing.Color.Gray;
            this.labTrackId.Location = new System.Drawing.Point(959, 460);
            this.labTrackId.Name = "labTrackId";
            this.labTrackId.Size = new System.Drawing.Size(218, 69);
            this.labTrackId.TabIndex = 15;
            this.labTrackId.Text = "?";
            this.labTrackId.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gray;
            this.label5.Location = new System.Drawing.Point(955, 440);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 20);
            this.label5.TabIndex = 14;
            this.label5.Text = "Track ID ";
            this.label5.Visible = false;
            // 
            // labClose
            // 
            this.labClose.BackColor = System.Drawing.Color.Transparent;
            this.labClose.Location = new System.Drawing.Point(1290, 29);
            this.labClose.Name = "labClose";
            this.labClose.Size = new System.Drawing.Size(28, 27);
            this.labClose.TabIndex = 15;
            this.labClose.Click += new System.EventHandler(this.labClose_Click);
            // 
            // labCaturaCedula
            // 
            this.labCaturaCedula.BackColor = System.Drawing.Color.Transparent;
            this.labCaturaCedula.Enabled = false;
            this.labCaturaCedula.Location = new System.Drawing.Point(138, 526);
            this.labCaturaCedula.Name = "labCaturaCedula";
            this.labCaturaCedula.Size = new System.Drawing.Size(188, 32);
            this.labCaturaCedula.TabIndex = 22;
            this.labCaturaCedula.Click += new System.EventHandler(this.labCaturaCedula_Click);
            // 
            // labCapturaSelfie
            // 
            this.labCapturaSelfie.BackColor = System.Drawing.Color.Transparent;
            this.labCapturaSelfie.Enabled = false;
            this.labCapturaSelfie.Location = new System.Drawing.Point(332, 526);
            this.labCapturaSelfie.Name = "labCapturaSelfie";
            this.labCapturaSelfie.Size = new System.Drawing.Size(192, 33);
            this.labCapturaSelfie.TabIndex = 23;
            this.labCapturaSelfie.Click += new System.EventHandler(this.labCapturaSelfie_Click);
            // 
            // labStartPreview
            // 
            this.labStartPreview.BackColor = System.Drawing.Color.Transparent;
            this.labStartPreview.Location = new System.Drawing.Point(635, 156);
            this.labStartPreview.Name = "labStartPreview";
            this.labStartPreview.Size = new System.Drawing.Size(23, 25);
            this.labStartPreview.TabIndex = 24;
            this.labStartPreview.Click += new System.EventHandler(this.labStartPreview_Click);
            // 
            // labStopPreview
            // 
            this.labStopPreview.BackColor = System.Drawing.Color.Transparent;
            this.labStopPreview.Enabled = false;
            this.labStopPreview.Location = new System.Drawing.Point(694, 156);
            this.labStopPreview.Name = "labStopPreview";
            this.labStopPreview.Size = new System.Drawing.Size(23, 25);
            this.labStopPreview.TabIndex = 25;
            this.labStopPreview.Click += new System.EventHandler(this.labStopPreview_Click);
            // 
            // picPDF
            // 
            this.picPDF.BackColor = System.Drawing.Color.Transparent;
            this.picPDF.Image = global::CIUIAdapter.Properties.Resources.pdf_blue;
            this.picPDF.Location = new System.Drawing.Point(1100, 539);
            this.picPDF.Name = "picPDF";
            this.picPDF.Size = new System.Drawing.Size(35, 40);
            this.picPDF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPDF.TabIndex = 26;
            this.picPDF.TabStop = false;
            this.picPDF.Visible = false;
            this.picPDF.Click += new System.EventHandler(this.picPDF_Click);
            // 
            // labVigencia
            // 
            this.labVigencia.BackColor = System.Drawing.Color.Transparent;
            this.labVigencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labVigencia.ForeColor = System.Drawing.Color.Green;
            this.labVigencia.Location = new System.Drawing.Point(797, 290);
            this.labVigencia.Name = "labVigencia";
            this.labVigencia.Size = new System.Drawing.Size(157, 18);
            this.labVigencia.TabIndex = 27;
            this.labVigencia.Text = "Cédula Vigente!";
            this.labVigencia.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labVigencia.Visible = false;
            this.labVigencia.Click += new System.EventHandler(this.labVigencia_Click);
            // 
            // labFVto
            // 
            this.labFVto.AutoSize = true;
            this.labFVto.BackColor = System.Drawing.Color.Transparent;
            this.labFVto.ForeColor = System.Drawing.Color.DimGray;
            this.labFVto.Location = new System.Drawing.Point(1079, 290);
            this.labFVto.Name = "labFVto";
            this.labFVto.Size = new System.Drawing.Size(65, 13);
            this.labFVto.TabIndex = 29;
            this.labFVto.Text = "22/07/2022";
            // 
            // labCertificaIdentidad
            // 
            this.labCertificaIdentidad.BackColor = System.Drawing.Color.Transparent;
            this.labCertificaIdentidad.Enabled = false;
            this.labCertificaIdentidad.Location = new System.Drawing.Point(530, 526);
            this.labCertificaIdentidad.Name = "labCertificaIdentidad";
            this.labCertificaIdentidad.Size = new System.Drawing.Size(185, 32);
            this.labCertificaIdentidad.TabIndex = 30;
            this.labCertificaIdentidad.Click += new System.EventHandler(this.labCertificaIdentidad_Click);
            // 
            // LabRutTitle
            // 
            this.LabRutTitle.AutoSize = true;
            this.LabRutTitle.BackColor = System.Drawing.Color.Transparent;
            this.LabRutTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabRutTitle.ForeColor = System.Drawing.Color.Gray;
            this.LabRutTitle.Location = new System.Drawing.Point(231, 125);
            this.LabRutTitle.Name = "LabRutTitle";
            this.LabRutTitle.Size = new System.Drawing.Size(127, 25);
            this.LabRutTitle.TabIndex = 31;
            this.LabRutTitle.Text = "21284415-2";
            this.LabRutTitle.Click += new System.EventHandler(this.LabRutTitle_Click);
            // 
            // labReInit
            // 
            this.labReInit.BackColor = System.Drawing.Color.Transparent;
            this.labReInit.Location = new System.Drawing.Point(1073, 27);
            this.labReInit.Name = "labReInit";
            this.labReInit.Size = new System.Drawing.Size(32, 27);
            this.labReInit.TabIndex = 32;
            this.labReInit.Click += new System.EventHandler(this.labReInit_Click);
            // 
            // labMail
            // 
            this.labMail.BackColor = System.Drawing.Color.WhiteSmoke;
            this.labMail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMail.ForeColor = System.Drawing.Color.Black;
            this.labMail.Location = new System.Drawing.Point(236, 156);
            this.labMail.Name = "labMail";
            this.labMail.Size = new System.Drawing.Size(219, 26);
            this.labMail.TabIndex = 33;
            this.labMail.TextChanged += new System.EventHandler(this.labMail_Leave);
            this.labMail.Leave += new System.EventHandler(this.labMail_Leave);
            // 
            // picLogo3ro
            // 
            this.picLogo3ro.BackColor = System.Drawing.Color.Transparent;
            this.picLogo3ro.Image = global::CIUIAdapter.Properties.Resources.biometrika_big_T;
            this.picLogo3ro.Location = new System.Drawing.Point(323, 36);
            this.picLogo3ro.Margin = new System.Windows.Forms.Padding(2);
            this.picLogo3ro.Name = "picLogo3ro";
            this.picLogo3ro.Size = new System.Drawing.Size(200, 45);
            this.picLogo3ro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo3ro.TabIndex = 45;
            this.picLogo3ro.TabStop = false;
            // 
            // timer_Close
            // 
            this.timer_Close.Tick += new System.EventHandler(this.timer_Close_Tick);
            // 
            // labMsgGuia
            // 
            this.labMsgGuia.BackColor = System.Drawing.Color.Transparent;
            this.labMsgGuia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMsgGuia.ForeColor = System.Drawing.Color.Gray;
            this.labMsgGuia.Location = new System.Drawing.Point(184, 592);
            this.labMsgGuia.Name = "labMsgGuia";
            this.labMsgGuia.Size = new System.Drawing.Size(498, 63);
            this.labMsgGuia.TabIndex = 46;
            this.labMsgGuia.Text = "Inicie el proceso de Certificación de Idendtidad siguiendo los pasos propuestos.." +
    ".";
            // 
            // picXML
            // 
            this.picXML.BackColor = System.Drawing.Color.Transparent;
            this.picXML.Image = global::CIUIAdapter.Properties.Resources.xml_blue;
            this.picXML.Location = new System.Drawing.Point(1141, 539);
            this.picXML.Name = "picXML";
            this.picXML.Size = new System.Drawing.Size(35, 40);
            this.picXML.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picXML.TabIndex = 47;
            this.picXML.TabStop = false;
            this.picXML.Visible = false;
            this.picXML.Click += new System.EventHandler(this.picXML_Click);
            // 
            // pbProcess
            // 
            this.pbProcess.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pbProcess.Location = new System.Drawing.Point(304, 577);
            this.pbProcess.Name = "pbProcess";
            this.pbProcess.Size = new System.Drawing.Size(400, 2);
            this.pbProcess.Step = 1;
            this.pbProcess.TabIndex = 49;
            // 
            // timer_ShowProgress
            // 
            this.timer_ShowProgress.Tick += new System.EventHandler(this.timer_ShowProgress_Tick);
            // 
            // labProcesando
            // 
            this.labProcesando.AutoSize = true;
            this.labProcesando.BackColor = System.Drawing.Color.Transparent;
            this.labProcesando.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labProcesando.ForeColor = System.Drawing.Color.SteelBlue;
            this.labProcesando.Location = new System.Drawing.Point(479, 651);
            this.labProcesando.Name = "labProcesando";
            this.labProcesando.Size = new System.Drawing.Size(225, 25);
            this.labProcesando.TabIndex = 50;
            this.labProcesando.Text = "Procesando. Espere...";
            this.labProcesando.Visible = false;
            // 
            // labHelp
            // 
            this.labHelp.BackColor = System.Drawing.Color.Transparent;
            this.labHelp.Location = new System.Drawing.Point(1144, 27);
            this.labHelp.Name = "labHelp";
            this.labHelp.Size = new System.Drawing.Size(32, 29);
            this.labHelp.TabIndex = 51;
            // 
            // labAbout
            // 
            this.labAbout.BackColor = System.Drawing.Color.Transparent;
            this.labAbout.Location = new System.Drawing.Point(1218, 28);
            this.labAbout.Name = "labAbout";
            this.labAbout.Size = new System.Drawing.Size(32, 29);
            this.labAbout.TabIndex = 52;
            // 
            // picCIOK
            // 
            this.picCIOK.BackColor = System.Drawing.Color.Transparent;
            this.picCIOK.Image = global::CIUIAdapter.Properties.Resources.CI_OK1;
            this.picCIOK.Location = new System.Drawing.Point(818, 440);
            this.picCIOK.Name = "picCIOK";
            this.picCIOK.Size = new System.Drawing.Size(115, 82);
            this.picCIOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCIOK.TabIndex = 53;
            this.picCIOK.TabStop = false;
            this.picCIOK.Visible = false;
            // 
            // CIUIAdapter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CIUIAdapter.Properties.Resources.FondoCI_Init_0;
            this.ClientSize = new System.Drawing.Size(1367, 772);
            this.Controls.Add(this.picCIOK);
            this.Controls.Add(this.labAbout);
            this.Controls.Add(this.labHelp);
            this.Controls.Add(this.labProcesando);
            this.Controls.Add(this.pbProcess);
            this.Controls.Add(this.picXML);
            this.Controls.Add(this.labMsgGuia);
            this.Controls.Add(this.labFVto);
            this.Controls.Add(this.labNombre);
            this.Controls.Add(this.labRUT);
            this.Controls.Add(this.picLogo3ro);
            this.Controls.Add(this.labMail);
            this.Controls.Add(this.labReInit);
            this.Controls.Add(this.LabRutTitle);
            this.Controls.Add(this.labCertificaIdentidad);
            this.Controls.Add(this.labVigencia);
            this.Controls.Add(this.picPDF);
            this.Controls.Add(this.labStopPreview);
            this.Controls.Add(this.labStartPreview);
            this.Controls.Add(this.labCapturaSelfie);
            this.Controls.Add(this.labCaturaCedula);
            this.Controls.Add(this.labTrackId);
            this.Controls.Add(this.imageSelfie);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.labSexo);
            this.Controls.Add(this.picQR);
            this.Controls.Add(this.imageCam);
            this.Controls.Add(this.labNacionalidad);
            this.Controls.Add(this.labClose);
            this.Controls.Add(this.labFNac);
            this.Controls.Add(this.imageCedula);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CIUIAdapter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CIUIAdapter";
            this.Load += new System.EventHandler(this.CIUIAdapter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCedula)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageSelfie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPDF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo3ro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picXML)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCIOK)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox imageCam;
        private System.Windows.Forms.PictureBox imageCedula;
        private System.Windows.Forms.PictureBox imageSelfie;
        private System.Windows.Forms.Timer timer_VerifyCedula;
        private System.Windows.Forms.Label labRUT;
        private System.Windows.Forms.Label labSexo;
        private System.Windows.Forms.Label labNacionalidad;
        private System.Windows.Forms.Label labFNac;
        private System.Windows.Forms.Label labNombre;
        private System.Windows.Forms.Timer timer_GeneratingCI;
        private System.Windows.Forms.PictureBox picQR;
        private System.Windows.Forms.Label labTrackId;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labClose;
        private System.Windows.Forms.Label labCaturaCedula;
        private System.Windows.Forms.Label labCapturaSelfie;
        private System.Windows.Forms.Label labStartPreview;
        private System.Windows.Forms.Label labStopPreview;
        private System.Windows.Forms.PictureBox picPDF;
        private System.Windows.Forms.Label labVigencia;
        private System.Windows.Forms.Label labFVto;
        private System.Windows.Forms.Label labCertificaIdentidad;
        private System.Windows.Forms.Label LabRutTitle;
        private System.Windows.Forms.Label labReInit;
        private System.Windows.Forms.TextBox labMail;
        private System.Windows.Forms.PictureBox picLogo3ro;
        private System.Windows.Forms.Timer timer_Close;
        private System.Windows.Forms.Label labMsgGuia;
        private System.Windows.Forms.PictureBox picXML;
        private System.Windows.Forms.ProgressBar pbProcess;
        private System.Windows.Forms.Timer timer_ShowProgress;
        private System.Windows.Forms.Label labProcesando;
        private System.Windows.Forms.Label labHelp;
        private System.Windows.Forms.Label labAbout;
        private System.Windows.Forms.PictureBox picCIOK;
    }
}