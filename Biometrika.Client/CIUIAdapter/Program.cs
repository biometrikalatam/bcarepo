﻿using Domain;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Forms;

namespace CIUIAdapter
{
    //static class Program
    //{
    //    /// <summary>
    //    /// The main entry point for the application.
    //    /// </summary>
    //    [STAThread]
    //    static void Main()
    //    {
    //        Application.EnableVisualStyles();
    //        Application.SetCompatibleTextRenderingDefault(false);
    //        Application.Run(new Form1());
    //    }
    //}
    public static class Program
    {
        public static bool HaveData
        {
            get
            {
                return ciClientUI == null ? false : ciClientUI.HaveData;
            }
        }
        public static BioPacket Packet { get; set; }

        private static CIUIAdapter ciClientUI;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            ciClientUI = new CIUIAdapter();
            int ret = ciClientUI.Channel(Packet);
            if (ret == 0)
            {
                ciClientUI.Show();
                Application.Run(ciClientUI);
            }
            else
            {
                Packet.PacketParamOut.Add("Error", GetError(ret));
            }
        }

        private static string GetError(int ret)
        {
            string sret = null;
            switch (ret)
            {
                case -1:
                    sret = "-1|Error desconocido. Ver log";
                    break;
                case -2:
                    sret = "-2|Parametros nulos";
                    break;
                case -3:
                    sret = "-3|Parametros src/TypeId/ValueId denben existir y no pueden ser nulos";
                    break;
                default:
                    sret = "-1|Error desconocido. Ver log";
                    break;
            }
            return sret;
        }

        public static void SetPacket(NameValueCollection queryString)
        {
            Packet = new BioPacket();
            Packet.PacketParamIn = queryString;
            Packet.PacketParamOut = new Dictionary<string, object>();
        }

        public static void Close()
        {
            ciClientUI.Close();
            ciClientUI = null;
        }
    }
}
