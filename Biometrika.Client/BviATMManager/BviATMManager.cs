﻿using Domain;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BviATMManager
{
 
    public class BviATMManager : IManager
    {
        private bool isOpen = false;
        private Dictionary<string, object> output = new Dictionary<string, object>();
        private string templateToken = string.Empty;

        public BviATMManager() { } // ctor de reflextion

        public BviATMManager(NameValueCollection queryString)
        {

            if (!isOpen)
            {
                isOpen = true;
                BviATMUIAdapter.Program.SetPacket(queryString);     // el program recibe el pedido
                BviATMUIAdapter.Program.Main(); 

                while (!BviATMUIAdapter.Program.HaveData) { }

                Dictionary<string, object> outputUI = BviATMUIAdapter.Program.Packet.PacketParamOut;
                if (outputUI == null || outputUI.Count == 0)
                {
                    output.Add("ErrorNumber", "-5");
                    output.Add("Mensaje", "Retrono de BviATMUI Nulo");
                   
                } else
                {
                    output = BviATMUIAdapter.Program.Packet.PacketParamOut;
                }
            }
        }


        public string Buffer => new JavaScriptSerializer().Serialize(output);

        public string Tag { get => "BVIATM"; }

    }
}
