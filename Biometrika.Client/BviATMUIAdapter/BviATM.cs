﻿using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BviATMUIAdapter
{
    public class BviATM
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BviATM));

        bool _HayCapturaOK = false;

        int _OPERATION = 1;

        bool typeExecutionGUI = false;
        private System.Timers.Timer timerControlGeneral;
        public string SerialSensor { get; set; }
        private BioPacket bioPacketLocal;
        
        //public int Tokencontent { get; set; }
        public bool HaveData { get; set; }


        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        internal int Channel(BioPacket bioPacket)
        {
            _HayCapturaOK = false;
            bioPacketLocal = bioPacket;
            //if (bioPacketLocal.PacketParamOut == null)
            //    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            int ret = CheckParameters(bioPacketLocal);

            if (ret == 0)
            {
                if (bioPacketLocal.PacketParamOut == null)
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
               
            }
            return ret;
        }

        private int CheckParameters(BioPacket bioPacketLocal)
        {
            /*
                  src=BVIATM&Op=1
                o 
                  src=BVIATM&Op=2&
                  qr=ajhdgajhg...dajhgdjhags&
                  minutiaeISO=Rk1bsabdajs...akdjakdhaksjd&
                  dataComplete=true
            */
            int ret = 0;
            try
            {
                if (bioPacketLocal == null) ret = -2;  //Lista de parametros no puede ser nulo

                //Paso a Dictionary

                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    dictParamIn.Add(k, bioPacketLocal.PacketParamIn[k]);
                }

                if (!dictParamIn.ContainsKey("src") || String.IsNullOrEmpty(dictParamIn["src"]))
                {
                    ret = -3;  //src deben ser enviados como parámetros
                }
                else
                {
                    if (!dictParamIn.ContainsKey("Op") || String.IsNullOrEmpty(dictParamIn["Op"]))
                    {
                        if (!dictParamIn.ContainsKey("Op"))
                        {
                            dictParamIn.Add("Op", "1");  //Seteo lectura de QR
                        }
                        else
                        {
                            dictParamIn["Op"] = "1";  //Seteo lectura de QR
                        }
                    }
                    if (dictParamIn["Op"].Equals("2") && dictParamIn.Count < 4)
                    {
                        ret = -4; //Faltan parametros para MOC (Ademas de src+Op deben venir qr+minutiaeISO al menos. Si no viene dataComplete se pone false
                    } else
                    { 
                        if (!dictParamIn.ContainsKey("dataComplete") || String.IsNullOrEmpty(dictParamIn["dataComplete"]))
                        {
                            if (!dictParamIn.ContainsKey("dataComplete"))
                            {
                                dictParamIn.Add("dataComplete", "false");
                            }
                            else
                            {
                                dictParamIn["dataComplete"] = "false"; //RUT
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BviATM.CheckParameters [Error=" + ex.Message + "]");
                ret = -1;
            }
            return ret;
        }

        internal BioPacket Run()
        {
            bioPacketLocal = null;
            HaveData = false;
            try
            {
                switch (Convert.ToInt32(dictParamIn["Op"]))
                {
                    case 1:
                        bioPacketLocal = ReadQR();
                        break;

                    case 2:
                        bioPacketLocal = GetSample();
                        break;

                    case 3:
                        bioPacketLocal = MatchOnCard();
                        break;

                    default:
                        bioPacketLocal = ReadQR();
                        break;
                }
            }
            catch (Exception ex)
            {
                bioPacketLocal = null;
                LOG.Error("BviATM.Run Error = " + ex.Message);
            }
            return bioPacketLocal;
        }


#region OPERATIONS

        /// <summary>
        /// Realiza lectura del QR y retorna el BioPaket con los datos pedidos
        /// </summary>
        /// <returns></returns>
        private BioPacket ReadQR()
        {
            BioPacket bpLoc = new BioPacket();
            try
            {
                //Esqueleto a reemplazar por operacion real, con un random devolviendo lectura ok o error.
                Random r = new Random();
                int irandom = r.Next(2);
                bpLoc = new BioPacket();
                bpLoc.PacketParamOut = new Dictionary<string, object>();
                if (irandom == 0)
                {
                    bpLoc.PacketParamOut.Add("ErrorNumber", "0");
                    bpLoc.PacketParamOut.Add("Mensaje", null);
                    bpLoc.PacketParamOut.Add("QR", "https://portal.sidiv.registrocivil.cl/docstatus?RUN=21284415-2&type=CEDULA_EXT&serial=601014310&mrz=601014310469092862208028");
                    //ParseQR("https://portal.sidiv.registrocivil.cl/docstatus?RUN=21284415-2&type=CEDULA_EXT&serial=601014310&mrz=601014310469092862208028")
                    //Este parseo quiza ya venga desde el lector de CB, asi que aca lo dejo comentado
                    //RUN = 21284415 - 2 & 
                    //type = CEDULA_EXT & 
                    //serial = 601014310 & 
                    //mrz = 601014310 4 690928 6 220802 8"
                    bpLoc.PacketParamOut.Add("RUT", "21284415-2");
                    bpLoc.PacketParamOut.Add("Tipo", "CEDULA_EXT");
                    bpLoc.PacketParamOut.Add("Serial", "601014310");
                    bpLoc.PacketParamOut.Add("Fecha Nacimiento", "28-09-1969");
                    bpLoc.PacketParamOut.Add("Fecha Vencimiento", "02-08-2022");
                }
                else
                {
                    bpLoc.PacketParamOut.Add("ErrorNumber", "-2");
                    bpLoc.PacketParamOut.Add("Mensaje", "Explicacion del error");
                }
            }
            catch (Exception ex)
            {
                bpLoc = new BioPacket();
                bpLoc.PacketParamOut = new Dictionary<string, object>();
                bpLoc.PacketParamOut.Add("ErrorNumber", "-1");
                bpLoc.PacketParamOut.Add("Mensaje", ex.Message);
                LOG.Error("BviATM.ReadQR Error = " + ex.Message);
            }
            HaveData = true;
            return bpLoc;
        }


        /// <summary>
        /// Realiza captura de huella y retorna el BioPaket con los datos pedidos
        /// </summary>
        /// <returns></returns>
        private BioPacket GetSample()
        {
            BioPacket bpLoc = new BioPacket();
            try
            {
                bpLoc.PacketParamOut = new Dictionary<string, object>();
                bpLoc.PacketParamOut.Add("ErrorNumber", "100");
                bpLoc.PacketParamOut.Add("Mensaje", "Operacion no impelementada");
            }
            catch (Exception ex)
            {
                bpLoc = null;
                LOG.Error("BviATM.GetSample Error = " + ex.Message);
            }
            HaveData = true;
            return bpLoc;
        }

        /// <summary>
        /// Realiza el Match On Card y retorna el BioPaket con los datos pedidos
        /// </summary>
        /// <returns></returns>
        private BioPacket MatchOnCard()
        {
            BioPacket bpLoc = new BioPacket();
            bpLoc.PacketParamOut = new Dictionary<string, object>();
            try
            {
                //Esqueleto a reemplazar por operacion real, con un random devolviendo lectura ok o error.
                Random r = new Random();
                int irandom = r.Next(2);
                if (irandom == 0)
                {
                    bpLoc.PacketParamOut.Add("ErrorNumber", "0");
                    bpLoc.PacketParamOut.Add("Mensaje", null);
                    //Aca debe devolver lo que devuelve ahora el resultado de BVI => BVIResponse
                    //Ahora agrego esto a modo de ejemplo
                    bpLoc.PacketParamOut.Add("VerificacionOK", "true");
                }
                else
                {
                    bpLoc.PacketParamOut.Add("ErrorNumber", "-2");
                    bpLoc.PacketParamOut.Add("Mensaje", "Explicacion del error");
                    bpLoc.PacketParamOut.Add("VerificacionOK", "false");
                }
            }
            catch (Exception ex)
            {
                //bpLoc = new BioPacket();
                bpLoc.PacketParamOut.Add("ErrorNumber", "-1");
                bpLoc.PacketParamOut.Add("Mensaje", ex.Message);
                LOG.Error("BviATM.MatchOnCard Error = " + ex.Message);
            }
            HaveData = true;
            return bpLoc;
        }

#endregion OPERATIONS
    }
}
