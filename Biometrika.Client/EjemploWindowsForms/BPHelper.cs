﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Bio.Core.Api;
using BioPortal.Server.Api;

namespace EjemploWindowsForms
{
    public class BPHelper
    {
        internal static int Enroll(string url, string rut, string companyid, BVIClientResponse respuestaDeserializada, out string msgErr)
        {
            int respuesta = 0;
            msgErr = null;

            try
            {
                XmlParamIn xmlParamIn = new XmlParamIn();

                using (BioPortalWSWEB.BioPortalServerWSWeb ws = new BioPortalWSWEB.BioPortalServerWSWeb())
                {
                    ws.Timeout = 30000;
                    ws.Url = url + "BioPortal.Server.WS.asmx";

                    xmlParamIn.Actionid = Bio.Core.Api.Constant.Action.ACTION_ENROLL;

                    xmlParamIn.Authenticationfactor = Bio.Core.Api.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;
                    xmlParamIn.Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005;

                    xmlParamIn.Bodypart = Bio.Core.Api.Constant.BodyPart.BODYPART_DEDOINDICEIZQUIERDO;

                    xmlParamIn.Clientid = "H39151000801";

                    xmlParamIn.OperationOrder = 1; //Default
                    xmlParamIn.Companyid = Convert.ToInt32(companyid);
                    xmlParamIn.Ipenduser = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();
                    xmlParamIn.Matchingtype = 1;
                    xmlParamIn.Origin = 1;
                    if (!String.IsNullOrEmpty(respuestaDeserializada.Persona.Wsq))
                    {
                        if (xmlParamIn.SampleCollection == null) xmlParamIn.SampleCollection = new List<Sample>();
                        Sample sample = new Sample
                        {
                            Data = respuestaDeserializada.Persona.Wsq,
                            Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_WSQ,
                            BodyPart = 7,
                            Additionaldata = null
                        };
                        xmlParamIn.SampleCollection.Add(sample);
                    }
                    if (!String.IsNullOrEmpty(respuestaDeserializada.Persona.Iso))
                    {
                        if (xmlParamIn.SampleCollection == null) xmlParamIn.SampleCollection = new List<Sample>();
                        Sample sample = new Sample
                        {
                            Data = respuestaDeserializada.Persona.Iso,
                            Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005,
                            BodyPart = 7,
                            Additionaldata = null
                        };
                        xmlParamIn.SampleCollection.Add(sample);
                    }
                    if (!String.IsNullOrEmpty(respuestaDeserializada.Persona.Ansi))
                    {
                        if (xmlParamIn.SampleCollection == null) xmlParamIn.SampleCollection = new List<Sample>();
                        Sample sample = new Sample
                        {
                            Data = respuestaDeserializada.Persona.Ansi,
                            Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004,
                            Additionaldata = null
                        };
                        xmlParamIn.SampleCollection.Add(sample);
                    }
                    xmlParamIn.Userid = 1; // No Aplica
                    xmlParamIn.Verifybyconnectorid = "Final";
                    xmlParamIn.InsertOption = 0; //Default  

                    //Datos de la Persona
                    xmlParamIn.PersonalData = new PersonalData
                    {
                        Typeid = "RUT",
                        Valueid = rut,
                        Companyidenroll = Convert.ToInt32(companyid),
                        Useridenroll = 1,
                        Patherlastname = respuestaDeserializada.Persona.Apellido,
                        Name = respuestaDeserializada.Persona.Nombre,
                        Photography = respuestaDeserializada.Persona.Foto,
                        Signatureimage = respuestaDeserializada.Persona.FotoFirma,
                        DocImageFront = respuestaDeserializada.Persona.FotoFrontCedula,
                        DocImageBack = respuestaDeserializada.Persona.FotoBackCedula,
                        Sex = respuestaDeserializada.Persona.Sexo,
                        Verificationsource = "CI",
                        Documentseriesnumber = respuestaDeserializada.Persona.Serie,
                        Documentexpirationdate = respuestaDeserializada.Persona.FechaExpiracion.HasValue ?
                                     respuestaDeserializada.Persona.FechaExpiracion.Value :
                                     DateTime.Now,
                        Birthdate = respuestaDeserializada.Persona.FechaNacimiento.HasValue?
                                     respuestaDeserializada.Persona.FechaNacimiento.Value:
                                     DateTime.Now,
                        //Birthplace = registryModel.LugarNacimiento,
                        //Nationality = registryModel.Nacionalidad,
                        //Profession = registryModel.Profesion,
                        //Visatype = registryModel.TipoVisa
                    };

                    //Serializamos.
                    string xmlparamin = XmlUtils.SerializeAnObject(xmlParamIn);
                    string xmlparamout = "";

                    respuesta = ws.Enroll(xmlparamin, out xmlparamout);

                    xmlparamout = xmlparamout.Replace("&#", "");
                    XmlParamOut paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlparamout);

                    if (respuesta == 0 && paramout != null)
                    {
                        msgErr = null;
                    }
                    else
                    {
                        msgErr = paramout.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                msgErr = "Error Enrolando [" + ex.Message + "]";
                respuesta = -1;
            }
            return respuesta;
        }
    

        internal static int Verify(string url, string rut, string companyid, string token, out string msgErr, out bool verifyOK, out int score, out string foto)
        {
            int respuesta = 0;
            msgErr = null;
            score = 0;
            foto = null;
            try
            {
                XmlParamIn xmlParamIn = new XmlParamIn();

                using (BioPortalWSWEB.BioPortalServerWSWeb ws = new BioPortalWSWEB.BioPortalServerWSWeb())
                {
                    ws.Timeout = 30000;
                    ws.Url = url + "BioPortal.Server.WS.WEB.asmx";

                    xmlParamIn.Actionid = Bio.Core.Api.Constant.Action.ACTION_VERIFYANDGET;

                    xmlParamIn.Authenticationfactor = Bio.Core.Api.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;
                    xmlParamIn.Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005;

                    xmlParamIn.Bodypart = Bio.Core.Api.Constant.BodyPart.BODYPART_ALL;

                    xmlParamIn.Clientid = "POCItau";

                    xmlParamIn.OperationOrder = 1; //Default
                    xmlParamIn.Companyid = Convert.ToInt32(companyid);
                    xmlParamIn.Ipenduser = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();
                    xmlParamIn.Matchingtype = 1;
                    xmlParamIn.Origin = 1;
                    if (!String.IsNullOrEmpty(token))
                    {
                        xmlParamIn.SampleCollection = new List<Sample>();
                        Sample sample = new Sample
                        {
                            Data = token,
                            Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_TOKEN,
                            Additionaldata = null
                        };
                        xmlParamIn.SampleCollection.Add(sample);
                    }
                    else
                    {
                        xmlParamIn.SampleCollection = null;
                    }
                    xmlParamIn.SaveVerified = 1;     
                    xmlParamIn.Threshold = 50; //Umbral recomendado para este algoritmo

                    xmlParamIn.Userid = 1; // No Aplica

                    xmlParamIn.Verifybyconnectorid = "Final";
                    xmlParamIn.InsertOption = 0; //Default  

                    //Datos de la Persona
                    xmlParamIn.PersonalData = new PersonalData
                    {
                        Typeid = "RUT",
                        Valueid = rut,
                        Companyidenroll = Convert.ToInt32(companyid),
                        Useridenroll = 1,
                    };

                    //Serializamos.
                    string xmlparamin = XmlUtils.SerializeAnObject(xmlParamIn);
                    string xmlparamout = "";

                    respuesta = ws.Verify(xmlparamin, out xmlparamout);

                    xmlparamout = xmlparamout.Replace("&#", "");
                    XmlParamOut paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlparamout);

                    if (respuesta == 0 && paramout != null && paramout.Result == 1)
                    {
                        verifyOK = true;
                        score = (int)paramout.Score;
                        foto = paramout.PersonalData.Photography;
                    } else
                    {
                        verifyOK = false;
                        score = (int)paramout.Score;
                        msgErr = paramout.Message;
                    }
                 }
            }
            catch (Exception ex)
            {
                msgErr = "Error Verificando [" + ex.Message + "]";
                verifyOK = false;
                score = 0;
                respuesta = -1;
            }
            return respuesta;
        }
    }
}
