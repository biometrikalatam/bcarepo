﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace EjemploWindowsForms
{
    public class DRRes<T>
    {
        public T DRResponse { get; set; }
    }
    public class DRClientResponse
    {
        public DRClientResponse() { }

        public string TrackIdCI { get; set; }
        public Persona Persona { get; set; }
    }

    public class BVIRes<T>
    {
        public T BVIResponse { get; set; }
    }
    public class BVIClientResponse
    {
        public BVIClientResponse() { }

        public string TrackIdCI { get; set; }
        public Persona Persona { get; set; }
    }

    public class Solicitud
    {
        public Solicitud() { }

        public Persona Persona { get; set; }
        public Configuracion Configuracion { get; set; }
    }

    public class Configuracion
    {
        public Configuracion() { }

        public int clientType { get; set; }
        public bool ignoreStepSelfieInWizard { get; set; }
        public bool bloqueoMenoresDeEdad { get; set; }
        public bool bloquearDocumentoVencido { get; set; }
        public bool bloqueoCedulaInSCReI { get; set; }
        public bool returnSampleImages { get; set; }
        public bool returnSampleMinutiae { get; set; }
        public string userName { get; set; }
    }

    public class Persona
    {
        public Persona() { }

        [JsonConverter(typeof(StringEnumConverter))]
        public TipoIdentificacion TipoIdentificacion { get; set; }
        public string FotoFrontCedula { get; set; }
        public string FotoBackCedula { get; set; }
        public string Foto { get; set; }
        public string FotoFirma { get; set; }
        public string Selfie { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Sexo { get; set; }
        public string Serie { get; set; }
        public string Nacionalidad { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public DateTime? FechaExpiracion { get; set; }
        public bool IsCedulaVigente { get; set; }
        public bool IsMenorDeEdad { get; set; }

        //Result
        public int clientType { get; set; } //1-Huella | 2-Facial
        public int verifyResult { get; set; } // 0-Aun no verifico | 1-Positivo | 2-Negativo
        public double score { get; set; }

        //Certify
        public string CertifyPdf { get; set; }

        //Samples
        public string Ansi { get; set; }
        public string Iso { get; set; }
        public string[] IsoCompact { get; set; }
        public string Raw { get; set; }
        public string Wsq { get; set; }
        public string JpegOriginal { get; set; }

        public string GetNameTipoIdentificacion()
        {
            if (TipoIdentificacion == TipoIdentificacion.CedulaChilenaAntigua)
                return "Cedula Chilena Antigua";
            if (TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
                return "Cedula Chilena Nueva";
            return "Indeterminado";

        }
    }

    public enum TipoIdentificacion
    {
        Indeterminado = 0,
        CedulaChilenaAntigua = 1,
        CedulaChilenaNueva = 2
    }

    public enum TipoBloqueo
    {
        Advertencia = 0,
        Bloqueo = 1,
        Permitir = 2
    }

    public enum ClientType
    {
        SeleccionManual = 0,
        Huella = 1,
        Facial = 2
    }

    public class Models
    {
    }
}
