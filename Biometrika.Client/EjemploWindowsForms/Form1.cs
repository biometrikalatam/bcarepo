﻿using Newtonsoft.Json;
using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text.Json;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace EjemploWindowsForms
{
    public partial class Form1 : Form
    {
        BVIClientResponse _respuestaDeserializada;
        DRClientResponse _respuestaDeserializadaDR;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LimpiarFormulario();
            string msg = null;
            BVIClientResponse lastResult = null;
            
            Certify((int)cboClientType.SelectedItem, "test", rutTxt.Text, out lastResult, out msg);

            //var persona = new Persona()
            //{
            //    Rut = rutTxt.Text
            //};
            //var configuracion = new Configuracion()
            //{
            //    BloqueoMenoresDeEdad = (TipoBloqueo) bloqueoVencidaCmbBx.SelectedItem,
            //    ExtraerFirma = extraerFirmaChk.Checked,
            //    extraerFoto = extraerFotoChk.Checked,
            //    vigenciaCedula = chkVigenciaCedula.Checked,
            //    bloqueoCedula = chkBloqueoCedula.Checked,
            //    raw = chkRaw.Checked,
            //    iso = chkISO.Checked,
            //    ansi = chkAnsi.Checked,
            //    wsq = chkWsq.Checked,
            //    jpeg = chkJpeg.Checked

            //};
            //var solicitud = new Solicitud()
            //{
            //    Persona = persona,
            //    Configuracion = configuracion
            //};
            //string hashGuid = Guid.NewGuid().ToString("N");
            ////Siempre debe ir acompañado del atributo SRC.
            //string queryString = "?src=BVICAM";
            //byte[] arr;
            //JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            //String result = jsonSerializer.Serialize(solicitud);
            //string absoluteURL = "";
            //string json = "";
            //try
            //{
            //    //Aca debe ir agregando los distintos parámetros.
            //    queryString = queryString + "&valueid=" + rutTxt.Text;
            //    queryString = queryString + "&SOLICITUD=" + result;

            //    BVIResponse bviresponse;
            //    absoluteURL = urlTxt.Text;  //ConfigurationManager.AppSettings["AbsoluteURL"];
            //    if (!absoluteURL.EndsWith("/"))
            //        absoluteURL = absoluteURL + "/";

            //    absoluteURL = absoluteURL + queryString;
            //    WebRequest request = WebRequest.Create(absoluteURL);
            //    request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
            //    request.Timeout = 1300000000;
            //    using (WebResponse response = (HttpWebResponse)request.GetResponse())
            //    {
            //        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            //        {
            //            json = reader.ReadToEnd();
            //            MessageBox.Show(json);
            //            _RespuestaDeserializada = JsonConvert.DeserializeObject<BVIRes<BVIResponse>>(json);
            //            LlenarFormulario(_RespuestaDeserializada);
            //        }
            //    }
            //}
            //catch (Exception exe)
            //{
            //    MessageBox.Show("exe:" + exe.Message);
            //}
            //rbtRequest.Text = absoluteURL;
            //rbtResponse.Text = json;

        }

        private void LimpiarFormulario()
        {
            _respuestaDeserializada = null;
            txtRutResult.Text = "";
            txtNombreResult.Text = "";
            txtApellidoPResult.Text = "";
            txtDOBResult.Text = "";
            txtDOEResult.Text = "";
            txtSerieResult.Text = "";
            txtRawResult.Text = "";
            txtIsoResult.Text = "";
            txtWsqResult.Text = "";

            picFirma.Image = null;
            picFoto.Image = null;
            picJpg.Image = null;
            picCedFront.Image = null;
            picCedBack.Image = null;
        }

        private void LlenarFormulario(Persona respuestaDeserializada)
        {

            if (respuestaDeserializada != null)
            {
                txtRutResult.Text = respuestaDeserializada.Rut; //
                //respuestaDeserializada.Persona.Rut.Substring(0, respuestaDeserializada.Persona.Rut.Length - 1) +
                //    "-" + respuestaDeserializada.Persona.Rut.Substring(respuestaDeserializada.Persona.Rut.Length - 1);
                txtNombreResult.Text = respuestaDeserializada.Nombre;
                txtApellidoPResult.Text = respuestaDeserializada.Apellido;
                //txtApellidoMResult.Text = respuestaDeserializada.Persona.ApellidoMaterno;
                txtDOBResult.Text = respuestaDeserializada.FechaNacimiento.ToString();
                txtDOEResult.Text = respuestaDeserializada.FechaExpiracion.ToString();
                txtSerieResult.Text = respuestaDeserializada.Serie;
                if (!string.IsNullOrEmpty(respuestaDeserializada.Sexo) && respuestaDeserializada.Sexo.Equals("F"))
                {
                    picMale.Hide();
                }
                else if (!string.IsNullOrEmpty(respuestaDeserializada.Sexo) && respuestaDeserializada.Sexo.Equals("M"))
                {
                    picFem.Hide();
                }
                txtRawResult.Text = respuestaDeserializada.Raw;
                txtIsoResult.Text = respuestaDeserializada.Iso;
                txtAnsiResult.Text = respuestaDeserializada.Ansi;
                txtWsqResult.Text = respuestaDeserializada.Wsq;

                if (!string.IsNullOrEmpty(respuestaDeserializada.JpegOriginal))
                { 
                    var jpg = respuestaDeserializada.JpegOriginal;
                    picJpg.Image = Base64ToBitmap(jpg);
                }
                //string ruta = Path.Combine(@"C:\Biometrika\fotoHuella.jpg");
                //picJpg.Image.Save(ruta, System.Drawing.Imaging.ImageFormat.Jpeg);
                if (!string.IsNullOrEmpty(respuestaDeserializada.Foto))
                {
                    try
                    {
                        picFoto.Image = Base64ToBitmap(respuestaDeserializada.Foto);
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (!string.IsNullOrEmpty(respuestaDeserializada.FotoFirma))
                {
                    try
                    {
                        picFirma.Image = Base64ToBitmap(respuestaDeserializada.FotoFirma);
                    }
                    catch (Exception ex)
                    {

                    }

                    //string ruta = Path.Combine(@"C:\Biometrika\fotoFirma.jpg");
                    //picFirma.Image.Save(ruta,System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                if (!string.IsNullOrEmpty(respuestaDeserializada.FotoFrontCedula))
                {
                    try
                    {
                        picCedFront.Image = Base64ToBitmap(respuestaDeserializada.FotoFrontCedula);
                    }
                    catch (Exception ex)
                    {

                    }

                    //string ruta = Path.Combine(@"C:\Biometrika\fotoFirma.jpg");
                    //picFirma.Image.Save(ruta,System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                if (!string.IsNullOrEmpty(respuestaDeserializada.FotoBackCedula))
                {
                    try
                    {
                        picCedBack.Image = Base64ToBitmap(respuestaDeserializada.FotoBackCedula);
                    }
                    catch (Exception ex)
                    {

                    }

                    //string ruta = Path.Combine(@"C:\Biometrika\fotoFirma.jpg");
                    //picFirma.Image.Save(ruta,System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }
            else
            {
                LimpiarFormulario();
            }
            
            if (respuestaDeserializada.verifyResult == 1)
            {
                if (chkVigenciaCedula.Checked)
                {
                    if (!respuestaDeserializada.IsCedulaVigente)
                    {
                        lblCVenResult.Text = "SI";
                        lblCVenResult.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        lblCVenResult.Text = "NO";
                        lblCVenResult.ForeColor = System.Drawing.Color.Green;
                    }
                }
                else
                {
                    lblCVenResult.Text = "NO VERIFICADO";
                }

                if (respuestaDeserializada.IsMenorDeEdad)
                {
                    if (bloqueoVencidaCmbBx.SelectedItem.ToString() == "Advertencia")
                    {
                        lblMdEResult.Text = "WARNING: MENOR DE EDAD";
                        lblMdEResult.ForeColor = System.Drawing.Color.Orange;
                    }
                    else if (bloqueoVencidaCmbBx.SelectedItem.ToString() == "Permitir")
                    {
                        lblMdEResult.Text = "PERMITIDO";
                        lblMdEResult.ForeColor = System.Drawing.Color.Green;
                    }
                    else if (bloqueoVencidaCmbBx.SelectedItem.ToString() == "Bloqueo")
                    {
                        lblMdEResult.Text = "BLOQUEADO";
                        lblMdEResult.ForeColor = System.Drawing.Color.Red;
                        gbxDatosPersonales.Enabled = false;
                    }
                }
                else
                {
                    lblMdEResult.Text = "ADULTO";
                    lblMdEResult.ForeColor = System.Drawing.Color.Green;
                }
                if (respuestaDeserializada.verifyResult == 1)
                {
                    lblVerificacion.Text = "VERIFICACIÓN POSITIVA";
                    lblVerificacion.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblVerificacion.Text = "VERIFICACIÓN NEGATIVA";
                    lblVerificacion.ForeColor = System.Drawing.Color.Red;
                }

                //if (chkBloqueoCedula.Checked)
                //{
                    //if (respuestaDeserializada.Data.ResultadoVerificacion.CedulaBloqueada)
                    //{
                    //    lblRegistroCivilResult.Text = "CÉDULA ACTIVA";
                    //    lblRegistroCivilResult.ForeColor = System.Drawing.Color.Green;
                    //}
                    //else
                    //{
                    //    lblRegistroCivilResult.Text = "CÉDULA INACTIVA";
                    //    lblRegistroCivilResult.ForeColor = System.Drawing.Color.Red;
                    //}
                //}
                //else
                //{
                //    lblRegistroCivilResult.Text = "NO VERIFICADO";
                //}
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            bloqueoVencidaCmbBx.DataSource = Enum.GetValues(typeof(TipoBloqueo));
            cboClientType.DataSource = Enum.GetValues(typeof(ClientType));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clienttype"></param>
        /// <param name="username"></param>
        /// <param name="rut"></param>
        /// <param name="lastResult"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal int Certify(int clienttype, string username, string rut, out BVIClientResponse lastResult, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            lastResult = null;
            try
            {

                var persona = new Persona()
                {
                    Rut = rut
                };
                var configuracion = new Configuracion()
                {
                    clientType = clienttype,
                    //extraerFoto = true,
                    ignoreStepSelfieInWizard = false, 
                    //extraerFirma = true,
                    bloqueoMenoresDeEdad = false,
                    bloquearDocumentoVencido = false, //chkVigenciaCedula.Checked,
                    bloqueoCedulaInSCReI = false,
                    userName = username
                };
                var solicitud = new Solicitud()
                {
                    Persona = persona,
                    Configuracion = configuracion
                };
                //string hashGuid = Guid.NewGuid().ToString("N");
                //Siempre debe ir acompañado del atributo SRC.
                string queryString = "?src=BVI7";
                byte[] arr;
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                string jsonSolicitud = jsonSerializer.Serialize(solicitud);
                string absoluteURL = "";
                string json = "";
                try
                {
                    //Aca debe ir agregando los distintos parámetros.
                    queryString = queryString + "&VALUEID=" + rut;
                    queryString = queryString + "&SOLICITUD=" + jsonSolicitud;


                    absoluteURL = urlTxt.Text;  //ConfigurationManager.AppSettings["AbsoluteURL"];
                    if (!absoluteURL.EndsWith("/"))
                        absoluteURL = absoluteURL + "/";

                    absoluteURL = absoluteURL + queryString;
                    WebRequest request = WebRequest.Create(absoluteURL);
                    rbtRequest.Text = absoluteURL;
                    request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
                    request.Timeout = 1300000000;
                    using (WebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            json = reader.ReadToEnd();
                            //rbtResponse.Text = json;
                            BVIRes<string> res = JsonConvert.DeserializeObject<BVIRes<string>>(json);

                            if (res != null && res.BVIResponse != null)
                            {
                                lastResult = JsonConvert.DeserializeObject<BVIClientResponse>(res.BVIResponse);
                                rbtResponse.Text = JsonPrettify(res.BVIResponse); // json;
                                _respuestaDeserializada = lastResult;
                                LlenarFormulario(lastResult.Persona);
                            }
                            else
                            {
                                msgerr = "res.BVIResponse = null => Restono cancelando...";
                            }

                            if (lastResult == null)
                            {
                                ret = -4;
                                msgerr = "Error deserializando respuesta";
                            }
                        }
                    }
                }
                catch (Exception exe)
                {
                    ret = -5;
                    msgerr = "Error en consulta al servicio BVI Client [" + exe.Message + "]";
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Error desconocido en consulta al servicio BVI Client [" + ex.Message + "]";
            }
            return ret;
        }

        public Image Base64ToImage(string base64String)
        {
            // Convert base 64 string to byte[] 
            byte[] imageBytes = Convert.FromBase64String(base64String);
            // Convert byte[] to Image 
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            { Image image = Image.FromStream(ms, true); return image; }
        }

        public static Bitmap Base64ToBitmap(string base64Bmp)
        {
            var buffer = Convert.FromBase64String(base64Bmp);
            var stream = new MemoryStream(buffer);
            stream.Position = 0;

            return (Bitmap)Image.FromStream(stream);
        }
        //public Boolean RegistroCivil(Persona persona)
        //{
        //    bool resp = false;
        //    RC_Wrapper rcWrapper = new RC_Wrapper(ConfigurationManager.AppSettings["UrlRC"]);
        //    ResponseDTO response = null;
        //    try
        //    {
        //        response = rcWrapper.Verify(DocType.CEDULA, persona.Rut, persona.Serie);
        //        if (response.ResponseMessage == "Vigente")
        //        {
        //            resp = true;
        //        }
        //        else
        //        {
        //            resp = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return resp;
        //}

        private void btnLimpiar_Click(object sender, EventArgs e)
        {

            try
            {

                //string json;
                //json = File.ReadAllText(@"c:\Biometrika\aaa.txt");

                //var msg= JsonConvert.DeserializeObject<BVIRes<BVIResponse>>(json);
                //var respuestaDeserializada = JsonConvert.DeserializeObject<BVIRes<BVIT>>(json);



            }
            catch (Exception exe)
            {

            }


            rutTxt.Text = "";
            //extraerFotoChk.Checked = false;
            //extraerFirmaChk.Checked = false;
            //chkAnsi.Checked = false;
            //chkRaw.Checked = false;
            //chkISO.Checked = false;
            //chkWsq.Checked = false;
            //chkJpeg.Checked = false;
            chkVigenciaCedula.Checked = false;
            chkBloqueoCedula.Checked = false;
            txtNombreResult.Text = "";
            txtApellidoPResult.Text = "";
            txtDOBResult.Text = "";
            txtDOEResult.Text = "";
            txtSerieResult.Text = "";
            txtRawResult.Text = "";
            txtIsoResult.Text = "";
            txtAnsiResult.Text = "";
            txtWsqResult.Text = "";
            picJpg.Image = null;
            picFoto.Image = null;
            picFirma.Image = null;
            lblMdEResult.Text = "Sin Consulta";
            lblMdEResult.ForeColor = Color.Black;
            lblCVenResult.Text = "Sin Consulta";
            lblCVenResult.ForeColor = Color.Black;
            lblRegistroCivilResult.Text = "Sin Consulta";
            lblRegistroCivilResult.ForeColor = Color.Black;
            lblVerificacion.Text = "Esperando Verificación";
            lblVerificacion.ForeColor = Color.Black;
            picFem.Visible = true;
            picMale.Visible = true;
            picCedBack.Image = null;
            picCedFront.Image = null;
        }

        private void btnBPEnroll_Click(object sender, EventArgs e)
        {
            string msgErr;
            //if (string.IsNullOrEmpty(txtRutResult.Text) || string.IsNullOrEmpty(txtWsqResult.Text) ||
            //    string.IsNullOrEmpty(txtURLBP.Text) || string.IsNullOrEmpty(txtCompanyId.Text) ||
            //    _RespuestaDeserializada == null)
            //{
            //    MessageBox.Show(this, "Faltan datos para enrolar", "Atención!!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            //} else
            //{
            //    int ret = BPHelper.Enroll(txtURLBP.Text, txtRutResult.Text, txtCompanyId.Text, _RespuestaDeserializada, out msgErr);
            //    if (ret < 0)
            //    {
            //        MessageBox.Show(this, "Error enrolando en en BioPortal [" + ret.ToString() + " - " + msgErr + "]", 
            //            "Atención!!", MessageBoxButtons.OK,MessageBoxIcon.Error);
            //    } else
            //    {
            //        MessageBox.Show(this, "Enroll en en BioPortal Correcto!",
            //            "Atención!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    }
            //} 
        }

        private void btnBPVerify_Click(object sender, EventArgs e)
        {
            string msgErr;
            if (string.IsNullOrEmpty(txtRutResult.Text) || string.IsNullOrEmpty(txtToken.Text) ||
                string.IsNullOrEmpty(txtURLBP.Text) || string.IsNullOrEmpty(txtCompanyId.Text))
            {
                MessageBox.Show(this, "Faltan datos para enrolar", "Atención!!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                int score;
                bool resultVerify;
                string foto;
                int ret = BPHelper.Verify(txtURLBP.Text, txtRutResult.Text, txtCompanyId.Text, txtToken.Text, out msgErr, out resultVerify, out score, out foto);
                if (ret < 0)
                {
                    MessageBox.Show(this, "Error enrolando en en BioPortal [" + ret.ToString() + " - " + msgErr + "]",
                        "Atención!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (!string.IsNullOrEmpty(foto))
                    {
                        picFotoBP.Image = Base64ToImage(foto);
                    }
                    if (resultVerify) MessageBox.Show(this, "Verificación POSITIVA! [Score=" + score + "]", "Atención!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else MessageBox.Show(this, "Verificación NEGATIVA! [Msg=" + msgErr + "]", "Atención!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string hashGuid = Guid.NewGuid().ToString("N");
            string queryString = "?src=bpc7";
            queryString = queryString + "&TypeId=RUT";
            queryString = queryString + "&ValueId=" + txtRutResult.Text;
            queryString = queryString + "&Tokencontent=0";
            queryString = queryString + "&Device=2";
            queryString = queryString + "&AuthenticationFactor=2";
            queryString = queryString + "&OperationType=1";
            queryString = queryString + "&MinutiaeType=14";
            queryString = queryString + "&Timeout=10000";
            queryString = queryString + "&Theme=" + (string.IsNullOrEmpty(txtTheme.Text)? "bioportalclient_itau.png":txtTheme.Text);
            queryString = queryString + "&BodyPartShow=";
            queryString = queryString + "&Hash=" + hashGuid;

            string absoluteURL = "http://localhost:9191";  //ConfigurationManager.AppSettings["AbsoluteURL"];
            if (!absoluteURL.EndsWith("/"))
                absoluteURL = absoluteURL + "/";

            absoluteURL = absoluteURL + queryString;

            WebRequest request = WebRequest.Create(absoluteURL);
            request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
            request.Timeout = 13000000;

            TokenResponse serverResponse;
            using (WebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    string json = reader.ReadToEnd();
                    serverResponse = JsonConvert.DeserializeObject<TokenResponse>(json);
                }
            }

            if (serverResponse.ResponseOK)
            {
                if (serverResponse.Hash == hashGuid)
                {
                    Byte[] bitmapData = Convert.FromBase64String(serverResponse.ImageSample);
                    MemoryStream streamBitmap = new MemoryStream(bitmapData);

                    picHuellaToken.Image = new Bitmap((Bitmap)Image.FromStream(streamBitmap));

                    txtToken.Text = serverResponse.Token;
                }
                else
                    MessageBox.Show(this, "No coinbcide el hash",
                       "Atención!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                MessageBox.Show(this, "No Hubo captura",
                       "Atención!!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            picFotoBP.Image = null;
            txtToken.Text = null;
            picHuellaToken.Image = null;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            //LlenarFormulario(_respuestaDeserializada.Persona);
            DRRes<string> res = JsonConvert.DeserializeObject<DRRes<string>>(rbtResponse.Text);
            //JsonSerializerSettings jss = new JsonSerializerSettings();

            //rbtResponse.Text = JsonConvert.SerializeObject(res, Formatting.Indented);
            DRClientResponse res1 = JsonConvert.DeserializeObject<DRClientResponse>(res.DRResponse);
            string s = JsonConvert.SerializeObject(res1, Formatting.Indented);
            rbtResponse.Text = JsonPrettify(s);
            //rbtResponse.Text = FormatJsonText(s);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string msg = null;
            DRClientResponse lastResult = null;

            DR("testDR", rutTxt.Text, out lastResult, out msg);
        }

        internal int DR(string username, string rut, out DRClientResponse lastResult, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            lastResult = null;
            try
            {
                
                //string hashGuid = Guid.NewGuid().ToString("N");
                //Siempre debe ir acompañado del atributo SRC.
                string queryString = "?src=DR";
                byte[] arr;
                string absoluteURL = "";
                string json = "";
                try
                {
                    //Aca debe ir agregando los distintos parámetros.
                    queryString = queryString;// + "&VALUEID=" + rut;
                    //queryString = queryString + "&SOLICITUD=" + jsonSolicitud;


                    absoluteURL = urlTxt.Text;  //ConfigurationManager.AppSettings["AbsoluteURL"];
                    if (!absoluteURL.EndsWith("/"))
                        absoluteURL = absoluteURL + "/";

                    absoluteURL = absoluteURL + queryString;
                    rbtRequest.Text = absoluteURL;
                    WebRequest request = WebRequest.Create(absoluteURL);
                    request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
                    request.Timeout = 1300000000;
                    using (WebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            json = reader.ReadToEnd();
                            rbtResponse.Text = JsonPrettify(json); // json;
                            DRRes<string> res = JsonConvert.DeserializeObject<DRRes<string>>(json);
                            //JsonSerializerSettings jss = new JsonSerializerSettings();

                            //rbtResponse.Text = JsonConvert.SerializeObject(res, Formatting.Indented);
                       
                            if (res != null && res.DRResponse != null)
                            {
                                lastResult = JsonConvert.DeserializeObject<DRClientResponse>(res.DRResponse);
                                rbtResponse.Text = JsonPrettify(res.DRResponse); // json;

                                _respuestaDeserializadaDR = lastResult;
                                LlenarFormulario(lastResult.Persona);
                            }
                            else
                            {
                                msgerr = "res.BVIResponse = null => Restono cancelando...";
                            }

                            if (lastResult == null)
                            {
                                ret = -4;
                                msgerr = "Error deserializando respuesta";
                            }
                        }
                    }
                }
                catch (Exception exe)
                {
                    ret = -5;
                    msgerr = "Error en consulta al servicio BVI Client [" + exe.Message + "]";
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Error desconocido en consulta al servicio BVI Client [" + ex.Message + "]";
            }
            return ret;
        }

        public static string JsonPrettify(string json)
        {
            using (var stringReader = new StringReader(json))
            using (var stringWriter = new StringWriter())
            {
                var jsonReader = new JsonTextReader(stringReader);
                var jsonWriter = new JsonTextWriter(stringWriter) { Formatting = Formatting.Indented };
                jsonWriter.WriteToken(jsonReader);
                return stringWriter.ToString();
            }
        }

        static string FormatJsonText(string jsonString)
        {
            
            var doc = JsonDocument.Parse(
                jsonString,
                new JsonDocumentOptions
                {
                    AllowTrailingCommas = true
                }
            );
            MemoryStream memoryStream = new MemoryStream();
            using (
                var utf8JsonWriter = new Utf8JsonWriter(
                    memoryStream,
                    new JsonWriterOptions
                    {
                        Indented = true
                    }
                )
            )
            {
                doc.WriteTo(utf8JsonWriter);
            }
            return new System.Text.UTF8Encoding()
                .GetString(memoryStream.ToArray());
        }

        private void txtToken_TextChanged(object sender, EventArgs e)
        {

        }
    }


    public class TokenResponse
    {
        public string Token { get; set; }

        public string Huella { get; set; }
        //public Image Image { get; set; }
        public string ImageSample { get; set; }


        public bool ResponseOK
        {
            get
            {
                if (Token == null) return false;
                return Token.Length > 0;
            }
        }

        public string Error { get; set; }
        public int BodyPart { get; set; }
        public string FingerName { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }
        public string Hash { get; set; }
    }
}
