﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploWindowsForms.Models
{
    public class BVIResponse
    {
        public BVIResponse()
        {

        }

        public BVIResponse(string trackid, Persona persona)
        {
            TrackIdCI = trackid;
            Persona = persona;
        }

        public string TrackIdCI { get; set; }
        public Persona Persona { get; set; }
    }

    public class Persona
    {
        public Persona() { }

        [JsonConverter(typeof(StringEnumConverter))]
        public TipoIdentificacion TipoIdentificacion { get; set; }
        public string FotoFrontCedula { get; set; }
        public string FotoBackCedula { get; set; }
        public string Foto { get; set; }
        public string FotoFirma { get; set; }
        public string Selfie { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Sexo { get; set; }
        public string Serie { get; set; }
        public string Nacionalidad { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public DateTime? FechaExpiracion { get; set; }
        public bool IsCedulaVigente { get; set; }
        public bool IsMenorDeEdad { get; set; }

        //Result
        public int clientType { get; set; } //1-Huella | 2-Facial
        public int verifyResult { get; set; } // 0-Aun no verifico | 1-Positivo | 2-Negativo
        public double score { get; set; }
        public int bodypart { get; set; }  //Retorna dedo verificado o bien 16 = cara

        //Certify
        public string CertifyPdf { get; set; }

        //Samples
        public string Ansi { get; set; }
        public string Iso { get; set; }
        public string[] IsoCompact { get; set; }
        public string Raw { get; set; }
        public string Wsq { get; set; }
        public string JpegOriginal { get; set; }
        //public string ApellidoMaterno { get; set; }
        //public string ApellidoPaterno { get; set; }

        public string GetNameTipoIdentificacion()
        {
            if (TipoIdentificacion == TipoIdentificacion.CedulaChilenaAntigua)
                return "Cedula Chilena Antigua";
            if (TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
                return "Cedula Chilena Nueva";
            return "Indeterminado";

        }
    }
    //public class BVIResponse
    //{
    //    public BVIResponse() { }

    //    public Persona Persona { get; set; }
    //    public ResultadoVerificacion ResultadoVerificacion { get; set; }
    //    public int ErrorNumber { get; set; }
    //    public string Mensaje { get; set; }
    //}

    //public class Persona
    //{
    //    public Persona() { }

    //    public int Finger { get; set; }
    //    public string Ansi { get; set; }
    //    public string Iso { get; set; }
    //    public string Raw { get; set; }
    //    public string Foto { get; set; }
    //    public string FotoFirma { get; set; }
    //    public string Serie { get; set; }
    //    public string Wsq { get; set; }
    //    public DateTime? FechaExpiracion { get; set; }
    //    public string Sexo { get; set; }
    //    public string ApellidoMaterno { get; set; }
    //    public string ApellidoPaterno { get; set; }
    //    public string Nombre { get; set; }
    //    public string Rut { get; set; }
    //    [JsonConverter(typeof(StringEnumConverter))]
    //    public TipoIdentificacion TipoIdentificacion { get; set; }
    //    public DateTime? FechaNacimiento { get; set; }
    //    public string Jpeg { get; set; }
    //}

    //public class ResultadoVerificacion
    //{
    //    public ResultadoVerificacion() { }

    //    public bool DatosPersonalesExtraidos { get; set; }
    //    public bool FotoExtraida { get; set; }
    //    public bool FotoFirmaExtraida { get; set; }
    //    public bool MenorDeEdad { get; set; }
    //    public bool CedulaVencida { get; set; }
    //    public bool CedulaBloqueada { get; set; }
    //    [JsonConverter(typeof(StringEnumConverter))]
    //    public Estado Estado { get; set; }
    //}

    public enum Estado
    {
        Cancelado = 0,
        Error = 1,
        VerificacionPositiva = 2,
        VerificacionNegativa = 3
    }

    public enum TipoIdentificacion
    {
        Indeterminado = 0,
        CedulaChilenaAntigua = 1,
        CedulaChilenaNueva = 2
    }
}
