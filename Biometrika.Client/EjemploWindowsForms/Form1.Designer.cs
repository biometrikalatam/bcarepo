﻿namespace EjemploWindowsForms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.rutTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.urlTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.bloqueoVencidaCmbBx = new System.Windows.Forms.ComboBox();
            this.chkVigenciaCedula = new System.Windows.Forms.CheckBox();
            this.gbxDatosPersonales = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.picCedBack = new System.Windows.Forms.PictureBox();
            this.picCedFront = new System.Windows.Forms.PictureBox();
            this.picMale = new System.Windows.Forms.PictureBox();
            this.picFem = new System.Windows.Forms.PictureBox();
            this.lblSex = new System.Windows.Forms.Label();
            this.txtWsqResult = new System.Windows.Forms.TextBox();
            this.txtAnsiResult = new System.Windows.Forms.TextBox();
            this.txtIsoResult = new System.Windows.Forms.TextBox();
            this.txtRawResult = new System.Windows.Forms.TextBox();
            this.txtSerieResult = new System.Windows.Forms.TextBox();
            this.txtDOEResult = new System.Windows.Forms.TextBox();
            this.txtDOBResult = new System.Windows.Forms.TextBox();
            this.txtApellidoPResult = new System.Windows.Forms.TextBox();
            this.txtNombreResult = new System.Windows.Forms.TextBox();
            this.txtRutResult = new System.Windows.Forms.TextBox();
            this.picFirma = new System.Windows.Forms.PictureBox();
            this.picFoto = new System.Windows.Forms.PictureBox();
            this.picJpg = new System.Windows.Forms.PictureBox();
            this.lblFirmaResult = new System.Windows.Forms.Label();
            this.lblJpgResult = new System.Windows.Forms.Label();
            this.lblFotoResult = new System.Windows.Forms.Label();
            this.lblWsqResult = new System.Windows.Forms.Label();
            this.lblAnsiResult = new System.Windows.Forms.Label();
            this.lblIsoResult = new System.Windows.Forms.Label();
            this.lblRawResult = new System.Windows.Forms.Label();
            this.lblSerieResult = new System.Windows.Forms.Label();
            this.lblDOEResult = new System.Windows.Forms.Label();
            this.lblDOBResult = new System.Windows.Forms.Label();
            this.lblApellPatResult = new System.Windows.Forms.Label();
            this.lblNombreResult = new System.Windows.Forms.Label();
            this.lblRutResult = new System.Windows.Forms.Label();
            this.gboxVerificacion = new System.Windows.Forms.GroupBox();
            this.lblRegistroCivilResult = new System.Windows.Forms.Label();
            this.lblRegistroCivil = new System.Windows.Forms.Label();
            this.lblCVenResult = new System.Windows.Forms.Label();
            this.lblMdEResult = new System.Windows.Forms.Label();
            this.lblVerificacion = new System.Windows.Forms.Label();
            this.lblCedulaVencidaResult = new System.Windows.Forms.Label();
            this.lblMenorEdadResult = new System.Windows.Forms.Label();
            this.chkBloqueoCedula = new System.Windows.Forms.CheckBox();
            this.gboxIngresarParam = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.cboClientType = new System.Windows.Forms.ComboBox();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.grpMensajeria = new System.Windows.Forms.GroupBox();
            this.txtMensaje = new System.Windows.Forms.TextBox();
            this.txtNroError = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rbtResponse = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.rbtRequest = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTheme = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.picFotoBP = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.picHuellaToken = new System.Windows.Forms.PictureBox();
            this.btnBPVerify = new System.Windows.Forms.Button();
            this.btnBPEnroll = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCompanyId = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtURLBP = new System.Windows.Forms.TextBox();
            this.gbxDatosPersonales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCedBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCedFront)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFirma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picJpg)).BeginInit();
            this.gboxVerificacion.SuspendLayout();
            this.gboxIngresarParam.SuspendLayout();
            this.grpMensajeria.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoBP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHuellaToken)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(21, 350);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Probar BVI";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // rutTxt
            // 
            this.rutTxt.Location = new System.Drawing.Point(24, 49);
            this.rutTxt.Name = "rutTxt";
            this.rutTxt.Size = new System.Drawing.Size(223, 20);
            this.rutTxt.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "RUT";
            // 
            // urlTxt
            // 
            this.urlTxt.Location = new System.Drawing.Point(21, 312);
            this.urlTxt.Name = "urlTxt";
            this.urlTxt.Size = new System.Drawing.Size(223, 20);
            this.urlTxt.TabIndex = 4;
            this.urlTxt.Text = "http://localhost:9191/";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 296);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "URL WebService";
            // 
            // bloqueoVencidaCmbBx
            // 
            this.bloqueoVencidaCmbBx.FormattingEnabled = true;
            this.bloqueoVencidaCmbBx.Location = new System.Drawing.Point(22, 190);
            this.bloqueoVencidaCmbBx.Name = "bloqueoVencidaCmbBx";
            this.bloqueoVencidaCmbBx.Size = new System.Drawing.Size(223, 21);
            this.bloqueoVencidaCmbBx.TabIndex = 7;
            // 
            // chkVigenciaCedula
            // 
            this.chkVigenciaCedula.AutoSize = true;
            this.chkVigenciaCedula.Checked = true;
            this.chkVigenciaCedula.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkVigenciaCedula.Location = new System.Drawing.Point(21, 235);
            this.chkVigenciaCedula.Name = "chkVigenciaCedula";
            this.chkVigenciaCedula.Size = new System.Drawing.Size(159, 17);
            this.chkVigenciaCedula.TabIndex = 9;
            this.chkVigenciaCedula.Text = "Verificar Vigencia de Cédula";
            this.chkVigenciaCedula.UseVisualStyleBackColor = true;
            // 
            // gbxDatosPersonales
            // 
            this.gbxDatosPersonales.Controls.Add(this.label14);
            this.gbxDatosPersonales.Controls.Add(this.label13);
            this.gbxDatosPersonales.Controls.Add(this.picCedBack);
            this.gbxDatosPersonales.Controls.Add(this.picCedFront);
            this.gbxDatosPersonales.Controls.Add(this.picMale);
            this.gbxDatosPersonales.Controls.Add(this.picFem);
            this.gbxDatosPersonales.Controls.Add(this.lblSex);
            this.gbxDatosPersonales.Controls.Add(this.txtWsqResult);
            this.gbxDatosPersonales.Controls.Add(this.txtAnsiResult);
            this.gbxDatosPersonales.Controls.Add(this.txtIsoResult);
            this.gbxDatosPersonales.Controls.Add(this.txtRawResult);
            this.gbxDatosPersonales.Controls.Add(this.txtSerieResult);
            this.gbxDatosPersonales.Controls.Add(this.txtDOEResult);
            this.gbxDatosPersonales.Controls.Add(this.txtDOBResult);
            this.gbxDatosPersonales.Controls.Add(this.txtApellidoPResult);
            this.gbxDatosPersonales.Controls.Add(this.txtNombreResult);
            this.gbxDatosPersonales.Controls.Add(this.txtRutResult);
            this.gbxDatosPersonales.Controls.Add(this.picFirma);
            this.gbxDatosPersonales.Controls.Add(this.picFoto);
            this.gbxDatosPersonales.Controls.Add(this.picJpg);
            this.gbxDatosPersonales.Controls.Add(this.lblFirmaResult);
            this.gbxDatosPersonales.Controls.Add(this.lblJpgResult);
            this.gbxDatosPersonales.Controls.Add(this.lblFotoResult);
            this.gbxDatosPersonales.Controls.Add(this.lblWsqResult);
            this.gbxDatosPersonales.Controls.Add(this.lblAnsiResult);
            this.gbxDatosPersonales.Controls.Add(this.lblIsoResult);
            this.gbxDatosPersonales.Controls.Add(this.lblRawResult);
            this.gbxDatosPersonales.Controls.Add(this.lblSerieResult);
            this.gbxDatosPersonales.Controls.Add(this.lblDOEResult);
            this.gbxDatosPersonales.Controls.Add(this.lblDOBResult);
            this.gbxDatosPersonales.Controls.Add(this.lblApellPatResult);
            this.gbxDatosPersonales.Controls.Add(this.lblNombreResult);
            this.gbxDatosPersonales.Controls.Add(this.lblRutResult);
            this.gbxDatosPersonales.Location = new System.Drawing.Point(276, 12);
            this.gbxDatosPersonales.Name = "gbxDatosPersonales";
            this.gbxDatosPersonales.Size = new System.Drawing.Size(446, 507);
            this.gbxDatosPersonales.TabIndex = 15;
            this.gbxDatosPersonales.TabStop = false;
            this.gbxDatosPersonales.Text = "Datos Personales";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(250, 382);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 34;
            this.label14.Text = "Cedula Dorso:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(56, 382);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "Cedula Frente:";
            // 
            // picCedBack
            // 
            this.picCedBack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picCedBack.Location = new System.Drawing.Point(252, 395);
            this.picCedBack.Name = "picCedBack";
            this.picCedBack.Size = new System.Drawing.Size(162, 95);
            this.picCedBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picCedBack.TabIndex = 32;
            this.picCedBack.TabStop = false;
            // 
            // picCedFront
            // 
            this.picCedFront.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picCedFront.Location = new System.Drawing.Point(52, 395);
            this.picCedFront.Name = "picCedFront";
            this.picCedFront.Size = new System.Drawing.Size(162, 95);
            this.picCedFront.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picCedFront.TabIndex = 31;
            this.picCedFront.TabStop = false;
            // 
            // picMale
            // 
            this.picMale.Image = global::EjemploWindowsForms.Properties.Resources.male;
            this.picMale.Location = new System.Drawing.Point(395, 11);
            this.picMale.Name = "picMale";
            this.picMale.Size = new System.Drawing.Size(34, 35);
            this.picMale.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMale.TabIndex = 30;
            this.picMale.TabStop = false;
            // 
            // picFem
            // 
            this.picFem.Image = global::EjemploWindowsForms.Properties.Resources.fem;
            this.picFem.Location = new System.Drawing.Point(350, 11);
            this.picFem.Name = "picFem";
            this.picFem.Size = new System.Drawing.Size(34, 35);
            this.picFem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picFem.TabIndex = 29;
            this.picFem.TabStop = false;
            // 
            // lblSex
            // 
            this.lblSex.AutoSize = true;
            this.lblSex.Location = new System.Drawing.Point(315, 28);
            this.lblSex.Name = "lblSex";
            this.lblSex.Size = new System.Drawing.Size(34, 13);
            this.lblSex.TabIndex = 28;
            this.lblSex.Text = "Sexo:";
            // 
            // txtWsqResult
            // 
            this.txtWsqResult.Location = new System.Drawing.Point(61, 225);
            this.txtWsqResult.Multiline = true;
            this.txtWsqResult.Name = "txtWsqResult";
            this.txtWsqResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtWsqResult.Size = new System.Drawing.Size(370, 38);
            this.txtWsqResult.TabIndex = 27;
            // 
            // txtAnsiResult
            // 
            this.txtAnsiResult.Location = new System.Drawing.Point(61, 197);
            this.txtAnsiResult.Name = "txtAnsiResult";
            this.txtAnsiResult.Size = new System.Drawing.Size(371, 20);
            this.txtAnsiResult.TabIndex = 26;
            // 
            // txtIsoResult
            // 
            this.txtIsoResult.Location = new System.Drawing.Point(61, 164);
            this.txtIsoResult.Name = "txtIsoResult";
            this.txtIsoResult.Size = new System.Drawing.Size(371, 20);
            this.txtIsoResult.TabIndex = 25;
            // 
            // txtRawResult
            // 
            this.txtRawResult.Location = new System.Drawing.Point(61, 117);
            this.txtRawResult.Multiline = true;
            this.txtRawResult.Name = "txtRawResult";
            this.txtRawResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRawResult.Size = new System.Drawing.Size(371, 37);
            this.txtRawResult.TabIndex = 24;
            // 
            // txtSerieResult
            // 
            this.txtSerieResult.Location = new System.Drawing.Point(332, 85);
            this.txtSerieResult.Name = "txtSerieResult";
            this.txtSerieResult.Size = new System.Drawing.Size(100, 20);
            this.txtSerieResult.TabIndex = 23;
            // 
            // txtDOEResult
            // 
            this.txtDOEResult.Location = new System.Drawing.Point(194, 85);
            this.txtDOEResult.Name = "txtDOEResult";
            this.txtDOEResult.Size = new System.Drawing.Size(100, 20);
            this.txtDOEResult.TabIndex = 22;
            // 
            // txtDOBResult
            // 
            this.txtDOBResult.Location = new System.Drawing.Point(60, 85);
            this.txtDOBResult.Name = "txtDOBResult";
            this.txtDOBResult.Size = new System.Drawing.Size(100, 20);
            this.txtDOBResult.TabIndex = 21;
            // 
            // txtApellidoPResult
            // 
            this.txtApellidoPResult.Location = new System.Drawing.Point(87, 51);
            this.txtApellidoPResult.Name = "txtApellidoPResult";
            this.txtApellidoPResult.Size = new System.Drawing.Size(345, 20);
            this.txtApellidoPResult.TabIndex = 19;
            // 
            // txtNombreResult
            // 
            this.txtNombreResult.Location = new System.Drawing.Point(186, 25);
            this.txtNombreResult.Name = "txtNombreResult";
            this.txtNombreResult.Size = new System.Drawing.Size(127, 20);
            this.txtNombreResult.TabIndex = 18;
            // 
            // txtRutResult
            // 
            this.txtRutResult.Location = new System.Drawing.Point(57, 25);
            this.txtRutResult.Name = "txtRutResult";
            this.txtRutResult.Size = new System.Drawing.Size(74, 20);
            this.txtRutResult.TabIndex = 17;
            // 
            // picFirma
            // 
            this.picFirma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFirma.Location = new System.Drawing.Point(272, 278);
            this.picFirma.Name = "picFirma";
            this.picFirma.Size = new System.Drawing.Size(162, 95);
            this.picFirma.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picFirma.TabIndex = 16;
            this.picFirma.TabStop = false;
            // 
            // picFoto
            // 
            this.picFoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFoto.Location = new System.Drawing.Point(155, 279);
            this.picFoto.Name = "picFoto";
            this.picFoto.Size = new System.Drawing.Size(94, 95);
            this.picFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picFoto.TabIndex = 15;
            this.picFoto.TabStop = false;
            // 
            // picJpg
            // 
            this.picJpg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picJpg.Location = new System.Drawing.Point(41, 278);
            this.picJpg.Name = "picJpg";
            this.picJpg.Size = new System.Drawing.Size(94, 96);
            this.picJpg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picJpg.TabIndex = 14;
            this.picJpg.TabStop = false;
            // 
            // lblFirmaResult
            // 
            this.lblFirmaResult.AutoSize = true;
            this.lblFirmaResult.Location = new System.Drawing.Point(269, 266);
            this.lblFirmaResult.Name = "lblFirmaResult";
            this.lblFirmaResult.Size = new System.Drawing.Size(35, 13);
            this.lblFirmaResult.TabIndex = 13;
            this.lblFirmaResult.Text = "Firma:";
            // 
            // lblJpgResult
            // 
            this.lblJpgResult.AutoSize = true;
            this.lblJpgResult.Location = new System.Drawing.Point(29, 263);
            this.lblJpgResult.Name = "lblJpgResult";
            this.lblJpgResult.Size = new System.Drawing.Size(27, 13);
            this.lblJpgResult.TabIndex = 12;
            this.lblJpgResult.Text = "Jpg:";
            // 
            // lblFotoResult
            // 
            this.lblFotoResult.AutoSize = true;
            this.lblFotoResult.Location = new System.Drawing.Point(152, 266);
            this.lblFotoResult.Name = "lblFotoResult";
            this.lblFotoResult.Size = new System.Drawing.Size(31, 13);
            this.lblFotoResult.TabIndex = 11;
            this.lblFotoResult.Text = "Foto:";
            // 
            // lblWsqResult
            // 
            this.lblWsqResult.AutoSize = true;
            this.lblWsqResult.Location = new System.Drawing.Point(29, 237);
            this.lblWsqResult.Name = "lblWsqResult";
            this.lblWsqResult.Size = new System.Drawing.Size(32, 13);
            this.lblWsqResult.TabIndex = 10;
            this.lblWsqResult.Text = "Wsq:";
            // 
            // lblAnsiResult
            // 
            this.lblAnsiResult.AutoSize = true;
            this.lblAnsiResult.Location = new System.Drawing.Point(28, 201);
            this.lblAnsiResult.Name = "lblAnsiResult";
            this.lblAnsiResult.Size = new System.Drawing.Size(30, 13);
            this.lblAnsiResult.TabIndex = 9;
            this.lblAnsiResult.Text = "Ansi:";
            // 
            // lblIsoResult
            // 
            this.lblIsoResult.AutoSize = true;
            this.lblIsoResult.Location = new System.Drawing.Point(28, 168);
            this.lblIsoResult.Name = "lblIsoResult";
            this.lblIsoResult.Size = new System.Drawing.Size(24, 13);
            this.lblIsoResult.TabIndex = 8;
            this.lblIsoResult.Text = "Iso:";
            // 
            // lblRawResult
            // 
            this.lblRawResult.AutoSize = true;
            this.lblRawResult.Location = new System.Drawing.Point(28, 130);
            this.lblRawResult.Name = "lblRawResult";
            this.lblRawResult.Size = new System.Drawing.Size(32, 13);
            this.lblRawResult.TabIndex = 7;
            this.lblRawResult.Text = "Raw:";
            // 
            // lblSerieResult
            // 
            this.lblSerieResult.AutoSize = true;
            this.lblSerieResult.Location = new System.Drawing.Point(297, 89);
            this.lblSerieResult.Name = "lblSerieResult";
            this.lblSerieResult.Size = new System.Drawing.Size(34, 13);
            this.lblSerieResult.TabIndex = 6;
            this.lblSerieResult.Text = "Serie:";
            // 
            // lblDOEResult
            // 
            this.lblDOEResult.AutoSize = true;
            this.lblDOEResult.Location = new System.Drawing.Point(163, 89);
            this.lblDOEResult.Name = "lblDOEResult";
            this.lblDOEResult.Size = new System.Drawing.Size(31, 13);
            this.lblDOEResult.TabIndex = 5;
            this.lblDOEResult.Text = "FDE:";
            // 
            // lblDOBResult
            // 
            this.lblDOBResult.AutoSize = true;
            this.lblDOBResult.Location = new System.Drawing.Point(28, 89);
            this.lblDOBResult.Name = "lblDOBResult";
            this.lblDOBResult.Size = new System.Drawing.Size(32, 13);
            this.lblDOBResult.TabIndex = 4;
            this.lblDOBResult.Text = "FDN:";
            // 
            // lblApellPatResult
            // 
            this.lblApellPatResult.AutoSize = true;
            this.lblApellPatResult.Location = new System.Drawing.Point(29, 56);
            this.lblApellPatResult.Name = "lblApellPatResult";
            this.lblApellPatResult.Size = new System.Drawing.Size(52, 13);
            this.lblApellPatResult.TabIndex = 2;
            this.lblApellPatResult.Text = "Apellidos:";
            // 
            // lblNombreResult
            // 
            this.lblNombreResult.AutoSize = true;
            this.lblNombreResult.Location = new System.Drawing.Point(136, 29);
            this.lblNombreResult.Name = "lblNombreResult";
            this.lblNombreResult.Size = new System.Drawing.Size(47, 13);
            this.lblNombreResult.TabIndex = 1;
            this.lblNombreResult.Text = "Nombre:";
            // 
            // lblRutResult
            // 
            this.lblRutResult.AutoSize = true;
            this.lblRutResult.Location = new System.Drawing.Point(28, 29);
            this.lblRutResult.Name = "lblRutResult";
            this.lblRutResult.Size = new System.Drawing.Size(27, 13);
            this.lblRutResult.TabIndex = 0;
            this.lblRutResult.Text = "Rut:";
            // 
            // gboxVerificacion
            // 
            this.gboxVerificacion.Controls.Add(this.lblRegistroCivilResult);
            this.gboxVerificacion.Controls.Add(this.lblRegistroCivil);
            this.gboxVerificacion.Controls.Add(this.lblCVenResult);
            this.gboxVerificacion.Controls.Add(this.lblMdEResult);
            this.gboxVerificacion.Controls.Add(this.lblVerificacion);
            this.gboxVerificacion.Controls.Add(this.lblCedulaVencidaResult);
            this.gboxVerificacion.Controls.Add(this.lblMenorEdadResult);
            this.gboxVerificacion.Location = new System.Drawing.Point(276, 525);
            this.gboxVerificacion.Name = "gboxVerificacion";
            this.gboxVerificacion.Size = new System.Drawing.Size(446, 117);
            this.gboxVerificacion.TabIndex = 16;
            this.gboxVerificacion.TabStop = false;
            this.gboxVerificacion.Text = "Verificación";
            // 
            // lblRegistroCivilResult
            // 
            this.lblRegistroCivilResult.AutoSize = true;
            this.lblRegistroCivilResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistroCivilResult.Location = new System.Drawing.Point(94, 76);
            this.lblRegistroCivilResult.Name = "lblRegistroCivilResult";
            this.lblRegistroCivilResult.Size = new System.Drawing.Size(78, 13);
            this.lblRegistroCivilResult.TabIndex = 6;
            this.lblRegistroCivilResult.Text = "Sin Consulta";
            // 
            // lblRegistroCivil
            // 
            this.lblRegistroCivil.AutoSize = true;
            this.lblRegistroCivil.Location = new System.Drawing.Point(5, 76);
            this.lblRegistroCivil.Name = "lblRegistroCivil";
            this.lblRegistroCivil.Size = new System.Drawing.Size(71, 13);
            this.lblRegistroCivil.TabIndex = 5;
            this.lblRegistroCivil.Text = "Registro Civil:";
            // 
            // lblCVenResult
            // 
            this.lblCVenResult.AutoSize = true;
            this.lblCVenResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCVenResult.Location = new System.Drawing.Point(94, 51);
            this.lblCVenResult.Name = "lblCVenResult";
            this.lblCVenResult.Size = new System.Drawing.Size(78, 13);
            this.lblCVenResult.TabIndex = 4;
            this.lblCVenResult.Text = "Sin Consulta";
            // 
            // lblMdEResult
            // 
            this.lblMdEResult.AutoSize = true;
            this.lblMdEResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMdEResult.Location = new System.Drawing.Point(94, 28);
            this.lblMdEResult.Name = "lblMdEResult";
            this.lblMdEResult.Size = new System.Drawing.Size(78, 13);
            this.lblMdEResult.TabIndex = 3;
            this.lblMdEResult.Text = "Sin Consulta";
            // 
            // lblVerificacion
            // 
            this.lblVerificacion.AutoSize = true;
            this.lblVerificacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVerificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVerificacion.Location = new System.Drawing.Point(206, 44);
            this.lblVerificacion.Name = "lblVerificacion";
            this.lblVerificacion.Size = new System.Drawing.Size(197, 22);
            this.lblVerificacion.TabIndex = 2;
            this.lblVerificacion.Text = "Esperando Verificación";
            // 
            // lblCedulaVencidaResult
            // 
            this.lblCedulaVencidaResult.AutoSize = true;
            this.lblCedulaVencidaResult.Location = new System.Drawing.Point(5, 51);
            this.lblCedulaVencidaResult.Name = "lblCedulaVencidaResult";
            this.lblCedulaVencidaResult.Size = new System.Drawing.Size(85, 13);
            this.lblCedulaVencidaResult.TabIndex = 1;
            this.lblCedulaVencidaResult.Text = "Cedula Vencida:";
            // 
            // lblMenorEdadResult
            // 
            this.lblMenorEdadResult.AutoSize = true;
            this.lblMenorEdadResult.Location = new System.Drawing.Point(5, 28);
            this.lblMenorEdadResult.Name = "lblMenorEdadResult";
            this.lblMenorEdadResult.Size = new System.Drawing.Size(83, 13);
            this.lblMenorEdadResult.TabIndex = 0;
            this.lblMenorEdadResult.Text = "Menor de Edad:";
            // 
            // chkBloqueoCedula
            // 
            this.chkBloqueoCedula.AutoSize = true;
            this.chkBloqueoCedula.Location = new System.Drawing.Point(21, 258);
            this.chkBloqueoCedula.Name = "chkBloqueoCedula";
            this.chkBloqueoCedula.Size = new System.Drawing.Size(157, 17);
            this.chkBloqueoCedula.TabIndex = 17;
            this.chkBloqueoCedula.Text = "Verificar Bloqueo de Cédula";
            this.chkBloqueoCedula.UseVisualStyleBackColor = true;
            // 
            // gboxIngresarParam
            // 
            this.gboxIngresarParam.Controls.Add(this.button4);
            this.gboxIngresarParam.Controls.Add(this.btnRefresh);
            this.gboxIngresarParam.Controls.Add(this.label12);
            this.gboxIngresarParam.Controls.Add(this.cboClientType);
            this.gboxIngresarParam.Controls.Add(this.btnLimpiar);
            this.gboxIngresarParam.Controls.Add(this.chkBloqueoCedula);
            this.gboxIngresarParam.Controls.Add(this.label1);
            this.gboxIngresarParam.Controls.Add(this.chkVigenciaCedula);
            this.gboxIngresarParam.Controls.Add(this.label3);
            this.gboxIngresarParam.Controls.Add(this.bloqueoVencidaCmbBx);
            this.gboxIngresarParam.Controls.Add(this.label2);
            this.gboxIngresarParam.Controls.Add(this.urlTxt);
            this.gboxIngresarParam.Controls.Add(this.rutTxt);
            this.gboxIngresarParam.Controls.Add(this.button1);
            this.gboxIngresarParam.Location = new System.Drawing.Point(9, 12);
            this.gboxIngresarParam.Name = "gboxIngresarParam";
            this.gboxIngresarParam.Size = new System.Drawing.Size(261, 419);
            this.gboxIngresarParam.TabIndex = 18;
            this.gboxIngresarParam.TabStop = false;
            this.gboxIngresarParam.Text = "Parámetros de Ingreso";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(21, 382);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 22;
            this.button4.Text = "Probar DR";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(159, 379);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 21;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 101);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Tipo Verificación";
            // 
            // cboClientType
            // 
            this.cboClientType.FormattingEnabled = true;
            this.cboClientType.Location = new System.Drawing.Point(24, 117);
            this.cboClientType.Name = "cboClientType";
            this.cboClientType.Size = new System.Drawing.Size(223, 21);
            this.cboClientType.TabIndex = 19;
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(159, 350);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 18;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(147, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Restricción Menores de Edad";
            // 
            // grpMensajeria
            // 
            this.grpMensajeria.Controls.Add(this.txtMensaje);
            this.grpMensajeria.Controls.Add(this.txtNroError);
            this.grpMensajeria.Controls.Add(this.label7);
            this.grpMensajeria.Controls.Add(this.label6);
            this.grpMensajeria.Controls.Add(this.label5);
            this.grpMensajeria.Controls.Add(this.rbtResponse);
            this.grpMensajeria.Controls.Add(this.label4);
            this.grpMensajeria.Controls.Add(this.rbtRequest);
            this.grpMensajeria.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpMensajeria.Location = new System.Drawing.Point(729, 13);
            this.grpMensajeria.Name = "grpMensajeria";
            this.grpMensajeria.Size = new System.Drawing.Size(659, 629);
            this.grpMensajeria.TabIndex = 19;
            this.grpMensajeria.TabStop = false;
            this.grpMensajeria.Text = "Mensajería";
            // 
            // txtMensaje
            // 
            this.txtMensaje.Location = new System.Drawing.Point(340, 558);
            this.txtMensaje.Multiline = true;
            this.txtMensaje.Name = "txtMensaje";
            this.txtMensaje.Size = new System.Drawing.Size(225, 57);
            this.txtMensaje.TabIndex = 19;
            // 
            // txtNroError
            // 
            this.txtNroError.Location = new System.Drawing.Point(134, 563);
            this.txtNroError.Name = "txtNroError";
            this.txtNroError.Size = new System.Drawing.Size(112, 21);
            this.txtNroError.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(338, 540);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 15);
            this.label7.TabIndex = 5;
            this.label7.Text = "Mensaje";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(46, 566);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "Error Número";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 15);
            this.label5.TabIndex = 3;
            this.label5.Text = "Retorno de Servicio";
            // 
            // rbtResponse
            // 
            this.rbtResponse.Location = new System.Drawing.Point(15, 163);
            this.rbtResponse.Name = "rbtResponse";
            this.rbtResponse.Size = new System.Drawing.Size(629, 356);
            this.rbtResponse.TabIndex = 2;
            this.rbtResponse.Text = "";
            this.rbtResponse.WordWrap = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "Llamado al servicio";
            // 
            // rbtRequest
            // 
            this.rbtRequest.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtRequest.Location = new System.Drawing.Point(15, 46);
            this.rbtRequest.Name = "rbtRequest";
            this.rbtRequest.Size = new System.Drawing.Size(629, 96);
            this.rbtRequest.TabIndex = 0;
            this.rbtRequest.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtTheme);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.picFotoBP);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtToken);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.picHuellaToken);
            this.groupBox1.Controls.Add(this.btnBPVerify);
            this.groupBox1.Controls.Add(this.btnBPEnroll);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtCompanyId);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtURLBP);
            this.groupBox1.Location = new System.Drawing.Point(13, 648);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(985, 101);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enroll & Verify via Biometrika BioPortal...";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(237, 76);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 33;
            this.label11.Text = "Theme";
            // 
            // txtTheme
            // 
            this.txtTheme.Location = new System.Drawing.Point(283, 71);
            this.txtTheme.Name = "txtTheme";
            this.txtTheme.Size = new System.Drawing.Size(76, 20);
            this.txtTheme.TabIndex = 32;
            this.txtTheme.Text = "itau1.png";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(771, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 31;
            this.button3.Text = "Borrar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // picFotoBP
            // 
            this.picFotoBP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFotoBP.Location = new System.Drawing.Point(884, 15);
            this.picFotoBP.Name = "picFotoBP";
            this.picFotoBP.Size = new System.Drawing.Size(78, 81);
            this.picFotoBP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picFotoBP.TabIndex = 30;
            this.picFotoBP.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(538, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "Token";
            // 
            // txtToken
            // 
            this.txtToken.Location = new System.Drawing.Point(535, 33);
            this.txtToken.Multiline = true;
            this.txtToken.Name = "txtToken";
            this.txtToken.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtToken.Size = new System.Drawing.Size(332, 61);
            this.txtToken.TabIndex = 28;
            this.txtToken.TextChanged += new System.EventHandler(this.txtToken_TextChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(283, 42);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 16;
            this.button2.Text = "Captura";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // picHuellaToken
            // 
            this.picHuellaToken.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picHuellaToken.Location = new System.Drawing.Point(365, 13);
            this.picHuellaToken.Name = "picHuellaToken";
            this.picHuellaToken.Size = new System.Drawing.Size(78, 81);
            this.picHuellaToken.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picHuellaToken.TabIndex = 15;
            this.picHuellaToken.TabStop = false;
            // 
            // btnBPVerify
            // 
            this.btnBPVerify.Location = new System.Drawing.Point(449, 43);
            this.btnBPVerify.Name = "btnBPVerify";
            this.btnBPVerify.Size = new System.Drawing.Size(75, 23);
            this.btnBPVerify.TabIndex = 7;
            this.btnBPVerify.Text = "Verificar";
            this.btnBPVerify.UseVisualStyleBackColor = true;
            this.btnBPVerify.Click += new System.EventHandler(this.btnBPVerify_Click);
            // 
            // btnBPEnroll
            // 
            this.btnBPEnroll.Location = new System.Drawing.Point(86, 71);
            this.btnBPEnroll.Name = "btnBPEnroll";
            this.btnBPEnroll.Size = new System.Drawing.Size(75, 23);
            this.btnBPEnroll.TabIndex = 6;
            this.btnBPEnroll.Text = "Enrolar";
            this.btnBPEnroll.UseVisualStyleBackColor = true;
            this.btnBPEnroll.Click += new System.EventHandler(this.btnBPEnroll_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Company Id";
            // 
            // txtCompanyId
            // 
            this.txtCompanyId.Location = new System.Drawing.Point(86, 45);
            this.txtCompanyId.Name = "txtCompanyId";
            this.txtCompanyId.Size = new System.Drawing.Size(69, 20);
            this.txtCompanyId.TabIndex = 4;
            this.txtCompanyId.Text = "18";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "RUT";
            // 
            // txtURLBP
            // 
            this.txtURLBP.Location = new System.Drawing.Point(58, 19);
            this.txtURLBP.Name = "txtURLBP";
            this.txtURLBP.Size = new System.Drawing.Size(220, 20);
            this.txtURLBP.TabIndex = 2;
            this.txtURLBP.Text = "http://qabpservice.biometrikalatam.com/";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1408, 645);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpMensajeria);
            this.Controls.Add(this.gboxIngresarParam);
            this.Controls.Add(this.gboxVerificacion);
            this.Controls.Add(this.gbxDatosPersonales);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "BVI - Client";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbxDatosPersonales.ResumeLayout(false);
            this.gbxDatosPersonales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCedBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCedFront)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFirma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picJpg)).EndInit();
            this.gboxVerificacion.ResumeLayout(false);
            this.gboxVerificacion.PerformLayout();
            this.gboxIngresarParam.ResumeLayout(false);
            this.gboxIngresarParam.PerformLayout();
            this.grpMensajeria.ResumeLayout(false);
            this.grpMensajeria.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoBP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHuellaToken)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox rutTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox urlTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox bloqueoVencidaCmbBx;
        private System.Windows.Forms.CheckBox chkVigenciaCedula;
        private System.Windows.Forms.GroupBox gbxDatosPersonales;
        private System.Windows.Forms.TextBox txtWsqResult;
        private System.Windows.Forms.TextBox txtAnsiResult;
        private System.Windows.Forms.TextBox txtIsoResult;
        private System.Windows.Forms.TextBox txtRawResult;
        private System.Windows.Forms.TextBox txtSerieResult;
        private System.Windows.Forms.TextBox txtDOEResult;
        private System.Windows.Forms.TextBox txtDOBResult;
        private System.Windows.Forms.TextBox txtApellidoPResult;
        private System.Windows.Forms.TextBox txtNombreResult;
        private System.Windows.Forms.TextBox txtRutResult;
        private System.Windows.Forms.PictureBox picFirma;
        private System.Windows.Forms.PictureBox picFoto;
        private System.Windows.Forms.PictureBox picJpg;
        private System.Windows.Forms.Label lblFirmaResult;
        private System.Windows.Forms.Label lblJpgResult;
        private System.Windows.Forms.Label lblFotoResult;
        private System.Windows.Forms.Label lblWsqResult;
        private System.Windows.Forms.Label lblAnsiResult;
        private System.Windows.Forms.Label lblIsoResult;
        private System.Windows.Forms.Label lblRawResult;
        private System.Windows.Forms.Label lblSerieResult;
        private System.Windows.Forms.Label lblDOEResult;
        private System.Windows.Forms.Label lblDOBResult;
        private System.Windows.Forms.Label lblApellPatResult;
        private System.Windows.Forms.Label lblNombreResult;
        private System.Windows.Forms.Label lblRutResult;
        private System.Windows.Forms.GroupBox gboxVerificacion;
        private System.Windows.Forms.Label lblCVenResult;
        private System.Windows.Forms.Label lblMdEResult;
        private System.Windows.Forms.Label lblVerificacion;
        private System.Windows.Forms.Label lblCedulaVencidaResult;
        private System.Windows.Forms.Label lblMenorEdadResult;
        private System.Windows.Forms.CheckBox chkBloqueoCedula;
        private System.Windows.Forms.GroupBox gboxIngresarParam;
        private System.Windows.Forms.Label lblRegistroCivilResult;
        private System.Windows.Forms.Label lblRegistroCivil;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.PictureBox picMale;
        private System.Windows.Forms.PictureBox picFem;
        private System.Windows.Forms.Label lblSex;
        private System.Windows.Forms.GroupBox grpMensajeria;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox rbtResponse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox rbtRequest;
        private System.Windows.Forms.TextBox txtMensaje;
        private System.Windows.Forms.TextBox txtNroError;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBPVerify;
        private System.Windows.Forms.Button btnBPEnroll;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCompanyId;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtURLBP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtToken;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox picHuellaToken;
        private System.Windows.Forms.PictureBox picFotoBP;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtTheme;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cboClientType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox picCedBack;
        private System.Windows.Forms.PictureBox picCedFront;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button button4;
    }
}

