﻿//using Biometrika.BioApi20.BSP;
using DRUIAdapter7.Helpers;
using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DRUIAdapter7
{
    public static class Program
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Program));

        internal static string _MACADRESS = "Desconocida";
        internal static string _GEOLOCALIZATION = null;


        public static bool HaveData
        {
            get {
                return bviUI == null ? false : bviUI.HaveData;
            }
        }
        public static BioPacket Packet { get; set; }

        private static DRUI bviUI;

        //private static BSPBiometrika _BSP;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();

            //Application.SetCompatibleTextRenderingDefault(false);
            bviUI = new DRUI();
            bviUI.ReadConfig();
            try
            {
                _MACADRESS = Utils.Utils.GetMACAddress();
                if (!string.IsNullOrEmpty(_MACADRESS) && _MACADRESS.Length > 50)
                {
                    _MACADRESS = _MACADRESS.Substring(0, 50);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Program.Main - Error Getting MACAddress: " + ex.Message);
            }

            try
            {
                _GEOLOCALIZATION = Utils.Utils.GetGeolocalization();
                if (!string.IsNullOrEmpty(_GEOLOCALIZATION) && _GEOLOCALIZATION.Length > 50)
                {
                    _GEOLOCALIZATION = _GEOLOCALIZATION.Substring(0, 50);
                }
                if (!string.IsNullOrEmpty(_GEOLOCALIZATION))
                {
                    _GEOLOCALIZATION = DRUI._DRCONFIG.CoordinatesGeoRef;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Program.Main - Error GetGeolocalization: " + ex.Message);
            }
           
            //bviUI.SetBSP(_BSP);
            int ret = bviUI.Channel(Packet);
            if (ret == 0)
            {
                bviUI.InitAdapter(null); //Setea lo inicial de acuerdo a parametros y configuraciones
                Application.Run(bviUI);
            }
            else
            {
                Packet.PacketParamOut.Add("Error", GetError(ret));
            }
        }

        //public static void SetBSP(BSPBiometrika bsp)
        //{
        //    try
        //    {
        //        _BSP = bsp;
        //    }
        //    catch (Exception ex)
        //    {
        //        Packet.PacketParamOut.Add("Error", GetError(-4));
        //    }
        //}

        private static string GetError(int ret)
        {
            string sret = null;
            switch (ret)
            {
                case -1:
                    sret = "-1|Error desconocido. Ver log";
                    break;
                case -2:
                    sret = "-2|Parametros nulos";
                    break;
                case -3:
                    sret = "-3|Parametros src/TypeId/ValueId denben existir y no pueden ser nulos";
                    break;
                case -4:
                    sret = "-4|Error seteando BSP";
                    break;
                default:
                    sret = "-1|Error desconocido. Ver log";
                    break;
            }
            return sret;
        }

        public static void SetPacket(NameValueCollection queryString)
        {
            Packet = new BioPacket();
            Packet.PacketParamIn = queryString;
            Packet.PacketParamOut = new Dictionary<string, object>();
        }

        public static void Close()
        {
            bviUI.Close();
            bviUI = null;
        }
    }
}
