﻿using Bio.Digital.Receipt.Api;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRUIAdapter7.Helpers
{
    internal class NVHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NVHelper));

        internal string _URLService = "";
        internal int _TimeoutService = 60000;
        internal int _CompanyService = 7;


        internal NVHelper() { }

        internal NVHelper(string URLService, int TimeoutService, int CompanyService) {
            _URLService = URLService;
            _TimeoutService = TimeoutService;
            _CompanyService = CompanyService;
        }

        //La tengo por si se necesita, pero por ahora uso directo la de procesar cedula que si no existe crea Tx
        //asi nos evitamos un llamado al servicio
        internal int InitTransaction(Persona inPersona, out string msgerr, out string trackidci)
        {
            int ret = 0;
            msgerr = null;
            trackidci = null;

            return ret;
        }

        /// <summary>
        /// Dadas las fotos de front y back de la cédula, se procesa en server para reconocer toda la información, y 
        /// se crea la transacción unica en server, la cual será utilizada en todos los procesos posteriores sin 
        /// importar el tipo de verificación realizada.
        /// </summary>
        /// <param name="inPersona"></param>
        /// <param name="outPersona"></param>
        /// <param name="msgerr"></param>
        /// <param name="trackidci"></param>
        /// <returns></returns>
        internal int ParseOCRDocument(int clientType, Persona inPersona, string username, 
                                      out Persona outPersona, out string msgerr, out string trackidci)
        {
            int ret = 0;
            msgerr = null;
            outPersona = inPersona;
            trackidci = null;

            try
            {
                LOG.Info("NVHelper.ParseOCRDocument IN...[RUT = " + outPersona.Rut + "]");
                string xmlparamin, xmlparamout;
                using (WSCI.WSCI ws = new WSCI.WSCI())
                {
                    ws.Url = DRUI._DRCONFIG.URLWebService; //Properties.Settings.Default.ServiceURLBase + "WSCI.asmx";
                    ws.Timeout = DRUI._DRCONFIG.ServiceTimeout;
                    Bio.Digital.Receipt.Api.XmlParamIn oParamin = new Bio.Digital.Receipt.Api.XmlParamIn();
                    oParamin.Actionid = 2;
                    oParamin.Origin = 1;
                    oParamin.Companyid = DRUI._DRCONFIG.ServiceCompany;
                    oParamin.ApplicationId = DRUI._DRCONFIG.ServiceAppId;
                    oParamin.CIParam = new Bio.Digital.Receipt.Api.CIParam();
                    oParamin.CIParam.TrackId = null;
                    oParamin.CIParam.TypeId = "RUT";
                    oParamin.CIParam.ValueId = string.IsNullOrEmpty(outPersona.Rut)?"NA": outPersona.Rut;
                    oParamin.CIParam.IDCardImageFront = outPersona.FotoFrontCedula;
                    oParamin.CIParam.IDCardImageBack = outPersona.FotoBackCedula;
                    oParamin.CIParam.WorkStationID = Program._MACADRESS;
                    oParamin.CIParam.GeoRef = Program._GEOLOCALIZATION;

                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    //Added para registrar user que hace la verificacion 
                    if (!string.IsNullOrEmpty(username))
                    {
                        dic.Add("UserName", username);
                    }
                    //Added para Reconocer PDF417
                    //if (clientType < 2) //Si es cedula con huella, pido reconocimiento de PDF17 sino no
                    //{
                        
                    //    dic.Add("PDF417Recognition", "ON");
                    //}
                    if (dic != null && dic.Count > 0)
                    {
                        oParamin.Additionaldata = JsonConvert.SerializeObject(dic);
                    } else
                    {
                        oParamin.Additionaldata = null;
                    }

                    xmlparamin = Bio.Digital.Receipt.Api.XmlUtils.SerializeAnObject(oParamin);
                    LOG.Debug("NVHelper.ParseOCRDocument - xmlparamin = " + xmlparamin);
                    LOG.Info("NVHelper.ParseOCRDocument - WSCI.WSCI URL = " + ws.Url);
                    ret = ws.Process(xmlparamin, out xmlparamout);
                    LOG.Debug("NVHelper.ParseOCRDocument - ret = " + ret + " => xmlparamout = " + (string.IsNullOrEmpty(xmlparamout) ? "NULL" : xmlparamout));
                    if (ret == 0)
                    {
                        if (!string.IsNullOrEmpty(xmlparamout))
                        {
                            XmlParamOut oParamout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                            if (oParamout.CIParam != null) 
                            {
                                if (!string.IsNullOrEmpty(oParamout.CIParam.TypeId) && !string.IsNullOrEmpty(oParamout.CIParam.ValueId))
                                {
                                    //Chequeo que el rut reconocido coincida con el pedido a verificar, 
                                    //sino bloqueo
                                    if ((!string.IsNullOrEmpty(outPersona.Rut) && !outPersona.Rut.Equals("NA")) &&
                                        !oParamout.CIParam.ValueId.Equals(outPersona.Rut))
                                    {
                                        msgerr = "RUT Reconocido [" + oParamout.CIParam.ValueId + "] no coincide con el RUT pedido [" +
                                                    outPersona.Rut + "]";
                                        LOG.Debug("NVHelper.ParseOCRDocument - Return = " +
                                                    Errors.IRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED.ToString() + " - " + msgerr);
                                        return Errors.IRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED;
                                    }

                                    outPersona.Serie = oParamout.CIParam.Serial;
                                    if (outPersona.Serie.Substring(0, 1).Equals("A"))
                                    {
                                        outPersona.TipoIdentificacion = TipoIdentificacion.CedulaChilenaAntigua;
                                        //Verifica que reconocio PDF417 sino pedir de nuevo

                                        if (clientType < 2 && !string.IsNullOrEmpty(oParamout.Additionaldata)) //.Equals("PDF417Recognized=OK"))
                                        {
                                            Dictionary<string, string> dicAD = JsonConvert.DeserializeObject<Dictionary<string, string>>(oParamout.Additionaldata);
                                            if (dicAD.ContainsKey("PDF417Recognized") && !dicAD["PDF417Recognized"].Equals("OK"))
                                            {
                                                msgerr = "PDF417 No Reconocido";
                                                LOG.Debug("NVHelper.ParseOCRDocument - Return = -3000 - " + msgerr);
                                                return -3000;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        outPersona.TipoIdentificacion = TipoIdentificacion.CedulaChilenaNueva;
                                    }

                                    trackidci = oParamout.CIParam.TrackId;
                                    LOG.Debug("NVHelper.ParseOCRDocument - TrackId Recibida = " + trackidci);
                                    outPersona.Rut = oParamout.CIParam.ValueId;
                                    LOG.Debug("NVHelper.ParseOCRDocument - Procesando RUT Reconocido = " + outPersona.Rut);
                                    outPersona.Nombre = oParamout.CIParam.Name;
                                    outPersona.Apellido = oParamout.CIParam.PhaterLastName +
                                                          (string.IsNullOrEmpty(oParamout.CIParam.MotherLastName) ? "" :
                                                           " " + oParamout.CIParam.MotherLastName);
                                    outPersona.FechaNacimiento = Utils.Utils.Parse(oParamout.CIParam.BirthDate);
                                    outPersona.Nacionalidad = oParamout.CIParam.Nacionality;
                                    outPersona.Sexo = oParamout.CIParam.Sex;
                                    outPersona.FechaExpiracion = Utils.Utils.Parse(oParamout.CIParam.ExpirationDate);
                                    
                                    LOG.Debug("NVHelper.ParseOCRDocument Cedula leida => " +
                                        oParamout.CIParam.ValueId + "-" +
                                        oParamout.CIParam.Name + "-" +
                                        oParamout.CIParam.BirthDate + "-" +
                                        oParamout.CIParam.Nacionality + "-" +
                                        oParamout.CIParam.Sex + "-" +
                                        oParamout.CIParam.ExpirationDate + "-" +
                                        oParamout.CIParam.Serial);

                                    outPersona.IsCedulaVigente = outPersona.FechaExpiracion.HasValue ?
                                                    Utils.Utils.IsCedulaVigente(outPersona.FechaExpiracion.Value) : false;
                                    LOG.Debug("NVHelper.ParseOCRDocument Cedula Vigente = " + outPersona.IsCedulaVigente.ToString());
                                    outPersona.IsMenorDeEdad = outPersona.FechaNacimiento.HasValue ?
                                                    Utils.Utils.IsMenorDe18(outPersona.FechaNacimiento.Value) : false;
                                    LOG.Debug("NVHelper.ParseOCRDocument Menore = " + outPersona.IsMenorDeEdad.ToString());

                                    outPersona.FotoFrontCedula = oParamout.CIParam.IDCardImageFront;
                                    outPersona.FotoBackCedula = oParamout.CIParam.IDCardImageBack;
                                    outPersona.Foto = oParamout.CIParam.IDCardPhotoImage;
                                    outPersona.FotoFirma = oParamout.CIParam.IDCardSignatureImage;

                                    outPersona.CertifyPdf = oParamout.CIParam.CertificatePDF;
                                }
                                else
                                {
                                    msgerr = "RUT No Reconocido";
                                    LOG.Debug("NVHelper.ParseOCRDocument - Return = " +
                                                Errors.IRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED.ToString() + " - " + msgerr);
                                    return Errors.IRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED;
                                }
                            }
                            else
                            {
                                msgerr = "Retorno Deserializado Vacio";
                                LOG.Debug("NVHelper.ParseOCRDocument - Return = " +
                                            Errors.IRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED.ToString() + " - " + msgerr);
                                return Errors.IRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED;
                            }
                        }
                        else
                        {
                            msgerr = "Retorno Vacio Desde Servicio";
                            LOG.Debug("NVHelper.ParseOCRDocument - Return = " +
                                        Errors.IRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED.ToString() + " - " + msgerr);
                            return Errors.IRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED;
                        }
                    }
                    else
                    {
                        msgerr = "Retorno Error Desde Servicio [" + ret.ToString() + "]";
                        LOG.Debug("NVHelper.ParseOCRDocument - Return = " +
                                    Errors.IRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED.ToString() + " - " + msgerr);
                        return Errors.IRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("NVHelper.ParseOCRDocument - Error Desconocido: " + ex.Message);
            }

            return ret;
        }

        internal int GenerateISOCompact(string wsq, out string ISOCompact, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            ISOCompact = null;

            return ret;
        }

        internal int CertificationComplete(string trackidci, Persona inPersona, bool resultMatch, out string certifypdf, out string msgerr)
        {
            int ret = Errors.IRET_OK;
            msgerr = null;
            certifypdf = null;
            try
            {
                LOG.Info("NVHelper.CertificationComplete - IN...");
                if (inPersona.TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
                {
                    LOG.Info("NVHelper.CertificationComplete - [Cedula Nueva] Salvando resultado de trackidci = " + trackidci +
                        " - RUT = " + inPersona.Rut + " => Verificacion = " + resultMatch.ToString() + "...");

                    LOG.Debug("BPHelper.TxCreateSigner - LLama a " + DRUI._DRCONFIG.URLServiceBase + "ciRegisterTx...");
                    var client = new RestClient(DRUI._DRCONFIG.URLServiceBase + "ciRegisterTx");
                    client.Timeout = DRUI._DRCONFIG.ServiceTimeout;
                    var request = new RestRequest(Method.POST);
                    client.UserAgent = "biometrika";  
                    request.AddHeader("Authorization", "Basic " +
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(DRUI._DRCONFIG.ServiceAccessName + ":" +
                                                                            DRUI._DRCONFIG.ServiceSecretKey)));
                    request.AddHeader("Content-Type", "application/json");
                    request.AddHeader("Accept", "application/json");
                    CIRegisterModel CIRModel = new CIRegisterModel(DRUI._DRCONFIG.ServiceCompany,
                                                                   DRUI._DRCONFIG.ServiceAppId, trackidci, 
                                                                   "RUT", inPersona.Rut, 40, (resultMatch?100:0), null, null, 
                                                                   inPersona.Selfie, inPersona.Wsq, inPersona.JpegOriginal); 
                    string jsonParams = JsonConvert.SerializeObject(CIRModel);
                    request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                    LOG.Debug("BPHelper.TxCreateSigner - call execute...");
                    IRestResponse response = client.Execute(request);

                    if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        CIRegisterModelR oResponse = JsonConvert.DeserializeObject<CIRegisterModelR>(response.Content);
                        if (oResponse != null)
                        {
                            LOG.Debug("NVHelper.CertificationComplete - Set CertifyPDF returned...");
                            certifypdf = oResponse.certifyPDF;
                        }
                        LOG.Debug("NVHelper.CertificationComplete - Response OK");
                    } else
                    {
                        LOG.Debug("NVHelper.CertificationComplete - Response Error");
                        ErrorModel err = JsonConvert.DeserializeObject<ErrorModel>(response.Content);
                        LOG.Debug("NVHelper.CertificationComplete - ErrorModel != null => " + (err != null).ToString());
                        msgerr = "Error del servicio: " + (response!=null ? response.StatusCode.ToString() : "") + 
                                 "[" + (err!=null ? err.message : "") + "]";
                        ret = Errors.IRET_ERR_CI_NO_COMPLETE;
                    }
                } else
                {
                    LOG.Info("NVHelper.CertificationComplete - [Cedula Vieja] Verificando trackidci = " + trackidci +
                        " - RUT = " + inPersona.Rut + "...");
                }

            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("NVHelper.CertificationComplete - Error: " + ex.Message);
            }

            return ret;
        }

        internal static int ExportPDFbyMail(string trackid, string mail, out string msgerr)
        {
            msgerr = "";
            int ret = Errors.IRET_OK;
            try
            {
                LOG.Info("NVHelper.ExportPDFbyMail - IN...");
                
                LOG.Debug("NVHelper.ExportPDFbyMail - LLama a " + DRUI._DRCONFIG.URLServiceBase + "ciInformPDFbyMail...");
                var client = new RestClient(DRUI._DRCONFIG.URLServiceBase + "ciInformPDFbyMail?trackid=" +
                                            trackid + "&mail=" + mail);
                client.Timeout = DRUI._DRCONFIG.ServiceTimeout;
                var request = new RestRequest(Method.POST);
                client.UserAgent = "biometrika";
                request.AddHeader("Authorization", "Basic " +
                    Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(DRUI._DRCONFIG.ServiceAccessName + ":" +
                                                                        DRUI._DRCONFIG.ServiceSecretKey)));

                LOG.Debug("NVHelper.ExportPDFbyMail - call execute...");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    LOG.Debug("NVHelper.ExportPDFbyMail - Response OK");
                }
                else
                {
                    LOG.Debug("NVHelper.ExportPDFbyMail - Response Error");
                    ErrorModel err = JsonConvert.DeserializeObject<ErrorModel>(response.Content);
                    LOG.Debug("NVHelper.ExportPDFbyMail - ErrorModel != null => " + (err != null).ToString());
                    msgerr = response.StatusCode.ToString() + "[" + err.message + "]";
                    ret = Errors.IRET_ERR_CI_NO_COMPLETE;
                }
                
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("NVHelper.ExportPDFbyMail - Error: " + ex.Message);
            }
            return ret;
        }

        internal static int DownloadCIPDF(string trackid, out byte[] byPDF, out string msgerr)
        {
            msgerr = "";
            byPDF = null;
            int ret = Errors.IRET_OK;
            try
            {
                LOG.Info("NVHelper.DownloadCIPDF - IN...");

                LOG.Debug("BPHelper.DownloadCIPDF - LLama a " + DRUI._DRCONFIG.URLServiceBase + "ciDownloadCI...");
                var client = new RestClient(DRUI._DRCONFIG.URLServiceBase + "ciDownloadCI?trackidci=" + trackid + 
                                            "&format=pdf");
                client.Timeout = DRUI._DRCONFIG.ServiceTimeout;
                var request = new RestRequest(Method.GET);
                client.UserAgent = "biometrika";
                request.AddHeader("Authorization", "Basic " +
                    Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(DRUI._DRCONFIG.ServiceAccessName + ":" +
                                                                        DRUI._DRCONFIG.ServiceSecretKey)));
                //request.AddHeader("Content-Type", "application/json");
                //request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                //request.AddParameter("trackid", trackid);
                //request.AddParameter("format", "pdf");
                LOG.Debug("BPHelper.ExportPDFbyMail - call execute...");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    LOG.Debug("NVHelper.ExportPDFbyMail - Response OK");
                    byPDF = (byte[])response.RawBytes;
                    LOG.Debug("NVHelper.ExportPDFbyMail - byPDF.Length" + (byPDF!=null?byPDF.Length.ToString():"Null"));
                }
                else
                {
                    LOG.Debug("NVHelper.ExportPDFbyMail - Response Error");
                    ErrorModel err = JsonConvert.DeserializeObject<ErrorModel>(response.Content);
                    LOG.Debug("NVHelper.ExportPDFbyMail - ErrorModel != null => " + (err != null).ToString());
                    msgerr = response.StatusCode.ToString() + "[" + err.message + "]";
                    ret = Errors.IRET_ERR_CI_NO_COMPLETE;
                }

            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("NVHelper.ExportPDFbyMail - Error: " + ex.Message);
            }
            return ret;
        }

        internal int VerifyByNec(string trackidci, string fullPdf417, Persona inPersona, 
                                 out bool resultMatch, out double score, out string certifypdf, out string msgerr)
        {
            int ret = Errors.IRET_OK;
            msgerr = null;
            resultMatch = false;
            score = 0;
            certifypdf = null;
            try
            {
                //LOG.Info("NVHelper.VerifyByNec - IN...");
                //if (inPersona.TipoIdentificacion == TipoIdentificacion.CedulaChilenaAntigua)
                //{
                //    LOG.Info("NVHelper.VerifyByNec - [Cedula Nueva] Salvando resultado de trackidci = " + trackidci +
                //        " - RUT = " + inPersona.Rut + " => Verificacion = " + resultMatch.ToString() + "...");

                //    LOG.Debug("NVHelper.VerifyByNec - LLama a " + Properties.Settings.Default.ServiceURLBase + "ciVerifyNECTx...");
                //    var client = new RestClient(Properties.Settings.Default.ServiceURLBase + "ciVerifyNECTx");
                //    client.Timeout = Properties.Settings.Default.ServiceTimeout;
                //    var request = new RestRequest(Method.POST);
                //    client.UserAgent = "biometrika";
                //    request.AddHeader("Authorization", "Basic " +
                //        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Properties.Settings.Default.ServiceAccessName + ":" +
                //                                                            Properties.Settings.Default.ServiceSecretKey)));
                //    request.AddHeader("Content-Type", "application/json");
                //    request.AddHeader("Accept", "application/json");
                //    CIVerifyModel CIVModel = new CIVerifyModel(Properties.Settings.Default.ServiceCompany,
                //                                                   Properties.Settings.Default.ServiceAppId, trackidci,
                //                                                   "RUT", inPersona.Rut, Properties.Settings.Default.NECThreshold, 
                //                                                   (resultMatch ? 100 : 0), null, null,
                //                                                   inPersona.Selfie, fullPdf417, inPersona.Wsq, inPersona.JpegOriginal);
                //    string jsonParams = JsonConvert.SerializeObject(CIVModel);
                //    request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                //    LOG.Debug("NVHelper.VerifyByNec - call execute...");
                //    IRestResponse response = client.Execute(request);

                //    if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                //    {
                //        LOG.Debug("NVHelper.VerifyByNec - Response OK");
                //        CIVerifyModelR oCIVerifyModelR = JsonConvert.DeserializeObject<CIVerifyModelR>(response.Content);
                //        if (oCIVerifyModelR != null)
                //        {
                //            resultMatch = (oCIVerifyModelR.resultVerify == 1);
                //            score = oCIVerifyModelR.score;
                //            certifypdf = oCIVerifyModelR.certifypdf;
                //            LOG.Debug("NVHelper.VerifyByNec - Result = " + oCIVerifyModelR.resultVerify.ToString() + "[Score=" +
                //                oCIVerifyModelR.score.ToString() + "/" + oCIVerifyModelR.threshold);
                //        } else
                //        {
                //            LOG.Debug("NVHelper.VerifyByNec - Error parseando retorno...CIVerifyModelR==null");
                //            ret = Errors.IRET_ERR_DESERIALIZE_RESPONSE;
                //        }
                //    }
                //    else
                //    {
                //        LOG.Debug("NVHelper.VerifyByNec - Response Error");
                //        ErrorModel err = JsonConvert.DeserializeObject<ErrorModel>(response.Content);
                //        LOG.Debug("NVHelper.VerifyByNec - ErrorModel != null => " + (err != null).ToString());
                //        msgerr = "Error del servicio: " + response.StatusCode.ToString() + "[" + err.message + "]";
                //        ret = Errors.IRET_ERR_CI_NO_COMPLETE;
                //    }
                //}
                //else
                //{
                //    LOG.Info("NVHelper.VerifyByNec - [Cedula Vieja] Verificando trackidci = " + trackidci +
                //        " - RUT = " + inPersona.Rut + "...");
                //}

            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("NVHelper.VerifyByNec - Error: " + ex.Message);
            }

            return ret;
        }

        internal int VerifyFacial(string trackidci, Persona inPersona, 
                                  out bool bReturnMatch, out double score, out string certifypdf, out string msgerr)
        {
            int ret = Errors.IRET_OK;
            bReturnMatch = false;
            score = 0;
            msgerr = null;
            certifypdf = null;
            try
            {
                //LOG.Debug("NVHelper.VerifyFacial - IN...");
                //string xmlparamin, xmlparamout;
                //using (WSCI.WSCI ws = new WSCI.WSCI())
                //{
                //    ws.Url = Properties.Settings.Default.DRUIAdapter7_WSCI_WSCI; //Properties.Settings.Default.ServiceURLBase + "WSCI.asmx";
                //    ws.Timeout = Properties.Settings.Default.ServiceTimeout;
                //    Bio.Digital.Receipt.Api.XmlParamIn oParamin = new Bio.Digital.Receipt.Api.XmlParamIn();
                //    oParamin.Actionid = 3;
                //    oParamin.Origin = 1;
                //    oParamin.Companyid = Properties.Settings.Default.ServiceCompany;
                //    oParamin.ApplicationId = Properties.Settings.Default.ServiceAppId;
                //    oParamin.CIParam = new Bio.Digital.Receipt.Api.CIParam();
                //    oParamin.CIParam.TrackId = trackidci;
                //    oParamin.CIParam.TypeId = "RUT";
                //    oParamin.CIParam.ValueId = string.IsNullOrEmpty(inPersona.Rut) ? "NA" : inPersona.Rut;
                //    oParamin.CIParam.IDCardImageFront = inPersona.FotoFrontCedula;
                //    oParamin.CIParam.IDCardImageBack = inPersona.FotoBackCedula;
                //    oParamin.CIParam.Selfie = inPersona.Selfie;
                //    oParamin.CIParam.WorkStationID = Program._MACADRESS;
                //    oParamin.CIParam.GeoRef = Program._GEOLOCALIZATION;
                //    oParamin.CIParam.DestinaryMail = "info@notariovirtual.cl";
                //    oParamin.CIParam.Threshold = Properties.Settings.Default.FacialThreshold.ToString();
                //    xmlparamin = Bio.Digital.Receipt.Api.XmlUtils.SerializeAnObject(oParamin);
                //    LOG.Info("NVHelper.VerifyFacial - Llamando a " + ws.Url + " - Verificando RUT = " + oParamin.CIParam.ValueId);
                //    ret = ws.Process(xmlparamin, out xmlparamout);
                //    if (ret == 0)
                //    {
                //        LOG.Debug("NVHelper.VerifyFacial - Procesando respuesta sin error...");
                //        if (!string.IsNullOrEmpty(xmlparamout))
                //        {
                //            Bio.Digital.Receipt.Api.XmlParamOut oParamout = 
                //                Bio.Digital.Receipt.Api.XmlUtils.DeserializeObject<Bio.Digital.Receipt.Api.XmlParamOut>(xmlparamout);
                //            if (oParamout != null)
                //            {
                //                bReturnMatch = (Convert.ToDouble(oParamout.CIParam.Score) >= Properties.Settings.Default.FacialThreshold);
                //                score = Convert.ToDouble(oParamout.CIParam.Score);
                //                certifypdf = (oParamout.CIParam != null ? oParamout.CIParam.CertificatePDF : null);
                //            } else
                //            {
                //                LOG.Debug("NVHelper.VerifyFacial - Error deserializando respuesta. Sale con error IRET_ERR_DESERIALIZE_RESPONSE");
                //                msgerr = "Error deserializando respuesta. Sale con error";
                //                ret = Errors.IRET_ERR_DESERIALIZE_RESPONSE;
                //            }
                //        }
                //        else
                //        {
                //            LOG.Debug("NVHelper.VerifyFacial - Error respuesta Nula. Sale con error IRET_ERR_DESERIALIZE_RESPONSE");
                //            msgerr = "Error respuesta Nula. Sale con error.";
                //            ret = Errors.IRET_ERR_DESERIALIZE_RESPONSE;
                //        }
                //    }
                //    else
                //    {
                //        LOG.Debug("NVHelper.VerifyFacial - Error retornado por Servicio = " + ret.ToString());
                //        msgerr = "Error retornado por Servicio = " + ret.ToString();
                //    }
                //}
            }
            catch (Exception ex)
            {
                LOG.Debug("NVHelper.VerifyFacial - Exception = " + ex.Message);
                msgerr = "Excepcion desconocida. Sale con error. Sale con error [" + ex.Message + "]";
                ret = Errors.IRET_ERR_DESCONOCIDO;
            }
            return ret;
        }

    }   
}
