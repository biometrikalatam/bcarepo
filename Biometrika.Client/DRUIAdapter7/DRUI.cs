﻿using AForge.Video;
using DRUIAdapter7.Helpers;
using BVIUIAdapter7.Libs;
using Domain;
using Emgu.CV;
using Emgu.CV.Structure;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net.NetworkInformation;
using System.Threading;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Reflection;
using DRUIAdapter7.Config;
using DRUIAdapter7.Utils;

namespace DRUIAdapter7
{
    public partial class DRUI : Form
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(DRUI));

        internal static DRConfig _DRCONFIG = null;

        //Biometrika.BioApi20.BSP.BSPBiometrika BSP;
        //List<Biometrika.BioApi20.Sample> _SamplesCaptured;
        byte[] _Sample_Standard;

        bool _HAY_PDF417 = false;

        EstadoWizard _ESTADO_WIZARD = EstadoWizard.EsperandoCapturaCedula;

        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        public Solicitud _SOLICITUD { get; set; }
        public Persona _PERSONA { get; set; }

        public int _CLIENT_TYPE = 0; //Usado para poder definir desde Solicitud o con seleccion en comenzar

        internal NVHelper _NVHELPER;

        internal string _CLR_DOE;
        internal string _CLR_DOB;
        internal string _ISO_COMPACT;

        //Variables para uso de Reconociento CB con WebCam
        private readonly CameraDevices camDevices;
        private Bitmap currentBitmapForDecoding;
        private readonly Thread decodingThread;
        private readonly Pen resultRectPen;
        private byte[] resultAsBinary { get; set; }
        /// <summary>
        /// Estructura para almacenar información de obtenido del código de barra con WebCam
        /// </summary>
        private struct Device
        {
            public int Index;
            public string Name;
            public override string ToString()
            {
                return Name;
            }
        }
        private IList<Device> devices = new List<Device>();
        //Variable que indica si existe un error en la cámara.
        private bool CameraError = false;
        public BarcodeWebCam Barcodewebcam { get; set; }

        private String BarCodeContent { get; set; }
        //EndVariables para uso de Reconociento CB con WebCam

        public string SerialSensor { get; set; }
        public bool SensorConnected; //=> ReaderFactory.ReaderConnected;
        //private AbstractReader reader;

        //public int Tokencontent { get; set; }
        public bool HaveData { get; set; }
        public static BioPacket Packet { get; set; }
        private BioPacket bioPacketLocal;

        private RUT _RUT;

        private string json;
        private bool isMenuOpen = false;
        private int timeLeft;
        private System.Timers.Timer timer;
        private System.Timers.Timer timerControlGeneral;


        Graphics paperDocument;
        Pen pencil;


        #region Variables de Cam por Emgu

        private string _pathPDF = null;
        private string _pathXML = null;
        private bool _MUSTRUN = false;
        internal string DeviceId { get; set; }
        internal string GeoRef { get; set; }
        public string DocumentId { get; set; }
        public string Mail { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string Company { get; set; }

        public string RDEnvelope { get; set; }
        public string _TRACK_ID_CI { get; set; }

        private Capture _CAPTURECAM;

        #endregion Variables de Cam por Emgu


        #region General

        public DRUI()
        {
            InitializeComponent();

            //Seccion para Recognition CB con WebCam
            camDevices = new CameraDevices();
            
            camDevices.SelectCamera(0);
            
        }

        internal void ReadConfig()
        {
            try
            {
                if (_DRCONFIG == null)
                {
                    _DRCONFIG = SerializeHelper.DeserializeFromFile<DRConfig>("DR7.Config.cfg");
                    if (_DRCONFIG == null)
                    {
                        _DRCONFIG = new DRConfig();
                        SerializeHelper.SerializeToFile(_DRCONFIG, "DR7.Config.cfg");
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.ReadConfig - Error: " + ex.Message);
            }
        }

        private void BVIUI_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            LOG.Debug("DRUI.BVIUI_Load - Iniatilazation...");
            this.TopMost = true;
            this.TopLevel = true;
            this.Size = new Size(1370, 770);
            this.Refresh();

            //labClientType.Text = "";
            //labClientType.Visible = true;
            //labClientType.Refresh();

            txtMessage.Text = "";
            txtMessage.Refresh();
            txtMessage.Visible = true;

            labSubTitle.Text = "";
            labSubTitle.Refresh();
            labSubTitle.Visible = true;

            //Redondea picturebox de la captura de huella
            //System.Drawing.Drawing2D.GraphicsPath objDraw = new System.Drawing.Drawing2D.GraphicsPath();
            //objDraw.AddEllipse(0, 0, this.picSample.Width, this.picSample.Height);
            //this.picSample.Region = new Region(objDraw);

            picGuiaCaptureDocument.Parent = imageCam;
            int x = (imageCam.Width - picGuiaCaptureDocument.Width) / 2;
            picGuiaCaptureDocument.Location = new Point(x, 10);
            //pictureBox3.Size = imageCam.Size;
            picGuiaCaptureDocument.BackColor = Color.Transparent;
            picGuiaCaptureDocument.Refresh();

            picGuiaCaptureSelfie.Parent = imageCamSelfie;
            x = (imageCamSelfie.Width - picGuiaCaptureSelfie.Width) / 2;
            picGuiaCaptureSelfie.Location = new Point(x, 10);
            //pictureBox3.Size = imageCam.Size;
            picGuiaCaptureSelfie.BackColor = Color.Transparent;
            picGuiaCaptureSelfie.Refresh();


            //Guia de captura en videos
            //Graphics paperDocument;
            paperDocument = imageCam.CreateGraphics();
            // Create a new pen.
            pencil = new Pen(Brushes.Green);
            // Set the pen's width.
            pencil.Width = 8.0F;
            // Set the LineJoin property.
            pencil.LineJoin = System.Drawing.Drawing2D.LineJoin.Bevel;
            paperDocument.DrawRectangle(pencil, 10, 10, imageCam.Width - 10, imageCam.Height - 10);

            labCapturaImgDocFront.BackColor = Color.Transparent;
            labCapturaImgDocBack.BackColor = Color.Transparent;


            try
            {
                Version version = Assembly.GetExecutingAssembly().GetName().Version;
                labVersion.Text = "v" + version.ToString();
                //picProgreso.Image = Image.FromFile(@"C:\TFSN\Biometrika Client\V7.5\Auxiliares\images\upload.gif");
                //picProgreso.SizeMode = PictureBoxSizeMode.StretchImage;

                //picGrpVerificandoBackground.Image = Image.FromFile(@"C:\TFSN\Biometrika Client\V7.5\Auxiliares\images\upload.gif");
                //picGrpVerificandoBackground.SizeMode = PictureBoxSizeMode.StretchImage;

                if (_DRCONFIG == null)
                {
                    _DRCONFIG = SerializeHelper.DeserializeFromFile<DRConfig>("DR7.Config.cfg");
                    if (_DRCONFIG == null)
                    {
                        _DRCONFIG = new DRConfig();
                        SerializeHelper.SerializeToFile(_DRCONFIG, "DR7.Config.cfg");
                    }
                }

                if (!string.IsNullOrEmpty(_DRCONFIG.CustomerLogoPath))
                {
                    LOG.Debug("DRUI.BVIUI_Load - Cargando CustomerLogoPath = " + _DRCONFIG.CustomerLogoPath);
                    picLogo.Image = Image.FromFile(_DRCONFIG.CustomerLogoPath);
                    picLogo.Refresh();
                } else
                {
                    LOG.Debug("DRUI.BVIUI_Load - Ignora carga de logo customer porque CustomerLogoPath es null o está erroneo => " +
                        (string.IsNullOrEmpty(_DRCONFIG.CustomerLogoPath)?"NULL": _DRCONFIG.CustomerLogoPath));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.BVIUI_Load - Error cargando logo de cliente: " + ex.Message);
            }

            //Setea NVHelper
            _NVHELPER = new NVHelper(_DRCONFIG.URLServiceBase, _DRCONFIG.ServiceTimeout, _DRCONFIG.ServiceCompany);
            if (_NVHELPER != null)
            {
                LOG.Debug("DRUI.BVIUI_Load - Creado _NVHELPER => API Url=" + _DRCONFIG.URLServiceBase + " - Company=" +
                            _DRCONFIG.ServiceCompany + " - Timeout=" + _DRCONFIG.ServiceTimeout.ToString());
                LOG.Debug("DRUI.BVIUI_Load - Creado _NVHELPER => WS Url=" + _DRCONFIG.URLWebService + " - Company=" +
                            _DRCONFIG.ServiceCompany + " - Timeout=" + _DRCONFIG.ServiceTimeout.ToString());
            }

            LOG.Debug("DRUI.BVIUI_Load OK!");
        }

        internal int Channel(BioPacket bioPacket)
        {
            int ret = 0;
            LOG.Info("DRUI.Channel In...");
            bioPacketLocal = bioPacket;

            ret = CheckParameters(bioPacketLocal);

            if (ret == 0)
            {
                LOG.Info("DRUI.Channel Out - Parámetros configurados correctamente");
                if (bioPacketLocal.PacketParamOut == null)
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            }
            LOG.Info("DRUI.Channel Out [ret=" + ret + "]...");
            return ret;
        }

        /// <summary>
        /// Construimos la salida de la solicitud que será devuelta al usuario final.
        /// </summary>
        /// <returns></returns>
        private Solicitud BuildSolicitud()
        {
            try
            {
                _SOLICITUD = new Solicitud();
                _SOLICITUD.Persona = new Persona();
                LOG.Info("DRUI.BuildSolicitud - Se construye el objeto persona para " + dictParamIn["VALUEID"] + "...");
                _SOLICITUD.Persona.Rut = dictParamIn["VALUEID"];

                _SOLICITUD.Configuracion = new Configuracion();
                _SOLICITUD.Configuracion.clientType = _DRCONFIG.ClientType;
                _SOLICITUD.Configuracion.ignoreStepSelfieInWizard = _DRCONFIG.IgnoreStepSelfieInWizard;
                //_SOLICITUD.Configuracion.extraerFirma = Properties.Settings.Default.ExtractSignature;
                //_SOLICITUD.Configuracion.extraerFoto = Properties.Settings.Default.ExtractPhoto;
                _SOLICITUD.Configuracion.bloquearDocumentoVencido = _DRCONFIG.BlockWizardWithExpiredDocument;
                _SOLICITUD.Configuracion.bloqueoMenoresDeEdad = _DRCONFIG.BlockWizardWithYounger;
                _SOLICITUD.Configuracion.bloqueoCedulaInSCReI = _DRCONFIG.BlockWizardWithBlockedDocumentInSRCeI;

                //_SOLICITUD.Configuracion.returnSampleImages = Properties.Settings.Default.ReturnSamplesImages;
                //_SOLICITUD.Configuracion.returnSampleMinutiae = Properties.Settings.Default.ReturnSamplesMinutiaes;
                _SOLICITUD.Configuracion.userName = _DRCONFIG.UserNameDefault;
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.BuildSolicitud - Error al construir la solicitud:" + ex.Message);
                LOG.Error("DRUI.BuildSolicitud - stacktrace:" + ex.StackTrace);
            }
            LOG.Info("DRUI.BuildSolicitud - Termine de construir la solicitud");
            return _SOLICITUD;

        }     

        public int CheckParameters(BioPacket bioPacketLocal)
        {
            int ret = 0;
            try
            {
                LOG.Info("DRUI.CheckParameters In...");
                if (bioPacketLocal == null)
                {
                    ret = -2;  //Lista de parametros no puede ser nulo
                    LOG.Error("DRUI.CheckParameters - Lista de parametros no puede ser nulo!");
                }

                LOG.Debug("DRUI.CheckParameters filling dictParamin...");
                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    LOG.Debug("DRUI.CheckParameters     Adding => " + k + "=" + (string)bioPacketLocal.PacketParamIn[k] + "...");
                    dictParamIn.Add(k.ToUpper(), bioPacketLocal.PacketParamIn[k]);

                }
                LOG.Debug("BBVIUI.CheckParameters dictParamin filled [length = " + dictParamIn.Count + "]");
                if ((!dictParamIn.ContainsKey("SRC") || String.IsNullOrEmpty(dictParamIn["SRC"])))
                {
                    ret = -3;  //src/TypeId/ValufeId deben ser enviados como parámetros
                    LOG.Error("DRUI.CheckParameters - src y valueid deben ser enviados como parámetros!");
                }
                else
                {
                    if (!dictParamIn.ContainsKey("VALUEID"))
                    {
                        dictParamIn.Add("VALUEID", "NA");
                    }
                    LOG.Info("DRUI.CheckParameters - Seteamos el rut definido => " + dictParamIn["VALUEID"]);
                    _PERSONA = new Persona();

                    LOG.Info("DRUI.CheckParameters - Definimos si viene el parámetro solicitud");
                    
                    try
                    {
                        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

                        if (dictParamIn.ContainsKey("SOLICITUD") && dictParamIn["SOLICITUD"] != null)
                        {
                            _SOLICITUD = jsonSerializer.Deserialize<Solicitud>(dictParamIn["SOLICITUD"]);
                            if (_SOLICITUD != null)
                            {
                                LOG.Info("DRUI.CheckParameters - La solicitud viene de acuerdo a los parámetros solicitados");
                            }
                            else
                            {
                                LOG.Error("DRUI.CheckParameters - se asume una verificación por defecto con los mínimos de valores configurados en parámetros");
                                _SOLICITUD = BuildSolicitud();
                            }
                        }
                        else
                        {
                            LOG.Error("DRUI.CheckParameters - se asume una verificación por defecto con los mínimos de valores configurados en parámetros");
                            _SOLICITUD = BuildSolicitud();
                        }

                    }
                    catch (Exception exe)
                    {
                        LOG.Error("DRUI.CheckParameters - se asume una verificación por defecto con los mínimos de valores configurados en parámetros", exe);
                        LOG.Info("DRUI.CheckParameters - Construye la solicitud");
                        _SOLICITUD = BuildSolicitud();
                    }
                    labTitle.Text = "Validando RUT " + _SOLICITUD.Persona.Rut + "...";
                    ret = 0;
                }
                //bioPacketLocal = bioPacketLocal;
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.CheckParameters - Error Biometrika BviCam.CheckParameters [" + ex.Message + "]");
                ret = -1;
            }
            LOG.Info("DRUI.CheckParameters - CheckParameters Ount [ret=" + ret + "]!");
            return ret;
        }   

        private void CloseControl(bool hasCapture)
        {
            //LOG.Info("CIUIAdapter.CloseControl In...");
            //try
            //{
            //    if (bioPacketLocal == null)
            //    {
            //        LOG.Debug("CIUIAdapter.CloseControl Creando bioPacketLocal...");
            //        bioPacketLocal = new BioPacket();
            //        bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            //    }

            //    if (_CurrentError != 0 || !hasCapture)  //Si hay error o no completo todos los datos
            //    {
            //        bioPacketLocal.PacketParamOut.Add("TrackIdCI", null);
            //        bioPacketLocal.PacketParamOut.Add("CI", null);
            //        bioPacketLocal.PacketParamOut.Add("DeviceId", DeviceId);
            //        bioPacketLocal.PacketParamOut.Add("Error", _CurrentError.ToString() + "|" + _sCurrentError);
            //        LOG.Debug("CIUIAdapter.CloseControl VAlores devueltos => TrackIdCI = null | CI = null | DeviceId = " + DeviceId + " | Error = " + _CurrentError.ToString() + "|" + _sCurrentError);
            //    }
            //    else
            //    {
            //        bioPacketLocal.PacketParamOut.Add("TrackIdCI", TrackIdCI);
            //        bioPacketLocal.PacketParamOut.Add("CI", RDEnvelope);
            //        bioPacketLocal.PacketParamOut.Add("DeviceId", DeviceId);
            //        bioPacketLocal.PacketParamOut.Add("Error", "0|No Error");
            //        LOG.Debug("CIUIAdapter.CloseControl VAlores devueltos => TrackIdCI = " + TrackIdCI + "| CI = " + RDEnvelope + "| DeviceId = " + DeviceId + " |0|No Error");
            //    }
            //    HaveData = true;
            //    this.Close();
            //}
            //catch (Exception ex)
            //{
            //    LOG.Error("CIUIAdapter.CloseControl Error Closing [" + ex.Message + "]");

            //}

            //LOG.Info("CIUIAdapter.CloseControl OUT!");

        }

        private void GetGeoRef()
        {
            try
            {
                //GeoRef = Properties.Settings.Default.CIGeoRef;
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.GetGeoRef Error [" + ex.Message + "]");
            }
        }

        private void GetDeviceId()
        {
            try
            {
                //DeviceId = GetMACAddress();
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.GetDeviceId Error [" + ex.Message + "]");
            }
        }

        public string GetMACAddress()
        {
            string sMacAddress = null;
            try
            {
                LOG.Debug("CIUIAdapter.GetMACAddress IN...");
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                sMacAddress = string.Empty;
                foreach (NetworkInterface adapter in nics)
                {
                    if (sMacAddress == String.Empty)// only return MAC Address from first card
                    {
                        if (adapter.OperationalStatus == OperationalStatus.Up && (!adapter.Description.Contains("Virtual")
                            && !adapter.Description.Contains("Pseudo")))
                        {
                            if (adapter.GetPhysicalAddress().ToString() != "")
                            {
                                IPInterfaceProperties properties = adapter.GetIPProperties();
                                sMacAddress = adapter.GetPhysicalAddress().ToString();
                                LOG.Debug("CIUIAdapter.GetMACAddress MAC Tomada = " + sMacAddress);
                            }
                        }
                    }
                }
                if (sMacAddress == string.Empty)
                {
                    //sMacAddress = Properties.Settings.Default.CIDeviceId;
                    LOG.Debug("CIUIAdapter.GetMACAddress MAC Tomada desde CIDeviceId = " + sMacAddress);
                }
                else
                {
                    string sAux = sMacAddress.Substring(0, 2);
                    int i = 2;
                    while (i < sMacAddress.Length)
                    {
                        sAux = sAux + "-" + sMacAddress.Substring(1, 2);
                        i = i + 2;
                    }
                    sMacAddress = sAux;
                    LOG.Debug("CIUIAdapter.GetMACAddress MAC formateada = " + sAux);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.GetMACAddress Error [" + ex.Message + "]");
            }

            return sMacAddress;
        }

        #endregion General

        #region Operacion

        #region SubRegion Menu Pantalla Principal
        private void labReinit_Click(object sender, EventArgs e)
        {
            ClearComponent();
        }

        private void labClose_Click(object sender, EventArgs e)
        {
            CloseControl();
        }

        private void CloseControl()
        {
            try
            {
                LOG.Info("DRUI.CloseControl In...");
                BVIResponse response = new BVIResponse(_TRACK_ID_CI, _PERSONA);
                string strResponse = JsonConvert.SerializeObject(response);
                if (bioPacketLocal == null)
                {
                    LOG.Debug("CIUIAdapter.CloseControl Creando bioPacketLocal...");
                    bioPacketLocal = new BioPacket();
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
                }

                //bioPacketLocal.PacketParamOut.Add("TrackIdCI", _TRACK_ID_CI);
                bioPacketLocal.PacketParamOut.Add("DRResponse", strResponse);
                HaveData = true;
                this.Close();
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.CloseControl Error: " + ex.Message);
            }
            LOG.Info("DRUI.CloseControl OUT!");
        }

        private void labInicioStart_Click(object sender, EventArgs e)
        {
            SetEstadoWizard(EstadoWizard.EsperandoCapturaCedula);
        }

        private void labInicioSelectClientTypeHuella_Click(object sender, EventArgs e)
        {
            _CLIENT_TYPE = 2; //_SOLICITUD.Configuracion.clientType; // Properties.Settings.Default.ClientType;
           
            labInicioStart_Click(null, null);
        }

        private void labInicioSelectClientTypeFacial_Click(object sender, EventArgs e)
        {
            _CLIENT_TYPE = 2; //Properties.Settings.Default.ClientType;
           
            labInicioStart_Click(null, null);
        }

        #endregion SubRegion Menu Pantalla Principal

        private void labSupport_Click(object sender, EventArgs e)
        {
            frmHelp frm = new frmHelp();
            frm.Show(this);
        }

        private void labAcercaDe_Click(object sender, EventArgs e)
        {
            frmHelp frm = new frmHelp();
            frm.Show(this);
        }

        private void btnSigGrpLCB_Click(object sender, EventArgs e)
        {
            if (_PERSONA.TipoIdentificacion == TipoIdentificacion.CedulaChilenaAntigua)
            {
                SetEstadoWizard(EstadoWizard.EsperandoHuellaEscaneadaCedulaVieja);
            } else
            {
                SetEstadoWizard(EstadoWizard.EsperandoHuellaEscaneadaCedulaNueva);
            }
            
        }

        /// <summary>
        /// Toma las imagenes capturadas y las envia al server para inicar el proceso de verificacion. 
        /// Retorna la info reconocida, para manejo interno y si hay errores, pide reproceso.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSigCaptureDocument_Click(object sender, EventArgs e)
        {
            Persona oPersona;
            string msgerror;
            string trackidciout;
            try
            {
                StopCamStream();
                picProcess1.Visible = true;
                picProcess1.Refresh();
                labMsgProcess1.Visible = true;
                labMsgProcess1.Refresh();
                int ret = _NVHELPER.ParseOCRDocument(_CLIENT_TYPE, _PERSONA, _SOLICITUD.Configuracion.userName,
                                                     out oPersona, out msgerror, out trackidciout);
                picProcess1.Visible = false;
                picProcess1.Refresh();
                labMsgProcess1.Visible = false;
                labMsgProcess1.Refresh();

                if (ret < 0)  //Hubo error => Informo y pido intente nuevamente
                {
                    MostrarMensaje(1, Color.Red, msgerror + " Reintente...");
                } else
                {
                    _PERSONA = oPersona;
                    _TRACK_ID_CI = trackidciout;
                    labTitle.Text = "Reconociendo RUT " + _PERSONA.Rut + "...";
                    labTitle.Visible = true;
                    labTitle.Refresh();
                    labTrackId.Text = "Track Id: " + _TRACK_ID_CI;
                    labTrackId.Visible = true;
                    labTrackId.Refresh();

                    //Lleno labs de otros groups
                    //labVerifyNombre.Text = _PERSONA.Nombre + " " + _PERSONA.Apellido;
                    //labVerifyFNac.Text = _PERSONA.FechaNacimiento.HasValue ? _PERSONA.FechaNacimiento.Value.ToString("dd/MM/yyyy"):"";
                    //labVerifyFVenc.Text = _PERSONA.FechaExpiracion.HasValue ? _PERSONA.FechaExpiracion.Value.ToString("dd/MM/yyyy") : "";
                    //labVerifySerial.Text = _PERSONA.Serie;
                    //labVerifySex.Text = _PERSONA.Sexo;
                    //labVerifyNacionality.Text = _PERSONA.Nacionalidad;


                    //StopCamStream();
                    SetEstadoWizard(EstadoWizard.EsperandoExportar);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.btnSigCaptureDocument_Click - Error: " + ex.Message);
            }
        }

        private void labSigCaptureDocument_Click(object sender, EventArgs e)
        {
            Persona oPersona;
            string msgerror;
            string trackidciout;
            try
            {
                //StopCamStream();
                picDocStop_Click(null, null);

                SetProgress(true);

                picProcess1.Visible = true;
                picProcess1.Refresh();
                labMsgProcess1.Visible = true;
                labMsgProcess1.Refresh();
                MostrarMensaje(1, Color.DarkGray, "Procesando imágenes. Por favor espere...");
                int ret = _NVHELPER.ParseOCRDocument(_CLIENT_TYPE, _PERSONA, _SOLICITUD.Configuracion.userName, 
                                                     out oPersona, out msgerror, out trackidciout);
                picProcess1.Visible = false;
                picProcess1.Refresh();
                labMsgProcess1.Visible = false;
                labMsgProcess1.Refresh();

                if (ret < 0)  //Hubo error => Informo y pido intente nuevamente
                {
                    MostrarMensaje(1, Color.Red, msgerror + "Capture nuevas imágenes. Verifique la luz. Reintente..."); 
                }
                else
                {
                    _PERSONA = oPersona;
                    _TRACK_ID_CI = trackidciout;
                    //bool isVigente = _PERSONA.FechaExpiracion.HasValue ? Utils.IsCedulaVigente(_PERSONA.FechaExpiracion.Value) : false;
                    //bool isMenor = _PERSONA.FechaNacimiento.HasValue ? Utils.IsMenorDe18(_PERSONA.FechaNacimiento.Value) : false;
                    if (_SOLICITUD.Configuracion.bloquearDocumentoVencido && !_PERSONA.IsCedulaVigente)
                    {
                        picCapDocStatusBlock.Image = Properties.Resources.ObsCedVencida;
                        labCapDocStatusBlock.Text = "Cédula Vencida";
                        grpCapDocStatusBlock.Visible = true;
                        grpCapDocStatusBlock.Refresh();
                    }
                    else if (_SOLICITUD.Configuracion.bloquearDocumentoVencido && _PERSONA.IsMenorDeEdad)
                    {
                        picCapDocStatusBlock.Image = Properties.Resources.IconoMenorDeEdad;
                        labCapDocStatusBlock.Text = "Menor de Edad";
                        grpCapDocStatusBlock.Visible = true;
                        grpCapDocStatusBlock.Refresh();
                    }
                    else
                    {
                        labTitle.Text = "Reconociendo RUT " + _PERSONA.Rut + "...";
                        labTitle.Visible = true;
                        labTitle.Refresh();
                        labTrackId.Text = "Track Id: " + _TRACK_ID_CI;
                        labTrackId.Visible = true;
                        labTrackId.Refresh();

                        //Lleno Subtitle
                        labSubTitle.Text = _PERSONA.Nombre + " " + _PERSONA.Apellido;
                        labSubTitle.Refresh();

                        ////Lleno labs de otros groups
                        //labVerifyNombre.Text = _PERSONA.Nombre + " " + _PERSONA.Apellido;
                        //labVerifyFNac.Text = _PERSONA.FechaNacimiento.HasValue ? _PERSONA.FechaNacimiento.Value.ToString("dd/MM/yyyy") : "";
                        //if (_PERSONA.IsMenorDeEdad)
                        //    labVerifyFNac.ForeColor = Color.Red;
                        //labVerifyFVenc.Text = _PERSONA.FechaExpiracion.HasValue ? _PERSONA.FechaExpiracion.Value.ToString("dd/MM/yyyy") : "";
                        //if (!_PERSONA.IsCedulaVigente)
                        //    labVerifyFVenc.ForeColor = Color.Red;
                        //labVerifySerial.Text = _PERSONA.Serie;
                        //labVerifySex.Text = _PERSONA.Sexo;
                        //labVerifyNacionality.Text = _PERSONA.Nacionalidad;

                        //Lleno labs de otros groups
                        //labLCBNombre.Text = _PERSONA.Nombre + " " + _PERSONA.Apellido;
                        //labLCBFNac.Text = _PERSONA.FechaNacimiento.HasValue ? _PERSONA.FechaNacimiento.Value.ToString("dd/MM/yyyy") : "";
                        //if (_PERSONA.IsMenorDeEdad)
                        //    labLCBFNac.ForeColor = Color.Red;
                        //labLCBFVen.Text = _PERSONA.FechaExpiracion.HasValue ? _PERSONA.FechaExpiracion.Value.ToString("dd/MM/yyyy") : "";
                        //if (!_PERSONA.IsCedulaVigente)
                        //    labLCBFVen.ForeColor = Color.Red;
                        //labLCBSerial.Text = _PERSONA.Serie;
                        //labLCBSex.Text = _PERSONA.Sexo;
                        //labLCBNacionality.Text = _PERSONA.Nacionalidad;
                        SetEstadoWizard(EstadoWizard.EsperandoExportar);
                        FillResumenGroup();
                    }
                    //StopCamStream();
                }
                SetProgress(false);
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.btnSigCaptureDocument_Click - Error: " + ex.Message);
            }
        }

        private void SetProgress(bool enable)
        {
            try
            {
                //grpProgress.Location = new Point(650, 300);
                //grpProgress.BringToFront();
                //grpProgress.Visible = enable;
                //grpProgress.Refresh();
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.SetProgress Error: " + ex.Message);
            }
        }

        private void labShowResumen_Click(object sender, EventArgs e)
        {
            grpResumen.Text = "";
            grpResumen.Location = new Point(379, 130);
            grpResumen.BringToFront();
            grpResumen.Visible = true;
            grpResumen.Refresh();
        }

        private void FillResumenGroup()
        {
            byte[] byImage;
            System.IO.MemoryStream ms;
            try
            {
                if (_PERSONA != null)
                {
                    //Imagenes documentos
                    if (!string.IsNullOrEmpty(_PERSONA.FotoFrontCedula))
                    {
                        byImage = Convert.FromBase64String(_PERSONA.FotoFrontCedula);
                        ms = new MemoryStream(byImage);
                        picResumenDocFront.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumenDocFront.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(_PERSONA.FotoBackCedula))
                    {
                        byImage = Convert.FromBase64String(_PERSONA.FotoBackCedula);
                        ms = new MemoryStream(byImage);
                        picResumenDocBack.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumenDocBack.Visible = true;
                    }

                    if (!string.IsNullOrEmpty(_PERSONA.FotoFirma))
                    {
                        byImage = Convert.FromBase64String(_PERSONA.FotoFirma);
                        ms = new MemoryStream(byImage);
                        picResumenFirma.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumenFirma.Visible = true;
                    }

                    if (!string.IsNullOrEmpty(_PERSONA.Selfie) || !string.IsNullOrEmpty(_PERSONA.Foto))
                    {
                        if (!string.IsNullOrEmpty(_PERSONA.Selfie))
                        {
                            byImage = Convert.FromBase64String(_PERSONA.Selfie);
                        } else
                        {
                            byImage = Convert.FromBase64String(_PERSONA.Foto);
                        }
                        ms = new MemoryStream(byImage);
                        picResumenSelfie.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumenSelfie.Visible = true;
                    }

                    //Data 
                    labResumenName.Text = _PERSONA.Nombre + " " + _PERSONA.Apellido;
                    labResumenFNac.Text = _PERSONA.FechaNacimiento.HasValue ? _PERSONA.FechaNacimiento.Value.ToString("dd/MM/yyyy") : "";
                    if (_PERSONA.IsMenorDeEdad)
                    {
                        labResumenFNac.ForeColor = Color.Red;
                        picResumenIconoMenorDeEdad.Visible = true;
                    }
                    labResumenFV.Text = _PERSONA.FechaExpiracion.HasValue ? _PERSONA.FechaExpiracion.Value.ToString("dd/MM/yyyy") : "";
                    if (!_PERSONA.IsCedulaVigente)
                    {
                        labResumenFV.ForeColor = Color.Red;
                        picResumenIconoCedVencida.Visible = true;
                    }
                    labResumenSerial.Text = _PERSONA.Serie;
                    labResumenSex.Text = _PERSONA.Sexo;
                    labResumenNacionality.Text = _PERSONA.Nacionalidad;

                }
                grpResumen.Refresh();
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.FillResumenGroup - Error: " + ex.Message);
            }
        }

        private string GetScorePercent(double score)
        {
            string sRet = "0%";
            try
            {
                double dScore = 0;
                //if (_CLIENT_TYPE == 1 && _PERSONA.TipoIdentificacion == TipoIdentificacion.CedulaChilenaAntigua)
                //{
                //    dScore = GetPorcentajeNEC(score, Properties.Settings.Default.NECThreshold);
                //} else
                //{
                //    dScore = score;
                //}
                sRet = dScore.ToString("##.##") + "%";
            }
            catch (Exception ex)
            {
                sRet = "0%";
                LOG.Error("DRUI.GetScorePercent Error: " + ex.Message);
            }
            return sRet;
        }

        private void MarkRangoResumenScore(bool visible, double score)
        {
            try
            {
                //if (!visible)
                //{
                //    labResumenRango1.Visible = false;
                //    labResumenRango2.Visible = false;
                //    labResumenRango3.Visible = false;
                //    labResumenRango4.Visible = false;
                //    labResumenRango5.Visible = false;
                //} else
                //{
                //    if (score < 20)
                //        labResumenRango1.Visible = true;
                //    else if (score < 40)
                //        labResumenRango2.Visible = true;
                //    else if (score < 60)
                //        labResumenRango3.Visible = true;
                //    else if (score < 80)
                //        labResumenRango4.Visible = true;
                //    else
                //        labResumenRango5.Visible = true;

                //}
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.MarkRangoResumenScore - Error: " + ex.Message);
            }
        }

        private void btnSigTabHuella_Click(object sender, EventArgs e)
        {
            try
            {
                //if (_PERSONA.TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
                //{
                    SetEstadoWizard(EstadoWizard.EsperandoLecturaContactless);
                //}
                //else
                //{
                //    SetEstadoWizard(EstadoWizard.Verificando);
                //}
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.btnSigTabHuella_Click - Error: " + ex.Message);
            }
        }

              

        private void labFinalSalir_Click(object sender, EventArgs e)
        {
            CloseControl();
        }

        private void labSigExport_Click(object sender, EventArgs e)
        {
            try
            {
                SetEstadoWizard(EstadoWizard.Final);
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.labSigExport_Click - Error: " + ex.Message);
            }
        }

                
        private void labVerificandoSalir_Click(object sender, EventArgs e)
        {
            CloseControl();
        }

        
        private void txtMailToSendPDF_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtMailToSendPDF.Text) && Utils.Utils.isFormatMailCorrect(txtMailToSendPDF.Text))
            {
                labExportarSend.Enabled = true;
            }
            else
            {
                labExportarSend.Enabled = false;
            }
        }

        private void txtMailToSendPDF_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtMailToSendPDF.Text) && Utils.Utils.isFormatMailCorrect(txtMailToSendPDF.Text))
            {
                labExportarSend.Enabled = true;
            }
            else
            {
                labExportarSend.Enabled = false;
            }
        }

        private void labExportarSend_Click(object sender, EventArgs e)
        {
            try
            {
                labResultSendMailPDF.Text = "Enviando mail...";
                labResultSendMailPDF.Visible = true;
                labResultSendMailPDF.Refresh();
                string msgerr;
                int ret = NVHelper.ExportPDFbyMail(_TRACK_ID_CI, txtMailToSendPDF.Text, out msgerr);

                if (ret == 0)
                {
                    labResultSendMailPDF.Text = "PDF enviado con éxito a " + txtMailToSendPDF.Text + "!";
                } else
                {
                    labResultSendMailPDF.Text = "Error enviando PDF a " + txtMailToSendPDF.Text + " [" + msgerr + "]. Reintente...";
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.labExportarSend_Click Error: " + ex.Message);
            }
        }

        private void labExportarDownloadPDF_Click(object sender, EventArgs e)
        {
            string msgerr = "";
            byte[] byPDF;
            string path = @"c:\";
            int ret = -1;
            try
            {
                if (!string.IsNullOrEmpty(_PERSONA.CertifyPdf))
                {
                    byPDF = Convert.FromBase64String(_PERSONA.CertifyPdf);
                }
                else
                {
                    ret = NVHelper.DownloadCIPDF(_TRACK_ID_CI, out byPDF, out msgerr);
                }

                if (!string.IsNullOrEmpty(_DRCONFIG.DownloadPath))
                {
                    path = _DRCONFIG.DownloadPath;
                    if (!path.EndsWith("\\"))
                    {
                        path = path + "\\";
                    }
                }

                if (ret == 0 && byPDF!=null)
                {
                    LOG.Error("DRUI.labExportarDownloadPDF_Click - PDF salvado en " + path + _TRACK_ID_CI + ".pdf...");
                    System.IO.File.WriteAllBytes(path + _TRACK_ID_CI + ".pdf", byPDF);
                    labResultSendMailPDF.Text = "PDF salvado en " + path + _TRACK_ID_CI + ".pdf";
                    //TODO => Descargar
                }
                else
                {
                    labResultSendMailPDF.Text = "Error Descargando PDF [" + msgerr + "]. Reintente...";
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.labExportarDownloadPDF_Click Error: " + ex.Message);
            }
        }

        private void ClearComponent()
        {
            _HAY_PDF417 = false;
            //timer_SetGetHuellaCedulaVieja.Enabled = true;

            //Limpia Resumen
            picResumenDocFront.Image = Properties.Resources.NoImage_DocFront;
            //picResumenDocFront.Visible = false;
            picResumenDocBack.Image = Properties.Resources.NoImage_DocBack;
            //picResumenDocBack.Visible = false;
            //picResumenFingerSample.Image = Properties.Resources.NoImage_Finger;
            //picResumenFingerSample.Visible = false;
            picResumenFirma.Image = Properties.Resources.NoImage_Firma;
            //picResumenFirma.Visible = false;
            picResumenSelfie.Image = Properties.Resources.NoImage_Selfie;
            //picResumenSelfie.Visible = false;
            labResumenName.Text = "";
            labResumenFNac.Text = "";
            labResumenFNac.ForeColor = Color.DimGray;
            picResumenIconoMenorDeEdad.Visible = false;
            labResumenFV.Text = "";
            labResumenFV.ForeColor = Color.DimGray;
            picResumenIconoCedVencida.Visible = false;
            labResumenSerial.Text = "";
            labResumenSex.Text = "";
            labResumenNacionality.Text = "";
            //picResumenResultVerify.Visible = false;
            //picResumenResultVerify.Image = Properties.Resources.VerifyPending;
            //labResumenRango1.Visible = false;
            //labResumenRango2.Visible = false;
            //labResumenRango3.Visible = false;
            //labResumenRango4.Visible = false;
            //labResumenRango5.Visible = false;
            //labResumenScore.Text = "0%";
            grpResumen.Refresh();

            //Stop Camera
            StopCamStream();

            //SubTitle
            labSubTitle.Text = "";
            labSubTitle.Refresh();

            //Oculto groups
            grpCapturaImagenDocumento.Visible = false;
            grpCapturaSelfie.Visible = false;

            //Clear componentes internos de Groups
            imageCam.Image = null;
            picDocumentFront.Image = null;
            picDocumentBack.Image = null;

            imageCamSelfie.Image = null;
            picSelfie.Image = null;
            labSigCaptureSelfie.Visible = false;
            picSelfieStatus.Image = Properties.Resources.SelfieStatus;

            picProcess1.Visible = false;
            picProcess2.Visible = false;

            picDocFrontStatus.Image = Properties.Resources.CedulaFrente;
            picDocBackStatus.Image = Properties.Resources.CedulaDorso;

            grpCapDocStatusBlock.Visible = false;

            labResultSendMailPDF.Visible = false;

            //Oculto guia de puntos
            picPaso1.Visible = false;
            picPaso2.Visible = false;
            //picPaso3.Visible = false;
            //picPaso4.Visible = false;
            //picPaso5.Visible = false;
            //picPaso6.Visible = false;
            //picPaso7.Visible = false;

            //Free variables de operacion
            _PERSONA = new Persona();

            //Clear msgs
            txtMessage.ForeColor = Color.Gray;
            txtMessage.Text = "Inicie operación y siga las instrucciones paso a paso...";

            //_CLIENT_TYPE = _SOLICITUD.Configuracion.clientType;
            //if (_CLIENT_TYPE != 0) // || !Properties.Settings.Default.ClientTypeSelectionEnabled)
            //{
            //    /*_CLIENT_TYPE = _SOLICITUD.Configuracion.clientType;*/ // Properties.Settings.Default.ClientType;
            labInicioStart.Visible = true;
            labInicioStart.Refresh();
            //    labClientType.Text = (_CLIENT_TYPE==1 ? "Tecnología: Huella" : "Tecnología: Facial");
            //    labClientType.Visible = true;
            //    labClientType.Refresh();
            //}
            //else //Sino se muestra para seleccionar en pantalla inicial
            //{
            //    labInicioStart.Visible = false;
            //    labInicioStart.Refresh();
            //    grpInicioSelectClientType.Location = new Point(395, 143);
            //    grpInicioSelectClientType.Text = "";
            //    grpInicioSelectClientType.Visible = true;
            //    grpInicioSelectClientType.Refresh();
            //}

            //picLCBObservaciones.Visible = false;
        }

        private void picResumenSalir_Click(object sender, EventArgs e)
        {
            grpResumen.Visible = false;
            grpResumen.Refresh();
        }

        /// <summary>
        /// Setea lo inicial de acuerdo a parametros y configuraciones
        /// </summary>
        /// <param name="obj"></param>
        public void InitAdapter(object obj)
        {
            ClearComponent();
            //Me fijo si es con webcam o con LCB para setear la captura, y tipo de verificacion (Huella / Facial)
            // ClientType = 0 - Huella con Lector CB | 1 - Huella con Webcam | 2 - Facial con 2D
            switch (_DRCONFIG.ClientType)
            {
                case 1: //Huella con Webcam

                    //grpReadCBWithWebCam.Visible = true;
                    break;
                case 2: //Facial con 2D

                    //grpReadCBWithWebCam.Visible = true;
                    break;
                default: //Asumo default 0-Huella con Lector CB
                    //grpReadCBWithWebCam.Visible = false;

                    //Si no se puede seleccionar el tipo de verificacion, se coloca el seteado en el config
                    //if (!Properties.Settings.Default.ClientTypeSelectionEnabled)
                    //{
                    //    _CLIENT_TYPE = Properties.Settings.Default.ClientType;
                    //    if (Properties.Settings.Default.WizardAutomatic)
                    //    {
                    //        //btnStart_Click(null, null);
                    //        labInicioStart_Click(null, null);
                    //    }
                    //}
                    //else //Sino se muestra para seleccionar en pantalla inicial
                    //{
                        labInicioStart.Visible = true;
                        labInicioStart.Refresh();
                        //grpInicioSelectClientType.Location = new Point(395, 143);
                        //grpInicioSelectClientType.Text = "";
                        //grpInicioSelectClientType.Visible = true;
                        //grpInicioSelectClientType.Refresh();
                    //}


                    LOG.Info("DRUI.InitAdapter - El componente se inicializa usando COM para lectura de codigo de barras con lector de CB");
                    //InitBCR();
                    break;
            }
        }

        
        /// <summary>
        /// Setea paso a paso flujo de operacion
        /// </summary>
        /// <param name="estadoCurrent"></param>
        internal void SetEstadoWizard(EstadoWizard estadoCurrent)
        {
            MostrarMensaje(1, Color.DarkGray, "");
            _ESTADO_WIZARD = estadoCurrent;
            /*
                public enum EstadoWizard
                {
                    Inicio,
                    EsperandoCapturaCedula,
                    EsperandoSelfie,
                    EsperandoLecturaCodigoBarras,
                    EsperandoHuellaEscaneadaCedulaVieja,
                    EsperandoHuellaEscaneadaCedulaNueva,
                    EsperandoLecturaContactless,
                    VerificacionPositiva,
                    VerificacionNegativa,
                    EsperandoExportar,
                    Final
                }
            */
            if (estadoCurrent == EstadoWizard.Inicio)
            {
                ClearComponent();
            }

            if (estadoCurrent == EstadoWizard.EsperandoCapturaCedula)
            {
                //grpInicioSelectClientType.Visible = false;
                //grpInicioSelectClientType.Refresh();
                labInicioStart.Visible = false;
                labInicioStart.Refresh();

                picPaso1.Image = Properties.Resources.CirculoAzul_small;
                picPaso1.Visible = true;
                picPaso1.Refresh();
                grpCapturaImagenDocumento.Location = new Point(395, 143);
                grpCapturaImagenDocumento.Text = "";
                grpCapturaImagenDocumento.Visible = true;
                grpCapturaImagenDocumento.Refresh();

                if (_DRCONFIG.AutomaticStartInCaptureDocument)
                {
                    picDocStart_Click(null, null);
                }
            }

            if (estadoCurrent == EstadoWizard.EsperandoExportar)
            {
                picPaso1.Image = Properties.Resources.CirculoVerde_small;
                picPaso1.Visible = true;
                picPaso1.Refresh();

                picPaso2.Image = Properties.Resources.CirculoAzul_small;
                picPaso2.Visible = true;
                picPaso2.Refresh();

                grpCapturaImagenDocumento.Visible = false;
                grpCapturaImagenDocumento.Refresh();

                if (_PERSONA.verifyResult == 1)
                {
                    grpExportar.Location = new Point(395, 143);
                    grpExportar.Text = "";
                    grpExportar.Visible = true;
                    grpExportar.Refresh();
                } else
                {
                    SetEstadoWizard(EstadoWizard.Final);
                }
            }

            if (estadoCurrent == EstadoWizard.Final)
            {
                picPaso2.Image = Properties.Resources.CirculoVerde_small;
                picPaso2.Visible = true;
                picPaso2.Refresh();

                picFinal.Image = Properties.Resources.CirculoVerde_big;
                picFinal.Visible = true;
                picFinal.Refresh();

                grpExportar.Visible = false;
                grpExportar.Refresh();

                grpFinal.Location = new Point(395, 143);
                grpFinal.Text = "";
                grpFinal.Visible = true;
                grpFinal.Refresh();
            }

        }


        /// <summary>
        /// Muestra mensajes en diferentes partes de la pantalla con el color adecuado
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="red"></param>
        /// <param name="v2"></param>
        private void MostrarMensaje(int posicion, Color colormsg, string message)
        {
            try
            {
                switch (posicion)
                {
                    default:
                        txtMessage.ForeColor = colormsg;
                        txtMessage.Text = message;
                        txtMessage.Refresh();
                        break;
                }
            }
            catch (Exception ex)
            {

                LOG.Error("DRUI.MostrarMensaje - Error: " + ex.Message);
            }
        }


        #endregion Operacion


        #region WebCam para captura de Doc Front/Back 

        private void picDocStart_Click(object sender, EventArgs e)
        {
            try
            {
                MostrarMensaje(0, Color.Gray, "");

                _CAPTURECAM = new Capture(HelperCamera.SelectCamera(_DRCONFIG.CAMCameraDocSelected));
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 1280);
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 720);

                //Set Start / Stop en botones
                picDocStart.Image = Properties.Resources.btn_Start_Disabled;
                picDocStart.Enabled = false;
                picDocStart.Refresh();
                picDocStop.Image = Properties.Resources.btn_Stop_Enabled;
                picDocStop.Enabled = true;
                picDocStop.Refresh();

                if (_ESTADO_WIZARD == EstadoWizard.EsperandoCapturaCedula)
                {
                    picGuiaCaptureDocument.Visible = true;
                    picGuiaCaptureDocument.Refresh();
                }
                else
                {

                }
                Application.Idle += ShowCamStream;
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.picDocStart_Click Error: " + ex.Message);
            }
        }

        private void picDocStop_Click(object sender, EventArgs e)
        {
            try
            {
                //Set Start / Stop en botones
                picDocStart.Image = Properties.Resources.btn_Start_Enabled;
                picDocStart.Enabled = true;
                picDocStart.Refresh();
                picDocStop.Image = Properties.Resources.btn_Stop_Disabled;
                picDocStop.Enabled = false;
                picDocStop.Refresh();
                StopCamStream();
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.picDocStop_Click Error: " + ex.Message);
            }
        }

        private void labCamStart1_Click(object sender, EventArgs e)
        {
            try
            {
                
                _CAPTURECAM = new Capture(HelperCamera.SelectCamera(_DRCONFIG.CAMCameraDocSelected));
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 1280);
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 720);
                if (_ESTADO_WIZARD == EstadoWizard.EsperandoCapturaCedula)
                {
                    picGuiaCaptureDocument.Visible = true;
                    picGuiaCaptureDocument.Refresh();
                } else
                {

                }
                Application.Idle += ShowCamStream;
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.labCamStart1_Click Error: " + ex.Message);
            }
        }

        private void labCamStop_Click(object sender, EventArgs e)
        {
            StopCamStream();
        }

        private void ShowCamStream(object sender, EventArgs e)
        {
            try
            {
                //paperDocument.DrawRectangle(pencil, 10, 10, imageCam.Im.Width - 10, imageCam.Height - 10);
                //paperDocument.DrawRectangle(pencil, 10, 10, 430, 250);
                Mat mat = new Mat();
                _CAPTURECAM.Retrieve(mat);
                Image<Bgr, Byte> img = mat.ToImage<Bgr, Byte>();
                //Image<Bgr, Byte> frame = img.Resize(1024, 768, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR).Copy();
                if (_ESTADO_WIZARD == EstadoWizard.EsperandoCapturaCedula)
                {
                    //Image<Gray, Byte> imgOut = img.Convert<Gray, byte>().ThresholdBinary(new Gray(100), new Gray(255));
                    //Emgu.CV.Util.VectorOfColorPoint vectorOfColorPoint = new Emgu.CV.Util.VectorOfColorPoint(); 
                    //Mat hier = new Mat();
                    //CvInvoke.FindContours(imgOut, vectorOfColorPoint, hier, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
                    //CvInvoke.DrawContours(img, vectorOfColorPoint, -1, new MCvScalar(255, 0, 0));


                    imageCam.Image = img.ToBitmap();
                } else if (_ESTADO_WIZARD == EstadoWizard.EsperandoSelfie)
                {
                    imageCamSelfie.Image = img.ToBitmap();
                }

                //Image<Gray, Byte> grayImage = img.Convert<Gray, Byte>();
                //var faces = grayImage.DetectHaarCascade(
                //                                   haar, 1.4, 4,
                //                                   HAAR_DETECTION_TYPE.DO_CANNY_PRUNING,
                //                                   new Size(nextFrame.Width / 8, nextFrame.Height / 8)
                //                                   )[0];
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.ShowCamStream Error = " + ex.Message, ex);
            }
        }

        private void StopCamStream()
        {
            LOG.Info("DRUI.StopCamStream IN...");
            try
            {
                Application.Idle -= ShowCamStream;
                _CAPTURECAM.Stop();
                _CAPTURECAM.Dispose();
                picGuiaCaptureDocument.Visible = false;
                picGuiaCaptureDocument.Refresh();
                picGuiaCaptureSelfie.Visible = true;
                picGuiaCaptureSelfie.Refresh();
                //if (_ESTADO_WIZARD == EstadoWizard.EsperandoCapturaCedula)
                //{
                //    picGuiaCaptureDocument.Visible = false;
                //    picGuiaCaptureDocument.Refresh();
                //}
                //else
                //{

                //}

            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.StopCamStream Error = " + ex.Message, ex);
            }
            LOG.Info("DRUI.StopCamStream OUT!");
        }

        private void labCapturaImgDocFront_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("DRUI.labCapturaImgDocFront_Click IN...");
                picDocumentFront.Image = imageCam.Image;
                picDocumentFront.Visible = true;
                _PERSONA.FotoFrontCedula = GetB64FromPictureBox(picDocumentFront);

                if (!string.IsNullOrEmpty(_PERSONA.FotoFrontCedula))
                {
                    picDocFrontStatus.Image = Properties.Resources.CedulaFrenteOK;
                } else
                {
                    picDocFrontStatus.Image = Properties.Resources.CedulaFrenteNOOK;
                }
                picDocFrontStatus.Refresh();
                LOG.Debug("DRUI.labCapturaImgDocFront_Click _ImageCedulaB64 = " + _PERSONA.FotoFrontCedula.Substring(0, 15) + "...");
                CheckEnableSiguiente();
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.labCapturaImgDocFront_Click Error = " + ex.Message, ex);
            }
            LOG.Info("DRUI.labCapturaImgDocFront_Click OUT!");
        }

        private void labCapturaImgDocBack_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("DRUI.labCapturaImgDocBack_Click IN...");
                picDocumentBack.Image = imageCam.Image;
                picDocumentBack.Visible = true;
                _PERSONA.FotoBackCedula = GetB64FromPictureBox(picDocumentBack);
                if (!string.IsNullOrEmpty(_PERSONA.FotoBackCedula))
                {
                    picDocBackStatus.Image = Properties.Resources.CedulaDorsoOK;
                }
                else
                {
                    picDocBackStatus.Image = Properties.Resources.CedulaFrenteNOOK;
                }
                picDocBackStatus.Refresh();
                LOG.Debug("DRUI.labCapturaImgDocBack_Click _ImageCedulaB64 = " + _PERSONA.FotoBackCedula.Substring(0, 15) + "...");
                CheckEnableSiguiente();
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.labCapturaImgDocBack_Click Error = " + ex.Message, ex);
            }
            LOG.Info("DRUI.labCapturaImgDocBack_Click OUT!");
        }

        private void labCapturaImgDocFront1_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("DRUI.labCapturaImgDocFront_Click IN...");
                picDocumentFront.Image = imageCam.Image;
                picDocumentFront.Visible = true;
                _PERSONA.FotoFrontCedula = GetB64FromPictureBox(picDocumentFront);
                if (!string.IsNullOrEmpty(_PERSONA.FotoFrontCedula))
                {
                    picDocFrontStatus.Image = Properties.Resources.CedulaFrenteOK;
                }
                else
                {
                    picDocFrontStatus.Image = Properties.Resources.CedulaFrenteNOOK;
                }
                picDocFrontStatus.Refresh();
                LOG.Debug("DRUI.labCapturaImgDocFront_Click _ImageCedulaB64 = " + _PERSONA.FotoFrontCedula.Substring(0, 15) + "...");
                CheckEnableSiguiente();
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.labCapturaImgDocFront_Click Error = " + ex.Message, ex);
            }
            LOG.Info("DRUI.labCapturaImgDocFront_Click OUT!");
        }

        private void labCapturaImgDocBack1_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("DRUI.labCapturaImgDocBack_Click IN...");
                picDocumentBack.Image = imageCam.Image;
                picDocumentBack.Visible = true;
                _PERSONA.FotoBackCedula = GetB64FromPictureBox(picDocumentBack);
                if (!string.IsNullOrEmpty(_PERSONA.FotoBackCedula))
                {
                    picDocBackStatus.Image = Properties.Resources.CedulaDorsoOK;
                }
                else
                {
                    picDocBackStatus.Image = Properties.Resources.CedulaFrenteNOOK;
                }
                picDocBackStatus.Refresh();
                LOG.Debug("DRUI.labCapturaImgDocBack_Click _ImageCedulaB64 = " + _PERSONA.FotoBackCedula.Substring(0, 15) + "...");
                CheckEnableSiguiente();
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.labCapturaImgDocBack_Click Error = " + ex.Message, ex);
            }
            LOG.Info("DRUI.labCapturaImgDocBack_Click OUT!");
        }

        private void CheckEnableSiguiente()
        {
            try
            {
                if (_ESTADO_WIZARD == EstadoWizard.EsperandoCapturaCedula)
                {
                    if (!string.IsNullOrEmpty(_PERSONA.FotoFrontCedula) && !string.IsNullOrEmpty(_PERSONA.FotoBackCedula))
                    {
                        labSigCaptureDocument.Visible = true;
                    }
                    if (_DRCONFIG.WizardAutomatic)
                    {
                        //btnSigCaptureDocument_Click(null, null);
                        labSigCaptureDocument_Click(null, null);
                    }
                } else if (_ESTADO_WIZARD == EstadoWizard.EsperandoSelfie)
                {
                    if (!string.IsNullOrEmpty(_PERSONA.Selfie))
                    {
                        //btnSigCapturaSelfie.Enabled = true;
                        labSigCaptureSelfie.Visible = true;
                    }
                    if (_DRCONFIG.WizardAutomatic)
                    {
                        //btnSigCapturaSelfie_Click(null, null);
                        labSigCaptureSelfie_Click(null, null);
                    }
                }

            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.CheckEnableSiguiente Error = " + ex.Message, ex);
            }
        }

        private void picCapturaSelfieStart_Click(object sender, EventArgs e)
        {
            try
            {
                _CAPTURECAM = new Capture(HelperCamera.SelectCamera(_DRCONFIG.CAMCameraSelfieSelected));
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 1280);
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 720);

                picGuiaCaptureSelfie.Visible = true;
                picGuiaCaptureSelfie.Refresh();

                //Set imagenes de start y stop 
                picCapturaSelfieStart.Image = Properties.Resources.btn_Start_Disabled;
                picCapturaSelfieStart.Refresh();
                picCapturaSelfieStart.Enabled = false;
                picCapturaSelfieStop.Image = Properties.Resources.btn_Stop_Enabled;
                picCapturaSelfieStop.Enabled = true;
                picCapturaSelfieStop.Refresh();

                Application.Idle += ShowCamStream;
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.picCapturaSelfieStart_Click Error: " + ex.Message);
            }
        }

        private void picCapturaSelfieStop_Click(object sender, EventArgs e)
        {
            try
            {
                StopCamStream();
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.picCapturaSelfieStop_Click Error = " + ex.Message, ex);
            }
        }

       

        private void labCamStop2_Click(object sender, EventArgs e)
        {
            StopCamStream();
        }

        private void labCapturaImgSelfie_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("DRUI.labCapturaImgSelfie_Click IN...");
                picSelfie.Image = imageCamSelfie.Image;
                picSelfie.Visible = true;
                _PERSONA.Selfie = GetB64FromPictureBox(picSelfie);
                if (!string.IsNullOrEmpty(_PERSONA.Selfie))
                {
                    picSelfieStatus.Image = Properties.Resources.SelfieStatusOK;
                }
                else
                {
                    picSelfieStatus.Image = Properties.Resources.SelfieStatusNOOK;
                }
                picSelfieStatus.Refresh();
                LOG.Debug("DRUI.labCapturaImgSelfie_Click _ImageCedulaB64 = " + _PERSONA.Selfie.Substring(0,15) + "...");
                CheckEnableSiguiente();
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.labCapturaImgSelfie_Click Error = " + ex.Message, ex);
            }
            LOG.Info("DRUI.labCapturaImgSelfie_Click OUT!");
        }

       

        private void btnSigCapturaSelfie_Click(object sender, EventArgs e)
        {
            string msgerror;
            try
            {
                SetEstadoWizard(EstadoWizard.EsperandoLecturaCodigoBarras);
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.btnSigCaptureDocument_Click - Error: " + ex.Message);
            }
        }

        private void labSigCaptureSelfie_Click(object sender, EventArgs e)
        {
            string msgerror;
            try
            {
                //StopCamStream();
                picCapturaSelfieStop_Click(null, null);
                SetEstadoWizard(EstadoWizard.EsperandoLecturaCodigoBarras);
                FillResumenGroup();
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.btnSigCaptureDocument_Click - Error: " + ex.Message);
            }
        }

        private void labCapDocStatusSalir_Click(object sender, EventArgs e)
        {
            CloseControl();
        }

        #endregion WebCam para captura de Doc Front/Back 
        
        #region Utils

        private string GetB64FromPictureBox(PictureBox pic)
        {
            return ImageToBase64(pic.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        public string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            string base64String = null;
            LOG.Debug("DRUI.ImageToBase64 IN...");
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    image.Save(ms, format);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    base64String = Convert.ToBase64String(imageBytes);
                    LOG.Debug("CIUIAdapter.ImageToBase64 base64String = " + base64String);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.ImageToBase64 Error = " + ex.Message, ex);
                base64String = null;
            }
            LOG.Debug("CIUIAdapter.ImageToBase64 OUT!");
            return base64String;
        }

      



        #endregion Utils




    }
}
