﻿using DirectShowLib;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BVIUIAdapter7.Libs
{
    internal static class HelperCamera
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(HelperCamera));

        #region Structure

        struct Video_Device
        {
            public string Device_Name;
            public int Device_ID;
            public Guid Identifier;

            public Video_Device(int ID, string Name, Guid Identity = new Guid())
            {
                Device_ID = ID;
                Device_Name = Name;
                Identifier = Identity;
            }

            /// <summary>
            /// Represent the Device as a String
            /// </summary>
            /// <returns>The string representation of this color</returns>
            public override string ToString()
            {
                return String.Format("[{0}] {1}: {2}", Device_ID, Device_Name, Identifier);
            }
        }

        #endregion Structure

        #region Funciones

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cameraName"></param>
        /// <returns></returns>
        static internal int SelectCamera(string cameraName)
        {
            int index = 0;

            try
            {
                LOG.Debug("HelperCamera.SelectCamera - IN => Buscando cameraName=" + cameraName);
                //-> Find systems cameras with DirectShow.Net dll
                //thanks to carles lloret
                DsDevice[] _SystemCamereas = DsDevice.GetDevicesOfCat(DirectShowLib.FilterCategory.VideoInputDevice);
                //Video_Device[] WebCams = new Video_Device[_SystemCamereas.Length];
                LOG.Debug("HelperCamera.SelectCamera - Q# Camaras encontradas: " + 
                            (_SystemCamereas != null? _SystemCamereas.Length.ToString() : "Null"));
                int iAux = 1;
                for (int i = 0; i < _SystemCamereas.Length; i++)
                {
                    LOG.Debug("HelperCamera.SelectCamera - Camara " + i.ToString() + " => Nombre=" + _SystemCamereas[i].Name +
                                " | ClassId=" + _SystemCamereas[i].ClassID.ToString() + 
                                " | DevicePath=" + _SystemCamereas[i].DevicePath);
                }
                //foreach (Video_Device item in WebCams)
                //{
                //    LOG.Debug("HelperCamera.SelectCamera - Camara " + iAux.ToString() + " => Nombre=" + item.Device_Name +
                //                " | DeviceId=" + item.Device_ID + " | Identifier=" + item.Identifier);
                //    iAux++;
                //}
                for (int i = 0; i < _SystemCamereas.Length; i++)
                {
                    if (_SystemCamereas[i].Name.Equals(cameraName))
                    {
                        index = i;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                index = 0;
                LOG.Error("HelperCamera.SelectCamera - Ex Error: " + ex.Message);
            }
            LOG.Debug("HelperCamera.SelectCamera - OUT! Index = " + index.ToString());
            return index;
        }

        #endregion Funciones
    }
}
