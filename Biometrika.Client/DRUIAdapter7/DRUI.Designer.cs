﻿namespace DRUIAdapter7
{
    partial class DRUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DRUI));
            this.labTitle = new System.Windows.Forms.Label();
            this.grpCapturaSelfie = new System.Windows.Forms.GroupBox();
            this.picCapturaSelfieStop = new System.Windows.Forms.PictureBox();
            this.picCapturaSelfieStart = new System.Windows.Forms.PictureBox();
            this.picSelfieStatus = new System.Windows.Forms.PictureBox();
            this.labSigCaptureSelfie = new System.Windows.Forms.Label();
            this.picGuiaCaptureSelfie = new System.Windows.Forms.PictureBox();
            this.labMsgProcess2 = new System.Windows.Forms.Label();
            this.picProcess2 = new System.Windows.Forms.PictureBox();
            this.labCapturaImgSelfie = new System.Windows.Forms.Label();
            this.picSelfie = new System.Windows.Forms.PictureBox();
            this.imageCamSelfie = new System.Windows.Forms.PictureBox();
            this.picCapSelfieBackgroung = new System.Windows.Forms.PictureBox();
            this.picPaso1 = new System.Windows.Forms.PictureBox();
            this.picPaso2 = new System.Windows.Forms.PictureBox();
            this.labClose = new System.Windows.Forms.Label();
            this.picFinal = new System.Windows.Forms.PictureBox();
            this.grpCapturaImagenDocumento = new System.Windows.Forms.GroupBox();
            this.grpCapDocStatusBlock = new System.Windows.Forms.GroupBox();
            this.labCapDocStatusBlockDetail = new System.Windows.Forms.Label();
            this.labCapDocStatusSalir = new System.Windows.Forms.Label();
            this.labCapDocStatusBlock = new System.Windows.Forms.Label();
            this.picCapDocStatusBlock = new System.Windows.Forms.PictureBox();
            this.labCapDocStatusMarco = new System.Windows.Forms.Label();
            this.picGuiaCaptureDocument = new System.Windows.Forms.PictureBox();
            this.picDocStop = new System.Windows.Forms.PictureBox();
            this.picDocStart = new System.Windows.Forms.PictureBox();
            this.picDocBackStatus = new System.Windows.Forms.PictureBox();
            this.picDocFrontStatus = new System.Windows.Forms.PictureBox();
            this.labCapturaImgDocBack = new System.Windows.Forms.Label();
            this.labCapturaImgDocFront = new System.Windows.Forms.Label();
            this.labSigCaptureDocument = new System.Windows.Forms.Label();
            this.labMsgProcess1 = new System.Windows.Forms.Label();
            this.picProcess1 = new System.Windows.Forms.PictureBox();
            this.picDocumentBack = new System.Windows.Forms.PictureBox();
            this.picDocumentFront = new System.Windows.Forms.PictureBox();
            this.imageCam = new System.Windows.Forms.PictureBox();
            this.picCapDocBackgroung = new System.Windows.Forms.PictureBox();
            this.labTrackId = new System.Windows.Forms.Label();
            this.txtMessage = new System.Windows.Forms.Label();
            this.labReinit = new System.Windows.Forms.Label();
            this.grpExportar = new System.Windows.Forms.GroupBox();
            this.labResultSendMailPDF = new System.Windows.Forms.Label();
            this.txtMailToSendPDF = new System.Windows.Forms.TextBox();
            this.labExportarDownloadPDF = new System.Windows.Forms.Label();
            this.labSigExport = new System.Windows.Forms.Label();
            this.labProgressExport = new System.Windows.Forms.Label();
            this.picProgressExport = new System.Windows.Forms.PictureBox();
            this.labExportarSend = new System.Windows.Forms.Label();
            this.picExportarBackground4 = new System.Windows.Forms.PictureBox();
            this.grpFinal = new System.Windows.Forms.GroupBox();
            this.labFinalSalir = new System.Windows.Forms.Label();
            this.picFinalBackground = new System.Windows.Forms.PictureBox();
            this.labInicioStart = new System.Windows.Forms.Label();
            this.picInicio = new System.Windows.Forms.PictureBox();
            this.labSupport = new System.Windows.Forms.Label();
            this.labAcercaDe = new System.Windows.Forms.Label();
            this.labSubTitle = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.grpResumen = new System.Windows.Forms.GroupBox();
            this.picResumenFirma = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.labResumenNacionality = new System.Windows.Forms.Label();
            this.labResumenSex = new System.Windows.Forms.Label();
            this.labResumenFV = new System.Windows.Forms.Label();
            this.labResumenSerial = new System.Windows.Forms.Label();
            this.labResumenFNac = new System.Windows.Forms.Label();
            this.labResumenName = new System.Windows.Forms.Label();
            this.picResumenIconoMenorDeEdad = new System.Windows.Forms.PictureBox();
            this.picResumenIconoCedVencida = new System.Windows.Forms.PictureBox();
            this.picResumenSelfie = new System.Windows.Forms.PictureBox();
            this.picResumenDocBack = new System.Windows.Forms.PictureBox();
            this.picResumenDocFront = new System.Windows.Forms.PictureBox();
            this.picResumenSalir = new System.Windows.Forms.PictureBox();
            this.picResumenFondo = new System.Windows.Forms.PictureBox();
            this.labShowResumen = new System.Windows.Forms.Label();
            this.grpProgress = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.picProgreso = new System.Windows.Forms.PictureBox();
            this.labVersion = new System.Windows.Forms.Label();
            this.grpCapturaSelfie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCapturaSelfieStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCapturaSelfieStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSelfieStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGuiaCaptureSelfie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProcess2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSelfie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCamSelfie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCapSelfieBackgroung)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFinal)).BeginInit();
            this.grpCapturaImagenDocumento.SuspendLayout();
            this.grpCapDocStatusBlock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCapDocStatusBlock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGuiaCaptureDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocBackStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocFrontStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProcess1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocumentBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocumentFront)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCapDocBackgroung)).BeginInit();
            this.grpExportar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picProgressExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picExportarBackground4)).BeginInit();
            this.grpFinal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFinalBackground)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.grpResumen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenFirma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenIconoMenorDeEdad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenIconoCedVencida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenSelfie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenDocBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenDocFront)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenFondo)).BeginInit();
            this.grpProgress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picProgreso)).BeginInit();
            this.SuspendLayout();
            // 
            // labTitle
            // 
            this.labTitle.BackColor = System.Drawing.Color.Transparent;
            this.labTitle.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTitle.ForeColor = System.Drawing.Color.Gray;
            this.labTitle.Location = new System.Drawing.Point(390, 60);
            this.labTitle.Name = "labTitle";
            this.labTitle.Size = new System.Drawing.Size(544, 35);
            this.labTitle.TabIndex = 12;
            this.labTitle.Text = "Reconociendo RUT ...";
            this.labTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpCapturaSelfie
            // 
            this.grpCapturaSelfie.BackColor = System.Drawing.Color.White;
            this.grpCapturaSelfie.Controls.Add(this.picCapturaSelfieStop);
            this.grpCapturaSelfie.Controls.Add(this.picCapturaSelfieStart);
            this.grpCapturaSelfie.Controls.Add(this.picSelfieStatus);
            this.grpCapturaSelfie.Controls.Add(this.labSigCaptureSelfie);
            this.grpCapturaSelfie.Controls.Add(this.picGuiaCaptureSelfie);
            this.grpCapturaSelfie.Controls.Add(this.labMsgProcess2);
            this.grpCapturaSelfie.Controls.Add(this.picProcess2);
            this.grpCapturaSelfie.Controls.Add(this.labCapturaImgSelfie);
            this.grpCapturaSelfie.Controls.Add(this.picSelfie);
            this.grpCapturaSelfie.Controls.Add(this.imageCamSelfie);
            this.grpCapturaSelfie.Controls.Add(this.picCapSelfieBackgroung);
            this.grpCapturaSelfie.Location = new System.Drawing.Point(1475, 121);
            this.grpCapturaSelfie.Name = "grpCapturaSelfie";
            this.grpCapturaSelfie.Size = new System.Drawing.Size(842, 517);
            this.grpCapturaSelfie.TabIndex = 35;
            this.grpCapturaSelfie.TabStop = false;
            this.grpCapturaSelfie.Text = "Captura Selfie...";
            this.grpCapturaSelfie.Visible = false;
            // 
            // picCapturaSelfieStop
            // 
            this.picCapturaSelfieStop.BackColor = System.Drawing.Color.Transparent;
            this.picCapturaSelfieStop.Enabled = false;
            this.picCapturaSelfieStop.Image = global::DRUIAdapter7.Properties.Resources.btn_Stop_Disabled;
            this.picCapturaSelfieStop.Location = new System.Drawing.Point(274, 409);
            this.picCapturaSelfieStop.Name = "picCapturaSelfieStop";
            this.picCapturaSelfieStop.Size = new System.Drawing.Size(185, 28);
            this.picCapturaSelfieStop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCapturaSelfieStop.TabIndex = 45;
            this.picCapturaSelfieStop.TabStop = false;
            this.picCapturaSelfieStop.Click += new System.EventHandler(this.picCapturaSelfieStop_Click);
            // 
            // picCapturaSelfieStart
            // 
            this.picCapturaSelfieStart.BackColor = System.Drawing.Color.Transparent;
            this.picCapturaSelfieStart.Image = global::DRUIAdapter7.Properties.Resources.btn_Start_Enabled;
            this.picCapturaSelfieStart.Location = new System.Drawing.Point(77, 409);
            this.picCapturaSelfieStart.Name = "picCapturaSelfieStart";
            this.picCapturaSelfieStart.Size = new System.Drawing.Size(185, 28);
            this.picCapturaSelfieStart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCapturaSelfieStart.TabIndex = 44;
            this.picCapturaSelfieStart.TabStop = false;
            this.picCapturaSelfieStart.Click += new System.EventHandler(this.picCapturaSelfieStart_Click);
            // 
            // picSelfieStatus
            // 
            this.picSelfieStatus.Image = global::DRUIAdapter7.Properties.Resources.SelfieStatus;
            this.picSelfieStatus.Location = new System.Drawing.Point(725, 136);
            this.picSelfieStatus.Name = "picSelfieStatus";
            this.picSelfieStatus.Size = new System.Drawing.Size(50, 50);
            this.picSelfieStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSelfieStatus.TabIndex = 43;
            this.picSelfieStatus.TabStop = false;
            // 
            // labSigCaptureSelfie
            // 
            this.labSigCaptureSelfie.Image = global::DRUIAdapter7.Properties.Resources.btn_Siguiente_Enabled;
            this.labSigCaptureSelfie.Location = new System.Drawing.Point(678, 448);
            this.labSigCaptureSelfie.Name = "labSigCaptureSelfie";
            this.labSigCaptureSelfie.Size = new System.Drawing.Size(131, 39);
            this.labSigCaptureSelfie.TabIndex = 42;
            this.labSigCaptureSelfie.Visible = false;
            this.labSigCaptureSelfie.Click += new System.EventHandler(this.labSigCaptureSelfie_Click);
            // 
            // picGuiaCaptureSelfie
            // 
            this.picGuiaCaptureSelfie.BackColor = System.Drawing.Color.Transparent;
            this.picGuiaCaptureSelfie.Image = global::DRUIAdapter7.Properties.Resources.img_Guia_Selfie;
            this.picGuiaCaptureSelfie.Location = new System.Drawing.Point(117, 163);
            this.picGuiaCaptureSelfie.Name = "picGuiaCaptureSelfie";
            this.picGuiaCaptureSelfie.Size = new System.Drawing.Size(310, 224);
            this.picGuiaCaptureSelfie.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picGuiaCaptureSelfie.TabIndex = 40;
            this.picGuiaCaptureSelfie.TabStop = false;
            this.picGuiaCaptureSelfie.Visible = false;
            // 
            // labMsgProcess2
            // 
            this.labMsgProcess2.Location = new System.Drawing.Point(560, 452);
            this.labMsgProcess2.Name = "labMsgProcess2";
            this.labMsgProcess2.Size = new System.Drawing.Size(143, 35);
            this.labMsgProcess2.TabIndex = 37;
            this.labMsgProcess2.Text = "Procesando imágenes. Espere por favor...";
            this.labMsgProcess2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labMsgProcess2.Visible = false;
            // 
            // picProcess2
            // 
            this.picProcess2.BackColor = System.Drawing.Color.Transparent;
            this.picProcess2.Image = global::DRUIAdapter7.Properties.Resources.upload;
            this.picProcess2.Location = new System.Drawing.Point(512, 452);
            this.picProcess2.Name = "picProcess2";
            this.picProcess2.Size = new System.Drawing.Size(35, 35);
            this.picProcess2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picProcess2.TabIndex = 36;
            this.picProcess2.TabStop = false;
            this.picProcess2.Visible = false;
            // 
            // labCapturaImgSelfie
            // 
            this.labCapturaImgSelfie.BackColor = System.Drawing.Color.Transparent;
            this.labCapturaImgSelfie.Image = ((System.Drawing.Image)(resources.GetObject("labCapturaImgSelfie.Image")));
            this.labCapturaImgSelfie.Location = new System.Drawing.Point(480, 272);
            this.labCapturaImgSelfie.Name = "labCapturaImgSelfie";
            this.labCapturaImgSelfie.Size = new System.Drawing.Size(63, 28);
            this.labCapturaImgSelfie.TabIndex = 21;
            this.labCapturaImgSelfie.Click += new System.EventHandler(this.labCapturaImgSelfie_Click);
            // 
            // picSelfie
            // 
            this.picSelfie.Location = new System.Drawing.Point(562, 192);
            this.picSelfie.Name = "picSelfie";
            this.picSelfie.Size = new System.Drawing.Size(200, 200);
            this.picSelfie.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSelfie.TabIndex = 17;
            this.picSelfie.TabStop = false;
            // 
            // imageCamSelfie
            // 
            this.imageCamSelfie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageCamSelfie.Location = new System.Drawing.Point(81, 150);
            this.imageCamSelfie.Name = "imageCamSelfie";
            this.imageCamSelfie.Size = new System.Drawing.Size(380, 250);
            this.imageCamSelfie.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageCamSelfie.TabIndex = 0;
            this.imageCamSelfie.TabStop = false;
            // 
            // picCapSelfieBackgroung
            // 
            this.picCapSelfieBackgroung.Image = global::DRUIAdapter7.Properties.Resources.Fondo_Captura_Selfie;
            this.picCapSelfieBackgroung.Location = new System.Drawing.Point(22, 35);
            this.picCapSelfieBackgroung.Name = "picCapSelfieBackgroung";
            this.picCapSelfieBackgroung.Size = new System.Drawing.Size(801, 469);
            this.picCapSelfieBackgroung.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCapSelfieBackgroung.TabIndex = 41;
            this.picCapSelfieBackgroung.TabStop = false;
            // 
            // picPaso1
            // 
            this.picPaso1.BackColor = System.Drawing.Color.Transparent;
            this.picPaso1.Image = global::DRUIAdapter7.Properties.Resources.CirculoAzul_small;
            this.picPaso1.Location = new System.Drawing.Point(135, 210);
            this.picPaso1.Name = "picPaso1";
            this.picPaso1.Size = new System.Drawing.Size(20, 20);
            this.picPaso1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPaso1.TabIndex = 25;
            this.picPaso1.TabStop = false;
            this.picPaso1.Visible = false;
            // 
            // picPaso2
            // 
            this.picPaso2.BackColor = System.Drawing.Color.Transparent;
            this.picPaso2.Image = global::DRUIAdapter7.Properties.Resources.CirculoAzul_small;
            this.picPaso2.Location = new System.Drawing.Point(134, 269);
            this.picPaso2.Name = "picPaso2";
            this.picPaso2.Size = new System.Drawing.Size(20, 20);
            this.picPaso2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPaso2.TabIndex = 26;
            this.picPaso2.TabStop = false;
            this.picPaso2.Visible = false;
            // 
            // labClose
            // 
            this.labClose.BackColor = System.Drawing.Color.Transparent;
            this.labClose.Location = new System.Drawing.Point(1258, 66);
            this.labClose.Name = "labClose";
            this.labClose.Size = new System.Drawing.Size(27, 27);
            this.labClose.TabIndex = 27;
            this.labClose.Click += new System.EventHandler(this.labClose_Click);
            // 
            // picFinal
            // 
            this.picFinal.BackColor = System.Drawing.Color.Transparent;
            this.picFinal.Image = global::DRUIAdapter7.Properties.Resources.CirculoAzul_small;
            this.picFinal.Location = new System.Drawing.Point(129, 329);
            this.picFinal.Name = "picFinal";
            this.picFinal.Size = new System.Drawing.Size(33, 34);
            this.picFinal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFinal.TabIndex = 33;
            this.picFinal.TabStop = false;
            this.picFinal.Visible = false;
            // 
            // grpCapturaImagenDocumento
            // 
            this.grpCapturaImagenDocumento.BackColor = System.Drawing.Color.White;
            this.grpCapturaImagenDocumento.Controls.Add(this.grpCapDocStatusBlock);
            this.grpCapturaImagenDocumento.Controls.Add(this.picGuiaCaptureDocument);
            this.grpCapturaImagenDocumento.Controls.Add(this.picDocStop);
            this.grpCapturaImagenDocumento.Controls.Add(this.picDocStart);
            this.grpCapturaImagenDocumento.Controls.Add(this.picDocBackStatus);
            this.grpCapturaImagenDocumento.Controls.Add(this.picDocFrontStatus);
            this.grpCapturaImagenDocumento.Controls.Add(this.labCapturaImgDocBack);
            this.grpCapturaImagenDocumento.Controls.Add(this.labCapturaImgDocFront);
            this.grpCapturaImagenDocumento.Controls.Add(this.labSigCaptureDocument);
            this.grpCapturaImagenDocumento.Controls.Add(this.labMsgProcess1);
            this.grpCapturaImagenDocumento.Controls.Add(this.picProcess1);
            this.grpCapturaImagenDocumento.Controls.Add(this.picDocumentBack);
            this.grpCapturaImagenDocumento.Controls.Add(this.picDocumentFront);
            this.grpCapturaImagenDocumento.Controls.Add(this.imageCam);
            this.grpCapturaImagenDocumento.Controls.Add(this.picCapDocBackgroung);
            this.grpCapturaImagenDocumento.Location = new System.Drawing.Point(1519, 36);
            this.grpCapturaImagenDocumento.Name = "grpCapturaImagenDocumento";
            this.grpCapturaImagenDocumento.Size = new System.Drawing.Size(842, 517);
            this.grpCapturaImagenDocumento.TabIndex = 34;
            this.grpCapturaImagenDocumento.TabStop = false;
            this.grpCapturaImagenDocumento.Text = "Captura Documento...";
            this.grpCapturaImagenDocumento.Visible = false;
            // 
            // grpCapDocStatusBlock
            // 
            this.grpCapDocStatusBlock.Controls.Add(this.labCapDocStatusBlockDetail);
            this.grpCapDocStatusBlock.Controls.Add(this.labCapDocStatusSalir);
            this.grpCapDocStatusBlock.Controls.Add(this.labCapDocStatusBlock);
            this.grpCapDocStatusBlock.Controls.Add(this.picCapDocStatusBlock);
            this.grpCapDocStatusBlock.Controls.Add(this.labCapDocStatusMarco);
            this.grpCapDocStatusBlock.Location = new System.Drawing.Point(220, 429);
            this.grpCapDocStatusBlock.Name = "grpCapDocStatusBlock";
            this.grpCapDocStatusBlock.Size = new System.Drawing.Size(340, 215);
            this.grpCapDocStatusBlock.TabIndex = 48;
            this.grpCapDocStatusBlock.TabStop = false;
            this.grpCapDocStatusBlock.Visible = false;
            // 
            // labCapDocStatusBlockDetail
            // 
            this.labCapDocStatusBlockDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCapDocStatusBlockDetail.ForeColor = System.Drawing.Color.Red;
            this.labCapDocStatusBlockDetail.Location = new System.Drawing.Point(153, 56);
            this.labCapDocStatusBlockDetail.Name = "labCapDocStatusBlockDetail";
            this.labCapDocStatusBlockDetail.Size = new System.Drawing.Size(175, 86);
            this.labCapDocStatusBlockDetail.TabIndex = 51;
            this.labCapDocStatusBlockDetail.Text = "Por definición de negocios no puede continuar con el proceso de certificación!";
            this.labCapDocStatusBlockDetail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labCapDocStatusSalir
            // 
            this.labCapDocStatusSalir.BackColor = System.Drawing.Color.Transparent;
            this.labCapDocStatusSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCapDocStatusSalir.ForeColor = System.Drawing.Color.White;
            this.labCapDocStatusSalir.Image = global::DRUIAdapter7.Properties.Resources.btnSalir_Enabled_Final;
            this.labCapDocStatusSalir.Location = new System.Drawing.Point(124, 159);
            this.labCapDocStatusSalir.Name = "labCapDocStatusSalir";
            this.labCapDocStatusSalir.Size = new System.Drawing.Size(114, 37);
            this.labCapDocStatusSalir.TabIndex = 50;
            this.labCapDocStatusSalir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labCapDocStatusSalir.Click += new System.EventHandler(this.labCapDocStatusSalir_Click);
            // 
            // labCapDocStatusBlock
            // 
            this.labCapDocStatusBlock.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCapDocStatusBlock.ForeColor = System.Drawing.Color.Red;
            this.labCapDocStatusBlock.Location = new System.Drawing.Point(152, 22);
            this.labCapDocStatusBlock.Name = "labCapDocStatusBlock";
            this.labCapDocStatusBlock.Size = new System.Drawing.Size(176, 34);
            this.labCapDocStatusBlock.TabIndex = 49;
            this.labCapDocStatusBlock.Text = "Cédula Vencida";
            this.labCapDocStatusBlock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picCapDocStatusBlock
            // 
            this.picCapDocStatusBlock.Image = global::DRUIAdapter7.Properties.Resources.ObsCedVencida;
            this.picCapDocStatusBlock.Location = new System.Drawing.Point(26, 26);
            this.picCapDocStatusBlock.Name = "picCapDocStatusBlock";
            this.picCapDocStatusBlock.Size = new System.Drawing.Size(115, 98);
            this.picCapDocStatusBlock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picCapDocStatusBlock.TabIndex = 48;
            this.picCapDocStatusBlock.TabStop = false;
            // 
            // labCapDocStatusMarco
            // 
            this.labCapDocStatusMarco.BackColor = System.Drawing.Color.Transparent;
            this.labCapDocStatusMarco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labCapDocStatusMarco.Location = new System.Drawing.Point(7, 12);
            this.labCapDocStatusMarco.Name = "labCapDocStatusMarco";
            this.labCapDocStatusMarco.Size = new System.Drawing.Size(326, 196);
            this.labCapDocStatusMarco.TabIndex = 52;
            // 
            // picGuiaCaptureDocument
            // 
            this.picGuiaCaptureDocument.BackColor = System.Drawing.Color.Transparent;
            this.picGuiaCaptureDocument.Image = global::DRUIAdapter7.Properties.Resources.img_guia_document_final;
            this.picGuiaCaptureDocument.Location = new System.Drawing.Point(78, 149);
            this.picGuiaCaptureDocument.Name = "picGuiaCaptureDocument";
            this.picGuiaCaptureDocument.Size = new System.Drawing.Size(381, 230);
            this.picGuiaCaptureDocument.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picGuiaCaptureDocument.TabIndex = 39;
            this.picGuiaCaptureDocument.TabStop = false;
            this.picGuiaCaptureDocument.Visible = false;
            // 
            // picDocStop
            // 
            this.picDocStop.BackColor = System.Drawing.Color.Transparent;
            this.picDocStop.Enabled = false;
            this.picDocStop.Image = global::DRUIAdapter7.Properties.Resources.btn_Stop_Disabled;
            this.picDocStop.Location = new System.Drawing.Point(274, 420);
            this.picDocStop.Name = "picDocStop";
            this.picDocStop.Size = new System.Drawing.Size(185, 28);
            this.picDocStop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDocStop.TabIndex = 50;
            this.picDocStop.TabStop = false;
            this.picDocStop.Click += new System.EventHandler(this.picDocStop_Click);
            // 
            // picDocStart
            // 
            this.picDocStart.BackColor = System.Drawing.Color.Transparent;
            this.picDocStart.Image = global::DRUIAdapter7.Properties.Resources.btn_Start_Enabled;
            this.picDocStart.Location = new System.Drawing.Point(77, 420);
            this.picDocStart.Name = "picDocStart";
            this.picDocStart.Size = new System.Drawing.Size(185, 28);
            this.picDocStart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDocStart.TabIndex = 49;
            this.picDocStart.TabStop = false;
            this.picDocStart.Click += new System.EventHandler(this.picDocStart_Click);
            // 
            // picDocBackStatus
            // 
            this.picDocBackStatus.Image = global::DRUIAdapter7.Properties.Resources.CedulaDorso;
            this.picDocBackStatus.Location = new System.Drawing.Point(724, 284);
            this.picDocBackStatus.Name = "picDocBackStatus";
            this.picDocBackStatus.Size = new System.Drawing.Size(45, 30);
            this.picDocBackStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDocBackStatus.TabIndex = 45;
            this.picDocBackStatus.TabStop = false;
            // 
            // picDocFrontStatus
            // 
            this.picDocFrontStatus.Image = global::DRUIAdapter7.Properties.Resources.CedulaFrente;
            this.picDocFrontStatus.Location = new System.Drawing.Point(725, 106);
            this.picDocFrontStatus.Name = "picDocFrontStatus";
            this.picDocFrontStatus.Size = new System.Drawing.Size(45, 30);
            this.picDocFrontStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDocFrontStatus.TabIndex = 44;
            this.picDocFrontStatus.TabStop = false;
            // 
            // labCapturaImgDocBack
            // 
            this.labCapturaImgDocBack.BackColor = System.Drawing.Color.Transparent;
            this.labCapturaImgDocBack.Image = ((System.Drawing.Image)(resources.GetObject("labCapturaImgDocBack.Image")));
            this.labCapturaImgDocBack.Location = new System.Drawing.Point(473, 358);
            this.labCapturaImgDocBack.Name = "labCapturaImgDocBack";
            this.labCapturaImgDocBack.Size = new System.Drawing.Size(65, 35);
            this.labCapturaImgDocBack.TabIndex = 43;
            this.labCapturaImgDocBack.Click += new System.EventHandler(this.labCapturaImgDocBack_Click);
            // 
            // labCapturaImgDocFront
            // 
            this.labCapturaImgDocFront.BackColor = System.Drawing.Color.Transparent;
            this.labCapturaImgDocFront.Image = ((System.Drawing.Image)(resources.GetObject("labCapturaImgDocFront.Image")));
            this.labCapturaImgDocFront.Location = new System.Drawing.Point(473, 183);
            this.labCapturaImgDocFront.Name = "labCapturaImgDocFront";
            this.labCapturaImgDocFront.Size = new System.Drawing.Size(62, 42);
            this.labCapturaImgDocFront.TabIndex = 42;
            this.labCapturaImgDocFront.Click += new System.EventHandler(this.labCapturaImgDocFront_Click);
            // 
            // labSigCaptureDocument
            // 
            this.labSigCaptureDocument.Image = global::DRUIAdapter7.Properties.Resources.btn_Siguiente_Enabled;
            this.labSigCaptureDocument.Location = new System.Drawing.Point(674, 454);
            this.labSigCaptureDocument.Name = "labSigCaptureDocument";
            this.labSigCaptureDocument.Size = new System.Drawing.Size(131, 39);
            this.labSigCaptureDocument.TabIndex = 40;
            this.labSigCaptureDocument.Visible = false;
            this.labSigCaptureDocument.Click += new System.EventHandler(this.labSigCaptureDocument_Click);
            // 
            // labMsgProcess1
            // 
            this.labMsgProcess1.Location = new System.Drawing.Point(558, 457);
            this.labMsgProcess1.Name = "labMsgProcess1";
            this.labMsgProcess1.Size = new System.Drawing.Size(123, 35);
            this.labMsgProcess1.TabIndex = 37;
            this.labMsgProcess1.Text = "Procesando imágenes. Espere por favor...";
            this.labMsgProcess1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labMsgProcess1.Visible = false;
            // 
            // picProcess1
            // 
            this.picProcess1.BackColor = System.Drawing.Color.Transparent;
            this.picProcess1.Image = global::DRUIAdapter7.Properties.Resources.upload;
            this.picProcess1.Location = new System.Drawing.Point(519, 457);
            this.picProcess1.Name = "picProcess1";
            this.picProcess1.Size = new System.Drawing.Size(35, 35);
            this.picProcess1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picProcess1.TabIndex = 36;
            this.picProcess1.TabStop = false;
            this.picProcess1.Visible = false;
            // 
            // picDocumentBack
            // 
            this.picDocumentBack.Location = new System.Drawing.Point(566, 325);
            this.picDocumentBack.Name = "picDocumentBack";
            this.picDocumentBack.Size = new System.Drawing.Size(191, 106);
            this.picDocumentBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDocumentBack.TabIndex = 18;
            this.picDocumentBack.TabStop = false;
            // 
            // picDocumentFront
            // 
            this.picDocumentFront.Location = new System.Drawing.Point(566, 148);
            this.picDocumentFront.Name = "picDocumentFront";
            this.picDocumentFront.Size = new System.Drawing.Size(191, 106);
            this.picDocumentFront.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDocumentFront.TabIndex = 17;
            this.picDocumentFront.TabStop = false;
            // 
            // imageCam
            // 
            this.imageCam.BackColor = System.Drawing.Color.Transparent;
            this.imageCam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageCam.Location = new System.Drawing.Point(78, 149);
            this.imageCam.Name = "imageCam";
            this.imageCam.Size = new System.Drawing.Size(381, 254);
            this.imageCam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageCam.TabIndex = 0;
            this.imageCam.TabStop = false;
            // 
            // picCapDocBackgroung
            // 
            this.picCapDocBackgroung.Image = global::DRUIAdapter7.Properties.Resources.Fondo_Captura_Document;
            this.picCapDocBackgroung.Location = new System.Drawing.Point(17, 19);
            this.picCapDocBackgroung.Name = "picCapDocBackgroung";
            this.picCapDocBackgroung.Size = new System.Drawing.Size(803, 486);
            this.picCapDocBackgroung.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCapDocBackgroung.TabIndex = 41;
            this.picCapDocBackgroung.TabStop = false;
            // 
            // labTrackId
            // 
            this.labTrackId.BackColor = System.Drawing.Color.Transparent;
            this.labTrackId.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTrackId.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labTrackId.Location = new System.Drawing.Point(823, 100);
            this.labTrackId.Name = "labTrackId";
            this.labTrackId.Size = new System.Drawing.Size(462, 20);
            this.labTrackId.TabIndex = 36;
            this.labTrackId.Text = "Track Id: ";
            this.labTrackId.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labTrackId.Visible = false;
            // 
            // txtMessage
            // 
            this.txtMessage.BackColor = System.Drawing.Color.Transparent;
            this.txtMessage.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMessage.ForeColor = System.Drawing.Color.Gray;
            this.txtMessage.Location = new System.Drawing.Point(82, 680);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(498, 67);
            this.txtMessage.TabIndex = 39;
            this.txtMessage.Text = "{message}";
            this.txtMessage.Visible = false;
            // 
            // labReinit
            // 
            this.labReinit.BackColor = System.Drawing.Color.Transparent;
            this.labReinit.Location = new System.Drawing.Point(1072, 65);
            this.labReinit.Name = "labReinit";
            this.labReinit.Size = new System.Drawing.Size(27, 27);
            this.labReinit.TabIndex = 40;
            this.labReinit.Click += new System.EventHandler(this.labReinit_Click);
            // 
            // grpExportar
            // 
            this.grpExportar.BackColor = System.Drawing.Color.White;
            this.grpExportar.Controls.Add(this.labResultSendMailPDF);
            this.grpExportar.Controls.Add(this.txtMailToSendPDF);
            this.grpExportar.Controls.Add(this.labExportarDownloadPDF);
            this.grpExportar.Controls.Add(this.labSigExport);
            this.grpExportar.Controls.Add(this.labProgressExport);
            this.grpExportar.Controls.Add(this.picProgressExport);
            this.grpExportar.Controls.Add(this.labExportarSend);
            this.grpExportar.Controls.Add(this.picExportarBackground4);
            this.grpExportar.Location = new System.Drawing.Point(1395, 170);
            this.grpExportar.Name = "grpExportar";
            this.grpExportar.Size = new System.Drawing.Size(842, 517);
            this.grpExportar.TabIndex = 41;
            this.grpExportar.TabStop = false;
            this.grpExportar.Text = "Exportar...";
            this.grpExportar.Visible = false;
            // 
            // labResultSendMailPDF
            // 
            this.labResultSendMailPDF.Location = new System.Drawing.Point(193, 368);
            this.labResultSendMailPDF.Name = "labResultSendMailPDF";
            this.labResultSendMailPDF.Size = new System.Drawing.Size(504, 25);
            this.labResultSendMailPDF.TabIndex = 42;
            this.labResultSendMailPDF.Text = "Enviando mail...";
            this.labResultSendMailPDF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labResultSendMailPDF.Visible = false;
            // 
            // txtMailToSendPDF
            // 
            this.txtMailToSendPDF.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMailToSendPDF.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMailToSendPDF.Location = new System.Drawing.Point(191, 309);
            this.txtMailToSendPDF.Name = "txtMailToSendPDF";
            this.txtMailToSendPDF.Size = new System.Drawing.Size(252, 19);
            this.txtMailToSendPDF.TabIndex = 41;
            this.txtMailToSendPDF.TextChanged += new System.EventHandler(this.txtMailToSendPDF_TextChanged);
            this.txtMailToSendPDF.Leave += new System.EventHandler(this.txtMailToSendPDF_Leave);
            // 
            // labExportarDownloadPDF
            // 
            this.labExportarDownloadPDF.BackColor = System.Drawing.Color.Transparent;
            this.labExportarDownloadPDF.Image = global::DRUIAdapter7.Properties.Resources.btnExportarPDF;
            this.labExportarDownloadPDF.Location = new System.Drawing.Point(350, 173);
            this.labExportarDownloadPDF.Name = "labExportarDownloadPDF";
            this.labExportarDownloadPDF.Size = new System.Drawing.Size(247, 59);
            this.labExportarDownloadPDF.TabIndex = 40;
            this.labExportarDownloadPDF.Click += new System.EventHandler(this.labExportarDownloadPDF_Click);
            // 
            // labSigExport
            // 
            this.labSigExport.Image = global::DRUIAdapter7.Properties.Resources.bntSiguiente;
            this.labSigExport.Location = new System.Drawing.Point(659, 453);
            this.labSigExport.Name = "labSigExport";
            this.labSigExport.Size = new System.Drawing.Size(131, 39);
            this.labSigExport.TabIndex = 38;
            this.labSigExport.Click += new System.EventHandler(this.labSigExport_Click);
            // 
            // labProgressExport
            // 
            this.labProgressExport.Location = new System.Drawing.Point(558, 459);
            this.labProgressExport.Name = "labProgressExport";
            this.labProgressExport.Size = new System.Drawing.Size(110, 32);
            this.labProgressExport.TabIndex = 37;
            this.labProgressExport.Text = "Procesando.               Espere por favor...";
            this.labProgressExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labProgressExport.Visible = false;
            // 
            // picProgressExport
            // 
            this.picProgressExport.BackColor = System.Drawing.Color.Transparent;
            this.picProgressExport.Image = global::DRUIAdapter7.Properties.Resources.upload;
            this.picProgressExport.Location = new System.Drawing.Point(519, 459);
            this.picProgressExport.Name = "picProgressExport";
            this.picProgressExport.Size = new System.Drawing.Size(35, 32);
            this.picProgressExport.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picProgressExport.TabIndex = 36;
            this.picProgressExport.TabStop = false;
            this.picProgressExport.Visible = false;
            // 
            // labExportarSend
            // 
            this.labExportarSend.BackColor = System.Drawing.Color.Transparent;
            this.labExportarSend.Image = global::DRUIAdapter7.Properties.Resources.btnEnviar;
            this.labExportarSend.Location = new System.Drawing.Point(459, 291);
            this.labExportarSend.Name = "labExportarSend";
            this.labExportarSend.Size = new System.Drawing.Size(249, 62);
            this.labExportarSend.TabIndex = 39;
            this.labExportarSend.Click += new System.EventHandler(this.labExportarSend_Click);
            // 
            // picExportarBackground4
            // 
            this.picExportarBackground4.Image = global::DRUIAdapter7.Properties.Resources.FondoExportar;
            this.picExportarBackground4.Location = new System.Drawing.Point(26, 20);
            this.picExportarBackground4.Name = "picExportarBackground4";
            this.picExportarBackground4.Size = new System.Drawing.Size(789, 481);
            this.picExportarBackground4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picExportarBackground4.TabIndex = 0;
            this.picExportarBackground4.TabStop = false;
            // 
            // grpFinal
            // 
            this.grpFinal.BackColor = System.Drawing.Color.White;
            this.grpFinal.Controls.Add(this.labFinalSalir);
            this.grpFinal.Controls.Add(this.picFinalBackground);
            this.grpFinal.Location = new System.Drawing.Point(1380, 318);
            this.grpFinal.Name = "grpFinal";
            this.grpFinal.Size = new System.Drawing.Size(842, 517);
            this.grpFinal.TabIndex = 42;
            this.grpFinal.TabStop = false;
            this.grpFinal.Text = "Final...";
            this.grpFinal.Visible = false;
            // 
            // labFinalSalir
            // 
            this.labFinalSalir.Image = global::DRUIAdapter7.Properties.Resources.btnSalir;
            this.labFinalSalir.Location = new System.Drawing.Point(361, 313);
            this.labFinalSalir.Name = "labFinalSalir";
            this.labFinalSalir.Size = new System.Drawing.Size(131, 39);
            this.labFinalSalir.TabIndex = 39;
            this.labFinalSalir.Click += new System.EventHandler(this.labFinalSalir_Click);
            // 
            // picFinalBackground
            // 
            this.picFinalBackground.Image = global::DRUIAdapter7.Properties.Resources.FondoFinal;
            this.picFinalBackground.Location = new System.Drawing.Point(20, 20);
            this.picFinalBackground.Name = "picFinalBackground";
            this.picFinalBackground.Size = new System.Drawing.Size(808, 478);
            this.picFinalBackground.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFinalBackground.TabIndex = 0;
            this.picFinalBackground.TabStop = false;
            // 
            // labInicioStart
            // 
            this.labInicioStart.BackColor = System.Drawing.Color.Transparent;
            this.labInicioStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labInicioStart.ForeColor = System.Drawing.Color.White;
            this.labInicioStart.Image = global::DRUIAdapter7.Properties.Resources.bntComenzar;
            this.labInicioStart.Location = new System.Drawing.Point(777, 469);
            this.labInicioStart.Name = "labInicioStart";
            this.labInicioStart.Size = new System.Drawing.Size(135, 46);
            this.labInicioStart.TabIndex = 51;
            this.labInicioStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labInicioStart.Click += new System.EventHandler(this.labInicioStart_Click);
            // 
            // picInicio
            // 
            this.picInicio.BackColor = System.Drawing.Color.Transparent;
            this.picInicio.Image = global::DRUIAdapter7.Properties.Resources.CirculoVerde_big;
            this.picInicio.Location = new System.Drawing.Point(128, 148);
            this.picInicio.Name = "picInicio";
            this.picInicio.Size = new System.Drawing.Size(33, 34);
            this.picInicio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picInicio.TabIndex = 52;
            this.picInicio.TabStop = false;
            // 
            // labSupport
            // 
            this.labSupport.BackColor = System.Drawing.Color.Transparent;
            this.labSupport.Location = new System.Drawing.Point(1135, 63);
            this.labSupport.Name = "labSupport";
            this.labSupport.Size = new System.Drawing.Size(32, 32);
            this.labSupport.TabIndex = 53;
            this.labSupport.Click += new System.EventHandler(this.labSupport_Click);
            // 
            // labAcercaDe
            // 
            this.labAcercaDe.BackColor = System.Drawing.Color.Transparent;
            this.labAcercaDe.Location = new System.Drawing.Point(1201, 64);
            this.labAcercaDe.Name = "labAcercaDe";
            this.labAcercaDe.Size = new System.Drawing.Size(32, 32);
            this.labAcercaDe.TabIndex = 54;
            this.labAcercaDe.Click += new System.EventHandler(this.labAcercaDe_Click);
            // 
            // labSubTitle
            // 
            this.labSubTitle.BackColor = System.Drawing.Color.Transparent;
            this.labSubTitle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSubTitle.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labSubTitle.Location = new System.Drawing.Point(396, 100);
            this.labSubTitle.Name = "labSubTitle";
            this.labSubTitle.Size = new System.Drawing.Size(410, 20);
            this.labSubTitle.TabIndex = 55;
            this.labSubTitle.Text = "Nombre";
            this.labSubTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labSubTitle.Visible = false;
            // 
            // picLogo
            // 
            this.picLogo.BackColor = System.Drawing.Color.Transparent;
            this.picLogo.Image = global::DRUIAdapter7.Properties.Resources.Biometrika_Bajada_Small;
            this.picLogo.Location = new System.Drawing.Point(81, 47);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(263, 56);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 55;
            this.picLogo.TabStop = false;
            this.picLogo.Visible = false;
            // 
            // grpResumen
            // 
            this.grpResumen.BackColor = System.Drawing.Color.White;
            this.grpResumen.Controls.Add(this.picResumenFirma);
            this.grpResumen.Controls.Add(this.label7);
            this.grpResumen.Controls.Add(this.labResumenNacionality);
            this.grpResumen.Controls.Add(this.labResumenSex);
            this.grpResumen.Controls.Add(this.labResumenFV);
            this.grpResumen.Controls.Add(this.labResumenSerial);
            this.grpResumen.Controls.Add(this.labResumenFNac);
            this.grpResumen.Controls.Add(this.labResumenName);
            this.grpResumen.Controls.Add(this.picResumenIconoMenorDeEdad);
            this.grpResumen.Controls.Add(this.picResumenIconoCedVencida);
            this.grpResumen.Controls.Add(this.picResumenSelfie);
            this.grpResumen.Controls.Add(this.picResumenDocBack);
            this.grpResumen.Controls.Add(this.picResumenDocFront);
            this.grpResumen.Controls.Add(this.picResumenSalir);
            this.grpResumen.Controls.Add(this.picResumenFondo);
            this.grpResumen.Location = new System.Drawing.Point(380, 136);
            this.grpResumen.Name = "grpResumen";
            this.grpResumen.Size = new System.Drawing.Size(905, 534);
            this.grpResumen.TabIndex = 56;
            this.grpResumen.TabStop = false;
            this.grpResumen.Text = "Resumen..";
            this.grpResumen.Visible = false;
            // 
            // picResumenFirma
            // 
            this.picResumenFirma.BackColor = System.Drawing.Color.Transparent;
            this.picResumenFirma.Image = global::DRUIAdapter7.Properties.Resources.NoImage_Firma;
            this.picResumenFirma.Location = new System.Drawing.Point(265, 347);
            this.picResumenFirma.Name = "picResumenFirma";
            this.picResumenFirma.Size = new System.Drawing.Size(107, 66);
            this.picResumenFirma.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumenFirma.TabIndex = 69;
            this.picResumenFirma.TabStop = false;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(92, 252);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(242, 22);
            this.label7.TabIndex = 68;
            this.label7.Text = "Cédula Nueva";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labResumenNacionality
            // 
            this.labResumenNacionality.BackColor = System.Drawing.Color.Transparent;
            this.labResumenNacionality.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenNacionality.ForeColor = System.Drawing.Color.DimGray;
            this.labResumenNacionality.Location = new System.Drawing.Point(606, 262);
            this.labResumenNacionality.Name = "labResumenNacionality";
            this.labResumenNacionality.Size = new System.Drawing.Size(178, 19);
            this.labResumenNacionality.TabIndex = 66;
            this.labResumenNacionality.Text = "ARG";
            // 
            // labResumenSex
            // 
            this.labResumenSex.BackColor = System.Drawing.Color.Transparent;
            this.labResumenSex.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenSex.ForeColor = System.Drawing.Color.DimGray;
            this.labResumenSex.Location = new System.Drawing.Point(607, 238);
            this.labResumenSex.Name = "labResumenSex";
            this.labResumenSex.Size = new System.Drawing.Size(178, 19);
            this.labResumenSex.TabIndex = 65;
            this.labResumenSex.Text = "M";
            // 
            // labResumenFV
            // 
            this.labResumenFV.BackColor = System.Drawing.Color.Transparent;
            this.labResumenFV.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenFV.ForeColor = System.Drawing.Color.DimGray;
            this.labResumenFV.Location = new System.Drawing.Point(606, 190);
            this.labResumenFV.Name = "labResumenFV";
            this.labResumenFV.Size = new System.Drawing.Size(74, 19);
            this.labResumenFV.TabIndex = 64;
            this.labResumenFV.Text = "02/08/2023";
            // 
            // labResumenSerial
            // 
            this.labResumenSerial.BackColor = System.Drawing.Color.Transparent;
            this.labResumenSerial.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenSerial.ForeColor = System.Drawing.Color.DimGray;
            this.labResumenSerial.Location = new System.Drawing.Point(606, 213);
            this.labResumenSerial.Name = "labResumenSerial";
            this.labResumenSerial.Size = new System.Drawing.Size(178, 19);
            this.labResumenSerial.TabIndex = 63;
            this.labResumenSerial.Text = "601540625";
            // 
            // labResumenFNac
            // 
            this.labResumenFNac.BackColor = System.Drawing.Color.Transparent;
            this.labResumenFNac.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenFNac.ForeColor = System.Drawing.Color.DimGray;
            this.labResumenFNac.Location = new System.Drawing.Point(606, 170);
            this.labResumenFNac.Name = "labResumenFNac";
            this.labResumenFNac.Size = new System.Drawing.Size(74, 19);
            this.labResumenFNac.TabIndex = 62;
            this.labResumenFNac.Text = "28/09/1969";
            // 
            // labResumenName
            // 
            this.labResumenName.BackColor = System.Drawing.Color.Transparent;
            this.labResumenName.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold);
            this.labResumenName.ForeColor = System.Drawing.Color.DimGray;
            this.labResumenName.Location = new System.Drawing.Point(603, 130);
            this.labResumenName.Name = "labResumenName";
            this.labResumenName.Size = new System.Drawing.Size(239, 44);
            this.labResumenName.TabIndex = 61;
            this.labResumenName.Text = "Gustavo Gerardo Suhit Gallucci Cortadi Ferretti";
            // 
            // picResumenIconoMenorDeEdad
            // 
            this.picResumenIconoMenorDeEdad.BackColor = System.Drawing.Color.Transparent;
            this.picResumenIconoMenorDeEdad.Image = global::DRUIAdapter7.Properties.Resources.IconoMenorDeEdad;
            this.picResumenIconoMenorDeEdad.Location = new System.Drawing.Point(494, 347);
            this.picResumenIconoMenorDeEdad.Name = "picResumenIconoMenorDeEdad";
            this.picResumenIconoMenorDeEdad.Size = new System.Drawing.Size(97, 83);
            this.picResumenIconoMenorDeEdad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumenIconoMenorDeEdad.TabIndex = 7;
            this.picResumenIconoMenorDeEdad.TabStop = false;
            this.picResumenIconoMenorDeEdad.Visible = false;
            // 
            // picResumenIconoCedVencida
            // 
            this.picResumenIconoCedVencida.BackColor = System.Drawing.Color.Transparent;
            this.picResumenIconoCedVencida.Image = global::DRUIAdapter7.Properties.Resources.ObsCedVencida;
            this.picResumenIconoCedVencida.Location = new System.Drawing.Point(607, 347);
            this.picResumenIconoCedVencida.Name = "picResumenIconoCedVencida";
            this.picResumenIconoCedVencida.Size = new System.Drawing.Size(97, 83);
            this.picResumenIconoCedVencida.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumenIconoCedVencida.TabIndex = 6;
            this.picResumenIconoCedVencida.TabStop = false;
            this.picResumenIconoCedVencida.Visible = false;
            // 
            // picResumenSelfie
            // 
            this.picResumenSelfie.BackColor = System.Drawing.Color.Transparent;
            this.picResumenSelfie.Image = global::DRUIAdapter7.Properties.Resources.NoImage_Selfie;
            this.picResumenSelfie.Location = new System.Drawing.Point(85, 338);
            this.picResumenSelfie.Name = "picResumenSelfie";
            this.picResumenSelfie.Size = new System.Drawing.Size(87, 87);
            this.picResumenSelfie.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumenSelfie.TabIndex = 4;
            this.picResumenSelfie.TabStop = false;
            // 
            // picResumenDocBack
            // 
            this.picResumenDocBack.BackColor = System.Drawing.Color.Transparent;
            this.picResumenDocBack.Image = global::DRUIAdapter7.Properties.Resources.NoImage_DocBack;
            this.picResumenDocBack.Location = new System.Drawing.Point(235, 141);
            this.picResumenDocBack.Name = "picResumenDocBack";
            this.picResumenDocBack.Size = new System.Drawing.Size(160, 100);
            this.picResumenDocBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumenDocBack.TabIndex = 2;
            this.picResumenDocBack.TabStop = false;
            // 
            // picResumenDocFront
            // 
            this.picResumenDocFront.BackColor = System.Drawing.Color.Transparent;
            this.picResumenDocFront.Image = global::DRUIAdapter7.Properties.Resources.NoImage_DocFront;
            this.picResumenDocFront.Location = new System.Drawing.Point(49, 141);
            this.picResumenDocFront.Name = "picResumenDocFront";
            this.picResumenDocFront.Size = new System.Drawing.Size(160, 100);
            this.picResumenDocFront.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumenDocFront.TabIndex = 1;
            this.picResumenDocFront.TabStop = false;
            // 
            // picResumenSalir
            // 
            this.picResumenSalir.Image = global::DRUIAdapter7.Properties.Resources.btnSalir_Enabled_Final;
            this.picResumenSalir.Location = new System.Drawing.Point(722, 457);
            this.picResumenSalir.Name = "picResumenSalir";
            this.picResumenSalir.Size = new System.Drawing.Size(120, 40);
            this.picResumenSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picResumenSalir.TabIndex = 0;
            this.picResumenSalir.TabStop = false;
            this.picResumenSalir.Click += new System.EventHandler(this.picResumenSalir_Click);
            // 
            // picResumenFondo
            // 
            this.picResumenFondo.Image = global::DRUIAdapter7.Properties.Resources.Fondo_Resumen_Final1;
            this.picResumenFondo.Location = new System.Drawing.Point(6, 11);
            this.picResumenFondo.Name = "picResumenFondo";
            this.picResumenFondo.Size = new System.Drawing.Size(893, 517);
            this.picResumenFondo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumenFondo.TabIndex = 5;
            this.picResumenFondo.TabStop = false;
            // 
            // labShowResumen
            // 
            this.labShowResumen.BackColor = System.Drawing.Color.Transparent;
            this.labShowResumen.Location = new System.Drawing.Point(1248, 130);
            this.labShowResumen.Name = "labShowResumen";
            this.labShowResumen.Size = new System.Drawing.Size(37, 45);
            this.labShowResumen.TabIndex = 57;
            this.labShowResumen.Click += new System.EventHandler(this.labShowResumen_Click);
            // 
            // grpProgress
            // 
            this.grpProgress.BackColor = System.Drawing.Color.White;
            this.grpProgress.Controls.Add(this.label1);
            this.grpProgress.Location = new System.Drawing.Point(135, 962);
            this.grpProgress.Name = "grpProgress";
            this.grpProgress.Size = new System.Drawing.Size(246, 107);
            this.grpProgress.TabIndex = 59;
            this.grpProgress.TabStop = false;
            this.grpProgress.Visible = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(100, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 43);
            this.label1.TabIndex = 63;
            this.label1.Text = "Procesando!                        Espere por favor...";
            // 
            // picProgreso
            // 
            this.picProgreso.BackColor = System.Drawing.Color.Transparent;
            this.picProgreso.Location = new System.Drawing.Point(303, 886);
            this.picProgreso.Name = "picProgreso";
            this.picProgreso.Size = new System.Drawing.Size(70, 70);
            this.picProgreso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picProgreso.TabIndex = 59;
            this.picProgreso.TabStop = false;
            // 
            // labVersion
            // 
            this.labVersion.BackColor = System.Drawing.Color.Transparent;
            this.labVersion.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labVersion.ForeColor = System.Drawing.Color.DimGray;
            this.labVersion.Location = new System.Drawing.Point(1162, 736);
            this.labVersion.Name = "labVersion";
            this.labVersion.Size = new System.Drawing.Size(163, 19);
            this.labVersion.TabIndex = 94;
            this.labVersion.Text = "v7.5.556.569874";
            this.labVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DRUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::DRUIAdapter7.Properties.Resources.themeNV_v11;
            this.ClientSize = new System.Drawing.Size(1370, 770);
            this.Controls.Add(this.labInicioStart);
            this.Controls.Add(this.labVersion);
            this.Controls.Add(this.grpFinal);
            this.Controls.Add(this.picProgreso);
            this.Controls.Add(this.grpExportar);
            this.Controls.Add(this.grpProgress);
            this.Controls.Add(this.grpCapturaSelfie);
            this.Controls.Add(this.grpCapturaImagenDocumento);
            this.Controls.Add(this.grpResumen);
            this.Controls.Add(this.labShowResumen);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.labSubTitle);
            this.Controls.Add(this.labAcercaDe);
            this.Controls.Add(this.labSupport);
            this.Controls.Add(this.picInicio);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.labReinit);
            this.Controls.Add(this.labTrackId);
            this.Controls.Add(this.picFinal);
            this.Controls.Add(this.labClose);
            this.Controls.Add(this.picPaso2);
            this.Controls.Add(this.picPaso1);
            this.Controls.Add(this.labTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DRUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BVIUI";
            this.Load += new System.EventHandler(this.BVIUI_Load);
            this.grpCapturaSelfie.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picCapturaSelfieStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCapturaSelfieStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSelfieStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGuiaCaptureSelfie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProcess2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSelfie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCamSelfie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCapSelfieBackgroung)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFinal)).EndInit();
            this.grpCapturaImagenDocumento.ResumeLayout(false);
            this.grpCapDocStatusBlock.ResumeLayout(false);
            this.grpCapDocStatusBlock.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCapDocStatusBlock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGuiaCaptureDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocBackStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocFrontStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProcess1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocumentBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocumentFront)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCapDocBackgroung)).EndInit();
            this.grpExportar.ResumeLayout(false);
            this.grpExportar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picProgressExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picExportarBackground4)).EndInit();
            this.grpFinal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picFinalBackground)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.grpResumen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picResumenFirma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenIconoMenorDeEdad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenIconoCedVencida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenSelfie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenDocBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenDocFront)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenFondo)).EndInit();
            this.grpProgress.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picProgreso)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label labTitle;
        private System.Windows.Forms.PictureBox picPaso1;
        private System.Windows.Forms.PictureBox picPaso2;
        private System.Windows.Forms.Label labClose;
        private System.Windows.Forms.PictureBox picFinal;
        private System.Windows.Forms.GroupBox grpCapturaImagenDocumento;
        private System.Windows.Forms.PictureBox imageCam;
        private System.Windows.Forms.PictureBox picDocumentBack;
        private System.Windows.Forms.PictureBox picDocumentFront;
        private System.Windows.Forms.Label labMsgProcess1;
        private System.Windows.Forms.PictureBox picProcess1;
        private System.Windows.Forms.GroupBox grpCapturaSelfie;
        private System.Windows.Forms.Label labMsgProcess2;
        private System.Windows.Forms.PictureBox picProcess2;
        private System.Windows.Forms.Label labCapturaImgSelfie;
        private System.Windows.Forms.PictureBox picSelfie;
        private System.Windows.Forms.PictureBox imageCamSelfie;
        private System.Windows.Forms.Label labTrackId;
        private System.Windows.Forms.Label txtMessage;
        private System.Windows.Forms.Label labReinit;
        private System.Windows.Forms.PictureBox picGuiaCaptureDocument;
        private System.Windows.Forms.PictureBox picGuiaCaptureSelfie;
        private System.Windows.Forms.Label labSigCaptureDocument;
        private System.Windows.Forms.PictureBox picCapDocBackgroung;
        private System.Windows.Forms.Label labCapturaImgDocBack;
        private System.Windows.Forms.Label labCapturaImgDocFront;
        private System.Windows.Forms.PictureBox picDocBackStatus;
        private System.Windows.Forms.PictureBox picDocFrontStatus;
        private System.Windows.Forms.PictureBox picCapSelfieBackgroung;
        private System.Windows.Forms.Label labSigCaptureSelfie;
        private System.Windows.Forms.PictureBox picSelfieStatus;
        private System.Windows.Forms.GroupBox grpExportar;
        private System.Windows.Forms.Label labSigExport;
        private System.Windows.Forms.Label labProgressExport;
        private System.Windows.Forms.PictureBox picProgressExport;
        private System.Windows.Forms.PictureBox picExportarBackground4;
        private System.Windows.Forms.Label labExportarDownloadPDF;
        private System.Windows.Forms.Label labExportarSend;
        private System.Windows.Forms.Label labResultSendMailPDF;
        private System.Windows.Forms.TextBox txtMailToSendPDF;
        private System.Windows.Forms.GroupBox grpFinal;
        private System.Windows.Forms.PictureBox picFinalBackground;
        private System.Windows.Forms.Label labFinalSalir;
        private System.Windows.Forms.GroupBox grpCapDocStatusBlock;
        private System.Windows.Forms.Label labCapDocStatusBlockDetail;
        private System.Windows.Forms.Label labCapDocStatusSalir;
        private System.Windows.Forms.Label labCapDocStatusBlock;
        private System.Windows.Forms.PictureBox picCapDocStatusBlock;
        private System.Windows.Forms.Label labCapDocStatusMarco;
        private System.Windows.Forms.Label labInicioStart;
        private System.Windows.Forms.PictureBox picInicio;
        private System.Windows.Forms.Label labSupport;
        private System.Windows.Forms.Label labAcercaDe;
        private System.Windows.Forms.Label labSubTitle;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.GroupBox grpResumen;
        private System.Windows.Forms.PictureBox picResumenSalir;
        private System.Windows.Forms.PictureBox picResumenSelfie;
        private System.Windows.Forms.PictureBox picResumenDocBack;
        private System.Windows.Forms.PictureBox picResumenDocFront;
        private System.Windows.Forms.Label labResumenNacionality;
        private System.Windows.Forms.Label labResumenSex;
        private System.Windows.Forms.Label labResumenFV;
        private System.Windows.Forms.Label labResumenSerial;
        private System.Windows.Forms.Label labResumenFNac;
        private System.Windows.Forms.Label labResumenName;
        private System.Windows.Forms.PictureBox picResumenIconoMenorDeEdad;
        private System.Windows.Forms.PictureBox picResumenIconoCedVencida;
        private System.Windows.Forms.PictureBox picResumenFondo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox picCapturaSelfieStart;
        private System.Windows.Forms.PictureBox picCapturaSelfieStop;
        private System.Windows.Forms.PictureBox picDocStop;
        private System.Windows.Forms.PictureBox picDocStart;
        private System.Windows.Forms.Label labShowResumen;
        private System.Windows.Forms.GroupBox grpProgress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picProgreso;
        private System.Windows.Forms.PictureBox picResumenFirma;
        private System.Windows.Forms.Label labVersion;
    }
}