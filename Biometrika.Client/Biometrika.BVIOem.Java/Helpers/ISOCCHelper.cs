﻿//using Innovatrics.AnsiIso;
//using Innovatrics.AnsiIso.Enums;
//using Innovatrics.Sdk.Commons;
using log4net;
using SecuGen.FDxSDKPro.Windows;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Componente de verificación de identidad, a través de huella dactilar, entre captura realizada
/// en lector Digital Persona o Secugen, y la cédula de identidad Chilena Nueva.
/// </summary>
namespace Biometrika.BVIOem.Java.Helpers
{
    internal class ISOCCHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ISOCCHelper));

        internal static string[] ConvertISOtoISOCC(string raw, out string msgerr)
        {
            string[] sret = null; 
            msgerr = null;

            try
            {
                Int32 iError;
                Byte[] fp_image;
                Int32 img_qlty;

                var max_template_size = 800;

                SGFingerPrintManager m_FPM = new SGFingerPrintManager();
                
                byte[] m_RegMinDP = new Byte[400];
                //fp_image = new Byte[512 * 512];
                fp_image = Convert.FromBase64String(raw); // System.IO.File.ReadAllBytes("RAWGGS.bin");
                img_qlty = 0;

                iError = m_FPM.Init(SGFPMDeviceName.DEV_AUTO);
                //m_FPM.GetImageQuality(512, 512, fp_image, ref img_qlty);

                //DrawImage(fp_image, pictureBoxR1);
                m_FPM.SetTemplateFormat(SGFPMTemplateFormat.ISO19794_COMPACT);
                iError = m_FPM.GetMaxTemplateSize(ref max_template_size);
                var templateGenerated = new byte[max_template_size];
                iError = m_FPM.CreateTemplate(fp_image, templateGenerated);

                sret = ISOCCHelper.ConvertToHexa(templateGenerated);

                int i = 0;
            }
            catch (Exception ex)
            {
                LOG.Error("ISOCCHelper.ConvertISOtoISOCC - Error: " + ex.Message);
            }

            return sret;
            
        }



        internal static string[] ISOToISOCC(string _iso, string pathlic, out string msgerr)
        {
            /*
             C#
                IEngine ansiiso = IEngine.Instance;
                byte[] licenseContent = File.ReadAllBytes("/path/to/mylicense.lic");
                ansiiso.SetLicenseContent(licenseContent);
                ansiiso.Init();
            */

            msgerr = null;
            byte[] _isoTemplate = Convert.FromBase64String(_iso);
            string[] _dataArray = null;
            LOG.Info("ISOCCHelper.ISOToISOCC IN...");
            try
            {
                LOG.Info("ISOCCHelper.ISOToISOCC - Comenzando la conversión de iso a iso compact...");

                LOG.Debug("ISOCCHelper.ISOToISOCC - Creo instancias...");
                //IEngine iEngine = IEngine.Instance;
                //Iso iso = Iso.Instance;

                LOG.Debug("ISOCCHelper.ISOToISOCC - set licencia = " + pathlic);
                byte[] licenseContent = File.ReadAllBytes(pathlic);
                //iEngine.SetLicenseContent(licenseContent);

                //Initialize
                LOG.Debug("ISOCCHelper.ISOToISOCC - Inicializo iEngine...");
                //iEngine.Init();

                //IEngineSortOrder sortOrder1 = IEngineSortOrder.SortNone;
                //IEngineSortOrder sortOrder2 = IEngineSortOrder.SortNone;
                int maximalMinutiaeCount = 63;

                LOG.Debug("ISOCCHelper.ISOToISOCC - Converting ISO to CC...");
                byte[] isoCcTemplate = null;
                //byte[] isoCcTemplate = iso.ConvertToISOCardCC(_isoTemplate, maximalMinutiaeCount, sortOrder1, sortOrder2);
                LOG.Debug("ISOCCHelper.ISOToISOCC - Converting result => " + (isoCcTemplate!=null).ToString());
                if (isoCcTemplate != null)
                {
                    LOG.Debug("ISOCCHelper.ISOToISOCC - Template len = " + isoCcTemplate.Length.ToString());
                }

                LOG.Debug("ISOCCHelper.ISOToISOCC - Terminate iEngine!");
                //iEngine.Terminate();

                Byte[] _temp = new Byte[4000];
                Int32 _isoLength = 0, _isocLength = 0;
                LOG.Debug("ISOCCHelper.ISOToISOCC - Formating template...");
                for (Int32 n = 0; n < isoCcTemplate.Length; n++)
                {
                    _temp[n] = isoCcTemplate[n + 28];

                    if (n > 0 && _temp[n - 1] == 0x00)
                    {
                        _isocLength = n;
                        break;
                    }
                }
                _isocLength = _isocLength - 1;
                Byte[] _isocTemplate = new Byte[_isocLength];
                for (Int32 n = 0; n < _isocLength; n++)
                {
                    _isocTemplate[n] = _temp[n];
                }

                LOG.Debug("ISOCCHelper.ISOToISOCC - Template len = " + 
                    (isoCcTemplate!=null?isoCcTemplate.Length.ToString():"Null"));
                string _data = BitConverter.ToString(_isocTemplate);
                _dataArray = _data.Split('-');
                LOG.Debug("ISOCCHelper.ISOToISOCC - Conversión de iso a iso compact exitosa!");

            }
            catch (Exception ex)
            {
                LOG.Error("Error Excp en ISOtoISOCC", ex);
            }
            LOG.Info("ISOCCHelper.ISOToISOCC OUT!");
            return _dataArray;
        }

        internal static string[] ConvertToHexa(byte[] data)
        {
            byte[] _temp = new Byte[4000];
            Int32 _isoLength = 0, _isocLength = 0;
            try
            {
                LOG.Debug("ISOCCHelper.ConvertToHexa Reorder Minutiae IN...");
                //Re-ordenamiento de minucias
                for (Int32 n = 0; n < data.Length; n++)
                {
                    _temp[n] = data[n + 28];

                    if (n > 0 && _temp[n - 1] == 0x00)
                    {
                        _isocLength = n;

                        break;
                    }
                }

                _isocLength = _isocLength - 1;

                Byte[] _isocTemplate = new Byte[_isocLength];

                for (Int32 n = 0; n < _isocLength; n++)
                {
                    _isocTemplate[n] = _temp[n];
                }
                LOG.Debug("ISOCCHelper.ConvertToHexa Reorder Minutiae OUT!");

                string _isoCCtoHexa = BitConverter.ToString(_isocTemplate);
                return _isoCCtoHexa.Split('-');
            }
            catch (Exception ex)
            {
                LOG.Error("ISOCCHelper.ConvertToHexa Error: " + ex.Message);
                return null;
            }
        }
    }
}
