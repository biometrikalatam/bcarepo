﻿using Bio.Core.BCR;
using Biometrika.BioApi20;
using Biometrika.BioApi20.BSP;
using Biometrika.BioApi20.Interfaces;
using Biometrika.BVIOem.Java.Helpers;
using Biometrika.BVIOem.Java.libs;
//using Biometrika.License.Check;
//using Biometrika.License.Common;
using CSJ2K;
using CSJ2K.Util;
using log4net;
using log4net.Config;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;

namespace Biometrika.BVIOem.Java
{
    /// <summary>
    /// <para>Clase principa de la componente que debe ser instanciada e inicializada, para ser utilizada posteriormente.</para> 
    /// <code language="c#" title="Modo de Uso">
    ///     BviAtm obj = new BviAtm();
    ///     string msgerr;
    ///     string jsongconfig = "{
    ///                             "SensorType": "1",
    ///                             "SerialSensor": "{6AAA4C29-B7C5-9846-9772-E1789F5286C5}",
    ///                             "FingerTimeout": "5000",
    ///                             "QualityThreshold": "50",
    ///                             "PathLogConfig": "Logger.cfg",
    ///                             "COMPort": "COM4",
    ///                             "ReaderProximityName": "HID Global OMNIKEY 5022 Smart Card Reader 0",
    ///                             "pathlic3Pty": "C:\\Biometrika\\iengine.lic",
    ///                             "IntegrityCheckEnabled": "true" + 
    ///                          };
    ///     obj.Initialize(out msgerr, true, jsongconfig);
    ///     ...
    ///     </code>
    ///     <example>
    ///<code>
    ///public partial class Form1 : Form
    ///{
    ///     BviAtm _ATM_COMPONENT;
    ///
    ///     public Form1()
    ///     {
    ///        CheckForIllegalCrossThreadCalls = false;
    ///        InitializeComponent();
    ///     }
    ///
    ///     private void Form1_Load(object sender, EventArgs e)
    ///     {
    ///         string msgerr;
    ///         string jsonconfig = "{" +
    ///                         "\"PathLicense\":  \"BVIOEMATMLicense.lic\"," +
    ///                         "\"SensorType\":  \"1\"," +
    ///                         "\"SerialSensor\":  \"{6AAA4C29-B7C5-9846-9772-E1789F5286C5}\"," +
    ///                         "\"FingerTimeout\":  \"5000\"," +
    ///                         "\"QualityThreshold\":  \"50\"," +
    ///                         "\"PathLogConfig\":  \"Logger.cfg\"," +
    ///                         "\"COMPort\":  \"COM4\"," +
    ///                         "\"ReaderProximityName\":  \"HID Global OMNIKEY 5022 Smart Card Reader 0\"," +
    ///                         "\"pathlic3Pty\":  \"C:\\\\TFSN\\\\Biometrika Client\\\\V7.5\\\\BiometrikaClient\\\\Biometrika.BVIOem.Atm.Test\\\\bin\\\\Debug\\\\iengine.lic\"" +
    ///                     "}";
    ///        _ATM_COMPONENT = new BviAtm();
    ///         int ret = _ATM_COMPONENT.Initialize(out msgerr, true, jsonconfig);
    ///
    ///        _ATM_COMPONENT._OnCapturedEvent += OnCaptureSampleEvent;
    ///        _ATM_COMPONENT._OnTimeoutEvent += OnTimeoutSampleEvent;
    ///        _ATM_COMPONENT._OnErrorEvent += OnErrorEvent;
    ///        _ATM_COMPONENT._OnReadBarcodeEvent += OnReadBarcodeEvent;
    ///        _ATM_COMPONENT._OnVerifytEvent += OnVerifyEvent;
    ///     }
    ///
    ///     private void OnVerifyEvent(int errCode, string errMessage, bool resultverify)
    ///     {
    ///         AddLog("OnVerifyEvent => " + errCode.ToString() + " - " + errMessage +
    ///                     " - Result Verify=" + resultverify.ToString());
    ///     }
    ///
    ///     private void OnReadBarcodeEvent(int errCode, string errMessage, string data)
    ///     {
    ///         AddLog("OnReadBarcodeEvent => " + errCode.ToString() + " - " + errMessage + " - RUT Leido=" + data);
    ///     }
    ///
    ///     private void OnErrorEvent(int errCode, string errMessage)
    ///     {
    ///         AddLog("OnErrorEvent => " + errCode.ToString() + " - " + errMessage);
    ///     }
    ///
    ///     private void OnTimeoutSampleEvent(int errCode, string errMessage)
    ///     {
    ///         AddLog("OnTimeoutSampleEvent => " + errCode.ToString() + " - " + errMessage);
    ///     }
    ///
    ///     private void OnCaptureSampleEvent(int errCode, string errMessage, PersonData pdata) 
    ///     {
    ///         int i = 0;
    ///         string msgerr;
    ///         string data;
    ///         i = _ATM_COMPONENT.GetData(out data, out msgerr);
    ///         AddLog(data);
    ///     }
    ///
    ///     private void btnGetSample_Click(object sender, EventArgs e)
    ///     {
    ///         string msgerr;
    ///         int ret = _ATM_COMPONENT.GetSample(out msgerr);
    ///         AddLog("GetSample ret = " + ret.ToString());
    ///     }
    ///
    ///     private void AddLog(string msg)
    ///     {
    ///         if (string.IsNullOrEmpty(rtbLog.Text))
    ///         {
    ///            rtbLog.Text = msg;
    ///         }
    ///         else
    ///         {
    ///            rtbLog.Text += Environment.NewLine + msg;
    ///         }
    ///     }
    ///
    ///     private void btnReadQR_Click(object sender, EventArgs e)
    ///     {
    ///         string msgerr;
    ///         int ret = _ATM_COMPONENT.ReadBarcode(out msgerr);
    ///         AddLog("ReadBarcode ret = " + ret.ToString());
    ///     }
    ///
    ///     private void button1_Click(object sender, EventArgs e)
    ///     {
    ///         int ret = _ATM_COMPONENT.SetInitVerifyProcess(textBox1.Text);
    ///         AddLog("SetInitVerifyProcess ret = " + ret.ToString());
    ///     }
    ///
    ///     private void btnGetData_Click(object sender, EventArgs e)
    ///     {
    ///         string msgerr;
    ///         string data;
    ///         int ret = _ATM_COMPONENT.GetData(out data, out msgerr, Convert.ToInt32(textBox2.Text));
    ///         AddLog("GetData ret = " + ret.ToString() + " | msgerr = " + msgerr + " | Data = " + data);
    ///     }
    ///
    ///     private void btnVerify_Click(object sender, EventArgs e)
    ///     {
    ///         string msgerr;
    ///         string data;
    ///         int ret = _ATM_COMPONENT.Verify(out msgerr);
    ///         AddLog("Verify ret = " + ret.ToString() + " | msgerr = " + msgerr);
    ///     }
    ///
    ///     private void button2_Click(object sender, EventArgs e)
    ///     {
    ///         rtbLog.Text = "";
    ///     }
    /// }
    /// </code>
    ///  </example>
    /// </summary>
    public class BviOem
    {
        private static readonly ILog LOG = log4net.LogManager.GetLogger(typeof(BviOem));

        //----------------------------------------
        private string _STR_CONFIG;
        private Dictionary<string, string> _CONFIG;

        private BSPBiometrika _BSP;
        private List<Sample> _CurrentSamplesCaptured;

        private BCR LectorCodigoDeBarras;
        private string _DOE_CURRENT;
        private string _DOB_CURRENT;


        private bool _IS_QR_READED = false;
        private bool _IS_SAMPLE_OK = false;

        //Habilita o no el chequeo cuando se hace la verificacion, que el rut en el chip usado para la verificacion este el 
        //mismo rut que el leido desde el QR. Sino sale por error de integridad.
        private bool _INTEGRITY_CHECK_ENABLED = false;

        //Habilita bloqueo de proceso si cédula está vencida. POr default está habilitado para que Diebold 
        //no tenga que modificar su integracion. PEro queda disponible la opciíon de deshabilitar esto en un futuro
        //por configuracion.
        private bool _BLOQUED_EXPIRATION_ENABLED = true;


        #region Public Properties

        /// <summary>
        /// Indica si la componente ya fue inicializada. Esto significa que se creo y se habilitó 
        /// el log si asi se definió, además de definir ciertos parámetros de uso, que se pueden revisar en 
        /// el método Initialize
        /// </summary>
        public bool IsInitialized { get; set; }

        /// <summary>
        /// Datos de la operación de verificación y/o extracción de información desde el chip (opcional)
        /// </summary>
        public PersonData PersonData { get; set; }

        /// <summary>
        /// Evento que se lanza cuando se termina la lectura de un código de barras. 
        /// Si errCode == 0 significa que reconoció bien el código de barras. 
        /// </summary>
        /// <param name="errCode">Ver posibles errores en constantes de error</param>
        /// <param name="errMessage">Descripción de error si se produjo</param>
        /// <param name="data">Datos leídos desde el código de barras QR de la cédula Chilena.</param>
        public delegate void ReadBarcodeCallbackDelegate(int errCode, string errMessage, string data);
        /// <summary>
        /// Implementacion del evento ReadBarcodeCallbackDelegate
        /// </summary>
        public event ReadBarcodeCallbackDelegate _OnReadBarcodeEvent;

        /// <summary>
        /// Evento que se lanza cuando se termina la captura de la huella pedida 
        /// Si errCode == 0 significa que capturo bien el template para verificación. 
        /// </summary>
        /// <param name="errCode">Ver posibles errores en constantes de error</param>
        /// <param name="errMessage">Descripción de error si se produjo</param>
        /// <param name="persondata">Datos recolectados en el proceso. Aqui con los datos de la captura biométrica</param>
        public delegate void CaptureCallbackDelegate(int errCode, string errMessage, PersonData persondata); // List<Sample> samplesCaptured);
        /// <summary>
        /// Implementacion del evento CaptureCallbackDelegate
        /// </summary>
        public event CaptureCallbackDelegate _OnCapturedEvent;

        /// <summary>
        /// Evento que se lanza cuando se produce un error en cualquier parte de la componente
        /// </summary>
        /// <param name="errCode">Ver posibles errores en constantes de error</param>
        /// <param name="errMessage">Descripción de error si se produjo</param>
        public delegate void ErrorCallbackDelegate(int errCode, string errMessage);
        /// <summary>
        /// Implementacion del evento ErrorCallbackDelegate
        /// </summary>
        public event ErrorCallbackDelegate _OnErrorEvent;

        /// <summary>
        /// Evento que se lanza cuando se cumple el timeout en la captura de la huella
        /// </summary>
        /// <param name="errCode">Ver posibles errores en constantes de error</param>
        /// <param name="errMessage">Descripción de error si se produjo</param>
        public delegate void TimeoutCallbackDelegate(int errCode, string errMessage);
        /// <summary>
        /// Implementacion del evento TimeoutCallbackDelegate
        /// </summary>
        public event TimeoutCallbackDelegate _OnTimeoutEvent;

        /// <summary>
        /// Evento que se lanza cuando se termina el proceso de verificación Match On Card
        /// </summary>
        /// <remarks>Si errCode == 0 significa que terminó bien la verificación, y resultverify tiene el resultado.</remarks> 
        /// <param name="errCode">Ver posibles errores en constantes de error</param>
        /// <param name="errMessage">Descripción de error si se produjo</param>
        /// <param name="resultverify">
        ///     Retorna el resultado de verificación:
        ///     <list type="bullet">
        ///     <listheader>Resultados Posibles</listheader>    
        ///     <description>Retorna si fue verificación positiva o negativa</description>
        ///     <item>
        ///         <term>true</term>
        ///         <description>Verificación POSITIVA</description>
        ///     </item>
        ///     <item>
        ///         <term>false</term>
        ///         <description>Verificación NEGATIVA</description>
        ///     </item>
        /// </list>
        /// Recuperando PersonData.Finger, se puede determinar con que dedo se verificó.
        /// </param>
        public delegate void VerifyCallbackDelegate(int errCode, string errMessage, bool resultverify);
        /// <summary>
        /// Implementacion del evento VerifyCallbackDelegate
        /// </summary>
        public event VerifyCallbackDelegate _OnVerifytEvent;


        #endregion Public Properties

        #region Public Method

        /// <summary>
        /// Inicializa la componente, habilitando el log de operaciones o no y enviando configuración para el correcto
        /// funcionamiento.
        /// </summary>
        /// <param name="msgerr">Mensaje de error si hay error</param>
        /// <param name="withlog">true-Habilita log | false-No habilita log</param>
        /// <param name="config">Json de configuración que recibe la componente. El formato es el siguiente:
        /// <code>
        /// {
        ///   "PathLicense":  "BVIOEMATMLicense.lic",
        ///   "SensorType": "1",
        ///   "SerialSensor": "{6AAA4C29-B7C5-9846-9772-E1789F5286C5}",
        ///   "FingerTimeout": "5000",
        ///   "QualityThreshold": "50",
        ///   "PathLogConfig": "Logger.cfg",
        ///   "COMPort": "COM4",
        ///   "ReaderProximityName": "HID Global OMNIKEY 5022 Smart Card Reader 0",
        ///   "pathlic3Pty": "C:\\Biometrika\\iengine.lic",
        ///   "IntegrityCheckEnabled": "true" 
        /// }
        /// </code>
        /// <list type="bullet">
        ///     <listheader>Valores de Configuración</listheader>    
        ///     <description>JSON con los valores de configuración para la operación de la compoente</description>
        ///     <item>
        ///         <term>SensorType</term>
        ///         <description>1-Digital Persona | 2-Secugen</description>
        ///     </item>
        ///     <item>
        ///         <term>SerialSensor</term>
        ///         <description>Número serial del lector utilizado que se utiliza cunado se pide la captura de la huella</description>
        ///     </item>
        ///     <item>
        ///         <term>FingerTimeout</term>
        ///         <description>Valor entero en Milisegundos, que indica cuanto tiempo se espera para la captura 
        ///         de la huella. Si no se logra una captura exitosa, se lanza el evento de Timeout (Ver eventos)</description>
        ///     </item>
        ///     <item>
        ///         <term>QualityThreshold</term>
        ///         <description>Valor entero de 0 a 100, indicando la calidad de imágen mínima pedida en la captura de huella
        ///         </description>
        ///     </item>
        ///     <item>
        ///         <term>PathLogConfig</term>
        ///         <description>Path completo o relaido del archivo de configuración del log (Log4Net)</description>
        ///     </item>
        ///     <item>
        ///         <term>COMPort</term>
        ///         <description>Puerto COM en que está conectado del lector de códigos de barras serial o USB modo serial</description>
        ///     </item>
        ///     <item>
        ///         <term>ReaderProximityName</term>
        ///         <description>Nombre del lector de proximidad utilizado para la interacción con el chip de la cédula.</description>
        ///     </item>
        ///     <item>
        ///         <term>pathlic3Pty</term>
        ///         <description>Path relativo o absoluto de la licencia de terceros necesaria para la conversión de minucias</description>
        ///     </item>
        ///     <item>
        ///         <term>IntegrityCheckEnabled</term>
        ///         <description>true | false - Indica que chequee de forma automática, que el rut leído desde el código de barras, 
        ///         sea el mismo que el leído desde el chip donde se validó la huella. En caso de cambio de chip entre lectura
        ///         del código de barras y la verificación en el lector NFC, el proceso se cancelará dando error.</description>
        ///     </item>
        /// </list>
        /// </param>
        /// <returns>Valor de retornp 0 significa que funcionó todo bien, mientras que si es negativo, es un error 
        /// (Ver tabla de Errores posibles en libs.Errors)</returns>
        public int Initialize(out string msgerr, bool withlog = true, string config = null)
        {
            int ret = 0;
            msgerr = null;
            string sRecolector = ""; //Usado porque si no esta el Log instanciado de antes no se ven los mensajes hasta 
            //que se instancia
            try
            {
                //LOG.Info("BviAtm.Initialize IN => withlog = " + (withlog.ToString()) + 
                //            " - Config Default = " + (config==null).ToString() + " ...");
                sRecolector = "BviAtm.Initialize IN => withlog = " + (withlog.ToString()) +
                            " - Config Default = " + (config == null).ToString() + " ...";
                if (!string.IsNullOrEmpty(config))
                {
                    _STR_CONFIG = config;
                } else
                {
                    ret = Errors.ERROR_INITIALIZING_CONFIG_NULL;
                    msgerr = "Archivo de config nulo! [" + ret.ToString() + "]";
                    LOG.Fatal("BviAtm.Initialize - " + msgerr);
                    _OnErrorEvent(ret, msgerr);
                    return ret;
                }
                
                //LOG.Debug("BviAtm.Initialize - Deserializando config...");
                sRecolector += Environment.NewLine + "BviAtm.Initialize - Deserializando config...";
                _CONFIG = JsonConvert.DeserializeObject<Dictionary<string, string>>(_STR_CONFIG);
                LOG.Debug("BviAtm.Initialize - _CONFIG != null => " + (_CONFIG != null).ToString());

                if (_CONFIG == null)
                {
                    ret = Errors.ERROR_INITIALIZING;
                    msgerr = "Error deserializando config!";
                    LOG.Debug("BviAtm.Initialize - " + msgerr + " Json Config = " + _STR_CONFIG);
                    return ret;
                } else
                {
                    //LOG.Debug("BviAtm.Initialize - Config => " + libs.Utils.ToStringDictionary(_CONFIG));
                    sRecolector += Environment.NewLine +
                                    "BviAtm.Initialize - Config => " + libs.Utils.ToStringDictionary(_CONFIG);
                }

                if (withlog) //Habilito el log
                {
                    XmlConfigurator.Configure(new FileInfo(_CONFIG["PathLogConfig"]));
                    LOG.Debug("BviAtm.Initialize - Inicializado Log...");
                    LOG.Debug(sRecolector);
                }
                PersonData = new PersonData();

                _INTEGRITY_CHECK_ENABLED = _CONFIG.ContainsKey("IntegrityCheckEnabled") ? 
                                            Convert.ToBoolean(_CONFIG["IntegrityCheckEnabled"]) : false;

                _BLOQUED_EXPIRATION_ENABLED = _CONFIG.ContainsKey("BloquedExpirationEnabled") ?
                                            Convert.ToBoolean(_CONFIG["BloquedExpirationEnabled"]) : true;

                LOG.Debug("BviAtm.Initialize - IntegrityCheckEnabled => " + _INTEGRITY_CHECK_ENABLED);

                _IS_LICENSE_OK = CheckLicense(_CONFIG["PathLicense"]);

                if (!_IS_LICENSE_OK)
                {
                    ret = Errors.ERROR_LICENSE;
                    msgerr = "Licencia Invalida!";
                    LOG.Debug("BviAtm.Initialize - " + msgerr);
                    return ret;
                } else
                {
                    LOG.Debug("BviAtm.Initialize - Licencia Válida!");
                    IsInitialized = true;
                }
            }
            catch (Exception ex)
            {
                IsInitialized = false;
                ret = Errors.ERROR_UNKKNOW;
                msgerr = "BviAtm.Initialize Excp Error [" + ex.Message + "]";
                LOG.Error("BviAtm.Initialize Excp Error: ", ex);
                _OnErrorEvent(Errors.ERROR_UNKKNOW, msgerr);
            }
            LOG.Info("BviAtm.Initialize OUT! ret = " + ret.ToString());
            return ret;
        }

        /// <summary>
        /// Seteo de Rut para inicio de proceso.
        /// </summary>
        /// <param name="rut">
        ///     <para>Opcional el envío de rut con formato NNNNNNNN-N o sino null.</para>
        ///     <para>Si se coloca null, se inicia sin un rut y se lo completa cuando se lee el QR. Pero si se
        ///     configura un rut, este se compara con el rut leido, y en caso de ser diferentes, retorna
        ///     error y lanza evento de error</para>
        /// </param>
        /// <returns>Valor de retornp 0 significa que funcionó todo bien, mientras que si es negativo, es un error 
        /// (Ver tabla de Errores posibles en libs.Errors)</returns>
        public int SetInitVerifyProcess(string rut)
        {
            int ret = 0;
            string msgerr;
            try
            {
                if (!IsInitialized)
                {
                    msgerr = "Componente no inicializada!";
                    return Errors.ERROR_NOT_INITIALIZED;
                }

                PersonData = new PersonData();
                PersonData.Rut = string.IsNullOrEmpty(rut) ? "NA" : rut;
            }
            catch (Exception ex)
            {
                LOG.Error("SetInitVerifyProcess Excp Error: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        ///     Se debe llamar a esté método para liberar los recursos tomados en el proceso. 
        /// </summary>
        /// <returns>Valor de retornp 0 significa que funcionó todo bien, mientras que si es negativo, es un error 
        /// (Ver tabla de Errores posibles en libs.Errors)</returns>
        public int SetEndVerifyProcess()
        {
            int ret = 0;
            try
            {
                LOG.Debug("SetEndVerifyProcess IN...");
                LOG.Debug("SetEndVerifyProcess - Check threads...");
                if (_Thread_License_BviOemAtm != null && _Thread_License_BviOemAtm.IsAlive)
                {
                    _Thread_License_BviOemAtm.Abort();
                    _Thread_License_BviOemAtm = null;
                    LOG.Debug("SetEndVerifyProcess - Free resources Thread OK!");
                }
                LOG.Debug("SetEndVerifyProcess - Check LCB...");
                if (LectorCodigoDeBarras != null)
                {
                    try
                    {
                        LectorCodigoDeBarras.Close();
                        LectorCodigoDeBarras = null;
                        LOG.Debug("SetEndVerifyProcess - Free resources LCB OK!");
                    }
                    catch (Exception ex)
                    {
                        ret = -1;
                        LOG.Warn("BviAtm.SetEndVerifyProcess - Err Free resources BC => " + ex.Message);
                    }
                }
                LOG.Debug("SetEndVerifyProcess - Check BSP..");
                if (_BSP != null)
                {
                    _BSP.OnCapturedEvent -= OnCaptureEvent;
                    _BSP.OnTimeoutEvent -= OnTimeoutEvent;
                    _BSP.BSPDetach();
                    LOG.Debug("SetEndVerifyProcess - Free resources BSP OK!");
                }
                IsInitialized = false;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("SetEndVerifyProcess Excp Error: " + ex.Message);
            }
            LOG.Debug("SetEndVerifyProcess OUT [ret=" + ret.ToString() + "]");
            return ret;
        }

        /// <summary>
        /// Retorna los datos pedidos de acuerdo al parámetro Flag enviado. 
        /// </summary>
        /// <param name="data">JSON conteniendo los datos existentes en la variable global PersonData,
        /// de acuerdo al falg enviado.</param>
        /// <param name="msgerr">Descripción de error si existió</param>
        /// <param name="flag">Flag que indica que tipo de información se necesita.
        ///   <list type="bullet">
        ///     <listheader>Flags Posibles</listheader>    
        ///     <description>Depende de este valor la información que se devuelve en un JSON</description>
        ///     <item>
        ///         <term>0</term>
        ///         <description>Default (Solo lo que hay en PersonData)</description>
        ///     </item>
        ///     <item>
        ///         <term>1</term>
        ///         <description>Complete (Completa datos desde lectura de Chip)</description>
        ///     </item>
        ///     <item>
        ///         <term>2</term>
        ///         <description>Incluye imagenes (foto y firma)</description>
        ///     </item>
        ///   </list>
        /// </param>
        /// <returns>Valor de retornp 0 significa que funcionó todo bien, mientras que si es negativo, es un error 
        /// (Ver tabla de Errores posibles en libs.Errors)</returns>
        public int GetData(out string data, out string msgerr, int flag = 0)
        {
            int ret = 0;
            msgerr = null;
            data = null;
            Bio.Core.CLR.CLR clr = null;
            try
            {
                LOG.Info("BviAtm.GetData IN! flag = " + flag.ToString());

                if (flag > 0) //Es compelta => Completo leyendo datos desde chip
                {
                    if (_IS_QR_READED && PersonData != null && 
                        !string.IsNullOrEmpty(_DOB_CURRENT) &&
                        !string.IsNullOrEmpty(_DOE_CURRENT))
                    {
                        clr = new Bio.Core.CLR.CLR();
                        LOG.Debug("BviAtm.GetData - Se inicializa con el lector:" + _CONFIG["ReaderProximityName"]);
                        clr.ReaderName = _CONFIG["ReaderProximityName"];
                        clr.CLR_OPEN(10);
                        Thread.Sleep(100);
                        LOG.Debug("BviAtm.GetData - Conectando al lector...");
                        clr.CLR_CONNECT(Convert.ToUInt32(5000));
                        if (clr.GetLasError() == 0)
                        {
                            LOG.Debug("BviAtm.GetData - Iniciando sesion al chip...");
                            clr.CLR_SECURITY(PersonData.Serie,
                                             _DOB_CURRENT,
                                             _DOE_CURRENT
                                            );
                            LOG.Debug("BviAtm.GetData - Leyendo data...");
                            clr.CLR_GETMRZ();
                            LOG.Debug("BviAtm.GetData - Rellenando datos en PersonData...");
                            PersonData.Nacionalidad = clr.Nationality;
                            PersonData.Apellido = clr.PatherLastName + " " +
                                                  (string.IsNullOrEmpty(clr.MotherLastName)?"": clr.MotherLastName);
                            PersonData.Nombre = clr.Name;
                            PersonData.Sexo = clr.Sex;
                            LOG.Debug("BviAtm.GetData - Datos completos para Rut=" + PersonData.Rut +
                                        " - Nombre=" + PersonData.Apellido + ", " + PersonData.Nombre + "!"); 
                                        
                            if (flag > 1) //Pide imagenes foto y firma incluidas
                            {
                                LOG.Debug("BviAtm.GetData - Recuperando foto...");
                                byte[] foto = clr.CLR_GETFOTO();

                                if (foto != null)
                                {
                                    LOG.Debug("BviAtm.GetData - Procesando foto recuperada...");
                                    BitmapImageCreator.Register();
                                    var por = J2kImage.FromBytes(foto);
                                    Bitmap img = por.As<Bitmap>();
                                    ImageFormat format = ImageFormat.Jpeg;
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        img.Save(ms, format);
                                    }

                                    LOG.Debug("BviAtm.GetData - Completando PersonaData con foto procesada a JPG...");
                                    PersonData.Foto = ImageProcessor.GetBase64Image(img);
                                    img.Dispose();
                                    img = null;
                                } else
                                {
                                    LOG.Debug("BviAtm.GetData - Foto NO recuperada de chip! (Null)");
                                }

                                LOG.Debug("BviAtm.GetData - Recuperando firma...");
                                byte[] firma = clr.CLR_GETFIRMA();
                                if (firma != null)
                                {
                                    LOG.Debug("BviAtm.GetData - Procesando frima recuperada...");
                                    BitmapImageCreator.Register();
                                    var por = J2kImage.FromBytes(firma);
                                    Bitmap img = por.As<Bitmap>();
                                    ImageFormat format = ImageFormat.Jpeg;
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        img.Save(ms, format);
                                    }

                                    LOG.Debug("BviAtm.GetData - Completando PersonaData con firma procesada a JPG...");
                                    PersonData.FotoFirma = ImageProcessor.GetBase64Image(img);
                                    img.Dispose();
                                    img = null;
                                } else
                                {
                                    LOG.Debug("BviAtm.GetData - Firma NO recuperada de chip! (Null)");
                                }
                            } else {
                                LOG.Debug("BviAtm.GetData - Borra data de Foto y Firma desde PersonData xq flag es < 1...");
                                PersonData.Foto = null;
                                PersonData.FotoFirma = null;
                            }
                        }
                        else if (clr.GetLasError() == -1)
                        {
                            msgerr = "Error leyendo chip por data";
                            LOG.Warn("BviAtm.GetData - " + msgerr);
                            _OnErrorEvent(Errors.ERROR_CLR_READING, msgerr);
                            return Errors.ERROR_CLR_READING;
                            //MostrarMensaje(1, Color.Red, "Error al detectar la cédula. Acérquela al dispositivo");
                            //LOG.Error("BVIUI.VerifyNewCedula - La cédula leída no corresponde a la huella capturada");
                        }
                        LOG.Debug("BviAtm.GetData - Desconecta y cierra lector...!");
                        clr.CLR_DISCONNECT();
                        clr.CLR_CLOSE();
                        LOG.Debug("BviAtm.GetData - CLR finalizado!");
                    } else
                    {
                        try
                        {
                            LOG.Debug("BviAtm.GetData - Cerrando CLR por error");
                            if (clr != null)
                            {
                                clr.CLR_DISCONNECT();
                                clr.CLR_CLOSE();
                                clr = null;
                            }
                            LOG.Debug("BviAtm.GetData - CLR Resuorces libarados ok!");
                        }
                        catch (Exception ex1)
                        {
                            clr = null;
                            LOG.Debug("BviAtm.GetData - CLR Resuorces libarados con excp [" + ex1.Message + "]");
                        }
                        msgerr = "Pide datos desde chip pero no leyo el QR => No se puede acceder ala info!";
                        LOG.Warn("BviAtm.GetData - " + msgerr);
                        _OnErrorEvent(Errors.ERROR_VERIFY_PARAM_BC_MISSING, msgerr);
                        return Errors.ERROR_VERIFY_PARAM_BC_MISSING;
                    }
                }
                LOG.Debug("BviAtm.GetData - Serializando salida...");
                data = JsonConvert.SerializeObject(PersonData);
            }
            catch (Exception ex)
            {
                msgerr = "Excp Error rn GetData [" + ex.Message + "]";
                LOG.Error("BviAtm.GetData - " + msgerr);
                _OnErrorEvent(Errors.ERROR_UNKKNOW, msgerr);
            }
            LOG.Debug("BviAtm.GetData - OUT!");
            return ret;
        }

        #region Barcode

        /// <summary>
        /// Inicializa lector para lectura de codigo de barras, en el puerto COM configurado. 
        /// </summary>
        /// <param name="msgerr">Descripción del error si existió</param>
        /// <returns>Valor de retornp 0 significa que funcionó todo bien, mientras que si es negativo, es un error 
        /// (Ver tabla de Errores posibles en libs.Errors)</returns>
        public int ReadBarcode(out string msgerr)
        {
            msgerr = null;
            int ret = 0;
            try
            {
                if (!IsInitialized)
                {
                    msgerr = "Componente no inicializada!";
                    return Errors.ERROR_NOT_INITIALIZED;
                }
                LOG.Info("BviAtm.ReadBarcode IN => readerserial = " + 
                            (_CONFIG.ContainsKey("COMPort") ?_CONFIG["COMPort"]:"NULL"));
                if (LectorCodigoDeBarras != null)
                {
                    try
                    {
                        LectorCodigoDeBarras.Close();
                        LectorCodigoDeBarras = null;
                    }
                    catch (Exception ex)
                    {
                        LOG.Warn("BviAtm.ReadBarcode - Error Excp Cerrando Com de BArcode Reader!");
                    }
                    
                }

                ClearPersonData(1); //Elimina solo datos de QR

                string comPort = _CONFIG["COMPort"].ToUpper();
                LOG.Debug("BviAtm.ReadBarcode - Abriendo el lector de código de barras configurado en Puerto COM: " + comPort);
                LectorCodigoDeBarras = new BCR(comPort);
                var bcrOpened = LectorCodigoDeBarras.Open();
                if (!bcrOpened)
                {
                    LOG.Error("BviAtm.ReadBarcode - Error al abrir el lector de código de barras");
                    msgerr = "Error abriendo el puerto COM: " + comPort + " del lector de códigos de barras!";
                    _OnErrorEvent(Errors.ERROR_BCR_OPEN, msgerr);
                    ret = Errors.ERROR_BCR_OPEN;
                    return ret; 
                }
                else 
                {
                    LectorCodigoDeBarras.DataCaptured += ProcesarCedulaLeida;
                    LOG.Debug("BviAtm.ReadBarcode - Lector de Código de barras abierto correctamente en puerto COM: " + comPort);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BviAtm.ReadBarcode - Excp Error ", ex);
                msgerr = "Error Excp abriendo el puerto COM del lector de códigos de barras! [" + ex.Message + "]";
                _OnErrorEvent(Errors.ERROR_BCR_OPEN, msgerr);
                ret = Errors.ERROR_BCR_OPEN;
                //LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }

        private void ProcesarCedulaLeida(object sender, EventArgs e)
        {
            LOG.Debug("BviAtm.ProcesarCedulaLeida - Captura de lectura de cédula detectada. Antes del control de excepciones");
            try
            { 
                LOG.Debug("BviAtm.ProcesarCedulaLeida - Procesa lectura de cédula detectada...");
                LOG.Debug("BviAtm.ProcesarCedulaLeida - PersonData != null => " + (PersonData != null).ToString());
                LOG.Debug("BviAtm.ProcesarCedulaLeida - LectorCodigoDeBarras.Rut != null => " + 
                                                        (string.IsNullOrEmpty(PersonData.Rut)).ToString());
                LOG.Debug("BviAtm.ProcesarCedulaLeida - LectorCodigoDeBarras != null => " + 
                                                        (LectorCodigoDeBarras != null).ToString());
                if (!PersonData.Rut.Equals("NA") &&
                    !LectorCodigoDeBarras.RutFormateado.Equals(PersonData.Rut)) //FormatRutFormateado(_SOLICITUD.Persona.Rut))
                {
                    LectorCodigoDeBarras.DataCaptured -= ProcesarCedulaLeida;
                    _OnErrorEvent(libs.Errors.ERROR_BCR_RUT_ERRONEOUS, "RUT leído diferente al esperado [Leido=" +
                        LectorCodigoDeBarras.RutFormateado + " | Esperado=" + PersonData.Rut);
                    _IS_QR_READED = false;
                }
                else
                {
                    LOG.Debug("BviAtm.ProcesarCedulaLeida - Cédula leída corresponde a rut de la solicitud");

                    if (LectorCodigoDeBarras.Pdf417)
                    {
                        PersonData.TipoIdentificacion = TipoIdentificacion.CedulaChilenaAntigua;
                        LOG.Warn("BviAtm.ProcesarCedulaLeida - Codigo de barras leído no es de Cédula Nueva");
                        _OnErrorEvent(libs.Errors.ERROR_BCR_IS_OLD_IDCARD, "Codigo de barras leído no es de Cédula Nueva");
                        _IS_QR_READED = false;
                    } else
                    {
                        LOG.Debug("BviAtm.ProcesarCedulaLeida - Copio datos a PersonData...");
                        PersonData.Rut = LectorCodigoDeBarras.RutFormateado;
                        LOG.Debug("BviAtm.ProcesarCedulaLeida -  RUT LEido desde QR => PersonData.Rut = " +
                                    (PersonData != null && !string.IsNullOrEmpty(PersonData.Rut) ?
                                        PersonData.Rut.ToString() : "NULL"));  
                        PersonData.Apellido = LectorCodigoDeBarras.LastName;
                        PersonData.FechaExpiracion = LectorCodigoDeBarras.ExpirationDate;
                        PersonData.IsCedulaVigente = libs.Utils.IsCedulaVigente(PersonData.FechaExpiracion.Value);
                        PersonData.FechaNacimiento = LectorCodigoDeBarras.BirthDate;
                        PersonData.IsMenorDeEdad = libs.Utils.IsMenorDe18(PersonData.FechaNacimiento.Value);
                        _DOB_CURRENT = LectorCodigoDeBarras.DOB;
                        _DOE_CURRENT = LectorCodigoDeBarras.DOE;
                        PersonData.Serie = LectorCodigoDeBarras.DocumentNumber;
                        PersonData.Nacionalidad = LectorCodigoDeBarras.Country;

                        if (_BLOQUED_EXPIRATION_ENABLED && !PersonData.IsCedulaVigente)
                        {
                            _IS_QR_READED = false;
                            LOG.Debug("BviAtm.ProcesarCedulaLeida - Datos copiados!");
                            _OnErrorEvent(libs.Errors.ERROR_BCR_IS_DOCUMENT_EXPIRATED, 
                                          "La cedula utilizada está vencida [Fecha Actual=" + DateTime.Now.ToString("dd/MM/yyyy") + 
                                          " y la Fecha Leida=" + 
                                          (PersonData.FechaExpiracion.HasValue ? 
                                                PersonData.FechaExpiracion.Value.ToString("dd/MM/yyyy") : "Null") + "]");
                        }
                        else
                        {
                            _IS_QR_READED = true;
                            LOG.Debug("BviAtm.ProcesarCedulaLeida - Datos copiados!");
                            _OnReadBarcodeEvent(Errors.ERROR_OK, "No Error", LectorCodigoDeBarras.RutFormateado);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _OnErrorEvent(libs.Errors.ERROR_UNKKNOW, "Excp Error Parseando Cédula [" + ex.Message + "]");
                _IS_QR_READED = false;
                LOG.Error("BviAtm.ProcesarCedulaLeida - Error al capturar los datos desde lector de códigos de barras", ex);
            }
            try
            {
                LectorCodigoDeBarras.Close();
                LectorCodigoDeBarras = null; //Para liberar recursos
            }
            catch (Exception ex)
            {
                LOG.Warn("BviAtm.ProcesarCedulaLeida Excp Error: ", ex);
            }
            LOG.Debug("BviAtm.ProcesarCedulaLeida OUT!");
        }

        #endregion Barcode

        #region BSP - GetSample

        /// <summary>
        /// Inicia la captura de la huella dactilar, considerando el tipo y serial del lector configurado, y
        /// el timeout.
        /// <para>Cuando termina la captura se lanza el evento de captura exitosa.</para>
        /// <para>Si se produyce un error, se lanza evento de error.</para>
        /// <para>Si se cumple el timeout definido, se lanza el evento de Timeout.</para>
        /// </summary>
        /// <param name="msgerr">Descripción del error si existió</param>
        /// <returns>Valor de retornp 0 significa que funcionó todo bien, mientras que si es negativo, es un error 
        /// (Ver tabla de Errores posibles en libs.Errors)</returns>
        public int GetSample(out string msgerr)
        {
            msgerr = null;
            int ret = 0;
            try
            {
                LOG.Info("BviAtm.GetSample IN => readerserial = " + _CONFIG["SerialSensor"]);
                if (!IsInitialized)
                {
                    msgerr = "Componente no inicializada!";
                    return Errors.ERROR_NOT_INITIALIZED;
                }

                ClearPersonData(2); //Elimina solo datos de samples
                _CurrentSamplesCaptured = null;

                LOG.Debug("BviAtm.GetSample - Create BSP...");
                _BSP = new Biometrika.BioApi20.BSP.BSPBiometrika();
                LOG.Debug("BviAtm.GetSample - _BSP != null => " + (_BSP != null).ToString());
                if (_BSP == null) //Sale
                {
                    msgerr = "Error creando BSP [BSP=null]";
                    return Errors.ERROR_BSP_ATTACH;
                }

                LOG.Debug("BviAtm.GetSample - Attaching BSP...");
                _BSP.BSPAttach("2.0", true);
                ret = _BSP.IsLoaded ? 0 : Errors.ERROR_BSP_ATTACH;
                LOG.Debug("BviAtm.GetSample BSP Loaded = " + ret.ToString());

                //Si dio error en la carga o bien no hay lector reconocido => Sale
                if (ret < 0 || (_BSP.SENSOR_FACTORY.htSensor != null && _BSP.SENSOR_FACTORY.htSensor.Count == 0)) //Sale
                {
                    string aux = _BSP.SENSOR_FACTORY.htSensor != null ? _BSP.SENSOR_FACTORY.htSensor.Count.ToString() : "Null"; 
                    msgerr = "Error atachando BSP [ret=" + ret.ToString() + " - Q Lectores: =" + aux + "]";
                    return Errors.ERROR_BSP_ATTACH;
                }

                _BSP.OnCapturedEvent += OnCaptureEvent;
                _BSP.OnTimeoutEvent += OnTimeoutEvent;

                int sensortype = _CONFIG.ContainsKey("SensorType") ? Convert.ToInt32(_CONFIG["SensorType"]) : 1;
                string serial = null;
                if (_CONFIG.ContainsKey("SerialSensor"))
                {
                    serial = _CONFIG["SerialSensor"];
                } else
                {
                    LOG.Fatal("BviAtm.GetSample - Serial Nulo!");
                    msgerr = "Error GetSample => Serial Nulo!";
                    return Errors.ERROR_INITIALIZING;
                }
                int timeout = _CONFIG.ContainsKey("FingerTimeout") ? Convert.ToInt32(_CONFIG["FingerTimeout"]) : 5000; 
                int quality = _CONFIG.ContainsKey("QualityThreshold") ? Convert.ToInt32(_CONFIG["QualityThreshold"]) : 50; ;
                int fingerId = 1;
                Error err;
                LOG.Debug("BviAtm.GetSample - Call _BSP.Capture... => _BSP Instanciado = " + (_BSP!=null).ToString());
                _CurrentSamplesCaptured = _BSP.Capture(sensortype, fingerId, serial, timeout, 0, quality, out err);

            }
            catch (Exception ex)
            {
                _OnErrorEvent(libs.Errors.ERROR_UNKKNOW, "Excp Error GetSample [" + ex.Message + "]");
                _IS_SAMPLE_OK = false;
                LOG.Error("BviAtm.GetSample - Excp Error GetSample", ex);
            }
            return ret;
        }

        private void OnCaptureEvent(int errCode, string errMessage, List<Sample> samplesCaptured)
        {
            try
            {
                LOG.Debug("BviAtm.OnCaptureEvent IN...");
                _CurrentSamplesCaptured = samplesCaptured;
                string aux = "";

                int idx = 0;
                LOG.Debug("BviAtm.OnCaptureEvent - Comienza proceso de samplesCaptured => Q = " +
                            (samplesCaptured==null?"Null":samplesCaptured.Count.ToString()));
                foreach (Sample item in samplesCaptured)
                {
                    if (item.MinutiaeType == 13 && item.Operation == 0)  //Solo proceso los de verify
                    {
                        LOG.Debug("BviAtm.OnCaptureEvent - Set ANSI...");
                        PersonData.Ansi = Convert.ToBase64String((byte[])item.Data);
                    }
                    if (item.MinutiaeType == 14 && item.Operation == 0)  //Solo proceso los de verify
                    {
                        LOG.Debug("BviAtm.OnCaptureEvent - Set ISO...");
                        PersonData.Iso = Convert.ToBase64String((byte[])item.Data);
                    }
                    if (item.MinutiaeType == 22)
                    {
                        LOG.Debug("BviAtm.OnCaptureEvent - Set RAW...");
                        PersonData.Raw = Convert.ToBase64String((byte[])item.Data);
                    }
                    if (item.MinutiaeType == 21)  //Solo proceso los de verify
                    {
                        LOG.Debug("BviAtm.OnCaptureEvent - Set WSQ...");
                        PersonData.Wsq = Convert.ToBase64String((byte[])item.Data);
                    }
                    if (item.MinutiaeType == 15 && item.Operation == 0)  //Solo proceso los de verify
                    {
                        //PersonData.IsoCompact = Utils.ConvertToHexa((byte[])item.Data); // Convert.ToBase64String((byte[])item.Data);
                    } else
                    {
                        //PersonData.IsoCompact = ISOCCHelper.ConvertISOtoISOCC(PersonData.Iso);
                    }
                    //if (item.MinutiaeType == 41)
                    //{
                    //    PersonData.JpegOriginal = Convert.ToBase64String((byte[])item.Data);
                    //}
                    
                }
                if (samplesCaptured[samplesCaptured.Count - 2].MinutiaeType == 41)
                {
                    LOG.Debug("BviAtm.OnCaptureEvent - Set JPG...");
                    PersonData.JpegOriginal = ImageProcessor.GetBase64Image((Bitmap)(samplesCaptured[samplesCaptured.Count - 2]).Data);
                }
                if (PersonData.Iso != null)
                {
                    string msgerr;
                    PersonData.IsoCompact = ISOCCHelper.ISOToISOCC(PersonData.Iso, _CONFIG["pathlic3Pty"], out msgerr);
                        //Biometrika.BVIOem.Atm.Helpers.ISOCCHelper.ConvertISOtoISOCC(PersonData.Raw, out msgerr);
                    if (PersonData.IsoCompact == null || PersonData.IsoCompact.Length == 0) 
                    {
                        LOG.Fatal("BviAtm.OnCaptureEvent - IsoCC no generado! (Null)");
                        _IS_SAMPLE_OK = false;
                        _OnErrorEvent(Errors.ERROR_BSP_EXTRACTING, "Error extrayendo minucias para comparar [" + 
                                        (string.IsNullOrEmpty(msgerr)?"":msgerr) + "]");
                    } else
                    {
                        LOG.Debug("BviAtm.OnCaptureEvent - IsoCC generado con exito!");
                        _OnCapturedEvent(0, "Capture OK!", PersonData); // samplesCaptured);
                        _IS_SAMPLE_OK = true;
                    }
                }       

            }
            catch (Exception ex)
            {
                _OnErrorEvent(Errors.ERROR_UNKKNOW, "Excp OnCaptureEvent [" + ex.Message + "]");
                LOG.Error("BviAtm.OnCaptureEvent Error Ex - " + ex.Message);
                _IS_SAMPLE_OK = false;
            }
        }

        private void OnTimeoutEvent(int errCode, string errMessage, ISensor sensor)
        {
            LOG.Debug("BviAtm.OnTimeoutEvent Throw!");
            _OnTimeoutEvent(Errors.ERROR_BSP_TIMEOUT, "Timeout de captura de huella [" +
                            errCode.ToString() + "-" + errMessage + "]");
        }

        #endregion BSP - GetSample

        #region Verify MoC

        /// <summary>
        /// Método que realiza la verificación Match On Card (MoC).
        /// <para>En el momento que se ejecuta este método, se debe haber ejecutado correctamente la 
        /// lectura del QR y la captura de la huella dactilar. Si alguno de los pasos no se ejecutó,
        /// se retorna el error y se lanza el evento de error.</para>
        /// <para>Si se procesa correctametne el proceso de verificación, ya sea que haya sido positivo o 
        /// negativo el resultado, se lanza el evento de Verificación, indicando el resultado.</para>
        /// <para>El resultado completo del proceso se puede obtener a través del método 
        /// <see cref="BviAtm.GetData">GetData()</see>.</para>
        /// </summary>
        /// <param name="msgerr">Descripción del error si existió</param>
        /// <returns>Valor de retornp 0 significa que funcionó todo bien, mientras que si es negativo, es un error 
        /// (Ver tabla de Errores posibles en libs.Errors)</returns>
        public int Verify(out string msgerr)
        {
            msgerr = null;
            int ret = 0;
            try
            {
                LOG.Info("BviAtm.Verify IN => ReaderProximityName = " + _CONFIG["ReaderProximityName"]);
                if (!IsInitialized)
                {
                    msgerr = "Componente no inicializada!";
                    _OnErrorEvent(libs.Errors.ERROR_NOT_INITIALIZED, msgerr);
                    return libs.Errors.ERROR_NOT_INITIALIZED;
                }

                if (!_IS_QR_READED)
                {
                    LOG.Warn("BviAtm.Verify - Falta lectura de BC!");
                    msgerr = "Falta lectura de código de barras!";
                    _OnErrorEvent(libs.Errors.ERROR_VERIFY_PARAM_BC_MISSING, msgerr);
                    return Errors.ERROR_VERIFY_PARAM_BC_MISSING;
                }

                if (!_IS_SAMPLE_OK || PersonData == null || PersonData.IsoCompact == null)
                {
                    LOG.Warn("BviAtm.Verify - Falta lectura de Huella!");
                    msgerr = "Falta lectura de Huella!";
                    _OnErrorEvent(libs.Errors.ERROR_VERIFY_PARAM_SAMPLE_MISSING, msgerr);
                    return Errors.ERROR_VERIFY_PARAM_SAMPLE_MISSING;
                }

                LOG.Info("BviAtm.Verify - Creando CLR...");
                var clr = new Bio.Core.CLR.CLR();

                LOG.Info("BviAtm.Verify - Se inicializa con el lector:" + _CONFIG["ReaderProximityName"]);
                clr.ReaderName = _CONFIG["ReaderProximityName"];
                clr.CLR_OPEN(10);
                Thread.Sleep(100);
                LOG.Debug("BviAtm.Verify - Entrando a CLR_CONNECT");

                clr.CLR_CONNECT(Convert.ToUInt32(5000));

                if (clr.GetLasError() == 0)
                {
                    LOG.Debug("BviAtm.Verify - Comenzando Verificación contra cédula nueva de RUT " + PersonData.Rut);
                    //string s = "";
                    //for (int i = 0; i < _PERSONA.IsoCompact.Length; i++)
                    //{
                    //    s += _PERSONA.IsoCompact[i];
                    //}
                    //System.IO.File.WriteAllText(@"c:\tmp\ggs_7_isocompact.txt", s);

                    //Esto se agrego solo para pruebas manales de nueva version 2020. 
                    //Dejarlo para posteriores pruebas si es necesario.
                    //_PERSONA.IsoCompact = ReadIsoCC(@"C:\tmp\TestNewMOC\GetSample\data\2.txt");

                    //                    bReturnMatch = clr.Match(_PERSONA.IsoCompact, out finger);
                    int finger;
                    LOG.Debug("BviAtm.Verify - Calling Match...");
                    bool bReturnMatch = clr.Match2020(PersonData.IsoCompact, out finger);

                    PersonData.VerifyResult = bReturnMatch ? 1 : 2;
                    PersonData.Score = bReturnMatch ? 100 : 0;
                    PersonData.Finger = bReturnMatch ? finger : 0;

                    if (!CheckIntegrity(out msgerr))
                    {
                        msgerr = "Error de integridad [" + (string.IsNullOrEmpty(msgerr)?"":msgerr) + "]";
                        _OnErrorEvent(libs.Errors.ERROR_VERIFY_INTEGRITY, msgerr);
                        ret = Errors.ERROR_VERIFY_INTEGRITY;
                    } else
                    {
                        LOG.Debug("BviAtm.Verify - CheckIntegrity = true => sigue...");
                        string msgEvOk = "Verificacion Completa sin Error ["
                                   + (bReturnMatch ? "POSITIVA" : "NEGATIVA") + "]";
                        LOG.Debug("BviAtm.Verify - Fire event _OnVerifytEvent(" + Errors.ERROR_OK.ToString() + "," +
                                                   msgEvOk + "," + bReturnMatch.ToString() + ")...!");
                        //_OnVerifyEvent(Errors.ERROR_OK, msgEvOk, bReturnMatch);
                        _OnVerifytEvent(0, msgEvOk, bReturnMatch);
                        LOG.Debug("BviAtm.Verify - Setting ret = 0...");
                        ret = 0; // Errors.ERROR_OK;
                    }
                } else
                {
                    LOG.Debug("BviAtm.Verify - clr.GetLasError() != 0...");
                    msgerr = "Error conectandose al dispositivo de proximidad [" + clr.GetLasError().ToString() + "]";
                    LOG.Debug("BviAtm.Verify - msgerr = " + msgerr + ". Fire EvError...");
                    _OnErrorEvent(libs.Errors.ERROR_VERIFY_CONNECT_READER, msgerr);
                    ret = Errors.ERROR_VERIFY_CONNECT_READER;
                }
            }
            catch (Exception ex)
            {
                _OnErrorEvent(libs.Errors.ERROR_UNKKNOW, "Excp Error Verify [" + ex.Message + "]");
                LOG.Error("BviAtm.Verify - Excp Error Verify", ex);
                ret = Errors.ERROR_UNKKNOW;
            }
            LOG.Debug("BviAtm.Verify OUT! ret = " + ret.ToString());
            return ret;
        }

        private bool CheckIntegrity(out string msgerr)
        {
            bool ret = false;
            msgerr = "";
            string rut_from_chip = null;
            try
            {
                LOG.Debug("BviAtm.CheckIntegrity IN...");
                if (_INTEGRITY_CHECK_ENABLED)
                {
                    if (GetRutFromChip(out rut_from_chip, out msgerr) == 0) {
                        LOG.Debug("RUT Leido QR = " + PersonData.Rut + " <=> Rut Leido de Chip = " + rut_from_chip);
                        ret = rut_from_chip.Equals(PersonData.Rut);
                    } else
                    {
                        ret = false;
                        msgerr = "Error GetRutFromChip [msgerr=" + msgerr + "]";
                        LOG.Error("BviAtm.CheckIntegrity - " + msgerr);
                    }
                } else
                {
                    LOG.Debug("BviAtm.CheckIntegrity - Integridad deshabilitada!");
                    ret = true;
                } 
            }
            catch (Exception ex)
            {
                ret = false;
                msgerr = "Excp Error CheckIntegrity [" + ex.Message + "]";
                LOG.Error("BviAtm.CheckIntegrity - Excp Error CheckIntegrity", ex);
            }
            LOG.Debug("BviAtm.CheckIntegrity OUT! CheckIntegrity ret => " + ret.ToString());
            return ret;
        }


        #endregion Verify MoC


        //public int WizardVerify(string comport, string readerserial, string readername, out string msgerr)
        //{
        //    msgerr = null;
        //    int ret = 0;
        //    try
        //    {
        //        /*
        //            1.- Lee QR via el comport
        //            2.- Lee sample y convierte a ISOCC
        //            3.- Realiza MoC
        //            4.- Retorna
        //        */
        //        //ret = ReadBarcode(comport, out msgerr);
        //        if (ret < 0)
        //        {

        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        //LOG.Error(" Error: " + ex.Message);
        //    }
        //    return ret;
        //}

        #endregion Public Method

        #region Private Method

        private int GetRutFromChip(out string rut, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            rut = null;
            Bio.Core.CLR.CLR clr = null;
            try
            {
                LOG.Info("BviAtm.GetRutFromChip IN!...");

                if (_IS_QR_READED && PersonData != null &&
                    !string.IsNullOrEmpty(_DOB_CURRENT) &&
                    !string.IsNullOrEmpty(_DOE_CURRENT))
                {
                        clr = new Bio.Core.CLR.CLR();
                        LOG.Debug("BviAtm.GetRutFromChip - Se inicializa con el lector:" + _CONFIG["ReaderProximityName"]);
                        clr.ReaderName = _CONFIG["ReaderProximityName"];
                        clr.CLR_OPEN(10);
                        Thread.Sleep(100);
                        LOG.Debug("BviAtm.GetRutFromChip - Conectando al lector...");
                        clr.CLR_CONNECT(Convert.ToUInt32(5000));
                        if (clr.GetLasError() == 0)
                        {
                            LOG.Debug("BviAtm.GetRutFromChip - Iniciando sesion al chip...");
                            clr.CLR_SECURITY(PersonData.Serie,
                                             _DOB_CURRENT,
                                             _DOE_CURRENT
                                            );
                            LOG.Debug("BviAtm.GetRutFromChip - Leyendo data...");
                            clr.CLR_GETMRZ();
                            rut = ((clr != null && clr.DNI != null) ? clr.DNI : null);
                            LOG.Debug("BviAtm.GetRutFromChip - Rut Leido => " + (string.IsNullOrEmpty(rut) ? "NULL" : rut));
                        }
                        else if (clr.GetLasError() == -1)
                        {
                            msgerr = "Error leyendo chip por rut";
                            LOG.Warn("BviAtm.GetRutFromChip - " + msgerr);
                            _OnErrorEvent(Errors.ERROR_CLR_READING, msgerr);
                            return Errors.ERROR_CLR_READING;
                        }
                        LOG.Debug("BviAtm.GetRutFromChip - Desconecta y cierra lector...!");
                        clr.CLR_DISCONNECT();
                        clr.CLR_CLOSE();
                        LOG.Debug("BviAtm.GetRutFromChip - CLR finalizado!");
                }
                else
                {
                    try
                    {
                        LOG.Debug("BviAtm.GetRutFromChip - Cerrando CLR por error");
                        if (clr != null)
                        {
                            clr.CLR_DISCONNECT();
                            clr.CLR_CLOSE();
                            clr = null;
                        }
                        LOG.Debug("BviAtm.GetRutFromChip - CLR Resuorces libarados ok!");
                    }
                    catch (Exception ex1)
                    {
                        clr = null;
                        LOG.Debug("BviAtm.GetRutFromChip - CLR Resuorces libarados con excp [" + ex1.Message + "]");
                    }
                    msgerr = "Pide datos desde chip pero no leyo el QR => No se puede acceder ala info!";
                    LOG.Warn("BviAtm.GetRutFromChip - " + msgerr);
                    _OnErrorEvent(Errors.ERROR_VERIFY_PARAM_BC_MISSING, msgerr);
                    return Errors.ERROR_VERIFY_PARAM_BC_MISSING;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.ERROR_UNKKNOW;
                msgerr = "Excp Error rn GetData [" + ex.Message + "]";
                LOG.Error("BviAtm.GetData - " + msgerr);
                _OnErrorEvent(Errors.ERROR_UNKKNOW, msgerr);
            }
            LOG.Debug("BviAtm.GetData - OUT!");
            return ret;
        }

        private void ClearPersonData(int flag)
        {
            try
            {
                LOG.Debug("BviAtm.ClearPersonData IN! flag = " + flag.ToString());
                switch (flag)
                {
                    case 1: //QR
                        PersonData.Apellido = null;
                        PersonData.FechaExpiracion = DateTime.MinValue;
                        PersonData.FechaNacimiento = DateTime.MinValue;
                        PersonData.Foto = null;
                        PersonData.FotoFirma = null;
                        PersonData.IsCedulaVigente = false;
                        PersonData.IsMenorDeEdad = false;
                        PersonData.Nacionalidad = null;
                        PersonData.Nombre = null;
                        PersonData.Serie = null;
                        PersonData.Sexo = null;
                        PersonData.TipoIdentificacion = TipoIdentificacion.Indeterminado;
                        PersonData.Score = 0;
                        PersonData.VerifyResult = 0;
                        _DOB_CURRENT = null;
                        _DOE_CURRENT = null;
                        break;
                    case 2: //Samples
                        PersonData.Ansi = null; //Todo el resto
                        PersonData.Iso = null;
                        PersonData.Raw = null;
                        PersonData.Wsq = null;
                        PersonData.IsoCompact = null;
                        PersonData.JpegOriginal = null;
                        PersonData.Score = 0;
                        PersonData.VerifyResult = 0;
                        break;

                    default: //0-Todo
                        string aux = PersonData != null ? PersonData.Rut : "NA";
                        PersonData = new PersonData();
                        PersonData.Rut = aux;
                        break;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BviAtm.ClearPersonData Error Excp: ", ex);
            }
            LOG.Debug("BviAtm.ClearPersonData OUT!");
        }

        //internal static BKLicense oBKLic;
        internal static string _PATH_LICENSE = null;
        internal static bool _IS_LICENSE_OK = false;
        internal static Thread _Thread_License_BviOemAtm = null;

        /// <summary>
        /// Chequea licnecia dado el path, y lanza un thread para que se chequee cada 1 hora.
        /// </summary>
        /// <param name="pathLicense">Path del archivo de licencia</param>
        internal bool CheckLicense(string pathLicense)
        {
            bool result = true;
            try
            {
                LOG.Info("BviAtm.CheckLicense IN => - CheckLicense In...");
                //LicenseCheck oCheck = new LicenseCheck(); //BioLicenciaCheckClass();
                //_PATH_LICENSE = pathLicense;
                //string msgErr = null;
                //string sCodigo = "BVIOEMATM7";
                //string AttrAux;
                //int iret = oCheck.CheckLicense(_PATH_LICENSE, sCodigo, 3600000, out oBKLic, out msgErr);
                //if (iret != 0 || oBKLic == null || !oBKLic.Status)
                //{
                //    if (oBKLic == null)
                //    {
                //        LOG.Fatal("BviAtm.CheckLicense - Objeto de licencia nulo");
                //    }
                //    else if (!oBKLic.Status)
                //    {
                //        LOG.Fatal("BviAtm.CheckLicense - Licencia Inválida! [" + msgErr + "]");
                //    }
                //    result = false;
                //}
                //else
                //{
                //    LOG.Info("BviAtm.CheckLicense - BioPortal Check License OK!");
                //    result = true;
                //}

                //if (_Thread_License_BviOemAtm == null || _Thread_License_BviOemAtm.IsAlive)
                //{
                //    LOG.Info("BviAtm.CheckLicense - Set Monitor License...!");
                //    ////Lanzo thread para chequeao cada 60 minutos =>>>> esto lo elimino porque la nueva lib de licencia lo hace solo
                //    _Thread_License_BviOemAtm = new Thread(new ThreadStart(ThreadCheckLicenseBVIOEMATM));
                //    _Thread_License_BviOemAtm.Start();
                //    LOG.Info("BviAtm.CheckLicense - Monitor License OK!");
                //}
                LOG.Info("BviAtm.CheckLicense - CheckLicense Out!");
            }
            catch (Exception ex)
            {
                LOG.Error("BviAtm.CheckLicense Excp Err", ex);
            }
            return result;
        }

        internal void ThreadCheckLicenseBVIOEMATM()
        {
            bool bRunning = true;
            bool isFirst = true;
            try
            {
                while (bRunning)
                {
                    if (!isFirst)
                    {
                        //Chequeo licencia, y si da falso, entonces sale del loop
                        bRunning = CheckLicense(_PATH_LICENSE);
                    } 

                    LOG.Info("BviAtm.CheckLicense [T] - " +
                                "Chequeó licencia en thread [" +
                                bRunning.ToString() + "]");
                    //Duerme una hora y rechequea
                    //(1000 miliseg x 60 seg x 60 minutos =
                    //				 3600000 milisegundos = 1 hora)
                    Thread.Sleep(3600000);
                    //Thread.Sleep(10000);
                    isFirst = false;
                    //Para test uso 1 minuto
                    //Thread.Sleep(60000);
                }

            }
            catch (Exception ex)
            {
                LOG.Error("BviAtm.CheckLicense [T] - " + ex.Message);
            }
        }

        #endregion PrivateMethod

    }
}
