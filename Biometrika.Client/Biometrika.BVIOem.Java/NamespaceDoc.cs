﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BVIOem.Java
{
    /// <summary>
    /// Componente de verificación de identidad, a través de huella dactilar, entre captura realizada
    /// en lector Digital Persona o Secugen, y la cédula de identidad Chilena Nueva.
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    class NamespaceDoc
    {
    }
}

