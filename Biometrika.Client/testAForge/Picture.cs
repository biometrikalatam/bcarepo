﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testAForge
{
    public class Picture
    {
        public string Base64ImageString { get; set; }
        public string Format { get; set; }
        public int LightIndex { get; set; }
        public int PageIndex { get; set; }

        public static string ConvertImageToBase64String(Image img)
        {
            string base64String;

            using (var ms = new MemoryStream())
            {
                img.Save(ms, ImageFormat.Jpeg);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                base64String = Convert.ToBase64String(imageBytes);
            }

            return base64String;
        }
    }
}
