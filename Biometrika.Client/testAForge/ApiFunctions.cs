﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace testAForge
{
    public class ApiFunctions
    {
        private static string Token;
        public static bool IsLoggedIn;

        public static bool Authenticate(string userName, string password)
        {
            userName = "testuser";
            password = "Regul@SdkTest";
            using (var client = new HttpClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                //ConfigurationManager.RefreshSection("appSettings");
                var userData = new UserData() { UserId = userName, Password = password };
                var url = "http://localhost:8090/webapi/" + "Authentication/Authenticate"; //ConfigurationManager.AppSettings["serviceUrl"] + "Authentication/Authenticate";
                var response = client.PostAsJsonAsync(url, userData).Result;
                if (response.IsSuccessStatusCode)
                {
                    Token = response.Headers.GetValues("X-Token").FirstOrDefault();
                    if (!string.IsNullOrWhiteSpace(Token))
                        return IsLoggedIn = true;
                }
            }
            return false;
        }

        public static string SubmitTransaction(List<Picture> pictureList, int capabilities, int authenticity)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("X-Token", Token);
                var response =
                    client.PostAsJsonAsync("http://localhost:8090/webapi/" + 
                                        "Transaction2/SubmitTransaction?capabilities=" + capabilities +
                                       "&authenticity=" + authenticity, pictureList).Result;
                //                    client.PostAsJsonAsync(ConfigurationManager.AppSettings["serviceUrl"] + "Transaction2/SubmitTransaction?capabilities=" + capabilities +
                //                                           "&authenticity=" + authenticity, pictureList).Result;
                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsStringAsync().Result;
                else
                    CheckIfAuthenticated(response);
            }
            return null;
        }

        public static string GetTransactionStatus(string transactionId)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("X-Token", Token);
                var response =
                    client.GetAsync("http://localhost:8090/webapi/" + "Transaction2/GetTransactionStatus?transactionId=" + transactionId).Result;
//                client.GetAsync(ConfigurationManager.AppSettings["serviceUrl"] + "Transaction2/GetTransactionStatus?transactionId=" + transactionId).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jObject = Newtonsoft.Json.Linq.JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    return jObject.GetValue("Status").ToString();
                }
                else
                    CheckIfAuthenticated(response);

            }
            return null;
        }

        public static string GetTransactionResult(string transactionId, int resultType)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("X-Token", Token);
                var response =
                    client.GetAsync("http://localhost:8090/webapi/" + "Transaction2/GetTransactionResult?transactionId=" + transactionId + "&resultType=" + resultType).Result;
//                client.GetAsync(ConfigurationManager.AppSettings["serviceUrl"] + "Transaction2/GetTransactionResult?transactionId=" + transactionId + "&resultType=" + resultType).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    return result.Equals("null") ? null : result;
                }
                else
                    CheckIfAuthenticated(response);
            }
            return null;
        }

        private static void CheckIfAuthenticated(HttpResponseMessage message)
        {
            if (message.StatusCode == HttpStatusCode.Unauthorized)
            {
                Token = string.Empty;
                IsLoggedIn = false;
            }
        }

        private class UserData
        {
            public string UserId { get; set; }
            public string Password { get; set; }
        }
    }
}
