﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testAForge
{
    public partial class ProgressForm : Form
    {
        public ProgressForm()
        {
            InitializeComponent();
        }

        public void SetMessage(string message)
        {
            if (label1.InvokeRequired)
            {
                label1.Invoke(new MethodInvoker(() => label1.Text = message));
            }
            else
                label1.Text = message;
        }

        private void ProgressForm_Load(object sender, EventArgs e)
        {
            CenterToParent();
        }
    }
}
