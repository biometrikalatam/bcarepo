﻿using BarcodeLib.BarcodeReader;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZXing;

namespace testAForge
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            testCBRead(); 
                
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        private static void testCBRead()
        {
            try
            {
                String[] datas = BarcodeLib.BarcodeReader.BarcodeReader.read(@"D:\tmp\Cedulas\Cedula_072012_0002.jpg",
                                            BarcodeLib.BarcodeReader.BarcodeReader.PDF417);


                int i = 0;

                ZXing.PDF417.PDF417Reader pdfreader = new ZXing.PDF417.PDF417Reader();
                //var pdfreader = new BarcodeReader();
                Bitmap bmp = (Bitmap)Image.FromFile(@"D:\tmp\Cedulas\Cedula_072012_0002.jpg");
                using (bmp)
                {
                    ZXing.LuminanceSource source;
                    source = new ZXing.BitmapLuminanceSource(bmp);
                    ZXing.BinaryBitmap bitmap = new ZXing.BinaryBitmap(new ZXing.Common.HybridBinarizer(source));
                    Result result = new ZXing.MultiFormatReader().decode(bitmap);

                    int ii = 0;
                }
                //OptimizeSetting setting = new OptimizeSetting();

                //setting.setMaxOneBarcodePerPage(true);

                //ScanArea top20 = new ScanArea(new PointF(0.0F, 0.0F), new PointF(100.0F, 20.0F));

                //ScanArea bottom20 = new ScanArea(new PointF(0.0F, 80.0F), new PointF(100.0F, 100.0F));

                //List<ScanArea> areas = new List<ScanArea>();
                //areas.Add(top20);
                //areas.Add(bottom20);

                //setting.setAreas(areas);

                //string[] results = BarcodeReader.read(@"D:\tmp\Cedulas\Cedula_072012_0002.jpg", BarcodeReader.PDF417, setting);
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }
    }
}
