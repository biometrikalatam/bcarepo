﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge;
using AForge.Imaging.Filters;
using AForge.Video;
using AForge.Video.DirectShow;
using DirectShowLib;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Pdf417DecoderLibrary;
using ZXing;

namespace testAForge
{
    public partial class Form1 : Form
    {
        bool _CAPTURE_ENABLED = true;
        Image _ImageToRecognition;
        private Pdf417Decoder Pdf417Decoder;
        private Bitmap Pdf417InputImage;
        private Rectangle ImageArea = new Rectangle();

        private static string _currentId;
        private static readonly BackgroundWorker bgWorker = new BackgroundWorker() { WorkerReportsProgress = true };
        private static ProgressForm progressForm = new ProgressForm();
        private static bool success = false;


        FilterInfoCollection _WEBCAMS;
        VideoCaptureDevice _CURRENT_WEBCAM;

        public Form1()
        {
            InitializeComponent();
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _WEBCAMS = new FilterInfoCollection(AForge.Video.DirectShow.FilterCategory.VideoInputDevice);
            foreach (AForge.Video.DirectShow.FilterInfo webcam in _WEBCAMS)
            {
                comboBox1.Items.Add(webcam.Name);
            }
            comboBox1.SelectedIndex = 0;
            textBox1.Text = _WEBCAMS[comboBox1.SelectedIndex].Name; // ((string)(comboBox1.Items[0])).Split('=')[2];

            bgWorker.DoWork += bgWorker_DoWork;
            bgWorker.ProgressChanged += bgWorker_ProgressChanged;
            bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;

            Pdf417Decoder = new Pdf417Decoder();

            //if (!ApiFunctions.IsLoggedIn)
            //{
            //    ApiFunctions.IsLoggedIn = ApiFunctions.Authenticate("testuser", "Regul@SdkTest");
            //    if (!ApiFunctions.IsLoggedIn) return;
            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _CURRENT_WEBCAM = new VideoCaptureDevice(_WEBCAMS[comboBox1.SelectedIndex].MonikerString);
            _CURRENT_WEBCAM.VideoResolution = _CURRENT_WEBCAM.VideoCapabilities.OrderByDescending(vc => vc.FrameSize.Width).First();
            _CURRENT_WEBCAM.NewFrame += new NewFrameEventHandler(NewFrameEvent);

            //videoCaptureDevice.Start();
            //_CURRENT_WEBCAM = new VideoCaptureDevice(_WEBCAMS[comboBox1.SelectedIndex].MonikerString);
            //_CURRENT_WEBCAM.NewFrame += new NewFrameEventHandler(NewFrameEvent);
            _CURRENT_WEBCAM.Start();
        }

        Image imagForCapture;
        private void NewFrameEvent(object sender, NewFrameEventArgs eventArgs)
        {
            try
            {
               
                //pictureBox1.Image = (Bitmap)eventArgs.Frame.Clone();
                if (_CAPTURE_ENABLED)
                {
                    if (pictureBox1.Image != null)
                    {
                        pictureBox1.Image.Dispose();
                    }
                
                    if (pictureBox3.Image != null)
                    {
                        pictureBox3.Image.Dispose();
                    }
                    //Bitmap frame = (Bitmap)eventArgs.Frame.Clone();
                    //_ImageToRecognition?.Dispose();
                    //_ImageToRecognition = frame.Clone(new Rectangle(0, 0, frame.Width, frame.Height), frame.PixelFormat);

                    //var resizedFrame = new ResizeNearestNeighbor(1920, 1080).Apply(_ImageToRecognition);
                    //panelGraphics.DrawImage(resizedFrame, 0, 0);
                    //resizedFrame.Dispose();
                    Invoke(new Action<Bitmap>(ShowFrame), eventArgs.Frame.Clone());
                }
                
                
            }
            catch (Exception ex)
            {

            }
        }

        private delegate void DisplayImageDelegate(Bitmap Image);
        private void DisplayImage(Bitmap Image)
        {
            if (pictureBox1.InvokeRequired)
            {
                try
                {
                    DisplayImageDelegate DI = new DisplayImageDelegate(DisplayImage);
                    this.BeginInvoke(DI, new object[] { Image });
                }
                catch (Exception ex)
                {
                }
            }
            else
            {
                pictureBox1.Image = Image;
            }
        }

        private void ShowFrame(Bitmap frame)
        {
            try
            {
                if (pictureBox1.InvokeRequired)
                {
                    try
                    {
                        DisplayImageDelegate DI = new DisplayImageDelegate(DisplayImage);
                        this.BeginInvoke(DI, new object[] { frame });
                    }
                    catch (Exception ex)
                    {
                    }
                }
                else
                {
                    pictureBox1.Image = frame;
                }
                //    if (pictureBox1.Width < frame.Width)
                //    {
                //        pictureBox1.Width = frame.Width;
                //    }
                //    if (pictureBox1.Height < frame.Height)
                //    {
                //        pictureBox1.Height = frame.Height;
                //    }
            //pictureBox1.Image = frame;
            //pictureBox3.Image = frame;
                //_ImageToRecognition = frame;
                //((Bitmap)pictureBox1.Image).Save(@"c:\tmp\cedulas_dorso_ggs_" + DateTime.Now.ToString("yyyyMMddHHmmssmmm") + ".jpg",
                //           System.Drawing.Imaging.ImageFormat.Jpeg);

            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
                    }

        private void button2_Click(object sender, EventArgs e)
        {
            if (_CURRENT_WEBCAM != null && _CURRENT_WEBCAM.IsRunning)
            {
                _CURRENT_WEBCAM.Stop();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            try
            {
                //if (_CURRENT_WEBCAM != null && _CURRENT_WEBCAM.IsRunning)
                //{
                //    _CURRENT_WEBCAM.Stop();
                //}
                pictureBox2.Image = (Image)pictureBox1.Image.Clone();
                _ImageToRecognition = (Image)pictureBox3.Image.Clone();
                _CAPTURE_ENABLED = false;
                button8.Enabled = true;
                button8.Refresh();
                //button2_Click(null,null);
            }
            catch (Exception ex)
            {
                richTextBox1.Text = "Fallo captura => " + ex.Message;
                //LOG.Error(" Error: " + ex.Message);
            }
            //if (_CURRENT_WEBCAM.IsRunning) _CURRENT_WEBCAM.Stop();
            //if (!ApiFunctions.IsLoggedIn)
            //{
            //    ApiFunctions.IsLoggedIn = ApiFunctions.Authenticate("testuser", "Regul@SdkTest");
            //    if (!ApiFunctions.IsLoggedIn) return;
            //}
            //var imgList = new List<Picture>();
            //var pic = new Picture();
            //pic.Format = ".jpg";
            //pic.LightIndex = 6;
            //pic.PageIndex = 0;
            //pic.Base64ImageString = Picture.ConvertImageToBase64String(pictureBox2.Image); //GetB64FromPictureBox(pictureBox2);//Convert.ToBase64String(File.ReadAllBytes(file));
            //imgList.Add(pic);

            


            //_currentId = ApiFunctions.SubmitTransaction(imgList, 192, 0);
            //if (!string.IsNullOrWhiteSpace(_currentId))
            //{
            //    _currentId = _currentId.Replace(@"\", string.Empty);
            //    _currentId = _currentId.Replace("\"", string.Empty);
            //    timer2.Enabled = true;
            //    //progressForm.ShowDialog(this);
            //    //progressForm.SetMessage("Submitted");
            //}
            //if (_CURRENT_WEBCAM.IsRunning) _CURRENT_WEBCAM.Stop();
        }

        private string DecodePDF417(Image img)
        {
            try
            {
                richTextBox1.Text = "Procesing...";
                // load image to bitmap
                Pdf417InputImage = new Bitmap(img);

                // decode barcodes
                int BarcodesCount = Pdf417Decoder.Decode(Pdf417InputImage);

                // no barcodes were found
                if (BarcodesCount == 0)
                {
                    // clear barcode data text box 
                    richTextBox1.Text = "No encontro CB!"; 
                }

                // one barcodes was found
                else if (BarcodesCount == 1)
                {
                    // decoding was successful
                    // convert binary data to text string
                    richTextBox1.Text = Convert.ToBase64String(Pdf417Decoder.BarcodesInfo[0].BarcodeData);
                }

                // more than one barcode
                else
                {
                    richTextBox1.Text = "Muchos"; //Convert.ToBase64String(Pdf417Decoder.BarcodesInfo[0].BarcodeData);
                    //StringBuilder Str = new StringBuilder();

                    //for (int Count = 0; Count < BarcodesCount; Count++)
                    //{
                    //    Str.AppendFormat("Barcode No. {0}\r\n{1}\r\n", Count + 1, Pdf417Decoder.BinaryDataToString(Count));
                    //}
                    //DataTextBox.Text = Str.ToString();
                }
            }
            catch (Exception ex)
            {

                richTextBox1.Text = "Decode Ex => " + ex.Message; 
            }
            return richTextBox1.Text.Trim();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            BarcodeReader Reader = new BarcodeReader();
            Result result = Reader.Decode((Bitmap)pictureBox1.Image);
            try
            {
                //((Bitmap)pictureBox1.Image).Save(@"c:\tmp\cedulas_dorso_ggs_" + DateTime.Now.ToString("yyyyMMddHHmmssmmm") + ".jpg",
                //       System.Drawing.Imaging.ImageFormat.Jpeg);
                string decoded = result.ToString().Trim();
                if (decoded != "")
                {
                    timer1.Stop();
                    MessageBox.Show(decoded);
                    //Form2 form = new Form2();
                    //form.Show();
                    //this.Hide();

                }
            }
            catch (Exception ex)
            {

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            timer1.Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_CURRENT_WEBCAM != null && _CURRENT_WEBCAM.IsRunning == true)
            {
                _CURRENT_WEBCAM.Stop();
            }

        }


        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (success)
            {
                var result = ApiFunctions.GetTransactionResult(_currentId, 15);
                if (!string.IsNullOrWhiteSpace(result))
                {
                    result = result.TrimStart('[').TrimEnd(']').TrimStart('\"').TrimEnd('\"');
                    result = result.Replace("\\n", string.Empty);
                    result = result.Replace("\\", string.Empty);
                    //    var o = new XmlDocument();
                    //    o.LoadXml(result);
                    //    var noList = o.GetElementsByTagName("Document_Field_Analysis_Info");

                    //    for (int i = 0; i < noList.Count; i++)
                    //    {
                    //        var Item = (XmlElement)noList.Item(i);

                    //        int j = dataGridView1.Rows.Add();

                    //        int FieldType = Convert.ToInt32(Item.GetElementsByTagName("FieldType").Item(0).InnerText);
                    //        int LCID = 0;
                    //        if (FieldType > 0xFFFF)
                    //        {
                    //            LCID = FieldType >> 16;
                    //            FieldType = (FieldType << 16) >> 16;
                    //        }
                    //        dataGridView1.Rows[j].Cells[0].Value = eVisualFieldType.GetName(typeof(eVisualFieldType), FieldType);
                    //        if (LCID > 0)
                    //        {
                    //            dataGridView1.Rows[j].Cells[0].Value += string.Format("({0})", LCID);
                    //        }
                    //        dataGridView1.Rows[j].Cells[1].Value = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                    //        switch (Convert.ToInt32(Item.GetElementsByTagName("Matrix1").Item(0).InnerText))
                    //        {
                    //            case 0:
                    //                dataGridView1.Rows[j].Cells[1].Style.ForeColor = Color.Black;
                    //                break;
                    //            case 1:
                    //                dataGridView1.Rows[j].Cells[1].Style.ForeColor = Color.DarkGreen;
                    //                break;
                    //            case 2:
                    //                dataGridView1.Rows[j].Cells[1].Style.ForeColor = Color.Red;
                    //                break;
                    //            default:
                    //                dataGridView1.Rows[j].Cells[1].Style.ForeColor = Color.Black;
                    //                break;
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    dataGridView1.Rows.Add();
                    //    dataGridView1.Rows[0].Cells[0].Value = "No result to show!";
                }
            }
            progressForm.Hide();
            _currentId = string.Empty;
            success = false;
        }

        private void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //if (!progressForm.IsDisposed)
            //    progressForm.SetMessage(e.UserState.ToString());
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var status = ApiFunctions.GetTransactionStatus(_currentId);
            while (status.Equals("1") || status.Equals("2"))
            {
                bgWorker.ReportProgress(0, "In progress");
                status = ApiFunctions.GetTransactionStatus(_currentId);
                Thread.Sleep(100);
            }
            if (status.Equals("4"))
                bgWorker.ReportProgress(0, "Error!");
            else if (status.Equals("3"))
            {
                //bgWorker.ReportProgress(0, "Finished!");
                timer3.Enabled = true;
                success = true;
            }
            else
            {
                if (!ApiFunctions.IsLoggedIn)
                {
                    //dataGridView1.Enabled = false;
                    //browseBtn.Enabled = false;
                    //connectToolStripMenuItem.Enabled = true;
                }
            }
        }

        private string GetB64FromPictureBox(PictureBox pic)
        {
            return ImageToBase64(pic.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        public string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format)
        {
            string base64String = null;
            //LOG.Debug("DocReaderUI.ImageToBase64 IN...");
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    image.Save(ms, format);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    base64String = Convert.ToBase64String(imageBytes);
                    //LOG.Debug("DocReaderUI.ImageToBase64 base64String = " + base64String);
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("DocReaderUI.ImageToBase64 Error = " + ex.Message, ex);
                base64String = null;
            }
            //LOG.Debug("DocReaderUI.ImageToBase64 OUT!");
            return base64String;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Enabled = false;
            var status = ApiFunctions.GetTransactionStatus(_currentId);
            while (status.Equals("1") || status.Equals("2"))
            {
                //bgWorker.ReportProgress(0, "In progress");
                status = ApiFunctions.GetTransactionStatus(_currentId);
                Thread.Sleep(100);
            }
            if (status.Equals("4")) MessageBox.Show("4 - Error");
            //bgWorker.ReportProgress(0, "Error!");
            else if (status.Equals("3"))
            {
                //bgWorker.ReportProgress(0, "Finished!");
                timer3.Enabled = true;
                success = true;
            }
            //if(!bgWorker.IsBusy) bgWorker.RunWorkerAsync();
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            timer3.Enabled = false;
            if (success)
            {
                var result = ApiFunctions.GetTransactionResult(_currentId, 15);
                if (!string.IsNullOrWhiteSpace(result))
                {
                    result = result.TrimStart('[').TrimEnd(']').TrimStart('\"').TrimEnd('\"');
                    result = result.Replace("\\n", string.Empty);
                    result = result.Replace("\\", string.Empty);
                    richTextBox1.Text = result;
                }
            }
            //progressForm.Hide();
            _currentId = string.Empty;
            success = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] byImage = System.IO.File.ReadAllBytes(@"C:\TFSN\Biometrika Client\V7.5\BiometrikaClient\testAForge\bin\Debug\Data\img19012021111132.jpg");

                System.IO.MemoryStream ms = new System.IO.MemoryStream(byImage);
                Image img = Image.FromStream(ms);
                ms.Close();

                // load image to bitmap
                //Bitmap Pdf417InputImage = new Bitmap((Image)img.Clone());
                //Image img = (Image)pictureBox2.Image.Clone(); //_ImageToRecognition; 

                //img.Save(@"C:\TFSN\Biometrika Client\V7.5\BiometrikaClient\testAForge\bin\Debug\Data\" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                string pdf417B64 = DecodePDF417(img);
            }
            catch (Exception ex)
            {
                richTextBox1.Text = "Error decoding pdf417 => " + ex.Message;
                //LOG.Error(" Error: " + ex.Message);
            }
            
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog Dialog = new OpenFileDialog
                {
                    Filter = "Image Files(*.png;*.jpg;*.gif;*.tif)|*.png;*.jpg;*.gif;*.tif;*.bmp)|All files (*.*)|*.*",
                    Title = "Load PDF417 Barcode Image",
                    InitialDirectory = Directory.GetCurrentDirectory(),
                    RestoreDirectory = true,
                    FileName = string.Empty
                };

                // display dialog box
                if (Dialog.ShowDialog() != DialogResult.OK)
                    return;

                // display file name
                int Ptr = Dialog.FileName.LastIndexOf('\\');
                int Ptr1 = Dialog.FileName.LastIndexOf('\\', Ptr - 1);
                //ImageFileLabel.Text = Dialog.FileName.Substring(Ptr1 + 1);

                // dispose previous image
                if (Pdf417InputImage != null)
                    Pdf417InputImage.Dispose();

                // load image to bitmap
                pictureBox2.Image = new Bitmap(Dialog.FileName);
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            _CAPTURE_ENABLED = true;
            button8.Enabled = false;
            button8.Refresh();
        }

        #region Emgu.CV

        private Capture _CAPTURECAM;
        Video_Device[] WebCams; //List containing all the camera available
        //Start
        private void button10_Click(object sender, EventArgs e)
        {
            _CAPTURECAM = new Capture(SelectCamera("Microsoft® LifeCam Cinema(TM)"));
            
            _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 1280);
            _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 720);
            Application.Idle += ShowCamStream;
        }

        private int SelectCamera(string cameraName)
        {
            int index = 0;
           
            try
            {
                //-> Find systems cameras with DirectShow.Net dll
                //thanks to carles lloret
                DsDevice[] _SystemCamereas = DsDevice.GetDevicesOfCat(DirectShowLib.FilterCategory.VideoInputDevice);
                WebCams = new Video_Device[_SystemCamereas.Length];
                for (int i = 0; i < _SystemCamereas.Length; i++)
                {
                    if (_SystemCamereas[i].Name.Equals(cameraName))
                    {
                        index = i;
                        break;
                    }
                    //WebCams[i] = new Video_Device(i, _SystemCamereas[i].Name, _SystemCamereas[i].ClassID); //fill web cam array
                    //Camera_Selection.Items.Add(WebCams[i].ToString());
                }
                //if (Camera_Selection.Items.Count > 0)
                //{
                //    Camera_Selection.SelectedIndex = 0; //Set the selected device the default
                //    captureButton.Enabled = true; //Enable the start
                //}
                //Capture oCapture = new Capture();
                //if (oCapture != null)
                //{
                //    foreach (var item in oCapture.Dev)
                //    {

                //    }
                //}
            }
            catch (Exception ex)
            {
                index = 0;
                //LOG.Error(" Error: " + ex.Message);
            }
            return index;
        }


        //Stop
        private void button9_Click(object sender, EventArgs e)
        {
            StopCamStream();
        }




        private void ShowCamStream(object sender, EventArgs e)
        {
            try
            {
                Mat mat = new Mat();
                _CAPTURECAM.Retrieve(mat);
                Image<Bgr, Byte> img = mat.ToImage<Bgr, Byte>();
                pictureBox1.Image = img.ToBitmap();
            }
            catch (Exception ex)
            {
                //LOG.Error("BVIUI.ShowCamStream Error = " + ex.Message, ex);
            }
        }

        private void StopCamStream()
        {
            //LOG.Info("BVIUI.StopCamStream IN...");
            try
            {
                Application.Idle -= ShowCamStream;
                _CAPTURECAM.Stop();
                _CAPTURECAM.Dispose();
            }
            catch (Exception ex)
            {
                //LOG.Error("BVIUI.StopCamStream Error = " + ex.Message, ex);
            }
            //LOG.Info("BVIUI.StopCamStream OUT!");
        }

        //private delegate void DisplayImageDelegate(Bitmap Image);
        //private void DisplayImage(Bitmap Image)
        //{
        //    if (captureBox.InvokeRequired)
        //    {
        //        try
        //        {
        //            DisplayImageDelegate DI = new DisplayImageDelegate(DisplayImage);
        //            this.BeginInvoke(DI, new object[] { Image });
        //        }
        //        catch (Exception ex)
        //        {
        //        }
        //    }
        //    else
        //    {
        //        captureBox.Image = Image;
        //    }
        //}


        #endregion Emgu.CV

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                pictureBox2.Image = pictureBox1.Image;
            }
            catch (Exception ex)
            {
                //LOG.Error("BVIUI.labCapturaImgDocFront_Click Error = " + ex.Message, ex);
            }
            //LOG.Info("BVIUI.labCapturaImgDocFront_Click OUT!");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog Dialog = new OpenFileDialog
                {
                    Filter = "Image Files(*.png;*.jpg;*.gif;*.tif)|*.png;*.jpg;*.gif;*.tif;*.bmp)|All files (*.*)|*.*",
                    Title = "Load PDF417 Barcode Image",
                    InitialDirectory = @"C:\tmp\a", // Directory.GetCurrentDirectory(),
                    RestoreDirectory = true,
                    FileName = string.Empty
                };

                // display dialog box
                if (Dialog.ShowDialog() != DialogResult.OK)
                    return;

                // display file name
                int Ptr = Dialog.FileName.LastIndexOf('\\');
                int Ptr1 = Dialog.FileName.LastIndexOf('\\', Ptr - 1);
                //ImageFileLabel.Text = Dialog.FileName.Substring(Ptr1 + 1);

                byte[] byImage = System.IO.File.ReadAllBytes(Dialog.FileName);

                System.IO.MemoryStream ms = new System.IO.MemoryStream(byImage);
                Image img = Image.FromStream(ms);
                ms.Close();

                //// dispose previous image
                //if (Pdf417InputImage != null)
                //    Pdf417InputImage.Dispose();

                string pdf417B64 = DecodePDF417(img);
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }
    }

}
    
