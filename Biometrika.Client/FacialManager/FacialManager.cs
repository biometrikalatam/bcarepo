﻿using Domain;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace FacialManager
{
    public class FacialManager : IManager
    {
        
        private bool isOpen = false;
        private Dictionary<string, object> output = new Dictionary<string, object>();
        private string templateToken = string.Empty;

        public FacialManager() { } // ctor de reflextion

        public FacialManager(NameValueCollection queryString)
        {

            if (!isOpen)
            {
                isOpen = true;
                
                FacialUIAdapter.Program.SetPacket(queryString);     // el program recibe el pedido
                FacialUIAdapter.Program.Main();

                Dictionary<string, object> outputUI = FacialUIAdapter.Program.Packet.PacketParamOut;

                // Este param out retorna la imagen de la captura y las minucias

                templateToken = "";
                //// String.Format(
                //          "{0}-septmpl-{1}-septmpl-{2}-septmpl-{3}",
                //          outputUI["TemplateVerify64"],
                //          outputUI["TemplateRegistry64"],
                //          outputUI["TemplateANSI64"],
                //          outputUI["TemplateISO64"]
                //  );
                //output.Add("Token", templateToken); 


                string huellab64;
                if (outputUI != null && outputUI.Count > 0)
                {
                    if (outputUI != null && outputUI.ContainsKey("Huella"))
                    {
                        huellab64 = GetImageB64FromImage(outputUI["Huella"]);
                    }
                    else
                    {
                        huellab64 = "NoHuella";
                    }
                    output.Add("Huella", huellab64);
                    output.Add("TemplateVerify64", outputUI.ContainsKey("TemplateVerify64") ? outputUI["TemplateVerify64"] : null);
                    output.Add("TemplateRegistry64", outputUI.ContainsKey("TemplateRegistry64") ? outputUI["TemplateRegistry64"] : null);
                    output.Add("TemplateANSI64", outputUI.ContainsKey("TemplateANSI64") ? outputUI["TemplateANSI64"] : null);
                    output.Add("TemplateISO64", outputUI.ContainsKey("TemplateISO64") ? outputUI["TemplateISO64"] : null);
                    output.Add("WSQ", outputUI.ContainsKey("WSQ") ? outputUI["WSQ"] : null);

                    output.Add("Error", outputUI.ContainsKey("Error") ? outputUI["Error"] : null);
                    output.Add("BodyPart", outputUI.ContainsKey("BodyPart") ? outputUI["BodyPart"] : 0);
                    output.Add("AuthenticationFactor", outputUI.ContainsKey("AuthenticationFactor") ? outputUI["AuthenticationFactor"] : 0);
                    output.Add("Width", outputUI.ContainsKey("Width") ? outputUI["Width"] : 0);
                    output.Add("Height", outputUI.ContainsKey("Height") ? outputUI["Height"] : 0);
                    output.Add("SerialDevice", outputUI.ContainsKey("SerialDevice") ? outputUI["SerialDevice"] : "");
                }
                else
                {
                    output.Add("Error", "-1|Retorno Vacio");
                    output.Add("Huella", "");
                    output.Add("TemplateVerify64", "");
                    output.Add("TemplateRegistry64", "");
                    output.Add("TemplateANSI64", "");
                    output.Add("TemplateISO64", "");
                    output.Add("WSQ", "");
                    output.Add("SerialDevice", "");

                    output.Add("BodyPart", 0);
                    output.Add("AuthenticationFactor", 0);
                    output.Add("Width", 0);
                    output.Add("Height", 0);
                }
            }
        }

        private string GetImageB64FromImage(object image)
        {
            byte[] byImg;
            try
            {
                System.Drawing.Image img = (System.Drawing.Image)(((System.Drawing.Image)image).Clone());
                byImg = ImageToByteArray(img);
                return Convert.ToBase64String(byImg);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            try
            {
                using (var ms = new MemoryStream())
                {
                    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string Buffer => new JavaScriptSerializer().Serialize(output);

        public string Tag { get => "FM"; }

    }

}
