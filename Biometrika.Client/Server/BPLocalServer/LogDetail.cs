﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BPLocalServer
{
    public partial class LogDetail : Form
    {
        string path;
        string directory;
        public LogDetail()
        {
            InitializeComponent();
        }

        private void Log_Load(object sender, EventArgs e)
        {
            log4net.Appender.FileAppender o = (log4net.Appender.FileAppender)Program.LOG.Logger.Repository.GetAppenders()[0];
            //Dictionary<string, object> detail = new Dictionary<string, object>();
            //detail.Add("path", o.File);
            //detail.Add("content", File.ReadAllText(path));
            //Packet = detail; // Log.GenerateInfo(); 
            path = o.File; // (string)Packet["path"];
            //string pathlog = path.Substring(0, path.LastIndexOf('\\')) + @"\BC7.log.txt";
            //txt_path.Text = path; // (string)Packet["path"];
            string content = "";

            ReadLogsFiles();

            txt_log.Text = ReadLog(path); // System.IO.File.ReadAllText(path);  // (string)Packet["content"];
        }

        private void ReadLogsFiles()
        {
            try
            {
                System.IO.FileInfo fi = new System.IO.FileInfo(path);
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(fi.Directory.FullName);
                directory = fi.Directory.FullName;
                foreach (System.IO.FileInfo item in di.GetFiles())
                {
                    if (item.FullName.Contains("BC7.log.txt"))
                    {
                        lbLogsFiles.Items.Add(item.Name);
                    }
                } 
            }
            catch (Exception ex)
            {

            }
        }

        private string ReadLog(string path)
        {
            string content = "";
            try
            {
                System.IO.File.Copy(path, path + ".ToRead");
                System.IO.StreamReader sr = new System.IO.StreamReader(path + ".ToRead");
                content = sr.ReadToEnd();
                sr.Close();
                System.IO.File.Delete(path + ".ToRead");
            }
            catch (Exception ex)
            {
                content = ex.Message;
            }
            return content;
        }

        public Dictionary<string, object> Packet { get; set; }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            path = (string)Packet["path"];
            txt_log.Text = ReadLog(path); //System.IO.File.ReadAllText(path);  // (string)Packet["content"];

            splitContainer1.Panel1.Width = 50;
            this.Refresh();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                //openFileDialog1.Title = "Seleccione arcghivo de log a visualizar...";
                //string path = (string)Packet["path"];
                //openFileDialog1.InitialDirectory = path.Substring(0, path.LastIndexOf('\\')); 
                //if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                //{
                //    System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
                //    txt_log.Text = sr.ReadToEnd();
                //    sr.Close();
                //}
            }
            catch (Exception ex)
            {
                txt_log1.Text = "Exception = " + ex.StackTrace;
            }
        }

        private void lbLogsFiles_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                string filename = (string)lbLogsFiles.SelectedItem;
                string pathAux = directory + "\\" + filename;
                txt_log.Text = ReadLog(pathAux);
            }
            catch (Exception ex)
            {
                txt_log.Text = "Error leyendo el archivo [" + ex.Message + "]";
            }
        }
    }
}