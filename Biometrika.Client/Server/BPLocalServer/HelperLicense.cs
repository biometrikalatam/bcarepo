﻿using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPLocalServer
{
    public class HelperLicense
    {
        internal static readonly ILog LOG = LogManager.GetLogger(typeof(HelperLicense));

        internal static int GetInfoRemoteLicense(string text, out List<StoreItem> ListStoreItems)
        {
            ListStoreItems = null;
            int ret = 0;
            try
            {
                LOG.Debug("HelperLicense.GetInfoRemoteLicense - IN => Calling => " +
                          Properties.Settings.Default.URLLicenseCenter + "api/License/GetVolumeLicenseInfo...");
                var client = new RestClient(Properties.Settings.Default.URLLicenseCenter + "api/License/GetVolumeLicenseInfo");
                client.Timeout = 30000;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    LOG.Debug("HelperLicense.GetInfoRemoteLicense - Response OK...");
                    //string cont = response.Content; //.Replace("\"[", "[").Replace("]\"", "]");
                    //var oListStoreItems = JsonConvert.DeserializeObject(cont);
                    ListStoreItems = JsonConvert.DeserializeObject<List<StoreItem>>(response.Content);
                    if (ListStoreItems != null)
                    {
                        LOG.Debug("HelperLicense.GetInfoRemoteLicense - ListStoreItems Len => " + ListStoreItems.Count());
                    }
                    else
                    {
                        LOG.Debug("HelperLicense.GetInfoRemoteLicense - ListStoreItems NULL!");
                    }
                }
                else
                {
                    LOG.Debug("HelperLicense.GetInfoRemoteLicense - Response Error");
                    LOG.Debug("HelperLicense.GetInfoRemoteLicense - Response Content = " +
                                (string.IsNullOrEmpty(response.Content) ? "null" : response.Content));
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("HelperLicense.GetInfoRemoteLicense - Excp Error: " + ex.Message);
            }
            LOG.Debug("HelperLicense.GetInfoRemoteLicense OUT! ret = " + ret);
            return ret;
        }
    }

    public class StoreItem
    {
        public StoreItem() { }

        public string Id;
        public bool IsValid;
        public string Code;
        public string LicenseType;
        public DateTime FechaDesde;
        public DateTime FechaHasta;

        //public List<Common.BKLicense> ListVolumeLicenses;
        public List<Client> ListClientsRegistered;

        //Para control de ocupados en la operacion rápida
        public int QTotal { get; set; }
        public int QAvailable { get; set; }
        //Hashtable htListVolumeLicenses;
        //internal Hashtable htListClientsRegistered;
        //internal BKLicense oLicense;
    }

    public class Client
    {
        public Client() { }

        public string IP;
        public string MacAdress;
        public string HDDSerial;
    }
}
