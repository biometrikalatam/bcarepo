﻿using Biometrika.License.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BPLocalServer
{
    public partial class Status : Form
    {
        public Status()
        {
            InitializeComponent();
        }

        private void Status_Load(object sender, EventArgs e)
        {
            txt_URI.Text = Program.URI; // (string)Packet["Uri"];
            txt_managers.Text = "";

            Packet = LocalServerManager.GenerateInfo();

            List<string> managers = (List<string>)Packet["Managers"]; //(List<string>)Program.BPConfig.Adapters; ;

            Version version = Assembly.GetEntryAssembly().GetName().Version;
            labVersion.Text = "Biometrika Client versión " + version.ToString();

            for (int i = 0; i < managers.Count; i++)
            {
                txt_managers.Text = txt_managers.Text + (i + 1).ToString() + ") " + managers[i] + Environment.NewLine;
            }

            txtLicenseInfo.Text = "License Type = " + Properties.Settings.Default.LicenseType + Environment.NewLine;
            if (Properties.Settings.Default.LicenseType.Equals("Remote"))
            {
                txtLicenseInfo.Text = txtLicenseInfo.Text + "Key Workstation (for Remote License) = " + Utils.Utils.GetSerialHHD(true) + Environment.NewLine;
            }
            if (Program.oLICENSE != null)
            {
                txtLicenseInfo.Text = txtLicenseInfo.Text + "Licencia Valida = " + Program.oLICENSE.Status.ToString() + Environment.NewLine;
                txtLicenseInfo.Text = txtLicenseInfo.Text + "TipoLicencia = " + Program.oLICENSE.TipoLicencia + Environment.NewLine;
                txtLicenseInfo.Text = txtLicenseInfo.Text + "Cliente = " + Program.oLICENSE.Cliente + Environment.NewLine;
                txtLicenseInfo.Text = txtLicenseInfo.Text + "Codigo = " + Program.oLICENSE.Codigo + Environment.NewLine;
                txtLicenseInfo.Text = txtLicenseInfo.Text + "FechaDesde = " + Program.oLICENSE.FechaDesde + Environment.NewLine;
                txtLicenseInfo.Text = txtLicenseInfo.Text + "FechaHasta = " + Program.oLICENSE.FechaHasta + Environment.NewLine;
                txtLicenseInfo.Text = txtLicenseInfo.Text + "Atributos Adicionales" + Environment.NewLine;
                txtLicenseInfo.Text = txtLicenseInfo.Text + "--------------------------------------------" + Environment.NewLine;
                foreach (BKLicenseAtributoAdicional item in Program.oLICENSE.AtributosAdicionales)
                {
                    txtLicenseInfo.Text = txtLicenseInfo.Text + item.Nombre + " = " + item.Valor + Environment.NewLine;
                }
            } else
            {
                txtLicenseInfo.Text = "No existe licencia configurada o error leyendo licencia!";
            }


        }

        public Dictionary<string, object> Packet { get; set; }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
