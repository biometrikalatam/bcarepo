﻿using Biometrika.License.Common;
using Domain;
using log4net;
using Microsoft.Owin.Hosting;
using Newtonsoft.Json;
using Owin;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using System.Web.Http;
using System.Reflection;

namespace BPLocalServer
{
    using AppFunc = Func<IDictionary<string, object>, Task>; 



    internal class Program 
    {
        internal static readonly ILog LOG = LogManager.GetLogger(typeof(Program)); 

        internal static bool RUNNING = true;  
        private static Mutex mutex;
        private const string MUTEX_NAME = "BIOMERIKA_MUTEX";
        internal static Biometrika.License.Common.BKLicense oLICENSE = new Biometrika.License.Common.BKLicense();
        private static Biometrika.License.Check.LicenseCheck oCHECK;
        private static bool LICENSE_VALID;
        private static bool IsStatusOpen;
        private static bool IsLogOpen;

        private static CentralConfig CentralConfig;
        internal static BPConfig BPConfig;
        internal static string URI;

        private static TrayIconForm TrayIconForm;
        private static Splash Splash;

        internal static string TypeLicense = "Local"; //Local o Remote

        //internal static Biometrika.BioApi20.BSP.BSPBiometrika _BSP;

        private static void Main(string[] args)
        {
            //string s = Utils.Utils.GetSerialHHD(true);

            Log4N.Configure();
            LOG.Info("Biometrika Server Client Iniciando...");

            //Added 22-03-2021 - PAra informacion acerca de Licencias Temporales
            if (args != null && args.Length > 0)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    LOG.Info("Biometrika Server Client Porgram - Param[" + i.ToString() + "] = " + args[i] + "...");
                }
                string[] param = args[0].Split('=');
                if (param[0].Equals("-rli") || param[0].Equals("-remotelicenseinfo"))  //-rli = -remotelicenseinfo
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    //    Application.Run(new frmTestSizing());
                    Application.Run(new frmLicense());
                    //frmLicense frmLic = new frmLicense();
                    //frmLic.Show();
                }
            }
            else
            {
                mutex = new Mutex(true, MUTEX_NAME);
                if (mutex.WaitOne(0, false))
                {
                    //Log.Add("Iniciando servidor");
                    LOG.Info("Iniciando BPLocalServer...");
                    Version version = Assembly.GetEntryAssembly().GetName().Version;
                    LOG.Info("Biometrika Client version v" + version.ToString());
                    LoadPreferences();

                    //Task.Run(() =>
                    //{
                    //    Splash = new Splash();
                    //    Splash.ControlBox = false;
                    //    Splash.TopMost = true;
                    //    Splash.BringToFront();
                    //    Splash.Show();
                    //});

                    CancellationTokenSource cts = new CancellationTokenSource();
                    CancellationToken canceltoken = cts.Token;
                    Task taskIcon = Task.Run(() =>
                    {
                        TrayIconForm = new TrayIconForm();

                        TrayIconForm.ShowDialog();
                    }, canceltoken);

                    string uri = new UriBuilder("http", "localhost", int.Parse(BPConfig.LocalPort), "/").ToString();
                    URI = uri;
                    LOG.Info("URL Biometrika Server Client = " + uri);
                    LocalServerManager.Uri = new Uri(uri);

                    IsStatusOpen = false;
                    IsLogOpen = false;

                    //1.- Chequeo de licencia.
                    ////Lanzo thread para chequeao cada 60 minutos
                    //tLicenseBK = new Thread(new ThreadStart(ThreadCheckLicenseBK));
                    //tLicenseBK.Start();
                    TypeLicense = Properties.Settings.Default.LicenseType;
                    string msg = "";
                    if (string.Equals(TypeLicense, "Local"))
                    {
                        LOG.Info("Checking licencia...[" + Properties.Settings.Default.LicensePath + "]");
                        oCHECK = new Biometrika.License.Check.LicenseCheck();

                        int retCheckLicense = oCHECK.CheckLicense(Properties.Settings.Default.LicensePath, "BC7", 3600000, out oLICENSE, out msg);
                        LICENSE_VALID = (retCheckLicense == 0);
                    }
                    else //Es remote => Consulto a WS
                    {
                        LICENSE_VALID = CheckVolumeRemoteLicense(out oLICENSE, out msg);
                    }

                    if (LICENSE_VALID)
                    {
                        LOG.Info("Status Licencia = " + Program.oLICENSE.Status.ToString());
                        LOG.Info("TipoLicencia = " + Program.oLICENSE.TipoLicencia);
                        LOG.Info("Cliente = " + Program.oLICENSE.Cliente);
                        LOG.Info("Codigo = " + Program.oLICENSE.Codigo);
                        LOG.Info("FechaDesde = " + Program.oLICENSE.FechaDesde);
                        LOG.Info("FechaHasta = " + Program.oLICENSE.FechaHasta);
                        LOG.Info("Atributos Adicionales");
                        LOG.Info("--------------------------------------------");
                        foreach (BKLicenseAtributoAdicional item in Program.oLICENSE.AtributosAdicionales)
                        {
                            LOG.Info(" >>> " + item.Nombre + " = " + item.Valor);
                        }
                    }
                    else
                    {
                        LOG.Fatal("Licencia inválida! [" + msg + "]");
                    }
                    //Splash.Close();
                    //using (WebApp.Start<Startup>(uri))
                    //{
                    //    while (RUNNING) { };
                    //}
                    LOG.Info("Iniciando Startup...");
                    using (WebApp.Start<Startup>(uri))
                    {
                        //LOG.Info("Webapp %CPU:" + cpuCounter.NextValue());
                        while (RUNNING)
                        { Thread.Sleep(100); };
                    }

                    //TrayIconForm.CloseForm();
                    cts.Cancel();
                    mutex.ReleaseMutex();
                    LOG.Info("Saliendo. Biometrika Client Cerrado!");
                    Application.Exit();
                    Environment.Exit(0);
                }
                else
                {
                    //Log.Add("Inicializado anteriormente");
                    LOG.Info("Inicializado anteriormente");
                }
            }
        }

        private static bool CheckVolumeRemoteLicense(out BKLicense oLICENSE, out string msgerr)
        {
            bool ret = false;
            msgerr = "";
            oLICENSE = null;
            string xmllic;
            try
            {
                LOG.Info("Program.CheckVolumeRemoteLicense IN...");
                using (LicenseCheckCenterWS.CheckCenterWS ws = new LicenseCheckCenterWS.CheckCenterWS())
                {
                    ws.Timeout = 30000;
                    ws.Url = Properties.Settings.Default.URLLicenseCenter + "CheckCenterWS.asmx";
                    string key = Utils.Utils.GetSerialHHD(true);
                    LOG.Info("Program.CheckVolumeRemoteLicense - Consultando en URL = " + ws.Url + " => key = " + key);
                    int iret = ws.CheckVolumeLicense(Properties.Settings.Default.IdVolumeLicense, key, out msgerr, out xmllic);
                    LOG.Info("Program.CheckVolumeRemoteLicense - Respuesta WS = " + iret.ToString() +
                        "[" + (string.IsNullOrEmpty(msgerr) ? "Sin Comentarios" : msgerr) + "]");
                    if (iret == 0 && !string.IsNullOrEmpty(xmllic))
                    {
                        LOG.Info("Program.CheckVolumeRemoteLicense - Parsing license...");
                        oLICENSE = XmlUtils.DeserializeObject<BKLicense>(xmllic);
                        LOG.Info("Program.CheckVolumeRemoteLicense - (oLICENSE!=null) => " + (oLICENSE != null).ToString());
                        ret = true;
                    }
                    else
                    {
                        LOG.Fatal("Program.CheckVolumeRemoteLicense - Error chequeando licencia en remoto!");
                        ret = false;
                    }
                }
            }
            catch (Exception ex)
            {
                msgerr = "BPLocalServer.CheckVolumeRemoteLicense Error - " + ex.Message;
                ret = false;
            }
            LOG.Info("Program.CheckVolumeRemoteLicense OUT! => ret = " + ret.ToString());
            return ret;
        }

        private static void LoadPreferences()
        {
            LOG.Info("LoadPreferences IN...");
            string actualReferencesPath = Path.Combine(new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName, "Config");
            LOG.Info("ActualReferencesPath = " + actualReferencesPath);
            CentralConfig = JsonConvert.DeserializeObject<CentralConfig>(File.ReadAllText(Path.Combine(actualReferencesPath, "Preferences.json")));
            LOG.Info("Config Leido = " + (CentralConfig != null));

            Populate(CentralConfig);
            LOG.Info("LoadPreferences OUT!");
        }

        private static void Populate(CentralConfig centralConfig)
        {
            LOG.Info("Populate IN...");
            if (centralConfig.ForceLocal)
            {
                LOG.Info("Populate centralConfig.ForceLocal...");
                string actualReferencesPath = Path.Combine(new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName, "Config");
                LOG.Info("Populate actualReferencesPath = " + actualReferencesPath);

                string localServerPath = Path.Combine(actualReferencesPath, "LocalServer.json");
                LOG.Info("Populate localServerPath = " + localServerPath);
                if (File.Exists(localServerPath))
                {
                    LOG.Info("Populate Reading BPConfig...");
                    BPConfig = JsonConvert.DeserializeObject<BPConfig>(File.ReadAllText(localServerPath));
                    LOG.Info("Populate Config Ok = " + (BPConfig!=null));
                    LOG.Info("Populate Config - Adapters Habilitados:");
                    int q = 1;
                    foreach (string item in BPConfig.Adapters)
                    {
                        LOG.Info("Populate Config - Adapter " + (q++) + " => " + item);
                    }
                }
                else
                {
                    LOG.Info("Populate Creating BPConfig...");
                    BPConfig.LocalPort = "9191";
                    BPConfig.Timeout = 30;
                    BPConfig.LoadBSP = true;
                    BPConfig.Adapters = new List<string>() { "CAM", "BPC7", "FS", "BVICAM" , "BVI" , "FEM" , "DR" };
                }
            }
            else
            {
                LOG.Info("Populate reading from server => URL = " + centralConfig.ServerConfig);
                HttpClient client = new HttpClient();

                HttpResponseMessage response = client.GetAsync(centralConfig.ServerConfig).Result;
                response.EnsureSuccessStatusCode();

                string plainResult = response.Content.ReadAsStringAsync().Result;
                LOG.Info("Populate Readed = " + plainResult);
                BPConfig = JsonConvert.DeserializeObject<BPConfig>(plainResult);
                LOG.Info("Populate Config Ok = " + (BPConfig != null));
            }
            //if (BPConfig.LoadBSP)
            //{
            //    InitBSP();
            //}
            LOG.Info("Populate OUT!");
            return;
        }

        

        public class Startup
        {
            public void Configuration(IAppBuilder app)
            {
                LOG.Info("Cargando Managers ...");
                LocalServerManager.InitializeManagers(BPConfig);
                LOG.Info("Cargados Managers!");

                //var configf = new HttpConfiguration();

                HttpConfiguration config = new HttpConfiguration();
                config.Routes.MapHttpRoute(
                    name: "DefaultApi",
                    routeTemplate: "api/{controller}/{action}",
                    defaults: new { id = RouteParameter.Optional }
                );
                app.UseWebApi(config);
                app.Use<Listener>();
            }
        }

        public class Listener
        {
            private AppFunc nextLocal;

            public Listener(AppFunc next)  
            {
                nextLocal = next;
            }

            [System.Diagnostics.CodeAnalysis.SuppressMessage("Await.Warning", "CS4014:Await.Warning")]
            public async Task Invoke(IDictionary<string, object> environment)  // Chrome hace 2 peticiones por GET , una es OPTIONS y la otra GET
                                                                               // IE solo hace GET
            {
                LOG.Debug("Listener.Invoke In...");
                string querystring = (string)environment["owin.RequestQueryString"];
                LOG.Debug("Listener.Invoke querystring = " + querystring);
                NameValueCollection qsParsed = HttpUtility.ParseQueryString(querystring);
                LOG.Debug("Listener.Invoke querystring parsed => " + (qsParsed != null && qsParsed.Count > 0));
                Stream response = null;
                char[] buffer = null;
                byte[] byteBuffer = null;
                Stream responseStream;

                IDictionary<string, string[]> responseHeaders = (IDictionary<string, string[]>)environment["owin.ResponseHeaders"]; // captura los headers
                string responseText;
                byte[] responseBytes;

                switch (environment["owin.RequestPath"] as string)
                {
                    case "/log":
                        if (!IsLogOpen)
                        {
                            LOG.Debug("Listener.Invoke Show Log IN...");
                            IsLogOpen = true;
                            //Log.Add("Se invoca el log"); 
                            Application.EnableVisualStyles();

                            LogDetail log = new LogDetail();
                            log4net.Appender.FileAppender o = (log4net.Appender.FileAppender)LOG.Logger.Repository.GetAppenders()[0];
                            Dictionary<string, object> detail = new Dictionary<string, object>();
                            detail.Add("path", o.File);
                            //detail.Add("content", File.ReadAllText(path));
                            log.Packet = detail; // Log.GenerateInfo(); 
                            Task.Run(() =>
                            { 
                                Application.Run(log);
                                IsLogOpen = false;
                            });
                            responseText = "Se ha mostrado el log";
                            responseBytes = Encoding.UTF8.GetBytes(responseText);

                            responseStream = (Stream)environment["owin.ResponseBody"];
                            responseHeaders = (IDictionary<string, string[]>)environment["owin.ResponseHeaders"];

                            responseHeaders["Content-Length"] = new string[] { responseBytes.Length.ToString(CultureInfo.InvariantCulture) };
                            responseHeaders["Content-Type"] = new string[] { "text/plain" };

                            responseStream.WriteAsync(responseBytes, 0, responseBytes.Length);
                            LOG.Debug("Listener.Invoke Show Log OUT!");
                        }
                        break;

                    case "/status":
                        if (!IsStatusOpen)
                        {
                            IsStatusOpen = true;

                            LOG.Debug("Listener.Invoke status IN...");
                            Application.EnableVisualStyles();

                            Status status = new Status();
                            status.Packet = LocalServerManager.GenerateInfo();
                            //Application.Run(status);

                            Task.Run(() =>
                            {
                                Application.Run(status);
                                IsStatusOpen = false;
                            });
                            responseText = "Se ha mostrado el status";
                            responseBytes = Encoding.UTF8.GetBytes(responseText);

                            responseStream = (Stream)environment["owin.ResponseBody"];
                            responseHeaders = (IDictionary<string, string[]>)environment["owin.ResponseHeaders"];

                            responseHeaders["Content-Length"] = new string[] { responseBytes.Length.ToString(CultureInfo.InvariantCulture) };
                            responseHeaders["Content-Type"] = new string[] { "text/plain" };

                            responseStream.WriteAsync(responseBytes, 0, responseBytes.Length);
                            LOG.Debug("Listener.Invoke status OUT!");
                        }
                        break;

                    case "/favicon.ico":
                        buffer = Encoding.UTF8.GetChars(Encoding.UTF8.GetBytes(""));
                        responseHeaders["Content-Type"] = new[] { "text/html" };
                        responseHeaders["Content-Length"] = new[] { buffer.Length.ToString() };
                        environment["owin.ResponseStatusCode"] = 404;
                        break;

                    default:
                        //if (LICENSE_VALID)
                        //{

                        //Add BSP si no la tiene
                        //qsParsed = AddBSP(qsParsed);
                        LOG.Debug("Listener.Invoke SRC IN...");
                        string JsonData = LocalServerManager.GetData(qsParsed);
                        LOG.Debug("Listener.Invoke JsonData =" + JsonData);
                        buffer = Encoding.UTF8.GetChars(Encoding.UTF8.GetBytes(JsonData));   //// datos formateados
                        byte[] byteArr = Encoding.UTF8.GetBytes(JsonData);
                        //responseHeaders["Content-Length"] = new string[] { buffer.Length.ToString() };
                        responseHeaders["Content-Length"] = new string[] { byteArr.Length.ToString() };
                        responseHeaders["Content-Type"] = new string[] { "application/json" };

                        responseHeaders["Access-Control-Allow-Origin"] = new string[] { "*" };
                        responseHeaders["Access-Control-Allow-Headers"] = new string[] { "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With" };
                        responseHeaders["Access-Control-Allow-Methods"] = new string[] { "GET" };

                        response = environment["owin.ResponseBody"] as Stream;

                        LOG.Debug("Listener.Invoke Envia a stream...");
                        using (var writer = new StreamWriter(response))
                        {
                            await writer.WriteAsync(buffer);
                        }
                        //}
                        //else
                        //{
                        //string JsonData = "{Error: ";
                        //buffer = Encoding.UTF8.GetChars(Encoding.UTF8.GetBytes(JsonData));   //// datos formateados

                        //responseHeaders["Content-Length"] = new string[] { buffer.Length.ToString() };
                        //responseHeaders["Content-Type"] = new string[] { "application/json" };

                        //responseHeaders["Access-Control-Allow-Origin"] = new string[] { "*" };
                        //responseHeaders["Access-Control-Allow-Headers"] = new string[] { "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With" };

                        //response = environment["owin.ResponseBody"] as Stream;

                        //using (var writer = new StreamWriter(response))
                        //{
                        //    await writer.WriteAsync(buffer);
                        //}
                        //}
                        break;
                }
            }

            //private NameValueCollection AddBSP(NameValueCollection qsParsed)
            //{
            //    NameValueCollection retNVC = new NameValueCollection();
            //    try
            //    {
            //        bool exist = false;
            //        foreach (var item in qsParsed)
            //        {
            //            if (item.ToString().Equals("BSP"))
            //            {
            //                retNVC.Add("BSP", _BSP);
            //            }
            //            retNVC.Add(item)
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        retNVC = qsParsed;
            //    }
            //    return retNVC;
            //}
        }
    }
}