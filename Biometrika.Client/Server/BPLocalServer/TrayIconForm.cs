﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BPLocalServer
{
    public partial class TrayIconForm : Form
    {
        enum Opciones     {  Status, Log, Salir, About  }
 
        public TrayIconForm()
        {
            InitializeComponent();

            ShowInTaskbar = false;
            FormBorderStyle = FormBorderStyle.None;
            Hide();
            
            notifyIcon1.Visible = true;
            notifyIcon1.ShowBalloonTip(3000);
            notifyIcon1.ContextMenuStrip.Items[(int) Opciones.Status].Click += ToolStripMenuItem1OnClick;
            notifyIcon1.ContextMenuStrip.Items[(int)Opciones.Log].Click += LogOnClick;
            notifyIcon1.ContextMenuStrip.Items[(int)Opciones.Salir].Click += SalirClick;
            notifyIcon1.ContextMenuStrip.Items[(int)Opciones.About].Click += AboutClick;
            
        }

        private void AboutClick(object sender, EventArgs e)
        {
            //System.Diagnostics.Process.Start("http://localhost:9191/status");
            Status statusForm = new Status();
            statusForm.Show(this);
        }

        private void SalirClick(object sender, EventArgs e)
        {
            Program.RUNNING = false;
        }

        private void LogOnClick(object sender, EventArgs e)
        {
            //System.Diagnostics.Process.Start("http://localhost:9191/log");
            LogDetail logDetailForm = new LogDetail();
            logDetailForm.Show(this);
        }

        private void ToolStripMenuItem1OnClick(object sender, EventArgs e)
        {
            //System.Diagnostics.Process.Start("http://localhost:9191/status");
            Status statusForm = new Status();
            statusForm.Show(this);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        public void CloseForm()
        {
            this.Close();
        }

        private void TrayIconForm_Load(object sender, EventArgs e)
        {
            Version version = Assembly.GetEntryAssembly().GetName().Version;
            notifyIcon1.Text = "Biometrika Client version v" + version.ToString();
            
        }
    }
}
