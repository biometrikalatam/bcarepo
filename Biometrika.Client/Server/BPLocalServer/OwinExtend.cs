﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;

namespace BPLocalServer
{
    class OwinExtend
    {
    }

    //public class RequestSizeLimitingMiddleware : OwinMiddleware
    //{
    //    public RequestSizeLimitingMiddleware(OwinMiddleware next, long maxRequestSizeInBytes)
    //        : base(next)
    //    {
    //        this.MaxRequestSizeInBytes = maxRequestSizeInBytes;
    //    }

    //    public long MaxRequestSizeInBytes { get; private set; }

    //    public override async Task Invoke(IOwinContext context)
    //    {
    //        IOwinRequest request = context.Request;

    //        if (request != null)
    //        {
    //            string[] values = null;
    //            if (request.Headers.TryGetValue("Content-Length", out values))
    //            {
    //                if (Convert.ToInt64(values[0]) > MaxRequestSizeInBytes)
    //                {
    //                    throw new InvalidOperationException(string.Format("Request size exceeds the allowed maximum size of {0} bytes", MaxRequestSizeInBytes));
    //                }
    //            }
    //            if (request.Headers.TryGetValue("Transfer-Encoding", out values)
    //                && values[0] == "chunked")
    //            {
    //                RequestSizeLimitingStream wrappingStream = new RequestSizeLimitingStream(request.Body, MaxRequestSizeInBytes);

    //                request.Body = wrappingStream;
    //            }
    //        }

    //        await Next.Invoke(context);
    //    }
    //}

    //public class RequestSizeLimitingStream : Stream
    //{
    //    private Stream _innerStream;
    //    private long totalBytesReadCount = 0;
    //    private long maxRequestSizeInBytes = 0;

    //    public RequestSizeLimitingStream(Stream innerStream, long maxReceivedMessageSize)
    //    {
    //        _innerStream = innerStream;
    //        this.maxRequestSizeInBytes = maxReceivedMessageSize;
    //    }

    //    protected Stream InnerStream
    //    {
    //        get { return _innerStream; }
    //    }

    //    public override bool CanRead
    //    {
    //        get { return _innerStream.CanRead; }
    //    }

    //    public override bool CanSeek
    //    {
    //        get { return _innerStream.CanSeek; }
    //    }

    //    public override bool CanWrite
    //    {
    //        get { return _innerStream.CanWrite; }
    //    }

    //    public override long Length
    //    {
    //        get { return _innerStream.Length; }
    //    }

    //    public override long Position
    //    {
    //        get { return _innerStream.Position; }
    //        set { _innerStream.Position = value; }
    //    }

    //    public override int ReadTimeout
    //    {
    //        get { return _innerStream.ReadTimeout; }
    //        set { _innerStream.ReadTimeout = value; }
    //    }

    //    public override bool CanTimeout
    //    {
    //        get { return _innerStream.CanTimeout; }
    //    }

    //    public override int WriteTimeout
    //    {
    //        get { return _innerStream.WriteTimeout; }
    //        set { _innerStream.WriteTimeout = value; }
    //    }

    //    protected override void Dispose(bool disposing)
    //    {
    //        if (disposing)
    //        {
    //            _innerStream.Dispose();
    //        }
    //        base.Dispose(disposing);
    //    }

    //    public override long Seek(long offset, SeekOrigin origin)
    //    {
    //        return _innerStream.Seek(offset, origin);
    //    }

    //    public override int Read(byte[] buffer, int offset, int count)
    //    {
    //        int currentNumberOfBytesRead = _innerStream.Read(buffer, offset, count);

    //        ValidateRequestSize(currentNumberOfBytesRead);

    //        return currentNumberOfBytesRead;
    //    }

    //    public override async Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
    //    {
    //        int currentNumberOfBytesRead = await _innerStream.ReadAsync(buffer, offset, count, cancellationToken);

    //        ValidateRequestSize(currentNumberOfBytesRead);

    //        return currentNumberOfBytesRead;
    //    }

    //    public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
    //    {
    //        return _innerStream.BeginRead(buffer, offset, count, callback, state);
    //    }

    //    public override int EndRead(IAsyncResult asyncResult)
    //    {
    //        int currentNumberOfBytesRead = _innerStream.EndRead(asyncResult);

    //        ValidateRequestSize(currentNumberOfBytesRead);

    //        return currentNumberOfBytesRead;
    //    }

    //    public override int ReadByte()
    //    {
    //        int currentNumberOfBytesRead = _innerStream.ReadByte();

    //        ValidateRequestSize(currentNumberOfBytesRead);

    //        return currentNumberOfBytesRead;
    //    }

    //    public override void Flush()
    //    {
    //        _innerStream.Flush();
    //    }

    //    public override Task CopyToAsync(Stream destination, int bufferSize, CancellationToken cancellationToken)
    //    {
    //        return _innerStream.CopyToAsync(destination, bufferSize, cancellationToken);
    //    }

    //    public override Task FlushAsync(CancellationToken cancellationToken)
    //    {
    //        return _innerStream.FlushAsync(cancellationToken);
    //    }

    //    public override void SetLength(long value)
    //    {
    //        _innerStream.SetLength(value);
    //    }

    //    public override void Write(byte[] buffer, int offset, int count)
    //    {
    //        _innerStream.Write(buffer, offset, count);
    //    }

    //    public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
    //    {
    //        return _innerStream.WriteAsync(buffer, offset, count, cancellationToken);
    //    }

    //    public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
    //    {
    //        return _innerStream.BeginWrite(buffer, offset, count, callback, state);
    //    }

    //    public override void EndWrite(IAsyncResult asyncResult)
    //    {
    //        _innerStream.EndWrite(asyncResult);
    //    }

    //    public override void WriteByte(byte value)
    //    {
    //        _innerStream.WriteByte(value);
    //    }

    //    private void ValidateRequestSize(int currentNumberOfBytesRead)
    //    {
    //        totalBytesReadCount += currentNumberOfBytesRead;

    //        if (totalBytesReadCount > maxRequestSizeInBytes)
    //        {
    //            throw new InvalidOperationException(string.Format("Request size exceeds the allowed maximum size of {0} bytes", maxRequestSizeInBytes));
    //        }
    //    }
    //}
}
