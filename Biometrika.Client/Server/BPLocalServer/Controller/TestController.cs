﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace BPLocalServer.Controller
{
    public class TestController : ApiController
    {
        public IEnumerable<string> GetTest()
        {
            return new string[] { "One", "Two", "Three" };
        }
    }
}
