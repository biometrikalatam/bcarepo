﻿namespace BPLocalServer
{
    partial class Status
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Status));
            this.btn_cerrar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_URI = new System.Windows.Forms.TextBox();
            this.Managers = new System.Windows.Forms.Label();
            this.txt_managers = new System.Windows.Forms.TextBox();
            this.txtLicenseInfo = new System.Windows.Forms.TextBox();
            this.labelLic = new System.Windows.Forms.Label();
            this.labVersion = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_cerrar
            // 
            this.btn_cerrar.Location = new System.Drawing.Point(774, 577);
            this.btn_cerrar.Margin = new System.Windows.Forms.Padding(2);
            this.btn_cerrar.Name = "btn_cerrar";
            this.btn_cerrar.Size = new System.Drawing.Size(90, 36);
            this.btn_cerrar.TabIndex = 0;
            this.btn_cerrar.Text = "Cerrar";
            this.btn_cerrar.UseVisualStyleBackColor = true;
            this.btn_cerrar.Click += new System.EventHandler(this.btn_cerrar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "URI";
            // 
            // txt_URI
            // 
            this.txt_URI.BackColor = System.Drawing.Color.LightYellow;
            this.txt_URI.Location = new System.Drawing.Point(70, 25);
            this.txt_URI.Margin = new System.Windows.Forms.Padding(2);
            this.txt_URI.Name = "txt_URI";
            this.txt_URI.ReadOnly = true;
            this.txt_URI.Size = new System.Drawing.Size(794, 20);
            this.txt_URI.TabIndex = 2;
            // 
            // Managers
            // 
            this.Managers.AutoSize = true;
            this.Managers.Location = new System.Drawing.Point(11, 60);
            this.Managers.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Managers.Name = "Managers";
            this.Managers.Size = new System.Drawing.Size(54, 13);
            this.Managers.TabIndex = 3;
            this.Managers.Text = "Managers";
            // 
            // txt_managers
            // 
            this.txt_managers.BackColor = System.Drawing.Color.LightYellow;
            this.txt_managers.Location = new System.Drawing.Point(70, 60);
            this.txt_managers.Margin = new System.Windows.Forms.Padding(2);
            this.txt_managers.Multiline = true;
            this.txt_managers.Name = "txt_managers";
            this.txt_managers.ReadOnly = true;
            this.txt_managers.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_managers.Size = new System.Drawing.Size(794, 221);
            this.txt_managers.TabIndex = 4;
            // 
            // txtLicenseInfo
            // 
            this.txtLicenseInfo.BackColor = System.Drawing.Color.LightYellow;
            this.txtLicenseInfo.Location = new System.Drawing.Point(70, 285);
            this.txtLicenseInfo.Margin = new System.Windows.Forms.Padding(2);
            this.txtLicenseInfo.Multiline = true;
            this.txtLicenseInfo.Name = "txtLicenseInfo";
            this.txtLicenseInfo.ReadOnly = true;
            this.txtLicenseInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLicenseInfo.Size = new System.Drawing.Size(794, 279);
            this.txtLicenseInfo.TabIndex = 6;
            // 
            // labelLic
            // 
            this.labelLic.AutoSize = true;
            this.labelLic.Location = new System.Drawing.Point(11, 288);
            this.labelLic.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelLic.Name = "labelLic";
            this.labelLic.Size = new System.Drawing.Size(47, 13);
            this.labelLic.TabIndex = 5;
            this.labelLic.Text = "Licencia";
            // 
            // labVersion
            // 
            this.labVersion.AutoSize = true;
            this.labVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labVersion.Location = new System.Drawing.Point(130, 580);
            this.labVersion.Name = "labVersion";
            this.labVersion.Size = new System.Drawing.Size(211, 18);
            this.labVersion.TabIndex = 7;
            this.labVersion.Text = "Biometrika Client versión 7";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(110, 579);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 18);
            this.label3.TabIndex = 8;
            this.label3.Text = "@";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::BPLocalServer.Properties.Resources.icono_36x36;
            this.pictureBox1.Location = new System.Drawing.Point(70, 579);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(38, 38);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(114, 599);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(261, 16);
            this.label2.TabIndex = 10;
            this.label2.Text = "www.biometrikalatam.com - +56-224029772";
            // 
            // Status
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 644);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labVersion);
            this.Controls.Add(this.txtLicenseInfo);
            this.Controls.Add(this.labelLic);
            this.Controls.Add(this.txt_managers);
            this.Controls.Add(this.Managers);
            this.Controls.Add(this.txt_URI);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_cerrar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Status";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Biometrika Client Status...";
            this.Load += new System.EventHandler(this.Status_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_cerrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_URI;
        private System.Windows.Forms.Label Managers;
        private System.Windows.Forms.TextBox txt_managers;
        private System.Windows.Forms.TextBox txtLicenseInfo;
        private System.Windows.Forms.Label labelLic;
        private System.Windows.Forms.Label labVersion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
    }
}