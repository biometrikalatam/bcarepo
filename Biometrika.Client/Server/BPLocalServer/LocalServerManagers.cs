﻿using Biometrika.License.Common;
using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace BPLocalServer
{
 
    public static class LocalServerManager
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(LocalServerManager));

        internal static Biometrika.BioApi20.BSP.BSPBiometrika _BSP;
        
        public static Uri Uri { get; set; }
        private static string ActualReferencesPath;

        public static void InitializeManagers(BPConfig bPConfig)
        {
            Managers.Initialize(bPConfig);

            if (bPConfig.LoadBSP) InitBSP();
        }

        private static bool InitBSP()
        {
            bool ret = false;
            try
            {
                LOG.Info("LocalServerManager.InitBSP In Loading BSP...");
                _BSP = new Biometrika.BioApi20.BSP.BSPBiometrika();

                _BSP.BSPAttach("2.0", true);
                ret = _BSP.IsLoaded;
                LOG.Info("LocalServerManager.InitBSP BSP Loaded = " + ret.ToString());
            }
            catch (Exception ex)
            {
                LOG.Error("LocalServerManager.InitBSP Error = " + ex.Message);
                ret = false;
            }
            return ret;
            //GetInfoFromBSP();

            //BSP.OnCapturedEvent += OnCaptureEvent;
            //BSP.OnTimeoutEvent += OnTimeoutEvent;
        }

        public static string GetData(NameValueCollection qs)
        {

            LOG.Debug("LocalServerManager.GetData IN...");
            string ret = "Licencia Invalida para ";
            string src = qs["src"].ToUpper();
            LOG.Debug("LocalServerManager.GetData " + string.Concat("src =", src));

            if (IsLicenseEnabled(Program.oLICENSE, src))
            {
                // esto funciona solo con algunos adapters... una pena
                Type type = Managers.TypeManagerFrom(src).Type;

                LOG.Debug("LocalServerManager.GetData " + string.Concat("Se instancia ", type));

                if (Program.BPConfig.LoadBSP && (_BSP == null || !_BSP.IsLoaded))
                {
                    InitBSP();
                }
                IManager manager = null;
                //if (_BSP != null)
                //{
                    manager = (IManager)Activator.CreateInstance(type, new object[] { qs, _BSP });
                //} else
                //{
                //    manager = (IManager)Activator.CreateInstance(type, new object[] { qs });
                //}
                ret = manager.Buffer;
                LOG.Debug("LocalServerManager.GetData manager.Buffer = " + ret);
            }
            else
            {
                ret = "{\"Error\": \"" + ret + src + "\"}";
                LOG.Debug("LocalServerManager.GetData Licencia invalida = " + ret);
            }
            LOG.Debug("LocalServerManager.GetData OUT!");
            return ret;
        }

        private static bool IsLicenseEnabled(BKLicense oLICENSE, string src)
        {
            bool ret = false;
            LOG.Debug("LocalServerManager.IsLicenseEnabled IN...");
            if (oLICENSE.Status)
            {
                foreach (BKLicenseAtributoAdicional item in oLICENSE.AtributosAdicionales)
                {
                    if (src.Equals(item.Nombre) && item.Valor.Equals("true"))
                    {
                        LOG.Debug("LocalServerManager.IsLicenseEnabled item.Nombre = true");
                        ret = true;
                        break;
                    }
                }
            } else
            {
                ret = false;
                LOG.Warn("LocalServerManager.IsLicenseEnabled = false");
            }
            LOG.Debug("LocalServerManager.IsLicenseEnabled OUT!");
            return ret;
        }

        internal static Dictionary<string, object> GenerateInfo()
        {
            Dictionary<string, object> propierties = new Dictionary<string, object>();

            propierties.Add("Uri", Uri.AbsoluteUri);
            propierties.Add("Managers", Managers.Collection());

            return propierties;
        }
    }
}