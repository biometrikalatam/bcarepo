﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BPLocalServer
{
    public partial class frmLicense : Form
    {
        internal static readonly ILog LOG = LogManager.GetLogger(typeof(frmLicense));

        public frmLicense()
        {
            InitializeComponent();
        }

        private void btnGetInfo_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtURL.Text))
                {
                    List<StoreItem> oInfo;
                    LOG.Debug("frmLicense.btnGetInfo_Click - Getting info URL = " + txtURL.Text);
                    int ret = HelperLicense.GetInfoRemoteLicense(txtURL.Text, out oInfo);
                    LOG.Debug("frmLicense.btnGetInfo_Click - ret = " + ret);
                    if (oInfo != null)
                    {
                        LOG.Debug("frmLicense.btnGetInfo_Click - Informo info recuperada...");
                        rtbInfo.Text = "Información Recuperada: " + oInfo.Count.ToString() + " stores..." + Environment.NewLine +
                                       "-----------------------------------------------------------------------------";
                        if (ret != 0 || oInfo == null)
                        {
                            if (oInfo == null)
                            {
                                rtbInfo.Text += "Información obtenida NULA! Contactese con el administrador...";
                            }
                        }
                        else
                        {
                            foreach (StoreItem item in oInfo)
                            {
                                rtbInfo.Text += "------------------------------------------------------------------------------------------------------"
                                    + Environment.NewLine;
                                rtbInfo.Text += "Id=" + item.Id + Environment.NewLine +
                                    "Code=" + item.Code + Environment.NewLine +
                                    "Valid=" + item.IsValid.ToString() + Environment.NewLine +
                                    "Desde=" + item.FechaDesde + Environment.NewLine +
                                    "Hasta=" + item.FechaHasta + Environment.NewLine +
                                    "Total=" + item.QTotal + Environment.NewLine +
                                    "Disponibles=" + item.QAvailable + Environment.NewLine +
                                     "Seriales Registrados =>";
                                if (item.ListClientsRegistered == null || item.ListClientsRegistered.Count == 0)
                                {
                                    rtbInfo.Text += " No existen clientes registrados!" + Environment.NewLine;
                                }
                                else
                                {
                                    rtbInfo.Text += Environment.NewLine;
                                    foreach (Client itemC in item.ListClientsRegistered)
                                    {
                                        rtbInfo.Text += "    >> " + itemC.HDDSerial + Environment.NewLine;
                                    }
                                }

                                rtbInfo.Text += Environment.NewLine + Environment.NewLine;
                            }
                        }
                    }
                    else
                    {
                        rtbInfo.Text = "Recuperación de información nula => [ret = " + ret + "]";
                    }

                    if (!string.IsNullOrEmpty(rtbInfo.Text) && !string.IsNullOrEmpty(labSerial.Text))
                    {
                        if (rtbInfo.Text.Contains(labSerial.Text.Trim()))
                        {
                            labStatus.ForeColor = Color.DarkGreen;
                            labStatus.Text = "REGISTRADO";
                            labStatus.Refresh();
                        }
                        else
                        {
                            labStatus.ForeColor = Color.Red;
                            labStatus.Text = "NO REGISTRADO";
                            labStatus.Refresh();
                        }
                    }
                    else
                    {
                        labStatus.ForeColor = Color.DarkBlue;
                        labStatus.Text = "SIN INFORMACIÓN";
                        labStatus.Refresh();
                    }

                    LOG.Debug("frmLicense.btnGetInfo_Click - " + rtbInfo.Text);
                }
                else
                {
                    MessageBox.Show(this, "Debe ingresar una URL válida...", "Atención",
                                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("frmLicense.btnGetInfo_Click - Error: " + ex.Message);
            }
            LOG.Debug("frmLicense.btnGetInfo_Click OUT!");
        }

        private void frmLicense_Load(object sender, EventArgs e)
        {
            try
            {
                labSerial.ForeColor = Color.DarkBlue;
                labStatus.Text = "SIN INFORMACIÓN";
                labStatus.Refresh();
                labSerial.Text = Utils.Utils.GetSerialHHD(true);
                labSerial.Refresh();
                Version version = Assembly.GetEntryAssembly().GetName().Version;
                this.Text = "Biometrika License Check Tool v" + version.ToString();
                this.txtURL.Text = Properties.Settings.Default.URLLicenseCenter;
            }
            catch (Exception ex)
            {
                LOG.Error("frmLicense.frmLicense_Load - Excp Error: " + ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            rtbInfo.Text = "";
            labStatus.Text = "";
            labStatus.Refresh();
        }
    }
}
