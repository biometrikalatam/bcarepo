﻿namespace FE3DExampleDE
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtValueId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTypeId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDocumentId = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMsgToRead = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.picFEMPhoto = new System.Windows.Forms.PictureBox();
            this.picFEMQR = new System.Windows.Forms.PictureBox();
            this.btnFEM = new System.Windows.Forms.Button();
            this.btnGeneraDE = new System.Windows.Forms.Button();
            this.btnVer = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnGeneraDEWebApi = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFEMPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFEMQR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtCompany);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtValueId);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtTypeId);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtMail);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.txtDocumentId);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.txtMsgToRead);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.richTextBox1);
            this.groupBox1.Controls.Add(this.picFEMPhoto);
            this.groupBox1.Controls.Add(this.picFEMQR);
            this.groupBox1.Controls.Add(this.btnFEM);
            this.groupBox1.Location = new System.Drawing.Point(24, 116);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(760, 290);
            this.groupBox1.TabIndex = 86;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(71, 61);
            this.txtCompany.Margin = new System.Windows.Forms.Padding(2);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(157, 20);
            this.txtCompany.TabIndex = 97;
            this.txtCompany.Text = "Mundo Pacífico";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 64);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 96;
            this.label3.Text = "Company";
            // 
            // txtValueId
            // 
            this.txtValueId.Location = new System.Drawing.Point(510, 28);
            this.txtValueId.Margin = new System.Windows.Forms.Padding(2);
            this.txtValueId.Name = "txtValueId";
            this.txtValueId.Size = new System.Drawing.Size(208, 20);
            this.txtValueId.TabIndex = 95;
            this.txtValueId.Text = "21284415-2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(447, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 94;
            this.label2.Text = "Value Id";
            // 
            // txtTypeId
            // 
            this.txtTypeId.Location = new System.Drawing.Point(216, 28);
            this.txtTypeId.Margin = new System.Windows.Forms.Padding(2);
            this.txtTypeId.Name = "txtTypeId";
            this.txtTypeId.Size = new System.Drawing.Size(208, 20);
            this.txtTypeId.TabIndex = 93;
            this.txtTypeId.Text = "RUT";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(137, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 92;
            this.label1.Text = "Type Id";
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(510, 61);
            this.txtMail.Margin = new System.Windows.Forms.Padding(2);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(226, 20);
            this.txtMail.TabIndex = 91;
            this.txtMail.Text = "gsuhit@gmail.com";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(464, 64);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(26, 13);
            this.label22.TabIndex = 90;
            this.label22.Text = "Mail";
            // 
            // txtDocumentId
            // 
            this.txtDocumentId.Location = new System.Drawing.Point(304, 61);
            this.txtDocumentId.Margin = new System.Windows.Forms.Padding(2);
            this.txtDocumentId.Name = "txtDocumentId";
            this.txtDocumentId.Size = new System.Drawing.Size(140, 20);
            this.txtDocumentId.TabIndex = 89;
            this.txtDocumentId.Text = "123456NN";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(232, 64);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 13);
            this.label21.TabIndex = 88;
            this.label21.Text = "Document Id";
            // 
            // txtMsgToRead
            // 
            this.txtMsgToRead.Location = new System.Drawing.Point(174, 85);
            this.txtMsgToRead.Margin = new System.Windows.Forms.Padding(2);
            this.txtMsgToRead.Name = "txtMsgToRead";
            this.txtMsgToRead.Size = new System.Drawing.Size(562, 20);
            this.txtMsgToRead.TabIndex = 87;
            this.txtMsgToRead.Text = "Yo, Gustavo Suhit, RUT 12345678-9, acepto contrato 123456NN";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(95, 88);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 13);
            this.label20.TabIndex = 86;
            this.label20.Text = "Msg To Read";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox1.Location = new System.Drawing.Point(169, 125);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(341, 135);
            this.richTextBox1.TabIndex = 84;
            this.richTextBox1.Text = "";
            // 
            // picFEMPhoto
            // 
            this.picFEMPhoto.BackColor = System.Drawing.Color.White;
            this.picFEMPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFEMPhoto.Location = new System.Drawing.Point(520, 125);
            this.picFEMPhoto.Margin = new System.Windows.Forms.Padding(2);
            this.picFEMPhoto.Name = "picFEMPhoto";
            this.picFEMPhoto.Size = new System.Drawing.Size(216, 135);
            this.picFEMPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFEMPhoto.TabIndex = 83;
            this.picFEMPhoto.TabStop = false;
            // 
            // picFEMQR
            // 
            this.picFEMQR.BackColor = System.Drawing.Color.White;
            this.picFEMQR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFEMQR.Location = new System.Drawing.Point(23, 125);
            this.picFEMQR.Margin = new System.Windows.Forms.Padding(2);
            this.picFEMQR.Name = "picFEMQR";
            this.picFEMQR.Size = new System.Drawing.Size(135, 135);
            this.picFEMQR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFEMQR.TabIndex = 82;
            this.picFEMQR.TabStop = false;
            // 
            // btnFEM
            // 
            this.btnFEM.Location = new System.Drawing.Point(5, 0);
            this.btnFEM.Margin = new System.Windows.Forms.Padding(2);
            this.btnFEM.Name = "btnFEM";
            this.btnFEM.Size = new System.Drawing.Size(117, 23);
            this.btnFEM.TabIndex = 81;
            this.btnFEM.Text = "FEM";
            this.btnFEM.UseVisualStyleBackColor = true;
            this.btnFEM.Click += new System.EventHandler(this.btnFEM_Click);
            // 
            // btnGeneraDE
            // 
            this.btnGeneraDE.Enabled = false;
            this.btnGeneraDE.Location = new System.Drawing.Point(454, 422);
            this.btnGeneraDE.Margin = new System.Windows.Forms.Padding(2);
            this.btnGeneraDE.Name = "btnGeneraDE";
            this.btnGeneraDE.Size = new System.Drawing.Size(154, 23);
            this.btnGeneraDE.TabIndex = 92;
            this.btnGeneraDE.Text = "Generar DE con FE3D (WS)";
            this.btnGeneraDE.UseVisualStyleBackColor = true;
            this.btnGeneraDE.Click += new System.EventHandler(this.btnGeneraDE_Click);
            // 
            // btnVer
            // 
            this.btnVer.Location = new System.Drawing.Point(290, 422);
            this.btnVer.Margin = new System.Windows.Forms.Padding(2);
            this.btnVer.Name = "btnVer";
            this.btnVer.Size = new System.Drawing.Size(147, 23);
            this.btnVer.TabIndex = 93;
            this.btnVer.Text = "Ver DE con FE3D";
            this.btnVer.UseVisualStyleBackColor = true;
            this.btnVer.Click += new System.EventHandler(this.btnVer_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(44, 422);
            this.btnBorrar.Margin = new System.Windows.Forms.Padding(2);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(147, 23);
            this.btnBorrar.TabIndex = 94;
            this.btnBorrar.Text = "Borrar";
            this.btnBorrar.UseVisualStyleBackColor = true;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = global::FE3DExampleDE.Properties.Resources.NV_RGB_CF;
            this.pictureBox3.Location = new System.Drawing.Point(571, 12);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(213, 98);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 97;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::FE3DExampleDE.Properties.Resources.Biometrika_Bajada_400x140;
            this.pictureBox2.Location = new System.Drawing.Point(24, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(326, 98);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 96;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::FE3DExampleDE.Properties.Resources.FE3D_BIG_50_50_80;
            this.pictureBox1.Location = new System.Drawing.Point(364, 37);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(195, 73);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 95;
            this.pictureBox1.TabStop = false;
            // 
            // btnGeneraDEWebApi
            // 
            this.btnGeneraDEWebApi.Enabled = false;
            this.btnGeneraDEWebApi.Location = new System.Drawing.Point(612, 422);
            this.btnGeneraDEWebApi.Margin = new System.Windows.Forms.Padding(2);
            this.btnGeneraDEWebApi.Name = "btnGeneraDEWebApi";
            this.btnGeneraDEWebApi.Size = new System.Drawing.Size(172, 23);
            this.btnGeneraDEWebApi.TabIndex = 98;
            this.btnGeneraDEWebApi.Text = "Generar DE con FE3D (WebAPI)";
            this.btnGeneraDEWebApi.UseVisualStyleBackColor = true;
            this.btnGeneraDEWebApi.Click += new System.EventHandler(this.btnGeneraDEWebApi_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 460);
            this.Controls.Add(this.btnGeneraDEWebApi);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.btnVer);
            this.Controls.Add(this.btnGeneraDE);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "FE3D - Ejemplo de Docuemnto Electrónico con FE3D...";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFEMPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFEMQR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtDocumentId;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtMsgToRead;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.PictureBox picFEMPhoto;
        private System.Windows.Forms.PictureBox picFEMQR;
        private System.Windows.Forms.Button btnFEM;
        private System.Windows.Forms.Button btnGeneraDE;
        private System.Windows.Forms.TextBox txtValueId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTypeId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnVer;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnGeneraDEWebApi;
    }
}

