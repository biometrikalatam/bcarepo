﻿using Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Bio.Document.Manager.Api;

namespace FE3DExampleDE
{
    public partial class Form1 : Form
    {

        private BS _BS;
        private string path;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnFEM_Click(object sender, EventArgs e)
        {
            string queryString = "?src=FEM&TypeId=" + txtTypeId.Text +
                                  "&ValueId=" + txtValueId.Text +
                                  "&Mail=" + txtMail.Text +
                                  "&Company=" + txtCompany.Text +
                                  "&DocumentId=" + txtDocumentId.Text +
                                  "&MsgToRead=" + txtMsgToRead.Text;

            string absoluteURL = "http://localhost:9191";  //ConfigurationManager.AppSettings["AbsoluteURL"];
            if (!absoluteURL.EndsWith("/"))
                absoluteURL = absoluteURL + "/";

            absoluteURL = absoluteURL + queryString;

            //txtResponse.Text = txtResponse.Text + absoluteURL + Environment.NewLine;

            WebRequest request = WebRequest.Create(absoluteURL);
            //request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
            request.Timeout = 13000000;

            string json;
            FEMResponse fEMResponse;
            using (WebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                    fEMResponse = JsonConvert.DeserializeObject<FEMResponse>(json);
                }
            }

            ShowResult(fEMResponse);
        }

        private int ParseFEMtoBS(string fem, out BS bs)
        {
            int ret = 0;
            bs = new BS();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = false;
                xmlDoc.LoadXml(fem);
                XmlNode node = xmlDoc.GetElementsByTagName("ExtensionItem")[1];
                bs.BiometricSignature = node.ChildNodes[1].InnerText;
                bs.BiometricSignatureW = 200;
                bs.BiometricSignatureH = 200;
                bs.BiometricProvider = "Notario Virtual";
                bs.BiometricSignatureType = 41;
                node = xmlDoc.GetElementsByTagName("ExtensionItem")[2];
                bs.IdentityId = node.ChildNodes[1].InnerText;
                bs.IdnetityName = node.ChildNodes[1].InnerText;
                node = xmlDoc.GetElementsByTagName("ExtensionItem")[7];
                bs.QRVerify = node.ChildNodes[1].InnerText;
                bs.BSId = xmlDoc.FirstChild.FirstChild.Attributes["reciboID"].Value;
                bs.NVreceiptId = xmlDoc.FirstChild.FirstChild.Attributes["reciboID"].Value;
                bs.TimestampSignature = xmlDoc.FirstChild.LastChild.ChildNodes[1].InnerText;
                bs.NVReceipttimestamp = xmlDoc.FirstChild.LastChild.ChildNodes[1].InnerText;
                bs.TSASerialNumber = xmlDoc.FirstChild.LastChild.ChildNodes[2].InnerText;
                bs.TSA = xmlDoc.FirstChild.LastChild.ChildNodes[3].InnerText;
                bs.TSADateTimeSign = xmlDoc.FirstChild.LastChild.ChildNodes[1].InnerText;
                bs.TSAPolicy = xmlDoc.FirstChild.LastChild.ChildNodes[0].InnerText;
                bs.TSAURLVerify = xmlDoc.FirstChild.LastChild.ChildNodes[5].InnerText;
            }
            catch (Exception ex)
            {

            }

            return 0;
        }


        private void ShowResult(FEMResponse fEMResponse)
        {
            try
            {
                if (fEMResponse != null && !String.IsNullOrEmpty(fEMResponse.FEM))
                {
                    string FEM = fEMResponse.FEM;

                    path = @"\de\fem_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml";
                    System.IO.File.WriteAllText(path, FEM);

                    if (ParseFEMtoBS(FEM, out _BS) == 0)
                    {
                        btnGeneraDE.Enabled = true;
                        btnGeneraDEWebApi.Enabled = true;
                    }
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.PreserveWhitespace = false;
                    xmlDoc.LoadXml(FEM);
                    string foto;
                    string qr;
                    if (FEM.StartsWith("<RD"))
                    {
                        XmlNode node = xmlDoc.GetElementsByTagName("ExtensionItem")[1];
                        foto = node.ChildNodes[1].InnerText;
                        XmlNode node1 = xmlDoc.GetElementsByTagName("ExtensionItem")[7];
                        qr = node1.ChildNodes[1].InnerText;
                    }
                    else
                    {
                        foto = null;
                        qr = null;
                    }

                    if (qr != null)
                    {
                        byte[] byImgQR = Convert.FromBase64String(qr);
                        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgQR);
                        Bitmap bmp = new Bitmap(Image.FromStream(m));
                        m.Close();
                        picFEMQR.Image = bmp;
                    }

                    if (foto != null)
                    {
                        byte[] byImgF = Convert.FromBase64String(foto);
                        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgF);
                        Bitmap bmp = new Bitmap(Image.FromStream(m));
                        m.Close();
                        picFEMPhoto.Image = bmp;
                    }
                    foreach (XmlNode item in xmlDoc.GetElementsByTagName("ExtensionItem"))
                    {
                        if (!item.ChildNodes[0].InnerText.Equals("Video") && !item.ChildNodes[0].InnerText.Equals("Photo")
                            && !item.ChildNodes[0].InnerText.Equals("qrlinkfem"))
                            richTextBox1.Text = richTextBox1.Text + item.ChildNodes[0].InnerText + "=" + item.ChildNodes[1].InnerText + Environment.NewLine;
                    }

                }
                //    XmlDocument xmlDocABS = new XmlDocument();
                //    xmlDocABS.PreserveWhitespace = false;
                //    xmlDocABS.LoadXml(abs);

                //    // Check arguments.
                //    if (xmlDoc != null)
                //    {
                //        //                        string sample = xmlDoc.GetElementsByTagName("Sample")[0].InnerText;
                //        string sample = xmlDocABS.GetElementsByTagName("Sample")[0].InnerText;
                //    }
                //} else
                //{

            }
            catch (Exception ex)
            {


            }
        }

        private void btnGeneraDE_Click(object sender, EventArgs e)
        {
            string xmlOut;
            string msgerr;
            

            try
            {
                int ret = HELPERS.DEHelper.DEGenerate(Properties.Settings.Default.FE3DExampleDE_WSDE_WSDE, txtDocumentId.Text, _BS, txtMail.Text, txtTypeId.Text, txtValueId.Text, 
                    false, out xmlOut, out msgerr, out path);
                if (ret != 0) {
                    MessageBox.Show("Error -> " + ret.ToString() + " - " + msgerr);
                } else
                {
                    btnVer.Enabled = true;
                }
                

            }
            catch (Exception ex)
            {

            }
            
        }

        private void btnVer_Click(object sender, EventArgs e)
        {
            try
            {
                //path = @"\de\de_20181204184825.pdf";
                frmView frw = new frmView();
                frw.path = path;
                frw.Show(this);
            }
            catch (Exception ex)
            {
                
            }
        }

        private void btnGeneraDEWebApi_Click(object sender, EventArgs e)
        {
            string xmlOut;
            string msgerr;


            try
            {
                int ret = HELPERS.DEHelper.DEGenerateWebApi(Properties.Settings.Default.FE3DExampleDE_WSDE_WSDE, txtDocumentId.Text, _BS, txtMail.Text, txtTypeId.Text, txtValueId.Text,
                    false, out xmlOut, out msgerr, out path);
                if (ret != 0)
                {
                    MessageBox.Show("Error -> " + ret.ToString() + " - " + msgerr);
                }
                else
                {
                    btnVer.Enabled = true;
                }


            }
            catch (Exception ex)
            {

            }
        }
    }
}
