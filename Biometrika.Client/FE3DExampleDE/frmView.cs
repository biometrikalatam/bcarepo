﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FE3DExampleDE
{
    public partial class frmView : Form
    {

        public string path;
        public frmView()
        {
            InitializeComponent();
        }

        private void frmView_Load(object sender, EventArgs e)
        {
            axAcroPDF1.LoadFile(path);
            this.Refresh();
        }
    }
}
