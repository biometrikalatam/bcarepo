﻿using Bio.Document.Manager.Api;
using Bio.Document.Manager.Api.Rest;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FE3DExampleDE.HELPERS
{
    internal class DEHelper
    {
        internal static int DEGenerate(string url, string documentid, BS bs, string mail, string typeid, string valueid, bool notarized, out string xmlOutDE, out string msg, out string path)
        {
            int ret = 0;
            msg = null;
            string xmlparamin;
            string xmlparamout;
            Bio.Document.Manager.Api.XmlParamOut oParamout;
            path = null;
            xmlOutDE = null;

            try
            {
                Bio.Document.Manager.Api.XmlParamIn oParamin = new Bio.Document.Manager.Api.XmlParamIn();
                oParamin.Actionid = 1;  //1 - Generar desde template word | 2 - Firmar un PDF | 3 - Get Documento
                oParamin.IdDE = 7;     //Id de tipo de documento documento, informado por Biometrika cuando se crea un nuevo tipo de documento   

                oParamin.EmployeeId = "11111111-1";  //RUT persona que envia a generar el documento o firmar (puede ser generico)
                oParamin.Enduser = "Test";           //Nombre persona que envia a generar el documento o firmar (puede ser generico)          
                oParamin.Ipenduser = "127.0.0.1";    //IP Estacion de trabajo donde se genera el documento
                oParamin.Company = 1;                //Id de compañia informada por Biometrika en el momento de dar de alta el servicio
                oParamin.User = "User";              //User para acceso al servicio
                oParamin.Password = "Password";      //Password para acceso al servicio
                oParamin.TypeId = typeid;            //Tipo de documento del cliente del contrato (Normalemnte en Chile RUT)
                oParamin.ValueId = valueid;          //Valor de documento del cliente del contrato (Normalemnte en Chile RUT xxxxxxxx-y)

                //Cargo los tags a reemplazar en el template del documento word.
                oParamin.Tags = new List<Bio.Document.Manager.Api.Tag>();
                Bio.Document.Manager.Api.Tag item;
                item = new Bio.Document.Manager.Api.Tag();
                item.IdTag = "#NROCONTRATO#";
                item.value = documentid;
                oParamin.Tags.Add(item);

                item = new Bio.Document.Manager.Api.Tag();
                item.IdTag = "#FECHA#";
                item.value = "Santiago, " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                oParamin.Tags.Add(item);
                item = new Bio.Document.Manager.Api.Tag();
                item.IdTag = "#RUT#";
                item.value = valueid;
                oParamin.Tags.Add(item);

                oParamin.BSSignatures = new List<Bio.Document.Manager.Api.BS>();
                oParamin.BSSignatures.Add(bs);
                xmlparamin = Bio.Document.Manager.Api.XmlUtils.SerializeAnObject(oParamin);

                using (WSDE.WSDE ws = new WSDE.WSDE())
                {
                    ws.Timeout = 200000;
                    ws.Url = url;

                    //Cargo el/los mail/s donde se enviará/n copia/s del documento generado 
                    oParamin.Notarize = notarized;
                    string[] mails = new string[1];
                    mails[0] = mail;
                    oParamin.MailsToNotify = mails;

                    string sxIN = Bio.Document.Manager.Api.XmlUtils.SerializeObject<Bio.Document.Manager.Api.XmlParamIn>(oParamin);

                    //LLamo al al web service de generación con parámetros 
                    ret = ws.Process(sxIN, out xmlparamout);
                    xmlOutDE = xmlparamout;

                    //Deserializo respuesta y greabo documento generado y/o firmado
                    XmlParamOut pout = XmlUtils.DeserializeObject<XmlParamOut>(xmlOutDE);
                    path = @"\de\de_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                    System.IO.File.WriteAllBytes(path, Convert.FromBase64String(pout.De));
                }
             
            }
            catch (Exception ex)
            {
                msg = "Error Desconocido Notarizando = " + ex.Message;
                ret = -1;
            }

            return ret;
        }

        internal static int DEGenerateWebApi(string url, string documentid, BS bs, string mail, string typeid, string valueid, bool notarized, out string xmlOutDE, out string msg, out string path)
        {
            int ret = 0;
            msg = null;
            //string oParamin;
            string xmlparamout;
            Bio.Document.Manager.Api.XmlParamOut oParamout;
            path = null;
            xmlOutDE = null;

            try
            {
                Bio.Document.Manager.Api.XmlParamIn oParamin = new Bio.Document.Manager.Api.XmlParamIn();
                oParamin.Actionid = 1;  //1 - Generar desde template word | 2 - Firmar un PDF | 3 - Get Documento
                oParamin.IdDE = 7;     //Id de tipo de documento documento, informado por Biometrika cuando se crea un nuevo tipo de documento   

                oParamin.EmployeeId = "11111111-1";  //RUT persona que envia a generar el documento o firmar (puede ser generico)
                oParamin.Enduser = "Test";           //Nombre persona que envia a generar el documento o firmar (puede ser generico)          
                oParamin.Ipenduser = "127.0.0.1";    //IP Estacion de trabajo donde se genera el documento
                oParamin.Company = 1;                //Id de compañia informada por Biometrika en el momento de dar de alta el servicio
                oParamin.User = "User";              //User para acceso al servicio
                oParamin.Password = "Password";      //Password para acceso al servicio
                oParamin.TypeId = typeid;            //Tipo de documento del cliente del contrato (Normalemnte en Chile RUT)
                oParamin.ValueId = valueid;          //Valor de documento del cliente del contrato (Normalemnte en Chile RUT xxxxxxxx-y)

                //Cargo los tags a reemplazar en el template del documento word.
                oParamin.Tags = new List<Bio.Document.Manager.Api.Tag>();
                Bio.Document.Manager.Api.Tag item;
                item = new Bio.Document.Manager.Api.Tag();
                item.IdTag = "#NROCONTRATO#";
                item.value = documentid;
                oParamin.Tags.Add(item);

                item = new Bio.Document.Manager.Api.Tag();
                item.IdTag = "#FECHA#";
                item.value = "Santiago, " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                oParamin.Tags.Add(item);
                item = new Bio.Document.Manager.Api.Tag();
                item.IdTag = "#RUT#";
                item.value = valueid;
                oParamin.Tags.Add(item);

                oParamin.BSSignatures = new List<Bio.Document.Manager.Api.BS>();
                oParamin.BSSignatures.Add(bs);
                //xmlparamin = Bio.Document.Manager.Api.XmlUtils.SerializeAnObject(oParamin);
                oParamin.Notarize = notarized;
                string[] mails = new string[1];
                mails[0] = mail;
                oParamin.MailsToNotify = mails;

                API API = new API(ConfigurationManager.AppSettings["API_Url"]);

                int timeOut = 0;
                if (!Int32.TryParse(ConfigurationManager.AppSettings["API_TimeOut"], out timeOut))
                    timeOut = 30000;

                XmlParamOut xmlParamOut = API.Process(oParamin, timeOut);

                if (xmlParamOut != null)
                {
                    ret = xmlParamOut.ExecutionResult;
                    xmlOutDE = xmlParamOut.De;

                    //Deserializo respuesta y greabo documento generado y/o firmado
                    path = @"\de\de_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                    System.IO.File.WriteAllBytes(path, Convert.FromBase64String(xmlOutDE));

                }

                //using (WSDE.WSDE ws = new WSDE.WSDE())
                //{
                //    ws.Timeout = 200000;
                //    ws.Url = url;

                //    //Cargo el/los mail/s donde se enviará/n copia/s del documento generado 
                //    oParamin.Notarize = notarized;
                //    string[] mails = new string[1];
                //    mails[0] = mail;
                //    oParamin.MailsToNotify = mails;

                //    string sxIN = Bio.Document.Manager.Api.XmlUtils.SerializeObject<Bio.Document.Manager.Api.XmlParamIn>(oParamin);

                //    //LLamo al al web service de generación con parámetros 
                //    ret = ws.Process(sxIN, out xmlparamout);
                //    xmlOutDE = xmlparamout;

                //    //Deserializo respuesta y greabo documento generado y/o firmado
                //    XmlParamOut pout = XmlUtils.DeserializeObject<XmlParamOut>(xmlOutDE);
                //    path = @"\de\de_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                //    System.IO.File.WriteAllBytes(path, Convert.FromBase64String(pout.De));
                //}

            }
            catch (Exception ex)
            {
                msg = "Error Desconocido Notarizando = " + ex.Message;
                ret = -1;
            }

            return ret;
        }
    }
}
