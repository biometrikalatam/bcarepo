﻿using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

using System.Web.Script.Serialization;
using BviCamUIAdapter;

namespace BviCamManager
{
    public class BviCamManager : IManager  
    {
        public Biometrika.BioApi20.BSP.BSPBiometrika _BSP;
        private bool _IS_BPS_CONFIGURED = false;
        private string templateToken = string.Empty;

        private static readonly ILog LOG = LogManager.GetLogger(typeof(BviCamManager));
        private bool isOpen = false;
        private Dictionary<string, object> output = new Dictionary<string, object>();
        NameValueCollection queryStringPass = null;
        public BviCamUIAdapter.App app;

        public BviCamManager()
        {
            LOG.Info("Se solicita BviUI");
        }  // ctor para el relexion

        public BviCamManager(NameValueCollection queryString)
        {
            LOG.Info("BviCamManager In...");
            LOG.Info("If IsOpened:" + isOpen);
            if (!isOpen)
            {
                try
                {
                    LOG.Info("BviCamManager isOpen...");
                    isOpen = true;
                    LOG.Info("BviCamManager SetPacket => " + queryString);
                    queryStringPass = queryString;
                         // el program recibe el pedido  
                    LOG.Debug("BviCamManager Main...");
                    
                    Thread thread = new Thread(() =>
                    {



                        ////Configuracion configuracion = new Configuracion();
                        ////configuracion.SetPacket(queryStringPass);
                        //////configuracion.OpenWindow();
                        //LOG.Info("BVIManager.. Iniciando componente");
                        //ScanBarCode scanbarcode = new ScanBarCode();
                        //scanbarcode.SetPacket(queryStringPass);        

                        //scanbarcode.ShowDialog();

                        ZxingScanForm a = new ZxingScanForm();
                        a.SetPacket(queryStringPass);
                        a.ShowDialog();  

                        //scanbarcode.Close();
                        //scanbarcode.Activate();

                        ////LOG.Info("Biometrika BVICam.. Se definen los parámetros");
                        ////scanbarcode.Activate();

                        ////scanbarcode.ShowDialog();
                        ////scanbarcode.Activate();

                        //////WpfSingleInstance.Make();
                        //Dictionary<string, object> outputUI = scanbarcode.Packet.PacketParamOut;
                        Dictionary<string, object> outputUI = a.Packet.PacketParamOut;

                        //output = scanbarcode.Packet.PacketParamOut;
                        output = a.Packet.PacketParamOut;



                    });
                    thread.SetApartmentState(ApartmentState.STA);                    
                    thread.Start();                    
                    thread.Join();
                    

                    //BviCamUIAdapter.App.SetPacket(queryStringPass);
                    //BviCamUIAdapter.App.Main();
                    //Dictionary<string, object> outputUI = BviCamUIAdapter.App.Packet.PacketParamOut;
                    //output = BviCamUIAdapter.App.Packet.PacketParamOut;

                    //Thread t = new Thread(new ThreadStart(StartNewStaThread));
                    //// Make sure to set the apartment state BEFORE starting the thread. 
                    //t.ApartmentState = ApartmentState.STA;
                    //t.Start();
                    //Thread.Sleep(1000);

                }
                catch (Exception exe)
                {
                    LOG.Error("exe:" + exe.Message, exe);
                }            
            }
        }

        public BviCamManager(NameValueCollection queryString, object objBSP)
        {
            LOG.Info("BviCamManager.BviCamManager In...");
            LOG.Info("If IsOpened:" + isOpen);
            if (!isOpen)
            {
                try
                {
                    LOG.Info("BviCamManager.BviCamManager isOpen...");
                    isOpen = true;
                    if (objBSP != null)
                    {
                        _BSP = (Biometrika.BioApi20.BSP.BSPBiometrika)objBSP;
                    }
                    BviCamUIAdapter.Program.SetBSP(_BSP);
                    LOG.Debug("BviCamManager.BviCamManager SetPacket => " + queryString);
                    BviCamUIAdapter.Program.SetPacket(queryString);     // el program recibe el pedido
                    LOG.Debug("BviCamManager.BviCamManager Mail...");
                    BviCamUIAdapter.Program.Main();

                    Dictionary<string, object> outputUI = BviCamUIAdapter.Program.Packet.PacketParamOut;
                    LOG.Debug("BviCamManager.BviCamManager outupUI.Length = " + outputUI.Count);

                    // Este param out retorna la imagen de la captura y las minucias
                    if (outputUI != null && outputUI.Count > 0)
                    {
                        output.Add("Token", outputUI.ContainsKey("Token") ? outputUI["Token"] : null);
                        LOG.Debug("BviCamUIAdapter => Token = " + (outputUI.ContainsKey("Token") ? outputUI["Token"] : "NO Token"));
                        output.Add("ImageSample", outputUI.ContainsKey("ImageSample") ? outputUI["ImageSample"] : null);
                        LOG.Debug("BviCamUIAdapter => ImageSample = " + (outputUI.ContainsKey("ImageSample") ? outputUI["ImageSample"] : "NO ImageSample"));
                        output.Add("Error", outputUI.ContainsKey("Error") ? outputUI["Error"] : null);
                        LOG.Debug("BviCamUIAdapter => Error = " + (outputUI.ContainsKey("Error") ? outputUI["Error"] : "NO Error"));
                        output.Add("BodyPart", outputUI.ContainsKey("BodyPart") ? outputUI["BodyPart"] : 0);
                        LOG.Debug("BviCamUIAdapter => BodyPart = " + (outputUI.ContainsKey("BodyPart") ? outputUI["BodyPart"] : "NO BodyPart"));
                        output.Add("Width", outputUI.ContainsKey("Width") ? outputUI["Width"] : 0);
                        LOG.Debug("BviCamUIAdapter => Width = " + (outputUI.ContainsKey("Width") ? outputUI["Token"] : "NO Width"));
                        output.Add("Height", outputUI.ContainsKey("Height") ? outputUI["Height"] : 0);
                        LOG.Debug("BviCamUIAdapter => Height = " + (outputUI.ContainsKey("Height") ? outputUI["Height"] : "NO Height"));
                        output.Add("Hash", outputUI.ContainsKey("Hash") ? outputUI["Hash"] : 0);
                        LOG.Debug("BviCamUIAdapter => Hash = " + (outputUI.ContainsKey("Hash") ? outputUI["Hash"] : "NO Hash"));
                        LOG.Info("BviCamUIAdapter Seting retorno...");
                    }

                    //LOG.Info("BviCamManager SetPacket => " + queryString);
                    //queryStringPass = queryString;
                    //// el program recibe el pedido  
                    //LOG.Debug("BviCamManager Main...");

                    //Thread thread = new Thread(() =>
                    //{
                    //    ZxingScanForm a = new ZxingScanForm();
                    //    a.SetPacket(queryStringPass);
                    //    a.ShowDialog();
                    //    Dictionary<string, object> outputUI = a.Packet.PacketParamOut;
                    //    output = a.Packet.PacketParamOut;
                    //});
                    //thread.SetApartmentState(ApartmentState.STA);
                    //thread.Start();
                    //thread.Join();
                }
                catch (Exception exe)
                {
                    LOG.Error("BviCamManager.BviCamManager Error" + exe.Message, exe);
                }
            }

            
            LOG.Info("BviCamManager.BviCamManager Out!");
        }
        

        public string Buffer => new JavaScriptSerializer().Serialize(output);

        public string Tag { get => "BVICAM"; }
    }
}
