﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Certification Identity Manager")]
[assembly: AssemblyDescription("Certificador de identidad de Notario Virtual")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Notario Virtual")]
[assembly: AssemblyProduct("CIManager")]
[assembly: AssemblyCopyright("Copyright © Notario Virtual 2019")]
[assembly: AssemblyTrademark("Notario Virtual")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("9ac29c40-de0e-40dd-a2e7-b6a470e4aa9e")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.1.0.0")]
[assembly: AssemblyFileVersion("2.1.0.0")]
