﻿using Domain;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace CIManager
{
    public class CIManager: IManager
    {
        private bool isOpen = false;
        private Dictionary<string, object> output = new Dictionary<string, object>();

        public CIManager()
        {
        }  // ctor para el relexion

        public CIManager(NameValueCollection queryString) 
        {
            if (!isOpen)
            { 
                isOpen = true;
                CIUIAdapter.Program.SetPacket(queryString);     // el program recibe el pedido
                CIUIAdapter.Program.Main();

                output = CIUIAdapter.Program.Packet.PacketParamOut;

            }
        }

        public string Buffer => new JavaScriptSerializer().Serialize(output);

        public string Tag { get => "CI"; }
    }


}
