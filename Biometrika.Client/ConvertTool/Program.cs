﻿using Leadtools.Multimedia;
using LMVIOvLyLib;
using LMVTextOverlayLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvertTool
{
    class Program
    {
        
        //public delegate void EventCompleteConvert(object sender, ErrorAbortEventArgs e);
        static bool converting = false;
        static UnlockSupport unlocksprt;
        [STAThread]
        static void Main(string[] args)
        {
            //EventCompleteConvert ev;
            //ConvertCtrl ConvertCtrl1; // = new ConvertCtrl();
            //string path;

            try 
            {

                frmConvert frm = new frmConvert();
                frm.path = args[0];
                frm.pathFileWM = args[1];
                frm.Company = args[2];
                frm.TypeId = args[3];
                frm.ValueId = args[4];
                frm.DocumentId = args[5];
                //frm.ShowDialog();
                
                Application.Run(frm);
                //frm.StartConvert();

                //while (frm.IsWorking) { }


                //unlocksprt = new UnlockSupport();
                //unlocksprt.Unlock("");
                ////ev = new EventCompleteConvert(ConvertCtrl1_Complete);
                ////ctlProgress.Visible = true;
                ////ctlProgress.Value = 0;
                ////ctlProgress.Maximum = 100;
                ////labFeedback.Visible = true;
                ////Converting = true;

                ////if (ConvertCtrl1 != null)
                ////{
                ////    ConvertCtrl1.ErrorAbort -= this.ConvertCtrl1_ErrorAbort;
                ////    ConvertCtrl1.Complete -= this.ConvertCtrl1_Complete;
                ////    ConvertCtrl1.Progress -= this.ConvertCtrl1_Progress;
                ////    ConvertCtrl1.Dispose();
                ////    ConvertCtrl1 = null;
                ////}
                //converting = true;
                //ConvertCtrl1 = new ConvertCtrl();

                //ConvertCtrl1.TargetFormat = Leadtools.Multimedia.TargetFormatType.ISO;
                //ConvertCtrl1.VideoCompressors.H264.Selected = true;
                //ConvertCtrl1.AudioCompressors.AAC.Selected = true;
                ////ConvertCtrl1.TargetFormat = TargetFormatType.ISO;
                ////ConvertCtrl1.AudioCompressors.Selection = Properties.Settings.Default.FEAudioCompressorId;
                ////ConvertCtrl1.VideoCompressors.Selection = Properties.Settings.Default.FEVideoCompressorId;
                //ConvertCtrl1.SourceFile = args[0]; // CaptureCtrl1.TargetFile;
                //ConvertCtrl1.TargetFile = args[0] + ".mp4"; // CaptureCtrl1.TargetFile + ".compressed";
                //ConvertCtrl1.ErrorAbort += ConvertCtrl1_ErrorAbort;
                ////EventCompleteConvert += ConvertCtrl1_Complete;
                //ConvertCtrl1.Progress += ConvertCtrl1_Progress;
                //ConvertCtrl1.Complete += ConvertCtrl1_Complete;
                ////ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.ImageOverlay);
                ////// get the the image overlay filter object
                ////LMVIOvLy _ImageOverLay = (LMVIOvLy)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor);
                ////_ImageOverLay.OverlayImageFileName = @".\disclaimer_big.bmp";
                ////_ImageOverLay.set_XPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);
                ////_ImageOverLay.set_YPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);

                //ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.ImageOverlay);
                //// get the the image overlay filter object
                //LMVIOvLy _ImageOverLay = (LMVIOvLy)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor);
                //_ImageOverLay.OverlayImageFileName = args[1]; // @".\disclaimer_big.bmp";
                //_ImageOverLay.set_XPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);
                //_ImageOverLay.set_YPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);

                ////Adding Text OverLay
                //ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.TextOverlay);

                //// get the the text overlay filter object
                //LMVTextOverlay _overlay = (LMVTextOverlay)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor + 1);
                //_overlay.OverlayText = args[2];
                //_overlay.ViewRectLeft = 0;
                //_overlay.ViewRectTop = 0;
                //_overlay.ViewRectRight = 800;
                //_overlay.ViewRectBottom = 600;
                //_overlay.EnableXYPosition = true;
                //_overlay.XPos = 80;
                //_overlay.YPos = 130;
                //_overlay.FontSize = 12;
                //_overlay.Bold = true;
                //_overlay.AutoReposToViewRect = true;

                //ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.TextOverlay);
                //LMVTextOverlay _overlay1 = (LMVTextOverlay)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor + 2);
                //_overlay1.OverlayText = args[3] + " " + args[4];
                //_overlay1.ViewRectLeft = 0;
                //_overlay1.ViewRectTop = 0;
                //_overlay1.ViewRectRight = 300;
                //_overlay1.ViewRectBottom = 600;
                //_overlay.WordWrap = true;
                //_overlay1.EnableXYPosition = true;
                //_overlay1.XPos = 80;
                //_overlay1.YPos = 170;
                //_overlay1.FontSize = 10;
                ////_overlay1.FontColor = 16777215;
                //_overlay1.AutoReposToViewRect = true;

                //ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.TextOverlay);
                //LMVTextOverlay _overlay2 = (LMVTextOverlay)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor + 3);
                //_overlay2.OverlayText = args[5];
                //_overlay2.ViewRectLeft = 0;
                //_overlay2.ViewRectTop = 0;
                //_overlay2.ViewRectRight = 800;
                //_overlay2.ViewRectBottom = 600;
                //_overlay2.EnableXYPosition = true;
                //_overlay2.XPos = 80;
                //_overlay2.YPos = 210;
                //_overlay2.FontSize = 10;
                ////_overlay1.FontColor = 16777215;
                //_overlay2.AutoReposToViewRect = true;


                //ConvertCtrl1.StartConvert();

                ////while (converting) { }

                ////System.Threading.Thread.CurrentThread.Join();
                ////System.IO.FileInfo fi = new System.IO.FileInfo(args[0]);
                ////long len = fi.Length;
                ////int lenmp4 = 

                //System.Threading.Thread.Sleep(20000);  // Properties.Settings1.Default.ConvertTimeToSave);
                //while (converting) { }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            //Environment.Exit(0);
        }

        static private void ConvertCtrl1_ErrorAbort(object sender, ErrorAbortEventArgs e)
        {
            //Converting = false; 
            //UpdateWizardStatus(3, 1);
            //_CurrentError = -12;
            //_LastError = "Error proceanso video ErrorAbort [" + e.errorMessage + "]";
            //System.Console.WriteLine("Error ConvertCtrl1_ErrorAbort - " + e.errorMessage);


        }

        static private void ConvertCtrl1_Progress(object sender, ProgressEventArgs e)
        {
            //System.Console.Write(" System.Convert.ToString(e.time)" + "% complete");
            //ctlProgress.Value = e.time;
        }

        static private void ConvertCtrl1_Complete(object sender, EventArgs e)
        {
            //System.Console.WriteLine("100% complete");
            //ctlProgress.Value = ctlProgress.Maximum;
            //System.Threading.Thread.Sleep(500);
            converting = false;
            //Converting = false;
            //UpdateWizardStatus(3, 0);
            //if (Properties.Settings.Default.FEStep3Automatic)
            //{
            //    timerNotarizar.Enabled = true;
            //}
            unlocksprt.Lock("");
            Environment.Exit(-1);
        }

    }
}
