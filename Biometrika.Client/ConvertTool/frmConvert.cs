﻿using Leadtools.Multimedia;
using LMVIOvLyLib;
using LMVTextOverlayLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvertTool
{
    public partial class frmConvert : Form
    {
        UnlockSupport unlocksprt;
        public bool IsWorking = false;
        public int CodeErr = -1;
        public string MsgErr = null;

        public string path;
        public string pathFileWM;
        public string Company;
        public string TypeId;
        public string ValueId;
        public string DocumentId;

        public frmConvert()
        {
            InitializeComponent();

           
        }

        private void convertCtrl1_ErrorAbort(object sender, Leadtools.Multimedia.ErrorAbortEventArgs e)
        {
            CodeErr = -2;
            MsgErr = e.errorcode.ToString() + " - " + e.errorMessage;
            IsWorking = false;
        }

        private void convertCtrl1_Complete(object sender, EventArgs e)
        {
            timerClose.Enabled = true;
            IsWorking = false;
        }

        private void convertCtrl1_Progress(object sender, Leadtools.Multimedia.ProgressEventArgs e)
        {
            WriteCaptureStatus(e.percent);
            int i = e.time;
            //ctlProgress.Value = (i > Properties.Settings.Default.FETimeLimit + 1) ? Properties.Settings.Default.FETimeLimit + 1 : i;
           

        }
    

        public void WriteCaptureStatus(int percent)
        {
            string s = null;
            int lDropped = 0;
            int lNotDropped = 0;
            double Time = 0;
            double fps = 0;

            s = "Procesado => " + System.Convert.ToString(percent) + " %";
            label1.Text = s;
           
        }

        //string path, string pathFileWM, string Company, string TypeId, string ValueId, string DocumentId
        public int StartConvert()
        {
            //EventCompleteConvert ev;

            try
            {
                unlocksprt = new UnlockSupport();
                unlocksprt.Unlock("");
                //ev = new EventCompleteConvert(ConvertCtrl1_Complete);
                //ctlProgress.Visible = true;
                //ctlProgress.Value = 0;
                //ctlProgress.Maximum = 100;
                //labFeedback.Visible = true;
                //Converting = true;

                //if (ConvertCtrl1 != null)
                //{
                //    ConvertCtrl1.ErrorAbort -= this.ConvertCtrl1_ErrorAbort;
                //    ConvertCtrl1.Complete -= this.ConvertCtrl1_Complete;
                //    ConvertCtrl1.Progress -= this.ConvertCtrl1_Progress;
                //    ConvertCtrl1.Dispose();
                //    ConvertCtrl1 = null;
                //}
                
                //ConvertCtrl1 = new ConvertCtrl();

                ConvertCtrl1.TargetFormat = Leadtools.Multimedia.TargetFormatType.ISO;
                ConvertCtrl1.VideoCompressors.H264.Selected = true;
                ConvertCtrl1.AudioCompressors.AAC.Selected = true;
                //ConvertCtrl1.TargetFormat = TargetFormatType.ISO;
                //ConvertCtrl1.AudioCompressors.Selection = Properties.Settings.Default.FEAudioCompressorId;
                //ConvertCtrl1.VideoCompressors.Selection = Properties.Settings.Default.FEVideoCompressorId;
                ConvertCtrl1.SourceFile = path; // CaptureCtrl1.TargetFile;
                ConvertCtrl1.TargetFile = path + ".mp4"; // CaptureCtrl1.TargetFile + ".compressed";
                //ConvertCtrl1.ErrorAbort += ConvertCtrl1_ErrorAbort;
                ////EventCompleteConvert += ConvertCtrl1_Complete;
                //ConvertCtrl1.Progress += ConvertCtrl1_Progress;
                //ConvertCtrl1.Complete += ConvertCtrl1_Complete;
                //ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.ImageOverlay);
                //// get the the image overlay filter object
                //LMVIOvLy _ImageOverLay = (LMVIOvLy)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor);
                //_ImageOverLay.OverlayImageFileName = @".\disclaimer_big.bmp";
                //_ImageOverLay.set_XPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);
                //_ImageOverLay.set_YPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);

                ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.ImageOverlay);
                // get the the image overlay filter object
                LMVIOvLy _ImageOverLay = (LMVIOvLy)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor);
                _ImageOverLay.OverlayImageFileName = pathFileWM; // @".\disclaimer_big.bmp";
                _ImageOverLay.set_XPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);
                _ImageOverLay.set_YPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);

                //Adding Text OverLay
                ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.TextOverlay);

                // get the the text overlay filter object
                LMVTextOverlay _overlay = (LMVTextOverlay)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor + 1);
                _overlay.OverlayText = Company;
                _overlay.ViewRectLeft = 0;
                _overlay.ViewRectTop = 0;
                _overlay.ViewRectRight = 800;
                _overlay.ViewRectBottom = 600;
                _overlay.EnableXYPosition = true;
                _overlay.XPos = 80;
                _overlay.YPos = 130;
                _overlay.FontSize = 12;
                _overlay.Bold = true;
                _overlay.AutoReposToViewRect = true;

                ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.TextOverlay);
                LMVTextOverlay _overlay1 = (LMVTextOverlay)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor + 2);
                _overlay1.OverlayText = TypeId + " " + ValueId;
                _overlay1.ViewRectLeft = 0;
                _overlay1.ViewRectTop = 0;
                _overlay1.ViewRectRight = 300;
                _overlay1.ViewRectBottom = 600;
                _overlay.WordWrap = true;
                _overlay1.EnableXYPosition = true;
                _overlay1.XPos = 80;
                _overlay1.YPos = 170;
                _overlay1.FontSize = 10;
                //_overlay1.FontColor = 16777215;
                _overlay1.AutoReposToViewRect = true;

                ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.TextOverlay);
                LMVTextOverlay _overlay2 = (LMVTextOverlay)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor + 3);
                _overlay2.OverlayText = DocumentId;
                _overlay2.ViewRectLeft = 0;
                _overlay2.ViewRectTop = 0;
                _overlay2.ViewRectRight = 800;
                _overlay2.ViewRectBottom = 600;
                _overlay2.EnableXYPosition = true;
                _overlay2.XPos = 80;
                _overlay2.YPos = 210;
                _overlay2.FontSize = 10;
                //_overlay1.FontColor = 16777215;
                _overlay2.AutoReposToViewRect = true;

                IsWorking = true;
                ConvertCtrl1.StartConvert();

                //while (converting) { }

                //System.Threading.Thread.CurrentThread.Join();
                //System.IO.FileInfo fi = new System.IO.FileInfo(args[0]);
                //long len = fi.Length;
                //int lenmp4 = 

                //System.Threading.Thread.Sleep(20000);  // Properties.Settings1.Default.ConvertTimeToSave);
                //while (converting) { }

                CodeErr = 0;
            }
            catch (Exception ex)
            {
                MsgErr = ex.Message;
                CodeErr = -1;
            }
            return CodeErr;
        }

        private void timerClose_Tick(object sender, EventArgs e)
        {
            timerClose.Enabled = false;
            unlocksprt.Lock("");
            this.Close();
        }

        private void frmConvert_Load(object sender, EventArgs e)
        {
            StartConvert();
        }
    }
}
