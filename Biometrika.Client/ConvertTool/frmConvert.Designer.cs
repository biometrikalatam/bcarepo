﻿namespace ConvertTool
{
    partial class frmConvert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ConvertCtrl1 = new Leadtools.Multimedia.ConvertCtrl();
            this.timerClose = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ConvertCtrl1)).BeginInit();
            this.SuspendLayout();
            // 
            // ConvertCtrl1
            // 
            this.ConvertCtrl1.AudioCompressors.Selection = -1;
            this.ConvertCtrl1.Location = new System.Drawing.Point(39, 25);
            this.ConvertCtrl1.Name = "ConvertCtrl1";
            this.ConvertCtrl1.OcxState = null;
            this.ConvertCtrl1.Size = new System.Drawing.Size(191, 97);
            this.ConvertCtrl1.SourceFile = null;
            this.ConvertCtrl1.TabIndex = 0;
            this.ConvertCtrl1.TargetDevices.Selection = -1;
            this.ConvertCtrl1.TargetFile = null;
            this.ConvertCtrl1.Text = "convertCtrl1";
            this.ConvertCtrl1.VideoCompressors.Selection = -1;
            this.ConvertCtrl1.WMProfile.Description = "";
            this.ConvertCtrl1.WMProfile.Name = "";
            this.ConvertCtrl1.Complete += new System.EventHandler(this.convertCtrl1_Complete);
            this.ConvertCtrl1.ErrorAbort += new Leadtools.Multimedia.ErrorAbortEventHandler(this.convertCtrl1_ErrorAbort);
            this.ConvertCtrl1.Progress += new Leadtools.Multimedia.ProgressEventHandler(this.convertCtrl1_Progress);
            // 
            // timerClose
            // 
            this.timerClose.Interval = 200;
            this.timerClose.Tick += new System.EventHandler(this.timerClose_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // frmConvert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 281);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ConvertCtrl1);
            this.Name = "frmConvert";
            this.Text = "frmConvert";
            this.Load += new System.EventHandler(this.frmConvert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ConvertCtrl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Leadtools.Multimedia.ConvertCtrl ConvertCtrl1;
        private System.Windows.Forms.Timer timerClose;
        private System.Windows.Forms.Label label1;
    }
}