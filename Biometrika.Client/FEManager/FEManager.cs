﻿using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;
using System.Web.Script.Serialization;

namespace FEManager
{
    public class FEManager : IManager
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(FEManager));

        //para desblouear licencia y bloquearla desde aca.
        private UnlockSupport unlocksprt = new UnlockSupport(); 

        private bool isOpen = false;
        private Dictionary<string, object> output = new Dictionary<string, object>();
        private string templateToken = string.Empty;
        public string Buffer => new JavaScriptSerializer().Serialize(output);

        public string Tag { get => "FEM"; } 
        
        public FEManager() { } // ctor de reflextion


        public FEManager(NameValueCollection queryString)
        {
            LOG.Info("FEManager In..."); 
            //Log.Add("BioPortalClient7Manager In...");
            if (!isOpen)
            {
                unlocksprt.Unlock(null);
                LOG.Debug("FEManager isOpen...");
                isOpen = true;
                LOG.Debug("FEManager SetPacket => " + queryString); 
                FEUIAdapter.Program.SetPacket(queryString);     // el program recibe el pedido
                LOG.Debug("FEManager Mail...");
                //FEUIAdapter.Program.Main();
                Dictionary<string, object> outputUI = null;  
                Thread thread = new Thread(() => 
                {
                    //doc.LoadFromHTML("file:///C:/contrato/s.html", true, true, false, setting);
                    FEUIAdapter.Program.Main(); 
                    outputUI = FEUIAdapter.Program.Packet.PacketParamOut;
                });
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
                thread.Join();

                thread.Abort();
                thread = null;

                //Dictionary<string, object> outputUI = FEUIAdapter.Program.Packet.PacketParamOut;
                LOG.Debug("FEManager outupUI.Length = " + outputUI.Count);

                // Este param out retorna la imagen de la captura y las minucias
                if (outputUI != null && outputUI.Count > 0)
                { 
                    //output.Add("Video", outputUI.ContainsKey("Video") ? outputUI["Video"] : null);
                    //LOG.Debug("FEManager => Video = " + (outputUI.ContainsKey("Video") ? outputUI["Video"] : "NO Video"));
                    //output.Add("Photo", outputUI.ContainsKey("Photo") ? outputUI["Photo"] : null);
                    //LOG.Debug("FEManager => Photo = " + (outputUI.ContainsKey("Photo") ? outputUI["Photo"] : "NO Photo"));
                    output.Add("Error", outputUI.ContainsKey("Error") ? outputUI["Error"] : null);
                    LOG.Debug("FEManager => Error = " + (outputUI.ContainsKey("Error") ? outputUI["Error"] : "NO Error"));
                    output.Add("DeviceId", outputUI.ContainsKey("DeviceId") ? outputUI["DeviceId"] : 0);
                    LOG.Debug("FEManager => DeviceId = " + (outputUI.ContainsKey("DeviceId") ? outputUI["DeviceId"] : "NO DeviceId"));
                    output.Add("FEM", outputUI.ContainsKey("FEM") ? outputUI["FEM"] : null);
                    LOG.Debug("FEManager => FEM = " + (outputUI.ContainsKey("FEM") ? outputUI["FEM"] : "NO Error"));
                    output.Add("Hash", outputUI.ContainsKey("Hash") ? outputUI["Hash"] : 0);
                    LOG.Debug("FEManager => Hash = " + (outputUI.ContainsKey("Hash") ? outputUI["Hash"] : "NO Hash"));
                    LOG.Info("FEManager Seting retorno...");
                    //thread.Abort();
                    
                }
                unlocksprt.Lock(null);
            }
            LOG.Info("FEManager Out!");
        }
    }

    public class MsgError
    {

        public MsgError() { }

        public MsgError(string strerr)
        {
            try
            {
                if (String.IsNullOrEmpty(strerr))
                {
                    Codigo = -1;
                    Description = "Retorno Nulo";
                }
                else
                {
                    string[] arrerr = strerr.Split('|');
                    Codigo = Convert.ToInt32(arrerr[0]);
                    Description = arrerr[1];
                } 
            }
            catch (Exception ex)
            {
                Description = "Error parseando error return [" + ex.Message + "]";
            }
        }

        public int Codigo { get; set; }
        public string Description { get; set; }

    }
}
