﻿namespace BiometrikaClient.Test
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.label17 = new System.Windows.Forms.Label();
            this.txtTheme = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.cmbAction = new System.Windows.Forms.ComboBox();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txttimeout = new System.Windows.Forms.TextBox();
            this.txtSerie = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSexo = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtConnectorId = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMsjCaptura = new System.Windows.Forms.TextBox();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBioportal = new System.Windows.Forms.TextBox();
            this.txtResponse = new System.Windows.Forms.TextBox();
            this.cmbMinutiaeType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbOperationType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbAuthenticationFactor = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbDevice = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbTokenContent = new System.Windows.Forms.ComboBox();
            this.txtValueId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTypeID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCaptura = new System.Windows.Forms.Button();
            this.txtCompanyId = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtVS = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDocumentId = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMsgToRead = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.picFEMPhoto = new System.Windows.Forms.PictureBox();
            this.picFEMQR = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imgHuella = new System.Windows.Forms.PictureBox();
            this.txtBPShow = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button6 = new System.Windows.Forms.Button();
            this.picCICedula = new System.Windows.Forms.PictureBox();
            this.button4 = new System.Windows.Forms.Button();
            this.txtMailCI = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.rtbCIResult = new System.Windows.Forms.RichTextBox();
            this.picCIFotoCedula = new System.Windows.Forms.PictureBox();
            this.picCIQR = new System.Windows.Forms.PictureBox();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtOp = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFEMPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFEMQR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgHuella)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCICedula)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCIFotoCedula)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCIQR)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(20, 462);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(118, 13);
            this.label17.TabIndex = 78;
            this.label17.Text = "theme_ [blue | ligthblue]";
            // 
            // txtTheme
            // 
            this.txtTheme.Location = new System.Drawing.Point(63, 434);
            this.txtTheme.Margin = new System.Windows.Forms.Padding(2);
            this.txtTheme.Name = "txtTheme";
            this.txtTheme.Size = new System.Drawing.Size(98, 20);
            this.txtTheme.TabIndex = 77;
            this.txtTheme.Text = "theme_ligthgreen";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(14, 437);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 75;
            this.label16.Text = "Theme";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(36, 136);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(90, 13);
            this.label32.TabIndex = 76;
            this.label32.Text = "Action del XMLIN";
            // 
            // cmbAction
            // 
            this.cmbAction.FormattingEnabled = true;
            this.cmbAction.Location = new System.Drawing.Point(137, 132);
            this.cmbAction.Name = "cmbAction";
            this.cmbAction.Size = new System.Drawing.Size(290, 21);
            this.cmbAction.TabIndex = 74;
            // 
            // txtToken
            // 
            this.txtToken.Location = new System.Drawing.Point(166, 424);
            this.txtToken.Margin = new System.Windows.Forms.Padding(2);
            this.txtToken.Multiline = true;
            this.txtToken.Name = "txtToken";
            this.txtToken.Size = new System.Drawing.Size(620, 118);
            this.txtToken.TabIndex = 73;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(89, 407);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 13);
            this.label15.TabIndex = 72;
            this.label15.Text = "Mili Segundos";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(0, 407);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 13);
            this.label14.TabIndex = 71;
            this.label14.Text = "Timeout";
            // 
            // txttimeout
            // 
            this.txttimeout.Location = new System.Drawing.Point(49, 404);
            this.txttimeout.Margin = new System.Windows.Forms.Padding(2);
            this.txttimeout.Name = "txttimeout";
            this.txttimeout.Size = new System.Drawing.Size(36, 20);
            this.txttimeout.TabIndex = 70;
            this.txttimeout.Text = "8000";
            // 
            // txtSerie
            // 
            this.txtSerie.Location = new System.Drawing.Point(520, 159);
            this.txtSerie.Margin = new System.Windows.Forms.Padding(2);
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.Size = new System.Drawing.Size(260, 20);
            this.txtSerie.TabIndex = 69;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(471, 162);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 68;
            this.label13.Text = "Serie";
            // 
            // txtSexo
            // 
            this.txtSexo.Location = new System.Drawing.Point(520, 135);
            this.txtSexo.Margin = new System.Windows.Forms.Padding(2);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.Size = new System.Drawing.Size(76, 20);
            this.txtSexo.TabIndex = 67;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(470, 139);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 13);
            this.label12.TabIndex = 66;
            this.label12.Text = "Sexo";
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(520, 114);
            this.txtApellido.Margin = new System.Windows.Forms.Padding(2);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(260, 20);
            this.txtApellido.TabIndex = 65;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(458, 117);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 64;
            this.label11.Text = "Apellido";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(520, 89);
            this.txtName.Margin = new System.Windows.Forms.Padding(2);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(260, 20);
            this.txtName.TabIndex = 63;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(455, 92);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 62;
            this.label10.Text = "Nombre";
            // 
            // txtConnectorId
            // 
            this.txtConnectorId.Location = new System.Drawing.Point(137, 190);
            this.txtConnectorId.Margin = new System.Windows.Forms.Padding(2);
            this.txtConnectorId.Name = "txtConnectorId";
            this.txtConnectorId.Size = new System.Drawing.Size(299, 20);
            this.txtConnectorId.TabIndex = 61;
            this.txtConnectorId.Text = "FINAL";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(61, 190);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 60;
            this.label9.Text = "ConnectorId";
            // 
            // txtMsjCaptura
            // 
            this.txtMsjCaptura.Location = new System.Drawing.Point(166, 215);
            this.txtMsjCaptura.Margin = new System.Windows.Forms.Padding(2);
            this.txtMsjCaptura.Multiline = true;
            this.txtMsjCaptura.Name = "txtMsjCaptura";
            this.txtMsjCaptura.Size = new System.Drawing.Size(616, 41);
            this.txtMsjCaptura.TabIndex = 59;
            // 
            // btnEnviar
            // 
            this.btnEnviar.Location = new System.Drawing.Point(703, 282);
            this.btnEnviar.Margin = new System.Windows.Forms.Padding(2);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(77, 20);
            this.btnEnviar.TabIndex = 58;
            this.btnEnviar.Text = "Enviar";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(163, 268);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 57;
            this.label8.Text = "BioPortal";
            // 
            // txtBioportal
            // 
            this.txtBioportal.Location = new System.Drawing.Point(167, 283);
            this.txtBioportal.Margin = new System.Windows.Forms.Padding(2);
            this.txtBioportal.Name = "txtBioportal";
            this.txtBioportal.Size = new System.Drawing.Size(510, 20);
            this.txtBioportal.TabIndex = 56;
            this.txtBioportal.Text = "http://localhost:4030/BioPortal.Server.WS.Web.asmx";
            // 
            // txtResponse
            // 
            this.txtResponse.Location = new System.Drawing.Point(166, 307);
            this.txtResponse.Margin = new System.Windows.Forms.Padding(2);
            this.txtResponse.Multiline = true;
            this.txtResponse.Name = "txtResponse";
            this.txtResponse.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResponse.Size = new System.Drawing.Size(620, 113);
            this.txtResponse.TabIndex = 55;
            // 
            // cmbMinutiaeType
            // 
            this.cmbMinutiaeType.FormattingEnabled = true;
            this.cmbMinutiaeType.Location = new System.Drawing.Point(137, 161);
            this.cmbMinutiaeType.Margin = new System.Windows.Forms.Padding(2);
            this.cmbMinutiaeType.Name = "cmbMinutiaeType";
            this.cmbMinutiaeType.Size = new System.Drawing.Size(299, 21);
            this.cmbMinutiaeType.TabIndex = 54;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(52, 159);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 53;
            this.label7.Text = "Minutiae Type";
            // 
            // cmbOperationType
            // 
            this.cmbOperationType.FormattingEnabled = true;
            this.cmbOperationType.Location = new System.Drawing.Point(15, 235);
            this.cmbOperationType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbOperationType.Name = "cmbOperationType";
            this.cmbOperationType.Size = new System.Drawing.Size(143, 21);
            this.cmbOperationType.TabIndex = 52;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(23, 220);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 13);
            this.label6.TabIndex = 51;
            this.label6.Text = "Operation Type del Token";
            // 
            // cmbAuthenticationFactor
            // 
            this.cmbAuthenticationFactor.FormattingEnabled = true;
            this.cmbAuthenticationFactor.Location = new System.Drawing.Point(137, 108);
            this.cmbAuthenticationFactor.Margin = new System.Windows.Forms.Padding(2);
            this.cmbAuthenticationFactor.Name = "cmbAuthenticationFactor";
            this.cmbAuthenticationFactor.Size = new System.Drawing.Size(299, 21);
            this.cmbAuthenticationFactor.TabIndex = 50;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 111);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 13);
            this.label5.TabIndex = 49;
            this.label5.Text = "Authentication Factor";
            // 
            // cmbDevice
            // 
            this.cmbDevice.FormattingEnabled = true;
            this.cmbDevice.Location = new System.Drawing.Point(137, 83);
            this.cmbDevice.Margin = new System.Windows.Forms.Padding(2);
            this.cmbDevice.Name = "cmbDevice";
            this.cmbDevice.Size = new System.Drawing.Size(299, 21);
            this.cmbDevice.TabIndex = 48;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(86, 89);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 47;
            this.label4.Text = "Device";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 66);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 46;
            this.label3.Text = "Token Content";
            // 
            // cmbTokenContent
            // 
            this.cmbTokenContent.FormattingEnabled = true;
            this.cmbTokenContent.Location = new System.Drawing.Point(137, 60);
            this.cmbTokenContent.Margin = new System.Windows.Forms.Padding(2);
            this.cmbTokenContent.Name = "cmbTokenContent";
            this.cmbTokenContent.Size = new System.Drawing.Size(299, 21);
            this.cmbTokenContent.TabIndex = 45;
            // 
            // txtValueId
            // 
            this.txtValueId.Location = new System.Drawing.Point(137, 38);
            this.txtValueId.Margin = new System.Windows.Forms.Padding(2);
            this.txtValueId.Name = "txtValueId";
            this.txtValueId.Size = new System.Drawing.Size(299, 20);
            this.txtValueId.TabIndex = 44;
            this.txtValueId.Text = "21284415-2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 42);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "ValueId";
            // 
            // txtTypeID
            // 
            this.txtTypeID.Location = new System.Drawing.Point(137, 14);
            this.txtTypeID.Margin = new System.Windows.Forms.Padding(2);
            this.txtTypeID.Name = "txtTypeID";
            this.txtTypeID.Size = new System.Drawing.Size(299, 20);
            this.txtTypeID.TabIndex = 42;
            this.txtTypeID.Text = "RUT";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(89, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 41;
            this.label1.Text = "Typeid";
            // 
            // btnCaptura
            // 
            this.btnCaptura.Location = new System.Drawing.Point(26, 493);
            this.btnCaptura.Margin = new System.Windows.Forms.Padding(2);
            this.btnCaptura.Name = "btnCaptura";
            this.btnCaptura.Size = new System.Drawing.Size(117, 23);
            this.btnCaptura.TabIndex = 40;
            this.btnCaptura.Text = "Capturar";
            this.btnCaptura.UseVisualStyleBackColor = true;
            this.btnCaptura.Click += new System.EventHandler(this.btnCaptura_Click);
            // 
            // txtCompanyId
            // 
            this.txtCompanyId.Location = new System.Drawing.Point(686, 260);
            this.txtCompanyId.Margin = new System.Windows.Forms.Padding(2);
            this.txtCompanyId.Name = "txtCompanyId";
            this.txtCompanyId.Size = new System.Drawing.Size(76, 20);
            this.txtCompanyId.TabIndex = 82;
            this.txtCompanyId.Text = "8";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(618, 263);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(60, 13);
            this.label18.TabIndex = 81;
            this.label18.Text = "CompanyId";
            // 
            // txtVS
            // 
            this.txtVS.Location = new System.Drawing.Point(520, 259);
            this.txtVS.Margin = new System.Windows.Forms.Padding(2);
            this.txtVS.Name = "txtVS";
            this.txtVS.Size = new System.Drawing.Size(76, 20);
            this.txtVS.TabIndex = 84;
            this.txtVS.Text = "CI";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(420, 263);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(96, 13);
            this.label19.TabIndex = 83;
            this.label19.Text = "Verification Source";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.txtMail);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.txtDocumentId);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.txtMsgToRead);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.richTextBox1);
            this.groupBox1.Controls.Add(this.picFEMPhoto);
            this.groupBox1.Controls.Add(this.picFEMQR);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(26, 559);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(760, 241);
            this.groupBox1.TabIndex = 85;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(5, 30);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 23);
            this.button2.TabIndex = 92;
            this.button2.Text = "Firmar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(465, 37);
            this.txtMail.Margin = new System.Windows.Forms.Padding(2);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(271, 20);
            this.txtMail.TabIndex = 91;
            this.txtMail.Text = "gsuhit@gmail.com";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(428, 40);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(26, 13);
            this.label22.TabIndex = 90;
            this.label22.Text = "Mail";
            // 
            // txtDocumentId
            // 
            this.txtDocumentId.Location = new System.Drawing.Point(216, 37);
            this.txtDocumentId.Margin = new System.Windows.Forms.Padding(2);
            this.txtDocumentId.Name = "txtDocumentId";
            this.txtDocumentId.Size = new System.Drawing.Size(208, 20);
            this.txtDocumentId.TabIndex = 89;
            this.txtDocumentId.Text = "123456NN";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(137, 40);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 13);
            this.label21.TabIndex = 88;
            this.label21.Text = "Document Id";
            // 
            // txtMsgToRead
            // 
            this.txtMsgToRead.Location = new System.Drawing.Point(216, 13);
            this.txtMsgToRead.Margin = new System.Windows.Forms.Padding(2);
            this.txtMsgToRead.Name = "txtMsgToRead";
            this.txtMsgToRead.Size = new System.Drawing.Size(520, 20);
            this.txtMsgToRead.TabIndex = 87;
            this.txtMsgToRead.Text = "Yo, Gustavo Suhit, RUT 12345678-9, acepto contrato 123456NN";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(137, 16);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 13);
            this.label20.TabIndex = 86;
            this.label20.Text = "Msg To Read";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(174, 66);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(410, 157);
            this.richTextBox1.TabIndex = 84;
            this.richTextBox1.Text = "";
            // 
            // picFEMPhoto
            // 
            this.picFEMPhoto.BackColor = System.Drawing.Color.White;
            this.picFEMPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFEMPhoto.Location = new System.Drawing.Point(601, 66);
            this.picFEMPhoto.Margin = new System.Windows.Forms.Padding(2);
            this.picFEMPhoto.Name = "picFEMPhoto";
            this.picFEMPhoto.Size = new System.Drawing.Size(135, 135);
            this.picFEMPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFEMPhoto.TabIndex = 83;
            this.picFEMPhoto.TabStop = false;
            // 
            // picFEMQR
            // 
            this.picFEMQR.BackColor = System.Drawing.Color.White;
            this.picFEMQR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFEMQR.Location = new System.Drawing.Point(23, 66);
            this.picFEMQR.Margin = new System.Windows.Forms.Padding(2);
            this.picFEMQR.Name = "picFEMQR";
            this.picFEMQR.Size = new System.Drawing.Size(135, 135);
            this.picFEMQR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFEMQR.TabIndex = 82;
            this.picFEMQR.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(5, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 23);
            this.button1.TabIndex = 81;
            this.button1.Text = "FEM";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::BiometrikaClient.Test.Properties.Resources.Biometrika_Bajada_400x150;
            this.pictureBox1.Location = new System.Drawing.Point(484, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(296, 69);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 79;
            this.pictureBox1.TabStop = false;
            // 
            // imgHuella
            // 
            this.imgHuella.BackColor = System.Drawing.Color.PeachPuff;
            this.imgHuella.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgHuella.Location = new System.Drawing.Point(18, 260);
            this.imgHuella.Margin = new System.Windows.Forms.Padding(2);
            this.imgHuella.Name = "imgHuella";
            this.imgHuella.Size = new System.Drawing.Size(136, 140);
            this.imgHuella.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgHuella.TabIndex = 39;
            this.imgHuella.TabStop = false;
            // 
            // txtBPShow
            // 
            this.txtBPShow.Location = new System.Drawing.Point(520, 187);
            this.txtBPShow.Margin = new System.Windows.Forms.Padding(2);
            this.txtBPShow.Name = "txtBPShow";
            this.txtBPShow.Size = new System.Drawing.Size(98, 20);
            this.txtBPShow.TabIndex = 87;
            this.txtBPShow.Text = "1|2|6|7";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(471, 190);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(45, 13);
            this.label23.TabIndex = 86;
            this.label23.Text = "Enrolled";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(621, 185);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(86, 23);
            this.button3.TabIndex = 88;
            this.button3.Text = "Get Enrolled";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.picCICedula);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.txtMailCI);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.rtbCIResult);
            this.groupBox2.Controls.Add(this.picCIFotoCedula);
            this.groupBox2.Controls.Add(this.picCIQR);
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Location = new System.Drawing.Point(819, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(463, 783);
            this.groupBox2.TabIndex = 89;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Certificacion de Identidad";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(321, 755);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(117, 23);
            this.button6.TabIndex = 90;
            this.button6.Text = "Borrar";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // picCICedula
            // 
            this.picCICedula.BackColor = System.Drawing.Color.White;
            this.picCICedula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picCICedula.Location = new System.Drawing.Point(25, 104);
            this.picCICedula.Margin = new System.Windows.Forms.Padding(2);
            this.picCICedula.Name = "picCICedula";
            this.picCICedula.Size = new System.Drawing.Size(164, 112);
            this.picCICedula.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCICedula.TabIndex = 89;
            this.picCICedula.TabStop = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(31, 755);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(117, 23);
            this.button4.TabIndex = 88;
            this.button4.Text = "Test Show CI ";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtMailCI
            // 
            this.txtMailCI.Location = new System.Drawing.Point(58, 29);
            this.txtMailCI.Margin = new System.Windows.Forms.Padding(2);
            this.txtMailCI.Name = "txtMailCI";
            this.txtMailCI.Size = new System.Drawing.Size(193, 20);
            this.txtMailCI.TabIndex = 87;
            this.txtMailCI.Text = "gsuhit@gmail.com";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(28, 32);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(26, 13);
            this.label26.TabIndex = 86;
            this.label26.Text = "Mail";
            // 
            // rtbCIResult
            // 
            this.rtbCIResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.rtbCIResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbCIResult.Location = new System.Drawing.Point(25, 226);
            this.rtbCIResult.Name = "rtbCIResult";
            this.rtbCIResult.Size = new System.Drawing.Size(413, 504);
            this.rtbCIResult.TabIndex = 84;
            this.rtbCIResult.Text = "";
            // 
            // picCIFotoCedula
            // 
            this.picCIFotoCedula.BackColor = System.Drawing.Color.White;
            this.picCIFotoCedula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picCIFotoCedula.Location = new System.Drawing.Point(207, 104);
            this.picCIFotoCedula.Margin = new System.Windows.Forms.Padding(2);
            this.picCIFotoCedula.Name = "picCIFotoCedula";
            this.picCIFotoCedula.Size = new System.Drawing.Size(105, 112);
            this.picCIFotoCedula.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCIFotoCedula.TabIndex = 83;
            this.picCIFotoCedula.TabStop = false;
            // 
            // picCIQR
            // 
            this.picCIQR.BackColor = System.Drawing.Color.White;
            this.picCIQR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picCIQR.Location = new System.Drawing.Point(329, 104);
            this.picCIQR.Margin = new System.Windows.Forms.Padding(2);
            this.picCIQR.Name = "picCIQR";
            this.picCIQR.Size = new System.Drawing.Size(109, 112);
            this.picCIQR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCIQR.TabIndex = 82;
            this.picCIQR.TabStop = false;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(25, 69);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(117, 23);
            this.button5.TabIndex = 81;
            this.button5.Text = "CI";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.txtOp);
            this.groupBox3.Controls.Add(this.button7);
            this.groupBox3.Location = new System.Drawing.Point(1288, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(212, 227);
            this.groupBox3.TabIndex = 90;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "BVI ATM...";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(15, 25);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(22, 13);
            this.label24.TabIndex = 89;
            this.label24.Text = "OP";
            // 
            // txtOp
            // 
            this.txtOp.Location = new System.Drawing.Point(41, 21);
            this.txtOp.Margin = new System.Windows.Forms.Padding(2);
            this.txtOp.Name = "txtOp";
            this.txtOp.Size = new System.Drawing.Size(119, 20);
            this.txtOp.TabIndex = 88;
            this.txtOp.Text = "1";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(18, 49);
            this.button7.Margin = new System.Windows.Forms.Padding(2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(117, 23);
            this.button7.TabIndex = 82;
            this.button7.Text = "Call BVI ATM";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1512, 812);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txtBPShow);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtVS);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtCompanyId);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtTheme);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.cmbAction);
            this.Controls.Add(this.txtToken);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txttimeout);
            this.Controls.Add(this.txtSerie);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtSexo);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtApellido);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtConnectorId);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtMsjCaptura);
            this.Controls.Add(this.btnEnviar);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtBioportal);
            this.Controls.Add(this.txtResponse);
            this.Controls.Add(this.cmbMinutiaeType);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbOperationType);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbAuthenticationFactor);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbDevice);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbTokenContent);
            this.Controls.Add(this.txtValueId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTypeID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCaptura);
            this.Controls.Add(this.imgHuella);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Test Biometrika Client...";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFEMPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFEMQR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgHuella)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCICedula)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCIFotoCedula)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCIQR)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTheme;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox cmbAction;
        private System.Windows.Forms.TextBox txtToken;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txttimeout;
        private System.Windows.Forms.TextBox txtSerie;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtSexo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtConnectorId;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMsjCaptura;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBioportal;
        private System.Windows.Forms.TextBox txtResponse;
        private System.Windows.Forms.ComboBox cmbMinutiaeType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbOperationType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbAuthenticationFactor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbDevice;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbTokenContent;
        private System.Windows.Forms.TextBox txtValueId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTypeID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCaptura;
        private System.Windows.Forms.PictureBox imgHuella;
        private System.Windows.Forms.TextBox txtCompanyId;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtVS;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.PictureBox picFEMPhoto;
        private System.Windows.Forms.PictureBox picFEMQR;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtDocumentId;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtMsgToRead;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtBPShow;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtMailCI;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.RichTextBox rtbCIResult;
        private System.Windows.Forms.PictureBox picCIFotoCedula;
        private System.Windows.Forms.PictureBox picCIQR;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.PictureBox picCICedula;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtOp;
        private System.Windows.Forms.Button button7;
    }
}

