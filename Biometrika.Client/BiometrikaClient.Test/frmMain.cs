﻿using BCR.Bio.Enum;
using BCR.Bio.Enum.BioPortal;
using Bio.Core.Api;
using BioPortal.Server.Api;
using Domain;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace BiometrikaClient.Test
{
    public partial class frmMain : Form
    {

        TokenResponse serverResponse;

        public frmMain()
        {
            InitializeComponent();
        }

        private void btnCaptura_Click(object sender, EventArgs e)
        {
            string hashGuid = Guid.NewGuid().ToString("N");
            string queryString = "?src=bpc7";
            queryString = queryString + "&TypeId=" + txtTypeID.Text;
            queryString = queryString + "&ValueId=" + txtValueId.Text;
            queryString = queryString + "&Tokencontent=" + (int)((TokenContentEnum)cmbTokenContent.SelectedItem);
            queryString = queryString + "&Device=" + (int)((DeviceEnum)cmbDevice.SelectedItem);
            queryString = queryString + "&AuthenticationFactor=" + (int)((AuthenticationFactorEnum)cmbAuthenticationFactor.SelectedItem);
            queryString = queryString + "&OperationType=" + (int)((OperationTypeEnum)cmbOperationType.SelectedItem);
            queryString = queryString + "&MinutiaeType=" + (int)((MinutiaeTypeEnum)cmbMinutiaeType.SelectedItem);
            queryString = queryString + "&Timeout=" + txttimeout.Text.Trim();
            queryString = queryString + "&Theme=" + txtTheme.Text.Trim();
            queryString = queryString + "&BodyPartShow=" + txtBPShow.Text.Trim();
            queryString = queryString + "&Hash=" + hashGuid;

            string absoluteURL = "http://localhost:9191";  //ConfigurationManager.AppSettings["AbsoluteURL"];
            if (!absoluteURL.EndsWith("/"))
                absoluteURL = absoluteURL + "/";

            absoluteURL = absoluteURL + queryString;

            txtResponse.Text = txtResponse.Text + absoluteURL + Environment.NewLine;

            WebRequest request = WebRequest.Create(absoluteURL);
            request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
            request.Timeout = 13000000;

            
            using (WebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    string json = reader.ReadToEnd();
                    serverResponse = JsonConvert.DeserializeObject<TokenResponse>(json);
                }
            }

            if (serverResponse.ResponseOK)
            {
                if (serverResponse.Hash == hashGuid)
                {
                    Byte[] bitmapData = Convert.FromBase64String(serverResponse.ImageSample);
                    MemoryStream streamBitmap = new MemoryStream(bitmapData);

                    imgHuella.Image = new Bitmap((Bitmap)Image.FromStream(streamBitmap));

                    txtMsjCaptura.Text = string.Concat("Se capturó Dedo => ", serverResponse.BodyPart);

                    txtToken.Text = "TOKEN => " + serverResponse.Token;
                }
                else
                    txtMsjCaptura.Text = "No coincide el hash";
            }
            else
                txtMsjCaptura.Text = "No hubo captura";
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            cmbTokenContent.DataSource = Enum.GetValues(typeof(TokenContentEnum));
            cmbTokenContent.SelectedItem = TokenContentEnum.ALL;

            cmbDevice.DataSource = Enum.GetValues(typeof(DeviceEnum));
            cmbDevice.SelectedItem = DeviceEnum.DEVICE_SECUGEN;

            cmbAuthenticationFactor.DataSource = Enum.GetValues(typeof(AuthenticationFactorEnum));
            cmbAuthenticationFactor.SelectedItem = AuthenticationFactorEnum.AUTHENTICATIONFACTOR_FINGERPRINT;

            cmbOperationType.DataSource = Enum.GetValues(typeof(OperationTypeEnum));
            cmbOperationType.SelectedItem = OperationTypeEnum.VERIFY;

            cmbMinutiaeType.DataSource = Enum.GetValues(typeof(MinutiaeTypeEnum));
            cmbMinutiaeType.SelectedItem = MinutiaeTypeEnum.MINUTIAETYPE_ANSI_INSITS_378_2004;

            cmbAction.DataSource = Enum.GetValues(typeof(ActionEnum));
            cmbAction.SelectedItem = ActionEnum.ACTION_VERIFY;
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            XmlParamIn xmlParamIn = new XmlParamIn();

            using (BioPortalWSWEB.BioPortalServerWSWeb ws = new BioPortalWSWEB.BioPortalServerWSWeb())
            {
                ws.Timeout = 99999;
                ws.Url = txtBioportal.Text;

                xmlParamIn.Actionid = (int)((ActionEnum)cmbAction.SelectedItem);

                xmlParamIn.Authenticationfactor = (int)((AuthenticationFactorEnum)cmbAuthenticationFactor.SelectedItem);  //Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;
                xmlParamIn.Minutiaetype = (int)((MinutiaeTypeEnum)cmbMinutiaeType.SelectedItem);

                xmlParamIn.Bodypart = serverResponse.BodyPart;

                xmlParamIn.Clientid = "Test-BiometrikaClient7";

                xmlParamIn.OperationOrder = 1; //Default
                xmlParamIn.Companyid = Convert.ToInt32(txtCompanyId.Text); // int.Parse(ConfigurationManager.AppSettings["CompanyId"]);
                xmlParamIn.Enduser = "Manager";
                xmlParamIn.Ipenduser = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();
                xmlParamIn.Matchingtype = 1;
                xmlParamIn.Origin = 2; //BP Manager
                if (!String.IsNullOrEmpty(serverResponse.Token))
                {
                    xmlParamIn.SampleCollection = new List<Sample>();
                    Sample sample = new Sample
                    {
                        Data = serverResponse.Token,
                        Minutiaetype = (int)MinutiaeTypeEnum.MINUTIAETYPE_TOKEN,
                        Additionaldata = null
                    };
                    xmlParamIn.SampleCollection.Add(sample);
                }
                else
                {
                    xmlParamIn.SampleCollection = null;
                }
                xmlParamIn.SaveVerified = 1;            /*  valor diferente */
                xmlParamIn.Threshold = 50; //!String.IsNullOrEmpty(txtUmbral.Text) ?
                //Convert.ToDouble(txtUmbral.Text) : 0;

                xmlParamIn.Userid = 1; // int.Parse(0);

                xmlParamIn.Verifybyconnectorid = String.IsNullOrEmpty(txtConnectorId.Text) ? "Final" : txtConnectorId.Text.Trim();
                xmlParamIn.InsertOption = 0; //Default    /*  valor diferente */

                //Datos de la Persona
                xmlParamIn.PersonalData = new PersonalData
                {
                    Typeid = txtTypeID.Text,
                    Valueid = txtValueId.Text,
                    Companyidenroll = 7, // int.Parse(ConfigurationManager.AppSettings["CompanyId"]),
                    Useridenroll = 1, // int.Parse(ConfigurationManager.AppSettings["UserId"]),
                    Patherlastname = txtApellido.Text,
                    //Motherlastname = registryModel.ApellidoMaterno,
                    //Photography = registryModel.Foto,
                    //Signatureimage = registryModel.Firma,
                    Name = txtName.Text,
                    Sex = txtSexo.Text,
                    Verificationsource = txtVS.Text,
                    Documentseriesnumber = txtSerie.Text,
                    //Documentexpirationdate = registryModel.FechaVencimiento,
                    //Birthdate = registryModel.FechaNacimiento,
                    //Birthplace = registryModel.LugarNacimiento,
                    //Nationality = registryModel.Nacionalidad,
                    //Profession = registryModel.Profesion,
                    //Visatype = registryModel.TipoVisa
                };

                //Serializamos.
                string xmlparamin = XmlUtils.SerializeAnObject(xmlParamIn);
                string xmlparamout = "";

                int respuesta;

                if ((OperationTypeEnum)cmbOperationType.SelectedItem == OperationTypeEnum.ENROLL)
                    respuesta = ws.Enroll(xmlparamin, out xmlparamout);

                if ((OperationTypeEnum)cmbOperationType.SelectedItem == OperationTypeEnum.VERIFY)
                    respuesta = ws.Verify(xmlparamin, out xmlparamout);

                xmlparamout = xmlparamout.Replace("&#", "");
                txtResponse.Text = txtResponse.Text + Environment.NewLine + xmlparamout;
            }
        }

        
        private void ShowResult(FEMResponse fEMResponse)
        {
            try
            {
                if (fEMResponse != null && !String.IsNullOrEmpty(fEMResponse.FEM)) {
                    string FEM = fEMResponse.FEM;
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.PreserveWhitespace = false;
                    xmlDoc.LoadXml(FEM);
                    string foto;
                    string qr;
                    if (FEM.StartsWith("<RD"))
                    {
                        XmlNode node = xmlDoc.GetElementsByTagName("ExtensionItem")[1];
                        foto = node.ChildNodes[1].InnerText;
                        XmlNode node1 = xmlDoc.GetElementsByTagName("ExtensionItem")[7];
                        qr = node1.ChildNodes[1].InnerText;
                    }
                    else
                    {
                        foto = null;
                        qr = null;
                    }

                    if (qr != null)
                    {
                        byte[] byImgQR = Convert.FromBase64String(qr);
                        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgQR);
                        Bitmap bmp = new Bitmap(Image.FromStream(m));
                        m.Close();
                        picFEMQR.Image = bmp;
                    }

                    if (foto != null)
                    {
                        byte[] byImgF = Convert.FromBase64String(foto);
                        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgF);
                        Bitmap bmp = new Bitmap(Image.FromStream(m));
                        m.Close();
                        picFEMPhoto.Image = bmp;
                    }
                    foreach (XmlNode item in xmlDoc.GetElementsByTagName("ExtensionItem"))
                    {
                        if (!item.ChildNodes[0].InnerText.Equals("Video") && !item.ChildNodes[0].InnerText.Equals("Photo") 
                            && !item.ChildNodes[0].InnerText.Equals("qrlinkfem"))
                        richTextBox1.Text = richTextBox1.Text + item.ChildNodes[0].InnerText + "=" + item.ChildNodes[1].InnerText + Environment.NewLine;
                    }
                    
                }
                //    XmlDocument xmlDocABS = new XmlDocument();
                //    xmlDocABS.PreserveWhitespace = false;
                //    xmlDocABS.LoadXml(abs);

                //    // Check arguments.
                //    if (xmlDoc != null)
                //    {
                //        //                        string sample = xmlDoc.GetElementsByTagName("Sample")[0].InnerText;
                //        string sample = xmlDocABS.GetElementsByTagName("Sample")[0].InnerText;
                //    }
                //} else
                //{

            }
            catch (Exception ex)
            {

              
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string queryString = "?src=FEM&TypeId=" + txtTypeID.Text +
                                  "&ValueId=" + txtValueId.Text +
                                  "&Mail=" + txtMail.Text +
                                  "&DocumentId=" + txtDocumentId.Text +
                                  "&MsgToRead=" + txtMsgToRead.Text;

            string absoluteURL = "http://localhost:9191";  //ConfigurationManager.AppSettings["AbsoluteURL"];
            if (!absoluteURL.EndsWith("/"))
                absoluteURL = absoluteURL + "/";

            absoluteURL = absoluteURL + queryString;

            txtResponse.Text = txtResponse.Text + absoluteURL + Environment.NewLine;

            WebRequest request = WebRequest.Create(absoluteURL);
            request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
            request.Timeout = 13000000;

            string json;
            FEMResponse fEMResponse;
            using (WebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                    fEMResponse = JsonConvert.DeserializeObject<FEMResponse>(json);
                }
            }

            ShowResult(fEMResponse);

            //txtToken.Text = "JSON => " + json;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            XmlParamIn xmlParamIn = new XmlParamIn();

            using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
            {
                ws.Timeout = 99999;
                ws.Url = txtBioportal.Text;

                xmlParamIn.Actionid = 26; //Get Enrolled

                xmlParamIn.Authenticationfactor = (int)((AuthenticationFactorEnum)cmbAuthenticationFactor.SelectedItem);  //Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;
                xmlParamIn.Minutiaetype = (int)((MinutiaeTypeEnum)cmbMinutiaeType.SelectedItem);

                xmlParamIn.Clientid = "Test-BiometrikaClient7";

                xmlParamIn.OperationOrder = 1; //Default
                xmlParamIn.Companyid = Convert.ToInt32(txtCompanyId.Text); // int.Parse(ConfigurationManager.AppSettings["CompanyId"]);
                xmlParamIn.Enduser = "Manager";
                xmlParamIn.Ipenduser = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();
                xmlParamIn.Matchingtype = 1;
                xmlParamIn.Origin = 1;

                //Datos de la Persona
                xmlParamIn.PersonalData = new PersonalData
                {
                    Typeid = txtTypeID.Text,
                    Valueid = txtValueId.Text,
                    Companyidenroll = Convert.ToInt32(txtCompanyId.Text)
                };

                //Serializamos.
                string xmlparamin = XmlUtils.SerializeAnObject(xmlParamIn);
                string xmlparamout = "";

                int respuesta;

            
                respuesta = ws.GetOp(xmlparamin, out xmlparamout);

                XmlParamOut outparam = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);

                DynamicDataItem dd = outparam.Additionaldata.DynamicDataItems[0];


                List<BpBirApi> arr = XmlUtils.DeserializeObject<List<BpBirApi>>(dd.value);

                txtBPShow.Text = GenerateBodyPartShow(arr);

                int f = 0;

            }
        }


        /// <summary>
        /// Dada la respuesta de dedos enrolados en BioPOrtal Server, se parsean para poder pasar como parámetro a BioPortal Client
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        private string GenerateBodyPartShow(List<BpBirApi> arr)
        {
            string strRet = null;
            Hashtable htControl = new Hashtable();
            try
            {
                if (arr == null || arr.Count == 0)
                {
                    strRet = "-1";
                } else
                {
                    foreach (BpBirApi item in arr)
                    {
                        if (!htControl.ContainsKey(item.Bodypart))
                        {
                            htControl.Add(item.Bodypart, item.Bodypart);
                        }
                    }

                    bool isFirst = true;
                   
                    foreach (DictionaryEntry item in htControl)
                    {
                        if (isFirst) {
                            isFirst = false;
                            strRet = item.Value.ToString();
                        } else
                        {
                            strRet = strRet + "|" + item.Value.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                strRet = "-1";
            }
            return strRet;
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            string queryString = "?src=CI&TypeId=" + txtTypeID.Text +
                                  "&ValueId=" + txtValueId.Text +
                                  "&Mail=" + txtMailCI.Text;

            string absoluteURL = "http://localhost:9191";  //ConfigurationManager.AppSettings["AbsoluteURL"];
            if (!absoluteURL.EndsWith("/"))
                absoluteURL = absoluteURL + "/";

            absoluteURL = absoluteURL + queryString;

            txtResponse.Text = txtResponse.Text + absoluteURL + Environment.NewLine;

            WebRequest request = WebRequest.Create(absoluteURL);
            request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
            request.Timeout = 13000000;

            string json;
            CIResponse ciResponse;
            using (WebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                    ciResponse = JsonConvert.DeserializeObject<CIResponse>(json);
                }
            }

            ShowResultCI(ciResponse);
        }

        private void ShowResultCI(CIResponse ciResponse)
        {
            try
            {
                if (ciResponse != null && !String.IsNullOrEmpty(ciResponse.CI))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.PreserveWhitespace = false;
                    xmlDoc.LoadXml(ciResponse.CI);

                    XmlNodeList n = xmlDoc.GetElementsByTagName("ExtensionItem");
                    System.Collections.Hashtable ht = new System.Collections.Hashtable();
                    string s;
                    foreach (XmlNode item in n)
                    {
                        if (item.ChildNodes.Count > 1 && !string.IsNullOrEmpty(item.ChildNodes[1].InnerText))
                            ht.Add(item.ChildNodes[0].InnerText, item.ChildNodes[1].InnerText);
                        int i = 0;
                    }


                    string foto = (string)ht["IDCardPhotoImage"];
                    string qr = (string)ht["QRCIVerify"];
                    string cedula = (string)ht["IDCardImage"];

                    if (cedula != null)
                    {
                        byte[] byImgCedula = Convert.FromBase64String(cedula);
                        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgCedula);
                        Bitmap bmp = new Bitmap(Image.FromStream(m));
                        m.Close();
                        picCICedula.Image = bmp;
                    }

                    if (qr != null)
                    {
                        byte[] byImgQR = Convert.FromBase64String(qr);
                        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgQR);
                        Bitmap bmp = new Bitmap(Image.FromStream(m));
                        m.Close();
                        picCIQR.Image = bmp;
                    }

                    if (foto != null)
                    {
                        byte[] byImgF = Convert.FromBase64String(foto);
                        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgF);
                        Bitmap bmp = new Bitmap(Image.FromStream(m));
                        m.Close();
                        picCIFotoCedula.Image = bmp;
                    }
                    foreach (XmlNode item in xmlDoc.GetElementsByTagName("ExtensionItem"))
                    {
                        if (!item.ChildNodes[0].InnerText.Equals("IDCardPhotoImage") &&
                            !item.ChildNodes[0].InnerText.Equals("QRCIVerify") && 
                            !item.ChildNodes[0].InnerText.Equals("IDCardImage"))
                            rtbCIResult.Text = rtbCIResult.Text + item.ChildNodes[0].InnerText + "=" + item.ChildNodes[1].InnerText.Substring(0, 20) + Environment.NewLine;
                    }

                }
                //    XmlDocument xmlDocABS = new XmlDocument();
                //    xmlDocABS.PreserveWhitespace = false;
                //    xmlDocABS.LoadXml(abs);

                //    // Check arguments.
                //    if (xmlDoc != null)
                //    {
                //        //                        string sample = xmlDoc.GetElementsByTagName("Sample")[0].InnerText;
                //        string sample = xmlDocABS.GetElementsByTagName("Sample")[0].InnerText;
                //    }
                //} else
                //{

            }
            catch (Exception ex)
            {


            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {

                string rd = System.IO.File.ReadAllText(@"C:\de\CI_21284415-2_20190809153404.xml");
                if (rd != null) // && !String.IsNullOrEmpty(ciResponse.CI))
                {
                    string CI = rd; // ciResponse.CI;

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.PreserveWhitespace = false;
                    xmlDoc.LoadXml(CI);

                    XmlNodeList n = xmlDoc.GetElementsByTagName("ExtensionItem");
                    System.Collections.Hashtable ht = new System.Collections.Hashtable();
                    string s;
                    foreach (XmlNode item in n)
                    {
                        if (item.ChildNodes.Count > 1 && !string.IsNullOrEmpty(item.ChildNodes[1].InnerText))
                            ht.Add(item.ChildNodes[0].InnerText, item.ChildNodes[1].InnerText);
                        int i = 0;
                    }


                    string foto = (string)ht["IDCardPhotoImage"];
                    string qr = (string)ht["QRCIVerify"];
                    string cedula = (string)ht["IDCardImage"];

                    if (cedula != null)
                    {
                        byte[] byImgCedula = Convert.FromBase64String(cedula);
                        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgCedula);
                        Bitmap bmp = new Bitmap(Image.FromStream(m));
                        m.Close();
                        picCICedula.Image = bmp;
                    }

                    if (qr != null)
                    {
                        byte[] byImgQR = Convert.FromBase64String(qr);
                        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgQR);
                        Bitmap bmp = new Bitmap(Image.FromStream(m));
                        m.Close();
                        picCIQR.Image = bmp;
                    }

                    if (foto != null)
                    {
                        byte[] byImgF = Convert.FromBase64String(foto);
                        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgF);
                        Bitmap bmp = new Bitmap(Image.FromStream(m));
                        m.Close();
                        picCIFotoCedula.Image = bmp;
                    }
                    foreach (XmlNode item in xmlDoc.GetElementsByTagName("ExtensionItem"))
                    {
                        //if (!item.ChildNodes[0].InnerText.Equals("IDCardPhotoImage") &&
                        //  !item.ChildNodes[0].InnerText.Equals("QRCIVerify") &&
                        //  !item.ChildNodes[0].InnerText.Equals("IDCardImage"))
                            if (item.ChildNodes.Count > 1)
                            {
                                rtbCIResult.Text = rtbCIResult.Text + item.ChildNodes[0].InnerText + "=" +
                                    ((item.ChildNodes[1] != null && item.ChildNodes[1].InnerText != null && item.ChildNodes[1].InnerText.Length > 20) ? item.ChildNodes[1].InnerText.Substring(0, 20) : item.ChildNodes[1].InnerText)
                                    + Environment.NewLine;
                            }
                    }

                }
                //    XmlDocument xmlDocABS = new XmlDocument();
                //    xmlDocABS.PreserveWhitespace = false;
                //    xmlDocABS.LoadXml(abs);

                //    // Check arguments.
                //    if (xmlDoc != null)
                //    {
                //        //                        string sample = xmlDoc.GetElementsByTagName("Sample")[0].InnerText;
                //        string sample = xmlDocABS.GetElementsByTagName("Sample")[0].InnerText;
                //    }
                //} else
                //{

            }
            catch (Exception ex)
            {


            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            rtbCIResult.Text = "";
            picCIQR.Image = null;
            picCIFotoCedula.Image = null;
            picCICedula.Image = null;
        }

        private string CallBiometrikaClient(int operation, string[] parameters)
        {
            string jsonReturn = null;

            try
            {
                string hashGuid = Guid.NewGuid().ToString("N");
                //Genero el query string a enviar, dependiendo 
                string queryString = "?src=bpc7";
                queryString = queryString + "&Hash=" + hashGuid; //Este hash es devuelto por la peticion, es para controlar que la respuesta 
                                                                 //recibida corresponde a la peticion relaizada
                string absoluteURL = "http://localhost:9191";  //Este puerto es configurable en el Biometrika CLient
                if (!absoluteURL.EndsWith("/"))
                    absoluteURL = absoluteURL + "/";

                WebRequest request = WebRequest.Create(absoluteURL);
                request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
                request.Timeout = 60000;  //Valor en milisegundos 
                switch (operation)
                {
                    case 1: //Lectura de QR
                            //No se agregan mas parámetros
                        break;
                    case 2: //MatchOnCard
                        queryString = queryString + "&qr=" + parameters[0];
                        queryString = queryString + "&minutiaeISO=" + parameters[1];
                        queryString = queryString + "&dataComple=" + parameters[2];   //Si es true devuelve además del resultado de verificación
                                                                                      //los datos adicionales del chip (Foto, nombre, etc)
                        break;
                }
                absoluteURL = absoluteURL + queryString;
                using (WebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        jsonReturn = reader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                jsonReturn = "{msgerror: " + ex.Message + "}";
            }

            return jsonReturn;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            rtbCIResult.Text = "Working...";
            string queryString = "?src=BVIATM&Op=" + txtOp.Text;

            if (txtOp.Text.Equals("2"))
            {
                queryString = queryString + "&qr=qrqrqrqrr&minutiaeISO=Rk1askjdaksjdkajsgdkjgas&dataComplete=true";
            }

            string absoluteURL = "http://localhost:9191";  //ConfigurationManager.AppSettings["AbsoluteURL"];
            if (!absoluteURL.EndsWith("/"))
                absoluteURL = absoluteURL + "/";

            absoluteURL = absoluteURL + queryString;

            txtResponse.Text = txtResponse.Text + absoluteURL + Environment.NewLine;

            WebRequest request = WebRequest.Create(absoluteURL);
            request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
            request.Timeout = 13000000;

            string json;
            CIResponse ciResponse;
            using (WebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                    rtbCIResult.Text = json;
                }
            }
        }
    }


    public class CIResponse
    {

        public string CI { get; set; }

        public string DeviceId { get; set; }
        public string Error { get; set; }

    }
}
