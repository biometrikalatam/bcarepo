﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace BiometrikaClient.Test
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            Dictionary<string, string> dicIN = new Dictionary<string, string>();
            dicIN.Add("k1", "v1");
            dicIN.Add("k2", "v2");
            string s = JsonConvert.SerializeObject(dicIN);


            Dictionary<string, string> dicINdes = JsonConvert.DeserializeObject<Dictionary<string, string>>(s);

            //Random r = new Random(2);
            //int i = r.Next(2);
            //i = r.Next(2);
            //i = r.Next(2);
            //i = r.Next(2);
            //i = r.Next(2);
            //i = r.Next(2);
            //i = r.Next(2);
            //string str = "c:\\app\\Log\\log.txt";
            //str = str.Substring(0, str.LastIndexOf('\\'));
            int i = 0;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
        //    Application.Run(new frmTestSizing());
            Application.Run(new frmMain());
        }
    }
}
