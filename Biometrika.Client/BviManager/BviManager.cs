﻿using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Threading;

namespace BviManager
{
    public class BviManager : IManager
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BviManager));
        private bool isOpen = false;
        private Dictionary<string, object> output = new Dictionary<string, object>();
        

        public BviManager()
        {
        }  // ctor para el relexion

        public BviManager(NameValueCollection queryString)
        {
            LOG.Info("BviManager In...");
            if (!isOpen)
            {
                try
                {
                    LOG.Debug("BviManager isOpen...");
                    isOpen = true;
                    LOG.Debug("BviManager SetPacket => " + queryString);

                    BviCamUIAdapter.App.SetPacket(queryString);     // el program recibe el pedido
                    LOG.Debug("BviManager Main...");
                    Thread t = new Thread(new ThreadStart(StartNewStaThread));
                    // Make sure to set the apartment state BEFORE starting the thread. 
                    t.ApartmentState = ApartmentState.STA;
                    t.Start();
                    Thread.Sleep(100);
                    Dictionary<string, object> outputUI = BviCamUIAdapter.App.Packet.PacketParamOut;
                    if (outputUI != null && outputUI.Count > 0)
                    {
                        LOG.Debug("BviManager outputUI != null && outputUI.Count > 0");
                    } else
                    {
                        LOG.Warn("BviManager outputUI nulo o outputUI.count = 0"); 
                    }
                }
                catch (Exception exe)
                {
                    LOG.Error("BviManager Error = " + exe.Message);
                }

                //output = BviUIAdapter.App.Packet.PacketParamOut;

            }
        }

        private void StartNewStaThread()
        {
            BviCamUIAdapter.App.Main();
        }

        public string Buffer => new JavaScriptSerializer().Serialize(output);

        public string Tag { get => "BVI"; }
    }
}
