﻿using BCR.Bio.BioPortal;
using BCR.Bio.Enum;
using BCR.Bio.Enum.BioPortal;
//using BioAPI;
using Biometrika.BioApi20;
using Biometrika.BioApi20.BSP;
using Biometrika.BioApi20.Interfaces;
using BioPortalClient7UIManager.Lib;
using Domain;
using log4net;
using Newtonsoft.Json;
//using SecuGen.FDxSDKPro.Windows;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Timers;
using System.Windows.Forms;

namespace BioPortalClient7UIAdapter
{ 
    public partial class BioPortalClient7UI : Form 
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BioPortalClient7UI));

        Biometrika.BioApi20.BSP.BSPBiometrika BSP;
        List<Biometrika.BioApi20.Sample> _SamplesCaptured;
        byte[] _Sample_Standard;

        public string SerialSensor { get; set; }
        public bool SensorConnected; //=> ReaderFactory.ReaderConnected;
        //private AbstractReader reader;

        //public int Tokencontent { get; set; }
        public bool HaveData { get; set; }
        public static BioPacket Packet { get; set; }
        private BioPacket bioPacketLocal;

        private RUT RUT;

        private string json;
        private bool isMenuOpen = false;
        private int timeLeft;
        private System.Timers.Timer timer;
        private System.Timers.Timer timerControlGeneral;

        private int Finger { get; set; }
        //private bool MustStop { get; set; } = false;

        public TokenContentEnum Tokencontent { get; set; }

        public DeviceEnum Device { get; set; }
        public AuthenticationFactorEnum AuthenticationFactor { get; set; }

        internal void SetBSP(BSPBiometrika bsp)
        {
            try
            {
                BSP = bsp;
                BSP.OnCapturedEvent += OnCaptureEvent;
                BSP.OnTimeoutEvent += OnTimeoutEvent;
            }
            catch (Exception ex)
            {
                LOG.Error("BioPortalClient7UI.SetBSP Error = " + ex.Message);
            }
        }

        internal void FreeBSP()
        {
            BSP.OnCapturedEvent -= OnCaptureEvent;
            BSP.OnTimeoutEvent -= OnTimeoutEvent;
        }

        public OperationTypeEnum OperationType { get; private set; }
        public MinutiaeTypeEnum MinutiaeType { get; set; }

        //public string TokenBioPortal { get; private set; }

        public TokenResponse TokenResponse { get; private set; }
        public string FingerName { get; private set; }

        private int iTinmerToShow = 10;
        private bool isStart = true;


        public BioPortalClient7UI()
        {
            InitializeComponent();
            //System.Drawing.Drawing2D.GraphicsPath objDraw1 = new System.Drawing.Drawing2D.GraphicsPath();
            ////objDraw1.AddEllipse(0, 0, this.Width, this.Height);
            //objDraw1.AddArc(0, 0, this.Width, this.Height,10,10);
            //this.Region = new Region(objDraw1);

            LOG.Info("BioPortalClient7UI Creating...");
            System.Drawing.Drawing2D.GraphicsPath objDraw = new System.Drawing.Drawing2D.GraphicsPath();
            objDraw.AddEllipse(0, 0, this.picSample.Width, this.picSample.Height);
            this.picSample.Region = new Region(objDraw);

            /// Timer del control general, se cierra en 10 segundos
            timerControlGeneral = new System.Timers.Timer(10000);
            timerControlGeneral.Elapsed += TimerControlGeneral_Elapsed;

            // timer de esperar la huella
            timer = new System.Timers.Timer(1000);
            timer.Elapsed += Timer_Elapsed;
            Tokencontent = TokenContentEnum.ALL;

            timer.Enabled = false;

            //InitBSP();

            //timerShowRestToTimeout.Enabled = false;
            //CloseControl(false);
            LOG.Info("BioPortalClient7UI Created!");
        }

        private void BioPortalClient7UI_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            this.TopMost = true;
            this.TopLevel = true;

            //ProcessBodyPartShow();

            try
            {
                Version version = Assembly.GetExecutingAssembly().GetName().Version;
                labVersion.Text = "v" + version.ToString();
            }
            catch (Exception ex)
            {
                labVersion.Text = "v7.5";
                LOG.Error("BioPortalClient7UI.frmMain_Load - Ecp Error: " + ex.Message);
            }

            LOG.Debug("BioPortalClient7UI.BioPortalClient7UI_Load OK!");
        }

        internal int Channel(BioPacket bioPacket)
        {
            int ret = 0;
            LOG.Info("BioPortalClient7UI.Channel In...");
            bioPacketLocal = bioPacket;
 
            ret = CheckParameters(bioPacketLocal);

            if (ret == 0)
            {
                if (bioPacketLocal.PacketParamOut == null)
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            }
            LOG.Info("BioPortalClient7UI.Channel Out [ret=" + ret + "]...");
            return ret;
        }

        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        private int CheckParameters(BioPacket bioPacketLocal)
        {
            /*
                 src=cam&
                 typeId=RUT&
                 valueId=21284415-2&
                 Tokencontent=1&
                 Device=1&
                 AuthenticationFactor=2&
                 OperationType=1&
                 MinutiaeType=7&
                 Timeout=5000&
                 Theme=theme_blue&
                 Hash=4jf4jf532f23f4#
            */
            int ret = 0;
            try
            {
                LOG.Info("BioPortalClient7UI.CheckParameters In...");
                if (bioPacketLocal == null) ret = -2;  //Lista de parametros no puede ser nulo

                //Paso a Dictionary

                LOG.Debug("BioPortalClient7UI.CheckParameters filling dictParamin...");
                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    LOG.Debug("BioPortalClient7UI.CheckParameters     Adding => " + k + "=" + (string)bioPacketLocal.PacketParamIn[k] + "...");
                    dictParamIn.Add(k.ToUpper(), bioPacketLocal.PacketParamIn[k]);
                }
                LOG.Debug("BioPortalClient7UI.CheckParameters dictParamin filled [length = " + dictParamIn.Count + "]");

                if (!dictParamIn.ContainsKey("SRC") || String.IsNullOrEmpty(dictParamIn["SRC"]) ||
                    !dictParamIn.ContainsKey("TYPEID") || String.IsNullOrEmpty(dictParamIn["TYPEID"]) ||
                    !dictParamIn.ContainsKey("VALUEID") || String.IsNullOrEmpty(dictParamIn["VALUEID"]))
                {
                    ret = -3;  //src/TypeId/ValueId deben ser enviados como parámetros
                    LOG.Debug("BioPortalClient7UI.CheckParameters src/TypeId/ValueId deben ser enviados como parámetros!");
                } else
                {
                    LOG.Debug("BioPortalClient7UI.CheckParameters evaluando cada parámetro...");
                    if (!dictParamIn.ContainsKey("TOKENCONTENT") || String.IsNullOrEmpty(dictParamIn["TOKENCONTENT"]))
                    {
                        if (!dictParamIn.ContainsKey("TOKENCONTENT"))
                        {
                            dictParamIn.Add("TOKENCONTENT", "0");
                        } else
                        {
                            dictParamIn["TOKENCONTENT"] = "0";  //Ambos Minucia + WSQ
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters Tokencontent = " + dictParamIn["TOKENCONTENT"]); 
                    if (!dictParamIn.ContainsKey("DEVICE") || String.IsNullOrEmpty(dictParamIn["DEVICE"]))
                    {
                        if (!dictParamIn.ContainsKey("DEVICE"))
                        {
                            dictParamIn.Add("DEVICE", "1");
                        }
                        else
                        {
                            dictParamIn["DEVICE"] = "1"; //DP - no imposta ahora
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters Device = " + dictParamIn["DEVICE"]);
                    if (!dictParamIn.ContainsKey("AUTHENTICATIONFACTOR") || String.IsNullOrEmpty(dictParamIn["AUTHENTICATIONFACTOR"]))
                    {
                        if (!dictParamIn.ContainsKey("AUTHENTICATIONFACTOR"))
                        {
                            dictParamIn.Add("AUTHENTICATIONFACTOR", "2");
                        }
                        else
                        {
                            dictParamIn["AUTHENTICATIONFACTOR"] = "2"; //Fingerprint
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters AuthenticationFactor = " + dictParamIn["AUTHENTICATIONFACTOR"]);
                    if (!dictParamIn.ContainsKey("OPERATIONTYPE") || String.IsNullOrEmpty(dictParamIn["OPERATIONTYPE"]))
                    {
                        if (!dictParamIn.ContainsKey("OPERATIONTYPE"))
                        {
                            dictParamIn.Add("OPERATIONTYPE", "1");
                        }
                        else
                        {
                            dictParamIn["OPERATIONTYPE"] = "1";
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters OperationType = " + dictParamIn["OPERATIONTYPE"]);
                    if (!dictParamIn.ContainsKey("MINUTIAETYPE") || String.IsNullOrEmpty(dictParamIn["MINUTIAETYPE"]))
                    {
                        if (!dictParamIn.ContainsKey("MINUTIAETYPE"))
                        {
                            dictParamIn.Add("MINUTIAETYPE", "14");
                        }
                        else
                        {
                            dictParamIn["MINUTIAETYPE"] = "14"; //ISO
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters MinutiaeType = " + dictParamIn["MINUTIAETYPE"]);
                    if (!dictParamIn.ContainsKey("TIMEOUT") || String.IsNullOrEmpty(dictParamIn["TIMEOUT"]))
                    {
                        if (!dictParamIn.ContainsKey("TIMEOUT"))
                        {
                            dictParamIn.Add("TIMEOUT", "10000");
                        }
                        else
                        {
                            dictParamIn["TIMEOUT"] = "10000";
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters Timeout = " + dictParamIn["TIMEOUT"]);
                    if (!dictParamIn.ContainsKey("THEME") || String.IsNullOrEmpty(dictParamIn["THEME"]))
                    {
                        if (!dictParamIn.ContainsKey("THEME"))
                        {
                            dictParamIn.Add("THEME", "theme_ligthgreen");
                        }
                        else
                        {
                            dictParamIn["THEME"] = "theme_green";
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters Theme = " + dictParamIn["THEME"]);
                    //Add 10-12-2018 - x AFEX - Para habilitar solo aquellos dedos que estan enrolados
                    if (!dictParamIn.ContainsKey("BODYPARTSHOW") || String.IsNullOrEmpty(dictParamIn["BODYPARTSHOW"]))
                    {
                        if (!dictParamIn.ContainsKey("BODYPARTSHOW"))
                        {
                            dictParamIn.Add("BODYPARTSHOW", "-1");  //Todos
                        }
                        else
                        {
                            dictParamIn["BODYPARTSHOW"] = "-1";
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters BodypartShow = " + dictParamIn["BODYPARTSHOW"]);


                    if (!dictParamIn.ContainsKey("HASH") || String.IsNullOrEmpty(dictParamIn["HASH"]))
                    {
                        if (!dictParamIn.ContainsKey("HASH"))
                        {
                            dictParamIn.Add("HASH", Guid.NewGuid().ToString("N"));
                        }
                        else
                        {
                            dictParamIn["HASH"] = Guid.NewGuid().ToString("N");
                        }
                    }
                    LOG.Debug("BioPortalClient7UI.CheckParameters Hash = " + dictParamIn["HASH"]);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Error BioPortalClient7UIAdapter.CheckParameters [" + ex.Message + "]");
                ret = -1;
            }
            LOG.Info("BioPortalClient7UI.CheckParameters Ount [ret=" + ret + "]!");
            return ret;
        }

 
        private void TimerControlGeneral_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                //if (iTinmerToShow > 0)
                //{
                //    iTinmerToShow--;
                //    labtimeout.Text = "Quedan " + iTinmerToShow.ToString() + " segundos...";
                //}
                //else
                //{
                LOG.Info("BioPortalClient7Manager.TimerControlGeneral_Elapsed In...");
                labtimeout.Text = "Timeout!";
                //}
                labtimeout.Refresh();
                //labDedoSeleccionado.Text = "Ningún dedo seleccionado";
                timerControlGeneral.Enabled = false;
                Log.Add("Timeout! -> no se detectó actividad en mas de 10 segundos");
                ClearBio();
                CloseControl(false);
                LOG.Info("BioPortalClient7Manager.TimerControlGeneral_Elapsed Out!");
            }
            catch (Exception)
            {
            }
            
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (timer.Enabled)
                if (timeLeft == 0)
                {
                    labtimeout.Text = "Timeout!";
                    //labDedoSeleccionado.Text = "Ningún dedo seleccionado";
                    timer.Enabled = false;
                    //timerShowRestToTimeout.Enabled = false;
                    LOG.Warn("Timeout! -> no se ingresó huella en mas de " + timer.Interval +  " milisegundos");
                    ClearBio();
                    CloseControl(false);
                }
                else
                {
                    //Console.WriteLine(timeLeft);
                    timeLeft--;
                    labtimeout.Text = "Quedan " + timeLeft.ToString() + " segundos...";
                    labtimeout.Refresh();
                    LOG.Debug("BioPortalClient7UI.Timer_Elapsed => " + timeLeft.ToString());
                }
        }

        private void timerShowRestToTimeout_Tick(object sender, EventArgs e)
        {
            //if (iTinmerToShow > 0)
            //{
            //    iTinmerToShow--;
            //    labtimeout.Text = "Quedan " + iTinmerToShow.ToString() + " segundos...";
            //}
        }

        //private BioRouterRequest CreateRequest(OperationTypeEnum operationTypeEnum)
        //{
        //     Dictionary<string, string> addParams = new Dictionary<string, string>();
        //    string accion = "Verification";
        //    if (operationTypeEnum == OperationTypeEnum.ENROLL)
        //        accion = "Enrollment";

        //    addParams.Add("type", accion);

        //    return bioRouterRequest;
        //}

       

        /// <summary>
        /// Dada la lista del parámetro o bien si el parametro de config dice que lo tome desde aca, toma la lista de bodypart enrolados
        /// y muestra las lab de seleccion de dedo que corresponda
        /// </summary>
        private void ProcessBodyPartShow()
        {
            try
            {
                //if (Properties.Settings.Default.BPCCheckEnrolled)
                //{

                //} else 
                if (!dictParamIn["BODYPARTSHOW"].Equals("-1"))  //Si no es -1 = Todos => recorro y hide cada lab que no esté en la lista
                {
                    string[] listBodyPart = dictParamIn["BODYPARTSHOW"].Split('|');
                    labF1.Visible = IsBodyPartInList("1", listBodyPart);
                    labF2.Visible = IsBodyPartInList("2", listBodyPart);
                    labF3.Visible = IsBodyPartInList("3", listBodyPart);
                    labF4.Visible = IsBodyPartInList("4", listBodyPart);
                    labF5.Visible = IsBodyPartInList("5", listBodyPart);
                    labF6.Visible = IsBodyPartInList("6", listBodyPart);
                    labF7.Visible = IsBodyPartInList("7", listBodyPart);
                    labF8.Visible = IsBodyPartInList("8", listBodyPart);
                    labF9.Visible = IsBodyPartInList("9", listBodyPart);
                    labF10.Visible = IsBodyPartInList("10", listBodyPart);
                }
                this.Refresh();
            }
            catch (Exception ex)
            {
                LOG.Error("BioPortalClient7UI.ProcessBodyPartShow Error = " + ex.Message);
            }
        }

        private bool IsBodyPartInList(string bodypart, string[] listBodyPart)
        {
            bool ret = false;
            try
            {
                for (int i = 0; i < listBodyPart.Length; i++)
                {
                    if (listBodyPart[i].Trim().Equals(bodypart))
                    {
                        ret = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BioPortalClient7UI.IsBodyPartInList Error = " + ex.Message);
            }
            return ret;
        }


        /// <summary>
        /// Selecciona un dedo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PictureDedosGrande_MouseClick(object sender, MouseEventArgs e)
        {
            int i = 1;
            if (RUT != null && RUT.HasRUT) //_sensorConnected &&
            {
                int sgDedoAux = 0;
                string nombrededo = Utils.DedoSeleccionado(e.X, e.Y, out sgDedoAux);
                if (sgDedoAux >= 1 && sgDedoAux <= 10)
                {
                    ProcesaDedoSeleccionado(sgDedoAux, nombrededo);
                    Finger = sgDedoAux;
                    FingerName = nombrededo;
                    CaptureFinger();
                }
            }
        }


        public void ShowCapture(ServerRequest serverRequest)
        {
            LOG.Info("BioPortalClient7UI.ShowCapture In...");

            serverRequest = new ServerRequest(bioPacketLocal.PacketParamIn);
            LOG.Debug("BioPortalClient7UI.ShowCapture ServerRequest(bioPacketLocal.PacketParamIn ok => serverRequest != null => " +
                      (serverRequest != null).ToString());
            //NameValueCollection qs = bioPacketLocal.PacketParamIn;

            try
            {
                this.BackgroundImage = Image.FromFile(serverRequest.Theme);
            }
            catch (Exception ex)
            {
                LOG.Warn("BioPortalClient7UI.ShowCapture Error Loading Theme [" + ex.Message + "]. Se cargo default theme_ligthgreen... ");
                this.BackgroundImage = Properties.Resources.theme_ligthgreen;
            }
            //if (serverRequest.Theme.Equals("theme_ligthblue")) this.BackgroundImage = Properties.Resources.theme_ligthblue;
            //else if (serverRequest.Theme.Equals("theme_blue")) this.BackgroundImage = Properties.Resources.theme_blue;
            //else this.BackgroundImage = Properties.Resources.theme_ligthgreen;
            LOG.Debug("BioPortalClient7UI.ShowCapture this.BackgroundImage = " + serverRequest.Theme);

            //Added 12/2018 - Verifica que dedos enrolados vienen por parametro o bein desde aqui se conecta a toamrlos hacia el bioportal
            ProcessBodyPartShow();


            labtimeout.Text = "";
            labtimeout.Refresh();
            timerControlGeneral.Enabled = true;

            timer.Enabled = true;
            RUT = serverRequest.RUT;
            LOG.Debug("BioPortalClient7UI.ShowCapture " +
                string.Format("Se inicia captura para {0} -{1} con hash {2}", RUT.TypeId, RUT.ValueId, serverRequest.Hash));
            Device = serverRequest.Device;
            LOG.Debug("BioPortalClient7UI.ShowCapture Device = " + Device);
            Tokencontent = serverRequest.Tokencontent;
            LOG.Debug("BioPortalClient7UI.ShowCapture Tokencontent = " + Tokencontent);

            timerControlGeneral.Interval = serverRequest.Timeout; // * 1000;
            LOG.Debug("BioPortalClient7UI.ShowCapture timerControlGeneral.Interval = " + timerControlGeneral.Interval);
            timeLeft = serverRequest.Timeout / 1000;
            LOG.Debug("BioPortalClient7UI.ShowCapture timeLeft = " + timeLeft);
            AuthenticationFactor = serverRequest.AuthenticationFactor;
            LOG.Debug("BioPortalClient7UI.ShowCapture AuthenticationFactor = " + AuthenticationFactor);
            OperationType = serverRequest.OperationType;
            LOG.Debug("BioPortalClient7UI.ShowCapture OperationType = " + OperationType);
            MinutiaeType = serverRequest.MinutiaeType;
            LOG.Debug("BioPortalClient7UI.ShowCapture MinutiaeType = " + MinutiaeType);

            //bioRouterRequest = CreateRequest(OperationType);
            //bioRouterRequest.Hash = serverRequest.Hash;

            picSample.Image = Properties.Resources.FondoBPC_Boton_plan_griso;

            labFeedbackBotton.Text = "PASO 1 - Seleccione dedo a capturar...";
            labDedoSeleccionado.Visible = true;
            labtimeout.Visible = true;
            //iTinmerToShow = 10;
            //timeLeft = 10;
            //timerShowRestToTimeout.Enabled = true;

            ShowInTaskbar = true;

            labMsg.Text = string.Concat("Validando: ", RUT.TypeId, " ", RUT.ValueId);
            WindowState = FormWindowState.Normal;
            this.StartPosition = FormStartPosition.CenterScreen;
            //this.Location = new System.Drawing.Point(200, 200);
            this.TopMost = true;
            this.TopLevel = true;
            this.Show();
            LOG.Info("BioPortalClient7UI.ShowCapture Out!");
        }



        public void CaptureFinger()
        {
            TokenResponse = new TokenResponse();
            bool hasCapture = false;

            LOG.Info("BioPortalClient7UI.CaptureFinger In...");
            //if (BioRouter.HasSensor && (RUT != null && RUT.HasRUT))
            if ((RUT != null && RUT.HasRUT))
            {
                LOG.Debug("BioPortalClient7UI.CaptureFinger RUT = " + RUT.ValueId);
                if (Finger >= 1 && Finger <= 10)
                {
                    LOG.Debug("BioPortalClient7UI.CaptureFinger Finger = " + Finger);
                    //timeLeft = 20;
                    //labtimeout.Text = timeLeft.ToString();
                    //labtimeout.Visible = true;

                    //Log.Add("Se activa el contador de 20 segundos");
                    timer.Enabled = false; // inicia el timeout para capturar, pasados 20 segundos genera error

                    ////////////////////
                    string hashGuid = Guid.NewGuid().ToString().Substring(0, 8);

                    CaptureFinger(dictParamIn["TYPEID"], dictParamIn["VALUEID"], Finger);

                    #region ANTERIOR CAPTURA
                    //string queryString = "?src=fs";

                    //queryString = queryString + "&TypeId=" + dictParamIn["TypeId"];    //bioPacketLocal.PacketParamIn["TypeId"];
                    //queryString = queryString + "&ValueId=" + dictParamIn["ValueId"];    //bioPacketLocal.PacketParamIn["ValueId"];
                    //queryString = queryString + "&Finger=" + Finger;
                    //queryString = queryString + "&GUI=false";
                    //queryString = queryString + "&Timeout=6000";

                    ////src=bpc7&typeId=RUT&valueId=21284415-2&Tokencontent=0&Device=1&AuthenticationFactor=2&OperationType=1&MinutiaeType=7&Timeout=5000&Theme=theme_blue&Hash=4jf4jf532f23f4
                    //string absoluteURL = Properties.Settings.Default.URLBaseBiometrikaClient;   //"http://localhost:9191/" + queryString; //?src=fs&typeId=RUT&valueId=21284415-2&Finger=7";
                    //if (String.IsNullOrEmpty(absoluteURL))
                    //{
                    //    absoluteURL = "http://localhost:9191/";
                    //}
                    //else
                    //{.
                    //    if (!absoluteURL.EndsWith("/"))
                    //        absoluteURL = absoluteURL + "/";
                    //}
                    //absoluteURL = absoluteURL + queryString;
                    //LOG.Debug("BioPortalClient7UI.CaptureFinger absoluteURL = " + absoluteURL);

                    //WebRequest request = WebRequest.Create(absoluteURL);
                    //request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
                    //request.Timeout = 13000000;
                    //SampleResponse serverResponse;
                    //using (WebResponse response = (HttpWebResponse)request.GetResponse())
                    //{
                    //    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    //    {
                    //        string json = reader.ReadToEnd();
                    //        LOG.Debug("BioPortalClient7UI.CaptureFinger Json recibido = " + json);
                    //        serverResponse = JsonConvert.DeserializeObject<SampleResponse>(json);
                    //    }
                    //}
                    //LOG.Debug("BioPortalClient7UI.CaptureFinger json parsed...");
                    //MsgError error = new MsgError(serverResponse.Error);
                    ////if (!String.IsNullOrEmpty(serverResponse.Error)) // bioRouterRequest.Hash == bioRouterResponse.Token)
                    //if (error.Codigo == 0) //No hubo error => Armo token
                    //{
                    //    LOG.Debug("BioPortalClient7UI.CaptureFinger No error..");
                    //    if (serverResponse.Huella != null)
                    //    {
                    //        LOG.Debug("BioPortalClient7UI.CaptureFinger Huella No Null...");
                    //        TokenResponse.Huella = serverResponse.Huella;
                    //        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.Huella = " + TokenResponse.Huella);
                    //        TokenResponse.Width = serverResponse.Width;
                    //        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.Width = " + TokenResponse.Width);
                    //        TokenResponse.Height = serverResponse.Height;
                    //        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.Height = " + TokenResponse.Height);
                    //        TokenResponse.BodyPart = serverResponse.BodyPart;
                    //        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.BodyPart = " + TokenResponse.BodyPart);
                    //        picSample.Image = BioRouterInterpreter.Base64ToImage(serverResponse.Huella);
                    //        picSample.Refresh();
                    //        LOG.Debug("BioPortalClient7UI.CaptureFinger picSample.Image filled (picSample.Image != null) => " +
                    //            (picSample.Image != null).ToString());
                    //        this.Refresh();

                    //        hasCapture = GenerateToken(serverResponse);
                    //        LOG.Debug("BioPortalClient7UI.CaptureFinger GenerateToken = " + hasCapture.ToString());
                    //        if (hasCapture) SetColorLabF(Color.Green);
                    //        else SetColorLabF(Color.Red);
                    //        Thread.Sleep(1000);
                    //    }
                    //    else
                    //    {
                    //        LOG.Debug("BioPortalClient7UI.CaptureFinger Huella NULL...");
                    //        SetColorLabF(Color.Red);
                    //        TokenResponse.Error = "-1|No se obtuvo respuesta del FingerScannerManager!";
                    //        LOG.Warn("BioPortalClient7UI.CaptureFinger TokenResponse.Error = " + TokenResponse.Error);
                    //        Thread.Sleep(1000);
                    //        ClearBio();   // TODO : esto debe limpiar los datos de memoria para evitar problemas
                    //    }
                    //}
                    //else
                    //{
                    //    SetColorLabF(Color.Red);
                    //    TokenResponse.Error = error.Codigo + "|" + error.Description;   //string.Format("No coinciden el hash de la petición {0}-{1}", bioRouterRequest.Hash, serverResponse.Error);
                    //    LOG.Error("BioPortalClient7UI.CaptureFinger error = " + TokenResponse.Error);
                    //    Thread.Sleep(1000);
                    //    ClearBio();   // TODO : esto debe limpiar los datos de memoria para evitar problemas
                    //}
                    #endregion ANTERIOR CAPTURA
                }
            }
            else
            {
                // notificar que no hay sensor
                TokenResponse.Error = "-1|Sensor no detectado";
                labtimeout.Text = TokenResponse.Error;
                LOG.Warn("BioPortalClient7UI.CaptureFinger " + string.Format(TokenResponse.Error));
                timer.Enabled = false;
                Thread.Sleep(1000);
                ClearBio();
            }
            LOG.Info("BioPortalClient7UI.CaptureFinger Out!");
            //CloseControl(hasCapture);
        }

        private void CloseControl(bool hasCapture)
        {
            LOG.Info("BioPortalClient7UI.CloseControl In...");
            //timerShowRestToTimeout.Enabled = false;
            timer.Enabled = false;
            //HuellaCapturadaEventArgs huellaCapturadaEventArgs = new HuellaCapturadaEventArgs();
            //huellaCapturadaEventArgs.TokenResponse = TokenResponse;
            //LOG.Debug("BioPortalClient7UI.CloseControl huellaCapturadaEventArgs.TokenResponse != null => " +
            //    (huellaCapturadaEventArgs.TokenResponse != null).ToString());
            //huellaCapturadaEventArgs.HasCapture = hasCapture;
            //LOG.Debug("BioPortalClient7UI.CloseControl huellaCapturadaEventArgs.HasCapture = " + huellaCapturadaEventArgs.HasCapture.ToString());
            //huellaCapturadaEventArgs.Hash = bioRouterRequest == null ? "" : bioRouterRequest.Hash;
            //LOG.Debug("BioPortalClient7UI.CloseControl huellaCapturadaEventArgs.Hash = " + huellaCapturadaEventArgs.Hash);

            if (hasCapture)
            {
                LOG.Debug("BioPortalClient7UI.CloseControl hasCapture = " + hasCapture.ToString());
                bioPacketLocal.PacketParamOut.Add("Token", TokenResponse.Token);
                LOG.Debug("BioPortalClient7UI.CloseControl Token = " + TokenResponse.Token);
                bioPacketLocal.PacketParamOut.Add("ImageSample", TokenResponse.Huella);
                LOG.Debug("BioPortalClient7UI.CloseControl TokenResponse.Huella = " + TokenResponse.Huella);
                bioPacketLocal.PacketParamOut.Add("BodyPart", TokenResponse.BodyPart);
                LOG.Debug("BioPortalClient7UI.CloseControl TokenResponse.BodyPart = " + TokenResponse.BodyPart);
                bioPacketLocal.PacketParamOut.Add("Width", TokenResponse.Width);
                LOG.Debug("BioPortalClient7UI.CloseControl TokenResponse.Width = " + TokenResponse.Width);
                bioPacketLocal.PacketParamOut.Add("Height", TokenResponse.Height);
                LOG.Debug("BioPortalClient7UI.CloseControl TokenResponse.Height = " + TokenResponse.Height);

                TokenResponse.Hash = dictParamIn["HASH"];
                bioPacketLocal.PacketParamOut.Add("Hash", TokenResponse.Hash);
                LOG.Debug("BioPortalClient7UI.CloseControl TokenResponse.Hash = " + TokenResponse.Hash);

                if (TokenResponse.ResponseOK) bioPacketLocal.PacketParamOut.Add("Error", "0|Capture OK");
                else bioPacketLocal.PacketParamOut.Add("Error", TokenResponse.Error);
                LOG.Debug("BioPortalClient7UI.CloseControl TokenResponse.Error = " + TokenResponse.Error);

                HaveData = true;
                LOG.Debug("BioPortalClient7UI.CloseControl HaveData = true");
                timer.Enabled = false;
                timerControlGeneral.Enabled = false;
                this.Close();

            } else
            {
                bioPacketLocal.PacketParamOut.Add("Token", null);
                bioPacketLocal.PacketParamOut.Add("ImageSample", null);
                bioPacketLocal.PacketParamOut.Add("Finger", 0);
                bioPacketLocal.PacketParamOut.Add("Width", 0);
                bioPacketLocal.PacketParamOut.Add("Height", 0);
                bioPacketLocal.PacketParamOut.Add("Hash", dictParamIn["HASH"]);
                if (!bioPacketLocal.PacketParamOut.ContainsKey("Error")) {
                    bioPacketLocal.PacketParamOut.Add("Error", "0|Close Sin Captura");
                }
                LOG.Debug("BioPortalClient7UI.CloseControl Error = 0 | Close Sin Captura");
                timer.Enabled = false;
                timerControlGeneral.Enabled = false;
                this.Close();
            }
            //bioRouterRequest = null;
            FreeBSP();

            LOG.Info("BioPortalClient7UI.CloseControl OUT!");
            // invocar al evento que finaliza la ventana
            //OnHuellaCapturada(huellaCapturadaEventArgs);
        }


        private bool GenerateToken(string wsq, string templateregistry, string templateverify, string templateANSI, string templateISO)
        {
            bool hasCaptureLocal = false;
            LOG.Info("BioPortalClient7UI.GenerateToken In...");
            try
            {
                //if (fingerCaptured.CaptureQuality == ReaderFactory.CaptureQuality.QUALITY_GOOD)
                //{
                Token token = new Token()
                {
                    TypeId = RUT.TypeId,
                    ValueId = RUT.ValueId,
                    SerialSensor = SerialSensor,
                    Finger = Finger,
                    Wsq_Template = wsq,
                    Token_Template = Token_Template(templateregistry, templateverify, templateANSI, templateISO),
                    Timestamp = DateTime.Now.ToString("ddMMyyyyHHmmss"),
                    IP = Utils.GetIP(),
                    Content = Tokencontent,
                    MinutiaeType = MinutiaeType,
                    Device = Device,
                    AuthenticationFactor = AuthenticationFactor,
                    OperationType = OperationType
                };

                string tokenBioPortal = token.Generate("AV050105");

                if (token.ErrorCode == 0)
                {
                    TokenResponse.Token = tokenBioPortal;
                    //LOG.Debug("BioPortalClient7UI.GenerateToken Token Generador = " + TokenResponse.Token);
                    //TokenResponse.Huella = _sampleResponse.Huella;
                    //LOG.Debug("BioPortalClient7UI.GenerateToken Huella = " + TokenResponse.Huella);
                    //TokenResponse.BodyPart = Finger;
                    TokenResponse.FingerName = FingerName;
                    //LOG.Debug("BioPortalClient7UI.GenerateToken TokenResponse.FingerName = " + TokenResponse.FingerName);
                    //TokenResponse.Width = _sampleResponse.Width;
                    //LOG.Debug("BioPortalClient7UI.GenerateToken TokenResponse.Width = " + TokenResponse.Width);
                    //TokenResponse.Height = _sampleResponse.Height;
                    //LOG.Debug("BioPortalClient7UI.GenerateToken TokenResponse.Height 0 " + TokenResponse.Height);
                    this.labFeedbackBotton.Text = "Token del dedo " + Utils.GetNombreDedo(Finger) + " generado con éxito!";
                    this.labFeedbackBotton.Refresh();
                    hasCaptureLocal = true;
                }
                else
                {
                    TokenResponse.Error = string.Format("-1|ERROR - Generando TOKEN [ErrNum={0}]", token.ErrorCode);

                    this.labFeedbackBotton.Text = "Error en la generación del Token. Reintente...";
                    this.labFeedbackBotton.Refresh();
                    LOG.Debug("BioPortalClient7UI.GenerateToken Error = " + TokenResponse.Error);
                }
                //}
                //else
                //{
                //    //picSample.Image = Properties.Resources.FondoBPC_Boton_planoerr; // .FondoHuella_NoOk;
                //    this.picSample.Image = Resources.FondoBPC_Boton_planoerr;
                //    this.picWizardStatus.Image = Resources.muestra_sensorNoOk;
                //    this.labFeedbackBotton.Text = "Mala Calidad Imagen!. Reintente...";
                //    Refresh();
                //    WriteLog("DEBUG - reader_On_Captured Mala Calidad Imagen!");
                //}
            }
            catch (Exception ex)
            {
                TokenResponse.Error = ex.Message;
                LOG.Debug("BioPortalClient7UI.GenerateToken Exc = " + TokenResponse.Error);
            }
            LOG.Info("BioPortalClient7UI.GenerateToken Out! [hasCaptureLocal=" + hasCaptureLocal.ToString());
            return hasCaptureLocal;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bioRouterResponse"></param>
        /// <returns></returns>
        private bool GenerateToken(SampleResponse _sampleResponse)
        {
            bool hasCaptureLocal = false;
            LOG.Info("BioPortalClient7UI.GenerateToken In...");
            try
            {
                //if (fingerCaptured.CaptureQuality == ReaderFactory.CaptureQuality.QUALITY_GOOD)
                //{
                Token token = new Token()
                {
                    TypeId = RUT.TypeId,
                    ValueId = RUT.ValueId,
                    SerialSensor = _sampleResponse.SerialDevice,
                    Finger = Finger,
                    Wsq_Template = Wsq_Template(_sampleResponse),
                    Token_Template = Token_Template(_sampleResponse),
                    Timestamp = DateTime.Now.ToString("ddMMyyyyHHmmss"),
                    IP = Utils.GetIP(),
                    Content = Tokencontent,
                    MinutiaeType = MinutiaeType,
                    Device = Device,
                    AuthenticationFactor = AuthenticationFactor,
                    OperationType = OperationType
                };
             
                string tokenBioPortal = token.Generate("AV050105");
                
                if (token.ErrorCode == 0)
                {
                    TokenResponse.Token = tokenBioPortal;
                    LOG.Debug("BioPortalClient7UI.GenerateToken Token Generador = " + TokenResponse.Token);
                    TokenResponse.Huella = _sampleResponse.Huella;
                    LOG.Debug("BioPortalClient7UI.GenerateToken Huella = " + TokenResponse.Huella);
                    //TokenResponse.BodyPart = Finger;
                    TokenResponse.FingerName = FingerName;
                    LOG.Debug("BioPortalClient7UI.GenerateToken TokenResponse.FingerName = " + TokenResponse.FingerName);
                    TokenResponse.Width = _sampleResponse.Width;
                    LOG.Debug("BioPortalClient7UI.GenerateToken TokenResponse.Width = " + TokenResponse.Width);
                    TokenResponse.Height = _sampleResponse.Height;
                    LOG.Debug("BioPortalClient7UI.GenerateToken TokenResponse.Height 0 " + TokenResponse.Height);
                    this.labFeedbackBotton.Text = "Token del dedo " + Utils.GetNombreDedo(Finger) + " generado con éxito!";
                    this.labFeedbackBotton.Refresh();
                    hasCaptureLocal = true;
                }
                else
                {
                    TokenResponse.Error = string.Format("-1|ERROR - Generando TOKEN [ErrNum={0}]", token.ErrorCode);

                    this.labFeedbackBotton.Text = "Error en la generación del Token. Reintente...";
                    this.labFeedbackBotton.Refresh();
                    LOG.Debug("BioPortalClient7UI.GenerateToken Error = " + TokenResponse.Error);
                }
                //}
                //else
                //{
                //    //picSample.Image = Properties.Resources.FondoBPC_Boton_planoerr; // .FondoHuella_NoOk;
                //    this.picSample.Image = Resources.FondoBPC_Boton_planoerr;
                //    this.picWizardStatus.Image = Resources.muestra_sensorNoOk;
                //    this.labFeedbackBotton.Text = "Mala Calidad Imagen!. Reintente...";
                //    Refresh();
                //    WriteLog("DEBUG - reader_On_Captured Mala Calidad Imagen!");
                //}
            }
            catch (Exception ex)
            {
                TokenResponse.Error = ex.Message;
                LOG.Debug("BioPortalClient7UI.GenerateToken Exc = " + TokenResponse.Error);
            }
            LOG.Info("BioPortalClient7UI.GenerateToken Out! [hasCaptureLocal=" + hasCaptureLocal.ToString());
            return hasCaptureLocal;
        }

        /// <summary>
        /// Evento que se dispara al capturar la huella correctamente o no
        /// </summary>
        /// <param name="e"></param>
        public void OnHuellaCapturada(HuellaCapturadaEventArgs e)
        {
            //EventHandler<HuellaCapturadaEventArgs> handler = HuellaCapturadaEvent;
            //if (handler != null)
            //{
            //    handler(this, e);
            //}
        }

        private void ProcesaDedoSeleccionado(int sgDedoAux, string nombrededo)
        {
            this.labDedoSeleccionado.Visible = false;
            labFeedbackBotton.Text = string.Format("PASO 2 - Coloque el dedo {0} en el lector...", nombrededo);

            //SetImageChica(sgDedoAux);
            this.Refresh();
        }

        private void picResetCptura_Click(object sender, EventArgs e)
        {
            ClearBio();
        }

        public void ClearBio()
        {
            timerControlGeneral.Enabled = false;

            ResetLabelsFingers();
            Finger = 0;

            //timerTimeouCapture.Enabled = false;
        }

#region Manejo Grafica

        private void ResetLabelsFingers()
        {
            Label lAux;
            for (int i = 1; i <= 10; i++)
            {
                if (this.Controls.Find("labF" + i, true).Length == 1)
                {
                    lAux = (Label)this.Controls.Find("labF" + i, true)[0];
                    lAux.BackColor = Color.Transparent;
                }
            }
        }

        private void SetColorLabF(Color color)
        {
            Label lAux;
            if (this.Controls.Find("labF" + Finger, true).Length == 1)
            {
                lAux = (Label)this.Controls.Find("labF" + Finger, true)[0];
                lAux.BackColor = color;
                lAux.Refresh();
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            frmHelp frmh = new frmHelp();
            frmh.ShowDialog(this);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            frmAbout frma = new frmAbout();
            frma.ShowDialog(this);
        }

        private void picDelete_Click(object sender, EventArgs e)
        {
            ClearBio();
        }

        private string Wsq_Template(SampleResponse _sampleResponse)
        {
            if (Tokencontent == TokenContentEnum.ALL || Tokencontent == TokenContentEnum.WSQ)
                return _sampleResponse.WSQ;
            //WriteLog(String.Format("DEBUG - WSQ Generado [{0}]", _wsq));
            else
                return string.Empty;
        }

        private string Token_Template(string TemplateRegistry64, string TemplateVerify64, string TemplateANSI64, string TemplateISO64)
        {
            if (Tokencontent == TokenContentEnum.ALL || Tokencontent == TokenContentEnum.TEMPLATE)

                return String.Format(
                        "{0}-septmpl-{1}-septmpl-{2}-septmpl-{3}",
                        TemplateRegistry64,
                        TemplateVerify64,
                        TemplateANSI64,
                        TemplateISO64
                );
            else
                return string.Empty;
        }

        private string Token_Template(SampleResponse _sampleResponse)
        {
            if (Tokencontent == TokenContentEnum.ALL || Tokencontent == TokenContentEnum.TEMPLATE)

                return String.Format(
                        "{0}-septmpl-{1}-septmpl-{2}-septmpl-{3}",
                        _sampleResponse.TemplateRegistry64,
                        _sampleResponse.TemplateVerify64,
                        _sampleResponse.TemplateANSI64,
                        _sampleResponse.TemplateISO64
                );
            else
                return string.Empty;
        }

        private void Control_VisibleChanged(object sender, EventArgs e)
        {
            //this.StartPosition = FormStartPosition.CenterScreen;
            //this.Location = new System.Drawing.Point(200, 200);
            //this.TopMost = true;
            //if (isStart)
            //{
            //    isStart = false;
            //    this.Hide();
            //}
        }

        private void picLectorStatus_Click(object sender, EventArgs e)
        {
            //this.Location = new Point(500, 400);
            //this.Refresh();
        }

        //private void labPicSalir_Click(object sender, EventArgs e)
        //{
        //    CloseControl(false);
        //}

        private void picSalir_Click(object sender, EventArgs e)
        {
            CloseControl(false);  
            this.Hide();
        }

        public void HideControl()
        {
            this.Hide();
        }

#region Seleccion Dedo

        private void ShowMsgLabel(int dedo)
        {
            if (dedo == 0)
            {
                labDedoSeleccionado.Text = "Ningún dedo seleccionado...";
            }
            else
            {
                labDedoSeleccionado.Text = "Dedo " + Utils.GetNombreDedo(dedo) + "...";
            }
            labDedoSeleccionado.Refresh();
        }

        private void labF1_MouseMove(object sender, MouseEventArgs e)
        {
            if (Finger == 0)
            {
                ShowMsgLabel(1);
                labF1.BackColor = Color.LightGray;
            }
        }

        private void labF1_MouseLeave(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                labF1.BackColor = Color.Transparent;
                ShowMsgLabel(0);
            }
        }

        private void labF1_MouseClick(object sender, MouseEventArgs e)
        {
            if (Finger == 0)
            {
                Finger = 1;
                labF1.BackColor = Color.LightBlue;
                string nombrededo = Utils.GetNombreDedo(Finger);
                ProcesaDedoSeleccionado(Finger, nombrededo);
                FingerName = nombrededo;
                CaptureFinger();
            }
        }

        private void labF2_Click(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                Finger = 2;
                labF2.BackColor = Color.LightBlue;
                string nombrededo = Utils.GetNombreDedo(Finger);
                ProcesaDedoSeleccionado(Finger, nombrededo);
                FingerName = nombrededo;
                CaptureFinger();
            }
        }

        private void labF2_MouseMove(object sender, MouseEventArgs e)
        {
            if (Finger == 0)
            {
                ShowMsgLabel(2);
                labF2.BackColor = Color.LightGray;
            }
        }

        private void labF2_MouseLeave(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                labF2.BackColor = Color.Transparent;
                ShowMsgLabel(0);
            }
        }

        private void labF3_Click(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                Finger = 3;
                labF3.BackColor = Color.LightBlue;
                string nombrededo = Utils.GetNombreDedo(Finger);
                ProcesaDedoSeleccionado(Finger, nombrededo);
                FingerName = nombrededo;
                CaptureFinger();
            }
        }

        private void labF3_MouseMove(object sender, MouseEventArgs e)
        {
            if (Finger == 0)
            {
                ShowMsgLabel(3);
                labF3.BackColor = Color.LightGray;
            }
        }

        private void labF3_MouseLeave(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                labF3.BackColor = Color.Transparent;
                ShowMsgLabel(0);
            }
        }

        private void labF4_Click(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                Finger = 4;
                labF4.BackColor = Color.LightBlue;
                string nombrededo = Utils.GetNombreDedo(Finger);
                ProcesaDedoSeleccionado(Finger, nombrededo);
                FingerName = nombrededo;
                CaptureFinger();
            }
        }

        private void labF4_MouseMove(object sender, MouseEventArgs e)
        {
            if (Finger == 0)
            {
                ShowMsgLabel(4);
                labF4.BackColor = Color.LightGray;
            }
        }

        private void labF4_MouseLeave(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                labF4.BackColor = Color.Transparent;
                ShowMsgLabel(0);
            }
        }

        private void labF5_Click(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                Finger = 5;
                labF5.BackColor = Color.LightBlue;
                string nombrededo = Utils.GetNombreDedo(Finger);
                ProcesaDedoSeleccionado(Finger, nombrededo);
                FingerName = nombrededo;
                CaptureFinger();
            }
        }

        private void labF5_MouseMove(object sender, MouseEventArgs e)
        {
            if (Finger == 0)
            {
                ShowMsgLabel(5);
                labF5.BackColor = Color.LightGray;
            }
        }

        private void labF5_MouseLeave(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                labF5.BackColor = Color.Transparent;
                ShowMsgLabel(0);
            }
        }

        private void labF6_Click(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                Finger = 6;
                labF6.BackColor = Color.LightBlue;
                string nombrededo = Utils.GetNombreDedo(Finger);
                ProcesaDedoSeleccionado(Finger, nombrededo);
                FingerName = nombrededo;
                CaptureFinger();
            }
        }

        private void labF6_MouseMove(object sender, MouseEventArgs e)
        {
            if (Finger == 0)
            {
                labF6.BackColor = Color.LightGray;
                ShowMsgLabel(0);
            }
        }

        private void labF6_MouseLeave(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                ShowMsgLabel(6);
                labF6.BackColor = Color.Transparent;
            }
        }

        private void labF7_Click(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                Finger = 7;
                labF7.BackColor = Color.LightBlue;
                string nombrededo = Utils.GetNombreDedo(Finger);
                ProcesaDedoSeleccionado(Finger, nombrededo);
                FingerName = nombrededo;
                CaptureFinger();
            }
        }

        private void labF7_MouseMove(object sender, MouseEventArgs e)
        {
            if (Finger == 0)
            {
                ShowMsgLabel(7);
                labF7.BackColor = Color.LightGray;
            }
        }

        private void labF7_MouseLeave(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                labF7.BackColor = Color.Transparent;
                ShowMsgLabel(0);
            }
        }

        private void labF8_Click(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                Finger = 8;
                labF8.BackColor = Color.LightBlue;
                string nombrededo = Utils.GetNombreDedo(Finger);
                ProcesaDedoSeleccionado(Finger, nombrededo);
                FingerName = nombrededo;
                CaptureFinger();
            }
        }

        private void labF8_MouseMove(object sender, MouseEventArgs e)
        {
            if (Finger == 0)
            {
                ShowMsgLabel(8);
                labF8.BackColor = Color.LightGray;
            }
        }

        private void labF8_MouseLeave(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                labF8.BackColor = Color.Transparent;
                ShowMsgLabel(0);
            }
        }

        private void labF9_Click(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                Finger = 9;
                labF9.BackColor = Color.LightBlue;
                string nombrededo = Utils.GetNombreDedo(Finger);
                ProcesaDedoSeleccionado(Finger, nombrededo);
                FingerName = nombrededo;
                CaptureFinger();
            }
        }

        private void labF9_MouseMove(object sender, MouseEventArgs e)
        {
            if (Finger == 0)
            {
                ShowMsgLabel(9);
                labF9.BackColor = Color.LightGray;
            }
        }

        private void labF9_MouseLeave(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                labF9.BackColor = Color.Transparent;
                ShowMsgLabel(0);
            }
        }

        private void labF10_Click(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                Finger = 10;
                labF10.BackColor = Color.LightBlue;
                string nombrededo = Utils.GetNombreDedo(Finger);
                ProcesaDedoSeleccionado(Finger, nombrededo);
                FingerName = nombrededo;
                CaptureFinger();
            }
        }

        private void labF10_MouseMove(object sender, MouseEventArgs e)
        {
            if (Finger == 0)
            {
                ShowMsgLabel(10);
                labF10.BackColor = Color.LightGray;
            }
        }

        private void labF10_MouseLeave(object sender, EventArgs e)
        {
            if (Finger == 0)
            {
                labF10.BackColor = Color.Transparent;
                ShowMsgLabel(0);
            }
        }

        private void labFeedbackBotton_Click(object sender, EventArgs e)
        {

        }
        #endregion
        #endregion Manejo Grafica

        // FOR BIOAPI
        private void CaptureFinger(string typeId, string valueId, int fingerId)
        {
            try
            {
                //if (reader == null)
                //    reader = (SecugenReader)ReaderFactory.GetReader(ReaderFactory.ReaderBrand.SECUGEN);
                //if (BSP == null) InitBSP();
                int quality = 70;
                try
                {
                    quality = Properties.Settings.Default.QualityCapture;
                }
                catch (Exception)
                {
                    quality = 70;
                }

                _SamplesCaptured = null;
                Error err = null;
                if (!String.IsNullOrEmpty(typeId) && !String.IsNullOrEmpty(valueId)) // && SensorConnected)
                {
                    if (fingerId >= 1 && fingerId <= 10)
                    {
                        BSP.Capture(Convert.ToInt32(dictParamIn["DEVICE"]), fingerId, null, 
                                    Convert.ToInt32(dictParamIn["TIMEOUT"]), 0, quality, out err);
                    }
                }
                if (err != null && err.ErrorCode > 0)
                {
                    LOG.Error("BioPortalClient7UIAdapter.BSP.Capture Error CaptureFinger [" + 
                        err.ErrorCode + " - " +err.ErrorDescription + "]");
                    //Tratar error
                }
                //SerialSensor = reader.ReaderInfo.SerialNumber;

                //if (SerialSensor.Contains("\0"))
                //{
                //    SerialSensor = SerialSensor.Replace("\0", "");
                //}
            }
            catch (Exception e)
            {
                var error = e.Message;
                LOG.Error("BioPortalClient7UIAdapter.CaptureFinger Error CaptureFinger [" + e.Message + "]");
                //throw;
                //ELog(string.Format("CaptureFinger -> GetReader() {0}", error));
            }

            if (SensorConnected && !String.IsNullOrEmpty(typeId) && !String.IsNullOrEmpty(valueId))
            {
                if (fingerId >= 1 && fingerId <= 10)
                {
                    BeginCapture(fingerId);
                }
            }
            else
            {
                //ELog(string.Format("CaptureFinger -> sc {0}", SensorConnected));
            }
        }

        private void BeginCapture(int fingerId)
        {
            try
            {
                //reader.OnCapturedEvent += OnCapturedEvent;

                //reader = ReaderFactory.GetReader();
                //SerialSensor = reader.ReaderInfo.SerialNumber;

                //if (reader.ActualReader() == null)
                //    reader.SetReaderSecugen(SecugenManager);

                //reader.HardCapture(reader.ActualReader(), fingerId);
                //reader.Capture(fingerId);
            }
            catch (Exception ex)
            {
                //reader.OnCapturedEvent -= OnCapturedEvent;
            }
        }

        //private void OnCapturedEvent(IFingerCaptured fingerCaptured)
        //{
        //    //TokenResponse = new TokenResponse();
        //    //bool hasCapture = false;
        //    //reader.OnCapturedEvent -= OnCapturedEvent;
        //    //try
        //    //{
        //    //    if (fingerCaptured.CaptureQuality == ReaderFactory.CaptureQuality.QUALITY_GOOD)
        //    //    {
        //    //        LOG.Debug("BioPortalClient7UI.CaptureFinger Huella No Null...");
        //    //        TokenResponse.Huella = GetImageB64FromImage(fingerCaptured.Image);
        //    //        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.Huella = " + TokenResponse.Huella);
        //    //        TokenResponse.Width = fingerCaptured.Image.Width;
        //    //        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.Width = " + TokenResponse.Width);
        //    //        TokenResponse.Height = fingerCaptured.Image.Height;
        //    //        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.Height = " + TokenResponse.Height);
        //    //        TokenResponse.BodyPart = Finger;
        //    //        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.BodyPart = " + TokenResponse.BodyPart);
        //    //        picSample.Image = fingerCaptured.Image; // BioRouterInterpreter.Base64ToImage(serverResponse.Huella);
        //    //        picSample.Refresh();
        //    //        LOG.Debug("BioPortalClient7UI.CaptureFinger picSample.Image filled (picSample.Image != null) => " +
        //    //            (picSample.Image != null).ToString());
        //    //        this.Refresh();

        //    //        hasCapture = GenerateToken(fingerCaptured.WSQ64, fingerCaptured.TemplateRegistry64, fingerCaptured.TemplateVerify64, 
        //    //                                    fingerCaptured.TemplateANSI64, fingerCaptured.TemplateISO64);
        //    //        LOG.Debug("BioPortalClient7UI.CaptureFinger GenerateToken = " + hasCapture.ToString());
        //    //        if (hasCapture) SetColorLabF(Color.Green);
        //    //        else SetColorLabF(Color.Red);
        //    //        Thread.Sleep(1000);

        //    //        //bioPacketLocal.PacketParamOut.Add("TemplateVerify64", fingerCaptured.TemplateVerify64);
        //    //        //bioPacketLocal.PacketParamOut.Add("TemplateRegistry64", fingerCaptured.TemplateRegistry64);
        //    //        //bioPacketLocal.PacketParamOut.Add("TemplateANSI64", fingerCaptured.TemplateANSI64);
        //    //        //bioPacketLocal.PacketParamOut.Add("TemplateISO64", fingerCaptured.TemplateISO64);
        //    //        //bioPacketLocal.PacketParamOut.Add("WSQ", fingerCaptured.WSQ64);

        //    //        //bioPacketLocal.PacketParamOut.Add("Huella", fingerCaptured.Image);
        //    //        //bioPacketLocal.PacketParamOut.Add("Width", fingerCaptured.Image.Width);
        //    //        //bioPacketLocal.PacketParamOut.Add("Height", fingerCaptured.Image.Height);

        //    //        //bioPacketLocal.PacketParamOut.Add("Error", "0|Capture OK");
        //    //        //bioPacketLocal.PacketParamOut.Add("BodyPart", dictParamIn["Finger"]);
        //    //        ////bioPacketLocal.PacketParamOut.Add("BodyPartName", );
        //    //        //bioPacketLocal.PacketParamOut.Add("AuthenticationFactor", 2);  //AF =  Fingerprint
        //    //        ////bioPacketLocal.PacketParamOut.Add("AuthenticationFactorName", );
        //    //        //bioPacketLocal.PacketParamOut.Add("SerialDevice", SerialSensor);

        //    //        //picSample.Image = fingerCaptured.Image;
        //    //        //picSample.Refresh();
        //    //        hasCapture = true;
        //    //    }
        //    //    else if (fingerCaptured.CaptureQuality == ReaderFactory.CaptureQuality.QUALITY_POOR)
        //    //    {
        //    //        bioPacketLocal.PacketParamOut.Add("Error", "-41|Mala Calidad Captura"); //Mala Calidad => SERR_WSQ_LOW_QUALITY
        //    //        picSample.Refresh();
        //    //    }
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    bioPacketLocal.PacketParamOut.Add("Error", "-1|Error Procesando Captura [" + ex.Message + "]");
        //    //    //WriteLog("ERROR - BeginCapture Error - " + ex.Message);
        //    //}
        //    //CloseControl(hasCapture);
        //}
        private string GetImageB64FromImage(object image)
        {
            byte[] byImg;
            try
            {
                System.Drawing.Image img = (System.Drawing.Image)(((System.Drawing.Image)image).Clone());
                byImg = ImageToByteArray(img);
                return Convert.ToBase64String(byImg);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            try
            {
                using (var ms = new MemoryStream())
                {
                    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

#region BioAPI.2.0

        private void InitBSP()
        {
            //BSP = new Biometrika.BioApi20.BSP.BSPBiometrika();

            //BSP.BSPAttach("2.0", true);

            //GetInfoFromBSP();

            BSP.OnCapturedEvent += OnCaptureEvent;
            BSP.OnTimeoutEvent += OnTimeoutEvent;
        }

        private void OnCaptureEvent(int errCode, string errMessage, List<Sample> samplesCaptured)
        {
            string _WSQ = null;
            string _TemplateEnroll = null;
            string _TemplateVerify = null;
            string _TemplateISO = null;
            string _TemplateANSI = null;
            try
            {
                _SamplesCaptured = samplesCaptured;

                //Sample s;
                //try
                //{
                //    if (samplesCaptured != null && samplesCaptured.Count > 0)
                //    {
                //        //if (chkOrigianal.Checked)
                //        //{
                //        //    s = samplesCaptured[samplesCaptured.Count - 2];
                //        //}
                //        //else
                //        //{
                //            s = samplesCaptured[samplesCaptured.Count - 1];
                //        //}
                //        if (s.MinutiaeType == 41) picSample.Image = (Bitmap)s.Data;
                //    }
                //}
                //catch (Exception ex)
                //{
                //    //richTextBox1.Text = "Error obteniendo imagen!" + Environment.NewLine;
                //}

                //richTextBox1.Text += "Samples Obtenidos:" + Environment.NewLine;
                //richTextBox1.Text += "----------------------------------------------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;
                //foreach (Sample item in samplesCaptured)
                //{
                //    string saux = item.SampleWith + "x" + item.SampleHeight;
                //    //richTextBox1.Text += "AFactor=" + item.AuthenticationFactor.ToString() +
                //    //                     " - MType=" + item.MinutiaeType.ToString() +
                //    //                     " - BodyPart=" + item.BodyPart.ToString() +
                //    //                     " - Operacion=" + (item.Operation == 0 ? "Verify" : "Enroll") +
                //    //                     " - WxH=" + saux +
                //    //                     " - Sample=" + (item.MinutiaeType == 41 ? "Image" : Convert.ToBase64String((byte[])item.Data).Substring(0, 10) + "...")
                //    //                     + Environment.NewLine;
                //}
                //richTextBox1.Text += "----------------------------------------------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;

                string aux = "";
                

                int idx = 0;
                foreach (Sample item in samplesCaptured)
                {
                    //if (item.MinutiaeType == 41)
                    //{
                    //    System.IO.File.WriteAllBytes(@"c:\std\sample_mt_41_" + (idx++) + ".bmp", (byte[])item.Data);
                    //    Bitmap b = (Bitmap)item.Data;
                    //    b.Save(@"c:\std\sample_mt_41_" + (idx++) + ".jpg", ImageFormat.Jpeg);
                    //}
                    //else
                    //{
                    if (item.MinutiaeType == 13 && item.Operation == 0)  //Solo proceso los de verify
                    {
                        _TemplateANSI = Convert.ToBase64String((byte[])item.Data);
                    }
                    if (item.MinutiaeType == 14 && item.Operation == 0)  //Solo proceso los de verify
                    {
                        _TemplateISO = Convert.ToBase64String((byte[])item.Data);
                    }

                    if (item.MinutiaeType == 21)  //Solo proceso los de verify
                    {
                        _WSQ = Convert.ToBase64String((byte[])item.Data);
                    }

                    if (item.MinutiaeType != 41 && item.MinutiaeType != 13 && item.MinutiaeType != 14 &&
                        item.MinutiaeType != 22 && item.MinutiaeType != 21)  //Solo proceso los de verify
                    {
                        if (item.Operation == 0) //Es verify
                        {
                            _TemplateVerify = Convert.ToBase64String((byte[])item.Data);
                        } else //es Enroll
                        {
                            _TemplateEnroll = Convert.ToBase64String((byte[])item.Data);
                        }
                    }
                    //}
                }

                TokenResponse = new TokenResponse();
                bool hasCapture = false;
                //reader.OnCapturedEvent -= OnCapturedEvent;
                try
                {
                    //if (fingerCaptured.CaptureQuality == ReaderFactory.CaptureQuality.QUALITY_GOOD)
                    if (errCode == 0 && samplesCaptured != null)
                    {
                        LOG.Debug("BioPortalClient7UI.CaptureFinger Huella No Null...");
                        TokenResponse.Huella = GetImageB64FromImage((Bitmap)(samplesCaptured[samplesCaptured.Count - 1]).Data);
                        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.Huella = " + TokenResponse.Huella);
                        TokenResponse.Width = samplesCaptured[samplesCaptured.Count - 1].SampleWith; // fingerCaptured.Image.Width;
                        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.Width = " + TokenResponse.Width);
                        TokenResponse.Height = samplesCaptured[samplesCaptured.Count - 1].SampleHeight; //fingerCaptured.Image.Height;
                        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.Height = " + TokenResponse.Height);
                        TokenResponse.BodyPart = Finger;
                        LOG.Debug("BioPortalClient7UI.CaptureFinger TokenResponse.BodyPart = " + TokenResponse.BodyPart);
                        picSample.Image = (Bitmap)(samplesCaptured[samplesCaptured.Count - 2]).Data; // fingerCaptured.Image; // BioRouterInterpreter.Base64ToImage(serverResponse.Huella);
                        picSample.Refresh();
                        LOG.Debug("BioPortalClient7UI.CaptureFinger picSample.Image filled (picSample.Image != null) => " +
                            (picSample.Image != null).ToString());
                        this.Refresh();

                        //hasCapture = GenerateToken(fingerCaptured.WSQ64, fingerCaptured.TemplateRegistry64,
                        //                            fingerCaptured.TemplateVerify64,
                        //                            fingerCaptured.TemplateANSI64, fingerCaptured.TemplateISO64);
                        hasCapture = GenerateToken(_WSQ, _TemplateEnroll, _TemplateVerify, _TemplateANSI, _TemplateISO);
                        LOG.Debug("BioPortalClient7UI.CaptureFinger GenerateToken = " + hasCapture.ToString());
                        if (hasCapture) SetColorLabF(Color.Green);
                        else SetColorLabF(Color.Red);
                        Thread.Sleep(1000);

                        //bioPacketLocal.PacketParamOut.Add("TemplateVerify64", fingerCaptured.TemplateVerify64);
                        //bioPacketLocal.PacketParamOut.Add("TemplateRegistry64", fingerCaptured.TemplateRegistry64);
                        //bioPacketLocal.PacketParamOut.Add("TemplateANSI64", fingerCaptured.TemplateANSI64);
                        //bioPacketLocal.PacketParamOut.Add("TemplateISO64", fingerCaptured.TemplateISO64);
                        //bioPacketLocal.PacketParamOut.Add("WSQ", fingerCaptured.WSQ64);

                        //bioPacketLocal.PacketParamOut.Add("Huella", fingerCaptured.Image);
                        //bioPacketLocal.PacketParamOut.Add("Width", fingerCaptured.Image.Width);
                        //bioPacketLocal.PacketParamOut.Add("Height", fingerCaptured.Image.Height);

                        //bioPacketLocal.PacketParamOut.Add("Error", "0|Capture OK");
                        //bioPacketLocal.PacketParamOut.Add("BodyPart", dictParamIn["Finger"]);
                        ////bioPacketLocal.PacketParamOut.Add("BodyPartName", );
                        //bioPacketLocal.PacketParamOut.Add("AuthenticationFactor", 2);  //AF =  Fingerprint
                        ////bioPacketLocal.PacketParamOut.Add("AuthenticationFactorName", );
                        //bioPacketLocal.PacketParamOut.Add("SerialDevice", SerialSensor);

                        //picSample.Image = fingerCaptured.Image;
                        //picSample.Refresh();
                        hasCapture = true;
                    }
                    //else if (fingerCaptured.CaptureQuality == ReaderFactory.CaptureQuality.QUALITY_POOR)
                    //{
                    //    bioPacketLocal.PacketParamOut.Add("Error", "-41|Mala Calidad Captura"); //Mala Calidad => SERR_WSQ_LOW_QUALITY
                    //    picSample.Refresh();
                    //}
                }
                catch (Exception ex)
                {
                    bioPacketLocal.PacketParamOut.Add("Error", "-1|Error Procesando Captura [" + ex.Message + "]");
                    //WriteLog("ERROR - BeginCapture Error - " + ex.Message);
                }
                CloseControl(hasCapture);
            }
            catch (Exception ex)
            {

            }
            //timer1.Enabled = true;
        }

        private void OnTimeoutEvent(int errCode, string errMessage, ISensor sensor)
        {
            picSample.Image = null;
            MessageBox.Show(errCode + " - " + errMessage);
        }

        #endregion BioaAPI.2.0

    }



    internal class MsgError
    {

        public MsgError() { }

        public MsgError(string strerr)
        {
            try
            {
                if (String.IsNullOrEmpty(strerr))
                {
                    Codigo = -1;
                    Description = "Retorno Nulo";
                }
                else
                {
                    string[] arrerr = strerr.Split('|');
                    Codigo = Convert.ToInt32(arrerr[0]);
                    Description = arrerr[1];
                }
            }
            catch (Exception ex)
            {
                Description = "Error parseando error return [" + ex.Message + "]";
            }
        }

        public int Codigo { get; set; }
        public string Description { get; set; }

    }

}
