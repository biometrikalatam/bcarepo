﻿using System.Windows.Forms;

namespace BioPortalClient7UIAdapter
{
    public partial class frmHelp : Form
    {
        public frmHelp()
        {
            InitializeComponent();
        }

        private void frmHelp_MouseClick(object sender, MouseEventArgs e)
        {
            this.Close();
        }
    }
}