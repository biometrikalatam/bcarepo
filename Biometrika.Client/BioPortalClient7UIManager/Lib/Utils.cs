﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Cryptography;

namespace BioPortalClient7UIManager.Lib
{
    internal static class Utils
    {
        internal static Posicion[] pDedos;

        private static string[] fingerlist = new string[11];

        static Utils()
        {
            fingerlist[0] = "Ningún Dedo Selecionado";
            fingerlist[1] = "Pulgar Derecho";
            fingerlist[2] = "Indice Derecho";
            fingerlist[3] = "Mayor Derecho";
            fingerlist[4] = "Anular Derecho";
            fingerlist[5] = "Meñique Derecho";
            fingerlist[6] = "Pulgar Izquierdo";
            fingerlist[7] = "Indice Izquierdo";
            fingerlist[8] = "Mayor Izquierdo";
            fingerlist[9] = "Anular Izquierdo";
            fingerlist[10] = "Meñique Izquierdo";
        }

        public static string[] GetFingerList()
        {
            return fingerlist;
        }

        public static Dictionary<int, string> GetFingers()
        {
            var fingers = new Dictionary<int, string>();
            for (var i = 0; i <= 10; i++)
            {
                fingers.Add(i, fingerlist[i]);
            }
            return fingers;
        }

        public static string FingerName(int fingerId)
        {
            return fingerlist[fingerId];
        }

        internal static void LoadPosicionesDedos()
        {
            pDedos = new Posicion[11];

            pDedos[1] = new Posicion() { nombre = "Dedo Pulgar Derecho", xMin = 132, xMax = 145, yMin = 50, yMax = 71 };

            pDedos[2] = new Posicion() { nombre = "Dedo Indice Derecho", xMin = 151, xMax = 159, yMin = 20, yMax = 48 };

            pDedos[3] = new Posicion() { nombre = "Dedo Mayor Derecho", xMin = 166, xMax = 171, yMin = 13, yMax = 47 };

            pDedos[4] = new Posicion() { nombre = "Dedo Anular Derecho", xMin = 179, xMax = 187, yMin = 18, yMax = 41 };

            pDedos[5] = new Posicion() { nombre = "Dedo Meñique Derecho", xMin = 191, xMax = 200, yMin = 32, yMax = 45 };

            pDedos[6] = new Posicion() { nombre = "Dedo Pulgar Izquierdo", xMin = 75, xMax = 86, yMin = 49, yMax = 69 };

            pDedos[7] = new Posicion() { nombre = "Dedo Indice Izquierdo", xMin = 59, xMax = 68, yMin = 19, yMax = 48 };

            pDedos[8] = new Posicion() { nombre = "Dedo Mayor Izquierdo", xMin = 47, xMax = 53, yMin = 12, yMax = 44 };

            pDedos[9] = new Posicion() { nombre = "Dedo Anular Izquierdo", xMin = 33, xMax = 40, yMin = 15, yMax = 47 };

            pDedos[10] = new Posicion() { nombre = "Dedo Meñique Izquierdo", xMin = 20, xMax = 28, yMin = 31, yMax = 51 };
        }

        internal static string DedoSeleccionado(int x, int y, out int indice)
        {
            var sret = string.Empty;
            indice = 0;
            if (pDedos==null)
                LoadPosicionesDedos();
            try
            {
                indice = 0;
                sret = string.Empty;
                for (var i = 1; i < 11; i++)
                {
                    if (x >= pDedos[i].xMin && pDedos[i].xMax >= x &&
                           y >= pDedos[i].yMin && pDedos[i].yMax >= y)
                    {
                        sret = pDedos[i].nombre;
                        indice = i;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return sret;
        }

        internal static string GetNombreDedo(int idedo)
        {
            switch (idedo)
            {
                case 1:
                    return "Pulgar Derecho";

                case 2:
                    return "Indice Derecho";

                case 3:
                    return "Mayor Derecho";

                case 4:
                    return "Anular Derecho";

                case 5:
                    return "Meñique Derecho";

                case 6:
                    return "Pulgar Izquierdo";

                case 7:
                    return "Indice Izquierdo";

                case 8:
                    return "Mayor Izquierdo";

                case 9:
                    return "Anular Izquierdo";

                case 10:
                    return "Meñique Izquierdo";

                default:
                    return "Ningún Dedo Selecionado";
            }
        }

        internal static string GetMACAddress()
        {
            var sMacAddress = string.Empty;
            var isFirst = true;

            var nics = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface adapter in nics)
            {
                adapter.GetIPProperties();
                if (isFirst)
                {
                    sMacAddress = FormatMAC(adapter.GetPhysicalAddress().ToString());
                    isFirst = false;
                }
                else
                {
                    sMacAddress = String.Format("{0}~{1}", sMacAddress, FormatMAC(adapter.GetPhysicalAddress().ToString()));
                }
            }

            return sMacAddress;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        internal static string GetIP()
        {
            var sIP = string.Empty;
            var isFirst = true;

            var localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress ipAddress in localIPs)
            {
                if (ipAddress.ToString().Length <= 15)
                {
                    if (isFirst)
                    {
                        sIP = ipAddress.ToString();
                        isFirst = false;
                    }
                    else
                    {
                        sIP = String.Format("{0}~{1}", sIP, ipAddress);
                    }
                }
            }

            return sIP;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="smac"></param>
        /// <returns></returns>
        private static string FormatMAC(string smac)
        {
            var sMacAddress = string.Empty;

            if (!String.IsNullOrEmpty(smac))
            {
                sMacAddress = String.Format("{0}-{1}-{2}-{3}", smac.Substring(0, 2), smac.Substring(2, 2), smac.Substring(4, 2), smac.Substring(6, 2));
            }

            return sMacAddress;
        }

        internal static string GeneraToken(string Typeid, string Valueid, string SerialSensor, short _finger, string _wsq, string templateToken,
            string _timestamp, string _ip, short _tokencontant, short _minutiaetype, short _device, short _afactor, short _operationtype,
            ref short errBpToken)
        {
            string _tokenret = null;
            errBpToken = 0;
            try
            {
                string _plaintoken = Typeid + "|" +
                                     Valueid + "|" +
                                     SerialSensor + "|" +
                                     _finger + "|" +
                                     _wsq + "|" +
                                     templateToken + "|" +
                                     _timestamp + "|" +
                                     _ip + "|" +
                                     _tokencontant + "|" +
                                     _minutiaetype + "|0|" +
                                     _device + "|" +
                                     _afactor + "|" +
                                     _operationtype;

                //byte[] byplaintoken = UTF8Encoding.UTF8.GetBytes(_plaintoken);
                //byte[] byencryptedtoken = RSAEncrypt(byplaintoken, false);

                //Encrypt token Symetric 3DES generando key
                //Encripto clave simetrica pero con RSA (Asymetric) y concateno
                _tokenret = "AV050105" + Crypto.EncryptWithSymmetricAid(Crypto.PUBLIC_KEY, _plaintoken);//Convert.ToBase64String(byencryptedtoken);
            }
            catch (Exception ex)
            {
                errBpToken = -1;
                // "UPBlient.Utils.GeneraToken Error [" + ex.Message + "]";
                _tokenret = null;
            }
            return _tokenret;
        }

        static public byte[] RSAEncrypt(byte[] DataToEncrypt, bool DoOAEPPadding) //RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            try
            {
                byte[] encryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    //Import the RSA Key information. This only needs
                    //toinclude the public key information.
                    //                    RSA.ImportParameters(RSAKeyInfo);
                    RSA.FromXmlString("<RSAKeyValue><Modulus>18aLaldVbtjt6eLyKLkOJOVctaPyi4rPRTLBU9Sxj5TF0RSkgiJTcScWVeUNxAIWWLoxHPO51itjdJtqhi/uS44fC+9S3Iao6Wq9kJvWwp4H/C1a8OE0ubT6+2IqV+bw12jjHCDEUiMFUlwJeZ+MWTHb9ZbSVn4TbEvNinwmaDM=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
                    //Encrypt the passed byte array and specify OAEP padding.
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.
                    encryptedData = RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
                }
                return encryptedData;
            }
            //Catch and display a CryptographicException
            //to the console.
            catch (CryptographicException e)
            {
                //Console.WriteLine(e.Message);

                return null;
            }
        }
    }
}