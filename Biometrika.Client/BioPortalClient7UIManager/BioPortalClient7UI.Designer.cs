﻿namespace BioPortalClient7UIAdapter
{
    partial class BioPortalClient7UI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
            catch (System.Exception)
            {
                
            }
            
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BioPortalClient7UI));
            this.labFeedbackBotton = new System.Windows.Forms.Label();
            this.labtimeout = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.picDelete = new System.Windows.Forms.PictureBox();
            this.labMsg = new System.Windows.Forms.Label();
            this.labDedoSeleccionado = new System.Windows.Forms.Label();
            this.picSample = new System.Windows.Forms.PictureBox();
            this.picSalir = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labF1 = new System.Windows.Forms.Label();
            this.labF2 = new System.Windows.Forms.Label();
            this.labF3 = new System.Windows.Forms.Label();
            this.labF4 = new System.Windows.Forms.Label();
            this.labF5 = new System.Windows.Forms.Label();
            this.labF6 = new System.Windows.Forms.Label();
            this.labF7 = new System.Windows.Forms.Label();
            this.labF8 = new System.Windows.Forms.Label();
            this.labF9 = new System.Windows.Forms.Label();
            this.labF10 = new System.Windows.Forms.Label();
            this.labVersion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSalir)).BeginInit();
            this.SuspendLayout();
            // 
            // labFeedbackBotton
            // 
            this.labFeedbackBotton.BackColor = System.Drawing.Color.Transparent;
            this.labFeedbackBotton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labFeedbackBotton.ForeColor = System.Drawing.Color.DimGray;
            this.labFeedbackBotton.Location = new System.Drawing.Point(42, 109);
            this.labFeedbackBotton.Name = "labFeedbackBotton";
            this.labFeedbackBotton.Size = new System.Drawing.Size(720, 52);
            this.labFeedbackBotton.TabIndex = 13;
            this.labFeedbackBotton.Text = "PASO 1 - Seleccione dedo a capturar...";
            this.labFeedbackBotton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labFeedbackBotton.Click += new System.EventHandler(this.labFeedbackBotton_Click);
            // 
            // labtimeout
            // 
            this.labtimeout.BackColor = System.Drawing.Color.Transparent;
            this.labtimeout.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labtimeout.ForeColor = System.Drawing.Color.Red;
            this.labtimeout.Location = new System.Drawing.Point(516, 88);
            this.labtimeout.Name = "labtimeout";
            this.labtimeout.Size = new System.Drawing.Size(235, 19);
            this.labtimeout.TabIndex = 12;
            this.labtimeout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Location = new System.Drawing.Point(660, 46);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(35, 35);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 11;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Location = new System.Drawing.Point(600, 46);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(35, 35);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // picDelete
            // 
            this.picDelete.BackColor = System.Drawing.Color.Transparent;
            this.picDelete.Location = new System.Drawing.Point(541, 46);
            this.picDelete.Name = "picDelete";
            this.picDelete.Size = new System.Drawing.Size(35, 35);
            this.picDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDelete.TabIndex = 9;
            this.picDelete.TabStop = false;
            this.picDelete.Click += new System.EventHandler(this.picDelete_Click);
            // 
            // labMsg
            // 
            this.labMsg.BackColor = System.Drawing.Color.Transparent;
            this.labMsg.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMsg.ForeColor = System.Drawing.Color.White;
            this.labMsg.Location = new System.Drawing.Point(108, 54);
            this.labMsg.Name = "labMsg";
            this.labMsg.Size = new System.Drawing.Size(369, 22);
            this.labMsg.TabIndex = 4;
            this.labMsg.Text = "Validando:";
            this.labMsg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labDedoSeleccionado
            // 
            this.labDedoSeleccionado.BackColor = System.Drawing.Color.Transparent;
            this.labDedoSeleccionado.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDedoSeleccionado.ForeColor = System.Drawing.Color.DarkGray;
            this.labDedoSeleccionado.Location = new System.Drawing.Point(142, 184);
            this.labDedoSeleccionado.Name = "labDedoSeleccionado";
            this.labDedoSeleccionado.Size = new System.Drawing.Size(241, 15);
            this.labDedoSeleccionado.TabIndex = 5;
            this.labDedoSeleccionado.Text = "Ningún dedo seleccionado...";
            this.labDedoSeleccionado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picSample
            // 
            this.picSample.Image = global::BioPortalClient7UIAdapter.Properties.Resources.FondoBPC_Boton_plan_griso;
            this.picSample.Location = new System.Drawing.Point(563, 261);
            this.picSample.Name = "picSample";
            this.picSample.Size = new System.Drawing.Size(151, 153);
            this.picSample.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSample.TabIndex = 3;
            this.picSample.TabStop = false;
            // 
            // picSalir
            // 
            this.picSalir.BackColor = System.Drawing.Color.Transparent;
            this.picSalir.Location = new System.Drawing.Point(715, 46);
            this.picSalir.Name = "picSalir";
            this.picSalir.Size = new System.Drawing.Size(35, 35);
            this.picSalir.TabIndex = 15;
            this.picSalir.TabStop = false;
            this.picSalir.Click += new System.EventHandler(this.picSalir_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(723, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 20);
            this.label1.TabIndex = 16;
            this.label1.Text = "X";
            this.label1.Click += new System.EventHandler(this.picSalir_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(667, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 20);
            this.label2.TabIndex = 17;
            this.label2.Text = "A";
            this.label2.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(607, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 20);
            this.label3.TabIndex = 18;
            this.label3.Text = "H";
            this.label3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(547, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 20);
            this.label4.TabIndex = 19;
            this.label4.Text = "B";
            this.label4.Click += new System.EventHandler(this.picDelete_Click);
            // 
            // labF1
            // 
            this.labF1.BackColor = System.Drawing.Color.Transparent;
            this.labF1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labF1.Location = new System.Drawing.Point(291, 329);
            this.labF1.Name = "labF1";
            this.labF1.Size = new System.Drawing.Size(24, 35);
            this.labF1.TabIndex = 20;
            this.labF1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.labF1_MouseClick);
            this.labF1.MouseLeave += new System.EventHandler(this.labF1_MouseLeave);
            this.labF1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labF1_MouseMove);
            // 
            // labF2
            // 
            this.labF2.BackColor = System.Drawing.Color.Transparent;
            this.labF2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labF2.Location = new System.Drawing.Point(332, 238);
            this.labF2.Name = "labF2";
            this.labF2.Size = new System.Drawing.Size(24, 35);
            this.labF2.TabIndex = 21;
            this.labF2.Click += new System.EventHandler(this.labF2_Click);
            this.labF2.MouseLeave += new System.EventHandler(this.labF2_MouseLeave);
            this.labF2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labF2_MouseMove);
            // 
            // labF3
            // 
            this.labF3.BackColor = System.Drawing.Color.Transparent;
            this.labF3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labF3.Location = new System.Drawing.Point(372, 212);
            this.labF3.Name = "labF3";
            this.labF3.Size = new System.Drawing.Size(24, 35);
            this.labF3.TabIndex = 22;
            this.labF3.Click += new System.EventHandler(this.labF3_Click);
            this.labF3.MouseLeave += new System.EventHandler(this.labF3_MouseLeave);
            this.labF3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labF3_MouseMove);
            // 
            // labF4
            // 
            this.labF4.BackColor = System.Drawing.Color.Transparent;
            this.labF4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labF4.Location = new System.Drawing.Point(411, 230);
            this.labF4.Name = "labF4";
            this.labF4.Size = new System.Drawing.Size(24, 35);
            this.labF4.TabIndex = 23;
            this.labF4.Click += new System.EventHandler(this.labF4_Click);
            this.labF4.MouseLeave += new System.EventHandler(this.labF4_MouseLeave);
            this.labF4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labF4_MouseMove);
            // 
            // labF5
            // 
            this.labF5.BackColor = System.Drawing.Color.Transparent;
            this.labF5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labF5.Location = new System.Drawing.Point(451, 246);
            this.labF5.Name = "labF5";
            this.labF5.Size = new System.Drawing.Size(24, 35);
            this.labF5.TabIndex = 24;
            this.labF5.Click += new System.EventHandler(this.labF5_Click);
            this.labF5.MouseLeave += new System.EventHandler(this.labF5_MouseLeave);
            this.labF5.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labF5_MouseMove);
            // 
            // labF6
            // 
            this.labF6.BackColor = System.Drawing.Color.Transparent;
            this.labF6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labF6.Location = new System.Drawing.Point(223, 329);
            this.labF6.Name = "labF6";
            this.labF6.Size = new System.Drawing.Size(24, 35);
            this.labF6.TabIndex = 25;
            this.labF6.Click += new System.EventHandler(this.labF6_Click);
            this.labF6.MouseLeave += new System.EventHandler(this.labF6_MouseLeave);
            this.labF6.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labF6_MouseMove);
            // 
            // labF7
            // 
            this.labF7.BackColor = System.Drawing.Color.Transparent;
            this.labF7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labF7.Location = new System.Drawing.Point(182, 238);
            this.labF7.Name = "labF7";
            this.labF7.Size = new System.Drawing.Size(24, 35);
            this.labF7.TabIndex = 26;
            this.labF7.Click += new System.EventHandler(this.labF7_Click);
            this.labF7.MouseLeave += new System.EventHandler(this.labF7_MouseLeave);
            this.labF7.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labF7_MouseMove);
            // 
            // labF8
            // 
            this.labF8.BackColor = System.Drawing.Color.Transparent;
            this.labF8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labF8.Location = new System.Drawing.Point(142, 212);
            this.labF8.Name = "labF8";
            this.labF8.Size = new System.Drawing.Size(24, 35);
            this.labF8.TabIndex = 27;
            this.labF8.Click += new System.EventHandler(this.labF8_Click);
            this.labF8.MouseLeave += new System.EventHandler(this.labF8_MouseLeave);
            this.labF8.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labF8_MouseMove);
            // 
            // labF9
            // 
            this.labF9.BackColor = System.Drawing.Color.Transparent;
            this.labF9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labF9.Location = new System.Drawing.Point(104, 230);
            this.labF9.Name = "labF9";
            this.labF9.Size = new System.Drawing.Size(24, 35);
            this.labF9.TabIndex = 28;
            this.labF9.Click += new System.EventHandler(this.labF9_Click);
            this.labF9.MouseLeave += new System.EventHandler(this.labF9_MouseLeave);
            this.labF9.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labF9_MouseMove);
            // 
            // labF10
            // 
            this.labF10.BackColor = System.Drawing.Color.Transparent;
            this.labF10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labF10.Location = new System.Drawing.Point(64, 246);
            this.labF10.Name = "labF10";
            this.labF10.Size = new System.Drawing.Size(24, 35);
            this.labF10.TabIndex = 29;
            this.labF10.Click += new System.EventHandler(this.labF10_Click);
            this.labF10.MouseLeave += new System.EventHandler(this.labF10_MouseLeave);
            this.labF10.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labF10_MouseMove);
            // 
            // labVersion
            // 
            this.labVersion.BackColor = System.Drawing.Color.Transparent;
            this.labVersion.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labVersion.ForeColor = System.Drawing.Color.DimGray;
            this.labVersion.Location = new System.Drawing.Point(521, 488);
            this.labVersion.Name = "labVersion";
            this.labVersion.Size = new System.Drawing.Size(223, 19);
            this.labVersion.TabIndex = 94;
            this.labVersion.Text = "v7.5.556.569874";
            this.labVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BioPortalClient7UI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LimeGreen;
            this.BackgroundImage = global::BioPortalClient7UIAdapter.Properties.Resources.theme_ligthgreen;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(806, 516);
            this.Controls.Add(this.labVersion);
            this.Controls.Add(this.labF10);
            this.Controls.Add(this.labF9);
            this.Controls.Add(this.labtimeout);
            this.Controls.Add(this.labFeedbackBotton);
            this.Controls.Add(this.labF8);
            this.Controls.Add(this.labF7);
            this.Controls.Add(this.labF6);
            this.Controls.Add(this.labF5);
            this.Controls.Add(this.labF4);
            this.Controls.Add(this.picSample);
            this.Controls.Add(this.labDedoSeleccionado);
            this.Controls.Add(this.labF3);
            this.Controls.Add(this.labF2);
            this.Controls.Add(this.labF1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.picSalir);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picDelete);
            this.Controls.Add(this.labMsg);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(300, 200);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BioPortalClient7UI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Huellero";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.LimeGreen;
            this.Load += new System.EventHandler(this.BioPortalClient7UI_Load);
            this.VisibleChanged += new System.EventHandler(this.Control_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSalir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picDelete;
        private System.Windows.Forms.Label labFeedbackBotton;
        private System.Windows.Forms.Label labtimeout;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label labMsg;
        private System.Windows.Forms.Label labDedoSeleccionado;
        private System.Windows.Forms.PictureBox picSample;
        private System.Windows.Forms.PictureBox picSalir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labF1;
        private System.Windows.Forms.Label labF2;
        private System.Windows.Forms.Label labF3;
        private System.Windows.Forms.Label labF4;
        private System.Windows.Forms.Label labF5;
        private System.Windows.Forms.Label labF6;
        private System.Windows.Forms.Label labF7;
        private System.Windows.Forms.Label labF8;
        private System.Windows.Forms.Label labF9;
        private System.Windows.Forms.Label labF10;
        private System.Windows.Forms.Label labVersion;
    }
}

