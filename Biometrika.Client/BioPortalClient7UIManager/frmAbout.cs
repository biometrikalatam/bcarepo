﻿using System;
using System.Reflection;
using System.Windows.Forms;

namespace BioPortalClient7UIAdapter
{
    public partial class frmAbout : Form
    {
        public frmAbout()
        {
            InitializeComponent();
        }

        private void frmAbout_Load(object sender, EventArgs e)
        {
            this.label1.Text = "v" + Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        private void frmAbout_MouseClick(object sender, MouseEventArgs e)
        {
            this.Close();
        }
    }
}