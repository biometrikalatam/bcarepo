﻿using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace Bio.Core.BCR
{
    public class BCR
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private const string BCRDATEFORMAT = "yyMMdd";

        private SerialPort SP;

        private Boolean pdf417;
        private Byte[] temp, serialTemp;
        private DateTime? expirationDate, birthDate;
        private Int32 length, serialIndex, fingerId;
        private String rut, documentNumber, lastName, country;
        public String DOB { get; set; }
        public String DOE { get; set; }
        private String fullPdf417;

        public BCR()
        {
        }

        public BCR(String PortName)
        {
            try
            {
                SP = new SerialPort(PortName, 9600, Parity.None, 8, StopBits.One);
                SP.Handshake = Handshake.XOnXOff;

                pdf417 = true;
                rut = "";
                documentNumber = "";
                birthDate = null;
                expirationDate = DateTime.Today;
                fingerId = 0;
                lastName = "";
                country = "";
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error al instanciar el lector de código de barras");

            }
        }
        public Boolean Open()
        {
            try
            {
                SP.Open();
                SP.DataReceived += new SerialDataReceivedEventHandler(this.Capture);

                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error al instanciar el lector de código de barras");
                return false;
            }
        }


        public Boolean Close()
        {
            try
            {
                SP.DataReceived -= new SerialDataReceivedEventHandler(this.Capture);
                SP.Close();
                SP.Dispose();

                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error al cerrar el lector de código de barras");
                return false;
            }
        }
        public Boolean OpenTest()
        {
            try
            {
                SP.Open();
                SP.DataReceived += new SerialDataReceivedEventHandler(this.CaptureTest);

                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error al instanciar el lector de código de barras");
                return false;
            }
        }


        public Boolean CloseTest()
        {
            try
            {
                SP.DataReceived -= new SerialDataReceivedEventHandler(this.CaptureTest);
                SP.Close();

                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error al cerrar el lector de código de barras");
                return false;
            }
        }

        private void Capture(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                SerialPort _sp = (SerialPort)sender;
                Int32 _bytes = _sp.BytesToRead;
                Byte[] _serialBuffer = new Byte[_bytes];
                Int32 _output = _sp.Read(_serialBuffer, 0, _bytes);

                Parse(_serialBuffer);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error al capturar los datos con el lector de código de barras");
            }
        }

        private void CaptureTest(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                SerialPort _sp = (SerialPort)sender;
                Int32 _bytes = _sp.BytesToRead;
                Byte[] _serialBuffer = new Byte[_bytes];
                Int32 _output = _sp.Read(_serialBuffer, 0, _bytes);

                LecturaCompleta(_serialBuffer);

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error al capturar los datos con el lector de código de barras");
            }

        }

        private void LecturaCompleta(Byte[] _binaryData)
        {

            string aux = Encoding.UTF7.GetString(_binaryData);
            String check = aux.Substring(0, 1);
            if (check == "1" || check == "2" || check == "3" || check == "4" || check == "5"
                    || check == "6" || check == "7" || check == "8" || check == "9")
            {
                pdf417 = true;
                this.fullPdf417 = Encoding.UTF8.GetString(_binaryData);
                DataCaptured(this, null);
            }
            else if (check == "h")
            {
                pdf417 = false;
                this.fullPdf417 = Encoding.UTF7.GetString(_binaryData);
                DataCaptured(this, null);
            }

        }

        private void Parse(Byte[] _binaryData)
        {
            try
            {

                String rawData = Encoding.UTF7.GetString(_binaryData);

               

                String check = rawData.Substring(0, 1);

                if (check == "1" || check == "2" || check == "3" || check == "4" || check == "5"
                    || check == "6" || check == "7" || check == "8" || check == "9")
                {
                    pdf417 = true;
                    rut = rawData.Substring(0, 9).Replace('\0', ' ').TrimEnd();
                    lastName = rawData.Substring(19, 30).Replace('\0', ' ').TrimEnd();
                    country = rawData.Substring(49, 3).Replace('\0', ' ').TrimEnd();
                    documentNumber = rawData.Substring(58, 10).Replace('\0', ' ').TrimEnd();
                    fingerId = (rawData[73] << 24) + (rawData[72] << 16) + (rawData[71] << 8) + (rawData[70]);

                    //Ésto es una posibilidad, no estoy seguro
                    fullPdf417 = Convert.ToBase64String(_binaryData);

                    String _year = rawData.Substring(52, 2);
                    String _month = rawData.Substring(54, 2);
                    String _day = rawData.Substring(56, 2);

                    expirationDate = new DateTime(Convert.ToInt32(_year) + 2000, Convert.ToInt32(_month), Convert.ToInt32(_day));

                    length = (_binaryData[77] << 24) + (_binaryData[76] << 16) + (_binaryData[75] << 8) + (_binaryData[74]);
                    temp = new Byte[400];
                    Buffer.BlockCopy(_binaryData, 78, temp, 0, length);
                    
                    DataCaptured(this, null);
                }
                else if (check == "h")
                {
                    pdf417 = false;
                    rut = rawData.Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[0] + rawData.Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[1];

                    Int32 _pos = rawData.LastIndexOf('=');
                    String _mrz = rawData.Substring(_pos + 1, 24);

                    documentNumber = _mrz.Substring(0, 9);
                    DOB = _mrz.Substring(10, 6);
                    DOE = _mrz.Substring(17, 6);
                    birthDate = DateTime.ParseExact(_mrz.Substring(10, 6), BCRDATEFORMAT, CultureInfo.InvariantCulture);
                    expirationDate = DateTime.ParseExact(_mrz.Substring(17, 6), BCRDATEFORMAT, CultureInfo.InvariantCulture);

                    string _exdate = _mrz.Substring(17, 6);
                    string _year = _exdate.Substring(0, 2);
                    string _month = _exdate.Substring(2, 2);
                    string _day = _exdate.Substring(4, 2);

                    expirationDate = new DateTime(Convert.ToInt32(_year) + 2000, Convert.ToInt32(_month), Convert.ToInt32(_day));

                    DataCaptured(this, null);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error al parsear los datos leídos por serial");
                throw;
            }
        }

        public Byte[] NEC
        {
            get
            {
                try
                {
                    Byte[] _necTemplate = new byte[length];
                    Byte[] _aux = (Byte[])temp;

                    for (Int32 i = 0; i < length; i++)
                    {
                        _necTemplate[i] = _aux[i];
                    }

                    return _necTemplate;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error en la creación del template NEC");
                    throw;
                }
            }
        }

        public BCR ParseString(string ocrRead)
        {
            BCR bcr = new BCR();
            String check = ocrRead.Substring(0, 1);

            if (check == "1" || check == "2" || check == "3" || check == "4" || check == "5"
                || check == "6" || check == "7" || check == "8" || check == "9")
            {
                pdf417 = true;
                rut = ocrRead.Substring(0, 9).Replace('\0', ' ').TrimEnd();
                lastName = ocrRead.Substring(19, 30).Replace('\0', ' ').TrimEnd();
                country = ocrRead.Substring(49, 3).Replace('\0', ' ').TrimEnd();
                documentNumber = ocrRead.Substring(58, 10).Replace('\0', ' ').TrimEnd();
                fingerId = (ocrRead[73] << 24) + (ocrRead[72] << 16) + (ocrRead[71] << 8) + (ocrRead[70]);

                String _year = ocrRead.Substring(52, 2);
                String _month = ocrRead.Substring(54, 2);
                String _day = ocrRead.Substring(56, 2);

                expirationDate = new DateTime(Convert.ToInt32(_year) + 2000, Convert.ToInt32(_month), Convert.ToInt32(_day));
            }
            else if (check == "h")
            {
                pdf417 = false;
                rut = ocrRead.Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[0] + ocrRead.Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[1];

                Int32 _pos = ocrRead.LastIndexOf('=');
                String _mrz = ocrRead.Substring(_pos + 1, 24);

                documentNumber = _mrz.Substring(0, 9);
                DOB = _mrz.Substring(10, 6);
                DOE = _mrz.Substring(17, 6);
                birthDate = DateTime.ParseExact(_mrz.Substring(10, 6), BCRDATEFORMAT, CultureInfo.InvariantCulture);
                expirationDate = DateTime.ParseExact(_mrz.Substring(17, 6), BCRDATEFORMAT, CultureInfo.InvariantCulture);

                string _exdate = _mrz.Substring(17, 6);
                string _year = _exdate.Substring(0, 2);
                string _month = _exdate.Substring(2, 2);
                string _day = _exdate.Substring(4, 2);

                expirationDate = new DateTime(Convert.ToInt32(_year) + 2000, Convert.ToInt32(_month), Convert.ToInt32(_day));
            }

            return bcr;
        }

        public event EventHandler DataCaptured;

        // Getters
        public Boolean Pdf417
        {
            get
            {
                try
                {
                    return pdf417;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error retornando en el PDF417");

                    throw;
                }
            }
        }
        public String Rut
        {
            get
            {
                try
                {
                    return this.rut;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error retornando el RUT");
                    throw;
                }
            }
        }

        public String RutFormateado
        {
            get
            {
                try
                {
                    return this.rut.Substring(0, rut.Length - 1) + "-" + this.rut.Substring(rut.Length - 1);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error retornando el RUT");
                    throw;
                }
            }
        }

        public String DocumentNumber
        {
            get
            {
                try
                {
                    return this.documentNumber;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error retornando el Número de Documento");
                    throw;
                }
            }
        }

        public DateTime? BirthDate
        {
            get
            {
                try
                {
                    return this.birthDate;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error retornando la fecha de nacimiento");
                    throw;
                }
            }
        }

        public DateTime? ExpirationDate
        {
            get
            {
                try
                {
                    return this.expirationDate;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error retornando la fecha de expiración");

                    throw;
                }
            }
        }

        public Int32 FingerId
        {
            get
            {
                try
                {
                    return this.fingerId;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error retornando finger id");
                    throw;
                }
            }
        }

        public String LastName
        {
            get
            {
                try
                {
                    return this.lastName;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error retornando el apellido");
                    throw;
                }
            }
        }

        public String Country
        {
            get
            {
                try
                {
                    return this.country;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error retornando el país");
                    throw;
                }
            }
        }

        public String FullPdf417
        {
            get
            {
                try
                {
                    return this.fullPdf417;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error retornando el país");
                    throw;
                }
            }
        }
    }
}
