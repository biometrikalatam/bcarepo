﻿using Biometrika.BVI.Desktop.Src.Models;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Biometrika.BVI.Desktop.Src.Helpers
{
    public class ServiceHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ServiceHelper));

        #region Service NV Manager

        public static int Login(string user, string password, out Models.LUser outUser, out string msg)
        {
            int ret = -2;
            msg = null;
            outUser = null;
            try
            {
                //if (!string.IsNullOrEmpty(user) && user.Trim().Equals("Admin") &&
                //    !string.IsNullOrEmpty(password) && password.Trim().Equals("Admin"))
                //{

                //var client = new RestClient("http://qamanager.enotario.cl/Account/LogInBVI");
                //client.Timeout = 30000;
                //var request = new RestRequest(Method.POST);
                //request.AddHeader("Content-Type", "application/json");
                //request.AddParameter("application/json", "{\r\n    \"username\": \"gsuhit@biometrika\",\r\n    \"password\": \"B1ometrika\"\r\n}", ParameterType.RequestBody);
                //IRestResponse response = client.Execute(request);

                LOG.Debug("ServiceHelper.Login - LLama a " + Program._BVI_CONFIG.URLServiceNV + "Account/LogInBVI...");
                var client = new RestClient(Program._BVI_CONFIG.URLServiceNV + "Account/LogInBVI");
                client.Timeout = Program._BVI_CONFIG.ServiceNVTimeout;
                var request = new RestRequest(Method.POST);
                //client.UserAgent = "biometrika";
                //request.AddHeader("Authorization", "Basic " +
                //    Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Properties.Settings.Default.ServiceAccessName + ":" +
                //                                                        Properties.Settings.Default.ServiceSecretKey)));
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                string jsonParams = "{\"username\": \"" + user.Trim() + "\",\"password\": \"" + password.Trim() + "\"}";
                request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                LOG.Debug("ServiceHelper.Login - call execute...");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    LOG.Debug("ServiceHelper.Login - Response OK");
                    BVILoginResponse oBVILoginResponse = JsonConvert.DeserializeObject<BVILoginResponse>(response.Content);
                    if (oBVILoginResponse != null)
                    {
                        ret = oBVILoginResponse.loginSuccess? 0 : -2;
                        if (ret == 0)
                        {
                            LOG.Debug("ServiceHelper.Login - " + user.Trim() + " Login = " + ret + " - Lleno User...");
                            outUser = new Models.LUser(user, 
                                                (oBVILoginResponse.user!=null ? oBVILoginResponse.user.Surname : "Desconocido"),
                                                (oBVILoginResponse.user != null ? oBVILoginResponse.user.Email : "Desconocido"),
                                                (oBVILoginResponse.user != null && oBVILoginResponse.user.Company != null  ? 
                                                                                        oBVILoginResponse.user.Company.Id : 0),
                                                (oBVILoginResponse.user != null && oBVILoginResponse.user.Company != null ?
                                                                                        oBVILoginResponse.user.Company.Name : "Desconocida"));
                            LOG.Debug("ServiceHelper.Login - " + user.Trim() + " Login = " + ret + " => Company = " + outUser.CompanyName);
                        } else
                        {
                            outUser = null;
                            msg = oBVILoginResponse.Message;
                            LOG.Debug("ServiceHelper.Login - " + user.Trim() + " Login = " + ret + " => MsgService = " + msg);
                        }
                    }
                    else
                    {
                        LOG.Debug("ServiceHelper.Login - Error parseando retorno...BVILoginResponse==null");
                        ret = -8;
                        msg = "Error deserializando respuesta del servicio para comprobar login. Consulte con soporte...";
                    }
                }
                else
                {
                    LOG.Debug("ServiceHelper.Login - Response Error");
                    //ErrorModel err = JsonConvert.DeserializeObject<ErrorModel>(response.Content);
                    LOG.Debug("ServiceHelper.Login - response.StatusCode => " + response.StatusCode.ToString());
                    msg = "Error del servicio: " + response.StatusCode.ToString();
                    ret = -9;
                }

                //} else
                //{
                //    ret = -3;
                //    msg = "Usuario/Clave no coinciden. Reintente...";
                //}
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ServiceHelper.Login - Exc Error: " + ex.Message);
            }
            return ret;
        }

        #endregion Service NV Manager


        #region BVI7 Client

        internal static int Certify(int clienttype, string username, string rut, out BVIClientResponse lastResult, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            lastResult = null;
            try
            {
                LOG.Debug("ServiceHelper.Certify - IN => Rut = " + (string.IsNullOrEmpty(rut)?"Null":rut));
                var persona = new Persona()
                {
                    Rut = rut
                };
                int ct = Program._BVI_CONFIG.ClientType;
                if (clienttype > 0)
                {
                    ct = clienttype;
                }
                var configuracion = new Configuracion()
                {
                    clientType = ct,
                    //extraerFoto = true,
                    ignoreStepSelfieInWizard = false,
                    //extraerFirma = true,
                    bloqueoMenoresDeEdad = false,
                    bloquearDocumentoVencido = false,
                    bloqueoCedulaInSCReI = false,
                    returnSampleImages = false,
                    returnSampleMinutiae = false,
                    userName = username
                };
                var solicitud = new Solicitud()
                {
                    Persona = persona,
                    Configuracion = configuracion
                };
                //string hashGuid = Guid.NewGuid().ToString("N");
                //Siempre debe ir acompañado del atributo SRC.
                string queryString = "?src=BVI7";
                byte[] arr;
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                string jsonSolicitud = jsonSerializer.Serialize(solicitud);
                LOG.Debug("ServiceHelper.Certify - Solicitud = " + jsonSolicitud);
                string absoluteURL = "";
                string json = "";
                try
                {
                    //Aca debe ir agregando los distintos parámetros.
                    queryString = queryString + "&VALUEID=" + rut;
                    queryString = queryString + "&SOLICITUD=" + jsonSolicitud;


                    absoluteURL = Program._BVI_CONFIG.URLServiceBVIClient; //urlTxt.Text;  //ConfigurationManager.AppSettings["AbsoluteURL"];
                    if (!absoluteURL.EndsWith("/"))
                        absoluteURL = absoluteURL + "/";

                    absoluteURL = absoluteURL + queryString;
                    LOG.Debug("ServiceHelper.Certify - Llamando a URL = " + absoluteURL);
                    WebRequest request = WebRequest.Create(absoluteURL);
                    request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
                    request.Timeout = 1300000000;
                    using (WebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            json = reader.ReadToEnd();
                            LOG.Debug("ServiceHelper.Certify - json Rpta = " + (string.IsNullOrEmpty(json)?"Null":json.Length.ToString()));
                            //rtbResult.Text = json;
                            LOG.Debug("ServiceHelper.Certify - Deserializa json Rpta...");
                            BVIRes<string> res = JsonConvert.DeserializeObject<BVIRes<string>>(json);
                            LOG.Debug("ServiceHelper.Certify - Deserializado Status = " + (res!=null?"OK":"NOOK"));

                            if (res != null && res.BVIResponse != null)
                            {
                                LOG.Debug("ServiceHelper.Certify - Deserializa Last Result...");
                                lastResult = JsonConvert.DeserializeObject<BVIClientResponse>(res.BVIResponse); 
                                LOG.Debug("ServiceHelper.Certify - Deserializado Last Result Status = " + (lastResult != null ? "OK" : "NOOK"));
                                //if (string.IsNullOrEmpty(lastResult.TrackIdCI))
                                //{
                                //    lastResult = null;
                                //}
                            } else
                            {
                                LOG.Debug("ServiceHelper.Certify - res.BVIResponse = null => Restono cancelando...");
                            }

                            if (lastResult == null)
                            {
                                ret = -4;
                                msgerr = "Error deserializando respuesta";
                            }
                        }
                    }
                }
                catch (Exception exe)
                {
                    ret = -5;
                    msgerr = "Error en consulta al servicio BVI Client [" + exe.Message + "]";
                    LOG.Error("ServiceHelper.Certify - Exp Err:" + msgerr);
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Error desconocido en consulta al servicio BVI Client [" + ex.Message + "]";
                LOG.Error("ServiceHelper.Certify - Error: " + msgerr);
            }
            return ret;
        }

        #endregion BVI7 Client
    }
}
