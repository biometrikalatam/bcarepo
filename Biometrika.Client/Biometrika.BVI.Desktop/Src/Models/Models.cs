﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BVI.Desktop.Src.Models
{
    public class LUser
    {
        public LUser() { }
        public LUser(string username) {
            UserName = username;        
        }

        public LUser(string username, string name, string mail, int companyid, string companyname)
        {
            UserName = username;
            Name = name;
            Mail = mail;
            CompanyId = companyid;
            CompanyName = companyname;
        }

        public string UserName { get; set; }
        public string Name { get; set; }
        public string Mail { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
    }


    public class BVIRes<T>
    {
        public T BVIResponse { get; set; }
    }

    public class BVIClientResponse
    {
        public BVIClientResponse() {}

        public string TrackIdCI { get; set; }
        public Persona Persona { get; set; }
    }


    public enum TipoIdentificacion
    {
        Indeterminado = 0,
        CedulaChilenaAntigua = 1,
        CedulaChilenaNueva = 2
    }

    public enum TipoBloqueo
    {
        Advertencia = 0,
        Bloqueo = 1,
        Permitir = 2
    }

    public class Solicitud
    {
        public Solicitud() { }

        public Persona Persona { get; set; }
        public Configuracion Configuracion { get; set; }
    }

    public class Configuracion
    {
        public Configuracion() { }

        public int clientType { get; set; }
        //public bool extraerFoto { get; set; }
        public bool ignoreStepSelfieInWizard { get; set; }
        //public bool extraerFirma { get; set; }
        public bool bloqueoMenoresDeEdad { get; set; }
        public bool bloquearDocumentoVencido { get; set; }
        public bool bloqueoCedulaInSCReI { get; set; }
        public bool returnSampleImages { get; set; }
        public bool returnSampleMinutiae { get; set; }
        public string userName { get; set; }
        //public bool vigenciaCedula { get; set; }
        //public bool raw { get; set; }
        //public bool iso { get; set; }
        //public bool ansi { get; set; }
        //public bool wsq { get; set; }
        //public bool jpeg { get; set; }
        //[JsonConverter(typeof(StringEnumConverter))]
        //public TipoBloqueo bloqueoMenoresDeEdad { get; set; }
        //[JsonConverter(typeof(StringEnumConverter))]
        //public TipoBloqueo bloquearDocumentoVencido { get; set; }
    }

    public class Persona
    {
        public Persona() { }

        [JsonConverter(typeof(StringEnumConverter))]
        public TipoIdentificacion TipoIdentificacion { get; set; }
        public string FotoFrontCedula { get; set; }
        public string FotoBackCedula { get; set; }
        public string Foto { get; set; }
        public string FotoFirma { get; set; }
        public string Selfie { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Sexo { get; set; }
        public string Serie { get; set; }
        public string Nacionalidad { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public DateTime? FechaExpiracion { get; set; }
        public bool IsCedulaVigente { get; set; }
        public bool IsMenorDeEdad { get; set; }

        //Result
        public int clientType { get; set; } //1-Huella | 2-Facial
        public int verifyResult { get; set; } // 0-Aun no verifico | 1-Positivo | 2-Negativo
        public double score { get; set; }

        //Certify
        public string CertifyPdf { get; set; }

        //Samples
        public string Ansi { get; set; }
        public string Iso { get; set; }
        public string[] IsoCompact { get; set; }
        public string Raw { get; set; }
        public string Wsq { get; set; }
        public string JpegOriginal { get; set; }
        //public string ApellidoMaterno { get; set; }
        //public string ApellidoPaterno { get; set; }

        public string GetNameTipoIdentificacion()
        {
            if (TipoIdentificacion == TipoIdentificacion.CedulaChilenaAntigua)
                return "Cedula Chilena Antigua";
            if (TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
                return "Cedula Chilena Nueva";
            return "Indeterminado";

        }
    }

    #region Service

    public class Companies
    {
        public string Name { get; set; }
        public string Domain { get; set; }
        public string Rut { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string EndDate { get; set; }
        public string AdditionalData { get; set; }
        public string ContactName { get; set; }
        public bool Status { get; set; }
        public int Holding { get; set; }
        public string HQs { get; set; }
        public string Sectors { get; set; }
        public string Users { get; set; }
        public bool HaveSectors { get; set; }
        public bool HaveHQs { get; set; }
        public bool IsActive { get; set; }
        public bool IsDisable { get; set; }
        public string AccessName { get; set; }
        public string SecretKey { get; set; }
        public int Id { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }

    }
    public class Roles
    {
        public string Name { get; set; }
        public int Lvl { get; set; }
        public int Id { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }

    }
    public class Company
    {
        public string Name { get; set; }
        public string Domain { get; set; }
        public string Rut { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string EndDate { get; set; }
        public string AdditionalData { get; set; }
        public string ContactName { get; set; }
        public bool Status { get; set; }
        public int Holding { get; set; }
        public string HQs { get; set; }
        public string Sectors { get; set; }
        public string Users { get; set; }
        public bool HaveSectors { get; set; }
        public bool HaveHQs { get; set; }
        public bool IsActive { get; set; }
        public bool IsDisable { get; set; }
        public string AccessName { get; set; }
        public string SecretKey { get; set; }
        public int Id { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }

    }
    public class User
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }
        public string PasswordSalt { get; set; }
        public int PasswordFormat { get; set; }
        public string PasswordQuestion { get; set; }
        public string PasswordAnswer { get; set; }
        public int FailedPasswordAttemptCount { get; set; }
        public string FailedPasswordAttemptWindowStart { get; set; }
        public int FailedPasswordAnswerAttemptCount { get; set; }
        public string FailedPasswordAnswerAttemptWindowStart { get; set; }
        public string LastPasswordChangedDate { get; set; }
        public string LastActivityDate { get; set; }
        public string LastLockOutDate { get; set; }
        public string LastLoginDate { get; set; }
        public string Comments { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string PostalCode { get; set; }
        public IList<Companies> Companies { get; set; }
        public IList<Roles> Roles { get; set; }
        public string Employee { get; set; }
        public Company Company { get; set; }
        public int Id { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }

    }
    public class BVILoginResponse
    {
        public bool loginSuccess { get; set; }
        public User user { get; set; }
        public string Message { get; set; }

    }


    #endregion Service


    public class Models
    {
    }
}
