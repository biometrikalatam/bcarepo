﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BVI.Desktop.Src.Config
{
    public class BVIDesktopConfig
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BVIDesktopConfig));

        public BVIDesktopConfig() {
            ShowDetailResultOnReturn = true;
            URLServiceBVIClient = "http://localhost:9191/";
            URLServiceNV = "http://localhost:9999/"; // "http://qamanager.enotario.cl/";
            ServiceNVTimeout = 60000;
            NECScore = 750;
            ClientType = 0;
        }

        public bool ShowDetailResultOnReturn { set;  get; }
        public string URLServiceBVIClient { set; get; }
        public string URLServiceNV { set; get; }
        public int ServiceNVTimeout { set; get; }
        public double NECScore { set; get; }
        public int ClientType { set; get; } //0-Default como este configurado en BVI7 | 1-Huella | 2-Facial

        public void ToString()
        {
            LOG.Info("BVIDesktopConfig Init [");
            LOG.Info("      ShowDetailResultOnReturn = " + ShowDetailResultOnReturn.ToString());
            LOG.Info("      URLServiceBVIClient = " + URLServiceBVIClient);
            LOG.Info("      URLServiceNV = " + URLServiceNV);
            LOG.Info("      ServiceNVTimeout = " + ServiceNVTimeout.ToString());
            LOG.Info("      NECScore = " + NECScore.ToString());
            LOG.Info("      ClientType = " + ClientType.ToString());
            LOG.Info("] End");

        }
    }
}
