﻿namespace Biometrika.BVI.Desktop
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnCallBVIClient = new System.Windows.Forms.Button();
            this.txtRUT = new System.Windows.Forms.TextBox();
            this.rtbResult = new System.Windows.Forms.RichTextBox();
            this.labVerificacion = new System.Windows.Forms.Label();
            this.labSalirResumen = new System.Windows.Forms.Label();
            this.labShowLastVerify = new System.Windows.Forms.Label();
            this.picResumeDocFront = new System.Windows.Forms.PictureBox();
            this.picResumeDocBack = new System.Windows.Forms.PictureBox();
            this.picResumeSelfie = new System.Windows.Forms.PictureBox();
            this.picResumeFirma = new System.Windows.Forms.PictureBox();
            this.picResumeHuella = new System.Windows.Forms.PictureBox();
            this.labResumeExport = new System.Windows.Forms.Label();
            this.picResumeResultVerify = new System.Windows.Forms.PictureBox();
            this.labResumeScore = new System.Windows.Forms.Label();
            this.labResumenNacionality = new System.Windows.Forms.Label();
            this.labResumenSex = new System.Windows.Forms.Label();
            this.labResumenFV = new System.Windows.Forms.Label();
            this.labResumenSerial = new System.Windows.Forms.Label();
            this.labResumenFNac = new System.Windows.Forms.Label();
            this.labResumenName = new System.Windows.Forms.Label();
            this.picResumenIconoMenorDeEdad = new System.Windows.Forms.PictureBox();
            this.picResumenIconoCedVencida = new System.Windows.Forms.PictureBox();
            this.labResumeRUT = new System.Windows.Forms.Label();
            this.labResumeTrackId = new System.Windows.Forms.Label();
            this.labResumenRango1 = new System.Windows.Forms.Label();
            this.labResumenRango2 = new System.Windows.Forms.Label();
            this.labResumenRango3 = new System.Windows.Forms.Label();
            this.labResumenRango4 = new System.Windows.Forms.Label();
            this.labResumenRango5 = new System.Windows.Forms.Label();
            this.labSalir = new System.Windows.Forms.Label();
            this.labConfig = new System.Windows.Forms.Label();
            this.grpLogin = new System.Windows.Forms.GroupBox();
            this.labMsgProcess = new System.Windows.Forms.Label();
            this.picProcess = new System.Windows.Forms.PictureBox();
            this.labTapa = new System.Windows.Forms.Label();
            this.labLoginMsg = new System.Windows.Forms.Label();
            this.labLogin = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.labUserLogged = new System.Windows.Forms.Label();
            this.labLogout = new System.Windows.Forms.Label();
            this.labMsgOperacion = new System.Windows.Forms.Label();
            this.labVerificacionHuella = new System.Windows.Forms.Label();
            this.labVerificacionFacial = new System.Windows.Forms.Label();
            this.labLastVerify = new System.Windows.Forms.Label();
            this.labClientType = new System.Windows.Forms.Label();
            this.labVersion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picResumeDocFront)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumeDocBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumeSelfie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumeFirma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumeHuella)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumeResultVerify)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenIconoMenorDeEdad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenIconoCedVencida)).BeginInit();
            this.grpLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCallBVIClient
            // 
            this.btnCallBVIClient.Location = new System.Drawing.Point(445, 21);
            this.btnCallBVIClient.Name = "btnCallBVIClient";
            this.btnCallBVIClient.Size = new System.Drawing.Size(75, 23);
            this.btnCallBVIClient.TabIndex = 0;
            this.btnCallBVIClient.Text = "Validar";
            this.btnCallBVIClient.UseVisualStyleBackColor = true;
            this.btnCallBVIClient.Visible = false;
            this.btnCallBVIClient.Click += new System.EventHandler(this.btnCallBVIClient_Click);
            // 
            // txtRUT
            // 
            this.txtRUT.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRUT.ForeColor = System.Drawing.Color.DimGray;
            this.txtRUT.Location = new System.Drawing.Point(52, 166);
            this.txtRUT.Name = "txtRUT";
            this.txtRUT.Size = new System.Drawing.Size(123, 29);
            this.txtRUT.TabIndex = 1;
            this.txtRUT.Text = "21284415-2";
            // 
            // rtbResult
            // 
            this.rtbResult.Location = new System.Drawing.Point(1063, 12);
            this.rtbResult.Name = "rtbResult";
            this.rtbResult.Size = new System.Drawing.Size(58, 41);
            this.rtbResult.TabIndex = 2;
            this.rtbResult.Text = "";
            this.rtbResult.Visible = false;
            // 
            // labVerificacion
            // 
            this.labVerificacion.BackColor = System.Drawing.Color.Transparent;
            this.labVerificacion.Location = new System.Drawing.Point(180, 154);
            this.labVerificacion.Name = "labVerificacion";
            this.labVerificacion.Size = new System.Drawing.Size(134, 53);
            this.labVerificacion.TabIndex = 3;
            this.labVerificacion.Click += new System.EventHandler(this.labVerificacion_Click);
            // 
            // labSalirResumen
            // 
            this.labSalirResumen.BackColor = System.Drawing.Color.Transparent;
            this.labSalirResumen.Location = new System.Drawing.Point(1114, 690);
            this.labSalirResumen.Name = "labSalirResumen";
            this.labSalirResumen.Size = new System.Drawing.Size(106, 32);
            this.labSalirResumen.TabIndex = 4;
            this.labSalirResumen.Click += new System.EventHandler(this.labSalirResumen_Click);
            // 
            // labShowLastVerify
            // 
            this.labShowLastVerify.BackColor = System.Drawing.Color.Transparent;
            this.labShowLastVerify.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labShowLastVerify.ForeColor = System.Drawing.Color.White;
            this.labShowLastVerify.Location = new System.Drawing.Point(44, 525);
            this.labShowLastVerify.Name = "labShowLastVerify";
            this.labShowLastVerify.Size = new System.Drawing.Size(271, 56);
            this.labShowLastVerify.TabIndex = 5;
            this.labShowLastVerify.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labShowLastVerify.Click += new System.EventHandler(this.labShowLastVerify_Click);
            // 
            // picResumeDocFront
            // 
            this.picResumeDocFront.Image = global::Biometrika.BVI.Desktop.Properties.Resources.NoImage_DocFront;
            this.picResumeDocFront.Location = new System.Drawing.Point(416, 232);
            this.picResumeDocFront.Name = "picResumeDocFront";
            this.picResumeDocFront.Size = new System.Drawing.Size(165, 105);
            this.picResumeDocFront.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumeDocFront.TabIndex = 6;
            this.picResumeDocFront.TabStop = false;
            // 
            // picResumeDocBack
            // 
            this.picResumeDocBack.Image = global::Biometrika.BVI.Desktop.Properties.Resources.NoImage_DocBack;
            this.picResumeDocBack.Location = new System.Drawing.Point(591, 232);
            this.picResumeDocBack.Name = "picResumeDocBack";
            this.picResumeDocBack.Size = new System.Drawing.Size(165, 105);
            this.picResumeDocBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumeDocBack.TabIndex = 7;
            this.picResumeDocBack.TabStop = false;
            // 
            // picResumeSelfie
            // 
            this.picResumeSelfie.Image = global::Biometrika.BVI.Desktop.Properties.Resources.NoImage_Selfie;
            this.picResumeSelfie.Location = new System.Drawing.Point(425, 404);
            this.picResumeSelfie.Name = "picResumeSelfie";
            this.picResumeSelfie.Size = new System.Drawing.Size(95, 100);
            this.picResumeSelfie.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumeSelfie.TabIndex = 8;
            this.picResumeSelfie.TabStop = false;
            // 
            // picResumeFirma
            // 
            this.picResumeFirma.Image = global::Biometrika.BVI.Desktop.Properties.Resources.NoImage_Firma;
            this.picResumeFirma.Location = new System.Drawing.Point(613, 404);
            this.picResumeFirma.Name = "picResumeFirma";
            this.picResumeFirma.Size = new System.Drawing.Size(114, 82);
            this.picResumeFirma.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumeFirma.TabIndex = 9;
            this.picResumeFirma.TabStop = false;
            // 
            // picResumeHuella
            // 
            this.picResumeHuella.Image = global::Biometrika.BVI.Desktop.Properties.Resources.NoImage_Finger;
            this.picResumeHuella.Location = new System.Drawing.Point(628, 550);
            this.picResumeHuella.Name = "picResumeHuella";
            this.picResumeHuella.Size = new System.Drawing.Size(99, 108);
            this.picResumeHuella.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumeHuella.TabIndex = 10;
            this.picResumeHuella.TabStop = false;
            // 
            // labResumeExport
            // 
            this.labResumeExport.BackColor = System.Drawing.Color.Transparent;
            this.labResumeExport.Location = new System.Drawing.Point(451, 556);
            this.labResumeExport.Name = "labResumeExport";
            this.labResumeExport.Size = new System.Drawing.Size(75, 89);
            this.labResumeExport.TabIndex = 11;
            this.labResumeExport.Click += new System.EventHandler(this.labResumeExport_Click);
            // 
            // picResumeResultVerify
            // 
            this.picResumeResultVerify.Image = global::Biometrika.BVI.Desktop.Properties.Resources.VerifyPending;
            this.picResumeResultVerify.Location = new System.Drawing.Point(844, 583);
            this.picResumeResultVerify.Name = "picResumeResultVerify";
            this.picResumeResultVerify.Size = new System.Drawing.Size(230, 36);
            this.picResumeResultVerify.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumeResultVerify.TabIndex = 12;
            this.picResumeResultVerify.TabStop = false;
            // 
            // labResumeScore
            // 
            this.labResumeScore.BackColor = System.Drawing.Color.Transparent;
            this.labResumeScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumeScore.Location = new System.Drawing.Point(1100, 563);
            this.labResumeScore.Name = "labResumeScore";
            this.labResumeScore.Size = new System.Drawing.Size(113, 41);
            this.labResumeScore.TabIndex = 13;
            this.labResumeScore.Text = "0%";
            this.labResumeScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labResumenNacionality
            // 
            this.labResumenNacionality.BackColor = System.Drawing.Color.Transparent;
            this.labResumenNacionality.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenNacionality.ForeColor = System.Drawing.Color.DimGray;
            this.labResumenNacionality.Location = new System.Drawing.Point(976, 340);
            this.labResumenNacionality.Name = "labResumenNacionality";
            this.labResumenNacionality.Size = new System.Drawing.Size(178, 19);
            this.labResumenNacionality.TabIndex = 74;
            this.labResumenNacionality.Text = "ARG";
            // 
            // labResumenSex
            // 
            this.labResumenSex.BackColor = System.Drawing.Color.Transparent;
            this.labResumenSex.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenSex.ForeColor = System.Drawing.Color.DimGray;
            this.labResumenSex.Location = new System.Drawing.Point(976, 323);
            this.labResumenSex.Name = "labResumenSex";
            this.labResumenSex.Size = new System.Drawing.Size(178, 19);
            this.labResumenSex.TabIndex = 73;
            this.labResumenSex.Text = "M";
            // 
            // labResumenFV
            // 
            this.labResumenFV.BackColor = System.Drawing.Color.Transparent;
            this.labResumenFV.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenFV.ForeColor = System.Drawing.Color.DimGray;
            this.labResumenFV.Location = new System.Drawing.Point(976, 288);
            this.labResumenFV.Name = "labResumenFV";
            this.labResumenFV.Size = new System.Drawing.Size(74, 19);
            this.labResumenFV.TabIndex = 72;
            this.labResumenFV.Text = "02/08/2023";
            // 
            // labResumenSerial
            // 
            this.labResumenSerial.BackColor = System.Drawing.Color.Transparent;
            this.labResumenSerial.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenSerial.ForeColor = System.Drawing.Color.DimGray;
            this.labResumenSerial.Location = new System.Drawing.Point(976, 306);
            this.labResumenSerial.Name = "labResumenSerial";
            this.labResumenSerial.Size = new System.Drawing.Size(178, 19);
            this.labResumenSerial.TabIndex = 71;
            this.labResumenSerial.Text = "601540625";
            // 
            // labResumenFNac
            // 
            this.labResumenFNac.BackColor = System.Drawing.Color.Transparent;
            this.labResumenFNac.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenFNac.ForeColor = System.Drawing.Color.DimGray;
            this.labResumenFNac.Location = new System.Drawing.Point(976, 272);
            this.labResumenFNac.Name = "labResumenFNac";
            this.labResumenFNac.Size = new System.Drawing.Size(74, 19);
            this.labResumenFNac.TabIndex = 70;
            this.labResumenFNac.Text = "28/09/1969";
            // 
            // labResumenName
            // 
            this.labResumenName.BackColor = System.Drawing.Color.Transparent;
            this.labResumenName.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold);
            this.labResumenName.ForeColor = System.Drawing.Color.DimGray;
            this.labResumenName.Location = new System.Drawing.Point(973, 232);
            this.labResumenName.Name = "labResumenName";
            this.labResumenName.Size = new System.Drawing.Size(239, 44);
            this.labResumenName.TabIndex = 69;
            this.labResumenName.Text = "Gustavo Gerardo Suhit Gallucci Cortadi Ferretti";
            // 
            // picResumenIconoMenorDeEdad
            // 
            this.picResumenIconoMenorDeEdad.BackColor = System.Drawing.Color.Transparent;
            this.picResumenIconoMenorDeEdad.Image = global::Biometrika.BVI.Desktop.Properties.Resources.ObsCedMenorDeEdad;
            this.picResumenIconoMenorDeEdad.Location = new System.Drawing.Point(844, 404);
            this.picResumenIconoMenorDeEdad.Name = "picResumenIconoMenorDeEdad";
            this.picResumenIconoMenorDeEdad.Size = new System.Drawing.Size(100, 80);
            this.picResumenIconoMenorDeEdad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumenIconoMenorDeEdad.TabIndex = 68;
            this.picResumenIconoMenorDeEdad.TabStop = false;
            this.picResumenIconoMenorDeEdad.Visible = false;
            // 
            // picResumenIconoCedVencida
            // 
            this.picResumenIconoCedVencida.BackColor = System.Drawing.Color.Transparent;
            this.picResumenIconoCedVencida.Image = global::Biometrika.BVI.Desktop.Properties.Resources.ObsCedVencida;
            this.picResumenIconoCedVencida.Location = new System.Drawing.Point(956, 404);
            this.picResumenIconoCedVencida.Name = "picResumenIconoCedVencida";
            this.picResumenIconoCedVencida.Size = new System.Drawing.Size(100, 80);
            this.picResumenIconoCedVencida.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResumenIconoCedVencida.TabIndex = 67;
            this.picResumenIconoCedVencida.TabStop = false;
            this.picResumenIconoCedVencida.Visible = false;
            // 
            // labResumeRUT
            // 
            this.labResumeRUT.BackColor = System.Drawing.Color.Transparent;
            this.labResumeRUT.Font = new System.Drawing.Font("Arial Black", 20F, System.Drawing.FontStyle.Bold);
            this.labResumeRUT.ForeColor = System.Drawing.Color.DimGray;
            this.labResumeRUT.Location = new System.Drawing.Point(388, 83);
            this.labResumeRUT.Name = "labResumeRUT";
            this.labResumeRUT.Size = new System.Drawing.Size(368, 44);
            this.labResumeRUT.TabIndex = 75;
            this.labResumeRUT.Text = "RUT: 21284415-2";
            // 
            // labResumeTrackId
            // 
            this.labResumeTrackId.BackColor = System.Drawing.Color.Transparent;
            this.labResumeTrackId.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumeTrackId.ForeColor = System.Drawing.Color.DimGray;
            this.labResumeTrackId.Location = new System.Drawing.Point(841, 90);
            this.labResumeTrackId.Name = "labResumeTrackId";
            this.labResumeTrackId.Size = new System.Drawing.Size(382, 19);
            this.labResumeTrackId.TabIndex = 76;
            this.labResumeTrackId.Text = "Track Id: 2h432hhjsjhdfgggksjg4jh2g34jg23jh4g234";
            this.labResumeTrackId.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labResumenRango1
            // 
            this.labResumenRango1.BackColor = System.Drawing.Color.White;
            this.labResumenRango1.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenRango1.ForeColor = System.Drawing.Color.Black;
            this.labResumenRango1.Location = new System.Drawing.Point(1114, 600);
            this.labResumenRango1.Name = "labResumenRango1";
            this.labResumenRango1.Size = new System.Drawing.Size(18, 19);
            this.labResumenRango1.TabIndex = 81;
            this.labResumenRango1.Text = "V";
            this.labResumenRango1.Visible = false;
            // 
            // labResumenRango2
            // 
            this.labResumenRango2.BackColor = System.Drawing.Color.White;
            this.labResumenRango2.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenRango2.ForeColor = System.Drawing.Color.Black;
            this.labResumenRango2.Location = new System.Drawing.Point(1132, 600);
            this.labResumenRango2.Name = "labResumenRango2";
            this.labResumenRango2.Size = new System.Drawing.Size(18, 19);
            this.labResumenRango2.TabIndex = 80;
            this.labResumenRango2.Text = "V";
            this.labResumenRango2.Visible = false;
            // 
            // labResumenRango3
            // 
            this.labResumenRango3.BackColor = System.Drawing.Color.White;
            this.labResumenRango3.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenRango3.ForeColor = System.Drawing.Color.Black;
            this.labResumenRango3.Location = new System.Drawing.Point(1151, 600);
            this.labResumenRango3.Name = "labResumenRango3";
            this.labResumenRango3.Size = new System.Drawing.Size(18, 19);
            this.labResumenRango3.TabIndex = 79;
            this.labResumenRango3.Text = "V";
            this.labResumenRango3.Visible = false;
            // 
            // labResumenRango4
            // 
            this.labResumenRango4.BackColor = System.Drawing.Color.White;
            this.labResumenRango4.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenRango4.ForeColor = System.Drawing.Color.Black;
            this.labResumenRango4.Location = new System.Drawing.Point(1170, 600);
            this.labResumenRango4.Name = "labResumenRango4";
            this.labResumenRango4.Size = new System.Drawing.Size(18, 19);
            this.labResumenRango4.TabIndex = 78;
            this.labResumenRango4.Text = "V";
            this.labResumenRango4.Visible = false;
            // 
            // labResumenRango5
            // 
            this.labResumenRango5.BackColor = System.Drawing.Color.White;
            this.labResumenRango5.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResumenRango5.ForeColor = System.Drawing.Color.Black;
            this.labResumenRango5.Location = new System.Drawing.Point(1188, 600);
            this.labResumenRango5.Name = "labResumenRango5";
            this.labResumenRango5.Size = new System.Drawing.Size(18, 19);
            this.labResumenRango5.TabIndex = 77;
            this.labResumenRango5.Text = "V";
            this.labResumenRango5.Visible = false;
            // 
            // labSalir
            // 
            this.labSalir.BackColor = System.Drawing.Color.Transparent;
            this.labSalir.Location = new System.Drawing.Point(267, 26);
            this.labSalir.Name = "labSalir";
            this.labSalir.Size = new System.Drawing.Size(27, 34);
            this.labSalir.TabIndex = 82;
            this.labSalir.Click += new System.EventHandler(this.labSalir_Click);
            // 
            // labConfig
            // 
            this.labConfig.BackColor = System.Drawing.Color.Transparent;
            this.labConfig.Location = new System.Drawing.Point(223, 26);
            this.labConfig.Name = "labConfig";
            this.labConfig.Size = new System.Drawing.Size(27, 34);
            this.labConfig.TabIndex = 83;
            this.labConfig.Click += new System.EventHandler(this.labConfig_Click);
            // 
            // grpLogin
            // 
            this.grpLogin.BackColor = System.Drawing.Color.White;
            this.grpLogin.BackgroundImage = global::Biometrika.BVI.Desktop.Properties.Resources.login;
            this.grpLogin.Controls.Add(this.labMsgProcess);
            this.grpLogin.Controls.Add(this.picProcess);
            this.grpLogin.Controls.Add(this.labTapa);
            this.grpLogin.Controls.Add(this.labLoginMsg);
            this.grpLogin.Controls.Add(this.labLogin);
            this.grpLogin.Controls.Add(this.txtPassword);
            this.grpLogin.Controls.Add(this.txtUser);
            this.grpLogin.Location = new System.Drawing.Point(1, 1);
            this.grpLogin.Name = "grpLogin";
            this.grpLogin.Size = new System.Drawing.Size(361, 759);
            this.grpLogin.TabIndex = 84;
            this.grpLogin.TabStop = false;
            // 
            // labMsgProcess
            // 
            this.labMsgProcess.Location = new System.Drawing.Point(139, 558);
            this.labMsgProcess.Name = "labMsgProcess";
            this.labMsgProcess.Size = new System.Drawing.Size(123, 35);
            this.labMsgProcess.TabIndex = 78;
            this.labMsgProcess.Text = "Verificando Acceso. Espere por favor...";
            this.labMsgProcess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labMsgProcess.Visible = false;
            // 
            // picProcess
            // 
            this.picProcess.BackColor = System.Drawing.Color.Transparent;
            this.picProcess.Image = global::Biometrika.BVI.Desktop.Properties.Resources.upload;
            this.picProcess.Location = new System.Drawing.Point(100, 558);
            this.picProcess.Name = "picProcess";
            this.picProcess.Size = new System.Drawing.Size(35, 35);
            this.picProcess.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picProcess.TabIndex = 77;
            this.picProcess.TabStop = false;
            this.picProcess.Visible = false;
            // 
            // labTapa
            // 
            this.labTapa.Location = new System.Drawing.Point(254, 11);
            this.labTapa.Name = "labTapa";
            this.labTapa.Size = new System.Drawing.Size(100, 47);
            this.labTapa.TabIndex = 76;
            // 
            // labLoginMsg
            // 
            this.labLoginMsg.BackColor = System.Drawing.Color.Transparent;
            this.labLoginMsg.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.labLoginMsg.ForeColor = System.Drawing.Color.DimGray;
            this.labLoginMsg.Image = global::Biometrika.BVI.Desktop.Properties.Resources.labErrorLogin;
            this.labLoginMsg.Location = new System.Drawing.Point(36, 490);
            this.labLoginMsg.Name = "labLoginMsg";
            this.labLoginMsg.Size = new System.Drawing.Size(284, 187);
            this.labLoginMsg.TabIndex = 75;
            this.labLoginMsg.Text = "{message}";
            this.labLoginMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labLogin
            // 
            this.labLogin.BackColor = System.Drawing.Color.Transparent;
            this.labLogin.Font = new System.Drawing.Font("Arial", 10F);
            this.labLogin.ForeColor = System.Drawing.Color.White;
            this.labLogin.Location = new System.Drawing.Point(43, 423);
            this.labLogin.Name = "labLogin";
            this.labLogin.Size = new System.Drawing.Size(271, 53);
            this.labLogin.TabIndex = 74;
            this.labLogin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labLogin.Click += new System.EventHandler(this.labLogin_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(97, 337);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(207, 26);
            this.txtPassword.TabIndex = 73;
            this.txtPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPassword_KeyPress);
            // 
            // txtUser
            // 
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.Location = new System.Drawing.Point(97, 236);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(207, 26);
            this.txtUser.TabIndex = 72;
            // 
            // labUserLogged
            // 
            this.labUserLogged.BackColor = System.Drawing.Color.Transparent;
            this.labUserLogged.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUserLogged.ForeColor = System.Drawing.Color.DimGray;
            this.labUserLogged.Location = new System.Drawing.Point(131, 104);
            this.labUserLogged.Name = "labUserLogged";
            this.labUserLogged.Size = new System.Drawing.Size(170, 19);
            this.labUserLogged.TabIndex = 85;
            this.labUserLogged.Text = "gsuhit@biometrika";
            this.labUserLogged.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labLogout
            // 
            this.labLogout.BackColor = System.Drawing.Color.Transparent;
            this.labLogout.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labLogout.ForeColor = System.Drawing.Color.DimGray;
            this.labLogout.Location = new System.Drawing.Point(306, 104);
            this.labLogout.Name = "labLogout";
            this.labLogout.Size = new System.Drawing.Size(27, 26);
            this.labLogout.TabIndex = 86;
            this.labLogout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labLogout.Click += new System.EventHandler(this.labLogout_Click);
            // 
            // labMsgOperacion
            // 
            this.labMsgOperacion.BackColor = System.Drawing.Color.Transparent;
            this.labMsgOperacion.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.labMsgOperacion.ForeColor = System.Drawing.Color.DimGray;
            this.labMsgOperacion.Location = new System.Drawing.Point(42, 594);
            this.labMsgOperacion.Name = "labMsgOperacion";
            this.labMsgOperacion.Size = new System.Drawing.Size(271, 81);
            this.labMsgOperacion.TabIndex = 87;
            this.labMsgOperacion.Text = "{message}";
            // 
            // labVerificacionHuella
            // 
            this.labVerificacionHuella.BackColor = System.Drawing.Color.Transparent;
            this.labVerificacionHuella.Location = new System.Drawing.Point(46, 325);
            this.labVerificacionHuella.Name = "labVerificacionHuella";
            this.labVerificacionHuella.Size = new System.Drawing.Size(268, 53);
            this.labVerificacionHuella.TabIndex = 88;
            this.labVerificacionHuella.Click += new System.EventHandler(this.labVerificacionHuella_Click);
            // 
            // labVerificacionFacial
            // 
            this.labVerificacionFacial.BackColor = System.Drawing.Color.Transparent;
            this.labVerificacionFacial.Location = new System.Drawing.Point(45, 425);
            this.labVerificacionFacial.Name = "labVerificacionFacial";
            this.labVerificacionFacial.Size = new System.Drawing.Size(268, 53);
            this.labVerificacionFacial.TabIndex = 89;
            this.labVerificacionFacial.Click += new System.EventHandler(this.labVerificacionFacial_Click);
            // 
            // labLastVerify
            // 
            this.labLastVerify.BackColor = System.Drawing.Color.Transparent;
            this.labLastVerify.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labLastVerify.ForeColor = System.Drawing.Color.DimGray;
            this.labLastVerify.Location = new System.Drawing.Point(49, 506);
            this.labLastVerify.Name = "labLastVerify";
            this.labLastVerify.Size = new System.Drawing.Size(264, 19);
            this.labLastVerify.TabIndex = 90;
            this.labLastVerify.Text = "RUT: 21284415-2 - GUSTAVO GERARDO SUHIT";
            this.labLastVerify.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labClientType
            // 
            this.labClientType.BackColor = System.Drawing.Color.Transparent;
            this.labClientType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labClientType.ForeColor = System.Drawing.Color.DimGray;
            this.labClientType.Location = new System.Drawing.Point(1100, 636);
            this.labClientType.Name = "labClientType";
            this.labClientType.Size = new System.Drawing.Size(113, 41);
            this.labClientType.TabIndex = 91;
            this.labClientType.Text = "Facial";
            this.labClientType.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labVersion
            // 
            this.labVersion.BackColor = System.Drawing.Color.Transparent;
            this.labVersion.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labVersion.ForeColor = System.Drawing.Color.DimGray;
            this.labVersion.Location = new System.Drawing.Point(91, 717);
            this.labVersion.Name = "labVersion";
            this.labVersion.Size = new System.Drawing.Size(223, 19);
            this.labVersion.TabIndex = 92;
            this.labVersion.Text = "v7.5";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::Biometrika.BVI.Desktop.Properties.Resources.MainBackground21;
            this.ClientSize = new System.Drawing.Size(1268, 762);
            this.Controls.Add(this.labVersion);
            this.Controls.Add(this.labClientType);
            this.Controls.Add(this.grpLogin);
            this.Controls.Add(this.labLastVerify);
            this.Controls.Add(this.labVerificacionFacial);
            this.Controls.Add(this.labVerificacionHuella);
            this.Controls.Add(this.labMsgOperacion);
            this.Controls.Add(this.labConfig);
            this.Controls.Add(this.labSalir);
            this.Controls.Add(this.labResumenRango1);
            this.Controls.Add(this.labResumenRango2);
            this.Controls.Add(this.labResumenRango3);
            this.Controls.Add(this.labResumenRango4);
            this.Controls.Add(this.labResumenRango5);
            this.Controls.Add(this.labResumeTrackId);
            this.Controls.Add(this.labResumeRUT);
            this.Controls.Add(this.labResumenNacionality);
            this.Controls.Add(this.labResumenSex);
            this.Controls.Add(this.labResumenFV);
            this.Controls.Add(this.labResumenSerial);
            this.Controls.Add(this.labResumenFNac);
            this.Controls.Add(this.labResumenName);
            this.Controls.Add(this.picResumenIconoMenorDeEdad);
            this.Controls.Add(this.picResumenIconoCedVencida);
            this.Controls.Add(this.labResumeScore);
            this.Controls.Add(this.picResumeResultVerify);
            this.Controls.Add(this.labResumeExport);
            this.Controls.Add(this.picResumeHuella);
            this.Controls.Add(this.picResumeFirma);
            this.Controls.Add(this.picResumeSelfie);
            this.Controls.Add(this.picResumeDocBack);
            this.Controls.Add(this.picResumeDocFront);
            this.Controls.Add(this.labShowLastVerify);
            this.Controls.Add(this.labSalirResumen);
            this.Controls.Add(this.labVerificacion);
            this.Controls.Add(this.rtbResult);
            this.Controls.Add(this.txtRUT);
            this.Controls.Add(this.btnCallBVIClient);
            this.Controls.Add(this.labLogout);
            this.Controls.Add(this.labUserLogged);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Biometrika Verification Identity Desktop - v7.1...";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Enter += new System.EventHandler(this.frmMain_Enter);
            ((System.ComponentModel.ISupportInitialize)(this.picResumeDocFront)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumeDocBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumeSelfie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumeFirma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumeHuella)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumeResultVerify)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenIconoMenorDeEdad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResumenIconoCedVencida)).EndInit();
            this.grpLogin.ResumeLayout(false);
            this.grpLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picProcess)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCallBVIClient;
        private System.Windows.Forms.TextBox txtRUT;
        private System.Windows.Forms.RichTextBox rtbResult;
        private System.Windows.Forms.Label labVerificacion;
        private System.Windows.Forms.Label labSalirResumen;
        private System.Windows.Forms.Label labShowLastVerify;
        private System.Windows.Forms.PictureBox picResumeDocFront;
        private System.Windows.Forms.PictureBox picResumeDocBack;
        private System.Windows.Forms.PictureBox picResumeSelfie;
        private System.Windows.Forms.PictureBox picResumeFirma;
        private System.Windows.Forms.PictureBox picResumeHuella;
        private System.Windows.Forms.Label labResumeExport;
        private System.Windows.Forms.PictureBox picResumeResultVerify;
        private System.Windows.Forms.Label labResumeScore;
        private System.Windows.Forms.Label labResumenNacionality;
        private System.Windows.Forms.Label labResumenSex;
        private System.Windows.Forms.Label labResumenFV;
        private System.Windows.Forms.Label labResumenSerial;
        private System.Windows.Forms.Label labResumenFNac;
        private System.Windows.Forms.Label labResumenName;
        private System.Windows.Forms.PictureBox picResumenIconoMenorDeEdad;
        private System.Windows.Forms.PictureBox picResumenIconoCedVencida;
        private System.Windows.Forms.Label labResumeRUT;
        private System.Windows.Forms.Label labResumeTrackId;
        private System.Windows.Forms.Label labResumenRango1;
        private System.Windows.Forms.Label labResumenRango2;
        private System.Windows.Forms.Label labResumenRango3;
        private System.Windows.Forms.Label labResumenRango4;
        private System.Windows.Forms.Label labResumenRango5;
        private System.Windows.Forms.Label labSalir;
        private System.Windows.Forms.Label labConfig;
        private System.Windows.Forms.GroupBox grpLogin;
        private System.Windows.Forms.Label labLoginMsg;
        private System.Windows.Forms.Label labLogin;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label labUserLogged;
        private System.Windows.Forms.Label labLogout;
        private System.Windows.Forms.Label labMsgOperacion;
        private System.Windows.Forms.Label labVerificacionHuella;
        private System.Windows.Forms.Label labVerificacionFacial;
        private System.Windows.Forms.Label labLastVerify;
        private System.Windows.Forms.Label labClientType;
        private System.Windows.Forms.Label labVersion;
        private System.Windows.Forms.Label labTapa;
        private System.Windows.Forms.Label labMsgProcess;
        private System.Windows.Forms.PictureBox picProcess;
    }
}

