﻿using Biometrika.BVI.Desktop.Src.Config;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.BVI.Desktop
{
    static class Program
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Program));

        public static BVIDesktopConfig _BVI_CONFIG;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                string currentPath = "Logger.cfg";
                XmlConfigurator.Configure(new FileInfo(currentPath)); //new FileInfo(path + "\\Logger.cfg"));
                LOG.Info("Biometrika.BVI.Desktop.Program.Main - Iniciando...");

                try
                {
                    _BVI_CONFIG = Src.Utils.SerializeHelper.DeserializeFromFile<BVIDesktopConfig>("Biometrika.BVI.Desktop.Config.cfg");
                    if (_BVI_CONFIG == null) {
                        _BVI_CONFIG = new BVIDesktopConfig();
                        Src.Utils.SerializeHelper.SerializeToFile(_BVI_CONFIG, "Biometrika.BVI.Desktop.Config.cfg");
                    }
                }
                catch (Exception ex)
                {
                    _BVI_CONFIG = new BVIDesktopConfig();
                    Src.Utils.SerializeHelper.SerializeToFile(_BVI_CONFIG, "Biometrika.BVI.Desktop.Config.cfg");
                    LOG.Error(" Error: " + ex.Message);
                }
                _BVI_CONFIG.ToString();

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmMain());
            }
            catch (Exception ex)
            {
                LOG.Error("Program.Main - Ex Error: " + ex.Message);
            }
            
        }
    }
}
