﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Biometrika.BVI.Desktop.Src.Models;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Biometrika.BVI.Desktop.Src.Helpers;
using System.Reflection;

namespace Biometrika.BVI.Desktop
{
    public partial class frmMain : Form
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(frmMain));
        internal static BVIClientResponse _LAST_RESULT;
        internal static LUser _USER_LOGGED = null;  

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            try
            {
                Version version = Assembly.GetEntryAssembly().GetName().Version;
                labVersion.Text = "v" + version.ToString();
            }
            catch (Exception ex)
            {
                labVersion.Text = "v7.5";
                LOG.Error("frmMain.frmMain_Load - Ecp Error: " + ex.Message);
            }


            labLogout.Visible = false;
            labLogout.Refresh();
            labUserLogged.Text = "";
            labUserLogged.Refresh();
            grpLogin.Location = new Point(0, 0);
            grpLogin.Visible = true;
            grpLogin.Refresh();
            _USER_LOGGED = null;
            labLoginMsg.Text = "";
            labLoginMsg.Visible = false;
            labLoginMsg.Refresh();

            labMsgOperacion.Text = "";
            labMsgOperacion.Visible = false;
            labMsgOperacion.ForeColor = Color.DimGray;
            labMsgOperacion.Refresh();

            labLoginMsg.Text = "";
            labLoginMsg.Visible = false;
            labLoginMsg.Refresh();

            labLastVerify.Text = "";
            labLastVerify.Refresh();
            txtRUT.Text = null;
            txtRUT.Refresh();

            ClearResume();
            labSalirResumen_Click(null, null);
        }

        internal void ClearResume()
        {
            labResumeTrackId.Text = "Track Id: ?";
            labResumeRUT.Text = "RUT ?";

            labResumenName.Text = "";
            labResumenFNac.Text = "";
            labResumenFNac.ForeColor = Color.DarkGray;
            labResumenFV.Text = "";
            labResumenFV.ForeColor = Color.DarkGray;
            labResumenSerial.Text = "";
            labResumenSex.Text = "";
            labResumenNacionality.Text = "";

            //Imagenes
            picResumeDocFront.Image = Properties.Resources.NoImage_DocFront;
            picResumeDocBack.Image = Properties.Resources.NoImage_DocBack;
            picResumeFirma.Image = Properties.Resources.NoImage_Firma;
            picResumeHuella.Image = Properties.Resources.NoImage_Finger;
            picResumeSelfie.Image = Properties.Resources.NoImage_Selfie;
            labResumeExport.Enabled = false;
            picResumenIconoCedVencida.Visible = false;
            picResumenIconoMenorDeEdad.Visible = false;

            //Result
            picResumeResultVerify.Image = Properties.Resources.VerifyPending;
            labResumeScore.Text = "0%";

            labResumenRango1.Visible = false;
            labResumenRango2.Visible = false;
            labResumenRango3.Visible = false;
            labResumenRango4.Visible = false;
            labResumenRango5.Visible = false;

        }

        #region Operacion

        private void btnCallBVIClient_Click(object sender, EventArgs e)
        {
            ShowResult(null);
        }

        //245,70
        private void labVerificacion_Click(object sender, EventArgs e)
        {
            string msgerr;
            try
            {
                //Si no hay logueado => No permito
                if (_USER_LOGGED == null)
                    return;

                labMsgOperacion.Text = "";
                labMsgOperacion.Refresh();

                int ret = ServiceHelper.Certify(0, _USER_LOGGED.UserName, txtRUT.Text, out _LAST_RESULT, out msgerr);
                if (ret == 0 && _LAST_RESULT != null)
                {
                    labLastVerify.Text = "RUT: " + _LAST_RESULT.Persona.Rut + " - " + _LAST_RESULT.Persona.Nombre;
                    if (Program._BVI_CONFIG.ShowDetailResultOnReturn)
                    {
                        ShowResult(_LAST_RESULT);
                    }
                } else
                {
                    labLastVerify.Text = "";
                    labLastVerify.Refresh();
                    labMsgOperacion.Text = "Error en proceso de certificación [" + ret + " - " + msgerr + "]. Reintente...";
                    labMsgOperacion.Visible = true;
                    labMsgOperacion.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {

                LOG.Error("frmMain.labVerificacion_Click - Error: " + ex.Message);
            }
        }

        private void labVerificacionHuella_Click(object sender, EventArgs e)
        {
            string msgerr;
            try
            {
                //Si no hay logueado => No permito
                if (_USER_LOGGED == null)
                    return;

                labMsgOperacion.Text = "";
                labMsgOperacion.Refresh();

                int ret = ServiceHelper.Certify(1, _USER_LOGGED.UserName, txtRUT.Text, out _LAST_RESULT, out msgerr);
                if (ret == 0 && _LAST_RESULT != null)
                {
                    labLastVerify.Text = "RUT: " + _LAST_RESULT.Persona.Rut + " - " + _LAST_RESULT.Persona.Nombre;
                    if (Program._BVI_CONFIG.ShowDetailResultOnReturn)
                    {
                        ShowResult(_LAST_RESULT);
                    }
                }
                else
                {
                    labLastVerify.Text = "";
                    labLastVerify.Refresh();
                    labMsgOperacion.Text = "Error en proceso de certificación [" + ret + " - " + msgerr + "]. Reintente...";
                    labMsgOperacion.Visible = true;
                    labMsgOperacion.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {

                LOG.Error("frmMain.labVerificacionHuella_Click - Error: " + ex.Message);
            }
        }

        private void labVerificacionFacial_Click(object sender, EventArgs e)
        {
            string msgerr;
            try
            {
                //Si no hay logueado => No permito
                if (_USER_LOGGED == null) 
                    return;

                labMsgOperacion.Text = "";
                labMsgOperacion.Refresh();

                int ret = ServiceHelper.Certify(2, _USER_LOGGED.UserName, txtRUT.Text, out _LAST_RESULT, out msgerr);
                if (ret == 0 && _LAST_RESULT != null)
                {
                    labLastVerify.Text = "RUT: " + _LAST_RESULT.Persona.Rut + " - " + _LAST_RESULT.Persona.Nombre;
                    if (Program._BVI_CONFIG.ShowDetailResultOnReturn)
                    {
                        ShowResult(_LAST_RESULT);
                    }
                }
                else
                {
                    labLastVerify.Text = "";
                    labLastVerify.Refresh();
                    labMsgOperacion.Text = "Error en proceso de certificación [" + ret + " - " + msgerr + "]. Reintente...";
                    labMsgOperacion.Visible = true;
                    labMsgOperacion.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {

                LOG.Error("frmMain.labVerificacionFacial_Click - Error: " + ex.Message);
            }
        }

        private void ShowResult(BVIClientResponse oResult)
        {
            try
            {
                if (oResult == null)
                {
                    MessageBox.Show(this, "No existen datos disponibles para mostrar. Realice una verificación primero...", "Atención...",
                                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
                } else if (string.IsNullOrEmpty(oResult.TrackIdCI))
                {
                     _LAST_RESULT = null;
                    labLastVerify.Text = "";
                    labLastVerify.Refresh();
                } else 
                {
                    //Datos
                    labResumeTrackId.Text = "Track Id: " + oResult.TrackIdCI;
                    labResumeTrackId.Refresh();
                    labResumeRUT.Text = "RUT: " + oResult.Persona.Rut;
                    labResumeRUT.Refresh();

                    labResumenName.Text = oResult.Persona.Nombre + " " + oResult.Persona.Apellido;
                    labResumenFNac.Text = oResult.Persona.FechaNacimiento.HasValue ? oResult.Persona.FechaNacimiento.Value.ToString("dd/MM/yyyy") : "";
                    if (oResult.Persona.IsMenorDeEdad)
                    {
                        labResumenFNac.ForeColor = Color.Red;
                        picResumenIconoMenorDeEdad.Visible = true;
                    }
                    labResumenFV.Text = oResult.Persona.FechaExpiracion.HasValue ? oResult.Persona.FechaExpiracion.Value.ToString("dd/MM/yyyy") : "";
                    if (!oResult.Persona.IsCedulaVigente)
                    {
                        labResumenFV.ForeColor = Color.Red;
                        picResumenIconoCedVencida.Visible = true;
                    }
                    labResumenSerial.Text = oResult.Persona.Serie;
                    labResumenSex.Text = oResult.Persona.Sexo;
                    labResumenNacionality.Text = oResult.Persona.Nacionalidad;

                    //Imagenes
                    byte[] byImage;
                    MemoryStream ms;
                    if (!string.IsNullOrEmpty(oResult.Persona.FotoFrontCedula))
                    {
                        byImage = Convert.FromBase64String(oResult.Persona.FotoFrontCedula);
                        ms = new MemoryStream(byImage);
                        picResumeDocFront.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumeDocFront.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(oResult.Persona.FotoBackCedula))
                    {
                        byImage = Convert.FromBase64String(oResult.Persona.FotoBackCedula);
                        ms = new MemoryStream(byImage);
                        picResumeDocBack.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumeDocBack.Visible = true;
                    }

                    if (!string.IsNullOrEmpty(oResult.Persona.FotoFirma))
                    {
                        byImage = Convert.FromBase64String(oResult.Persona.FotoFirma);
                        ms = new MemoryStream(byImage);
                        picResumeFirma.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumeFirma.Visible = true;
                    }

                    //Imagenes muestras
                    if (!string.IsNullOrEmpty(oResult.Persona.JpegOriginal))
                    {
                        byImage = Convert.FromBase64String(oResult.Persona.JpegOriginal);
                        ms = new MemoryStream(byImage);
                        picResumeHuella.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumeHuella.Visible = true;
                        labClientType.Text = "Huella";
                        labClientType.Refresh();
                    } else
                    {
                        labClientType.Text = "Facial";
                        labClientType.Refresh();
                    }
                    if (!string.IsNullOrEmpty(oResult.Persona.Selfie))
                    {
                        byImage = Convert.FromBase64String(oResult.Persona.Selfie);
                        ms = new MemoryStream(byImage);
                        picResumeSelfie.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumeSelfie.Visible = true;
                    }

                    //Certify
                    if (!string.IsNullOrEmpty(oResult.Persona.CertifyPdf))
                    {
                        labResumeExport.Enabled = true;
                        labResumeExport.Refresh();
                        //byImage = Convert.FromBase64String(oResult.Persona.CertifyPdf);
                        //ms = new MemoryStream(byImage);
                        //picResumeSelfie.Image = Image.FromStream(ms);
                        //ms.Close();
                        //picResumeSelfie.Visible = true;
                    }

                    //Result
                    if (oResult.Persona.verifyResult > 0)
                    {
                        if (oResult.Persona.verifyResult == 1)
                        {
                            picResumeResultVerify.Image = Properties.Resources.VerifyOK;
                            picResumeResultVerify.Visible = true;
                            MarkRangoResumenScore(true,  oResult.Persona.score);
                            labResumeScore.Text = GetScorePercent(oResult.Persona.clientType, oResult.Persona.TipoIdentificacion, oResult.Persona.score);
                            labResumeScore.Refresh();
                        }
                        else
                        {
                            picResumeResultVerify.Image = Properties.Resources.VerifyNOOK;
                            picResumeResultVerify.Visible = true;
                            MarkRangoResumenScore(false, 0);
                            labResumeScore.Text = "0%";
                            labResumeScore.Refresh();
                        }
                    }
                    else
                    {
                        picResumeResultVerify.Image = Properties.Resources.VerifyPending;
                        picResumeResultVerify.Visible = true;
                    }

                    //Agrando form y centralizo
                    this.Width = 1284;
                    this.CenterToScreen();
                    this.Refresh();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("frmMain.ShowResult - Error: " + ex.Message);
            }
        }

        private void labSalirResumen_Click(object sender, EventArgs e)
        {
            //Agrando form y centralizo
            this.Width = 377;
            this.CenterToScreen();
            this.Refresh();
        }

        private void labShowLastVerify_Click(object sender, EventArgs e)
        {
            //Si no hay logueado => No permito
            if (_USER_LOGGED == null)
                return;
            //if (_LAST_RESULT != null)
            //{
            ShowResult(_LAST_RESULT);
            //} else
            //{

            //}
        }

        private void labResumeExport_Click(object sender, EventArgs e)
        {
            try
            {
                if (_LAST_RESULT != null && _LAST_RESULT.Persona != null &&
                    !string.IsNullOrEmpty(_LAST_RESULT.Persona.CertifyPdf))
                {
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Filter = "Archivo PDF|*.pdf";
                    saveFileDialog1.Title = "Grabe Certificación de Identidad en...";
                    saveFileDialog1.ShowDialog();

                    // If the file name is not an empty string open it for saving.
                    if (saveFileDialog1.FileName != "")
                    {
                        System.IO.File.WriteAllBytes(saveFileDialog1.FileName, Convert.FromBase64String(_LAST_RESULT.Persona.CertifyPdf));
                    }
                } else
                {
                    MessageBox.Show(this, "No existe PDF disponible para grabar. Realice una verificación primero...", "Atención...",
                                  MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            catch (Exception ex)
            {

                LOG.Error("frmMain.labResumeExport_Click Error: " + ex.Message);
            }
        }

        private void labSalir_Click(object sender, EventArgs e)
        {
            _USER_LOGGED = null;
            this.Close();
        }

        private void labConfig_Click(object sender, EventArgs e)
        {
            //TODO Hacer config form
        }

        #endregion Operacion

        #region Utils

        private string GetScorePercent(int clienttype, TipoIdentificacion typoidentificacion, double score)
        {
            string sRet = "0%";
            try
            {
                double dScore = 0;
                if (clienttype == 1 && typoidentificacion == TipoIdentificacion.CedulaChilenaAntigua)
                {
                    dScore = GetPorcentajeNEC(score, Program._BVI_CONFIG.NECScore);
                }
                else
                {
                    dScore = score;
                }
                sRet = dScore.ToString("##.##") + "%";
            }
            catch (Exception ex)
            {
                sRet = "0%";
                LOG.Error("frmMain.GetScorePercent Error: " + ex.Message);
            }
            return sRet;
        }

        private void MarkRangoResumenScore(bool visible, double score)
        {
            try
            {
                if (!visible)
                {
                    labResumenRango1.Visible = false;
                    labResumenRango2.Visible = false;
                    labResumenRango3.Visible = false;
                    labResumenRango4.Visible = false;
                    labResumenRango5.Visible = false;
                }
                else
                {
                    if (score < 20)
                        labResumenRango1.Visible = true;
                    else if (score < 40)
                        labResumenRango2.Visible = true;
                    else if (score < 60)
                        labResumenRango3.Visible = true;
                    else if (score < 80)
                        labResumenRango4.Visible = true;
                    else
                        labResumenRango5.Visible = true;

                }
            }
            catch (Exception ex)
            {
                LOG.Error("frmMain.MarkRangoResumenScore - Error: " + ex.Message);
            }
        }

        internal static double GetPorcentajeNEC(double score, double threshold)
        {
            double ret = 100;
            try
            {
                LOG.Debug("frmMain.GetPorcentajeNEC - IN => score = " + score.ToString());
                if (score >= 15000 - threshold)
                {
                    ret = 100;
                }
                else
                {
                    double resto = 15000 - threshold;  //Resto de la diferencia entre 15000 y el umbral
                    double porcentajeResto = score * 100 / resto; //Calculo el porcentaje de lo que significa ese score para ese resto
                    double toSume = 20 * porcentajeResto / 100; //Saco el porcentaje sobre el 20% variable que uso
                    ret = 80 + toSume; //A un 80% fijo por ser positivo le sumo el variable obtenido
                    if (ret > 100) //Por si acaso se pasa (no deberia pero para estar seguro)
                        ret = 100;
                }

            }
            catch (Exception ex)
            {
                ret = 100;
                LOG.Error("frmMain.GetPorcentajeNEC - Error: " + ex.Message);
            }
            LOG.Debug("frmMain.GetPorcentajeNEC - OUT! - Porcentaje = " + ret.ToString());
            return ret;
        }



        #endregion Utils

        #region Login

        private void labLogin_Click(object sender, EventArgs e)
        {
            string msg;

            try
            {
                labLoginMsg.Visible = false;
                labLoginMsg.Refresh();

                picProcess.Visible = true;
                picProcess.Refresh();
                labMsgProcess.Visible = true;
                labMsgProcess.Refresh();

                int iret = ServiceHelper.Login(txtUser.Text, txtPassword.Text, out _USER_LOGGED, out msg);

                picProcess.Visible = false;
                picProcess.Refresh();
                labMsgProcess.Visible = false;
                labMsgProcess.Refresh();

                if (iret == 0)
                {
                    labTapa.Visible = false;
                    labTapa.Refresh();

                    labLogout.Visible = true;
                    labLogout.Refresh();
                    labUserLogged.Text = _USER_LOGGED.UserName;
                    labUserLogged.Refresh();
                    grpLogin.Visible = false;
                    grpLogin.Refresh();
                    //_USER_LOGGED = txtUser.Text;
                }
                else
                {
                    labTapa.Visible = true;
                    labTapa.Refresh();

                    labLogout.Visible = false;
                    labLogout.Refresh();
                    labUserLogged.Text = "";
                    labUserLogged.Refresh();
                    grpLogin.Visible = true;
                    grpLogin.Refresh();
                    _USER_LOGGED = null;
                    labLoginMsg.Text = msg + " [Codigo=" + iret + "]";
                    labLoginMsg.ForeColor = Color.Red;
                    labLoginMsg.Visible = true;
                    labLoginMsg.Refresh();
                }
                txtUser.Text = null;
                txtPassword.Text = null;
            }
            catch (Exception ex)
            {
                LOG.Error(" Error: " + ex.Message);
                labLoginMsg.Text = "Error procesando Login. Revise log...";
                labLoginMsg.Visible = true;
                labLoginMsg.Refresh();
            }

        }

        private void labLogout_Click(object sender, EventArgs e)
        {
            try
            {
                labTapa.Visible = true;
                labTapa.Refresh();

                labLogout.Visible = false;
                labLogout.Refresh();
                labUserLogged.Text = "";
                labUserLogged.Refresh();
                grpLogin.Visible = true;
                grpLogin.Refresh();
                _USER_LOGGED = null;
                labLoginMsg.Text = "";
                labLoginMsg.Visible = false;
                labLoginMsg.Refresh();
            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }
        }

        #endregion Login

        private void frmMain_Enter(object sender, EventArgs e)
        {
            //if (_USER_LOGGED == null)
            //{
            //    labLogin_Click(null, null);
            //}
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {

        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (_USER_LOGGED == null && !string.IsNullOrEmpty(txtUser.Text) && !string.IsNullOrEmpty(txtPassword.Text))
                {
                    labLogin_Click(null, null);
                }
            }
        }

        
    }
}
