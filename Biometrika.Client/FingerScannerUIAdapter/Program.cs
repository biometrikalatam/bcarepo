﻿using Domain;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FingerScannerUIAdapter
{
    public static class Program
    {
        public static bool HaveData
        {
            get
            {
                return fingerScanUI == null ? false : fingerScanUI.HaveData;
            }
        }
        public static BioPacket Packet { get; set; }

        private static FingerScanUI fingerScanUI;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
             Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            fingerScanUI = new FingerScanUI();
            int ret = fingerScanUI.Channel(Packet);  //chequea que tenga los parámetros correctos ingresados y los pasa a dictionary
            if (ret == 0)
            {
                Application.Run(fingerScanUI);
            } else
            {
                Packet.PacketParamOut.Add("Error",GetError(ret));
            }

            //fingerScanerUI = new FingerScannerUI();
            //fingerScanerUI.Channel(Packet);
            //Application.Run(fingerScanerUI);
        }

        private static string GetError(int ret)
        {
            string sret = null;
            switch (ret)
            {
                case -1:
                    sret = "-1|Error desconocido. Ver log";
                    break;
                case -2:
                    sret = "-2|Parametros nulos";
                    break;
                case -3:
                    sret = "-3|Parametros src/TypeId/ValueId denben existir y no pueden ser nulos";
                    break;
                default:
                    sret = "-1|Error desconocido. Ver log";
                    break;
            }
            return sret;
        }

        public static void SetPacket(NameValueCollection queryString)
        { 
            Packet = new BioPacket();
            Packet.PacketParamIn = queryString;
            Packet.PacketParamOut = new Dictionary<string, object>();
         }

        public static void Close()
        {
            fingerScanUI.Close();
            //cameraUI.Dispose(true);
            fingerScanUI = null;
        }
    }
}
