﻿namespace FingerScannerUIAdapter
{
    partial class FingerScanUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FingerScanUI));
            this.btn_Capture = new System.Windows.Forms.Button();
            this.txt_datos = new System.Windows.Forms.TextBox();
            this.btn_close = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.labtimeout = new System.Windows.Forms.Label();
            this.picSample = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Capture
            // 
            this.btn_Capture.Location = new System.Drawing.Point(161, 27);
            this.btn_Capture.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_Capture.Name = "btn_Capture";
            this.btn_Capture.Size = new System.Drawing.Size(106, 33);
            this.btn_Capture.TabIndex = 1;
            this.btn_Capture.Text = "&Capturar";
            this.btn_Capture.UseVisualStyleBackColor = true;
            this.btn_Capture.Click += new System.EventHandler(this.btn_Capture_Click);
            // 
            // txt_datos
            // 
            this.txt_datos.Location = new System.Drawing.Point(11, 232);
            this.txt_datos.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txt_datos.Multiline = true;
            this.txt_datos.Name = "txt_datos";
            this.txt_datos.Size = new System.Drawing.Size(255, 38);
            this.txt_datos.TabIndex = 2;
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(161, 195);
            this.btn_close.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(106, 33);
            this.btn_close.TabIndex = 3;
            this.btn_close.Text = "&Salir";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.button1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(161, 64);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 33);
            this.button1.TabIndex = 4;
            this.button1.Text = "&Borrar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // labtimeout
            // 
            this.labtimeout.AutoSize = true;
            this.labtimeout.Location = new System.Drawing.Point(13, 9);
            this.labtimeout.Name = "labtimeout";
            this.labtimeout.Size = new System.Drawing.Size(13, 13);
            this.labtimeout.TabIndex = 5;
            this.labtimeout.Text = "0";
            // 
            // picSample
            // 
            this.picSample.Location = new System.Drawing.Point(12, 27);
            this.picSample.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.picSample.Name = "picSample";
            this.picSample.Size = new System.Drawing.Size(145, 201);
            this.picSample.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSample.TabIndex = 0;
            this.picSample.TabStop = false;
            // 
            // FingerScanUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 283);
            this.Controls.Add(this.labtimeout);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.txt_datos);
            this.Controls.Add(this.btn_Capture);
            this.Controls.Add(this.picSample);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FingerScanUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BC7 FingerGet...";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FingerScanUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picSample;
        private System.Windows.Forms.Button btn_Capture;
        private System.Windows.Forms.TextBox txt_datos;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labtimeout;
    }
}

