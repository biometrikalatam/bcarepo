﻿using BioAPI;
using Domain;
using SecuGen.FDxSDKPro.Windows;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Timers;
using System.Windows.Forms;

namespace FingerScannerUIAdapter
{
    public partial class FingerScanUI : Form
    {
        bool _HayCapturaOK = false;

        bool typeExecutionGUI = false;
        private System.Timers.Timer timerControlGeneral;
        private SecugenReader reader = null;
        public string SerialSensor { get; set; }
        private BioPacket bioPacketLocal;
        public bool SensorConnected => ReaderFactory.ReaderConnected;
        //public FingerCaptured FingerCaptured { get; private set; }
        private SGFingerPrintManager SecugenManager;

        //public int Tokencontent { get; set; }
        public bool HaveData { get; set; }

        public FingerScanUI()
        {
            InitializeComponent();
            
        }

        delegate void StringArgReturningVoidDelegate(string text);
        private object lockTimeout = new object();
        private void SetTimeout(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.labtimeout.InvokeRequired)
            {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(SetTimeout);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                lock (lockTimeout)
                {
                    try
                    {
                        this.labtimeout.Text = text;
                    }
                    catch (Exception)
                    {
                        
                    }
                    
                }
            }
        }

        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        internal int Channel(BioPacket bioPacket)
        {
            _HayCapturaOK = false;
            bioPacketLocal = bioPacket;
            //if (bioPacketLocal.PacketParamOut == null)
            //    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            int ret = CheckParameters(bioPacketLocal);

            if (ret == 0)
            {
                if (bioPacketLocal.PacketParamOut == null)
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
                //Chequea si es con o sin visualización
                //NameValueCollection qs = bioPacketLocal.PacketParamIn;
                if (String.Equals(dictParamIn["GUI"], "false"))
                {
                    this.Visible = false;
                    typeExecutionGUI = false;
                }
                else
                {
                    typeExecutionGUI = true;
                }

                int timeout = 2000;
                try
                {
                    timeout = Convert.ToInt32(dictParamIn["Timeout"]);
                }
                catch (Exception ex)
                {
                    timeout = 5000;
                }
                if (timeout == 0) timeout = 5000;
                labtimeout.Text = timeout.ToString();
                labtimeout.Refresh();
                /// Timer del control general, se cierra en 10 segundos
                timerControlGeneral = new System.Timers.Timer(timeout);
                timerControlGeneral.Elapsed += TimerControlGeneral_Elapsed;
                timerControlGeneral.Enabled = true;
            }
            return ret;


            /*
            bioPacketLocal = bioPacket;
            if (bioPacketLocal.PacketParamOut == null)
                bioPacketLocal.PacketParamOut = new Dictionary<string, object>();

            //Chequea si es con o sin visualización
            NameValueCollection qs = bioPacketLocal.PacketParamIn;
            if (String.Equals((string)qs["GUI"],"false"))
            {
                this.Visible = false;
                typeExecutionGUI = false;
            } else
            {
                typeExecutionGUI = true;
            }

            int timeout = 2000;
            try
            {
                timeout = Convert.ToInt32(qs["timeout"]);
            }
            catch (Exception ex)
            {
                timeout = 5000;
            }
            if (timeout == 0) timeout = 5000;
            labtimeout.Text = timeout.ToString();
            labtimeout.Refresh();
            /// Timer del control general, se cierra en 10 segundos
            timerControlGeneral = new System.Timers.Timer(timeout);
            timerControlGeneral.Elapsed += TimerControlGeneral_Elapsed;
            timerControlGeneral.Enabled = true;
            */
        }

        private int CheckParameters(BioPacket bioPacketLocal)
        {
            /*
                 src=fs&
                 GUI=false&
                 timeout=3000&
                 typeid=rut&
                 valueid=232&
                 finger=7#
            */
            int ret = 0;
            try
            {
                if (bioPacketLocal == null) ret = -2;  //Lista de parametros no puede ser nulo

                //Paso a Dictionary

                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    dictParamIn.Add(k, bioPacketLocal.PacketParamIn[k]);
                }

                if (!dictParamIn.ContainsKey("src") || String.IsNullOrEmpty(dictParamIn["src"]))
                {
                    ret = -3;  //src deben ser enviados como parámetros
                }
                else
                {
                    if (!dictParamIn.ContainsKey("GUI") || String.IsNullOrEmpty(dictParamIn["GUI"]))
                    {
                        if (!dictParamIn.ContainsKey("GUI"))
                        {
                            dictParamIn.Add("GUI", "true");
                        }
                        else
                        {
                            dictParamIn["GUI"] = "true";  //Muestra pantalla dse captura
                        }
                    }
                    if (!dictParamIn.ContainsKey("TypeId") || String.IsNullOrEmpty(dictParamIn["TypeId"]))
                    {
                        if (!dictParamIn.ContainsKey("TypeId"))
                        {
                            dictParamIn.Add("TypeId", "RUT");
                        }
                        else
                        {
                            dictParamIn["TypeId"] = "RUT"; //RUT
                        }
                    }
                    if (!dictParamIn.ContainsKey("ValueId") || String.IsNullOrEmpty(dictParamIn["ValueId"]))
                    {
                        if (!dictParamIn.ContainsKey("ValueId"))
                        {
                            dictParamIn.Add("ValueId", "NA");
                        }
                        else
                        {
                            dictParamIn["ValueId"] = "NA"; //NA - No Aplica
                        }
                    }
                    if (!dictParamIn.ContainsKey("Timeout") || String.IsNullOrEmpty(dictParamIn["Timeout"]))
                    {
                        if (!dictParamIn.ContainsKey("Timeout"))
                        {
                            dictParamIn.Add("Timeout", "10000");
                        }
                        else
                        {
                            dictParamIn["Timeout"] = "10000";
                        }
                    }
                    if (!dictParamIn.ContainsKey("Finger") || String.IsNullOrEmpty(dictParamIn["Finger"]))
                    {
                        if (!dictParamIn.ContainsKey("Finger"))
                        {
                            dictParamIn.Add("Finger", "1");
                        }
                        else
                        {
                            dictParamIn["Finger"] = "1";
                        }
                    }
                    //if (!dictParamIn.ContainsKey("Theme") || String.IsNullOrEmpty(dictParamIn["Theme"]))
                    //{
                    //    if (!dictParamIn.ContainsKey("Theme"))
                    //    {
                    //        dictParamIn.Add("Theme", "theme_ligthgreen");
                    //    }
                    //    else
                    //    {
                    //        dictParamIn["Theme"] = "theme_green";
                    //    }
                    //}
                    //if (!dictParamIn.ContainsKey("Hash") || String.IsNullOrEmpty(dictParamIn["Hash"]))
                    //{
                    //    if (!dictParamIn.ContainsKey("Hash"))
                    //    {
                    //        dictParamIn.Add("Hash", Guid.NewGuid().ToString("N"));
                    //    }
                    //    else
                    //    {
                    //        dictParamIn["Hash"] = Guid.NewGuid().ToString("N");
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                Log.WriteLog("Error BioPortalClient7UIAdapter.CheckParameters [" + ex.Message + "]");
                ret = -1;
            }
            return ret;
        }

        private void TimerControlGeneral_Elapsed(object sender, ElapsedEventArgs e)
        {
            //if (iTinmerToShow > 0)
            //{
            //    iTinmerToShow--;
            //    labtimeout.Text = "Quedan " + iTinmerToShow.ToString() + " segundos...";
            //}
            //else
            //{
            timerControlGeneral.Enabled = false;
            labtimeout.Text = "Timeout!";
            //}
            labtimeout.Refresh();
            //labDedoSeleccionado.Text = "Ningún dedo seleccionado";

            if (!_HayCapturaOK)
            {
                Log.WriteLog("Timeout! -> no se detectó actividad en mas de 10 segundos");
                if (bioPacketLocal.PacketParamOut.ContainsKey("Error"))
                {
                    bioPacketLocal.PacketParamOut["Error"] = "-5|Timeout";
                }
                else
                {
                    bioPacketLocal.PacketParamOut.Add("Error", "-5|Timeout");
                }
            }
            //ClearBio();
            CloseControl(_HayCapturaOK);
        }

        private void OnCapturedEvent(IFingerCaptured fingerCaptured)
        {
            reader.OnCapturedEvent -= OnCapturedEvent;
            try
            {
                if (fingerCaptured.CaptureQuality == ReaderFactory.CaptureQuality.QUALITY_GOOD)
                {
                    bioPacketLocal.PacketParamOut.Add("TemplateVerify64", fingerCaptured.TemplateVerify64);
                    bioPacketLocal.PacketParamOut.Add("TemplateRegistry64", fingerCaptured.TemplateRegistry64);
                    bioPacketLocal.PacketParamOut.Add("TemplateANSI64", fingerCaptured.TemplateANSI64);
                    bioPacketLocal.PacketParamOut.Add("TemplateISO64", fingerCaptured.TemplateISO64);
                    bioPacketLocal.PacketParamOut.Add("WSQ", fingerCaptured.WSQ64);

                    bioPacketLocal.PacketParamOut.Add("Huella", fingerCaptured.Image);
                    bioPacketLocal.PacketParamOut.Add("Width", fingerCaptured.Image.Width);
                    bioPacketLocal.PacketParamOut.Add("Height", fingerCaptured.Image.Height);

                    bioPacketLocal.PacketParamOut.Add("Error", "0|Capture OK");
                    bioPacketLocal.PacketParamOut.Add("BodyPart", dictParamIn["Finger"]);
                    //bioPacketLocal.PacketParamOut.Add("BodyPartName", );
                    bioPacketLocal.PacketParamOut.Add("AuthenticationFactor", 2);  //AF =  Fingerprint
                    //bioPacketLocal.PacketParamOut.Add("AuthenticationFactorName", );
                    bioPacketLocal.PacketParamOut.Add("SerialDevice", SerialSensor);

                    picSample.Image = fingerCaptured.Image;
                    picSample.Refresh();
                    _HayCapturaOK = true;
                }
                else if (fingerCaptured.CaptureQuality == ReaderFactory.CaptureQuality.QUALITY_POOR)
                {
                    bioPacketLocal.PacketParamOut.Add("Error", "-41|Mala Calidad Captura"); //Mala Calidad => SERR_WSQ_LOW_QUALITY
                    picSample.Refresh();
                } 
            }
            catch (Exception ex)
            {
                bioPacketLocal.PacketParamOut.Add("Error", "-1|Error Procesando Captura [" + ex.Message + "]");
                //WriteLog("ERROR - BeginCapture Error - " + ex.Message);
            }
            //CloseControl(true);
        }
        private string GetImageB64FromImage(object image)
        {
            byte[] byImg;
            try
            {
                System.Drawing.Image img = (System.Drawing.Image)(((System.Drawing.Image)image).Clone());
                byImg = ImageToByteArray(img);
                return Convert.ToBase64String(byImg);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            try
            {
                using (var ms = new MemoryStream())
                {
                    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        private void btn_Capture_Click(object sender, EventArgs e)
        {
            NameValueCollection qs = bioPacketLocal.PacketParamIn;

            //CaptureFinger(qs["TypeId"], qs["ValueId"], Convert.ToInt16(qs["Finger"]));
            CaptureFinger(dictParamIn["TypeId"], dictParamIn["ValueId"], Convert.ToInt16(dictParamIn["Finger"]));
        }

        private void CaptureFinger(string typeId, string valueId, int fingerId)
        {
            try
            {
                if (reader == null)
                    reader = (SecugenReader)ReaderFactory.GetReader(ReaderFactory.ReaderBrand.SECUGEN);

                SerialSensor = reader.ReaderInfo.SerialNumber;
            }
            catch (Exception e)
            {
                var error = e.Message;
                throw;
                //ELog(string.Format("CaptureFinger -> GetReader() {0}", error));
            }

            if (SensorConnected && !String.IsNullOrEmpty(typeId) && !String.IsNullOrEmpty(valueId))
            {
                if (fingerId >= 1 && fingerId <= 10)
                {
                    BeginCapture(fingerId);
                }
            }
            else
            {
                //ELog(string.Format("CaptureFinger -> sc {0}", SensorConnected));
            }
        }

        private void BeginCapture(int fingerId)
        {
            try
            {
                reader.OnCapturedEvent += OnCapturedEvent;

                if (reader.ActualReader() == null)
                    reader.SetReaderSecugen(SecugenManager);

                reader.HardCapture(reader.ActualReader(), fingerId);
            }
            catch (Exception ex)
            {
                reader.OnCapturedEvent -= OnCapturedEvent;
            }
        }

        private void FingerScanUI_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            NameValueCollection qs = bioPacketLocal.PacketParamIn;

//            txt_datos.Text = string.Concat(qs["TypeId"], "-", qs["ValueId"], "-", Convert.ToInt16(qs["Finger"]));
            txt_datos.Text = string.Concat(dictParamIn["TypeId"], "-", dictParamIn["ValueId"], "-", Convert.ToInt16(dictParamIn["Finger"]));

            this.Visible = typeExecutionGUI;
            if (!typeExecutionGUI)
            {
                this.Width = 0;
                this.Height = 0;
                btn_Capture_Click(null, null);
            }
            this.TopMost = true;
            this.Refresh();

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CloseControl(true);
        }

        private void CloseControl(bool flag)
        {
            HaveData = flag;
            this.Close();
        }
    }
}