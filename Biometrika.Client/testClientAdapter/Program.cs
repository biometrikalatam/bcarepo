﻿using Domain;
//using Manager;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using log4net.Config;
using log4net;
using System.Device.Location;
using BVIManager7;
using DRManager7;


namespace testClientAdapter
{
    public class Program
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Program));

        static Biometrika.BioApi20.BSP.BSPBiometrika _BSP;

        private static void Main(string[] args)
        {
            string currentPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            XmlConfigurator.Configure(new FileInfo(currentPath + "\\log\\Logger.cfg"));
            LOG.Info("testClientAdapter.Main Init...");
            //byte[] byPDF = new byte[2];
            //System.IO.File.WriteAllBytes(Environment.SpecialFolder.MyDocuments + @"\t.pdf", byPDF);
            //Test GeoReferenciacion
            //string coordenadas = GetGeolocalization();
            //GeoCoordinateWatcher oWather = new GeoCoordinateWatcher();
            //oWather.PositionChanged += (S, E) =>
            //{
            //    GeoCoordinate oCoordinate = oWather.Position.Location;
            //    if (oCoordinate != null && oCoordinate.Latitude != null && oCoordinate.Longitude != null)
            //    {
            //        coordenadas = oCoordinate.Latitude.ToString().Replace(",",".") + "," + oCoordinate.Longitude.ToString().Replace(",", ".");
            //    }
            //    if (!string.IsNullOrEmpty(coordenadas))
            //        oWather.Stop();
            //};
            //oWather.Start();


            //TestDR();

            TestBVI7();
            //Managers.Initialize();
            //TestCammera();
            //TestFinger();
            //TestManager();
            Console.Read();
        }

        private static string GetGeolocalization()
        {
            string ret = null;
            try
            {
                int qRepeticiones = 5;
                GeoCoordinateWatcher watcher = new GeoCoordinateWatcher();

                // Do not suppress prompt, and wait 1000 milliseconds to start.
                watcher.TryStart(false, TimeSpan.FromMilliseconds(1000));

                while (qRepeticiones > 0)
                {
                    System.Threading.Thread.Sleep(1000);
                    GeoCoordinate coord = watcher.Position.Location;

                    if (coord.IsUnknown != true)
                    {
                        ret = coord.Latitude.ToString().Replace(",", ".") + "," + coord.Longitude.ToString().Replace(",", ".");
                        qRepeticiones = 0;
                    }
                    else
                    {
                        qRepeticiones--;
                        //Console.WriteLine("Unknown latitude and longitude.");
                    }
                }
            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }


        private static void TestDR()
        {
            NameValueCollection nameValueCollection = new NameValueCollection();
            nameValueCollection.Add("src", "DR");
            //nameValueCollection.Add("TypeID", "RUT");
            //nameValueCollection.Add("ValueID", "21284415-2");

            //InitBSP();

            DRManager drmanager = new DRManager(nameValueCollection, null);

            string result = drmanager.Buffer; //  Managers.GetData(nameValueCollection);

            Console.WriteLine(result);
        }

        private static void TestBVI7()
        {
            NameValueCollection nameValueCollection = new NameValueCollection();
            nameValueCollection.Add("src", "BVI7");
            nameValueCollection.Add("TypeID", "RUT");
            nameValueCollection.Add("ValueID", "21284415-2");

            InitBSP();

            BVIManager7.BVIManager bvimanager = new BVIManager7.BVIManager(nameValueCollection, _BSP);

            string result = bvimanager.Buffer; //  Managers.GetData(nameValueCollection);

            Console.WriteLine(result);
        }

        private static bool InitBSP()
        {
            bool ret = false;
            try
            {
                //LOG.Info("LocalServerManager.InitBSP In Loading BSP...");
                _BSP = new Biometrika.BioApi20.BSP.BSPBiometrika();

                _BSP.BSPAttach("2.0", true);
                ret = _BSP.IsLoaded;
                //LOG.Info("LocalServerManager.InitBSP BSP Loaded = " + ret.ToString());
            }
            catch (Exception ex)
            {
                //LOG.Error("LocalServerManager.InitBSP Error = " + ex.Message);
                ret = false;
            }
            return ret;
            //GetInfoFromBSP();

            //BSP.OnCapturedEvent += OnCaptureEvent;
            //BSP.OnTimeoutEvent += OnTimeoutEvent;
        }

        private static void TestManager()
        {

            //DateTime nacimiento = new DateTime(1969, 9, 28); //Fecha de nacimiento
            //int edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
            //nacimiento = new DateTime(1967, 01, 16); //Fecha de nacimiento
            //edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
            //nacimiento = new DateTime(2001, 12, 02); //Fecha de nacimiento
            //edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
            //nacimiento = new DateTime(2008, 02, 06); //Fecha de nacimiento
            //edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;

            NameValueCollection nameValueCollection = new NameValueCollection();
            nameValueCollection.Add("src", "DR");
            nameValueCollection.Add("TypeID", "RUT");
            nameValueCollection.Add("ValueID", null);
            nameValueCollection.Add("OT", "0");

            //DocReaderManager.DocReaderManager drm = new DocReaderManager.DocReaderManager(nameValueCollection);

            //string result = drm.Buffer; //  Managers.GetData(nameValueCollection);

            //Console.WriteLine(result);
        }

        private static void TestFinger()
        {
            NameValueCollection nameValueCollection = new NameValueCollection();
            nameValueCollection.Add("TypeId", "RUT");
            nameValueCollection.Add("ValueId", "212121212");
            nameValueCollection.Add("Finger", "1");

            //FingerScannerManager manager = new FingerScannerManager(nameValueCollection);

            //Console.WriteLine(manager.Buffer);
        }

        private static void TestCammera()
        {
            NameValueCollection nameValueCollection = new NameValueCollection();
            nameValueCollection.Add("Type", "RUT");
            nameValueCollection.Add("Value", "212121212");
            nameValueCollection.Add("Size", "3x2");

            //CameraManager cameraManager = new CameraManager(nameValueCollection);

            //Console.WriteLine(cameraManager.Buffer);
        }
    }
}