﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvertTool.Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            textBox2.Text = "Empieza...";
            Refresh();
            System.Diagnostics.ProcessStartInfo sinfo = new System.Diagnostics.ProcessStartInfo();
           

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = Application.StartupPath + "\\ConvertTool.exe";
            process.StartInfo.Arguments = textBox1.Text + @" C:\tmp\aa\WaterMark.bmp MP RUT 21284415-2 123456N";
            process.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
            process.Start();
            process.WaitForExit(10000); // Waits here for the process to exit.

            //.Start(Application.StartupPath + "\\ConvertTool.exe", path);
            textBox2.Text = "Termino!";
            Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox2.Text = "mpeg in...";
            Refresh();
            using (Process p = new Process())
            {
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.FileName = "ffmpeg.exe";
                p.StartInfo.Arguments = "-i video.mp4 -i marca_de_agua.png -filter_complex \"overlay=20:20\" video_con_marca.mp4";
                p.Start();
                p.WaitForExit();
            }
            textBox2.Text = "mpeg END!";
            Refresh();

        }
    }
}
