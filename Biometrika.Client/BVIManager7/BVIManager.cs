﻿using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Script.Serialization;

namespace BVIManager7
{
    public class BVIManager : IManager
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(BVIManager));

        public Biometrika.BioApi20.BSP.BSPBiometrika _BSP;
        private bool _IS_BPS_CONFIGURED = false;
        private bool isOpen = false;
        private Dictionary<string, object> output = new Dictionary<string, object>();
        private string templateToken = string.Empty;

        public string Buffer => new JavaScriptSerializer().Serialize(output);

        public string Tag { get => "BVI7"; }

        public BVIManager() { } // ctor de reflextion

        public BVIManager(NameValueCollection queryString, object objBSP)
        {
            LOG.Info("BVIManager In...");
            //Log.Add("BioPortalClient7Manager In...");
            if (!isOpen)
            {
                LOG.Debug("BVIManager isOpen...");
                isOpen = true;

                if (objBSP != null)
                {
                    _BSP = (Biometrika.BioApi20.BSP.BSPBiometrika)objBSP;
                }

                BVIUIAdapter7.Program.SetBSP(_BSP);
                LOG.Debug("BioPortalClient7Manager SetPacket => " + queryString);
                BVIUIAdapter7.Program.SetPacket(queryString);     // el program recibe el pedido
                LOG.Debug("BioPortalClient7Manager Mail...");
                BVIUIAdapter7.Program.Main();

                Dictionary<string, object> outputUI = BVIUIAdapter7.Program.Packet.PacketParamOut;
                LOG.Debug("BioPortalClient7Manager outupUI.Length = " + outputUI.Count);

                // Este param out retorna la imagen de la captura y las minucias
                if (outputUI != null && outputUI.Count > 0)
                {
                    //output.Add("TrackIdCI", outputUI.ContainsKey("TrackIdCI") ? outputUI["TrackIdCI"] : null);
                    //LOG.Debug("BVIManager => TrackIdCI = " + (outputUI.ContainsKey("TrackIdCI") ? outputUI["TrackIdCI"] : "NO TrackIdCI"));
                    output.Add("BVIResponse", outputUI.ContainsKey("BVIResponse") ? outputUI["BVIResponse"] : null);
                    LOG.Debug("BVIManager => BVIResponse = " + (outputUI.ContainsKey("BVIResponse") ? outputUI["BVIResponse"] : "NO Resultado"));
                }
            }
            LOG.Info("BioPortalClient7Manager Out!");
        }

        

        // DESCARTE
        public BVIManager(NameValueCollection queryString)
        {
            //LOG.Info("BVIManager In...");
            ////Log.Add("BioPortalClient7Manager In...");
            //if (!isOpen)
            //{
            //    LOG.Debug("BVIManager isOpen...");
            //    isOpen = true;

            //    LOG.Debug("BVIManager SetPacket => " + queryString);
            //    BVIUIAdapter7.Program.SetPacket(queryString);     // el program recibe el pedido
            //    LOG.Debug("BVIManager Mail...");
            //    BVIUIAdapter7.Program.Main();

            //    Dictionary<string, object> outputUI = BVIUIAdapter7.Program.Packet.PacketParamOut;
            //    LOG.Debug("BVIManager outupUI.Length = " + outputUI.Count);

            //    // Este param out retorna la imagen de la captura y las minucias
            //    if (outputUI != null && outputUI.Count > 0)
            //    {
            //        //output.Add("TrackIdCI", outputUI.ContainsKey("TrackIdCI") ? outputUI["TrackIdCI"] : null);
            //        //LOG.Debug("BVIManager => TrackIdCI = " + (outputUI.ContainsKey("TrackIdCI") ? outputUI["TrackIdCI"] : "NO TrackIdCI"));
            //        output.Add("BVIResponse", outputUI.ContainsKey("BVIResponse") ? outputUI["BVIResponse"] : null);
            //        LOG.Debug("BVIManager => BVIResponse = " + (outputUI.ContainsKey("BVIResponse") ? outputUI["BVIResponse"] : "NO Resultado"));
            //        //output.Add("Error", outputUI.ContainsKey("Error") ? outputUI["Error"] : null);
            //        //LOG.Debug("BVIManager => Error = " + (outputUI.ContainsKey("Error") ? outputUI["Error"] : "NO Error"));
            //        //output.Add("BodyPart", outputUI.ContainsKey("BodyPart") ? outputUI["BodyPart"] : 0);
            //        //LOG.Debug("BVIManager => BodyPart = " + (outputUI.ContainsKey("BodyPart") ? outputUI["BodyPart"] : "NO BodyPart"));
            //        //output.Add("Width", outputUI.ContainsKey("Width") ? outputUI["Width"] : 0);
            //        //LOG.Debug("BVIManager => Width = " + (outputUI.ContainsKey("Width") ? outputUI["Token"] : "NO Width"));
            //        //output.Add("Height", outputUI.ContainsKey("Height") ? outputUI["Height"] : 0);
            //        //LOG.Debug("BVIManager => Height = " + (outputUI.ContainsKey("Height") ? outputUI["Height"] : "NO Height"));
            //        //output.Add("Hash", outputUI.ContainsKey("Hash") ? outputUI["Hash"] : 0);
            //        //LOG.Debug("BVIManager => Hash = " + (outputUI.ContainsKey("Hash") ? outputUI["Hash"] : "NO Hash"));
            //        LOG.Info("BVIManager Seting retorno...");
            //    }
            //}
            //LOG.Info("BVIManager Out!");
        }

        //private bool InitBSP()
        //{
        //    bool ret = false;
        //    try
        //    {
        //        _BSP = new Biometrika.BioApi20.BSP.BSPBiometrika();

        //        _BSP.BSPAttach("2.0", true);
        //        ret = _BSP.IsLoaded;
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("BioPortalClient7Manager.InitBSP Error = " + ex.Message);
        //        ret = false;
        //    }
        //    return ret;
        //    //GetInfoFromBSP();

        //    //BSP.OnCapturedEvent += OnCaptureEvent;
        //    //BSP.OnTimeoutEvent += OnTimeoutEvent;
        //}
    }

    public class MsgError
    {

        public MsgError() { }

        public MsgError(string strerr)
        {
            try
            {
                if (String.IsNullOrEmpty(strerr))
                {
                    Codigo = -1;
                    Description = "Retorno Nulo";
                }
                else
                {
                    string[] arrerr = strerr.Split('|');
                    Codigo = Convert.ToInt32(arrerr[0]);
                    Description = arrerr[1];
                }
            }
            catch (Exception ex)
            {
                Description = "Error parseando error return [" + ex.Message + "]";
            }
        }

        public int Codigo { get; set; }
        public string Description { get; set; }



    }
}
