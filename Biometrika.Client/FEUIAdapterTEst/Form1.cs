﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;

namespace FEUIAdapterTEst
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string queryString1 = "?src=FEM&TypeId=RUT" +
                                 "&ValueId=21284415-2" +
                                 "&Mail=gsuhit@gmail.com" + 
                                 "&DocumentId=123456NM" +
                                 "&MsgToRead=txtMsgToRead";

            NameValueCollection queryString = HttpUtility.ParseQueryString(queryString1);

            FEUIAdapter.Program.SetPacket(queryString);     // el program recibe el pedido
            //FEUIAdapter.Program.Main();
            Dictionary<string, object> outputUI = null;
            Thread thread = new Thread(() =>
            {
                //doc.LoadFromHTML("file:///C:/contrato/s.html", true, true, false, setting);
                FEUIAdapter.Program.Main();
                outputUI = FEUIAdapter.Program.Packet.PacketParamOut;
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();

            thread.Abort();
            thread = null;

              // Este param out retorna la imagen de la captura y las minucias
            if (outputUI != null && outputUI.Count > 0)
            {
                richTextBox1.Text = richTextBox1.Text + Environment.NewLine + "FEManager => Error = " + (outputUI.ContainsKey("Error") ? outputUI["Error"] : "NO Error");
                richTextBox1.Text = richTextBox1.Text + Environment.NewLine + "FEManager => DeviceId = " + (outputUI.ContainsKey("DeviceId") ? outputUI["DeviceId"] : "NO DeviceId");
                //LOG.Debug("FEManager => FEM = " + (outputUI.ContainsKey("FEM") ? outputUI["FEM"] : "NO Error"));
                //LOG.Debug("FEManager => Hash = " + (outputUI.ContainsKey("Hash") ? outputUI["Hash"] : "NO Hash"));
                //thread.Abort();

            }
        }
    }
}
