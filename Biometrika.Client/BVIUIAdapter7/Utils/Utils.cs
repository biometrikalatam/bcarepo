﻿using log4net;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BVIUIAdapter7.Utils
{
    internal class Utils
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Utils));

        internal static String FormatRutFormateado(String rut)
        {
            return rut.Substring(0, rut.Length - 1) + "-" + rut.Substring(rut.Length - 1);

        }

        internal static bool IsMenorDe18(DateTime from)
        {
            if (from.AddYears(18) < DateTime.Now)
                return false;
            else
                return true;
        }

        internal static bool IsCedulaVigente(DateTime expirationDate)
        {
            bool ret;
            try
            {
                log.Debug("Utils.IsCedulaVigente IN - expirationDate = " + expirationDate);
                DateTime dt = expirationDate; // DateTime.Parse(expirationDate);
                log.Debug("Utils.IsCedulaVigente Now = " + DateTime.Now.ToString("dd/MM/yyyy"));
                ret = (DateTime.Now < dt);
                log.Debug("Utils.IsCedulaVigente => " + ret.ToString());
            }
            catch (Exception ex)
            {
                ret = false;
                log.Error("Utils.IsCedulaVigente Error = " + ex.Message, ex);
            }
            return ret;
        }

        /// <summary>
        /// Parsea una fecha con diferentes formatos. Sino devuelve MinValue
        /// Formatos de fecha procesados:
        ///     yyyy/MM/dd
        ///     yyyy/M/dd
        ///     dd/MM/yyyy
        ///     dd/M/yyyy
        ///     M/dd/yyyy
        ///     MM/dd/yyyy
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static DateTime Parse(string strDate)
        {
            DateTime dRet = new DateTime(1900, 1, 1);
            bool mustReturn = false;
            try
            {
                log.Debug("Utils.Parse IN - Fechav = " + (string.IsNullOrEmpty(strDate) ? "Null" : strDate));
                if (string.IsNullOrEmpty(strDate))
                    return dRet;

                //Reemplazo - por /
                strDate = strDate.Replace("-", "/");
                log.Debug("Utils.Parse IN - Fechav = " + strDate);
                try
                {
                    log.Debug("Utils.Parse try yyyy/MM/dd...");
                    dRet = DateTime.ParseExact(strDate, "yyyy/MM/dd", null);
                    log.Debug("Utils.Parse try yyyy/MM/dd OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;

                try
                {
                    log.Debug("Utils.Parse try yyyy/M/dd...");
                    dRet = DateTime.ParseExact(strDate, "yyyy/M/dd", null);
                    log.Debug("Utils.Parse try yyyy/M/dd OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;
                //------------------
                try
                {
                    log.Debug("Utils.Parse try dd/MM/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
                    log.Debug("Utils.Parse try dd/MM/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;

                try
                {
                    log.Debug("Utils.Parse try dd/M/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "dd/M/yyyy", null);
                    log.Debug("Utils.Parse try dd/M/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;

                try
                {
                    log.Debug("Utils.Parse try M/dd/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "M/dd/yyyy", null);
                    log.Debug("Utils.Parse try M/dd/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;

                try
                {
                    log.Debug("Utils.Parse try MM/dd/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "MM/dd/yyyy", null);
                    log.Debug("Utils.Parse try MM/dd/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;

                try
                {
                    log.Debug("Utils.Parse try M/d/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "M/d/yyyy", null);
                    log.Debug("Utils.Parse try M/d/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    log.Debug("Utils.Parse Ex: " + ex1.Message);
                }
            }
            catch (Exception ex)
            {
                log.Error("Utils.Pars Error: " + ex.Message);
            }
            return dRet;
        }

        internal static string[] ConvertToHexa(byte[] data)
        {
            byte[] _temp = new Byte[4000];
            Int32 _isoLength = 0, _isocLength = 0;
            try
            {
                log.Debug("Utils.ConvertToHexa Reorder Minutiae IN...");
                //Re-ordenamiento de minucias
                for (Int32 n = 0; n < data.Length; n++)
                {
                    _temp[n] = data[n + 28];

                    if (n > 0 && _temp[n - 1] == 0x00)
                    {
                        _isocLength = n;

                        break;
                    }
                }

                _isocLength = _isocLength - 1;

                Byte[] _isocTemplate = new Byte[_isocLength];

                for (Int32 n = 0; n < _isocLength; n++)
                {
                    _isocTemplate[n] = _temp[n];
                }
                log.Debug("Utils.ConvertToHexa Reorder Minutiae OUT!");

                string _isoCCtoHexa = BitConverter.ToString(_isocTemplate);
                return _isoCCtoHexa.Split('-');
            }
            catch (Exception ex)
            {
                log.Error("Utils.ConvertToHexa Error: " + ex.Message);
                return null;
            }
        }

        internal static string GetMACAddress()
        {
            var sMacAddress = string.Empty;
            var isFirst = true;

            var nics = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface adapter in nics)
            {
                adapter.GetIPProperties();
                if (isFirst)
                {
                    if (adapter.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                    {
                        sMacAddress = FormatMAC(adapter.GetPhysicalAddress().ToString());
                        isFirst = false;
                        break;
                    }
                }
                else
                {
                    //sMacAddress = String.Format("{0}~{1}", sMacAddress, FormatMAC(adapter.GetPhysicalAddress().ToString()));
                }
            }

            return sMacAddress;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        internal static string GetIP()
        {
            var sIP = string.Empty;
            var isFirst = true;

            var localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress ipAddress in localIPs)
            {
                if (ipAddress.ToString().Length <= 15)
                {
                    if (isFirst)
                    {
                        sIP = ipAddress.ToString();
                        isFirst = false;
                    }
                    else
                    {
                        sIP = String.Format("{0}~{1}", sIP, ipAddress);
                    }
                }
            }

            return sIP;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="smac"></param>
        /// <returns></returns>
        private static string FormatMAC(string smac)
        {
            var sMacAddress = string.Empty;

            if (!String.IsNullOrEmpty(smac))
            {
                sMacAddress = String.Format("{0}-{1}-{2}-{3}", smac.Substring(0, 2), smac.Substring(2, 2), smac.Substring(4, 2), smac.Substring(6, 2));
            }

            return sMacAddress;
        }

        internal static string GetGeolocalization()
        {
            string ret = "Desconocido";
            try
            {
                log.Debug("Utils.GetGeolocalization - IN...");
                int qRepeticiones = BVIUI._BVI7CONFIG.CoordinatesAttempts;
                GeoCoordinateWatcher watcher = new GeoCoordinateWatcher();

                // Do not suppress prompt, and wait 1000 milliseconds to start.
                watcher.TryStart(false, TimeSpan.FromMilliseconds(1000));

                while (qRepeticiones > 0)
                {
                    System.Threading.Thread.Sleep(1000);
                    GeoCoordinate coord = watcher.Position.Location;

                    if (coord.IsUnknown != true)
                    {
                        ret = coord.Latitude.ToString().Replace(",", ".") + "," + coord.Longitude.ToString().Replace(",", ".");
                        log.Debug("Utils.GetGeolocalization - Coordenada = " + ret); 
                        qRepeticiones = 0;
                    }
                    else
                    {
                        log.Debug("Utils.GetGeolocalization - Intento = " + qRepeticiones.ToString());
                        qRepeticiones--;
                        //Console.WriteLine("Unknown latitude and longitude.");
                    }
                }
            }
            catch (Exception ex)
            {
                ret = "Desconocido";
                log.Error("Utils.GetGeolocalization Error: " + ex.Message);
            }
            log.Debug("Utils.GetGeolocalization - OUT!");
            return ret;
        }

        internal static bool isFormatMailCorrect(string mail)
        {
            try
            {
                log.Debug("Utils.isFormatMailCorrect - IN - [Mail = " + (string.IsNullOrEmpty(mail)?"Null":mail + "]"));
                String expresion;
                expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
                if (Regex.IsMatch(mail, expresion))
                {
                    if (Regex.Replace(mail, expresion, String.Empty).Length == 0)
                    {
                        log.Debug("Utils.isFormatMailCorrect - OUT True!");
                        return true;
                    }
                    else
                    {
                        log.Debug("Utils.isFormatMailCorrect - OUT False!");
                        return false;
                    }
                }
                else
                {
                    log.Debug("Utils.isFormatMailCorrect - OUT False!");
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Error("Utils.isFormatMailCorrect Error: " + ex.Message);
                log.Debug("Utils.isFormatMailCorrect - OUT False!");
                return false;
            }
            
        }

        /// <summary>
        /// Si se perdio la coma en el double (Ver por que se pierde desde NV Service, aca la coloca
        /// </summary>
        /// <param name="score"></param>
        /// <returns></returns>
        internal static string FormatPorcentual(string score)
        {
            string ret = "0";
            string aux = "0";
            try
            {
                //NumberFormatInfo formatProvider = new NumberFormatInfo();
                //formatProvider.NumberDecimalSeparator = ", ";
                //formatProvider.NumberGroupSeparator = ".";
                //formatProvider.NumberGroupSizes = new int[] { 2 };
                //Console.WriteLine("Converted Decimal value...");
                //double res = Convert.ToDouble(val, formatProvider);
                
                if (!string.IsNullOrEmpty(score))
                {
                    log.Error("Utils.FormatPorcentual - IN => score = " + score);

                    //Si es mas grande de 2 digitos y ademas no hay un punto o coma => pongo punto y pruebo de convertir
                    if (score.Length > 2 && score.IndexOf(".") < 0 && score.IndexOf(",") < 0)
                    {
                        aux = score.Substring(0, 2) + "." + score.Substring(2);
                    } else
                    {
                        aux = score;
                    }

                    double dNumber = Convert.ToDouble(aux);

                    if (dNumber > 100)
                    {
                        aux = aux.Replace(".", ",");
                        dNumber = Convert.ToDouble(aux);

                        if (dNumber > 100)
                        {
                            aux = score.Substring(0, 2);
                        }
                    }
                    ret = aux;
                } else
                {
                    log.Error("Utils.FormatPorcentual - IN => score = NULL");
                }
            }
            catch (Exception ex)
            {
                ret = "0";
                log.Error("Utils.FormatPorcentual Error: " + ex.Message);
            }
            log.Error("Utils.FormatPorcentual - OUT => score formateado = " + ret);
            return ret;
        }
    }
}
