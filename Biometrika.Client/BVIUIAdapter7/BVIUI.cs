﻿using AForge.Video;
using Bio.Core.BCR;
using Bio.Core.CLR;
using Biometrika.BioApi20;
using Biometrika.BioApi20.BSP;
using Biometrika.BioApi20.Interfaces;
using BVIUIAdapter7.Config;
using BVIUIAdapter7.Helpers;
using BVIUIAdapter7.Libs;
using BVIUIAdapter7.Utils;
using Domain;
using Emgu.CV;
using Emgu.CV.Structure;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Threading;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using ZXing;

namespace BVIUIAdapter7
{
    public partial class BVIUI : Form
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BVIUI));

        internal static BVI7Config _BVI7CONFIG = null;
            
        Biometrika.BioApi20.BSP.BSPBiometrika BSP;
        List<Biometrika.BioApi20.Sample> _SamplesCaptured;
        byte[] _Sample_Standard;

        BCR _LECTOR_CODIGO_DE_BARRAS;
        bool _HAY_PDF417 = false;

        EstadoWizard _ESTADO_WIZARD = EstadoWizard.EsperandoLecturaCodigoBarras;

        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        public Solicitud _SOLICITUD { get; set; }
        public Persona _PERSONA { get; set; }

        public int _CLIENT_TYPE = 0; //Usado para poder definir desde Solicitud o con seleccion en comenzar

        internal NVHelper _NVHELPER;

        internal string _CLR_DOE;
        internal string _CLR_DOB;
        internal string _ISO_COMPACT;

        //Variables para uso de Reconociento CB con WebCam
        private readonly CameraDevices camDevices;
        private Bitmap currentBitmapForDecoding;
        private readonly Thread decodingThread;
        private Result currentResult;
        private readonly Pen resultRectPen;
        private byte[] resultAsBinary { get; set; }
        /// <summary>
        /// Estructura para almacenar información de obtenido del código de barra con WebCam
        /// </summary>
        private struct Device
        {
            public int Index;
            public string Name;
            public override string ToString()
            {
                return Name;
            }
        }
        private IList<Device> devices = new List<Device>();
        //Variable que indica si existe un error en la cámara.
        private bool CameraError = false;
        public BarcodeWebCam Barcodewebcam { get; set; }

        private String BarCodeContent { get; set; }
        //EndVariables para uso de Reconociento CB con WebCam

        public string SerialSensor { get; set; }
        public bool SensorConnected; //=> ReaderFactory.ReaderConnected;
        //private AbstractReader reader;

        //public int Tokencontent { get; set; }
        public bool HaveData { get; set; }
        public static BioPacket Packet { get; set; }
        private BioPacket bioPacketLocal;

        private RUT _RUT;

        private string json;
        private bool isMenuOpen = false;
        private int timeLeft;
        private System.Timers.Timer timer;
        private System.Timers.Timer timerControlGeneral;


        Graphics paperDocument;
        Pen pencil;


        #region Variables de Cam por Emgu

        private string _pathPDF = null;
        private string _pathXML = null;
        private bool _MUSTRUN = false;
        internal string DeviceId { get; set; }
        internal string GeoRef { get; set; }
        public string DocumentId { get; set; }
        public string Mail { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string Company { get; set; }

        public string RDEnvelope { get; set; }
        public string _TRACK_ID_CI { get; set; }

        private Capture _CAPTURECAM;

        #endregion Variables de Cam por Emgu


        #region General

        public BVIUI()
        {
            InitializeComponent();

            //Seccion para Recognition CB con WebCam
            camDevices = new CameraDevices();
            
            camDevices.SelectCamera(0);
            
            decodingThread = new Thread(DecodeBarcode);
            decodingThread.Start();

            picImageGrpStream.Paint += picImageGrpStream_Paint;
            resultRectPen = new Pen(Color.Green, 10);
        }

        

        private void BVIUI_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            LOG.Debug("BVIUI.BVIUI_Load - Iniatilazation...");
            //this.TopMost = true;
            //this.TopLevel = true;
            this.Size = new Size(1370, 770);
            this.StartPosition = FormStartPosition.CenterScreen;
            //this.Location = new Point(100, 100);
            this.Refresh();

            //labClientType.Text = "";
            //labClientType.Visible = true;
            //labClientType.Refresh();

            txtMessage.Text = "";
            txtMessage.Refresh();
            txtMessage.Visible = true;

            labSubTitle.Text = "";
            labSubTitle.Refresh();
            labSubTitle.Visible = true;

            //Redondea picturebox de la captura de huella
            //System.Drawing.Drawing2D.GraphicsPath objDraw = new System.Drawing.Drawing2D.GraphicsPath();
            //objDraw.AddEllipse(0, 0, this.picSample.Width, this.picSample.Height);
            //this.picSample.Region = new Region(objDraw);

            picGuiaCaptureDocument.Parent = imageCam;
            int x = (imageCam.Width - picGuiaCaptureDocument.Width) / 2;
            picGuiaCaptureDocument.Location = new Point(x, 10);
            //pictureBox3.Size = imageCam.Size;
            picGuiaCaptureDocument.BackColor = Color.Transparent;
            picGuiaCaptureDocument.Refresh();

            picGuiaCaptureSelfie.Parent = imageCamSelfie;
            x = (imageCamSelfie.Width - picGuiaCaptureSelfie.Width) / 2;
            picGuiaCaptureSelfie.Location = new Point(x, 10);
            //pictureBox3.Size = imageCam.Size;
            picGuiaCaptureSelfie.BackColor = Color.Transparent;
            picGuiaCaptureSelfie.Refresh();


            //Guia de captura en videos
            //Graphics paperDocument;
            paperDocument = imageCam.CreateGraphics();
            // Create a new pen.
            pencil = new Pen(Brushes.Green);
            // Set the pen's width.
            pencil.Width = 8.0F;
            // Set the LineJoin property.
            pencil.LineJoin = System.Drawing.Drawing2D.LineJoin.Bevel;
            paperDocument.DrawRectangle(pencil, 10, 10, imageCam.Width - 10, imageCam.Height - 10);

            labCapturaImgDocFront.BackColor = Color.Transparent;
            labCapturaImgDocBack.BackColor = Color.Transparent;


            try
            {
                Version version = Assembly.GetExecutingAssembly().GetName().Version;
                labVersion.Text = "v" + version.ToString();
                LOG.Debug("BVIUI.BVIUI_Load - Version inicada = " + labVersion.Text);
                //picProgreso.Image = Image.FromFile(@"C:\TFSN\Biometrika Client\V7.5\Auxiliares\images\upload.gif");
                //picProgreso.SizeMode = PictureBoxSizeMode.StretchImage;

                //picGrpVerificandoBackground.Image = Image.FromFile(@"C:\TFSN\Biometrika Client\V7.5\Auxiliares\images\upload.gif");
                //picGrpVerificandoBackground.SizeMode = PictureBoxSizeMode.StretchImage;

                if (_BVI7CONFIG == null)
                {
                    _BVI7CONFIG = SerializeHelper.DeserializeFromFile<BVI7Config>("BVI7.Config.cfg");
                    if (_BVI7CONFIG == null)
                    {
                        _BVI7CONFIG = new BVI7Config();
                        SerializeHelper.SerializeToFile(_BVI7CONFIG, "BVI7.Config.cfg");
                    }
                }

                if (!string.IsNullOrEmpty(_BVI7CONFIG.CustomerLogoPath))
                {
                    LOG.Debug("BVIUI.BVIUI_Load - Cargando CustomerLogoPath = " + _BVI7CONFIG.CustomerLogoPath);
                    picLogo.Image = Image.FromFile(_BVI7CONFIG.CustomerLogoPath);
                    picLogo.Refresh();
                } else
                {
                    LOG.Debug("BVIUI.BVIUI_Load - Ignora carga de logo customer porque CustomerLogoPath es null o está erroneo => " +
                        (string.IsNullOrEmpty(_BVI7CONFIG.CustomerLogoPath)?"NULL": _BVI7CONFIG.CustomerLogoPath));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.BVIUI_Load - Error cargando logo de cliente: " + ex.Message);
            }

            //Setea NVHelper
            //_NVHELPER = new NVHelper(Properties.Settings.Default.ServiceURL, Properties.Settings.Default.ServiceTimeout,
            //                         Properties.Settings.Default.ServiceCompany);
            _NVHELPER = new NVHelper(_BVI7CONFIG.URLServiceBase, _BVI7CONFIG.ServiceTimeout,
                                     _BVI7CONFIG.ServiceCompany);
            if (_NVHELPER != null)
            {
                LOG.Debug("BVIUI.BVIUI_Load - Creado _NVHELPER => Url=" + _BVI7CONFIG.URLServiceBase + " - Company=" +
                            _BVI7CONFIG.ServiceCompany + " - Timeout=" + _BVI7CONFIG.ServiceTimeout.ToString());
                LOG.Debug("BVIUI.BVIUI_Load - Creado _NVHELPER => WS Url=" + _BVI7CONFIG.URLWebService + " - Company=" +
                            _BVI7CONFIG.ServiceCompany + " - Timeout=" + _BVI7CONFIG.ServiceTimeout.ToString());
            }

            LOG.Debug("BVIUI.BVIUI_Load OK!");
        }

        internal void ReadConfig()
        {
            try
            {
                LOG.Debug("BVIUI.ReadConfig IN...");
                if (_BVI7CONFIG == null)
                {
                    LOG.Debug("BVIUI.ReadConfig - Reading from HDD...");
                    _BVI7CONFIG = SerializeHelper.DeserializeFromFile<BVI7Config>("BVI7.Config.cfg");
                    if (_BVI7CONFIG == null)
                    {
                        LOG.Debug("BVIUI.ReadConfig - Creando BVI7.Config.cfg xq no existe...");
                        _BVI7CONFIG = new BVI7Config();
                        if (SerializeHelper.SerializeToFile(_BVI7CONFIG, "BVI7.Config.cfg"))
                        {
                            LOG.Debug("BVIUI.ReadConfig - Creado BVI7.Config.cfg!");
                        } else
                        {
                            LOG.Debug("BVIUI.ReadConfig - Error creando BVI7.Config.cfg!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DRUI.ReadConfig - Error: " + ex.Message);
            }
        }

        internal int Channel(BioPacket bioPacket)
        {
            int ret = 0;
            LOG.Info("BVIUI.Channel In...");
            bioPacketLocal = bioPacket;

            ret = CheckParameters(bioPacketLocal);

            if (ret == 0)
            {
                LOG.Info("BVIUI.Channel Out - Parámetros configurados correctamente");
                if (bioPacketLocal.PacketParamOut == null)
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            }
            LOG.Info("BVIUI.Channel Out [ret=" + ret + "]...");
            return ret;
        }

        /// <summary>
        /// Construimos la salida de la solicitud que será devuelta al usuario final.
        /// </summary>
        /// <returns></returns>
        private Solicitud BuildSolicitud()
        {
            try
            {
                _SOLICITUD = new Solicitud();
                _SOLICITUD.Persona = new Persona();
                LOG.Info("BVIUI.BuildSolicitud - Se construye el objeto persona para " + dictParamIn["VALUEID"] + "...");
                _SOLICITUD.Persona.Rut = dictParamIn["VALUEID"];

                _SOLICITUD.Configuracion = new Configuracion();
                _SOLICITUD.Configuracion.clientType = _BVI7CONFIG.ClientType;
                _SOLICITUD.Configuracion.ignoreStepSelfieInWizard = _BVI7CONFIG.IgnoreStepSelfieInWizard;
                //_SOLICITUD.Configuracion.extraerFirma = Properties.Settings.Default.ExtractSignature;
                //_SOLICITUD.Configuracion.extraerFoto = Properties.Settings.Default.ExtractPhoto;
                _SOLICITUD.Configuracion.bloquearDocumentoVencido = _BVI7CONFIG.BlockWizardWithExpiredDocument;
                _SOLICITUD.Configuracion.bloqueoMenoresDeEdad = _BVI7CONFIG.BlockWizardWithYounger;
                _SOLICITUD.Configuracion.bloqueoCedulaInSCReI = _BVI7CONFIG.BlockWizardWithBlockedDocumentInSRCeI;

                _SOLICITUD.Configuracion.returnSampleImages = _BVI7CONFIG.ReturnSamplesImages;
                _SOLICITUD.Configuracion.returnSampleMinutiae = _BVI7CONFIG.ReturnSamplesMinutiaes;
                _SOLICITUD.Configuracion.userName = _BVI7CONFIG.UserNameDefault;
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.BuildSolicitud - Error al construir la solicitud:" + ex.Message);
                LOG.Error("BVIUI.BuildSolicitud - stacktrace:" + ex.StackTrace);
            }
            LOG.Info("BVIUI.BuildSolicitud - Termine de construir la solicitud");
            return _SOLICITUD;

        }     

        public int CheckParameters(BioPacket bioPacketLocal)
        {
            int ret = 0;
            try
            {
                LOG.Info("BVIUI.CheckParameters In...");
                if (bioPacketLocal == null)
                {
                    ret = -2;  //Lista de parametros no puede ser nulo
                    LOG.Error("BVIUI.CheckParameters - Lista de parametros no puede ser nulo!");
                }

                LOG.Debug("BVIUI.CheckParameters filling dictParamin...");
                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    LOG.Debug("BVIUI.CheckParameters     Adding => " + k + "=" + (string)bioPacketLocal.PacketParamIn[k] + "...");
                    dictParamIn.Add(k.ToUpper(), bioPacketLocal.PacketParamIn[k]);

                }
                LOG.Debug("BBVIUI.CheckParameters dictParamin filled [length = " + dictParamIn.Count + "]");
                if ((!dictParamIn.ContainsKey("SRC") || String.IsNullOrEmpty(dictParamIn["SRC"])))
                {
                    ret = -3;  //src/TypeId/ValufeId deben ser enviados como parámetros
                    LOG.Error("BVIUI.CheckParameters - src debe ser enviado como parámetros!");
                }
                else
                {

                    if (!dictParamIn.ContainsKey("VALUEID"))
                    {
                        dictParamIn.Add("VALUEID", "NA");
                    }
                    LOG.Info("BVIUI.CheckParameters - Seteamos el rut definido => " + dictParamIn["VALUEID"]);
                    _PERSONA = new Persona();

                    LOG.Info("BVIUI.CheckParameters - Definimos si viene el parámetro solicitud");
                    
                    try
                    {

                        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                        if (dictParamIn.ContainsKey("SOLICITUD") && dictParamIn["SOLICITUD"] != null)
                        {
                            _SOLICITUD = jsonSerializer.Deserialize<Solicitud>(dictParamIn["SOLICITUD"]);
                            if (_SOLICITUD != null)
                            {
                                LOG.Info("BVIUI.CheckParameters - La solicitud viene de acuerdo a los parámetros solicitados");
                            }
                            else
                            {
                                LOG.Error("BVIUI.CheckParameters - se asume una verificación por defecto con los mínimos de valores configurados en parámetros");
                                _SOLICITUD = BuildSolicitud();
                            }
                        }
                        else
                        {
                            LOG.Error("BVIUI.CheckParameters - se asume una verificación por defecto con los mínimos de valores configurados en parámetros");
                            _SOLICITUD = BuildSolicitud();
                        }

                    }
                    catch (Exception exe)
                    {
                        LOG.Error("BVIUI.CheckParameters - se asume una verificación por defecto con los mínimos de valores configurados en parámetros", exe);
                        LOG.Info("BVIUI.CheckParameters - Construye la solicitud");
                        _SOLICITUD = BuildSolicitud();
                    }
                    labTitle.Text = "Validando RUT " + _SOLICITUD.Persona.Rut + "...";
                    ret = 0;
                }
                //bioPacketLocal = bioPacketLocal;
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.CheckParameters - Error Biometrika BviCam.CheckParameters [" + ex.Message + "]");
                ret = -1;
            }
            _CLIENT_TYPE = _SOLICITUD.Configuracion.clientType;
            LOG.Info("BVIUI.CheckParameters - CheckParameters Ount [ret=" + ret + "]!");
            return ret;
        }   

        private void CloseControl(bool hasCapture)
        {
            //LOG.Info("CIUIAdapter.CloseControl In...");
            //try
            //{
            //    if (bioPacketLocal == null)
            //    {
            //        LOG.Debug("CIUIAdapter.CloseControl Creando bioPacketLocal...");
            //        bioPacketLocal = new BioPacket();
            //        bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            //    }

            //    if (_CurrentError != 0 || !hasCapture)  //Si hay error o no completo todos los datos
            //    {
            //        bioPacketLocal.PacketParamOut.Add("TrackIdCI", null);
            //        bioPacketLocal.PacketParamOut.Add("CI", null);
            //        bioPacketLocal.PacketParamOut.Add("DeviceId", DeviceId);
            //        bioPacketLocal.PacketParamOut.Add("Error", _CurrentError.ToString() + "|" + _sCurrentError);
            //        LOG.Debug("CIUIAdapter.CloseControl VAlores devueltos => TrackIdCI = null | CI = null | DeviceId = " + DeviceId + " | Error = " + _CurrentError.ToString() + "|" + _sCurrentError);
            //    }
            //    else
            //    {
            //        bioPacketLocal.PacketParamOut.Add("TrackIdCI", TrackIdCI);
            //        bioPacketLocal.PacketParamOut.Add("CI", RDEnvelope);
            //        bioPacketLocal.PacketParamOut.Add("DeviceId", DeviceId);
            //        bioPacketLocal.PacketParamOut.Add("Error", "0|No Error");
            //        LOG.Debug("CIUIAdapter.CloseControl VAlores devueltos => TrackIdCI = " + TrackIdCI + "| CI = " + RDEnvelope + "| DeviceId = " + DeviceId + " |0|No Error");
            //    }
            //    HaveData = true;
            //    this.Close();
            //}
            //catch (Exception ex)
            //{
            //    LOG.Error("CIUIAdapter.CloseControl Error Closing [" + ex.Message + "]");

            //}

            //LOG.Info("CIUIAdapter.CloseControl OUT!");

        }

        private void GetGeoRef()
        {
            try
            {
                //GeoRef = Properties.Settings.Default.CIGeoRef;
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.GetGeoRef Error [" + ex.Message + "]");
            }
        }

        private void GetDeviceId()
        {
            try
            {
                //DeviceId = GetMACAddress();
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.GetDeviceId Error [" + ex.Message + "]");
            }
        }

        public string GetMACAddress()
        {
            string sMacAddress = null;
            try
            {
                LOG.Debug("CIUIAdapter.GetMACAddress IN...");
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                sMacAddress = string.Empty;
                foreach (NetworkInterface adapter in nics)
                {
                    if (sMacAddress == String.Empty)// only return MAC Address from first card
                    {
                        if (adapter.OperationalStatus == OperationalStatus.Up && (!adapter.Description.Contains("Virtual")
                            && !adapter.Description.Contains("Pseudo")))
                        {
                            if (adapter.GetPhysicalAddress().ToString() != "")
                            {
                                IPInterfaceProperties properties = adapter.GetIPProperties();
                                sMacAddress = adapter.GetPhysicalAddress().ToString();
                                LOG.Debug("CIUIAdapter.GetMACAddress MAC Tomada = " + sMacAddress);
                            }
                        }
                    }
                }
                if (sMacAddress == string.Empty)
                {
                    //sMacAddress = Properties.Settings.Default.CIDeviceId;
                    LOG.Debug("CIUIAdapter.GetMACAddress MAC Tomada desde CIDeviceId = " + sMacAddress);
                }
                else
                {
                    string sAux = sMacAddress.Substring(0, 2);
                    int i = 2;
                    while (i < sMacAddress.Length)
                    {
                        sAux = sAux + "-" + sMacAddress.Substring(1, 2);
                        i = i + 2;
                    }
                    sMacAddress = sAux;
                    LOG.Debug("CIUIAdapter.GetMACAddress MAC formateada = " + sAux);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.GetMACAddress Error [" + ex.Message + "]");
            }

            return sMacAddress;
        }

        #endregion General

        #region Operacion

        #region SubRegion Menu Pantalla Principal
        private void labReinit_Click(object sender, EventArgs e)
        {
            ClearComponent();
        }

        private void labClose_Click(object sender, EventArgs e)
        {
            CloseControl();
        }

        private void CloseControl()
        {
            try
            {
                LOG.Info("BVIUI.CloseControl In...");

                //Pongo null las imagenes de muestras 
                //if (_SOLICITUD.Configuracion.returnSampleImages)
                //{

                //}
                BVIResponse response = new BVIResponse(_TRACK_ID_CI, _PERSONA);
                string strResponse = JsonConvert.SerializeObject(response);
                if (bioPacketLocal == null)
                {
                    LOG.Debug("CIUIAdapter.CloseControl Creando bioPacketLocal...");
                    bioPacketLocal = new BioPacket();
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
                }

                //bioPacketLocal.PacketParamOut.Add("TrackIdCI", _TRACK_ID_CI);
                bioPacketLocal.PacketParamOut.Add("BVIResponse", strResponse);
                HaveData = true;
                this.Close();
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.CloseControl Error: " + ex.Message);
            }
            LOG.Info("BVIUI.CloseControl OUT!");
        }

        private void labInicioStart_Click(object sender, EventArgs e)
        {
            SetEstadoWizard(EstadoWizard.EsperandoCapturaCedula);
        }

        private void labInicioSelectClientTypeHuella_Click(object sender, EventArgs e)
        {
            _CLIENT_TYPE = 1; //_SOLICITUD.Configuracion.clientType; // Properties.Settings.Default.ClientType;
            labClientType.Text = "Tecnología: Huella";
            labClientType.Visible = true;
            labClientType.Refresh();
            labInicioStart_Click(null, null);
        }

        private void labInicioSelectClientTypeFacial_Click(object sender, EventArgs e)
        {
            _CLIENT_TYPE = 2; //Properties.Settings.Default.ClientType;
            labClientType.Text = "Tecnología: Facial";
            labClientType.Visible = true;
            labClientType.Refresh();
            labInicioStart_Click(null, null);
        }

        #endregion SubRegion Menu Pantalla Principal

        private void labSupport_Click(object sender, EventArgs e)
        {
            frmHelp frm = new frmHelp();
            frm.Show(this);
        }

        private void labAcercaDe_Click(object sender, EventArgs e)
        {
            frmHelp frm = new frmHelp();
            frm.Show(this);
        }

        private void btnSigGrpLCB_Click(object sender, EventArgs e)
        {
            if (_PERSONA.TipoIdentificacion == TipoIdentificacion.CedulaChilenaAntigua)
            {
                SetEstadoWizard(EstadoWizard.EsperandoHuellaEscaneadaCedulaVieja);
            } else
            {
                SetEstadoWizard(EstadoWizard.EsperandoHuellaEscaneadaCedulaNueva);
            }
            
        }

        /// <summary>
        /// Toma las imagenes capturadas y las envia al server para inicar el proceso de verificacion. 
        /// Retorna la info reconocida, para manejo interno y si hay errores, pide reproceso.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSigCaptureDocument_Click(object sender, EventArgs e)
        {
            Persona oPersona;
            string msgerror;
            string trackidciout;
            try
            {
                StopCamStream();
                picProcess1.Visible = true;
                picProcess1.Refresh();
                labMsgProcess1.Visible = true;
                labMsgProcess1.Refresh();
                _PERSONA.Rut = (_SOLICITUD != null && _SOLICITUD.Persona.Rut != null ? 
                    _SOLICITUD.Persona.Rut.ToString() : null);
                int ret = _NVHELPER.ParseOCRDocument(_CLIENT_TYPE, _PERSONA, _SOLICITUD.Configuracion.userName,
                                                     out oPersona, out msgerror, out trackidciout);
                picProcess1.Visible = false;
                picProcess1.Refresh();
                labMsgProcess1.Visible = false;
                labMsgProcess1.Refresh();

                if (ret < 0)  //Hubo error => Informo y pido intente nuevamente
                {
                    MostrarMensaje(1, Color.Red, msgerror + " Reintente...");
                } else
                {
                    _PERSONA = oPersona;
                    _TRACK_ID_CI = trackidciout;
                    labTitle.Text = "Verificando RUT " + _PERSONA.Rut + "...";
                    labTitle.Visible = true;
                    labTitle.Refresh();
                    labTrackId.Text = "Track Id: " + _TRACK_ID_CI;
                    labTrackId.Visible = true;
                    labTrackId.Refresh();

                    //Lleno labs de otros groups
                    labVerifyNombre.Text = _PERSONA.Nombre + " " + _PERSONA.Apellido;
                    labVerifyFNac.Text = _PERSONA.FechaNacimiento.HasValue ? _PERSONA.FechaNacimiento.Value.ToString("dd/MM/yyyy"):"";
                    labVerifyFVenc.Text = _PERSONA.FechaExpiracion.HasValue ? _PERSONA.FechaExpiracion.Value.ToString("dd/MM/yyyy") : "";
                    labVerifySerial.Text = _PERSONA.Serie;
                    labVerifySex.Text = _PERSONA.Sexo;
                    labVerifyNacionality.Text = _PERSONA.Nacionalidad;


                    //StopCamStream();
                    SetEstadoWizard(EstadoWizard.EsperandoSelfie);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.btnSigCaptureDocument_Click - Error: " + ex.Message);
            }
        }

        private void labSigCaptureDocument_Click(object sender, EventArgs e)
        {
            Persona oPersona;
            string msgerror;
            string trackidciout;
            try
            {
                //StopCamStream();
                picDocStop_Click(null, null);

                SetProgress(true);

                picProcess1.Visible = true;
                picProcess1.Refresh();
                labMsgProcess1.Visible = true;
                labMsgProcess1.Refresh();
                MostrarMensaje(1, Color.DarkGray, "Procesando imágenes. Por favor espere...");
                _PERSONA.Rut = (_SOLICITUD != null && _SOLICITUD.Persona.Rut != null ?
                   _SOLICITUD.Persona.Rut.ToString() : null);
                int ret = _NVHELPER.ParseOCRDocument(_CLIENT_TYPE, _PERSONA, _SOLICITUD.Configuracion.userName, 
                                                     out oPersona, out msgerror, out trackidciout);
                picProcess1.Visible = false;
                picProcess1.Refresh();
                labMsgProcess1.Visible = false;
                labMsgProcess1.Refresh();
   
                if (ret < 0)  //Hubo error => Informo y pido intente nuevamente
                {
                    if (ret == -421 || ret == -411)
                    {
                        MostrarMensaje(1, Color.Red, msgerror + ". Reintente...");
                            //"Número de documento reconocido [" + 
                            //            (oPersona != null && oPersona.Rut != null ? oPersona.Rut : "Nulo") + "]" +
                            //" no coincide con el pedido para verificar [" + _PERSONA.Rut + "]. Reintente...");
                    }
                    else
                    {
                        MostrarMensaje(1, Color.Red, msgerror + "Capture nuevas imágenes. Verifique la luz. Reintente...");
                    }
                }
                else 
                {
                    _PERSONA = oPersona;
                    _TRACK_ID_CI = trackidciout;
                    //bool isVigente = _PERSONA.FechaExpiracion.HasValue ? Utils.IsCedulaVigente(_PERSONA.FechaExpiracion.Value) : false;
                    //bool isMenor = _PERSONA.FechaNacimiento.HasValue ? Utils.IsMenorDe18(_PERSONA.FechaNacimiento.Value) : false;
                    if (_SOLICITUD.Configuracion.bloquearDocumentoVencido && !_PERSONA.IsCedulaVigente)
                    {
                        picCapDocStatusBlock.Image = Properties.Resources.ObsCedVencida;
                        labCapDocStatusBlock.Text = "Cédula Vencida";
                        grpCapDocStatusBlock.Visible = true;
                        grpCapDocStatusBlock.Refresh();
                    }
                    else if (_SOLICITUD.Configuracion.bloquearDocumentoVencido && _PERSONA.IsMenorDeEdad)
                    {
                        picCapDocStatusBlock.Image = Properties.Resources.IconoMenorDeEdad;
                        labCapDocStatusBlock.Text = "Menor de Edad";
                        grpCapDocStatusBlock.Visible = true;
                        grpCapDocStatusBlock.Refresh();
                    }
                    else
                    {
                        labTitle.Text = "Verificando RUT " + _PERSONA.Rut + "...";
                        labTitle.Visible = true;
                        labTitle.Refresh();
                        labTrackId.Text = "Track Id: " + _TRACK_ID_CI;
                        labTrackId.Visible = true;
                        labTrackId.Refresh();

                        //Lleno Subtitle
                        labSubTitle.Text = _PERSONA.Nombre + " " + _PERSONA.Apellido;
                        labSubTitle.Refresh();

                        //Lleno labs de otros groups
                        labVerifyNombre.Text = _PERSONA.Nombre + " " + _PERSONA.Apellido;
                        labVerifyFNac.Text = _PERSONA.FechaNacimiento.HasValue ? _PERSONA.FechaNacimiento.Value.ToString("dd/MM/yyyy") : "";
                        if (_PERSONA.IsMenorDeEdad)
                            labVerifyFNac.ForeColor = Color.Red;
                        labVerifyFVenc.Text = _PERSONA.FechaExpiracion.HasValue ? _PERSONA.FechaExpiracion.Value.ToString("dd/MM/yyyy") : "";
                        if (!_PERSONA.IsCedulaVigente)
                            labVerifyFVenc.ForeColor = Color.Red;
                        labVerifySerial.Text = _PERSONA.Serie;
                        labVerifySex.Text = _PERSONA.Sexo;
                        labVerifyNacionality.Text = _PERSONA.Nacionalidad;

                        //Lleno labs de otros groups
                        labLCBNombre.Text = _PERSONA.Nombre + " " + _PERSONA.Apellido;
                        labLCBFNac.Text = _PERSONA.FechaNacimiento.HasValue ? _PERSONA.FechaNacimiento.Value.ToString("dd/MM/yyyy") : "";
                        if (_PERSONA.IsMenorDeEdad)
                            labLCBFNac.ForeColor = Color.Red;
                        labLCBFVen.Text = _PERSONA.FechaExpiracion.HasValue ? _PERSONA.FechaExpiracion.Value.ToString("dd/MM/yyyy") : "";
                        if (!_PERSONA.IsCedulaVigente)
                            labLCBFVen.ForeColor = Color.Red;
                        labLCBSerial.Text = _PERSONA.Serie;
                        labLCBSex.Text = _PERSONA.Sexo;
                        labLCBNacionality.Text = _PERSONA.Nacionalidad;
                        SetEstadoWizard(EstadoWizard.EsperandoSelfie);
                        FillResumenGroup();
                    }
                    //StopCamStream();
                }
                SetProgress(false);
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.btnSigCaptureDocument_Click - Error: " + ex.Message);
            }
        }

        private void SetProgress(bool enable)
        {
            try
            {
                //grpProgress.Location = new Point(650, 300);
                //grpProgress.BringToFront();
                //grpProgress.Visible = enable;
                //grpProgress.Refresh();
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.SetProgress Error: " + ex.Message);
            }
        }

        private void labShowResumen_Click(object sender, EventArgs e)
        {
            grpResumen.Text = "";
            grpResumen.Location = new Point(379, 130);
            grpResumen.BringToFront();
            grpResumen.Visible = true;
            grpResumen.Refresh();
        }

        private void FillResumenGroup()
        {
            byte[] byImage;
            System.IO.MemoryStream ms;
            try
            {
                if (_PERSONA != null)
                {
                    //Imagenes documentos
                    if (!string.IsNullOrEmpty(_PERSONA.FotoFrontCedula))
                    {
                        byImage = Convert.FromBase64String(_PERSONA.FotoFrontCedula);
                        ms = new MemoryStream(byImage);
                        picResumenDocFront.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumenDocFront.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(_PERSONA.FotoBackCedula))
                    {
                        byImage = Convert.FromBase64String(_PERSONA.FotoBackCedula);
                        ms = new MemoryStream(byImage);
                        picResumenDocBack.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumenDocBack.Visible = true;
                    }

                    if (!string.IsNullOrEmpty(_PERSONA.FotoFirma))
                    {
                        byImage = Convert.FromBase64String(_PERSONA.FotoFirma);
                        ms = new MemoryStream(byImage);
                        picResumenFirma.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumenFirma.Visible = true;
                    }

                    //Imagenes muestras
                    if (!string.IsNullOrEmpty(_PERSONA.JpegOriginal))
                    {
                        byImage = Convert.FromBase64String(_PERSONA.JpegOriginal);
                        ms = new MemoryStream(byImage);
                        picResumenFingerSample.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumenFingerSample.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(_PERSONA.Selfie))
                    {
                        byImage = Convert.FromBase64String(_PERSONA.Selfie);
                        ms = new MemoryStream(byImage);
                        picResumenSelfie.Image = Image.FromStream(ms);
                        ms.Close();
                        picResumenSelfie.Visible = true;
                    }

                    //Data 
                    labResumenName.Text = _PERSONA.Nombre + " " + _PERSONA.Apellido;
                    labResumenFNac.Text = _PERSONA.FechaNacimiento.HasValue ? _PERSONA.FechaNacimiento.Value.ToString("dd/MM/yyyy") : "";
                    if (_PERSONA.IsMenorDeEdad)
                    {
                        labResumenFNac.ForeColor = Color.Red;
                        picResumenIconoMenorDeEdad.Visible = true;
                    }
                    labResumenFV.Text = _PERSONA.FechaExpiracion.HasValue ? _PERSONA.FechaExpiracion.Value.ToString("dd/MM/yyyy") : "";
                    if (!_PERSONA.IsCedulaVigente)
                    {
                        labResumenFV.ForeColor = Color.Red;
                        picResumenIconoCedVencida.Visible = true;
                    }
                    labResumenSerial.Text = _PERSONA.Serie;
                    labResumenSex.Text = _PERSONA.Sexo;
                    labResumenNacionality.Text = _PERSONA.Nacionalidad;

                    //Resultado Verify
                    if (_PERSONA.verifyResult > 0)
                    {
                        if (_PERSONA.verifyResult == 1)
                        {
                            picResumenResultVerify.Image = Properties.Resources.VerifyOK;
                            picResumenResultVerify.Visible = true;
                            MarkRangoResumenScore(true, _PERSONA.score);
                            labResumenScore.Text = GetScorePercent(_PERSONA.score);
                            labResumenScore.Refresh();
                        } else
                        {
                            picResumenResultVerify.Image = Properties.Resources.VerifyNOOK;
                            picResumenResultVerify.Visible = true;
                            MarkRangoResumenScore(false, 0);
                            labResumenScore.Text = "0%";
                            labResumenScore.Refresh();
                        }
                        
                    } else
                    {
                        picResumenResultVerify.Image = Properties.Resources.VerifyPending;
                        picResumenResultVerify.Visible = true;
                    }
                }
                grpResumen.Refresh();
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.FillResumenGroup - Error: " + ex.Message);
            }
        }

        private string GetScorePercent(double score)
        {
            string sRet = "0%";
            try
            {
                double dScore = 0;
                if (_CLIENT_TYPE == 1 && _PERSONA.TipoIdentificacion == TipoIdentificacion.CedulaChilenaAntigua)
                {
                    dScore = GetPorcentajeNEC(score, _BVI7CONFIG.NECThreshold);
                } else
                {
                    dScore = score;
                }
                sRet = dScore.ToString("##.##") + "%";
            }
            catch (Exception ex)
            {
                sRet = "0%";
                LOG.Error("BVIUI.GetScorePercent Error: " + ex.Message);
            }
            return sRet;
        }

        private void MarkRangoResumenScore(bool visible, double score)
        {
            try
            {
                if (!visible)
                {
                    labResumenRango1.Visible = false;
                    labResumenRango2.Visible = false;
                    labResumenRango3.Visible = false;
                    labResumenRango4.Visible = false;
                    labResumenRango5.Visible = false;
                } else
                {
                    if (score < 20)
                        labResumenRango1.Visible = true;
                    else if (score < 40)
                        labResumenRango2.Visible = true;
                    else if (score < 60)
                        labResumenRango3.Visible = true;
                    else if (score < 80)
                        labResumenRango4.Visible = true;
                    else
                        labResumenRango5.Visible = true;

                }
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.MarkRangoResumenScore - Error: " + ex.Message);
            }
        }

        private void btnSigTabHuella_Click(object sender, EventArgs e)
        {
            try
            {
                //if (_PERSONA.TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
                //{
                    SetEstadoWizard(EstadoWizard.EsperandoLecturaContactless);
                //}
                //else
                //{
                //    SetEstadoWizard(EstadoWizard.Verificando);
                //}
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.btnSigTabHuella_Click - Error: " + ex.Message);
            }
        }

        private void btnReintentarCapturaHuella_Click(object sender, EventArgs e)
        {
            MostrarMensaje(1, Color.Gray, "");
            _PERSONA.Wsq = null;
            _PERSONA.Ansi = null;
            _PERSONA.Iso = null;
            _PERSONA.IsoCompact = null;
            _PERSONA.JpegOriginal = null;
            _PERSONA.Raw = null;
            btnReintentarCapturaHuella.Visible = false;
            btnReintentarCapturaHuella.Refresh();
            CaptureFinger();
            //btnReintentarCapturaHuella.Visible = true;
        }

        private void labSigLCB_Click(object sender, EventArgs e)
        {
            if (_PERSONA.TipoIdentificacion == TipoIdentificacion.CedulaChilenaAntigua)
            {
                SetEstadoWizard(EstadoWizard.EsperandoHuellaEscaneadaCedulaVieja);
            }
            else
            {
                SetEstadoWizard(EstadoWizard.EsperandoHuellaEscaneadaCedulaNueva);
            }
        }

        private void labSigCaptureHuella_Click(object sender, EventArgs e)
        {
            try
            {
                if (_PERSONA.TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
                {
                    SetEstadoWizard(EstadoWizard.EsperandoLecturaContactless);
                }
                else
                {
                    SetEstadoWizard(EstadoWizard.Verificando);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.btnSigTabHuella_Click - Error: " + ex.Message);
            }
        }

        private void labFinalSalir_Click(object sender, EventArgs e)
        {
            CloseControl();
        }

        private void btnSigAcercaNFC_Click(object sender, EventArgs e)
        {
            try
            {
                SetEstadoWizard(EstadoWizard.Verificando);
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.btnSigTabHuella_Click - Error: " + ex.Message);
            }
        }

        private void labSigExport_Click(object sender, EventArgs e)
        {
            try
            {
                SetEstadoWizard(EstadoWizard.Final);
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.labSigExport_Click - Error: " + ex.Message);
            }
        }

        private void labSigAcercarNFC_Click(object sender, EventArgs e)
        {
            try
            {
                SetEstadoWizard(EstadoWizard.Verificando);
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.labSigAcercarNFC_Click - Error: " + ex.Message);
            }
        }

        private void btnVerify_Click(object sender, EventArgs e)
        {
            //labResultVerify.Visible = false;
            //labResultVerify.Refresh();
            picGrpVerificandoBackground.Image = Properties.Resources.VerifyDoing;
            picGrpVerificandoBackground.Refresh();
            btnVerify.Visible = false;
            picResultVerify.Visible = false;
            picResultVerify.Refresh();
            Verify();
        }

        private void labVerificandoSalir_Click(object sender, EventArgs e)
        {
            CloseControl();
        }

        private void labVerificandoSiguiente_Click(object sender, EventArgs e)
        {
            try
            {
                //if (_PERSONA.verifyResult == 1)
                //{
                    SetEstadoWizard(EstadoWizard.EsperandoExportar);
                //} else
                //{
                //    SetEstadoWizard(EstadoWizard.);
                //}
                FillResumenGroup();
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.labVerificandoSiguiente_Click - Error: " + ex.Message);
            }
        }

        private void txtMailToSendPDF_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtMailToSendPDF.Text) && Utils.Utils.isFormatMailCorrect(txtMailToSendPDF.Text))
            {
                labExportarSend.Enabled = true;
            }
            else
            {
                labExportarSend.Enabled = false;
            }
        }

        private void txtMailToSendPDF_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtMailToSendPDF.Text) && Utils.Utils.isFormatMailCorrect(txtMailToSendPDF.Text))
            {
                labExportarSend.Enabled = true;
            }
            else
            {
                labExportarSend.Enabled = false;
            }
        }

        private void labExportarSend_Click(object sender, EventArgs e)
        {
            try
            {
                labResultSendMailPDF.Text = "Enviando mail...";
                labResultSendMailPDF.Visible = true;
                labResultSendMailPDF.Refresh();
                string msgerr;
                int ret = NVHelper.ExportPDFbyMail(_TRACK_ID_CI, txtMailToSendPDF.Text, out msgerr);

                if (ret == 0)
                {
                    labResultSendMailPDF.Text = "PDF enviado con éxito a " + txtMailToSendPDF.Text + "!";
                } else
                {
                    labResultSendMailPDF.Text = "Error enviando PDF a " + txtMailToSendPDF.Text + " [" + msgerr + "]. Reintente...";
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.labExportarSend_Click Error: " + ex.Message);
            }
        }

        private void labExportarDownloadPDF_Click(object sender, EventArgs e)
        {
            string msgerr = "";
            byte[] byPDF;
            string path = @"c:\";
            int ret = 0;
            try
            {
                if (!string.IsNullOrEmpty(_PERSONA.CertifyPdf))
                {
                    byPDF = Convert.FromBase64String(_PERSONA.CertifyPdf);
                }
                else
                {
                    ret = NVHelper.DownloadCIPDF(_TRACK_ID_CI, out byPDF, out msgerr);
                }

                if (!string.IsNullOrEmpty(_BVI7CONFIG.DownloadPath))
                {
                    path = _BVI7CONFIG.DownloadPath;
                    if (!path.EndsWith("\\"))
                    {
                        path = path + "\\";
                    }
                }

                if (ret == 0 && byPDF!=null)
                {
                    LOG.Error("BVIUI.labExportarDownloadPDF_Click - PDF salvado en " + path + _TRACK_ID_CI + ".pdf...");
                    System.IO.File.WriteAllBytes(path + _TRACK_ID_CI + ".pdf", byPDF);
                    labResultSendMailPDF.Text = "PDF salvado en " + path + _TRACK_ID_CI + ".pdf";
                    //TODO => Descargar
                }
                else
                {
                    labResultSendMailPDF.Text = "Error Descargando PDF [" + msgerr + "]. Reintente...";
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.labExportarDownloadPDF_Click Error: " + ex.Message);
            }
        }

        private void ClearComponent()
        {
            _HAY_PDF417 = false;
            timer_SetGetHuellaCedulaVieja.Enabled = true;

            //Limpia Resumen
            picResumenDocFront.Image = Properties.Resources.NoImage_DocFront;
            //picResumenDocFront.Visible = false;
            picResumenDocBack.Image = Properties.Resources.NoImage_DocBack;
            //picResumenDocBack.Visible = false;
            picResumenFingerSample.Image = Properties.Resources.NoImage_Finger;
            //picResumenFingerSample.Visible = false;
            picResumenFirma.Image = Properties.Resources.NoImage_Firma;
            //picResumenFirma.Visible = false;
            picResumenSelfie.Image = Properties.Resources.NoImage_Selfie;
            //picResumenSelfie.Visible = false;
            labResumenName.Text = "";
            labResumenFNac.Text = "";
            labResumenFNac.ForeColor = Color.DimGray;
            picResumenIconoMenorDeEdad.Visible = false;
            labResumenFV.Text = "";
            labResumenFV.ForeColor = Color.DimGray;
            picResumenIconoCedVencida.Visible = false;
            labResumenSerial.Text = "";
            labResumenSex.Text = "";
            labResumenNacionality.Text = "";
            //picResumenResultVerify.Visible = false;
            picResumenResultVerify.Image = Properties.Resources.VerifyPending;
            labResumenRango1.Visible = false;
            labResumenRango2.Visible = false;
            labResumenRango3.Visible = false;
            labResumenRango4.Visible = false;
            labResumenRango5.Visible = false;
            labResumenScore.Text = "0%";
            grpResumen.Refresh();

            //Stop Camera
            StopCamStream();

            //SubTitle
            labSubTitle.Text = "";
            labSubTitle.Refresh();

            //Oculto groups
            grpAcercaNFC.Visible = false;
            grpCapturaImagenDocumento.Visible = false;
            grpCapturaSelfie.Visible = false;
            grpCaptureHuella.Visible = false;
            grpEscaneoCBWithLCB.Visible = false;
            grpReadCBWithWebCam.Visible = false;
            grpVerificactionResult.Visible = false;

            //Clear componentes internos de Groups
            imageCam.Image = null;
            picDocumentFront.Image = null;
            picDocumentBack.Image = null;

            imageCamSelfie.Image = null;
            picSelfie.Image = null;
            picImageGrpStream.Image = null;
            labSigCaptureSelfie.Visible = false;
            picSelfieStatus.Image = Properties.Resources.SelfieStatus;

            picProcess1.Visible = false;
            picProcess2.Visible = false;
            picProgresVerify.Visible = false;
            picResultVerify.Visible = false;

            picDocFrontStatus.Image = Properties.Resources.CedulaFrente;
            picDocBackStatus.Image = Properties.Resources.CedulaDorso;

            labVerifyNombre.Text = "";
            labVerifyFNac.Text = "";
            labVerifyFNac.ForeColor = Color.Black;
            labVerifyFVenc.Text = "";
            labVerifyFVenc.ForeColor = Color.Black;
            labVerifySerial.Text = "";
            labVerifySex.Text = "";
            labVerifyNacionality.Text = "";

            labLCBFVen.ForeColor = Color.Black;
            labLCBFNac.ForeColor = Color.Black;

            grpCapDocStatusBlock.Visible = false;

            picSample.Image = null; //Properties.Resources.FondoBPC_Boton_plan_griso;
            picSample.Visible = false;

            labResultSendMailPDF.Visible = false;

            //Oculto guia de puntos
            picPaso1.Visible = false;
            picPaso2.Visible = false;
            picPaso3.Visible = false;
            picPaso4.Visible = false;
            picPaso5.Visible = false;
            picPaso6.Visible = false;
            picPaso7.Visible = false;

            //Free variables de operacion
            _PERSONA = new Persona();

            //Clear msgs
            txtMessage.ForeColor = Color.Gray;
            txtMessage.Text = "Inicie operación y siga las instrucciones paso a paso...";

            _CLIENT_TYPE = _SOLICITUD.Configuracion.clientType;
            if (_CLIENT_TYPE != 0) // || !Properties.Settings.Default.ClientTypeSelectionEnabled)
            {
                /*_CLIENT_TYPE = _SOLICITUD.Configuracion.clientType;*/ // Properties.Settings.Default.ClientType;
                labInicioStart.Visible = true;
                labInicioStart.Refresh();
                labClientType.Text = (_CLIENT_TYPE==1 ? "Tecnología: Huella" : "Tecnología: Facial");
                labClientType.Visible = true;
                labClientType.Refresh();
            }
            else //Sino se muestra para seleccionar en pantalla inicial
            {
                labInicioStart.Visible = false;
                labInicioStart.Refresh();
                grpInicioSelectClientType.Location = new Point(395, 143);
                grpInicioSelectClientType.Text = "";
                grpInicioSelectClientType.Visible = true;
                grpInicioSelectClientType.Refresh();
            }

            picLCBObservaciones.Visible = false;
        }

        private void picResumenSalir_Click(object sender, EventArgs e)
        {
            grpResumen.Visible = false;
            grpResumen.Refresh();
        }

        /// <summary>
        /// Sirve apra salir del evento de la lectura de CB desde ProcesarCedulaLeida e iniciar captura huella
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_EnableSigLCB_Tick(object sender, EventArgs e)
        {
            try
            {
                timer_EnableSigLCB.Enabled = false;
                picLCBObservaciones.Image = Properties.Resources.VerifyOK;
                picLCBObservaciones.Visible = true;
                picLCBObservaciones.Refresh();
                labLCBStatus.Text = "Lectura Correcta de Código de Barras";
                labLCBStatus.ForeColor = Color.DarkGreen;
                labLCBStatus.Visible = true;
                labLCBStatus.Refresh();
                labSigLCB.Visible = true;
                labSigLCB.Refresh();
                _LECTOR_CODIGO_DE_BARRAS.DataCaptured -= ProcesarCedulaLeida;
            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }
        }

        private void timer_SetGetHuellaCedulaVieja_Tick(object sender, EventArgs e)
        {
            if (_HAY_PDF417)
            {
                timer_SetGetHuellaCedulaVieja.Enabled = false;
                SetEstadoWizard(EstadoWizard.EsperandoHuellaEscaneadaCedulaVieja);
            }
        }

        /// <summary>
        /// Setea lo inicial de acuerdo a parametros y configuraciones
        /// </summary>
        /// <param name="obj"></param>
        public void InitAdapter(object obj)
        {
            ClearComponent();
            //Me fijo si es con webcam o con LCB para setear la captura, y tipo de verificacion (Huella / Facial)
            //  ClientType = 0-Huella con Lector CB | 1-Huella con Webcam | 2-Facial con 2D    
            switch (_BVI7CONFIG.ClientType)
            {
                case 1: //Huella con Webcam

                    //grpReadCBWithWebCam.Visible = true;
                    break;
                case 2: //Facial con 2D

                    //grpReadCBWithWebCam.Visible = true;
                    break;
                default: //Asumo default 0-Huella con Lector CB
                    //grpReadCBWithWebCam.Visible = false;

                    //Si no se puede seleccionar el tipo de verificacion, se coloca el seteado en el config
                    if (!_BVI7CONFIG.ClientTypeSelectionEnabled || _CLIENT_TYPE > 0)
                    {
                        //_CLIENT_TYPE = Properties.Settings.Default.ClientType;
                        if (_BVI7CONFIG.WizardAutomatic)
                        {
                            //btnStart_Click(null, null);
                            labInicioStart_Click(null, null);
                        }
                    }
                    else //Sino se muestra para seleccionar en pantalla inicial
                    {
                        labInicioStart.Visible = false;
                        labInicioStart.Refresh();
                        grpInicioSelectClientType.Location = new Point(395, 143);
                        grpInicioSelectClientType.Text = "";
                        grpInicioSelectClientType.Visible = true;
                        grpInicioSelectClientType.Refresh();
                    }

                    
                    LOG.Info("BVIUI.InitAdapter - El componente se inicializa usando COM para lectura de codigo de barras con lector de CB");
                    //InitBCR();
                    break;
            }
        }

        
        /// <summary>
        /// Setea paso a paso flujo de operacion
        /// </summary>
        /// <param name="estadoCurrent"></param>
        internal void SetEstadoWizard(EstadoWizard estadoCurrent)
        {
            MostrarMensaje(1, Color.DarkGray, "");
            _ESTADO_WIZARD = estadoCurrent;
            /*
                public enum EstadoWizard
                {
                    Inicio,
                    EsperandoCapturaCedula,
                    EsperandoSelfie,
                    EsperandoLecturaCodigoBarras,
                    EsperandoHuellaEscaneadaCedulaVieja,
                    EsperandoHuellaEscaneadaCedulaNueva,
                    EsperandoLecturaContactless,
                    VerificacionPositiva,
                    VerificacionNegativa,
                    EsperandoExportar,
                    Final
                }
            */
            if (estadoCurrent == EstadoWizard.Inicio)
            {
                ClearComponent();
            }

            if (estadoCurrent == EstadoWizard.EsperandoCapturaCedula)
            {
                grpInicioSelectClientType.Visible = false;
                grpInicioSelectClientType.Refresh();
                labInicioStart.Visible = false;
                labInicioStart.Refresh();

                picPaso1.Image = Properties.Resources.CirculoAzul_small;
                picPaso1.Visible = true;
                picPaso1.Refresh();
                grpCapturaImagenDocumento.Location = new Point(395, 143);
                grpCapturaImagenDocumento.Text = "";
                grpCapturaImagenDocumento.Visible = true;
                grpCapturaImagenDocumento.BringToFront();
                grpCapturaImagenDocumento.Refresh();

                if (_BVI7CONFIG.AutomaticStartInCaptureDocument)
                {
                    picDocStart_Click(null, null);
                }
            }

            if (estadoCurrent == EstadoWizard.EsperandoSelfie)
            {
                picPaso1.Image = Properties.Resources.CirculoVerde_small;
                picPaso1.Visible = true;
                picPaso1.Refresh();

                picPaso2.Image = Properties.Resources.CirculoAzul_small;
                picPaso2.Visible = true;
                picPaso2.Refresh();
                grpCapturaSelfie.Location = new Point(395, 143);
                grpCapturaSelfie.Text = "";

                grpCapturaImagenDocumento.Visible = false;
                if (!_SOLICITUD.Configuracion.ignoreStepSelfieInWizard)
                {
                    grpCapturaSelfie.Visible = true;
                    grpCapturaSelfie.BringToFront();
                    grpCapturaSelfie.Refresh();

                    if (_BVI7CONFIG.AutomaticStartInCaptureSelfie)
                    {
                        picCapturaSelfieStart_Click(null, null);
                    }

                } else
                {
                    _PERSONA.Selfie = _PERSONA.Foto;
                    FillResumenGroup();
                    SetEstadoWizard(EstadoWizard.EsperandoLecturaCodigoBarras);
                }
            }

            if (estadoCurrent == EstadoWizard.EsperandoLecturaCodigoBarras)
            {
                picPaso2.Image = Properties.Resources.CirculoVerde_small;
                picPaso2.Visible = true;
                picPaso2.Refresh();

                picPaso3.Image = Properties.Resources.CirculoAzul_small;
                picPaso3.Visible = true;
                picPaso3.Refresh();
                grpEscaneoCBWithLCB.Location = new Point(395,143);
                grpEscaneoCBWithLCB.Text = "";
                //grpEscaneoCBWithLCB.Visible = true;
                //grpEscaneoCBWithLCB.Refresh();
                grpCapturaSelfie.Visible = false;

                //Si es con huella de cualqueir tipo, y es cedula nueva => pasa directo porque
                //con el parse del server tengo los datos, no necesito leer el QR
                //if (_PERSONA.TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva &&
                if (_CLIENT_TYPE == 0 || _CLIENT_TYPE == 1) //Cambio ahora en ambos se pide la huella porque el PDF417 ya se obtuvo en server
                {
                      SetEstadoCaptureHuella(estadoCurrent);
                } else if (_CLIENT_TYPE == 2)
                {
                    //Si es Facial 2D paso directo a Verificacion no captura huella ni LCB ni NFC
                    picPaso3.Image = Properties.Resources.CirculoVerde_small;
                    picPaso3.Visible = true;
                    picPaso3.Refresh();

                    picPaso4.Image = Properties.Resources.CirculoVerde_small;
                    picPaso4.Visible = true;
                    picPaso4.Refresh();

                    SetEstadoWizard(EstadoWizard.Verificando);
                } else 
                {
                    switch (_CLIENT_TYPE)
                    {
                        case 1: //Huella con Webcam
                            LOG.Info("BVIUI.InitAdapter - EstadoWizard.EsperandoLecturaCodigoBarras => " +
                                "El componente se inicializa usando WebCam para lectura de codigo de barras");
                            
                            grpReadCBWithWebCam.Location = new Point(395, 143);
                            grpReadCBWithWebCam.Text = "";
                            grpReadCBWithWebCam.Visible = true;
                            grpReadCBWithWebCam.BringToFront();
                            grpReadCBWithWebCam.Refresh();
                            InitCamera();
                            break;
                        case 2: //Facial con 2D
                            LOG.Info("BVIUI.InitAdapter - EstadoWizard.EsperandoLecturaCodigoBarras => " +
                                "Pasa directo porque es facial...");
                            //TODO
                            //grpReadCBWithWebCam.Visible = true;
                            break;
                        default: //Asumo default 0-Huella con Lector CB
                            LOG.Info("BVIUI.InitAdapter - EstadoWizard.EsperandoLecturaCodigoBarras => " + 
                                "El componente se inicializa usando COM para lectura de codigo de barras con lector de CB");
                            grpEscaneoCBWithLCB.Visible = true;
                            grpReadCBWithWebCam.BringToFront();
                            grpEscaneoCBWithLCB.Refresh();
                            InitBCR();
                            //InitBCR();
                            break;
                    }
                }

                //labGuiaOperacion.TextAlign = ContentAlignment.MiddleCenter;
                //Show images y msg de ayuda
            }

            if (estadoCurrent == EstadoWizard.EsperandoHuellaEscaneadaCedulaVieja ||
                estadoCurrent == EstadoWizard.EsperandoHuellaEscaneadaCedulaNueva)
            {
                SetEstadoCaptureHuella(estadoCurrent);
            }

            if (estadoCurrent == EstadoWizard.EsperandoLecturaContactless)
            {
                picPaso4.Image = Properties.Resources.CirculoVerde_small;
                picPaso4.Visible = true;
                picPaso4.Refresh();

                picPaso5.Image = Properties.Resources.CirculoAzul_small;
                picPaso5.Visible = true;
                picPaso5.Refresh();

                grpCaptureHuella.Visible = false;
                grpCaptureHuella.Refresh();

                grpAcercaNFC.Location = new Point(395, 143);
                grpAcercaNFC.Text = "";
                grpAcercaNFC.Visible = true;
                grpAcercaNFC.BringToFront();
                grpAcercaNFC.Refresh();

                btnSigAcercaNFC.Enabled = true;
                if (_PERSONA.TipoIdentificacion == TipoIdentificacion.CedulaChilenaAntigua)
                {
                    btnSigAcercaNFC_Click(null, null);
                } 

            }

            if (estadoCurrent == EstadoWizard.Verificando)
            {
                picPaso4.Image = Properties.Resources.CirculoVerde_small;
                picPaso4.Visible = true;
                picPaso4.Refresh();

                picPaso5.Image = Properties.Resources.CirculoVerde_small;
                picPaso5.Visible = true;
                picPaso5.Refresh();

                picPaso6.Image = Properties.Resources.CirculoAzul_small;
                picPaso6.Visible = true;
                picPaso6.Refresh();

                grpCapturaSelfie.Visible = false;
                grpCapturaSelfie.Refresh();

                grpCaptureHuella.Visible = false;
                grpCaptureHuella.Refresh();

                grpAcercaNFC.Visible = false;
                grpAcercaNFC.Refresh();

                grpVerificactionResult.Location = new Point(395, 143);
                grpVerificactionResult.Text = "";
                grpVerificactionResult.Visible = true;
                grpVerificactionResult.BringToFront();
                grpVerificactionResult.Refresh();
             
                Verify();
            }

            if (estadoCurrent == EstadoWizard.EsperandoExportar)
            {
                picPaso6.Image = Properties.Resources.CirculoVerde_small;
                picPaso6.Visible = true;
                picPaso6.Refresh();

                picPaso7.Image = Properties.Resources.CirculoAzul_small;
                picPaso7.Visible = true;
                picPaso7.Refresh();

                grpVerificactionResult.Visible = false;
                grpVerificactionResult.Refresh();

                if (_PERSONA.verifyResult == 1)
                {
                    grpExportar.Location = new Point(395, 143);
                    grpExportar.Text = "";
                    grpExportar.Visible = true;
                    grpExportar.BringToFront();
                    grpExportar.Refresh();
                } else
                {
                    SetEstadoWizard(EstadoWizard.Final);
                }
            }

            if (estadoCurrent == EstadoWizard.Final)
            {
                picPaso7.Image = Properties.Resources.CirculoVerde_small;
                picPaso7.Visible = true;
                picPaso7.Refresh();

                picFinal.Image = Properties.Resources.CirculoVerde_big;
                picFinal.Visible = true;
                picFinal.Refresh();

                grpExportar.Visible = false;
                grpExportar.Refresh();

                grpFinal.Location = new Point(395, 143);
                grpFinal.Text = "";
                grpFinal.Visible = true;
                grpFinal.Refresh();
            }

        }

        

        private void SetEstadoCaptureHuella(EstadoWizard estadoCurrent)
        {
            picPaso3.Image = Properties.Resources.CirculoVerde_small;
            picPaso3.Visible = true;
            picPaso3.Refresh();

            picPaso4.Image = Properties.Resources.CirculoAzul_small;
            picPaso4.Visible = true;
            picPaso4.Refresh();
            //labGuiaOperacion.TextAlign = ContentAlignment.TopCenter;
            //labGuiaOperacion.Text = "Capturar huella dactilar...";

            //picGuiaOperacion.Image = Properties.Resources.ReadFinger_50_75;
            //picGuiaOperacion.Refresh();
            //labGuiaOperacion.Refresh();

            grpEscaneoCBWithLCB.Visible = false;
            grpCaptureHuella.Visible = true;
            if (_PERSONA.TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
            {
                picGuiaOperacionHuella.Image = Properties.Resources.DedosIndices;
                picGuiaOperacionHuella.Visible = true;
                picGuiaOperacionHuella.Refresh();
            }
            else
            {
                picGuiaOperacionHuella.Image = Properties.Resources.DedoPulgar;
                picGuiaOperacionHuella.Visible = true;
                picGuiaOperacionHuella.Refresh();
            }
            //picSample.Visible = true;
            //picSample.Refresh();
            grpCaptureHuella.Location = new Point(395, 143);
            grpCaptureHuella.Text = "";
            grpCaptureHuella.Visible = true;
            grpCaptureHuella.Refresh();
            //Show images y msg de ayuda
            CaptureFinger();
        }

        /// <summary>
        /// Muestra mensajes en diferentes partes de la pantalla con el color adecuado
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="red"></param>
        /// <param name="v2"></param>
        private void MostrarMensaje(int posicion, Color colormsg, string message)
        {
            try
            {
                switch (posicion)
                {
                    default:
                        txtMessage.ForeColor = colormsg;
                        txtMessage.Text = message;
                        txtMessage.Refresh();
                        break;
                }
            }
            catch (Exception ex)
            {

                LOG.Error("BVIUI.MostrarMensaje - Error: " + ex.Message);
            }
        }

        private void Verify()
        {
            try
            {
                LOG.Info("BVIUI.InitAdapter IN...");

                //Si es Facial 2D => Mando a verificar y registrar en Server e informo
                if (_CLIENT_TYPE == 2)
                {
                    VerifyFacial();
                }
                else //Es Huella => Segun sea MOC o Nec hago la verificacion que corresponda 
                {
                    if (_PERSONA.TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
                    {
                        //Debe verificar MOC y grabar resultado en Service
                        VerifyNewCedula();
                    }
                    else
                    {
                        //TODO - Envia a verificar NEC en Service y graba resultado o si es facial, usa verify con 3
                        VerifyOldCedula();
                    }
                }

                //Si sigue pendiente pongo visible el boton de verificar de nuevo, sino no
                if (_PERSONA.verifyResult == 0)
                {
                    btnVerify.Visible = true;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.InitAdapter Error: " + ex.Message);
            }
        }

        private void VerifyFacial()
        {
            string msgerr;
            Int32 finger;
            bool bReturnMatch = false;
            double score = 0;
            string certifypdf;
            try
            {
                LOG.Info("BVIUI.VerifyFacial - IN...");
                var clr = new Bio.Core.CLR.CLR();

                picProgresVerify.Visible = true;
                picProgresVerify.Refresh();
                labProgresVerify.Visible = true;
                labProgresVerify.Refresh();

                //clr.ReaderName = ConfigurationManager.AppSettings["CLRReaderName"];
                LOG.Info("BVIUI.VerifyFacial - Llamando a Service...");
                MostrarMensaje(1, Color.DarkGray, "Certificando en línea. Por favor espere...");
                int ret = _NVHELPER.VerifyFacial(_TRACK_ID_CI, _PERSONA, out bReturnMatch, out score, out certifypdf, out msgerr);

                if (ret == 0)
                {
                    LOG.Debug("BVIUI.VerifyFacial - Return del servicio sin error. Procesando...");
                    _PERSONA.verifyResult = bReturnMatch ? 1 : 2;
                    _PERSONA.score = score;
                    _PERSONA.CertifyPdf = certifypdf;
                    if (bReturnMatch)
                    {
                        //picResultVerify.Image = Properties.Resources.VerifyOK;
                        //picResultVerify.Visible = true;
                        //picResultVerify.Refresh();
                        picGrpVerificandoBackground.Image = Properties.Resources.VerifyPositive;
                        picGrpVerificandoBackground.Refresh();
                        //labResultVerify.Text = "Verificacion Positiva";
                        //labResultVerify.ForeColor = Color.DarkGreen;
                        //labResultVerify.Visible = true;
                        //labResultVerify.Refresh();
                        //labProgresVerify.Text = "Registrando...          Espere por favor...";
                        //labProgresVerify.Refresh();
                        labVerificandoSalir.Visible = true;
                        labVerificandoSalir.Refresh();
                        labVerificandoSiguiente.Visible = true;
                        labVerificandoSiguiente.Refresh();
                        MostrarMensaje(1, Color.Green, "Certificación de Identidad positiva registrada en repositorio con éxito!");
                    }
                    else
                    {
                        //picResultVerify.Image = Properties.Resources.VerifyNOOK;
                        //picResultVerify.Visible = true;
                        //picResultVerify.Refresh();
                        picGrpVerificandoBackground.Image = Properties.Resources.VerifyNegative;
                        picGrpVerificandoBackground.Refresh();

                        //labResultVerify.Text = "Verificacion Negativa";
                        //labResultVerify.ForeColor = Color.Red;
                        //labResultVerify.Visible = true;
                        //labResultVerify.Refresh();
                        btnVerify.Visible = true; //Habilito para reintento...
                        MostrarMensaje(1, Color.Orange, "Certificación de Identidad Negativa registrada en repositorio! Reintente...");
                    } 
                    LOG.Debug($"BVIUI.VerifyFacial - Resultado Match Facial: {_PERSONA.verifyResult.ToString()}");
                }
                else if (ret == -406)
                {
                    //picResultVerify.Image = Properties.Resources.VerifyNOOK;
                    //picResultVerify.Visible = true;
                    //picResultVerify.Refresh();
                    picGrpVerificandoBackground.Image = Properties.Resources.VerifyNegative;
                    picGrpVerificandoBackground.Refresh();

                    //labResultVerify.Text = "Verificacion Negativa";
                    //labResultVerify.ForeColor = Color.Red;
                    //labResultVerify.Visible = true;
                    //labResultVerify.Refresh();
                    btnVerify.Visible = true; //Habilito para reintento...
                    MostrarMensaje(1, Color.Orange, "Certificación de Identidad Negativa registrada en repositorio! Reintente...");
                } else 
                {
                    MostrarMensaje(1, Color.Red, "Error registrando certificación de identidad en repositorio ["
                                                 + ret + "-" + msgerr + "]");
                    LOG.Error("BVIUI.VerifyFacial - Error registrando certificación de identidad en repositorio [" + ret + "-" +
                        msgerr + "]");
                    btnVerify.Visible = true; //Habilito para reintento...
                }
            }
            catch (EndOfStreamException ex)
            {
                MostrarMensaje(1, Color.Red, "Excepcion EndOfStreamException");
                LOG.Error("BVIUI.VerifyFacial - EndOfStreamException ", ex);
            }
            catch (Exception ex)
            {
                MostrarMensaje(1, Color.Red, "Excepcion Exception [" + ex.Message + "]");
                LOG.Error("BVIUI.VerifyFacial - Excepcion Exception [" + ex.Message + "]", ex);
            }
            FillResumenGroup();
            picProgresVerify.Visible = false;
            picProgresVerify.Refresh();
            labProgresVerify.Visible = false;
            labProgresVerify.Refresh();
        }

        private void VerifyOldCedula()
        {
            string msgerr;
            Int32 finger;
            bool bReturnMatch = false;
            double score = 0;
            string certifypdf;
            try
            {
                LOG.Info("BVIUI.VerifyOldCedula - IN...");
                //var clr = new Bio.Core.CLR.CLR();

                //labResultVerify.Visible = false;
                //labResultVerify.Refresh();
                btnVerify.Visible = false;
                //picResultVerify.Visible = false;
                //picResultVerify.Refresh();

                picProgresVerify.Visible = true;
                picProgresVerify.Refresh();
                labProgresVerify.Visible = true;
                labProgresVerify.Refresh();

                //clr.ReaderName = ConfigurationManager.AppSettings["CLRReaderName"];
                LOG.Info("BVIUI.VerifyOldCedula - Llamando a Service...");
                MostrarMensaje(1, Color.DarkGray, "Certificando en línea. Por favor espere...");

                int ret = _NVHELPER.VerifyByNec(_TRACK_ID_CI, 
                                                (_LECTOR_CODIGO_DE_BARRAS!=null?_LECTOR_CODIGO_DE_BARRAS.FullPdf417:null), 
                                                _PERSONA, 
                                                out bReturnMatch, out score, out certifypdf, out msgerr);

                if (ret == 0)
                {
                    LOG.Debug("BVIUI.VerifyOldCedula - Return del servicio sin error. Procesando...");
                    _PERSONA.verifyResult = bReturnMatch ? 1 : 2;
                    _PERSONA.score = score;
                    _PERSONA.CertifyPdf = certifypdf;
                    if (bReturnMatch)
                    {
                        //picResultVerify.Image = Properties.Resources.VerifyOK;
                        //picResultVerify.Visible = true;
                        //picResultVerify.Refresh();
                        picGrpVerificandoBackground.Image = Properties.Resources.VerifyPositive;
                        picGrpVerificandoBackground.Refresh();
                        //labResultVerify.Text = "Verificacion Positiva";
                        //labResultVerify.ForeColor = Color.DarkGreen;
                        //labResultVerify.Visible = true;
                        //labResultVerify.Refresh();
                        //labProgresVerify.Text = "Registrando...          Espere por favor...";
                        //labProgresVerify.Refresh();
                        labVerificandoSalir.Visible = true;
                        labVerificandoSalir.Refresh();
                        labVerificandoSiguiente.Visible = true;
                        labVerificandoSiguiente.Refresh();
                    }
                    else
                    {
                        //picResultVerify.Image = Properties.Resources.VerifyNOOK;
                        //picResultVerify.Visible = true;
                        //picResultVerify.Refresh();
                        picGrpVerificandoBackground.Image = Properties.Resources.VerifyNegative;
                        picGrpVerificandoBackground.Refresh();
                        //labResultVerify.Text = "Verificacion Negativa";
                        //labResultVerify.ForeColor = Color.Red;
                        //labResultVerify.Visible = true;
                        //labResultVerify.Refresh();
                        btnVerify.Enabled = true; //Habilito para reintento...
                    } 
                    LOG.Debug($"BVIUI.VerifyOldCedula - SetupClr: Resultado Match: {_PERSONA.verifyResult}");
                    MostrarMensaje(1, Color.Green, "Certificación de Identidad registrada en repositorio con éxito!");
                } else if (ret == -406)
                {
                    picGrpVerificandoBackground.Image = Properties.Resources.VerifyNegative;
                    picGrpVerificandoBackground.Refresh();
                    btnVerify.Enabled = true; //Habilito para reintento...
                } else 
                {
                    MostrarMensaje(1, Color.Red, "Error registrando certificación de identidad en repositorio [" 
                                                 + ret + "-" + msgerr + "]");
                    LOG.Error("BVIUI.VerifyOldCedula - Error registrando certificación de identidad en repositorio [" + ret + "-" + 
                        msgerr + "]");
                    btnVerify.Visible = true; //Habilito para reintento...
                }
            }
            catch (EndOfStreamException ex)
            {
                MostrarMensaje(1, Color.Red, "Excepcion EndOfStreamException");
                LOG.Error("BVIUI.VerifyOldCedula - EndOfStreamException ", ex);
            }
            catch (Exception ex)
            {
                MostrarMensaje(1, Color.Red, "Excepcion Exception [" + ex.Message + "]");
                LOG.Error("BVIUI.VerifyOldCedula - Excepcion Exception [" + ex.Message + "]", ex);
            }
            FillResumenGroup();
            picProgresVerify.Visible = false;
            picProgresVerify.Refresh();
            labProgresVerify.Visible = false;
            labProgresVerify.Refresh();
        }

        internal static double GetPorcentajeNEC(double score, double threshold)
        {
            double ret = 100;
            try
            {
                LOG.Debug("BVIUI.GetPorcentajeNEC - IN => score = " + score.ToString());
                if (score >= 15000 - threshold)
                {
                    ret = 100;
                }
                else
                {
                    double resto = 15000 - threshold;  //Resto de la diferencia entre 15000 y el umbral
                    double porcentajeResto = score * 100 / resto; //Calculo el porcentaje de lo que significa ese score para ese resto
                    double toSume = 20 * porcentajeResto / 100; //Saco el porcentaje sobre el 20% variable que uso
                    ret = 80 + toSume; //A un 80% fijo por ser positivo le sumo el variable obtenido
                    if (ret > 100) //Por si acaso se pasa (no deberia pero para estar seguro)
                        ret = 100;
                }

            }
            catch (Exception ex)
            {
                ret = 100;
                LOG.Error("BVIUI.GetPorcentajeNEC - Error: " + ex.Message);
            }
            LOG.Debug("BVIUI.GetPorcentajeNEC - OUT! - Porcentaje = " + ret.ToString());
            return ret;
        }

        #endregion Operacion

        #region NFC

        private void VerifyNewCedula()
        {
            string msgerr;
            Int32 finger;
            bool bReturnMatch = false;
            try
            {
                LOG.Info("BVIUI.VerifyNewCedula - IN...");
                var clr = new Bio.Core.CLR.CLR();

                picProgresVerify.Visible = true;
                picProgresVerify.Refresh();
                labProgresVerify.Visible = true;
                labProgresVerify.Refresh();

                //clr.ReaderName = ConfigurationManager.AppSettings["CLRReaderName"];
                LOG.Info("BVIUI.VerifyNewCedula - Se inicializa con el lector:" + _BVI7CONFIG.CLRReaderName);
                clr.ReaderName = _BVI7CONFIG.CLRReaderName;
                clr.CLR_OPEN(10);
                Thread.Sleep(100);
                LOG.Debug("BVIUI.VerifyNewCedula - Entrando a CLR_CONNECT");

                clr.CLR_CONNECT(Convert.ToUInt32(5000));

                if (clr.GetLasError() == 0)
                {
                    LOG.Debug("BVIUI.VerifyNewCedula - Extracción de datos de RUT " + _PERSONA.Rut);
                    //ExtractMrzData(clr);
                    clr.CLR_SECURITY(
                            _PERSONA.Serie,
                            _PERSONA.FechaNacimiento.Value.ToString("yyMMdd"),
                            _PERSONA.FechaExpiracion.Value.ToString("yyMMdd")
                            );

                    //barcodeWebCam.DocumentNumber,
                    //barcodeWebCam.DOB,
                    //barcodeWebCam.DOE);
                    //Thread.Sleep(100);
                    LOG.Debug("BVIUI.VerifyNewCedula - Extracción de datos terminada!");

                    LOG.Debug("BVIUI.VerifyNewCedula - Comenzando Verificación contra cédula nueva de RUT " + _PERSONA.Rut);

                    //string _ISOCompact = null;
                    //string msgerr = null;
                    //int ret = _NVHELPER.GenerateISOCompact(_PERSONA.Iso, out _ISOCompact, out msgerr);

                    //if (ret != 0)
                    //{
                    //    //TODO - Show Error 
                    //}
                    //else
                    //{
                        string resultado = "Verificacion Positiva";
                    //string s = "";
                    //for (int i = 0; i < _PERSONA.IsoCompact.Length; i++)
                    //{
                    //    s += _PERSONA.IsoCompact[i];
                    //}
                    //System.IO.File.WriteAllText(@"c:\tmp\ggs_7_isocompact.txt", s);

                        bReturnMatch = clr.Match(_PERSONA.IsoCompact, out finger);

                    //picProgresVerify.Visible = false;
                    //picProgresVerify.Refresh();
                    //labProgresVerify.Visible = false;
                    //labProgresVerify.Refresh();
                    _PERSONA.verifyResult = bReturnMatch ? 1 : 2;
                    
                    if (bReturnMatch)
                    {
                        //picResultVerify.Image = Properties.Resources.VerifyOK;
                        //picResultVerify.Visible = true;
                        //picResultVerify.Refresh();
                        picGrpVerificandoBackground.Image = Properties.Resources.VerifyPositive;
                        picGrpVerificandoBackground.Refresh();
                        //labResultVerify.Text = "Verificacion Positiva";
                        //labResultVerify.ForeColor = Color.DarkGreen;
                        //labResultVerify.Visible = true;
                        //labResultVerify.Refresh();
                        resultado = "Verificacion Positiva";
                        labProgresVerify.Text = "Registrando...          Espere por favor...";
                        labProgresVerify.Refresh();
                        _PERSONA.score = 100;
                    }
                    else
                    {
                        //picResultVerify.Image = Properties.Resources.VerifyNOOK;
                        //picResultVerify.Visible = true;
                        //picResultVerify.Refresh();
                        picGrpVerificandoBackground.Image = Properties.Resources.VerifyNegative;
                        picGrpVerificandoBackground.Refresh();
                        //labResultVerify.Text = "Verificacion NEgativa";
                        //labResultVerify.ForeColor = Color.Red;
                        //labResultVerify.Visible = true;
                        //labResultVerify.Refresh();
                        resultado = "Verificacion Negativa";
                        btnVerify.Visible = true; //Habilito para reintento...
                        _PERSONA.score = 0; 
                    }
                    LOG.Debug($"BVIUI.VerifyNewCedula - SetupClr: Resultado Match: {resultado}");

                    string certifypdf;    
                    int ret = _NVHELPER.CertificationComplete(_TRACK_ID_CI, _PERSONA, bReturnMatch, out certifypdf, out msgerr);

                    if (ret < 0)
                    {
                        MostrarMensaje(1, Color.Red, "Error registrando certificación de identidad en repositorio!");
                        LOG.Error("BVIUI.VerifyNewCedula - Error registrando certificación de identidad en repositorio [" +
                            msgerr + "]");
                    } else
                    {
                        LOG.Error("BVIUI.VerifyNewCedula - Registro en servicio ok!");
                        _PERSONA.CertifyPdf = certifypdf;
                        labVerificandoSalir.Visible = true;
                        labVerificandoSalir.Refresh();
                        labVerificandoSiguiente.Visible = true;
                        labVerificandoSiguiente.Refresh();
                        MostrarMensaje(1, Color.Green, "Certificación de Identidad registrada en repositorio con éxito!");
                    }
                         
                    //Paso = (match) ? EstadoVerificacion.VerificacionPositiva : EstadoVerificacion.VerificacionNegativa;

                    //}

                    //UpdateButtonsImages();
                }
                else if (clr.GetLasError() == -1)
                {
                    MostrarMensaje(1, Color.Red, "Error al detectar la cédula. Acérquela al dispositivo");
                    LOG.Error("BVIUI.VerifyNewCedula - La cédula leída no corresponde a la huella capturada");
                }
                clr.CLR_DISCONNECT();
                clr.CLR_CLOSE();
            }
            catch (EndOfStreamException ex)
            {
                MostrarMensaje(1, Color.Red, "Excepcion EndOfStreamException");
                LOG.Error("BVIUI.VerifyNewCedula - EndOfStreamException ", ex);
            }
            catch (Exception ex)
            {
                MostrarMensaje(1, Color.Red, "Excepcion Exception [" + ex.Message + "]");
                LOG.Error("BVIUI.VerifyNewCedula - Excepcion Exception [" + ex.Message + "]", ex);
            }
            picProgresVerify.Visible = false;
            picProgresVerify.Refresh();
            labProgresVerify.Visible = false;
            labProgresVerify.Refresh();
            FillResumenGroup();
        }

        /// <summary>
        /// Se extraen los datos de la cédula y se actualiza el viewmodel
        /// </summary>
        /// <param name="clr">Lector Contactless</param>
        void ExtractMrzData(CLR clr)
        {

            //if (Convert.ToBoolean(appConfig.AppSettings.Settings["WebCamActive"].Value) == true)
            //{
            //    logger.Info("SetupClr: Entrando a CLR_SECURITY con número de documento: " +
            //                barcodeWebCam.DocumentNumber);

            //    clr.CLR_SECURITY(
            //        barcodeWebCam.DocumentNumber,
            //        barcodeWebCam.DOB,
            //        barcodeWebCam.DOE);
            //}
            //else
            //{
            //    logger.Info("SetupClr: Entrando a CLR_SECURITY con número de documento: " +
            //     LectorCodigoDeBarras.DocumentNumber);

            //    clr.CLR_SECURITY(
            //        LectorCodigoDeBarras.DocumentNumber,
            //        LectorCodigoDeBarras.DOB,
            //        LectorCodigoDeBarras.DOE);
            //}

            //logger.Info("SetupClr: Entrando a CLR_GETMRZ");

            //clr.CLR_GETMRZ();

            //if (SolicitudVerificacion.Configuracion.ExtraerDatosPersonales)
            //{
            //    ViewModel.Persona.ApellidoPaterno = clr.PatherLastName;
            //    ViewModel.Persona.Nombre = clr.Name;
            //    ViewModel.Persona.ApellidoMaterno = clr.MotherLastName;
            //    ViewModel.Persona.Serie = clr.DocumentNumber;
            //    ViewModel.Persona.Sexo = clr.Sex;
            //    //Bloque de los controles para que no puedan ser modificados
            //    ViewModel.Controles.NombresIsEnabled = false;
            //    ViewModel.Controles.ApellidoPaternoIsEnabled = false;
            //    ViewModel.Controles.ApellidoMaternoIsEnabled = false;

            //    ViewModel.Persona.FechaNacimiento = DateTime.ParseExact(clr.DateOfBirth, "yyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            //    ViewModel.Persona.FechaExpiracion = DateTime.ParseExact(clr.DateOfExpiry, "yyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            //    ViewModel.Resultado.DatosPersonalesExtraidos = true;



            //}
            //else
            //{
            //    ViewModel.Persona.ApellidoPaterno = "";
            //    ViewModel.Persona.Nombre = "";
            //    ViewModel.Persona.ApellidoMaterno = "";
            //    ViewModel.Persona.Serie = "";
            //    ViewModel.Persona.Sexo = "";
            //    //Bloque de los controles para que no puedan ser modificados
            //    ViewModel.Controles.NombresIsEnabled = false;
            //    ViewModel.Controles.ApellidoPaternoIsEnabled = false;
            //    ViewModel.Controles.ApellidoMaternoIsEnabled = false;

            //    ViewModel.Resultado.DatosPersonalesExtraidos = false;
            //}

            ////EXTRACCIÓN FOTOS
            //if (SolicitudVerificacion.Configuracion.extraerFoto)
            //{
            //    byte[] fotoCed = clr.CLR_GETFOTO();
            //    if (fotoCed != null)
            //    {
            //        try
            //        {
            //            logger.Info("Test");
            //            //var foto = Convert.ToBase64String(fotoCed);
            //            BitmapImageCreator.Register();
            //            var por = J2kImage.FromBytes(fotoCed);
            //            //Bitmap img = por.As<Bitmap>();//ACA SE CAE
            //            int sdf = 0;
            //            Bitmap img = por.As<Bitmap>();

            //            ImageFormat format = ImageFormat.Jpeg;

            //            using (MemoryStream ms = new MemoryStream())
            //            {
            //                img.Save(ms, format);
            //                ViewModel.Persona.Foto = Convert.ToBase64String(ms.ToArray());
            //            }

            //            ViewModel.Resultado.FotoExtraida = true;
            //            this.Dispatcher.Invoke(new Action(() =>
            //            {
            //                Foto.Source = BitmapToImageSource(img);
            //            }));
            //        }
            //        catch (Exception ex)
            //        {

            //        }
            //    }
            //    else
            //    {
            //        ViewModel.Persona.Foto = "Null";
            //        logger.Error("No se ha podido extraer la foto");
            //    }
            //}
            //else
            //{
            //    ViewModel.Persona.Foto = "";
            //}

            //if (SolicitudVerificacion.Configuracion.ExtraerFirma)
            //{
            //    byte[] fotoFir = clr.GET_CLRFIRMA();
            //    if (fotoFir != null)
            //    {
            //        BitmapImageCreator.Register();
            //        //var fotoF = Convert.ToBase64String(fotoFir);
            //        var porF = J2kImage.FromBytes(fotoFir);
            //        Bitmap imgF = porF.As<Bitmap>();

            //        ImageFormat format = ImageFormat.Jpeg;

            //        using (MemoryStream ms = new MemoryStream())
            //        {
            //            imgF.Save(ms, format);
            //            ViewModel.Persona.FotoFirma = Convert.ToBase64String(ms.ToArray());
            //        }

            //        ViewModel.Resultado.FotoFirmaExtraida = true;
            //    }
            //    else
            //    {
            //        ViewModel.Persona.FotoFirma = "Null";
            //        logger.Error("No se ha podido extraer la foto firma");
            //    }
            //}
            //else
            //{
            //    ViewModel.Persona.FotoFirma = "";
            //}

        }   

        #endregion NFC

        #region BIOAPI 2.0

        internal void SetBSP(BSPBiometrika bsp)
        {
            try
            {
                BSP = bsp;
                BSP.OnCapturedEvent += OnCaptureEvent;
                BSP.OnTimeoutEvent += OnTimeoutEvent;
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.SetBSP Error = " + ex.Message);
            }
        }

        internal void FreeBSP()
        {
            BSP.OnCapturedEvent -= OnCaptureEvent;
            BSP.OnTimeoutEvent -= OnTimeoutEvent;
        }

        private void InitBSP()
        {
            //BSP = new Biometrika.BioApi20.BSP.BSPBiometrika();

            //BSP.BSPAttach("2.0", true);

            //GetInfoFromBSP();

            BSP.OnCapturedEvent += OnCaptureEvent;
            BSP.OnTimeoutEvent += OnTimeoutEvent;
        }

        private void OnCaptureEvent(int errCode, string errMessage, List<Sample> samplesCaptured)
        {
            string _WSQ = null;
            string _TemplateEnroll = null;
            string _TemplateVerify = null;
            string _TemplateISO = null;
            string _TemplateANSI = null;
            try
            {
                if (string.IsNullOrEmpty(_PERSONA.Wsq))
                {
                    _SamplesCaptured = samplesCaptured;
                    string aux = "";

                    int idx = 0;
                    foreach (Sample item in samplesCaptured)
                    {
                        //if (item.MinutiaeType == 41)
                        //{
                        //    System.IO.File.WriteAllBytes(@"c:\std\sample_mt_41_" + (idx++) + ".bmp", (byte[])item.Data);
                        //    Bitmap b = (Bitmap)item.Data;
                        //    b.Save(@"c:\std\sample_mt_41_" + (idx++) + ".jpg", ImageFormat.Jpeg);
                        //}
                        //else
                        //{
                        if (item.MinutiaeType == 13 && item.Operation == 0)  //Solo proceso los de verify
                        {
                            _PERSONA.Ansi = Convert.ToBase64String((byte[])item.Data);
                        }
                        if (item.MinutiaeType == 14 && item.Operation == 0)  //Solo proceso los de verify
                        {
                            _PERSONA.Iso = Convert.ToBase64String((byte[])item.Data);
                        }
                        if (item.MinutiaeType == 22)
                        {
                            _PERSONA.Raw = Convert.ToBase64String((byte[])item.Data);
                        }
                        if (item.MinutiaeType == 21)  //Solo proceso los de verify
                        {
                            _PERSONA.Wsq = Convert.ToBase64String((byte[])item.Data);
                        }
                        if (item.MinutiaeType == 15 && item.Operation == 0)  //Solo proceso los de verify
                        {
                            _PERSONA.IsoCompact = Utils.Utils.ConvertToHexa((byte[])item.Data); // Convert.ToBase64String((byte[])item.Data);
                        }

                        if (item.MinutiaeType != 41 && item.MinutiaeType != 13 && item.MinutiaeType != 14 &&
                            item.MinutiaeType != 22 && item.MinutiaeType != 21 && item.MinutiaeType != 15)  //Solo proceso los de verify
                        {
                            if (item.Operation == 0) //Es verify
                            {
                                _TemplateVerify = Convert.ToBase64String((byte[])item.Data);
                            }
                            else //es Enroll
                            {
                                _TemplateEnroll = Convert.ToBase64String((byte[])item.Data);
                            }
                        }
                        //}
                    }

                    //TokenResponse = new TokenResponse();
                    bool hasCapture = false;
                    //reader.OnCapturedEvent -= OnCapturedEvent;
                    try
                    {
                        if (errCode == 0 && samplesCaptured != null)
                        {

                            //Invoke(new Action<Bitmap>(ShowFingerprintImage), (Bitmap)(samplesCaptured[samplesCaptured.Count - 2]).Data);
                            picSample.Image = (Bitmap)(samplesCaptured[samplesCaptured.Count - 2]).Data;
                            picSample.Visible = true;
                            picSample.Refresh();
                            Thread.Sleep(250);
                            _PERSONA.JpegOriginal = GetB64FromPictureBox(picSample);

                            //hasCapture = true;
                           
                            labSigCaptureHuella.Visible = true;
                            labSigCaptureHuella.Refresh();
                            FillResumenGroup();
                        }
                        //else if (fingerCaptured.CaptureQuality == ReaderFactory.CaptureQuality.QUALITY_POOR)
                        //{
                        //    bioPacketLocal.PacketParamOut.Add("Error", "-41|Mala Calidad Captura"); //Mala Calidad => SERR_WSQ_LOW_QUALITY
                        //    picSample.Refresh();
                        //}
                    }
                    catch (Exception ex)
                    {
                        bioPacketLocal.PacketParamOut.Add("Error", "-1|Error Procesando Captura [" + ex.Message + "]");
                    }
                    btnReintentarCapturaHuella.Visible = true;
                    btnReintentarCapturaHuella.Refresh();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.OnCaptureEvent Error Ex - " + ex.Message);
            }
            //timer1.Enabled = true;
        }

        private void ShowFingerprintImage(Bitmap img)
        {
            try
            {
                picSample.Image = img;
                picSample.Visible = true;
                picSample.Refresh();
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.ShowFingerprintImage Error: " + ex.Message);
            }
        }

        private void OnTimeoutEvent(int errCode, string errMessage, ISensor sensor)
        {
            //picSample.Image = null;
            MostrarMensaje(0, Color.Red, "Se excedio el tiempo de captura. Reintente...");
            btnReintentarCapturaHuella.Visible = true;
            //btnReintentarCapturaHuella.Enabled = true;
            btnReintentarCapturaHuella.Refresh();
            //MessageBox.Show(errCode + " - " + errMessage);
        }

        public void CaptureFinger()
        {
            bool hasCapture = false;
            

            LOG.Info("BVIUI.CaptureFinger In...");
            //if (BioRouter.HasSensor && (RUT != null && RUT.HasRUT))
            if ((_PERSONA != null && !string.IsNullOrEmpty(_PERSONA.Rut)))
            {
                LOG.Debug("BVIUI.CaptureFinger - Capturando para RUT = " + _PERSONA.Rut);
                //TODO - Sacar el dedo que corresponda desde el PDF417 o desde chip
                int _Finger = (_PERSONA.TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva ? 2 : 7);
                //if (Finger >= 1 && Finger <= 10)
                //{
                    LOG.Debug("BVIUI.CaptureFinger Finger = " + _Finger);
                //Log.Add("Se activa el contador de 20 segundos");
                //timer.Enabled = false; // inicia el timeout para capturar, pasados 20 segundos genera error

                ////////////////////
                //string hashGuid = Guid.NewGuid().ToString().Substring(0, 8);
                btnReintentarCapturaHuella.Visible = false;
                btnReintentarCapturaHuella.Refresh();
                CaptureFinger("RUT", _PERSONA.Rut, _Finger);

                //}
            }
            else
            {
                // notificar que no hay sensor
                //TokenResponse.Error = "-1|Sensor no detectado";
                //labtimeout.Text = TokenResponse.Error;
                LOG.Warn("BVIUI.CaptureFinger - _PERSONA nulo o rut nulo => Sale...");
                //timer.Enabled = false;
                Thread.Sleep(500);
                //ClearBio();
            }
            LOG.Info("BVIUI.CaptureFinger Out!");
            //CloseControl(hasCapture);
        }

        // FOR BIOAPI
        private void CaptureFinger(string typeId, string valueId, int fingerId)
        {
            try
            {
                LOG.Debug("BVIUI.CaptureFinger IN...");
                int quality = 50;
                try
                {
                    quality = _BVI7CONFIG.FingerQualityCapture;
                }
                catch (Exception)
                {
                    quality = 50;
                }

                _SamplesCaptured = null;
                Error err = null;
                if (!String.IsNullOrEmpty(typeId) && !String.IsNullOrEmpty(valueId)) // && SensorConnected)
                {
                    if (fingerId >= 1 && fingerId <= 10)
                    {
                        btnReintentarCapturaHuella.Visible = false;
                        btnReintentarCapturaHuella.Refresh();
                        BSP.Capture(_BVI7CONFIG.FingerSensorType, fingerId, _BVI7CONFIG.FingerSerialSensor,
                                    _BVI7CONFIG.FingerTimeout, 0, quality, out err);
                    }
                }
                if (err != null && err.ErrorCode > 0)
                {
                    LOG.Error("BVIUI.CaptureFinger Error CaptureFinger [" +
                        err.ErrorCode + " - " + err.ErrorDescription + "]");
                }
             }
            catch (Exception e)
            {
                var error = e.Message;
                LOG.Error("BVIUI.CaptureFinger - Error Ex [" + e.Message + "]");
            }
            LOG.Debug("BVIUI.CaptureFinger OUT!");
        }

         #endregion BIOAPI 2.0


        #region BCR - Manejo Lector Codigo de Barras

        /// <summary>
        /// Inicialización del código de barras
        /// </summary>
        /// <returns>true si es que se inicializó, false en caso contrario</returns>
        bool InitBCR()
        {
            try
            {
                //var comPort = ConfigurationManager.AppSettings["COMPort"].ToUpper();
                var comPort = _BVI7CONFIG.COMPort.ToUpper(); //appConfig.AppSettings.Settings["COMPort"].Value.ToUpper();
                LOG.Info($"BVIUI.InitBCR - Abriendo el lector de código de barras configurado en {comPort}");
                _LECTOR_CODIGO_DE_BARRAS = new BCR(comPort);
                _LECTOR_CODIGO_DE_BARRAS.DataCaptured += ProcesarCedulaLeida;
                var bcrOpened = _LECTOR_CODIGO_DE_BARRAS.Open();
                if (!bcrOpened)
                {
                    LOG.Error("BVIUI.InitBCR - Error al abrir el lector de código de barras " + comPort);
                    MostrarMensaje(0, Color.Red, "Error al abrir el lector de código de barras en puerto " + comPort);
                }
                else
                {
                    LOG.Info("BVIUI.InitBCR - Lector de Código de barras abierto correctamente");
                    _ESTADO_WIZARD = EstadoWizard.EsperandoLecturaCodigoBarras;
                }
                return bcrOpened;
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.InitBCR - Error al abrir el lector de código de barras. Puerto " +
                            _BVI7CONFIG.COMPort.ToUpper(), ex);
                return false;
            }
        }

        /// <summary>
        /// Método que se llama al leer una cédula
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ProcesarCedulaLeida(object sender, EventArgs e)
        {
            LOG.Debug("BVIUI.ProcesarCedulaLeida - Captura de lectura de cédula detectada. Antes del control de excepciones");
            try
            {
                if (_ESTADO_WIZARD != EstadoWizard.EsperandoLecturaCodigoBarras || _HAY_PDF417) //!string.IsNullOrEmpty(_LECTOR_CODIGO_DE_BARRAS.FullPdf417))
                {
                    LOG.Debug("BVIUI.ProcesarCedulaLeida - Sale sin procesar porque no es estado correcto");
                    return;
                }

                LOG.Debug("BVIUI.ProcesarCedulaLeida - Procesa de lectura de cédula detectada");
                if (!_LECTOR_CODIGO_DE_BARRAS.RutFormateado.Equals(_SOLICITUD.Persona.Rut)) //FormatRutFormateado(_SOLICITUD.Persona.Rut))
                {
                    picLCBObservaciones.Image = Properties.Resources.VerifyNOOK;
                    picLCBObservaciones.Refresh();
                    labLCBStatus.Text = "Lectura Incorrecta de Código de Barras";
                    labLCBStatus.ForeColor = Color.Red;
                    labLCBStatus.Refresh();

                    LOG.Info("Cédula leída no corresponde a cédula de la solicitud");
                    MostrarMensaje(0, Color.Red, "La cédula leída [" + _LECTOR_CODIGO_DE_BARRAS.RutFormateado  + 
                                   "no corresponde al rut de la solicitud [" + Utils.Utils.FormatRutFormateado(_SOLICITUD.Persona.Rut) + "]");
                    _LECTOR_CODIGO_DE_BARRAS.DataCaptured -= ProcesarCedulaLeida;
                    SetEstadoWizard(EstadoWizard.EsperandoLecturaCodigoBarras);
                }
                else
                {
                    LOG.Info("BVIUI.ProcesarCedulaLeida - Cédula leída corresponde a rut de la solicitud");
 
                    if (_LECTOR_CODIGO_DE_BARRAS.Pdf417)
                    {
                        _PERSONA.TipoIdentificacion = TipoIdentificacion.CedulaChilenaAntigua;
                        _HAY_PDF417 = true;
                        //if (Properties.Settings.Default.WizardAutomatic)
                        //{
                        //timer_SetGetHuellaCedulaVieja.Interval = 3000;
                        //timer_SetGetHuellaCedulaVieja.Enabled = true;
                        //SetEstadoWizard(EstadoWizard.EsperandoHuellaEscaneadaCedulaVieja);
                        //} else
                        //{
                        //    timer_EnableSigLCB.Enabled = true;
                        //    //btnSigGrpLCB.Enabled = true;
                        //    //picLCBObservaciones.Image = Properties.Resources.positive_small;
                        //    //picLCBObservaciones.Visible = true;
                        //    //picLCBObservaciones.Refresh();
                        //    //labLCBStatus.Text = "Lectura Correcta de Código de Barras";
                        //    //labLCBStatus.ForeColor = Color.DarkGreen;
                        //    //labLCBStatus.Visible = true;
                        //    //labLCBStatus.Refresh();
                        //    //labSigLCB.Visible = true;
                        //    //labSigLCB.Refresh();
                        //    //_LECTOR_CODIGO_DE_BARRAS.DataCaptured -= ProcesarCedulaLeida;
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                MostrarMensaje(0, Color.Red, "Error al capturar los datos desde lector de códigos de barras [" + ex.Message + "]");
                LOG.Error("BVIUI.ProcesarCedulaLeida - Error al capturar los datos desde lector de códigos de barras", ex);
            }
        }

        #endregion BCR - Manejo Lector Codigo de Barras

        #region WebCam para captura de Doc Front/Back 

        private void picDocStart_Click(object sender, EventArgs e)
        {
            try
            {
                _CAPTURECAM = new Capture(HelperCamera.SelectCamera(_BVI7CONFIG.CAMCameraDocSelected));
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 1280);
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 720);

                //Set Start / Stop en botones
                picDocStart.Image = Properties.Resources.btn_Start_Disabled;
                picDocStart.Enabled = false;
                picDocStart.Refresh();
                picDocStop.Image = Properties.Resources.btn_Stop_Enabled;
                picDocStop.Enabled = true;
                picDocStop.Refresh();

                if (_ESTADO_WIZARD == EstadoWizard.EsperandoCapturaCedula)
                {
                    picGuiaCaptureDocument.Visible = true;
                    picGuiaCaptureDocument.Refresh();
                }
                else
                {

                }
                Application.Idle += ShowCamStream;
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.picDocStart_Click Error: " + ex.Message);
            }
        }

        private void picDocStop_Click(object sender, EventArgs e)
        {
            try
            {
                //Set Start / Stop en botones
                picDocStart.Image = Properties.Resources.btn_Start_Enabled;
                picDocStart.Enabled = true;
                picDocStart.Refresh();
                picDocStop.Image = Properties.Resources.btn_Stop_Disabled;
                picDocStop.Enabled = false;
                picDocStop.Refresh();
                StopCamStream();
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.picDocStop_Click Error: " + ex.Message);
            }
        }

        private void labCamStart1_Click(object sender, EventArgs e)
        {
            try
            {
                
                _CAPTURECAM = new Capture(HelperCamera.SelectCamera(_BVI7CONFIG.CAMCameraDocSelected));
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 1280);
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 720);
                if (_ESTADO_WIZARD == EstadoWizard.EsperandoCapturaCedula)
                {
                    picGuiaCaptureDocument.Visible = true;
                    picGuiaCaptureDocument.Refresh();
                } else
                {

                }
                Application.Idle += ShowCamStream;
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.labCamStart1_Click Error: " + ex.Message);
            }
        }

        private void labCamStop_Click(object sender, EventArgs e)
        {
            StopCamStream();
        }

        private void ShowCamStream(object sender, EventArgs e)
        {
            try
            {
                //paperDocument.DrawRectangle(pencil, 10, 10, imageCam.Im.Width - 10, imageCam.Height - 10);
                //paperDocument.DrawRectangle(pencil, 10, 10, 430, 250);
                Mat mat = new Mat();
                _CAPTURECAM.Retrieve(mat);
                Image<Bgr, Byte> img = mat.ToImage<Bgr, Byte>();
                //Image<Bgr, Byte> frame = img.Resize(1024, 768, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR).Copy();
                if (_ESTADO_WIZARD == EstadoWizard.EsperandoCapturaCedula)
                {
                    //Image<Gray, Byte> imgOut = img.Convert<Gray, byte>().ThresholdBinary(new Gray(100), new Gray(255));
                    //Emgu.CV.Util.VectorOfColorPoint vectorOfColorPoint = new Emgu.CV.Util.VectorOfColorPoint(); 
                    //Mat hier = new Mat();
                    //CvInvoke.FindContours(imgOut, vectorOfColorPoint, hier, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
                    //CvInvoke.DrawContours(img, vectorOfColorPoint, -1, new MCvScalar(255, 0, 0));


                    imageCam.Image = img.ToBitmap();
                } else if (_ESTADO_WIZARD == EstadoWizard.EsperandoSelfie)
                {
                    imageCamSelfie.Image = img.ToBitmap();
                }

                //Image<Gray, Byte> grayImage = img.Convert<Gray, Byte>();
                //var faces = grayImage.DetectHaarCascade(
                //                                   haar, 1.4, 4,
                //                                   HAAR_DETECTION_TYPE.DO_CANNY_PRUNING,
                //                                   new Size(nextFrame.Width / 8, nextFrame.Height / 8)
                //                                   )[0];
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.ShowCamStream Error = " + ex.Message, ex);
            }
        }

        private void StopCamStream()
        {
            LOG.Info("BVIUI.StopCamStream IN...");
            try
            {
                Application.Idle -= ShowCamStream;
                if (_CAPTURECAM != null)
                {
                    _CAPTURECAM.Stop();
                    _CAPTURECAM.Dispose();
                }
                picGuiaCaptureDocument.Visible = false;
                picGuiaCaptureDocument.Refresh();
                picGuiaCaptureSelfie.Visible = true;
                picGuiaCaptureSelfie.Refresh();
                //if (_ESTADO_WIZARD == EstadoWizard.EsperandoCapturaCedula)
                //{
                //    picGuiaCaptureDocument.Visible = false;
                //    picGuiaCaptureDocument.Refresh();
                //}
                //else
                //{

                //}

            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.StopCamStream Error = " + ex.Message, ex);
            }
            LOG.Info("BVIUI.StopCamStream OUT!");
        }

        private void labCapturaImgDocFront_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("BVIUI.labCapturaImgDocFront_Click IN...");
                picDocumentFront.Image = imageCam.Image;
                picDocumentFront.Visible = true;
                _PERSONA.FotoFrontCedula = GetB64FromPictureBox(picDocumentFront);

                if (!string.IsNullOrEmpty(_PERSONA.FotoFrontCedula))
                {
                    picDocFrontStatus.Image = Properties.Resources.CedulaFrenteOK;
                } else
                {
                    picDocFrontStatus.Image = Properties.Resources.CedulaFrenteNOOK;
                }
                picDocFrontStatus.Refresh();
                LOG.Debug("BVIUI.labCapturaImgDocFront_Click _ImageCedulaB64 = " + _PERSONA.FotoFrontCedula.Substring(0, 15) + "...");
                CheckEnableSiguiente();
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.labCapturaImgDocFront_Click Error = " + ex.Message, ex);
            }
            LOG.Info("BVIUI.labCapturaImgDocFront_Click OUT!");
        }

        private void labCapturaImgDocBack_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("BVIUI.labCapturaImgDocBack_Click IN...");
                picDocumentBack.Image = imageCam.Image;
                picDocumentBack.Visible = true;
                _PERSONA.FotoBackCedula = GetB64FromPictureBox(picDocumentBack);
                if (!string.IsNullOrEmpty(_PERSONA.FotoBackCedula))
                {
                    picDocBackStatus.Image = Properties.Resources.CedulaDorsoOK;
                }
                else
                {
                    picDocBackStatus.Image = Properties.Resources.CedulaFrenteNOOK;
                }
                picDocBackStatus.Refresh();
                LOG.Debug("BVIUI.labCapturaImgDocBack_Click _ImageCedulaB64 = " + _PERSONA.FotoBackCedula.Substring(0, 15) + "...");
                CheckEnableSiguiente();
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.labCapturaImgDocBack_Click Error = " + ex.Message, ex);
            }
            LOG.Info("BVIUI.labCapturaImgDocBack_Click OUT!");
        }

        private void labCapturaImgDocFront1_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("BVIUI.labCapturaImgDocFront_Click IN...");
                picDocumentFront.Image = imageCam.Image;
                picDocumentFront.Visible = true;
                _PERSONA.FotoFrontCedula = GetB64FromPictureBox(picDocumentFront);
                if (!string.IsNullOrEmpty(_PERSONA.FotoFrontCedula))
                {
                    picDocFrontStatus.Image = Properties.Resources.CedulaFrenteOK;
                }
                else
                {
                    picDocFrontStatus.Image = Properties.Resources.CedulaFrenteNOOK;
                }
                picDocFrontStatus.Refresh();
                LOG.Debug("BVIUI.labCapturaImgDocFront_Click _ImageCedulaB64 = " + _PERSONA.FotoFrontCedula.Substring(0, 15) + "...");
                CheckEnableSiguiente();
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.labCapturaImgDocFront_Click Error = " + ex.Message, ex);
            }
            LOG.Info("BVIUI.labCapturaImgDocFront_Click OUT!");
        }

        private void labCapturaImgDocBack1_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("BVIUI.labCapturaImgDocBack_Click IN...");
                picDocumentBack.Image = imageCam.Image;
                picDocumentBack.Visible = true;
                _PERSONA.FotoBackCedula = GetB64FromPictureBox(picDocumentBack);
                if (!string.IsNullOrEmpty(_PERSONA.FotoBackCedula))
                {
                    picDocBackStatus.Image = Properties.Resources.CedulaDorsoOK;
                }
                else
                {
                    picDocBackStatus.Image = Properties.Resources.CedulaFrenteNOOK;
                }
                picDocBackStatus.Refresh();
                LOG.Debug("BVIUI.labCapturaImgDocBack_Click _ImageCedulaB64 = " + _PERSONA.FotoBackCedula.Substring(0, 15) + "...");
                CheckEnableSiguiente();
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.labCapturaImgDocBack_Click Error = " + ex.Message, ex);
            }
            LOG.Info("BVIUI.labCapturaImgDocBack_Click OUT!");
        }

        private void CheckEnableSiguiente()
        {
            try
            {
                if (_ESTADO_WIZARD == EstadoWizard.EsperandoCapturaCedula)
                {
                    if (!string.IsNullOrEmpty(_PERSONA.FotoFrontCedula) && !string.IsNullOrEmpty(_PERSONA.FotoBackCedula))
                    {
                        labSigCaptureDocument.Visible = true;
                    }
                    if (_BVI7CONFIG.WizardAutomatic)
                    {
                        //btnSigCaptureDocument_Click(null, null);
                        labSigCaptureDocument_Click(null, null);
                    }
                } else if (_ESTADO_WIZARD == EstadoWizard.EsperandoSelfie)
                {
                    if (!string.IsNullOrEmpty(_PERSONA.Selfie))
                    {
                        //btnSigCapturaSelfie.Enabled = true;
                        labSigCaptureSelfie.Visible = true;
                    }
                    if (_BVI7CONFIG.WizardAutomatic)
                    {
                        //btnSigCapturaSelfie_Click(null, null);
                        labSigCaptureSelfie_Click(null, null);
                    }
                }

            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.CheckEnableSiguiente Error = " + ex.Message, ex);
            }
        }

        private void picCapturaSelfieStart_Click(object sender, EventArgs e)
        {
            try
            {
                _CAPTURECAM = new Capture(HelperCamera.SelectCamera(_BVI7CONFIG.CAMCameraSelfieSelected));
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 1280);
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 720);

                picGuiaCaptureSelfie.Visible = true;
                picGuiaCaptureSelfie.Refresh();

                //Set imagenes de start y stop 
                picCapturaSelfieStart.Image = Properties.Resources.btn_Start_Disabled;
                picCapturaSelfieStart.Refresh();
                picCapturaSelfieStart.Enabled = false;
                picCapturaSelfieStop.Image = Properties.Resources.btn_Stop_Enabled;
                picCapturaSelfieStop.Enabled = true;
                picCapturaSelfieStop.Refresh();

                Application.Idle += ShowCamStream;
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.picCapturaSelfieStart_Click Error: " + ex.Message);
            }
        }

        private void picCapturaSelfieStop_Click(object sender, EventArgs e)
        {
            try
            {
                StopCamStream();
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.picCapturaSelfieStop_Click Error = " + ex.Message, ex);
            }
        }

       

        private void labCamStop2_Click(object sender, EventArgs e)
        {
            StopCamStream();
        }

        private void labCapturaImgSelfie_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("BVIUI.labCapturaImgSelfie_Click IN...");
                picSelfie.Image = imageCamSelfie.Image;
                picSelfie.Visible = true;
                _PERSONA.Selfie = GetB64FromPictureBox(picSelfie);
                if (!string.IsNullOrEmpty(_PERSONA.Selfie))
                {
                    picSelfieStatus.Image = Properties.Resources.SelfieStatusOK;
                }
                else
                {
                    picSelfieStatus.Image = Properties.Resources.SelfieStatusNOOK;
                }
                picSelfieStatus.Refresh();
                LOG.Debug("BVIUI.labCapturaImgSelfie_Click _ImageCedulaB64 = " + _PERSONA.Selfie.Substring(0,15) + "...");
                CheckEnableSiguiente();
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.labCapturaImgSelfie_Click Error = " + ex.Message, ex);
            }
            LOG.Info("BVIUI.labCapturaImgSelfie_Click OUT!");
        }

       

        private void btnSigCapturaSelfie_Click(object sender, EventArgs e)
        {
            string msgerror;
            try
            {
                SetEstadoWizard(EstadoWizard.EsperandoLecturaCodigoBarras);
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.btnSigCaptureDocument_Click - Error: " + ex.Message);
            }
        }

        private void labSigCaptureSelfie_Click(object sender, EventArgs e)
        {
            string msgerror;
            try
            {
                //StopCamStream();
                picCapturaSelfieStop_Click(null, null);
                SetEstadoWizard(EstadoWizard.EsperandoLecturaCodigoBarras);
                FillResumenGroup();
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.btnSigCaptureDocument_Click - Error: " + ex.Message);
            }
        }

        private void labCapDocStatusSalir_Click(object sender, EventArgs e)
        {
            CloseControl();
        }

        #endregion WebCam para captura de Doc Front/Back 
        
        #region Utils

        private string GetB64FromPictureBox(PictureBox pic)
        {
            return ImageToBase64(pic.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        public string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            string base64String = null;
            LOG.Debug("BVIUI.ImageToBase64 IN...");
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    image.Save(ms, format);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    base64String = Convert.ToBase64String(imageBytes);
                    LOG.Debug("CIUIAdapter.ImageToBase64 base64String = " + base64String);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("CIUIAdapter.ImageToBase64 Error = " + ex.Message, ex);
                base64String = null;
            }
            LOG.Debug("CIUIAdapter.ImageToBase64 OUT!");
            return base64String;
        }





        #endregion Utils
        
        #region Lectura CB con WebCam

        private void InitCamera()
        {
            int indexselected = -1;

            try
            {
                LOG.Debug("BVIUI.InitCamera - Se realiza el Inicio de la Cámara");
                //Recorremos el arreglo de cámaras
                LOG.Debug("BVIUI.InitCamera - Cámara a buscar:" + _BVI7CONFIG.CAMCameraDocSelected);
                for (var index = 0; index < camDevices.Devices.Count; index++)
                {

                    if (camDevices.Devices[index].Name == _BVI7CONFIG.CAMCameraDocSelected)
                    {
                        LOG.Debug("BVIUI.InitCamera - Se seleccionó la cámara:" + _BVI7CONFIG.CAMCameraDocSelected);
                        indexselected = index;
                        LOG.Debug("BVIUI.InitCamera - Número de cámara seleccionada:" + index);
                    }
                    Device deviceaux = new Device();
                    deviceaux.Index = index;
                    deviceaux.Name = camDevices.Devices[index].Name;
                    devices.Add(deviceaux);
                    LOG.Debug("BVIUI.InitCamera - Nombre de cámara encontrado:" + deviceaux.Name);
                }
                if (indexselected < -1)
                {
                    LOG.Error("BVIUI.InitCamera - no pudo subir la cámara");

                    CameraError = true;
                    MostrarMensaje(1, Color.Red, "No se ha inicialiado la cámara para reconocimeintoo de código de barras");
                }
                else
                {
                    camDevices.SelectCamera(indexselected);
                    if (camDevices.Current != null)
                    {
                        camDevices.Current.NewFrame -= Current_NewFrame;
                        if (camDevices.Current.IsRunning)
                        {
                            camDevices.Current.SignalToStop();
                        }
                        camDevices.SelectCamera(devices[indexselected].Index);
                        camDevices.Current.NewFrame += Current_NewFrame;
                        camDevices.Current.Start();
                        LOG.Debug("BVIUI.InitCamera - Se  termina el Inicio de la Cámara");
                    }
                    else
                    {
                        LOG.Debug("BVIUI.InitCamera -  no pudo subir la cámara");

                        CameraError = true;
                        MostrarMensaje(1, Color.Red, "No se ha inicialiado la cámara para reconocimeintoo de código de barras");
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.InitCamera - Se produjo una excepción al iniciar la cámara:" + ex.Message, ex);
                CameraError = false;
            }
        }

        private void Current_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            if (IsDisposed)
            {
                return;
            }
            try
            {
                if (currentBitmapForDecoding == null)
                {
                    currentBitmapForDecoding = (Bitmap)eventArgs.Frame.Clone();
                }
                if (picImageGrpStream.Image != null)
                {
                    picImageGrpStream.Image.Dispose();
                }
                Invoke(new Action<Bitmap>(ShowFrame), eventArgs.Frame.Clone());
            }
            catch (ObjectDisposedException)
            {
                // not sure, why....
            }
        }

        private void ShowFrame(Bitmap frame)
        {
            try
            {
                /*
                 if (captureBox.InvokeRequired)
                    {
                        try
                        {
                            DisplayImageDelegate DI = new DisplayImageDelegate(DisplayImage);
                            this.BeginInvoke(DI, new object[] { Image });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    else
                    {
                        captureBox.Image = Image;
                    }
                */
                if (picImageGrpStream.Width < frame.Width)
                {
                    picImageGrpStream.Width = frame.Width;
                }
                if (picImageGrpStream.Height < frame.Height)
                {
                    picImageGrpStream.Height = frame.Height;
                }
                picImageGrpStream.Image = frame;
            }
            catch (Exception ex)
            {
                LOG.Error("BVIUI.ShowFrame - Error: " + ex.Message);
            }
            
        }

        private void picImageGrpStream_Paint(object sender, PaintEventArgs e)
        {
            if (currentResult == null)
                return;
        }

        /// <summary>
        /// Método que realiza la decofificación de la imagen, usando la librería ZXING.
        /// Importante no cambiar la configuración del encoding, ya que el PDF417 es una imagen binaria.
        /// </summary>
        private void DecodeBarcode()
        {
            
            var reader = new BarcodeReader();
            while (true)
            {
                try
                {
                    if (currentBitmapForDecoding != null)
                    {
                        var result = reader.Decode(currentBitmapForDecoding);
                        if (result != null)
                        {
                            if (result.BarcodeFormat.ToString() == "PDF_417" || result.BarcodeFormat.ToString() == "QR_CODE")
                            {

                                LOG.Debug("BVIUI.DecodeBarcode - Se detectó un código PDF417 o QR");
                                LOG.Debug("BVIUI.DecodeBarcode - Código de barra leído:" + result.BarcodeFormat.ToString());
                                resultAsBinary = System.Text.Encoding.GetEncoding("ISO8859-1").GetBytes(result.Text);
                                LOG.Debug("BVIUI.DecodeBarcode - Contenido código de barra:" + result.Text);

                                Invoke(new Action<Result>(ShowResult), result);
                            }
                        } else
                        {
                            LOG.Debug("BVIUI.DecodeBarcode - result reader.Decode = null");
                        }
                        currentBitmapForDecoding.Dispose();
                        currentBitmapForDecoding = null;
                    }
                    Thread.Sleep(200);
                }
                catch (Exception ex)
                {
                    LOG.Error("BVIUI.DecodeBarcode - Error: " + ex.Message);
                }
                
            }
        }

        /// <summary>
        /// Una vez identificado un código de barra, procedemos a decodificar la información.
        /// </summary>
        /// <param name="result"></param>
        private void ShowResult(Result result)
        {
            String rut = "";
            currentResult = result;
            //txtBarcodeFormat.Text = result.BarcodeFormat.ToString();
            //txtContent.Text = result.Text;
            Barcodewebcam = new BarcodeWebCam();
            try
            {
                if (result.BarcodeFormat.ToString() == "PDF_417")
                {
                    LOG.Debug("BVIUI.ShowResult - Retorna el contenido del PDF417");
                    //    logger.Debug("Biometrika BviCAM.. Código PDF:" + barcode.Value);
                    //    logger.Debug("Biometrika BviCAM.. Código PDF:" + barcode.Value.Length);
                    BarCodeContent = result.Text.Substring(0, 40);

                    Barcodewebcam.ValueId = result.Text.Substring(0, 9).Replace('\0', ' ').TrimEnd();
                    LOG.Debug("BVIUI.ShowResult - Barcodewebcam.ValueId = " + Barcodewebcam.ValueId);
                    Barcodewebcam.LastName = result.Text.Substring(19, 30).Replace('\0', ' ').TrimEnd();
                    LOG.Debug("BVIUI.ShowResult - Barcodewebcam.LastName = " + Barcodewebcam.LastName);
                    Barcodewebcam.DocumentNumber = result.Text.Substring(58, 10).Replace('\0', ' ').TrimEnd();
                    LOG.Debug("BVIUI.ShowResult - Barcodewebcam.DocumentNumber = " + Barcodewebcam.DocumentNumber);
                    Barcodewebcam.Pdf417 = true;
                    ////Barcodewebcam.FullPdf417= Convert.ToBase64String(_binaryData);
                    String _year = result.Text.Substring(52, 2);
                    String _month = result.Text.Substring(54, 2);
                    String _day = result.Text.Substring(56, 2);
                    Barcodewebcam.ExpirationDate = new DateTime(Convert.ToInt32(_year) + 2000, Convert.ToInt32(_month), Convert.ToInt32(_day));
                    LOG.Debug("BVIUI.ShowResult - Barcodewebcam.ExpirationDate = " + Barcodewebcam.ExpirationDate.ToString("dd/MM/yyyy"));
                    //String recorte = barcode.Value;
                    //int indice = 0;
                    //indice = barcode.Value.IndexOf(">TRIAL VERSION EXPIRES 90 DAYS AFTER INSTALLATION<");
                    //String total;
                    //total = barcode.Value.Substring(0, 420);
                    Barcodewebcam._binaryData = resultAsBinary;
                    Barcodewebcam.ProcessNec();
                }
                else if (result.BarcodeFormat.ToString() == "QR_CODE")
                {
                    LOG.Debug("BVIUI.ShowResult - Retorna el contenido del qr");
                    //Barcodewebcam = new BarcodeWebCam();
                    Barcodewebcam.Qr = true;
                    //BarCodeContent = result.Text.Substring(0, result.Text.Length - largo);
                    Barcodewebcam.ValueId = result.Text.Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[0] + result.Text.Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[1];
                    LOG.Debug("BVIUI.ShowResult - Barcodewebcam.ValueId = " + Barcodewebcam.ValueId);
                    Int32 _pos = result.Text.LastIndexOf('=');
                    String _mrz = result.Text.Substring(_pos + 1, 24);

                    Barcodewebcam.DocumentNumber = _mrz.Substring(0, 9);
                    LOG.Debug("BVIUI.ShowResult - Barcodewebcam.DocumentNumber = " + Barcodewebcam.DocumentNumber);
                    Barcodewebcam.DOB = _mrz.Substring(10, 6);
                    LOG.Debug("BVIUI.ShowResult - Barcodewebcam.DOB = " + Barcodewebcam.DOB);
                    Barcodewebcam.DOE = _mrz.Substring(17, 6);
                    LOG.Debug("BVIUI.ShowResult - Barcodewebcam.DOE = " + Barcodewebcam.DOE);

                    string _exdate = _mrz.Substring(17, 6);
                    string _year = _exdate.Substring(0, 2);
                    string _month = _exdate.Substring(2, 2);
                    string _day = _exdate.Substring(4, 2);

                    Barcodewebcam.ExpirationDate = new DateTime(Convert.ToInt32(_year) + 2000, Convert.ToInt32(_month), Convert.ToInt32(_day));
                    LOG.Debug("BVIUI.ShowResult - Barcodewebcam.ExpirationDate = " + Barcodewebcam.ExpirationDate.ToString("dd/MM/yyyy"));
                }

                if (Barcodewebcam.ValueId.Equals(_PERSONA.Rut)) //Si leyo ok el rut que se debe verificar pasa de estado
                {
                    decodingThread.Abort();
                    LOG.Debug("BVIUI.ShowResult - Se obtiene el rut");
                    _HAY_PDF417 = true;
                } else
                {
                    LOG.Debug("BVIUI.ShowResult - Sique intentando porque rut leido no corresponde => RUT a Verificar = " + _PERSONA.Rut + 
                        " / RUT Leido = " + Barcodewebcam.ValueId);
                    Barcodewebcam = null;
                }
            }
            catch (Exception ex)
            {
                LOG.Debug("BVIUI.ShowResult - Error: " + ex.Message);
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            if (!e.Cancel)
            {
                decodingThread.Abort();
                if (camDevices.Current != null)
                {
                    camDevices.Current.NewFrame -= Current_NewFrame;
                    if (camDevices.Current.IsRunning)
                    {
                        camDevices.Current.SignalToStop();
                    }
                }
            }
        }

        #endregion Lectura CB con WebCam

        
    }
}
