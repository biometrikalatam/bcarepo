﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BVIUIAdapter7.Config
{
    public class BVI7Config
    {

        public BVI7Config()
        {
            IgnoreStepSelfieInWizard = false;
            URLWebService = "http://localhost:9999/WSCI.asmx";
            URLServiceBase = "http://localhost:9999/api/ciweb/proxy/";
            AutomaticStartInCaptureDocument = true;
            AutomaticStartInCaptureSelfie = true;
            BlockWizardWithExpiredDocument = false;
            BlockWizardWithBlockedDocumentInSRCeI = false;
            BlockWizardWithYounger = false;
            ClientTypeSelectionEnabled = true;
            ClientType = 0;
            ServiceTimeout = 60000;
            ServiceCompany = 1;
            WizardAutomatic = false;
            ServiceAppId = 7;
            ServiceAccessName = "username";
            ServiceSecretKey = "secretkey";
            CoordinatesGeoRef = "-33.3518753333333,-70.5044326666667";
            CoordinatesAttempts = 0;
            DownloadPath = @"c:\Biometrika\Biometrikca.Client\Downloads\";
            CAMCameraDocSelected = "Microsoft® LifeCam Cinema(TM)";
            CAMCameraDocSelectedId = "123456789";
            CAMCameraSelfieSelectedId = "987654321";
            CAMCameraSelfieSelected = "Microsoft® LifeCam Cinema(TM)";
            CustomerLogoPath = "customerlogo.png";
            UserNameDefault = "gsuhit @biometrika";
            ReturnSamplesImages = true;
            ReturnSamplesMinutiaes = true;
            COMPort = "COM2";
            FingerQualityCapture = 50;
            FingerSerialSensor = "H39150402301";
            FingerTimeout = 5000;
            CLRReaderName = "OMNIKEY Cardman 5x21-CL 0";
            CLRTimeout = 5000;
            FacialThreshold = 40;
            FingerSensorType = 2; //Secugen
            NECThreshold = 1000;
    }

        public bool IgnoreStepSelfieInWizard = false;
        public string URLWebService = "http://localhost:9999/WSCI.asmx";
        public string URLServiceBase = "http://localhost:9999/api/ciweb/proxy/";
        public bool AutomaticStartInCaptureDocument = true;
        public bool AutomaticStartInCaptureSelfie = true;
        public bool BlockWizardWithExpiredDocument = false;
        public bool BlockWizardWithBlockedDocumentInSRCeI = false;
        public bool BlockWizardWithYounger = false;
        public bool ClientTypeSelectionEnabled = true;
        public int ClientType = 0;
        public int ServiceTimeout = 60000;
        public int ServiceCompany = 1;
        public bool WizardAutomatic = false;
        public int ServiceAppId = 7;
        public string ServiceAccessName = "username";
        public string ServiceSecretKey = "secretkey";
        public string CoordinatesGeoRef = "-33.3518753333333,-70.5044326666667";
        public int CoordinatesAttempts = 0;
        public string DownloadPath = @"c:\Biometrika\Biometrikca.Client\Downloads\";
        public string CAMCameraDocSelected = "Microsoft® LifeCam Cinema(TM)";
        public string CAMCameraDocSelectedId = "123456789";
        public string CAMCameraSelfieSelectedId = "987654321";
        public string CAMCameraSelfieSelected = "Microsoft® LifeCam Cinema(TM)";
        public string CustomerLogoPath = "customerlogo.png";
        public string UserNameDefault = "gsuhit @biometrika";
        public bool ReturnSamplesImages = true;
        public bool ReturnSamplesMinutiaes = true;
        public string COMPort = "COM2";
        public int FingerQualityCapture = 50;
        public string FingerSerialSensor = "H39150402301";
        public int FingerTimeout = 5000;
        public string CLRReaderName = "OMNIKEY Cardman 5x21-CL 0";
        public int CLRTimeout = 5000;
        public float FacialThreshold = 40;
        public int FingerSensorType = 2; //Secugen
        public float NECThreshold = 1000; 
    }
}
