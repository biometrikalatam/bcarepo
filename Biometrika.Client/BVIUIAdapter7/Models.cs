﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BVIUIAdapter7
{
    public class BVIResponse
    {
        public BVIResponse()
        {

        }

        public BVIResponse(string trackid, Persona persona)
        {
            TrackIdCI = trackid;
            Persona = persona;
        }

        public string TrackIdCI { get; set; }
        public Persona Persona { get; set; }
    }

    public enum EstadoWizard
    {
        Inicio,
        EsperandoCapturaCedula,
        EsperandoSelfie,
        EsperandoLecturaCodigoBarras,
        EsperandoHuellaEscaneadaCedulaVieja,
        EsperandoHuellaEscaneadaCedulaNueva,
        EsperandoLecturaContactless,
        Verificando,
        VerificacionPositiva,
        VerificacionNegativa,
        EsperandoExportar,
        Final
    }

    public enum ClientType
    {
        SeleccionManual = 0,
        Huella = 1,
        Facial = 2
    }

    public enum TipoIdentificacion
    {
        Indeterminado = 0,
        CedulaChilenaAntigua = 1,
        CedulaChilenaNueva = 2
    }

    public enum TipoBloqueo
    {
        Advertencia = 0,
        Bloqueo = 1,
        Permitir = 2
    }



    public class Solicitud
    {
        public Solicitud() { }

        public Persona Persona { get; set; }
        public Configuracion Configuracion { get; set; }
    }

    public class Configuracion
    {
        public Configuracion() { }

        public int clientType { get; set; }
        //public bool extraerFoto { get; set; }
        public bool ignoreStepSelfieInWizard { get; set; }
        //public bool extraerFirma { get; set; }
        public bool bloqueoMenoresDeEdad { get; set; }
        public bool bloquearDocumentoVencido { get; set; }
        public bool bloqueoCedulaInSCReI { get; set; }
        public bool returnSampleImages { get; set; }
        public bool returnSampleMinutiae { get; set; }

        public string userName { get; set; }
        //public bool vigenciaCedula { get; set; }
        //public bool raw { get; set; }
        //public bool iso { get; set; }
        //public bool ansi { get; set; }
        //public bool wsq { get; set; }
        //public bool jpeg { get; set; }
        //[JsonConverter(typeof(StringEnumConverter))]
        //public TipoBloqueo bloqueoMenoresDeEdad { get; set; }
        //[JsonConverter(typeof(StringEnumConverter))]
        //public TipoBloqueo bloquearDocumentoVencido { get; set; }
    }

    public class Persona
    {
        public Persona() { }

        [JsonConverter(typeof(StringEnumConverter))]
        public TipoIdentificacion TipoIdentificacion { get; set; }
        public string FotoFrontCedula { get; set; }
        public string FotoBackCedula { get; set; }
        public string Foto { get; set; }
        public string FotoFirma { get; set; }
        public string Selfie { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Sexo { get; set; }
        public string Serie { get; set; }
        public string Nacionalidad { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public DateTime? FechaExpiracion { get; set; }
        public bool IsCedulaVigente { get; set; }
        public bool IsMenorDeEdad { get; set; }

        //Result
        public int clientType { get; set; } //1-Huella | 2-Facial
        public int verifyResult { get; set; } // 0-Aun no verifico | 1-Positivo | 2-Negativo
        public double score { get; set; }

        //Certify
        public string CertifyPdf { get; set; }

        //Samples
        public string Ansi { get; set; }
        public string Iso { get; set; }
        public string[] IsoCompact { get; set; }
        public string Raw { get; set; }
        public string Wsq { get; set; }
        public string JpegOriginal { get; set; }
        //public string ApellidoMaterno { get; set; }
        //public string ApellidoPaterno { get; set; }

        public string GetNameTipoIdentificacion()
        {
            if (TipoIdentificacion == TipoIdentificacion.CedulaChilenaAntigua)
                return "Cedula Chilena Antigua";
            if (TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
                return "Cedula Chilena Nueva";
            return "Indeterminado";

        }
    }

    #region Models para Service

    public class CIRegisterModel
    {
        public CIRegisterModel()
        {
        }

        public CIRegisterModel(int _company, int _application, string _trackid, string _typeid, string _valueid,
                               float _threshold, float _score, string _mailCertify, string _cellNumber,
                               string _selfie, string _fingerSample, string _fingersamplejpg)
        {
            company = _company;
            application = _application;
            trackId = _trackid;
            typeId = _typeid;
            valueId = _valueid;
            mailCertify = _mailCertify;
            cellNumber = _cellNumber;
            threshold = _threshold;
            score = _score;
            selfie = _selfie;
            fingerSample = _fingerSample;
            fingerSampleJpg = _fingersamplejpg;
        }

        public int company;
        public int application;
        public string trackId;
        public string typeId;
        public string valueId;
        public string mailCertify;
        public string cellNumber;
        public float threshold = 60; //0 a 100 => Default 60
        public float score = 0; //0 a 100 => Default 60
        public string selfie;
        public string fingerSample; //WSQ del dedo
        public string fingerSampleJpg; //JPG del dedo
    }

    public class CIRegisterModelR
    {
        public CIRegisterModelR()
        {
        }

        public CIRegisterModelR(string _trackid, string _certifyPDF)
        {
            trackId = _trackid;
            certifyPDF = _certifyPDF;
        }

        public string trackId;
        public string certifyPDF;
    }

    public class ErrorModel
    {

        public ErrorModel(int _code, string _message, string _detailmessage)
        {
            code = _code;
            message = _message;
            detailMessage = _detailmessage;
        }


        public int code;
        public string message;
        public string detailMessage;
    }

    public class CIVerifyModel
    {
        public CIVerifyModel()
        {
        }

        public CIVerifyModel(int _company, int _application, string _trackid, string _typeid, string _valueid,
                               float _threshold, float _score, string _mailCertify, string _cellNumber,
                               string _selfie, string _pdf417, string _fingerSample, string _fingersamplejpg)
        {
            company = _company;
            application = _application;
            trackId = _trackid;
            typeId = _typeid;
            valueId = _valueid;
            mailCertify = _mailCertify;
            cellNumber = _cellNumber;
            threshold = _threshold;
            score = _score;
            selfie = _selfie;
            pdf417 = _pdf417;
            fingerSample = _fingerSample;
            fingerSampleJpg = _fingersamplejpg;
        }

        public int company;
        public int application;
        public string trackId;
        public string typeId;
        public string valueId;
        public string mailCertify;
        public string cellNumber;
        public float threshold = 60; //0 a 100 => Default 60
        public float score = 0; //0 a 100 => Default 60
        public string selfie;
        public string pdf417;
        public string fingerSample; //WSQ del dedo
        public string fingerSampleJpg; //JPG del dedo
    }

    public class CIVerifyModelR
    {
        public CIVerifyModelR()
        {
        }

        public CIVerifyModelR(string _trackid, string _typeid, string _valueid, double _threshold, double _score, int _resultVerify, string _certifypdf)
        {
            trackId = _trackid;
            typeId = _typeid;
            valueId = _valueid;
            threshold = _threshold;
            score = _score;
            resultVerify = _resultVerify;
            certifypdf = _certifypdf;
        }

        public string trackId;
        public string typeId;
        public string valueId;
        public double threshold = 60; //0 a 100 => Default 60
        public double score = 0; //0 a 100 => Default 60
        public int resultVerify; //1-Positivo | 2 - Negativo
        public string certifypdf;
    }

    

    #endregion Models para Service


    #region Models para Recognition CB con WebCam

    public class BarcodeWebCam
    {
        public bool Pdf417 { get; set; }
        public bool Qr { get; set; }
        public string FullPdf417 { get; set; }
        public Byte[] _binaryData { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string DOB { get; set; }
        public string DOE { get; set; }
        public string LastName { get; set; }
        public string ValueId { get; set; }
        public int length { get; set; }
        private byte[] temp;
        public byte[] NecTemplate;

        public void ProcessNec()
        {
            for (int z = 0; z < _binaryData.Length; z++)
            {
                if (_binaryData[z] == 32)
                {
                    _binaryData[z] = 0;
                }
            }
            FullPdf417 = Convert.ToBase64String(_binaryData);
            length = (_binaryData[77] << 24) + (_binaryData[76] << 16) + (_binaryData[75] << 8) + (_binaryData[74]);
            temp = new Byte[400];
            Buffer.BlockCopy(_binaryData, 78, temp, 0, length);
            Byte[] _necTemplate = new byte[length];
            Byte[] _aux = (Byte[])temp;
            for (Int32 i = 0; i < length; i++)
            {
                _necTemplate[i] = _aux[i];
            }
            NecTemplate = _necTemplate;
        }
        public String RutFormateado
        {
            get {
                return this.ValueId.Substring(0, ValueId.Length - 1) + "-" + this.ValueId.Substring(this.ValueId.Length - 1);
            }
        }
    }

    #endregion Models para Recognition CB con WebCam

    class Models
    {
    }
}
