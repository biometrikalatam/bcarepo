﻿namespace BVIUIAdapter7
{
    partial class frmHelp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labSalir = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labSalir
            // 
            this.labSalir.BackColor = System.Drawing.Color.Transparent;
            this.labSalir.Location = new System.Drawing.Point(539, 33);
            this.labSalir.Name = "labSalir";
            this.labSalir.Size = new System.Drawing.Size(28, 26);
            this.labSalir.TabIndex = 0;
            this.labSalir.Click += new System.EventHandler(this.labSalir_Click);
            // 
            // frmHelp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::BVIUIAdapter7.Properties.Resources.CIHelp;
            this.ClientSize = new System.Drawing.Size(598, 309);
            this.Controls.Add(this.labSalir);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmHelp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labSalir;
    }
}