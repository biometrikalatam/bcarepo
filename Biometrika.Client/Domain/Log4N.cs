﻿using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Log4N
    {

        private static DirectoryInfo directoryInfo;
        private static string path;

        public static bool Configure()
        {
            bool ret = false;
            try
            {
                string currentPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

                //                directoryInfo = Directory.CreateDirectory(Path.Combine(currentPath, "Log"));
                directoryInfo = Directory.CreateDirectory(currentPath);

                //path = Path.Combine(directoryInfo.FullName, "Logger.cfg");
                path = directoryInfo.FullName;
                XmlConfigurator.Configure(new FileInfo(path + "\\Logger.cfg"));
            }
            catch (Exception ex)
            {

            }

            return ret;

        }


    }
}
