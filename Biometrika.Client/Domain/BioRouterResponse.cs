﻿namespace Domain
{
    public class BioRouterResponse
    {
        public Device Device { get; set; }

        public string SampleANSI { get; set; }
        public string SampleISO { get; set; }
        public string SampleOwner { get; set; }
        public string Wsq { get; set; }
        public string Status { get; set; }
        public string Token { get; set; }
        public string Png { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}


//{
//  "Device": {
//    "Id": 1,
//    "Product": {
//      "VID": "1162",
//      "PID": "0330",
//      "FisicalTypeId": 1,
//      "BiometricTypeId": 1,
//      "Description": "Secugen",
//      "DriverAdapterType": "SecuGenAdapters.SecuGenAdapter, SecuGenAdapters"
//    },
//    "SerialNumber": "SN2A10000018775"
//  },
//  "SampleANSI": "",
//  "SampleISO": "",
//  "SampleOwner": "",
//  "Wsq": "",
//  "Status": "OK",
//  "Token": "917"
//}