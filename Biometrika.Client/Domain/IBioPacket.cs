﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace Domain
{
    public interface IBioPacket
    {
    }

    public class BioPacket : IBioPacket
    {
        public NameValueCollection PacketParamIn { get; set; }
        public Dictionary<string,object> PacketParamOut { get; set; }
    }
}