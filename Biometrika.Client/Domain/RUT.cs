﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class RUT
    {
        public RUT(string typeId , string  valueId)
        {
            TypeId = typeId;
            ValueId = valueId;
        }

        public string TypeId { get; set; }
        public  string ValueId { get; set; }

        public bool HasRUT
        {
            get
            {
                return !string.IsNullOrEmpty(TypeId) && !string.IsNullOrEmpty(ValueId);
            }
        }

        public string Print()
        {
            return TypeId +"-"+ ValueId;
        }

    }
}
