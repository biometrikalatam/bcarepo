﻿using BCR.Bio.Enum;
using BCR.Bio.Enum.BioPortal;
using System.Collections.Specialized;

namespace Domain
{
    public class ServerRequest
    {
        public ServerRequest(NameValueCollection queryString)
        {
            if (!string.IsNullOrEmpty(queryString["TypeId"]) && !string.IsNullOrEmpty(queryString["ValueId"]))
            {
                RUT = new RUT(queryString["TypeId"], queryString["ValueId"]);
            }

            if (!string.IsNullOrEmpty(queryString["Hash"]))
                Hash = queryString["Hash"];
            else
                Hash = string.Empty;

            if (!string.IsNullOrEmpty(queryString["Tokencontent"]))
                Tokencontent = (TokenContentEnum)int.Parse(queryString["Tokencontent"]);
            else
                Tokencontent = TokenContentEnum.ALL;

            if (!string.IsNullOrEmpty(queryString["Device"]))
                Device = (DeviceEnum)int.Parse(queryString["Device"]);
            else
                Device = DeviceEnum.DEVICE_DIGITALPERSONA;

            if (!string.IsNullOrEmpty(queryString["AuthenticationFactor"]))
                AuthenticationFactor = (AuthenticationFactorEnum)int.Parse(queryString["AuthenticationFactor"]);
            else
                AuthenticationFactor = AuthenticationFactorEnum.AUTHENTICATIONFACTOR_FINGERPRINT;

            if (!string.IsNullOrEmpty(queryString["OperationType"]))
                OperationType = (OperationTypeEnum)int.Parse(queryString["OperationType"]);
            else
                OperationType = OperationTypeEnum.VERIFY;

            if (!string.IsNullOrEmpty(queryString["MinutiaeType"]))
                MinutiaeType = (MinutiaeTypeEnum)int.Parse(queryString["MinutiaeType"]);
            else
                MinutiaeType = MinutiaeTypeEnum.MINUTIAETYPE_ISO_IEC_19794_2_2005;

            if (!string.IsNullOrEmpty(queryString["Timeout"]))
                Timeout = int.Parse(queryString["timeout"]); 
            else
                Timeout = 10;

            if (!string.IsNullOrEmpty(queryString["Theme"]))
                Theme = queryString["Theme"];
            else
                Theme = "theme_ligthgreen";
        }

        public RUT RUT { get; set; }

        public TokenContentEnum Tokencontent { get; set; }
        public DeviceEnum Device { get; set; }
        public AuthenticationFactorEnum AuthenticationFactor { get; set; }
        public OperationTypeEnum OperationType { get; set; }
        public MinutiaeTypeEnum MinutiaeType { get; set; }
        public int Timeout { get; set; }
        public string Theme { get; set; }

        public string Hash { get; set; }
        public bool HasData
        {
            get
            {
                return (RUT != null) && (!string.IsNullOrEmpty( Hash));
            }
        }
    }
}