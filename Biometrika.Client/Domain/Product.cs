﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Product
    {
        public string VID { get; set; }
        public string PID { get; set; }
        public string FisicalTypeId { get; set; }
        public string BiometricTypeId { get; set; }
        public string Description { get; set; }
        public string DriverAdapterType { get; set; }
    }
}
