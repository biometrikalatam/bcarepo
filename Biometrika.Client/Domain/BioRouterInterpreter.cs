﻿using System;
using System.Drawing;
using System.IO;
using System.Net;

namespace Domain
{
    public static class BioRouterInterpreter
    {
        public static string GetJsonByParams(BioRouterRequest requestParam)
        {

            string json;
            try
            {           
                string URL = requestParam.BuildRequest();

                WebRequest request = WebRequest.Create(URL);
                request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");

                Log.WriteLog(string.Format("Petición al BR"));


                WebResponse response = request.GetResponse();

                Stream stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(stream);

                json = reader.ReadToEnd();

                reader.Close();
                response.Close();

            }
            catch (Exception)
            {
                json = null;
            }
            return json;
        }

        public static Image Base64ToImage(string base64String)
        {
            if (base64String != null)
            {
                // Convert base 64 string to byte[]
                byte[] imageBytes = Convert.FromBase64String(base64String);
                // Convert byte[] to Image
                using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
                {
                    Image image = Image.FromStream(ms);
                    return image;
                }
            }
            else
                return null;
        }
    }
}