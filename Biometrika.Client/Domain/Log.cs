﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Domain
{
    public static class Log
    {
        private static DirectoryInfo directoryInfo;
        private static string path;

        private static void Create()
        {
            string currentPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            directoryInfo = Directory.CreateDirectory(Path.Combine(currentPath, "Log"));

            path = Path.Combine(directoryInfo.FullName, "log.txt");
        }

        public static void Add(string msg)
        {
            Create();
            msg = string.Concat(DateTime.Now, "-", msg, Environment.NewLine);
            if (!File.Exists(path))
                File.WriteAllText(path, msg);
            else
                File.AppendAllText(path, msg);
        }

        public static Dictionary<string, object> GenerateInfo()
        {
            Dictionary<string, object> detail = new Dictionary<string, object>();
            detail.Add("path", path);
            detail.Add("content", File.ReadAllText(path));

            return detail;
        }

        public static void WriteLog(string msg)
        {
            //string linea = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - " + msg;

            //if (Debug)
            //{
            //    StreamWriter sr = new StreamWriter(PathDebug, File.Exists(PathDebug));
            //    sr.WriteLine(linea);
            //    sr.Close();
            //}
            //StreamWriter streamWriter = File.CreateText(Path.Combine(directoryInfo.FullName, "log.txt"));
            //streamWriter.WriteLine(string.Concat(DateTime.Now, "-", msg));
            //streamWriter.Close();
        }
    }
}