﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class FEMResponse
    {
    
            public string FEM { get; set; }

            public string DeviceId { get; set; }
            public string Error { get; set; }
            public string Hash { get; set; }

    }
}
