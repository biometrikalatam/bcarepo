﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class BCResponse
    {
        //Code 0 - OK | <0 - Error
        public int Code { get; set; }
        //Mensaje de error si code < 0
        public string Message { get; set; }
        //Json conteniendo la data actual de cada componente
        public object Data { get; set; }

    }
}
