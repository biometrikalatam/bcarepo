﻿//using System.Drawing;

namespace Domain
{
    public class TokenResponse
    {
        public string Token { get; set; }

        public string Huella { get; set; }
        //public Image Image { get; set; }
        public string ImageSample { get; set; }


        public bool ResponseOK
        {
            get
            {
                if (Token == null) return false;
                return Token.Length > 0;
            }
        }

        public string Error { get; set; }
        public int BodyPart { get; set; }
        public string FingerName { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }
        public string Hash { get; set; }
    }
}