﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Errors
    {

        public const int ERROR_OK = 0;

        //Generales
        public const int ERROR_UNKNOWN = -1;
        public const int ERROR_LICENSE = -2;
        public const int ERROR_PARAMS_NULL = -3;
        public const int ERROR_PARAMS_MISSING = -4;
        public const int ERROR_PARAMS_PARSING = -5;
        public const int ERROR_SERVICE_REMOTE_NOT_EXIST = -6;
        public const int ERROR_SERVICE_REMOTE_TIMEOUT = -7;
        public const int ERROR_SERVICE_REMOTE = -8;


        public const int ERROR_HW_FINGERPRINT_NOT_CONFIGURED = -10;
        public const int ERROR_HW_BARCODE_NOT_CONFIGURED = -11;
        public const int ERROR_HW_WEBCAM_NOT_CONFIGURED = -12;
        public const int ERROR_HW_NFC_NOT_CONFIGURED = -13;

        public const int ERROR_HW_FINGERPRINT_NOT_CONNECTED = -14;
        public const int ERROR_HW_BARCODE_NOT_CONNECTED = -15;
        public const int ERROR_HW_WEBCAM_NOT_CONNECTED = -16;
        public const int ERROR_HW_NFC_NOT_CONNECTED = -17;

        public const int ERROR_HW_FINGERPRINT_TIMEOUT = -18;
        public const int ERROR_HW_BARCODE_TIMEOUT = -19;
        public const int ERROR_HW_WEBCAM_TIMEOUT = -20;
        public const int ERROR_HW_NFC_TIMEOUT = -22;

        public const int ERROR_BCRESPONSE_NULL = -21;

        public const int ERROR_HW_FINGERPRINT_INITCAPTURE = -23;
        public const int ERROR_SAMPLE_BAD_QUALITY = -24;

        public const int ERROR_MANUAL_CLOSE = -25;
        public const int ERROR_MUTEX_DISCARD = -26;

        public const int ERROR_HW_WEBCAM_INITCAPTURE = -27;
        public const int ERROR_HW_WEBCAM_STOPCAPTURE = -28;
        
        //BC7
        public const int ERROR_BC7_CREATING_TOKEN = -40;


        //BVI
        public const int ERROR_BVI_RUTSETTED_DIFFERENT_RUTREADED = -60;
        public const int ERROR_CANCEL_WITHOUT_DATA = -61;
        public const int ERROR_CANCEL_WITH_CB_READEDOK = -62;
        public const int ERROR_CANCEL_WITH_SAMPLE_READEDOK = -63;
        public const int ERROR_BARCODE_DECODING = -64;
        public const int ERROR_INCOMPLET_PROCESS = -65;

        public const int ERROR_CLIENT_NOT_AVAILABLE_IN_BIOPORTAL = -501;
        public const int ERROR_CONVERSION_TO_ISOCC = -502;
        public const int ERROR_VERIFY_OLD_CEDULA = -503;

        static public string GetMessage(int error)
        {
            switch (error)
            {
                case ERROR_OK:
                    return "No Error";
                case ERROR_LICENSE:
                    return "Error de licencia";
                case ERROR_PARAMS_NULL:
                    return "Error Parametros Nulos";
                case ERROR_PARAMS_MISSING:
                    return "Error Falta alguno de los parametros obligatorios";
                case ERROR_PARAMS_PARSING:
                    return "Error procesando parametros";
                case ERROR_SERVICE_REMOTE_NOT_EXIST:
                    return "Error servicio remoto no existe o no responde";
                case ERROR_SERVICE_REMOTE_TIMEOUT:
                    return "Error timeout de conexion a servicio remoto";
                case ERROR_SERVICE_REMOTE:
                    return "Error retornado desde el servicio remoto";

                case ERROR_HW_FINGERPRINT_NOT_CONFIGURED:
                    return "No existe un lector de huellas conectado";
                case ERROR_HW_BARCODE_NOT_CONFIGURED:
                    return "No existe un lector de codigos de barras conectado";
                case ERROR_HW_WEBCAM_NOT_CONFIGURED:
                    return "No existe una webcam conectada";
                case ERROR_HW_NFC_NOT_CONFIGURED:
                    return "No existe un lector de NFC conectado";

                case ERROR_HW_FINGERPRINT_TIMEOUT:
                    return "Timeout en lectura de huella";
                case ERROR_HW_BARCODE_TIMEOUT:
                    return "Timeout en lectura de codigo de barras";
                case ERROR_HW_WEBCAM_TIMEOUT:
                    return "Timeout en lectura de imagen por webcam";
                case ERROR_HW_NFC_TIMEOUT:
                    return "Timeout de lectura NFC";

                case ERROR_BCRESPONSE_NULL:
                    return "Respuesta del cliente Nula";

                case ERROR_HW_FINGERPRINT_INITCAPTURE:
                    return "Error inicializando lector de huella";
                case ERROR_SAMPLE_BAD_QUALITY:
                    return "Mala calidad de imagen de huella capturada";

                case ERROR_MANUAL_CLOSE:
                    return "Cierre manual de cliente sin captura";
                case ERROR_MUTEX_DISCARD:
                    return "Error descarte por servicio ocupado";

                case ERROR_HW_WEBCAM_INITCAPTURE:
                    return "Error inicializando uso webcam";
                case ERROR_HW_WEBCAM_STOPCAPTURE:
                    return "Error parando uso webcam";
                //BC7
                case ERROR_BC7_CREATING_TOKEN:
                    return "Error creando token";

                //BVI
                case ERROR_BVI_RUTSETTED_DIFFERENT_RUTREADED:
                    return "Error No coincide Rut configurado con Rut leido";
                case ERROR_CANCEL_WITHOUT_DATA:
                    return "Cancela proceso sin error y sin datos";
                case ERROR_CANCEL_WITH_CB_READEDOK:
                    return "Cancela proceso sin error con lectura codigo de barras";
                case ERROR_CANCEL_WITH_SAMPLE_READEDOK:
                    return "Cancela proceso sin error con lectura codigo de barras y huella";
                case ERROR_BARCODE_DECODING:
                    return "Error decodigifando codigo de barras";
                case ERROR_INCOMPLET_PROCESS:
                    return "Proceso de verificacion imcompleto";

                case ERROR_CLIENT_NOT_AVAILABLE_IN_BIOPORTAL:
                    return "Error por Dispositivo NO Habilitado en plataforma central";
                case ERROR_CONVERSION_TO_ISOCC:
                    return "Error en la conversion de minucias a ISOCC";
                case ERROR_VERIFY_OLD_CEDULA:
                    return "Error verificanbdo cedula antigua";
                default:
                    return "Error Desconocido [" + error.ToString() + "]";
            }
        }
    }
}