﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class SampleResponse
    {
        public string WSQ { get; set; }
        public string TemplateVerify64 { get; set; }
        public string TemplateRegistry64 { get; set; }
        public string TemplateANSI64 { get; set; }
        public string TemplateISO64 { get; set; }
        public string Huella { get; set; }
        //public Image Image { get; set; }

        public bool ResponseOK
        {
            get
            {
                if (WSQ == null) return false;
                return WSQ.Length > 0;
            }
        }

        public string Error { get; set; }
        public int BodyPart { get; set; }
        //public string BodyPartName { get; set; }
        public int AuthenticationFactor { get; set; }
        //public string AuthenticationFactorName { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }
        public string SerialDevice { get; set; }

    }
}
