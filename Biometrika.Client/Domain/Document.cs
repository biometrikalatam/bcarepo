﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{

    public class Document : System.ICloneable
    {

        public Document() { }

        public string IDCardImageFront { get; set; }
        public string IDCardImageBack { get; set; }
        public string IDCardPhotoImage { get; set; }
        public string IDCardSignatureImage { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string Name { get; set; }
        public string PhaterLastName { get; set; }
        public string MotherLastName { get; set; }
        public string Sex { get; set; }
        public string BirthDate { get; set; }
        public string Nacionality { get; set; }
        public string IssueDate { get; set; }
        public string ExpirationDate { get; set; }
        public string Serial { get; set; }
        //Valido segun fecha ExpirationDate (0-No Expirada | 1-Expirada)
        public int DateValid { get; set; }
        //Bloqueada en SRCeI (0-No Bloqueada | 1-Bloqueada)
        public int BlockedInSRCeI { get; set; }
        //Es menor de edad segun ExpirationDate (0-Mayor Edad | 1-Menor de Edad)
        public int Younger { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
    
}
