﻿using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Domain
{
    public class TypeManager
    {
        public Assembly Assembly { get; set; }
        public Type Type { get; set; }
    }

    public static class Managers
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Managers));

        private static Dictionary<string, TypeManager> managers = new Dictionary<string, TypeManager>();

        public static void Initialize(BPConfig bPConfig)
        {

            try
            {
                string currentPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)); // + "\\Managers";

                LOG.Info("Domain.Managers - Buscando managers en " + currentPath);

                string[] dirs;

                ArrayList arrayList = new ArrayList();

                //foreach (string d in Directory.GetDirectories(currentPath))
                //{
                //    arrayList.AddRange(Directory.GetFiles(d, "*Manager.dll"));
                //    string[] listfi = Directory.GetFiles(currentPath, "*Manager.dll");
                //    foreach (string item in listfi)
                //    {
                //        arrayList.Add(item);
                //    }
                //}

                string[] listfi = Directory.GetFiles(currentPath, "*Manager.dll");
                LOG.Debug("Domain.Managers - List Size de Managers = " + listfi.Length);
                foreach (string item in listfi)
                {
                    arrayList.Add(item);
                }

                LOG.Debug("Domain.Managers - Comeinza carga...");
                foreach (string fileName in arrayList)
                {
                    LOG.Debug("Domain.Managers -    => Cargando = " + fileName);
                    Assembly assembly = Assembly.LoadFrom(fileName);

                    LOG.Debug("Domain.Managers -      => " + string.Concat("Se levantó en memoria el archivo ", fileName, " con Assembly ", assembly.FullName));
                    foreach (Type type in assembly.GetExportedTypes())
                    {
                        if (type.GetInterfaces().Contains(typeof(IManager)))
                        {
                            LOG.Debug("Domain.Managers -      => Procesando = " + type.FullName);
                            IManager manager = (IManager)assembly.CreateInstance(type.FullName);
                            string tag = manager.Tag;
                            if (bPConfig.Adapters.Contains(tag))
                            {
                                LOG.Debug("Domain.Managers -      => Agregando manager con Tag = " + tag);
                                managers.Add(tag, new TypeManager() { Assembly = assembly, Type = type });
                            }
                        }
                        else
                            LOG.Debug("Domain.Managers -      => " + string.Concat("El type ", type.FullName, "  no hereda de  IManager (Descartado)"));
                    }
                }

            }
            catch (Exception ex)
            {
                LOG.Error("Domain.Managers - Error cargando Managers [" + ex.Message + "]");
              }
        }

        public static Assembly AssemblyFrom(string src)
        {
            return managers[src].Assembly;
        }

        public static List<string> Collection()
        {
            List<string> managerList = new List<string>();
            foreach (string item in managers.Keys)
            {
                managerList.Add(string.Concat(item, "-", managers[item].Assembly.FullName, "-", managers[item].Type.FullName));
            }

            return managerList;
        }

        public static TypeManager TypeManagerFrom(string tag) => managers[tag];
    }
}