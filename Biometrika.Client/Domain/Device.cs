﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Device
    {
        public string Id { get; set; }
        public Product Product { get; set; }
        public string SerialNumber { get; set; }
    }

}
