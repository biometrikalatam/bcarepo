﻿namespace Domain
{
    public enum FingerAction
    {
        Capture, Verify
    }
}