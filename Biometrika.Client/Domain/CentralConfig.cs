﻿namespace Domain
{
    public class CentralConfig
    {
        public string ServerConfig { get; set; } = "http://localhost:62457/config";
        public int UpdateEveryHours { get; set; } = 24;
        public bool ForceLocal { get; set; } = false;
    }
}