﻿using System.Collections.Generic;

namespace Domain
{
    public class BPConfig
    {
        public string LocalPort { get; set; }   // puerto a usar en el cliente
        public int Timeout { get; set; }
        public bool LoadBSP { get; set; }  //Levanta BSP para pasarlo por parametro a quienes lo usan, asi no se reinicializa cada vez
        public IList<string> Adapters { get; set; }   // lista de adapters basados en la key
    }
}