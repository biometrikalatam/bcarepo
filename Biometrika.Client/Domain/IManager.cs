﻿namespace Domain
{
    public interface IManager
    {
        string Tag { get; }
        string Buffer { get; }
    }
}