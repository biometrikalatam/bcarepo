﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class HuellaCapturadaEventArgs : EventArgs
    {
        public TokenResponse TokenResponse { get; set; }

        public string Hash { get; set; }

        public bool HasCapture { get; set; }
    }
}
