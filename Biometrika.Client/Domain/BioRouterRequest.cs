﻿using System.Collections.Generic;

namespace Domain
{
    public class BioRouterRequest
    {
        public string AbsoluteURL { get; set; }

        public string Verb { get; set; }    /// may be enumerable

        public string SerialNumberDevice
        {
            get; set;
        }

        public string BuildRequest()
        {
            //UriBuilder builder = new UriBuilder();
            //builder.Host = ;
            string uri = AbsoluteURL;

            uri = string.Concat(AbsoluteURL, "/api/", Verb, "/", SerialNumberDevice);
            if (AditionalParams.Count > 0)
            {
                uri = uri + "?";
                foreach (string key in AditionalParams.Keys)
                {
                    uri = uri + key + "=" + AditionalParams[key] + "&";
                }
            }

            uri = string.Concat(uri, "token=", Hash);

            Log.WriteLog(string.Format("Uri Generada -> {0}", uri));
            return uri;
        }

        public Dictionary<string, string> AditionalParams { get; set; }

        public string Hash { get; set; }
    }
}

//http://localhost:8080/api/bir/SN2A10000018775?type=Verification&token=917
