﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bio.Core.Wsq.Decoder;
using Biometrika.BioApi20.Interfaces;
using DPUruNet;
using log4net;

namespace Biometrika.BioApi20.BFPProcessingExtractor.ISO
{
    public class ISO : IExtractor
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ISO));

        public int AuthenticationFactor { get; set; }
        public int MinutiaeType { get; set; }
        public double Threshold { get; set; }
        public string Parameters { get; set; }

        public int Extract(Sample SampleIn, int purpose, out Sample sampleOut)
        {
            int ret = 0;
            sampleOut = null;
            try
            {
                LOG.Debug("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract IN...");
 
                if (sampleOut == null) return Error.IERR_NULL_TEMPLATE;
                if (sampleOut.Data == null) return Error.IERR_NULL_TEMPLATE;

                if (SampleIn.AuthenticationFactor !=
                    Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT ||
                    (SampleIn.MinutiaeType != Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005 &&
                     SampleIn.MinutiaeType != Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ &&
                     SampleIn.MinutiaeType != Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW))
                {
                    return Error.IERR_INVALID_TEMPLATE;
                }
                LOG.Debug("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract Check MinutiaeType OK");

                //Depende de la minucia que venga, hago:
                //  1.- Si es ISO = Ya viene generado, solo asigno
                //  2.- Si es RAW => extraigo minucias ISO (Con DP preferente x ser gratuito) 
                //  3.- Si es WSQ => descomrpimo y trato como RAW
                sampleOut = SampleIn;
                switch (SampleIn.MinutiaeType)
                {
                    case 14: //Constant.MinutiaeType.MINUTIAETYPE_ISO
                        sampleOut.Data = SampleIn.Data; // DataISO;
                        LOG.Debug("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract Template ISO Asignado => "
                                    + Convert.ToBase64String((byte[])sampleOut.Data));
                        break;
                    case 21: //Constant.MinutiaeType.MINUTIAETYPE_WSQ:
                    case 22: //Constant.MinutiaeType.MINUTIAETYPE_RAW:
                        bool rawOK = false;
                        byte[] raw = null;
                        short w, h;
                        if (SampleIn.MinutiaeType == 21) //Constant.MinutiaeType.MINUTIAETYPE_WSQ
                        {
                            LOG.Debug("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract - In WSQ...");
                            //1.- Descomprimo WSQ
                            WsqDecoder decoder = new WsqDecoder();
                            byte[] wsq = (byte[])SampleIn.Data;
                            if (!decoder.DecodeMemory(wsq, out w, out h, out raw))
                            {
                                ret = Error.IERR_WSQ_DECOMPRESSING;
                                LOG.Error("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract Error [decoder.DecodeMemory]");
                            }
                            else
                            {
                                rawOK = true;
                            }
                            LOG.Debug("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract - OUT WSQ!");
                        }
                        else
                        {
                            raw = (byte[])SampleIn.Data;
                            rawOK = true;
                            LOG.Debug("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract - RAW Recibido...");
                        }
                        if (rawOK)
                        {
                            LOG.Debug("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract FeatureExtraction.CreateFmdFromRaw IN...");
                            LOG.Debug("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract    RAW => " + Convert.ToBase64String(raw));
                            DataResult<Fmd> resultConversion1 = FeatureExtraction.CreateFmdFromRaw(raw, 1, 0, 512, 512, 500, Constants.Formats.Fmd.ISO);
                            LOG.Debug("ISO_19794_2_2005.Extractor.Extract FeatureExtraction.CreateFmdFromRaw OUT!");
                            if (resultConversion1.ResultCode == Constants.ResultCode.DP_SUCCESS)
                            {
                                LOG.Debug("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract FeatureExtraction.CreateFmdFromRaw OK");
                                sampleOut.Data = resultConversion1.Data.Bytes;
                                sampleOut.MinutiaeType = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005;
                                LOG.Debug("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract   Template ISO Generado => "
                                                + Convert.ToBase64String((byte[])sampleOut.Data));
                            }
                            else
                            {
                                LOG.Debug("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract Error Extracting [ResultCode=" + resultConversion1.ResultCode.ToString() + "]");
                                return Error.IERR_EXTRACTING;
                            }
                        }
                        break;
                    default:
                        LOG.Debug("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract Invalid Template Out!");
                        return Error.IERR_INVALID_TEMPLATE;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Biometrika.BioApi20.BFPProcessingExtractor.ISO.Extract Error = " + ex.Message);
            }
            return ret;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~ISOCompact() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
