﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Biometrika.BVIOem.Atm")]
[assembly: AssemblyDescription("Componente de verificación contra cédula Chilena para ATMs")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Biometrika S.A.")]
[assembly: AssemblyProduct("Biometrika.BVIOem.Atm")]
[assembly: AssemblyCopyright("Copyright ©Biometrika  2021")]
[assembly: AssemblyTrademark("Biometrika")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d34692e5-ebd4-4b4f-8d52-54cb5e7ac8e2")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("7.5.*")]
[assembly: AssemblyFileVersion("7.5")]
