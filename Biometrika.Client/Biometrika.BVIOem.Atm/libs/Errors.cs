﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Componente de verificación de identidad, a través de huella dactilar, entre captura realizada
/// en lector Digital Persona o Secugen, y la cédula de identidad Chilena Nueva.
/// </summary>
namespace Biometrika.BVIOem.Atm.libs
{
    /// <summary>
    /// Constantes de error para la componente
    /// </summary>
    public class Errors
    {
        /// <summary>
        /// Significa que la operación realizada NO tiene error.
        /// </summary>
        public const int ERROR_OK = 0;
        /// <summary>
        /// Error desconocido. Se da en excepcions que no están controladas.
        /// </summary>
        public const int ERROR_UNKKNOW = -1;
        /// <summary>
        /// Error cuando la licencia es incorrecta (vencida, no existe, etc)
        /// </summary>
        public const int ERROR_LICENSE = -2;
        /// <summary>
        /// Error cuando la licnecia adicional es incorrecta o no existe.
        /// </summary>
        public const int ERROR_LICENSE_3RD = -3;
        /// <summary>
        /// Error inicializando la componente. Puede ser por licencia o por falta de archivos de configuracion.
        /// </summary>
        public const int ERROR_INITIALIZING = -4;
        /// <summary>
        /// Error cuando se intenta usar un metodo sin haber inicializado antes con el metodo Initialize.
        /// </summary>
        public const int ERROR_NOT_INITIALIZED = -5;
        /// <summary>
        /// Error cuando el archivo de configuracion es nulo
        /// </summary>
        public const int ERROR_INITIALIZING_CONFIG_NULL = -6;
                     
        /// <summary>
        /// Error cuando se intenta crear el (BSP) Biometric Service Provider para tomar controld el lector de huella
        /// </summary>
        public const int ERROR_BSP_CREATE = -10;
        /// <summary>
        /// Error cuando se intenta isntanciar el BSP
        /// </summary>
        public const int ERROR_BSP_ATTACH = -11;
        /// <summary>
        /// Error cuando se intenta extraer minucias del formato necesario para el Match On Card
        /// </summary>
        public const int ERROR_BSP_EXTRACTING = -12;
        /// <summary>
        /// Error de timeout en la captura de la huella
        /// </summary>
        public const int ERROR_BSP_TIMEOUT = -13;

        /// <summary>
        /// Error en la apertura del lector de código de barras
        /// </summary>
        public const int ERROR_BCR_OPEN = -20;
        /// <summary>
        /// Error cuando se lee un RUT desde un QR diferente al configurado para verificar
        /// </summary>
        public const int ERROR_BCR_RUT_ERRONEOUS = -21;
        /// <summary>
        /// Error cuando se lee un código de barras de cédula antigua y no se debe procesar
        /// </summary>
        public const int ERROR_BCR_IS_OLD_IDCARD = -22;
        /// <summary>
        /// Error cuando cédula está vencida por fecha y esta habilitado el Bloque de proceso cunado ocurre ese evento
        /// </summary>
        public const int ERROR_BCR_IS_DOCUMENT_EXPIRATED = -23;

        /// <summary>
        /// Error que se devuelve cuando se intenta verificar y alguno de los procesos predecesores no están completos 
        /// (Lectura de QR y captura de Huella) 
        /// </summary>
        public const int ERROR_VERIFY_PARAM_BC_MISSING = -30;
        /// <summary>
        /// Error cuando se intenta verificar y aun no se generó el template en el formato necesario para el Match On Card.
        /// </summary>
        public const int ERROR_VERIFY_PARAM_SAMPLE_MISSING = -31;
        /// <summary>
        /// Error al intentar conectar al lector de proximidad para la realización del Match On Card.
        /// </summary>
        public const int ERROR_VERIFY_CONNECT_READER = -32;
        /// <summary>
        /// Error de integridad. Se da Cuando el rut del chip utilizado para la verificación no coincide con el leído en el código de barras.
        /// </summary>
        public const int ERROR_VERIFY_INTEGRITY = -33;

        /// <summary>
        /// Error cuando se intenta leer datos desde el chip
        /// </summary>
        public const int ERROR_CLR_READING = -40;

        
    }
}
