﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Biometrika.BVIOem.Atm.libs
{
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Utils
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Utils));

        internal static string ToStringFromProperty(object obj)
        {
            string ret = "";
            bool isFirst = true;
            object oAux = null;
            try
            {
                LOG.Debug("Utils.GetValueOfPropert IN...");
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
                {
                    oAux = descriptor.GetValue(obj);
                    if (oAux == null) oAux = "";
                    if (isFirst)
                    {
                        isFirst = false;
                        ret = descriptor.Name + " = " + oAux.ToString();
                    }
                    else
                    {
                        ret += "|" + descriptor.Name + " = " + oAux.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ret = "Utils.GetValueOfProperty Excp Error: " + ex.Message;
                LOG.Error("Utils.GetValueOfProperty Excp Error: ", ex);
            }
            LOG.Debug("Utils.GetValueOfPropert OUT!");
            return ret;
        }

        internal static bool IsMenorDe18(DateTime from)
        {
            if (from.AddYears(18) < DateTime.Now)
                return false;
            else
                return true;
        }

        internal static bool IsCedulaVigente(DateTime expirationDate)
        {
            bool ret;
            try
            {
                LOG.Debug("Utils.IsCedulaVigente IN - expirationDate = " + expirationDate);
                DateTime dt = expirationDate; // DateTime.Parse(expirationDate);
                LOG.Debug("Utils.IsCedulaVigente Now = " + DateTime.Now.ToString("dd/MM/yyyy"));
                ret = (DateTime.Now < dt);
                LOG.Debug("Utils.IsCedulaVigente => " + ret.ToString());
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("Utils.IsCedulaVigente Error = " + ex.Message, ex);
            }
            return ret;
        }

        internal static string ToStringDictionary(Dictionary<string, string> obj)
        {
            string ret = "";
            bool isFirst = true;
            object oAux = null;
            try
            {
                LOG.Debug("Utils.ToStringDictionary IN...");
                if (obj != null) {
                    foreach (var item in obj)
                    {
                        if (isFirst)
                        {
                            isFirst = false;
                            ret = item.Key + " = " + item.Value.ToString();
                        }
                        else
                        {
                            ret += "|" + item.Key + " = " + item.Value.ToString();
                        }
                    }
                } else
                {
                    ret = "";
                }
            }
            catch (Exception ex)
            {
                ret = "Utils.ToStringDictionary Excp Error: " + ex.Message;
                LOG.Error("Utils.ToStringDictionary Excp Error: ", ex);
            }
            LOG.Debug("Utils.ToStringDictionary OUT!");
            return ret;
        }
    }
}
