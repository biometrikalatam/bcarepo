﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BVIOem.Atm.libs
{
    internal class Model
    {
    }

    /// <summary>
    /// <para>PersonData es el objeto devuelto serializado en JSON con la información requerida, tal como
    /// RUR, Nombre, Apellido, Serial, Fechas de Nacimiento y Expiracion, etc. </para>
    /// <para>Los datos biométricos también son devueltos si se piden. Aqui se describen los formatos devueltos:
    /// <list type="bullet">
    ///     <listheader>Formatos Biometricos</listheader>    
    ///     <description>Todos se devuelven en Base 64, y deben convertirse de acuerdo a su formato.</description>
    ///     <item>
    ///         <term>RAW</term>
    ///         <description>Captura de huella en formato RAW</description>
    ///     </item>
    ///     <item>
    ///         <term>WSQ</term>
    ///         <description>Captura de huella en formato comprimido WSQ, certificado por el FBI.</description>
    ///     </item>
    ///     <item>
    ///         <term>Ansi</term>
    ///         <description>Captura de huella en formato estandard ANSI</description>
    ///     </item>
    ///     <item>
    ///         <term>Iso</term>
    ///         <description>Captura de huella en formato estandard ISO</description>
    ///     </item>
    ///     <item>
    ///         <term>IsoCompact</term>
    ///         <description>Captura de huella en formato estandard ISOCOMPACT</description>
    ///     </item>
    ///     <item>
    ///         <term>JpegOriginal</term>
    ///         <description>Imágen de huella capturada en formato JPG.</description>
    ///     </item>
    ///  </list>
    /// </para>
    /// </summary>
    public class PersonData
    {

        public PersonData() { }

        [JsonConverter(typeof(StringEnumConverter))]
        public TipoIdentificacion TipoIdentificacion { get; set; }
        /// <summary>
        /// Foto extraída desde chip de la cédula  en formato JPG
        /// </summary>
        public string Foto { get; set; }
        /// <summary>
        /// Imágen de firma extraída desde chip de la cédula en formato JPG
        /// </summary>
        public string FotoFirma { get; set; }
        /// <summary>
        /// RUT leído desde la cédula en formato NNNNNNNN-N
        /// </summary>
        public string Rut { get; set; }
        /// <summary>
        /// Nombres de la persona
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Apellidos Paterno y Materno (si existe)
        /// </summary>
        public string Apellido { get; set; }
        /// <summary>
        /// Seco obtenido desde la cédula [M | F]
        /// </summary>
        public string Sexo { get; set; }
        /// <summary>
        /// Número de serie de la cédula
        /// </summary>
        public string Serie { get; set; }
        /// <summary>
        /// Nacionalidad obtenida
        /// </summary>
        public string Nacionalidad { get; set; }
        /// <summary>
        /// Fecha de nacimiento obtenida
        /// </summary>
        public DateTime? FechaNacimiento { get; set; }
        /// <summary>
        /// Fecha de expiración de la cédula procesada
        /// </summary>
        public DateTime? FechaExpiracion { get; set; }
        /// <summary>
        /// Valores posibles: true si la fecha de vencimiento es posterior a la fecha en que se procesa, o 
        /// false si la cédula está vencida.
        /// </summary>
        public bool IsCedulaVigente { get; set; }
        /// <summary>
        /// Valores posibles: true si la cédula corresponde a un menor de edad, y false si es mayor.
        /// </summary>
        public bool IsMenorDeEdad { get; set; }

        //Result
        /// <summary>
        /// Número de dedo verificado. 
        /// <list type="bullet">
        ///     <listheader>Números de Dedos</listheader>    
        ///     <description>Se utiliza esta nuemración de dedos de acuerdo al estandard internacional</description>
        ///     <item>
        ///         <term>1</term>
        ///         <description>Dedo Pulgar Derecho</description>
        ///     </item>
        ///     <item>
        ///         <term>2</term>
        ///         <description>Dedo Indice Derecho</description>
        ///     </item>
        ///     <item>
        ///         <term>3</term>
        ///         <description>Dedo Mayor Derecho</description>
        ///     </item>
        ///     <item>
        ///         <term>4</term>
        ///         <description>Dedo Anular Derecho</description>
        ///     </item>
        ///     <item>
        ///         <term>5</term>
        ///         <description>Dedo Meñique Derecho</description>
        ///     </item>
        ///     <item>
        ///         <term>6</term>
        ///         <description>Dedo Anular Izquierdo</description>
        ///     </item>
        ///     <item>
        ///         <term>7</term>
        ///         <description>Dedo Indice Izquierdo</description>
        ///     </item>
        ///     <item>
        ///         <term>8</term>
        ///         <description>Dedo Mayor Izquierdo</description>
        ///     </item>
        ///     <item>
        ///         <term>9</term>
        ///         <description>Dedo Anular Izquierdo</description>
        ///     </item>
        ///     <item>
        ///         <term>10</term>
        ///         <description>Dedo Meñique Izquierdo</description>
        ///     </item>
        ///   </list>
        /// </summary>
        public int Finger { get; set; }
        /// <summary>
        /// Contiene el resultado de la verificación.
        /// <list type="bullet">
        ///     <listheader>Valores Posibles</listheader>    
        ///     <description>Valores de verificación según sea la etapa en que se encuentre el proceso</description>
        ///     <item>
        ///         <term>0</term>
        ///         <description>Aun no se verificó</description>
        ///     </item>
        ///     <item>
        ///         <term>1</term>
        ///         <description>Verificación  <font color="green"><b>POSITIVA</b></font></description>
        ///     </item>
        ///     <item>
        ///         <term>2</term>
        ///         <description>Verificación <font color="red"><b>NEGATIVA</b></font></description>
        ///     </item>
        ///  </list>
        /// </summary>
        public int VerifyResult { get; set; } // 0-Aun no verifico | 1-Positivo | 2-Negativo
        /// <summary>
        /// Valor de score. En el caso de cédula nueva solo es 100 si VerifyResult es igual a 1, o 0 si VerifyResult es igual a 2.
        /// </summary>
        public double Score { get; set; }

        //Samples
        /// <summary>
        /// Huella en el formato segun su nombre
        /// </summary>
        public string Ansi { get; set; }
        /// <summary>
        /// Huella en el formato segun su nombre
        /// </summary>
        public string Iso { get; set; }
        /// <summary>
        /// Huella en el formato segun su nombre
        /// </summary>
        public string[] IsoCompact { get; set; }
        /// <summary>
        /// Huella en el formato segun su nombre
        /// </summary>
        public string Raw { get; set; }
        /// <summary>
        /// Huella en el formato segun su nombre
        /// </summary>
        public string Wsq { get; set; }
        /// <summary>
        /// Huella en el formato segun su nombre
        /// </summary>
        public string JpegOriginal { get; set; }

        public string GetNameTipoIdentificacion()
        {
            if (TipoIdentificacion == TipoIdentificacion.CedulaChilenaAntigua)
                return "Cedula Chilena Antigua";
            if (TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
                return "Cedula Chilena Nueva";
            return "Indeterminado";

        }
    }

    /// <summary>
    /// Procesos de Verificacion Disponibles
    /// </summary>
    public enum TipoIdentificacion
    {
        Indeterminado = 0,
        CedulaChilenaAntigua = 1,
        CedulaChilenaNueva = 2
    }
}
