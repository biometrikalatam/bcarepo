﻿using log4net;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

/// <summary>
/// Componente de verificación de identidad, a través de huella dactilar, entre captura realizada
/// en lector Digital Persona o Secugen, y la cédula de identidad Chilena Nueva.
/// </summary>
namespace Biometrika.BVIOem.Atm.libs
{
    internal class ImageProcessor
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ImageProcessor));

        static public string GetBase64Image(Image image)
        {
            string sRet = null;
            try
            {
                Bitmap bmp = new Bitmap(image);
                using (MemoryStream msb = new MemoryStream())
                {
                    bmp.Save(msb, ImageFormat.Jpeg);
                    sRet = Convert.ToBase64String(msb.ToArray());
                }
            }
            catch (Exception ex)
            {
                LOG.Error("ImageProcessor.GetBase64Image", ex);
                sRet = null;
            }
            return sRet;
        }
    }
}
