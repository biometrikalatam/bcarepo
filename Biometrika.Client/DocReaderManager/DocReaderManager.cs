﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Domain;

namespace DocReaderManager
{
    
    public class DocReaderManager : IManager
    {
        private bool isOpen = false;
        private Dictionary<string, object> output = new Dictionary<string, object>();

        public DocReaderManager()
        {
        }  // ctor para el relexion

        public DocReaderManager(NameValueCollection queryString)
        {
            if (!isOpen)
            {
                isOpen = true;
                DocReaderUIAdapter.Program.SetPacket(queryString);      // el program recibe el pedido
                DocReaderUIAdapter.Program.Main();

                output = DocReaderUIAdapter.Program.Packet.PacketParamOut;

            }
        }

        public string Buffer => new JavaScriptSerializer().Serialize(output);

        public string Tag { get => "DR"; }

    }
}
