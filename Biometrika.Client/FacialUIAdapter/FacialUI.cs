﻿using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacialUIAdapter
{
    public partial class FacialUI : Form
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(FacialUI));

        bool _HayCapturaOK = false;

        bool typeExecutionGUI = false;
        private System.Timers.Timer timerControlGeneral;
        public string SerialSensor { get; set; }
        private BioPacket bioPacketLocal;


        //public int Tokencontent { get; set; }
        public bool HaveData { get; set; }

        public FacialUI()
        {
            InitializeComponent();
        }

        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        internal int Channel(BioPacket bioPacket)
        {
            _HayCapturaOK = false;
            bioPacketLocal = bioPacket;
            //if (bioPacketLocal.PacketParamOut == null)
            //    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            int ret = 0; // CheckParameters(bioPacketLocal);

            if (ret == 0)
            {
                if (bioPacketLocal.PacketParamOut == null)
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
                //Chequea si es con o sin visualización
                //NameValueCollection qs = bioPacketLocal.PacketParamIn;
                //if (String.Equals(dictParamIn["GUI"], "false"))
                //{
                //    this.Visible = false;
                //    typeExecutionGUI = false;
                //}
                //else
                //{
                //    typeExecutionGUI = true;
                //}

                //int timeout = 2000;
                //try
                //{
                //    timeout = Convert.ToInt32(dictParamIn["Timeout"]);
                //}
                //catch (Exception ex)
                //{
                //    timeout = 5000;
                //}
                //if (timeout == 0) timeout = 5000;
                //labtimeout.Text = timeout.ToString();
                //labtimeout.Refresh();
                /// Timer del control general, se cierra en 10 segundos
                //timerControlGeneral = new System.Timers.Timer(timeout);
                //timerControlGeneral.Elapsed += TimerControlGeneral_Elapsed;
                //timerControlGeneral.Enabled = true;
            }
            return ret;
        }
    }
}
