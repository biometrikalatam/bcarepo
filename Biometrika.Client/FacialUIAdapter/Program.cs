﻿using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacialUIAdapter
{
    public static class Program
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Program));

        public static bool HaveData
        {
            get
            {
                return facialUI == null ? false : facialUI.HaveData;
            }
        }
        public static BioPacket Packet { get; set; }

        private static FacialUI facialUI;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            try
            {
                LOG.Debug("FacialUIAdapter.Main IN...");
                Application.EnableVisualStyles();
                //Application.SetCompatibleTextRenderingDefault(false);
                LOG.Debug("FacialUIAdapter.Main New FacialUI");
                facialUI = new FacialUI();
                LOG.Debug("FacialUIAdapter.Main Set Channel...");
                int ret = facialUI.Channel(Packet);  //chequea que tenga los parámetros correctos ingresados y los pasa a dictionary
                if (ret == 0)
                {
                    LOG.Debug("FacialUIAdapter.Main Application.Run(facialUI)...");
                    Application.Run(facialUI);
                }
                else
                {
                    LOG.Error("Error seteando channel = " + GetError(ret));
                    Packet.PacketParamOut.Add("Error", GetError(ret));
                }

                //fingerScanerUI = new FingerScannerUI();
                //fingerScanerUI.Channel(Packet);
                //Application.Run(fingerScanerUI);
            }
            catch (Exception ex)
            {
                LOG.Error("FacialUIAdapter.Main Error = " + ex.Message); 
            }
            
        }

        private static string GetError(int ret)
        {
            string sret = null;
            switch (ret)
            {
                case -1:
                    sret = "-1|Error desconocido. Ver log";
                    break;
                case -2:
                    sret = "-2|Parametros nulos";
                    break;
                case -3:
                    sret = "-3|Parametros src/TypeId/ValueId denben existir y no pueden ser nulos";
                    break;
                default:
                    sret = "-1|Error desconocido. Ver log";
                    break;
            }
            return sret;
        }

        public static void SetPacket(NameValueCollection queryString)
        {
            Packet = new BioPacket();
            Packet.PacketParamIn = queryString;
            Packet.PacketParamOut = new Dictionary<string, object>();
        }

        public static void Close()
        {
            facialUI.Close();
            //cameraUI.Dispose(true);
            facialUI = null;
        }
    }
}
