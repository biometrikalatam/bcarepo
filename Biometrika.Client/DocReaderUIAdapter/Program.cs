﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domain;

namespace DocReaderUIAdapter
{
    public static class Program
    {
        ///// <summary>
        ///// The main entry point for the application.
        ///// </summary>
        //[STAThread]
        //public static void Main()
        //{
        //    Application.EnableVisualStyles();
        //    Application.SetCompatibleTextRenderingDefault(false);
        //    Application.Run(new DocReaderUI());
        //}
        public static bool HaveData
        {
            get
            {
                return docReaderUI == null ? false : docReaderUI.HaveData;
            }
        }
        public static BioPacket Packet { get; set; }

        private static DocReaderUI docReaderUI;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            docReaderUI = new DocReaderUI();
            int ret = docReaderUI.Channel(Packet);
            if (ret == 0)
            {
                docReaderUI.Show();
                Application.Run(docReaderUI);
            }
            else
            {
                Packet.PacketParamOut.Add("Error", GetError(ret));
            }
        }

        private static string GetError(int ret)
        {
            string sret = null;
            switch (ret)
            {
                case -1:
                    sret = "-1|Error desconocido. Ver log";
                    break;
                case -2:
                    sret = "-2|Parametros nulos";
                    break;
                case -3:
                    sret = "-3|Parametros src/TypeId/ValueId denben existir y no pueden ser nulos";
                    break;
                default:
                    sret = "-1|Error desconocido. Ver log";
                    break;
            }
            return sret;
        }

        public static void SetPacket(NameValueCollection queryString)
        {
            Packet = new BioPacket();
            Packet.PacketParamIn = queryString;
            Packet.PacketParamOut = new Dictionary<string, object>();
        }

        public static void Close()
        {
            docReaderUI.Close();
            docReaderUI = null;
        }
    }
}
