﻿namespace DocReaderUIAdapter
{
    partial class DocReaderUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imageCam = new System.Windows.Forms.PictureBox();
            this.labSexo = new System.Windows.Forms.Label();
            this.labNacionalidad = new System.Windows.Forms.Label();
            this.labFNac = new System.Windows.Forms.Label();
            this.labNombre = new System.Windows.Forms.Label();
            this.labRUT = new System.Windows.Forms.Label();
            this.imageCedulaFront = new System.Windows.Forms.PictureBox();
            this.imageCedulaBack = new System.Windows.Forms.PictureBox();
            this.timer_VerifyCedula = new System.Windows.Forms.Timer(this.components);
            this.timer_GeneratingCI = new System.Windows.Forms.Timer(this.components);
            this.labBloqueoSRCeI = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labClose = new System.Windows.Forms.Label();
            this.labCaturaCedula = new System.Windows.Forms.Label();
            this.labStartPreview = new System.Windows.Forms.Label();
            this.labStopPreview = new System.Windows.Forms.Label();
            this.labVigencia = new System.Windows.Forms.Label();
            this.labFVto = new System.Windows.Forms.Label();
            this.LabRutTitle = new System.Windows.Forms.Label();
            this.labReInit = new System.Windows.Forms.Label();
            this.picLogo3ro = new System.Windows.Forms.PictureBox();
            this.timer_Close = new System.Windows.Forms.Timer(this.components);
            this.labMsgGuia = new System.Windows.Forms.Label();
            this.pbProcess = new System.Windows.Forms.ProgressBar();
            this.timer_ShowProgress = new System.Windows.Forms.Timer(this.components);
            this.labProcesando = new System.Windows.Forms.Label();
            this.labHelp = new System.Windows.Forms.Label();
            this.labAbout = new System.Windows.Forms.Label();
            this.picBloqueo = new System.Windows.Forms.PictureBox();
            this.imgFoto = new System.Windows.Forms.PictureBox();
            this.imgFirma = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.grpCedulaGet = new System.Windows.Forms.GroupBox();
            this.labSerial = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.idCardDetectorControl = new Namku.UI.WindowsForm.IDCardDetector();
            this.cboCamaras = new System.Windows.Forms.ComboBox();
            this.labCamaras = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCedulaFront)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCedulaBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo3ro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBloqueo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFirma)).BeginInit();
            this.grpCedulaGet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.idCardDetectorControl)).BeginInit();
            this.SuspendLayout();
            // 
            // imageCam
            // 
            this.imageCam.BackColor = System.Drawing.Color.Transparent;
            this.imageCam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageCam.Image = global::DocReaderUIAdapter.Properties.Resources.play_video_bg;
            this.imageCam.Location = new System.Drawing.Point(75, 106);
            this.imageCam.Name = "imageCam";
            this.imageCam.Size = new System.Drawing.Size(419, 234);
            this.imageCam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageCam.TabIndex = 0;
            this.imageCam.TabStop = false;
            // 
            // labSexo
            // 
            this.labSexo.AutoSize = true;
            this.labSexo.BackColor = System.Drawing.Color.Transparent;
            this.labSexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSexo.ForeColor = System.Drawing.Color.DimGray;
            this.labSexo.Location = new System.Drawing.Point(85, 269);
            this.labSexo.Name = "labSexo";
            this.labSexo.Size = new System.Drawing.Size(19, 16);
            this.labSexo.TabIndex = 21;
            this.labSexo.Text = "M";
            // 
            // labNacionalidad
            // 
            this.labNacionalidad.AutoSize = true;
            this.labNacionalidad.BackColor = System.Drawing.Color.Transparent;
            this.labNacionalidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNacionalidad.ForeColor = System.Drawing.Color.DimGray;
            this.labNacionalidad.Location = new System.Drawing.Point(187, 232);
            this.labNacionalidad.Name = "labNacionalidad";
            this.labNacionalidad.Size = new System.Drawing.Size(37, 16);
            this.labNacionalidad.TabIndex = 19;
            this.labNacionalidad.Text = "ARG";
            // 
            // labFNac
            // 
            this.labFNac.AutoSize = true;
            this.labFNac.BackColor = System.Drawing.Color.Transparent;
            this.labFNac.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labFNac.ForeColor = System.Drawing.Color.DimGray;
            this.labFNac.Location = new System.Drawing.Point(84, 232);
            this.labFNac.Name = "labFNac";
            this.labFNac.Size = new System.Drawing.Size(72, 16);
            this.labFNac.TabIndex = 17;
            this.labFNac.Text = "28/09/1969";
            // 
            // labNombre
            // 
            this.labNombre.BackColor = System.Drawing.Color.Transparent;
            this.labNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNombre.ForeColor = System.Drawing.Color.DimGray;
            this.labNombre.Location = new System.Drawing.Point(82, 178);
            this.labNombre.Name = "labNombre";
            this.labNombre.Size = new System.Drawing.Size(208, 33);
            this.labNombre.TabIndex = 15;
            this.labNombre.Text = "Gustavo Gerardo Suhit Gallucci Cortadi Ferretti";
            this.labNombre.Click += new System.EventHandler(this.labNombre_Click);
            // 
            // labRUT
            // 
            this.labRUT.AutoSize = true;
            this.labRUT.BackColor = System.Drawing.Color.Transparent;
            this.labRUT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labRUT.ForeColor = System.Drawing.Color.DimGray;
            this.labRUT.Location = new System.Drawing.Point(131, 136);
            this.labRUT.Name = "labRUT";
            this.labRUT.Size = new System.Drawing.Size(75, 16);
            this.labRUT.TabIndex = 13;
            this.labRUT.Text = "21284415-2";
            // 
            // imageCedulaFront
            // 
            this.imageCedulaFront.BackColor = System.Drawing.Color.Transparent;
            this.imageCedulaFront.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageCedulaFront.Image = global::DocReaderUIAdapter.Properties.Resources.front;
            this.imageCedulaFront.InitialImage = global::DocReaderUIAdapter.Properties.Resources.front;
            this.imageCedulaFront.Location = new System.Drawing.Point(17, 40);
            this.imageCedulaFront.Name = "imageCedulaFront";
            this.imageCedulaFront.Size = new System.Drawing.Size(125, 80);
            this.imageCedulaFront.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageCedulaFront.TabIndex = 1;
            this.imageCedulaFront.TabStop = false;
            // 
            // imageCedulaBack
            // 
            this.imageCedulaBack.BackColor = System.Drawing.Color.Transparent;
            this.imageCedulaBack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageCedulaBack.Image = global::DocReaderUIAdapter.Properties.Resources.back;
            this.imageCedulaBack.Location = new System.Drawing.Point(160, 40);
            this.imageCedulaBack.Name = "imageCedulaBack";
            this.imageCedulaBack.Size = new System.Drawing.Size(125, 80);
            this.imageCedulaBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageCedulaBack.TabIndex = 2;
            this.imageCedulaBack.TabStop = false;
            // 
            // timer_VerifyCedula
            // 
            this.timer_VerifyCedula.Enabled = true;
            this.timer_VerifyCedula.Tick += new System.EventHandler(this.timer_VerifyCedula_Tick);
            // 
            // timer_GeneratingCI
            // 
            this.timer_GeneratingCI.Interval = 200;
            this.timer_GeneratingCI.Tick += new System.EventHandler(this.timer_GeneratingCI_Tick);
            // 
            // labBloqueoSRCeI
            // 
            this.labBloqueoSRCeI.BackColor = System.Drawing.Color.Transparent;
            this.labBloqueoSRCeI.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labBloqueoSRCeI.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labBloqueoSRCeI.Location = new System.Drawing.Point(96, 313);
            this.labBloqueoSRCeI.Name = "labBloqueoSRCeI";
            this.labBloqueoSRCeI.Size = new System.Drawing.Size(128, 30);
            this.labBloqueoSRCeI.TabIndex = 15;
            this.labBloqueoSRCeI.Text = "No Bloqueada";
            this.labBloqueoSRCeI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labBloqueoSRCeI.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gray;
            this.label5.Location = new System.Drawing.Point(32, 475);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 20);
            this.label5.TabIndex = 14;
            this.label5.Text = "Track ID ";
            this.label5.Visible = false;
            // 
            // labClose
            // 
            this.labClose.BackColor = System.Drawing.Color.Transparent;
            this.labClose.Location = new System.Drawing.Point(755, 7);
            this.labClose.Name = "labClose";
            this.labClose.Size = new System.Drawing.Size(28, 27);
            this.labClose.TabIndex = 15;
            this.labClose.Click += new System.EventHandler(this.labClose_Click);
            // 
            // labCaturaCedula
            // 
            this.labCaturaCedula.BackColor = System.Drawing.Color.Transparent;
            this.labCaturaCedula.Enabled = false;
            this.labCaturaCedula.Location = new System.Drawing.Point(459, 78);
            this.labCaturaCedula.Name = "labCaturaCedula";
            this.labCaturaCedula.Size = new System.Drawing.Size(18, 17);
            this.labCaturaCedula.TabIndex = 22;
            this.labCaturaCedula.Click += new System.EventHandler(this.labCaturaCedula_Click);
            // 
            // labStartPreview
            // 
            this.labStartPreview.BackColor = System.Drawing.Color.Transparent;
            this.labStartPreview.Location = new System.Drawing.Point(419, 78);
            this.labStartPreview.Name = "labStartPreview";
            this.labStartPreview.Size = new System.Drawing.Size(19, 17);
            this.labStartPreview.TabIndex = 24;
            this.labStartPreview.Click += new System.EventHandler(this.labStartPreview_Click);
            // 
            // labStopPreview
            // 
            this.labStopPreview.BackColor = System.Drawing.Color.Transparent;
            this.labStopPreview.Enabled = false;
            this.labStopPreview.Location = new System.Drawing.Point(442, 78);
            this.labStopPreview.Name = "labStopPreview";
            this.labStopPreview.Size = new System.Drawing.Size(13, 17);
            this.labStopPreview.TabIndex = 25;
            this.labStopPreview.Click += new System.EventHandler(this.labStopPreview_Click);
            // 
            // labVigencia
            // 
            this.labVigencia.BackColor = System.Drawing.Color.Transparent;
            this.labVigencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labVigencia.ForeColor = System.Drawing.Color.Green;
            this.labVigencia.Location = new System.Drawing.Point(397, 488);
            this.labVigencia.Name = "labVigencia";
            this.labVigencia.Size = new System.Drawing.Size(141, 18);
            this.labVigencia.TabIndex = 27;
            this.labVigencia.Text = "Cédula Vigente!";
            this.labVigencia.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labVigencia.Visible = false;
            this.labVigencia.Click += new System.EventHandler(this.labVigencia_Click);
            // 
            // labFVto
            // 
            this.labFVto.AutoSize = true;
            this.labFVto.BackColor = System.Drawing.Color.Transparent;
            this.labFVto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labFVto.ForeColor = System.Drawing.Color.DimGray;
            this.labFVto.Location = new System.Drawing.Point(180, 269);
            this.labFVto.Name = "labFVto";
            this.labFVto.Size = new System.Drawing.Size(72, 16);
            this.labFVto.TabIndex = 29;
            this.labFVto.Text = "22/07/2022";
            // 
            // LabRutTitle
            // 
            this.LabRutTitle.AutoSize = true;
            this.LabRutTitle.BackColor = System.Drawing.Color.Transparent;
            this.LabRutTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabRutTitle.ForeColor = System.Drawing.Color.Gray;
            this.LabRutTitle.Location = new System.Drawing.Point(266, 40);
            this.LabRutTitle.Name = "LabRutTitle";
            this.LabRutTitle.Size = new System.Drawing.Size(127, 25);
            this.LabRutTitle.TabIndex = 31;
            this.LabRutTitle.Text = "21284415-2";
            this.LabRutTitle.Click += new System.EventHandler(this.LabRutTitle_Click);
            // 
            // labReInit
            // 
            this.labReInit.BackColor = System.Drawing.Color.Transparent;
            this.labReInit.Location = new System.Drawing.Point(621, 7);
            this.labReInit.Name = "labReInit";
            this.labReInit.Size = new System.Drawing.Size(31, 27);
            this.labReInit.TabIndex = 32;
            this.labReInit.Click += new System.EventHandler(this.labReInit_Click);
            // 
            // picLogo3ro
            // 
            this.picLogo3ro.BackColor = System.Drawing.Color.Transparent;
            this.picLogo3ro.Location = new System.Drawing.Point(580, 450);
            this.picLogo3ro.Margin = new System.Windows.Forms.Padding(2);
            this.picLogo3ro.Name = "picLogo3ro";
            this.picLogo3ro.Size = new System.Drawing.Size(200, 45);
            this.picLogo3ro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo3ro.TabIndex = 45;
            this.picLogo3ro.TabStop = false;
            // 
            // timer_Close
            // 
            this.timer_Close.Tick += new System.EventHandler(this.timer_Close_Tick);
            // 
            // labMsgGuia
            // 
            this.labMsgGuia.BackColor = System.Drawing.Color.Transparent;
            this.labMsgGuia.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMsgGuia.ForeColor = System.Drawing.Color.Gray;
            this.labMsgGuia.Location = new System.Drawing.Point(53, 378);
            this.labMsgGuia.Name = "labMsgGuia";
            this.labMsgGuia.Size = new System.Drawing.Size(414, 44);
            this.labMsgGuia.TabIndex = 46;
            this.labMsgGuia.Text = "Inicie previsualización,  y siga los pasos propuestos...";
            // 
            // pbProcess
            // 
            this.pbProcess.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pbProcess.Location = new System.Drawing.Point(48, 470);
            this.pbProcess.Maximum = 1000;
            this.pbProcess.Name = "pbProcess";
            this.pbProcess.Size = new System.Drawing.Size(419, 5);
            this.pbProcess.Step = 1;
            this.pbProcess.TabIndex = 49;
            this.pbProcess.Visible = false;
            // 
            // timer_ShowProgress
            // 
            this.timer_ShowProgress.Tick += new System.EventHandler(this.timer_ShowProgress_Tick);
            // 
            // labProcesando
            // 
            this.labProcesando.BackColor = System.Drawing.Color.White;
            this.labProcesando.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labProcesando.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labProcesando.ForeColor = System.Drawing.Color.SteelBlue;
            this.labProcesando.Location = new System.Drawing.Point(56, 203);
            this.labProcesando.Name = "labProcesando";
            this.labProcesando.Size = new System.Drawing.Size(399, 25);
            this.labProcesando.TabIndex = 50;
            this.labProcesando.Text = "Procesando. Espere...";
            this.labProcesando.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labProcesando.Visible = false;
            // 
            // labHelp
            // 
            this.labHelp.BackColor = System.Drawing.Color.Transparent;
            this.labHelp.Location = new System.Drawing.Point(663, 6);
            this.labHelp.Name = "labHelp";
            this.labHelp.Size = new System.Drawing.Size(32, 29);
            this.labHelp.TabIndex = 51;
            // 
            // labAbout
            // 
            this.labAbout.BackColor = System.Drawing.Color.Transparent;
            this.labAbout.Location = new System.Drawing.Point(709, 6);
            this.labAbout.Name = "labAbout";
            this.labAbout.Size = new System.Drawing.Size(32, 29);
            this.labAbout.TabIndex = 52;
            // 
            // picBloqueo
            // 
            this.picBloqueo.BackColor = System.Drawing.Color.Transparent;
            this.picBloqueo.Image = global::DocReaderUIAdapter.Properties.Resources.srcei_50;
            this.picBloqueo.InitialImage = global::DocReaderUIAdapter.Properties.Resources.srcei_50;
            this.picBloqueo.Location = new System.Drawing.Point(58, 313);
            this.picBloqueo.Name = "picBloqueo";
            this.picBloqueo.Size = new System.Drawing.Size(31, 30);
            this.picBloqueo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBloqueo.TabIndex = 53;
            this.picBloqueo.TabStop = false;
            this.picBloqueo.Visible = false;
            // 
            // imgFoto
            // 
            this.imgFoto.BackColor = System.Drawing.Color.Transparent;
            this.imgFoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgFoto.Image = global::DocReaderUIAdapter.Properties.Resources.no_picture;
            this.imgFoto.Location = new System.Drawing.Point(17, 131);
            this.imgFoto.Name = "imgFoto";
            this.imgFoto.Size = new System.Drawing.Size(60, 59);
            this.imgFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgFoto.TabIndex = 54;
            this.imgFoto.TabStop = false;
            // 
            // imgFirma
            // 
            this.imgFirma.BackColor = System.Drawing.Color.Transparent;
            this.imgFirma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgFirma.Image = global::DocReaderUIAdapter.Properties.Resources.no_signature;
            this.imgFirma.Location = new System.Drawing.Point(17, 218);
            this.imgFirma.Name = "imgFirma";
            this.imgFirma.Size = new System.Drawing.Size(60, 46);
            this.imgFirma.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgFirma.TabIndex = 55;
            this.imgFirma.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(18, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 16);
            this.label1.TabIndex = 56;
            this.label1.Text = "Frente";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(167, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 16);
            this.label2.TabIndex = 57;
            this.label2.Text = "Dorso";
            // 
            // grpCedulaGet
            // 
            this.grpCedulaGet.BackColor = System.Drawing.Color.Transparent;
            this.grpCedulaGet.Controls.Add(this.labSerial);
            this.grpCedulaGet.Controls.Add(this.label10);
            this.grpCedulaGet.Controls.Add(this.imageCedulaFront);
            this.grpCedulaGet.Controls.Add(this.label9);
            this.grpCedulaGet.Controls.Add(this.label8);
            this.grpCedulaGet.Controls.Add(this.label7);
            this.grpCedulaGet.Controls.Add(this.label6);
            this.grpCedulaGet.Controls.Add(this.label4);
            this.grpCedulaGet.Controls.Add(this.label3);
            this.grpCedulaGet.Controls.Add(this.label2);
            this.grpCedulaGet.Controls.Add(this.labFNac);
            this.grpCedulaGet.Controls.Add(this.label1);
            this.grpCedulaGet.Controls.Add(this.labNacionalidad);
            this.grpCedulaGet.Controls.Add(this.imgFirma);
            this.grpCedulaGet.Controls.Add(this.labSexo);
            this.grpCedulaGet.Controls.Add(this.imgFoto);
            this.grpCedulaGet.Controls.Add(this.imageCedulaBack);
            this.grpCedulaGet.Controls.Add(this.picBloqueo);
            this.grpCedulaGet.Controls.Add(this.labRUT);
            this.grpCedulaGet.Controls.Add(this.labNombre);
            this.grpCedulaGet.Controls.Add(this.labBloqueoSRCeI);
            this.grpCedulaGet.Controls.Add(this.labFVto);
            this.grpCedulaGet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCedulaGet.ForeColor = System.Drawing.Color.DimGray;
            this.grpCedulaGet.Location = new System.Drawing.Point(483, 79);
            this.grpCedulaGet.Name = "grpCedulaGet";
            this.grpCedulaGet.Size = new System.Drawing.Size(306, 358);
            this.grpCedulaGet.TabIndex = 58;
            this.grpCedulaGet.TabStop = false;
            this.grpCedulaGet.Text = "Datos Capturados...";
            // 
            // labSerial
            // 
            this.labSerial.AutoSize = true;
            this.labSerial.BackColor = System.Drawing.Color.Transparent;
            this.labSerial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSerial.ForeColor = System.Drawing.Color.DimGray;
            this.labSerial.Location = new System.Drawing.Point(140, 289);
            this.labSerial.Name = "labSerial";
            this.labSerial.Size = new System.Drawing.Size(71, 16);
            this.labSerial.TabIndex = 65;
            this.labSerial.Text = "601014310";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.DimGray;
            this.label10.Location = new System.Drawing.Point(85, 289);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 16);
            this.label10.TabIndex = 64;
            this.label10.Text = "Serial:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.DimGray;
            this.label9.Location = new System.Drawing.Point(180, 252);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 16);
            this.label9.TabIndex = 63;
            this.label9.Text = "Fec. Exp.:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.DimGray;
            this.label8.Location = new System.Drawing.Point(83, 252);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 16);
            this.label8.TabIndex = 62;
            this.label8.Text = "Sexo:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(180, 215);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 16);
            this.label7.TabIndex = 61;
            this.label7.Text = "Nacionalidad:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DimGray;
            this.label6.Location = new System.Drawing.Point(84, 215);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 16);
            this.label6.TabIndex = 60;
            this.label6.Text = "Fec. Nac.:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(83, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 16);
            this.label4.TabIndex = 59;
            this.label4.Text = "Nombre:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(82, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 16);
            this.label3.TabIndex = 58;
            this.label3.Text = "RUN:";
            // 
            // idCardDetectorControl
            // 
            this.idCardDetectorControl.BackgroundImage = global::DocReaderUIAdapter.Properties.Resources.play_video_bg;
            this.idCardDetectorControl.BlurrinessFailedIcon = null;
            this.idCardDetectorControl.BlurrinessIconRect = new System.Drawing.Rectangle(8, 8, 64, 64);
            this.idCardDetectorControl.BlurrinessOkIcon = null;
            this.idCardDetectorControl.BlurrinessThreshold = 0D;
            this.idCardDetectorControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.idCardDetectorControl.DetectionFps = 3D;
            this.idCardDetectorControl.DrawFaces = true;
            this.idCardDetectorControl.DrawIDCardContour = true;
            this.idCardDetectorControl.IDFaceMinSizeRatio = 0D;
            this.idCardDetectorControl.Location = new System.Drawing.Point(48, 106);
            this.idCardDetectorControl.Name = "idCardDetectorControl";
            this.idCardDetectorControl.Roi = 0.8D;
            this.idCardDetectorControl.Size = new System.Drawing.Size(419, 234);
            this.idCardDetectorControl.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.idCardDetectorControl.TabIndex = 59;
            this.idCardDetectorControl.TabStop = false;
            this.idCardDetectorControl.Visible = false;
            // 
            // cboCamaras
            // 
            this.cboCamaras.FormattingEnabled = true;
            this.cboCamaras.Location = new System.Drawing.Point(116, 76);
            this.cboCamaras.Name = "cboCamaras";
            this.cboCamaras.Size = new System.Drawing.Size(182, 21);
            this.cboCamaras.TabIndex = 60;
            this.cboCamaras.Visible = false;
            this.cboCamaras.SelectedIndexChanged += new System.EventHandler(this.cboCamaras_SelectedIndexChanged);
            // 
            // labCamaras
            // 
            this.labCamaras.AutoSize = true;
            this.labCamaras.BackColor = System.Drawing.Color.Transparent;
            this.labCamaras.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCamaras.ForeColor = System.Drawing.Color.DimGray;
            this.labCamaras.Location = new System.Drawing.Point(42, 79);
            this.labCamaras.Name = "labCamaras";
            this.labCamaras.Size = new System.Drawing.Size(72, 15);
            this.labCamaras.TabIndex = 64;
            this.labCamaras.Text = "Camaras :";
            this.labCamaras.Visible = false;
            // 
            // DocReaderUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::DocReaderUIAdapter.Properties.Resources.BCReader_drafv2;
            this.ClientSize = new System.Drawing.Size(801, 514);
            this.Controls.Add(this.labCamaras);
            this.Controls.Add(this.cboCamaras);
            this.Controls.Add(this.labProcesando);
            this.Controls.Add(this.grpCedulaGet);
            this.Controls.Add(this.labAbout);
            this.Controls.Add(this.labHelp);
            this.Controls.Add(this.pbProcess);
            this.Controls.Add(this.labMsgGuia);
            this.Controls.Add(this.picLogo3ro);
            this.Controls.Add(this.labReInit);
            this.Controls.Add(this.LabRutTitle);
            this.Controls.Add(this.labStopPreview);
            this.Controls.Add(this.labStartPreview);
            this.Controls.Add(this.labCaturaCedula);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.imageCam);
            this.Controls.Add(this.labClose);
            this.Controls.Add(this.labVigencia);
            this.Controls.Add(this.idCardDetectorControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DocReaderUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CIUIAdapter";
            this.Load += new System.EventHandler(this.DocReaderUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCedulaFront)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCedulaBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo3ro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBloqueo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFirma)).EndInit();
            this.grpCedulaGet.ResumeLayout(false);
            this.grpCedulaGet.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.idCardDetectorControl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox imageCam;
        private System.Windows.Forms.PictureBox imageCedulaFront;
        private System.Windows.Forms.PictureBox imageCedulaBack;
        private System.Windows.Forms.Timer timer_VerifyCedula;
        private System.Windows.Forms.Label labRUT;
        private System.Windows.Forms.Label labSexo;
        private System.Windows.Forms.Label labNacionalidad;
        private System.Windows.Forms.Label labFNac;
        private System.Windows.Forms.Label labNombre;
        private System.Windows.Forms.Timer timer_GeneratingCI;
        private System.Windows.Forms.Label labBloqueoSRCeI;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labClose;
        private System.Windows.Forms.Label labCaturaCedula;
        private System.Windows.Forms.Label labStartPreview;
        private System.Windows.Forms.Label labStopPreview;
        private System.Windows.Forms.Label labVigencia;
        private System.Windows.Forms.Label labFVto;
        private System.Windows.Forms.Label LabRutTitle;
        private System.Windows.Forms.Label labReInit;
        private System.Windows.Forms.PictureBox picLogo3ro;
        private System.Windows.Forms.Timer timer_Close;
        private System.Windows.Forms.Label labMsgGuia;
        private System.Windows.Forms.ProgressBar pbProcess;
        private System.Windows.Forms.Timer timer_ShowProgress;
        private System.Windows.Forms.Label labProcesando;
        private System.Windows.Forms.Label labHelp;
        private System.Windows.Forms.Label labAbout;
        private System.Windows.Forms.PictureBox picBloqueo;
        private System.Windows.Forms.PictureBox imgFoto;
        private System.Windows.Forms.PictureBox imgFirma;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grpCedulaGet;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Namku.UI.WindowsForm.IDCardDetector idCardDetectorControl;
        private System.Windows.Forms.ComboBox cboCamaras;
        private System.Windows.Forms.Label labCamaras;
        private System.Windows.Forms.Label labSerial;
        private System.Windows.Forms.Label label10;
    }
}

