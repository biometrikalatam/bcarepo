﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net.NetworkInformation;
using System.Threading;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using DocReaderUIAdapter.src.Data;
using Domain;
using Emgu.CV;
using Emgu.CV.Structure;
using log4net;
using Namku.UI.WindowsForm;
using ZXing;

namespace DocReaderUIAdapter
{
    public partial class DocReaderUI : Form
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(DocReaderUI));

        public bool HaveData { get; set; }
        public static BioPacket Packet { get; set; }
        private BioPacket bioPacketLocal;

        internal int _CurrentError = 0;
        internal string _sCurrentError = null;

        internal Document _DOCUMENT = null;
        internal string _RUN = null;

        /// <summary>
        /// Tipo d eacciones que hará:
        ///        0 - Lee ambos lados de cedula
        ///        1 - Lee solo front
        ///        2 - Lee solo back
        /// </summary>
        internal int _DR_OperationType = 0;

        /// <summary>
        /// Habilita o no OCR en Server:
        ///          0 - No Habilitado
        ///          1 - Habilitado
        /// </summary>
        internal int _DR_OCREnabled = 1;
        
        /// <summary>
        /// Habilita o no MRZ local:
        ///          0 - No Habilitado
        ///          1 - Habilitado
        /// </summary>
        internal int _DR_MRZEnabled = 0;

        /// <summary>
        /// Wizard Automatico o no:
        ///          0 - Manual
        ///          1 - Automatico
        /// </summary>
        internal int _DR_AutomaticStep = 0;

        /// <summary>
        /// Habilita o no chequeo bloqueo x server:
        ///          0 - Habilitado
        ///          1 - No Habilitado
        /// </summary>
        internal int _DR_CheckBlocking = 0;

        /// <summary>
        /// Bloquea wizard si _DR_CheckBlocking dio Cedula Bloqueada:
        ///          0 - No Habilitado
        ///          1 - Habilitado (Bloquea el wizard y retornalo que leyo hasta el momento)
        /// </summary>
        internal int _DR_BlockBloking = 0;

        /// <summary>
        /// Bloquea wizard si fecha cedula está vencida:
        ///          0 - No Habilitado
        ///          1 - Habilitado (Bloquea el wizard y retorna lo que leyo hasta el momento)
        /// </summary>
        internal int _DR_BlockValidity = 0;

        /// <summary>
        /// Bloquea wizard si fecha cedula es menor de edad:
        ///          0 - No Habilitado
        ///          1 - Habilitado (Bloquea el wizard y retorna lo que leyo hasta el momento)
        /// </summary>
        internal int _DR_BlockAge = 0;

        /// <summary>
        /// Habilita o no Lectura Barcode local:
        ///          0 - No Habilitado
        ///          1 - Habilitado
        /// </summary>
        internal int _DR_BarcodeEnabled = 0;

        FilterInfoCollection _WEBCAMS;
        int _CURRENT_WEBCAM_ID = 0;  //Setea el id de camara para conectar 
        VideoCaptureDevice _CURRENT_WEBCAM;
        private Bitmap currentBitmapForDecoding;
        private Thread _DecodingThread;
        private bool _BARCODE_DECODE_RUNNING;
        private Result _CurrentResult;
        public BarcodeWebCam Barcodewebcam { get; private set; }
        private String BarCodeContent { get; set; }
        private readonly Pen resultRectPen;
        private byte[] resultAsBinary { get; set; }

        //private string _pathPDF = null;
        //private string _pathXML = null;
        private bool _MUSTRUN = false;
        internal string DeviceId { get; set; }
        internal string GeoRef { get; set; }
        public string DocumentId { get; set; }
        //public string Mail { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string Company { get; set; }

        //public string RDEnvelope { get; set; }
        public string TrackId { get; set; }

        private Capture captureCam;
        private RUT RUT;

        private string json;
        private bool isMenuOpen = false;
        private int timeLeft;
        private System.Timers.Timer timer;
        private System.Timers.Timer timerControlGeneral;

        private int Finger { get; set; }
        //private bool MustStop { get; set; } = false;

        private int iTinmerToShow = 10;
        private bool isStart = true;

        private bool _CAPTURING = false; //Controla si esta cpaturando para hacer stop
        private bool _VERIFING = false;

        /// <summary>
        /// Pasos a seguir:
        ///     0 - Inicio
        ///     1 - Inicio Captura Frente
        ///     1 - Inicio Captura Dorso
        /// </summary>
        private int _CURRENT_STEP = 0;

        private string _ImageCedulaFrontB64;
        private string _ImageCedulaBackB64;
        //private string _ImageSelfieB64;
        //CIParam _CIPARAM = null;

        #region DocReaderUI General Operation
        public DocReaderUI()
        {
            InitializeComponent();
        }

        private void DocReaderUI_Load(object sender, System.EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            //this.TopMost = true;
            //this.TopLevel = true;

            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;

            ReadConfigParams();

            Init();

            //CheckTmpDirectory();

            GetDeviceId();

            GetGeoRef();

            try
            {
                if (!dictParamIn["THEME"].Equals("Default")) //Si no es fondo default => Cargo fondo 
                {
                    this.BackgroundImage = Image.FromFile(dictParamIn["THEME"]);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.DocReaderUI_Load Error cargando fondo = " + dictParamIn["THEME"]);
            }

            LabRutTitle.Text = (dictParamIn!=null & 
                                dictParamIn.Keys.Contains("VALUEID")
                                & !(dictParamIn["VALUEID"].Equals("NA"))) ?dictParamIn["VALUEID"]:"Obtener...";
            ValueId = (dictParamIn != null & dictParamIn.Keys.Contains("VALUEID")) ? dictParamIn["VALUEID"] : null;

            try
            {
                picLogo3ro.Image = Image.FromFile(Properties.Settings.Default.DR_Logo3ro);
                picLogo3ro.Width = Properties.Settings.Default.DR_Logo3roW;
                picLogo3ro.Height = Properties.Settings.Default.DR_Logo3roH;
                picLogo3ro.Left = Properties.Settings.Default.DR_Logo3roX;
                picLogo3ro.Top = Properties.Settings.Default.DR_Logo3roY;
                picLogo3ro.Visible = true;
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.DocReaderUI_Load Error Cargando logo externo...");
                picLogo3ro.Visible = false;
            }

            SetCamera();

            if (_DR_BarcodeEnabled == 1)
            {
                _DecodingThread = new Thread(DecodeBarcode);
                _DecodingThread.Start();
            }

          
            //this.Disposed += OnUserControlDispose;
            //idCardDetectorControl.BlurrinessCalculated += OnBlurrinessCalculated;
            //idCardDetectorControl.IDCardDetected += OnIDCardDetected;
            //idCardDetectorControl.BlurrinessThreshold = 20;

            LOG.Debug("DocReaderUI.DocReaderUI_Load OK!");
        }

        private void ReadConfigParams()
        {
            LOG.Debug("DocReaderUI.ReadConfigParams IN...");
            try
            {
                _DR_BarcodeEnabled = Properties.Settings.Default.DR_BarcodeEnabled;
                _DR_BlockBloking = Properties.Settings.Default.DR_BlockBloking;
            }
            catch (Exception ex)
            {
                LOG.Debug("DocReaderUI.ReadConfigParams Error: " + ex.Message);
            }
            LOG.Debug("DocReaderUI.ReadConfigParams OUT!");
        }


        private System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return null
            ;
        }

        internal int Channel(BioPacket bioPacket)
        {
            int ret = 0;
            LOG.Info("DocReaderUI.Channel In...");
            bioPacketLocal = bioPacket;

            ret = CheckParameters(bioPacketLocal);

            if (ret == 0)
            {
                if (bioPacketLocal.PacketParamOut == null)
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            }
            LOG.Info("DocReaderUI.Channel Out [ret=" + ret + "]...");
            return ret;
        }

        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        private int CheckParameters(BioPacket bioPacketLocal)
        {
            /*
                 src=dr&
                 typeId=RUT&
                 valueId=21284415-2&
                 ot=0&
                 theme=Properties.Resources.BCReader_drafv2
            */
            int ret = 0;
            try
            {
                LOG.Info("DocReaderUI.CheckParameters In...");
                if (bioPacketLocal == null) ret = -2;  //Lista de parametros no puede ser nulo

                //Paso a Dictionary

                LOG.Debug("DocReaderUI.CheckParameters filling dictParamin...");
                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    LOG.Debug("DocReaderUI.CheckParameters     Adding => " + k + "=" + (string)bioPacketLocal.PacketParamIn[k] + "...");
                    dictParamIn.Add(k.ToUpper(), bioPacketLocal.PacketParamIn[k]);
                }
                LOG.Debug("DocReaderUI.CheckParameters dictParamin filled [length = " + dictParamIn.Count + "]");

                // ||
                //!dictParamIn.ContainsKey("TYPEID") || String.IsNullOrEmpty(dictParamIn["TYPEID"]) ||
                //!dictParamIn.ContainsKey("VALUEID") || String.IsNullOrEmpty(dictParamIn["VALUEID"])
                if (!dictParamIn.ContainsKey("SRC") || String.IsNullOrEmpty(dictParamIn["SRC"]))
                {
                    ret = -3;  //src/TypeId/ValueId deben ser enviados como parámetros
                    LOG.Debug("DocReaderUI.CheckParameters src/TypeId/ValueId deben ser enviados como parámetros!");
                }
                else
                {
                    LOG.Debug("DocReaderUI.CheckParameters evaluando cada parámetro...");
                    if (!dictParamIn.ContainsKey("TYPEID") || String.IsNullOrEmpty(dictParamIn["TYPEID"]))
                    {
                        if (!dictParamIn.ContainsKey("TYPEID"))
                        {
                            dictParamIn.Add("TYPEID", "RUT");
                        }
                        else
                        {
                            dictParamIn["TYPEID"] = "RUT";  //Ambos Minucia + WSQ
                        }
                    }
                    if (!dictParamIn.ContainsKey("VALUEID") || String.IsNullOrEmpty(dictParamIn["VALUEID"]))
                    {
                        if (!dictParamIn.ContainsKey("VALUEID"))
                        {
                            dictParamIn.Add("VALUEID", "NA");
                        }
                        else
                        {
                            dictParamIn["VALUEID"] = "NA";  //Ambos Minucia + WSQ
                        }
                    }
                    //if (!dictParamIn.ContainsKey("MAIL") || String.IsNullOrEmpty(dictParamIn["MAIL"]))
                    //{
                    //    if (!dictParamIn.ContainsKey("MAIL"))
                    //    {
                    //        dictParamIn.Add("MAIL", "info@notariovirtual.cl");
                    //    }
                    //    else
                    //    {
                    //        dictParamIn["TOKENCONTENT"] = "info@notariovirtual.cl";  //Ambos Minucia + WSQ
                    //    }
                    //}
                    if (!dictParamIn.ContainsKey("TIMEOUT") || String.IsNullOrEmpty(dictParamIn["TIMEOUT"]))
                    {
                        if (!dictParamIn.ContainsKey("TIMEOUT"))
                        {
                            dictParamIn.Add("TIMEOUT", "10000");
                        }
                        else
                        {
                            dictParamIn["TIMEOUT"] = "10000";
                        }
                    }
                    LOG.Debug("DocReaderUI.CheckParameters Timeout = " + dictParamIn["TIMEOUT"]);
                    if (!dictParamIn.ContainsKey("THEME") || String.IsNullOrEmpty(dictParamIn["THEME"]))
                    {
                        if (!dictParamIn.ContainsKey("THEME"))
                        {
                            dictParamIn.Add("THEME", "BCReader_drafv2.png");
                        }
                        else
                        {
                            dictParamIn["THEME"] = "BCReader_drafv2.png";
                        }
                    }
                    LOG.Debug("DocReaderUI.CheckParameters Theme = " + dictParamIn["THEME"]);

                    if (!dictParamIn.ContainsKey("OT") || String.IsNullOrEmpty(dictParamIn["OT"]))
                    {
                        if (!dictParamIn.ContainsKey("OT"))
                        {
                            dictParamIn.Add("OT", "0");
                        }
                        else
                        {
                            dictParamIn["OT"] = "0";
                        }
                    }
                    _DR_OperationType = Convert.ToInt32(dictParamIn["OT"]);
                    if (_DR_OperationType <= 1) //Si lee ambos lados o solo frente
                    {
                        _CURRENT_STEP = 0;
                    }
                    else //Sino lee solo dorso
                    {
                        _CURRENT_STEP = 1;
                    }
                    LOG.Debug("DocReaderUI.CheckParameters OperationType = " + dictParamIn["OT"]);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Error DocReaderUI.CheckParameters [" + ex.Message + "]");
                ret = -1;
            }
            LOG.Info("DocReaderUI.CheckParameters Ount [ret=" + ret + "]!");
            return ret;
        }

        private void Init()
        { 
            
            try
            {
                //StopCamStream();
                this.BackgroundImage = Properties.Resources.BCReader_drafv2;
                LOG.Info("DocReaderUI.Init IN...");

                if (_CAPTURING)
                {
                    StopCamStream();
                }
                _VERIFING = false;
                labMsgGuia.ForeColor = Color.Gray;
                //if (_DR_OperationType <= 1)
                //{
                labMsgGuia.Text = "Inicie previsualización,  y siga los pasos propuestos...";
                //} else
                //{
                //    labMsgGuia.Text = "Capturando Dorso Cédula. Presione CAPTURAR CÉDULA cuando tenga una imágen clara...";
                //}

                HaveData = false;
                //labMail.Text = dictParamIn["MAIL"];
                LabRutTitle.Text = ValueId;
                LabRutTitle.ForeColor = Color.Gray;

                _DOCUMENT = null;
                imgFoto.Image = Properties.Resources.no_picture;
                imgFirma.Image = Properties.Resources.no_signature;
                imageCedulaFront.Image = Properties.Resources.front;
                imageCedulaBack.Image = Properties.Resources.back;
                imageCam.Image = Properties.Resources.play_video_bg;
                //imageCedulaFront.Visible = false;
                //imageCedulaBack.Visible = false;
                //picQR.Visible = false;
                //picPDF.Visible = false;
                _ImageCedulaFrontB64 = null;
                _ImageCedulaBackB64 = null;
                //_ImageSelfieB64 = null;

                if (_DR_OperationType <= 1) _CURRENT_STEP = 0;
                else _CURRENT_STEP = 1;

                labSerial.Text = "?";
                labRUT.Text = "?";
                labNombre.Text = "?";
                labFNac.Text = "?";
                labFNac.ForeColor = Color.Gray;
                labNacionalidad.Text = "?";
                labSexo.Text = "?";
                //labVigencia.Visible = false;
                labFVto.ForeColor = Color.Gray;
                labFVto.Text = "?";
                //labCapturaSelfie.Enabled = false;
                labCaturaCedula.Enabled = false;
                labStartPreview.Enabled = true;
                labStopPreview.Enabled = false;
                //labCertificaIdentidad.Enabled = false;
                //picQR.Visible = false;
                //picPDF.Visible = false;
                //_pathPDF = null;
                ////picXML.Visible = false;
                //_pathXML = null;
                picBloqueo.Visible = false;
                labBloqueoSRCeI.Visible = false;
                label5.Visible = false;

                this.Refresh();
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.Init Error = " + ex.Message, ex);
            }
            LOG.Info("DocReaderUI.Init OUT!");
        }

        private void ShowStepStatus(string error = "")
        {
            try
            {
                LOG.Info("DocReaderUI.ShowStepStatus IN =>  _CURRENT_STEP = " + _CURRENT_STEP);
                switch (_CURRENT_STEP)
                {
                    case 1:  //Capturando cedula Back
                        labMsgGuia.ForeColor = Color.Gray;
                        labMsgGuia.Text = "Paso 1 - Capturando Frente de Cédula. Presione CAPTURAR CÉDULA cuando tenga una imágen clara...";
                        labMsgGuia.Refresh();
                        this.BackgroundImage = Properties.Resources.BCReader_drafv2;
                        this.Refresh();
                        _CurrentError = 0;
                        _sCurrentError = "";
                        break;
                    case 2:  //Capturando cedula Back
                        labMsgGuia.ForeColor = Color.Gray;
                        labMsgGuia.Text = "Paso 2 - Capturando Dorso de Cédula. Presione CAPTURAR CÉDULA cuando tenga una imágen clara...";
                        labMsgGuia.Refresh();
                        this.BackgroundImage = Properties.Resources.BCReader_drafv2;
                        this.Refresh();
                        _CurrentError = 0;
                        _sCurrentError = "";

                        if (Properties.Settings.Default.DR_BarcodeEnabled == 1)
                        {
                            _BARCODE_DECODE_RUNNING = true;
                        }

                        break;
                    //case 21:  //Cedula Capturada OK
                    //    this.BackgroundImage = Properties.Resources.BCReader_drafv2;
                    //    this.Refresh();
                    //    labMsgGuia.ForeColor = Color.Gray;
                    //    labMsgGuia.Text = "Cédula Caturada!" + Environment.NewLine + "Paso 2 -  Capturando Selfie. Presione CAPTURAR SELFIE cuando tenga una imágen clara...";
                    //    labMsgGuia.Refresh();
                    //    //labCapturaSelfie.Enabled = true;
                    //    _CurrentError = 0;
                    //    _sCurrentError = "";
                    //    break;
                    case 22:  //Cedula Capturada NO OK
                        labMsgGuia.Text = "Paso 1 - Error Procesando Cédula [" + error + "]";
                        labMsgGuia.ForeColor = Color.Red;
                        labMsgGuia.Refresh();
                        this.BackgroundImage = Properties.Resources.BCReader_drafv2;
                        this.Refresh();

                        StopCamStream();
                        //labCapturaSelfie.Enabled = false;

                        _CurrentError = -22;
                        _sCurrentError = "Paso 1 - Error Procesando Cédula [" + error + "]";

                        break;
                    //case 3:  //Capturando selfie
                    //    labMsgGuia.ForeColor = Color.Gray;
                    //    labMsgGuia.Text = "Cédula Capturada!" + Environment.NewLine + "Paso 2 -  Capturando Selfie. Presione CAPTURAR SELFIE cuando tenga una imágen clara...";
                    //    labMsgGuia.Refresh();

                    //    this.BackgroundImage = Properties.Resources.BCReader_drafv2;
                    //    this.Refresh();

                    //    _CurrentError = 0;
                    //    _sCurrentError = "";

                    //    labCapturaSelfie.Enabled = true;

                    //    break;
                    //case 31:  //Cedula Capturada OK
                    //    labMsgGuia.ForeColor = Color.Gray;
                    //    labMsgGuia.Text = "Selfie Caturada!" + Environment.NewLine + "Paso 3 -  Certificando Identidad con Notario Virtual. Espere...";
                    //    labMsgGuia.Refresh();

                    //    _CURRENT_STEP = 4;

                    //    this.BackgroundImage = Properties.Resources.BCReader_drafv2;
                    //    this.Refresh();
                    //    _CurrentError = 0;
                    //    _sCurrentError = "";
                    //    break;
                    //case 32:  //Cedula Capturada NO OK
                    //    labMsgGuia.Text = "Paso 2 - Error Capturando Selfie [" + error + "]";
                    //    labMsgGuia.ForeColor = Color.Red;
                    //    labMsgGuia.Refresh();

                    //    this.BackgroundImage = Properties.Resources.BCReader_drafv2;
                    //    this.Refresh();

                    //    _CurrentError = -32;
                    //    _sCurrentError = "Paso 2 - Error Capturando Selfie [" + error + "]";

                    //    break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.ShowStepStatus Error = " + ex.Message, ex);
            }
            LOG.Info("DocReaderUI.ShowStepStatus OUT!");
        }

        #endregion DocReaderUI General Operation



        #region AForge.NET Handle
        /// <summary>
        /// Revisa si está definida la webcam a usar, sino genera combo de opciones
        /// </summary>
        private void SetCamera()
        {
            try
            {
                LOG.Debug("DocReaderUI.SetCamera IN...");
                LOG.Debug("DocReaderUI.SetCamera Get list Cameras..."); 
                _WEBCAMS = new FilterInfoCollection(FilterCategory.VideoInputDevice);

                if (_WEBCAMS == null || _WEBCAMS.Count == 0)
                {
                    LOG.Error("DocReaderUI.SetCamera No jay camaras conectadas!");
                    _CurrentError = -11;
                    _sCurrentError = "No existe camara conectada para trabajar!";
                    CloseControl(false);
                }
                else
                {
                    bool existCamConfigured = false;
                    if (!string.IsNullOrEmpty(Properties.Settings.Default.DR_WebCamSource) &&
                        !Properties.Settings.Default.DR_WebCamSource.Equals("Default"))
                    {
                        LOG.Debug("DocReaderUI.SetCamera Camara configurada = " + Properties.Settings.Default.DR_WebCamSource);
                        for (int i = 0; i < _WEBCAMS.Count; i++)
                        {
                            if (_WEBCAMS[i].Name.Equals(Properties.Settings.Default.DR_WebCamSource))
                            {
                                LOG.Debug("DocReaderUI.SetCamera Existe! Idx = " + i.ToString());
                                _CURRENT_WEBCAM_ID = i;
                                existCamConfigured = true;
                                break;
                            }
                            else
                            {
                                LOG.Debug("DocReaderUI.SetCamera Camara idx" + i.ToString() + " = " + _WEBCAMS[i].Name);
                            }
                        }
                    } 

                    if (!existCamConfigured) //Esto porque 
                    {
                        LOG.Debug("DocReaderUI.SetCamera Llenando lista webcams...");
                        foreach (FilterInfo webcam in _WEBCAMS)
                        {
                            cboCamaras.Items.Add(webcam.Name);
                        }
                        cboCamaras.SelectedIndex = 0;
                        _CURRENT_WEBCAM_ID = 0;
                        labCamaras.Visible = true;
                        cboCamaras.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _CurrentError = -1;
                _sCurrentError = "Error seteando camara para trabajar!";
                CloseControl(false);
                LOG.Error("DocReaderUI.SetCamera Error: " + ex.Message);
            }
        }

        private void NewFrameEvent(object sender, NewFrameEventArgs eventArgs)
        {
            try
            {
                //if (currentBitmapForDecoding == null)
                //{
                    currentBitmapForDecoding = (Bitmap)eventArgs.Frame.Clone();
                //}
                imageCam.Image = (Bitmap)eventArgs.Frame.Clone();
            }
            catch (Exception ex)
            {

                LOG.Error("DocReaderUI.NewFrameEvent Error: " + ex.Message);
            }
        }


        #endregion AForge.NET Handle


        #region Namku IdCardDetector
        private void OnIDCardDetected(object sender, Bitmap frame)
        {
            UIUtils.UpdatePictureBoxImageInUIThread(this, imageCedulaFront, frame);
            //_ImageCedulaFrontB64 = GetB64FromPictureBox(imageCedulaFront);
            
            labStopPreview.Enabled = false;
            labStartPreview.Enabled = true;
            _VERIFING = true;
            timer_VerifyCedula.Enabled = true;
            idCardDetectorControl.Stop();
        }


        private void OnBlurrinessCalculated(object sender, double e)
        {
            //throw new NotImplementedException();
        }


        private void OnUserControlDispose(object sender, EventArgs e)
        {
            idCardDetectorControl.Dispose();
        }

        #endregion Namku IdCardDetector

    

        #region EmguCV Handle
        private void StopCamStream()
        {
            LOG.Info("DocReaderUI.StopCamStream IN...");
            try
            {
                _CAPTURING = false;
                #region EmuCV Stop
                //Application.Idle -= ShowCamStream;
                //captureCam.Stop();
                //captureCam.Dispose();
                #endregion EmuCV Stop

                #region AForge.NET Stop
                if (_CURRENT_WEBCAM != null && _CURRENT_WEBCAM.IsRunning)
                {
                    _CURRENT_WEBCAM.Stop();
                }
                #endregion AForge.NET Stop

                #region Namku idCardDetector
                //idCardDetectorControl.Stop();
                #endregion Namku idCardDetector

                labStopPreview.Enabled = false;
                labStartPreview.Enabled = true;
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.StopCamStream Error = " + ex.Message, ex);
            }
            LOG.Info("DocReaderUI.StopCamStream OUT!");
        }

        private void ShowCamStream(object sender, EventArgs e)

        {
            try
            {
                Mat mat = new Mat();
                captureCam.Retrieve(mat);
                Image<Emgu.CV.Structure.Bgr, Byte> img = mat.ToImage<Bgr, Byte>();

                //Image<Gray, Byte> grayImage = img.Convert<Gray, Byte>();
                //var faces = grayImage.DetectHaarCascade(
                //                                   haar, 1.4, 4,
                //                                   HAAR_DETECTION_TYPE.DO_CANNY_PRUNING,
                //                                   new Size(nextFrame.Width / 8, nextFrame.Height / 8)
                //                                   )[0];
                imageCam.Image = img.ToBitmap();

            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.ShowCamStream Error = " + ex.Message, ex);
            }

        }
        #endregion EmguCV Handle

 

        private void btnStartCedula_Click(object sender, EventArgs e)
        {
            LOG.Info("DocReaderUI.btnStartCedula_Click IN...");
            try
            {
                _CAPTURING = true;
                _CURRENT_STEP = 1;
                ShowStepStatus();

                #region EmguCV Start

                //captureCam = new Capture();
                //Application.Idle += ShowCamStream;
                #endregion EmguCV Start

                #region AForge.NET Start
                //_CURRENT_WEBCAM = new VideoCaptureDevice(_WEBCAMS[comboBox1.SelectedIndex].MonikerString);
                //_CURRENT_WEBCAM.NewFrame += new NewFrameEventHandler(NewFrameEvent);
                //_CURRENT_WEBCAM.Start();

                #endregion AForge.NET Start

            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.btnStartCedula_Click Error = " + ex.Message, ex);
            }
            LOG.Info("DocReaderUI.btnStartCedula_Click OUT!");
        }

        private void timer_VerifyCedula_Tick(object sender, EventArgs e)
        {
            string err = "";
            if (_VERIFING)
            {
                try
                {

                    _VERIFING = false;
                    //timer_VerifyCedula.Enabled = false;
                    LOG.Info("DocReaderUI.timer_VerifyCedula_Tick IN...");
                    //labRUT.Text = "21284415-2";
                    //labNombre.Text = "Gustavo Gerardo Suhit GAllucci";
                    //labFNac.Text = "28-09-2169";
                    //labNacionalidad.Text = "ARG";
                    //labSexo.Text = "M";
                    //_CURRENT_STEP = 21;

                    string xmlparamin, xmlparamout;

                    //_ImageCedulaFrontB64 = GetB64FromPictureBox(imageCedulaFront);

                    string trackid;
                    string labMsgGuia;
                    LOG.Debug("DocReaderUI.timer_VerifyCedula_Tick - Iniciando BPHelper.OCRIDCard...");
                    //_MUSTRUN = true;
                    //ShowProgress();
                    labProcesando.Visible = true;
                    labProcesando.Refresh();

                    int iret = src.Helpers.BPHelper.OCRIDCard(_ImageCedulaFrontB64, ValueId, out labMsgGuia, out _DOCUMENT, out trackid);
                    //_MUSTRUN = false;
                    //CleanProgressTH();
                    labProcesando.Visible = false;
                    labProcesando.Refresh();

                    LOG.Debug("DocReaderUI.timer_VerifyCedula_Tick - ret = " + iret.ToString());
                    TrackId = trackid;

                    if (iret != 0 || _DOCUMENT == null || string.IsNullOrEmpty(trackid))
                    {
                        err = "-DR105 - Retorno OCR Service con error [" + iret.ToString() + "]";
                        _CURRENT_STEP = 22;
                        LOG.Error("DocReaderUI.timer_VerifyCedula_Tick Error = " + err);
                    }
                    else
                    {
                        ShowDocumentInControl();
                    }

                }
                catch (Exception ex)
                {
                    err = "-DR106 - Desconocido [OCR Service]";
                    _CURRENT_STEP = 22;
                    LOG.Error("DocReaderUI.timer_VerifyCedula_Tick Error = " + ex.Message, ex);
                }
                ShowStepStatus(err);
                LOG.Info("DocReaderUI.timer_VerifyCedula_Tick OUT!");
                labCaturaCedula.Enabled = true;
            }
        }

        private void ShowDocumentInControl()
        {
            string err;
            try
            {
                labRUT.Text = _DOCUMENT.ValueId;
                labNombre.Text = (string.IsNullOrEmpty(_DOCUMENT.Name) ? "" : _DOCUMENT.Name) + " " +
                                 (string.IsNullOrEmpty(_DOCUMENT.PhaterLastName) ? "" : _DOCUMENT.PhaterLastName) + " " +
                                 (string.IsNullOrEmpty(_DOCUMENT.MotherLastName) ? "" : _DOCUMENT.MotherLastName);
                labFNac.Text = _DOCUMENT.BirthDate;
                labSexo.Text = _DOCUMENT.Sex;
                labFVto.Text = _DOCUMENT.ExpirationDate;
                labNacionalidad.Text = _DOCUMENT.Nacionality;
                imgFoto.Image = Base64ToImage(_DOCUMENT.IDCardPhotoImage);
                imgFirma.Image = Base64ToImage(_DOCUMENT.IDCardSignatureImage);

                if (IsCedulaVigente(_DOCUMENT.ExpirationDate))
                {
                    labFVto.ForeColor = Color.Green;
                    _DOCUMENT.DateValid = 0;
                }
                else
                {
                    labFVto.ForeColor = Color.Red;
                    _DOCUMENT.DateValid = 1;
                    if (_DR_BlockValidity == 1)
                    {
                        err = "-DR107 - Bloqueo del proceso! Cédula Expirada!";
                        _CURRENT_STEP = 22;
                    }
                }

                if (IsYounger(_DOCUMENT.BirthDate))
                {
                    labFNac.ForeColor = Color.Red;
                    _DOCUMENT.Younger = 1;
                    if (_DR_BlockAge == 1)
                    {
                        err = "-DR107 - Bloqueo del proceso! MENOR DE EDAD!";
                        _CURRENT_STEP = 22;
                    }
                }
                else
                {
                    labFNac.ForeColor = Color.Green;
                    _DOCUMENT.Younger = 0;
                }
            }
            catch (Exception ex)
            {
                LOG.Error(" Error: " + ex.Message);
            }
        }

        private void CleanProgressTH()
        {
            try
            {
                if (tShowProgress.IsAlive) tShowProgress.Abort();
                tShowProgress = null;
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.CleanProgressTH Error = " + ex.Message);
            }
        }

        Thread tShowProgress;
        private void ShowProgress()
        {
            try
            {
                tShowProgress = new Thread(new ThreadStart(ShowProgressTH));
                tShowProgress.Start();
                //labProcesando.Visible = true;
                //pbProcess.Visible = true;
                //pbProcess.Value = 0;
                //pbProcess.Refresh();
                //while (_MUSTRUN)
                //{
                //    pbProcess.Value += 1;
                //    if (pbProcess.Value == pbProcess.Maximum)
                //    {
                //        pbProcess.Value = 0;
                //    }
                //    pbProcess.Refresh();
                //}
                //labProcesando.Visible = false;
                //pbProcess.Visible = false;
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.ShowProgress Error = " + ex.Message);
            }
        }

        private void ShowProgressTH()
        {
            try
            {
                labProcesando.Visible = true;
                pbProcess.Visible = true;
                pbProcess.Value = 0;
                pbProcess.Refresh();
                while (_MUSTRUN)
                {
                    pbProcess.Value += 1;
                    if (pbProcess.Value == pbProcess.Maximum)
                    {
                        pbProcess.Value = 0;
                    }
                    pbProcess.Refresh();
                }
                labProcesando.Visible = false;
                pbProcess.Visible = false;

            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.ShowProgress Error = " + ex.Message);
            }
        }

 

        private void timer_GeneratingCI_Tick(object sender, EventArgs e)
        {
            try
            {
                timer_GeneratingCI.Enabled = false;
                LOG.Info("DocReaderUI.timer_GeneratingCI_Tick IN...");

                //picQR.Image = Properties.Resources.QR_Example;
                //labTrackId.Text = "2gf32hg32f2xGDo88687ass876sda87a8d78";
                //lab3_Info.Text = "Identidad Certificada!!";
                //lab3_Info.Refresh();
                //picPDF.Visible = true;

                //CertificaIDentidad();
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.timer_GeneratingCI_Tick Error = " + ex.Message, ex);
            }
            LOG.Info("DocReaderUI.timer_GeneratingCI_Tick OUT!");

            //labCapturaSelfie.Enabled = true;
        }


        #region Utils
        private bool IsCedulaVigente(string expirationDate)
        {
            bool ret;
            try
            {
                LOG.Debug("DocReaderUI.IsCedulaVigente IN - expirationDate = " + expirationDate);
                DateTime dt = DateTime.Parse(expirationDate);
                LOG.Debug("DocReaderUI.IsCedulaVigente Now = " + DateTime.Now.ToString("dd/MM/yyyy"));
                ret = (DateTime.Now < dt);
                LOG.Debug("DocReaderUI.IsCedulaVigente => " + ret.ToString());
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("DocReaderUI.IsCedulaVigente Error = " + ex.Message, ex);
            }
            return ret;
        }

        private bool IsYounger(string birthdate)
        {
            bool ret;
            try
            {
                LOG.Debug("DocReaderUI.IsYounger IN - birthdate = " + birthdate);
                DateTime dt = DateTime.Parse(birthdate);
                LOG.Debug("DocReaderUI.IsYounger Now = " + DateTime.Now.ToString("dd/MM/yyyy"));
                DateTime nacimiento = new DateTime(1969, 9, 28); //Fecha de nacimiento
                int edad = (DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1);
                LOG.Debug("DocReaderUI.IsYounger Edad = " + edad.ToString());
                ret = edad < 18;
                LOG.Debug("DocReaderUI.IsYounger => " + ret.ToString());
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("DocReaderUI.IsYounger Error = " + ex.Message, ex);
            }
            return ret;
        }

        public Image Base64ToImage(string imageB64)
        {
            Image imgRet = null;
            LOG.Debug("DocReaderUI.Base64ToImage IN...");
            try
            {
                byte[] imageBytes = Convert.FromBase64String(imageB64);

                using (MemoryStream ms = new MemoryStream(imageBytes))
                {
                    imgRet = Image.FromStream(ms);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.Base64ToImage Error = " + ex.Message, ex);
                imgRet = null;
            }
            LOG.Debug("DocReaderUI.Base64ToImage OUT!");
            return imgRet;
        }

        private string GetB64FromPictureBox(PictureBox pic)
        {
            return ImageToBase64(pic.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        public string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            string base64String = null;
            LOG.Debug("DocReaderUI.ImageToBase64 IN...");
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    image.Save(ms, format);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    base64String = Convert.ToBase64String(imageBytes);
                    LOG.Debug("DocReaderUI.ImageToBase64 base64String = " + base64String);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.ImageToBase64 Error = " + ex.Message, ex);
                base64String = null;
            }
            LOG.Debug("DocReaderUI.ImageToBase64 OUT!");
            return base64String;
        }


        #endregion Utils

        private void labClose_Click(object sender, EventArgs e)
        {
            LOG.Info("DocReaderUI.labClose_Click Clicked");
            bool statusClose = true; // (document (!string.IsNullOrEmpty(RDEnvelope));
            CloseControl(statusClose);
        }

        private void labCaturaCedula_Click(object sender, EventArgs e)
        {
            try
            {
                LOG.Info("DocReaderUI.labCaturaCedula_Click IN...");
                labCaturaCedula.Enabled = false;
                //_CAPTURING = 0;
                //_CURRENT_STEP = 21;
                if (_CURRENT_STEP == 1)
                {
                    imageCedulaFront.Image = imageCam.Image;
                    _ImageCedulaFrontB64 = GetB64FromPictureBox(imageCedulaFront);
                    LOG.Debug("DocReaderUI.labCaturaCedula_Click _ImageCedulaFrontB64 = " + _ImageCedulaFrontB64);

                    if (_DR_OCREnabled == 1)
                    {
                        //btnCapturaCedula.Enabled = false;
                        labMsgGuia.Text = "Paso 1.1 - Procesando Cédula en línea [OCR]! Espere...";
                        labMsgGuia.Refresh();

                        _VERIFING = true;
                        timer_VerifyCedula.Enabled = true;
                        //imageCedulaFront.Visible = true;                    }
                    }
                    _CURRENT_STEP++;
                    //ShowProgress();
                    if (_DR_OperationType == 1)
                    {
                        _CURRENT_STEP++;
                        StopCamStream();
                    }
                }
                else if (_CURRENT_STEP == 2)
                {
                    imageCedulaBack.Image = imageCam.Image;
                    _ImageCedulaBackB64 = GetB64FromPictureBox(imageCedulaBack);
                    LOG.Debug("DocReaderUI.labCaturaCedula_Click _ImageCedulaBackB64 = " + _ImageCedulaBackB64);
                    if (_DOCUMENT == null) _DOCUMENT = new Document();

                    _DOCUMENT.IDCardImageBack = _ImageCedulaBackB64;
                    if (_DR_BarcodeEnabled == 1)
                    {
                        //btnCapturaCedula.Enabled = false;
                        labMsgGuia.Text = "Paso 2.1 - Reconociendo Códigos de Barras! Espere...";
                        labMsgGuia.Refresh();

                        //timer_VerifyCedula.Enabled = true;
                        //imageCedulaFront.Visible = true;                    }
                    }
                    _CURRENT_STEP++;
                    //ShowProgress();
                    //if (_DR_OperationType <= 1)
                    StopCamStream();
                }

                //_CURRENT_STEP++;
                //ShowProgress();
                
            }
            catch (Exception ex)
            {
                _CURRENT_STEP = 22;
                _CurrentError = -22;
                _sCurrentError = "Error Procesando Cedula [" + ex.Message + "]";
                LOG.Error("DocReaderUI.labCaturaCedula_Click Error = " + ex.Message, ex);
            }
            LOG.Info("DocReaderUI.labCaturaCedula_Click OUT!");
            //StopCamStream();
            //ShowStepStatus();
        }



        private void labStartPreview_Click(object sender, EventArgs e)
        {
            LOG.Info("DocReaderUI.labStartPreview_Click IN...");
            try
            {
                if (_CURRENT_STEP == 3)
                {
                    Init();
                }

                _CAPTURING = true;
                //_CURRENT_STEP = 1;
                //ShowStepStatus();

                #region AForge.NET
                _CURRENT_WEBCAM = new VideoCaptureDevice(_WEBCAMS[_CURRENT_WEBCAM_ID].MonikerString);
                _CURRENT_WEBCAM.NewFrame += new NewFrameEventHandler(NewFrameEvent);
                _CURRENT_WEBCAM.Start();

                #endregion AForge.NET

                #region EmguCV
                //captureCam = new Capture();
                //Application.Idle += ShowCamStream;
                #endregion EmguCV

                //idCardDetectorControl.Start();

                if (_CURRENT_STEP == 0) //Si viene desde paso inicial o hubo error o no en lectura de cedula => Reinicio desde ahi
                {
                    _CURRENT_STEP++;
                    ShowStepStatus();
                    _ImageCedulaFrontB64 = null;
                    //_ImageCedulaFrontB64 = null;
                    //RDEnvelope = null;
                    TrackId = null;
                    labCaturaCedula.Enabled = true;
                } else if (_CURRENT_STEP == 1) //Si termino paso 0 o es solo back
                {
                    _CURRENT_STEP++;
                    ShowStepStatus();
                    _ImageCedulaBackB64 = null;
                    //RDEnvelope = null;
                    TrackId = null;
                    labCaturaCedula.Enabled = true;
                    //labCapturaSelfie.Enabled = true;
                }

                //if (_CURRENT_STEP > 32 || _CURRENT_STEP == 4) //Si retomo desde final con o sin error => Reinicio captura de selfie
                //{
                //    _CURRENT_STEP = 21;
                //    _ImageSelfieB64 = null;
                //    //RDEnvelope = null;
                //    TrackId = null;
                //    ShowStepStatus();
                //    labCapturaSelfie.Enabled = true;
                //}
                labStopPreview.Enabled = true;
                labStartPreview.Enabled = false;
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.labStartPreview_Click Error = " + ex.Message, ex);
                labCaturaCedula.Enabled = false;
            }
            LOG.Info("DocReaderUI.labStartPreview_Click OUT!");
        }

 

        

        private void labStopPreview_Click(object sender, EventArgs e)
        {
            //_CAPTURING = 1;
            //_CURRENT_STEP = 1;
            //ShowStepStatus();
            StopCamStream();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void labVigencia_Click(object sender, EventArgs e)
        {

        }

       
        private void labReInit_Click(object sender, EventArgs e)
        {
            LOG.Info("DocReaderUI.labReInit_Click Clicked");
            Init();
        }

        private void CloseControl(bool hasCapture)
        {
            LOG.Info("DocReaderUI.CloseControl In...");
            try
            {
                if (bioPacketLocal == null)
                {
                    LOG.Debug("DocReaderUI.CloseControl Creando bioPacketLocal...");
                    bioPacketLocal = new BioPacket();
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
                }

                if (_CurrentError != 0 || !hasCapture)  //Si hay error o no completo todos los datos
                {
                    bioPacketLocal.PacketParamOut.Add("TrackId", null);
                    bioPacketLocal.PacketParamOut.Add("CI", null);
                    //bioPacketLocal.PacketParamOut.Add("DeviceId", DeviceId);
                    bioPacketLocal.PacketParamOut.Add("Error", _CurrentError.ToString() + "|" + _sCurrentError);
                    LOG.Debug("DocReaderUI.CloseControl VAlores devueltos => TrackId = null | Error = " + _CurrentError.ToString() + "|" + _sCurrentError);
                }
                else
                {
                    bioPacketLocal.PacketParamOut.Add("TrackId", TrackId);
                    bioPacketLocal.PacketParamOut.Add("Document", _DOCUMENT);
                    //bioPacketLocal.PacketParamOut.Add("DeviceId", DeviceId);
                    bioPacketLocal.PacketParamOut.Add("Error", "0|No Error");
                    LOG.Debug("DocReaderUI.CloseControl VAlores devueltos => TrackId = " + TrackId + "|0|No Error");
                }
                HaveData = true;
                this.Close();
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.CloseControl Error Closing [" + ex.Message + "]");

            }

            LOG.Info("DocReaderUI.CloseControl OUT!");

        }

        #region Utils
        private bool MailValid()
        {
            bool ret = false;
            LOG.Debug("DocReaderUI.MailNotValid IN...");
            try
            {
                //var addr = new System.Net.Mail.MailAddress(labMail.Text);
                //ret = (addr.Address == labMail.Text);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("DocReaderUI.MailNotValid Error = " + ex.Message, ex);
            }
            LOG.Debug("DocReaderUI.MailNotValid OUT!");
            return ret;
        }
        public String FormatRutFormateado(String rut)
        {
            return rut.Substring(0, rut.Length - 1) + "-" + rut.Substring(rut.Length - 1);

        }

        private void GetGeoRef()
        {
            try
            {
                //GeoRef = Properties.Settings.Default.CIGeoRef;
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.GetGeoRef Error [" + ex.Message + "]");
            }
        }

        private void GetDeviceId()
        {
            try
            {
                DeviceId = GetMACAddress();
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.GetDeviceId Error [" + ex.Message + "]");
            }
        }

        public string GetMACAddress()
        {
            string sMacAddress = null;
            try
            {
                LOG.Debug("DocReaderUI.GetMACAddress IN...");
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                sMacAddress = string.Empty;
                foreach (NetworkInterface adapter in nics)
                {
                    if (sMacAddress == String.Empty)// only return MAC Address from first card
                    {
                        if (adapter.OperationalStatus == OperationalStatus.Up && (!adapter.Description.Contains("Virtual")
                            && !adapter.Description.Contains("Pseudo")))
                        {
                            if (adapter.GetPhysicalAddress().ToString() != "")
                            {
                                IPInterfaceProperties properties = adapter.GetIPProperties();
                                sMacAddress = adapter.GetPhysicalAddress().ToString();
                                LOG.Debug("DocReaderUI.GetMACAddress MAC Tomada = " + sMacAddress);
                            }
                        }
                    }
                }
                if (sMacAddress == string.Empty)
                {
                    //sMacAddress = Properties.Settings.Default.CIDeviceId;
                    LOG.Debug("DocReaderUI.GetMACAddress MAC Tomada desde CIDeviceId = " + sMacAddress);
                }
                else
                {
                    string sAux = sMacAddress.Substring(0, 2);
                    int i = 2;
                    while (i < sMacAddress.Length)
                    {
                        sAux = sAux + "-" + sMacAddress.Substring(1, 2);
                        i = i + 2;
                    }
                    sMacAddress = sAux;
                    LOG.Debug("DocReaderUI.GetMACAddress MAC formateada = " + sAux);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.GetMACAddress Error [" + ex.Message + "]");
            }

            return sMacAddress;
        }

        /// <summary>
        /// Chequea si existe el directorio de proceso temporal si FETargetVideoType = 0, sino, si es 1, 
        /// es Stream el destino.
        /// </summary>
        private void CheckTmpDirectory()
        {
            try
            {

                //if (!Directory.Exists(Properties.Settings.Default.CIDirTemp))
                //{
                //    DirectoryInfo di = Directory.CreateDirectory(Properties.Settings.Default.CIDirTemp);
                //    if (di != null)
                //    {
                //        LOG.Info("DocReaderUI.CheckTmpDirectory Se creo directorio temporal " + Properties.Settings.Default.CIDirTemp);
                //    }
                //    else
                //    {
                //        LOG.Warn("DocReaderUI.CheckTmpDirectory Atencion!! Problemas en la creación del directorio temporal " + Properties.Settings.Default.CIDirTemp);
                //    }
                //}
                //else
                //{
                //    LOG.Info("DocReaderUI.CheckTmpDirectory Directorio temporal OK - " + Properties.Settings.Default.CIDirTemp);
                //}
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.CheckTmpDirectory Error [" + ex.Message + "]");
            }
        }

        private void timer_Close_Tick(object sender, EventArgs e)
        {
            LOG.Info("DocReaderUI.timer_Close_Tick In...");
            timer_Close.Enabled = false;
            CloseControl(HaveData);
        }

        private void LabRutTitle_Click(object sender, EventArgs e)
        {

        }

        private void picPDF_Click(object sender, EventArgs e)
        {
            try
            {
                //LOG.Debug("DocReaderUI.picPDF_Click IN... Path PDF = " + _pathPDF);

                //if (!string.IsNullOrEmpty(_pathPDF))
                //{
                //    System.Diagnostics.Process.Start(_pathPDF);
                //}
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.picPDF_Click Error [" + ex.Message + "]");
            }
            LOG.Debug("DocReaderUI.picPDF_Click Out!");
        }

        private void picXML_Click(object sender, EventArgs e)
        {
            try
            {
                //LOG.Debug("DocReaderUI.picXML_Click IN... Path PDF = " + _pathXML);

                //if (!string.IsNullOrEmpty(_pathXML))
                //{
                //    System.Diagnostics.Process.Start(_pathXML);
                //}
            }
            catch (Exception ex)
            {
                LOG.Error("DocReaderUI.picXML_Click Error [" + ex.Message + "]");
            }
            LOG.Debug("DocReaderUI.picXML_Click Out!");
        }

        private void process1_Exited(object sender, EventArgs e)
        {

        }

        private void timer_ShowProgress_Tick(object sender, EventArgs e)
        {
            try
            {
                if (pbProcess.Value == 100)
                {
                    pbProcess.Value = 0;
                    pbProcess.Refresh();
                }
                pbProcess.Value += 1;
                pbProcess.Refresh();
            }
            catch (Exception ex)
            {

            }
        }

        private void labMail_Leave(object sender, EventArgs e)
        {
            if (!MailValid())
            {
                //labMail.ForeColor = Color.White;
                //labMail.BackColor = Color.Red;
                //labMail.Focus();
            }
            else
            {
                //labMail.ForeColor = Color.Black;
                //labMail.BackColor = Color.WhiteSmoke;
            }


        }

        private void labNombre_Click(object sender, EventArgs e)
        {

        }

        private void cboCamaras_SelectedIndexChanged(object sender, EventArgs e)
        {
            _CURRENT_WEBCAM_ID = cboCamaras.SelectedIndex;
        }

        #endregion Utils

        //#region ZXing Zone

        //private void DecodeBarcode()
        //{
        //    LOG.Debug("DocReaderUI.DecodeBarcode IN...");
        //    var reader = new BarcodeReader();
        //    while (true)
        //    {

        //        if (_BARCODE_DECODE_RUNNING && currentBitmapForDecoding != null)
        //        {
        //            var result = reader.Decode(currentBitmapForDecoding);



        //            if (result != null)
        //            {
        //                if (result.BarcodeFormat.ToString() == "PDF_417" || result.BarcodeFormat.ToString() == "QR_CODE")
        //                {

        //                    LOG.Debug("DocReaderUI.DecodeBarcode - Se detectó un código PDF417 o QR");
        //                    LOG.Debug("DocReaderUI.DecodeBarcode - Codigo de barra leido:" + result.BarcodeFormat.ToString());
        //                    resultAsBinary = System.Text.Encoding.GetEncoding("ISO8859-1").GetBytes(result.Text);
        //                    LOG.Debug("DocReaderUI.DecodeBarcode - Contenido código de barra:" + result.Text);

        //                    Invoke(new Action<Result>(ShowResult), result);

        //                    _BARCODE_DECODE_RUNNING = false;

        //                }
        //            }
        //            currentBitmapForDecoding.Dispose();
        //            currentBitmapForDecoding = null;
        //        }
        //        Thread.Sleep(200);
        //    }
        //}

        ///// <summary>
        ///// Una vez identificado un código de barra, procedemos a decodificar la información.
        ///// </summary>
        ///// <param name="result"></param>
        //private void ShowResult(Result result)
        //{
        //    String rut = "";
        //    _CurrentResult = result;
        //    //txtBarcodeFormat.Text = result.BarcodeFormat.ToString();
        //    //txtContent.Text = result.Text;
        //    Barcodewebcam = new BarcodeWebCam();
        //    if (result.BarcodeFormat.ToString() == "PDF_417")
        //    {
        //        LOG.Debug("DocReaderUI.ShowResult - Retorna el contenido del PDF417...");
        //        //    logger.Debug("Biometrika BviCAM.. Código PDF:" + barcode.Value);
        //        //    logger.Debug("Biometrika BviCAM.. Código PDF:" + barcode.Value.Length);
        //        BarCodeContent = result.Text.Substring(0, 40);


        //        Barcodewebcam.ValueId = result.Text.Substring(0, 9).Replace('\0', ' ').TrimEnd();
        //        LOG.Debug("DocReaderUI.ShowResult - RUT Leido desde PDF417 = " + Barcodewebcam.ValueId);
        //        Barcodewebcam.LastName = result.Text.Substring(19, 30).Replace('\0', ' ').TrimEnd();
        //        Barcodewebcam.DocumentNumber = result.Text.Substring(58, 10).Replace('\0', ' ').TrimEnd();
        //        Barcodewebcam.Pdf417 = true;
        //        ////Barcodewebcam.FullPdf417= Convert.ToBase64String(_binaryData);
        //        String _year = result.Text.Substring(52, 2);
        //        String _month = result.Text.Substring(54, 2);
        //        String _day = result.Text.Substring(56, 2);
        //        Barcodewebcam.ExpirationDate = new DateTime(Convert.ToInt32(_year) + 2000, Convert.ToInt32(_month), Convert.ToInt32(_day));
        //        //String recorte = barcode.Value;
        //        //int indice = 0;
        //        //indice = barcode.Value.IndexOf(">TRIAL VERSION EXPIRES 90 DAYS AFTER INSTALLATION<");
        //        //String total;
        //        //total = barcode.Value.Substring(0, 420);
        //        Barcodewebcam._binaryData = resultAsBinary;
        //        Barcodewebcam.ProcessNec();
        //    }
        //    else if (result.BarcodeFormat.ToString() == "QR_CODE")
        //    {
        //        LOG.Debug("DocReaderUI.ShowResult - Retorna el contenido del qr...");
        //        //Barcodewebcam = new BarcodeWebCam();
        //        Barcodewebcam.Qr = true;
        //        //BarCodeContent = result.Text.Substring(0, result.Text.Length - largo);
        //        Barcodewebcam.ValueId = result.Text.Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[0] + result.Text.Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[1];
        //        LOG.Debug("DocReaderUI.ShowResult - RUT Leido desde QR = " + Barcodewebcam.ValueId);

        //        Int32 _pos = result.Text.LastIndexOf('=');
        //        String _mrz = result.Text.Substring(_pos + 1, 24);

        //        Barcodewebcam.DocumentNumber = _mrz.Substring(0, 9);
        //        Barcodewebcam.DOB = _mrz.Substring(10, 6);
        //        Barcodewebcam.DOE = _mrz.Substring(17, 6);

        //        string _exdate = _mrz.Substring(17, 6);
        //        string _year = _exdate.Substring(0, 2);
        //        string _month = _exdate.Substring(2, 2);
        //        string _day = _exdate.Substring(4, 2);

        //        Barcodewebcam.ExpirationDate = new DateTime(Convert.ToInt32(_year) + 2000, Convert.ToInt32(_month), Convert.ToInt32(_day));

        //    }

        //    _DecodingThread.Abort();
        //    LOG.Debug("DocReaderUI.ShowResult - Se obtiene el rut => Se actualiza informacion de _DOCUMENT...");

        //    ActualizaDocument(Barcodewebcam);

        //    //rut = Barcodewebcam.ValueId;
        //    //if (rut == lblRut.Text)
        //    //{
        //    //    this.Hide();
        //    //    int Timeout = 0;
        //    //    if (dictParamIn.ContainsKey("TIMEOUT"))
        //    //    {
        //    //        if (dictParamIn["TIMEOUT"] != null)
        //    //            Timeout = Convert.ToInt32(dictParamIn["TIMEOUT"]);
        //    //    }
        //    //    else
        //    //        Timeout = Convert.ToInt32(appConfig.AppSettings.Settings["Timeout"].Value);
        //    //    MainWindow objMain = new MainWindow(solicitud, Timeout);
        //    //    objMain.appConfig = this.appConfig;
        //    //    objMain.bioPacketLocal = this.bioPacketLocal;
        //    //    objMain.barcodeWebCam = Barcodewebcam;
        //    //    objMain.ShowDialog();
        //    //    ShowBvi = true;
        //    //    //camDevices.Current.Stop();
        //    //}
        //    //else
        //    //{
        //    //    logger.Error("Biometrika BviCAM.. no coincide el rut leído");
        //    //    //MessageBox.Show("El rut configurado no coincide con el rut leído en la cédula");                            
        //    //    BVIResponse bvi = BuildRespuesta(-3000, "El rut no coincide con la lectura");
        //    //    SetPackageResponse(-3000, "El rut no coincide con la lectura", bvi);

        //    //    this.Close();
        //    //}
        //    LOG.Debug("DocReaderUI.ShowResult - OUT!");
        //}

        //private void ActualizaDocument(BarcodeWebCam bc)
        //{
        //    LOG.Error("DocReaderUI.ActualizaDocument - IN...");
        //    try
        //    {
        //        if (_DOCUMENT == null)
        //            _DOCUMENT = new Document();

        //        _DOCUMENT.ValueId = !string.IsNullOrEmpty(bc.ValueId) ? bc.ValueId : _DOCUMENT.ValueId;
        //        //labRUT.Text = _DOCUMENT.ValueId;
        //        _DOCUMENT.Serial = !string.IsNullOrEmpty(bc.DocumentNumber) ? bc.DocumentNumber : _DOCUMENT.Serial;
        //        //labSerial.Text = _DOCUMENT.Serial;
        //        //_DOCUMENT.Name = !string.IsNullOrEmpty(bc.) ? bc.DocumentNumber : _DOCUMENT.Serial;
        //        _DOCUMENT.PhaterLastName = !string.IsNullOrEmpty(bc.LastName) ? bc.LastName : _DOCUMENT.PhaterLastName;
        //        //labNombre.Text = (string.IsNullOrEmpty(_DOCUMENT.Name) ? "" : _DOCUMENT.Name) + " " +
        //        //                 (string.IsNullOrEmpty(_DOCUMENT.PhaterLastName) ? "" : _DOCUMENT.PhaterLastName) + " " +
        //        //                 (string.IsNullOrEmpty(_DOCUMENT.MotherLastName) ? "" : _DOCUMENT.MotherLastName);
        //        _DOCUMENT.BirthDate = !(bc.BirthDate == null) ? bc.BirthDate.ToString("dd/MM/yyyy") : _DOCUMENT.BirthDate;
        //        _DOCUMENT.ExpirationDate = !(bc.ExpirationDate == null) ? bc.ExpirationDate.ToString("dd/MM/yyyy") : _DOCUMENT.ExpirationDate;

        //        ShowDocumentInControl();
        //    }
        //    catch (Exception ex)
        //    {

        //        LOG.Error("DocReaderUI.ActualizaDocument - Error: " + ex.Message);
        //    }
        //    LOG.Error("DocReaderUI.ActualizaDocument - OUT!");
        //}

        //#endregion ZXing Zone
    }
}
