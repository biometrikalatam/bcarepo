﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bio.Core.Api;
using Bio.Core.Api.Constant;
using BioPortal.Server.Api;
using Domain;
using log4net;

namespace DocReaderUIAdapter.src.Helpers
{
    internal class BPHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BPHelper));

        /// <summary>
        /// Dada imagen de cedula de frente, reconoce caracteres con Namku
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="msg"></param>
        /// <param name="mRZIDCard"></param>
        /// <param name="trackIdBP"></param>
        /// <returns></returns>
        internal static int OCRIDCard(string imageCardFront, string run, out string msg, out Document document, out string trackIdBP)
        {
            //msg = null;
            //mRZIDCard = new CIParam();
            //document.TypeId = "RUT";
            //document.ValueId = "21284415-2";
            //document.Name = "Gustavo Suhit";
            //document.Nacionality = "ARG";
            //document.Sex = "M";
            //document.BirthDate = "28/09/1969";

            //trackIdBP = "1950ac4827c14334a807c3be19dd470a";
            //return 0;
            int ret = 0;
            msg = null;
            trackIdBP = null;
            document = null;
            try
            {
                LOG.Debug("BPHelper.OCRIDCard IN...");
                LOG.Debug("BPHelper.OCRIDCard RUN = " + (string.IsNullOrEmpty(run) ? "Sin RUN" : run) + "...");
                BioPortal.Server.Api.XmlParamOut paramout = null;
                XmlParamIn xmlParamIn = new XmlParamIn();

                using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
                {
                    ws.Url = Properties.Settings.Default.DR_BPWSUrl;
                    ws.Timeout = Properties.Settings.Default.DR_BPWSTimeout;
                    BioPortal.Server.Api.XmlParamIn paramin = new BioPortal.Server.Api.XmlParamIn();
                    paramin.Actionid = 4;
                    paramin.Companyid = Properties.Settings.Default.DR_BPCompanyId;
                    paramin.Clientid = "BPDR"; //oXmlIn.Clientid;
                    paramin.Origin = Properties.Settings.Default.DR_BPOriginId;
                    paramin.Authenticationfactor = 4;
                    paramin.Minutiaetype = 44;
                    paramin.Bodypart = 16;
                    paramin.Verifybyconnectorid = "MRZNamku";
                    paramin.OperationOrder = 3;
                    //paramin.InsertOption = 0; //No hace enroll porque es solo reconocimeinto de datos

                    paramin.PersonalData = new PersonalData();
                    paramin.PersonalData.Typeid = "RUN";
                    paramin.PersonalData.Valueid = (string.IsNullOrEmpty(run) ? "NA" : run);
                    paramin.PersonalData.DocImageFront = imageCardFront;
                    LOG.Debug("BPHelper.OCRIDCard oXmlIn.CIParam.TypeId/ValueId = " + paramin.PersonalData.Typeid + "/" + paramin.PersonalData.Valueid);
                    LOG.Debug("BPHelper.OCRIDCard oXmlIn.CIParam.IDCardImageFront = " + imageCardFront);

                    string xmlin = BioPortal.Server.Api.XmlUtils.SerializeAnObject(paramin);
                    string xmlout;
                    DateTime start = DateTime.Now;
                    int iret = ws.Get(xmlin, out xmlout);
                    DateTime end = DateTime.Now;
                    LOG.Debug("BPHelper.OCRIDCard Tiempo = " + (end - start).TotalMilliseconds.ToString() + " - ret = " + iret);
                    if (iret != 0)
                    {
                        ret = iret;
                        msg = "BPHelper.OCRIDCard - Error retornado desde BioPortal = " + iret;
                        if (!string.IsNullOrEmpty(xmlout))
                        {
                            paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlout);
                            if (paramout != null)
                            {
                                msg = msg + " - MsgErr = " +
                                    (string.IsNullOrEmpty(paramout.Message) ? "S/C" : paramout.Message);
                            }
                        }
                        LOG.Error(msg);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(xmlout))
                        {
                            ret = Errors.IERR_CONX_WS;
                            msg = "BPHelper.OCRIDCard - Retorno desde BioPOrtal Null";
                            LOG.Error(msg);
                        }
                        else
                        {
                            paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlout);
                            if (paramout != null)
                            {
                                //Chequeo ciertos valores básicos para rechazar si no reconoció bien
                                //if (paramout.PersonalData.Valueid.Equals("NA") || string.IsNullOrEmpty(paramout.PersonalData.Valueid) ||
                                //    string.IsNullOrEmpty(paramout.PersonalData.Patherlastname) ||
                                //    string.IsNullOrEmpty(paramout.PersonalData.Name) ||
                                //    string.IsNullOrEmpty(paramout.PersonalData.Photography))
                                //{
                                //    ret = Errors.IERR_BAD_PARAMETER;
                                //    msg = "BPHelper.OCRIDCard - Parseo XmlParamOut Null [" + Errors.SERR_BAD_PARAMETER + "]";
                                //    LOG.Error(msg);
                                //}

                                //Added 27/11 si vienen datos en los XmlParamIn los uso, porque pueden ser valores 
                                //ingresados a mano
                                document = new Document();
                                //document.TrackId = oXmlIn.CIParam.TrackId;
                                document.TypeId = "RUN";
                                document.ValueId = paramout.PersonalData.Valueid;
                                document.IDCardImageFront = paramout.PersonalData.DocImageFront;
                                document.BirthDate = paramout.PersonalData.Birthdate.ToString("dd/MM/yyyy");
                                document.ExpirationDate = paramout.PersonalData.Documentexpirationdate.ToString("dd/MM/yyyy");
                                document.IssueDate = paramout.PersonalData.Creation.ToString("dd/MM/yyyy");
                                document.Serial = paramout.PersonalData.Documentseriesnumber;
                                document.Name = paramout.PersonalData.Name;
                                document.PhaterLastName = paramout.PersonalData.Patherlastname;
                                document.MotherLastName = paramout.PersonalData.Motherlastname;
                                document.Nacionality = paramout.PersonalData.Nationality;
                                document.Sex = paramout.PersonalData.Sex;
                                document.IDCardPhotoImage = paramout.PersonalData.Photography;
                                document.IDCardSignatureImage = paramout.PersonalData.Signatureimage;
                                LOG.Debug("BPHelper.OCRIDCard - ValueId = " + document.ValueId + " - Nombre = " +
                                    (string.IsNullOrEmpty(document.Name) ? "" : document.Name) + " " +
                                    (string.IsNullOrEmpty(document.PhaterLastName) ? "" : document.PhaterLastName) +
                                    (string.IsNullOrEmpty(document.MotherLastName) ? "" : document.MotherLastName));
                                trackIdBP = paramout.Trackid;
                            }
                            else
                            {
                                ret = Errors.IERR_BAD_PARAMETER;
                                msg = "BPHelper.OCRIDCard - Parseo XmlParamOut Null";
                                LOG.Error(msg);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msg = "BPHelper.OCRIDCard Error = " + ex.Message;
                LOG.Error(msg);
            }
            return ret;
        }

        internal static int MRZIDCard(string imageCardFront, string run, out string msg, out Document document, out string trackIdBP)
        {
            msg = null;
            document = new Document();
            document.TypeId = "RUT";
            document.ValueId = "21284415-2";
            document.Name = "Gustavo Suhit";
            document.Nacionality = "ARG";
            document.Sex = "M";
            document.BirthDate = "28/09/1969";

            trackIdBP = "1950ac4827c14334a807c3be19dd470a";
            return 0;
        }

        /// <summary>
        /// Dado los datos necesarios, dice que una cedula está bloqueada en SRCeI via SIISA u otro proveedor.
        /// </summary>
        /// <param name="serial"></param>
        /// <param name="run"></param>
        /// <param name="msg"></param>
        /// <param name="document"></param>
        /// <param name="trackIdBP"></param>
        /// <returns></returns>
        internal static int BlockedDocument(string serial, string run, out string msg, out string trackIdBP)
        {
            msg = null;
            //mRZIDCard = new CIParam();
            msg = null;

            trackIdBP = "1950ac4827c14334a807c3be19dd470a";
            return 0;
            //int ret = 0;
            //msg = null;
            //trackIdBP = null;
            //document = null;
            //try
            //{
            //    LOG.Debug("BPHelper.BlockedDocument IN...");
            //    LOG.Debug("BPHelper.BlockedDocument RUN = " + (string.IsNullOrEmpty(run) ? "Sin RUN" : run) + "...");
            //    BioPortal.Server.Api.XmlParamOut paramout = null;
            //    XmlParamIn xmlParamIn = new XmlParamIn();

            //    using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
            //    {
            //        ws.Url = Properties.Settings.Default.DR_BPWSUrl;
            //        ws.Timeout = Properties.Settings.Default.DR_BPWSTimeout;
            //        BioPortal.Server.Api.XmlParamIn paramin = new BioPortal.Server.Api.XmlParamIn();
            //        paramin.Actionid = 4;
            //        paramin.Companyid = Properties.Settings.Default.DR_BPCompanyId;
            //        paramin.Clientid = "BPDR"; //oXmlIn.Clientid;
            //        paramin.Origin = Properties.Settings.Default.DR_BPOriginId;
            //        paramin.Authenticationfactor = 4;
            //        paramin.Minutiaetype = 44;
            //        paramin.Bodypart = 16;
            //        paramin.Verifybyconnectorid = "SIISA";
            //        paramin.OperationOrder = 3;
            //        //paramin.InsertOption = 0; //No hace enroll porque es solo reconocimeinto de datos

            //        paramin.PersonalData = new PersonalData();
            //        paramin.PersonalData.Typeid = "RUN";
            //        paramin.PersonalData.Valueid = (string.IsNullOrEmpty(run) ? "NA" : run);
            //        paramin.PersonalData.Documentseriesnumber = serial;
            //        //paramin.PersonalData.DocImageFront = imageCardFront;
            //        LOG.Debug("BPHelper.BlockedDocument oXmlIn.CIParam.TypeId/ValueId = " + paramin.PersonalData.Typeid + "/" + paramin.PersonalData.Valueid);
            //        LOG.Debug("BPHelper.BlockedDocument oXmlIn.CIParam.Serial = " + serial);

            //        string xmlin = BioPortal.Server.Api.XmlUtils.SerializeAnObject(paramin);
            //        string xmlout;
            //        DateTime start = DateTime.Now;
            //        int iret = ws.Get(xmlin, out xmlout);
            //        DateTime end = DateTime.Now;
            //        LOG.Debug("BPHelper.BlockedDocument Tiempo = " + (end - start).TotalMilliseconds.ToString() + " - ret = " + iret);
            //        if (iret != 0)
            //        {
            //            ret = iret;
            //            msg = "BPHelper.BlockedDocument - Error retornado desde BioPortal = " + iret;
            //            if (!string.IsNullOrEmpty(xmlout))
            //            {
            //                paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlout);
            //                if (paramout != null)
            //                {
            //                    msg = msg + " - MsgErr = " +
            //                        (string.IsNullOrEmpty(paramout.Message) ? "S/C" : paramout.Message);
            //                }
            //            }
            //            LOG.Error(msg);
            //        }
            //        else
            //        {
            //            if (string.IsNullOrEmpty(xmlout))
            //            {
            //                ret = Errors.IERR_CONX_WS;
            //                msg = "BPHelper.BlockedDocument - Retorno desde BioPOrtal Null";
            //                LOG.Error(msg);
            //            }
            //            else
            //            {
            //                paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlout);
            //                if (paramout != null)
            //                {
            //                    //Chequeo ciertos valores básicos para rechazar si no reconoció bien
            //                    if (paramout.PersonalData.Valueid.Equals("NA") || string.IsNullOrEmpty(paramout.PersonalData.Valueid) ||
            //                        string.IsNullOrEmpty(paramout.PersonalData.Patherlastname) ||
            //                        string.IsNullOrEmpty(paramout.PersonalData.Name) ||
            //                        string.IsNullOrEmpty(paramout.PersonalData.Photography))
            //                    {
            //                        ret = Errors.IERR_BAD_PARAMETER;
            //                        msg = "BPHelper.BlockedDocument - Parseo XmlParamOut Null [" + Errors.SERR_BAD_PARAMETER + "]";
            //                        LOG.Error(msg);
            //                    }

            //                    // TODO - PArsear resultado de blocqueo 
            //                    trackIdBP = paramout.Trackid;
            //                }
            //                else
            //                {
            //                    ret = Errors.IERR_BAD_PARAMETER;
            //                    msg = "BPHelper.OCRIDCard - Parseo XmlParamOut Null";
            //                    LOG.Error(msg);
            //                }
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ret = Errors.IERR_UNKNOWN;
            //    msg = "BPHelper.OCRIDCard Error = " + ex.Message;
            //    LOG.Error(msg);
            //}
            //return ret;
        }
    }
}
