﻿namespace CameraUIAdapter
{
    partial class CameraUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imageCam = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnCaptura = new System.Windows.Forms.Button();
            this.imgCapture = new System.Windows.Forms.PictureBox();
            this.btnEndCapture = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).BeginInit();
            this.SuspendLayout();
            // 
            // imageCam
            // 
            this.imageCam.Location = new System.Drawing.Point(9, 10);
            this.imageCam.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.imageCam.Name = "imageCam";
            this.imageCam.Size = new System.Drawing.Size(436, 479);
            this.imageCam.TabIndex = 0;
            this.imageCam.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(25, 555);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 37);
            this.button1.TabIndex = 1;
            this.button1.Text = "Iniciar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnCaptura
            // 
            this.btnCaptura.Location = new System.Drawing.Point(172, 555);
            this.btnCaptura.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCaptura.Name = "btnCaptura";
            this.btnCaptura.Size = new System.Drawing.Size(112, 37);
            this.btnCaptura.TabIndex = 2;
            this.btnCaptura.Text = "Capturar";
            this.btnCaptura.UseVisualStyleBackColor = true;
            this.btnCaptura.Click += new System.EventHandler(this.btnCaptura_Click);
            // 
            // imgCapture
            // 
            this.imgCapture.Location = new System.Drawing.Point(476, 10);
            this.imgCapture.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.imgCapture.Name = "imgCapture";
            this.imgCapture.Size = new System.Drawing.Size(436, 479);
            this.imgCapture.TabIndex = 3;
            this.imgCapture.TabStop = false;
            // 
            // btnEndCapture
            // 
            this.btnEndCapture.Location = new System.Drawing.Point(320, 555);
            this.btnEndCapture.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEndCapture.Name = "btnEndCapture";
            this.btnEndCapture.Size = new System.Drawing.Size(112, 37);
            this.btnEndCapture.TabIndex = 4;
            this.btnEndCapture.Text = "Finalizar";
            this.btnEndCapture.UseVisualStyleBackColor = true;
            this.btnEndCapture.Click += new System.EventHandler(this.btnEndCapture_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(25, 504);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 37);
            this.button2.TabIndex = 5;
            this.button2.Text = "Iniciar Timer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // CameraUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 602);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnEndCapture);
            this.Controls.Add(this.imgCapture);
            this.Controls.Add(this.btnCaptura);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.imageCam);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "CameraUI";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.CameraUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox imageCam;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnCaptura;
        private System.Windows.Forms.PictureBox imgCapture;
        private System.Windows.Forms.Button btnEndCapture;
        private System.Windows.Forms.Button button2;
    }
}

