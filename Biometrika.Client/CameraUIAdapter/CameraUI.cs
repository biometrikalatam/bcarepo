﻿using Bytescout.BarCodeReader;
using Domain;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace CameraUIAdapter
{
    public partial class CameraUI : Form
    {
        public bool HaveData { get; set; }

        private Capture captureCam;
        private BioPacket bioPacketLocal;
        private Reader reader = new Reader();
        

        public CameraUI()
        {
            InitializeComponent();
                        

            reader.RegistrationName = "demo";
            reader.RegistrationKey = "demo";

            // Set barcode type to find
            reader.BarcodeTypesToFind.QRCode = true;

            // Read barcodes
            
        }

        

        internal void Channel(BioPacket bioPacket)
        {
            bioPacketLocal = bioPacket;
        }

        private void CameraUI_Load(object sender, EventArgs e)
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }

        private System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return null
            ;
        }

        private void StopCamStream()
        {
            Application.Idle -= ShowCamStream;
            captureCam.Stop();
            captureCam.Dispose();
        }

        private void ShowCamStream(object sender, EventArgs e)

        {
            Mat mat = new Mat();
            captureCam.Retrieve(mat);
            Image<Bgr, Byte> img = mat.ToImage<Bgr, Byte>();

            //Image<Gray, Byte> grayImage = img.Convert<Gray, Byte>();
            //var faces = grayImage.DetectHaarCascade(
            //                                   haar, 1.4, 4,
            //                                   HAAR_DETECTION_TYPE.DO_CANNY_PRUNING,
            //                                   new Size(nextFrame.Width / 8, nextFrame.Height / 8)
            //                                   )[0];

            imageCam.Image = img.ToBitmap(imageCam.Image.Width, imageCam.Image.Height);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            captureCam = new Capture();
            Application.Idle += ShowCamStream;
           
        }

        private void btnCaptura_Click(object sender, EventArgs e)
        {
            imgCapture.Image = imageCam.Image;
        }

        private void btnEndCapture_Click(object sender, EventArgs e)
        {
            if (bioPacketLocal.PacketParamOut == null)
                throw new Exception("Este proceso se debe invocar asi el packet esta inicializado. USar el test o invocar");
            //bioPacketLocal.bioPacketParamOut = new BioPacketParamOut();
            using (MemoryStream stream = new MemoryStream())
            {
                imgCapture.Image.Save(stream, ImageFormat.Bmp);

                bioPacketLocal.PacketParamOut.Add("Image", Convert.ToBase64String(stream.ToArray()));
            }

            StopCamStream();
            HaveData = true;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            captureCam = new Capture();
            
        }
    }
}