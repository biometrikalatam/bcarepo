﻿using Bytescout.BarCodeReader;
using CameraUIAdapter.Constant;
using Domain;
using log4net;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TouchlessLib;

namespace CameraUIAdapter
{
    public partial class CameraUIB : Form
    {

        /* Descripción de parámetros:
         *  CAMTypeCapture, puede ser 1 o 2. Si es uno sólo captura imagen, si es 2, Captura imagen con reconocimiento de código de barra.
         *  CAMTypeOP, puede ser 1 o 2. Si es 1 es automático, se comporta como una pistola, reconoce el código de barra y sale. Si es 2, es Manual y se hace esa operación en forma manual.
         *  CAMTypeBcToRead, indica el tipo de código de barra a leer.  1 corresponde a PF417, 2 corresponde a QR, 3 lee cualquier tipo de código de barra 2D
         *  CAMImageW,indica el ancho de la imagen a capturar.
         *  CAMImageH,indica el alto de la imagen a capturar.
         *  CAMCameraSelection, puede ser 1 o 2. Si es 1, usa el nombre de la cámara seleccionado en el siguiente parámetro. Si es 2, muestra el listado de cámaras disponibles.
         *  CAMCameraSelected, corresponde al nombre de la cámara a usar.
        */
        public bool HaveData { get; set; }

        private String Photo { get; set; }

        private BioPacket bioPacketLocal;

        

        // Scan delay, ms.
        private int SCAN_DELAY = 1500; // scan barcodes every 1.5 sec

        // Touchless SDK library manager (to use it you should have TouchlessLib.dll referenced and WebCamLib.dll in the build output directory)
        //Esta es la librería que permite el manejo de la webcam.
        readonly TouchlessMgr _touchlessLibManager;

        // Background thread for barcode scanning
        readonly BackgroundWorker _backgroundWorker = new BackgroundWorker();
        // Synchronization event
        readonly AutoResetEvent _synchronizationEvent = new AutoResetEvent(false);

        //Variable de log.
        private static readonly ILog LOG = LogManager.GetLogger(typeof(CameraUIB));

        /// <summary>
        /// Variable que define los tipos de código de barra a leer.
        /// </summary>
        private BarcodeTypeSelector barcodeTypesToFind=new BarcodeTypeSelector();

        public CameraUIB()
        {
            LOG.Info("Biometrika BioCam - Se inicializa el componente de cámara");
            InitializeComponent();

            // Create Touchless library manager
            _touchlessLibManager = new TouchlessMgr();

            // Setup background worker
            _backgroundWorker.WorkerSupportsCancellation = true;
            _backgroundWorker.DoWork += BackgroundWorker_DoWork;
            _backgroundWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;

            LOG.Info("Biometrika BioCam - Terminamos de configurar la cámara");

        }

        internal int Channel(BioPacket bioPacket)
        {
            int ret = 0;
            LOG.Info("Biometrika BioCam.Channel In...");
            bioPacketLocal = bioPacket;

            ret = CheckParameters(bioPacketLocal);

            if (ret == 0)
            {
                LOG.Info("Biometrika BioCam - Parámetros configurados correctamente");
                if (bioPacketLocal.PacketParamOut == null)
                    bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
            }
            LOG.Info("Biometrika BioCam Out [ret=" + ret + "]...");
            return ret;
        }



        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        private int CheckParameters(BioPacket bioPacketLocal)
        {
            /*
                 src=bpc7&
                 CAMTypeCapture=&
                 CAMTypeOp=21284415-2&
                 CAMTypeBCToRead=ALL&
                 CAMImageW=600&
                 CAMImageH=480&
                 CAMCamaraSelection=1&
                 CAMCamaraSelected=7&
                 Theme=;              
            */
            int ret = 0;
            try
            {
                LOG.Info("Biometrika BioCam.CheckParameters In...");
                if (bioPacketLocal == null) ret = -2;  //Lista de parametros no puede ser nulo

                LOG.Debug("Biometrika BioCam.CheckParameters filling dictParamin...");
                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    LOG.Debug("Biometrika BioCam.CheckParameters     Adding => " + k + "=" + (string)bioPacketLocal.PacketParamIn[k] + "...");
                    dictParamIn.Add(k.ToUpper(), bioPacketLocal.PacketParamIn[k]);
                }
                LOG.Debug("Biometrika BioCam.CheckParameters dictParamin filled [length = " + dictParamIn.Count + "]");
                if (!dictParamIn.ContainsKey("SRC") || String.IsNullOrEmpty(dictParamIn["SRC"]))
                {
                    ret = -3;  //src/TypeId/ValueId deben ser enviados como parámetros
                    LOG.Debug("Biometrika BioCam.CheckParameters src deben ser enviados como parámetros!");
                }
                else
                {
                    
                    if (!dictParamIn.ContainsKey("CAMTYPECAPTURE") || String.IsNullOrEmpty(dictParamIn["CAMTYPECAPTURE"]))
                    {
                        LOG.Info("Biometrika BioCam.CheckParameters El parámetro CAMTYPECAPTURE no cumple con los solicitado, se procede a completar");
                        
                        if (!dictParamIn.ContainsKey("CAMTYPECAPTURE"))
                        {
                            LOG.Info("Biometrika BioCam - Obtenemos la configuración desde el archivo app.config");
                            LOG.Info("CAMTYPECAPTURE:" + Properties.Settings.Default.CAMTypeCapture.ToString());
                            dictParamIn.Add("CAMTYPECAPTURE", Properties.Settings.Default.CAMTypeCapture.ToString());
                        }
                        else
                        {
                            LOG.Info("CAMTYPECAPTURE:" + Properties.Settings.Default.CAMTypeCapture.ToString());
                            dictParamIn["CAMTYPECAPTURE"] = Properties.Settings.Default.CAMTypeCapture.ToString();
                        }
                    }
                    if (!dictParamIn.ContainsKey("CAMTYPEOP") || String.IsNullOrEmpty(dictParamIn["CAMTYPEOP"]))
                    {
                        LOG.Info("Biometrika BioCam.CheckParameters El parámetro CAMTYPEOP no cumple con los solicitado, se procede a completar");
                        if (!dictParamIn.ContainsKey("CAMTYPEOP"))
                        {
                            LOG.Info("CAMTYPEOP:" + Properties.Settings.Default.CAMTypeOp.ToString());
                            dictParamIn.Add("CAMTYPEOP", Properties.Settings.Default.CAMTypeOp.ToString());
                        }
                        else
                        {
                            LOG.Info("CAMTYPEOP:" + Properties.Settings.Default.CAMTypeOp.ToString());
                            dictParamIn["CAMTYPEOP"] = Properties.Settings.Default.CAMTypeOp.ToString();
                        }
                    }
                    if (!dictParamIn.ContainsKey("CAMTYPEBCTOREAD") || String.IsNullOrEmpty(dictParamIn["CAMTYPEBCTOREAD"]))
                    {
                        LOG.Info("Biometrika BioCam.CheckParameters El parámetro CAMTYPEBCTOREAD no cumple con los solicitado, se procede a completar");
                        if (!dictParamIn.ContainsKey("CAMTYPEBCTOREAD"))
                        {
                            LOG.Info("CAMTYPEBCTOREAD:" + Properties.Settings.Default.CAMTypeBcToRead.ToString());
                            dictParamIn.Add("CAMTYPEBCTOREAD", Properties.Settings.Default.CAMTypeBcToRead.ToString());
                        }
                        else
                        {
                            LOG.Info("CAMTYPEBCTOREAD:" + Properties.Settings.Default.CAMTypeBcToRead.ToString());
                            dictParamIn["CAMTYPEBCTOREAD"] = Properties.Settings.Default.CAMTypeBcToRead.ToString();
                        }
                    }
                    if (!dictParamIn.ContainsKey("CAMIMAGEW") || String.IsNullOrEmpty(dictParamIn["CAMIMAGEW"]))
                    {
                        LOG.Info("Biometrika BioCam.CheckParameters El parámetro CAMIMAGEW no cumple con los solicitado, se procede a completar");
                        if (!dictParamIn.ContainsKey("CAMIMAGEW"))
                        {
                            dictParamIn.Add("CAMIMAGEW",Properties.Settings.Default.CAMImageW.ToString());
                        }
                        else
                        {
                            dictParamIn["CAMIMAGEW"] = Properties.Settings.Default.CAMImageW.ToString();
                        }
                    }
                    if (!dictParamIn.ContainsKey("CAMIMAGEH") || String.IsNullOrEmpty(dictParamIn["CAMIMAGEH"]))
                    {
                        LOG.Info("Biometrika BioCam.CheckParameters El parámetro CAMIMAGEH no cumple con los solicitado, se procede a completar");
                        if (!dictParamIn.ContainsKey("CAMIMAGEH"))
                        {
                            dictParamIn.Add("CAMIMAGEH", Properties.Settings.Default.CAMImageH.ToString());
                        }
                        else
                        {
                            dictParamIn["CAMIMAGEH"] = Properties.Settings.Default.CAMImageH.ToString();
                        }
                    }
                    if (!dictParamIn.ContainsKey("CAMCAMARASELECTION") || String.IsNullOrEmpty(dictParamIn["CAMCAMARASELECTION"]))
                    {
                        LOG.Info("Biometrika BioCam.CheckParameters El parámetro CAMCAMARASELECTION no cumple con los solicitado, se procede a completar");
                        if (!dictParamIn.ContainsKey("CAMCAMARASELECTION"))
                        {
                            dictParamIn.Add("CAMCAMARASELECTION", Properties.Settings.Default.CAMCameraSelection.ToString());
                        }
                        else
                        {
                            dictParamIn["CAMCAMARASELECTION"] = Properties.Settings.Default.CAMCameraSelection.ToString();
                        }
                    }
                    if (!dictParamIn.ContainsKey("CAMCAMARASELECTED") || String.IsNullOrEmpty(dictParamIn["CAMCAMARASELECTED"]))
                    {
                        LOG.Info("Biometrika BioCam.CheckParameters El parámetro CAMCAMARASELECTED no cumple con los solicitado, se procede a completar");
                        if (!dictParamIn.ContainsKey("CAMCAMARASELECTED"))
                        {
                            dictParamIn.Add("CAMCAMARASELECTED", Properties.Settings.Default.CAMCameraSelected.ToString());
                        }
                        else
                        {
                            dictParamIn["CAMCAMARASELECTED"] = Properties.Settings.Default.CAMCameraSelected.ToString();
                        }
                    }

                }
                

            }
            catch (Exception ex)
            {
                LOG.Error("Error Biometrika BioCam.CheckParameters [" + ex.Message + "]");
                ret = -1;
            }
            LOG.Info("Biometrika BioCam.CheckParameters Ount [ret=" + ret + "]!");
            return ret;
        }

        // Background thread is finished
        private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Update UI asynchronously
            if(e.Cancelled==false)
                BeginInvoke(new Action<RunWorkerCompletedEventArgs>(OnBackgroundWorkerFinished), new object[] { e });
            else
            {
                CloseForm();
            }
        }

        void OnBackgroundWorkerFinished(RunWorkerCompletedEventArgs completedEventArgs)
        {
            if (completedEventArgs.Cancelled)
            {
                lblScanning.Text = "Detener";
            }
            else if (completedEventArgs.Error != null)
            {
                lblScanning.Text = "Error: " + completedEventArgs.Error.Message;
            }
            else
            {
                lblScanning.Text = "Encontrado";
            }

            UpdateControls(false);
        }


        public void BlockControl(bool value)
        {
            cmbCamera.Enabled = value;

        }

        // Background thread procedure used by BackgroundWorker
        public void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            //BarcodeTypeSelector barcodeTypesToFind = (BarcodeTypeSelector)e.Argument;

            // Create and setup barcode reader instance
            using (Reader reader = new Reader())
            {
                

                reader.RegistrationName = "demo";
                reader.RegistrationKey = "demo";
                Thread.Sleep(100);
                //Definimos el tipo de código a filtrar
                if(picPdf417.Image == CameraUIAdapter.Properties.Resources.codigo_pdf417_bn1)
                {
                    LOG.Info("Biometrika Cam - Sólo se encuentra activo el tipo de código PDF417");
                    reader.BarcodeTypesToFind.PDF417 = true;
                    reader.BarcodeTypesToFind.QRCode = false;
                }
                else if (picQR.Image == CameraUIAdapter.Properties.Resources.codigo_qr_bn1)
                {
                    LOG.Info("Biometrika Cam - Sólo se encuentra activo el tipo de código QR");
                    reader.BarcodeTypesToFind.QRCode = true;
                    reader.BarcodeTypesToFind.PDF417 = false;

                }
                else
                {
                    LOG.Info("Biometrika Cam - Sólo se encuentra activo el tipo de código PDF417 y QR");
                    reader.BarcodeTypesToFind.PDF417 = true;
                    reader.BarcodeTypesToFind.QRCode = true;
                }
                reader.TextEncoding = Encoding.Default; // default ANSI encoding
                Thread.Sleep(500);
                // Work while not canceled
                while (true)
                {
                    // Check cancellation
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        _synchronizationEvent.Set();
                        return;
                    }
                    try
                    { 
                        // Get image from camera by invoking method from UI thread
                        Bitmap bitmap = (Bitmap)Invoke(new GetCameraImageDelegate(GetCameraImage));
                        if (bitmap == null)
                        {
                            e.Result = null;
                            return;
                        }

                        // Search the image for barcodes
                        FoundBarcode[] result = reader.ReadFrom(bitmap);

                        // Update UI asynchronously
                        BeginInvoke(new Action<FoundBarcode[]>(UpdateStatus), new object[] { result });

                        // Pause
                        Thread.Sleep(SCAN_DELAY);
                    }
                    catch(ObjectDisposedException ignore)
                    { }
                }
            }
        }

        // Update UI with found barcodes information
        void UpdateStatus(FoundBarcode[] foundBarcodes)
        {
            byte[] mypicture = null;
            String value = "";
            String type = "";
            float confidence = 0;

            if (foundBarcodes != null && foundBarcodes.Length > 0)
            {
                Bitmap fotobmp = null;
                fotobmp = GetCameraImage();
                // Play sound if we found any barcode
                SystemSounds.Beep.Play();

                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.AppendFormat("Time: {0:HH:mm:ss:tt}", DateTime.Now);
                stringBuilder.AppendLine();
                bool readall= false;
                // Display found barcodes in the output text box
                foreach (FoundBarcode barcode in foundBarcodes)
                {
                    
                    

                    stringBuilder.AppendFormat("Código de barra encontrados: {0} {1}", barcode.Type, barcode.Value);
                    stringBuilder.AppendLine();
                    if (bioPacketLocal.PacketParamOut == null)
                        throw new Exception("Este proceso se debe invocar asi el packet esta inicializado. USar el test o invocar");
                    //bioPacketLocal.bioPacketParamOut = new BioPacketParamOut();
                    if (dictParamIn["CAMTYPEBCTOREAD"] == ((int)Configuration.BCType.ALL).ToString())
                    {
                        if((barcode.Type== Bytescout.BarCodeReader.SymbologyType.QRCode) || (barcode.Type== Bytescout.BarCodeReader.SymbologyType.PDF417))
                        {
                            readall = true;
                        }
                    }
                    
                    
                    MemoryStream memoryStream = new MemoryStream();
                    fotobmp.Save(memoryStream, ImageFormat.Jpeg);
                    memoryStream.Position = 0;
                    mypicture = memoryStream.ToArray();
                    type = barcode.Type.ToString();
                    //value = barcode.Value.ToString();
                    confidence = barcode.Confidence;

                    String recorte = barcode.Value;
                    String pdf417="";
                    String qr = "";
                    int indice = 0;
                   
                    if (barcode.Type == Bytescout.BarCodeReader.SymbologyType.PDF417)
                    {
                        LOG.Info("Biometrika Cam - retorna el contenido del PDF417");
                        LOG.Debug("Biometrika Cam - Código PDF:" + barcode.Value);
                        LOG.Debug("Biometrika Cam - Código PDF:" + barcode.Value.Length);
                        value = barcode.Value.Substring(0, 40);
                    }
                    else if(barcode.Type == Bytescout.BarCodeReader.SymbologyType.QRCode)
                    {
                        LOG.Info("Biometrika Cam - retorna el contenido del qr");
                        LOG.Debug("Biometrika Cam - Código QR:" + barcode.Value);
                        int largo = (">TRIAL VERSION EXPIRES 90 DAYS AFTER INSTALLATION<").Length;
                        value = barcode.Value.Substring(0, barcode.Value.Length - largo);
                    }

                    memoryStream.Close();
                    if (bioPacketLocal.PacketParamOut.Count > 0)
                    {
                        bioPacketLocal.PacketParamOut = null;
                        bioPacketLocal.PacketParamOut = new Dictionary<string, object>();
                    }
                    SetPackageResponse(Convert.ToBase64String(mypicture), value, type, confidence, (int)Errors.Response.None, "");

                }

                rtbFoundBarcodes.Text = stringBuilder.ToString();

                // Update status text with number of found barcodes
                lblFoundBarcodes.Text = string.Format("Código de barra encontrados: {0}:", foundBarcodes.Length);
            }

            // Make "Scanning..." label flicker.
            lblScanning.Visible = !lblScanning.Visible;
            lblScanning.Refresh();

            // Check if we need to stop on first barcode found
            if (dictParamIn["CAMTYPEOP"].ToString() == "1" && foundBarcodes != null && foundBarcodes.Length > 0)
            {
                StopDecoding();
                

            }
        }

        public void StartDecoding()
        {
            if (cmbCamera.SelectedIndex == -1)
                return;

            // Clear the output text box
            rtbFoundBarcodes.Clear();

            // Check if we have camera selected
            if (cmbCamera.SelectedIndex != -1)
            {
                // Start the decoding in the background thread
                //BarcodeTypeSelector barcodeTypesToFind = GetBarcodeTypeFromCombobox();
                _backgroundWorker.RunWorkerAsync(barcodeTypesToFind);

                UpdateControls(true);
            }
            else
            {
                MessageBox.Show("Please select the camera first!");
            }
        }

        

        void UpdateControls(bool started)
        {
            if (started)
            {
                //cmbBarcodeType.Enabled = false;
                cmbCamera.Enabled = false;
                cbStopOnFirstBarcode.Enabled = false;
                lblScanning.Visible = true;
                lblScanning.Text = "Scanning...";
            }
            else
            {
                //cmbBarcodeType.Enabled = true;
                cmbCamera.Enabled = true;
                cbStopOnFirstBarcode.Enabled = true;                
                lblScanning.Visible = true;
            }
        }


        private void StopDecoding()
        {
            _backgroundWorker.CancelAsync();

            // Wait until BackgroundWorker finished
            if (_backgroundWorker.IsBusy)
                _synchronizationEvent.WaitOne();

            UpdateControls(false);
        }



        delegate Bitmap GetCameraImageDelegate();

        Bitmap GetCameraImage()
        {
            if (!IsDisposed && !Disposing && _touchlessLibManager.CurrentCamera != null)
                return _touchlessLibManager.CurrentCamera.GetCurrentImage();

            return null;
        }

        public void SetPackageResponse(String image, String content, String typebarcode, float confidence, int error, String msg)
        {
            bioPacketLocal.PacketParamOut.Add("Imagen", image);
            bioPacketLocal.PacketParamOut.Add("Content", content);
            bioPacketLocal.PacketParamOut.Add("TypeBarCode", typebarcode);
            bioPacketLocal.PacketParamOut.Add("Confidence",confidence);
            bioPacketLocal.PacketParamOut.Add("Error", error);
            bioPacketLocal.PacketParamOut.Add("Mensaje", msg);
        }



        private void CamaraUIB_Load(object sender, EventArgs e)
        {
   
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;

            LOG.Info("Biometrika Cam - Se listan las cámaras configurados");
            // Fill devices combobox with available video cameras
            foreach (Camera camera in _touchlessLibManager.Cameras)
            { 
                cmbCamera.Items.Add(camera);
                
            }

            LOG.Info("Biometrika Cam - Se configura el delay");
            if(CameraUIAdapter.Properties.Settings.Default.ScanDelay>0)
            {
                LOG.Info("Biometrika Cam - Se configura un tiempo de acuerdo al archivo de configuración");
                SCAN_DELAY = CameraUIAdapter.Properties.Settings.Default.ScanDelay;
            }
            else
            {
                LOG.Info("Biometrika Cam - Se configura un tiempo por defecto");
                SCAN_DELAY = 1000;
            }

            ShowControl();                       

            // Select the first available camera. See also cmbCamera_SelectedIndexChanged event handler.
            if (_touchlessLibManager.Cameras.Count > 0)
            { 
                cmbCamera.SelectedItem = _touchlessLibManager.Cameras[0];
                LOG.Info("BioCam - Cantidad de códigos de barra obtenidos:" + _touchlessLibManager.Cameras.Count);
            }
            else
            {
                LOG.Info("BIOCam - No hay cámara configuradas");
                //Retornamos error y cerramos componente
                BlockControl(true);
                lblError.Text = "Cámara no conectada o no corresponde con el nombre configurado";
                cmbCamera.Enabled = false;
                lblError.ForeColor = Color.Red;                
                SetPackageResponse("","","",0,(int)WebCam.WebCamError.NotFound,"No se encuentra la cámara conectada");
                LOG.Info("BioCam - Se bloquean los controles");

            }

            
            
            ShowPanelBarcodeType();

        }

        /// <summary>
        /// Se muestran los controles en forma desactivada la primera vez.
        /// </summary>
        private void ShowControl()
        {
            picPdf417.Image = CameraUIAdapter.Properties.Resources.codigo_pdf417_bn;
            picQR.Image = CameraUIAdapter.Properties.Resources.codigo_qr_bn;
        }

        /// <summary>
        /// Permite mostrar o ocultar los botones de tipos de código, influyen 2 parámetros para su funcionamiento.
        /// CAMTYPECAPTURE y CAMTYPEBCTOREAD
        /// </summary>
        private void ShowPanelBarcodeType()
        {
            
            if(dictParamIn["CAMTYPECAPTURE"]==((int)Configuration.TypeCapture.OnlyImagen).ToString())
            {
                LOG.Info("Biometrika BioCam - Sólo se activa el componente para sacar foto sin reconocimiento de barcode");
                
                lblFoundBarcodes.Text = "No esta activo el reconocimiento de código de barra";
                picTakePicture.Enabled = true;
                picPdf417.Image = CameraUIAdapter.Properties.Resources.codigo_pdf417_bn;
                picQR.Image = CameraUIAdapter.Properties.Resources.codigo_qr_bn1;

            }
            else
            {
                picPdf417.Visible = true;
                picQR.Visible = true;
                LOG.Info("Biometrika BioCam - Se activa la foto y reconocimiento de fotografía");

                StartDecoding();
               
                if(dictParamIn["CAMTYPEBCTOREAD"]== ((int)Configuration.BCType.ALL).ToString())
                {
                    picPdf417.Image = CameraUIAdapter.Properties.Resources.codigo_pdf417_color;
                    picQR.Image = CameraUIAdapter.Properties.Resources.codigo_qr_color;
                    barcodeTypesToFind.All2D = true;
                   

                }
                else if(dictParamIn["CAMTYPEBCTOREAD"] == ((int)Configuration.BCType.QR).ToString())
                {
                    picPdf417.Image = CameraUIAdapter.Properties.Resources.codigo_pdf417_bn;
                    picQR.Image = CameraUIAdapter.Properties.Resources.codigo_qr_color;
                    barcodeTypesToFind.QRCode = true;
                }
                else if (dictParamIn["CAMTYPEBCTOREAD"] == ((int)Configuration.BCType.PDF417).ToString())
                {
                    picPdf417.Image = CameraUIAdapter.Properties.Resources.codigo_pdf417_color;
                    picQR.Image = CameraUIAdapter.Properties.Resources.codigo_qr_bn1;
                    barcodeTypesToFind.PDF417 = true;
                }
                //Se habilita el reconocimiento de QR.
                

            }
        }

        private System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return null
            ;
        }
       
        void CurrentCamera_OnImageCaptured(object sender, CameraEventArgs e)
        {
            pictureBoxPreview.Image = e.Image;
        }

        private void cmbCamera_RightToLeftChanged(object sender, EventArgs e)
        {

        }

        private void cmbCamera_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            { 
                if (_touchlessLibManager.CurrentCamera != null)
                    _touchlessLibManager.CurrentCamera.OnImageCaptured -= CurrentCamera_OnImageCaptured;

                if (cmbCamera.SelectedIndex != -1)
                {
                    Camera camera = _touchlessLibManager.Cameras[cmbCamera.SelectedIndex];

                    if (camera != null)
                    {
                        LOG.Info("Biometrika Cam - Se configura correctamente la webcam");
                        // Set camera output image dimensions
                        camera.CaptureWidth = Convert.ToInt32(dictParamIn["CAMIMAGEW"].ToString()) ;
                        camera.CaptureHeight = Convert.ToInt32(dictParamIn["CAMIMAGEH"].ToString());

                        camera.OnImageCaptured += CurrentCamera_OnImageCaptured;

                        // Select the camera
                        _touchlessLibManager.CurrentCamera = camera;
                        LOG.Info("Biometrika Cam - Finaliza la configuración de la cámara");
                    }
                }
            }
            catch(Exception exe)
            {
                LOG.Error("Biometrika CAM - Se produjo un error al capturar el video:" + exe.Message);
                LOG.Error("Biometrika CAM - Excepcion:", exe);
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            lblFoundBarcodes.Text = "Código de barra encontrados: 0";
            StartDecoding();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            StopDecoding();
        }

        private void btnCapturar_Click(object sender, EventArgs e)
        {
            String base64String = "";
            Bitmap fotobmp = null;
            fotobmp = GetCameraImage();
            MemoryStream memoryStream = new MemoryStream();
            fotobmp.Save(memoryStream, ImageFormat.Jpeg);
            memoryStream.Position = 0;
            byte[] byteBuffer = memoryStream.ToArray();


            memoryStream.Close();


            base64String = Convert.ToBase64String(byteBuffer);
            byteBuffer = null;



        }

       
        private void button1_Click(object sender, EventArgs e)
        {
            cmbCamera.SelectedIndex = 0;
        }

        private void picTakePicture_Click(object sender, EventArgs e)
        {
            LOG.Info("Biometrika BioCam - Se solicita sacar una foto");
            String base64String = "";
            Bitmap fotobmp = null;
            fotobmp = GetCameraImage();
            try
            {
                MemoryStream memoryStream = new MemoryStream();
                fotobmp.Save(memoryStream, ImageFormat.Jpeg);
                memoryStream.Position = 0;
                byte[] mypicture = memoryStream.ToArray();


                memoryStream.Close();

                
                base64String = Convert.ToBase64String(mypicture);
                mypicture = null;
                LOG.Info("Biometrika BioCam - Se genera el json de retorno");
                if (Convert.ToInt32(dictParamIn["CAMTYPEOP"])==(int)Configuration.TypeOperation.Automatic)
                {
                    
                    LOG.Info("Biometrika BioCam - Se trata de una captura automática, el componente se cerrará");
                    SetPackageResponse(base64String, "", "", 0, (int)Errors.Response.None, "");
                    CloseForm();
                }

            }
            catch(Exception exe)
            {
                SetPackageResponse(base64String, "", "", 0, (int)Errors.Response.ErrorCapturaFoto, "Se produjo un error al momento de capturar la foto");
                LOG.Error("Se produjo un error al tomar la foto:" + exe.Message, exe);
                CloseForm();

            }

        }

        private void CloseForm()
        {
            LOG.Info("Biometrika Cam - Se solicita cerrar la aplicación");
            _backgroundWorker.CancelAsync();

            // Wait until BackgroundWorker finished
            if (_backgroundWorker.IsBusy)
                _synchronizationEvent.WaitOne();

            UpdateControls(false);
            if (bioPacketLocal.PacketParamOut.Count == 0)
            {
                LOG.Info("No hay parámetros de salida");
                SetPackageResponse("", "", "", 0, (int)Errors.Response.CierreSinDatos, "Cerro el componente sin datos");

            }

            this.Close();
        }

        private void picCerrar_Click(object sender, EventArgs e)
        {

            CloseForm();

        }

        private void CameraUIB_FormClosing(object sender, FormClosingEventArgs e)
        {

            
            _touchlessLibManager.Dispose();

            base.OnClosing(e);
        }

        private void picTakePicture_Click_1(object sender, EventArgs e)
        {

        }
    }
}
