﻿namespace CameraUIAdapter
{
    partial class CameraUIB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CameraUIB));
            this.pictureBoxPreview = new System.Windows.Forms.PictureBox();
            this.lblFoundBarcodes = new System.Windows.Forms.Label();
            this.rtbFoundBarcodes = new System.Windows.Forms.RichTextBox();
            this.lblScanning = new System.Windows.Forms.Label();
            this.cbStopOnFirstBarcode = new System.Windows.Forms.CheckBox();
            this.cmbCamera = new System.Windows.Forms.ComboBox();
            this.lblError = new System.Windows.Forms.Label();
            this.picPdf417 = new System.Windows.Forms.PictureBox();
            this.picQR = new System.Windows.Forms.PictureBox();
            this.picCerrar = new System.Windows.Forms.PictureBox();
            this.picBiometrika = new System.Windows.Forms.PictureBox();
            this.picAyuda = new System.Windows.Forms.PictureBox();
            this.picActualizar = new System.Windows.Forms.PictureBox();
            this.picTakePicture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPdf417)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBiometrika)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAyuda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picActualizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTakePicture)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxPreview
            // 
            this.pictureBoxPreview.Location = new System.Drawing.Point(25, 123);
            this.pictureBoxPreview.Name = "pictureBoxPreview";
            this.pictureBoxPreview.Size = new System.Drawing.Size(630, 462);
            this.pictureBoxPreview.TabIndex = 0;
            this.pictureBoxPreview.TabStop = false;
            // 
            // lblFoundBarcodes
            // 
            this.lblFoundBarcodes.AutoSize = true;
            this.lblFoundBarcodes.BackColor = System.Drawing.Color.White;
            this.lblFoundBarcodes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFoundBarcodes.Location = new System.Drawing.Point(32, 585);
            this.lblFoundBarcodes.Name = "lblFoundBarcodes";
            this.lblFoundBarcodes.Size = new System.Drawing.Size(179, 15);
            this.lblFoundBarcodes.TabIndex = 3;
            this.lblFoundBarcodes.Text = "Código de barra encontrados: 0";
            // 
            // rtbFoundBarcodes
            // 
            this.rtbFoundBarcodes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbFoundBarcodes.Location = new System.Drawing.Point(25, 614);
            this.rtbFoundBarcodes.Name = "rtbFoundBarcodes";
            this.rtbFoundBarcodes.Size = new System.Drawing.Size(630, 30);
            this.rtbFoundBarcodes.TabIndex = 8;
            this.rtbFoundBarcodes.Text = "";
            // 
            // lblScanning
            // 
            this.lblScanning.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblScanning.AutoSize = true;
            this.lblScanning.BackColor = System.Drawing.Color.White;
            this.lblScanning.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblScanning.Location = new System.Drawing.Point(485, 585);
            this.lblScanning.Name = "lblScanning";
            this.lblScanning.Size = new System.Drawing.Size(72, 13);
            this.lblScanning.TabIndex = 25;
            this.lblScanning.Text = "Scanning...";
            this.lblScanning.Visible = false;
            // 
            // cbStopOnFirstBarcode
            // 
            this.cbStopOnFirstBarcode.AutoSize = true;
            this.cbStopOnFirstBarcode.Location = new System.Drawing.Point(25, 19);
            this.cbStopOnFirstBarcode.Name = "cbStopOnFirstBarcode";
            this.cbStopOnFirstBarcode.Size = new System.Drawing.Size(266, 17);
            this.cbStopOnFirstBarcode.TabIndex = 31;
            this.cbStopOnFirstBarcode.Text = "Detener ante el reconocimiento de código de barra";
            this.cbStopOnFirstBarcode.UseVisualStyleBackColor = true;
            this.cbStopOnFirstBarcode.Visible = false;
            // 
            // cmbCamera
            // 
            this.cmbCamera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCamera.FormattingEnabled = true;
            this.cmbCamera.Location = new System.Drawing.Point(137, 96);
            this.cmbCamera.Name = "cmbCamera";
            this.cmbCamera.Size = new System.Drawing.Size(179, 21);
            this.cmbCamera.TabIndex = 33;
            this.cmbCamera.SelectedIndexChanged += new System.EventHandler(this.cmbCamera_SelectedIndexChanged);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.BackColor = System.Drawing.Color.White;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.Location = new System.Drawing.Point(59, 466);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 20);
            this.lblError.TabIndex = 34;
            // 
            // picPdf417
            // 
            this.picPdf417.Image = global::CameraUIAdapter.Properties.Resources.codigo_pdf417_bn1;
            this.picPdf417.Location = new System.Drawing.Point(451, 84);
            this.picPdf417.Name = "picPdf417";
            this.picPdf417.Size = new System.Drawing.Size(54, 33);
            this.picPdf417.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picPdf417.TabIndex = 1;
            this.picPdf417.TabStop = false;
            // 
            // picQR
            // 
            this.picQR.Image = global::CameraUIAdapter.Properties.Resources.codigo_qr_bn1;
            this.picQR.Location = new System.Drawing.Point(391, 84);
            this.picQR.Name = "picQR";
            this.picQR.Size = new System.Drawing.Size(54, 33);
            this.picQR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picQR.TabIndex = 0;
            this.picQR.TabStop = false;
            // 
            // picCerrar
            // 
            this.picCerrar.Image = global::CameraUIAdapter.Properties.Resources.menu_cerrar_color;
            this.picCerrar.Location = new System.Drawing.Point(622, 12);
            this.picCerrar.Name = "picCerrar";
            this.picCerrar.Size = new System.Drawing.Size(44, 44);
            this.picCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picCerrar.TabIndex = 3;
            this.picCerrar.TabStop = false;
            this.picCerrar.Click += new System.EventHandler(this.picCerrar_Click);
            // 
            // picBiometrika
            // 
            this.picBiometrika.Image = global::CameraUIAdapter.Properties.Resources.menu_bk_color;
            this.picBiometrika.Location = new System.Drawing.Point(574, 12);
            this.picBiometrika.Name = "picBiometrika";
            this.picBiometrika.Size = new System.Drawing.Size(44, 44);
            this.picBiometrika.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBiometrika.TabIndex = 2;
            this.picBiometrika.TabStop = false;
            // 
            // picAyuda
            // 
            this.picAyuda.Image = global::CameraUIAdapter.Properties.Resources.menu_ayuda_color;
            this.picAyuda.Location = new System.Drawing.Point(528, 12);
            this.picAyuda.Name = "picAyuda";
            this.picAyuda.Size = new System.Drawing.Size(44, 44);
            this.picAyuda.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picAyuda.TabIndex = 1;
            this.picAyuda.TabStop = false;
            // 
            // picActualizar
            // 
            this.picActualizar.Image = global::CameraUIAdapter.Properties.Resources.menu_actualizar_color;
            this.picActualizar.Location = new System.Drawing.Point(488, 12);
            this.picActualizar.Name = "picActualizar";
            this.picActualizar.Size = new System.Drawing.Size(44, 44);
            this.picActualizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picActualizar.TabIndex = 0;
            this.picActualizar.TabStop = false;
            // 
            // picTakePicture
            // 
            this.picTakePicture.Image = global::CameraUIAdapter.Properties.Resources.tomarfoto_color1;
            this.picTakePicture.Location = new System.Drawing.Point(549, 92);
            this.picTakePicture.Name = "picTakePicture";
            this.picTakePicture.Size = new System.Drawing.Size(114, 29);
            this.picTakePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picTakePicture.TabIndex = 35;
            this.picTakePicture.TabStop = false;
            this.picTakePicture.Click += new System.EventHandler(this.picTakePicture_Click_1);
            // 
            // CameraUIB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CameraUIAdapter.Properties.Resources.afex_app_sinmenu;
            this.ClientSize = new System.Drawing.Size(678, 691);
            this.ControlBox = false;
            this.Controls.Add(this.picTakePicture);
            this.Controls.Add(this.picCerrar);
            this.Controls.Add(this.picPdf417);
            this.Controls.Add(this.picBiometrika);
            this.Controls.Add(this.picAyuda);
            this.Controls.Add(this.picQR);
            this.Controls.Add(this.picActualizar);
            this.Controls.Add(this.rtbFoundBarcodes);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.cmbCamera);
            this.Controls.Add(this.cbStopOnFirstBarcode);
            this.Controls.Add(this.lblScanning);
            this.Controls.Add(this.lblFoundBarcodes);
            this.Controls.Add(this.pictureBoxPreview);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CameraUIB";
            this.Text = "Biometrika - WebCam";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CameraUIB_FormClosing);
            this.Load += new System.EventHandler(this.CamaraUIB_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPdf417)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBiometrika)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAyuda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picActualizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTakePicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxPreview;
        private System.Windows.Forms.Label lblFoundBarcodes;
        private System.Windows.Forms.RichTextBox rtbFoundBarcodes;
        private System.Windows.Forms.Label lblScanning;
        private System.Windows.Forms.CheckBox cbStopOnFirstBarcode;
        private System.Windows.Forms.ComboBox cmbCamera;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.PictureBox picQR;
        private System.Windows.Forms.PictureBox picPdf417;
        private System.Windows.Forms.PictureBox picActualizar;
        private System.Windows.Forms.PictureBox picAyuda;
        private System.Windows.Forms.PictureBox picBiometrika;
        private System.Windows.Forms.PictureBox picCerrar;
        private System.Windows.Forms.PictureBox picTakePicture;
    }
}