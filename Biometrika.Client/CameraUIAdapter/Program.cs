﻿using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Forms;

namespace CameraUIAdapter
{
    public static class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        public static bool HaveData
        {
            get
            {
                return cameraUIB == null ? false : cameraUIB.HaveData;
            }
        }

        public static BioPacket Packet { get; set; }

        private static CameraUIB cameraUIB;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            

            Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);

            cameraUIB = new CameraUIB();
            cameraUIB.Channel(Packet);
            Application.Run(cameraUIB);           // ejecuta no debe hacer show

        }

        public static void Close()
        {
            cameraUIB.Close();
            //cameraUI.Dispose(true);
            cameraUIB = null;
        }

        public static void SetPacket(NameValueCollection queryString)
        {
            Packet = new BioPacket();
            Packet.PacketParamIn = queryString;
            Packet.PacketParamOut = new Dictionary<string, object>();
        }

        //public static void Hide()
        //{
        //    cameraUI.Hide();
        //}

        //public static void Show()
        //{
        //    cameraUI.Show();
        //}
    }
}