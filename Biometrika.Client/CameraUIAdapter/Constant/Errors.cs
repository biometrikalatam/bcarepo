﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraUIAdapter.Constant
{
    public class Errors
    {
        public enum Response
        {
            None=0,
            CierreSinDatos=-2000,
            ErrorCapturaFoto=-2001
        }
    }
}
