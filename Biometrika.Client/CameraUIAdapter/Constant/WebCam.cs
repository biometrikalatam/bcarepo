﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraUIAdapter.Constant
{
    
    public static class WebCam
    {
        public  enum WebCamError
        {
            [Description("No error")]
            None=0,
            [Description("No se encuentra la cámara conectada o no coincide con el nombre configurado")]
            NotFound =-1000
        }

    }
}
