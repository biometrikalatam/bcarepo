﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraUIAdapter.Constant
{
    public static class Configuration
    {
        /// <summary>
        /// Tipo de configuración para lectura del código de barra
        /// </summary>
        public enum BCType
        {
            PDF417=1,
            QR=2,
            ALL=3
        }

        /// <summary>
        /// Define si se usará solo para captura de una imagen o una imagen con reconocimiento
        /// </summary>
        public enum TypeCapture
        {
            OnlyImagen = 1,
            ImagewithRecog = 2            
        }

        /// <summary>
        /// Define el tipo de operación del componente emulará la captura de una imagen o el 
        /// reconocimiento de esta.
        /// </summary>
        public enum TypeOperation
        {
            Automatic=1,
            Manual=2
        }

        /// <summary>
        /// Define si la cámara será seleccionable o será definida por parámetro de configuración
        /// </summary>
        public enum SelectCamara
        {
            Default=1,
            All=2
        }

    }
}
