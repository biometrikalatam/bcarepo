﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TouchlessLib;

namespace BiometrikaCAM_Test
{
    public partial class frmTest : Form
    {
        // Touchless SDK library manager (to use it you should have TouchlessLib.dll referenced and WebCamLib.dll in the build output directory)
        //Esta es la librería que permite el manejo de la webcam.
        readonly TouchlessMgr _touchlessLibManager;

        private int alto;
        private int ancho;

        private CamApiResponse CamApiResponse;

        public frmTest()
        {
            // Create Touchless library manager
            _touchlessLibManager = new TouchlessMgr();


            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void frmTest_Load(object sender, EventArgs e)
        {

            Limpiar();
            // Fill devices combobox with available video cameras
            foreach (Camera camera in _touchlessLibManager.Cameras)
            {
                cmbCamera.Items.Add(camera);

            }
            if (_touchlessLibManager.Cameras.Count > 0)
                cmbCamera.SelectedIndex = 0;

            if (cmbCamera.SelectedIndex == -1)
            { 
                cmbCamera.Text = "No hay cámaras conectadas";
                MessageBox.Show("No existen cámaras conectadas, el componente de captura de código de barra no puede funcionar", "Biometrika", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }

            cmbCamTypeCapture.SelectedIndex = 0;
            cmbCamTypeOp.SelectedIndex = 0;
            cmbCamTypeBcToRead.SelectedIndex = 0;

            if(txtAlto.Text.Trim().Length>0)
            {
                try
                {
                    alto = int.Parse(txtAlto.Text);
                } 
                catch(FormatException exe)
                {
                    alto = 640;
                }
            }
            else
            {
                alto = 640;
            }
            if (txtAncho.Text.Trim().Length > 0)
            {
                try
                {
                    ancho = int.Parse(txtAncho.Text);
                }
                catch (FormatException exe)
                {
                    ancho = 480;
                }
            }
            else
            {
                ancho = 480;
            }


        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnGetBiometrikaCam_Click(object sender, EventArgs e)
        {
            Limpiar();
            string hashGuid = Guid.NewGuid().ToString("N");
            //Siempre debe ir acompañado del atributo SRC.
            string queryString = "?src=CAM";
            byte[] arr;

            //Aca debe ir agregando los distintos parámetros.
            queryString = queryString + "&CAMTypeOp=" + cmbCamTypeOp.Text.Split('-')[0].Trim();
            queryString = queryString + "&CAMTypeCapture=" + cmbCamTypeCapture.Text.Split('-')[0].Trim();
            queryString = queryString + "&CAMTypeBcToRead=" + cmbCamTypeBcToRead.Text.Split('-')[0].Trim();


            string absoluteURL = txtUrl.Text;  //ConfigurationManager.AppSettings["AbsoluteURL"];
            if (!absoluteURL.EndsWith("/"))
                absoluteURL = absoluteURL + "/";

            absoluteURL = absoluteURL + queryString;
            rbtRequest.Text = absoluteURL;
            //rbtResponse.Text = rbtResponse.Text + absoluteURL + Environment.NewLine;
            try
            { 
                WebRequest request = WebRequest.Create(absoluteURL);
                request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
                request.Timeout = 13000000;
                using (WebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string json = reader.ReadToEnd();
                        rbtResponse.Text = json;
                        CamApiResponse = JsonConvert.DeserializeObject<CamApiResponse>(json);
                        
                    }
                    
                }
                if(CamApiResponse!=null)
                {
                    if(CamApiResponse.Imagen.Length!=0)
                    {
                        // Visualizamos la imagen en una picture.
                        arr = Convert.FromBase64String(CamApiResponse.Imagen);
                        using (var ms = new MemoryStream(arr, 0, arr.Length))
                        {
                            picImagen.Image = Image.FromStream(ms, true);
                        }
                        picImagen.Refresh();
                    }
                    if(CamApiResponse.TypeBarCode.Length!=0)
                    {
                        lblTypeCodeBar.Text = CamApiResponse.TypeBarCode;
                    }
                    try
                    { 
                        rbtContenido.Text = CamApiResponse.Content;

                    }
                    catch(Exception exe)
                    {
                        //rbtContenido.Text = //Encoding.Default.GetString(Convert.FromBase64String(CamApiResponse.Content.ToString()));
                    }
                    lblConfidence.Text = CamApiResponse.Confidence.ToString();
                    lblError.Text = CamApiResponse.Error.ToString();
                    if(CamApiResponse.Mensaje.Length>0)
                    {
                        lblMensaje.Text = CamApiResponse.Mensaje;
                        lblMensaje.ForeColor = Color.Red;
                    }
                    else
                    {
                        lblMensaje.Text = CamApiResponse.Mensaje;
                        lblMensaje.ForeColor = Color.Black;
                    }
                }

            }
            catch(Exception WebException)
            {
                MessageBox.Show( "No se encuentra el servicio Biometrika Client, Verifique que el servicio Biometrika Client esté en ejecución", "Biometrika",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }

        }

        private void Limpiar()
        {
            picImagen.Image = null;            
            rbtContenido.Text = "";
            rbtRequest.Text = "";
            rbtResponse.Text = "";
            lblTypeCodeBar.Text = "";
            
        }

        private void cmdCerrar_Click(object sender, EventArgs e)
        {
            Environment.Exit(-1);
        }
    }
}
