﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiometrikaCAM_Test
{
    public class CamApiResponse
    {
        public String Imagen { get; set; }

        public String Content { get; set; }

        public String TypeBarCode { get; set; }

        public float Confidence { get; set; }

        public int Error { get; set; }

        public String Mensaje { get; set; }
    }
}
