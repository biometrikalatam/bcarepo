﻿namespace BiometrikaCAM_Test
{
    partial class frmTest
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTest));
            this.label1 = new System.Windows.Forms.Label();
            this.grpConfiguracion = new System.Windows.Forms.GroupBox();
            this.txtAlto = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAncho = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnGetBiometrikaCam = new System.Windows.Forms.Button();
            this.cmbCamTypeCapture = new System.Windows.Forms.ComboBox();
            this.cmbCamTypeOp = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.lblUrl = new System.Windows.Forms.Label();
            this.cmbCamera = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblConfidence = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.rbtContenido = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTypeCodeBar = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.picImagen = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbtRequest = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.rbtResponse = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmdCerrar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbCamTypeBcToRead = new System.Windows.Forms.ComboBox();
            this.grpConfiguracion.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImagen)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Listado de Cámaras";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // grpConfiguracion
            // 
            this.grpConfiguracion.Controls.Add(this.cmbCamTypeBcToRead);
            this.grpConfiguracion.Controls.Add(this.label14);
            this.grpConfiguracion.Controls.Add(this.txtAlto);
            this.grpConfiguracion.Controls.Add(this.label9);
            this.grpConfiguracion.Controls.Add(this.txtAncho);
            this.grpConfiguracion.Controls.Add(this.label8);
            this.grpConfiguracion.Controls.Add(this.btnGetBiometrikaCam);
            this.grpConfiguracion.Controls.Add(this.cmbCamTypeCapture);
            this.grpConfiguracion.Controls.Add(this.cmbCamTypeOp);
            this.grpConfiguracion.Controls.Add(this.label2);
            this.grpConfiguracion.Controls.Add(this.label3);
            this.grpConfiguracion.Controls.Add(this.txtUrl);
            this.grpConfiguracion.Controls.Add(this.lblUrl);
            this.grpConfiguracion.Controls.Add(this.cmbCamera);
            this.grpConfiguracion.Controls.Add(this.label1);
            this.grpConfiguracion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpConfiguracion.Location = new System.Drawing.Point(30, 12);
            this.grpConfiguracion.Name = "grpConfiguracion";
            this.grpConfiguracion.Size = new System.Drawing.Size(350, 472);
            this.grpConfiguracion.TabIndex = 1;
            this.grpConfiguracion.TabStop = false;
            this.grpConfiguracion.Text = "Configuración Biometrika CAM";
            // 
            // txtAlto
            // 
            this.txtAlto.Location = new System.Drawing.Point(30, 414);
            this.txtAlto.Name = "txtAlto";
            this.txtAlto.Size = new System.Drawing.Size(100, 24);
            this.txtAlto.TabIndex = 12;
            this.txtAlto.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(27, 394);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 16);
            this.label9.TabIndex = 11;
            this.label9.Text = "Alto Imagen";
            this.label9.Visible = false;
            // 
            // txtAncho
            // 
            this.txtAncho.Location = new System.Drawing.Point(30, 356);
            this.txtAncho.Name = "txtAncho";
            this.txtAncho.Size = new System.Drawing.Size(100, 24);
            this.txtAncho.TabIndex = 10;
            this.txtAncho.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(27, 336);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 16);
            this.label8.TabIndex = 9;
            this.label8.Text = "Ancho Imagen";
            this.label8.Visible = false;
            // 
            // btnGetBiometrikaCam
            // 
            this.btnGetBiometrikaCam.Location = new System.Drawing.Point(176, 418);
            this.btnGetBiometrikaCam.Name = "btnGetBiometrikaCam";
            this.btnGetBiometrikaCam.Size = new System.Drawing.Size(154, 33);
            this.btnGetBiometrikaCam.TabIndex = 8;
            this.btnGetBiometrikaCam.Text = "Get Biometrika CAM";
            this.btnGetBiometrikaCam.UseVisualStyleBackColor = true;
            this.btnGetBiometrikaCam.Click += new System.EventHandler(this.btnGetBiometrikaCam_Click);
            // 
            // cmbCamTypeCapture
            // 
            this.cmbCamTypeCapture.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCamTypeCapture.FormattingEnabled = true;
            this.cmbCamTypeCapture.Items.AddRange(new object[] {
            "1 - Solo captura Imagen",
            "2 - Imagen con reconocimiento"});
            this.cmbCamTypeCapture.Location = new System.Drawing.Point(18, 155);
            this.cmbCamTypeCapture.Name = "cmbCamTypeCapture";
            this.cmbCamTypeCapture.Size = new System.Drawing.Size(270, 26);
            this.cmbCamTypeCapture.TabIndex = 7;
            // 
            // cmbCamTypeOp
            // 
            this.cmbCamTypeOp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCamTypeOp.FormattingEnabled = true;
            this.cmbCamTypeOp.Items.AddRange(new object[] {
            "1 - Automático",
            "2 - Manual"});
            this.cmbCamTypeOp.Location = new System.Drawing.Point(20, 214);
            this.cmbCamTypeOp.Name = "cmbCamTypeOp";
            this.cmbCamTypeOp.Size = new System.Drawing.Size(270, 26);
            this.cmbCamTypeOp.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 194);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(189, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Tipo Operación(CAMTypeOp)\r\n";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(202, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tipo Captura(CAMTypeCapture)\r\n";
            // 
            // txtUrl
            // 
            this.txtUrl.Location = new System.Drawing.Point(19, 51);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(271, 24);
            this.txtUrl.TabIndex = 3;
            this.txtUrl.Text = "http://localhost:9191";
            // 
            // lblUrl
            // 
            this.lblUrl.AutoSize = true;
            this.lblUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUrl.Location = new System.Drawing.Point(17, 30);
            this.lblUrl.Name = "lblUrl";
            this.lblUrl.Size = new System.Drawing.Size(77, 16);
            this.lblUrl.TabIndex = 2;
            this.lblUrl.Text = "Url Servicio";
            // 
            // cmbCamera
            // 
            this.cmbCamera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCamera.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCamera.FormattingEnabled = true;
            this.cmbCamera.Location = new System.Drawing.Point(16, 108);
            this.cmbCamera.Name = "cmbCamera";
            this.cmbCamera.Size = new System.Drawing.Size(275, 24);
            this.cmbCamera.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblMensaje);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.lblError);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.lblConfidence);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.rbtContenido);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lblTypeCodeBar);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.picImagen);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(405, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(423, 472);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resultados";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // lblMensaje
            // 
            this.lblMensaje.AutoSize = true;
            this.lblMensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje.Location = new System.Drawing.Point(182, 453);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(0, 16);
            this.lblMensaje.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(30, 453);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 16);
            this.label12.TabIndex = 17;
            this.label12.Text = "Mensaje";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.Location = new System.Drawing.Point(208, 427);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 16);
            this.lblError.TabIndex = 16;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(30, 427);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 16);
            this.label13.TabIndex = 15;
            this.label13.Text = "Error";
            // 
            // lblConfidence
            // 
            this.lblConfidence.AutoSize = true;
            this.lblConfidence.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConfidence.Location = new System.Drawing.Point(208, 403);
            this.lblConfidence.Name = "lblConfidence";
            this.lblConfidence.Size = new System.Drawing.Size(0, 16);
            this.lblConfidence.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(30, 403);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 16);
            this.label11.TabIndex = 13;
            this.label11.Text = "Confidence";
            // 
            // rbtContenido
            // 
            this.rbtContenido.Location = new System.Drawing.Point(24, 336);
            this.rbtContenido.Name = "rbtContenido";
            this.rbtContenido.Size = new System.Drawing.Size(364, 51);
            this.rbtContenido.TabIndex = 8;
            this.rbtContenido.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(30, 312);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "Contenido";
            // 
            // lblTypeCodeBar
            // 
            this.lblTypeCodeBar.AutoSize = true;
            this.lblTypeCodeBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTypeCodeBar.Location = new System.Drawing.Point(208, 282);
            this.lblTypeCodeBar.Name = "lblTypeCodeBar";
            this.lblTypeCodeBar.Size = new System.Drawing.Size(0, 16);
            this.lblTypeCodeBar.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(30, 282);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Tipo Código de Barra";
            // 
            // picImagen
            // 
            this.picImagen.Location = new System.Drawing.Point(24, 49);
            this.picImagen.Name = "picImagen";
            this.picImagen.Size = new System.Drawing.Size(373, 221);
            this.picImagen.TabIndex = 4;
            this.picImagen.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(21, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Imagen";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbtRequest);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.rbtResponse);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(857, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(423, 472);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Comunicación";
            // 
            // rbtRequest
            // 
            this.rbtRequest.AcceptsTab = true;
            this.rbtRequest.Location = new System.Drawing.Point(24, 49);
            this.rbtRequest.Name = "rbtRequest";
            this.rbtRequest.Size = new System.Drawing.Size(364, 53);
            this.rbtRequest.TabIndex = 10;
            this.rbtRequest.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(21, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 16);
            this.label7.TabIndex = 9;
            this.label7.Text = "Request";
            // 
            // rbtResponse
            // 
            this.rbtResponse.Location = new System.Drawing.Point(24, 135);
            this.rbtResponse.Name = "rbtResponse";
            this.rbtResponse.Size = new System.Drawing.Size(364, 320);
            this.rbtResponse.TabIndex = 8;
            this.rbtResponse.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(21, 108);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 16);
            this.label10.TabIndex = 3;
            this.label10.Text = "Response";
            // 
            // cmdCerrar
            // 
            this.cmdCerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCerrar.Location = new System.Drawing.Point(1128, 493);
            this.cmdCerrar.Name = "cmdCerrar";
            this.cmdCerrar.Size = new System.Drawing.Size(154, 33);
            this.cmdCerrar.TabIndex = 10;
            this.cmdCerrar.Text = "Cerrar";
            this.cmdCerrar.UseVisualStyleBackColor = true;
            this.cmdCerrar.Click += new System.EventHandler(this.cmdCerrar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::BiometrikaCAM_Test.Properties.Resources.biometrika;
            this.pictureBox1.Location = new System.Drawing.Point(30, 493);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(283, 33);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(17, 254);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(274, 16);
            this.label14.TabIndex = 13;
            this.label14.Text = "Tipo Código de Barra(CAMTypeBcToRead)\r\n";
            // 
            // cmbCamTypeBcToRead
            // 
            this.cmbCamTypeBcToRead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCamTypeBcToRead.FormattingEnabled = true;
            this.cmbCamTypeBcToRead.Items.AddRange(new object[] {
            "1 - PDF417",
            "2 - QR",
            "3 - PDF417 - QR"});
            this.cmbCamTypeBcToRead.Location = new System.Drawing.Point(20, 277);
            this.cmbCamTypeBcToRead.Name = "cmbCamTypeBcToRead";
            this.cmbCamTypeBcToRead.Size = new System.Drawing.Size(270, 26);
            this.cmbCamTypeBcToRead.TabIndex = 14;
            // 
            // frmTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1294, 538);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cmdCerrar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpConfiguracion);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmTest";
            this.Text = "Biometrika CAM Test";
            this.Load += new System.EventHandler(this.frmTest_Load);
            this.grpConfiguracion.ResumeLayout(false);
            this.grpConfiguracion.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImagen)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpConfiguracion;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.Label lblUrl;
        private System.Windows.Forms.ComboBox cmbCamera;
        private System.Windows.Forms.ComboBox cmbCamTypeOp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbCamTypeCapture;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnGetBiometrikaCam;
        private System.Windows.Forms.RichTextBox rbtContenido;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTypeCodeBar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox picImagen;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox rbtRequest;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox rbtResponse;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtAlto;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAncho;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button cmdCerrar;
        private System.Windows.Forms.Label lblMensaje;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblConfidence;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbCamTypeBcToRead;
    }
}

