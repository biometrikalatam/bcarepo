﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSCT.Core.ConsoleTests
{
    class TLStruct: ICloneable
    {
        private int tag, length, valueBytesRead;

        public TLStruct(int tag) { this.tag = tag; this.length = Int32.MaxValue; this.valueBytesRead = 0; }

        public void setLength(int length) { this.length = length; }

        public int getTag() { return tag; }

        public int getLength() { return length; }

        public int getValueBytesProcessed() { return valueBytesRead; }

        public void updateValueBytesProcessed(int n) { this.valueBytesRead += n; }

        
        #region ICloneable Members

        public object Clone()
        {
            TLStruct copy = new TLStruct(tag); copy.length = this.length; copy.valueBytesRead = this.valueBytesRead; return copy;
        }

        #endregion
    }
}
