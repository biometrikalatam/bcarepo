﻿namespace WSCT.Core.ConsoleTests
{
    using System;
    using System.IO;

    internal class WrappedSystemStream : Stream
    {
        private InputStream ist;
        private OutputStream ost;
        int position;
        int markedPosition;

        public WrappedSystemStream(InputStream ist)
        {
            this.ist = ist;
        }

        public WrappedSystemStream(OutputStream ost)
        {
            this.ost = ost;
        }

        public InputStream InputStream
        {
            get { return ist; }
        }

        public OutputStream OutputStream
        {
            get { return ost; }
        }

        public override void Close()
        {
            if (this.ist != null)
            {
                this.ist.Close();
            }
            if (this.ost != null)
            {
                this.ost.Close();
            }
        }

        public override void Flush()
        {
            this.ost.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int res = this.ist.Read(buffer, offset, count);
            if (res != -1)
            {
                position += res;
                return res;
            }
            else
                return 0;
        }

        public override int ReadByte()
        {
            int res = this.ist.Read();
            if (res != -1)
                position++;
            return res;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            if (origin == SeekOrigin.Begin)
                Position = offset;
            else if (origin == SeekOrigin.Current)
                Position = Position + offset;
            else if (origin == SeekOrigin.End)
                Position = Length + offset;
            return Position;
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            this.ost.Write(buffer, offset, count);
            position += count;
        }

        public override void WriteByte(byte value)
        {
            this.ost.Write(value);
            position++;
        }

        public override bool CanRead
        {
            get { return (this.ist != null); }
        }

        public override bool CanSeek
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return (this.ost != null); }
        }

        public override long Length
        {
            get
            {
                return ist.Wrapped.Length;
            }
        }

        internal void OnMark(int nb)
        {
            markedPosition = position;
            ist.Mark(nb);
        }

        public override long Position
        {
            get
            {
                if (ist != null && ist.CanSeek())
                    return ist.Position;
                else
                    return position;
            }
            set
            {
                if (value == position)
                    return;
                else if (value == markedPosition)
                    ist.Reset();
                else if (ist != null && ist.CanSeek())
                {
                    ist.Position = value;
                }
                else
                    throw new NotSupportedException();
            }
        }
    }

    /*internal class WrappedSystemStream : Stream
    {
        private InputStream ist;

        private OutputStream ost;

        int position;
        int markedPosition;

        public InputStream InputStream
        {
            get { return ist; }
        }
        public WrappedSystemStream(InputStream ist)
        {
            this.ist = ist;
        }

        public WrappedSystemStream(OutputStream ost)
        {
            this.ost = ost;
        }

        public override void Close()
        {
            if (this.ist != null)
            {
                this.ist.Close();
            }
            if (this.ost != null)
            {
                this.ost.Close();
            }
        }

        public override void Flush()
        {
            this.ost.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int res = this.ist.Read(buffer, offset, count);
            return res != -1 ? res : 0;
        }

        public override int ReadByte()
        {
            return this.ist.Read();
        }

     

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            this.ost.Write(buffer, offset, count);
        }

        public override void WriteByte(byte value)
        {
            this.ost.Write(value);
        }

        public override bool CanRead
        {
            get { return (this.ist != null); }
        }
        public override long Seek(long offset, SeekOrigin origin)
        {
            if (origin == SeekOrigin.Begin)
                return Position = offset;
            else
                throw new NotSupportedException();
        }
        public override bool CanSeek
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return (this.ost != null); }
        }

        public override long Length
        {
            get
            {
                throw new NotSupportedException();
            }
        }

        public override long Position
        {
            get
            {
                if (ist != null && ist.CanSeek())
                    return ist.Position;
                else
                    return position;
            }
            set
            {
                if (value == position)
                    return;
                else if (value == markedPosition)
                    ist.Reset();
                else if (ist != null && ist.CanSeek())
                {
                    ist.Position = value;
                }
                else
                    throw new NotSupportedException();
            }
        }
    }*/
    internal class InputStream : IDisposable
    {
        private long mark;
        internal Stream Wrapped;
        protected Stream BaseStream;

        public static implicit operator InputStream (Stream s)
        {
            return Wrap (s);
        }

        public static implicit operator Stream (InputStream s)
        {
            return s == null 
                ? null 
                : s.GetWrappedStream();
        }
        
        public virtual int Available ()
        {
            if (Wrapped is WrappedSystemStream)
                return ((WrappedSystemStream)Wrapped).InputStream.Available ();
            else
                return 0;
        }

        public virtual void Close ()
        {
            if (Wrapped != null) {
                Wrapped.Close ();
            }
        }

        public void Dispose ()
        {
            Close ();
        }

        internal Stream GetWrappedStream ()
        {
            // Always create a wrapper stream (not directly Wrapped) since the subclass
            // may be overriding methods that need to be called when used through the Stream class
            return new WrappedSystemStream (this);
        }

        public virtual void Mark (int readlimit)
        {
            if (Wrapped is WrappedSystemStream)
                ((WrappedSystemStream)Wrapped).InputStream.Mark (readlimit);
            else {
                if (BaseStream is WrappedSystemStream)
                    ((WrappedSystemStream)BaseStream).OnMark (readlimit);
                if (Wrapped != null)
                    this.mark = Wrapped.Position;
            }
        }
        
        public virtual bool MarkSupported ()
        {
            if (Wrapped is WrappedSystemStream)
                return ((WrappedSystemStream)Wrapped).InputStream.MarkSupported ();
            else
                return ((Wrapped != null) && Wrapped.CanSeek);
        }

        public virtual int Read ()
        {
            if (Wrapped == null) {
                throw new NotImplementedException ();
            }
           
            return Wrapped.ReadByte ();
        }

        public virtual int Read (byte[] buf)
        {
            return Read (buf, 0, buf.Length);
        }

        public virtual int Read (byte[] b, int off, int len)
        {
            if (Wrapped is WrappedSystemStream)
                return ((WrappedSystemStream)Wrapped).InputStream.Read (b, off, len);
            
            if (Wrapped != null) {
                int num = Wrapped.Read (b, off, len);
                return ((num <= 0) ? -1 : num);
            }
            int totalRead = 0;
            while (totalRead < len) {
                int nr = Read ();
                if (nr == -1)
                    return -1;
                b[off + totalRead] = (byte)nr;
                totalRead++;
            }
            return totalRead;
        }

        public virtual void Reset ()
        {
            if (Wrapped is WrappedSystemStream)
                ((WrappedSystemStream)Wrapped).InputStream.Reset ();
            else {
                if (Wrapped == null)
                    throw new IOException ();
                Wrapped.Position = mark;
            }
        }

        public virtual long Skip (long cnt)
        {
            if (Wrapped is WrappedSystemStream)
                return ((WrappedSystemStream)Wrapped).InputStream.Skip (cnt);
            
            long n = cnt;
            while (n > 0) {
                if (Read () == -1)
                    return cnt - n;
                n--;
            }
            return cnt - n;
        }
        
        internal bool CanSeek ()
        {
            if (Wrapped != null)
                return Wrapped.CanSeek;
            else
                return false;
        }
        
        internal long Position {
            get {
                if (Wrapped != null)
                    return Wrapped.Position;
                else
                    throw new NotSupportedException ();
            }
            set {
                if (Wrapped != null)
                    Wrapped.Position = value;
                else
                    throw new NotSupportedException ();
            }
        }

        static internal InputStream Wrap (Stream s)
        {
            InputStream stream = new InputStream ();
            stream.Wrapped = s;
            return stream;
        }
    }
    /*public class InputStream : IDisposable
    {
        private long mark;

        protected Stream Wrapped;

        public Stream Stream
        {
            get
            {
                return Wrapped;
            }
        }

        public static implicit operator InputStream(Stream s)
        {
            return Wrap(s);
        }

        public static implicit operator Stream(InputStream s)
        {
            return s.GetWrappedStream();
        }

        public virtual int Available()
        {
            if (Wrapped is WrappedSystemStream)
                return ((WrappedSystemStream)Wrapped).InputStream.Available();
            else
                return 0;
        }

        

        public virtual void Close()
        {
            if (Wrapped != null)
            {
                Wrapped.Close();
            }
        }

        public void Dispose()
        {
            Close();
        }

        internal Stream GetWrappedStream()
        {
            if (Wrapped != null)
            {
                return Wrapped;
            }
            return new WrappedSystemStream(this);
        }

        public virtual void Mark(int readlimit)
        {
            if (Wrapped != null)
            {
                this.mark = Wrapped.Position;
            }
        }

        public virtual bool MarkSupported()
        {
            return ((Wrapped != null) && Wrapped.CanSeek);
        }

        public virtual int Read()
        {
            if (Wrapped == null)
            {
                throw new NotImplementedException();
            }

            return Wrapped.ReadByte();
        }

        public virtual int Read(byte[] buf)
        {
            return Read(buf, 0, buf.Length);
        }

        public virtual int Read(byte[] b, int off, int len)
        {
            if (Wrapped != null)
            {
                int num = Wrapped.Read(b, off, len);
                return ((num <= 0) ? -1 : num);
            }
            int totalRead = 0;
            while (totalRead < len)
            {
                int nr = Read();
                if (nr == -1)
                    return -1;
                b[off + totalRead] = (byte)nr;
                totalRead++;
            }
            return totalRead;
        }

        public virtual void Reset()
        {
            if (Wrapped == null)
            {
                throw new IOException();
            }
            Wrapped.Position = mark;
        }

        public virtual long Skip(long cnt)
        {
            long n = cnt;
            while (n > 0)
            {
                if (Read() == -1)
                    return cnt - n;
                n--;
            }
            return cnt - n;
        }
        internal bool CanSeek()
        {
            if (Wrapped != null)
                return Wrapped.CanSeek;
            else
                return false;
        }

        internal long Position
        {
            get
            {
                if (Wrapped != null)
                    return Wrapped.Position;
                else
                    throw new NotSupportedException();
            }
            set
            {
                if (Wrapped != null)
                    Wrapped.Position = value;
                else
                    throw new NotSupportedException();
            }
        }

        static internal InputStream Wrap(Stream s)
        {
            InputStream stream = new InputStream();
            stream.Wrapped = s;
            return stream;
        }*/
    
}
