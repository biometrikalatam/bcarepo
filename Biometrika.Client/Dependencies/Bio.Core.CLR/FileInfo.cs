﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSCT.Core.ConsoleTests
{
    public abstract class FileInfo
    {
        public abstract short getFID();

        public abstract int getFileLength();
    }
}
