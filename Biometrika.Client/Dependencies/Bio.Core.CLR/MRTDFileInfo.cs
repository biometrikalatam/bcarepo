﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WSCT.Core.ConsoleTests
{
    public class MRTDFileInfo: FileInfo
    {
        private short fid;
        private int length;

        public MRTDFileInfo(short fid, int length) { this.fid = fid; this.length = length; }

        

        public int getSelectedFile() { return fid; }

        public override short getFID()
        {
            return fid;
        }

        public override int getFileLength()
        {
            return length;
        }
    }
}
