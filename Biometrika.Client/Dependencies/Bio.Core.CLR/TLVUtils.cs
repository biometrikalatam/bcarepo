﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WSCT.Core.ConsoleTests
{
    public class TLVUtils
    {
        public static int UNIVERSAL_CLASS = 0;		/* 00 x xxxxx */
	    /** Application tag class. */
	    public static  int APPLICATION_CLASS = 1;		/* 01 x xxxxx */
	    /** Context specific tag class. */
	    public static int CONTEXT_SPECIFIC_CLASS = 2;	/* 10 x xxxxx */
	    /** Private tag class. */
	    public static  int PRIVATE_CLASS = 3;	
        public static bool isPrimitive(int tag)
        {
            int i = 3;
            for (; i >= 0; i--)
            {
                int mask = (0xFF << (8 * i));
                if ((tag & mask) != 0x00) { break; }
            }
            int msByte = (((tag & (0xFF << (8 * i))) >> (8 * i)) & 0xFF);
            bool result = ((msByte & 0x20) == 0x00);
            return result;
        }
        public static int getTagLength(int tag)
        {
            return getTagAsBytes(tag).Length;
        }
        public static int getLengthLength(int length)
        {
            return getLengthAsBytes(length).Length;
        }
        public static byte[] getTagAsBytes(int tag) 
        {
		
                MemoryStream ms = new MemoryStream();
                                   
                    using (BinaryWriter bw = new BinaryWriter(ms))
                    {
                        int byteCount = (int)(Math.Log(tag) / Math.Log(256)) + 1;
                        for (int i = 0; i < byteCount; i++) {
			                    int pos = 8 * (byteCount - i - 1);
			                    bw. Write((tag & (0xFF << pos)) >> pos);
		                }
                        
                    }
                    
                byte[] tagBytes =ms.ToArray();   
                
		        switch (getTagClass(tag)) 
                {
		            case 1:// TLVUtils.APPLICATION_CLASS:
			             tagBytes[0] |= 0x40;
			             break;
		            case 2://CONTEXT_SPECIFIC_CLASS:
			             tagBytes[0] |= 0x80;
			             break;
		            case 3://PRIVATE_CLASS:
			            tagBytes[0] |= 0xC0;
			            break;
		        }
		        if (!isPrimitive(tag)) {
			        tagBytes[0] |= 0x20;
		        }
        
		        return tagBytes;		
		
	    }
        public static byte[] getLengthAsBytes(int length) {
            MemoryStream ms=new MemoryStream();
            using (BinaryWriter bw = new BinaryWriter(ms))
            {
                if (length < 128) {
			       /* short form */
			        bw.Write(length);
		        } 
                else 
                {
			        int byteCount = log(length, 256);
			        bw.Write(0x80 | byteCount);
			        for (int i = 0; i < byteCount; i++) {
				        int pos = 8 * (byteCount - i - 1);
				        bw.Write((length & (0xFF << pos)) >> pos);
			        }
		        }
            
            }
		    
		    byte[] test=new byte[1];
            Array.Copy(ms.ToArray(), test, 1);
            return test;
	}

	public static int getTagClass(int tag) {
		int i = 3;
		for (; i >= 0; i--) {
			int mask = (0xFF << (8 * i));
			if ((tag & mask) != 0x00) { break; }
		}
		int msByte = (((tag & (0xFF << (8 * i))) >> (8 * i)) & 0xFF);
		switch (msByte & 0xC0) {
		case 0x00: return UNIVERSAL_CLASS;
		case 0x40: return APPLICATION_CLASS;
		case 0x80: return CONTEXT_SPECIFIC_CLASS;
		case 0xC0:
		default: return PRIVATE_CLASS;
		}
	}

	private static int log(int n, int bases) {
		int result = 0;
		while (n > 0) {
			n = n / bases;
			result ++;
		}
		return result;
	}


    }
}
