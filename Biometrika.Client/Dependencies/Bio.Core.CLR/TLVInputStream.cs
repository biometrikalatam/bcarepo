﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;
using NLog;

namespace WSCT.Core.ConsoleTests
{
    class TLVInputStream : InputStream
    {
        private static int MAX_BUFFER_LENGTH = 65535;
        private InputStream originalInputStream;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private DataInputStream inputStream;

        private int bufferSize;

        private TLVInputState state;
	    private TLVInputState markedState;

        public TLVInputStream(InputStream inputStream)
        {
            this.bufferSize = 0;
            try
            {
                bool result = (inputStream is ByteArrayInputStream);
                if (result == true)
                    this.bufferSize = inputStream.Available();
                result = (inputStream is InputStream);
                if (result == true)
                    this.bufferSize = inputStream.Available();
            }
            catch (Exception exe)
            {
                logger.Error(exe, "Error en constructor de TLVInputstream");
                throw;
            }
            this.originalInputStream = inputStream;
            bool results = (inputStream is DataInputStream);
            if (results == false)
                this.inputStream = new DataInputStream(inputStream);
            state = new TLVInputState();
             markedState = null;
            
        }

        public int readTag() 
        {
            if ((state.isAtStartOfTag == false) && (state.isProcessingValue) == false) { }//En esta línea hay que retornar error
            int tag = -1;
		    int bytesRead = 0;

            int b = inputStream.ReadUnsignedByte(); bytesRead++;
            while (b == 0x00 || b == 0xFF) {
				b = inputStream.ReadUnsignedByte(); bytesRead++; /* skip 00 and FF */
			}
            switch (b & 0x1F) {
            case 0x1F:
				tag = b; /* We store the first byte including LHS nibble */
				b = inputStream.ReadUnsignedByte(); bytesRead++;
				while ((b & 0x80) == 0x80) {
					tag <<= 8;
					tag |= (b & 0x7F);
					b = inputStream.ReadUnsignedByte(); bytesRead++;
				}
				tag <<= 8;
				tag |= (b & 0x7F);
				
				break;
			default:
				tag = b;
				break;
			
            }
            state.setTagProcessed(tag, bytesRead);
            return tag;
        }
        public int readLength()
        {
            if (state.isAtStartOfLength == false) { return -1; } //retornar error}
            int bytesRead = 0;
			int length = 0;
			int b = inputStream.ReadUnsignedByte(); bytesRead++;
			if ((b & 0x80) == 0x00) {
                length = b;
			} else {
                int count = b & 0x7F;
                length = 0;
                for (int i = 0; i < count; i++) {
					b = inputStream.ReadUnsignedByte(); bytesRead++;
					length <<= 8;
					length |= b;
				}
            }
            state.setLengthProcessed(length, bytesRead);
            return length;
        }

        public byte[] readValue()
        {
            if (!state.isProcessingValue) {  }
            int length = state.getLength();
            byte[] value = new byte[length];
            inputStream.ReadFully(value);
            state.updateValueBytesProcessed(length);
            return value;
        }

        private long skipValue()
        {
            if (state.isAtStartOfTag) { return 0; }
            if (state.isAtStartOfLength) { return 0; }
            int bytesLeft = state.getValueBytesLeft();
		    return skip(bytesLeft);
        }

        public void skipToTag(int searchTag)
        {
            while(true) {
        
                    int tag = -1;
			    if (state.isAtStartOfTag) {
				    /* Nothing. */
			    } else if (state.isAtStartOfLength) {
				    readLength();
				    if (TLVUtils.isPrimitive(state.getTag())) { skipValue(); }
			    } else {
				    if (TLVUtils.isPrimitive(state.getTag())) { skipValue(); }

			    }
                tag = readTag();
			    if  (tag == searchTag) { return; }

			    if (TLVUtils.isPrimitive(tag)) {
				    int length = readLength();
				    int skippedBytes = (int)skipValue();
				    if (skippedBytes >= length) {
					    /* Now at next tag. */
					    continue;
				    } else {
					    /* Could only skip less than length bytes,
					     * we're lost, probably at EOF. */
					    break;
				    }
			    }
            }
        }

        public int available()  {
            return inputStream.Available();
	    }
       
	    public int read()  {
		    int result = inputStream.Read();
		    if (result < 0) { return -1; }
		    state.updateValueBytesProcessed(1);
		    return result;
	    }

        public long skip(long n)  {
		    if (n <= 0) { return 0; }
		    long result=0;// = inputStream.skip(n);
		    state.updateValueBytesProcessed((int)result);
		    return result;
	    }
        [MethodImpl(MethodImplOptions.Synchronized)]
        public  void mark(int readLimit) {
            inputStream.Mark(readLimit);
		    markedState = (TLVInputState)state.Clone();
        }

        public bool markSupported() {
		    return false;
	    }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public  void reset()
        {
            if (!markSupported()) {}
            inputStream.Reset();
		    state = markedState;
		    markedState = null;
        }

       public void close()  {
		    inputStream.Close();
	   }

	    public String toString() {
            return state.ToString();
	    }


    }
}
