﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Bio.Core.CLR
using Innovatrics.AnsiIso;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Macs;
using Org.BouncyCastle.Crypto.Parameters;
using System.IO;
using System.Security.Cryptography;
using WSCT.Core;
using WSCT.Core.APDU;
using WSCT.Helpers;
using WSCT.ISO7816;
using WSCT.Stack;
using WSCT.Wrapper;
using WSCT.Core.ConsoleTests;
using NLog;
using System.Drawing;

namespace Bio.Core.CLR
{
    public class CLR
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public HMACSHA1 ksEnc; //Llave para wrapper security key
        public long ssc;
        public RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();
        public WSCT.Core.ICardChannel cardChannel;
        public WSCT.Core.ICardContext context = new WSCT.Core.CardContext();
        public WSCT.Core.StatusChangeMonitor monitor;
        public AbstractReaderState readerState;
        public CommandAPDU cAPDU = new CommandAPDU();
        public ICardResponse rAPDU = new ResponseAPDU();
        public WSCT.Core.ConsoleTests.SecureMessagingWrapper objSecureMessagingWrapper;
        private int _timeout;
        //Variable que almacena el MRZ obtenido de la lectura de la cédula.
        private String Mrz = "";
        private String documentnumber;
        private String country;
        private String dateofbirth;
        private String sex;
        private String dateofexpiry;
        private String nacionality;
        private String patherlastmame;
        private String motherlastmame;
        private String name;
        private String lastrow;
        private String dni;
        private String readername;
        private int error;
        public byte[] jpg2k_magic_number = { 0x00, 0x00, 0x00, 0x0C, 0x6A, 0x50,
            0x20, 0x20, 0x0D, 0x0A, 0x87, 0x0A, 0x00, 0x00, 0x00, 0x14, 0x66, 0x74,
            0x79, 0x70, 0x6A, 0x70, 0x32, 0x20, 0x00, 0x00, 0x00, 0x00, 0x6A, 0x70,
            0x32, 0x20, 0x00, 0x00, 0x00, 0x2D, 0x6A, 0x70, 0x32, 0x68, 0x00, 0x00,
            0x00, 0x16, 0x69, 0x68, 0x64, 0x72 };
        byte[] prefix = null;
        int totalLength = 0;
        int tlLength = 0;
        int len = 0;
        int fileLength = 0;
        BufferedInputStream2 bufferedIn;
        ByteArrayInputStream baInputStream;
        TLVInputStream tlvInputStream;
        int tag = 0;
        int vLength = 0;
        int largo = 0;


        /// <summary>
        /// Rutina que ejecuta los comandos necesarios para realizar match on card en la cédula.
        /// </summary>
        /// <param name="iso">minucia en formato iso compact</param>
        /// <param name="docnum">número de documento de la cédula</param>
        /// <param name="dob">fecha de nacimiento</param>
        /// <param name="doe">fecha de expiración</param>
        /// <param name="fingerverified">dedo a verificar</param>
        /// <returns></returns>
        public bool MatchOnCard(string iso, string docnum, string dob, string doe, out int fingerverified)
        {
            fingerverified = 0;
            bool HIToNoHIT = false;

            WSCT.Core.ICardContext context = new WSCT.Core.CardContext();

            context.establish();
            context.listReaderGroups();
            context.listReaders(context.groups[0]);

            WSCT.Core.StatusChangeMonitor monitor = new WSCT.Core.StatusChangeMonitor(context);

            AbstractReaderState readerState = monitor.waitForCardPresence(0);
            do
            {
                readerState = monitor.waitForCardPresence(15000);
            }
            while (readerState == null);

            //String readersname = "OMNIKEY CardMan 5x21-CL 0";
            cardChannel = new WSCT.Core.CardChannel(context, readername);

            cardChannel.connect(WSCT.Wrapper.ShareMode.SCARD_SHARE_SHARED, WSCT.Wrapper.Protocol.SCARD_PROTOCOL_ANY);
            logger.Info("CLR_Security: Canal seleccionado:." + cardChannel.readerName);

            Byte[] recvBuffer = null;
            cardChannel.getAttrib(Attrib.SCARD_ATTR_ATR_STRING, ref recvBuffer);

            recvBuffer = null;
            cardChannel.getAttrib(Attrib.SCARD_ATTR_DEVICE_FRIENDLY_NAME, ref recvBuffer);

            recvBuffer = null;
            cardChannel.getAttrib(Attrib.SCARD_ATTR_ATR_STRING, ref recvBuffer);

            cardChannel.reconnect(WSCT.Wrapper.ShareMode.SCARD_SHARE_SHARED, WSCT.Wrapper.Protocol.SCARD_PROTOCOL_T1, WSCT.Wrapper.Disposition.SCARD_RESET_CARD);

            cAPDU = new CommandAPDU("00A4040005A000000069");
            rAPDU = new ResponseAPDU();
            cardChannel.transmit(cAPDU, rAPDU);

            cAPDU = new CommandAPDU("00A404000E315041592E5359532E4444463031");
            rAPDU = new ResponseAPDU();
            cardChannel.transmit(cAPDU, rAPDU);

            cAPDU = new CommandAPDU(0x00, 0xA4, 0x00, 0x00, 0x02, new Byte[2] { 0x3F, 0x00 });
            rAPDU = new ResponseAPDU();
            cardChannel.transmit(cAPDU, rAPDU);

            byte[] data = new byte[] { 0xa0, 0x00, 0x00, 0x02, 0x47, 0x10, 0x01 };

            cAPDU = new CommandAPDU();
            cAPDU.cla = (byte)0x00;
            cAPDU.ins = (byte)0xA4;
            cAPDU.p1 = (byte)0x04;
            cAPDU.p2 = (byte)0x0C;
            cAPDU.udc = new Byte[] { (byte)0xa0, (byte)0x00, (byte)0x00, (byte)0x02, (byte)0x47, (byte)0x10, (byte)0x01 };
            rAPDU = new ResponseAPDU();
            cardChannel.transmit(cAPDU, rAPDU);

            if (((ResponseAPDU)rAPDU).sw1 == 0x90)
            {
                //MRZ Final
                String documentNumber = docnum;
                String dateOfBirth = dob;
                String dateOfExpiry = doe;

                cAPDU = new CommandAPDU();
                cAPDU.cla = (byte)0x00;
                cAPDU.p1 = (byte)0x00;
                cAPDU.p2 = (byte)0x00;
                cAPDU.ins = (byte)0x84;
                cAPDU.le = 8;
                rAPDU = new ResponseAPDU();
                cardChannel.transmit(cAPDU, rAPDU);

                if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                {
                    byte[] cd1 = getBytes(checkDigit(documentNumber).ToString());
                    byte[] cd2 = getBytes(checkDigit(dateOfBirth).ToString());
                    byte[] cd3 = getBytes(checkDigit(dateOfExpiry).ToString());

                    SHA1 sha = SHA1Managed.Create();
                    sha.TransformBlock(getBytes(documentNumber), 0, getBytes(documentNumber).Length, getBytes(documentNumber), 0);
                    sha.TransformBlock(cd1, 0, cd1.Length, cd1, 0);
                    sha.TransformBlock(getBytes(dateOfBirth), 0, getBytes(dateOfBirth).Length, null, 0);
                    sha.TransformBlock(cd2, 0, cd2.Length, cd2, 0);
                    sha.TransformBlock(getBytes(dateOfExpiry), 0, getBytes(dateOfExpiry).Length, null, 0);
                    sha.TransformFinalBlock(cd3, 0, cd3.Length);
                    byte[] hash = sha.Hash;
                    byte[] keySeed = new byte[16];
                    Array.Copy(hash, keySeed, 16);
                    HMACSHA1 kEnc = deriveKey(keySeed, 1);
                    HMACSHA1 kMac = deriveKey(keySeed, 2);

                    //Creamos el random.
                    byte[] rndIFD = new byte[8];
                    rngCsp.GetBytes(rndIFD);
                    byte[] kIFD = new byte[16];
                    rngCsp.GetBytes(kIFD);
                    byte[] rndICC = ((ResponseAPDU)rAPDU).udr;
                    if (rndIFD == null || rndIFD.Length != 8) { throw new Exception("rndIFD wrong length"); }
                    if (rndICC == null || rndICC.Length != 8) { rndICC = new byte[8]; }
                    if (kIFD == null || kIFD.Length != 16) { throw new Exception("kIFD wrong length"); }
                    if (kEnc == null) { throw new Exception("kEnc == null"); }
                    if (kMac == null) { throw new Exception("kMac == null"); }

                    TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
                    TDESAlgorithm.Key = kEnc.Key;
                    TDESAlgorithm.IV = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
                    TDESAlgorithm.Mode = CipherMode.CBC;
                    TDESAlgorithm.Padding = PaddingMode.None;

                    byte[] plaintext = new byte[32];
                    Array.Copy(rndIFD, 0, plaintext, 0, 8);
                    Array.Copy(rndICC, 0, plaintext, 8, 8);
                    Array.Copy(kIFD, 0, plaintext, 16, 16);

                    MemoryStream memoryStream = new MemoryStream();

                    CryptoStream cryptoStream_Encryptor = new CryptoStream(memoryStream, TDESAlgorithm.CreateEncryptor(), CryptoStreamMode.Write);
                    cryptoStream_Encryptor.Write(plaintext, 0, plaintext.Length);
                    cryptoStream_Encryptor.Close();
                    byte[] ciphertext = memoryStream.ToArray();
                    memoryStream.Close();

                    byte[] newArray = new byte[40];
                    newArray[32] = 128;
                    Array.Copy(ciphertext, 0, newArray, 0, ciphertext.Length);
                    IMac macProvider = new ISO9797Alg3Mac(new DesEngine());
                    macProvider.Init(new KeyParameter(kMac.Key));
                    macProvider.Reset();

                    macProvider.BlockUpdate(newArray, 0, newArray.Length);
                    byte[] output = new byte[macProvider.GetMacSize()];
                    macProvider.DoFinal(output, 0);

                    byte[] datas = new byte[32 + 8];
                    Array.Copy(ciphertext, 0, datas, 0, 32);
                    Array.Copy(output, 0, datas, 32, 8);

                    //Tomamos la respuesta del APDU
                    byte[] objRes = null;
                    cAPDU = new CommandAPDU();
                    cAPDU.cla = (byte)0x00;
                    cAPDU.ins = (byte)0x82;
                    cAPDU.p1 = (byte)0x00;
                    cAPDU.p2 = (byte)0x00;
                    cAPDU.udc = datas;
                    cAPDU.le = 40;
                    rAPDU = new ResponseAPDU();
                    cardChannel.transmit(cAPDU, rAPDU);

                    byte[] objresfinal = new byte[32];

                    if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                    {
                        byte[] resultado = new byte[32];
                        //El APDU retorna sin el sw1 y sw2, o sea 40 bytes.
                        objRes = ((ResponseAPDU)rAPDU).udr;
                        //Se copian solo los 32 bits significativos
                        Array.Copy(objRes, 0, resultado, 0, 32);

                        //Crypto                       
                        memoryStream = new MemoryStream(objRes);
                        cryptoStream_Encryptor = new CryptoStream(memoryStream, TDESAlgorithm.CreateDecryptor(), CryptoStreamMode.Read);
                        cryptoStream_Encryptor.Read(resultado, 0, 32);
                        memoryStream.Close();

                        byte[] kICC = new byte[16];
                        Array.Copy(resultado, 16, kICC, 0, 16);
                        //Construimos el nuevo Kseed.
                        keySeed = new byte[16];
                        for (int i = 0; i < 16; i++)
                        {
                            keySeed[i] = (byte)((kIFD[i] & 0xFF) ^ (kICC[i] & 0xFF));
                        }

                        ksEnc = deriveKey(keySeed, 1);
                        HMACSHA1 ksMac = deriveKey(keySeed, 2);
                        MemoryStream os = new MemoryStream();
                        ssc = computeSendSequenceCounter(rndICC, rndIFD);

                        WSCT.Core.ConsoleTests.SecureMessagingWrapper objSecureMessagingWrapper = new WSCT.Core.ConsoleTests.SecureMessagingWrapper(ksEnc, ksMac, ssc);

                        //Leemos el sector donde esta el EF_COM
                        short fid = 286;
                        byte[] fiddle = { (byte)((fid >> 8) & 0x000000FF), (byte)(fid & 0x000000FF) };
                        cAPDU = new CommandAPDU();
                        cAPDU.cla = (byte)0x00;
                        cAPDU.ins = (byte)0xA4;
                        cAPDU.p1 = (byte)0x02;
                        cAPDU.p2 = (byte)0x0c;
                        cAPDU.udc = fiddle;
                        rAPDU = new ResponseAPDU();
                        ResponseAPDU respuestaSectorEF_COM = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
                        logger.Info("Respuesta lectura sector EF_COM: " + respuestaSectorEF_COM.ToString());
                        //Fin lectura del sector EF_COM

                        //Seleccionar aplicación MOC.
                        cAPDU = new CommandAPDU();
                        cAPDU.cla = (byte)0x00;
                        cAPDU.ins = (byte)0xA4;
                        cAPDU.p1 = (byte)0x04;
                        cAPDU.p2 = (byte)0x0C;
                        cAPDU.lc = (byte)0x0A;
                        cAPDU.le = (byte)0x00;
                        cAPDU.udc = StringToByteArrayFastest("E828BD080FD25043686C43432D654944");
                        ResponseAPDU respuestaSeleccionMOC = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
                        logger.Info("Respuesta lectura selección MOC: " + respuestaSeleccionMOC.ToString());

                        cAPDU = new CommandAPDU();
                        cAPDU.cla = (byte)0x00;
                        cAPDU.ins = (byte)0xCB;
                        cAPDU.p1 = (byte)0x3F;
                        cAPDU.p2 = (byte)0xFF;
                        cAPDU.lc = (byte)0x0E;
                        cAPDU.le = (byte)0x00;
                        cAPDU.udc = StringToByteArrayFastest("4D0C700ABF8212067F50037F6080");

                        respuestaSeleccionMOC = transmit(cardChannel, null, cAPDU);
                        logger.Info("Respuesta lectura selección MOC: " + respuestaSeleccionMOC.ToString());

                        //hit
                        //cAPDU = new CommandAPDU("002100916B7F2E68816665159b6c167a3219617523996d31994937bd54379c1e38a5433ba0634077114686364d424252ba3b57bd47579a505d5615604a426259446b47226f896770554e75977b7995207f472c804152809c688a9a4a8b9e188d457b8f571994416f949c5b969e37a36400");

                        //nohit
                        //cAPDU = new CommandAPDU("00210091C17F2DC081BE5b5c9b505d7c56679a5e51465350444f677f6251a14d517b584c5d6852684868784e6e5a54705d6170626d4f6a454b7d6d4baa673c633f41bc5882457e596b687f886c7f48723d687f6ba94836bf6835606132427c76a92c55777b7b68875d8a875f8c65894783708b7e78a9688987602ea15d2c815929817c818e5f29823784596f8c654d2982782f656727a2758da3728f652a3cbc7d8a85332e80792aa31851b8541b83174bb915517722327f31977b6da34273a3436fa683311a4100");NHit

                        //Final MoC Indice Derecho
                        cAPDU = new CommandAPDU();
                        cAPDU.cla = (byte)0x00;
                        cAPDU.ins = (byte)0x21;
                        cAPDU.p1 = (byte)0x00;
                        cAPDU.p2 = (byte)0x91;

                        String[] RawData = ISOToISOC(iso);
                        cAPDU.lc = (uint)RawData.GetLength(0) + 5;

                        String[] Data = new String[cAPDU.lc];

                        Data[0] = "7F";
                        Data[1] = "2E";

                        uint DataA = cAPDU.lc - 3;
                        string DataAString = String.Format("{0:X}", DataA);
                        Data[2] = DataAString;

                        Data[3] = "81";

                        uint DataB = cAPDU.lc - 5;
                        string DataBString = String.Format("{0:X}", DataB);
                        Data[4] = DataBString;

                        for (int n = 0; n < cAPDU.lc - 5; n++)
                        {
                            Data[n + 5] = RawData[n];
                        }

                        string FinalData = "";

                        for (int n = 0; n < cAPDU.lc; n++)
                        {
                            FinalData = FinalData + Data[n];
                        }

                        cAPDU.udc = StringToByteArrayFastest(FinalData);

                        cAPDU.le = (byte)0x00;

                        //MoC Indice Derecho
                        ResponseAPDU HitDerecho = transmit(cardChannel, null, cAPDU);
                        logger.Info("Respuesta Hit Derecho: " + HitDerecho.ToString());

                        if (HitDerecho.ToString() == "90-00")
                        {
                            HIToNoHIT = true;
                            fingerverified = 2;
                        }

                        //Final MoC Indice Izquierdo si con dedo izq no es positivo
                        if (!HIToNoHIT)
                        {
                            cAPDU = new CommandAPDU();
                            cAPDU.cla = (byte)0x00;
                            cAPDU.ins = (byte)0x21;
                            cAPDU.p1 = (byte)0x00;
                            cAPDU.p2 = (byte)0x92;
                            cAPDU.lc = (uint)RawData.GetLength(0) + 5;
                            cAPDU.udc = StringToByteArrayFastest(FinalData);
                            cAPDU.le = (byte)0x00;

                            //MoC Indice Izquierdo
                            ResponseAPDU HitIzquierdo = transmit(cardChannel, null, cAPDU);
                            logger.Info("Respuesta Hit Derecho: " + HitDerecho.ToString());

                            if (HitIzquierdo.ToString() == "90-00")
                            {
                                HIToNoHIT = true;
                                fingerverified = 2;
                            }
                        }
                    }
                }
            }
            //context.release();
            context.cancel();

            return HIToNoHIT;
        }

        public String[] ISOToISOC(string iso)
        {
            Byte[] ISOTemplate = Convert.FromBase64String(iso);

            IEngine.Init();

            Byte[] Temp = new Byte[4000];
            int ISOLength = 0, ISOCLength = 0;

            Iso.ConvertToISOCardCC(ISOTemplate, 63, 0, 0, ref ISOLength, null);
            Byte[] ISOCTemplateWrapped = new Byte[ISOLength];
            Iso.ConvertToISOCardCC(ISOTemplate, 63, 0, 0, ref ISOLength, ISOCTemplateWrapped);

            IEngine.Terminate();

            for (int n = 0; n < ISOCTemplateWrapped.Length; n++)
            {
                Temp[n] = ISOCTemplateWrapped[n + 28];
                if (n > 0 && Temp[n - 1] == 0x00)
                {
                    ISOCLength = n;
                    break;
                }
            }

            ISOCLength = ISOCLength - 1;

            Byte[] ISOCTemplate = new Byte[ISOCLength];

            for (int n = 0; n < ISOCLength; n++)
            {
                ISOCTemplate[n] = Temp[n];
            }

            String Data = BitConverter.ToString(ISOCTemplate);
            String[] DataArray = Data.Split('-');

            return DataArray;
        }

        public byte[] getBytes(String str)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(str); //Convert.FromBase64String(str);
            UTF8Encoding utf8 = new UTF8Encoding();

            byte[] encodedBytes;
            try
            {
                bytes = Encoding.UTF8.GetBytes(str);
                encodedBytes = utf8.GetBytes(str);
            }
            catch (Exception use)
            {
                logger.Error(use, "Excepción en CLR.cs getBytes()");
                /* NOTE: unlikely. */
                //use.printStackTrace();
                //MessageBox.Show("Error en getBytes");
            }
            return bytes;
        }

        public char checkDigit(String str)
        {
            return checkDigit(str, false);
        }

        public char checkDigit(String str, bool preferFillerOverZero)
        {
            try
            {

                byte[] chars = str == null ? new byte[] { } : getBytes(str);//str.getBytes("UTF-8");
                int[] weights = { 7, 3, 1 };
                int result = 0;
                for (int i = 0; i < chars.Length; i++)
                {
                    result = (result + weights[i % 3] * decodeMRZDigit((char)chars[i])) % 10;
                }
                String checkDigitString = Convert.ToString(result);
                if (checkDigitString.Length != 1) { throw new Exception("Error in computing check digit."); /* NOTE: Never happens. */ }
                //char checkDigit = (char)checkDigitString.getBytes("UTF-8")[0];
                char checkDigit = (char)Encoding.UTF8.GetBytes(checkDigitString)[0];

                if (preferFillerOverZero && checkDigit == '0') { checkDigit = '<'; }
                return checkDigit;
            }
            catch (Exception nfe)
            {
                /* NOTE: never happens. */
                //nfe.printStackTrace();
                logger.Error(nfe, "Error in computing check digit. CLR_checkDigit.");
                throw new Exception("Error in computing check digit.");
            }

        }

        public int decodeMRZDigit(char ch)
        {
            switch (ch)
            {
                case '<':
                case '0': return 0;
                case '1': return 1;
                case '2': return 2;
                case '3': return 3;
                case '4': return 4;
                case '5': return 5;
                case '6': return 6;
                case '7': return 7;
                case '8': return 8;
                case '9': return 9;
                case 'a':
                case 'A': return 10;
                case 'b':
                case 'B': return 11;
                case 'c':
                case 'C': return 12;
                case 'd':
                case 'D': return 13;
                case 'e':
                case 'E': return 14;
                case 'f':
                case 'F': return 15;
                case 'g':
                case 'G': return 16;
                case 'h':
                case 'H': return 17;
                case 'i':
                case 'I': return 18;
                case 'j':
                case 'J': return 19;
                case 'k':
                case 'K': return 20;
                case 'l':
                case 'L': return 21;
                case 'm':
                case 'M': return 22;
                case 'n':
                case 'N': return 23;
                case 'o':
                case 'O': return 24;
                case 'p':
                case 'P': return 25;
                case 'q':
                case 'Q': return 26;
                case 'r':
                case 'R': return 27;
                case 's':
                case 'S': return 28;
                case 't':
                case 'T': return 29;
                case 'u':
                case 'U': return 30;
                case 'v':
                case 'V': return 31;
                case 'w':
                case 'W': return 32;
                case 'x':
                case 'X': return 33;
                case 'y':
                case 'Y': return 34;
                case 'z':
                case 'Z': return 35;
                default:
                    throw new Exception("Could not decode MRZ character "
                            + ch + " ('" + Convert.ToString((char)ch) + "')");
            }
        }

        public HMACSHA1 deriveKey(byte[] keySeed, String cipherAlg, int keyLength, byte[] nonce, int counter)
        {
            String digestAlg = inferDigestAlgorithmFromCipherAlgorithmForKeyDerivation(cipherAlg, keyLength);
            //LOGGER.info("DEBUG: key derivation uses digestAlg = " + digestAlg);
            //MessageDigest digest = MessageDigest.getInstance(digestAlg);
            SHA1 sha = SHA1Managed.Create();
            sha.Initialize();
            //digest.update(keySeed);
            sha.TransformBlock(keySeed, 0, keySeed.Length, keySeed, 0);
            if (nonce != null)
            {
                //digest.update(nonce);
                sha.TransformBlock(nonce, 0, nonce.Length, nonce, 0);
            }
            //digest.update(new byte[] { 0x00, 0x00, 0x00, (byte)counter });
            sha.TransformFinalBlock(new byte[] { 0x00, 0x00, 0x0, (byte)counter }, 0, new byte[] { 0x00, 0x00, 0x0, (byte)counter }.Length);
            //byte[] hashResult = digest.digest();
            byte[] hashResult = sha.Hash;
            //LOGGER.info("DEBUG: hashResult.length = " + hashResult.length);
            byte[] keyBytes = null;
            if ("DESede".Equals(cipherAlg) || "3DES".Equals(cipherAlg))
            {
                /* TR-SAC 1.01, 4.2.1. */
                switch (keyLength)
                {
                    case 112:
                    case 128:
                        keyBytes = new byte[24];
                        Array.Copy(hashResult, 0, keyBytes, 0, 8); /* E  (octets 1 to 8) */
                        Array.Copy(hashResult, 8, keyBytes, 8, 8); /* D  (octets 9 to 16) */
                        Array.Copy(hashResult, 0, keyBytes, 16, 8); /* E (again octets 1 to 8, i.e. 112-bit 3DES key) */
                        break;
                    default:
                        throw new Exception("KDF can only use DESede with 128-bit key length");
                }
            }
            else if ("AES".Equals(cipherAlg) || cipherAlg.StartsWith("AES"))
            {
                //LOGGER.info("DEBUG: key derivation with AES uses key length " + keyLength);
                /* TR-SAC 1.01, 4.2.2. */
                switch (keyLength)
                {
                    case 128:
                        keyBytes = new byte[16]; /* NOTE: 128 = 16 * 8 */
                        Array.Copy(hashResult, 0, keyBytes, 0, 16);
                        break;
                    case 192:
                        keyBytes = new byte[24]; /* NOTE: 192 = 24 * 8 */
                        Array.Copy(hashResult, 0, keyBytes, 0, 24);
                        break;
                    case 256:
                        keyBytes = new byte[32]; /* NOTE: 256 = 32 * 8 */
                        Array.Copy(hashResult, 0, keyBytes, 0, 32);
                        break;
                    default:
                        throw new Exception("KDF can only use AES with 128-bit, 192-bit key or 256-bit length, found: " + keyLength + "-bit key length");
                }
            }
            //		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(cipherAlgName);
            //		return keyFactory.generateSecret(new SecretKeySpec(keyBytes, cipherAlgName));
            return new HMACSHA1(keyBytes);
        }

        public HMACSHA1 deriveKey(byte[] keySeed, int mode)
        {
            return deriveKey(keySeed, "DESede", 128, mode);
        }

        public HMACSHA1 deriveKey(byte[] keySeed, String cipherAlgName, int keyLength, int mode)
        {
            return deriveKey(keySeed, cipherAlgName, keyLength, null, mode);
        }

        public long computeSendSequenceCounter(byte[] rndICC, byte[] rndIFD)
        {
            if (rndICC == null || rndICC.Length != 8
                    || rndIFD == null || rndIFD.Length != 8)
            {
                throw new Exception("Wrong length input");
            }
            long ssc = 0;
            for (int i = 4; i < 8; i++)
            {
                ssc <<= 8;
                ssc += (long)(rndICC[i] & 0x000000FF);
            }
            for (int i = 4; i < 8; i++)
            {
                ssc <<= 8;
                ssc += (long)(rndIFD[i] & 0x000000FF);
            }

            return ssc;
        }

        public ResponseAPDU transmit(WSCT.Core.ICardChannel cardChannel, WSCT.Core.ConsoleTests.SecureMessagingWrapper wrapper, CommandAPDU capdu)
        {
            CommandAPDU plainCapdu = capdu;
            if (wrapper != null)
            {
                capdu = wrapper.wrap(capdu);
            }
            ResponseAPDU rapdu = new ResponseAPDU();
            cardChannel.transmit(capdu, rapdu);
            if (wrapper != null)
            {
                rapdu = wrapper.unwrap(rapdu, rapdu.udr.Length);
            }
            int sw = 0;
            if (rapdu != null)
            {
                if ((int)rapdu.sw1 < 8)
                    sw = rapdu.sw1;
                else
                    sw = rapdu.sw2;
                //ISO7816.SW_CORRECT_LENGTH_00 27648 [0x6c00]

                if ((sw & 27648) == 27648)
                {
                    int ne = (sw & 0xFF);
                    plainCapdu = new CommandAPDU();
                    plainCapdu.cla = plainCapdu.cla;
                    plainCapdu.ins = plainCapdu.ins;
                    plainCapdu.p1 = plainCapdu.p1;
                    plainCapdu.p2 = plainCapdu.p2;
                    plainCapdu.udc = plainCapdu.udc;
                    plainCapdu.le = (byte)ne;
                    if (wrapper != null)
                    {
                        capdu = wrapper.wrap(plainCapdu);
                    }
                    rapdu = null;
                    cardChannel.transmit(plainCapdu, rapdu);
                    if (wrapper != null)
                    {
                        rapdu = wrapper.unwrap(rapdu, rapdu.udr.Length);
                        //notifyExchangedPlainTextAPDU(++plainAPDUCount, plainCapdu, rapdu);
                    }
                }
            }
            return rapdu;
        }

        public byte[] StringToByteArrayFastest(string hex)
        {
            if (hex.Length % 2 == 1)
                throw new Exception("The binary key cannot have an odd number of digits");

            byte[] arr = new byte[hex.Length >> 1];

            for (int i = 0; i < (hex.Length >> 1); ++i)
            {
                arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
            }

            return arr;
        }

        public int GetHexVal(char hex)
        {
            int val = (int)hex;
            //For uppercase A-F letters:
            return val - (val < 58 ? 48 : 55);
            //For lowercase a-f letters:
            //return val - (val < 58 ? 48 : 87);
            //Or the two combined, but a bit slower:
            //return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }

        public String inferDigestAlgorithmFromCipherAlgorithmForKeyDerivation(String cipherAlg, int keyLength)
        {
            if (cipherAlg == null) { throw new Exception(); }
            if ("DESede".Equals(cipherAlg) || "AES-128".Equals(cipherAlg)) { return "SHA-1"; }
            if ("AES".Equals(cipherAlg) && keyLength == 128) { return "SHA-1"; }
            if ("AES-256".Equals(cipherAlg) || "AES-192".Equals(cipherAlg)) { return "SHA-256"; }
            if ("AES".Equals(cipherAlg) && (keyLength == 192 || keyLength == 256)) { return "SHA-256"; }
            throw new Exception("Unsupported cipher algorithm or key length \"" + cipherAlg + "\", " + keyLength);
        }


        public void CLR_OPEN(uint timeout)
        {
            int indexreaders = 0;
            context.establish();
            context.listReaderGroups();
            context.listReaders(context.groups[0]);
            logger.Info("CLR_OPEN: L:" + timeout);
            for (int i = 0; i < context.readersCount; i++)
            {
                logger.Info("CLR_OPEN: Lector encontrado:" + context.readers[i]);
                if (context.readers[i].Contains(readername) || context.readers[i].Equals(readername)
                    || String.Equals(context.readers[i], readername))
                {
                    indexreaders = i;
                    logger.Info("CLR_OPEN: Lector seleccionado:" + context.readers[i]);
                    i = context.readersCount + 1;
                }
                else
                {
                    logger.Error("CLR_OPEN: No se encuentra conectado el Lector seleccionado:" + context.readers[i]);
                }

            }

            logger.Info("CLR_OPEN: TIMEOUT ASIGNADO:" + timeout);

            monitor = new WSCT.Core.StatusChangeMonitor(context, new string[] { ReaderName });
        }

        public List<String> GetConnectedDevices()
        {
            var lista = new List<String>();

            context.establish();
            try
            {
                var error = context.listReaderGroups();
                logger.Info("hello moto");
                error = context.listReaders(context.groups[0]);
                for (int i = 0; i < context.readersCount; i++)
                {
                    lista.Add(context.readers[i]);
                }
            }
            catch (OverflowException ex)
            {
                logger.Info(ex, "No se encontraron dispositivos conectados");
            }
            catch (Exception ex)
            {
                logger.Info(ex, "No se encontraron dispositivos conectados");

            }
            return lista;

        }

        public Boolean Match(String[] _isocc, out int _fingerVerified)
        {
            logger.Info("Huella que llega al Match: " + ConvertStringArrayToString(_isocc));
            Boolean _hit = false;
            _fingerVerified = 0;
            ResponseAPDU respuesta = new ResponseAPDU();
            try
            {

                //Seleccionar aplicación MOC.
                cAPDU = new CommandAPDU();
                cAPDU.cla = (Byte)0x00;
                cAPDU.ins = (Byte)0xA4;
                cAPDU.p1 = (Byte)0x04;
                cAPDU.p2 = (Byte)0x0C;
                cAPDU.lc = (Byte)0x0A;
                cAPDU.le = (Byte)0x00;
                cAPDU.udc = StringToByteArrayFastest("E828BD080FD25043686C43432D654944");
                respuesta = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
                if (respuesta != null)
                {
                    logger.Info("Respuesta selección aplicación MOC: " + respuesta.ToString());
                }
                else
                {
                    logger.Info("Respuesta a selección aplicación MOC: null");
                }

                cAPDU = new CommandAPDU();
                cAPDU.cla = (Byte)0x00;
                cAPDU.ins = (Byte)0xCB;
                cAPDU.p1 = (Byte)0x3F;
                cAPDU.p2 = (Byte)0xFF;
                cAPDU.lc = (Byte)0x0E;
                cAPDU.le = (Byte)0x00;
                cAPDU.udc = StringToByteArrayFastest("4D0C700ABF8212067F50037F6080");

                respuesta = transmit(cardChannel, null, cAPDU);
                logger.Info("Segunda Respuesta selección aplicación MOC: " + respuesta.ToString());

                //Final MoC Indice Derecho
                cAPDU = new CommandAPDU();
                cAPDU.cla = (Byte)0x00;
                cAPDU.ins = (Byte)0x21;
                cAPDU.p1 = (Byte)0x00;
                cAPDU.p2 = (Byte)0x91;

                String[] _rawData = _isocc;
                cAPDU.lc = (uint)_rawData.GetLength(0) + 5;

                String[] data = new String[cAPDU.lc];

                data[0] = "7F";
                data[1] = "2E";

                uint _dataA = cAPDU.lc - 3;
                String _dataAString = String.Format("{0:X}", _dataA);
                data[2] = _dataAString;

                data[3] = "81";

                uint _dataB = cAPDU.lc - 5;
                String _dataBString = String.Format("{0:X}", _dataB);
                data[4] = _dataBString;

                for (Int32 n = 0; n < cAPDU.lc - 5; n++)
                {
                    data[n + 5] = _rawData[n];
                }

                String _finalData = "";

                for (Int32 n = 0; n < cAPDU.lc; n++)
                {
                    _finalData = _finalData + data[n];
                }

                cAPDU.udc = StringToByteArrayFastest(_finalData);

                cAPDU.le = (Byte)0x00;

                //MoC Indice Derecho
                ResponseAPDU _hitRight = transmit(cardChannel, null, cAPDU);
                logger.Info("Resultado HIT contra Derecho: " + _hitRight.ToString());

                if (_hitRight.ToString() == "90-00")
                {
                    _hit = true;
                    _fingerVerified = 7;
                }

                //Final MoC Indice Derecho
                if (!_hit)
                {
                    cAPDU = new CommandAPDU();
                    cAPDU.cla = (Byte)0x00;
                    cAPDU.ins = (Byte)0x21;
                    cAPDU.p1 = (Byte)0x00;
                    cAPDU.p2 = (Byte)0x92;
                    cAPDU.lc = (uint)_rawData.GetLength(0) + 5;
                    cAPDU.udc = StringToByteArrayFastest(_finalData);
                    cAPDU.le = (Byte)0x00;

                    //MoC Indice Izquierdo
                    ResponseAPDU hitLeft = transmit(cardChannel, null, cAPDU);
                    logger.Info("Resultado HIT contra izquierdo: " + _hitRight.ToString());

                    if (hitLeft.ToString() == "90-00")
                    {
                        _hit = true;
                        _fingerVerified = 4;
                    }
                }
                context.cancel();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error al capturar el _hit");
            }
            return _hit;
        }

        public Boolean Match2020(String[] _isocc, out int _fingerVerified)
        {
            logger.Info("CLR.Match2020 - Huella que llega al Match: " + ConvertStringArrayToString(_isocc));
            Boolean _hit = false;
            _fingerVerified = 0;
            ResponseAPDU respuesta = new ResponseAPDU();
            try
            {
                logger.Debug("CLR.Match - Selecciono Aplicacion...");

                //Seleccionar aplicación MOC.
                cAPDU = new CommandAPDU();
                cAPDU.cla = (Byte)0x00;
                cAPDU.ins = (Byte)0xA4;
                cAPDU.p1 = (Byte)0x04;
                cAPDU.p2 = (Byte)0x0C;
                cAPDU.lc = (Byte)0x0A;
                cAPDU.le = (Byte)0x00;
                cAPDU.udc = StringToByteArrayFastest("E828BD080FD25043686C43432D654944");
                logger.Debug("CLR.Match2020 - lc = 0x0A - APDU = " + "E828BD080FD25043686C43432D654944");
                respuesta = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
                //respuesta = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
                if (respuesta != null)
                {
                    //logger.Info("Respuesta selección aplicación MOC: " + respuesta.ToString());
                    logger.Debug("CLR.Match2020 - Respuesta selección aplicación MOC: " + respuesta.ToString());
                }
                else
                {
                    logger.Info("Respuesta a selección aplicación MOC: null");
                    logger.Debug("CLR.Match2020 - Respuesta a selección aplicación MOC: null");
                }

                cAPDU = new CommandAPDU();
                cAPDU.cla = (Byte)0x00;
                cAPDU.ins = (Byte)0xCB;
                cAPDU.p1 = (Byte)0x3F;
                cAPDU.p2 = (Byte)0xFF;
                cAPDU.lc = (Byte)0x0E;
                cAPDU.le = (Byte)0x00;
                //cAPDU.udc = StringToByteArrayFastest("4D0C700ABF8212067F50037F6080");
                cAPDU.udc = StringToByteArrayFastest("4D0C700ABF8212067F50037F6080");
                logger.Debug("CLR.Match2020 - APDU = " + "4D0C700ABF8212067F50037F6080");
                respuesta = transmit(cardChannel, null, cAPDU);
                //respuesta = transmitAES(cardChannel, null, cAPDU);
                if (respuesta != null)
                {
                    logger.Debug("CLR.Match2020 - Respuesta selección aplicación 2 MOC: " + respuesta.ToString());
                }
                else
                {
                    logger.Debug("CLR.Match2020 - Respuesta a selección aplicación MOC: null");
                }
                //logger.Info("Segunda Respuesta selección aplicación MOC: " + respuesta.ToString());


                //Final MoC Indice Derecho
                logger.Debug("CLR.Match2020 - Armando APDU de huella 1...");
                cAPDU = new CommandAPDU();
                cAPDU.cla = (Byte)0x00;
                cAPDU.ins = (Byte)0x21;
                cAPDU.p1 = (Byte)0x00;
                cAPDU.p2 = (Byte)0x91;

                String[] _rawData = _isocc;
                cAPDU.lc = (uint)_rawData.GetLength(0) + 7;

                String[] data = new String[cAPDU.lc];

                data[0] = "7F";
                data[1] = "2E";
                data[2] = "81";

                uint _dataA = cAPDU.lc - 3;
                String _dataAString = String.Format("{0:X}", _dataA);
                data[3] = _dataAString;

                data[4] = "81";

                data[5] = "81";
                uint _dataB = cAPDU.lc - 7;
                String _dataBString = String.Format("{0:X}", _dataB);
                data[6] = _dataBString;

                for (Int32 n = 0; n < cAPDU.lc - 7; n++)
                {
                    data[n + 7] = _rawData[n];
                }

                String _finalData = "";

                for (Int32 n = 0; n < cAPDU.lc; n++)
                {
                    _finalData = _finalData + data[n];
                }

                cAPDU.udc = StringToByteArrayFastest(_finalData);

                cAPDU.le = (Byte)0x00;

                //MoC Indice Derecho
                logger.Debug("CLR.Match2020 - APDU Indice Derecho = " + _finalData);
                ResponseAPDU _hitRight = transmit(cardChannel, null, cAPDU);
                logger.Debug("CLR.Match2020 - Resultado HIT contra Derecho: " + _hitRight.ToString());

                if (_hitRight.ToString() == "90-00")
                {
                    _hit = true;
                    _fingerVerified = 2;
                }

                //Final MoC Indice Derecho
                if (!_hit)
                {
                    cAPDU = new CommandAPDU();
                    cAPDU.cla = (Byte)0x00;
                    cAPDU.ins = (Byte)0x21;
                    cAPDU.p1 = (Byte)0x00;
                    cAPDU.p2 = (Byte)0x92;
                    cAPDU.lc = (uint)_rawData.GetLength(0) + 7;
                    cAPDU.udc = StringToByteArrayFastest(_finalData);
                    cAPDU.le = (Byte)0x00;

                    //MoC Indice Izquierdo
                    logger.Debug("CLR.Match2020 - APDU Indice Izquierdo = " + _finalData);
                    ResponseAPDU hitLeft = transmit(cardChannel, null, cAPDU);
                    logger.Debug("CLR.Match2020 - Resultado HIT contra izquierdo: " + _hitRight.ToString());

                    if (hitLeft.ToString() == "90-00")
                    {
                        _hit = true;
                        _fingerVerified = 7;
                    }
                }
                context.cancel();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "CLR.Match2020 Excp Error al capturar el _hit");
                //LOG.Error("CLR.Match2020 Excp - " + ex.Message);
            }
            logger.Debug("CLR.Match2020 OUT! _hit = " + _hit.ToString());
            return _hit;
        }

        static string ConvertStringArrayToString(string[] array)
        {
            //
            // Concatenate all the elements into a StringBuilder.
            //
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
            }
            return builder.ToString();
        }

        public Boolean CLR_SIMPLE_CONNECT()
        {
            monitor = new WSCT.Core.StatusChangeMonitor(context, new string[] { ReaderName });
            readerState = monitor.waitForCardPresence(0);
            if (readerState == null)
            {
                logger.Info("CLR_CONNECT: Definitivamente no hay tarjeta");
                return false;
            }
            else
            {
                logger.Info("CLR_OPEN Hay tarjeta en el lector:" + ReaderName);
                return true;
            }
        }

        public void CLR_CONNECT(uint timeout)
        {

            logger.Info("CLR_Connect TIMEOUT ASIGNADO:" + timeout);

            monitor = new WSCT.Core.StatusChangeMonitor(context, new string[] { ReaderName });

            logger.Info("Entrando a Timeout");
            readerState = monitor.waitForCardPresence(0);
            logger.Info("Timeout");

            if (readerState == null)
            {
                readerState = monitor.waitForCardPresence(timeout);
                logger.Info("CLR_CONNECT: No hay tarjeta presente en el lector:" + ReaderName);
            }
            if (readerState == null)
            {
                logger.Info("CLR_CONNECT: Definitivamente no hay tarjeta");
                error = -1;
            }
            else
            {
                logger.Info("CLR_OPEN Hay tarjeta en el lector:" + ReaderName);
                error = 0;
            }

            //cardChannel = new WSCT.Core.CardChannel(context, readerState.readerName);
            cardChannel = new WSCT.Core.CardChannel(context, readername);
            cardChannel.connect(WSCT.Wrapper.ShareMode.SCARD_SHARE_SHARED, WSCT.Wrapper.Protocol.SCARD_PROTOCOL_ANY);
            Byte[] recvBuffer = null;
            cardChannel.getAttrib(Attrib.SCARD_ATTR_ATR_STRING, ref recvBuffer);

            recvBuffer = null;
            cardChannel.getAttrib(Attrib.SCARD_ATTR_DEVICE_FRIENDLY_NAME, ref recvBuffer);

            recvBuffer = null;
            cardChannel.getAttrib(Attrib.SCARD_ATTR_ATR_STRING, ref recvBuffer);

            cardChannel.reconnect(WSCT.Wrapper.ShareMode.SCARD_SHARE_SHARED, WSCT.Wrapper.Protocol.SCARD_PROTOCOL_T1, WSCT.Wrapper.Disposition.SCARD_RESET_CARD);

            logger.Info("CLR_CONNECT: Conectado a:" + cardChannel.readerName);
            logger.Info("CLR_CONNECT: Conectado");
        }

        public void CLR_SECURITY(string docnum, string dob, string doe)
        {
            logger.Info("CLR_Security: Comenzando Transmisión de Comandos APDU canal seleccionado:." + cardChannel.readerName);

            CommandAPDU cAPDU = new CommandAPDU("00A4040005A000000069");
            ICardResponse rAPDU = new ResponseAPDU();
            logger.Info("CLR_Security: APDU: A.");
            cardChannel.transmit(cAPDU, rAPDU);


            logger.Info("CLR_Security: Respuesta APDU:" + ((ResponseAPDU)rAPDU).sw1.ToString());

            cAPDU = new CommandAPDU("00A404000E315041592E5359532E4444463031");
            rAPDU = new ResponseAPDU();
            logger.Info("CLR_Security: APDU: B.");
            cardChannel.transmit(cAPDU, rAPDU);

            cAPDU = new CommandAPDU(0x00, 0xA4, 0x00, 0x00, 0x02, new Byte[2] { 0x3F, 0x00 });
            rAPDU = new ResponseAPDU();
            logger.Info("CLR_Security: APDU: C.");
            cardChannel.transmit(cAPDU, rAPDU);

            byte[] data = new byte[] { 0xa0, 0x00, 0x00, 0x02, 0x47, 0x10, 0x01 };

            cAPDU = new CommandAPDU();
            cAPDU.cla = (byte)0x00;
            cAPDU.ins = (byte)0xA4;
            cAPDU.p1 = (byte)0x04;
            cAPDU.p2 = (byte)0x0C;
            cAPDU.udc = new Byte[] { (byte)0xa0, (byte)0x00, (byte)0x00, (byte)0x02, (byte)0x47, (byte)0x10, (byte)0x01 };
            rAPDU = new ResponseAPDU();
            logger.Info("CLR_Security: APDU: D.");
            cardChannel.transmit(cAPDU, rAPDU);

            if (((ResponseAPDU)rAPDU).sw1 == 0x90)
            {
                //MRZ Final
                String documentNumber = docnum;
                String dateOfBirth = dob;
                String dateOfExpiry = doe;

                logger.Info("CLR_Security: documentNumber:" + docnum);

                cAPDU = new CommandAPDU();
                cAPDU.cla = (byte)0x00;
                cAPDU.p1 = (byte)0x00;
                cAPDU.p2 = (byte)0x00;
                cAPDU.ins = (byte)0x84;
                cAPDU.le = 8;
                rAPDU = new ResponseAPDU();
                logger.Info("CLR_Security: APDU: E.");
                cardChannel.transmit(cAPDU, rAPDU);

                if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                {
                    byte[] cd1 = getBytes(checkDigit(documentNumber).ToString());
                    byte[] cd2 = getBytes(checkDigit(dateOfBirth).ToString());
                    byte[] cd3 = getBytes(checkDigit(dateOfExpiry).ToString());

                    SHA1 sha = SHA1Managed.Create();
                    sha.TransformBlock(getBytes(documentNumber), 0, getBytes(documentNumber).Length, getBytes(documentNumber), 0);
                    sha.TransformBlock(cd1, 0, cd1.Length, cd1, 0);
                    sha.TransformBlock(getBytes(dateOfBirth), 0, getBytes(dateOfBirth).Length, null, 0);
                    sha.TransformBlock(cd2, 0, cd2.Length, cd2, 0);
                    sha.TransformBlock(getBytes(dateOfExpiry), 0, getBytes(dateOfExpiry).Length, null, 0);
                    sha.TransformFinalBlock(cd3, 0, cd3.Length);
                    byte[] hash = sha.Hash;
                    byte[] keySeed = new byte[16];
                    Array.Copy(hash, keySeed, 16);
                    HMACSHA1 kEnc = deriveKey(keySeed, 1);
                    HMACSHA1 kMac = deriveKey(keySeed, 2);

                    //Creamos el random.
                    byte[] rndIFD = new byte[8];
                    rngCsp.GetBytes(rndIFD);
                    byte[] kIFD = new byte[16];
                    rngCsp.GetBytes(kIFD);
                    byte[] rndICC = ((ResponseAPDU)rAPDU).udr;
                    if (rndIFD == null || rndIFD.Length != 8) { throw new Exception("rndIFD wrong length"); }
                    if (rndICC == null || rndICC.Length != 8) { rndICC = new byte[8]; }
                    if (kIFD == null || kIFD.Length != 16) { throw new Exception("kIFD wrong length"); }
                    if (kEnc == null) { throw new Exception("kEnc == null"); }
                    if (kMac == null) { throw new Exception("kMac == null"); }

                    TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
                    TDESAlgorithm.Key = kEnc.Key;
                    TDESAlgorithm.IV = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
                    TDESAlgorithm.Mode = CipherMode.CBC;
                    TDESAlgorithm.Padding = PaddingMode.None;

                    byte[] plaintext = new byte[32];
                    Array.Copy(rndIFD, 0, plaintext, 0, 8);
                    Array.Copy(rndICC, 0, plaintext, 8, 8);
                    Array.Copy(kIFD, 0, plaintext, 16, 16);

                    MemoryStream memoryStream = new MemoryStream();

                    CryptoStream cryptoStream_Encryptor = new CryptoStream(memoryStream, TDESAlgorithm.CreateEncryptor(), CryptoStreamMode.Write);
                    cryptoStream_Encryptor.Write(plaintext, 0, plaintext.Length);
                    cryptoStream_Encryptor.Close();
                    byte[] ciphertext = memoryStream.ToArray();
                    memoryStream.Close();

                    byte[] newArray = new byte[40];
                    newArray[32] = 128;
                    Array.Copy(ciphertext, 0, newArray, 0, ciphertext.Length);
                    IMac macProvider = new ISO9797Alg3Mac(new DesEngine());
                    macProvider.Init(new KeyParameter(kMac.Key));
                    macProvider.Reset();

                    macProvider.BlockUpdate(newArray, 0, newArray.Length);
                    byte[] output = new byte[macProvider.GetMacSize()];
                    macProvider.DoFinal(output, 0);

                    byte[] datas = new byte[32 + 8];
                    Array.Copy(ciphertext, 0, datas, 0, 32);
                    Array.Copy(output, 0, datas, 32, 8);

                    //Tomamos la respuesta del APDU
                    byte[] objRes = null;
                    cAPDU = new CommandAPDU();
                    cAPDU.cla = (byte)0x00;
                    cAPDU.ins = (byte)0x82;
                    cAPDU.p1 = (byte)0x00;
                    cAPDU.p2 = (byte)0x00;
                    cAPDU.udc = datas;
                    cAPDU.le = 40;
                    rAPDU = new ResponseAPDU();
                    logger.Info("CLR_Security: APDU: F.");
                    cardChannel.transmit(cAPDU, rAPDU);

                    byte[] objresfinal = new byte[32];

                    if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                    {
                        byte[] resultado = new byte[32];
                        //El APDU retorna sin el sw1 y sw2, o sea 40 bytes.
                        objRes = ((ResponseAPDU)rAPDU).udr;
                        //Se copian solo los 32 bits significativos
                        Array.Copy(objRes, 0, resultado, 0, 32);

                        //Crypto                       
                        memoryStream = new MemoryStream(objRes);
                        cryptoStream_Encryptor = new CryptoStream(memoryStream, TDESAlgorithm.CreateDecryptor(), CryptoStreamMode.Read);
                        cryptoStream_Encryptor.Read(resultado, 0, 32);
                        memoryStream.Close();

                        byte[] kICC = new byte[16];
                        Array.Copy(resultado, 16, kICC, 0, 16);
                        //Construimos el nuevo Kseed.
                        keySeed = new byte[16];
                        for (int i = 0; i < 16; i++)
                        {
                            keySeed[i] = (byte)((kIFD[i] & 0xFF) ^ (kICC[i] & 0xFF));
                        }

                        ksEnc = deriveKey(keySeed, 1);
                        HMACSHA1 ksMac = deriveKey(keySeed, 2);
                        MemoryStream os = new MemoryStream();
                        ssc = computeSendSequenceCounter(rndICC, rndIFD);

                        objSecureMessagingWrapper = new WSCT.Core.ConsoleTests.SecureMessagingWrapper(ksEnc, ksMac, ssc);

                        //Leemos el sector donde esta el EF_COM
                        short fid = 286;
                        byte[] fiddle = { (byte)((fid >> 8) & 0x000000FF), (byte)(fid & 0x000000FF) };
                        cAPDU = new CommandAPDU();
                        cAPDU.cla = (byte)0x00;
                        cAPDU.ins = (byte)0xA4;
                        cAPDU.p1 = (byte)0x02;
                        cAPDU.p2 = (byte)0x0c;
                        cAPDU.udc = fiddle;
                        rAPDU = new ResponseAPDU();
                        logger.Info("CLR_Security: APDU: G.");
                        transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
                        //Fin lectura del sector EF_COM  
                    }
                }
            }
        }

        public bool CLR_MOC(string[] isocc, out int fingerverified)
        {
            fingerverified = 0;
            bool HIToNoHIT = false;

            //Seleccionar aplicación MOC.
            cAPDU = new CommandAPDU();
            cAPDU.cla = (byte)0x00;
            cAPDU.ins = (byte)0xA4;
            cAPDU.p1 = (byte)0x04;
            cAPDU.p2 = (byte)0x0C;
            cAPDU.lc = (byte)0x0A;
            cAPDU.le = (byte)0x00;
            cAPDU.udc = StringToByteArrayFastest("E828BD080FD25043686C43432D654944");
            transmit(cardChannel, objSecureMessagingWrapper, cAPDU);

            cAPDU = new CommandAPDU();
            cAPDU.cla = (byte)0x00;
            cAPDU.ins = (byte)0xCB;
            cAPDU.p1 = (byte)0x3F;
            cAPDU.p2 = (byte)0xFF;
            cAPDU.lc = (byte)0x0E;
            cAPDU.le = (byte)0x00;
            cAPDU.udc = StringToByteArrayFastest("4D0C700ABF8212067F50037F6080");

            transmit(cardChannel, null, cAPDU);

            //Final MoC Indice Derecho
            cAPDU = new CommandAPDU();
            cAPDU.cla = (byte)0x00;
            cAPDU.ins = (byte)0x21;
            cAPDU.p1 = (byte)0x00;
            cAPDU.p2 = (byte)0x91;

            String[] RawData = isocc;
            cAPDU.lc = (uint)RawData.GetLength(0) + 5;

            String[] Data = new String[cAPDU.lc];

            Data[0] = "7F";
            Data[1] = "2E";

            uint DataA = cAPDU.lc - 3;
            string DataAString = String.Format("{0:X}", DataA);
            Data[2] = DataAString;

            Data[3] = "81";

            uint DataB = cAPDU.lc - 5;
            string DataBString = String.Format("{0:X}", DataB);
            Data[4] = DataBString;

            for (int n = 0; n < cAPDU.lc - 5; n++)
            {
                Data[n + 5] = RawData[n];
            }

            string FinalData = "";

            for (int n = 0; n < cAPDU.lc; n++)
            {
                FinalData = FinalData + Data[n];
            }

            cAPDU.udc = StringToByteArrayFastest(FinalData);

            cAPDU.le = (byte)0x00;

            //MoC Indice Derecho
            ResponseAPDU HitDerecho = transmit(cardChannel, null, cAPDU);

            if (HitDerecho.ToString() == "90-00")
            {
                HIToNoHIT = true;
                fingerverified = 7;
                logger.Info("CLR_Security: HitDerecho:" + fingerverified);
            }

            //Final MoC Indice Derecho
            if (!HIToNoHIT)
            {
                cAPDU = new CommandAPDU();
                cAPDU.cla = (byte)0x00;
                cAPDU.ins = (byte)0x21;
                cAPDU.p1 = (byte)0x00;
                cAPDU.p2 = (byte)0x92;
                cAPDU.lc = (uint)RawData.GetLength(0) + 5;
                cAPDU.udc = StringToByteArrayFastest(FinalData);
                cAPDU.le = (byte)0x00;

                //MoC Indice Izquierdo
                ResponseAPDU HitIzquierdo = transmit(cardChannel, null, cAPDU);

                if (HitIzquierdo.ToString() == "90-00")
                {
                    HIToNoHIT = true;
                    fingerverified = 4;
                    logger.Info("CLR_Security: HitIzquierdo:" + fingerverified);
                }
            }

            context.cancel();
            return HIToNoHIT;
        }

        public void CLR_DISCONNECT()
        {
            cardChannel.disconnect(WSCT.Wrapper.Disposition.SCARD_RESET_CARD);
            cardChannel.disconnect(WSCT.Wrapper.Disposition.SCARD_EJECT_CARD);
            logger.Info("CLR_Security: CLR_DISCONNECT.");
        }

        public void CLR_CLOSE()
        {
            context.cancel();
            context.release();
            logger.Info("CLR_Security: CLR_CLOSE.");
        }

        public int GetLasError()
        {
            return error;
        }

        public void CLR_GETMRZ()
        {
            int totalLength = 0;
            int tlLength = 0;
            int len = 0;
            int fileLength = 0;
            int tag = 0;
            int vLength = 0;
            byte[] prefix = null;
            BufferedInputStream2 bufferedIn;
            ByteArrayInputStream baInputStream;
            TLVInputStream tlvInputStream;
            Dictionary<int, int> fileLengths = new Dictionary<int, int>();
            Dictionary<short, InputStream> rawStreams = new Dictionary<short, InputStream>();
            MRTDFileInfo objMRTDFileInfo;
            CardFileInputStream cardInputStream;          

            #region COM286
            cAPDU = new CommandAPDU();
            byte[] datos = new byte[] { 1, 30 };
            cAPDU.cla = (byte)0x00;
            cAPDU.ins = (byte)0xA4;
            cAPDU.p1 = (byte)0x02;
            cAPDU.p2 = (byte)0x0c;
            cAPDU.udc = datos;
            rAPDU = new ResponseAPDU();
            rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
            cAPDU = new CommandAPDU("00B0000008");

            rAPDU = new ResponseAPDU();
            rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
            prefix = ((ResponseAPDU)rAPDU).udr;
            baInputStream = new ByteArrayInputStream(prefix);
            tlvInputStream = new TLVInputStream(baInputStream);
            tag = tlvInputStream.readTag();//97
            vLength = tlvInputStream.readLength();//93
            tlLength = prefix.Length - baInputStream.Available();//2
            len = vLength + tlLength;

            objMRTDFileInfo = new MRTDFileInfo(286, len);//len=95
            cardInputStream = new CardFileInputStream(223, objMRTDFileInfo);
            fileLength = cardInputStream.getLength();//95
            bufferedIn = new BufferedInputStream2(cardInputStream, fileLength + 1);
            totalLength = totalLength + fileLength;//2203                                               
            fileLengths.Add(286, fileLength);
            bufferedIn.Mark(fileLength + 1);
            rawStreams.Add(286, bufferedIn);
            #endregion COM286
            #region SOD
            cAPDU = new CommandAPDU();
            datos = new byte[] { 1, 29 };
            cAPDU.cla = (byte)0x00;
            cAPDU.ins = (byte)0xA4;
            cAPDU.p1 = (byte)0x02;
            cAPDU.p2 = (byte)0x0c;
            cAPDU.udc = datos;//(Hay que reemplazar datos acá)                                               
            rAPDU = new ResponseAPDU();
            rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);

            cAPDU = new CommandAPDU("00B0000008");

            rAPDU = new ResponseAPDU();
            rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);//[119, -126, 8, 29, 48, -126, 8, 25, -112, 0]
            prefix = ((ResponseAPDU)rAPDU).udr;
            baInputStream = new ByteArrayInputStream(prefix);
            tlvInputStream = new TLVInputStream(baInputStream);
            tag = tlvInputStream.readTag();//97
            vLength = tlvInputStream.readLength();//93
            tlLength = prefix.Length - baInputStream.Available();//2
            len = vLength + tlLength;

            objMRTDFileInfo = new MRTDFileInfo(285, len);//len=95
            cardInputStream = new CardFileInputStream(223, objMRTDFileInfo);
            fileLength = cardInputStream.getLength();//95
            bufferedIn = new BufferedInputStream2(cardInputStream, fileLength + 1);
            totalLength = totalLength + fileLength;//2203                                               
            fileLengths.Add(285, fileLength);
            bufferedIn.Mark(fileLength + 1);
            rawStreams.Add(285, bufferedIn);

            #endregion SOD

            #region APDU Sector 1
            datos = new byte[] { 1, 1 };
            cAPDU = new CommandAPDU();
            cAPDU.cla = (byte)0x00;
            cAPDU.ins = (byte)0xA4;
            cAPDU.p1 = (byte)0x02;
            cAPDU.p2 = (byte)0x0c;
            cAPDU.udc = datos;//(Hay que reemplazar datos acá)                       
            rAPDU = new ResponseAPDU();
            rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);

            cAPDU = new CommandAPDU("00B0000008");

            rAPDU = new ResponseAPDU();
            rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);

            prefix = ((ResponseAPDU)rAPDU).udr;
            baInputStream = new ByteArrayInputStream(prefix);
            tlvInputStream = new TLVInputStream(baInputStream);
            tag = tlvInputStream.readTag();
            vLength = tlvInputStream.readLength();
            tlLength = prefix.Length - baInputStream.Available();
            len = vLength + tlLength;

            objMRTDFileInfo = new MRTDFileInfo(257, len);
            cardInputStream = new CardFileInputStream(223, objMRTDFileInfo);
            fileLength = cardInputStream.getLength();
            BufferedInputStream2 bufferedIn2 = new BufferedInputStream2(cardInputStream, fileLength + 1);
            totalLength = totalLength + fileLength;
            fileLengths.Add(257, fileLength);
            bufferedIn2.Mark(fileLength + 1);
            rawStreams.Add(257, bufferedIn);

            cAPDU = new CommandAPDU("00B000005F");

            rAPDU = new ResponseAPDU();
            rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);

            byte[] resultadodg1 = ((ResponseAPDU)rAPDU).udr;

            String documentcode = System.Text.Encoding.UTF8.GetString(resultadodg1);
            Mrz = documentcode.Substring(documentcode.IndexOf("I"));
            lastrow = Mrz.Substring(60, 30);

            #endregion APDU Sector 1

            
        }

        public string ToHex(int value)
        {
            return String.Format("0x{0:X}", value);
        }

        #region APDU Sector 2
        public byte[] CLR_GETFOTO()
        {
            byte[] foto = null;
            Bitmap retrato = null;
            cAPDU = new CommandAPDU("00A4020C020102");

            int largo = 0;
            rAPDU = new ResponseAPDU();
            rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
            if (((ResponseAPDU)rAPDU).sw1 == 0x90)
            {
                cAPDU = new CommandAPDU("00B0000008");
                rAPDU = new ResponseAPDU();
                rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
                if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                {
                    prefix = ((ResponseAPDU)rAPDU).udr;
                    baInputStream = new ByteArrayInputStream(prefix);
                    tlvInputStream = new TLVInputStream(baInputStream);
                    tag = tlvInputStream.readTag();//97
                    vLength = tlvInputStream.readLength();//93
                    tlLength = prefix.Length - baInputStream.Available();//2
                                                                         //Corresponde al largo del archivo.
                    len = vLength + tlLength;
                    //Almacenamos el total de bytes recuperados de los APDU
                    byte[] bytes = new byte[len];
                    Array.Copy(prefix, bytes, prefix.Length);

                    cAPDU = new CommandAPDU("00B00008D8");
                    rAPDU = new ResponseAPDU();
                    rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
                    byte[] res = null;
                    if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                    {
                        res = ((ResponseAPDU)rAPDU).udr;
                        Array.Copy(res, 0, bytes, 8, res.Length);
                    }

                    //00 B0 00 E0 D8

                    //Recorremos.
                    int offset = 224;
                    byte off1;
                    byte off2;
                    int l1;
                    String hex = "";
                    int ne = 216;
                    int def = 0;
                    while (offset < len)
                    {

                        off1 = (byte)((offset & 0x0000FF00) >> 8);
                        off2 = (byte)(offset & 0x000000FF);
                        l1 = ne != 256 ? ne : 0;

                        CommandAPDU apdu = new CommandAPDU();
                        apdu.cla = 0;
                        apdu.ins = 176;
                        apdu.p1 = off1;
                        apdu.p2 = off2;
                        apdu.le = (uint)l1;
                        rAPDU = new ResponseAPDU();
                        rAPDU = transmit(cardChannel, objSecureMessagingWrapper, apdu);
                        res = null;
                        if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                        {
                            res = ((ResponseAPDU)rAPDU).udr;
                            Array.Copy(res, 0, bytes, offset, res.Length);
                        }

                        def = len - offset;
                        if (def > ne)
                            offset = offset + ne;
                        else
                        {
                            apdu = new CommandAPDU();
                            apdu.cla = 0;
                            apdu.ins = 176;
                            apdu.p1 = off1;
                            apdu.p2 = off2;
                            apdu.le = (uint)def;
                            rAPDU = new ResponseAPDU();
                            rAPDU = transmit(cardChannel, objSecureMessagingWrapper, apdu);
                            res = null;
                            if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                            {
                                res = ((ResponseAPDU)rAPDU).udr;
                                Array.Copy(res, 0, bytes, offset, res.Length);
                            }

                            break;
                        }
                    }

                    //Buscamos la foto.
                    int loc = ArrayUtils.Locate(bytes, jpg2k_magic_number)[0];
                    foto = bytes.SubArray(loc, bytes.Length - loc);           
                }

            }
            else
            {
                logger.Error("Error - rAPDU != 0x90 // " , rAPDU);
                
            }
            return foto;
        }
        #endregion APDU Sector 2
        #region APDU Sector 7
        public byte[] GET_CLRFIRMA()
        {
            cAPDU = new CommandAPDU("00A4020C020107");
            byte[] foto = null;
            largo = 0;
            rAPDU = new ResponseAPDU();
            rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
            if (((ResponseAPDU)rAPDU).sw1 == 0x90)
            {
                cAPDU = new CommandAPDU("00B0000008");
                rAPDU = new ResponseAPDU();
                rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
                if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                {
                    prefix = ((ResponseAPDU)rAPDU).udr;
                    baInputStream = new ByteArrayInputStream(prefix);
                    tlvInputStream = new TLVInputStream(baInputStream);
                    tag = tlvInputStream.readTag();//97
                    vLength = tlvInputStream.readLength();//93
                    tlLength = prefix.Length - baInputStream.Available();//2
                                                                         //Corresponde al largo del archivo.
                    len = vLength + tlLength;
                    //Almacenamos el total de bytes recuperados de los APDU
                    byte[] bytes = new byte[len];
                    Array.Copy(prefix, bytes, prefix.Length);

                    cAPDU = new CommandAPDU("00B00008D8");
                    rAPDU = new ResponseAPDU();
                    rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
                    byte[] res = null;
                    if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                    {
                        res = ((ResponseAPDU)rAPDU).udr;
                        Array.Copy(res, 0, bytes, 8, res.Length);
                    }

                    //00 B0 00 E0 D8

                    //Recorremos.
                    int offset = 224;
                    byte off1;
                    byte off2;
                    int l1;
                    String hex = "";
                    int ne = 216;
                    int def = 0;
                    while (offset < len)
                    {

                        off1 = (byte)((offset & 0x0000FF00) >> 8);
                        off2 = (byte)(offset & 0x000000FF);
                        l1 = ne != 256 ? ne : 0;

                        CommandAPDU apdu = new CommandAPDU();
                        apdu.cla = 0;
                        apdu.ins = 176;
                        apdu.p1 = off1;
                        apdu.p2 = off2;
                        apdu.le = (uint)l1;
                        rAPDU = new ResponseAPDU();
                        rAPDU = transmit(cardChannel, objSecureMessagingWrapper, apdu);
                        res = null;
                        if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                        {
                            res = ((ResponseAPDU)rAPDU).udr;
                            Array.Copy(res, 0, bytes, offset, res.Length);
                        }

                        def = len - offset;
                        if (def > ne)
                            offset = offset + ne;
                        else
                        {
                            apdu = new CommandAPDU();
                            apdu.cla = 0;
                            apdu.ins = 176;
                            apdu.p1 = off1;
                            apdu.p2 = off2;
                            apdu.le = (uint)def;
                            rAPDU = new ResponseAPDU();
                            rAPDU = transmit(cardChannel, objSecureMessagingWrapper, apdu);
                            res = null;
                            if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                            {
                                res = ((ResponseAPDU)rAPDU).udr;
                                Array.Copy(res, 0, bytes, offset, res.Length);
                            }

                            break;
                        }
                    }

                    //Buscamos la foto.
                    int loc = ArrayUtils.Locate(bytes, jpg2k_magic_number)[0];                    
                    foto = bytes.SubArray(loc, bytes.Length - loc);                
                }

            }
            return foto;
        }

        public byte[] CLR_GETFIRMA()
        {
            cAPDU = new CommandAPDU("00A4020C020107");
            byte[] foto = null;
            largo = 0;
            rAPDU = new ResponseAPDU();
            rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
            if (((ResponseAPDU)rAPDU).sw1 == 0x90)
            {
                cAPDU = new CommandAPDU("00B0000008");
                rAPDU = new ResponseAPDU();
                rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
                if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                {
                    prefix = ((ResponseAPDU)rAPDU).udr;
                    baInputStream = new ByteArrayInputStream(prefix);
                    tlvInputStream = new TLVInputStream(baInputStream);
                    tag = tlvInputStream.readTag();//97
                    vLength = tlvInputStream.readLength();//93
                    tlLength = prefix.Length - baInputStream.Available();//2
                                                                         //Corresponde al largo del archivo.
                    len = vLength + tlLength;
                    //Almacenamos el total de bytes recuperados de los APDU
                    byte[] bytes = new byte[len];
                    Array.Copy(prefix, bytes, prefix.Length);

                    cAPDU = new CommandAPDU("00B00008D8");
                    rAPDU = new ResponseAPDU();
                    rAPDU = transmit(cardChannel, objSecureMessagingWrapper, cAPDU);
                    byte[] res = null;
                    if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                    {
                        res = ((ResponseAPDU)rAPDU).udr;
                        Array.Copy(res, 0, bytes, 8, res.Length);
                    }

                    //00 B0 00 E0 D8

                    //Recorremos.
                    int offset = 224;
                    byte off1;
                    byte off2;
                    int l1;
                    String hex = "";
                    int ne = 216;
                    int def = 0;
                    while (offset < len)
                    {

                        off1 = (byte)((offset & 0x0000FF00) >> 8);
                        off2 = (byte)(offset & 0x000000FF);
                        l1 = ne != 256 ? ne : 0;

                        CommandAPDU apdu = new CommandAPDU();
                        apdu.cla = 0;
                        apdu.ins = 176;
                        apdu.p1 = off1;
                        apdu.p2 = off2;
                        apdu.le = (uint)l1;
                        rAPDU = new ResponseAPDU();
                        rAPDU = transmit(cardChannel, objSecureMessagingWrapper, apdu);
                        res = null;
                        if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                        {
                            res = ((ResponseAPDU)rAPDU).udr;
                            Array.Copy(res, 0, bytes, offset, res.Length);
                        }

                        def = len - offset;
                        if (def > ne)
                            offset = offset + ne;
                        else
                        {
                            apdu = new CommandAPDU();
                            apdu.cla = 0;
                            apdu.ins = 176;
                            apdu.p1 = off1;
                            apdu.p2 = off2;
                            apdu.le = (uint)def;
                            rAPDU = new ResponseAPDU();
                            rAPDU = transmit(cardChannel, objSecureMessagingWrapper, apdu);
                            res = null;
                            if (((ResponseAPDU)rAPDU).sw1 == 0x90)
                            {
                                res = ((ResponseAPDU)rAPDU).udr;
                                Array.Copy(res, 0, bytes, offset, res.Length);
                            }

                            break;
                        }
                    }

                    //Buscamos la foto.
                    int loc = ArrayUtils.Locate(bytes, jpg2k_magic_number)[0];
                    foto = bytes.SubArray(loc, bytes.Length - loc);
                }

            }
            return foto;
        }
        #endregion  APDU Sector 7     


        /// <summary>
        /// Se define el nombre del contactless utilizado por la aplicación.
        /// </summary>
        public String ReaderName
        {
            get { return readername; }
            set
            {
                readername = value;
                logger.Info("CLR_SET_READERNAME: Se define nombre del readername");
            }
        }

        #region ValoresMRZ
        /// <summary>
        ///  Permite recuperar el número de documento de la cédula desde el MRZ
        /// </summary>
        public String DocumentNumber
        {
            get
            {
                if (String.IsNullOrEmpty(Mrz))
                {
                    logger.Info("CLR_GET_DocumentNumber: No se ha inicializado la lectura de cédula, no se puede recuperar el nùmero de documento");
                    documentnumber = "N/I";
                }
                else
                {
                    documentnumber = Mrz.Substring(5, 9); ;
                }
                return documentnumber;
            }
        }
        /// <summary>
        /// Permite recuperar el país de emisión de la cédula desde el MRZ.
        /// </summary>
        public String Country
        {
            get
            {
                if (String.IsNullOrEmpty(Mrz))
                {
                    logger.Info("CLR_GET_Country: No se ha inicializado la lectura de cédula, no se puede recuperar el país");
                    country = "N/I";
                }
                else
                {
                    country = Mrz.Substring(2, 3);
                }
                return country;
            }
        }
        /// <summary>
        /// Permite recuperar la fecha de nacimiento de la persona desde el MRZ.
        /// </summary>
        public String DateOfBirth
        {
            get
            {
                if (String.IsNullOrEmpty(Mrz))
                {
                    logger.Info("CLR_GET_Country: No se ha inicializado la lectura de cédula, no se puede recuperar la fecha de nacimiento");
                    dateofbirth = "N/I";
                }
                else
                {
                    dateofbirth = Mrz.Substring(30, 6);
                }
                return dateofbirth;
            }
        }
        /// <summary>
        /// Permite recuperar el sexo de la persona, F o M, desde el MRZ
        /// </summary>
        public String Sex
        {
            get
            {
                if (String.IsNullOrEmpty(Mrz))
                {
                    logger.Info("CLR_GET_Country: No se ha inicializado la lectura de cédula, no se puede recuperar el sexo de la persona");
                    sex = "N/I";
                }
                else
                {
                    sex = Mrz.Substring(37, 1);
                }
                return sex;
            }
        }
        /// <summary>
        /// Permite recuperar la fecha de expiración de la cédula desde el MRZ
        /// </summary>
        public String DateOfExpiry
        {
            get
            {
                if (String.IsNullOrEmpty(Mrz))
                {
                    logger.Info("CLR_GET_Country: No se ha inicializado la lectura de cédula, no se puede recuperar la fecha de vencimiento");
                    dateofexpiry = "N/I";
                }
                else
                {
                    dateofexpiry = Mrz.Substring(38, 6);
                }
                return dateofexpiry;
            }
        }
        /// <summary>
        /// Permite recuperar la nacionalidad de la persona desde el MRZ.
        /// </summary>
        public String Nationality
        {
            get
            {
                if (String.IsNullOrEmpty(Mrz))
                {
                    logger.Info("CLR_GET_Country: No se ha inicializado la lectura de cédula, no se puede recuperar la nacionalidad");
                    nacionality = "N/I";
                }
                else
                {
                    nacionality = Mrz.Substring(45, 3);
                }
                return nacionality;
            }
        }
        /// <summary>
        /// Permite recuperar el apellido paterno de la persona desde el MRZ
        /// </summary>
        public String PatherLastName
        {
            get
            {
                if (String.IsNullOrEmpty(Mrz))
                {
                    logger.Info("CLR_GET_Country: No se ha inicializado la lectura de cédula, no se puede recuperar el apellido paterno");
                    patherlastmame = "N/I";
                }
                else
                {
                    int separator = lastrow.IndexOf("<<");
                    patherlastmame = lastrow.Substring(0, separator).Split('<')[0];
                }
                return patherlastmame.Trim();
            }
        }
        /// <summary>
        /// Permite recuperar el apellido materno de la persona desde el MRZ
        /// </summary>
        public String MotherLastName
        {
            get
            {
                if (String.IsNullOrEmpty(Mrz))
                {
                    logger.Info("CLR_GET_Country: No se ha inicializado la lectura de cédula, no se puede recuperar el apellido materno");
                    motherlastmame = "N/I";
                }
                else
                {
                    int separator = lastrow.IndexOf("<<");
                    if (lastrow.Substring(0, separator).Contains('<'))
                    {
                        motherlastmame = lastrow.Substring(0, separator).Split('<')[1];
                    }
                    else
                    {
                        logger.Info("No hay un apellido materno presente");
                        motherlastmame = String.Empty;
                    }
                }
                return motherlastmame.Trim();
            }
        }
        /// <summary>
        /// Permite recuperar el nombre extraído del MRZ.
        /// </summary>
        public String Name
        {
            get
            {
                if (String.IsNullOrEmpty(Mrz))
                {
                    logger.Info("CLR_GET_Country: No se ha inicializado la lectura de cédula, no se puede recuperar los nombres");
                    name = "N/I";
                }
                else
                {
                    int separator = lastrow.IndexOf("<<");
                    name = lastrow.Substring(separator + 1).Replace('<', ' ');

                }
                return name.Trim();
            }
        }
        public String DNI
        {
            get
            {
                if (String.IsNullOrEmpty(Mrz))
                {
                    logger.Info("CLR_GET_Country: No se ha inicializado la lectura de cédula, no se puede recuperar el rut");
                    name = "N/I";
                }
                else
                {
                    dni = Mrz.Substring(48, 11).Split('<')[0] + "-" + Mrz.Substring(48, 11).Split('<')[1]; ;
                }
                return dni;
            }
        }
        #endregion  ValoresMRZ

      

       
    }
    static class ArrayUtils
    {
        //Esegue lo slice di un array
        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        //array vuoto
        public static readonly int[] Empty = new int[0];

        //Localizza le occorenze di "candidate" in "self"
        public static int[] Locate(this byte[] self, byte[] candidate)
        {
            if (IsEmptyLocate(self, candidate))
                return Empty;

            var list = new List<int>();

            for (int i = 0; i < self.Length; i++)
            {
                if (!IsMatch(self, i, candidate))
                    continue;

                list.Add(i);
            }

            return list.Count == 0 ? Empty : list.ToArray();
        }

        /* Verifica il mach tra array e candidate nella posizione array[position] */
        static bool IsMatch(byte[] array, int position, byte[] candidate)
        {
            if (candidate.Length > (array.Length - position))
                return false;

            for (int i = 0; i < candidate.Length; i++)
                if (array[position + i] != candidate[i])
                    return false;

            return true;
        }

        /* Controllo, se è vero allora sicuramente non ci sono occorrenze */
        static bool IsEmptyLocate(byte[] array, byte[] candidate)
        {
            return array == null
                || candidate == null
                || array.Length == 0
                || candidate.Length == 0
                || candidate.Length > array.Length;
        }
    }


}
