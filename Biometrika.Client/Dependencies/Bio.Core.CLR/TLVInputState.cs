﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSCT.Core.ConsoleTests
{
    class TLVInputState: ICloneable
    {
        private Stack<TLStruct> state;
        private bool _isAtStartOfTag, _isAtStartOfLength, _isReadingValue;

        public TLVInputState()
        {
            state = new Stack<TLStruct>();
            _isAtStartOfTag = true;
            _isAtStartOfLength = false;
            _isReadingValue = false;
        }

        private TLVInputState(Stack<TLStruct> state, bool isAtStartOfTag, bool isAtStartOfLength, bool isReadingValue)
        {
            this.state = state;
            this._isAtStartOfTag = isAtStartOfTag;
            this._isAtStartOfLength = isAtStartOfLength;
            this._isReadingValue = isReadingValue;
        }
        public bool isAtStartOfTag
        {
            get{return _isAtStartOfTag;}
        }

        public bool isAtStartOfLength
        {
            get { return _isAtStartOfLength; }
        }

        public bool isProcessingValue
        {
            get { return _isReadingValue; }
        }

        public int getTag()
        {
            
            if (state==null)
            {
                
            }
            TLStruct currentObject = state.Peek();
            return currentObject.getTag();
        }

        public int getLength()
        {
            if (state==null)
            {
                //throw new IllegalStateException("Length not yet known.");
            }
            TLStruct currentObject = state.Peek();
            int length = currentObject.getLength();
            return length;
        }

        public int getValueBytesProcessed()
        {
            TLStruct currentObject = state.Peek();
            return currentObject.getValueBytesProcessed();
        }

        public int getValueBytesLeft()
        {
            if (state==null)
            {
                //throw new IllegalStateException("Length of value is unknown.");
            }
            TLStruct currentObject = state.Peek();
            int currentLength = currentObject.getLength();
            int valueBytesRead = currentObject.getValueBytesProcessed();
            return currentLength - valueBytesRead;
        }

        public void setTagProcessed(int tag, int byteCount)
        {
            /* Length is set to MAX INT, we will update it when caller calls our setLengthProcessed. */
            TLStruct obj = new TLStruct(tag);
            if (state.Count>0)
            {
                TLStruct parent = state.Peek();
                parent.updateValueBytesProcessed(byteCount);
            }
            state.Push(obj);
            _isAtStartOfTag = false;
            _isAtStartOfLength = true;
            _isReadingValue = false;
        }

        public void setDummyLengthProcessed()
        {
            _isAtStartOfTag = false;
            _isAtStartOfLength = false;
            _isReadingValue = true;
        }

        public void setLengthProcessed(int length, int byteCount)
        {
            if (length < 0)
            {
                //throw new IllegalArgumentException("Cannot set negative length (length = " + length + ", 0x" + Integer.toHexString(length) + " for tag " + Integer.toHexString(getTag()) + ").");
            }
            TLStruct obj = state.Pop();
            if (state.Count>0)
            {
                TLStruct parent = state.Peek();
                parent.updateValueBytesProcessed(byteCount);
            }
            obj.setLength(length);
            state.Push(obj);
            _isAtStartOfTag = false;
            _isAtStartOfLength = false;
            _isReadingValue = true;
        }

        public void updateValueBytesProcessed(int byteCount)
        {
            if (state==null) { return; }
            TLStruct currentObject = state.Peek();
            int bytesLeft = currentObject.getLength() - currentObject.getValueBytesProcessed();
            if (byteCount > bytesLeft)
            {
                //throw new IllegalArgumentException("Cannot process " + byteCount + " bytes! Only " + bytesLeft + " bytes left in this TLV object " + currentObject);
            }
            currentObject.updateValueBytesProcessed(byteCount);
            int currentLength = currentObject.getLength();
            if (currentObject.getValueBytesProcessed() == currentLength)
            {
                state.Pop();
                /* Stand back! I'm going to try recursion! Update parent(s)... */
                updateValueBytesProcessed(currentLength);
                _isAtStartOfTag = true;
                _isAtStartOfLength = false;
                _isReadingValue = false;
            }
            else
            {
                _isAtStartOfTag = false;
                _isAtStartOfLength = false;
                _isReadingValue = true;
            }
        }






        #region ICloneable Members

        public object Clone()
        {
            Stack<TLStruct> newState = new Stack<TLStruct>();
            foreach (TLStruct tls in state)
	        {
                TLStruct tlStruct = tls;
                newState.Push((TLStruct)tlStruct.Clone());
	        }
           
            return new TLVInputState(newState, _isAtStartOfTag, _isAtStartOfLength, _isReadingValue);
        }

        #endregion
    }
}
