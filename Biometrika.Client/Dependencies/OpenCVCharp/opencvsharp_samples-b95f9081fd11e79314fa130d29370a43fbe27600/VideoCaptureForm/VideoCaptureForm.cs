﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace VideoCaptureForm
{
    public partial class VideoCaptureForm : Form
    {
        private VideoCapture capture;
        private CascadeClassifier cascadeClassifier;

        public VideoCaptureForm()
        {
            InitializeComponent();
            
           

            
        }

        bool _RUNNING = false;

        private void VideoCaptureForm_Load(object sender, EventArgs e)
        {
            
           
            //Application.Idle += new EventHandler(FrameGrabber);
            //backgroundWorker1.RunWorkerAsync();
        }

        private void FrameGrabber(object sender, EventArgs e)
        {
            try
            {
               
                using (var frameMat = capture.RetrieveMat())
                {
                    var rects = cascadeClassifier.DetectMultiScale(frameMat, 1.1, 5, 
                                        HaarDetectionType.ScaleImage, new OpenCvSharp.Size(30, 30));
                    if (rects.Length > 0)
                    {
                        Cv2.Rectangle(frameMat, rects[0], Scalar.Red);
                    }

                    var frameBitmap = BitmapConverter.ToBitmap(frameMat);
                    //bgWorker.ReportProgress(0, frameBitmap);
                }
                Thread.Sleep(100);
            }
            catch (Exception ex)
            {
            }
        }

        private void VideoCaptureForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (capture != null)
            {
                capture.Dispose();
                cascadeClassifier.Dispose();
            }
            //backgroundWorker1.CancelAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            var bgWorker = (BackgroundWorker) sender;

            while (!bgWorker.CancellationPending)
            {
                using (var frameMat = capture.RetrieveMat())
                {
                    var rects = cascadeClassifier.DetectMultiScale(frameMat, 1.1, 5, HaarDetectionType.ScaleImage, new OpenCvSharp.Size(30, 30));
                    if (rects.Length > 0)
                    {
                        Cv2.Rectangle(frameMat, rects[0], Scalar.Red);
                    }

                    var frameBitmap = BitmapConverter.ToBitmap(frameMat);
                    bgWorker.ReportProgress(0, frameBitmap);
                }
                Thread.Sleep(100);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var frameBitmap = (Bitmap)e.UserState;
            pictureBox1.Image?.Dispose();
            pictureBox1.Image = frameBitmap;
        }

        private void button2_Click(object sender, EventArgs e)
        {

            VideoCapture capture = new VideoCapture(0);
            cascadeClassifier = new CascadeClassifier("haarcascade_frontalface_default.xml");
            _RUNNING = true;
            //using (Window window = new Window("Camera"))
            using (Mat image = new Mat()) // Frame image buffer
            {
                // When the movie playback reaches end, Mat.data becomes NULL.
                while (_RUNNING)
                {
                    capture.Read(image); // same as cvQueryFrame
                    if (image.Empty()) break;
                    pictureBox1.Image = image.ToBitmap();
                    var rects = cascadeClassifier.DetectMultiScale(image, 1.1, 5, HaarDetectionType.ScaleImage, new OpenCvSharp.Size(30, 30));
                    if (rects.Length > 0)
                    {
                        Cv2.Rectangle(image, rects[0], Scalar.Red);
                    }
                    //window.ShowImage(image);
                    Cv2.WaitKey(30);
                }
            }
            //capture = new VideoCapture(0);
            //cascadeClassifier = new CascadeClassifier("haarcascade_frontalface_default.xml");
            //capture.Open(0, VideoCaptureAPIs.ANY);
            //if (!capture.IsOpened())
            //{
            //    Close();
            //    return;
            //}

            ////ClientSize = new System.Drawing.Size(capture.FrameWidth, capture.FrameHeight);
            //_RUNNING = true;
            ////using (var window = new Window("capture"))
            ////{
            //    // Frame image buffer
            //    Mat image = new Mat();

            //    // When the movie playback reaches end, Mat.data becomes NULL.
            //    while (_RUNNING)
            //    {
            //        capture.Read(image); // same as cvQueryFrame
            //        if (image.Empty())
            //            break;
            //        pictureBox1.Image = image.ToBitmap();
            //        //window.ShowImage(image);
            //        //Cv2.WaitKey(sleepTime);
            //    }
            ////}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _RUNNING = false;
            //if (capture!=null && capture.IsOpened())
            //{
            //    capture.Dispose();
            //    cascadeClassifier.Dispose();
            //}
        }
    }
}
