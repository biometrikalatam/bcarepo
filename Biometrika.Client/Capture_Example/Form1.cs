﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Leadtools.Multimedia;
using System.IO;
using LMVTextOverlayLib;
using LMVIOvLyLib;

namespace Capture_Example
{
   public partial class Form1 : Form
   {
      public MemoryStream targetstream;      
      public Form1()
      {
         InitializeComponent();
            captureCtrl1.VideoDevices[0].Selected = true;
            captureCtrl1.AudioDevices[0].Selected = true;
            //captureCtrl1.Dock = DockStyle.Fill;
            captureCtrl1.Preview = true;
        }

      private void btnRec_Click(object sender, EventArgs e)
      {
            captureCtrl1.Preview = true;
            captureCtrl1.StartCapture(CaptureMode.VideoAndAudio);
            captureCtrl1.EditGraph();
        }      

      void captureCtrl1_Progress(object sender, ProgressEventArgs e)
      {
         this.Text = e.time.ToString();         
      }

      private void btnStop_Click(object sender, EventArgs e)
      {
         try
         {
            captureCtrl1.StopCapture();
            System.IO.File.WriteAllBytes(@".\result.mp4", targetstream.ToArray());
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message);
         }

      }

      private void btnTxtOverLay_Click(object sender, EventArgs e)
      {
            captureCtrl1.Preview = false;

            //Adding Image OverLay
            //captureCtrl1.SelectedVideoProcessors.Add(captureCtrl1.VideoProcessors.ImageOverlay);
            //// get the the image overlay filter object
            //LMVIOvLy _ImageOverLay = (LMVIOvLy)captureCtrl1.GetSubObject(CaptureObject.SelVideoProcessor);
            //_ImageOverLay.OverlayImageFileName = @".\disclaimer_big.bmp";
            //_ImageOverLay.set_XPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);
            //_ImageOverLay.set_YPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);

            ////Adding Text OverLay
            //captureCtrl1.SelectedVideoProcessors.Add(captureCtrl1.VideoProcessors.TextOverlay);

            //// get the the text overlay filter object
            //LMVTextOverlay _overlay = (LMVTextOverlay)captureCtrl1.GetSubObject(CaptureObject.SelVideoProcessor + 1);
            //_overlay.OverlayText = "Mundo Pacifico";
            //_overlay.ViewRectLeft = 0;
            //_overlay.ViewRectTop = 0;
            //_overlay.ViewRectRight = 800;
            //_overlay.ViewRectBottom = 600;
            //_overlay.EnableXYPosition = true;
            //_overlay.XPos = 80;
            //_overlay.YPos = 130;
            //_overlay.FontSize = 12;
            //_overlay.Bold = true;
            //_overlay.FontColor = 0;
            //_overlay.AutoReposToViewRect = true;

            //captureCtrl1.SelectedVideoProcessors.Add(captureCtrl1.VideoProcessors.TextOverlay);
            //LMVTextOverlay _overlay1 = (LMVTextOverlay)captureCtrl1.GetSubObject(CaptureObject.SelVideoProcessor + 2);
            //_overlay1.OverlayText = "RUT 21284415-2";
            //_overlay1.ViewRectLeft = 0;
            //_overlay1.ViewRectTop = 0;
            //_overlay1.ViewRectRight = 300;
            //_overlay1.ViewRectBottom = 600;
            //_overlay.WordWrap = true;
            //_overlay1.EnableXYPosition = true;
            //_overlay1.XPos = 80;
            //_overlay1.YPos = 170;
            //_overlay1.FontSize = 10;
            ////_overlay1.FontColor = 16777215;
            //_overlay1.AutoReposToViewRect = true;

            //captureCtrl1.SelectedVideoProcessors.Add(captureCtrl1.VideoProcessors.TextOverlay);
            //LMVTextOverlay _overlay2 = (LMVTextOverlay)captureCtrl1.GetSubObject(CaptureObject.SelVideoProcessor + 3);
            //_overlay2.OverlayText = "123456NN";
            //_overlay2.ViewRectLeft = 0;
            //_overlay2.ViewRectTop = 0;
            //_overlay2.ViewRectRight = 800;
            //_overlay2.ViewRectBottom = 600;
            //_overlay2.EnableXYPosition = true;
            //_overlay2.XPos = 80;
            //_overlay2.YPos = 210;
            //_overlay2.FontSize = 10;
            ////_overlay1.FontColor = 16777215;
            //_overlay2.AutoReposToViewRect = true;




            targetstream = new MemoryStream();
            captureCtrl1.PreferredVideoRenderer = VideoRendererType.VMR7;
            captureCtrl1.PreviewTap = CapturePreviewTap.Compressor;
            captureCtrl1.TargetFormat = Leadtools.Multimedia.TargetFormatType.AVI; //.ISO;
            //captureCtrl1.VideoCompressors.H264.Selected = true;
            //captureCtrl1.AudioCompressors.AAC.Selected = true;
            captureCtrl1.TargetType = Leadtools.Multimedia.TargetObjectType.Stream;
            captureCtrl1.TargetStream = targetstream;
            captureCtrl1.Progress += new ProgressEventHandler(captureCtrl1_Progress);
            captureCtrl1.Preview = true;

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
