﻿// *************************************************************
// Copyright (c) 1991-2018 LEAD Technologies, Inc.              
// All Rights Reserved.                                         
// *************************************************************
using System;
/// <summary>
/// Summary description for UnlockSupport
/// </summary>
public class UnlockSupport
{
    private const string SZ_DEFAULTAPPID = "LEADTOOLS Multimedia Application";
    private const string LTMM_SERIAL_NUMBER = "722010-1580056-000"; //"YourKey";
    private const string CODEC_UNLOCK_KEY = "221400-4900281-000"; //"YourKey";

    public UnlockSupport()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void Unlock(string pszAppID)
    {
        if ((pszAppID == null) || (pszAppID.Length == 0))
            pszAppID = SZ_DEFAULTAPPID;

        // Unlock the main SDK for this application ONLY
        if (LTMM_SERIAL_NUMBER != "YourKey")
        {
            Leadtools.Multimedia.Common.MultimediaSupport.UnlockModule(LTMM_SERIAL_NUMBER, Leadtools.Multimedia.Common.LockType.Application, pszAppID);
            Leadtools.Multimedia.Common.MultimediaSupport.UnlockModule(CODEC_UNLOCK_KEY, Leadtools.Multimedia.Common.LockType.Application, pszAppID);
        }

        // Optional: Unlock additional filters for any application on the machine
        // Call method once for each additional unlock key obtained from LEADTOOLS
        // Leadtools.Multimedia.Common.MultimediaSupport.UnlockModule(CODEC_UNLOCK_KEY, Leadtools.Multimedia.Common.LockType.Computer, pszAppID);
        // Leadtools.Multimedia.Common.MultimediaSupport.UnlockModule(MODULE_UNLOCK_KEY, Leadtools.Multimedia.Common.LockType.Computer, pszAppID);
    }

    public void Lock(string pszAppID)
    {
        if ((pszAppID == null) || (pszAppID.Length == 0))
            pszAppID = SZ_DEFAULTAPPID;

        try
        {
            Leadtools.Multimedia.Common.MultimediaSupport.LockModules(Leadtools.Multimedia.Common.LockType.Application, pszAppID);
        }
        catch (Exception)
        {
        }
    }
}