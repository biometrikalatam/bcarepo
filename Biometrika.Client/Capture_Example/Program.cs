﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Capture_Example
{
   static class Program
   {
      /// <summary>
      /// The main entry point for the application.
      /// </summary>
      [STAThread]
      static void Main()
      {

             UnlockSupport unlocksprt = new UnlockSupport();
             unlocksprt.Unlock("722010-1580056-000");
             Application.EnableVisualStyles();
             Application.SetCompatibleTextRenderingDefault(false);
             Application.Run(new Form2());
            unlocksprt.Lock("722010-1580056-000");
        }
   }
}
