﻿namespace Capture_Example
{
   partial class Form1
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            this.btnRec = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnTxtOverLay = new System.Windows.Forms.Button();
            this.captureCtrl1 = new Leadtools.Multimedia.CaptureCtrl();
            ((System.ComponentModel.ISupportInitialize)(this.captureCtrl1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRec
            // 
            this.btnRec.Location = new System.Drawing.Point(403, 16);
            this.btnRec.Name = "btnRec";
            this.btnRec.Size = new System.Drawing.Size(175, 30);
            this.btnRec.TabIndex = 2;
            this.btnRec.Text = "Record";
            this.btnRec.UseVisualStyleBackColor = true;
            this.btnRec.Click += new System.EventHandler(this.btnRec_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.SystemColors.Control;
            this.btnStop.Location = new System.Drawing.Point(403, 52);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(175, 25);
            this.btnStop.TabIndex = 3;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnTxtOverLay
            // 
            this.btnTxtOverLay.Location = new System.Drawing.Point(403, 83);
            this.btnTxtOverLay.Name = "btnTxtOverLay";
            this.btnTxtOverLay.Size = new System.Drawing.Size(175, 23);
            this.btnTxtOverLay.TabIndex = 4;
            this.btnTxtOverLay.Text = "Add Text And Image OverLay";
            this.btnTxtOverLay.UseVisualStyleBackColor = true;
            this.btnTxtOverLay.Click += new System.EventHandler(this.btnTxtOverLay_Click);
            // 
            // captureCtrl1
            // 
            this.captureCtrl1.AudioCompressors.Selection = -1;
            this.captureCtrl1.AudioDevices.Selection = -1;
            this.captureCtrl1.Location = new System.Drawing.Point(23, 16);
            this.captureCtrl1.Name = "captureCtrl1";
            this.captureCtrl1.OcxState = null;
            this.captureCtrl1.Size = new System.Drawing.Size(374, 234);
            this.captureCtrl1.TabIndex = 5;
            this.captureCtrl1.TargetDevices.Selection = -1;
            this.captureCtrl1.TargetFile = "C:\\Users\\gsuhi\\Documents\\capture.avi";
            this.captureCtrl1.Text = "captureCtrl1";
            this.captureCtrl1.VideoCompressors.Selection = -1;
            this.captureCtrl1.VideoDevices.Selection = -1;
            this.captureCtrl1.WMProfile.Description = "";
            this.captureCtrl1.WMProfile.Name = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 384);
            this.Controls.Add(this.captureCtrl1);
            this.Controls.Add(this.btnTxtOverLay);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnRec);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.captureCtrl1)).EndInit();
            this.ResumeLayout(false);

      }

      #endregion

      //private Leadtools.Multimedia.CaptureCtrl captureCtrl1;
      private System.Windows.Forms.Button btnRec;
      private System.Windows.Forms.Button btnStop;
      private System.Windows.Forms.Button btnTxtOverLay;
        private Leadtools.Multimedia.CaptureCtrl captureCtrl1;
    }
}

