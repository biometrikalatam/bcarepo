﻿using Domain;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Script.Serialization;

namespace Manager
{
    public class CameraManager : IManager
    {
        private bool isOpen = false;
        private Dictionary<string, object> output = new Dictionary<string, object>();

        public CameraManager()
        {
        }  // ctor para el relexion

        public CameraManager(NameValueCollection queryString)
        {
            if (!isOpen)
            {
                isOpen = true;
                CameraUIAdapter.Program.SetPacket(queryString);     // el program recibe el pedido
                CameraUIAdapter.Program.Main();
               
                output = CameraUIAdapter.Program.Packet.PacketParamOut;
               
            }
        }

        public string Buffer => new JavaScriptSerializer().Serialize(output);

        public string Tag { get => "CAM"; }
    }
}