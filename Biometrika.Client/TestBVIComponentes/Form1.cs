﻿using CSJ2K;
using CSJ2K.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace TestBVIComponentes
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        public static Bitmap Base64ToBitmap(string base64Bmp)
        {
            var buffer = Convert.FromBase64String(base64Bmp);
            var stream = new MemoryStream(buffer);
            stream.Position = 0;

            return (Bitmap)Image.FromStream(stream);
        }
        public Image Base64ToImage(string base64String)
        {
            // Convert base 64 string to byte[] 
            byte[] imageBytes = Convert.FromBase64String(base64String);
            // Convert byte[] to Image 
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            { Image image = Image.FromStream(ms, true); return image; }
        }

        private void btnObtener_Click(object sender, EventArgs e)
        {
            if ((txtNroDocumento.Text.Trim().Length != 0)
                || (txtFecNacimiento.Text.Trim().Length != 0)
                || (txtFecVen.Text.Trim().Length != 0)
                )
            {
                var clr = new Bio.Core.CLR.CLR();
                clr.ReaderName = comboBox1.Text;
                clr.CLR_OPEN(10);
                Thread.Sleep(100);
                int finger = 0;
                string[] isocompact = txtIsoCompact.Text.Split('-');
               

                    clr.CLR_CONNECT(Convert.ToUInt32(5000));

                if (clr.GetLasError() == 0)
                {
                    clr.CLR_SECURITY(
                 txtNroDocumento.Text,
                 txtFecNacimiento.Text,
                 txtFecVen.Text);

                  

                    clr.CLR_GETMRZ();
                    lblRut.Text = clr.DNI;
                    lblNombre.Text = clr.Name;
                    lblApellidoPaterno.Text = clr.PatherLastName;
                    lblApellidoMaterno.Text = clr.MotherLastName;
                    Thread.Sleep(100);
                    //Match on Card
                    bool match = clr.Match(isocompact, out finger);
                    if (match)
                    {
                        lblResultado.Text = "Verificación Positiva";
                        lblResultado.ForeColor = Color.Green;
                    }
                    else
                    {
                        lblResultado.Text = "Verificación Negativa";
                        lblResultado.ForeColor = Color.Red;
                    }

                    //Fin de MatchonCard
                    Thread.Sleep(500);
                    if (chkFoto.Checked==true)
                    {
                        clr.CLR_SECURITY(
                            txtNroDocumento.Text,
                            txtFecNacimiento.Text,
                            txtFecVen.Text);
                        if (clr.GetLasError() == 0)
                        {
                            var jpg = clr.CLR_GETFOTO();

                            BitmapImageCreator.Register();
                            var por = J2kImage.FromBytes(jpg);
                            //Bitmap img = por.As<Bitmap>();//ACA SE CAE
                            int sdf = 0;
                            Bitmap img = por.As<Bitmap>();

                            ImageFormat format = ImageFormat.Jpeg;

                            using (MemoryStream ms = new MemoryStream())
                            {
                                img.Save(ms, format);
                                picFoto.Image = Base64ToBitmap(Convert.ToBase64String(ms.ToArray()));
                            }
                        }
                    }
                    if (chkObtenerFirma.Checked == true)
                    {
                        clr.CLR_SECURITY(
                            txtNroDocumento.Text,
                            txtFecNacimiento.Text,
                            txtFecVen.Text);
                        if (clr.GetLasError() == 0)
                        {
                            var jpg = clr.GET_CLRFIRMA();

                            BitmapImageCreator.Register();
                            var por = J2kImage.FromBytes(jpg);
                            //Bitmap img = por.As<Bitmap>();//ACA SE CAE
                            int sdf = 0;
                            Bitmap img = por.As<Bitmap>();

                            ImageFormat format = ImageFormat.Jpeg;

                            using (MemoryStream ms = new MemoryStream())
                            {
                                img.Save(ms, format);
                                picFirma.Image = Base64ToBitmap(Convert.ToBase64String(ms.ToArray()));
                            }
                        }
                    }

                }
                else
                {
                    MessageBox.Show("No se pudo establecer comunicación con el lector");
                }
                clr.CLR_DISCONNECT();
                clr.CLR_CLOSE();
            }
            else
            {
                MessageBox.Show("Debe ingresar los valores de la cédula para iniciar proceso");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 1;
        }

        private void btnMatchEnCard_Click(object sender, EventArgs e)
        {
            if(txtIsoCompact.Text.Trim().Length!=0)
            {

            }
        }
    }
}
