﻿using Bio.Core.BCR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestBVIComponentes
{
    public partial class Form2 : Form
    {
        private BCR bcr;
        public Form2()
        {
            InitializeComponent();
        }

       
        private void Form2_Load(object sender, EventArgs e)
        {
            

        }

        private void initBCR()
        {
            bcr = new BCR(txtCom.Text);
            bcr.DataCaptured += ProcesarCedulaLeida;
            
        }
        /// <summary>
        /// Evento de captura de lo leído en el COM
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProcesarCedulaLeida(object sender, EventArgs e)
        {
            //Para efecto de este ejemplo se retorna la propiedad DocumentNumber,
            //pero existen otras propiedades, como:
            //Rut, pdf417, DOB(Fecha Nacimiento), DOE(Fecha Expiración), fullPdf417
            MessageBox.Show("DocumentNumber:" + bcr.DocumentNumber);

            richTextBox1.Text = bcr.FullPdf417;
        }

        private void btnInicializar_Click(object sender, EventArgs e)
        {
            if(txtCom.Text.Trim().Length!=0)
            {
                initBCR();
                MessageBox.Show("Puerto inicializado");
                btnClose.Enabled = true;
                btnOpen.Enabled = true;
            }
            else
            {
                MessageBox.Show("No se pudo configurar el serial");
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            var bcrOpened = bcr.Open();
            if(bcrOpened==true)
            {
                MessageBox.Show("Puerto abierto");
            }
            else
            {
                MessageBox.Show("No se pudo abrir");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            var bcrClosed=bcr.Close();
            if (bcrClosed == true)
                MessageBox.Show("Puerto cerrado");
            else
                MessageBox.Show("No se pudo cerrar el puerto");
        }
    }
}
