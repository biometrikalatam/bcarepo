﻿namespace TestBVIComponentes
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNroDocumento = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFecVen = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFecNacimiento = new System.Windows.Forms.TextBox();
            this.chkTraerDatos = new System.Windows.Forms.CheckBox();
            this.chkFoto = new System.Windows.Forms.CheckBox();
            this.chkObtenerFirma = new System.Windows.Forms.CheckBox();
            this.btnObtener = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.picFirma = new System.Windows.Forms.PictureBox();
            this.picFoto = new System.Windows.Forms.PictureBox();
            this.lblRut = new System.Windows.Forms.Label();
            this.lblSexo = new System.Windows.Forms.Label();
            this.lblApellidoMaterno = new System.Windows.Forms.Label();
            this.lblApellidoPaterno = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIsoCompact = new System.Windows.Forms.TextBox();
            this.lblResultado = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFirma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lector de NFC";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "OMNIKEY CardMan 5x21-CL 0",
            "HID Global OMNIKEY 5022 Smart Card Reader 0"});
            this.comboBox1.Location = new System.Drawing.Point(143, 20);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(365, 21);
            this.comboBox1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(28, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Número Documento";
            // 
            // txtNroDocumento
            // 
            this.txtNroDocumento.Location = new System.Drawing.Point(163, 65);
            this.txtNroDocumento.Name = "txtNroDocumento";
            this.txtNroDocumento.Size = new System.Drawing.Size(261, 20);
            this.txtNroDocumento.TabIndex = 3;
            this.txtNroDocumento.Text = "601014310";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(28, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Fecha Vencimiento";
            // 
            // txtFecVen
            // 
            this.txtFecVen.Location = new System.Drawing.Point(163, 103);
            this.txtFecVen.Name = "txtFecVen";
            this.txtFecVen.Size = new System.Drawing.Size(261, 20);
            this.txtFecVen.TabIndex = 5;
            this.txtFecVen.Text = "020822";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(28, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Fecha Nacimiento";
            // 
            // txtFecNacimiento
            // 
            this.txtFecNacimiento.Location = new System.Drawing.Point(163, 140);
            this.txtFecNacimiento.Name = "txtFecNacimiento";
            this.txtFecNacimiento.Size = new System.Drawing.Size(261, 20);
            this.txtFecNacimiento.TabIndex = 7;
            this.txtFecNacimiento.Text = "280969";
            // 
            // chkTraerDatos
            // 
            this.chkTraerDatos.AutoSize = true;
            this.chkTraerDatos.Checked = true;
            this.chkTraerDatos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTraerDatos.Enabled = false;
            this.chkTraerDatos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTraerDatos.Location = new System.Drawing.Point(31, 210);
            this.chkTraerDatos.Name = "chkTraerDatos";
            this.chkTraerDatos.Size = new System.Drawing.Size(99, 20);
            this.chkTraerDatos.TabIndex = 8;
            this.chkTraerDatos.Text = "Obtener Mrz";
            this.chkTraerDatos.UseVisualStyleBackColor = true;
            // 
            // chkFoto
            // 
            this.chkFoto.AutoSize = true;
            this.chkFoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFoto.Location = new System.Drawing.Point(143, 210);
            this.chkFoto.Name = "chkFoto";
            this.chkFoto.Size = new System.Drawing.Size(105, 20);
            this.chkFoto.TabIndex = 9;
            this.chkFoto.Text = "Obtener Foto";
            this.chkFoto.UseVisualStyleBackColor = true;
            // 
            // chkObtenerFirma
            // 
            this.chkObtenerFirma.AutoSize = true;
            this.chkObtenerFirma.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkObtenerFirma.Location = new System.Drawing.Point(252, 210);
            this.chkObtenerFirma.Name = "chkObtenerFirma";
            this.chkObtenerFirma.Size = new System.Drawing.Size(112, 20);
            this.chkObtenerFirma.TabIndex = 10;
            this.chkObtenerFirma.Text = "Obtener Firma";
            this.chkObtenerFirma.UseVisualStyleBackColor = true;
            // 
            // btnObtener
            // 
            this.btnObtener.Location = new System.Drawing.Point(101, 254);
            this.btnObtener.Name = "btnObtener";
            this.btnObtener.Size = new System.Drawing.Size(108, 35);
            this.btnObtener.TabIndex = 11;
            this.btnObtener.Text = "Obtener";
            this.btnObtener.UseVisualStyleBackColor = true;
            this.btnObtener.Click += new System.EventHandler(this.btnObtener_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.picFirma);
            this.groupBox1.Controls.Add(this.picFoto);
            this.groupBox1.Controls.Add(this.lblRut);
            this.groupBox1.Controls.Add(this.lblSexo);
            this.groupBox1.Controls.Add(this.lblApellidoMaterno);
            this.groupBox1.Controls.Add(this.lblApellidoPaterno);
            this.groupBox1.Controls.Add(this.lblNombre);
            this.groupBox1.Location = new System.Drawing.Point(31, 307);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(533, 306);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Personales";
            // 
            // picFirma
            // 
            this.picFirma.Location = new System.Drawing.Point(284, 238);
            this.picFirma.Name = "picFirma";
            this.picFirma.Size = new System.Drawing.Size(217, 50);
            this.picFirma.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFirma.TabIndex = 9;
            this.picFirma.TabStop = false;
            // 
            // picFoto
            // 
            this.picFoto.Location = new System.Drawing.Point(401, 20);
            this.picFoto.Name = "picFoto";
            this.picFoto.Size = new System.Drawing.Size(100, 90);
            this.picFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFoto.TabIndex = 8;
            this.picFoto.TabStop = false;
            // 
            // lblRut
            // 
            this.lblRut.AutoSize = true;
            this.lblRut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRut.Location = new System.Drawing.Point(19, 41);
            this.lblRut.Name = "lblRut";
            this.lblRut.Size = new System.Drawing.Size(12, 16);
            this.lblRut.TabIndex = 7;
            this.lblRut.Text = "-";
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexo.Location = new System.Drawing.Point(19, 145);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(12, 16);
            this.lblSexo.TabIndex = 6;
            this.lblSexo.Text = "-";
            // 
            // lblApellidoMaterno
            // 
            this.lblApellidoMaterno.AutoSize = true;
            this.lblApellidoMaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidoMaterno.Location = new System.Drawing.Point(19, 111);
            this.lblApellidoMaterno.Name = "lblApellidoMaterno";
            this.lblApellidoMaterno.Size = new System.Drawing.Size(12, 16);
            this.lblApellidoMaterno.TabIndex = 5;
            this.lblApellidoMaterno.Text = "-";
            // 
            // lblApellidoPaterno
            // 
            this.lblApellidoPaterno.AutoSize = true;
            this.lblApellidoPaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidoPaterno.Location = new System.Drawing.Point(19, 84);
            this.lblApellidoPaterno.Name = "lblApellidoPaterno";
            this.lblApellidoPaterno.Size = new System.Drawing.Size(12, 16);
            this.lblApellidoPaterno.TabIndex = 4;
            this.lblApellidoPaterno.Text = "-";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(19, 57);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(12, 16);
            this.lblNombre.TabIndex = 3;
            this.lblNombre.Text = "-";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(28, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 16);
            this.label5.TabIndex = 13;
            this.label5.Text = "Iso Compact";
            // 
            // txtIsoCompact
            // 
            this.txtIsoCompact.Location = new System.Drawing.Point(163, 176);
            this.txtIsoCompact.Name = "txtIsoCompact";
            this.txtIsoCompact.Size = new System.Drawing.Size(369, 20);
            this.txtIsoCompact.TabIndex = 14;
            this.txtIsoCompact.Text = resources.GetString("txtIsoCompact.Text");
            // 
            // lblResultado
            // 
            this.lblResultado.AutoSize = true;
            this.lblResultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResultado.Location = new System.Drawing.Point(326, 254);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(0, 20);
            this.lblResultado.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 631);
            this.Controls.Add(this.lblResultado);
            this.Controls.Add(this.txtIsoCompact);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnObtener);
            this.Controls.Add(this.chkObtenerFirma);
            this.Controls.Add(this.chkFoto);
            this.Controls.Add(this.chkTraerDatos);
            this.Controls.Add(this.txtFecNacimiento);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtFecVen);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNroDocumento);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Test de Componentes BVI";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFirma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNroDocumento;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFecVen;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFecNacimiento;
        private System.Windows.Forms.CheckBox chkTraerDatos;
        private System.Windows.Forms.CheckBox chkFoto;
        private System.Windows.Forms.CheckBox chkObtenerFirma;
        private System.Windows.Forms.Button btnObtener;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox picFoto;
        private System.Windows.Forms.Label lblRut;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Label lblApellidoMaterno;
        private System.Windows.Forms.Label lblApellidoPaterno;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.PictureBox picFirma;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtIsoCompact;
        private System.Windows.Forms.Label lblResultado;
    }
}

