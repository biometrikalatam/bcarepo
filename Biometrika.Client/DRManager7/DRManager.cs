﻿using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Script.Serialization;

namespace DRManager7
{
    public class DRManager : IManager
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(DRManager));

        private bool isOpen = false;
        private Dictionary<string, object> output = new Dictionary<string, object>();
        private string templateToken = string.Empty;

        public string Buffer => new JavaScriptSerializer().Serialize(output);

        public string Tag { get => "DR"; }

        public DRManager() { } // ctor de reflextion

        public DRManager(NameValueCollection queryString, object obj)
        {
            LOG.Info("DRManager In...");
            //Log.Add("BioPortalClient7Manager In...");
            if (!isOpen)
            {
                LOG.Debug("DRManager isOpen...");
                isOpen = true;

                //DRUIAdapter7.Program.SetBSP(_BSP);
                LOG.Debug("BioPortalClient7Manager SetPacket => " + queryString);
                DRUIAdapter7.Program.SetPacket(queryString);     // el program recibe el pedido
                LOG.Debug("BioPortalClient7Manager Mail...");
                DRUIAdapter7.Program.Main();

                Dictionary<string, object> outputUI = DRUIAdapter7.Program.Packet.PacketParamOut;
                LOG.Debug("BioPortalClient7Manager outupUI.Length = " + outputUI.Count);

                // Este param out retorna la imagen de la captura y las minucias
                if (outputUI != null && outputUI.Count > 0)
                {
                    //output.Add("TrackIdCI", outputUI.ContainsKey("TrackIdCI") ? outputUI["TrackIdCI"] : null);
                    //LOG.Debug("DRManager => TrackIdCI = " + (outputUI.ContainsKey("TrackIdCI") ? outputUI["TrackIdCI"] : "NO TrackIdCI"));
                    output.Add("DRResponse", outputUI.ContainsKey("DRResponse") ? outputUI["DRResponse"] : null);
                    LOG.Debug("DRManager => DRResponse = " + (outputUI.ContainsKey("DRResponse") ? outputUI["DRResponse"] : "NO Resultado"));
                }
            }
            LOG.Info("DRManager Out!");
        }

    } 


    public class MsgError
    {

        public MsgError() { }

        public MsgError(string strerr)
        {
            try
            {
                if (String.IsNullOrEmpty(strerr))
                {
                    Codigo = -1;
                    Description = "Retorno Nulo";
                }
                else
                {
                    string[] arrerr = strerr.Split('|');
                    Codigo = Convert.ToInt32(arrerr[0]);
                    Description = arrerr[1];
                }
            }
            catch (Exception ex)
            {
                Description = "Error parseando error return [" + ex.Message + "]";
            }
        }

        public int Codigo { get; set; }
        public string Description { get; set; }



    }
}
