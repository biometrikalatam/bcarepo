﻿using Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Script.Serialization;

namespace BioPortalClient7Manager
{
    public class BioPortalClient7Manager : IManager 
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(BioPortalClient7Manager));

        public Biometrika.BioApi20.BSP.BSPBiometrika _BSP; 
        private bool _IS_BPS_CONFIGURED = false;
        private bool isOpen = false;
        private Dictionary<string, object> output = new Dictionary<string, object>();
        private string templateToken = string.Empty;

        public BioPortalClient7Manager() { } // ctor de reflextion

        public BioPortalClient7Manager(NameValueCollection queryString) 
        {
            LOG.Info("BioPortalClient7Manager In...");   
            //Log.Add("BioPortalClient7Manager In...");
            if (!isOpen)
            {
                LOG.Debug("BioPortalClient7Manager isOpen..."); 
                isOpen = true;

                //if (!_IS_BPS_CONFIGURED)
                //{
                //    _IS_BPS_CONFIGURED = InitBSP();
                //}
                //if (!_IS_BPS_CONFIGURED)
                //{
                //    LOG.Fatal("BioPortalClient7Manager Fatal Error => BPSBiometrika No pudo ser configurado correctamente!");
                //} else
                //{
                //    //BioPortalClient7UIAdapter.Program.SetBSP(_BSP);
                //}
                LOG.Debug("BioPortalClient7Manager SetPacket => " + queryString);
                BioPortalClient7UIAdapter.Program.SetPacket(queryString);     // el program recibe el pedido
                LOG.Debug("BioPortalClient7Manager Mail...");
                BioPortalClient7UIAdapter.Program.Main(); 

                Dictionary<string, object> outputUI = BioPortalClient7UIAdapter.Program.Packet.PacketParamOut;
                LOG.Debug("BioPortalClient7Manager outupUI.Length = " + outputUI.Count);
                
                // Este param out retorna la imagen de la captura y las minucias
                if (outputUI != null && outputUI.Count > 0)  
                { 
                    output.Add("Token", outputUI.ContainsKey("Token") ? outputUI["Token"] : null);
                    LOG.Debug("BioPortalClient7Manager => Token = " + (outputUI.ContainsKey("Token") ? outputUI["Token"] : "NO Token"));
                    output.Add("ImageSample", outputUI.ContainsKey("ImageSample") ? outputUI["ImageSample"] : null);
                    LOG.Debug("BioPortalClient7Manager => ImageSample = " + (outputUI.ContainsKey("ImageSample") ? outputUI["ImageSample"] : "NO ImageSample"));
                    output.Add("Error", outputUI.ContainsKey("Error") ? outputUI["Error"] : null);
                    LOG.Debug("BioPortalClient7Manager => Error = " + (outputUI.ContainsKey("Error") ? outputUI["Error"] : "NO Error"));
                    output.Add("BodyPart", outputUI.ContainsKey("BodyPart") ? outputUI["BodyPart"] : 0);
                    LOG.Debug("BioPortalClient7Manager => BodyPart = " + (outputUI.ContainsKey("BodyPart") ? outputUI["BodyPart"] : "NO BodyPart"));
                    output.Add("Width", outputUI.ContainsKey("Width") ? outputUI["Width"] : 0);
                    LOG.Debug("BioPortalClient7Manager => Width = " + (outputUI.ContainsKey("Width") ? outputUI["Token"] : "NO Width"));
                    output.Add("Height", outputUI.ContainsKey("Height") ? outputUI["Height"] : 0);
                    LOG.Debug("BioPortalClient7Manager => Height = " + (outputUI.ContainsKey("Height") ? outputUI["Height"] : "NO Height"));
                    output.Add("Hash", outputUI.ContainsKey("Hash") ? outputUI["Hash"] : 0);
                    LOG.Debug("BioPortalClient7Manager => Hash = " + (outputUI.ContainsKey("Hash") ? outputUI["Hash"] : "NO Hash"));
                    LOG.Info("BioPortalClient7Manager Seting retorno...");
                }


                //if (error.Codigo == 0) //No hubo error => Armo token
                //{
                //    templateToken = String.Format(
                //              "{0}-septmpl-{1}-septmpl-{2}-septmpl-{3}",
                //              outputUI["TemplateVerify64"],
                //              outputUI["TemplateRegistry64"],
                //              outputUI["TemplateANSI64"],
                //              outputUI["TemplateISO64"]
                //    );
                //} else
                //{
                //    templateToken = null;
                //}

                //output.Add("Token", templateToken);
            }
            LOG.Info("BioPortalClient7Manager Out!");
        }

        public BioPortalClient7Manager(NameValueCollection queryString, object objBSP)
        {
            LOG.Info("BioPortalClient7Manager In...");
            //Log.Add("BioPortalClient7Manager In...");
            if (!isOpen)
            {
                LOG.Debug("BioPortalClient7Manager isOpen...");
                isOpen = true;

                if (objBSP != null)
                {
                    _BSP = (Biometrika.BioApi20.BSP.BSPBiometrika)objBSP;
                }

                //if (!_IS_BPS_CONFIGURED)
                //{
                //    _IS_BPS_CONFIGURED = InitBSP();
                //}
                //if (!_IS_BPS_CONFIGURED)
                //{
                //    LOG.Fatal("BioPortalClient7Manager Fatal Error => BPSBiometrika No pudo ser configurado correctamente!");
                //} else
                //{
                //    //BioPortalClient7UIAdapter.Program.SetBSP(_BSP);
                //}
                BioPortalClient7UIAdapter.Program.SetBSP(_BSP);
                LOG.Debug("BioPortalClient7Manager SetPacket => " + queryString);
                BioPortalClient7UIAdapter.Program.SetPacket(queryString);     // el program recibe el pedido
                LOG.Debug("BioPortalClient7Manager Mail...");
                BioPortalClient7UIAdapter.Program.Main();

                Dictionary<string, object> outputUI = BioPortalClient7UIAdapter.Program.Packet.PacketParamOut;
                LOG.Debug("BioPortalClient7Manager outupUI.Length = " + outputUI.Count);

                // Este param out retorna la imagen de la captura y las minucias
                if (outputUI != null && outputUI.Count > 0)
                {
                    output.Add("Token", outputUI.ContainsKey("Token") ? outputUI["Token"] : null);
                    LOG.Debug("BioPortalClient7Manager => Token = " + (outputUI.ContainsKey("Token") ? outputUI["Token"] : "NO Token"));
                    output.Add("ImageSample", outputUI.ContainsKey("ImageSample") ? outputUI["ImageSample"] : null);
                    LOG.Debug("BioPortalClient7Manager => ImageSample = " + (outputUI.ContainsKey("ImageSample") ? outputUI["ImageSample"] : "NO ImageSample"));
                    output.Add("Error", outputUI.ContainsKey("Error") ? outputUI["Error"] : null);
                    LOG.Debug("BioPortalClient7Manager => Error = " + (outputUI.ContainsKey("Error") ? outputUI["Error"] : "NO Error"));
                    output.Add("BodyPart", outputUI.ContainsKey("BodyPart") ? outputUI["BodyPart"] : 0);
                    LOG.Debug("BioPortalClient7Manager => BodyPart = " + (outputUI.ContainsKey("BodyPart") ? outputUI["BodyPart"] : "NO BodyPart"));
                    output.Add("Width", outputUI.ContainsKey("Width") ? outputUI["Width"] : 0);
                    LOG.Debug("BioPortalClient7Manager => Width = " + (outputUI.ContainsKey("Width") ? outputUI["Token"] : "NO Width"));
                    output.Add("Height", outputUI.ContainsKey("Height") ? outputUI["Height"] : 0);
                    LOG.Debug("BioPortalClient7Manager => Height = " + (outputUI.ContainsKey("Height") ? outputUI["Height"] : "NO Height"));
                    output.Add("Hash", outputUI.ContainsKey("Hash") ? outputUI["Hash"] : 0);
                    LOG.Debug("BioPortalClient7Manager => Hash = " + (outputUI.ContainsKey("Hash") ? outputUI["Hash"] : "NO Hash"));
                    LOG.Info("BioPortalClient7Manager Seting retorno...");
                }


                //if (error.Codigo == 0) //No hubo error => Armo token
                //{
                //    templateToken = String.Format(
                //              "{0}-septmpl-{1}-septmpl-{2}-septmpl-{3}",
                //              outputUI["TemplateVerify64"],
                //              outputUI["TemplateRegistry64"],
                //              outputUI["TemplateANSI64"],
                //              outputUI["TemplateISO64"]
                //    );
                //} else
                //{
                //    templateToken = null;
                //}

                //output.Add("Token", templateToken);
            }
            LOG.Info("BioPortalClient7Manager Out!");
        }
        //private bool InitBSP()
        //{
        //    bool ret = false;
        //    try
        //    {
        //        _BSP = new Biometrika.BioApi20.BSP.BSPBiometrika();

        //        _BSP.BSPAttach("2.0", true);
        //        ret = _BSP.IsLoaded;
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("BioPortalClient7Manager.InitBSP Error = " + ex.Message);
        //        ret = false;
        //    }
        //    return ret;
        //    //GetInfoFromBSP();

        //    //BSP.OnCapturedEvent += OnCaptureEvent;
        //    //BSP.OnTimeoutEvent += OnTimeoutEvent;
        //}


        public string Buffer => new JavaScriptSerializer().Serialize(output);

        public string Tag { get => "BPC7"; }
    }

    public class MsgError
    {

        public MsgError() { }

        public MsgError(string strerr) {
            try
            {
                if (String.IsNullOrEmpty(strerr))
                {
                    Codigo = -1;
                    Description = "Retorno Nulo";
                }
                else
                {
                    string[] arrerr = strerr.Split('|');
                    Codigo = Convert.ToInt32(arrerr[0]);
                    Description = arrerr[1];
                }
            }
            catch (Exception ex)
            {
                Description = "Error parseando error return [" + ex.Message + "]";
            }
        }

        public int Codigo { get; set; }
        public string Description { get; set; }



    }
}
