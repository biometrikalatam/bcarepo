﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ProgressBar;

namespace SerialPortSyncTest
{
    public partial class Form1 : Form
    {
        //SerialPort _serialPort;
        SerialPortManager _SerialPortManager;
        public Form1()
        {
            InitializeComponent();
            //_serialPort = new SerialPort("COM7", 9600, Parity.None, 8, StopBits.One);
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string barcodereaded;
            string msgerr;

            if (_SerialPortManager == null) button2_Click(null, null);

            if (_SerialPortManager != null)
            {
                richTextBox1.Text = "Leyendo con timeout " + (Convert.ToInt32(txtTimeout.Text.Trim()) / 1000) + " segundos...";
                richTextBox1.Refresh();
                int ierr = _SerialPortManager.ReadSync(Convert.ToInt32(txtTimeout.Text.Trim()), out barcodereaded, out msgerr);
                if (ierr == 0)
                {
                    if (!string.IsNullOrEmpty(barcodereaded))
                    {
                        richTextBox1.Text = "Barcode=" + barcodereaded;
                    }
                } else
                {
                    richTextBox1.Text = "Code=" + ierr + " - MsgErr=" + msgerr;
                }
            } else
            {
                richTextBox1.Text = "_SerialPortManager != null";
            }
            richTextBox1.Refresh();

            //byte[] bytesReaded = null;
            //int timeout = 3000;
            //int checkTimeout = 0;
            //try
            //{
            //    //_serialPort = new SerialPort(port);
            //    //_serialPort.BaudRate = Convert.ToInt32(baudRate);
            //    //_serialPort.Parity = Parity.None;
            //    //_serialPort.StopBits = StopBits.One;
            //    //_serialPort.DataBits = 8;
            //    _serialPort.Handshake = Handshake.None;
            //    //_serialPort.RtsEnable = true;
            //    //_serialPort.ReadTimeout = 3000;
            //    if (_serialPort.IsOpen)
            //    {
            //        _serialPort.Close();
            //        _serialPort.Open();
            //    }
            //    else
            //    {
            //        _serialPort.Open();
            //    }
            //    string data;

            //    bool _Timeout = false;
            //    bool _Readed = false;
            //    while (!_Timeout && !_Readed)
            //    {
            //        Thread.Sleep(1000);
            //        checkTimeout += 1000;
            //        if (checkTimeout > timeout)
            //        {
            //            _Timeout = true;
            //            richTextBox1.Text = "TIMEOUT => " + checkTimeout;
            //        }
            //        else
            //        {
            //            int count = _serialPort.BytesToRead;
            //            if (count < 1)
            //            {
            //                richTextBox1.Text = "No Data to Read..." + count;
            //            }
            //            else
            //            {
            //                bytesReaded = new byte[count];
            //                int i = 0;
            //                while (count > 0)
            //                {
            //                    //int byteData = _serialPort.ReadByte();
            //                    bytesReaded[i] = (byte)_serialPort.ReadByte();  // data + Convert.ToChar(byteData);
            //                    count--;
            //                    i++;
            //                }
            //                _Readed = true;
            //            }
            //        }
            //    }



            //    _serialPort.DiscardOutBuffer();
            //    _serialPort.Close();
            //    if (_Readed && bytesReaded != null)
            //    {
            //        richTextBox1.Text = ASCIIEncoding.UTF8.GetString(bytesReaded);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    if (_serialPort.IsOpen)
            //        _serialPort.Close();
            //    richTextBox1.Text = ex.ToString();
            //}

        }

        private void button2_Click(object sender, EventArgs e)
        {
            _SerialPortManager = new SerialPortManager(txtCom.Text.Trim(), Convert.ToInt32(txtBaude.Text.Trim()));
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            //SerialPort.Net.Windows.WindowsSerialPortDevice dev =
            //    new SerialPort.Net.Windows.WindowsSerialPortDevice("COM9", 9600, SerialPort.Net.StopBits.One,
            //                        SerialPort.Net.Parity.None, 8, 1024, null);
            //Device.Net.ConnectedDeviceDefinition def = dev.ConnectedDeviceDefinition;
            //Device.Net.TransferResult tr = await dev.ReadAsync(default);

            int i = 0;
        }
    }
}
