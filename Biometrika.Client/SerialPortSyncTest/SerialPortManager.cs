﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerialPortSyncTest
{
    public class SerialPortManager
    {

        private SerialPort _SERIALPORT;
        public string _ComPort { get; set; }
        public int _BaudRate { get; set; }

        public SerialPortManager(string comPort, int baudRate)
        {
            _ComPort = comPort;
            _BaudRate = baudRate;
            _SERIALPORT = new SerialPort(_ComPort, _BaudRate, Parity.None, 8, StopBits.One);
            //_SERIALPORT = new SerialPort(_ComPort);
            _SERIALPORT.Handshake = Handshake.None;
            _SERIALPORT.RtsEnable = true;
            _SERIALPORT.DtrEnable = false;
            //_SERIALPORT.Encoding = Encoding.UTF8;
            _SERIALPORT.WriteTimeout = 5000;
            _SERIALPORT.ReadTimeout = 5000;
            //string[] ports = SerialPort.GetPortNames();
            //for (int i = 0; i < ports.Length; i++)
            //{
            //    Console.WriteLine(ports[i]);
            //}

            try
            {
                if (_SERIALPORT.IsOpen)
                {
                    _SERIALPORT.Close();
                    _SERIALPORT.Open();
                }
                else
                {
                    _SERIALPORT.Open();
                }
            }
            catch (Exception ex)
            {
                if (_SERIALPORT != null && _SERIALPORT.IsOpen)
                    _SERIALPORT.Close();
                string msgerr = "Excp [" + ex.Message + "]";
            }

        }



        //
        public int ReadSync(int timeout, out string barcode, out string msgerr)
        {
            int ret = 0;
            byte[] bytesReaded = null;
            int checkTimeout = 0;
            msgerr = null;
            barcode = null;
            try
            { 
                if (_SERIALPORT.IsOpen)
                {
                    _SERIALPORT.Close();
                    _SERIALPORT.Open();
                }
                else
                {
                    _SERIALPORT.Open();
                }

                bool _Timeout = false;
                bool _Readed = false;
                while (!_Timeout && !_Readed)
                {
                    Thread.Sleep(1000);
                    checkTimeout += 1000;
                    if (checkTimeout >= timeout)
                    {
                        _Timeout = true;
                        ret = -2;
                        msgerr = "TIMEOUT => " + checkTimeout;
                    }
                    else
                    {
                        int count = _SERIALPORT.BytesToRead;
                        if (count < 1)
                        {
                            //Add Log //richTextBox1.Text = "No Data to Read..." + count;
                        }
                        else
                        {
                            bytesReaded = new byte[count];
                            int i = 0;
                            while (count > 0)
                            {
                                //int byteData = _serialPort.ReadByte();
                                bytesReaded[i] = (byte)_SERIALPORT.ReadByte();  // data + Convert.ToChar(byteData);
                                count--;
                                i++;
                            }
                            _Readed = true;
                        }
                    }
                }

                _SERIALPORT.DiscardOutBuffer();
                _SERIALPORT.Close();

                if (_Readed)
                {
                    string dataReaded = ASCIIEncoding.UTF8.GetString(bytesReaded);
                    if (dataReaded.StartsWith("h"))
                    {
                        barcode = dataReaded;
                    } else
                    {
                        barcode = Convert.ToBase64String(bytesReaded);
                    }
                }
            }
            catch (Exception ex)
            {
                if (_SERIALPORT != null && _SERIALPORT.IsOpen)
                    _SERIALPORT.Close();
                ret = -1;
                msgerr = "Excp [" + ex.Message + "]";
            }

            return ret;
        } 

    }
}
