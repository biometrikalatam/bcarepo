﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerialPortSyncTest
{
    internal static class Program
    {
        static bool _continue = true;
        //static SerialPort _serialPort;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
        //public static void Main()
        //{
        //    string name;
        //    string message;
        //    StringComparer stringComparer = StringComparer.OrdinalIgnoreCase;
        //    Thread readThread = new Thread(Read);

        //    // Create a new SerialPort object with default settings.
        //    SerialPort _serialPort = new SerialPort();

        //    // Allow the user to set the appropriate properties.
        //    _serialPort.PortName = "COM9"; // SetPortName(_serialPort.PortName);
        //    _serialPort.BaudRate = 9600; // SetPortBaudRate(_serialPort.BaudRate);
        //    _serialPort.Parity = Parity.None; // SetPortParity(_serialPort.Parity);
        //    _serialPort.DataBits = 8; // SetPortDataBits(_serialPort.DataBits);
        //    _serialPort.StopBits = StopBits.One; // SetPortStopBits(_serialPort.StopBits);
        //    _serialPort.Handshake = Handshake.None; // SetPortHandshake(_serialPort.Handshake);

        //    // Set the read/write timeouts
        //    _serialPort.ReadTimeout = 500;
        //    _serialPort.WriteTimeout = 500;

        //    _serialPort.Open();
        //    bool _continue = true;
        //    readThread.Start();

        //    Console.Write("Name: ");
        //    name = Console.ReadLine();

        //    Console.WriteLine("Type QUIT to exit");

        //    while (_continue)
        //    {
        //        message = Console.ReadLine();

        //        if (stringComparer.Equals("quit", message))
        //        {
        //            _continue = false;
        //        }
        //        else
        //        {
        //            _serialPort.WriteLine(
        //                String.Format("<{0}>: {1}", name, message));
        //        }
        //    }

        //    readThread.Join();
        //    _serialPort.Close();
        //}

        //public static void Read()
        //{
        //    while (_continue)
        //    {
        //        try
        //        {
        //            string message = _serialPort.ReadLine();
        //            Console.WriteLine(message);
        //        }
        //        catch (TimeoutException) { }
        //    }
        //}
    }
}
