﻿using BviCamUIAdapter.Model;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BviCamUIAdapter.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public MainWindowViewModel()
        {
            Persona = new Person();
            Resultado = new Resultado();
            Controles = new ElementosInterfaz();
        }

        Resultado resultado;
        public Resultado Resultado
        {
            get { return resultado; }
            set
            {
                resultado = value;
            }
        }

        public ElementosInterfaz Controles { get; set; }

        public Person Persona { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
