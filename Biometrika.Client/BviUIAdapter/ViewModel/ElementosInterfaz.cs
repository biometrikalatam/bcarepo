﻿using BviCamUIAdapter.Model;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace BviCamUIAdapter.ViewModel
{
    public class ElementosInterfaz : INotifyPropertyChanged
    {
        public ElementosInterfaz()
        {
            PanelErrorVisibility = Visibility.Hidden;
            BotonErrorVisibility = Visibility.Hidden;
            SiguienteVisibility = Visibility.Collapsed;
            BarraProgresoVisibility = Visibility.Hidden;
            RutIsEnabled = true;
            NombresIsEnabled = true;
            ApellidoPaternoIsEnabled = true;
            ApellidoMaternoIsEnabled = true;
            TipoDocumentoIsEnabled = true;
            Imagenes = new Imagenes();
            TextoResultado = "Resultado";
            ErrorWrapperColumnSpan = 1;

            ImagenCedula = new BitmapImage();
            ImagenHuella = new BitmapImage();
            ImagenContactless = new BitmapImage();
            ImagenResultado = new BitmapImage();
        }

        BitmapImage imagenCapturaHuella;
        public BitmapImage ImagenCapturaHuella
        {
            get { return imagenCapturaHuella; }
            set
            {
                if (value != null)
                {
                    imagenCapturaHuella = value;
                    NotifyPropertyChanged();
                }
                else
                {
                    imagenCapturaHuella = new BitmapImage();
                    NotifyPropertyChanged();
                }
            }
        }
        string textoBotonHuella = "Capturar Huella Dactilar";
        public string TextoBotonHuella
        {
            get { return textoBotonHuella; }
            set
            {
                textoBotonHuella = value;
                NotifyPropertyChanged();
            }
        }

        EstadoVerificacion step;
        public EstadoVerificacion Paso
        {
            get { return step; }
            set
            {
                step = value;
                NotifyPropertyChanged();
            }
        }

        Visibility panelErrorVisibility;
        public Visibility PanelErrorVisibility
        {
            get { return panelErrorVisibility; }
            set
            {
                panelErrorVisibility = value;
                NotifyPropertyChanged();
            }
        }

        Visibility botonErrorVisibility;
        public Visibility BotonErrorVisibility
        {
            get { return botonErrorVisibility; ; }
            set
            {
                botonErrorVisibility = value;
                NotifyPropertyChanged();
            }
        }

        Visibility siguienteVisibility;
        public Visibility SiguienteVisibility
        {
            get { return siguienteVisibility; }
            set
            {
                siguienteVisibility = value;
                NotifyPropertyChanged();
            }
        }

        Visibility barraProgresoVisibility;
        public Visibility BarraProgresoVisibility
        {
            get { return barraProgresoVisibility; }
            set
            {
                barraProgresoVisibility = value;
                NotifyPropertyChanged();
            }
        }

        bool botonSiguienteIsEnabled;
        public bool BotonSiguienteIsEnabled
        {
            get { return botonSiguienteIsEnabled; }
            set
            {
                botonSiguienteIsEnabled = value;
                NotifyPropertyChanged();
            }
        }

        bool nombresIsEnabled;
        public bool NombresIsEnabled
        {
            get { return nombresIsEnabled; }
            set
            {
                nombresIsEnabled = value;
                NotifyPropertyChanged();
            }
        }

        bool rutIsEnabled;
        public bool RutIsEnabled
        {
            get { return rutIsEnabled; }
            set
            {
                rutIsEnabled = value;
                NotifyPropertyChanged();
            }
        }

        bool apellidoPaternoIsEnabled;
        public bool ApellidoPaternoIsEnabled
        {
            get { return apellidoPaternoIsEnabled; }
            set
            {
                apellidoPaternoIsEnabled = value;
                NotifyPropertyChanged();
            }
        }

        bool apellidoMaternoIsEnabled;
        public bool ApellidoMaternoIsEnabled
        {
            get { return apellidoMaternoIsEnabled; }
            set
            {
                apellidoMaternoIsEnabled = value;
                NotifyPropertyChanged();
            }
        }

        bool tipoDocumentoIsEnabled;
        public bool TipoDocumentoIsEnabled
        {
            get { return tipoDocumentoIsEnabled; }
            set
            {
                tipoDocumentoIsEnabled = value;
                NotifyPropertyChanged();
            }
        }

        string textoError;
        public string TextoError
        {
            get { return textoError; }
            set
            {
                textoError = value;
                NotifyPropertyChanged();
            }
        }

        string textoSiguiente;
        public string TextoSiguiente
        {
            get { return textoSiguiente; }
            set
            {
                textoSiguiente = value;
                NotifyPropertyChanged();
            }
        }

        Imagenes imagenes;
        public Imagenes Imagenes
        {
            get { return imagenes; }
            set
            {
                imagenes = value;
                NotifyPropertyChanged();
            }
        }

        string textoResultado;
        public string TextoResultado
        {
            get { return textoResultado; }
            set
            {
                textoResultado = value;
                NotifyPropertyChanged();
            }
        }

        int errorWrapperColumnSpan;
        public int ErrorWrapperColumnSpan
        {
            get { return errorWrapperColumnSpan; }
            set
            {
                errorWrapperColumnSpan = value;
                NotifyPropertyChanged();
            }
        }

        ImageSource imagenCedula;
        public ImageSource ImagenCedula
        {
            get { return imagenCedula; }
            set
            {
                imagenCedula = value;
                NotifyPropertyChanged();
            }
        }

        ImageSource imagenHuella;
        public ImageSource ImagenHuella
        {
            get { return imagenHuella; }
            set
            {
                imagenHuella = value;
                NotifyPropertyChanged();
            }
        }

        ImageSource imagenContactless;
        public ImageSource ImagenContactless
        {
            get { return imagenContactless; }
            set
            {
                imagenContactless = value;
                NotifyPropertyChanged();
            }
        }

        ImageSource imagenResultado;
        public ImageSource ImagenResultado
        {
            get { return imagenResultado; }
            set
            {
                imagenResultado = value;
                NotifyPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
