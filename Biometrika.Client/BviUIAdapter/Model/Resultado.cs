﻿using BiometrikaComponentServiceDomain.Model;
using BiometrikaComponentServiceDomain.Model.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace BviCamUIAdapter.Model
{
    public class Resultado : INotifyPropertyChanged
    {
        // True = Verificación positiva, false = Verificación negativa
        //bool verificacionPositiva = false;
        //public bool VerificacionPositiva
        //{
        //    get { return verificacionPositiva; }
        //    set
        //    {
        //        verificacionPositiva = value;
        //        NotifyPropertyChanged();
        //    }
        //}
        // True = Se extrayeron los datos personales, false = no se extrayeron
        bool datosPersonalesExtraidos = false;
        public bool DatosPersonalesExtraidos
        {
            get { return datosPersonalesExtraidos; }
            set
            {
                datosPersonalesExtraidos = value;
                NotifyPropertyChanged();
            }
        }
        // True = se extrajo la foto desde la cédula, false = no se extrajo
        bool fotoExtraida = false;
        public bool FotoExtraida
        {
            get { return fotoExtraida; }
            set
            {
                fotoExtraida = value;
                NotifyPropertyChanged();
            }
        }
        // True = se extrajo la foto de la firma desde la cédula, false = no se extrajo
        bool fotoFirmaExtraida = false;
        public bool FotoFirmaExtraida
        {
            get { return fotoFirmaExtraida; }
            set
            {
                fotoFirmaExtraida = value;
                NotifyPropertyChanged();
            }
        }
        // True = la cédula con la que se realizó la verifica ción pertenece a un menor de edad
        bool menorDeEdad = false;
        public bool MenorDeEdad
        {
            get { return menorDeEdad; }
            set
            {
                menorDeEdad = value;
                NotifyPropertyChanged();
            }
        }
        // True = la cédula de identidad se encuentra vencida, false = la cédula aún está vigente
        bool cedulaVencida = false;
        public bool CedulaVencida
        {
            get { return cedulaVencida; }
            set
            {
                cedulaVencida = value;
                NotifyPropertyChanged();
            }
        }

        bool cedulaBloqueada = false;
        public bool CedulaBloqueada
        {
            get { return cedulaBloqueada; }
            set
            {
                cedulaBloqueada = value;
                NotifyPropertyChanged();
            }
        }

        public Estado estado;
        public Estado Estado
        {
            get { return estado; }
            set
            {
                estado = value;
                NotifyPropertyChanged();
            }
        }

        public ResultadoVerificacion CreateResultado()
        {
            return new ResultadoVerificacion()
            {
                DatosPersonalesExtraidos = this.DatosPersonalesExtraidos,
                FotoExtraida = this.FotoExtraida,
                FotoFirmaExtraida = this.FotoFirmaExtraida,
                CedulaVencida = this.CedulaVencida,
                MenorDeEdad = this.MenorDeEdad,
                CedulaBloqueada = this.CedulaBloqueada,
                Estado = this.Estado
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
