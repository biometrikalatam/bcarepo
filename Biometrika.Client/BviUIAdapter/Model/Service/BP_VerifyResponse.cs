﻿using Bio.Core.Constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BviCamUIAdapter.Model.Service
{
    public class BP_VerifyResponse
    {
        public int Code { get; set; }
        public string Data { get; set; }

        public bool VerificationResult { get; set; }
    }
}
