﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BviCamUIAdapter.Model.Service
{
    public class ApiResponse<T>
    {
        //ApiResponseEnum _status;

        //public string Status => _status.ToString();

        //public ApiResponseEnum SetStatus
        //{
        //    set { _status = value; }
        //}

        public ApiResponseEnum Status { get; set; }

        public T Data { get; set; }

        public string Message { get; set; }
    }
}
