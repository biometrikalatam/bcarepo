﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BviCamUIAdapter.Model.Service
{
    public enum ApiResponseEnum
    {
        Error,
        Success,
        EndPointNotFound,
        Timeout,
        UriFormatError
    }
}
