﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BviCamUIAdapter.Model.Service
{
    public class NecDataToCompare
    {
        public string rawData { get; set; }
        public string necTemplate { get; set; }
        public string rut { get; set; }
    }
}
