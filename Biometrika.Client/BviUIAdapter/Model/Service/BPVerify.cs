﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BviCamUIAdapter.Model.Service
{
    public class BPVerify
    {
        /// <summary>
        /// Acción a realizar
        /// </summary>
        public BP_VerifyActionType Action { get; set; }
        /// <summary>
        /// TypeId del documento
        /// </summary>
        public string TypeId { get; set; }
        /// <summary>
        /// ValueId del documento
        /// </summary>
        public string ValueId { get; set; }
        /// <summary>
        /// Indicador de tipo de dato de la propiedad WSQoRAW
        /// </summary>
        public BP_VerifyFlag Flag { get; set; }
        /// <summary>
        /// WSQ o RAW en base 64, de aceurdo a lo indicado en el parametro anterior
        /// </summary>
        public string WSQoRAW { get; set; }
        /// <summary>
        /// Minucias NEC en base 64, extraídas desde la cédula vieja
        /// </summary>
        public string MinuciaNEC { get; set; }
        /// <summary>
        /// Opcional: Umbral para la comparación. Por defecto se asume el seteado por default en BioPortal
        /// </summary>
        public int? Umbral { get; set; }
        /// <summary>
        /// IP del cliente final que usa esto, o bien el serialid del sensor, sino algo fijo
        /// </summary>
        public string ClientId { get; set; }
        /// <summary>
        /// No aplica para esto igual pero podria ser un id int configurable
        /// </summary>
        public int CompanyId { get; set; }
        /// <summary>
        /// Algun string si se puede del cliente que manda a hacer esta funcion, IP, Serial del lector, o algo. Sino algo fijo
        /// </summary>
        public string EndUser { get; set; }
        /// <summary>
        /// IP del cliente final o sino ip ficticia 0.0.0.0 o 127.0.0.1
        /// </summary>
        public string IpEndUser { get; set; }
        /// <summary>
        /// Int configurable. default es 1 = Origen desconocido
        /// </summary>
        public int Origin { get; set; }
    }
}
