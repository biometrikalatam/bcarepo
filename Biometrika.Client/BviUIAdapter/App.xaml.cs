﻿using BiometrikaComponentServiceDomain.Model;
using BiometrikaComponentServiceDomain.Model.Enum;
using BviCamUIAdapter.Model.Service;
using Domain;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace BviCamUIAdapter
{
    
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App : Application
    {
        static Logger LOG = LogManager.GetCurrentClassLogger();

        public static BioPacket Packet { get; set; }

        public Solicitud solicitud { get; set; }


        public static void SetPacket(NameValueCollection queryString)
        {
            Packet = new BioPacket();
            Packet.PacketParamIn = queryString;
            Packet.PacketParamOut = new Dictionary<string, object>();

            

        }


        protected override void OnStartup(StartupEventArgs e)
        {
            int ret = 0;
            LOG.Info("BVIManager iniciando la aplicación, procedemos a tomar los valors");
            base.OnStartup(e);
            //Tomamos el Package.
            ret = CheckParameters(Packet);
            if (ret == 0)
            {
                LOG.Info("Validado los parámetros se construye la solicitud");
                BuildSolicitud();
                //Creamos el MainWindows con los datos del Package
                MainWindow window = new MainWindow(solicitud);
                window.ShowDialog();
            }
            else
            {
                LOG.Info("No se pudo iniciar el componente. No se encuentran definidos los parámetros");
                MessageBox.Show("Faltan parámetros de configuración", "Biometrika BVI", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BuildSolicitud()
        {
            solicitud = new Solicitud();
            solicitud.Persona = new Persona();
            solicitud.Persona.Rut = dictParamIn["valueid"];
            solicitud.Configuracion = new BiometrikaComponentServiceDomain.Configuracion();
            solicitud.Configuracion.ExtraerDatosPersonales = Convert.ToBoolean(dictParamIn["extraerdatospersonales"]);
            solicitud.Configuracion.ExtraerFirma = Convert.ToBoolean(dictParamIn["extraerfirma"]);
            solicitud.Configuracion.extraerFoto = Convert.ToBoolean(dictParamIn["extraerfoto"]);
            solicitud.Configuracion.bloqueoCedula = Convert.ToBoolean(dictParamIn["bloqueocedula"]);
            solicitud.Configuracion.iso = Convert.ToBoolean(dictParamIn["iso"]);
            solicitud.Configuracion.ansi = Convert.ToBoolean(dictParamIn["ansi"]);
            solicitud.Configuracion.raw = Convert.ToBoolean(dictParamIn["raw"]);
            solicitud.Configuracion.vigenciaCedula = Convert.ToBoolean(dictParamIn["vigenciacedula"]);
            solicitud.Configuracion.wsq = Convert.ToBoolean(dictParamIn["wsq"]);
            solicitud.Configuracion.jpeg = Convert.ToBoolean(dictParamIn["jpeg"]);

            
        }
        IDictionary<string, string> dictParamIn = new Dictionary<string, string>();
        private int CheckParameters(BioPacket bioPacketLocal)
        {
            int ret = 0;
            try
            {
                LOG.Info("BviUIAdapter.CheckParameters In...");
                if (bioPacketLocal == null) ret = -2;  //Lista de parametros no puede ser nulo

                LOG.Debug("BviUIAdapter.CheckParameters filling dictParamin...");
                foreach (var k in bioPacketLocal.PacketParamIn.AllKeys)
                {
                    LOG.Debug("BviUIAdapter.CheckParameters     Adding => " + k + "=" + (string)bioPacketLocal.PacketParamIn[k] + "...");
                    dictParamIn.Add(k, bioPacketLocal.PacketParamIn[k]);
                }
                if (!dictParamIn.ContainsKey("valueid") || String.IsNullOrEmpty(dictParamIn["valueid"]))
                {
                    ret = -3;  //ValueId deben ser enviados como parámetros
                    LOG.Debug("BviUIAdapter.CheckParameters ValueId deben ser enviados como parámetros!");
                }
                if (!dictParamIn.ContainsKey("extraerdatospersonales") || String.IsNullOrEmpty(dictParamIn["extraerdatospersonales"]))
                {
                    dictParamIn.Add("extraerdatospersonales", "0");
                }
                if (!dictParamIn.ContainsKey("extraerfirma") || String.IsNullOrEmpty(dictParamIn["extraerfirma"]))
                {
                    dictParamIn.Add("extraerfirma", "0");
                }
                if (!dictParamIn.ContainsKey("extraerfoto") || String.IsNullOrEmpty(dictParamIn["extraerfoto"]))
                {
                    dictParamIn.Add("extraerfoto", "0");
                }
                if (!dictParamIn.ContainsKey("vigenciacedula") || String.IsNullOrEmpty(dictParamIn["vigenciacedula"]))
                {
                    dictParamIn.Add("vigenciacedula", "0");
                }
                if (!dictParamIn.ContainsKey("bloqueocedula") || String.IsNullOrEmpty(dictParamIn["bloqueocedula"]))
                {
                    dictParamIn.Add("bloqueocedula", "0");
                }
                if (!dictParamIn.ContainsKey("bloqueomenoreseeedad") || String.IsNullOrEmpty(dictParamIn["bloqueomenoreseeedad"]))
                {
                    dictParamIn.Add("bloqueomenoreseeedad", "0");
                }
                if (!dictParamIn.ContainsKey("raw") || String.IsNullOrEmpty(dictParamIn["raw"]))
                {
                    dictParamIn.Add("raw", "0");
                }
                if (!dictParamIn.ContainsKey("iso") || String.IsNullOrEmpty(dictParamIn["iso"]))
                {
                    dictParamIn.Add("iso", "0");
                }
                if (!dictParamIn.ContainsKey("ansi") || String.IsNullOrEmpty(dictParamIn["ansi"]))
                {
                    dictParamIn.Add("ansi", "0");
                }
                if (!dictParamIn.ContainsKey("wsq") || String.IsNullOrEmpty(dictParamIn["wsq"]))
                {
                    dictParamIn.Add("wsq", "0");
                }
                if (!dictParamIn.ContainsKey("jpeg") || String.IsNullOrEmpty(dictParamIn["jpeg"]))
                {
                    dictParamIn.Add("jpeg", "0");
                }
                if (!dictParamIn.ContainsKey("bloqueomenoreseeedad") || String.IsNullOrEmpty(dictParamIn["bloqueomenoreseeedad"]))
                {
                    TipoBloqueo tipoBloqueo;
                    try
                    {
                        tipoBloqueo = (TipoBloqueo)Enum.Parse(typeof(TipoBloqueo), dictParamIn["bloqueomenoreseeedad"]);
                    }
                    catch(Exception e)
                    {
                        LOG.Error("El valor definido para el bloqueo informado");
                        dictParamIn.Add("bloqueomenoreseeedad", TipoBloqueo.Permitir.ToString());
                    }

                }
                else
                {
                    LOG.Info("Datos informados correctamente");
                }
            }
            catch(Exception exe)
            {

            }
            return ret;
        }
        public static void Close()
        {
            
        }
    }
}
