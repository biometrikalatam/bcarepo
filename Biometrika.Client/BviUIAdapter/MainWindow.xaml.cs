﻿using Bio.Core.CLR;
using BiometrikaComponentServiceDomain.Model;
using BiometrikaComponentServiceDomain.Model.Enum;
using BviCamUIAdapter.Libs;
using BviCamUIAdapter.Model;
using BviCamUIAdapter.Model.Service;
using BviCamUIAdapter.ViewModel;
using LibBiometrikaVerification;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LibBiometrikaVerification;
using BCR_RegistroCivil_Wrapper;
using BCR_RegistroCivil_Wrapper.Response;
using BCR_RegistroCivil_Wrapper.Enum;
using CSJ2K;
using System.Drawing.Imaging;
using CSJ2K.Util;
using System.Drawing;
using System.IO;
using System.Threading;
using System.ServiceModel;

namespace BviCamUIAdapter
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BCR LectorCodigoDeBarras;
        FPR LectorHuellaDigital;

        EstadoVerificacion Paso;
        Solicitud SolicitudVerificacion;

        public MainWindowViewModel ViewModel;
        static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Constructor de la ventana
        /// </summary>
        
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            Closing += MainWindowClosing;
        }

        /// <summary>
        /// Constructor de la ventana con parámetro
        /// </summary>
        /// <param name="solicitud"></param>
        public MainWindow(Solicitud solicitud)
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            Closing += MainWindowClosing;
            SetupProxy();

            SolicitudVerificacion = solicitud;
        }

        void SetupProxy()
        {
            bool usarProxy = false;
            var parseoExitoso = bool.TryParse(
                ConfigurationManager.AppSettings["UsarProxyPorDefecto"],
                out usarProxy);
            if (parseoExitoso && usarProxy)
            {
                System.Net.WebRequest.DefaultWebProxy.Credentials
                    = System.Net.CredentialCache.DefaultNetworkCredentials;
            }
        }
        /// <summary>
        /// Método que se llama cuando la ventana principal ya se ha cargado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Paso = EstadoVerificacion.EsperandoLecturaCodigoBarras;

            Siguiente.Click += Boton_Click;
            Cerrar.Click += (o, i) =>
            {
                ViewModel.Resultado.Estado = Estado.Cancelado;
                ViewModel.Persona.TipoIdentificacion = TipoIdentificacion.Indeterminado;
                this.Close();
            };
            BotonOculto.Click += (o, i) =>
            {
                var helper = new DiagnosticHelper();
                helper.EnviarCorreo();
            };

            Titulo.Text = Titulo.Text.Replace(
                "$RUT$",
                SolicitudVerificacion.Persona.Rut);

            ViewModel = new MainWindowViewModel();
            DataContext = ViewModel;

            PersonalizarInterfaz();

            InitBCR();
        }

        /// <summary>
        /// Método que se llama cuando se está cerrando la aplicación (previo al cerrado)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MainWindowClosing(object sender, EventArgs e)
        {
            if (LectorCodigoDeBarras != null)
            {
                Thread.Sleep(100);
                LectorCodigoDeBarras.Close();
            }
        }

        /// <summary>
        /// Personalización de la interfaz
        /// </summary>
        void PersonalizarInterfaz()
        {
            CargarIcono();
            SetBackgroundForControls(ConfigurationManager.AppSettings["Color"]);
            CargarImagenes();
        }

        /// <summary>
        /// Asocia las imágenes bindeadas al viewmodel
        /// </summary>
        private void CargarImagenes()
        {
            ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaCompletado"];
            ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaInactivo"];
            ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessInactivo"];
            ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoInactivo"];
        }

        /// <summary>
        /// Inicialización del código de barras
        /// </summary>
        /// <returns>true si es que se inicializó, false en caso contrario</returns>
        bool InitBCR()
        {
            try
            {
                var comPort = ConfigurationManager.AppSettings["COMPort"].ToUpper();
                logger.Info($"Abriendo el lector de código de barras configurado en {comPort}");
                LectorCodigoDeBarras = new BCR(comPort);
                LectorCodigoDeBarras.DataCaptured += ProcesarCedulaLeida;
                var bcrOpened = LectorCodigoDeBarras.Open();
                if (!bcrOpened)
                {
                    logger.Info("Error al abrir el lector de código de barras");
                    MostrarMensajeError(null, "Error al abrir el lector de código de barras");
                }
                else
                {
                    logger.Info("Lector de Código de barras abierto correctamente");
                }
                return bcrOpened;
            }
            catch (Exception ex)
            {
                logger.Error(
                    ex,
                    "Error al abrir el lector de código de barras. Puerto " +
                        ConfigurationManager.AppSettings["COMPort"].ToUpper());
                return false;
            }
        }

        /// <summary>
        /// Método que se llama al leer una cédula
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ProcesarCedulaLeida(object sender, EventArgs e)
        {
            logger.Info("Captura de lectura de cédula detectada. Antes del control de excepciones");
            try
            {
                logger.Info("Captura de lectura de cédula detectada");
                this.Dispatcher.Invoke(new Action(() =>
                {
                    ResetControls();
                    logger.Info("Comenzando procesamiento de lectura");
                    if (LectorCodigoDeBarras.RutFormateado != SolicitudVerificacion.Persona.Rut)
                    {
                        logger.Info("Cédula leída no corresponde a cédula de la solicitud");
                        MostrarMensajeError(null, "La cédula leída no corresponde al rut de la solicitud");
                        Paso = EstadoVerificacion.EsperandoLecturaCodigoBarras;
                        UpdateButtonsImages();
                    }
                    else
                    {
                        logger.Info("Cédula leída corresponde a rut de la solicitud");
                        DesbloquearSiguiente();
                        ViewModel.Persona.Rut = LectorCodigoDeBarras.RutFormateado;
                        ViewModel.Persona.ApellidoPaterno = LectorCodigoDeBarras.LastName;
                        ViewModel.Persona.Serie = LectorCodigoDeBarras.DocumentNumber;
                        if (SolicitudVerificacion.Configuracion.ExtraerDatosPersonales)
                        {
                            ViewModel.Persona.FechaExpiracion = LectorCodigoDeBarras.ExpirationDate;
                            ViewModel.Persona.FechaNacimiento = LectorCodigoDeBarras.BirthDate;
                        }
                        else
                        {
                            ViewModel.Persona.FechaExpiracion = Convert.ToDateTime("0001-01-01T00:00:00");
                            ViewModel.Persona.FechaNacimiento = Convert.ToDateTime("0001-01-01T00:00:00");
                        }

                        //Bloqueo de los controles
                        ViewModel.Controles.RutIsEnabled = false;
                        ViewModel.Controles.TipoDocumentoIsEnabled = false;

                        Paso = EstadoVerificacion.EsperandoHuellaEscaneadaCedulaNueva;
                        ViewModel.Persona.TipoIdentificacion = TipoIdentificacion.CedulaChilenaNueva;
                        ViewModel.Persona.TipoDocumento = "Cédula Nueva";

                        if (LectorCodigoDeBarras.Pdf417)
                        {
                            Paso = EstadoVerificacion.EsperandoHuellaEscaneadaCedulaVieja;
                            ViewModel.Persona.TipoIdentificacion = TipoIdentificacion.CedulaChilenaAntigua;
                            ViewModel.Persona.TipoDocumento = "Cédula Antigua";

                            ViewModel.Controles.ApellidoPaternoIsEnabled = false;
                        }
                        else if (LectorCodigoDeBarras.Pdf417 == false)
                        {
                            if (SolicitudVerificacion.Configuracion.BloqueoMenoresDeEdad == TipoBloqueo.Bloqueo)
                            {
                                if (IsMoreThan18((DateTime)LectorCodigoDeBarras.BirthDate) == true)
                                {
                                    MostrarMensajeError(null, "Menor de Edad");
                                }
                            }
                        }
                        TextoHuella.Visibility = Visibility.Visible;
                        UpdateButtonsImages();
                    }
                }));
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error al capturar los datos");
            }
        }

        /// <summary>
        /// Llamada de métodos cuando se presiona el botón siguiente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Boton_Click(object sender, RoutedEventArgs e)
        {
            switch (Paso)
            {
                case EstadoVerificacion.EsperandoLecturaCodigoBarras:
                    ResetControls();
                    break;
                case EstadoVerificacion.EsperandoHuellaEscaneadaCedulaNueva:
                case EstadoVerificacion.EsperandoHuellaEscaneadaCedulaVieja:
                    CapturarHuella();
                    break;
                case EstadoVerificacion.EsperandoLecturaContactless:
                    //SetupClr();
                    VerificarContraCedula();
                    break;
                case EstadoVerificacion.VerificacionNegativa:
                case EstadoVerificacion.VerificacionPositiva:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Método que llama la captura de la huella
        /// </summary>
        void CapturarHuella()
        {
            logger.Info("Capturando Huella Dactilar");
            LectorHuellaDigital = new FPR();

            int timeoutLector = AppConfigHelper.GetTimeoutLector();
            //Solo para sacar el serial del secugen
            LectorHuellaDigital.SimpleOpen();

            int respuesta = LectorHuellaDigital.LongCaptureWithTimeOut(timeoutLector, 70);
            if (respuesta == FPR.NoError)
            {
                logger.Info("Huella Dactilar Capturada");
                ProcesarHuellaCapturada();
            }
            else if (respuesta == FPR.CaptureTimeout)
            {
                MostrarMensajeError(CapturarHuella, "Ha tomado mucho tiempo en capturar la huella");
            }
        }

        /// <summary>
        /// Procesamiento de los datos obtenidos por la huealla
        /// </summary>
        void ProcesarHuellaCapturada()
        {
            logger.Info("Procesando Huella Capturada");
            if (String.IsNullOrEmpty(LectorHuellaDigital.ISO))
            {
                logger.Error("Error al capturar la huella");
                CapturarHuella();
            }

            var fingerprint = WpfImageHelper.Base64ToBitmap(LectorHuellaDigital.BMP);
            ViewModel.Controles.ImagenCapturaHuella = WpfImageHelper.BitmapToBitmapImage(fingerprint);
            if (SolicitudVerificacion.Configuracion.ansi)
            {
                ViewModel.Persona.Ansi = LectorHuellaDigital.ANSI;
            }
            else
            {
                ViewModel.Persona.Ansi = "";
            }
            if (SolicitudVerificacion.Configuracion.raw)
            {
                ViewModel.Persona.Raw = LectorHuellaDigital.RAW;
            }
            else
            {
                ViewModel.Persona.Raw = "";
            }
            if (SolicitudVerificacion.Configuracion.iso)
            {
                ViewModel.Persona.Iso = LectorHuellaDigital.ISO;
            }
            else
            {
                ViewModel.Persona.Iso = "";
            }
            if (SolicitudVerificacion.Configuracion.wsq)
            {
                ViewModel.Persona.Wsq = LectorHuellaDigital.WSQ;
            }
            else
            {
                ViewModel.Persona.Wsq = "";
            }
            if (SolicitudVerificacion.Configuracion.jpeg)
            {
                ViewModel.Persona.Jpg = LectorHuellaDigital.BMP.ToString();
            }
            else
            {
                ViewModel.Persona.Jpg = "";
            }

            TextoHuella.Visibility = Visibility.Collapsed;

            if (LectorCodigoDeBarras.Pdf417)
            {
                logger.Info("Realizando Verificación de cédula antigua");
                VerificarCedulaAntiguaAsync();
            }
            else
            {
                logger.Info("Realizando verificación de cédula nueva");
                ConvertirIsoToIsoCCAsync(LectorHuellaDigital.ISO);
            }
        }

        /// <summary>
        /// Creación del worker que se encarga de la verificación con cédula antigua
        /// </summary>
        void VerificarCedulaAntiguaAsync()
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Visible;

            BackgroundWorker verificacionWorker = new BackgroundWorker();
            verificacionWorker.DoWork += VerificacionWorker_DoWork;
            verificacionWorker.RunWorkerCompleted += VerificacionWorker_RunWorkerCompleted;
            verificacionWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Método del worker de verificación de cédula antigua
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void VerificacionWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ProxyCaller proxy = new ProxyCaller();
                var necData = new NecDataToCompare()
                {
                    necTemplate = Convert.ToBase64String(LectorCodigoDeBarras.NEC),
                    rawData = Convert.ToBase64String(LectorHuellaDigital.NEC),
                    rut = LectorCodigoDeBarras.RutFormateado
                };

                var respuesta = proxy.VerifyNec(
                    necData,
                    LectorHuellaDigital.SERIALNUMBER);
                e.Result = respuesta;
            }
            catch (TimeoutException ex)
            {
                logger.Error(ex, "El servicio de verificación ha tomado mucho tiempo en responder");
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.Timeout,
                    Message = "El servicio de verificación ha tomado mucho tiempo en responder"
                };
                e.Result = ApiResponseEnum.Timeout;
            }
            catch (EndpointNotFoundException ex)
            {
                logger.Error(ex, "El servicio de verificación no se ha encontrado");
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.EndPointNotFound,
                    Message = "El servicio de verificación de identidad no se ha encontrado. Verifique su conexión"
                };
            }
            catch (UriFormatException ex)
            {
                logger.Error(ex, "La URL ingresada no tiene un formato válido");
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.UriFormatError,
                    Message = "La URL ingresada no tiene un formato válido. Comuníquese con soporte@biometrika.cl"
                };
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error indeterminado");
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.Error,
                    Message = "Ha ocurrido un error con el servicio de verificación. Comuníquese con soporte@biometrik.cl"
                };
            }
        }

        private bool IsMoreThan18(DateTime from)
        {
            if (from.AddYears(18) < DateTime.Now)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Método que se ejecuta al terminar la verificación de identidad remota
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void VerificacionWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Collapsed;
            //Verificamos si la cédula se encuentra vencida.
            logger.Info("la fecha de expiración de la cédula es:" + LectorCodigoDeBarras.ExpirationDate.ToString());
            if (LectorCodigoDeBarras.ExpirationDate < DateTime.Now)
            {
                ViewModel.Resultado.CedulaVencida = true;
            }
            if (ViewModel.Persona.TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
            {
                if (IsMoreThan18((DateTime)LectorCodigoDeBarras.BirthDate) == true)
                {
                    ViewModel.Resultado.MenorDeEdad = true;
                }
                else
                {
                    ViewModel.Resultado.MenorDeEdad = false;
                }

            }
            if (SolicitudVerificacion.Configuracion.bloqueoCedula)
            {
                if (RegistroCivil())
                {
                    ViewModel.Resultado.CedulaBloqueada = false;
                }
                else
                {
                    ViewModel.Resultado.CedulaBloqueada = true;
                }
            }
            else
            {
                ViewModel.Resultado.CedulaBloqueada = false;
            }

            var resultado = (Model.Service.ApiResponse<BP_VerifyResponse>)e.Result;
            switch (resultado.Status)
            {
                case ApiResponseEnum.Success:
                    if (resultado.Data.VerificationResult)
                    {
                        Paso = EstadoVerificacion.VerificacionPositiva;
                        ViewModel.Resultado.Estado = Estado.VerificacionPositiva;
                    }
                    else
                    {
                        Paso = EstadoVerificacion.VerificacionNegativa;
                        ViewModel.Resultado.Estado = Estado.VerificacionNegativa;
                    }
                    UpdateButtonsImages();
                    break;
                case ApiResponseEnum.Timeout:
                    MostrarMensajeError(
                        VerificarCedulaAntiguaAsync,
                        resultado.Message);
                    break;
                case ApiResponseEnum.EndPointNotFound:
                case ApiResponseEnum.UriFormatError:
                case ApiResponseEnum.Error:
                    MostrarMensajeError(
                        null,
                        resultado.Message);
                    break;
            }
        }

        public Boolean RegistroCivil()
        {
            bool resp = false;
            RC_Wrapper rcWrapper = new RC_Wrapper(ConfigurationManager.AppSettings["UrlRC"]);
            ResponseDTO response = null;
            try
            {
                response = rcWrapper.Verify(DocType.CEDULA, LectorCodigoDeBarras.RutFormateado, LectorCodigoDeBarras.DocumentNumber);
                if (response.ResponseMessage.Equals("Vigente"))
                {
                    resp = true;
                }
                else
                {
                    resp = false;
                }
            }
            catch (Exception ex)
            {

            }
            return resp;
        }


        /// <summary>
        /// Creación del Worker de conversión de Iso a Iso CC
        /// </summary>
        /// <param name="iso"></param>
        void ConvertirIsoToIsoCCAsync(string iso)
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Visible;

            BackgroundWorker conversionWorker = new BackgroundWorker();
            conversionWorker.DoWork += ConversionWorker_DoWork;
            conversionWorker.RunWorkerCompleted += ConversionWorker_Completed;
            conversionWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Trabajo que se ejecuta en el worker de conversion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ConversionWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ProxyCaller proxy = new ProxyCaller();
                Model.Service.ApiResponse<BP_VerifyResponse> respuesta = proxy.IsoToIsoCC(
                    LectorHuellaDigital.ISO,
                    LectorHuellaDigital.SERIALNUMBER);
                if (respuesta.Status == ApiResponseEnum.Success)
                {
                    LectorHuellaDigital.ISOCC = respuesta.Data.Data.Split('-');
                    e.Result = respuesta;
                }
                else
                {
                    e.Result = respuesta;
                }
            }
            catch (TimeoutException ex)
            {
                logger.Error(ex, "El servicio de verificación ha tomado mucho tiempo en responder");
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.Timeout,
                    Message = "El servicio de verificación ha tomado mucho tiempo en responder"
                };
                e.Result = ApiResponseEnum.Timeout;
            }
            catch (EndpointNotFoundException ex)
            {
                logger.Error(ex, "El servicio de verificación de identidad no se ha encontrado. Verifique su conexión");
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.EndPointNotFound,
                    Message = "El servicio de verificación de identidad no se ha encontrado. Verifique su conexión"
                };
            }
            catch (Exception ex)
            {
                e.Result = new Model.Service.ApiResponse<BP_VerifyResponse>()
                {
                    Status = ApiResponseEnum.Error,
                    Message = "Se ha producido un error con el servicio de verificación"
                };
                logger.Error(ex, "Error no controlado en el timeout");
            }
        }

        /// <summary>
        /// Método a ejecutar cuando se termina el proceso del background worker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ConversionWorker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Collapsed;

            var resultado = (Model.Service.ApiResponse<BP_VerifyResponse>)e.Result;
            switch (resultado.Status)
            {
                case ApiResponseEnum.Success:
                    Paso = EstadoVerificacion.EsperandoLecturaContactless;
                    UpdateButtonsImages();
                    break;
                case ApiResponseEnum.Timeout:
                    MostrarMensajeErrorConParametro<String>(
                        ConvertirIsoToIsoCCAsync,
                        LectorHuellaDigital.ISO,
                        resultado.Message);
                    break;
                case ApiResponseEnum.EndPointNotFound:
                case ApiResponseEnum.UriFormatError:
                case ApiResponseEnum.Error:
                    MostrarMensajeError(
                        null,
                        resultado.Message);
                    break;
            }
        }

        /// <summary>
        /// Worker que se encarga de realizar el match on card
        /// </summary>
        void VerificarContraCedula()
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Visible;
            BackgroundWorker matchOnCardWorker = new BackgroundWorker();
            matchOnCardWorker.DoWork += VerificarConCedula_DoWork;
            matchOnCardWorker.RunWorkerCompleted += VerificacionConCedula_Completed;
            matchOnCardWorker.RunWorkerAsync();
        }

        void VerificarConCedula_DoWork(object sender, DoWorkEventArgs args)
        {
            SetupClr();
        }

        void VerificacionConCedula_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Hidden;
            if (LectorCodigoDeBarras.ExpirationDate < DateTime.Now)
            {
                ViewModel.Resultado.CedulaVencida = true;
            }
            if (ViewModel.Persona.TipoIdentificacion == TipoIdentificacion.CedulaChilenaNueva)
            {
                if (IsMoreThan18((DateTime)LectorCodigoDeBarras.BirthDate) == false)
                {
                    ViewModel.Resultado.MenorDeEdad = false;
                }
                else
                {
                    ViewModel.Resultado.MenorDeEdad = true;
                }

            }


            UpdateButtonsImages();
        }

        /// <summary>
        /// Se llama al método que consume la API de comparación de datos
        /// </summary>
        /// <param name="necData"></param>
        void VerificarCedulaAntiguaConProxy(NecDataToCompare necData)
        {
            ProxyCaller proxy = new ProxyCaller();
            Model.Service.ApiResponse<BP_VerifyResponse> respuesta = proxy.VerifyNec(
                necData,
                LectorHuellaDigital.SERIALNUMBER);
            if (respuesta.Status == ApiResponseEnum.Success)
            {
                if (respuesta.Data.VerificationResult)
                {
                    Paso = EstadoVerificacion.VerificacionPositiva;
                    ViewModel.Resultado.Estado = Estado.VerificacionPositiva;
                }
                else
                {
                    Paso = EstadoVerificacion.VerificacionNegativa;
                    ViewModel.Resultado.Estado = Estado.VerificacionNegativa;
                }
            }
            else
            {
                logger.Error($"Error en la verificación en BCRProxy {respuesta.Message}");
            }
        }

        /// <summary>
        /// Se limpia el viewmodel y se resetea el estado del paso
        /// </summary>
        void ResetControls()
        {
            logger.Info("Reestableciendo controles a estado por defecto");
            ViewModel.Persona.Nombre = String.Empty;
            ViewModel.Persona.ApellidoPaterno = String.Empty;
            ViewModel.Persona.ApellidoMaterno = String.Empty;
            ViewModel.Controles.ImagenCapturaHuella = null;

            //Bloqueo de los controles
            ViewModel.Controles.RutIsEnabled = true;
            ViewModel.Controles.NombresIsEnabled = true;
            ViewModel.Controles.ApellidoPaternoIsEnabled = true;
            ViewModel.Controles.ApellidoMaternoIsEnabled = true;
            ViewModel.Controles.TipoDocumentoIsEnabled = true;
            ViewModel.Controles.TextoSiguiente = "Siguiente";
            //ViewModel.Resultado = new Resultado();

            Paso = EstadoVerificacion.EsperandoLecturaCodigoBarras;
        }

        /// <summary>
        /// Abre el lector contactless, extrae los datos del MRZ y realiza la verificación de
        /// identidad en el caso de cédula nueva
        /// </summary>
        void SetupClr()
        {
            Int32 finger;
            try
            {
                var clr = new Bio.Core.CLR.CLR();

                clr.ReaderName = ConfigurationManager.AppSettings["CLRReaderName"];
                clr.CLR_OPEN(10);
                Thread.Sleep(100);
                logger.Trace("Entrando a CLR_CONNECT");

                clr.CLR_CONNECT(Convert.ToUInt32(5000));

                if (clr.GetLasError() == 0)
                {
                    ExtractMrzData(clr);


                    Thread.Sleep(100);

                    logger.Trace("Extracción de datos terminada");
                    logger.Trace("Comenzando Verificación contra cédula nueva");
                    bool match = clr.Match(LectorHuellaDigital.ISOCC, out finger);
                    if (match)
                    {
                        ViewModel.Resultado.Estado = Estado.VerificacionPositiva;
                    }
                    else
                    {
                        ViewModel.Resultado.Estado = Estado.VerificacionNegativa;
                    }
                    logger.Info($"SetupClr: Resultado Match: {ViewModel.Resultado.Estado}");

                    Paso = (match) ? EstadoVerificacion.VerificacionPositiva : EstadoVerificacion.VerificacionNegativa;

                    //UpdateButtonsImages();
                }
                else if (clr.GetLasError() == -1)
                {
                    MostrarMensajeError(VerificarContraCedula, "Error al detectar la cédula. Acérquela al dispositivo");
                }
                clr.CLR_DISCONNECT();
                clr.CLR_CLOSE();
            }
            catch (EndOfStreamException ex)
            {
                MostrarMensajeError(VerificarContraCedula, "Error al detectar la cédula. Acérquela al dispositivo");
                logger.Error(ex, "La cédula leída no corresponde a la huella capturada");
            }
            catch (Exception ex)
            {
                //MostrarMensajeError(SetupClr, "Error al detectar la cédula. Acérquela al dispositivo");
                MostrarMensajeError(VerificarContraCedula, "Error al detectar la cédula. Acérquela al dispositivo");
                ViewModel.Controles.BarraProgresoVisibility = Visibility.Hidden;
                logger.Error(ex, "Error con el lector Contactless");
            }
        }

        /// <summary>
        /// Se extraen los datos de la cédula y se actualiza el viewmodel
        /// </summary>
        /// <param name="clr">Lector Contactless</param>
        void ExtractMrzData(CLR clr)
        {
            logger.Info("SetupClr: Entrando a CLR_SECURITY con número de documento: " +
                LectorCodigoDeBarras.DocumentNumber);

            clr.CLR_SECURITY(
                LectorCodigoDeBarras.DocumentNumber,
                LectorCodigoDeBarras.DOB,
                LectorCodigoDeBarras.DOE);

            logger.Info("SetupClr: Entrando a CLR_GETMRZ");

            clr.CLR_GETMRZ();

            if (SolicitudVerificacion.Configuracion.ExtraerDatosPersonales)
            {
                ViewModel.Persona.ApellidoPaterno = clr.PatherLastName;
                ViewModel.Persona.Nombre = clr.Name;
                ViewModel.Persona.ApellidoMaterno = clr.MotherLastName;
                ViewModel.Persona.Serie = clr.DocumentNumber;
                ViewModel.Persona.Sexo = clr.Sex;
                //Bloque de los controles para que no puedan ser modificados
                ViewModel.Controles.NombresIsEnabled = false;
                ViewModel.Controles.ApellidoPaternoIsEnabled = false;
                ViewModel.Controles.ApellidoMaternoIsEnabled = false;

                ViewModel.Resultado.DatosPersonalesExtraidos = true;
            }
            else
            {
                ViewModel.Persona.ApellidoPaterno = "";
                ViewModel.Persona.Nombre = "";
                ViewModel.Persona.ApellidoMaterno = "";
                ViewModel.Persona.Serie = "";
                ViewModel.Persona.Sexo = "";
                //Bloque de los controles para que no puedan ser modificados
                ViewModel.Controles.NombresIsEnabled = false;
                ViewModel.Controles.ApellidoPaternoIsEnabled = false;
                ViewModel.Controles.ApellidoMaternoIsEnabled = false;

                ViewModel.Resultado.DatosPersonalesExtraidos = false;
            }

            //EXTRACCIÓN FOTOS
            if (SolicitudVerificacion.Configuracion.extraerFoto)
            {
                byte[] fotoCed = clr.CLR_GETFOTO();
                if (fotoCed != null)
                {
                    try
                    {
                        //var foto = Convert.ToBase64String(fotoCed);
                        BitmapImageCreator.Register();
                        var por = J2kImage.FromBytes(fotoCed);
                        //Bitmap img = por.As<Bitmap>();//ACA SE CAE
                        int sdf = 0;
                        Bitmap img = por.As<Bitmap>();

                        ImageFormat format = ImageFormat.Jpeg;

                        using (MemoryStream ms = new MemoryStream())
                        {
                            img.Save(ms, format);
                            ViewModel.Persona.Foto = Convert.ToBase64String(ms.ToArray());
                        }

                        ViewModel.Resultado.FotoExtraida = true;
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    ViewModel.Persona.Foto = "Null";
                    logger.Error("No se ha podido extraer la foto");
                }
            }
            else
            {
                ViewModel.Persona.Foto = "";
            }

            if (SolicitudVerificacion.Configuracion.ExtraerFirma)
            {
                byte[] fotoFir = clr.GET_CLRFIRMA();
                if (fotoFir != null)
                {
                    BitmapImageCreator.Register();
                    //var fotoF = Convert.ToBase64String(fotoFir);
                    var porF = J2kImage.FromBytes(fotoFir);
                    Bitmap imgF = porF.As<Bitmap>();

                    ImageFormat format = ImageFormat.Jpeg;

                    using (MemoryStream ms = new MemoryStream())
                    {
                        imgF.Save(ms, format);
                        ViewModel.Persona.FotoFirma = Convert.ToBase64String(ms.ToArray());
                    }

                    ViewModel.Resultado.FotoFirmaExtraida = true;
                }
                else
                {
                    ViewModel.Persona.FotoFirma = "Null";
                    logger.Error("No se ha podido extraer la foto firma");
                }
            }
            else
            {
                ViewModel.Persona.FotoFirma = "";
            }

        }

        /// <summary>
        /// Actualiza los botones de acuerdo al paso en el que se encuentra actualmente la aplicación
        /// </summary>
        void UpdateButtonsImages()
        {
            logger.Info("Actualizando controles");
            switch (Paso)
            {
                case EstadoVerificacion.EsperandoLecturaCodigoBarras:
                    ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaCompletado"];
                    ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaInactivo"];
                    ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessInactivo"];
                    //ImagenCapturaHuella.Source = new BitmapImage();
                    ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoInactivo"];
                    break;
                case EstadoVerificacion.EsperandoHuellaEscaneadaCedulaNueva:
                    ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaVerde"];
                    ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaCompletado"];
                    ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessInactivo"];
                    ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoInactivo"];
                    break;
                case EstadoVerificacion.EsperandoHuellaEscaneadaCedulaVieja:
                    ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaVerde"];
                    ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaCompletado"];
                    ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessInactivo"];
                    ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoInactivo"];
                    break;
                case EstadoVerificacion.EsperandoLecturaContactless:
                    ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaVerde"];
                    ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaVerde"];
                    ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessCompletado"];
                    ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoInactivo"];
                    break;
                case EstadoVerificacion.VerificacionPositiva:
                    ViewModel.Controles.TextoResultado = "Verificación Positiva";
                    ViewModel.Controles.TextoSiguiente = "Finalizar";
                    ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaVerde"];
                    ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaVerde"];
                    ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessVerde"];
                    ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoPositivo"];
                    break;
                case EstadoVerificacion.VerificacionNegativa:
                    ViewModel.Controles.TextoResultado = "Verificación Negativa";
                    ViewModel.Controles.TextoSiguiente = "Finalizar";
                    ViewModel.Controles.ImagenCedula = (ImageSource)GridPadre.Resources["CedulaVerde"];
                    ViewModel.Controles.ImagenHuella = (ImageSource)GridPadre.Resources["HuellaVerde"];
                    ViewModel.Controles.ImagenContactless = (ImageSource)GridPadre.Resources["ContactlessVerde"];
                    ViewModel.Controles.ImagenResultado = (ImageSource)GridPadre.Resources["ResultadoNegativo"];
                    break;
            }
        }

        /// <summary>
        /// Se cambia el ícono de la aplicación
        /// </summary>
        void CargarIcono()
        {
            if (File.Exists(ConfigurationManager.AppSettings["Logo"]))
            {
                Bitmap bitmapImage = (Bitmap)System.Drawing.Image.FromFile(ConfigurationManager.AppSettings["Logo"]);
                BitmapImage image = WpfImageHelper.PngToBitmapImage(bitmapImage);
                Logo.Source = image;
            }
        }

        /// <summary>
        /// Muestra mensaje de Error y asocia método sin parámetros al botón
        /// </summary>
        /// <param name="metodo"></param>
        /// <param name="mensaje"></param>
        void MostrarMensajeError(Action metodo, String mensaje)
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Hidden;
            ViewModel.Controles.PanelErrorVisibility = Visibility.Visible;
            ViewModel.Controles.TextoError = mensaje;
            ViewModel.Controles.SiguienteVisibility = Visibility.Hidden;
            if (metodo != null)
            {
                //Grid.SetColumnSpan(ErrorWrapper, 1);
                ViewModel.Controles.ErrorWrapperColumnSpan = 1;
                ViewModel.Controles.BotonErrorVisibility = Visibility.Visible;
                BotonError.Click += (sender, e) =>
                {
                    ViewModel.Controles.PanelErrorVisibility = Visibility.Hidden;
                    ViewModel.Controles.TextoError = String.Empty;
                    ViewModel.Controles.SiguienteVisibility = Visibility.Visible;
                    metodo();
                };
            }
            else
            {
                ViewModel.Controles.BotonErrorVisibility = Visibility.Collapsed;
                //Grid.SetColumnSpan(ErrorWrapper, 2);
                ViewModel.Controles.ErrorWrapperColumnSpan = 2;
            }
        }

        /// <summary>
        /// Muestra mensaje de Error y asocia método al botón
        /// </summary>
        /// <typeparam name="T">Genérico</typeparam>
        /// <param name="metodo">Método a asociar al botón de error</param>
        /// <param name="parametro">Parámetro que se le pasará a 'metodo'</param>
        /// <param name="mensaje">Mensaje a ser desplegado en el campo de error</param>
        void MostrarMensajeErrorConParametro<T>(
            Action<T> metodo,
            T parametro,
            String mensaje)
        {
            ViewModel.Controles.BarraProgresoVisibility = Visibility.Hidden;
            ViewModel.Controles.PanelErrorVisibility = Visibility.Visible;
            ViewModel.Controles.TextoError = mensaje;
            ViewModel.Controles.SiguienteVisibility = Visibility.Hidden;
            if (metodo != null)
            {
                //Grid.SetColumnSpan(ErrorWrapper, 1);
                ViewModel.Controles.ErrorWrapperColumnSpan = 1;
                ViewModel.Controles.BotonErrorVisibility = Visibility.Visible;
                BotonError.Click += (sender, e) =>
                {
                    //GridError.Visibility = Visibility.Hidden;
                    ViewModel.Controles.PanelErrorVisibility = Visibility.Hidden;
                    //Siguiente.Visibility = Visibility.Visible;
                    ViewModel.Controles.TextoError = String.Empty;
                    ViewModel.Controles.SiguienteVisibility = Visibility.Visible;
                    metodo(parametro);
                };
            }
            else
            {
                ViewModel.Controles.BotonErrorVisibility = Visibility.Collapsed;
                //Grid.SetColumnSpan(ErrorWrapper, 2);
                ViewModel.Controles.ErrorWrapperColumnSpan = 2;
            }
        }

        /// <summary>
        /// Cambia el fondo de los controles
        /// </summary>
        /// <param name="hexColor">Color eh formato hex. Ejemplo: #00FF00</param>
        void SetBackgroundForControls(string hexColor)
        {
            if (!String.IsNullOrEmpty(hexColor))
            {
                SolidColorBrush solidColorBrushDefault = (SolidColorBrush)GridPadre.FindResource("PrimaryBrush");
                SolidColorBrush solidColorBrushCustom = solidColorBrushDefault;
                try
                {
                    solidColorBrushCustom = (SolidColorBrush)(new BrushConverter().ConvertFrom(hexColor));
                }
                catch (FormatException ex)
                {
                    logger.Error(ex, "Error");
                }
                System.Windows.Media.Color colorResource = (System.Windows.Media.Color)GridPadre.FindResource("PrimaryColor");
                colorResource.A = 100;
                solidColorBrushDefault.Color = solidColorBrushCustom.Color;
                logger.Info("Se modificarán los controles de acuerdo al color configurado");
            }
        }

        /// <summary>
        /// Bloquea el botón de siguiente
        /// </summary>
        void BloquearSiguiente()
        {
            Siguiente.IsEnabled = false;
        }

        /// <summary>
        /// Desbloquea los controles para poder seguir con el funcionamiento
        /// </summary>
        void DesbloquearSiguiente()
        {
            //GridError.Visibility = Visibility.Hidden;
            ViewModel.Controles.PanelErrorVisibility = Visibility.Hidden;
            Siguiente.IsEnabled = true;
            ViewModel.Controles.SiguienteVisibility = Visibility.Visible;
        }

    }
}
