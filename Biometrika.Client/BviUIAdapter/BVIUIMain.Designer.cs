﻿namespace BviUIAdapter
{
    partial class BVIUIMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblRut = new System.Windows.Forms.Label();
            this.labTitleRut = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(35, 43);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(48, 48);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // lblRut
            // 
            this.lblRut.AutoSize = true;
            this.lblRut.BackColor = System.Drawing.Color.Transparent;
            this.lblRut.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRut.ForeColor = System.Drawing.Color.White;
            this.lblRut.Location = new System.Drawing.Point(307, 48);
            this.lblRut.Name = "lblRut";
            this.lblRut.Size = new System.Drawing.Size(151, 32);
            this.lblRut.TabIndex = 8;
            this.lblRut.Text = "11111111-1";
            // 
            // labTitleRut
            // 
            this.labTitleRut.AutoSize = true;
            this.labTitleRut.BackColor = System.Drawing.Color.Transparent;
            this.labTitleRut.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTitleRut.ForeColor = System.Drawing.Color.White;
            this.labTitleRut.Location = new System.Drawing.Point(110, 48);
            this.labTitleRut.Name = "labTitleRut";
            this.labTitleRut.Size = new System.Drawing.Size(199, 32);
            this.labTitleRut.TabIndex = 7;
            this.labTitleRut.Text = "Verificando RUT";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(116, 232);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "label1";
            // 
            // BVIUIMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::BviUIAdapter.Properties.Resources.theme_ligthgreen_bvi;
            this.ClientSize = new System.Drawing.Size(805, 516);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblRut);
            this.Controls.Add(this.labTitleRut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BVIUIMain";
            this.Text = "BVIUIMain";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblRut;
        private System.Windows.Forms.Label labTitleRut;
        private System.Windows.Forms.Label label1;
    }
}