﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace BviCamUIAdapter.Libs
{
    static class WpfImageHelper
    {
        const String MimeType = "image/jpeg";
        public static BitmapImage BitmapToBitmapImage(Bitmap bitmap)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                bitmap.Save(stream, ImageFormat.Bmp);
                stream.Position = 0;
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = stream;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                return bitmapImage;
            }
        }

        public static BitmapImage PngToBitmapImage(Bitmap bitmap)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                bitmap.Save(stream, ImageFormat.Png);

                stream.Position = 0;
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.StreamSource = stream;
                bitmapImage.EndInit();
                bitmapImage.Freeze();
                return bitmapImage;
            }
        }

        public static Bitmap Base64ToBitmap(string base64Bmp)
        {
            var buffer = Convert.FromBase64String(base64Bmp);
            var stream = new MemoryStream(buffer);
            stream.Position = 0;

            return (Bitmap)Image.FromStream(stream);
        }

        public static Bitmap JpegByteArrayToBitmap(byte[] jpegByteArray)
        {
            using (var ms = new MemoryStream(jpegByteArray))
            {
                return new Bitmap(ms);
            }

        }

        public static byte[] BitmapToJpegByteArray(Bitmap image)
        {
            var qualityEncoder = System.Drawing.Imaging.Encoder.Quality;
            var quality = (long)60;
            var ratio = new EncoderParameter(qualityEncoder, quality);

            var codecParams = new EncoderParameters(1);
            codecParams.Param[0] = ratio;

            ImageCodecInfo codecInfo = ImageCodecInfo.GetImageEncoders()
                .FirstOrDefault(codec => codec.MimeType.Contains(MimeType));

            using (var ms = new MemoryStream())
            {
                image.Save(ms, codecInfo, codecParams);
                return ms.ToArray();
            }
        }
    }
}
