﻿
using Easy.Common;
using NLog;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BviCamUIAdapter.Libs
{
    public class DiagnosticHelper
    {
        const string from = "soporte@biometrika.cl";
        const string to = "soporte@biometrika.cl";
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void EnviarCorreo()
        {

            var message = new MailMessage(
                from,
                to);

            message.Subject = $"Diagnóstico Biometrika Verification Service : " +
                Environment.MachineName;
            message.Body = DiagnosticReport.Generate();
            string ubicacionLog = ObtenerUbicacionLog();
            if (!String.IsNullOrEmpty(ubicacionLog))
            {
                message.Attachments.Add(new Attachment(ObtenerUbicacionLog()));
            }

            MailHelper.SendEmail(message);
        }

        string ObtenerUbicacionLog()
        {
            try
            {                
                var fileTarget = (FileTarget)LogManager.Configuration.FindTargetByName("f");
                var logEventInfo = new LogEventInfo { TimeStamp = DateTime.Now };
                string location = fileTarget.FileName.Render(logEventInfo);
                return location;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error obteniendo la ubicación del Log");
                return String.Empty;
            }
        }
    }

}
