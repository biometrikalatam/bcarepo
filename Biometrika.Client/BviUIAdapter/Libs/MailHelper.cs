﻿
using NLog;
using System;
using System.Net;
using System.Net.Mail;


namespace BviCamUIAdapter.Libs
{
    public static class MailHelper
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        static MailMessage message;
        const int smtpPort = 25;
        const string adminMail = @"soporte@biometrika.cl";
        const string adminPassword = @"B1ometrika2014";
        const string smtpServer = @"smtp.gmail.com";

        public static bool SendEmail(MailMessage mensaje)
        {
            message = mensaje;

            SmtpClient client = new SmtpClient();
            client.Port = smtpPort;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.UseDefaultCredentials = true;
            client.Host = smtpServer;
            client.Credentials = new NetworkCredential(
                adminMail,
                adminPassword);
            client.Timeout = 20000;

            try
            {
                client.SendAsync(message, null);
                client.SendCompleted += Client_SendCompleted;

                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error al enviar el reporte de marcas por e-mail");
                return false;
            }
        }

        private static void Client_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                logger.Info("El correo fue enviado exitosamente");
            }
            else
            {
                logger.Error($"Errar al enviar el correo: {e.Error.Message}");
            }
            message.Dispose();
        }
    }
}
