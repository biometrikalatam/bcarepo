﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.License.GUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            string format = "dd/MM/yyyy";
            CultureInfo provider = CultureInfo.InvariantCulture;
            string s = DateTime.Now.ToString(format, provider);

            string sa = "20/01/2018";
            //string format = "MM/dd/yyyy";
            //CultureInfo provider = CultureInfo.InvariantCulture;
            //DateTime dt = DateTime.ParseExact(s, format, provider);
            //int i = 0;

            //List<Common.BKLicenseAtributoAdicional> itemList = new List<Common.BKLicenseAtributoAdicional>();
            //Common.BKLicenseAtributoAdicional it = new Common.BKLicenseAtributoAdicional();
            //it.Nombre = "NombreAtr1";
            //it.Valor = "ValorAtr1";
            //itemList.Add(it);
            //Biometrika.License.Common.BKLicense lic =
            //    new Common.BKLicense("Cliente 1", DateTime.Now, DateTime.Now.AddDays(30), "BP4", "BioPortal Server 4",
            //                         "Tiral", "Keykeykeykey", itemList);
            //Biometrika.License.Common.XmlUtils.SerializeAnObject(lic);

            //Biometrika.License.Common.BKProducts bKProducts = new Common.BKProducts();
            //Biometrika.License.Common.BKProduct bKProduct = new Common.BKProduct();
            //bKProduct.Name = "BioPortal Server 4";
            //bKProduct.Code = "BP4";
            //bKProducts.Products = new List<Common.BKProduct>();
            //bKProducts.Products.Add(bKProduct);
            //System.IO.File.WriteAllText("Products.xml", Biometrika.License.Common.XmlUtils.SerializeAnObject(bKProducts));

            //string sRet = null;
            //string sHDD = "";
            //bool isFirst;

            //sRet = GetIP() + " == " + GetMACAddress();

            //ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");
            //ManagementObject HardDrive;
            //int i = 0;
            //foreach (ManagementObject wmi_HD in searcher.Get())
            //{
            //    // get the hard drive from collection
            //    // using index
            //    ManagementObject hd = (ManagementObject)wmi_HD;
            //    isFirst = false;

            //    // get the hardware serial no.
            //    if (isFirst)
            //    {
            //        if (wmi_HD["SerialNumber"] != null)
            //            sHDD += wmi_HD["SerialNumber"].ToString();
            //    }
            //    else
            //    {
            //        string s1 = "";

            //        if (((string)(wmi_HD["MediaType"])).Equals("Fixed hard disk media") && wmi_HD["SerialNumber"] != null)
            //        {
            //            sHDD += "|" + wmi_HD["SerialNumber"].ToString();


            //            s1 += "|Model: " + wmi_HD["Name"].ToString();

            //            s1 += "|Serial: " + wmi_HD["SerialNumber"].ToString();

            //            s1 += "|Interface: " + wmi_HD["InterfaceType"].ToString();

            //            s1 += "Type: " + wmi_HD["MediaType"].ToString();
            //            int j = 0;
            //        }
            //    }
            //}

            //ManagementClass oMClass = new ManagementClass("Win32_NetworkAdapterConfiguration");
            //ManagementObjectCollection colMObj = oMClass.GetInstances();
            //string s2 = "";
            //foreach (ManagementObject objMO in colMObj)
            //{

            //    if (objMO["MacAddress"] != null)
            //    {
            //        s2 += "|Interface:" + objMO["MacAddress"].ToString();
            //    }
            //}

            ////NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
            ////List<string> strArray = new List<string>();
            ////string mac = "";
            ////foreach (NetworkInterface adapter in interfaces)
            ////{

            ////    PhysicalAddress address = adapter.GetPhysicalAddress();

            ////    mac += "|Mac=" + address.GetAddressBytes().ToString();

            ////    //strArray.Add((b).ToString() + " ");
            ////    int a = 0;

            ////}

            //List<string> strArray = new List<string>();

            //NetworkInterface[] networkCards = NetworkInterface.GetAllNetworkInterfaces();
            //string mac = "";
            //foreach (NetworkInterface nic in networkCards)
            //{
            //    if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
            //    {
            //        mac += "|Mac=" + nic.GetPhysicalAddress().ToString();
            //        break;
            //    }
            //}

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }

        static public string GetMACAddress()
        {
            String sMacAddress = "";
            bool isFirst = true;
            try
            {
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface adapter in nics)
                {
                    //if (sMacAddress == String.Empty)// only return MAC Address from first card
                    //{
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    if (isFirst)
                    {
                        sMacAddress = FormatMAC(adapter.GetPhysicalAddress().ToString());
                        isFirst = false;
                    }
                    else
                    {
                        sMacAddress = sMacAddress + "~" + FormatMAC(adapter.GetPhysicalAddress().ToString());
                    }
                    //}
                }
            }
            catch (Exception ex)
            {
                //WriteLog("ERROR - GetMACAddress Error - " + ex.Message);
            }
            return sMacAddress;
        }

        static private string FormatMAC(string smac)
        {
            String sMacAddress = "";
            try
            {
                bool isFirst = true;
                if (!String.IsNullOrEmpty(smac))
                {
                    for (int i = 0; i < smac.Length; i++)
                    {
                        if (i % 2 == 0)
                        {
                            if (isFirst)
                            {
                                sMacAddress = smac.Substring(i, 2);
                                isFirst = false;
                            }
                            else
                            {
                                if (smac.Length >= i + 2)
                                {
                                    sMacAddress += "-" + smac.Substring(i, 2);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //WriteLog("ERROR - FormatMAC Error - " + ex.Message);
            }
            return sMacAddress;
        }

        static private string GetIP()
        {
            String sIP = "";
            bool isFirst = true;
            try
            {
                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
                foreach (IPAddress ipAddress in localIPs)
                {
                    if (ipAddress.ToString().Length <= 15)
                    {
                        if (isFirst)
                        {
                            sIP = ipAddress.ToString();
                            isFirst = false;
                        }
                        else
                        {
                            sIP = sIP + "~" + ipAddress.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //WriteLog("ERROR - GetMACAddress Error - " + ex.Message);
            }
            return sIP;
        }
    }
}
