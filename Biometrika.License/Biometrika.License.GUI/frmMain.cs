﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.License.GUI
{
    public partial class frmMain : Form
    {

        License.Check.LicenseCheck oCHECK; 

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            cbTipoLicencia.SelectedIndex = 0;
            cbProductos.SelectedIndex = 0;
            lbAtrAdic.Items.Clear();

            oCHECK = new Check.LicenseCheck();

            Common.BKProducts bkProducts = Common.XmlUtils.DeserializeObject<Common.BKProducts>(System.IO.File.ReadAllText("Products.xml"));
            cbProductos.Items.Clear();
            if (bkProducts != null && bkProducts.Products != null && bkProducts.Products.Count > 0) {
                foreach (Common.BKProduct item in bkProducts.Products)
                {
                    cbProductos.Items.Add(item.Code + "-" + item.Name);
                }
            } else
            {
                cbProductos.Items.Add("BP4-BioPortal Server 4");
            }
        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(rtbKey.Text))
            {
                rtbKey.Text = Common.Utils.GetPlainKey(rtbKey.Text.Trim());
            }
        }

        private void btnAddAtrAdic_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtNameAtrAdic.Text) && !String.IsNullOrEmpty(txtValueAtrAdic.Text))
            {
                lbAtrAdic.Items.Add(txtNameAtrAdic.Text.Trim() + ":" + txtValueAtrAdic.Text);
            }
        }

        private void btnDeleteAtrAdic_Click(object sender, EventArgs e)
        {
            try
            {
                lbAtrAdic.Items.RemoveAt(lbAtrAdic.SelectedIndex);
            }
            catch (Exception ex)
            {
                
            }
        }

        private void btnEditAtrAdic_Click(object sender, EventArgs e)
        {
            try
            {
                string atr = (string)lbAtrAdic.Items[lbAtrAdic.SelectedIndex];
                string[] arratr = atr.Split(':');
                lbAtrAdic.Items.RemoveAt(lbAtrAdic.SelectedIndex);
                txtNameAtrAdic.Text = arratr[0];
                txtValueAtrAdic.Text = arratr[1];
            }
            catch (Exception ex)
            {

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
//                DialogResult dr = saveFileDialog1.ShowDialog(this);
                DialogResult dr = openFileDialog1.ShowDialog(this);

                if (dr.Equals(DialogResult.OK))
                {
                    txtPathDestino.Text = openFileDialog1.FileName.Substring(0, openFileDialog1.FileName.IndexOf(openFileDialog1.SafeFileName));
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Seleccione un archivo de licencia...";
                //openFileDialog1.Filter = "xml|*.xml";
                DialogResult dr = openFileDialog1.ShowDialog(this);

                if (dr.Equals(DialogResult.OK))
                {
                    txtPathLicense.Text = openFileDialog1.FileName;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string[] arratr;
            Common.BKLicenseAtributoAdicional itemList = null;
            List<Common.BKLicenseAtributoAdicional> listAtrAdic = null;
            try
            {
                if (String.IsNullOrEmpty(txtPathDestino.Text))
                {
                    MessageBox.Show(this, "Debe seleccionar un path de destino...", "Atencion...", 
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {

                    string tipolicense = (string)cbTipoLicencia.Items[cbTipoLicencia.SelectedIndex];
                    string[] aux = ((string)cbProductos.Items[cbProductos.SelectedIndex]).Split('-');
                    string codigo = aux[0];
                    string producto = aux[1];

                    if (lbAtrAdic.Items.Count > 0)
                    {
                        listAtrAdic = new List<Common.BKLicenseAtributoAdicional>();

                        foreach (string item in lbAtrAdic.Items)
                        {
                            arratr = item.Split(':');
                            itemList = new Common.BKLicenseAtributoAdicional();
                            itemList.Nombre = arratr[0];
                            itemList.Valor = arratr[1];
                            listAtrAdic.Add(itemList);
                        }
                    }

                    //Si es volumen debemos agregar dos variables para indicarlo: IsLicenseVolume y QVolumeTotal
                    if (chkIsVolumeLicense.Checked)
                    {
                        itemList = new Common.BKLicenseAtributoAdicional();
                        itemList.Nombre = "IsLicenseVolume";
                        itemList.Valor = "true";
                        listAtrAdic.Add(itemList);
                        itemList = new Common.BKLicenseAtributoAdicional();
                        itemList.Nombre = "QVolumeTotal";
                        itemList.Valor = txtQTotalVolume.Text.Trim();
                        listAtrAdic.Add(itemList);
                    }

                    string sLicense = Generator.LicenseGenerator.GenerateLicense(tipolicense, txtCliente.Text,
                                          txtFD.Text, txtFH.Text, txtDaysTrial.Text, rtbKey.Text.Trim(), codigo, producto, listAtrAdic);

                    string filename = tipolicense + "_" + codigo + "_" + txtCliente.Text + "_" + DateTime.Now.ToString("yyMMddHHmmss") + ".txt";

                    System.IO.File.WriteAllText(txtPathDestino.Text + filename, sLicense);

                    MessageBox.Show(this, "Licencia generada y salvada en: " + txtPathDestino.Text + filename, "Atencion...",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error => " + ex.Message, "Atencion...", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReadLicense_Click(object sender, EventArgs e)
        {
            string[] arratr;
            Common.BKLicenseAtributoAdicional itemList = null;
            List<Common.BKLicenseAtributoAdicional> listAtrAdic = null;
            string msg;
            try
            {
                if (String.IsNullOrEmpty(txtPathLicense.Text))
                {
                    MessageBox.Show(this, "Debe seleccionar un archivo de licencia...", "Atencion...",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string sLicense = System.IO.File.ReadAllText(txtPathLicense.Text);

                    Common.BKLicense lic = null; 

                    if (chkCheckLic.Checked)
                    {
                        int ret = oCHECK.CheckLicense(txtPathLicense.Text, txtCode.Text, 0, out lic, out msg);
                        if (ret == 0)
                        {
                            MessageBox.Show(this, "LICENCIA VALIDA!", "Atención...");
                        } else
                        {
                            MessageBox.Show(this, "LICENCIA INVALIDA! [" + ret + "]", "Atención...");
                        }
                    }
                    else
                    {
                        lic = Common.Utils.OGetLicense(sLicense);
                    }

                    if (lic != null)
                    { 
                        SelectComboTipoLicencia(lic.TipoLicencia);
                        SelectComboProducto(lic.Codigo);
                        txtCliente.Text = lic.Cliente;
                        string format = "dd/MM/yyyy";
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        txtFD.Text = lic.FechaDesde.ToString(format, provider);
                        txtFH.Text = lic.FechaHasta.ToString(format, provider);
                        rtbKey.Text = lic.Key;
                        lbAtrAdic.Items.Clear();
                        if (lic.AtributosAdicionales != null)
                        {
                            foreach (Common.BKLicenseAtributoAdicional item in lic.AtributosAdicionales)
                            {
                                lbAtrAdic.Items.Add(item.Nombre + ":" + item.Valor);
                            }
                        }
                    } else
                    {
                        MessageBox.Show(this, "Error parseando o verificando licencia...", "Atención...");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error => " + ex.Message, "Atencion...", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SelectComboProducto(string codigo)
        {
            try
            {
                for (int i = 0; i < cbProductos.Items.Count; i++)
                {
                    if (cbProductos.Items[i].ToString().IndexOf(codigo) >= 0)
                    {
                        cbProductos.SelectedIndex = i;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private void SelectComboTipoLicencia(string tipoLicencia)
        {
            try
            {
                for (int i = 0; i < cbTipoLicencia.Items.Count; i++)
                {
                    if (cbTipoLicencia.Items[i].ToString().IndexOf(tipoLicencia) >= 0)
                    {
                        cbTipoLicencia.SelectedIndex = i;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btnEraseAll_Click(object sender, EventArgs e)
        {
            txtCliente.Text = "";
            txtCode.Text = "";
            txtFD.Text = "";
            txtFH.Text = "";
            txtKey.Text = "";
            txtNameAtrAdic.Text = "";
            txtValueAtrAdic.Text = "";
            txtPathDestino.Text = "";
            txtPathLicense.Text = "";
            cbProductos.SelectedIndex = 0;
            cbTipoLicencia.SelectedIndex = 0;
            chkCheckLic.Checked = false;
            lbAtrAdic.Items.Clear();
            rtbKey.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void chkIsVolumeLicense_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsVolumeLicense.Checked)
            {
                txtQTotalVolume.Enabled = true;
            } else
            {
                txtQTotalVolume.Enabled = false;
            }
        }
    }
}
