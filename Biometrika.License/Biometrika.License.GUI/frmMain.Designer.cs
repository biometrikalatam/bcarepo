﻿namespace Biometrika.License.GUI
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCheckToString = new System.Windows.Forms.Button();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkCheckLic = new System.Windows.Forms.CheckBox();
            this.btnReadLicense = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtPathLicense = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnEraseAll = new System.Windows.Forms.Button();
            this.btnDecrypt = new System.Windows.Forms.Button();
            this.rtbKey = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbProductos = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbTipoLicencia = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFH = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFD = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.grpIsVolume = new System.Windows.Forms.GroupBox();
            this.txtQTotalVolume = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.chkIsVolumeLicense = new System.Windows.Forms.CheckBox();
            this.lbAtrAdic = new System.Windows.Forms.ListBox();
            this.btnDeleteAtrAdic = new System.Windows.Forms.Button();
            this.btnEditAtrAdic = new System.Windows.Forms.Button();
            this.btnAddAtrAdic = new System.Windows.Forms.Button();
            this.txtValueAtrAdic = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNameAtrAdic = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.txtPathDestino = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtDaysTrial = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.grpIsVolume.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(340, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(527, 30);
            this.label1.TabIndex = 3;
            this.label1.Text = "Herramienta para la generación de licencias de productos Biometrika...";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Biometrika.License.GUI.Properties.Resources.Biometrika_Bajada_Big_250px;
            this.pictureBox1.Location = new System.Drawing.Point(54, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(250, 45);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCheckToString);
            this.groupBox1.Controls.Add(this.txtCode);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.chkCheckLic);
            this.groupBox1.Controls.Add(this.btnReadLicense);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.txtPathLicense);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(28, 92);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(856, 81);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tomar Licencia Base...";
            // 
            // btnCheckToString
            // 
            this.btnCheckToString.Location = new System.Drawing.Point(663, 46);
            this.btnCheckToString.Name = "btnCheckToString";
            this.btnCheckToString.Size = new System.Drawing.Size(146, 20);
            this.btnCheckToString.TabIndex = 7;
            this.btnCheckToString.Text = "&Check ToString...";
            this.btnCheckToString.UseVisualStyleBackColor = true;
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(571, 46);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(86, 20);
            this.txtCode.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(519, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Código :";
            // 
            // chkCheckLic
            // 
            this.chkCheckLic.AutoSize = true;
            this.chkCheckLic.Location = new System.Drawing.Point(63, 46);
            this.chkCheckLic.Name = "chkCheckLic";
            this.chkCheckLic.Size = new System.Drawing.Size(115, 17);
            this.chkCheckLic.TabIndex = 4;
            this.chkCheckLic.Text = "Chequear Licencia";
            this.chkCheckLic.UseVisualStyleBackColor = true;
            // 
            // btnReadLicense
            // 
            this.btnReadLicense.Location = new System.Drawing.Point(184, 43);
            this.btnReadLicense.Name = "btnReadLicense";
            this.btnReadLicense.Size = new System.Drawing.Size(146, 20);
            this.btnReadLicense.TabIndex = 3;
            this.btnReadLicense.Text = "&Leer Licencia...";
            this.btnReadLicense.UseVisualStyleBackColor = true;
            this.btnReadLicense.Click += new System.EventHandler(this.btnReadLicense_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(815, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(24, 20);
            this.button1.TabIndex = 2;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtPathLicense
            // 
            this.txtPathLicense.Location = new System.Drawing.Point(63, 20);
            this.txtPathLicense.Name = "txtPathLicense";
            this.txtPathLicense.Size = new System.Drawing.Size(746, 20);
            this.txtPathLicense.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Archivo :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtDaysTrial);
            this.groupBox2.Controls.Add(this.btnEraseAll);
            this.groupBox2.Controls.Add(this.btnDecrypt);
            this.groupBox2.Controls.Add(this.rtbKey);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.cbProductos);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtKey);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.cbTipoLicencia);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtFH);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtFD);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtCliente);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(28, 179);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(856, 466);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos de la Licencia...";
            // 
            // btnEraseAll
            // 
            this.btnEraseAll.Location = new System.Drawing.Point(663, 436);
            this.btnEraseAll.Name = "btnEraseAll";
            this.btnEraseAll.Size = new System.Drawing.Size(176, 20);
            this.btnEraseAll.TabIndex = 17;
            this.btnEraseAll.Text = "Borrar Todo";
            this.btnEraseAll.UseVisualStyleBackColor = true;
            this.btnEraseAll.Click += new System.EventHandler(this.btnEraseAll_Click);
            // 
            // btnDecrypt
            // 
            this.btnDecrypt.Location = new System.Drawing.Point(123, 283);
            this.btnDecrypt.Name = "btnDecrypt";
            this.btnDecrypt.Size = new System.Drawing.Size(83, 20);
            this.btnDecrypt.TabIndex = 16;
            this.btnDecrypt.Text = "Desencriptar Key...";
            this.btnDecrypt.UseVisualStyleBackColor = true;
            this.btnDecrypt.Click += new System.EventHandler(this.btnDecrypt_Click);
            // 
            // rtbKey
            // 
            this.rtbKey.Location = new System.Drawing.Point(15, 309);
            this.rtbKey.Name = "rtbKey";
            this.rtbKey.Size = new System.Drawing.Size(824, 121);
            this.rtbKey.TabIndex = 15;
            this.rtbKey.Text = resources.GetString("rtbKey.Text");
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(23, 287);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Key de GetInfo :";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // cbProductos
            // 
            this.cbProductos.FormattingEnabled = true;
            this.cbProductos.Items.AddRange(new object[] {
            "BP4 - BioPortal Server 4",
            "BP5 - BioPortal Server 5",
            "BP6 - BioPortal Server 6",
            "BVI - Biometrika Verification"});
            this.cbProductos.Location = new System.Drawing.Point(95, 117);
            this.cbProductos.Name = "cbProductos";
            this.cbProductos.Size = new System.Drawing.Size(360, 21);
            this.cbProductos.TabIndex = 13;
            this.cbProductos.Text = "BP4 - BioPortal Server 4";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 120);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Producto :";
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(95, 207);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(360, 20);
            this.txtKey.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(44, 210);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Key :";
            // 
            // cbTipoLicencia
            // 
            this.cbTipoLicencia.FormattingEnabled = true;
            this.cbTipoLicencia.Items.AddRange(new object[] {
            "Trial",
            "Produccion"});
            this.cbTipoLicencia.Location = new System.Drawing.Point(95, 164);
            this.cbTipoLicencia.Name = "cbTipoLicencia";
            this.cbTipoLicencia.Size = new System.Drawing.Size(191, 21);
            this.cbTipoLicencia.TabIndex = 9;
            this.cbTipoLicencia.Text = "Trial";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Tipo Licencia :";
            // 
            // txtFH
            // 
            this.txtFH.Location = new System.Drawing.Point(255, 54);
            this.txtFH.Name = "txtFH";
            this.txtFH.Size = new System.Drawing.Size(132, 20);
            this.txtFH.TabIndex = 7;
            this.txtFH.Text = "28/02/2018";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(208, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Hasta :";
            // 
            // txtFD
            // 
            this.txtFD.Location = new System.Drawing.Point(63, 54);
            this.txtFD.Name = "txtFD";
            this.txtFD.Size = new System.Drawing.Size(132, 20);
            this.txtFD.TabIndex = 5;
            this.txtFD.Text = "01/01/2018";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Desde :";
            // 
            // txtCliente
            // 
            this.txtCliente.Location = new System.Drawing.Point(62, 28);
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.Size = new System.Drawing.Size(393, 20);
            this.txtCliente.TabIndex = 3;
            this.txtCliente.Text = "Cliente";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Cliente :";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.grpIsVolume);
            this.groupBox3.Controls.Add(this.lbAtrAdic);
            this.groupBox3.Controls.Add(this.btnDeleteAtrAdic);
            this.groupBox3.Controls.Add(this.btnEditAtrAdic);
            this.groupBox3.Controls.Add(this.btnAddAtrAdic);
            this.groupBox3.Controls.Add(this.txtValueAtrAdic);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txtNameAtrAdic);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(474, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(365, 274);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Atributos Adicionales";
            // 
            // grpIsVolume
            // 
            this.grpIsVolume.Controls.Add(this.txtQTotalVolume);
            this.grpIsVolume.Controls.Add(this.label13);
            this.grpIsVolume.Controls.Add(this.chkIsVolumeLicense);
            this.grpIsVolume.Location = new System.Drawing.Point(28, 214);
            this.grpIsVolume.Name = "grpIsVolume";
            this.grpIsVolume.Size = new System.Drawing.Size(313, 54);
            this.grpIsVolume.TabIndex = 18;
            this.grpIsVolume.TabStop = false;
            // 
            // txtQTotalVolume
            // 
            this.txtQTotalVolume.Enabled = false;
            this.txtQTotalVolume.Location = new System.Drawing.Point(125, 23);
            this.txtQTotalVolume.Name = "txtQTotalVolume";
            this.txtQTotalVolume.Size = new System.Drawing.Size(109, 20);
            this.txtQTotalVolume.TabIndex = 7;
            this.txtQTotalVolume.Text = "1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(40, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Cantidad Total:";
            // 
            // chkIsVolumeLicense
            // 
            this.chkIsVolumeLicense.AutoSize = true;
            this.chkIsVolumeLicense.Location = new System.Drawing.Point(20, 0);
            this.chkIsVolumeLicense.Name = "chkIsVolumeLicense";
            this.chkIsVolumeLicense.Size = new System.Drawing.Size(214, 17);
            this.chkIsVolumeLicense.TabIndex = 0;
            this.chkIsVolumeLicense.Text = "Es Licencia de Volumen (Center Check)";
            this.chkIsVolumeLicense.UseVisualStyleBackColor = true;
            this.chkIsVolumeLicense.CheckedChanged += new System.EventHandler(this.chkIsVolumeLicense_CheckedChanged);
            // 
            // lbAtrAdic
            // 
            this.lbAtrAdic.FormattingEnabled = true;
            this.lbAtrAdic.Items.AddRange(new object[] {
            "IP: 127.0.0.1",
            "QConcurrent: 50"});
            this.lbAtrAdic.Location = new System.Drawing.Point(28, 126);
            this.lbAtrAdic.Name = "lbAtrAdic";
            this.lbAtrAdic.Size = new System.Drawing.Size(313, 82);
            this.lbAtrAdic.TabIndex = 13;
            // 
            // btnDeleteAtrAdic
            // 
            this.btnDeleteAtrAdic.Location = new System.Drawing.Point(117, 100);
            this.btnDeleteAtrAdic.Name = "btnDeleteAtrAdic";
            this.btnDeleteAtrAdic.Size = new System.Drawing.Size(83, 20);
            this.btnDeleteAtrAdic.TabIndex = 12;
            this.btnDeleteAtrAdic.Text = "&Eliminar...";
            this.btnDeleteAtrAdic.UseVisualStyleBackColor = true;
            this.btnDeleteAtrAdic.Click += new System.EventHandler(this.btnDeleteAtrAdic_Click);
            // 
            // btnEditAtrAdic
            // 
            this.btnEditAtrAdic.Location = new System.Drawing.Point(28, 100);
            this.btnEditAtrAdic.Name = "btnEditAtrAdic";
            this.btnEditAtrAdic.Size = new System.Drawing.Size(83, 20);
            this.btnEditAtrAdic.TabIndex = 11;
            this.btnEditAtrAdic.Text = "&Editar...";
            this.btnEditAtrAdic.UseVisualStyleBackColor = true;
            this.btnEditAtrAdic.Click += new System.EventHandler(this.btnEditAtrAdic_Click);
            // 
            // btnAddAtrAdic
            // 
            this.btnAddAtrAdic.Location = new System.Drawing.Point(118, 72);
            this.btnAddAtrAdic.Name = "btnAddAtrAdic";
            this.btnAddAtrAdic.Size = new System.Drawing.Size(177, 20);
            this.btnAddAtrAdic.TabIndex = 10;
            this.btnAddAtrAdic.Text = "&Agregar Atributo Adicional...";
            this.btnAddAtrAdic.UseVisualStyleBackColor = true;
            this.btnAddAtrAdic.Click += new System.EventHandler(this.btnAddAtrAdic_Click);
            // 
            // txtValueAtrAdic
            // 
            this.txtValueAtrAdic.Location = new System.Drawing.Point(76, 45);
            this.txtValueAtrAdic.Name = "txtValueAtrAdic";
            this.txtValueAtrAdic.Size = new System.Drawing.Size(219, 20);
            this.txtValueAtrAdic.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Valor :";
            // 
            // txtNameAtrAdic
            // 
            this.txtNameAtrAdic.Location = new System.Drawing.Point(76, 19);
            this.txtNameAtrAdic.Name = "txtNameAtrAdic";
            this.txtNameAtrAdic.Size = new System.Drawing.Size(219, 20);
            this.txtNameAtrAdic.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Nombre :";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button3);
            this.groupBox4.Controls.Add(this.button4);
            this.groupBox4.Controls.Add(this.txtPathDestino);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Location = new System.Drawing.Point(28, 660);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(856, 81);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Path Destino Licencia...";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(663, 46);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(146, 20);
            this.button3.TabIndex = 3;
            this.button3.Text = "Crear Licencia...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(815, 20);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(24, 20);
            this.button4.TabIndex = 2;
            this.button4.Text = "...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtPathDestino
            // 
            this.txtPathDestino.Location = new System.Drawing.Point(63, 20);
            this.txtPathDestino.Name = "txtPathDestino";
            this.txtPathDestino.Size = new System.Drawing.Size(746, 20);
            this.txtPathDestino.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Archivo :";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(691, 757);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(146, 20);
            this.button2.TabIndex = 7;
            this.button2.Text = "Salir";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Title = "Seleccione path para grabar el archivo de licencia...";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // txtDaysTrial
            // 
            this.txtDaysTrial.Location = new System.Drawing.Point(332, 165);
            this.txtDaysTrial.Name = "txtDaysTrial";
            this.txtDaysTrial.Size = new System.Drawing.Size(91, 20);
            this.txtDaysTrial.TabIndex = 18;
            this.txtDaysTrial.Text = "30";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(292, 167);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "Dias :";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 790);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Biometrika License Generator GUI v5...";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.grpIsVolume.ResumeLayout(false);
            this.grpIsVolume.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCheckToString;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkCheckLic;
        private System.Windows.Forms.Button btnReadLicense;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtPathLicense;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbTipoLicencia;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFH;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtFD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCliente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cbProductos;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox lbAtrAdic;
        private System.Windows.Forms.Button btnDeleteAtrAdic;
        private System.Windows.Forms.Button btnEditAtrAdic;
        private System.Windows.Forms.Button btnAddAtrAdic;
        private System.Windows.Forms.TextBox txtValueAtrAdic;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNameAtrAdic;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox rtbKey;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnEraseAll;
        private System.Windows.Forms.Button btnDecrypt;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtPathDestino;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox grpIsVolume;
        private System.Windows.Forms.TextBox txtQTotalVolume;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkIsVolumeLicense;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtDaysTrial;
    }
}

