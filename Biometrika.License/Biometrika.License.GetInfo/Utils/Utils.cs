﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace Biometrika.License.GetInfo.Utils
{
    public class Utils
    {

        public static string GetSerialHHD()
        {
            string ret = "";

            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");
                int i = 0;
                bool isFirst = true;
                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    // get the hard drive from collection
                    // using index
                    ManagementObject hd = (ManagementObject)wmi_HD;
                    

                    // get the hardware serial no.
                    if (isFirst)
                    {
                        if (((string)(wmi_HD["MediaType"])).Equals("Fixed hard disk media") && wmi_HD["SerialNumber"] != null)
                        {
                            ret = wmi_HD["SerialNumber"].ToString().Trim();
                            isFirst = false;
                        }
                    }
                    else
                    {
                        if (((string)(wmi_HD["MediaType"])).Equals("Fixed hard disk media") && wmi_HD["SerialNumber"] != null)
                        {
                            ret += "~" + wmi_HD["SerialNumber"].ToString().Trim();
                            //s1 += "|Model: " + wmi_HD["Name"].ToString();
                            //s1 += "|Serial: " + wmi_HD["SerialNumber"].ToString();
                            //s1 += "|Interface: " + wmi_HD["InterfaceType"].ToString();
                            //s1 += "Type: " + wmi_HD["MediaType"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                ret = "";
            }
            return ret;
        }

        static public string GetMACAddress()
        {
            String sMacAddress = "";
            bool isFirst = true;
            try
            {
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface adapter in nics)
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    if (adapter.GetPhysicalAddress() != null && adapter.GetPhysicalAddress().ToString().Length > 0)
                    {
                        if (isFirst)
                        {
                            sMacAddress = FormatMAC(adapter.GetPhysicalAddress().ToString());
                            isFirst = false;
                        }
                        else
                        {
                            sMacAddress = sMacAddress + "~" + FormatMAC(adapter.GetPhysicalAddress().ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            return sMacAddress;
        }

        static private string FormatMAC(string smac)
        {
            String sMacAddress = "";
            try
            {
                bool isFirst = true;
                if (!String.IsNullOrEmpty(smac))
                {
                    for (int i = 0; i < smac.Length; i++)
                    {
                        if (i % 2 == 0)
                        {
                            if (isFirst)
                            {
                                sMacAddress = smac.Substring(i, 2);
                                isFirst = false;
                            } else
                            {
                                if (smac.Length >= i + 2)
                                {
                                    sMacAddress += "-" + smac.Substring(i, 2);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //WriteLog("ERROR - FormatMAC Error - " + ex.Message);
            }
            return sMacAddress;
        }

        static public string GetIP()
        {
            String sIP = "";
            bool isFirst = true;
            try
            {
                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
                foreach (IPAddress ipAddress in localIPs)
                {
                    if (ipAddress.ToString().Length <= 15)
                    {
                        if (isFirst)
                        {
                            sIP = ipAddress.ToString();
                            isFirst = false;
                        }
                        else
                        {
                            sIP = sIP + "~" + ipAddress.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            return sIP;
        }

    }
}
