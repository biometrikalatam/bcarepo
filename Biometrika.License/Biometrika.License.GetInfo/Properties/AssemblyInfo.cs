﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Biometrika.License.GetInfo")]
[assembly: AssemblyDescription("Generador de Key para Licencia de Producto Biometrika")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Biometrika S.A.")]
[assembly: AssemblyProduct("Biometrika.License.GetInfo")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("Biometrika")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("65b545f7-20ae-41b2-9bbe-55e71e6b95ed")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("5.1.*")]
[assembly: AssemblyFileVersion("5.1.*")]
