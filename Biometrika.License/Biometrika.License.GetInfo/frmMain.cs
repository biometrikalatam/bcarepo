﻿using System;
using System.Windows.Forms;
using Biometrika.License.GetInfo.Utils;

namespace Biometrika.License.GetInfo
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            try
            {
                //labKey.Text = Utils.Utils.GetSerialHHD() + "|" + Utils.Utils.GetMACAddress() + "|" + Utils.Utils.GetIP();
                labKey.Text = Crypto.EncryptWithSymmetricAid(Crypto.PUBLIC_KEY, Utils.Utils.GetSerialHHD() + "|" + Utils.Utils.GetMACAddress() + "|" + Utils.Utils.GetIP());
            }
            catch (Exception ex)
            {
                labKey.Text = "Error => " + ex.Message;
            }
        }

        private void btnErase_Click(object sender, EventArgs e)
        {
            labKey.Text = "";
        }

        private void btnOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(labKey.Text))
            {
                Clipboard.SetText(labKey.Text);
            }
        }
    }
}
