﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace Biometrika.License.Common
{
    public class Utils
    {

        public static string GetSerialHHD()
        {
            string ret = "";

            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");
                int i = 0;
                bool isFirst = true;
                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    // get the hard drive from collection
                    // using index
                    ManagementObject hd = (ManagementObject)wmi_HD;
                    

                    // get the hardware serial no.
                    if (isFirst)
                    {
                        if (((string)(wmi_HD["MediaType"])).Equals("Fixed hard disk media") && wmi_HD["SerialNumber"] != null)
                        {
                            ret = wmi_HD["SerialNumber"].ToString().Trim();
                            isFirst = false;
                        }
                    }
                    else
                    {
                        if (((string)(wmi_HD["MediaType"])).Equals("Fixed hard disk media") && wmi_HD["SerialNumber"] != null)
                        {
                            ret += "~" + wmi_HD["SerialNumber"].ToString().Trim();
                            //s1 += "|Model: " + wmi_HD["Name"].ToString();
                            //s1 += "|Serial: " + wmi_HD["SerialNumber"].ToString();
                            //s1 += "|Interface: " + wmi_HD["InterfaceType"].ToString();
                            //s1 += "Type: " + wmi_HD["MediaType"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                ret = "";
            }
            return ret;
        }

        static public string GetMACAddress()
        {
            String sMacAddress = "";
            bool isFirst = true;
            try
            {
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface adapter in nics)
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    if (adapter.GetPhysicalAddress() != null && adapter.GetPhysicalAddress().ToString().Length > 0)
                    {
                        if (isFirst)
                        {
                            sMacAddress = FormatMAC(adapter.GetPhysicalAddress().ToString());
                            isFirst = false;
                        }
                        else
                        {
                            sMacAddress = sMacAddress + "~" + FormatMAC(adapter.GetPhysicalAddress().ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            return sMacAddress;
        }

 

        static private string FormatMAC(string smac)
        {
            String sMacAddress = "";
            try
            {
                bool isFirst = true;
                if (!String.IsNullOrEmpty(smac))
                {
                    for (int i = 0; i < smac.Length; i++)
                    {
                        if (i % 2 == 0)
                        {
                            if (isFirst)
                            {
                                sMacAddress = smac.Substring(i, 2);
                                isFirst = false;
                            } else
                            {
                                if (smac.Length >= i + 2)
                                {
                                    sMacAddress += "-" + smac.Substring(i, 2);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //WriteLog("ERROR - FormatMAC Error - " + ex.Message);
            }
            return sMacAddress;
        }

        static public string GetIP()
        {
            String sIP = "";
            bool isFirst = true;
            try
            {
                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
                foreach (IPAddress ipAddress in localIPs)
                {
                    if (ipAddress.ToString().Length <= 15)
                    {
                        if (isFirst)
                        {
                            sIP = ipAddress.ToString();
                            isFirst = false;
                        }
                        else
                        {
                            sIP = sIP + "~" + ipAddress.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            return sIP;
        }

        static public string GetPlainKey(string key)
        {
            return Crypto.DecryptWithSymmetricAid(Crypto.PRIVATE_KEY, key);
        }

        public static string GetLicenseProtected(string xmlLicense)
        {
            return Crypto.EncryptWithSymmetricAid(Crypto.PRIVATE_KEY, xmlLicense);
        }

        public static BKLicense OGetLicense(string sLicense)
        {
            BKLicense ret = null;
            try
            {
                string licProtected = sLicense.Substring(sLicense.IndexOf("--- BEGIN LICENSE ---") + 21);
                licProtected = licProtected.Substring(0, licProtected.IndexOf("--- END LICENSE ---"));
                string xmlLicense = GetPlainKey(licProtected);

                ret = XmlUtils.DeserializeObject<BKLicense>(xmlLicense);

            }
            catch (Exception ex)
            {
                ret = null;
            }
            return ret;
        }

        public static bool CheckKeys(string key)
        {
            bool ret = false;
            int sum = 0;
            try
            {
                // 50026B7254065B19~TF755AWHH5LXXK|3C-97-0E-A7-80-0D~42-15-4D-FC-EE-99|172.17.141.81~192.168.126.1~192.168.149.1~192.168.0.1
                string[] listkeys = key.Split('|');

                string[] listSerialHDDFromKey = listkeys[0].Split('~');
                string[] listSerialHDDFromGet = GetSerialHHD().Split('~');

                string[] listMACFromKey = listkeys[1].Split('~');
                string[] listMACFromGet = GetMACAddress().Split('~');

                string[] listIPFromKey = listkeys[2].Split('~');
                string[] listIPFromGet = GetIP().Split('~');

                bool hayCoincidencia = false;
                for (int i = 0; i < listSerialHDDFromKey.Length; i++)
                {
                    for (int j = 0; j < listSerialHDDFromGet.Length; j++)
                    {
                        if (listSerialHDDFromKey[i].Equals(listSerialHDDFromGet[j])) {
                            sum++;
                            hayCoincidencia = true;
                            break;
                        }
                    }
                    if (hayCoincidencia) break;
                }

                hayCoincidencia = false;
                for (int i = 0; i < listMACFromKey.Length; i++)
                {
                    for (int j = 0; j < listMACFromGet.Length; j++)
                    {
                        if (listMACFromKey[i].Equals(listMACFromGet[j])) {
                            sum++;
                            hayCoincidencia = true;
                            break;
                        }
                    }
                    if (hayCoincidencia) break;
                }

                hayCoincidencia = false;
                for (int i = 0; i < listIPFromKey.Length; i++)
                {
                    for (int j = 0; j < listIPFromGet.Length; j++)
                    {
                        if (listIPFromKey[i].Equals(listIPFromGet[j])) {
                            sum++;
                            hayCoincidencia = true;
                            break;
                        }
                    }
                    if (hayCoincidencia) break;
                }

                if (sum >= 2) ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
            }
            return ret;
        }
    }
}
 