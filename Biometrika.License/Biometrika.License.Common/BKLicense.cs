﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Biometrika.License.Common
{
    public class BKLicense
    {
        public bool Status;
        public string Cliente;
        public DateTime FechaDesde;
        public DateTime FechaHasta;
        public string Codigo;
        public string Producto;
        public string TipoLicencia;
        public string Key;
        public List<BKLicenseAtributoAdicional> AtributosAdicionales = new List<BKLicenseAtributoAdicional>();

        public BKLicense() { }

        public BKLicense(string cliente, DateTime fechaDesde, DateTime fechaHasta, string codigo, string producto,
            string tipoLicencia, string key, List<BKLicenseAtributoAdicional> atributosAdicionales)
        {
            Cliente = cliente;
            FechaDesde = fechaDesde;
            FechaHasta = fechaHasta;
            Codigo = codigo;
            Producto = producto;
            TipoLicencia = tipoLicencia;
            Key = key;
            AtributosAdicionales = atributosAdicionales;
        }

        public string GetValueAtrAdic(string nombre)
        {
            string ret = null;
            try
            {
                if (AtributosAdicionales != null)
                {
                    foreach (BKLicenseAtributoAdicional item in AtributosAdicionales)
                    {
                        if (item.Nombre.Equals(nombre))
                        {
                            ret = item.Valor;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
            }
            return ret;
        }

        public string ToString()
        {
            string format = "dd/MM/yyyy";
            CultureInfo provider = CultureInfo.InvariantCulture;
            return "Cliente: " + Cliente + Environment.NewLine +
                "Fecha Desde: " + FechaDesde.ToString(format, provider) + Environment.NewLine +
                "Fecha Hasta: " + FechaHasta.ToString(format, provider) + Environment.NewLine +
                "Codigo: " + Codigo + Environment.NewLine +
                "Producto: " + Producto + Environment.NewLine +
                "Tipo licencia: " + TipoLicencia;
        }
    }

    public class BKLicenseAtributoAdicional
    {
        public string Nombre;
        public string Valor;
    }

   

}
