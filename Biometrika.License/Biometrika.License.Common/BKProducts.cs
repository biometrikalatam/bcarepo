﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.License.Common
{
    public class BKProducts
    {
        public List<BKProduct> Products;
        
    }

    public class BKProduct
    {
        public string Name;
        public string Code;

    }

}
