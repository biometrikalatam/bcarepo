﻿using System;
using System.Threading;
using Biometrika.License.Common;
using log4net;

namespace Biometrika.License.Check
{
    public class LicenseCheck
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(LicenseCheck));

        internal bool bRunningPeriodic = true;
        internal static Thread tLicenseBK = null;
        private string _pathLicense;
        private string _code;
        private int _timeforcheckperiodic = 360000; //1 hora
        private BKLicense _LICENSE;

        public string PathLicense { get => _pathLicense; set => _pathLicense = value; }
        public string Code { get => _code; set => _code = value; }
        public int TimeForCheckPeriodic { get => _timeforcheckperiodic; set => _timeforcheckperiodic = value; }
        public BKLicense LICENSE { get => _LICENSE; }

        public LicenseCheck() { }

        public LicenseCheck(string pathLicense, string code, int timeforcheckperiodic) {
            _pathLicense = pathLicense;
            _code = code;
            _timeforcheckperiodic = timeforcheckperiodic; 
        }

        /// <summary>
        /// Chequea licencia por una vez o en forma periódica cada timeforcheckperiodic milisegundos, desde un path dado y con un codigo dado. 
        /// </summary>
        /// <param name="pathLicense">Path carchivo de licencia</param>
        /// <param name="code">codigo de producto</param>
        /// <param name="timeforcheckperiodic">Tiempo a esperar en milisegundos para que se chequee periodicamente la licencia. Si es 0
        /// no se lanza Thread de rechequeo.</param>
        /// <param name="oLic">Objeto BKLicense con status de validez</param>
        /// <param name="msg">Mensaje de explicación si la licencia no es válida.</param>
        /// <returns></returns>
        public int CheckLicense(string pathLicense, string code, int timeforcheckperiodic, out Common.BKLicense oLic, out string msg)
        {
            oLic = null;
            int ret = -1;
            msg = "";
            try
            {
                LOG.Info("Biometrika.License.Check.CheckLicense IN...");
                _pathLicense = pathLicense;
                _code = code;
                _timeforcheckperiodic = timeforcheckperiodic;

                LOG.Debug("Biometrika.License.Check.CheckLicense Read License = " + _pathLicense);
                string sLicense = System.IO.File.ReadAllText(pathLicense);

                LOG.Debug("Biometrika.License.Check.CheckLicense Parse License...");
                _LICENSE = Common.Utils.OGetLicense(sLicense);

                if (_LICENSE == null)
                {
                    LOG.Debug("Biometrika.License.Check.CheckLicense Parse Error...!");
                    msg = "Objeto de Licencia nulo. Error de parseo";
                    return -2; //Parseo erroneo
                }
                else
                {
                    oLic = _LICENSE;
                    //Si es trial, solo cuequeo fecha y codigo
                    if (oLic.TipoLicencia.Equals("Trial"))
                    {
                        LOG.Debug("Biometrika.License.Check.CheckLicense Check Trial...");
                        if (oLic.Codigo.Equals(code) && 
                            (oLic.FechaDesde <= DateTime.Now && DateTime.Now <= oLic.FechaHasta))
                        {
                            oLic.Status = true;
                            msg = "Licencia Tiral Válida!";
                            ret = 0;
                        } else 
                        {
                            oLic.Status = false;
                            if (!oLic.Codigo.Equals(code))
                            {
                                msg = "Licencia Trial Inválida. Codigo no corresponde con el producto. Debe ser: " + code + 
                                    " pero la licencia contiene: " + oLic.Codigo;
                                ret = -3;
                            } else
                            {
                                msg = "Licencia Trial Inválida. Fecha Inválida";
                                ret = -4;
                            }
                        }
                    } else 
                    {
                        //Si e sproducción, ademas de lo de trial, chequeo que al menos dos de los tres elementos (MAC, IP, Serial HDD) coinicdan.
                        //En todos tenemos una lista de datos, con que coincida uno de la lista consideramos ok la licencia.
                        // Ejemplo key: 
                        // 50026B7254065B19~TF755AWHH5LXXK|3C-97-0E-A7-80-0D~42-15-4D-FC-EE-99~6C-88-14-CA-46-15~00-50-56-C0-00-01~00-50-56-C0-00-08~6C-88-14-CA-46-14~24-FD-52-03-00-D8~|172.17.141.81~192.168.126.1~192.168.149.1~192.168.0.13
                        LOG.Debug("Biometrika.License.Check.CheckLicense Check Produccion...");
                        if (!oLic.Codigo.Equals(code))
                        {
                            msg = "Licencia Produccion Inválida. Codigo no corresponde con el producto. Debe ser: " + code +
                                    " pero la licencia contiene: " + oLic.Codigo;
                            oLic.Status = false;
                            ret = -3;
                        }
                        else if (DateTime.Now < oLic.FechaDesde || DateTime.Now > oLic.FechaHasta)
                        {
                            msg = "Licencia Produccion Inválida. Fecha Inválida";
                            oLic.Status = false;
                            ret = -4;
                        }
                        else if (!CheckKey(oLic.Key))
                        {
                            msg = "Licencia Produccion Inválida. Key Inválida";
                            oLic.Status = false;
                            ret = -5;
                        }
                        else
                        {
                            msg = "Licencia Producción Válida!";
                            oLic.Status = true;
                            ret = 0;
                        }
                    }
                    LOG.Debug("Biometrika.License.Check.CheckLicense Check retorno = " + ret);
                }

                //Si el parámetro timeforcheckperiodic != 0 => Genero Thread para chequear cada ese tiempo
                if (_timeforcheckperiodic > 0)
                {
                    LOG.Debug("Biometrika.License.Check.CheckLicense - Start Thread [" + _timeforcheckperiodic + "]...");
                    tLicenseBK = new Thread(new ThreadStart(ThreadCheckLicenseBK));
                    tLicenseBK.Start();
                }

            }
            catch (Exception ex)
            {
               LOG.Error("Biometrika.License.Check.CheckLicense - Error = " + ex.Message);
               oLic = null;
               ret = -1;
            }
            LOG.Debug("Biometrika.License.Check.CheckLicense OUT!");
            return ret;
        }

        public void KillCheckPeriodic()
        {
            LOG.Info("Biometrika.License.Check.KillCheckPeriodic IN...");
            try
            {
                if (tLicenseBK != null && tLicenseBK.IsAlive)
                {
                    LOG.Info("Biometrika.License.Check.KillCheckPeriodic Licensing Check Periodic Aborting...");
                    tLicenseBK.Abort();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Biometrika.License.Check.KillCheckPeriodic - Error = " + ex.Message);
            }
            LOG.Info("Biometrika.License.Check.KillCheckPeriodic OUT!");
            bRunningPeriodic = false;
        }

        private void ThreadCheckLicenseBK()
        {
            try
            {
                while (bRunningPeriodic)
                {
                    //Chequeo licencia, y si da falso, entonces sale del loop
                    bRunningPeriodic = RefreshStatusLicense();

                    LOG.Info("Program.ThreadCheckLicenseBK - " +
                                "Chequeó licencia en thread [" +
                                bRunningPeriodic.ToString() + " - " + _timeforcheckperiodic.ToString() +  "]");
                    //Duerme una hora y rechequea
                    //(1000 miliseg x 60 seg x 60 minutos =
                    //				 3600000 milisegundos = 1 hora)
                    Thread.Sleep(_timeforcheckperiodic);

                    //Para test uso 1 minuto
                    //Thread.Sleep(60000);
                }

                if (!bRunningPeriodic)
                {
                    LOG.Info("Biometrika.License.Check.ThreadCheckLicenseBK Out from ThreadCheckLicenseBK!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Biometrika.License.Check.ThreadCheckLicenseBK - Error = " + ex.Message);
            }
        }

        private bool RefreshStatusLicense()
        {
            bool result = false;
            try
            {
                LOG.Info("Biometrika.License.Check.RefreshStatusLicense - CheckLicense In...");
                string sPathLicencia = _pathLicense;
                string msgErr = null;
                string sCodigo = _code;
                int retLic = -1;
                lock (_LICENSE)
                {
                        //Copio el intervalo
                    int _copyTimeForcheckPeriodic = _timeforcheckperiodic;
                        //Llamo a chech sin intervalo para que no cree nuevo Thread
                    retLic = CheckLicense(sPathLicencia, sCodigo, 0,  out _LICENSE, out msgErr);
                        //Restauro valor anterior
                    this._timeforcheckperiodic = _copyTimeForcheckPeriodic;
                }
                if (retLic == 0)
                {
                    if (_LICENSE == null)
                    {
                        LOG.Fatal("Biometrika.License.Check.RefreshStatusLicense - Objeto de licencia nulo");
                        result = false;
                    }
                    else if (!_LICENSE.Status)
                    {
                        LOG.Fatal("Biometrika.License.Check.RefreshStatusLicense - Licencia Inválida! [" + retLic + "-" + msgErr + "]" +
                            _LICENSE.ToString() + "]");
                        result = false;
                    }
                    else
                    {
                        LOG.Info("Biometrika.License.Check.RefreshStatusLicense - Licencia Válida! " + _LICENSE.ToString());
                        result = true;
                    }
                }
                else
                {
                    result = false;
                    LOG.Fatal("Biometrika.License.Check.RefreshStatusLicense - Licencia Inválida! [" + retLic + "-" + msgErr + "]");
                }


                LOG.Info("Biometrika.License.Check.RefreshStatusLicense - CheckLicense Out!");
            }
            catch (Exception ex)
            {
                LOG.Error("Biometrika.License.Check.RefreshStatusLicense", ex);
            }
            return result;
        }

        private static bool CheckKey(string key)
        {
            
            try
            {
                return Common.Utils.CheckKeys(key);
            }
            catch (Exception ex)
            {
                LOG.Error("Biometrika.License.Check.CheckKey - Error = " + ex.Message);
                return false;
            }
        }
    }
}
