﻿using Biometrika.License.CheckCenter.Code;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Biometrika.License.CheckCenter
{
    /// <summary>
    /// Summary description for CheckCenterWS
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CheckCenterWS : System.Web.Services.WebService
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(CheckCenterWS));

        [WebMethod]
        public int CheckVolumeLicense(string id, string idclient, out string msgError, out string oLic)
        {
            msgError = null;
            int ret = 0;
            oLic = null;
            try 
            {
                LOG.Info("CheckCenterWS.CheckVolumeLicense IN => Id = " + id + " - idclient = " + idclient); 
                ret = ServiceManager.CheckLicense(id, idclient, out msgError, out oLic);
            }
            catch (Exception ex)
            {
                ret = -1;
                oLic = null;
                LOG.Error("CheckCenterWS.CheckVolumeLicense Error => " + ex.Message);
            }
            LOG.Info("CheckCenterWS.CheckVolumeLicense OUT! ret = " + ret);
            LOG.Debug("CheckCenterWS.CheckVolumeLicense OUT! oLic = " + (oLic != null ? oLic : "Nulo"));
            return ret;
        }

        [WebMethod]
        public int GetAvailableVolumeLicense(string id, string code, out string msgError, out string listAvailable)
        {
            msgError = null;
            int ret = 0;
            listAvailable = null;
            try
            {
                LOG.Info("CheckCenterWS.GetAvailableVolumeLicense IN => Id = " + id + " - code = " + code);
                ret = ServiceManager.GetAvailableVolumeLicense(id, code, out msgError, out listAvailable);
            }
            catch (Exception ex)
            {
                ret = -1;
                listAvailable = null;
                LOG.Error("CheckCenterWS.GetAvailableVolumeLicense Error => " + ex.Message);
            }
            LOG.Info("CheckCenterWS.GetAvailableVolumeLicense OUT! ret = " + ret);
            LOG.Debug("CheckCenterWS.GetAvailableVolumeLicense OUT! oLic = " + (listAvailable != null ? listAvailable : "Nulo"));
            return ret;
        }
    }
}
