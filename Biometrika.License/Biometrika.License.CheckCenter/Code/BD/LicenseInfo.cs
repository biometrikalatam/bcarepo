﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Biometrika.License.CheckCenter.Code.BD
{
    public class LicenseInfo
    {
        public int code;
        public string id;
        public string msgErr;
        public string xmlLicense;
    }
}