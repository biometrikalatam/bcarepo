﻿using Biometrika.License.Common;
using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Biometrika.License.CheckCenter.Code.BD
{
    public class Store: ICloneable
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Store));

        
        //Usado para las actualizaciones y consumos de licencias
        [NonSerialized()]
        internal object SEMAFORO = new object();
        [NonSerialized()]
        internal string PATH_DIR_LICENSES;        
        [NonSerialized()]
        internal Hashtable htStoreItems;

        public string Company;
        public List<StoreItem> ListStoreItems;


        public Store() { }

        /// <summary>
        /// Inicializa el objeto Store, refresca status de ls licencias, y setea el chequeo cada 1 hora
        /// </summary>
        /// <param name="pathLienses"></param>
        /// <returns></returns>
        internal int Initialization(string pathDirectoryLienses)
        {
            int ret = 0;
            try
            {
                LOG.Info("Store.Initialization IN...");
                if (string.IsNullOrEmpty(pathDirectoryLienses))
                {
                    LOG.Warn("Store.Initialization PAth de Licencias Nulo! Revise el parámetro y reinicie el servicio...");
                    ret = -2;
                } else
                {
                    PATH_DIR_LICENSES = pathDirectoryLienses;
                    if (RefreshCheckVolumeLicense() == 0)
                    {
                        //Seteo Thread para que se repita cada 1 hora...
                        LOG.Info("Store.Initialization Initialization OK!");
                        LOG.Info(ToString());
                    }
                } 
            }
            catch (Exception ex)
            {
                LOG.Error("Store.Initialization Error = " + ex.Message);
                ret = -1;
            }
            LOG.Info("Store.Initialization OUT!");
            return ret;
        }

        /// <summary>
        ///         0.- Lock SEMAFORO
        ///         1.- Revisar que existe el id en ht en STORE, sino devolver error
        ///         2.- Si está,
        ///             2.1.- Actualizar QAvailable, 
        ///             2.2.- Registar el cliente 
        ///             2.3.- Salvar Store en disco
        ///         3.- UNLOCK Semaforo
        ///         4.- Devolver ok con el objeto de licencia (por los adicionales necesarios) 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idclient"></param>
        /// <param name="msgErr"></param>
        /// <returns></returns>
        internal StoreItem CheckLicenseById(string id, string idclient, out string msgErr)
        {
            msgErr = null;
            StoreItem ret = null;
            try
            {
                LOG.Info("Store.CheckLicenseById IN => id=" + id + "|idclient=" + idclient + "...");
                lock (SEMAFORO)
                {
                    //1.- Revisar que existe el id en ht en STORE, sino devolver error
                    ret = (StoreItem)htStoreItems[id];
                    if (ret == null)
                    {
                        //LOG
                        LOG.Warn("Store.CheckLicenseById - Id [" + id + "] no existe en el store!"); 
                        ret = null;
                        msgErr = "No existe Id = " + id + " en el store centralizado!";
                    } else
                    {
                        LOG.Warn("Store.CheckLicenseById - Ingresando a chequear que no este registrado...");
                        //Chequeo que ya se haya registrado antes.
                        if (ret.htListClientsRegistered != null && (ret.htListClientsRegistered.ContainsKey(idclient)))
                        {
                            LOG.Debug("Store.CheckLicenseById - Cliente ya registrado...");
                            if (!ret.IsValid)
                            {
                                //LOG => MAnda msg fatal a Log
                                LOG.Warn("Store.CheckLicenseById - Licencia Invalida!");
                                ret = null;
                                msgErr = "Licencia Inválida! Consulte con el administrador...";
                            } else
                            {
                                LOG.Debug("Store.CheckLicenseById - Licencia Valida!");
                                //LOG Solo informo chequeo ok
                            }
                        } else //Si no está registrado lo agrego
                        {
                            LOG.Debug("Store.CheckLicenseById - Cliente NO registrado => Consume si hay disponibles..." +
                                      "[Disponibles=" + ret.QAvailable.ToString() + " de un Total=" + ret.QTotal.ToString());
                            if (ret.QAvailable > 0)
                            {
                                LOG.Debug("Store.CheckLicenseById - Disponibles OK => Registrando...");
                                if (ret.htListClientsRegistered == null) ret.htListClientsRegistered = new Hashtable();
                                LOG.Debug("Store.CheckLicenseById - Len Registrados = " + ret.htListClientsRegistered.Count.ToString());
                                htStoreItems.Remove(id);
                                Client clt = new Client();
                                clt.HDDSerial = idclient;
                                ret.htListClientsRegistered.Add(idclient, clt);
                                ret.QAvailable -= 1;
                                htStoreItems.Add(id, ret);
                                if (ret.ListClientsRegistered == null)
                                    ret.ListClientsRegistered = new List<Client>();
                                ret.ListClientsRegistered.Add(clt);
                                LOG.Debug("Store.CheckLicenseById - Nuevo idclinet [" + idclient + 
                                          "] Registrado = " + ret.htListClientsRegistered.Count.ToString());
                                for (int i = 0; i < ListStoreItems.Count; i++)
                                {
                                    if (string.Equals(ListStoreItems[i].Id, ret.Id))
                                    {
                                        ListStoreItems[i] = ret;
                                        break;
                                    }
                                }
                                //Grabo actualización
                                string xml = XmlUtils.SerializeObject<Store>(this);
                                System.IO.File.WriteAllText(PATH_DIR_LICENSES + "Licenses.Store.xml", xml);
                                LOG.Debug("Store.CheckLicenseById - Actualizado Store Licenses.Store.xml...");
                            } else
                            {
                                ret = null;
                                msgErr = "No existen licencias disponibles para el id = " + id;
                                LOG.Warn("Store.CheckLicenseById - " + msgErr);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msgErr = "Store.CheckLicenseById Error - " + ex.Message;
                ret = null;
                LOG.Error("Store.CheckLicenseById - Excp = " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Se hacen los siguientes pasos:
        /// 0.- Se bloquea el objeto SEMAFORO
        /// 1.- Se lee el Store almacenado desde disco, y se actualiza los Q
        /// 2.- Se leen la lista de archivos de licencia en el directorio de licencias, se actualiza si alguna vencio, y se agregan las nuevas
        /// 3.- Se deslockea el objeto SEMAFORO
        /// </summary>
        private int RefreshCheckVolumeLicense()
        {
            int ret = 0;
            BKLicense oBKLicAux;
            Store storeAux = null;
            string xmlStore = null;
            try
            {
                // 0.- Se bloquea el objeto SEMAFORO
                lock (SEMAFORO)
                {
                    // 1.- Se lee el Store almacenado desde disco, y se actualiza los Q
                    if (System.IO.File.Exists((PATH_DIR_LICENSES + "Licenses.Store.xml"))) {
                        xmlStore = System.IO.File.ReadAllText(PATH_DIR_LICENSES + "Licenses.Store.xml");
                        storeAux = XmlUtils.DeserializeObject<Store>(xmlStore);
                        if (storeAux.ListStoreItems != null && storeAux.ListStoreItems.Count > 0)
                        {
                            foreach (StoreItem item in storeAux.ListStoreItems)
                            {
                                if (item.ListClientsRegistered != null && item.ListClientsRegistered.Count > 0)
                                {
                                    item.htListClientsRegistered = new Hashtable();
                                    foreach (Client clt in item.ListClientsRegistered)
                                    {
                                        item.htListClientsRegistered.Add(clt.HDDSerial, clt);
                                    }
                                    item.QAvailable = item.QTotal - 
                                        ((item.htListClientsRegistered!=null)?item.htListClientsRegistered.Count:0);
                                }
                            }
                        }
                        storeAux = CopyListToHashtable(storeAux);
                        Company = storeAux.Company;
                        ListStoreItems = storeAux.ListStoreItems;
                        htStoreItems = storeAux.htStoreItems;
                        //CopyListToHashtable();
                    } else
                    {
                        storeAux = new Store();
                        htStoreItems = new Hashtable();
                        ListStoreItems = new List<StoreItem>();
                    }

                    //2.- Se leen la lista de archivos de licencia en el directorio de licencias, se actualiza si alguna vencio, y se agregan las nuevas
                    System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(PATH_DIR_LICENSES);
                    System.IO.FileInfo[] Files_Licenses = di.GetFiles();
                    if (Files_Licenses == null || Files_Licenses.Length == 0)
                    {
                        ret = -2;
                        LOG.Error("Store.RefreshCheckVolumeLicense Directorio de licencias [" + PATH_DIR_LICENSES + "] vació. Copie los archivos de licencia y reinicie...");
                    } else
                    {
                        License.Check.LicenseCheck oCheck = new Check.LicenseCheck();
                        string[] infoLic;
                        int retCheck;
                        string msgErr;
                        string idVolume;
                        foreach (System.IO.FileInfo file in Files_Licenses)
                        {
                            //El formato del archivo debe ser <CODE>_<IdUnicoDeVolumeLicense>_YYYYMMDDHHMMSS.lic. Con code se chequea la licencia 
                            if (file.FullName.EndsWith(".lic")) {
                                BKLicense oBKLicenseAux = Common.Utils.OGetLicense(System.IO.File.ReadAllText(file.FullName));
                                //infoLic = file.FullName.Split('_');
                                idVolume = oBKLicenseAux.GetValueAtrAdic("IdVolume"); // GetIdVolumeLicense(file.FullName);
                                //if (!string.IsNullOrEmpty(idVolume))
                                if (oBKLicenseAux != null && !string.IsNullOrEmpty(idVolume))
                                {
                                    retCheck = oCheck.CheckLicense(file.FullName, oBKLicenseAux.Codigo, 0, out oBKLicAux, out msgErr);
                                    if (retCheck < 0 || oBKLicAux == null)
                                    {
                                        LOG.Error("Store.RefreshCheckVolumeLicense Archivo = " + file.FullName + " no pudo ser procesado [Error = " + msgErr + "]");
                                        //Actualizo store para inhabilitar la licencia
                                        //DisableLicenseInStore(GetIdVolumeLicense(file.FullName));
                                        DisableLicenseInStore(idVolume);
                                    }
                                    else
                                    {
                                        //ActualizoStore(idVolume, oBKLicAux);
                                        ActualizoStore(idVolume, oBKLicAux);
                                    }
                                } else
                                {
                                    LOG.Error("Store.RefreshCheckVolumeLicense La licencia ubicada en: " + file.FullName + " fue procesada, pero no se encontró el Id de Volumen, " +
                                        " por lo que se ignoró...");
                                }
                            }
                        }

                        //Grabo actualización
                        string xml = XmlUtils.SerializeObject<Store>(this);
                        System.IO.File.WriteAllText(PATH_DIR_LICENSES + "Licenses.Store.xml", xml);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Store.RefreshCheckVolumeLicense Error = " + ex.Message);
                ret = -1;
            }
            return ret;
        }

       


        /// <summary>
        /// Dado un objeto de licencia chequeado correcto, lo agrega en la lista y el HT o bien si ya existe,
        /// actualiza los Q de acuerdo a los seriales registrados ya. 
        /// </summary>
        /// <param name="oBKLicAux"></param>
        private void ActualizoStore(string id, BKLicense oBKLicAux)
        {
            try
            {
                //Disable en HT
                LOG.Debug("Store.ActualizoStore IN. htStoreItems.Count = " + htStoreItems.Count);
                StoreItem item = (StoreItem)htStoreItems[id];
                if (item != null)
                {
                    item.IsValid = oBKLicAux.Status;
                    item.QAvailable = item.QTotal - item.ListClientsRegistered.Count;
                    htStoreItems.Remove(id);
                    item.oLicense = oBKLicAux;
                    htStoreItems.Add(id, item);
                    LOG.Debug("Store.ActualizoStore Id Actualizado => " + id + " - QAvailable = " + item.QAvailable);
                }
                else
                {
                    LOG.Debug("Store.ActualizoStore No existe id = " + id + ", se agrega...");
                    item = new StoreItem();
                    item.Id = id;
                    item.Code = oBKLicAux.Codigo;
                    item.FechaDesde = oBKLicAux.FechaDesde;
                    item.FechaHasta = oBKLicAux.FechaHasta;
                    item.IsValid = oBKLicAux.Status;
                    item.LicenseType = oBKLicAux.TipoLicencia;
                    item.QTotal = Convert.ToInt32(oBKLicAux.GetValueAtrAdic("QVolumeTotal"));
                    item.QAvailable = item.QTotal;
                    item.oLicense = oBKLicAux;
                    htStoreItems.Add(id, item);
                }

                //Disable in List
                bool _found = false;
                for (int i = 0; i < ListStoreItems.Count; i++)
                {
                    if (string.Equals(ListStoreItems[i].Id, id))
                    {
                        _found = true;
                        ListStoreItems[i].IsValid = oBKLicAux.Status;
                        ListStoreItems[i].LicenseType = oBKLicAux.TipoLicencia;
                        LOG.Info("Store.ActualizoStore Id Actualizado => " + id + " - Status Licencia = " + oBKLicAux.Status.ToString());
                        break;
                    }
                }
                if (!_found) //La agrego
                {
                    item = new StoreItem();
                    item.Id = id;
                    item.Code = oBKLicAux.Codigo;
                    item.FechaDesde = oBKLicAux.FechaDesde;
                    item.FechaHasta = oBKLicAux.FechaHasta;
                    item.IsValid = oBKLicAux.Status;
                    item.LicenseType = oBKLicAux.TipoLicencia;
                    item.QTotal = Convert.ToInt32(oBKLicAux.GetValueAtrAdic("QVolumeTotal"));
                    item.QAvailable = item.QTotal;
                    ListStoreItems.Add(item);
                    LOG.Info("Store.ActualizoStore Id Agregado a Lista => " + id + " - Status Licencia = " + oBKLicAux.Status.ToString());
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Store.ActualizoStore Error = " + ex.Message);
            }
        }


        /// <summary>
        /// Copia la lista L hastable para hacer mas facil y directo luego el chequeo
        /// </summary>
        private void CopyListToHashtable()
        {
            try
            {
                htStoreItems = new Hashtable();
                foreach (var item in ListStoreItems)
                {
                    htStoreItems.Add(item.Id, item);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Store.CopyListToHashtable Error = " + ex.Message);
            }
        }

        private Store CopyListToHashtable(Store storeAux)
        {
            try
            {
                if (storeAux == null) return null;

                if (storeAux.htStoreItems == null) storeAux.htStoreItems = new Hashtable();
                foreach (var item in storeAux.ListStoreItems)
                {
                    storeAux.htStoreItems.Add(item.Id, item);
                }
            }
            catch (Exception ex)
            {
                storeAux = null;
                LOG.Error("Store.CopyListToHashtable Error = " + ex.Message);
            }
            return storeAux;
        }

        /// <summary>
        /// Recorre la lista de StoreItems y deshabilita la licencia porque no pudo cheauearla o se venció
        /// Lo mismo en el Hashtable
        /// </summary>
        /// <param name="id"></param>
        private void DisableLicenseInStore(string id)
        {
            try
            {
                //Disable en HT
                LOG.Debug("Store.DisableLicenseInStore IN. htStoreItems.Count = " + htStoreItems.Count);
                StoreItem item = (StoreItem)htStoreItems[id];
                if (item != null)
                {
                    item.IsValid = false;
                    htStoreItems.Remove(id);
                    htStoreItems.Add(id, item);
                    LOG.Debug("Store.DisableLicenseInStore Id Deshabilitado en htStoreItems => " + id);
                } else
                {
                    LOG.Debug("Store.DisableLicenseInStore No existe para deshabilitar id = " + id); 
                }

                //Disable in List
                for (int i = 0; i < ListStoreItems.Count; i++)
                {
                    if (string.Equals(ListStoreItems[i].Id, id))
                    {
                        LOG.Info("Store.DisableLicenseInStore Id Deshabilitado => " + id);
                        ListStoreItems[i].IsValid = false;
                        break;
                    }
                }

            }
            catch (Exception ex)
            {
                LOG.Error("Store.DisableLicenseInStore Error = " + ex.Message);
            }
        }


        /// <summary>
        /// Paresea a mano el archivo de licencia para sacar el ID (Debe venir en una licencia de volumen es un Guid con "N"
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        private string GetIdVolumeLicense(string fullName)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Muestra detalles de todaslas licencias actuales y valores almacenados.
        /// </summary>
        /// <returns></returns>
        private string ToString()
        {
            string ret = "STORE Information" + Environment.NewLine + 
                "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" +
                    Environment.NewLine + "  >> Path Licenses = " + this.PATH_DIR_LICENSES +
                Environment.NewLine + "  >> Q Licenses Procesed = " + this.ListStoreItems.Count;

            if (this.ListStoreItems.Count > 0) {
                ret += Environment.NewLine + "Id de Volumenes Cargados" + Environment.NewLine +
                    "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";
                foreach (StoreItem item in this.ListStoreItems)
                {
                    ret += Environment.NewLine + " Id = " + item.Id + " - Status = " + item.IsValid.ToString() + " - Code = " + item.Code + " - Type = " + item.LicenseType +
                        " - QTotal = " + item.QTotal + " - QAvailabe = " + item.QAvailable + 
                        " - Fechas desde = " + item.FechaDesde.ToString("dd/MM/yyyy") + " hasta " + item.FechaHasta.ToString("dd/MM/yyyy");
                }
            }
            ret += Environment.NewLine +
                "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";

            return ret;
        }

        public object Clone()
        {
            Store storeAux = new Store();
            storeAux = this;
            return storeAux;
        }
    }

    public class StoreItem
    {
        public StoreItem() { }

        public string Id;
        public bool IsValid;
        public string Code;
        public string LicenseType;
        public DateTime FechaDesde;
        public DateTime FechaHasta;

        //public List<Common.BKLicense> ListVolumeLicenses;
        public List<Client> ListClientsRegistered;

        //Para control de ocupados en la operacion rápida
        public int QTotal { get; set; }
        public int QAvailable { get; set; }
        //Hashtable htListVolumeLicenses;
        internal Hashtable htListClientsRegistered;
        internal BKLicense oLicense;
    }

    public class Client
    {
        public Client() { }

        public string IP;
        public string MacAdress;
        public string HDDSerial;

        

    }

    public class StoreItemInfo
    {
        public StoreItemInfo() { }

        public string Id;
        public string Code;
        public string LicenseType;
        public DateTime FechaDesde;
        public DateTime FechaHasta;
        public int QTotal { get; set; }
        public int QAvailable { get; set; }
    }
}