﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Biometrika.License.CheckCenter.Code
{
    public class ServiceManager
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(ServiceManager));

        /// <summary>
        /// Debe hacer lo siguiente:
        ///         1.- Revisar que existe el id en ht en STORE, sino devolver error
        ///         2.- Si está,
        ///             2.0.- Lock SEMAFORO
        ///             2.1.- Actualizar QAvailable, 
        ///             2.2.- Registar el cliente 
        ///             2.3.- UNLOCK Semaforo
        ///             2.4.- Devolver ok con el objeto de licencia (por los adicionales necesarios)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idclient"></param>
        /// <returns></returns>
        public static int CheckLicense(string id, string idclient, out string msgErr, out string oLic)
        {
            msgErr = null;
            int ret = 0;
            oLic = null;
            try
            {
                LOG.Info("ServiceManager.CheckLicense IN => Id = " + id + " - idclient = " + idclient);
                //1.- Revisar que existe el id en ht en STORE, sino devolver error
                BD.StoreItem _storeItem = WebApiApplication.STORE.CheckLicenseById(id, idclient, out msgErr);

                if (_storeItem != null)
                {
                    LOG.Info("ServiceManager.CheckLicense _storeItem != nulo => Hay Licencia...");
                    oLic = Common.XmlUtils.SerializeObject<Common.BKLicense>(_storeItem.oLicense);
                    LOG.Debug("ServiceManager.CheckLicense oLic = " + (oLic != null ? oLic : "Nulo")); 
                } else
                {
                    LOG.Warn("ServiceManager.CheckLicense _storeItem == Nulo => No hay licencia configurada con ese id!");
                    ret = -2;
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                oLic = null;
                ret = -1;
                LOG.Error("ServiceManager.CheckLicense Error => " + msgErr);
            }
            LOG.Info("ServiceManager.CheckLicense OUT!");
            return ret;
        }

        internal static int GetAvailableVolumeLicense(string id, string code, out string msgErr, out string listAvailable)
        {
            msgErr = null;
            int ret = 0;
            listAvailable = null;
            try
            {
                LOG.Info("ServiceManager.GetAvailableVolumeLicense IN => Id = " + id + " - code = " + code);
                BD.Store storeAux = new BD.Store();
                storeAux.Company = WebApiApplication.STORE.Company;
                storeAux.ListStoreItems = new List<BD.StoreItem>();
                BD.StoreItem storeItemAux;
                for (int i = 0; i < WebApiApplication.STORE.ListStoreItems.Count; i++)
                {
                    if (WebApiApplication.STORE.ListStoreItems[i].IsValid && 
                       (string.IsNullOrEmpty(code) || string.Equals(WebApiApplication.STORE.ListStoreItems[i].Code, code)) &&
                       (string.IsNullOrEmpty(id) || string.Equals(WebApiApplication.STORE.ListStoreItems[i].Id, id)))
                    {
                        storeItemAux = new BD.StoreItem();
                        storeItemAux.Code = WebApiApplication.STORE.ListStoreItems[i].Code;
                        storeItemAux.FechaDesde = WebApiApplication.STORE.ListStoreItems[i].FechaDesde;
                        storeItemAux.FechaHasta = WebApiApplication.STORE.ListStoreItems[i].FechaHasta;
                        storeItemAux.Id = WebApiApplication.STORE.ListStoreItems[i].Id;
                        storeItemAux.IsValid = WebApiApplication.STORE.ListStoreItems[i].IsValid;
                        storeItemAux.LicenseType = WebApiApplication.STORE.ListStoreItems[i].LicenseType;
                        storeItemAux.QAvailable = WebApiApplication.STORE.ListStoreItems[i].QAvailable;
                        storeItemAux.QTotal = WebApiApplication.STORE.ListStoreItems[i].QTotal;
                        storeItemAux.ListClientsRegistered = WebApiApplication.STORE.ListStoreItems[i].ListClientsRegistered;
                        storeAux.ListStoreItems.Add(storeItemAux);
                        LOG.Debug("ServiceManager.GetAvailableVolumeLicense Item agregado => id = " + storeItemAux.Id + " - code = " + storeItemAux.Code);
                    }
                } 

                listAvailable = Common.XmlUtils.SerializeObject<BD.Store>(storeAux);
                LOG.Debug("ServiceManager.GetAvailableVolumeLicense listAvailable = " + listAvailable);
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                listAvailable = null;
                ret = -1;
                LOG.Error("ServiceManager.GetAvailableVolumeLicense Error => " + msgErr);
            }
            LOG.Info("ServiceManager.GetAvailableVolumeLicense OUT!");
            return ret;
        }
    }
}