﻿using Biometrika.License.CheckCenter.Code.BD;
using log4net;
using log4net.Config;
using System;
using System.IO;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Biometrika.License.CheckCenter
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(WebApiApplication));
        internal static Store STORE = new Store();


        protected void Application_Start()
        {
            try
            {
                XmlConfigurator.Configure(new FileInfo(Properties.Settings.Default.PathLogConfig));
                LOG.Info("Biometrika.License.CheckCenter Starting...");

                AreaRegistration.RegisterAllAreas();
                GlobalConfiguration.Configure(WebApiConfig.Register);
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                BundleConfig.RegisterBundles(BundleTable.Bundles);

                //CreateStore();

                //Inicializo el Store e informo en log 
                LOG.Info("Biometrika.License.CheckCenter Initializing Store PathLicenses => " + Properties.Settings.Default.PathLienses);
                int ret = STORE.Initialization(Properties.Settings.Default.PathLienses);
                LOG.Info("Biometrika.License.CheckCenter Initialized Store => ret = " + ret);
                LOG.Info("Biometrika.License.CheckCenter " + STORE.ToString());
            }
            catch (Exception ex)
            {
                LOG.Error("Biometrika.License.CheckCenter.Application_Start Excp Error: " + ex.Message);
            }
           

        }

        private void CreateStore()
        {
            Store STORE = new Store();
            STORE.Company = "AFEX";
            STORE.ListStoreItems = new System.Collections.Generic.List<StoreItem>();

            StoreItem item = new StoreItem();
            item.Id = Guid.NewGuid().ToString("N");
            item.Code = "BC7";
            item.FechaDesde = DateTime.Parse("01/01/2019");
            item.FechaHasta = DateTime.Parse("01/06/2019");
            item.IsValid = true;
            item.ListClientsRegistered = new System.Collections.Generic.List<Client>();
            Client oCli = new Client();
            oCli.HDDSerial = "HDD6644-8921";
            item.ListClientsRegistered.Add(oCli);
            oCli = new Client();
            oCli.HDDSerial = "HDD6644-9999";
            item.ListClientsRegistered.Add(oCli);

            STORE.ListStoreItems.Add(item);

            System.IO.File.WriteAllText("c:\\tmp\\VolumeLicenses\\Licenses.Store.xml", Common.XmlUtils.SerializeObject<Store>(STORE)); 
        }
    }
}
