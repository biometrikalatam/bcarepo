﻿using Biometrika.License.CheckCenter.Code;
using Biometrika.License.CheckCenter.Code.BD;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;

namespace Biometrika.License.CheckCenter.Controllers
{
    public class LicenseController : ApiController
    {
        private static readonly ILog LOG = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [Route("api/License/CheckLicense/")]
        [HttpPost]
        public Code.BD.LicenseInfo CheckLicense(string id, string idclient)
        {
            int ret = 0;
            string msgErr = null;
            string infoLicense = null;

            LOG.Info("LicenseController.CheckLicense IN - id = " + id + " - idclient = " + idclient);

            LOG.Debug("LicenseController.CheckLicense Call ServiceManager.CheckLicense...");
            ret = ServiceManager.CheckLicense(id, idclient, out msgErr, out infoLicense);
            LOG.Debug("LicenseController.CheckLicense ret = " + ret + " - infoLicense = " + infoLicense);

            Code.BD.LicenseInfo infoLic = new Code.BD.LicenseInfo();
            infoLic.code = ret;
            infoLic.id = id;
            infoLic.msgErr = msgErr;
            infoLic.xmlLicense = infoLicense;
            LOG.Info("LicenseController.CheckLicense OUT!");
            return infoLic;
        }

        [Route("api/License/GetAvailableVolumeLicense/")]
        [HttpGet]
        public Code.BD.LicenseInfo GetAvailableVolumeLicense(string id, string code)
        {
            int ret = 0;
            string msgErr = null;
            string infoLicense = null;

            try
            {

            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }
            LOG.Info("LicenseController.GetAvailableVolumeLicense IN - id = " + id + " - code = " + code);

            LOG.Debug("LicenseController.GetAvailableVolumeLicense Call ServiceManager.GetAvailableVolumeLicense...");
            ret = ServiceManager.GetAvailableVolumeLicense(id, code, out msgErr, out infoLicense);
            LOG.Debug("LicenseController.GetAvailableVolumeLicense ret = " + ret + " - infoLicense = " + infoLicense);

            Code.BD.LicenseInfo infoLic = new Code.BD.LicenseInfo();
            infoLic.code = ret;
            infoLic.id = id;
            infoLic.msgErr = msgErr;
            infoLic.xmlLicense = infoLicense;
            LOG.Info("LicenseController.GetAvailableVolumeLicense OUT!");
            return infoLic;
        }

        [Route("api/License/GetVolumeLicenseInfo/")]
        [HttpGet]
        public object GetVolumeLicenseInfo()
        {
            
            string msgErr = null;
            string infoLicense = null;

            try
            {
                LOG.Info("LicenseController.GetVolumeLicenseInfo IN...");
                //foreach (StoreItem item in WebApiApplication.STORE.ListStoreItems)
                //{

                //}

                //infoLicense = JsonConvert.SerializeObject(WebApiApplication.STORE.ListStoreItems);

                //List<StoreItem> s = JsonConvert.DeserializeObject<List<StoreItem>>(infoLicense);
            }
            catch (Exception ex)
            {
                infoLicense = "{\"error\": -1 [" + ex.Message + "]}";
                LOG.Error("GetVolumeLicenseInfo Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, infoLicense);
            }
            LOG.Info("LicenseController.GetVolumeLicenseInfo OUT! => infoLicense = " + infoLicense);
            return Request.CreateResponse(HttpStatusCode.OK, WebApiApplication.STORE.ListStoreItems);

        }
    }
}