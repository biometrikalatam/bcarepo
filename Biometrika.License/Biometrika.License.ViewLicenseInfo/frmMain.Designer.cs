﻿namespace Biometrika.License.ViewLicenseInfo
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.grpLocal = new System.Windows.Forms.GroupBox();
            this.btnReadLicense = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtPathLicense = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rbLocal = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.rbRemote = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.WebViewLicense = new System.Windows.Forms.WebBrowser();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rtbInfoLic = new System.Windows.Forms.RichTextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grpLocal.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(364, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(536, 45);
            this.label1.TabIndex = 5;
            this.label1.Text = "Herramienta para la visualización de licencias de productos Biometrika...";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Biometrika.License.ViewLicenseInfo.Properties.Resources.Biometrika_Bajada_Big_250px;
            this.pictureBox1.Location = new System.Drawing.Point(76, 29);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(250, 45);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // grpLocal
            // 
            this.grpLocal.Controls.Add(this.btnReadLicense);
            this.grpLocal.Controls.Add(this.button1);
            this.grpLocal.Controls.Add(this.txtPathLicense);
            this.grpLocal.Controls.Add(this.label2);
            this.grpLocal.Location = new System.Drawing.Point(44, 153);
            this.grpLocal.Name = "grpLocal";
            this.grpLocal.Size = new System.Drawing.Size(856, 81);
            this.grpLocal.TabIndex = 6;
            this.grpLocal.TabStop = false;
            this.grpLocal.Text = "Seleccionar Archivo de Licencia...";
            // 
            // btnReadLicense
            // 
            this.btnReadLicense.Location = new System.Drawing.Point(63, 44);
            this.btnReadLicense.Name = "btnReadLicense";
            this.btnReadLicense.Size = new System.Drawing.Size(146, 29);
            this.btnReadLicense.TabIndex = 3;
            this.btnReadLicense.Text = "&Leer Licencia...";
            this.btnReadLicense.UseVisualStyleBackColor = true;
            this.btnReadLicense.Click += new System.EventHandler(this.btnReadLicense_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(815, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(24, 20);
            this.button1.TabIndex = 2;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtPathLicense
            // 
            this.txtPathLicense.Location = new System.Drawing.Point(63, 20);
            this.txtPathLicense.Name = "txtPathLicense";
            this.txtPathLicense.Size = new System.Drawing.Size(746, 20);
            this.txtPathLicense.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Archivo :";
            // 
            // rbLocal
            // 
            this.rbLocal.AutoSize = true;
            this.rbLocal.Location = new System.Drawing.Point(474, 111);
            this.rbLocal.Name = "rbLocal";
            this.rbLocal.Size = new System.Drawing.Size(51, 17);
            this.rbLocal.TabIndex = 7;
            this.rbLocal.TabStop = true;
            this.rbLocal.Text = "Local";
            this.rbLocal.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(310, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Tipo de Licencia:";
            // 
            // rbRemote
            // 
            this.rbRemote.AutoSize = true;
            this.rbRemote.Location = new System.Drawing.Point(547, 111);
            this.rbRemote.Name = "rbRemote";
            this.rbRemote.Size = new System.Drawing.Size(62, 17);
            this.rbRemote.TabIndex = 9;
            this.rbRemote.TabStop = true;
            this.rbRemote.Text = "Remota";
            this.rbRemote.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(44, 249);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(856, 81);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Revisar Licencia Remota...";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(169, 46);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(215, 29);
            this.button3.TabIndex = 3;
            this.button3.Text = "&Obtener Detalle de Licencias...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(169, 20);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(670, 20);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = "http://localhost:52557/CheckCenterWS.asmx";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(161, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "URL Biometrika License Center :";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(1177, 320);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(127, 23);
            this.button5.TabIndex = 34;
            this.button5.Text = "&Borrar Todo";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.WebViewLicense);
            this.groupBox2.Location = new System.Drawing.Point(44, 358);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(681, 418);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vista XML...";
            // 
            // WebViewLicense
            // 
            this.WebViewLicense.Location = new System.Drawing.Point(13, 19);
            this.WebViewLicense.MinimumSize = new System.Drawing.Size(20, 20);
            this.WebViewLicense.Name = "WebViewLicense";
            this.WebViewLicense.Size = new System.Drawing.Size(660, 381);
            this.WebViewLicense.TabIndex = 14;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rtbInfoLic);
            this.groupBox3.Location = new System.Drawing.Point(731, 358);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(605, 418);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Vista Información...";
            // 
            // rtbInfoLic
            // 
            this.rtbInfoLic.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.rtbInfoLic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbInfoLic.Location = new System.Drawing.Point(7, 19);
            this.rtbInfoLic.Name = "rtbInfoLic";
            this.rtbInfoLic.ReadOnly = true;
            this.rtbInfoLic.Size = new System.Drawing.Size(581, 381);
            this.rtbInfoLic.TabIndex = 0;
            this.rtbInfoLic.Text = "";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1348, 788);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.rbRemote);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.rbLocal);
            this.Controls.Add(this.grpLocal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Biometrika Licence View Information...";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grpLocal.ResumeLayout(false);
            this.grpLocal.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox grpLocal;
        private System.Windows.Forms.Button btnReadLicense;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtPathLicense;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbLocal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rbRemote;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.WebBrowser WebViewLicense;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox rtbInfoLic;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

