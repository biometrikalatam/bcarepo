﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.License.ViewLicenseInfo
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Seleccione un archivo de licencia...";
                //openFileDialog1.Filter = "xml|*.xml";
                DialogResult dr = openFileDialog1.ShowDialog(this);

                if (dr.Equals(DialogResult.OK))
                {
                    txtPathLicense.Text = openFileDialog1.FileName;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btnReadLicense_Click(object sender, EventArgs e)
        {
            string[] arratr;
            Common.BKLicenseAtributoAdicional itemList = null;
            List<Common.BKLicenseAtributoAdicional> listAtrAdic = null;
            string msg;
            try
            {
                if (String.IsNullOrEmpty(txtPathLicense.Text))
                {
                    MessageBox.Show(this, "Debe seleccionar un archivo de licencia...", "Atencion...",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string sLicense = System.IO.File.ReadAllText(txtPathLicense.Text);

                    Common.BKLicense lic = null;

                    lic = Common.Utils.OGetLicense(sLicense);
                    lic.Key = "";
                    string xmllic = Common.XmlUtils.SerializeObject<Common.BKLicense>(lic);
                    if (lic != null && xmllic != null)
                    {
                        //Muestro en WebBroewser control
                        System.IO.File.WriteAllText(Application.StartupPath + "\\tmp.xml", xmllic);
                        WebViewLicense.Navigate(Application.StartupPath + "\\tmp.xml");

                        //Muestro en Texto
                        rtbInfoLic.Text = "Información Detallada de la Lciencia" + Environment.NewLine + "----------------------------------------------------------------------------- ";
                        rtbInfoLic.Text += Environment.NewLine + "Cliente = " + lic.Cliente;
                        rtbInfoLic.Text += Environment.NewLine + "Codigo = " + lic.Codigo;
                        rtbInfoLic.Text += Environment.NewLine + "Tipo Licencia = " + lic.TipoLicencia;
                        rtbInfoLic.Text += Environment.NewLine + "Fecha Desde = " + lic.FechaDesde;
                        rtbInfoLic.Text += Environment.NewLine + "Fecha Hasta = " + lic.FechaHasta + Environment.NewLine;
                        rtbInfoLic.Text += Environment.NewLine + "   Atributos Adicionales" + Environment.NewLine + "---------------------------------------";
                        foreach (Common.BKLicenseAtributoAdicional item in lic.AtributosAdicionales)
                        {
                            rtbInfoLic.Text += Environment.NewLine + "   " + item.Nombre + " => " + item.Valor;
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, "Error parseando o verificando licencia...", "Atención...");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error => " + ex.Message, "Atencion...", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            System.IO.File.Delete(Application.StartupPath + "\\tmp.xml");
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.IO.File.Delete(Application.StartupPath + "\\tmp.xml");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int ret = 0;
            string oLic;
            string msgerr;
            using (LicenseCheckCenterWS.CheckCenterWS ws = new LicenseCheckCenterWS.CheckCenterWS())
            {
                ws.Timeout = 600000;
                ret = ws.GetAvailableVolumeLicense("", "", out msgerr, out oLic);
            }

            if (oLic != null && ret == 0)
            {
                //Muestro en WebBroewser control
                System.IO.File.WriteAllText(Application.StartupPath + "\\tmp.xml", oLic);
                WebViewLicense.Navigate(Application.StartupPath + "\\tmp.xml");

                //Parseo licencia
                Store _STORE = Common.XmlUtils.DeserializeObject<Store>(oLic);
                
                if (_STORE != null && _STORE.ListStoreItems != null)
                {
                    rtbInfoLic.Text = "Información Detallada de las Lciencias en Servicio Central" + Environment.NewLine + 
                        "--------------------------------------------------------------------------------------------------------------- " + Environment.NewLine;

                    if (_STORE.ListStoreItems.Count == 0)
                    {
                        rtbInfoLic.Text += "No existen licencias configradas en el servicio central!";
                    } else
                    {
                        int i = 1;
                    
                        foreach (StoreItem item in _STORE.ListStoreItems)
                        {
                            rtbInfoLic.Text += Environment.NewLine + "Licencia " + i++ + " -----------------------------------------------------------------------------";
                            rtbInfoLic.Text += Environment.NewLine + "Id Volume License = " + item.Id + "[Es Válida? =" + item.IsValid.ToString() + "]";
                            rtbInfoLic.Text += Environment.NewLine + "   Detalles => Válida Desde: " + item.FechaDesde.ToString("dd/MM/yyyy") + "- Hasta: " + item.FechaHasta.ToString("dd/MM/yyyy");
                            rtbInfoLic.Text += Environment.NewLine + "               Tipo Licencia = " + item.LicenseType;
                            rtbInfoLic.Text += Environment.NewLine + "               Code = " + item.Code;
                            rtbInfoLic.Text += Environment.NewLine + "               Total = " + item.QTotal;
                            rtbInfoLic.Text += Environment.NewLine + "               Usadas = " + (item.QTotal-item.QAvailable);
                            rtbInfoLic.Text += Environment.NewLine + "               Disponibles = " + item.QAvailable;
                            rtbInfoLic.Text += Environment.NewLine + "  >> Clientes Registrados -------------";
                            foreach (Client client in item.ListClientsRegistered)
                            {
                                rtbInfoLic.Text += Environment.NewLine + "     >> MAC:" + (!string.IsNullOrEmpty(client.MacAdress)?client.MacAdress:"") + " / IP:"
                                                                                   + (!string.IsNullOrEmpty(client.IP)?client.IP:"") + " / HDD:"
                                                                                   + (!string.IsNullOrEmpty(client.HDDSerial)?client.HDDSerial:"");
                            }
                            //rtbInfoLic.Text += Environment.NewLine + "Fecha Desde = " + lic.FechaDesde;
                            //rtbInfoLic.Text += Environment.NewLine + "Fecha Hasta = " + lic.FechaHasta + Environment.NewLine;
                            //rtbInfoLic.Text += Environment.NewLine + "   Atributos Adicionales" + Environment.NewLine + "---------------------------------------";
                            //foreach (Common.BKLicenseAtributoAdicional item in lic.AtributosAdicionales)
                            //{
                            //    rtbInfoLic.Text += Environment.NewLine + "   " + item.Nombre + " => " + item.Valor;
                            //}
                        }
                    }
                    
                }

            } else
            {
                rtbInfoLic.Text = "Error consultando servicio => Codigo Retorno = " + ret + " - MsgErr = " + msgerr;
            }
        }
    }
}
