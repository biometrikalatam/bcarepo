﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biometrika.License.ViewLicenseInfo
{

    public class Store
    {
        public string Company;
        public List<StoreItem> ListStoreItems;
        public Store() { }
    }

    public class StoreItem
    {
        public StoreItem() { }

        public string Id;
        public bool IsValid;
        public string Code;
        public string LicenseType;
        public DateTime FechaDesde;
        public DateTime FechaHasta;

        //public List<Common.BKLicense> ListVolumeLicenses;
        public List<Client> ListClientsRegistered;

        //Para control de ocupados en la operacion rápida
        public int QTotal { get; set; }
        public int QAvailable { get; set; }

    }

    public class Client
    {
        public Client() { }

        public string IP;
        public string MacAdress;
        public string HDDSerial;

    }
}
