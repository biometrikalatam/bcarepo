﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.License.Generator
{
    public static class LicenseGenerator
    {
        
        public static string GenerateLicense(string tipolicense, string cliente, string fdesde, string fhasta, string days,
            string key, string codigo, string producto, List<Common.BKLicenseAtributoAdicional> itemList)
        {
            string filelicense = null;
            string format = "dd/MM/yyyy";
            CultureInfo provider = CultureInfo.InvariantCulture;
            try
            {
                if (!tipolicense.Equals("Produccion") && !tipolicense.Equals("Trial"))
                {
                    tipolicense = "Trial";
                }

                if (tipolicense.Equals("Trial"))
                {
                    int trialdays = 30;
                    try
                    {
                        trialdays = Convert.ToInt32(days);
                        if (trialdays > 60) trialdays = 60;
                    }
                    catch (Exception ex)
                    {
                        trialdays = 30;
                    }
                    fdesde = DateTime.Now.ToString(format, provider);
                    fhasta = DateTime.Now.AddDays(trialdays).ToString(format, provider);
                }

                filelicense = "-- Licencia de Producto Biometrika --";

                filelicense += Environment.NewLine + "Cliente: " + cliente;
                filelicense += Environment.NewLine + "Codigo: " + codigo;
                filelicense += Environment.NewLine + "Producto: " + producto;
                filelicense += Environment.NewLine + "Tipo Licencia: " + tipolicense;
                filelicense += Environment.NewLine + "Fecha Desde: " + fdesde;
                filelicense += Environment.NewLine + "Fecha Hasta: " + fhasta;

                //Para revisar que venga atributo con valor IsLIcenseVolume = true => Se coloca Qtotal con el 
                //atributo QTotal si existe sino se coloca valor default 1.
                bool _isLicenseVolume = false;
                int _QVolumeTotal = 0;
                if (itemList != null)
                {
                    foreach (Common.BKLicenseAtributoAdicional item in itemList)
                    {
                        if (string.Equals(item.Nombre, "IsLicenseVolume"))
                        {
                            _isLicenseVolume = true;
                        }
                        if (string.Equals(item.Nombre, "QVolumeTotal")) _QVolumeTotal = Convert.ToInt32(item.Valor);
                        filelicense += Environment.NewLine + item.Nombre + ": " + item.Valor;
                    }
                    if (_isLicenseVolume)
                    {
                        string _idVolume = "IdVolume_" + Guid.NewGuid().ToString("N");
                        Common.BKLicenseAtributoAdicional attr = new Common.BKLicenseAtributoAdicional();
                        attr.Nombre = "IdVolume";
                        attr.Valor = _idVolume;
                        itemList.Add(attr);
                        filelicense += Environment.NewLine + attr.Nombre + ": " + attr.Valor;

                        if (_QVolumeTotal == 0)
                        { //Significa que se seteo como volumen la licencia pero no viene cantidad => coloca 1 por default

                            attr = new Common.BKLicenseAtributoAdicional();
                            attr.Nombre = "QVolumeTotal";
                            attr.Valor = "1";
                            itemList.Add(attr);
                            filelicense += Environment.NewLine + attr.Nombre + ": " + attr.Valor;
                        }
                    }
                }

                filelicense += Environment.NewLine + Environment.NewLine + "--- BEGIN LICENSE ---";
                
                DateTime dtDesde = DateTime.ParseExact(fdesde, format, provider);
                DateTime dtHasta = DateTime.ParseExact(fhasta, format, provider);
                Common.BKLicense lic = new Common.BKLicense(cliente, dtDesde, dtHasta, codigo, producto, tipolicense, key, itemList);
                string xmlLicense = Common.XmlUtils.SerializeAnObject(lic);
                string xmlLicenseProtected = Common.Utils.GetLicenseProtected(xmlLicense);

                filelicense += Environment.NewLine + xmlLicenseProtected;

                filelicense += Environment.NewLine + "--- END LICENSE ---";


            }
            catch (Exception ex)
            {
                filelicense = null;
            }
            return filelicense;
        }
    }
}
