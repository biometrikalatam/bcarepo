﻿using log4net.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.License.TestDemo
{
    public partial class frmMain : Form
    {
        Biometrika.License.Common.BKLicense oLic;
        Biometrika.License.Check.LicenseCheck oCHECK;

       
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //0.- Inicializo LOG
            XmlConfigurator.Configure(new FileInfo("Logger.cfg"));

            oCHECK = new Check.LicenseCheck();
            txtPathLicense.Text = Application.StartupPath + "\\data\\Produccion_BP4_Cliente_180209105432.txt";

           
        }


        /// <summary>
        /// Dado un path del archivo de licencia, y un codigo de producto, lee la licencia y chequea validez, SOLO UNA VEZ, 
        /// dado que el parámetro 3 está en 0, no lanza el thread de chequeo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReadLicense_Click(object sender, EventArgs e)
        {
            string msg;
            int ret = oCHECK.CheckLicense(txtPathLicense.Text, txtCode.Text, 0, out oLic, out msg);
            if (ret == 0)
            {
                btnGetAtr.Enabled = true;
                MessageBox.Show(this, "Licencia Válida => " + Environment.NewLine + oLic.ToString());
            } else
            {
                MessageBox.Show(this, "Licencia Inválida [" + ret + "] => " + Environment.NewLine + (oLic != null ? oLic.ToString() : "") +
                    " - Msg=" + msg);
            }
        }

        /// <summary>
        /// Devuelve el valor de un valor adicional en la licencia, para que por licencia en cada producto se pueda habilitar/deshabilitar 
        /// funicionalidades. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetAtr_Click(object sender, EventArgs e)
        {
            if (oLic != null)
            {
                txtValor.Text = oLic.GetValueAtrAdic(txtNombre.Text.Trim());
            }
        }

        /// <summary>
        /// Dado un path del archivo de licencia, y un codigo de producto, lee la licencia y chequea validez, con periodicidad, 
        /// dado que el parámetro 3 está es > 0, Lanza el thread de chequeo y se ejecuta cada Convert.ToInt32(txtTFCP.Text.Trim())
        /// milisegundos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            string msg;
            int ret = oCHECK.CheckLicense(txtPathLicense.Text, txtCode.Text, Convert.ToInt32(txtTFCP.Text.Trim()), out oLic, out msg);
            if (ret == 0)
            {
                btnGetAtr.Enabled = true;
                MessageBox.Show(this, "Licencia Válida => " + Environment.NewLine + oLic.ToString());
            }
            else
            {
                MessageBox.Show(this, "Licencia Inválida [" + ret + "] => " + Environment.NewLine + (oLic != null ? oLic.ToString() : "") +
                    " - Msg=" + msg);
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            oCHECK.KillCheckPeriodic();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int ret = 0;
            string oLic;
            string msgerr;
            using (LicenseCheckCenterWS.CheckCenterWS ws = new LicenseCheckCenterWS.CheckCenterWS())
            {
                ws.Timeout = 600000;
                ret = ws.CheckVolumeLicense(txtIdVolume.Text, txtIdCliente.Text, out msgerr, out oLic);
            }
            txtVolumeRet.Text = ret + " - " + msgerr;
            rtResult.Text = oLic;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int ret = 0;
            string oLic;
            string msgerr;
            using (LicenseCheckCenterWS.CheckCenterWS ws = new LicenseCheckCenterWS.CheckCenterWS())
            {
                ws.Timeout = 600000;
                ret = ws.GetAvailableVolumeLicense(txtIdVolume.Text, txtCode1.Text, out msgerr, out oLic);
            }
            txtVolumeRet.Text = ret + " - " + msgerr;
            rtResult.Text = oLic;
        }
    }
}
