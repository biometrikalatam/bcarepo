﻿namespace Biometrika.License.TestDemo
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.txtCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtPathLicense = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnReadLicense = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnGetAtr = new System.Windows.Forms.Button();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTFCP = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.txtVolumeRet = new System.Windows.Forms.TextBox();
            this.txtIdCliente = new System.Windows.Forms.TextBox();
            this.txtIdVolume = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.rtResult = new System.Windows.Forms.RichTextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCode1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(72, 63);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(86, 20);
            this.txtCode.TabIndex = 11;
            this.txtCode.Text = "BP4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Código :";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(824, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(24, 20);
            this.button1.TabIndex = 9;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // txtPathLicense
            // 
            this.txtPathLicense.Location = new System.Drawing.Point(72, 26);
            this.txtPathLicense.Name = "txtPathLicense";
            this.txtPathLicense.Size = new System.Drawing.Size(746, 20);
            this.txtPathLicense.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Archivo :";
            // 
            // btnReadLicense
            // 
            this.btnReadLicense.Location = new System.Drawing.Point(188, 63);
            this.btnReadLicense.Name = "btnReadLicense";
            this.btnReadLicense.Size = new System.Drawing.Size(146, 20);
            this.btnReadLicense.TabIndex = 12;
            this.btnReadLicense.Text = "Check Licencia";
            this.btnReadLicense.UseVisualStyleBackColor = true;
            this.btnReadLicense.Click += new System.EventHandler(this.btnReadLicense_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(593, 65);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(188, 20);
            this.button2.TabIndex = 13;
            this.button2.Text = "Check Licencia con Thread";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnGetAtr
            // 
            this.btnGetAtr.Enabled = false;
            this.btnGetAtr.Location = new System.Drawing.Point(203, 110);
            this.btnGetAtr.Name = "btnGetAtr";
            this.btnGetAtr.Size = new System.Drawing.Size(170, 20);
            this.btnGetAtr.TabIndex = 16;
            this.btnGetAtr.Text = "Get Valor Atributo Adicional";
            this.btnGetAtr.UseVisualStyleBackColor = true;
            this.btnGetAtr.Click += new System.EventHandler(this.btnGetAtr_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(72, 110);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(125, 20);
            this.txtNombre.TabIndex = 15;
            this.txtNombre.Text = "IP";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Nombre :";
            // 
            // txtValor
            // 
            this.txtValor.Location = new System.Drawing.Point(441, 110);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(163, 20);
            this.txtValor.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(398, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Valor :";
            // 
            // txtTFCP
            // 
            this.txtTFCP.Location = new System.Drawing.Point(501, 64);
            this.txtTFCP.Name = "txtTFCP";
            this.txtTFCP.Size = new System.Drawing.Size(86, 20);
            this.txtTFCP.TabIndex = 20;
            this.txtTFCP.Text = "5000";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(369, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Time for Check Periodic :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(498, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(208, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "En Milisegundos => Ej: 5000 = 5 segundos";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(34, 198);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(170, 20);
            this.button3.TabIndex = 22;
            this.button3.Text = "Check Volume Licencia";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtVolumeRet
            // 
            this.txtVolumeRet.Location = new System.Drawing.Point(210, 199);
            this.txtVolumeRet.Name = "txtVolumeRet";
            this.txtVolumeRet.Size = new System.Drawing.Size(649, 20);
            this.txtVolumeRet.TabIndex = 23;
            // 
            // txtIdCliente
            // 
            this.txtIdCliente.Location = new System.Drawing.Point(34, 172);
            this.txtIdCliente.Name = "txtIdCliente";
            this.txtIdCliente.Size = new System.Drawing.Size(163, 20);
            this.txtIdCliente.TabIndex = 24;
            this.txtIdCliente.Text = "HDD111111";
            // 
            // txtIdVolume
            // 
            this.txtIdVolume.Location = new System.Drawing.Point(227, 172);
            this.txtIdVolume.Name = "txtIdVolume";
            this.txtIdVolume.Size = new System.Drawing.Size(278, 20);
            this.txtIdVolume.TabIndex = 25;
            this.txtIdVolume.Text = "IdVolume_9d01afe8fba1465d8c38a68bac76da2e";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "IdCliente :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(224, 156);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Id Volume :";
            // 
            // rtResult
            // 
            this.rtResult.Location = new System.Drawing.Point(34, 225);
            this.rtResult.Name = "rtResult";
            this.rtResult.Size = new System.Drawing.Size(825, 375);
            this.rtResult.TabIndex = 28;
            this.rtResult.Text = "";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(689, 172);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(170, 20);
            this.button4.TabIndex = 29;
            this.button4.Text = "Get Available  Volume Licencia";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(586, 156);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "Code :";
            // 
            // txtCode1
            // 
            this.txtCode1.Location = new System.Drawing.Point(589, 172);
            this.txtCode1.Name = "txtCode1";
            this.txtCode1.Size = new System.Drawing.Size(94, 20);
            this.txtCode1.TabIndex = 30;
            this.txtCode1.Text = "BC7";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(887, 629);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtCode1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.rtResult);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtIdVolume);
            this.Controls.Add(this.txtIdCliente);
            this.Controls.Add(this.txtVolumeRet);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtTFCP);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtValor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnGetAtr);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnReadLicense);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtPathLicense);
            this.Controls.Add(this.label2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Demo uso Biometrika.License.Check...";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtPathLicense;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnReadLicense;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnGetAtr;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTFCP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtVolumeRet;
        private System.Windows.Forms.TextBox txtIdCliente;
        private System.Windows.Forms.TextBox txtIdVolume;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox rtResult;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCode1;
    }
}

