﻿using Biometrika.Solutions.Shared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Biometrika.Solutions.BPWeb.Repositorios
{
    public class HttpResponseWrapper<T>
    {
        public HttpResponseWrapper(T response, bool error, HttpResponseMessage httpResponseMessage)
        {
            Error = error;
            Response = response;
            HttpResponseMessage = httpResponseMessage;
            //if (!error) //Si no hay error, deserializo el BSResponse estandar asi cada cual trabaja desde ahi
            //{
            //    try
            //    {

            //    }
            //    catch (Exception ex)
            //    {
            //        Error = false;

            //    }
            //}
        }

        public bool Error { get; set; }
        public T Response { get; set; }
        public HttpResponseMessage HttpResponseMessage { get; set; }

        public BSResponse BSResponse { get; set; }

        public async Task<string> GetBody()
        {
            return await HttpResponseMessage.Content.ReadAsStringAsync();
        }
    }
}
