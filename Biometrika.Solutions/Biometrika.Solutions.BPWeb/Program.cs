using Biometrika.Solutions.BPWeb;
using Biometrika.Solutions.BPWeb.Helpers;
using Biometrika.Solutions.BPWeb.Repositorios;
using Biometrika.Solutions.BPWeb.Services;
using Biometrika.Solutions.BPWeb.Services.Interfaces;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Radzen;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

var BaseURLBP = builder.Configuration.GetValue<string>("BaseURLBP");

//builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
builder.Services.AddHttpClient<HttpClientConToken>(
              cliente => cliente.BaseAddress = new Uri(BaseURLBP))
              .AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>();

builder.Services.AddHttpClient<HttpClientSinToken>(
   cliente => cliente.BaseAddress = new Uri(BaseURLBP));

builder.Services.AddHttpClient("Biometrika.Solutions.ServerAPI",
    client => client.BaseAddress = new Uri(BaseURLBP))
    .AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>();

// Supply HttpClient instances that include access tokens when making requests to the server project
builder.Services.AddScoped(sp => sp.GetRequiredService<IHttpClientFactory>().CreateClient("Biometrika.Solutions.BPWeb"));

builder.Services.AddApiAuthorization();

//Radzen
builder.Services.AddScoped<ContextMenuService>();
builder.Services.AddScoped<DialogService>();
builder.Services.AddScoped<NotificationService>();
builder.Services.AddScoped<TooltipService>();

//Service Biometrika
builder.Services.AddOptions();
builder.Services.AddScoped<IRepositorio, Repositorio>();
builder.Services.AddScoped<IMostrarMensajes, MostrarMensajes>();
builder.Services.AddScoped<IServiceBP, ServiceBP>();

await builder.Build().RunAsync();
