﻿using Biometrika.Solutions.BPWeb.Services;
using Biometrika.Solutions.Shared;
using Biometrika.Solutions.Shared.DTOs;

namespace Biometrika.Solutions.BPWeb.Services.Interfaces
{
    public interface IServiceBP
    {
        Task<ResponseDTO> CIGetList(FilterDTO filters);
        Task<ResponseDTO> InitializeBpTx(string trackid);
    }
}
