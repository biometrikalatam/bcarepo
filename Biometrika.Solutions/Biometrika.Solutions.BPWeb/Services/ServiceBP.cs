﻿using Biometrika.Solutions.BPWeb.Services.Interfaces;
using Biometrika.Solutions.BPWeb.Repositorios;
using Biometrika.Solutions.Shared.DTOs;
using Newtonsoft.Json;

namespace Biometrika.Solutions.BPWeb.Services
{
    public class ServiceBP : IServiceBP
    {

        private readonly IRepositorio _Repositorio;

        public ServiceBP(IRepositorio Repositorio)
        {
            _Repositorio = Repositorio;
        }
        public async Task<ResponseDTO> CIGetList(FilterDTO filters)
        {
            ResponseDTO response = new ResponseDTO(0, null, null);
            try
            {
                var responseHttp1 = await _Repositorio.Post<FilterDTO, BSResponse>("CI/citxlist", filters);
                if (responseHttp1 != null && responseHttp1.Response != null && responseHttp1.Response.response != null)
                {
                    response.Response = JsonConvert.DeserializeObject<List<BPtxGridDTO>>(responseHttp1.Response.response.ToString());
                }
            }
            catch (Exception ex)
            {
                response = new ResponseDTO(-1, "Error recuperando lista de transacciones [" + ex.Message + "]", null);
            }
            return response;
        }

        public async Task<ResponseDTO> InitializeBpTx(string trackid)
        {
            ResponseDTO response = new ResponseDTO(0, null, null);
            try
            {
                var responseHttp1 = await _Repositorio.Get<BSResponse>("api/v1/bptx/initialize/" + trackid, false);
                if (responseHttp1 != null && responseHttp1.Response != null && responseHttp1.Response.response != null)
                {
                    response.Response = JsonConvert.DeserializeObject<BpTxDTO>(responseHttp1.Response.response.ToString());
                }
            }
            catch (Exception ex)
            {
                response = new ResponseDTO(-1, "Error recuperando transaccion [" + ex.Message + "]", null);
            }
            return response;
        }
    }
}
