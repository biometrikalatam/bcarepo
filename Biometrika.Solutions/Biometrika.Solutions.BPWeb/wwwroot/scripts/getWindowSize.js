
export function getWindowSize() {
    return {
        width: window.innerWidth,
        height: window.innerHeight
    };
};


export function close_tab() {
    if (confirm("Do you want to close this tab?")) {
        window.close();
    }
}