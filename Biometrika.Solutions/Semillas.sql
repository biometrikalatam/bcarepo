USE [BiometrikaSolutions]
GO

INSERT INTO [dbo].[company]
           ([rut],[address],[name],[phone],[phone2],[fax],[createDate],[endate],[updateDate],[domain],[additionaldata],[contactname],[status],[holding],[accessname],[secretkey])
     VALUES
           ('76102607-0','Tabancura 1515','Biometrika S.A.','56224029772','56224029772','56224029772',getdate(),null,getdate(),'@biometrika',null,'Gustavo Suhit',0,0,'username1','secretkey1')
GO


INSERT INTO [dbo].[bp_company_metamap]
           ([companyid], [name] ,[description], [uniquemetamapguid])
     VALUES
           (1, 'Liveness', 'Realiza Liveness con Metamap', '62c380090cfa49001bf70cd7')
GO


INSERT INTO [dbo].[bp_company_theme]
            ([companyid],[name],[jsonconfig])
     VALUES
           (1, 'Theme1','{ "backcolor": "#45DF32"}')
GO

INSERT INTO [dbo].[bp_merit]
           ([name],[description],[uniquemetamapguid])
     VALUES
           ('Geolocalizacion','Geolocalizacion Biometrika','62c380090cfa49001bf7abcd')
GO


INSERT INTO [dbo].[bp_company_workflow]
           ([companyid],[name],[theme],[expiration])
     VALUES
           (1 ,'Wokflow1','Theme1', 60)
GO

INSERT INTO [dbo].[bp_company_workflow_item]
           ([bpcompanyworkflowid],[name],[jsonconfig],[bpcompanymetamapaid],[bpmeritid])
     VALUES
           (1,'Default','{ "backcolor": "#45DF32"}',1,1)
GO

