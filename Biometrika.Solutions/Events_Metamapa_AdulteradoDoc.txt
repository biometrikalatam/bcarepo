{
  "computed": {
    "age": {
      "data": 52
    },
    "isDocumentExpired": {
      "data": {
        "national-id": false
      }
    }
  },
  "documents": [
    {
      "country": "CL",
      "region": null,
      "type": "national-id",
      "steps": [
        {
          "status": 200,
          "id": "chilean-registro-civil-validation",
          "error": {
            "type": "SystemError",
            "code": "system.internalError",
            "message": "Internal error"
          }
        },
        {
          "status": 200,
          "id": "chilean-driver-license-validation",
          "error": {
            "type": "SystemError",
            "code": "system.internalError",
            "message": "Internal error"
          }
        },
        {
          "status": 200,
          "id": "chilean-rut-validation",
          "error": null,
          "data": {
            "fullName": "CARLOS ALEJANDRO PARDO LOPEZ",
            "rut": "11641418-K",
            "active": true,
            "economicActivities": [
              {
                "activity": "OTRAS ACTIVIDADES DE SERVICIOS DE APOYO A LAS EMPRESAS N.C.P.",
                "code": "829900",
                "category": "Segunda",
                "affectsVat": false,
                "date": "28-05-2005"
              },
              {
                "activity": "ACTIVIDADES DE CONSULTORIA DE INFORMATICA Y DE GESTION DE INSTALACIONE",
                "code": "620200",
                "category": "Segunda",
                "affectsVat": false,
                "date": "25-12-1994"
              }
            ]
          }
        },
        {
          "status": 200,
          "id": "age-check",
          "data": {
            "age": 52,
            "ageThreshold": 18,
            "underage": false
          },
          "error": null
        },
        {
          "status": 200,
          "id": "alteration-detection",
          "error": {
            "type": "StepError",
            "code": "alterationDetection.fraudAttempt",
            "message": "Document is considered as fraud attempt"
          }
        },
        {
          "status": 200,
          "id": "facematch",
          "data": {
            "score": 100
          },
          "error": null
        },
        {
          "status": 200,
          "id": "premium-aml-watchlists-search-validation",
          "error": null,
          "data": {
            "nameSearched": "CARLOS ALEJANDRO PARDO LOPEZ",
            "profileUrl": "https://app.complyadvantage.com/public/search/1656972160-cXrTnOUn/27b0ae284909",
            "searchedOn": "2022-07-04 22:02:40",
            "searchId": 959371749
          }
        },
        {
          "status": 200,
          "id": "template-matching",
          "error": null
        },
        {
          "status": 200,
          "id": "document-reading",
          "data": {
            "fullName": {
              "value": "CARLOS ALEJANDRO PARDO LOPEZ",
              "required": true,
              "label": "Name"
            },
            "emissionDate": {
              "value": "2020-03-25",
              "required": false,
              "label": "Emission Date",
              "format": "date"
            },
            "documentNumber": {
              "value": "521.954.421",
              "required": true,
              "label": "Document Number"
            },
            "dateOfBirth": {
              "value": "1970-03-24",
              "required": true,
              "label": "Day of Birth",
              "format": "date"
            },
            "expirationDate": {
              "value": "2030-03-24",
              "required": false,
              "label": "Date of Expiration",
              "format": "date"
            },
            "documentType": {
              "value": "IN",
              "required": false,
              "label": "Document Type"
            },
            "firstName": {
              "value": "CARLOS ALEJANDRO",
              "required": false,
              "label": "First Name"
            },
            "issueCountry": {
              "value": "CHL",
              "required": false,
              "label": "Issue Country"
            },
            "nationality": {
              "value": "CHL",
              "required": false,
              "label": "Nationality"
            },
            "optional1": {
              "value": "P02",
              "required": false,
              "label": "Optional 1"
            },
            "optional2": {
              "value": "11641418 K",
              "required": false,
              "label": "Optional 2"
            },
            "runNumber": {
              "value": "11.641.418-K",
              "required": true,
              "label": "Run number"
            },
            "sex": {
              "value": "M",
              "required": false,
              "label": "Sex"
            },
            "surname": {
              "value": "PARDO LOPEZ",
              "required": false,
              "label": "Surname"
            }
          },
          "error": null
        },
        {
          "status": 200,
          "id": "watchlists",
          "error": null
        }
      ],
      "fields": {
        "dateOfBirth": {
          "value": "1970-03-24"
        },
        "documentNumber": {
          "value": "521.954.421"
        },
        "documentType": {
          "value": "IN"
        },
        "emissionDate": {
          "value": "2020-03-25"
        },
        "expirationDate": {
          "value": "2030-03-24"
        },
        "firstName": {
          "value": "CARLOS ALEJANDRO"
        },
        "fullName": {
          "value": "CARLOS ALEJANDRO PARDO LOPEZ"
        },
        "issueCountry": {
          "value": "CHL"
        },
        "nationality": {
          "value": "CHL"
        },
        "optional1": {
          "value": "P02"
        },
        "optional2": {
          "value": "11641418 K"
        },
        "runNumber": {
          "value": "11.641.418-K"
        },
        "sex": {
          "value": "M"
        },
        "surname": {
          "value": "PARDO LOPEZ"
        }
      },
      "photos": [
        "https://media.getmati.com/file?location=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6ImU3OGQzMTIzLTdkZTEtNGE2MC1hNWY0LTIxZjhlMDBkYmI1OS5qcGVnIiwiZm9sZGVyIjoiZG9jdW1lbnQiLCJpYXQiOjE2NjExODA4MjUsImV4cCI6MTY2MTI2NzIyNSwiYXVkIjoiZWFjMTYwZmYtYTBlNS00M2QxLWFkNTktNTIwMjI1YmQ3OWI4In0.W_RarKrSUpHvd-FoR3k4Pq4_MjeTb6yOXxAjfMrkXIk",
        "https://media.getmati.com/file?location=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6ImEwZjczOTJiLTgwOGQtNDFmMi05ZDU0LTliMzlkYmQ1YWY2My5qcGVnIiwiZm9sZGVyIjoiZG9jdW1lbnQiLCJpYXQiOjE2NjExODA4MjUsImV4cCI6MTY2MTI2NzIyNSwiYXVkIjoiZWFjMTYwZmYtYTBlNS00M2QxLWFkNTktNTIwMjI1YmQ3OWI4In0.wdFKhYW3mwpODZeoMOgj-y3jLanZsrFFxYXtg-x3JjU"
      ]
    }
  ],
  "expired": false,
  "flow": {
    "id": "62c34ef510147a001c5ca46e",
    "name": "Biometrika1 (preview)"
  },
  "identity": {
    "status": "rejected"
  },
  "steps": [
    {
      "status": 200,
      "id": "email-ownership-validation",
      "data": {
        "emailAddress": "gsuhit@biometrika.cl"
      },
      "error": null
    },
    {
      "status": 200,
      "id": "email-risk-validation",
      "error": null,
      "data": {
        "emailAddress": "gsuhit@biometrika.cl",
        "riskScore": 43,
        "riskThreshold": 80,
        "message": "Success.",
        "success": true,
        "valid": true,
        "disposable": false,
        "smtp_score": 0,
        "overall_score": 1,
        "first_name": "Unknown",
        "generic": false,
        "common": false,
        "dns_valid": true,
        "honeypot": false,
        "deliverability": "medium",
        "frequent_complainer": false,
        "spam_trap_score": "none",
        "catch_all": false,
        "timed_out": false,
        "suspect": false,
        "recent_abuse": true,
        "suggested_domain": "N/A",
        "leaked": true,
        "domain_age": {
          "human": "13 years ago",
          "timestamp": 1237595098,
          "iso": "2009-03-20T20:24:58-04:00"
        },
        "first_seen": {
          "human": "5 years ago",
          "timestamp": 1491022861,
          "iso": "2017-04-01T01:01:01-04:00"
        },
        "sanitized_email": "gsuhit@biometrika.cl",
        "domain_velocity": "none",
        "user_activity": "high",
        "associated_names": {
          "status": "No associated names found.",
          "names": []
        },
        "associated_phone_numbers": {
          "status": "No associated phone numbers found.",
          "phone_numbers": []
        },
        "request_id": "5FibiLmyOA"
      }
    },
    {
      "status": 200,
      "id": "ip-validation",
      "error": null,
      "data": {
        "country": "Chile",
        "countryCode": "CL",
        "region": "Santiago Metropolitan",
        "regionCode": "RM",
        "city": "Santiago",
        "zip": "34033",
        "latitude": -33.4513,
        "longitude": -70.6653,
        "safe": true,
        "ipRestrictionEnabled": false,
        "vpnDetectionEnabled": false,
        "platform": "web_desktop"
      }
    },
    {
      "status": 200,
      "id": "liveness",
      "data": {
        "videoUrl": "https://media.getmati.com/file?location=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6IjhhNWJhNThiLWE1ZDQtNDExNS04YzhmLWUxNDRiN2Q3ODA3Yy5ta3YiLCJmb2xkZXIiOiJ2aWRlbyIsImlhdCI6MTY2MTE4MDgyNSwiZXhwIjoxNjYxMjY3MjI1LCJhdWQiOiJlYWMxNjBmZi1hMGU1LTQzZDEtYWQ1OS01MjAyMjViZDc5YjgifQ.H0XEOCT28f99xGVMlMOS20pt1oEf7tn4_52o3fxb1Rs",
        "spriteUrl": "https://media.getmati.com/file?location=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6ImVkMjlkMTAyLWNlZmYtNDQ0Yi04NTY2LWViNjU1ZDI0MmFhZS5qcGVnIiwiZm9sZGVyIjoic2VsZmllIiwiaWF0IjoxNjYxMTgwODI1LCJleHAiOjE2NjEyNjcyMjUsImF1ZCI6ImVhYzE2MGZmLWEwZTUtNDNkMS1hZDU5LTUyMDIyNWJkNzliOCJ9.Gzow-aP4C5rDk2oyIb8MjUWcVLrIuDGW5Korffs0XNQ",
        "selfieUrl": "https://media.getmati.com/file?location=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6ImU4ZWMxNzJhLWNmZmQtNDkwNS1iOGM4LThmMTY3MzFkZWRiOC5qcGVnIiwiZm9sZGVyIjoic2VsZmllIiwiaWF0IjoxNjYxMTgwODI1LCJleHAiOjE2NjEyNjcyMjUsImF1ZCI6ImVhYzE2MGZmLWEwZTUtNDNkMS1hZDU5LTUyMDIyNWJkNzliOCJ9.frsaewXsATCjj7jonb7TpvtoqskCn9Uetxa9ZwEGArY"
      },
      "error": null
    },
    {
      "status": 200,
      "id": "phone-ownership-validation",
      "error": {
        "type": "StepError",
        "code": "phoneOwnership.skipped",
        "message": "User skipped this step"
      }
    },
    {
      "status": 200,
      "id": "phone-risk-analysis-validation",
      "error": {
        "type": "StepError",
        "code": "phoneRisk.skipped",
        "message": "User skipped this step"
      }
    },
    {
      "status": 200,
      "id": "voice",
      "data": {
        "videoUrl": "https://media.getmati.com/file?location=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6IjFkMDhmZDk2LTNmNDAtNDNhMi04ZmUxLTBlOWI1NGMzMjQ1Yy5ta3YiLCJmb2xkZXIiOiJ2aWRlbyIsImlhdCI6MTY2MTE4MDgyNSwiZXhwIjoxNjYxMjY3MjI1LCJhdWQiOiJlYWMxNjBmZi1hMGU1LTQzZDEtYWQ1OS01MjAyMjViZDc5YjgifQ.PyKa0AKFr6mq-Ie2wp0EDf1TWINBbvtFW8q5Yu-aCFQ",
        "text": "7-2-7",
        "selfiePhotoUrl": "https://media.getmati.com/file?location=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6IjZiZDM0YTMwLTY3MjgtNDlhMy1hNDhkLWZmZTY1OGI0YWZmOC5qcGVnIiwiZm9sZGVyIjoic2VsZmllIiwiaWF0IjoxNjYxMTgwODI1LCJleHAiOjE2NjEyNjcyMjUsImF1ZCI6ImVhYzE2MGZmLWEwZTUtNDNkMS1hZDU5LTUyMDIyNWJkNzliOCJ9.sUURuPvWCLyuWaQEG0FurEqc3-P4XNjKfJRadCx23x4",
        "spriteUrl": "https://media.getmati.com/file?location=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6IjMwNDQ5MWQyLWQ5MzEtNDgzZC05YTdjLTdkMmJkYTVjNzhjZS5qcGVnIiwiZm9sZGVyIjoic2VsZmllIiwiaWF0IjoxNjYxMTgwODI1LCJleHAiOjE2NjEyNjcyMjUsImF1ZCI6ImVhYzE2MGZmLWEwZTUtNDNkMS1hZDU5LTUyMDIyNWJkNzliOCJ9.8013_vuESadMEh0keZC_9NX5kl7tYNKfe2eFgVmHeCA"
      },
      "error": null
    }
  ],
  "masJobToBePostpone": false,
  "id": "62c36311707b6e001cb77ad3",
  "deviceFingerprint": {
    "ua": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36",
    "browser": {
      "name": "Chrome",
      "version": "103.0.0.0",
      "major": "103"
    },
    "engine": {
      "name": "Blink",
      "version": "103.0.0.0"
    },
    "os": {
      "name": "Windows",
      "version": "10"
    },
    "cpu": {
      "architecture": "amd64"
    },
    "app": {
      "platform": "web_desktop",
      "version": "local"
    },
    "ip": "191.112.155.58",
    "vpnDetectionEnabled": false
  },
  "hasProblem": true
}