﻿using AutoMapper;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.Model;

namespace Biometrika.Solutions.Server.Utilidades
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            //General 
            CreateMap<ApplicationUser, UserDTO>();
            CreateMap<UserDTO, ApplicationUser>();

            CreateMap<Company, CompanyDTO>();
            CreateMap<CompanyDTO, Company>();

            //BtpTx Params
            CreateMap<BpTxCreateDTO, BpTx>();

            //BP
            CreateMap<BpTx, BpTxDTO>();
            CreateMap<BpTxDTO, BpTx>();

            CreateMap<BpClientDTO, BpClient>();
            CreateMap<BpClient, BpClientDTO>();

            CreateMap<BpCompanyMetamapDTO, BpCompanyMetamap>();
            CreateMap<BpCompanyMetamap, BpCompanyMetamapDTO>();

            CreateMap<BpCompanyThemeDTO, BpCompanyTheme>();
            CreateMap<BpCompanyTheme, BpCompanyThemeDTO>();

            CreateMap<BpCompanyWorkflowDTO, BpCompanyWorkflow>();
            CreateMap<BpCompanyWorkflow, BpCompanyWorkflowDTO>();

            CreateMap<BpCompanyWorkflowItemDTO, BpCompanyWorkflowItem>();
            CreateMap<BpCompanyWorkflowItem, BpCompanyWorkflowItemDTO>();

            CreateMap<BpOriginDTO, BpOrigin>();
            CreateMap<BpOrigin, BpOriginDTO>();

            CreateMap<BpIdentity, BpIdentityDTO>();
            CreateMap<BpIdentityDTO, BpIdentity>()
                .ForMember(dest => dest.Docimagefront, opt => opt.Condition(src => !string.IsNullOrEmpty(src.Docimagefront)))
                .ForMember(dest => dest.Docimageback, opt => opt.Condition(src => !string.IsNullOrEmpty(src.Docimageback)))
                .ForMember(dest => dest.Name, opt => opt.Condition(src => !string.IsNullOrEmpty(src.Name)))
                .ForMember(dest => dest.Patherlastname, opt => opt.Condition(src => !string.IsNullOrEmpty(src.Patherlastname)))
                .ForMember(dest => dest.Motherlastname, opt => opt.Condition(src => !string.IsNullOrEmpty(src.Motherlastname)))
                .ForMember(dest => dest.Photography, opt => opt.Condition(src => !string.IsNullOrEmpty(src.Photography)))
                .ForMember(dest => dest.Valueid, opt => opt.Condition(src => !string.IsNullOrEmpty(src.Valueid)))
                .ForMember(dest => dest.Typeid, opt => opt.Condition(src => !string.IsNullOrEmpty(src.Typeid)))
                .ForMember(dest => dest.Nick, opt => opt.Condition(src => !string.IsNullOrEmpty(src.Nick)))
                .ForMember(dest => dest.Selfie, opt => opt.Condition(src => !string.IsNullOrEmpty(src.Selfie)))
                .ForMember(dest => dest.Signatureimage, opt => opt.Condition(src => !string.IsNullOrEmpty(src.Signatureimage)))
                .ForMember(dest => dest.BpIdentity3ro, opt => opt.Condition(src => src.BpIdentity3ro!=null && src.BpIdentity3ro.Count > 0))
                .ForMember(dest => dest.BpBir, opt => opt.Condition(src => src.BpBir != null && src.BpBir.Count > 0))
                .ForMember(dest => dest.BpIdentityDynamicdata, opt => opt.Condition(src => src.BpIdentityDynamicdata != null && src.BpIdentityDynamicdata.Count > 0))
                .ForMember(dest => dest.Nationality, opt => opt.Condition(src => !string.IsNullOrEmpty(src.Nationality)));

        }
    }
}
