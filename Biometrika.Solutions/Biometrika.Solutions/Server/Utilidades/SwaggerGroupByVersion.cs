﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

namespace Biometrika.Solutions.Server.Utilidades
{
    public class SwaggerGroupByVersion : IControllerModelConvention
    {
        public void Apply(ControllerModel controller)
        {
            var namespaceController = controller.ControllerType.Namespace; //Controlller.V1
            var versionAPI = namespaceController.Split('.').Last().ToLower(); // V1
            controller.ApiExplorer.GroupName = versionAPI;
        }
    }
}
