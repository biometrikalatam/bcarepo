﻿using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Server.Repositories
{
    public class RepositoryCountry : IRepositoryCountry
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<RepositoryCountry> _logger;

        public RepositoryCountry(ApplicationDbContext dbContext, ILogger<RepositoryCountry> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task<List<Country>> CountryList()
        {
            List<Country> countries = null;
            try
            {
                _logger.LogDebug("CountryController.Get IN...");
                countries = null; // await _dbContext.Country.ToListAsync();
            }
            catch (Exception ex)
            {
                //countries = null;
                string e = ex.Message;
                _logger.LogError("CountryController.Get Excp => " + ex.Message);
            }
            _logger.LogDebug("CountryController.Get OUT!");
            return countries;
        }

        public async Task<List<Country>> Get()
        {
            List<Country> countries = null;
            try
            {
                _logger.LogDebug("CountryController.Get IN...");
                countries = null; // await _dbContext.Country.ToListAsync();
            }
            catch (Exception ex)
            {
                //countries = null;
                string e = ex.Message;
                _logger.LogError("CountryController.Get Excp => " + ex.Message);
            }
            _logger.LogDebug("CountryController.Get OUT!");
            return countries;
        }
    }
}
