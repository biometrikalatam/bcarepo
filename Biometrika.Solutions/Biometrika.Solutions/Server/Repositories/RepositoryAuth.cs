﻿using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace Biometrika.Solutions.Server.Repositories
{
    public class RepositoryAuth : IRepositoryAuth
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<RepositoryCountry> _logger;
        private readonly IConfiguration _config;
        private readonly UserManager<ApplicationUser> _userManager;

        public RepositoryAuth(ApplicationDbContext dbContext,
                            ILogger<RepositoryCountry> logger,
                            IConfiguration config,
                            UserManager<ApplicationUser> userManager)
        {
            _dbContext = dbContext;
            _logger = logger;
            _config = config;
            _userManager = userManager;
        }
        public async Task<ApplicationUser> RetrieveUser(string userid)
        {
            ApplicationUser apuser = null;
            try
            {
                _logger.LogInformation("RepositoryAuth.RetrieveUser IN => userid = " + (string.IsNullOrEmpty(userid)?"Null":userid));
                if (!string.IsNullOrEmpty(userid))
                {
                    _logger.LogDebug("RepositoryAuth.RetrieveUser - In FindByIdAsync...");
                    apuser = await _userManager.FindByIdAsync(userid);
                }
            }
            catch (Exception ex)
            {
                apuser = null;
                _logger.LogError(ex, "RepositoryAuth.RetrieveUser Excp Error [" + ex.Message + "]");
            }
            _logger.LogInformation("RepositoryAuth.RetrieveUser OUT! apuser => " + (apuser == null?"NULL":apuser.UserName));
            return apuser;
        }
    }
}
