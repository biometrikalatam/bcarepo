﻿using AutoMapper;
using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using MudBlazor.Charts;

namespace Biometrika.Solutions.Server.Repositories
{
    public class RepositoryBP : IRepositoryBP
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<RepositoryCountry> _logger;
        private readonly IConfiguration _config;
        private readonly IRepositoryAuth _repositoryAuth;
        private readonly IMapper _mapper;
        public RepositoryBP(ApplicationDbContext dbContext, 
                            ILogger<RepositoryCountry> logger,
                            IConfiguration config,
                            IRepositoryAuth repositoryAuth,
                            IMapper mapper)
        {
            _dbContext = dbContext;
            _logger = logger;
            _config = config;
            _repositoryAuth = repositoryAuth;
            _mapper = mapper;
        }


#region Manager

        public async Task<List<BPtxGridDTO>> bptxListToDTOGrid(FilterDTO filters)
        {
            /*
             Filtros fosibles:
                fromdate: 10/12/2022
                tilldate: 20/12/2022
                companyid: int
                estate: int
                trackid: string
                valueid: string
                name: string
                phaterlastname: string
             * 
             */
            List<BPtxGridDTO> txList = null;
            DateTime dFrom = DateTime.MinValue;
            DateTime dTill = DateTime.MinValue;
            try
            {
                _logger.LogDebug("RepositoryBP.bptxListToDTOGrid IN...");
                //txList = await _dbContext.RdCitx.ToListAsync();

                var query = _dbContext.BpTx
                                .Include(tx => tx.Company)
                                .AsQueryable();
                    //.Select(tx =>
                    //           new RdCitxGridDTO
                    //           {
                    //               Id = tx.Id,
                    //               Trackid = tx.Trackid,
                    //               Actionid = tx.Actionid,
                    //               Valueid = tx.Valueid,
                    //               Dategen = tx.Dategen,
                    //               Score = tx.Score
                    //           });


                if (filters == null || filters.filters == null || filters.filters.Count == 0)
                {
                    _logger.LogDebug("RepositoryBP.bptxListToDTOGrid - Con filtro vacio...");
                    txList = await query.Select(tx =>
                                           new BPtxGridDTO
                                           {
                                               Id = tx.Id,
                                               TrackId = tx.Trackid,
                                               ActionType = tx.Actiontype,
                                               OperationCode = tx.Operationcode,
                                               TypeId = tx.Typeid,
                                               ValueId = tx.Valueid,
                                               DateStart = tx.Timestampstart,
                                               DateEnd = tx.Timestampend,
                                               ClientId = tx.Clientid,
                                               CompanyName = tx.Company.Name
                                           })
                                     .Where(d => d.DateStart <= DateTime.Now)
                                     .Where(d => d.DateStart > DateTime.Now.AddDays(-1 * Convert.ToInt32((string)_config["RangeGrid"])))
                                     .OrderByDescending(tx => tx.DateStart)
                                     .ToListAsync<BPtxGridDTO>();
                } else
                {
                    //Parseo fechadesde y fecha hasta sino pongo limite a mano desde hoy 30 dias para atras

                    //Desde: Si viene lo parseo, sino pongo fecha de hoy menos el rango definido en config
                    //tambien debo chequear que rango enviado no sea mayor al enviado sino acomoda a fecha hasta - RangeGrid
                    try
                    {
                        if (filters.filters.ContainsKey("fromdate"))
                        {
                            dFrom = DateTime.ParseExact(filters.filters["fromdate"], "dd/MM/yyyy", null);
                        }
                        if (filters.filters.ContainsKey("tilldate"))
                        {
                            dTill = DateTime.ParseExact(filters.filters["tilldate"], "dd/MM/yyyy", null);
                        }
                    }
                    catch (Exception ex)
                    {
                        dFrom = DateTime.MinValue;
                        dTill = DateTime.MinValue;
                    }

                    //TODO - revisar que dif de fechas no sea mayor a rangeGrid sino ajustar
                    if ((dFrom == DateTime.MinValue && dTill == DateTime.MinValue) || 
                        ((dTill - dFrom).Days > Convert.ToInt32((string)_config["RangeGrid"])))
                    {
                        if (dTill == DateTime.MinValue)
                        {
                            dTill = DateTime.Now;
                        }
                        dFrom = dTill.AddDays(-1 * Convert.ToInt32((string)_config["RangeGrid"]));
                    } 

                    query = query.Where(d => d.Timestampstart > dFrom);
                    query = query.Where(d => d.Timestampstart <= dTill);

                    //Agrego los demas filtros si vienen 
                    foreach (var item in filters.filters)
                    {
                        if (item.Key != null && item.Value != null)
                        {
                            _logger.LogDebug("RepositoryBP.bptxListToDTOGrid - Add filtro " + item.Key + " - " + item.Value);
                            if (item.Key.Equals("estate"))
                            {
                                query = query.Where(tx => tx.Actiontype == Convert.ToInt32(item.Value));
                            }
                            if (item.Key.Equals("companyid"))
                            {
                                query = query.Where(tx => tx.CompanyId == Convert.ToInt32(item.Value));
                            }
                            if (item.Key.Equals("trackid"))
                            {
                                query = query.Where(tx => tx.Trackid.Equals(item.Value));
                            }
                            if (item.Key.Equals("valueid"))
                            {
                                query = query.Where(tx => tx.Valueid.Equals(item.Value));
                            }
                            if (item.Key.Equals("client"))
                            {
                                query = query.Where(tx => tx.Clientid.Contains(item.Value));
                            }
                            //if (item.Key.Equals("phaterlastname"))
                            //{
                            //    query = query.Where(tx => tx.Phaterlastname.Contains(item.Value));
                            //}
                        }
                    }
                    txList = await query.Select(tx =>
                                           new BPtxGridDTO
                                           {
                                               Id = tx.Id,
                                               TrackId = tx.Trackid,
                                               ActionType = tx.Actiontype,
                                               OperationCode = tx.Operationcode,
                                               TypeId = tx.Typeid,
                                               ValueId = tx.Valueid,
                                               DateStart = tx.Timestampstart,
                                               DateEnd = tx.Timestampend,
                                               ClientId = tx.Clientid,
                                               CompanyName = tx.Company.Name
                                           })
                                      .OrderByDescending(tx => tx.DateStart)
                                     .ToListAsync<BPtxGridDTO>();
                }


                //txList = await _dbContext.RdCitx.Select(tx =>
                //                new RdCitxGridDTO
                //                {
                //                    Id = tx.Id,
                //                    Trackid = tx.Trackid,
                //                    Actionid = tx.Actionid,
                //                    Valueid = tx.Valueid,   
                //                    Dategen = tx.Dategen,
                //                    Score = tx.Score
                //                })
                //                .OrderByDescending(tx => tx.Dategen)
                //                .ToListAsync<RdCitxGridDTO>();
            }
            catch (Exception ex)
            {
                //countries = null;
                string e = ex.Message;
                _logger.LogError("RepositoryCI.Get Excp => " + ex.Message);
            }
            _logger.LogDebug("RepositoryCI.Get OUT!");
            return txList;
        }


#endregion Manager


#region Services

        public async Task<BSResponse> EnrollOrUpdate(int companyid, BpIdentityDTO identities)
        {
            BSResponse ret = new BSResponse(0, null, null);
            try
            {
                _logger.LogDebug("RepositoryBP.EnrollOrUpdate IN...");

                if (identities == null || string.IsNullOrEmpty(identities.Typeid) || string.IsNullOrEmpty(identities.Valueid))
                {
                    ret.code = StatusCodes.Status400BadRequest;
                    ret.message = "No pudo enrolar o actualizar una identidad por valores nulos!";
                    ret.response = null;
                }

                //TODO - Agregar filtro de que no se actualice segun ceirta fecha del config, para no actualizar seimpre. O que solo 
                //actualice si cambia fecha vencimiento de doc presentado

                BpIdentity? _Identity = await _dbContext.BpIdentity
                                                .Where(i => i.Valueid.Equals(identities.Valueid) &&
                                                       i.Typeid.Equals(identities.Typeid))
                                                .FirstOrDefaultAsync();

                if (_Identity == null)
                {
                    //Enroll
                    _Identity = _mapper.Map<BpIdentity>(identities);
                    _Identity.CompanyId = companyid;
                    _dbContext.Add(_Identity);
                    int count = await _dbContext.SaveChangesAsync();
                    if (count > 0)
                    {
                        ret.code = StatusCodes.Status201Created;
                        ret.message = null;
                        ret.response = null;
                    } else
                    {
                        ret.code = StatusCodes.Status304NotModified;
                        ret.message = "No pudo enrolar la nueva identidad [valuedid=" + identities.Valueid + "]";
                        ret.response = null;
                    }
                } else
                {
                    //Update si se cumple alguno de estos filtros
                    int daysToRefresh = 60;
                    try
                    {
                        daysToRefresh = Convert.ToInt32(_config["BPConfig:URLBaseBPWeb"]);
                    }
                    catch (Exception ex)
                    {
                        daysToRefresh = 60;
                    }
                    if ((_Identity.Documentexpirationdate.HasValue && _Identity.Documentexpirationdate.Value < DateTime.Now)
                          ||
                         (_Identity.Documentexpirationdate.HasValue && identities.Documentexpirationdate.HasValue &&
                          _Identity.Documentexpirationdate.Value < identities.Documentexpirationdate.Value))
                    {
                        int id = _Identity.Id;
                        _Identity = _mapper.Map<BpIdentityDTO, BpIdentity>(identities, _Identity);
                        _Identity.Id = id;
                        _Identity.CompanyId = companyid;
                        _dbContext.Update(_Identity);
                        int count = await _dbContext.SaveChangesAsync();
                        if (count > 0)
                        {
                            ret.code = StatusCodes.Status202Accepted;
                            ret.message = null;
                            ret.response = null;
                        }
                        else
                        {
                            ret.code = StatusCodes.Status304NotModified;
                            ret.message = "No pudo modificar la identidad [valuedid=" + identities.Valueid + "]";
                            ret.response = null;
                        }
                    } else
                    {
                        ret.code = StatusCodes.Status304NotModified;
                        ret.message = "No modifico la identidad [valuedid=" + identities.Valueid + "] porque no se actualizaban datos!";
                        ret.response = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ret.code = StatusCodes.Status500InternalServerError;
                ret.message = "RepositoryBP.EnrollOrUpdate Excp => " + ex.Message;
                ret.response = null;
                _logger.LogError(ret.message);
            }
            _logger.LogDebug("RepositoryBP.EnrollOrUpdate OUT! ret.code = " + ret.code);
            return ret;
        }

#endregion Services
    }
}
