﻿using AutoMapper;
using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Server.Repositories
{
    public class RepositoryBPInfo : IRepositoryBPInfo
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<RepositoryBPInfo> _logger;
        private readonly IConfiguration _config;
        private readonly IRepositoryAuth _repositoryAuth;
        private readonly IMapper _mapper;

        public RepositoryBPInfo(ApplicationDbContext dbContext, 
                            ILogger<RepositoryBPInfo> logger,
                            IConfiguration config,
                            IRepositoryAuth repositoryAuth,
                            IMapper mapper)
        {
            _dbContext = dbContext;
            _logger = logger;
            _config = config;
            _repositoryAuth = repositoryAuth;
            _mapper = mapper;
        }

        public Task<BSResponse> GetBpTxByTrackId(string trackid)
        {
            throw new NotImplementedException();
        }

        public Task<BSResponse> GetBpTxInfoByFilters(FilterDTO filters)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Crea la transaccion con los pa´rametros mínimos necesarios
        /// </summary>
        /// <param name="bpTxCreateDTO"></param>
        /// <param name="userid"></param>
        /// <returns>TrackId + URL de conexión por si queiren usarlo web</returns>
        public async Task<BSResponse> Report(BpTxInfoParamDTO bpTxCreateDTO, string userid)
        {
            /*
                0.- Saco user/company desde userid que viene (Ver como manejar que user/company esten disabled)
                1.- Tomo el tipo de reporte y verifico si tengo los datos en la BD local
                    1.1.- Sino uso servicio de tercero y update BD Local
                2.- LLeno de la TX todos los datos que puedo desde Workflow recuperado
                3.- Creo URL de BPWeb
                4.- Retorno TrackId + URL BPWeb + Fecha/Hora Vencimiento Tx
            */ 
            BSResponse response = new BSResponse(0, null, null);
            try
            {
                _logger.LogInformation("RepositoryBPInfo.Report IN...");
                ApplicationUser user = await _repositoryAuth.RetrieveUser(userid);
                if (user == null)
                {
                    _logger.LogDebug("RepositoryBPInfo.Report - Get User info => No Encontrado!");
                    response.code = StatusCodes.Status404NotFound;
                    response.message = "Usuario " + userid + " no encontrado!";
                    return response;
                }

                //var wf = await _dbContext.BpCompanyWorkflow
                //                    .Include(w => w.BpCompanyWorkflowItem)
                //                    .Where(w => w.Name == bpTxCreateDTO.Workflowname && w.CompanyId == user.CompanyId)
                //                    .FirstOrDefaultAsync();
                //if (wf == null)
                //{
                //    _logger.LogDebug("RepositoryBPInfo.Report - get Workflow [" + bpTxCreateDTO.Workflowname +
                //                        "] info  => No Encontrado para ComapnyId = " + user.CompanyId.ToString() + "!");
                //    response.code = StatusCodes.Status404NotFound;
                //    response.message = "Workflow " + bpTxCreateDTO.Workflowname + " no encontrado para ComapnyId = "
                //                            + user.CompanyId.ToString() + "!";
                //    return response;
                //}

                //Tengo toda la info => completo tx...
                _logger.LogDebug("RepositoryBPInfo.Report - Inicio BpTx grabacion...");
                BpTx tx = new BpTx();
                tx = _mapper.Map<BpTx>(bpTxCreateDTO);
                //if (string.IsNullOrEmpty(tx.Clientid))
                //{
                //    tx.Clientid = "ClientBPWebTx";
                //}
                //tx.Trackid = await GenerateUniqueTrackId();
                //_logger.LogDebug("RepositoryBPInfo.Report - Generado TrackId = " + tx.Trackid + "...");
                //tx.Actiontype = Biometrika.Core._6.Contant.Action.ACTION_VERIFYANDGET; //Ver si vale la pena agregar mas Actions
                //tx.Status = (int)TxStatus.Created;
                //tx.UserId = user.Id;
                //tx.CompanyId = user.CompanyId;
                //tx.BpCompanyWorkflowId = wf.Id;
                //tx.Callbackurl = wf.CallbackUrl;
                //tx.Redirecturl = wf.RedirectUrl;
                //tx.StatusType = wf.StatusType;
                //tx.Metadata = bpTxCreateDTO.Metadata;
                //tx.URLBpWeb = _config["BPConfig:URLBaseBPWeb"] + "trackid=" + tx.Trackid;
                //tx.Expiration = wf.Expiration > 0 ? DateTime.Now.AddMinutes(wf.Expiration) : null;
                tx.Timestampstart = DateTime.Now;
                tx.Timestampend = DateTime.Now;
    
                _dbContext.Add(tx);
                int record = await _dbContext.SaveChangesAsync();

                if (record == 0)
                {
                    response.code = StatusCodes.Status304NotModified;
                    response.message = "Error salvando la transaccion en base de datos!";
                } else
                {
                    response.response = new
                    {
                        TrackId = tx.Trackid,
                        URLBpWeb = _config["BPConfig:URLBaseBPWeb"] + "trackid=" + tx.Trackid,
                        Expiration = tx.Expiration
                    };

                    //var txResponse = _mapper.Map<BpTxDTO>(tx);
                    //response.response = txResponse;
                }
            }
            catch (Exception ex)
            {
                response.code = -1;
                response.message = "RepositoryBPInfo.Report Excp Error [" + ex.Message + "]";
                response.response = null;
                _logger.LogError(ex, "RepositoryBPInfo.Report Excp Error [" + ex.Message + "]");
            }
            _logger.LogInformation("RepositoryBPInfo.Report OUT! Code = " + response.code.ToString());
            return response;
        }

        
        
    }


   
}
