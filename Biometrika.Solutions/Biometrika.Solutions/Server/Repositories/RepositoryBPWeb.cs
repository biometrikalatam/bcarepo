﻿using AutoMapper;
using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Server.Helpers;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.DTOs.BP.Metamap;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NuGet.DependencyResolver;

namespace Biometrika.Solutions.Server.Repositories
{
    public class RepositoryBPWeb : IRepositoryBPWeb
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<RepositoryCountry> _logger;
        private readonly IConfiguration _config;
        private readonly IRepositoryAuth _repositoryAuth;
        private readonly IRepositoryBP _repositoryBP;
        private readonly IMapper _mapper;

        public RepositoryBPWeb(ApplicationDbContext dbContext, 
                            ILogger<RepositoryCountry> logger,
                            IConfiguration config,
                            IRepositoryAuth repositoryAuth,
                            IMapper mapper,
                            IRepositoryBP repositoryBP)
        {
            _dbContext = dbContext;
            _logger = logger;
            _config = config;
            _repositoryAuth = repositoryAuth;
            _repositoryBP = repositoryBP;
            _mapper = mapper;
        }

        /// <summary>
        /// Crea la transaccion con los pa´rametros mínimos necesarios
        /// </summary>
        /// <param name="bpTxCreateDTO"></param>
        /// <param name="userid"></param>
        /// <returns>TrackId + URL de conexión por si queiren usarlo web</returns>
        public async Task<BSResponse> CreateBpTx(BpTxCreateDTO bpTxCreateDTO, string userid)
        {
            /*
                0.- Saco user/company desde userid que viene (Ver como manejar que user/company esten disabled)
                1.- Tomo Workflow desde BpWorkflow si existe sino responde BadRequest
                2.- LLeno de la TX todos los datos que puedo desde Workflow recuperado
                3.- Creo URL de BPWeb
                4.- Retorno TrackId + URL BPWeb + Fecha/Hora Vencimiento Tx
            */ 
            BSResponse response = new BSResponse(0, null, null);
            try
            {
                _logger.LogInformation("RepositoryBPWeb.CreateBpTx IN...");
                ApplicationUser user = await _repositoryAuth.RetrieveUser(userid);
                if (user == null)
                {
                    _logger.LogDebug("RepositoryBPWeb.CreateBpTx - Get User info => No Encontrado!");
                    response.code = StatusCodes.Status404NotFound;
                    response.message = "Usuario " + userid + " no encontrado!";
                    return response;
                }

                var wf = await _dbContext.BpCompanyWorkflow
                                    .Include(w => w.BpCompanyWorkflowItem)
                                    .Where(w => w.Name == bpTxCreateDTO.Workflowname && w.CompanyId == user.CompanyId)
                                    .FirstOrDefaultAsync();
                if (wf == null)
                {
                    _logger.LogDebug("RepositoryBPWeb.CreateBpTx - get Workflow [" + bpTxCreateDTO.Workflowname +
                                        "] info  => No Encontrado para ComapnyId = " + user.CompanyId.ToString() + "!");
                    response.code = StatusCodes.Status404NotFound;
                    response.message = "Workflow " + bpTxCreateDTO.Workflowname + " no encontrado para ComapnyId = "
                                            + user.CompanyId.ToString() + "!";
                    return response;
                }

                //Tengo toda la info => completo tx...
                _logger.LogDebug("RepositoryBPWeb.CreateBpTx - Inicio BpTx grabacion...");
                BpTx tx = new BpTx();
                tx = _mapper.Map<BpTx>(bpTxCreateDTO);
                if (string.IsNullOrEmpty(tx.Clientid))
                {
                    tx.Clientid = "ClientBPWebTx";
                }
                tx.Trackid = await GenerateUniqueTrackId();
                _logger.LogDebug("RepositoryBPWeb.CreateBpTx - Generado TrackId = " + tx.Trackid + "...");
                tx.Actiontype = Biometrika.Core._6.Contant.Action.ACTION_VERIFYANDGET; //Ver si vale la pena agregar mas Actions
                tx.Status = (int)TxStatus.Created;
                tx.UserId = user.Id;
                tx.CompanyId = user.CompanyId;
                tx.BpCompanyWorkflowId = wf.Id;
                tx.Callbackurl = wf.CallbackUrl;
                tx.Redirecturl = wf.RedirectUrl;
                tx.StatusType = wf.StatusType;
                tx.Metadata = bpTxCreateDTO.Metadata;
                tx.URLBpWeb = _config["BPConfig:URLBaseBPWeb"] + "trackid=" + tx.Trackid;
                tx.Expiration = wf.Expiration > 0 ? DateTime.Now.AddMinutes(wf.Expiration) : null;
                tx.Timestampstart = DateTime.Now;
                tx.Timestampend = DateTime.Now;
    
                _dbContext.Add(tx);
                int record = await _dbContext.SaveChangesAsync();

                if (record == 0)
                {
                    response.code = StatusCodes.Status304NotModified;
                    response.message = "Error salvando la transaccion en base de datos!";
                } else
                {
                    response.response = new
                    {
                        TrackId = tx.Trackid,
                        URLBpWeb = _config["BPConfig:URLBaseBPWeb"] + "trackid=" + tx.Trackid,
                        Expiration = tx.Expiration
                    };

                    //var txResponse = _mapper.Map<BpTxDTO>(tx);
                    //response.response = txResponse;
                }
            }
            catch (Exception ex)
            {
                response.code = -1;
                response.message = "RepositoryBPWeb.CreateBpTx Excp Error [" + ex.Message + "]";
                response.response = null;
                _logger.LogError(ex, "RepositoryBPWeb.CreateBpTx Excp Error [" + ex.Message + "]");
            }
            _logger.LogInformation("RepositoryBPWeb.CreateBpTx OUT! Code = " + response.code.ToString());
            return response;
        }

        /// <summary>
        /// Dado un trackid valido retonra todos los datos asociados a la transaccion
        /// </summary>
        /// <param name="trackid"></param>
        /// <returns></returns>
        public async Task<BSResponse> GetBpTxByTrackId(string trackid)
        {
            BSResponse response = new BSResponse(0, null, null);
            try
            {

                _logger.LogInformation("RepositoryBPWeb.GetBpTxByTrackId IN...");
                //.ThenInclude(wi => wi.BpCompanyWorkflowItem)
                var tx = _dbContext.BpTx
                            .Include(txc => txc.BpCompanyWorkflow)
                                .ThenInclude(wi => wi.BpCompanyWorkflowItem)
                                    .ThenInclude(mm => mm.BpCompanyMetamap)
                            .Include(txc => txc.BpCompanyWorkflow)
                                .ThenInclude(wi => wi.BpCompanyWorkflowItem)
                                    .ThenInclude(mm => mm.BpMerit)
                            .Include(txc => txc.BpCompanyWorkflow)
                                .ThenInclude(th => th.BpCompanyTheme)
                            .Include(txc => txc.BpTxConx)
                            .Include(txc => txc.BpTxData)
                            .Include(txc => txc.Company)
                            .Where(tx => tx.Trackid == trackid)
                            .AsSplitQuery()
                            .FirstOrDefault();

                if (tx != null)
                {
                    BpTxDTO txdto = _mapper.Map<BpTxDTO>(tx);
                    response.code = StatusCodes.Status302Found;
                    response.message = "Trackid " + trackid + " existe!";
                    response.response = txdto;
                }
                else
                {
                    response.code = StatusCodes.Status404NotFound;
                    response.message = "Trackid " + trackid + " no existe!";
                    response.response = null;
                    _logger.LogError("RepositoryBPWeb.GetBpTxByTrackId - " + response.message);
                }

            }
            catch (Exception ex)
            {
                response.code = -1;
                response.message = "RepositoryBPWeb.GetBpTxByTrackId Excp Error [" + ex.Message + "]";
                response.response = null;
                _logger.LogError(ex, "RepositoryBPWeb.GetBpTxByTrackId Excp Error [" + ex.Message + "]");
            }
            _logger.LogInformation("RepositoryBPWeb.GetBpTxByTrackId OUT!");
            return response;
        }

        /// <summary>
        /// Dada un trackid válido, devuelve los datos de la transacción para inicar la operacion a trabvpes de BPWeb,
        /// siempre que no esté expirada o tengo un estado superior a Initialized, y modifica el estado a Initialized.
        /// </summary>
        /// <param name="trackid"></param>
        /// <returns></returns>
        public async Task<BSResponse> InitializeBpTx(string trackid)
        {
            BSResponse response = new BSResponse(0, null, null);
            try
            {

                _logger.LogInformation("RepositoryBPWeb.InitializeBpTx IN - trackid = " + trackid + "...");
                //.ThenInclude(wi => wi.BpCompanyWorkflowItem)
                var tx = _dbContext.BpTx
                            .Include(txc => txc.BpCompanyWorkflow)
                                .ThenInclude(wi => wi.BpCompanyWorkflowItem)
                                    .ThenInclude(mm => mm.BpCompanyMetamap)
                            .Include(txc => txc.BpCompanyWorkflow)
                                .ThenInclude(wi => wi.BpCompanyWorkflowItem)
                                    .ThenInclude(mm => mm.BpMerit)
                            .Include(txc => txc.BpCompanyWorkflow)
                                .ThenInclude(th => th.BpCompanyTheme)
                            .Include(txc => txc.BpTxConx)
                            .Include(txc => txc.BpTxData)
                            .Include(txc => txc.Company)
                            .Where(tx => tx.Trackid == trackid)
                            .AsSplitQuery()
                            .FirstOrDefault();

                if (tx != null)
                {
                    if (tx.Expiration.HasValue && tx.Expiration.Value <= DateTime.Now)
                    {
                        response.code = StatusCodes.Status412PreconditionFailed;
                        response.message = "Trackid " + trackid + " expiro con fecha " +
                                            tx.Expiration.Value.ToString("dd/MM/yyyy HH:mm:ss")  + "!";
                        response.response = null;
                    } else
                    {
                        tx.Status = (int)TxStatus.Initialized;
                        tx.Timestampend = DateTime.Now;
                        _dbContext.Update(tx);
                        int recordUpdated = await _dbContext.SaveChangesAsync();
                        if (recordUpdated > 0)
                        {
                            BpTxDTO txdto = _mapper.Map<BpTxDTO>(tx);
                            response.code = StatusCodes.Status202Accepted;
                            response.message = "Trackid " + trackid + " ha sido inicializado!";
                            response.response = txdto;
                        }
                        else
                        {
                            response.code = StatusCodes.Status304NotModified;
                            response.message = "Trackid " + trackid + " no pudo ser inicializado!";
                            response.response = null;
                        }
                    }
                }
                else
                {
                    response.code = StatusCodes.Status404NotFound;
                    response.message = "Trackid " + trackid + " no existe!";
                    response.response = null;
                }
                _logger.LogDebug("RepositoryBPWeb.InitializeBpTx - code=" + response.code.ToString() + " - " + response.message);
            }
            catch (Exception ex)
            {
                response.code = -1;
                response.message = "RepositoryBPWeb.InitializeBpTx Excp Error [" + ex.Message + "]";
                response.response = null;
                _logger.LogError(ex, "RepositoryBPWeb.InitializeBpTx Excp Error [" + ex.Message + "]");
            }
            _logger.LogInformation("RepositoryBPWeb.InitializeBpTx OUT!");
            return response;
        }

        public async Task<BSResponse> ChangeStatusBpTx(BpTxChangeStatusDTO bpTxChangeStatusDTO)
        {
            BSResponse response = new BSResponse(0, null, null);
            try
            {
                _logger.LogInformation("RepositoryBPWeb. IN...");

            }
            catch (Exception ex)
            {
                response.code = -1;
                response.message = "RepositoryBPWeb. Excp Error [" + ex.Message + "]";
                response.response = null;
                _logger.LogError(ex, "RepositoryBPWeb. Excp Error [" + ex.Message + "]");
            }
            _logger.LogInformation("RepositoryBPWeb. OUT!");
            return response;
        }

        public async Task<BSResponse> GetDataBpTx(string trackid)
        {
            BSResponse response = new BSResponse(0, null, null);
            try
            {
                _logger.LogInformation("RepositoryBPWeb. IN...");

            }
            catch (Exception ex)
            {
                response.code = -1;
                response.message = "RepositoryBPWeb. Excp Error [" + ex.Message + "]";
                response.response = null;
                _logger.LogError(ex, "RepositoryBPWeb. Excp Error [" + ex.Message + "]");
            }
            _logger.LogInformation("RepositoryBPWeb. OUT!");
            return response;
        }

        public async Task<BSResponse> GetStatusBpTx(string trackid)
        {
            BSResponse response = new BSResponse(0, null, null);
            try
            {
                _logger.LogInformation("RepositoryBPWeb. IN...");

            }
            catch (Exception ex)
            {
                response.code = -1;
                response.message = "RepositoryBPWeb. Excp Error [" + ex.Message + "]";
                response.response = null;
                _logger.LogError(ex, "RepositoryBPWeb. Excp Error [" + ex.Message + "]");
            }
            _logger.LogInformation("RepositoryBPWeb. OUT!");
            return response;
        }

       

        public async Task<BSResponse> ListBpTx(FilterDTO filters)
        {
            BSResponse response = new BSResponse(0, null, null);
            try
            {
                _logger.LogInformation("RepositoryBPWeb. IN...");

            }
            catch (Exception ex)
            {
                response.code = -1;
                response.message = "RepositoryBPWeb. Excp Error [" + ex.Message + "]";
                response.response = null;
                _logger.LogError(ex, "RepositoryBPWeb. Excp Error [" + ex.Message + "]");
            }
            _logger.LogInformation("RepositoryBPWeb. OUT!");
            return response;
            /*
             Filtros fosibles:
                fromdate: 10/12/2022
                tilldate: 20/12/2022
                companyid: int
                estate: int
                trackid: string
                valueid: string
                name: string
                phaterlastname: string
             * 
             */
            //List<RdCitxGridDTO> txList = null;
            //DateTime dFrom = DateTime.MinValue;
            //DateTime dTill = DateTime.MinValue;
            //try
            //{
            //    _logger.LogDebug("RepositoryCI.CItxListToDTOGrid IN...");
            //    //txList = await _dbContext.RdCitx.ToListAsync();

            //    var query = _dbContext.RdCitx.AsQueryable();
            //    //.Select(tx =>
            //    //           new RdCitxGridDTO
            //    //           {
            //    //               Id = tx.Id,
            //    //               Trackid = tx.Trackid,
            //    //               Actionid = tx.Actionid,
            //    //               Valueid = tx.Valueid,
            //    //               Dategen = tx.Dategen,
            //    //               Score = tx.Score
            //    //           });


            //    if (filters == null || filters.filters == null || filters.filters.Count == 0)
            //    {
            //        _logger.LogDebug("RepositoryCI.CItxListToDTOGrid - Con filtro vacio...");
            //        txList = await query.Select(tx =>
            //                               new RdCitxGridDTO
            //                               {
            //                                   Id = tx.Id,
            //                                   Trackid = tx.Trackid,
            //                                   Actionid = tx.Actionid,
            //                                   Type = tx.Type,
            //                                   Typeid = tx.Typeid,
            //                                   Valueid = tx.Valueid,
            //                                   Dategen = tx.Dategen,
            //                                   NombreCompleto = tx.Name + " " + tx.Phaterlastname,
            //                                   Score = tx.Score
            //                               })
            //                         .Where(d => d.Dategen <= DateTime.Now)
            //                         .Where(d => d.Dategen > DateTime.Now.AddDays(-1 * Convert.ToInt32((string)_config["RangeGrid"])))
            //                         .OrderByDescending(tx => tx.Dategen)
            //                         .ToListAsync<RdCitxGridDTO>();
            //    }
            //    else
            //    {
            //        //Parseo fechadesde y fecha hasta sino pongo limite a mano desde hoy 30 dias para atras

            //        //Desde: Si viene lo parseo, sino pongo fecha de hoy menos el rango definido en config
            //        //tambien debo chequear que rango enviado no sea mayor al enviado sino acomoda a fecha hasta - RangeGrid
            //        try
            //        {
            //            if (filters.filters.ContainsKey("fromdate"))
            //            {
            //                dFrom = DateTime.ParseExact(filters.filters["fromdate"], "dd/MM/yyyy", null);
            //            }
            //            if (filters.filters.ContainsKey("tilldate"))
            //            {
            //                dTill = DateTime.ParseExact(filters.filters["tilldate"], "dd/MM/yyyy", null);
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            dFrom = DateTime.MinValue;
            //            dTill = DateTime.MinValue;
            //        }

            //        //TODO - revisar que dif de fechas no sea mayor a rangeGrid sino ajustar
            //        if ((dFrom == DateTime.MinValue && dTill == DateTime.MinValue) ||
            //            ((dTill - dFrom).Days > Convert.ToInt32((string)_config["RangeGrid"])))
            //        {
            //            if (dTill == DateTime.MinValue)
            //            {
            //                dTill = DateTime.Now;
            //            }
            //            dFrom = dTill.AddDays(-1 * Convert.ToInt32((string)_config["RangeGrid"]));
            //        }

            //        query = query.Where(d => d.Dategen.Value > dFrom);
            //        query = query.Where(d => d.Dategen.Value <= dTill);

            //        //Agrego los demas filtros si vienen 
            //        foreach (var item in filters.filters)
            //        {
            //            if (item.Key != null && item.Value != null)
            //            {
            //                _logger.LogDebug("RepositoryCI.CItxListToDTOGrid - Add filtro " + item.Key + " - " + item.Value);
            //                if (item.Key.Equals("estate"))
            //                {
            //                    query = query.Where(tx => tx.Actionid == Convert.ToInt32(item.Value));
            //                }
            //                if (item.Key.Equals("companyid"))
            //                {
            //                    query = query.Where(tx => tx.Companyid == Convert.ToInt32(item.Value));
            //                }
            //                if (item.Key.Equals("trackid"))
            //                {
            //                    query = query.Where(tx => tx.Trackid.Equals(item.Value));
            //                }
            //                if (item.Key.Equals("valueid"))
            //                {
            //                    query = query.Where(tx => tx.Valueid.Equals(item.Value));
            //                }
            //                if (item.Key.Equals("name"))
            //                {
            //                    query = query.Where(tx => tx.Name.Contains(item.Value));
            //                }
            //                if (item.Key.Equals("phaterlastname"))
            //                {
            //                    query = query.Where(tx => tx.Phaterlastname.Contains(item.Value));
            //                }
            //            }
            //        }
            //        txList = await query.Select(tx =>
            //                               new RdCitxGridDTO
            //                               {
            //                                   Id = tx.Id,
            //                                   Trackid = tx.Trackid,
            //                                   Actionid = tx.Actionid,
            //                                   Valueid = tx.Valueid,
            //                                   Dategen = tx.Dategen,
            //                                   Score = tx.Score
            //                               })
            //                          .OrderByDescending(tx => tx.Dategen)
            //                         .ToListAsync<RdCitxGridDTO>();
            //    }


            //    //txList = await _dbContext.RdCitx.Select(tx =>
            //    //                new RdCitxGridDTO
            //    //                {
            //    //                    Id = tx.Id,
            //    //                    Trackid = tx.Trackid,
            //    //                    Actionid = tx.Actionid,
            //    //                    Valueid = tx.Valueid,   
            //    //                    Dategen = tx.Dategen,
            //    //                    Score = tx.Score
            //    //                })
            //    //                .OrderByDescending(tx => tx.Dategen)
            //    //                .ToListAsync<RdCitxGridDTO>();
            //}
            //catch (Exception ex)
            //{
            //    //countries = null;
            //    string e = ex.Message;
            //    _logger.LogError("RepositoryCI.Get Excp => " + ex.Message);
            //}
            //_logger.LogDebug("RepositoryCI.Get OUT!");
            //return txList;
        }

        public async Task<BSResponse> ProcessBpTxWebHook(string jsonResponse)
        {
            BSResponse response = new BSResponse(0, null, null);
            try
            {
                _logger.LogInformation("RepositoryBPWeb. IN...");

            }
            catch (Exception ex)
            {
                response.code = -1;
                response.message = "RepositoryBPWeb. Excp Error [" + ex.Message + "]";
                response.response = null;
                _logger.LogError(ex, "RepositoryBPWeb. Excp Error [" + ex.Message + "]");
            }
            _logger.LogInformation("RepositoryBPWeb. OUT!");
            return response;
        }

        //public async Task<List<Country>> Get()
        //{
        //    List<Country> countries = null;
        //    try
        //    {
        //        _logger.LogDebug("RepositoryCI.Get IN...");
        //        countries = await _dbContext.Country.ToListAsync();
        //    }
        //    catch (Exception ex)
        //    {
        //        //countries = null;
        //        string e = ex.Message;
        //        _logger.LogError("RepositoryCI.Get Excp => " + ex.Message);
        //    }
        //    _logger.LogDebug("RepositoryCI.Get OUT!");
        //    return countries;
        //}

        /// <summary>
        /// Genera un TrackId unico para la talba de BpTx, corroborando que no exista en la tabla
        /// </summary>
        /// <returns></returns>
        private async Task<string> GenerateUniqueTrackId()
        {
            string ret = null;
            bool _continueChecking = true;
            try
            {

                _logger.LogInformation("RepositoryBPWeb.GenerateUniqueTrackId. IN...");
                while (_continueChecking)
                {
                    ret = Guid.NewGuid().ToString("N");
                    _continueChecking = await _dbContext.BpTx.AnyAsync(tx => tx.Trackid == ret);
                }
            }
            catch (Exception ex)
            {
                ret = null;
                _logger.LogError(ex, "RepositoryBPWeb.GenerateUniqueTrackId Excp Error [" + ex.Message + "]");
            }
            _logger.LogInformation("RepositoryBPWeb.GenerateUniqueTrackId OUT! => " + (string.IsNullOrEmpty(ret)?"Null":ret));
            return ret;
        }

        public async Task<BSResponse> ProcesaWebhookMsgReceived(MetamapResponseModel model)
        {
            BSResponse response = new BSResponse(0, null, null);
            try
            {
                if (model == null || model.metadata == null || string.IsNullOrEmpty(model.metadata.trackid))
                {
                    response.code = StatusCodes.Status400BadRequest;
                    response.message = "Trackid nulo";
                    response.response = null;
                    _logger.LogError("RepositoryBPWeb.ProcesaWebhookMsgReceived IN - trackid nulo! => Sale!");
                    return response;
                }

                _logger.LogInformation("RepositoryBPWeb.ProcesaWebhookMsgReceived IN - trackid = " + model.metadata.trackid + "...");
                //.ThenInclude(wi => wi.BpCompanyWorkflowItem)
                var tx = _dbContext.BpTx
                            .Include(txc => txc.BpCompanyWorkflow)
                                .ThenInclude(wi => wi.BpCompanyWorkflowItem)
                                    .ThenInclude(mm => mm.BpCompanyMetamap)
                            .Include(txc => txc.BpCompanyWorkflow)
                                .ThenInclude(wi => wi.BpCompanyWorkflowItem)
                                    .ThenInclude(mm => mm.BpMerit)
                            .Include(txc => txc.BpCompanyWorkflow)
                                .ThenInclude(th => th.BpCompanyTheme)
                            .Include(txc => txc.BpTxConx)
                            .Include(txc => txc.BpTxData)
                            .Include(txc => txc.BpTxStatusHistory)
                .Include(txc => txc.Company)
                            .Where(tx => tx.Trackid == model.metadata.trackid)
                            .AsSplitQuery()
                            .FirstOrDefault();

                if (tx != null)
                {
                    if (tx.Expiration.HasValue && tx.Expiration.Value <= DateTime.Now && tx.Status == (int)TxStatus.Created)
                    {
                        response.code = StatusCodes.Status412PreconditionFailed;
                        response.message = "Trackid " + model.metadata.trackid + " expiro con fecha " +
                                            tx.Expiration.Value.ToString("dd/MM/yyyy HH:mm:ss") + "!";
                        response.response = null;
                    }
                    else
                    {
                        tx = await ActualizaTxFromWebHook(tx, model); 
                        _dbContext.Update(tx);
                        int recordUpdated = await _dbContext.SaveChangesAsync();
                        if (recordUpdated > 0)
                        {
                            BpTxDTO txdto = _mapper.Map<BpTxDTO>(tx);
                            response.code = StatusCodes.Status202Accepted;
                            response.message = "Trackid " + model.metadata.trackid + " ha sido actualizada!";
                            response.response = txdto;
                        }
                        else
                        {
                            response.code = StatusCodes.Status304NotModified;
                            response.message = "Trackid " + model.metadata.trackid + " no pudo ser actualizada!";
                            response.response = null;
                        }
                    }
                }
                else
                {
                    response.code = StatusCodes.Status404NotFound;
                    response.message = "Trackid " + model.metadata.trackid + " no existe!";
                    response.response = null;
                }
                _logger.LogDebug("RepositoryBPWeb.ProcesaWebhookMsgReceived - code=" + response.code.ToString() + " - " + response.message);
            }
            catch (Exception ex)
            {
                response.code = -1;
                response.message = "RepositoryBPWeb.ProcesaWebhookMsgReceived Excp Error [" + ex.Message + "]";
                response.response = null;
                _logger.LogError(ex, "RepositoryBPWeb.ProcesaWebhookMsgReceived Excp Error [" + ex.Message + "]");
            }
            _logger.LogInformation("RepositoryBPWeb.ProcesaWebhookMsgReceived OUT!");
            return response;
        }


        /*
            webhook sent after each verification step is completed (liveness, face match, document-reading, alteration-detection, template-matching, watchlist)
        */
        private async Task<BpTx> ActualizaTxFromWebHook(BpTx tx, MetamapResponseModel model)
        {
            BpTx txret = tx;
            try
            {
                _logger.LogInformation("RepositoryBPWeb.ActualizaTxFromWebHook IN - trackid = " + model.metadata.trackid + "...");

                if (model.eventName == null) //Significa que es el JSON de recuperacion completa
                {
                    _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook IN Process - " + model.eventName +
                                        " - Time=" + txret.Timestampend.ToString("dd/MM/yyyy HH:mm:ss:ffff"));
                    txret = await MetamapHelper.ProcessVerificationCompleted(_dbContext, txret, model, _logger, _repositoryBP);
                    _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook End Process - " + model.eventName +
                                    " - Time=" + txret.Timestampend.ToString("dd/MM/yyyy HH:mm:ss:ffff"));
                    return txret;
                }
                else if (model.eventName.Equals("verification_completed") || model.eventName.Equals("verification_updated"))
                {
                    /*
                        1.- Guardo el paso en BpTxData
                        2.- Recupero el json de verify completa 
                        3.- Llamo a ProcessVerificationCompleted
                    */
                    _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook IN Process - " + model.eventName +
                                        " - Time=" + txret.Timestampend.ToString("dd/MM/yyyy HH:mm:ss:ffff"));
                    //1.- Guardo el paso en BpTxData
                    MetamapResponseModel modelVerifyCompleted = null;
                    txret = await MetamapHelper.ProcessStepVerificationCompletedOrUpdated(_dbContext, txret, model,
                                                                 _logger, _repositoryBP); //, out modelVerifyCompleted);
                    _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook - Recupero data del json total..."); 
                    modelVerifyCompleted = GetJsonResultTx(txret, _logger);

                    if (modelVerifyCompleted != null)
                    {
                        _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook - ProcessVerificationCompleted in process...");
                        txret = await MetamapHelper.ProcessVerificationCompleted(_dbContext, txret, modelVerifyCompleted, _logger, _repositoryBP);
                        _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook - ProcessVerificationCompleted finished!");
                    } else
                    {
                        _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook - Json Completo de Tx No encontrado o nulo!");
                    }
                    _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook End Process - " + model.eventName +
                                    " - Time=" + txret.Timestampend.ToString("dd/MM/yyyy HH:mm:ss:ffff"));
                    return txret;
                } else if (model.eventName.Equals("verification_started"))
                {
                    txret.Status = MetamapHelper.DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Started); 
                    txret.Timestampend = DateTime.Now;
                    _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook verification_started - Time=" +
                            txret.Timestampend.ToString("dd/MM/yyyy HH:mm:ss:ffff"));
                    return txret;
                } else if (model.eventName.Equals("step_completed"))
                {
                    _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook step_completed = " +
                                    (string.IsNullOrEmpty(model.step.id) ? "Id Nulo" : model.step.id) +
                                    " - Time=" + txret.Timestampend.ToString("dd/MM/yyyy HH:mm:ss:ffff"));
                    txret = await MetamapHelper.ProcessStep(_dbContext, txret, model, _logger, _repositoryBP);
                    return txret;
                } else if (model.eventName.Equals("verification_inputs_completed"))
                {
                    txret.Status = MetamapHelper.DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Processing);
                    txret.Timestampend = DateTime.Now;
                    _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook verification_inputs_completed - Time=" +
                            txret.Timestampend.ToString("dd/MM/yyyy HH:mm:ss:ffff"));
                    return txret;
                } else if (model.eventName.Equals("verification_expired"))
                {
                    txret.Status = MetamapHelper.DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Expired); 
                    txret.Timestampend = DateTime.Now;
                    _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook verification_expired - Time=" +
                            txret.Timestampend.ToString("dd/MM/yyyy HH:mm:ss:ffff"));
                    return txret;
                } else if (model.eventName.Equals("verification_postponed"))
                {
                    txret.Status = MetamapHelper.DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Postponed);
                    txret.Timestampend = DateTime.Now;
                    _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook verification_postponed - Time=" +
                            txret.Timestampend.ToString("dd/MM/yyyy HH:mm:ss:ffff"));
                    return txret;
                } else if (model.eventName.Equals("verification_signed"))
                {
                    txret.Timestampend = DateTime.Now;
                    _logger.LogDebug("RepositoryBPWeb.ActualizaTxFromWebHook verification_completed Signed - Time=" +
                                txret.Timestampend.ToString("dd/MM/yyyy HH:mm:ss:ffff"));
                    txret = MetamapHelper.AddOrUpdateBpTxData(model, txret, _logger);
                    return txret;
                } 
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "RepositoryBPWeb.ProcesaWebhookMsgReceived Excp Error [" + ex.Message + "]");
            }
            _logger.LogInformation("RepositoryBPWeb.ProcesaWebhookMsgReceived OUT!");
            return txret;
        }

        private MetamapResponseModel GetJsonResultTx(BpTx txret, ILogger _logger)
        {
            MetamapResponseModel modelVerifyCompleted = null;
            try
            {

                string jsonret = txret.BpTxData.Where(td => td.Id.Equals("result-completed-tx")).FirstOrDefault().DataRaw;

                if (!string.IsNullOrEmpty(jsonret))
                {
                    _logger.LogDebug("RepositoryBPWeb.GetJsonResultTx - Deserializando Json completo de Tx...");
                    modelVerifyCompleted = JsonConvert.DeserializeObject<MetamapResponseModel>(jsonret);
                    _logger.LogDebug("RepositoryBPWeb.GetJsonResultTx - Deserializado Json completo OK => " +
                                        (modelVerifyCompleted != null).ToString());
                }
                else
                {
                    _logger.LogDebug("RepositoryBPWeb.GetJsonResultTx - Json completo = null => No se deserializo");
                    //modelVerifyCompleted = null;
                }
            }
            catch (Exception ex)
            {
                _logger.LogDebug("RepositoryBPWeb.GetJsonResultTx - Excp [" + ex.Message + "]");
                modelVerifyCompleted = null;
            }
            return modelVerifyCompleted;
        }
    }


   
}
