﻿using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Server.Repositories
{
    public class RepositoryCI : IRepositoryCI
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<RepositoryCountry> _logger;
        private readonly IConfiguration _config;

        public RepositoryCI(ApplicationDbContext dbContext, 
                            ILogger<RepositoryCountry> logger,
                            IConfiguration config)
        {
            _dbContext = dbContext;
            _logger = logger;
            this._config = config;
        }

        

        public async Task<List<RdCitxGridDTO>> CItxListToDTOGrid(FilterDTO filters)
        {
            /*
             Filtros fosibles:
                fromdate: 10/12/2022
                tilldate: 20/12/2022
                companyid: int
                estate: int
                trackid: string
                valueid: string
                name: string
                phaterlastname: string
             * 
             */
            List<RdCitxGridDTO> txList = null;
            DateTime dFrom = DateTime.MinValue;
            DateTime dTill = DateTime.MinValue;
            try
            {
                _logger.LogDebug("RepositoryCI.CItxListToDTOGrid IN...");
                //txList = await _dbContext.RdCitx.ToListAsync();

                var query = _dbContext.RdCitx.AsQueryable();
                    //.Select(tx =>
                    //           new RdCitxGridDTO
                    //           {
                    //               Id = tx.Id,
                    //               Trackid = tx.Trackid,
                    //               Actionid = tx.Actionid,
                    //               Valueid = tx.Valueid,
                    //               Dategen = tx.Dategen,
                    //               Score = tx.Score
                    //           });


                if (filters == null || filters.filters == null || filters.filters.Count == 0)
                {
                    _logger.LogDebug("RepositoryCI.CItxListToDTOGrid - Con filtro vacio...");
                    txList = await query.Select(tx =>
                                           new RdCitxGridDTO
                                           {
                                               Id = tx.Id,
                                               Trackid = tx.Trackid,
                                               Actionid = tx.Actionid,
                                               Type = tx.Type,
                                               Typeid = tx.Typeid,
                                               Valueid = tx.Valueid,
                                               Dategen = tx.Dategen,
                                               NombreCompleto = tx.Name + " " + tx.Phaterlastname,
                                               Score = tx.Score
                                           })
                                     .Where(d => d.Dategen <= DateTime.Now)
                                     .Where(d => d.Dategen > DateTime.Now.AddDays(-1 * Convert.ToInt32((string)_config["RangeGrid"])))
                                     .OrderByDescending(tx => tx.Dategen)
                                     .ToListAsync<RdCitxGridDTO>();
                } else
                {
                    //Parseo fechadesde y fecha hasta sino pongo limite a mano desde hoy 30 dias para atras

                    //Desde: Si viene lo parseo, sino pongo fecha de hoy menos el rango definido en config
                    //tambien debo chequear que rango enviado no sea mayor al enviado sino acomoda a fecha hasta - RangeGrid
                    try
                    {
                        if (filters.filters.ContainsKey("fromdate"))
                        {
                            dFrom = DateTime.ParseExact(filters.filters["fromdate"], "dd/MM/yyyy", null);
                        }
                        if (filters.filters.ContainsKey("tilldate"))
                        {
                            dTill = DateTime.ParseExact(filters.filters["tilldate"], "dd/MM/yyyy", null);
                        }
                    }
                    catch (Exception ex)
                    {
                        dFrom = DateTime.MinValue;
                        dTill = DateTime.MinValue;
                    }

                    //TODO - revisar que dif de fechas no sea mayor a rangeGrid sino ajustar
                    if ((dFrom == DateTime.MinValue && dTill == DateTime.MinValue) || 
                        ((dTill - dFrom).Days > Convert.ToInt32((string)_config["RangeGrid"])))
                    {
                        if (dTill == DateTime.MinValue)
                        {
                            dTill = DateTime.Now;
                        }
                        dFrom = dTill.AddDays(-1 * Convert.ToInt32((string)_config["RangeGrid"]));
                    } 

                    query = query.Where(d => d.Dategen.Value > dFrom);
                    query = query.Where(d => d.Dategen.Value <= dTill);

                    //Agrego los demas filtros si vienen 
                    foreach (var item in filters.filters)
                    {
                        if (item.Key != null && item.Value != null)
                        {
                            _logger.LogDebug("RepositoryCI.CItxListToDTOGrid - Add filtro " + item.Key + " - " + item.Value);
                            if (item.Key.Equals("estate"))
                            {
                                query = query.Where(tx => tx.Actionid == Convert.ToInt32(item.Value));
                            }
                            if (item.Key.Equals("companyid"))
                            {
                                query = query.Where(tx => tx.Companyid == Convert.ToInt32(item.Value));
                            }
                            if (item.Key.Equals("trackid"))
                            {
                                query = query.Where(tx => tx.Trackid.Equals(item.Value));
                            }
                            if (item.Key.Equals("valueid"))
                            {
                                query = query.Where(tx => tx.Valueid.Equals(item.Value));
                            }
                            if (item.Key.Equals("name"))
                            {
                                query = query.Where(tx => tx.Name.Contains(item.Value));
                            }
                            if (item.Key.Equals("phaterlastname"))
                            {
                                query = query.Where(tx => tx.Phaterlastname.Contains(item.Value));
                            }
                        }
                    }
                    txList = await query.Select(tx =>
                                           new RdCitxGridDTO
                                           {
                                               Id = tx.Id,
                                               Trackid = tx.Trackid,
                                               Actionid = tx.Actionid,
                                               Valueid = tx.Valueid,
                                               Dategen = tx.Dategen,
                                               Score = tx.Score
                                           })
                                      .OrderByDescending(tx => tx.Dategen)
                                     .ToListAsync<RdCitxGridDTO>();
                }


                //txList = await _dbContext.RdCitx.Select(tx =>
                //                new RdCitxGridDTO
                //                {
                //                    Id = tx.Id,
                //                    Trackid = tx.Trackid,
                //                    Actionid = tx.Actionid,
                //                    Valueid = tx.Valueid,   
                //                    Dategen = tx.Dategen,
                //                    Score = tx.Score
                //                })
                //                .OrderByDescending(tx => tx.Dategen)
                //                .ToListAsync<RdCitxGridDTO>();
            }
            catch (Exception ex)
            {
                //countries = null;
                string e = ex.Message;
                _logger.LogError("RepositoryCI.Get Excp => " + ex.Message);
            }
            _logger.LogDebug("RepositoryCI.Get OUT!");
            return txList;
        }

        //public async Task<List<Country>> Get()
        //{
        //    List<Country> countries = null;
        //    try
        //    {
        //        _logger.LogDebug("RepositoryCI.Get IN...");
        //        countries = await _dbContext.Country.ToListAsync();
        //    }
        //    catch (Exception ex)
        //    {
        //        //countries = null;
        //        string e = ex.Message;
        //        _logger.LogError("RepositoryCI.Get Excp => " + ex.Message);
        //    }
        //    _logger.LogDebug("RepositoryCI.Get OUT!");
        //    return countries;
        //}
    }
}
