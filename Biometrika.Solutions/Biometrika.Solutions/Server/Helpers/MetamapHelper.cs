﻿using Biometrika.Metamap.Api;
using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Server.Repositories;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.DTOs.BP.Metamap;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Duende.IdentityServer.Extensions;
using Microsoft.DotNet.Scaffolding.Shared.CodeModifier.CodeChange;
using Microsoft.VisualStudio.Debugger.Contracts;
using Microsoft.Win32;
using MudBlazor.Extensions;
using Newtonsoft.Json;
using NuGet.DependencyResolver;
using RestSharp;
using Serilog.Filters;
using System.ComponentModel;
using static Duende.IdentityServer.Models.IdentityResources;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Model;
using static MudBlazor.CategoryTypes;

namespace Biometrika.Solutions.Server.Helpers
{
    public class MetamapHelper
    {
        internal static MetamapClient _METAMAP_CLIENT;


        internal static int Initialization()
        {
            int ret = 0;
            string msg;
            if (_METAMAP_CLIENT == null || !_METAMAP_CLIENT._IS_INITIALIZED)
            {
                _METAMAP_CLIENT = new MetamapClient();
                ret = _METAMAP_CLIENT.Initialize("https://api.getmati.com/", 30000,
                                             "62c34849e98ff3001c9dac34",
                                             "Q7YUGFRUN6W5XNMJ4T09EDH46YJZ2VPH", out msg);           
            }  

            //if (ret == 0)
            //{
            //    ret = _METAMAP_CLIENT.ValidateToken(out msg);
            //}
            return ret;
        }


        //public static ProcessStep()
        internal async static Task<BpTx> ProcessStep(ApplicationDbContext _dbContext, BpTx txin, MetamapResponseModel model,
                                         ILogger _logger, IRepositoryBP _repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (string.IsNullOrEmpty(model.step.id))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep NOT IN => model.step.id == null!");
                    return txret;
                }
                _logger.LogDebug("MetamapHelper.ProcessStep IN => " + model.step.id + "...");

                if (model.step.id.Equals("document-reading"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - document-reading IN...");
                    txret = await ProcessStepDocumentReading(_dbContext, txin, model, _logger, _repositoryBP);

                }
                else if (model.step.id.Equals("template-matching"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - document-reading IN...");
                    txret = txin; //await ProcessStepDocumentReading(_dbContext, txin, model, _logger, _repositoryBP);

                }
                else if (model.step.id.Equals("alteration-detection"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - alteration-detection IN...");
                    txret = await ProcessStepAlterationDetection(_dbContext, txin, model, _logger, _repositoryBP);

                }
                else if (model.step.id.Equals("ip-validation"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - ip-validation IN...");
                    txret = await ProcessStepIpValidation(_dbContext, txin, model, _logger, _repositoryBP);

                }
                else if (model.step.id.Equals("watchlists"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - watchlists IN...");
                    txret = await ProcessStepWatchlist(_dbContext, txin, model, _logger, _repositoryBP);

                }
                else if (model.step.id.Equals("premium-aml-watchlists-search-validation"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep -  premium-aml-watchlists-validation IN...");
                    txret = await ProcessStepPremiumWatchList(_dbContext, txin, model, _logger, _repositoryBP);

                }
                else if (model.step.id.Equals("age-check"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - age-check IN...");
                    txret = await ProcessStepAgeCheck(_dbContext, txin, model, _logger, _repositoryBP);

                }
                else if (model.step.id.Equals("liveness"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - liveness IN...");
                    txret = await ProcessStepLiveness(_dbContext, txin, model, _logger, _repositoryBP);

                }
                else if (model.step.id.Equals("voice"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - voice IN...");
                    txret = await ProcessStepLivenessVoice(_dbContext, txin, model, _logger, _repositoryBP);

                }
                else if (model.step.id.Equals("facematch"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - facematch IN...");
                    txret = await ProcessStepFacematch(_dbContext, txin, model, _logger, _repositoryBP);

                }
                else if (model.step.id.Equals("phone-ownership-validation"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - phone-ownership-validation IN...");
                    txret = await ProcessStepOwnerPhoneValidation(_dbContext, txin, model, _logger, _repositoryBP);

                }
                else if (model.step.id.Equals("phone-risk-analysis-validation"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - phone-risk-analysis-validation IN...");
                    txret = await ProcessStepRiskPhoneValidation(_dbContext, txin, model, _logger, _repositoryBP);
                }
                else if (model.step.id.Equals("email-ownership-validation"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - email-ownership-validation IN...");
                    txret = await ProcessStepOwnerMailValidation(_dbContext, txin, model, _logger, _repositoryBP);
                }
                else if (model.step.id.Equals("email-risk-validation"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - email-risk-validation IN...");
                    txret = await ProcessStepRiskMailValidation(_dbContext, txin, model, _logger, _repositoryBP);
                }
                else if (model.step.id.Equals("geolocation"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - geolocation IN...");
                    txret = await ProcessStepGeolocation(_dbContext, txin, model, _logger, _repositoryBP);
                }
                else if (model.step.id.Equals("verification_signed"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - verification_signed IN...");
                    txret = txin; // await ProcessStepGeolocation(_dbContext, txin, model, _logger, _repositoryBP);
                }
                else if (model.step.id.Equals("background-drucker-validation"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - background-drucker-validation IN...");
                    txret = txin; // await ProcessStepGeolocation(_dbContext, txin, model, _logger, _repositoryBP);
                }
                else if (model.step.id.Equals("chilean-registro-civil-validation"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - chilean-registro-civil-validation IN...");
                    txret = txin; // await ProcessStepGeolocation(_dbContext, txin, model, _logger, _repositoryBP);
                }
                else if (model.step.id.Equals("chilean-driver-license-validation"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - chilean-driver-license-validationn IN...");
                    txret = txin; // await ProcessStepGeolocation(_dbContext, txin, model, _logger, _repositoryBP);
                }
                else if (model.step.id.Equals("chilean-rut-validation"))
                {
                    _logger.LogDebug("MetamapHelper.ProcessStep - chilean-rut-validation IN...");
                    txret = txin; // await ProcessStepGeolocation(_dbContext, txin, model, _logger, _repositoryBP);
                }
                //else if (model.step.id.Equals("verification_completed"))
                //{
                //    _logger.LogDebug("MetamapHelper.ProcessStep - verification_completed IN...");
                //    txret = await ProcessVerificationCompleted(_dbContext, txin, model, _logger, _repositoryBP);
                //}

                //Agrego la data raw recibida
                txret = AddOrUpdateBpTxData(model, txret, _logger);
                
            }
            catch (Exception ex)
            {
                _logger.LogError("MetamapHelper.ProcessStep Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("MetamapHelper.ProcessStep OUT!");
            return txret;
        }

        /// <summary>
        /// Agrega el nuevo BpTxData del Step, pero verifica que si existe lo reemplaza, para evitar duplicar registros.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="txret"></param>
        /// <returns></returns>
        internal static BpTx AddOrUpdateBpTxData(MetamapResponseModel model, BpTx txin, ILogger _logger)
        {
            BpTx txret = txin;
            bool _existed = false;
            try
            {
                _logger.LogDebug("MetamapHelper.AddOrUpdateBpTxData IN...");

                if (txret.BpTxData == null) txret.BpTxData = new List<BpTxData>();

                foreach (BpTxData item in txret.BpTxData)
                {
                    if (item.Id.Equals(model.step.id))
                    {
                        _logger.LogDebug("MetamapHelper.AddOrUpdateBpTxData - Updating txid = " + item.BpTxId 
                                         + " - id = " + item.Id + "...");
                        if (model.step.data != null) item.DataRaw = model.step.data.ToString();
                        else if (model.step.error != null && !string.IsNullOrEmpty(model.step.error.message))
                        {
                            item.DataRaw = "Error => " + model.step.error.code + "-" + model.step.error.message;
                        }
                        else
                        {
                            item.DataRaw = "No Error - No Data";
                        }
                        _existed = true;
                        break;
                    }
                }

                if (!_existed)
                {
                    BpTxData bptxdatacurrent = new BpTxData();
                    bptxdatacurrent.Key = model.step.id;
                    if (model.step.data != null) bptxdatacurrent.DataRaw = model.step.data.ToString();
                    else if (model.step.error != null && !string.IsNullOrEmpty(model.step.error.message))
                    {
                        bptxdatacurrent.DataRaw = "Error => " + model.step.error.code + "-" + model.step.error.message;
                    }
                    else
                    {
                        bptxdatacurrent.DataRaw = "No Error - No Data";
                    }
                    //else
                    //{
                    //    bptxdatacurrent.DataRaw = null;
                    //}
                    bptxdatacurrent.Create = DateTime.Now;
                    bptxdatacurrent.BpTxId = txret.Id;
                    //if (txret.BpTxData == null) txret.BpTxData = new List<BpTxData>();
                    _logger.LogDebug("MetamapHelper.AddOrUpdateBpTxData - Adding txid = " + bptxdatacurrent.BpTxId
                                         + " - id = " + bptxdatacurrent.Id + "...");
                    txret.BpTxData.Add(bptxdatacurrent);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("MetamapHelper.AddOrUpdateBpTxData Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("MetamapHelper.AddOrUpdateBpTxData OUT!");
            return txret;
        }

        internal static BpTx AddOrUpdateBpTxData(string id, string dataraw, BpTx txin, ILogger _logger)
        {
            BpTx txret = txin;
            bool _existed = false;
            try
            {
                _logger.LogDebug("MetamapHelper.AddOrUpdateBpTxData IN...");

                if (txret.BpTxData == null) txret.BpTxData = new List<BpTxData>();

                foreach (BpTxData item in txret.BpTxData)
                {
                    if (item.Id.Equals(id))
                    {
                        _logger.LogDebug("MetamapHelper.AddOrUpdateBpTxData - Updating txid = " + item.BpTxId
                                         + " - id = " + item.Id + "...");
                        item.DataRaw = string.IsNullOrEmpty(dataraw) ? "No Error - No Data" : dataraw;
                        _existed = true;
                        break;
                    }
                }

                if (!_existed)
                {
                    BpTxData bptxdatacurrent = new BpTxData();
                    bptxdatacurrent.Key = id;
                    bptxdatacurrent.DataRaw = string.IsNullOrEmpty(dataraw) ? "No Error - No Data" : dataraw;
                    bptxdatacurrent.Create = DateTime.Now;
                    bptxdatacurrent.BpTxId = txret.Id;
                    _logger.LogDebug("MetamapHelper.AddOrUpdateBpTxData - Adding txid = " + bptxdatacurrent.BpTxId
                                         + " - id = " + bptxdatacurrent.Id + "...");
                    txret.BpTxData.Add(bptxdatacurrent);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("MetamapHelper.AddOrUpdateBpTxData Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("MetamapHelper.AddOrUpdateBpTxData OUT!");
            return txret;
        }
        private async static Task<BpTx> ProcessStepFacematch(ApplicationDbContext dbContext, BpTx txin, MetamapResponseModel model, 
                                                        ILogger _logger, IRepositoryBP repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepFacematch IN proceso...");
                    if (model.step.error != null)
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Rejected);
                        _logger.LogDebug("MetamapHelper.ProcessStepFacematch - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = "Facematch - " + model.step.error.code + " / " + model.step.error.message;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepFacematch -  Mail Owner = " + model.step.error.code
                                           + " / " + model.step.error.message);
                    }
                    else if (model.step.data != null)
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepFacematch - Deserializando data...");
                        DataFacematch dfm = JsonConvert.DeserializeObject<DataFacematch>(model.step.data.ToString());
                        if (dfm != null)
                        {
                            _logger.LogDebug("MetamapHelper.ProcessStepFacematch - Deserializacion OK!");
                            txret.Score = dfm.score;
                            _logger.LogDebug("MetamapHelper.ProcessStepFacematch - Score = " + txret.Score.ToString() + 
                                              " / Threshold = " + txret.Threshold);
                            if (txret.Score < txret.Threshold)
                            {
                                _logger.LogDebug("MetamapHelper.ProcessStepFacematch - Set status Rejected/Review por score < threshold...");
                                txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Rejected);
                            }
                        }
                        else
                        {
                            txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Reviewing);
                            _logger.LogWarning("MetamapHelper.ProcessStepFacematch - Deserializacion con objeto Nulo!");
                            BpTxStatusHistory txh = new BpTxStatusHistory();
                            txh.BpTxId = txret.Id;
                            txh.Update = DateTime.Now;
                            txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                            txh.Status = txret.Status;
                            txh.Reason = "Facematch - Error deserializando score!";
                            if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                            txret.BpTxStatusHistory.Add(txh);
                        }
                    }
                    else
                    {
                        _logger.LogDebug("RepositoryBPWeb.ProcessStepFacematch NO Procesado! model.step.data = null");
                    }
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepFacematch Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepFacematch OUT!");
            return txret;
        }

        internal async static Task<BpTx> ProcessStepVerificationCompletedOrUpdated(ApplicationDbContext dbContext, 
                                                            BpTx txin, MetamapResponseModel model, ILogger<RepositoryCountry> _logger, 
                                                            IRepositoryBP repositoryBP) //, out MetamapResponseModel modelVerifyCompleted)
        {
            BpTx txret = txin;
            //modelVerifyCompleted = null;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepVerificationCompletedOrUpdated IN proceso...");
                    if (model.step.error != null)
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Rejected);
                        _logger.LogDebug("MetamapHelper.ProcessStepVerificationCompletedOrUpdated - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = "VerificationCompleted - " + model.step.error.code + " / " + model.step.error.message;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepVerificationCompletedOrUpdated = " + model.step.error.code
                                           + " / " + model.step.error.message);
                    }
                    else if (!string.IsNullOrEmpty(model.resource))
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepVerificationCompletedOrUpdated - Recuperando Json completo de Tx de " +
                                            model.resource + "...");
                        string msgerr;
                        string retJsonTx = _METAMAP_CLIENT.GetResultTx(model.resource, out msgerr);

                        if (!string.IsNullOrEmpty(retJsonTx))
                        {
                            //Agrego la data raw recibida
                            txret = AddOrUpdateBpTxData("result-completed-tx", retJsonTx, txret, _logger);
                            //    _logger.LogDebug("MetamapHelper.ProcessStepVerificationCompletedOrUpdated - Deserializando Json completo de Tx...");
                            //    modelVerifyCompleted = JsonConvert.DeserializeObject<MetamapResponseModel>(retJsonTx);
                            //    _logger.LogDebug("MetamapHelper.ProcessStepFacematch - Deserializado Json completo OK => " + 
                            //                        (modelVerifyCompleted!=null).ToString());
                        }
                        else
                        {
                            _logger.LogDebug("MetamapHelper.ProcessStepVerificationCompletedOrUpdated - Json completo = null => No se agrego eb BpTxData!");
                            //modelVerifyCompleted = null;
                        }
                    }
                    else
                    {
                        _logger.LogDebug("RepositoryBPWeb.ProcessStepVerificationCompletedOrUpdated NO Procesado! resource = null");
                    }
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepFacematch Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepFacematch OUT!");
            return txret;
        }
        internal async static Task<BpTx> ProcessVerificationCompleted(ApplicationDbContext dbContext, BpTx txin,     
                                            MetamapResponseModel model, ILogger _logger, IRepositoryBP repositoryBP)
        {
            /*
                1.- Verificar que haya terminado bien y con eso poder sacar el status => Actualziar status
                2.- Eliminar los BpTxData existentes para cargar solo respuesta total
                3.- Recorrer los steps para recolectar la informacion alli entregada y procesando uno por uno para cargar lo neesario
                    y actualizar los datos necesarios
            */
            BpTx txret = txin;

            //imagenes para update en BpIdentity [Ver con que logica]
            string sBack = null;
            string sFront = null;
            string sSelfie = null;
            try
            {
                _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted IN proceso...");

                //Elimino los BpTxData si existen para regenerarlos
                //txret.BpTxData = new List<BpTxData>();

                //Proceso el/los document/s
                if (model.documents != null && model.documents.Count > 0)
                {
                    _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted - Procesando documents...");
                    foreach (var currentDocument in model.documents)
                    {
                        foreach (var step in currentDocument.steps)
                        {
                            model.step = step;
                            _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted - Procesando stem = " + model.step.id + "...");
                            txret = await ProcessStep(dbContext, txret, model, _logger, repositoryBP);
                        }
                        if (currentDocument.photos != null && currentDocument.photos.Count > 0)
                        {
                            sFront = await GetMedia(currentDocument.photos[0], "jpg", _logger);
                            if (!string.IsNullOrEmpty(sFront))
                            {
                                _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted - Including image document front...");

                                //Agrego la data raw recibida
                                txret = AddOrUpdateBpTxData("document-front", sFront, txret, _logger);

                                //BpTxData bpTxData = new BpTxData();
                                //bpTxData.Key = "document-front";
                                //bpTxData.DataRaw = sFront;
                                //bpTxData.Create = DateTime.Now;
                                //if (txret.BpTxData == null) txret.BpTxData = new List<BpTxData>();
                                //txret.BpTxData.Add(bpTxData);
                                _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted - Added document-front!");
                            }
                            if (currentDocument.photos.Count > 1)
                            {
                                sBack = await GetMedia(currentDocument.photos[1], "jpg", _logger);
                                if (!string.IsNullOrEmpty(sBack))
                                {
                                    _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted - Including image document back...");
                                    //Agrego la data raw recibida
                                    txret = AddOrUpdateBpTxData("document-back", sBack, txret, _logger);
                                    //BpTxData bpTxData = new BpTxData();
                                    //bpTxData.Key = "document-back";
                                    //bpTxData.DataRaw = sBack;
                                    //bpTxData.Create = DateTime.Now;
                                    //if (txret.BpTxData == null) txret.BpTxData = new List<BpTxData>();
                                    //txret.BpTxData.Add(bpTxData);
                                    _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted - Added document-back!");
                                }
                            }
                        }
                    }
                }

                //Proceso el resto de pasos
                if (model.steps != null && model.steps.Count > 0)
                {
                    _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted - Procesando steps...");
                    foreach (var step in model.steps)
                    {
                        model.step = step;
                        _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted - Procesando stem = " + model.step.id + "...");
                        txret = await ProcessStep(dbContext, txret, model, _logger, repositoryBP);
                    }
                }


                if (!string.IsNullOrEmpty(sFront) || !string.IsNullOrEmpty(sBack) || !string.IsNullOrEmpty(sSelfie))
                {
                    //TODO - Ver con que lógica updateamos en BpIdentity
                }

                if (txret.Checkadult == 1 && model.computed.age.data < 18)
                {
                    txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Rejected);
                    _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted - Seted txret.Status [Menor Edad] = " + txret.Status);
                } else if (txret.Checkexpired == 1 
                            && model.computed != null 
                            && model.computed.isDocumentExpired != null
                            && model.computed.isDocumentExpired.data != null
                            && model.computed.isDocumentExpired.data.nationalid)
                {
                    txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Reviewing);
                    _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted - Seted txret.Status [Doc Expired] = " + txret.Status);
                } else 
                {
                    if (model.identity != null)
                    {
                        if (model.identity.status.Equals("reviewNeeded"))
                        {
                            txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Reviewing);
                        }
                        else if (model.identity.status.Equals("rejected"))
                        {
                            txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Rejected);
                        }
                        else if (model.identity.status.Equals("verified"))
                        {
                            txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Verified);
                        }
                        _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted - Seted txret.Status = " + txret.Status);

                    }
                    else
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Reviewing);
                        _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted - Seted txret.Status [Identity null] = " + txret.Status);
                    }
                }
                
                BpTxStatusHistory txh = new BpTxStatusHistory();
                txh.BpTxId = txret.Id;
                txh.Update = DateTime.Now;
                txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                txh.Status = txret.Status;
                txh.Reason = "VerificationComplete - Existen Errores => " + model.hasProblem.ToString();
                if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                txret.BpTxStatusHistory.Add(txh);
                _logger.LogDebug("MetamapHelper.ProcessVerificationCompleted -  VerificationComplete - Existen Errores => "
                                    + model.hasProblem.ToString());
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessVerificationCompleted Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessVerificationCompleted OUT!");
            return txret;


        }

        private async static Task<BpTx> ProcessStepRiskMailValidation(ApplicationDbContext dbContext, BpTx txin, MetamapResponseModel model, 
                                                                ILogger _logger, IRepositoryBP repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepRiskMailValidation IN proceso...");
                    if (model.step.error != null)
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Reviewing);
                        _logger.LogDebug("MetamapHelper.ProcessStepRiskMailValidation - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = "Mail Risk - " + model.step.error.code + " / " + model.step.error.message;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepRiskMailValidation - Mail Risk = " + model.step.error.code
                                           + " / " + model.step.error.message);
                    }
                    else if (model.step.data != null)
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepRiskMailValidation - Deserializando data...");
                        DataPhoneRisk dpr = JsonConvert.DeserializeObject<DataPhoneRisk>(model.step.data.ToString());
                        if (dpr != null)
                        {
                            _logger.LogDebug("MetamapHelper.ProcessStepRiskMailValidation - Deserializacion OK!");
                            /*
                                TODO
                                Ver si cargo algo en algun lado
                            */
                        }
                        else
                        {
                            _logger.LogWarning("MetamapHelper.ProcessStepRiskMailValidation - Deserializacion con objeto Nulo!");
                        }
                    }
                    else
                    {
                        _logger.LogDebug("RepositoryBPWeb.ProcessStepRiskMailValidation NO Procesado! model.step.data = null");
                    }
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepRiskMailValidation Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepRiskMailValidation OUT!");
            return txret;
        }

        private async static Task<BpTx> ProcessStepOwnerMailValidation(ApplicationDbContext dbContext, BpTx txin, MetamapResponseModel model,
                                                                 ILogger _logger, IRepositoryBP repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepOwnerMailValidation IN proceso...");
                    if (model.step.error != null)
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Reviewing);
                        _logger.LogDebug("MetamapHelper.ProcessStepOwnerMailValidation - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = "Mail Owner - " + model.step.error.code + " / " + model.step.error.message;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepOwnerMailValidation -  Mail Owner = " + model.step.error.code
                                           + " / " + model.step.error.message);
                    }
                    else if (model.step.data != null)
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepOwnerMailValidation - Deserializando data...");
                        DataMailOwnerValidation domv = JsonConvert.DeserializeObject<DataMailOwnerValidation>(model.step.data.ToString());
                        if (domv != null)
                        {
                            _logger.LogDebug("MetamapHelper.ProcessStepOwnerMailValidation - Deserializacion OK!");
                            /*
                                TODO
                                Hay que rescatar el mail valido y actualizar BpIdentity con ese dato
                            */
                        }
                        else
                        {
                            _logger.LogWarning("MetamapHelper.ProcessStepOwnerMailValidation - Deserializacion con objeto Nulo!");
                        }
                    }
                    else
                    {
                        _logger.LogDebug("RepositoryBPWeb.ProcessStepOwnerMailValidation NO Procesado! model.step.data = null");
                    }
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepOwnerMailValidation Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepOwnerMailValidation OUT!");
            return txret;
        }

        private async static Task<BpTx> ProcessStepRiskPhoneValidation(ApplicationDbContext dbContext, BpTx txin, MetamapResponseModel model, 
                                                                 ILogger _logger, IRepositoryBP repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepRiskPhoneValidation IN proceso...");
                    if (model.step.error != null)
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Reviewing);
                        _logger.LogDebug("MetamapHelper.ProcessStepRiskPhoneValidation - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = "Phone Risk - " + model.step.error.code + " / " + model.step.error.message;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepOwnerPhoneValidation -  Phone Risk = " + model.step.error.code
                                           + " / " + model.step.error.message);
                    }
                    else if (model.step.data != null)
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepOwnerPhoneValidation - Deserializando data...");
                        DataPhoneRisk dpr = JsonConvert.DeserializeObject<DataPhoneRisk>(model.step.data.ToString());
                        if (dpr != null)
                        {
                            _logger.LogDebug("MetamapHelper.ProcessStepOwnerPhoneValidation - Deserializacion OK!");
                            /*
                                TODO
                                Ver si cargo algo en algun lado
                            */
                        }
                        else
                        {
                            _logger.LogWarning("MetamapHelper.ProcessStepOwnerPhoneValidation - Deserializacion con objeto Nulo!");
                        }
                    }
                    else
                    {
                        _logger.LogDebug("RepositoryBPWeb.ProcessStepOwnerPhoneValidation NO Procesado! model.step.data = null");
                    }
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepOwnerPhoneValidation Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepOwnerPhoneValidation OUT!");
            return txret;
        }

        private async static Task<BpTx> ProcessStepOwnerPhoneValidation(ApplicationDbContext dbContext, BpTx txin, MetamapResponseModel model, 
                                                                  ILogger _logger, IRepositoryBP repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepOwnerPhoneValidation IN proceso...");
                    if (model.step.error != null)
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Reviewing);
                        _logger.LogDebug("MetamapHelper.ProcessStepOwnerPhoneValidation - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = "Phone Owner - " + model.step.error.code + " / " + model.step.error.message;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepOwnerPhoneValidation -  Phone Owner = " + model.step.error.code
                                           + " / " + model.step.error.message);
                    }
                    else if (model.step.data != null)
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepOwnerPhoneValidation - Deserializando data...");
                        DataPhoneOwnerValidation dopv = JsonConvert.DeserializeObject<DataPhoneOwnerValidation>(model.step.data.ToString());
                        if (dopv != null)
                        {
                            _logger.LogDebug("MetamapHelper.ProcessStepOwnerPhoneValidation - Deserializacion OK!");
                            /*
                                TODO
                                Hay que rescatar el numero de telefono valido y actualizar BpIdentity con ese dato
                            */
                        }
                        else
                        {
                            _logger.LogWarning("MetamapHelper.ProcessStepOwnerPhoneValidation - Deserializacion con objeto Nulo!");
                        }
                    }
                    else
                    {
                        _logger.LogDebug("RepositoryBPWeb.ProcessStepOwnerPhoneValidation NO Procesado! model.step.data = null");
                    }
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepOwnerPhoneValidation Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepOwnerPhoneValidation OUT!");
            return txret;
        }

        private async static Task<BpTx> ProcessStepLivenessVoice(ApplicationDbContext dbContext, BpTx txin, MetamapResponseModel model, 
                                                           ILogger _logger, IRepositoryBP repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepLivenessVoice IN proceso...");
                    if (model.step.error != null)
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Reviewing);
                        _logger.LogDebug("MetamapHelper.ProcessStepLivenessVoice - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = "Voice - " + model.step.error.code + " / " + model.step.error.message;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepLivenessVoice -  Liveness = " + model.step.error.code
                                           + " / " + model.step.error.message);
                    }
                    else if (model.step.data != null)
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepLivenessVoice - Deserializando data...");
                        DataVoice dv = JsonConvert.DeserializeObject<DataVoice>(model.step.data.ToString());
                        if (dv != null)
                        {
                            _logger.LogDebug("MetamapHelper.ProcessStepLivenessVoice - Deserializacion OK!");
                            string sVideo = await GetMedia(dv.videoUrl, "mp4", _logger);
                            if (!string.IsNullOrEmpty(sVideo))
                            {
                                //Agrego la data raw recibida
                                txret = AddOrUpdateBpTxData("Voice-Video", sVideo, txret, _logger);
                                //BpTxData bpTxData = new BpTxData();
                                //bpTxData.Key = "Voice-Video";
                                //bpTxData.DataRaw = sVideo;
                                //bpTxData.Create = DateTime.Now;
                                //if (txret.BpTxData == null) txret.BpTxData = new List<BpTxData>();
                                //txret.BpTxData.Add(bpTxData);
                                _logger.LogDebug("MetamapHelper.ProcessStepLivenessVoice - Added Voice-Video!");
                            }
                            if (!string.IsNullOrEmpty(dv.text))
                            {
                                //Agrego la data raw recibida
                                txret = AddOrUpdateBpTxData("Voice-Text", sVideo, txret, _logger); 

                                //BpTxData bpTxData = new BpTxData();
                                //bpTxData.Key = "Voice-Text";
                                //bpTxData.DataRaw = dv.text;
                                //bpTxData.Create = DateTime.Now;
                                //if (txret.BpTxData == null) txret.BpTxData = new List<BpTxData>();
                                //txret.BpTxData.Add(bpTxData);
                                _logger.LogDebug("MetamapHelper.ProcessStepLivenessVoice - Added Voice-Text!");
                            }
                            string sSelfie = await GetMedia(dv.selfiePhotoUrl, "jpg", _logger);
                            if (!string.IsNullOrEmpty(sSelfie))
                            {
                                //Agrego la data raw recibida
                                txret = AddOrUpdateBpTxData("Voice-Selfie", sSelfie, txret, _logger);

                                //BpTxData bpTxData = new BpTxData();
                                //bpTxData.Key = "Voice-Selfie";
                                //bpTxData.DataRaw = sSelfie;
                                //bpTxData.Create = DateTime.Now;
                                //if (txret.BpTxData == null) txret.BpTxData = new List<BpTxData>();
                                //txret.BpTxData.Add(bpTxData);
                                _logger.LogDebug("MetamapHelper.ProcessStepLivenessVoice - Added Voice-Selfie!");
                            }
                            string sSecuence = await GetMedia(dv.spriteUrl, "jpg", _logger);
                            if (!string.IsNullOrEmpty(sSecuence))
                            {
                                //Agrego la data raw recibida
                                txret = AddOrUpdateBpTxData("Voice-Sprite", sSecuence, txret, _logger);
                                //BpTxData bpTxData = new BpTxData();
                                //bpTxData.Key = "Voice-Sprite";
                                //bpTxData.DataRaw = sSecuence;
                                //bpTxData.Create = DateTime.Now;
                                //if (txret.BpTxData == null) txret.BpTxData = new List<BpTxData>();
                                //txret.BpTxData.Add(bpTxData);
                                _logger.LogDebug("MetamapHelper.ProcessStepLivenessVoice - Added Voice-Sprite!");
                            }
                            /*
                                TODO
                                Hay que tomar los datos selfies y de videos y se deben almacenar de alguna forma. 
                                Puede ser en Blob o en BD. 
                                Además hay que generar los links para visualizarlos cuando se necesiten entregar o mostrar
                                en el manager.
                                QUIZA ampliar DynamicData de string a List<DynamicData> como en BpIdentity, para agregar
                                    todos los datos adicionales de selfies, videos, etc.
                            */

                        }
                        else
                        {
                            _logger.LogWarning("MetamapHelper.ProcessStepLivenessVoice - Deserializacion con objeto Nulo!");
                        }
                    }
                    else
                    {
                        _logger.LogDebug("RepositoryBPWeb.ProcessStepLivenessVoice NO Procesado! model.step.data = null");
                    }
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepLivenessVoice Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepLivenessVoice OUT!");
            return txret;
        }

        private async static Task<BpTx> ProcessStepLiveness(ApplicationDbContext dbContext, BpTx txin, MetamapResponseModel model, 
                                                      ILogger _logger, IRepositoryBP repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepLiveness IN proceso...");
                    if (model.step.error != null)
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Reviewing);
                        _logger.LogDebug("MetamapHelper.ProcessStepLiveness - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = "Liveness - " + model.step.error.code + " / " + model.step.error.message;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepLiveness -  Liveness = " + model.step.error.code
                                           + " / " + model.step.error.message);
                    }
                    else if (model.step.data != null)
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepLiveness - Deserializando data...");
                        DataLiveness dl = JsonConvert.DeserializeObject<DataLiveness>(model.step.data.ToString());
                        if (dl != null)
                        {
                            _logger.LogDebug("MetamapHelper.ProcessStepLiveness - Deserializacion OK! => Guardo imagenes y videos...");
                            string sVideo = await GetMedia(dl.videoUrl, "mp4", _logger);
                            if (!string.IsNullOrEmpty(sVideo))
                            {
                                //Agrego la data raw recibida
                                txret = AddOrUpdateBpTxData("Liveness-Video", sVideo, txret, _logger);
                                //BpTxData bpTxData = new BpTxData();
                                //bpTxData.Key = "Liveness-Video";
                                //bpTxData.DataRaw = sVideo;
                                //bpTxData.Create = DateTime.Now;
                                //if (txret.BpTxData == null) txret.BpTxData = new List<BpTxData>();
                                //txret.BpTxData.Add(bpTxData);
                                _logger.LogDebug("MetamapHelper.ProcessStepLiveness - Added Liveness-Video!");
                            }
                            string sSelfie = await GetMedia(dl.selfieUrl, "jpg", _logger);
                            if (!string.IsNullOrEmpty(sVideo))
                            {
                                //Agrego la data raw recibida
                                txret = AddOrUpdateBpTxData("Liveness-Selfie", sVideo, txret, _logger);
                                //BpTxData bpTxData = new BpTxData();
                                //bpTxData.Key = "Liveness-Selfie";
                                //bpTxData.DataRaw = sSelfie;
                                //bpTxData.Create = DateTime.Now;
                                //if (txret.BpTxData == null) txret.BpTxData = new List<BpTxData>();
                                //txret.BpTxData.Add(bpTxData);
                                _logger.LogDebug("MetamapHelper.ProcessStepLiveness - Added Liveness-Selfie!");
                            }
                            string sSecuence = await GetMedia(dl.spriteUrl, "jpg", _logger);
                            if (!string.IsNullOrEmpty(sSecuence))
                            {
                                //Agrego la data raw recibida
                                txret = AddOrUpdateBpTxData("Liveness-Sprite", sSecuence, txret, _logger);
                                //BpTxData bpTxData = new BpTxData();
                                //bpTxData.Key = "Liveness-Sprite";
                                //bpTxData.DataRaw = sSecuence;
                                //bpTxData.Create = DateTime.Now;
                                //if (txret.BpTxData == null) txret.BpTxData = new List<BpTxData>();
                                //txret.BpTxData.Add(bpTxData);
                                _logger.LogDebug("MetamapHelper.ProcessStepLiveness - Added Liveness-Sprite!");
                            }
                            /*
                                TODO
                                Hay que tomar los datos selfies y de videos y se deben almacenar de alguna forma. 
                                Puede ser en Blob o en BD. 
                                Además hay que generar los links para visualizarlos cuando se necesiten entregar o mostrar
                                en el manager.
                                QUIZA ampliar DynamicData de string a List<DynamicData> como en BpIdentity, para agregar
                                    todos los datos adicionales de selfies, videos, etc.
                            */

                        }
                        else
                        {
                            _logger.LogWarning("MetamapHelper.ProcessStepLiveness - Deserializacion con objeto Nulo!");
                        }
                    }
                    else
                    {
                        _logger.LogDebug("RepositoryBPWeb.ProcessStepLiveness NO Procesado! model.step.data = null");
                    }
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepLiveness Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepLiveness OUT!");
            return txret;
        }

       
        private async static Task<BpTx> ProcessStepAgeCheck(ApplicationDbContext dbContext, BpTx txin, MetamapResponseModel model, 
                                                      ILogger _logger, IRepositoryBP repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (txin.Checkadult == 1) //Si está habilitado para chequear sino ignora
                {
                    if (model.step.status == 200 && model.step.error == null)
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepAgeCheck IN proceso...");                      
                        DataAgeCheck dac = JsonConvert.DeserializeObject<DataAgeCheck>(model.step.data.ToString());
                        if (dac != null)
                        {
                            if (dac.underage)
                            {
                                txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Rejected);
                                _logger.LogDebug("MetamapHelper.ProcessStepAgeCheck - Seted txret.Status = " + txret.Status);
                                BpTxStatusHistory txh = new BpTxStatusHistory();
                                txh.BpTxId = txret.Id;
                                txh.Update = DateTime.Now;
                                txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                                txh.Status = txret.Status;
                                txh.Reason = "ProcessStepAgeCheck - Edad=" + dac.age + " / Umbral=" + dac.ageThreshold;
                                if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                                txret.BpTxStatusHistory.Add(txh);
                                _logger.LogDebug("MetamapHelper.ProcessStepAgeCheck - ProcessStepAgeCheck - Edad=" + dac.age 
                                                    + " / Umbral=" + dac.ageThreshold);
                            }
                            else
                            {
                                _logger.LogDebug("MetamapHelper.ProcessStepAgeCheck - Check Adulteration OK!");
                            }
                        }
                    } else
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Reviewing);
                        _logger.LogDebug("MetamapHelper.ProcessStepAgeCheck - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = "ProcessStepAgeCheck - Error chequeando Edad = " + model.step.error.code + 
                                                            " / " + model.step.error.message;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepAgeCheck - Error chequeando Edad = " + model.step.error.code + 
                                                            " / " + model.step.error.message);
                    }
                } else
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepAgeCheck - NO Check Adulteration x Config Tx! [txin.Checkadult=" 
                                     + txin.Checkadult + "]");
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepAgeCheck Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepAgeCheck OUT!");
            return txret;
        }

        private async static Task<BpTx> ProcessStepGeolocation(ApplicationDbContext dbContext, BpTx txin, MetamapResponseModel model, 
                                                         ILogger _logger, IRepositoryBP repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepGeolocation IN proceso...");
                    if (model.step.error != null)
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Reviewing);
                        _logger.LogDebug("MetamapHelper.ProcessStepGeolocation - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = "Geolocation - " + model.step.error.code + " / " + model.step.error.message;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepGeolocation -  Geolocation = " + model.step.error.code
                                           + " / " + model.step.error.message);
                    }
                    else if (model.step.data != null)
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepGeolocation - Deserializando data...");
                        DataGeolocationValitation dip = JsonConvert.DeserializeObject<DataGeolocationValitation>(model.step.data.ToString());
                        if (dip != null)
                        {
                            _logger.LogDebug("MetamapHelper.ProcessStepGeolocation - Deserializacion OK!");
                            txret.Geolocation = dip.latitude.ToString() + "," + dip.longitude.ToString();
                            _logger.LogDebug("MetamapHelper.ProcessStepGeolocation - txret.Geolocation = " + txret.Geolocation);
                            //txret.Dynamicdata = "ipRestrictionEnabled=" + dip.ipRestrictionEnabled.ToString();
                        }
                        else
                        {
                            _logger.LogWarning("MetamapHelper.ProcessStepGeolocation - Deserializacion con objeto Nulo!");
                        }
                    }
                    else
                    {
                        _logger.LogDebug("RepositoryBPWeb.ProcessStepGeolocation ip-validation NO Procesado! model.step.data = null");
                    }
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepGeolocation Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepGeolocation OUT!");
            return txret;
        }

        private async static Task<BpTx> ProcessStepPremiumWatchList(ApplicationDbContext dbContext, BpTx txin, 
                                                              MetamapResponseModel model, ILogger _logger, IRepositoryBP repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepPremiumWatchList IN proceso...");
                    if (model.step.error != null)
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Rejected);
                        _logger.LogDebug("MetamapHelper.ProcessStepPremiumWatchList - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = "ProcessStepPremiumWatchList - " + model.step.error.code + " / " + model.step.error.message;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepPremiumWatchList - Premium Watchlist = " + model.step.error.code
                                           + " / " + model.step.error.message);
                    }
                    else
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepPremiumWatchList - Check Premium Watchlist OK!");
                    }
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepPremiumWatchList Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepPremiumWatchList OUT!");
            return txret;
        }

        private async static Task<BpTx> ProcessStepWatchlist(ApplicationDbContext dbContext, BpTx txin, MetamapResponseModel model,
                                                       ILogger _logger, IRepositoryBP repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepWatchlist IN proceso...");
                    if (model.step.error != null)
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Rejected);
                        _logger.LogDebug("MetamapHelper.ProcessStepWatchlist - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = model.step.error.code + " / " + model.step.error.message + " / " + model.step.error.reasonCode;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepWatchlist - Watchlist = " + model.step.error.code
                                           + " / " + model.step.error.message + " / " + model.step.error.reasonCode);
                    }
                    else
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepWatchlist - Check Watchlist OK!");
                    }
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepWatchlist Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepWatchlist OUT!");
            return txret;
        }

        
        private async static Task<BpTx> ProcessStepAlterationDetection(ApplicationDbContext dbContext, BpTx txin, 
                                                                 MetamapResponseModel model, ILogger _logger, 
                                                                 IRepositoryBP repositoryBP)
        {
            /*
                easonCode	Dashboard Statement
                digitalPhotoReplacement	The photo on the document has been digitally altered
                fake	The image is synthetic. The information on it is unreal.
                textReplacement	The text on the document has been replaced with different information
                manualPhotoReplacement	The photo on the document has been altered
                differentFrontAndBack	The document front information or type does not match the back information or type
                underage	The person on the document is underage
                physicalObstruction	There is a physical obstruction that prevents to see the entire document
                digitalObstruction	There is a digital obstruction that prevents to see the entire document
                blurred	The image is blurred that hides information on the document
                pixelated	The image is pixelated that hides information on the document
                screenPhoto	The image is a picture of a screen where the document is stored
                grayscale	The image is a black and white copy of the document
                cropped	The document is cropped
                distorted	The image is distorted making the quality of the image poor
                sameImages	The same image (front or back) was uploaded twice
                colorCopy	The image is a color copy of the document
                screenshot	The image is a screenshot
                incorrectDocument	The document uploaded is incorrect
                noDocument	There's no document on the picture
            */
            BpTx txret = txin;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepAlterationDetection IN proceso...");
                    if (model.step.error != null)
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Rejected);
                        _logger.LogDebug("MetamapHelper.ProcessStepAlterationDetection - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = "AlterationDetection - "  + model.step.error.code + " / " 
                                        + model.step.error.message + " / " + model.step.error.reasonCode;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepAlterationDetection - Adulteration = " + model.step.error.code
                                           + " / " + model.step.error.message + " / " + model.step.error.reasonCode);
                    }
                    else
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepAlterationDetection - Check Adulteration OK!");
                    }
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepAlterationDetection Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepAlterationDetection OUT!");
            return txret;
        }

        private async static Task<BpTx> ProcessStepIpValidation(ApplicationDbContext dbContext, BpTx txin,
                                                          MetamapResponseModel model, ILogger _logger, IRepositoryBP repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepIpValidation IN proceso...");
                    if (model.step.error != null)
                    {
                        txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Reviewing);
                        _logger.LogDebug("MetamapHelper.ProcessStepIpValidation - Seted txret.Status = " + txret.Status);
                        BpTxStatusHistory txh = new BpTxStatusHistory();
                        txh.BpTxId = txret.Id;
                        txh.Update = DateTime.Now;
                        txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                        txh.Status = txret.Status;
                        txh.Reason = "Ip Validation - " + model.step.error.code + " / " + model.step.error.message;
                        if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                        txret.BpTxStatusHistory.Add(txh);
                        _logger.LogDebug("MetamapHelper.ProcessStepAlterationDetection - Ip Validation = " + model.step.error.code
                                           + " / " + model.step.error.message);
                    }
                    else if(model.step.data != null)
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepIpValidation - Deserializando data...");
                        DataIPValitation dip = JsonConvert.DeserializeObject<DataIPValitation>(model.step.data.ToString());
                        if (dip != null)
                        {
                            _logger.LogDebug("MetamapHelper.ProcessStepIpValidation - Deserializacion OK!");
                            _logger.LogDebug("MetamapHelper.ProcessStepIpValidation - IP Country = " + dip.countryCode + "/" + dip.country);
                            txret.Geolocation = dip.latitude.ToString() + "," + dip.longitude.ToString();
                            //txret.Dynamicdata = "ipRestrictionEnabled=" + dip.ipRestrictionEnabled.ToString();
                        }
                        else
                        {
                            _logger.LogWarning("MetamapHelper.ProcessStepIpValidation - Deserializacion con objeto Nulo!");
                        }
                    }
                    else
                    {
                        _logger.LogDebug("RepositoryBPWeb.ProcessStepIpValidation ip-validation NO Procesado! model.step.data = null");
                    }
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepIpValidation Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepIpValidation OUT!");
            return txret;
        }

        private async static Task<BpTx> ProcessStepDocumentReading(ApplicationDbContext _dbContext, BpTx txin,
                                                MetamapResponseModel model, ILogger _logger, IRepositoryBP _repositoryBP)
        {
            BpTx txret = txin;
            try
            {
                if (model.step.status == 200)
                {
                    _logger.LogDebug("MetamapHelper.ProcessStepDocumentReading IN proceso...");
                    if (model.step.data != null)
                    {
                        _logger.LogDebug("MetamapHelper.ProcessStepDocumentReading - Deserializando data...");
                        DataDocumetnReading ddr = JsonConvert.DeserializeObject<DataDocumetnReading>(model.step.data.ToString());
                        if (ddr != null)
                        {
                            string sValueId = null;
                            if (ddr.documentType.value.Equals("IE"))
                            {
                                sValueId = (ddr.runNumber != null && !string.IsNullOrEmpty(ddr.runNumber.value)) ?
                                        ddr.runNumber.value.Replace(".", "") : ddr.optional2.value.Replace(" ", "-");
                            }
                            else
                            {
                                //TODO - Ver si es otra cedula o pasaporte si debemo snormalizar el dato!!!
                                sValueId = ddr.documentNumber.value;
                            }
                            _logger.LogDebug("MetamapHelper.ProcessStepDocumentReading => Chequeo integridad...");
                            _logger.LogDebug("MetamapHelper.ProcessStepDocumentReading - txret.Valueid = " + txret.Valueid +
                                                " vs ddr.optional2.value = " + ddr.optional2.value + "...");
                            if (!txret.Valueid.Equals(sValueId) && !txret.Valueid.Equals("NA"))
                            {
                                _logger.LogDebug("MetamapHelper.ProcessStepDocumentReading - Sale con Rejected x inconsistencia!");
                                txret.Status = DefineStatus(txret.StatusType, txret.Status, (int)TxStatus.Rejected);
                                _logger.LogDebug("MetamapHelper.ProcessStepDocumentReading - Seted txret.Status = " + txret.Status);
                                txret.Timestampend = DateTime.Now;
                                BpTxStatusHistory txh = new BpTxStatusHistory();
                                txh.BpTxId = txret.Id;
                                txh.Update = DateTime.Now;
                                txh.UserId = txret.UserId; //TODO Debo conseguir el user. Si es por API debe ser user generico de api
                                txh.Status = txret.Status;
                                txh.Reason = "Inconsistencia entre numero de documento pedido [" + txret.Valueid + "] y reconocido [" + sValueId + "]";
                                if (txret.BpTxStatusHistory == null) txret.BpTxStatusHistory = new List<BpTxStatusHistory>();
                                txret.BpTxStatusHistory.Add(txh);
                                _logger.LogDebug("MetamapHelper.ProcessStepAlterationDetection - document-reading = " + model.step.error.code
                                                   + " / " + model.step.error.message);
                                //Crear BpTxStatusHistory y agregar el motivo valueid != al reconocido
                            }
                            else
                            {
                                //1-Enroll o Update datos de Identity, si fecha es mayor a XXX del config
                                _logger.LogDebug("MetamapHelper.ProcessStepDocumentReading - Creanto IdentityDTO..:");
                                BpIdentityDTO idenDTO = new BpIdentityDTO();
                                idenDTO.Birthdate = DateTime.Parse(ddr.dateOfBirth.value);
                                idenDTO.Documentexpirationdate = DateTime.Parse(ddr.expirationDate.value);
                                idenDTO.Documentseriesnumber = ddr.documentNumber.value;
                                idenDTO.Name = ddr.firstName.value;
                                idenDTO.Patherlastname = ddr.surname.value;
                                idenDTO.Nationality = ddr.nationality.value;
                                idenDTO.Verificationsource = "CI";
                                idenDTO.Typeid = txret.Typeid;
                                idenDTO.Valueid = txret.Valueid;
                                idenDTO.Sex = ddr.sex.value;

                                //public IssueCountryDR issueCountry { get; set; }

                                _logger.LogDebug("MetamapHelper.ProcessStepDocumentReading - EnrollOrUpdate ...");
                                BSResponse bsr = await _repositoryBP.EnrollOrUpdate(txin.CompanyId, idenDTO);
                                _logger.LogDebug("MetamapHelper.ProcessStepDocumentReading - EnrollOrUpdate ret = " +
                                                    (bsr != null ? bsr.code.ToString() : "Null"));
                                //2-Ver si debo actaulizar algo de la tabla Tx con algun dato que viene aqui
                            }
                        }
                        else
                        {
                            _logger.LogWarning("MetamapHelper.ProcessStepDocumentReading - Deserializacion con objeto Nulo!");
                        }

                    }
                    else
                    {
                        _logger.LogDebug("RepositoryBPWeb.ProcessStepDocumentReading document-reading NO Procesado! model.step.data = null");
                    }
                }
                else
                {
                    _logger.LogDebug("RepositoryBPWeb.ProcessStepDocumentReading document-reading NO Procesado! status = " + model.step.status.ToString());
                }
            }
            catch (Exception ex)
            {
                txret = txin;
                _logger.LogError("MetamapHelper.ProcessStepDocumentReading Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("RepositoryBPWeb.ProcessStepDocumetnReading OUT!");
            return txret;
        }

        internal static int DefineStatus(int statustype, int currentstatus, int newstatus)
        {

            //Si es un estado menor a Reviewing y llega una rechazado o verificado, pero el statustype = 1 (Revision Obligatoria)
            // Devuelve el Reviewing 
            if (currentstatus < (int)TxStatus.Reviewing && 
                ((newstatus == (int)TxStatus.Rejected || newstatus == (int)TxStatus.Verified) && statustype == 1)) 
            {
                return (int)TxStatus.Reviewing;
            } 

            //Sino analiza cada estado por separado.
            switch (currentstatus)
            {
                case (int)TxStatus.Created: //Devuelvo mismo estado enviado porque se actualiza desde el inicial
                    return newstatus;
                    break;
                case (int)TxStatus.Initialized:
                    if (newstatus < (int)TxStatus.Initialized) return (int)TxStatus.Initialized;
                    else return newstatus;
                    break;
                case (int)TxStatus.Started:
                    if (newstatus < (int)TxStatus.Started) return (int)TxStatus.Started;
                    else return newstatus;
                    break;
                case (int)TxStatus.Processing:
                    if (newstatus < (int)TxStatus.Processing) return (int)TxStatus.Processing;
                    else return newstatus;
                    break;
                case (int)TxStatus.Postponed:
                    if (newstatus < (int)TxStatus.Postponed) return (int)TxStatus.Postponed;
                    else return newstatus;
                    break;
                case (int)TxStatus.Cancelled:
                    return newstatus;
                    break;
                case (int)TxStatus.Expired:
                    return newstatus;
                    break;
                case (int)TxStatus.Reviewing:
                    if (newstatus <= (int)TxStatus.Reviewing) return (int)TxStatus.Reviewing;
                    else return newstatus;
                    break;
                case (int)TxStatus.Rejected:
                    //Si esta en rejected y llega status menor a review sigue en rejected
                    if (newstatus < (int)TxStatus.Reviewing) return (int)TxStatus.Rejected;
                    //Si esta en rejected y llega review o verified se cambia a esos estados
                    if (newstatus == (int)TxStatus.Reviewing || newstatus == (int)TxStatus.Verified) return newstatus;
                    return newstatus;
                    break;
                case (int)TxStatus.Verified:
                    //Si esta en Verified y llega status menor a review sigue en verified
                    if (newstatus < (int)TxStatus.Reviewing) return (int)TxStatus.Verified;
                    //Si esta en Verified y llega review o rejected se cambia a esos estados
                    if (newstatus == (int)TxStatus.Reviewing || newstatus == (int)TxStatus.Rejected) return newstatus;
                    return newstatus;
                    break;
                default:
                    return newstatus;
                    break;
            }
        }

        private async static Task<string> GetMedia(string url, string type, ILogger _logger)
        {
            string ret = null;
            string msg;
            _logger.LogDebug("MetamapHelper.GetMedia Excp IN...");
            try
            {
                //Ver si se necesita el type
                //TODO
                //   1) Recuperar desde metamap el dato pedido (url)
                //   2) Devolver en Base64

                //Crea o refresca token segun sea el caso
                if (Initialization() == 0) {
                    //var request = new RestRequest(url, RestSharp.Method.Get);
                    //request.AddHeader("Authorization", "Bearer " + _METAMAP_CLIENT.GetToken(out msg));
                    //var client = new RestClient(new RestClientOptions { Timeout = _METAMAP_CLIENT._URL_TIMEOUT });
                    //var response = client.Execute(request);

                    //byte[] video = response.RawBytes;

                    ret = _METAMAP_CLIENT.GetMedia(url, type, out msg);


                } else
                {
                    _logger.LogError("MetamapHelper.GetMedia Error inicializando MetamapHelper.Client [" + ret.ToString() + "]");
                    return null;
                }

            }
            catch (Exception ex)
            {
                ret = null;
                _logger.LogError("MetamapHelper.GetMedia Excp [" + ex.Message + "]");
            }
            _logger.LogDebug("MetamapHelper.GetMedia Excp OUT! ret!=null => " + (ret!=null).ToString());
            return ret;
        }

       
    }
}
