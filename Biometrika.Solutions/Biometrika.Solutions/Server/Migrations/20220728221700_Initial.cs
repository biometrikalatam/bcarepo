﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Biometrika.Solutions.Server.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "bp_merit",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    description = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: true),
                    uniquemetamapguid = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_merit", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "bp_origin",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    description = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_origin", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "company",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    rut = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: false),
                    address = table.Column<string>(type: "varchar(80)", unicode: false, maxLength: 80, nullable: false),
                    name = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: false),
                    phone = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: false),
                    phone2 = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true),
                    fax = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true),
                    createDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    endate = table.Column<DateTime>(type: "datetime", nullable: true),
                    updateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    domain = table.Column<string>(type: "varchar(15)", unicode: false, maxLength: 15, nullable: false),
                    additionaldata = table.Column<string>(type: "text", nullable: true),
                    contactname = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    status = table.Column<int>(type: "int", nullable: false),
                    holding = table.Column<int>(type: "int", nullable: true),
                    accessname = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    secretkey = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_company", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "DeviceCodes",
                columns: table => new
                {
                    UserCode = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    DeviceCode = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    SubjectId = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    SessionId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ClientId = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Expiration = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Data = table.Column<string>(type: "nvarchar(max)", maxLength: 50000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceCodes", x => x.UserCode);
                });

            migrationBuilder.CreateTable(
                name: "Keys",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Version = table.Column<int>(type: "int", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Use = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Algorithm = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    IsX509Certificate = table.Column<bool>(type: "bit", nullable: false),
                    DataProtected = table.Column<bool>(type: "bit", nullable: false),
                    Data = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Keys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PersistedGrants",
                columns: table => new
                {
                    Key = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Type = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    SubjectId = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    SessionId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ClientId = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Expiration = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ConsumedTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Data = table.Column<string>(type: "nvarchar(max)", maxLength: 50000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersistedGrants", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "company",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "bp_company_metamap",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    companyid = table.Column<int>(type: "int", nullable: false),
                    name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    description = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: true),
                    uniquemetamapguid = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_company_metamap", x => x.id);
                    table.ForeignKey(
                        name: "FK_bp_company_metamap_company_companyid",
                        column: x => x.companyid,
                        principalTable: "company",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "bp_company_theme",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    companyid = table.Column<int>(type: "int", nullable: false),
                    name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    jsonconfig = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_company_theme", x => x.id);
                    table.ForeignKey(
                        name: "FK_bp_company_theme_company_companyid",
                        column: x => x.companyid,
                        principalTable: "company",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "bp_company_workflow",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    companyid = table.Column<int>(type: "int", nullable: false),
                    name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    theme = table.Column<string>(type: "int", nullable: false, defaultValueSql: "((1))"),
                    expiration = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((0))"),
                    statustype = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((0))"),
                    usetheme = table.Column<int>(type: "bit", nullable: false, defaultValueSql: "((1))"),
                    usefirstpage = table.Column<int>(type: "bit", nullable: false, defaultValueSql: "((1))"),
                    callbackurl = table.Column<string>(type: "varchar(1024)", unicode: false, maxLength: 1024, nullable: true),
                    redirecturl = table.Column<string>(type: "varchar(1024)", unicode: false, maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_company_workflow", x => x.id);
                    table.ForeignKey(
                        name: "FK_bp_company_workflow_company_companyid",
                        column: x => x.companyid,
                        principalTable: "company",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "rd_CITx",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    trackid = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    type = table.Column<int>(type: "int", nullable: false),
                    actionid = table.Column<int>(type: "int", nullable: false),
                    receiptid = table.Column<int>(type: "int", nullable: true),
                    trackidbp = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    trackidde = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    dategen = table.Column<DateTime>(type: "datetime", nullable: true),
                    validitytype = table.Column<int>(type: "int", nullable: false),
                    validitydays = table.Column<int>(type: "int", nullable: false),
                    resultcode = table.Column<int>(type: "int", nullable: false),
                    certifypdf = table.Column<string>(type: "text", nullable: true),
                    qrgenerated = table.Column<string>(type: "text", nullable: true),
                    qrvalidity = table.Column<int>(type: "int", nullable: true),
                    lastmodify = table.Column<DateTime>(type: "datetime", nullable: true),
                    destinatarymail = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    typeid = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: true),
                    valueid = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    name = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
                    phaterlastname = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
                    motherlastname = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
                    idcardimagefront = table.Column<string>(type: "text", nullable: true),
                    idcardimageback = table.Column<string>(type: "text", nullable: true),
                    idcardphotoimage = table.Column<string>(type: "text", nullable: true),
                    idcardsignatureimage = table.Column<string>(type: "text", nullable: true),
                    selfie = table.Column<string>(type: "text", nullable: true),
                    fingersample = table.Column<string>(type: "text", nullable: true),
                    fingersamplejpg = table.Column<string>(type: "text", nullable: true),
                    pdf417 = table.Column<string>(type: "text", nullable: true),
                    workstationid = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    georef = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    sex = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: true),
                    birthdate = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true),
                    issuedate = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true),
                    exprationdate = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true),
                    serial = table.Column<string>(type: "varchar(25)", unicode: false, maxLength: 25, nullable: true),
                    nacionality = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true),
                    score = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    threshold = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    map = table.Column<string>(type: "text", nullable: true),
                    urlbpweb = table.Column<string>(type: "varchar(1024)", unicode: false, maxLength: 1024, nullable: true),
                    listmailsdistribution = table.Column<string>(type: "varchar(1024)", unicode: false, maxLength: 1024, nullable: true),
                    cellphone = table.Column<string>(type: "varchar(25)", unicode: false, maxLength: 25, nullable: true),
                    manualsignatureimage = table.Column<string>(type: "text", nullable: true),
                    carregisterimagefront = table.Column<string>(type: "text", nullable: true),
                    carregisterimageback = table.Column<string>(type: "text", nullable: true),
                    writingimage = table.Column<string>(type: "text", nullable: true),
                    form = table.Column<string>(type: "text", nullable: true),
                    securitycode = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
                    taxidcompany = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    dinamicparam = table.Column<string>(type: "text", nullable: true),
                    videourl = table.Column<string>(type: "varchar(512)", unicode: false, maxLength: 512, nullable: true),
                    username = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    companyid = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_rd_CITx", x => x.id);
                    table.ForeignKey(
                        name: "FK_rd_CITx_company_companyid",
                        column: x => x.companyid,
                        principalTable: "company",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "bp_client",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    companyid = table.Column<int>(type: "int", nullable: false),
                    clientid = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    createddate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    lastmodify = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    status = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((0))"),
                    userlastmodify = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_client", x => x.id);
                    table.ForeignKey(
                        name: "FK_bp_client_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_bp_client_company_companyid",
                        column: x => x.companyid,
                        principalTable: "company",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "bp_identity",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nick = table.Column<string>(type: "varchar(80)", unicode: false, maxLength: 80, nullable: true),
                    typeid = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: false),
                    valueid = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    patherlastname = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    motherlastname = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    sex = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: true),
                    documentseriesnumber = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: true),
                    documentexpirationdate = table.Column<DateTime>(type: "datetime", nullable: true),
                    visatype = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    birthdate = table.Column<DateTime>(type: "datetime", nullable: true),
                    birthplace = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    nationality = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    photography = table.Column<string>(type: "text", nullable: true),
                    signatureimage = table.Column<string>(type: "text", nullable: true),
                    profession = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    dynamicdata = table.Column<string>(type: "text", nullable: true),
                    enrollinfo = table.Column<string>(type: "text", nullable: true),
                    creation = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    verificationsource = table.Column<string>(type: "text", nullable: true),
                    companyid = table.Column<int>(type: "int", nullable: false),
                    userid = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    docimagefront = table.Column<string>(type: "text", nullable: true),
                    docimageback = table.Column<string>(type: "text", nullable: true),
                    selfie = table.Column<string>(type: "text", nullable: true),
                    type3roservice = table.Column<string>(type: "varchar(25)", unicode: false, maxLength: 25, nullable: true),
                    id3roservice = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_identity", x => x.id);
                    table.ForeignKey(
                        name: "FK_bp_identity_AspNetUsers_userid",
                        column: x => x.userid,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_bp_identity_company_companyid",
                        column: x => x.companyid,
                        principalTable: "company",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "bp_company_workflow_item",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    bpcompanyworkflowid = table.Column<int>(type: "int", nullable: false),
                    name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    jsonconfig = table.Column<string>(type: "text", nullable: false),
                    bpcompanymetamapaid = table.Column<int>(type: "int", nullable: true),
                    bpmeritid = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_company_workflow_item", x => x.id);
                    //table.ForeignKey(
                    //    name: "FK_bp_company_workflow_item_bp_company_metamap_bpcompanymetamapaid",
                    //    column: x => x.bpcompanymetamapaid,
                    //    principalTable: "bp_company_metamap",
                    //    principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_bp_company_workflow_item_bp_company_workflow_bpcompanyworkflowid",
                        column: x => x.bpcompanyworkflowid,
                        principalTable: "bp_company_workflow",
                        principalColumn: "id");
                    //table.ForeignKey(
                    //    name: "FK_bp_company_workflow_item_bp_merit_bpmeritid",
                    //    column: x => x.bpmeritid,
                    //    principalTable: "bp_merit",
                    //    principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "bp_tx",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    trackid = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: false),
                    statustype = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((0))"),
                    status = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((0))"),
                    expiration = table.Column<DateTime>(type: "datetime", nullable: true),
                    operationcode = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((0))"),
                    typeid = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: false),
                    valueid = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    taxidcompany = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    result = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((0))"),
                    score = table.Column<double>(type: "float", nullable: false, defaultValueSql: "((0))"),
                    threshold = table.Column<double>(type: "float", nullable: false, defaultValueSql: "((50))"),
                    timestampstart = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    timestampend = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    authenticationfactor = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((4))"),
                    minutiaetype = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((41))"),
                    bodypart = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((16))"),
                    actiontype = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((1))"),
                    bporiginid = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((1))"),
                    timestampclient = table.Column<DateTime>(type: "datetime", nullable: true),
                    clientid = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    ipenduser = table.Column<string>(type: "varchar(15)", unicode: false, maxLength: 15, nullable: true),
                    enduser = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    dynamicdata = table.Column<string>(type: "text", nullable: true),
                    companyid = table.Column<int>(type: "int", nullable: false),
                    userid = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    operationsource = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((0))"),
                    abs = table.Column<string>(type: "text", nullable: true),
                    consumed = table.Column<DateTime>(type: "datetime", nullable: true),
                    checklock = table.Column<int>(type: "int", nullable: true),
                    estadolock = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: true),
                    razonlock = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: true),
                    vigenciafromlock = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true),
                    signaturemanual = table.Column<string>(type: "text", nullable: true),
                    geolocation = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    checkadult = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((0))"),
                    checkexpired = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((0))"),
                    bpcompanyworkflowid = table.Column<int>(type: "int", nullable: true),
                    metadata = table.Column<string>(type: "varchar(max)", nullable: true),
                    urlbpweb = table.Column<string>(type: "varchar(max)", nullable: true),
                    callbackurl = table.Column<string>(type: "varchar(1024)", unicode: false, maxLength: 1024, nullable: true),
                    redirecturl = table.Column<string>(type: "varchar(1024)", unicode: false, maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_tx", x => x.id);
                    table.ForeignKey(
                        name: "FK_bp_tx_AspNetUsers_userid",
                        column: x => x.userid,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_bp_tx_bp_company_workflow_bpcompanyworkflowid",
                        column: x => x.bpcompanyworkflowid,
                        principalTable: "bp_company_workflow",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_bp_tx_bp_origin_bporiginid",
                        column: x => x.bporiginid,
                        principalTable: "bp_origin",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_bp_tx_company_companyid",
                        column: x => x.companyid,
                        principalTable: "company",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "bp_bir",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    bpidentityid = table.Column<int>(type: "int", nullable: false),
                    authenticationfactor = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((4))"),
                    minutiaetype = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((41))"),
                    bodypart = table.Column<int>(type: "int", nullable: false, defaultValueSql: "((16))"),
                    data = table.Column<string>(type: "text", nullable: false),
                    additionaldata = table.Column<string>(type: "varchar(2048)", unicode: false, maxLength: 2048, nullable: true),
                    creation = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    useridenroll = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_bir", x => x.id);
                    table.ForeignKey(
                        name: "FK_bp_bir_bp_identity_bpidentityid",
                        column: x => x.bpidentityid,
                        principalTable: "bp_identity",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "bp_identity_3ro",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    bpidentityid = table.Column<int>(type: "int", nullable: false),
                    type3roservice = table.Column<int>(type: "int", nullable: false),
                    trackid3ro = table.Column<string>(type: "text", nullable: false),
                    creation = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    lastupdate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_identity_3ro", x => x.id);
                    table.ForeignKey(
                        name: "FK_bp_identity_3ro_bp_identity_bpidentityid",
                        column: x => x.bpidentityid,
                        principalTable: "bp_identity",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "bp_identity_dynamicdata",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    bpidentityid = table.Column<int>(type: "int", nullable: false),
                    ddkey = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    ddvalue = table.Column<string>(type: "text", nullable: false),
                    creation = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    lastupdate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_identity_dynamicdata", x => x.id);
                    table.ForeignKey(
                        name: "FK_bp_identity_dynamicdata_bp_identity_bpidentityid",
                        column: x => x.bpidentityid,
                        principalTable: "bp_identity",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "bp_tx_conx",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    bptxid = table.Column<int>(type: "int", nullable: false),
                    consultationtype = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false, defaultValueSql: "((1))"),
                    connectorid = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    trackid = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: false),
                    status = table.Column<int>(type: "int", nullable: true),
                    result = table.Column<int>(type: "int", nullable: true),
                    score = table.Column<double>(type: "float", nullable: true),
                    threshold = table.Column<double>(type: "float", nullable: true),
                    timestamp = table.Column<string>(type: "varchar(25)", unicode: false, maxLength: 25, nullable: false, defaultValueSql: "CONVERT(VARCHAR(20),getdate(),121)"),
                    customertrackid = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
                    trackid3ro = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
                    url3ro = table.Column<string>(type: "varchar(1024)", unicode: false, maxLength: 1024, nullable: true),
                    sample3ro = table.Column<string>(type: "text", nullable: true),
                    callbackurl = table.Column<string>(type: "varchar(250)", unicode: false, maxLength: 250, nullable: true),
                    redirecturl = table.Column<string>(type: "varchar(250)", unicode: false, maxLength: 250, nullable: true),
                    successurl = table.Column<string>(type: "varchar(250)", unicode: false, maxLength: 250, nullable: true),
                    onboardingmandatory = table.Column<int>(type: "int", nullable: true),
                    onboardingtype = table.Column<int>(type: "int", nullable: true),
                    signerinclude = table.Column<int>(type: "int", nullable: true),
                    videoinclude = table.Column<int>(type: "int", nullable: true),
                    videomessage = table.Column<string>(type: "text", nullable: true),
                    videourl = table.Column<string>(type: "varchar(512)", unicode: false, maxLength: 512, nullable: true),
                    theme = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    codereject = table.Column<int>(type: "int", nullable: true),
                    descriptionreject = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
                    session = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
                    xuseragent = table.Column<string>(type: "varchar(1024)", unicode: false, maxLength: 1024, nullable: true),
                    sample3rosource = table.Column<string>(type: "text", nullable: true),
                    sample3rosourcetype = table.Column<int>(type: "int", nullable: true),
                    sample3rotarget = table.Column<string>(type: "text", nullable: true),
                    sample3rotargettype = table.Column<int>(type: "int", nullable: true),
                    audittrailimage = table.Column<string>(type: "text", nullable: true),
                    lowqualityaudittrailimage = table.Column<string>(type: "text", nullable: true),
                    enrollmentIdentifier = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
                    idscan = table.Column<string>(type: "text", nullable: true),
                    idscanfrontimage = table.Column<string>(type: "text", nullable: true),
                    idscanbackimage = table.Column<string>(type: "text", nullable: true),
                    idscanportrait = table.Column<string>(type: "text", nullable: true),
                    idscansignature = table.Column<string>(type: "text", nullable: true),
                    responseocr = table.Column<string>(type: "text", nullable: true),
                    response3ro = table.Column<string>(type: "text", nullable: true),
                    workflow = table.Column<string>(type: "text", nullable: true),
                    autorizationinclude = table.Column<int>(type: "int", nullable: true),
                    autorization = table.Column<int>(type: "int", nullable: true),
                    autorizationmessage = table.Column<string>(type: "text", nullable: true),
                    autorizationdate = table.Column<DateTime>(type: "datetime", nullable: true),
                    selfie2d = table.Column<string>(type: "text", nullable: true),
                    idcardfrontimage = table.Column<string>(type: "text", nullable: true),
                    idcardbackimage = table.Column<string>(type: "text", nullable: true),
                    carregisterimageinclude = table.Column<int>(type: "int", nullable: true),
                    carregisterimagefront = table.Column<string>(type: "text", nullable: true),
                    carregisterimageback = table.Column<string>(type: "text", nullable: true),
                    writingimageinclude = table.Column<int>(type: "int", nullable: true),
                    writingimage = table.Column<string>(type: "text", nullable: true),
                    forminclude = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    form = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_tx_conx", x => x.id);
                    table.ForeignKey(
                        name: "FK_bp_tx_conx_bp_tx_bptxid",
                        column: x => x.bptxid,
                        principalTable: "bp_tx",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "bp_tx_data",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    bptxid = table.Column<int>(type: "int", nullable: false),
                    create = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    dataraw = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_tx_data", x => x.id);
                    table.ForeignKey(
                        name: "FK_bp_tx_data_bp_tx_bptxid",
                        column: x => x.bptxid,
                        principalTable: "bp_tx",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "bp_tx_status_history",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    bptxid = table.Column<int>(type: "int", nullable: false),
                    userid = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    status = table.Column<int>(type: "int", nullable: false),
                    reason = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: false),
                    Update = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bp_tx_status_history", x => x.id);
                    table.ForeignKey(
                        name: "FK_bp_tx_status_history_bp_tx_bptxid",
                        column: x => x.bptxid,
                        principalTable: "bp_tx",
                        principalColumn: "id");
                });

            migrationBuilder.InsertData(
                table: "bp_merit",
                columns: new[] { "id", "description", "name", "uniquemetamapguid" },
                values: new object[] { 1, "Geolocalizacion Biometrika", "Geolocalizacion", "62c380090cfa49001bf7abcd" });

            migrationBuilder.InsertData(
                table: "bp_origin",
                columns: new[] { "id", "description" },
                values: new object[] { 1, "Default" });

            migrationBuilder.InsertData(
                table: "company",
                columns: new[] { "id", "accessname", "additionaldata", "address", "contactname", "createDate", "domain", "endate", "fax", "holding", "name", "phone", "phone2", "rut", "secretkey", "status", "updateDate" },
                values: new object[] { 1, "username1", null, "Tabancura 1515, OF 418, Vitacura", "Gustavo Suhit", new DateTime(2022, 7, 28, 18, 16, 59, 847, DateTimeKind.Local).AddTicks(7187), "@biometrika", null, null, 0, "Biometrika Latam S.A.", "56224029772", "56224029772", "76102607-0", "Secrecretkey1", 0, null });

            migrationBuilder.InsertData(
                table: "bp_company_metamap",
                columns: new[] { "id", "companyid", "description", "name", "uniquemetamapguid" },
                values: new object[] { 1, 1, "Realiza Liveness con Metamap", "Liveness", "62c380090cfa49001bf70cd7" });

            migrationBuilder.InsertData(
                table: "bp_company_theme",
                columns: new[] { "id", "companyid", "jsonconfig", "name" },
                values: new object[] { 1, 1, "{ \"backcolor\": \"#45DF32\"}", "Default" });

            migrationBuilder.InsertData(
                table: "bp_company_workflow",
                columns: new[] { "id", "companyid", "expiration", "name", "statustype", "theme" },
                values: new object[] { 1, 1, 60, "Default", 0, "Default" });

            migrationBuilder.InsertData(
                table: "bp_company_workflow_item",
                columns: new[] { "id", "bpcompanymetamapaid", "bpcompanyworkflowid", "bpmeritid", "jsonconfig", "name" },
                values: new object[] { 1, 1, 1, 0, "{ \"backcolor\": \"#45DF32\"}", "Default" });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_bp_bir_identid",
                table: "bp_bir",
                column: "bpidentityid");

            migrationBuilder.CreateIndex(
                name: "bp_client_by_bp_company",
                table: "bp_client",
                columns: new[] { "companyid", "clientid" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_bp_client_UserId",
                table: "bp_client",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "bp_company_uniquemetamapguid",
                table: "bp_company_metamap",
                columns: new[] { "companyid", "uniquemetamapguid" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "bp_company_theme_name",
                table: "bp_company_theme",
                columns: new[] { "companyid", "name" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "bp_company_workflow_name",
                table: "bp_company_workflow",
                columns: new[] { "companyid", "name" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_bp_company_workflow_item_bpcompanymetamapaid",
                table: "bp_company_workflow_item",
                column: "bpcompanymetamapaid");

            migrationBuilder.CreateIndex(
                name: "IX_bp_company_workflow_item_bpcompanyworkflowid",
                table: "bp_company_workflow_item",
                column: "bpcompanyworkflowid");

            migrationBuilder.CreateIndex(
                name: "IX_bp_company_workflow_item_bpmeritid",
                table: "bp_company_workflow_item",
                column: "bpmeritid");

            migrationBuilder.CreateIndex(
                name: "IX_bp_identity_companyidenroll",
                table: "bp_identity",
                column: "companyid");

            migrationBuilder.CreateIndex(
                name: "IX_bp_identity_companyidenroll_typeid_valueid",
                table: "bp_identity",
                columns: new[] { "companyid", "typeid", "valueid" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_bp_identity_userid",
                table: "bp_identity",
                column: "userid");

            migrationBuilder.CreateIndex(
                name: "IX_bp_identity_valueid",
                table: "bp_identity",
                column: "valueid");

            migrationBuilder.CreateIndex(
                name: "IDX_IDENT_TYPE3RO",
                table: "bp_identity_3ro",
                columns: new[] { "type3roservice", "bpidentityid" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_bp_identity_3ro_bpidentityid",
                table: "bp_identity_3ro",
                column: "bpidentityid");

            migrationBuilder.CreateIndex(
                name: "Idx_Identity",
                table: "bp_identity_dynamicdata",
                column: "bpidentityid");

            migrationBuilder.CreateIndex(
                name: "bp_merit_name",
                table: "bp_merit",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "bp_merit_uniquemetamapguid",
                table: "bp_merit",
                column: "uniquemetamapguid");

            migrationBuilder.CreateIndex(
                name: "bp_tx_company",
                table: "bp_tx",
                column: "companyid");

            migrationBuilder.CreateIndex(
                name: "bp_tx_startdate",
                table: "bp_tx",
                column: "timestampstart");

            migrationBuilder.CreateIndex(
                name: "IX_bp_tx_bpcompanyworkflowid",
                table: "bp_tx",
                column: "bpcompanyworkflowid");

            migrationBuilder.CreateIndex(
                name: "IX_bp_tx_bporiginid",
                table: "bp_tx",
                column: "bporiginid");

            migrationBuilder.CreateIndex(
                name: "IX_bp_tx_userid",
                table: "bp_tx",
                column: "userid");

            migrationBuilder.CreateIndex(
                name: "bp_tx_conx_bp_tx",
                table: "bp_tx_conx",
                column: "bptxid");

            migrationBuilder.CreateIndex(
                name: "bp_tx_data_create",
                table: "bp_tx_data",
                column: "create");

            migrationBuilder.CreateIndex(
                name: "IX_bp_tx_data_bptxid",
                table: "bp_tx_data",
                column: "bptxid");

            migrationBuilder.CreateIndex(
                name: "bp_tx_status_history_txid_date",
                table: "bp_tx_status_history",
                columns: new[] { "bptxid", "Update" });

            migrationBuilder.CreateIndex(
                name: "IDX_ACCESSNAME",
                table: "company",
                column: "accessname",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DeviceCodes_DeviceCode",
                table: "DeviceCodes",
                column: "DeviceCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DeviceCodes_Expiration",
                table: "DeviceCodes",
                column: "Expiration");

            migrationBuilder.CreateIndex(
                name: "IX_Keys_Use",
                table: "Keys",
                column: "Use");

            migrationBuilder.CreateIndex(
                name: "IX_PersistedGrants_ConsumedTime",
                table: "PersistedGrants",
                column: "ConsumedTime");

            migrationBuilder.CreateIndex(
                name: "IX_PersistedGrants_Expiration",
                table: "PersistedGrants",
                column: "Expiration");

            migrationBuilder.CreateIndex(
                name: "IX_PersistedGrants_SubjectId_ClientId_Type",
                table: "PersistedGrants",
                columns: new[] { "SubjectId", "ClientId", "Type" });

            migrationBuilder.CreateIndex(
                name: "IX_PersistedGrants_SubjectId_SessionId_Type",
                table: "PersistedGrants",
                columns: new[] { "SubjectId", "SessionId", "Type" });

            migrationBuilder.CreateIndex(
                name: "IDX_TrackIdBP",
                table: "rd_CITx",
                columns: new[] { "trackidbp", "companyid" });

            migrationBuilder.CreateIndex(
                name: "IX_rd_CITx_companyid",
                table: "rd_CITx",
                column: "companyid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "bp_bir");

            migrationBuilder.DropTable(
                name: "bp_client");

            migrationBuilder.DropTable(
                name: "bp_company_theme");

            migrationBuilder.DropTable(
                name: "bp_company_workflow_item");

            migrationBuilder.DropTable(
                name: "bp_identity_3ro");

            migrationBuilder.DropTable(
                name: "bp_identity_dynamicdata");

            migrationBuilder.DropTable(
                name: "bp_tx_conx");

            migrationBuilder.DropTable(
                name: "bp_tx_data");

            migrationBuilder.DropTable(
                name: "bp_tx_status_history");

            migrationBuilder.DropTable(
                name: "DeviceCodes");

            migrationBuilder.DropTable(
                name: "Keys");

            migrationBuilder.DropTable(
                name: "PersistedGrants");

            migrationBuilder.DropTable(
                name: "rd_CITx");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "bp_company_metamap");

            migrationBuilder.DropTable(
                name: "bp_merit");

            migrationBuilder.DropTable(
                name: "bp_identity");

            migrationBuilder.DropTable(
                name: "bp_tx");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "bp_company_workflow");

            migrationBuilder.DropTable(
                name: "bp_origin");

            migrationBuilder.DropTable(
                name: "company");
        }
    }
}
