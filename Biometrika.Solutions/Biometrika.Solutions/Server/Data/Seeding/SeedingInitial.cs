﻿using Biometrika.Solutions.Shared.Model;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Server.Data.Seeding
{
    public static class SeedingInitial
    {
        public static void Seed(ModelBuilder modelBuilder)
        {
            /*
            INSERT INTO [dbo].[company]
               ([rut],[address],[name],[phone],[phone2],[fax],[createDate],[endate],[updateDate],[domain],[additionaldata],
                    [contactname],[status],[holding],[accessname],[secretkey])
                     VALUES
                           ('76102607-0','Tabancura 1515','Biometrika S.A.','56224029772','56224029772','56224029772',getdate(),
                            null,getdate(),'@biometrika',null,'Gustavo Suhit',0,0,'username1','secretkey1')
                GO
            */
            var companyInitial = new Company
            {
                Id = 1,
                Rut = "76102607-0", Address = "Tabancura 1515, OF 418, Vitacura", Name = "Biometrika Latam S.A.",
                Phone = "56224029772", Phone2 = "56224029772", CreateDate = DateTime.Now, Endate = null,
                Domain = "@biometrika", Additionaldata = null,Contactname = "Gustavo Suhit",Status = 0,
                Holding = 0, Accessname = "username1",Secretkey = "Secrecretkey1"
            };

            modelBuilder.Entity<Company>().HasData(companyInitial);

            /*
             INSERT INTO [dbo].[bp_company_metamap]
                       ([companyid], [name] ,[description], [uniquemetamapguid])
                 VALUES
                       (1, 'Liveness', 'Realiza Liveness con Metamap', '62c380090cfa49001bf70cd7')
            GO
            */
            var metamapInitial = new BpCompanyMetamap
            {
                Id = 1,
                Companyid = 1,
                Name = "Liveness",
                Description = "Realiza Liveness con Metamap",
                UniqueMetamapGuid = "62c380090cfa49001bf70cd7"
            };
            modelBuilder.Entity<BpCompanyMetamap>().HasData(metamapInitial);

            /*
             INSERT INTO [dbo].[bp_company_theme]
                        ([companyid],[name],[jsonconfig])
                 VALUES
                       (1, 'Theme1','{ "backcolor": "#45DF32"}')
                GO
            */
            var themeInitial = new BpCompanyTheme
            {
                Id = 1,
                CompanyId = 1,
                Name = "Default",
                JsonConfig = "{" +
                                "\"logoServiceCompany\": \"/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABAExAAIAAAARAAAAPlEQAAEAAAABAQAAAFERAAQAAAABAAAAAFESAAQAAAABAAA//Z\"," +
                                "\"logoCustomer\": null," +
                                "\"dark\": false," +
                                "\"colors\": {" +
                                            "\"background\": \"#F7F8FB\"," +
                                            "\"primary\": \"#97BE0D\"," +
                                            "\"secondary\": \"#424242\"," +
                                            "\"accent\": \"#82B1FF\"," +
                                            "\"error\": \"#FF5252\"," +
                                            "\"info\": \"#2196F3\"," +
                                            "\"success\": \"#4CAF50\"," +
                                            "\"warning\": \"#FFC107\"," +
                                          "}" +
                              "}"
            };
            modelBuilder.Entity<BpCompanyTheme>().HasData(themeInitial);


            /*
            INSERT INTO [dbo].[bp_merit]
                       ([name],[description],[uniquemetamapguid])
                 VALUES
                       ('Geolocalizacion','Geolocalizacion Biometrika','62c380090cfa49001bf7abcd')
            GO

             */
            var meritInitial = new BpMerit
            {
                Id = 1,
                Name = "Geolocalizacion",
                Description = "Geolocalizacion Biometrika",
                UniqueMetamapGuid = "62c380090cfa49001bf7abcd"
            };
            modelBuilder.Entity<BpMerit>().HasData(meritInitial);

            /*
               INSERT INTO [dbo].[bp_company_workflow]
                          ([companyid],[name],[theme],[expiration])
                    VALUES
                          (1 ,'Wokflow1','Theme1', 60)
               GO
            */
            var wf = new BpCompanyWorkflow
            {
                Id = 1,
                CompanyId = 1,
                Name = "Default",
                BpCompanyThemeId = 1,
                Expiration = 60,
                StatusType = 1, //Hibrid 
                CallbackUrl = "http://webhook.com",
                RedirectUrl = "http://www.biometrika.cl"
            };
            modelBuilder.Entity<BpCompanyWorkflow>().HasData(wf);


            /*
             INSERT INTO [dbo].[bp_company_workflow_item]
                          ([bpcompanyworkflowid],[name],[jsonconfig],[bpcompanymetamapaid],[bpmeritid])
                    VALUES
                          (1,'Default','{ "backcolor": "#45DF32"}',1,1)
               GO
            */
            var wfItem = new BpCompanyWorkflowItem
            {
                Id = 1,
                BpCompanyWorkflowId = 1,
                Name = "Default",
                JsonConfig = "{ \"backcolor\": \"#45DF32\"}",
                BpCompanyMetamapId = 1,
                BpMeritId = 0
            };
            modelBuilder.Entity<BpCompanyWorkflowItem>().HasData(wfItem);

            var bpOrigin = new BpOrigin
            {
                Id = 1,
                Description = "Default"
            };
            modelBuilder.Entity<BpOrigin>().HasData(bpOrigin);


        }
    }
}
