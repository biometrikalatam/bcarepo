﻿using Biometrika.Solutions.Server.Data.Seeding;
using Biometrika.Solutions.Shared.Model;
using Duende.IdentityServer.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Reflection;

namespace Biometrika.Solutions.Server.Data
{
    public partial class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        public ApplicationDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }

        #region General
        //public virtual DbSet<Holding> Holding { get; set; }

        public virtual DbSet<Company> Company { get; set; }
        #endregion General

        #region BP

        //Transacciones
        public virtual DbSet<BpTx> BpTx { get; set; }
        public virtual DbSet<BpTxConx> BpTxConx { get; set; }
        public virtual DbSet<BpTxStatusHistory> BpTxStatusHistory { get; set; }
        public virtual DbSet<BpTxData> BpTxData { get; set; }
        
        //Identity
        public virtual DbSet<BpIdentity> BpIdentity { get; set; }
        public virtual DbSet<BpBir> BpBir { get; set; }
        public virtual DbSet<BpIdentity3ro> BpIdentity3ro { get; set; }
        public virtual DbSet<BpIdentityDynamicdata> BpIdentityDynamicdata { get; set; }
        //Gral
        public virtual DbSet<BpOrigin> BpOrigin { get; set; }
        //Company
        public virtual DbSet<BpCompanyTheme> BpCompanyTheme { get; set; }
        public virtual DbSet<BpCompanyMetamap> BpCompanyMetamap { get; set; }
        public virtual DbSet<BpCompanyWorkflow> BpCompanyWorkflow { get; set; }
        public virtual DbSet<BpCompanyWorkflowItem> BpCompanyWorkflowItem { get; set; }
        //Gral
        public virtual DbSet<BpMerit> BpMerit { get; set; }

        #endregion BP

        //public virtual DbSet<City> City { get; set; }

        //public virtual DbSet<CompanySector> CompanySector { get; set; }
        ////public virtual DbSet<CompanyUser> CompanyUser { get; set; }
        //public virtual DbSet<CompanyWorkflow> CompanyWorkflow { get; set; }
        //public virtual DbSet<Config> Config { get; set; }
        //public virtual DbSet<Country> Country { get; set; }
        //public virtual DbSet<Dept> Dept { get; set; }

        //public virtual DbSet<Hq> Hq { get; set; }
        //public virtual DbSet<NvNotaries> NvNotaries { get; set; }
        //public virtual DbSet<NvNotaryProcedures> NvNotaryProcedures { get; set; }
        //public virtual DbSet<NvNotaryUsers> NvNotaryUsers { get; set; }
        //public virtual DbSet<NvProcedures> NvProcedures { get; set; }
        //public virtual DbSet<NvProceduresEvidences> NvProceduresEvidences { get; set; }
        //public virtual DbSet<NvUsers> NvUsers { get; set; }
        //public virtual DbSet<NvUsersProcedures> NvUsersProcedures { get; set; }
        //public virtual DbSet<NvUsersProceduresEvidences> NvUsersProceduresEvidences { get; set; }
        //public virtual DbSet<NvUsersProceduresSignatory> NvUsersProceduresSignatory { get; set; }
        //public virtual DbSet<Point> Point { get; set; }
        //public virtual DbSet<RdAplicaciones> RdAplicaciones { get; set; }
        public virtual DbSet<RdCitx> RdCitx { get; set; }
        //public virtual DbSet<RdExtensiones> RdExtensiones { get; set; }
        //public virtual DbSet<RdLog> RdLog { get; set; }
        //public virtual DbSet<RdRecibos> RdRecibos { get; set; }
        ////public virtual DbSet<Rol> Rol { get; set; }
        //public virtual DbSet<Sector> Sector { get; set; }
        //public virtual DbSet<Town> Town { get; set; }
        //public virtual DbSet<User> User { get; set; }
        //public virtual DbSet<UserRol> UserRol { get; set; }





        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            // Global turn off delete behaviour on foreign keys
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.NoAction;
            }
            //object value = modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            //modelBuilder.ApplyConfiguration(new BpTxConfiguration());
            //Toma todas las config adicionales de cada tabla dentro del assembly
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            SeedingInitial.Seed(modelBuilder);

            OnModelCreatingPartial(modelBuilder);

            //modelBuilder.Entity<CompanyWorkflow>(entity =>
            //{
            //    entity.HasIndex(e => new { e.Companyid, e.Name }, "IDX_CompanyId_NAme")
            //        .IsUnique()
            //        .IsClustered();

            //    entity.Property(e => e.Bpwebtypeonboarding).HasDefaultValueSql("('tv3d')");

            //    entity.Property(e => e.Bpwebtypeverify).HasDefaultValueSql("('tv3d')");

            //    entity.Property(e => e.Datecreate).HasDefaultValueSql("(getdate())");

            //    entity.Property(e => e.Dateupdate).HasDefaultValueSql("(getdate())");

            //    entity.Property(e => e.Id).ValueGeneratedOnAdd();

            //    entity.Property(e => e.Theme).HasDefaultValueSql("('nv')");

            //    entity.Property(e => e.Threshold).HasDefaultValueSql("((50))");

            //    entity.HasOne(d => d.Company)
            //        .WithMany()
            //        .HasForeignKey(d => d.Companyid)
            //        .HasConstraintName("FK_bp_Company_workflow_bp_Company");
            //});

            //modelBuilder.Entity<Config>(entity =>
            //{
            //    entity.HasOne(d => d.CompanyNavigation)
            //        .WithMany(p => p.Config)
            //        .HasForeignKey(d => d.Company)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("FK_Config_bp_Company");
            //});

            //modelBuilder.Entity<NvNotaries>(entity =>
            //{
            //    entity.Property(e => e.Dateinit).HasDefaultValueSql("(getdate())");

            //    entity.Property(e => e.Enddate).HasDefaultValueSql("((1))");

            //    entity.Property(e => e.Status).HasDefaultValueSql("((1))");
            //});

            //modelBuilder.Entity<NvNotaryProcedures>(entity =>
            //{
            //    entity.Property(e => e.Dateinit).HasDefaultValueSql("(getdate())");

            //    entity.Property(e => e.Enabled).HasDefaultValueSql("((1))");

            //    entity.HasOne(d => d.IdnotaryNavigation)
            //        .WithMany(p => p.NvNotaryProcedures)
            //        .HasForeignKey(d => d.Idnotary)
            //        .HasConstraintName("FK_nv_notary_procedures_nv_notaries");

            //    entity.HasOne(d => d.IdprocedureNavigation)
            //        .WithMany(p => p.NvNotaryProcedures)
            //        .HasForeignKey(d => d.Idprocedure)
            //        .HasConstraintName("FK_nv_notary_procedures_nv_procedures");
            //});

            //modelBuilder.Entity<NvNotaryUsers>(entity =>
            //{
            //    entity.Property(e => e.Datecreate).HasDefaultValueSql("(getdate())");

            //    entity.Property(e => e.Rol).HasDefaultValueSql("((2))");
            //});

            //modelBuilder.Entity<NvProceduresEvidences>(entity =>
            //{
            //    entity.Property(e => e.Type)
            //        .HasDefaultValueSql("((1))")
            //        .HasComment("1-CI|2-Texto|3-JPG|4-PDF|5-XML");

            //    entity.HasOne(d => d.IdprocedureNavigation)
            //        .WithMany(p => p.NvProceduresEvidences)
            //        .HasForeignKey(d => d.Idprocedure)
            //        .HasConstraintName("FK_nv_procedures_evidences_nv_procedures");
            //});

            //modelBuilder.Entity<NvUsers>(entity =>
            //{
            //    entity.Property(e => e.Datecreate).HasDefaultValueSql("(getdate())");

            //    entity.Property(e => e.Datelastlogin).HasDefaultValueSql("(getdate())");
            //});

            //modelBuilder.Entity<NvUsersProcedures>(entity =>
            //{
            //    entity.Property(e => e.Dateinit).HasDefaultValueSql("(getdate())");

            //    entity.Property(e => e.Idprovider).HasDefaultValueSql("((1))");

            //    entity.Property(e => e.Message).HasComment("Mensaje del cleinte final explicando particularidad del tramite en si");

            //    entity.Property(e => e.Status)
            //        .HasDefaultValueSql("((1))")
            //        .HasComment("1-Init|2-InNotarizing|3-Error|4-Success");

            //    entity.Property(e => e.Statusnotary).HasDefaultValueSql("((0))");

            //    entity.HasOne(d => d.IduserNavigation)
            //        .WithMany(p => p.NvUsersProcedures)
            //        .HasForeignKey(d => d.Iduser)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("FK_nv_users_procedures_nv_users");
            //});

            //modelBuilder.Entity<NvUsersProceduresEvidences>(entity =>
            //{
            //    entity.HasOne(d => d.IduserprocedureNavigation)
            //        .WithMany(p => p.NvUsersProceduresEvidences)
            //        .HasForeignKey(d => d.Iduserprocedure)
            //        .HasConstraintName("FK_nv_users_procedures_evidences_nv_users_procedures");
            //});

            //modelBuilder.Entity<NvUsersProceduresSignatory>(entity =>
            //{
            //    entity.Property(e => e.Signatoryorder).HasDefaultValueSql("((0))");

            //    entity.Property(e => e.Typeid).HasDefaultValueSql("('RUT')");

            //    entity.HasOne(d => d.IduserprocedureNavigation)
            //        .WithMany(p => p.NvUsersProceduresSignatory)
            //        .HasForeignKey(d => d.Iduserprocedure)
            //        .HasConstraintName("FK_nv_users_procedures_signatory_nv_users_procedures");
            //});

            //modelBuilder.Entity<Point>(entity =>
            //{
            //    entity.Property(e => e.DevSerialnumber).IsFixedLength();
            //});

            //modelBuilder.Entity<RdAplicaciones>(entity =>
            //{
            //    entity.Property(e => e.Checkingen).HasComment("Indica si se chequean extensiones con tipo en generacion [0-Segun Programacion|1-Siempre|2-Nunca]");

            //    entity.Property(e => e.Qextensiones).HasDefaultValueSql("((1))");

            //    entity.Property(e => e.Sendmailresult).HasDefaultValueSql("((0))");

            //    entity.HasOne(d => d.CompanyNavigation)
            //        .WithMany(p => p.RdAplicaciones)
            //        .HasForeignKey(d => d.Company)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("FK_rd_aplicaciones_bp_Company");
            //});

            //modelBuilder.Entity<RdCitx>(entity =>
            //{
            //    entity.Property(e => e.Dategen).HasDefaultValueSql("(getdate())");

            //    entity.Property(e => e.Lastmodify).HasDefaultValueSql("(getdate())");

            //    entity.Property(e => e.Qrvalidity)
            //        .HasDefaultValueSql("((5))")
            //        .HasComment("Minutos de validez del QR generado");

            //    entity.Property(e => e.Resultcode).HasDefaultValueSql("((2))");

            //    entity.Property(e => e.Username).IsFixedLength();

            //    entity.Property(e => e.Validitydays).HasDefaultValueSql("((7))");

            //    entity.HasOne(d => d.Company)
            //        .WithMany(p => p.RdCitx)
            //        .HasForeignKey(d => d.Companyid)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("FK_rd_CITx_bp_Company");
            //});

            //modelBuilder.Entity<RdExtensiones>(entity =>
            //{
            //    entity.Property(e => e.Tipo).HasDefaultValueSql("('string')");

            //    entity.HasOne(d => d.IdaplicacionNavigation)
            //        .WithMany(p => p.RdExtensiones)
            //        .HasForeignKey(d => d.Idaplicacion)
            //        .HasConstraintName("FK_rd_extensiones_rd_aplicaciones");
            //});

            //modelBuilder.Entity<RdRecibos>(entity =>
            //{
            //    entity.Property(e => e.Fecha).HasDefaultValueSql("(getdate())");

            //    entity.HasOne(d => d.IdaplicacionNavigation)
            //        .WithMany(p => p.RdRecibos)
            //        .HasForeignKey(d => d.Idaplicacion)
            //        .HasConstraintName("FK_rd_recibos_rd_aplicaciones");
            //});

            //modelBuilder.Entity<User>(entity =>
            //{
            //    entity.Property(e => e.Id).ValueGeneratedOnAdd();

            //    entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");

            //    entity.Property(e => e.UpdateDate).HasDefaultValueSql("(getdate())");

            //    entity.HasOne(d => d.IdNavigation)
            //        .WithOne(p => p.User)
            //        .HasForeignKey<User>(d => d.Id)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("FK_User_CompanyUser");
            //});

            //modelBuilder.Entity<UserRol>(entity =>
            //{
            //    entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");

            //    entity.Property(e => e.UpdateDate).HasDefaultValueSql("(getdate())");

            //    entity.HasOne(d => d.User)
            //        .WithMany(p => p.UserRol)
            //        .HasForeignKey(d => d.UserId)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("FK_UserRol_Rol");

            //    entity.HasOne(d => d.UserNavigation)
            //        .WithMany(p => p.UserRol)
            //        .HasForeignKey(d => d.UserId)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("FK_UserRol_User");
            //});


        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}