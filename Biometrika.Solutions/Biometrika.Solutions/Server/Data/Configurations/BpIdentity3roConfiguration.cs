﻿using Biometrika.Solutions.Shared.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Biometrika.Solutions.Server.Data.Configurations
{
    public class BpIdentity3roConfiguration : IEntityTypeConfiguration<BpIdentity3ro>
    {


        public void Configure(EntityTypeBuilder<BpIdentity3ro> builder)
        {
            builder.Property(prop => prop.Creation).HasDefaultValueSql("(getdate())");
            builder.Property(prop => prop.Lastupdate).HasDefaultValueSql("(getdate())");           
        }

    }
}
