﻿using Biometrika.Solutions.Shared.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Biometrika.Solutions.Server.Data.Configurations
{
    public class BpTxConfiguration : IEntityTypeConfiguration<BpTx>
    {


        public void Configure(EntityTypeBuilder<BpTx> builder)
        {
            builder.Property(prop => prop.Timestampstart).HasDefaultValueSql("(getdate())");
            builder.Property(prop => prop.Timestampend).HasDefaultValueSql("(getdate())");

            builder.Property(prop => prop.StatusType).HasDefaultValueSql("((0))");
            builder.Property(prop => prop.Status).HasDefaultValueSql("((0))");

            builder.Property(prop => prop.Actiontype).HasDefaultValueSql("((1))");      //Verify
            builder.Property(prop => prop.Operationcode).HasDefaultValueSql("((0))");   //Sin Error
            builder.Property(prop => prop.Operationsource).HasDefaultValueSql("((0))"); //Default (Solo Local?)

            builder.Property(prop => prop.Result).HasDefaultValueSql("((0))");
            builder.Property(prop => prop.Score).HasDefaultValueSql("((0))");
            builder.Property(prop => prop.Threshold).HasDefaultValueSql("((50))");

            builder.Property(prop => prop.BpOriginId).HasDefaultValueSql("((1))");
            
            builder.Property(prop => prop.Authenticationfactor).HasDefaultValueSql("((4))");    //Facial
            builder.Property(prop => prop.Minutiaetype).HasDefaultValueSql("((41))");           //JPG
            builder.Property(prop => prop.Bodypart).HasDefaultValueSql("((16))");               //CAra
            
            builder.Property(prop => prop.Checkadult).HasDefaultValueSql("((0))");
            builder.Property(prop => prop.Checkexpired).HasDefaultValueSql("((0))");
        }

    }
}
