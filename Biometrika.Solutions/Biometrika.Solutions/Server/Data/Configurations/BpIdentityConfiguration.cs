﻿using Biometrika.Solutions.Shared.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Biometrika.Solutions.Server.Data.Configurations
{
    public class BpIdentityConfiguration : IEntityTypeConfiguration<BpIdentity>
    {


        public void Configure(EntityTypeBuilder<BpIdentity> builder)
        {
            builder.Property(prop => prop.Creation).HasDefaultValueSql("(getdate())");
         
        }

    }
}
