﻿using Biometrika.Solutions.Shared.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Biometrika.Solutions.Server.Data.Configurations
{
    public class BpIdentityDynamicdataConfiguration : IEntityTypeConfiguration<BpIdentityDynamicdata>
    {


        public void Configure(EntityTypeBuilder<BpIdentityDynamicdata> builder)
        {
            builder.Property(prop => prop.Creation).HasDefaultValueSql("(getdate())");
            builder.Property(prop => prop.Lastupdate).HasDefaultValueSql("(getdate())");           
        }

    }
}
