﻿using Biometrika.Solutions.Shared.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Biometrika.Solutions.Server.Data.Configurations
{
    public class BpTxDataConfiguration : IEntityTypeConfiguration<BpTxData>
    {


        public void Configure(EntityTypeBuilder<BpTxData> builder)
        {
            builder.Property(prop => prop.Create).HasDefaultValueSql("(getdate())");
        }

    }
}
