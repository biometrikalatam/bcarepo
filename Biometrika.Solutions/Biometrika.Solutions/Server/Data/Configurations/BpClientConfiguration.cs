﻿using Biometrika.Solutions.Shared.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Biometrika.Solutions.Server.Data.Configurations
{
    public class BpClientConfiguration : IEntityTypeConfiguration<BpClient>
    {


        public void Configure(EntityTypeBuilder<BpClient> builder)
        {

            builder.Property(prop => prop.Createddate).HasDefaultValueSql("(getdate())");
            builder.Property(prop => prop.Lastmodify).HasDefaultValueSql("(getdate())");
            builder.Property(prop => prop.Status).HasDefaultValueSql("((0))");              //Disabled

        }

    }
}
