﻿using Biometrika.Solutions.Shared.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Biometrika.Solutions.Server.Data.Configurations
{
    public class BpBirConfiguration : IEntityTypeConfiguration<BpBir>
    {


        public void Configure(EntityTypeBuilder<BpBir> builder)
        {
            builder.Property(prop => prop.Creation).HasDefaultValueSql("(getdate())");
            builder.Property(prop => prop.Authenticationfactor).HasDefaultValueSql("((4))");    //Facial
            builder.Property(prop => prop.Minutiaetype).HasDefaultValueSql("((41))");           //JPG
            builder.Property(prop => prop.Bodypart).HasDefaultValueSql("((16))");               //CAra
        }

    }
}
