﻿using Biometrika.Solutions.Shared.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Biometrika.Solutions.Server.Data.Configurations
{
    public class BpTxConxConfiguration : IEntityTypeConfiguration<BpTxConx>
    {


        public void Configure(EntityTypeBuilder<BpTxConx> builder)
        {
            builder.Property(prop => prop.Consultationtype).HasDefaultValueSql("((1))");
            builder.Property(prop => prop.Timestamp).HasDefaultValueSql("CONVERT(VARCHAR(20),getdate(),121)");
            
            //builder.Property(prop => prop.Connectorid).HasDefaultValueSql("Metamap");

        }

    }
}
