﻿using Biometrika.Solutions.Shared.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Biometrika.Solutions.Server.Data.Configurations
{
    public class BpTxStatusHistoryConfiguration : IEntityTypeConfiguration<BpTxStatusHistory>
    {


        public void Configure(EntityTypeBuilder<BpTxStatusHistory> builder)
        {
            builder.Property(prop => prop.Update).HasDefaultValueSql("(getdate())");
            
        }

    }
}
