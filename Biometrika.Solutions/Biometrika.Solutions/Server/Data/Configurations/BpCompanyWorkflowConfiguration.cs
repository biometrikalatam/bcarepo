﻿using Biometrika.Solutions.Shared.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Biometrika.Solutions.Server.Data.Configurations
{
    public class BpCompanyWorkflowConfiguration : IEntityTypeConfiguration<BpCompanyWorkflow>
    {


        public void Configure(EntityTypeBuilder<BpCompanyWorkflow> builder)
        {
            builder.Property(prop => prop.UseTheme).HasDefaultValueSql("((1))");
            builder.Property(prop => prop.Expiration).HasDefaultValueSql("((60))");
            builder.Property(prop => prop.StatusType).HasDefaultValueSql("((0))");  //0 - Acepta Automatico | 1 - Coloca default para revisar
           
        }

    }
}
