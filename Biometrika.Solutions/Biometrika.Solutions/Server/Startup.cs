﻿using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Server.Repositories;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Biometrika.Solutions.Server.Utilidades;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Biometrika.Solutions.Server.Servicios;
using System.Text.Json.Serialization;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace Biometrika.Solutions.Server
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {


            // Add services to the container.
            var connectionString = Configuration.GetConnectionString("DefaultConnection");

            services.AddControllers(options =>
            {
                options.Conventions.Add(new SwaggerGroupByVersion());
            }).AddJsonOptions(options => options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

            services.AddDbContext<ApplicationDbContext>(options =>
                {
                    options.UseSqlServer(connectionString);
                    //Hace que los query sean de solo lectura => mas rapidos. Si se necesita reescribir en la consulta se hace:
                    // return await context.Generos.AsTracking.ToListAsync();
                    options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
                }
            );
            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {
                    Title = "Biometrika Solutions API",
                    Version = "v1",
                    Description = "API Rest v1 de conexión a soluciones de Biometrika: Biometrika BioPortal," +
                                  " Biometrika Certification Identity, Biometrika Document Electronic, Biometrika WAIS." +
                                  Environment.NewLine + 
                                  "Todos los accesos a los enpoints se realizan presentando un JWT Token generado en Auth.",
                    Contact = new OpenApiContact
                    {
                        Email = "info@biometrika.cl",
                        Name = "Biometrika Latam S.A.",
                        Url = new Uri("http://www.biometrikalatam.com")
                    }
                });
                c.SwaggerDoc("v1private", new OpenApiInfo
                {
                    Title = "Biometrika Solutions API [Private]",
                    Version = "v1",
                    Description = "API Rest v1 [Private] de conexión a soluciones de Biometrika: Biometrika BioPortal, Biometrika Certification Identity, Biometrika Document Electronic, Biometrika WAIS",
                    Contact = new OpenApiContact
                    {
                        Email = "info@biometrika.cl",
                        Name = "Biometrika Latam S.A.",
                        Url = new Uri("http://www.biometrikalatam.com")
                    }
                });
                c.SwaggerDoc("v2", new OpenApiInfo {
                    Title = "Biometrika Solutions API",
                    Version = "v2",
                    Description = "API Rest v2 de conexión a soluciones de Biometrika: Biometrika BioPortal, Biometrika Certification Identity, Biometrika Document Electronic, Biometrika WAIS",
                    Contact = new OpenApiContact
                    {
                        Email = "info@biometrika.cl",
                        Name = "Biometrika Latam S.A.",
                        Url = new Uri("http://www.biometrikalatam.com")
                    }
                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });

                var fileXML = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var routeXML = Path.Combine(AppContext.BaseDirectory, fileXML);
                c.IncludeXmlComments(routeXML);
            });


            //services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
            //    .AddUserManager<UserManager<ApplicationUser>>()
            //    .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options => options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(Configuration["JWTKey"])),
                        ClockSkew = TimeSpan.Zero
                    });

            //.AddUserManager<UserManager<ApplicationUser>>()
            services.AddIdentity<ApplicationUser, IdentityRole>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddUserManager<UserManager<ApplicationUser>>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders()
                .AddDefaultUI(); 

            //services.AddIdentity<ApplicationUser, IdentityRole>()
            //    .AddEntityFrameworkStores<ApplicationDbContext>()
            //    .AddDefaultTokenProviders();

            services.AddIdentityServer()
                .AddApiAuthorization<ApplicationUser, ApplicationDbContext>();

            services.AddAuthorization(options =>
            {
                options.AddPolicy("SuperAdmin", policy => policy.RequireClaim("SuperAdmin"));
                options.AddPolicy("Admin", policy => policy.RequireClaim("Admin"));
            });

            //services.AddAuthentication(options =>
            //{
            //    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //}).AddIdentityServerJwt();

            //Habilito CORS para cualqueir URL que se conecte
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
                });
            });

            //Permite usar servicio para encrypt/decrypt
            services.AddDataProtection();

            //AutoMapper
            services.AddAutoMapper(typeof(Startup));

            //Repositories
            services.AddScoped<IRepositoryCountry, RepositoryCountry>();
            services.AddScoped<IRepositoryCI, RepositoryCI>();
            services.AddScoped<IRepositoryAuth, RepositoryAuth>();
            services.AddScoped<IRepositoryBPWeb, RepositoryBPWeb>();
            services.AddScoped<IRepositoryBP, RepositoryBP>();


            services.AddTransient<HashService>();


            services.AddControllersWithViews();
            services.AddRazorPages();
        } 

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            // Configure the HTTP request pipeline.
            if (env.IsDevelopment())
            {
                app.UseMigrationsEndPoint();
                app.UseWebAssemblyDebugging();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseCors();

            app.UseIdentityServer();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "api/docs";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Biometrika Solutions API v1");
                //c.SwaggerEndpoint("/swagger/v1.private/swagger.json", "Biometrika Solutions API v1 [Private]");
                c.SwaggerEndpoint("/swagger/v2/swagger.json", "Biometrika Solutions API v2");
            });
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "private/api/docs";
                c.SwaggerEndpoint("/swagger/v1private/swagger.json", "Biometrika Solutions API v1 [Private]");
            });
            app.UseStaticFiles();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("index.html");
            });

        }
       
    }
}


//  BACKUP
//using Biometrika.Solutions.Server.Data;
//
//using Microsoft.AspNetCore.Authentication;
//using Microsoft.AspNetCore.Authentication.JwtBearer;
//using Microsoft.AspNetCore.Identity;
//using Microsoft.EntityFrameworkCore;

//namespace Biometrika.Solutions.Server
//{
//    public class Startup
//    {
//        public IConfiguration Configuration { get; }

//        public Startup(IConfiguration configuration)
//        {
//            Configuration = configuration;
//        }

//        public void ConfigureServices(IServiceCollection services)
//        {
//            // Add services to the container.
//            var connectionString = Configuration.GetConnectionString("DefaultConnection");


//            services.AddDbContext<ApplicationDbContext>(options =>
//               options.UseSqlServer(connectionString));
//            services.AddDatabaseDeveloperPageExceptionFilter();

//            services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
//                .AddUserManager<UserManager<ApplicationUser>>()
//                .AddEntityFrameworkStores<ApplicationDbContext>();

//            services.AddIdentityServer()
//                .AddApiAuthorization<ApplicationUser, ApplicationDbContext>();

//            services.AddAuthentication()
//                .AddIdentityServerJwt();

//            //services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer();
//            ////.AddIdentityServerJwt();
//            ////.AddJwtBearer();

//            ////services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
//            ////    .AddEntityFrameworkStores<ApplicationDbContext>();
//            ////services.AddIdentity<IdentityUser, IdentityRole>()
//            ////        .AddEntityFrameworkStores<ApplicationDbContext>()
//            ////        .AddDefaultTokenProviders();
//            //services.AddDefaultIdentity<IdentityUser>().AddRoles<IdentityRole>()
//            //    .AddEntityFrameworkStores<ApplicationDbContext>()
//            //    .AddDefaultTokenProviders();
//            //    //.AddUserManager<UserManager>();
//            //    //.AddInMemoryIdentityResources(IdentityConfiguration.GetIdentityResources())
//            //    //.AddInMemoryClients(IdentityConfiguration.GetClients(Configuration));

//            ////services.AddIdentityServer()
//            ////    .AddApiAuthorization<ApplicationUser, ApplicationDbContext>();

//            ////services.AddAuthentication()
//            ////        .AddIdentityServerJwt();



//            services.AddControllersWithViews();
//            services.AddRazorPages();
//        }

//        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
//        {

//            // Configure the HTTP request pipeline.
//            if (env.IsDevelopment())
//            {
//                app.UseMigrationsEndPoint();
//                app.UseWebAssemblyDebugging();
//            }
//            else
//            {
//                app.UseExceptionHandler("/Error");
//                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
//                app.UseHsts();
//            }

//            app.UseHttpsRedirection();

//            app.UseBlazorFrameworkFiles();
//            app.UseStaticFiles();

//            app.UseRouting();

//            app.UseIdentityServer();
//            app.UseAuthentication();
//            app.UseAuthorization();

//            app.UseEndpoints(endpoints =>
//            {
//                endpoints.MapRazorPages();
//                endpoints.MapControllers();
//                endpoints.MapFallbackToFile("index.html");
//            });

//        }

//    }
//}