using Biometrika.Solutions.Server;
using Biometrika.Solutions.Server.Data;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Host.UseSerilog((context, config) =>
{
    config.ReadFrom.Configuration(context.Configuration);
});

var startup = new Startup(builder.Configuration);

startup.ConfigureServices(builder.Services);

var app = builder.Build();

startup.Configure(app, app.Environment);

//DateTime d = DateTime.ParseExact("01/05/2022", "dd/MM/yyyy", null);

//try
//{
//    DateTime d1 = DateTime.ParseExact("01/05/2022", "yyyy-MM-dd", null);
//}
//catch (Exception ex)
//{
//    string s = ex.Message;
//}


app.Run();


//using Biometrika.Solutions.Server;
//using Biometrika.Solutions.Server.Data;
//
//using Microsoft.AspNetCore.Authentication;
//using Microsoft.AspNetCore.ResponseCompression;
//using Microsoft.EntityFrameworkCore;

//var builder = WebApplication.CreateBuilder(args);

//var startup = new Startup(builder.Configuration);

//startup.ConfigureServices(builder.Services);

//var app = builder.Build();

//startup.Configure(app, app.Environment);

//app.Run();
