﻿using AutoMapper;
using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Server.Controllers.V2
{

    ///// <summary>
    ///// Controller para manejo interacción con BPWeb
    ///// </summary>
    //[ApiController]
    //[Route("api/v2")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)] //, Policy = "SuperAdmin,Admin")]
    //public class BpWebController : ControllerBase
    //{
    //    private readonly ApplicationDbContext _DbContext;
    //    private readonly ILogger<CountryController> _logger;
    //    private readonly IRepositoryCI _RepositoryCI;
    //    private readonly IMapper _mapper;

    //    public BpWebController(ApplicationDbContext _ApplicationDbContext,
    //                             ILogger<CountryController> logger,
    //                             IRepositoryCI repositoryCI,
    //                             IMapper mapper)
    //    {
    //        _DbContext = _ApplicationDbContext;
    //        _logger = logger;
    //        _RepositoryCI = repositoryCI;
    //        _mapper = mapper;
    //    }

    //    // public async Task<ActionResult<List<RdCitxGridDTO>>> Get(FilterDTO filters)
    //    [HttpPost]
    //    [Route("citxlist")]
    //    public async Task<ActionResult<BSResponse>> Get(FilterDTO filters)
    //    {
    //        BSResponse ret = new BSResponse(0, null, null);
    //        List<BPtxGridDTO> txList = null;
    //        try
    //        {
    //            var email = HttpContext.User.Claims.Where(claim => claim.Type == "email").FirstOrDefault();

    //            //Ejemplo de Autommaper
    //            //var origen = _mapper.Map<BpOrigen>(BpOrigenDTO); 


    //            if (filters == null)
    //            {
    //                ret.code = 400;
    //                ret.message = "Los filtros no pueden ser nulos";
    //                return StatusCode(StatusCodes.Status400BadRequest, ret);
    //            }
    //            _logger.LogDebug("BpWebController.txlist IN...");
    //            //txList = await _RepositoryCI.CItxListToDTOGrid(filters);//await _DbContext.Country.ToListAsync();
    //            ret.response = txList;
    //        }
    //        catch (Exception ex)
    //        {
    //            ret.code = StatusCodes.Status500InternalServerError;
    //            ret.message = "BpWebController.Get Excp = " + ex.Message;
    //            ret.response = null;
    //            _logger.LogError("BpWebController.Get.CheckAcces - " + ret.code + " - " + ret.message);
    //            return StatusCode(StatusCodes.Status500InternalServerError, ret);
    //        }
    //        ret.message = "V2";
    //        _logger.LogDebug("BpWebController.Get OUT!");
    //        return StatusCode(StatusCodes.Status200OK, ret); ;
    //    }
    //}
}
