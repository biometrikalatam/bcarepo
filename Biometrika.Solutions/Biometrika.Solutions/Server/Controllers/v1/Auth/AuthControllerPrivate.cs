﻿using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Biometrika.Solutions.Server.Servicios;

namespace Biometrika.Solutions.Server.Controllers.V1Private
{
    [ApiController]
    [Route("api/v1")]
    public class AuthControllerPrivate : ControllerBase
    {
        private readonly ApplicationDbContext _DbContext;
        private readonly IConfiguration _configuration;
        private readonly ILogger<AuthControllerPrivate> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly HashService _hashService;
        private readonly IDataProtector _dataProtector;

        //private readonly IRepositoryCI _RepositoryCI;
        public AuthControllerPrivate(ApplicationDbContext _ApplicationDbContext,
                              IConfiguration configuration,
                                 ILogger<AuthControllerPrivate> logger,
                                 UserManager<ApplicationUser> userManager,
                                 SignInManager<ApplicationUser> signInManager,
                                 IDataProtectionProvider dataProtectorProvider,
                                 HashService hashService)
        {
            _DbContext = _ApplicationDbContext;
            _configuration = configuration;
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
            _hashService = hashService;
            //_RepositoryCI = repositoryCI;
            _dataProtector = dataProtectorProvider.CreateProtector("mi_llave_unica_y_privada");// _configuration["DataProtectorKey"]);
        }

        //#region Demo

        ////[HttpGet("encrypt")]
        ////public ActionResult Encrypt()
        ////{
        ////    var textPlano = "Biometrika";
        ////    var textEncrypted = _dataProtector.Protect(textPlano);
        ////    var textDecrypted = _dataProtector.Unprotect(textEncrypted);

        ////    return Ok(new
        ////    {
        ////        textPLano = textPlano,
        ////        textEncrypted = textEncrypted,
        ////        textDecrypted = textDecrypted
        ////    });
        ////}

        //[HttpGet("encryptByTime")]
        //public ActionResult EncryptByTime()
        //{
        //    var protectByTime = _dataProtector.ToTimeLimitedDataProtector();

        //    var textPlano = "Biometrika";
        //    var textEncrypted = protectByTime.Protect(textPlano, lifetime: TimeSpan.FromSeconds(5));
        //    var textDecrypted = protectByTime.Unprotect(textEncrypted);
        //    Thread.Sleep(6000);
        //    var textDecrypted2 = protectByTime.Unprotect(textEncrypted);

        //    return Ok(new
        //    {
        //        textPLano = textPlano,
        //        textEncrypted = textEncrypted,
        //        textDecrypted = textDecrypted,
        //        textDecrypted2 = textDecrypted2
        //    });
        //}

        //[HttpGet("hash/{plainTex}")]
        //public ActionResult DoHash(string plainText)
        //{
        //    var result1 = _hashService.Hash(plainText);
        //    var result2 = _hashService.Hash(plainText);

        //    return Ok(new
        //    {
        //        plainText = plainText,
        //        hash1 = result1,
        //        hash2 = result2
        //    });
        //}

        //#endregion Demo

        //#region Documentation

        ///// <summary>
        ///// Pedido de token para interactuar con el API
        ///// </summary>
        ///// <remarks>
        ///// Info dentro de remark
        ///// </remarks>
        ///// <param name="token">Si llega, se revisa si es un token válido. Si lo es, retorna el mismo</param>
        ///// <param name="apikey">Llave de acceso a la API, entregada en el inicio del servicio</param>
        ///// <param name="secretkey">Clave secreta de acceso a la API entregada al inicio del servicio.</param>
        ///// <response code="201">Success Created. Funcionamiento ok. Devuelve objeto estándard de respuesta: Token + Expiración</response>
        ///// <response code="400">Bad Request. Indica que faltan parámetros o bien los indicados son incorrectos</response>
        ///// <response code="401">Unahutorized. Apikey/Secretkey no autorizado</response>
        ///// <response code="404">Not Found. Credenciales no encontradas para esa compañía</response>
        ///// <response code="500">Internal Server Error. Error inesperado</response>
        ///// <returns></returns>
        //[HttpGet]
        //[Route("getToken")]
        //[ProducesResponseType(StatusCodes.Status201Created, Type = typeof(ResponseAuthDTO))]
        //[ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ResponseAuthDTO))]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseAuthDTO))]
        //[ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ResponseAuthDTO))]
        //[ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ResponseAuthDTO))]
        //public async Task<ActionResult<ResponseAuthDTO>> getToken(string token, string apikey, string secretkey)
        //{

        //    var emailClaim = HttpContext.User.Claims.Where(claim => claim.Type == "email").FirstOrDefault();
        //    if (emailClaim != null)
        //    {
        //        var email = emailClaim.Value;
        //        var credentialUserDTO = new CredentialUserDTO
        //        {
        //            Email = email
        //        };

        //        return await BuildToken(credentialUserDTO);
        //    }
        //    else
        //    {
        //        return new ResponseAuthDTO
        //        {
        //            Token = null,
        //            Expiration = DateTime.Now
        //        };
        //    }

        //}

        //#endregion documentation


        [HttpPost("register")]
        public async Task<ActionResult<ResponseAuthDTO>> Register(CredentialUserDTO credentialUserDTO)
        {
            var usuario = new ApplicationUser {  UserName = credentialUserDTO.Email , 
                                                 Email = credentialUserDTO.Email,
                                                 CompanyId = credentialUserDTO.CompanyId
                                              };
            var result = await _userManager.CreateAsync(usuario, credentialUserDTO.Password);

            if (result.Succeeded)
            {
                return await BuildToken(credentialUserDTO);
            } else
            {
                return BadRequest("Error registrando usuario [" + result.Errors + "]");
            }

        }


        [HttpPost("login")]
        public async Task<ActionResult<ResponseAuthDTO>> Login(CredentialUserDTO credentialUserDTO)
        {

            var result = await _signInManager.PasswordSignInAsync(credentialUserDTO.Email, credentialUserDTO.Password,
                                                                  isPersistent: false, lockoutOnFailure: false);

            if (result.Succeeded)
            {
                return await BuildToken(credentialUserDTO);
            }
            else
            {
                return BadRequest("Login Incorrecto!");
            }
        }

        [HttpPost("renewToken")]
        public async Task<ActionResult<ResponseAuthDTO>> RenewToken()
        {

            var emailClaim = HttpContext.User.Claims.Where(claim => claim.Type == "email").FirstOrDefault();
            if (emailClaim != null)
            {
                var email = emailClaim.Value;
                var credentialUserDTO = new CredentialUserDTO
                {
                    Email = email
                };

                return await BuildToken(credentialUserDTO);
            } else
            {
                return new ResponseAuthDTO
                {
                    Token = null,
                    Expiration = DateTime.Now
                };
            }
            
        }

        [HttpPost("setRolByClaim")]
        public async Task<ActionResult> SetRolByClaim(string mail, string rol)
        {
            var user = await _userManager.FindByEmailAsync(mail);
            await _userManager.AddClaimAsync(user, new Claim(rol, "1"));
            return NoContent();

        }

        [HttpPost("removeRolByClaim")]
        public async Task<ActionResult> RemoveRolByClaim(string mail, string rol)
        {
            var user = await _userManager.FindByEmailAsync(mail);
            await _userManager.RemoveClaimAsync(user, new Claim(rol, "1"));
            return NoContent();

        }

        #region Private Methods

        private async Task<ResponseAuthDTO> BuildToken(CredentialUserDTO credentialUserDTO)
        {
            var claims = new List<Claim>
            {
                new Claim("email", credentialUserDTO.Email),
                new Claim("rol", "admin")
            };

            var user = await _userManager.FindByEmailAsync(credentialUserDTO.Email);
            claims.Add(new Claim("userid", user.Id));
            claims.Add(new Claim("companyid", user.CompanyId.ToString()));
            var claimDB = await _userManager.GetClaimsAsync(user);

            if (claimDB != null) claims.AddRange(claimDB);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWTKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expiration = DateTime.UtcNow.AddMinutes(Convert.ToDouble(_configuration["JWTExpiration"]));
            var securityToken = new JwtSecurityToken(issuer: null, audience: null, claims: claims,
                                                     expires: expiration, signingCredentials: creds);

            return new ResponseAuthDTO
            {
                Token = new JwtSecurityTokenHandler().WriteToken(securityToken),
                Expiration = expiration
            };
        }


        #endregion Private Methods


    }
}
