﻿using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Biometrika.Solutions.Server.Servicios;

namespace Biometrika.Solutions.Server.Controllers.V1
{
    /// <summary>
    /// Controlador destinado a la obtención de un JWT token para interactuar con el API.
    /// Es necesario que la compañía se haya dado de alta en el ambiente de la solucón, y se cuente con la APIKey y SecretKey
    /// pedidos para la obtención del token.
    /// </summary>
    [ApiController]
    [Route("api/v1")]
    public class AuthController : ControllerBase
    {
        private readonly ApplicationDbContext _DbContext;
        private readonly IConfiguration _configuration;
        private readonly ILogger<AuthController> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly HashService _hashService;
        private readonly IDataProtector _dataProtector;

        //private readonly IRepositoryCI _RepositoryCI;
        public AuthController(ApplicationDbContext _ApplicationDbContext,
                              IConfiguration configuration,
                                 ILogger<AuthController> logger,
                                 UserManager<ApplicationUser> userManager,
                                 SignInManager<ApplicationUser> signInManager,
                                 IDataProtectionProvider dataProtectorProvider,
                                 HashService hashService)
        {
            _DbContext = _ApplicationDbContext;
            _configuration = configuration;
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
            _hashService = hashService;
            //_RepositoryCI = repositoryCI;
            _dataProtector = dataProtectorProvider.CreateProtector("mi_llave_unica_y_privada");// _configuration["DataProtectorKey"]);
        }


        #region Documentation Publica

        /// <summary>
        /// Pedido de token para interactuar con el API. 
        /// </summary>
        /// <remarks>
        /// <para>Se envian como parámetrois la Api Key y la Secret Key entregadas al inicio del servicio. En caso de no poseerlas ponerse
        /// en contacto con soporte@biometrika.cl.</para>
        /// <para>
        /// Se controla que los valores seán válidos, exista en el servicio y estén habilitados.
        /// Si todo funciona bien, se entrega un token válido y la fecha de vencimiento.
        /// Si existen errores se indica en el retorno, un codigo de error de los descritos debajo, y una descripción amigable en 
        /// la propiedad Message del objeto de retorno. 
        /// </para>
        /// <para>Ejemplo de respuesta con funcionamiento correcto e incorrecto sería:</para>
        /// <code>
        /// Correcto
        /// 
        ///     {
        ///         "code": 200,
        ///         "message": "Funcionamiento OK,
        ///         "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImluZm9AYm...",
        ///         "expiration": "2022-09-26T19:35:40.371Z"
        ///     }
        ///
        /// Incorrecto
        ///
        ///     {
        ///         "code": 401,
        ///         "message": "Compañia deshabilitada desde el 01/06/2021",
        ///         "token": null,
        ///         "expiration": null
        ///     }
        /// </code>
        /// </remarks>
        /// <param name="apikey">Llave de acceso a la API, entregada en el inicio del servicio</param>
        /// <param name="secretkey">Clave secreta de acceso a la API entregada al inicio del servicio.</param>
        /// <response code="201">Success Created. Funcionamiento ok. Devuelve objeto estándard de respuesta: Token + Expiración</response>
        /// <response code="400">Bad Request. Indica que faltan parámetros o bien los indicados son incorrectos</response>
        /// <response code="401">Unahutorized. Apikey/Secretkey no autorizado</response>
        /// <response code="404">Not Found. Credenciales no encontradas para esa compañía</response>
        /// <response code="500">Internal Server Error. Error inesperado</response>
        /// <returns></returns>
        [HttpGet]
        [Route("getToken")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(ResponseAuthDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ResponseAuthDTO))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseAuthDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ResponseAuthDTO))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ResponseAuthDTO))]
        public async Task<ActionResult<ResponseAuthDTO>> getToken(string apikey, string secretkey)
        {

            var emailClaim = HttpContext.User.Claims.Where(claim => claim.Type == "email").FirstOrDefault();
            if (emailClaim != null)
            {
                var email = emailClaim.Value;
                var credentialUserDTO = new CredentialUserDTO
                {
                    Email = email
                };

                //return await BuildToken(credentialUserDTO);
                return new ResponseAuthDTO
                {
                    Token = null,
                    Expiration = DateTime.Now
                };
            }
            else
            {
                return new ResponseAuthDTO
                {
                    Token = null,
                    Expiration = DateTime.Now
                };
            }

        }

        #endregion documentation Publica

    }
}
