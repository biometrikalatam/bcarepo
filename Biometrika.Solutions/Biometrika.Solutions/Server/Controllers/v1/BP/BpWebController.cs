﻿    using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NuGet.DependencyResolver;

namespace Biometrika.Solutions.Server.Controllers.V1
{
    /// <summary>
    /// Controller para manejo interacción con BPWeb v1.  
    /// <para>Es posible crear transacciones asincrónicas, de un tipo de workflow (combinación de méritos [validaciones]), para ser 
    /// procesadas via una url que puede ser ejecutada en cualquier navegador de una PC, teléfono celular o tablet</para>
    /// <para>Cuando se crea un nuevo workflow se configura una url de un WebHook, para informar de forma automática cuando se 
    /// llega a un estadoterminal: Reviewing / Rejected / Verified</para>
    /// <para>Al webhook se envía solamente el trackid de la transacción y el estado de la transacción, para que luego el 
    /// sistema del cliente pueda recuoperar la información completa de la transacción si lo desea, con bpweb/tx/get/{trackid}</para>
    /// </summary>
    /// <remarks>
    /// Fecha: 07-2022
    /// </remarks>
    [ApiController]
    [Route("api/v1")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)] //, Policy = "SuperAdmin,Admin")]
    public class BpWebController : ControllerBase
    {
        private readonly ApplicationDbContext _DbContext;
        private readonly ILogger<BpWebController> _logger;
        private readonly IRepositoryBPWeb _RepositoryBPWeb;
        public BpWebController(ApplicationDbContext _ApplicationDbContext,
                                 ILogger<BpWebController> logger,
                                 IRepositoryBPWeb repositoryBPWeb)
        {
            _DbContext = _ApplicationDbContext;
            _logger = logger;
            _RepositoryBPWeb = repositoryBPWeb;
        }

        #region Acceso con Token

        /// <summary>
        /// Crea una transacción para que una persona la compelte a travpes de una URL con BPWeb. 
        /// </summary>
        /// <remarks>
        /// En primer lugar se debe definir el tipo de transacción, indicando los méritos deseados en la misma. 
        /// Los méritos son los tipos de chequeos a realizar, por ejemplo: verificación de documento, liveness, antecedentes judiciales, etc.
        /// Los pasos que realiza este endpoint son (además de los controles de autirización e integridad de parámetros):
        /// <para>>> 1.- Revisa si id de workflow de verificación enviado existe para esta compañía y está habilitado</para>
        /// <para>>> 2.- Crea una nueva transacción con status = CREATED y genera una URL para acceso al wizard de verificación</para>
        /// <para>>> 3.- Retorna los valores generados y envía mails/wathsapps con el link para ingreso al proceso</para>
        /// <para>Los parámetros de entrada son:</para>
        /// <para>* typeid: Valor del tipo de documento a verificar, por ejempo RUT, PAS (Pasamorte), DNI, etc.</para>
        /// <para>* valueid: Número de cédula o pasaporte a verificar. Si no se tiene ese valor y se desea reconocer los datos desde el documento 
        ///                  verificado, se debe enviar "NA". En caso de enviar un valor concreto diferente a NA, cuando se reconoce el documento
        ///                  se verifica que el número reconocido sea igual al pedido, de otra forma retorna error.
        ///                  Si el valor llega en nulo retorna error.</para>
        /// <para>* workflowname: Nombre único para cada compañía de uno de los workflows definidos de verificación. POr ejemplo: "BasciOnBoarding". 
        ///                       Si este valor llega en nulo se retonra error en la creación. </para>
        /// <para>* metadata: Valor flexible para que cada cliente que crea una transacción envíe por ejemplo valores relevantes de su sistema, de tal
        ///                   forma de relacionar esta transacción con el workflow de verificación. 
        ///                   Este valor se retorna luego, en cada recuperación de los datos relacionados a la transacción. 
        ///                   Un ejemplo pdría ser un Id único de transacción de su sistema.</para>
        /// </remarks>
        /// <param name="bpTxCreateDTO">Parámetros de entrada necesarios para operar</param>
        /// <response code="201">Success Created. Funcionamiento ok. Devuelve objeto estándard de respuesta. En data devuelve 
        /// TrackId + URL + Fecha Expiracion</response>
        /// <response code="400">Bad Request. Indica que faltan parámetros o bien los indicados son incorrectos</response>
        /// <response code="401">Unahutorized. Error con el token presentado</response>
        /// <response code="403">Forbidden. Rol del token presentado sin privilegios para esta operación</response>
        /// <response code="404">Not Found. Tipo de operacion pedida (Workflow) no encontrada para esa compañía</response>
        /// <response code="500">Internal Server Error. Error inesperado</response>
        /// <returns></returns>
        [HttpPost]
        [Route("bpweb/tx/create")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status403Forbidden, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(BSResponse))]
        public async Task<ActionResult<BSResponse>> TxCreate([FromBody] BpTxCreateDTO bpTxCreateDTO)
        {
            BSResponse ret = new BSResponse(0, null, null);
            List<BPtxGridDTO> txList = null;
            try
            {
                if (bpTxCreateDTO == null || 
                    string.IsNullOrEmpty(bpTxCreateDTO.Typeid) || string.IsNullOrEmpty(bpTxCreateDTO.Valueid) ||
                    string.IsNullOrEmpty(bpTxCreateDTO.Workflowname))
                {
                    ret.code = StatusCodes.Status400BadRequest;
                    ret.message = "Los parámetros de entrada no pueden ser nulos";
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                }

                //Obtengo el User/Company para usar en la creacion de Tx
                var userid = HttpContext.User.Claims.Where(claim => claim.Type == "userid").FirstOrDefault();
                if (userid == null || string.IsNullOrEmpty(userid.Value))
                {
                    ret.code = StatusCodes.Status404NotFound;
                    ret.message = "Los filtros no pueden ser nulos";
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                } else
                {
                    _logger.LogDebug("BpWebController.TxCreate - CreateBpTx IN...");
                    ret = await _RepositoryBPWeb.CreateBpTx(bpTxCreateDTO, userid.Value);
                }
            }
            catch (Exception ex)
            {
                ret.code = StatusCodes.Status500InternalServerError;
                ret.message = "BpWebController.TxCreate Excp = " + ex.Message;
                ret.response = null;
                _logger.LogError("BpWebController.TxCreate - " + ret.code + " - " + ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }
            _logger.LogDebug("BpWebController.TxCreate OUT! [ret.code = " + ret.code.ToString() + "]");
            return StatusCode(ret.code, ret);
        }

        /// <summary>
        /// Dado un trackid válido devuelve solamente el status de dicha transacción. 
        /// </summary>
        /// <remarks>
        /// Los status posibles son:
        /// <para>* 0 - Created : Cuando se crea la Tx</para>
        /// <para>* 1 - Initialized : Cuando se comenzo a tomar las muestras y se inicio el wizard</para>
        /// <para>* 2 - Started : Cuando se comenzo a toamr las muestras o se inicio el metamapa</para>
        /// <para>* 3 - Processing : En estado de revision xq algun dato no es correcto (Cuando ya completo la captura de datos)</para>
        /// <para>* 4 - Postponed : Cuando se pospone por falta de algun servicio externo</para>
        /// <para>* 5 - Cancelled : Si presionan boton cancelar desde BpWeb</para>
        /// <para>* 6 - Expired : Se marca cuando expiro porque tiene fecha de expiracion </para>
        /// <para>* 7 - Reviewing: Marcado para revisión (ya sea automatico o manual)</para>
        /// <para>* 8 - Rejected : Si queda como rechazado (ya sea automatico o manual)</para>
        /// <para>* 9 - Verified : Verificado ok (ya sea automatico o manual)</para>
        /// </remarks>
        /// <param name="trackid">Id Unico de la transacción, generado al momento de su creación</param>
        /// <response code="302">Found. Encontro la transacción con el trackid indicado, y retorna el status</response>
        /// <response code="400">Bad Request. Indica que faltan parámetros o bien los indicados son incorrectos</response>
        /// <response code="401">Unahutorized. Error con el token presentado</response>
        /// <response code="404">Not Found. NO Encontro la transacción con el trackid indicado</response>
        /// <response code="500">Internal Server Error. Error inesperado</response>
        /// <returns></returns>
        [HttpGet]
        [Route("bpweb/tx/status/{trackid}")]
        [ProducesResponseType(StatusCodes.Status302Found, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(BSResponse))]
        public async Task<ActionResult<BSResponse>> GetBpTxStatusByTrackId(string trackid)
        {
            BSResponse ret = new BSResponse(0, null, null);
            //List<RdCitxGridDTO> txList = null;
            try
            {
                if (string.IsNullOrEmpty(trackid) == null)
                {
                    ret.code = 400;
                    ret.message = "El trackid NO puede ser nulo";
                    _logger.LogDebug("BpWebController.GetBpTxStatusByTrackId IN - " + ret.message);
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                }
                _logger.LogDebug("BpWebController.GetBpTxStatusByTrackId IN...");
                ret = await _RepositoryBPWeb.GetStatusBpTx(trackid);//await _DbContext.Country.ToListAsync();
                //ret.response = txList;
            }
            catch (Exception ex)
            {
                ret.code = StatusCodes.Status500InternalServerError;
                ret.message = "BpWebController.GetBpTxStatusByTrackId Excp = " + ex.Message;
                ret.response = null;
                _logger.LogError("BpWebController.GetBpTGetBpTxStatusByTrackIdxByTrackId - " + ret.code + " - " + ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }
            _logger.LogDebug("BpWebController.GetBpTxStatusByTrackId OUT!");
            return StatusCode(ret.code, ret);
        }

        /// <summary>
        /// Dado un trackid válido devuelve la informacion relacionada a esa transacción. 
        /// </summary>
        /// <param name="trackid">Id Unico de la transacción, generado al momento de su creación</param>
        /// <response code="302">Found. Encontro la transacción con el trackid indicado, y la retorna</response>
        /// <response code="400">Bad Request. Indica que faltan parámetros o bien los indicados son incorrectos</response>
        /// <response code="401">Unahutorized. Error con el token presentado</response>
        /// <response code="404">Not Found. NO Encontro la transacción con el trackid indicado</response>
        /// <response code="500">Internal Server Error. Error inesperado</response>
        /// <returns></returns>
        [HttpGet]
        [Route("bpweb/tx/get/{trackid}")]
        [ProducesResponseType(StatusCodes.Status302Found, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(BSResponse))]
        public async Task<ActionResult<BSResponse>> GetBpTxByTrackId(string trackid)
        {
            BSResponse ret = new BSResponse(0, null, null);
            //List<RdCitxGridDTO> txList = null;
            try
            {
                if (string.IsNullOrEmpty(trackid) == null)
                {
                    ret.code = 400;
                    ret.message = "El trackid NO puede ser nulo";
                    _logger.LogDebug("BpWebController.GetBpTxByTrackId IN - " + ret.message);
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                }
                _logger.LogDebug("BpWebController.GetBpTxByTrackId IN...");
                ret = await _RepositoryBPWeb.GetBpTxByTrackId(trackid);//await _DbContext.Country.ToListAsync();
                //ret.response = txList;
            }
            catch (Exception ex)
            {
                ret.code = StatusCodes.Status500InternalServerError;
                ret.message = "BpWebController.GetBpTxByTrackId Excp = " + ex.Message;
                ret.response = null;
                _logger.LogError("BpWebController.GetBpTxByTrackId - " + ret.code + " - " + ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }
            _logger.LogDebug("BpWebController.GetBpTxByTrackId OUT!");
            return StatusCode(ret.code, ret);
        }

        /// <summary>
        /// Devuelve listado de transacciones realizadas en BioPortal a través de BPWeb, con sus status.
        /// </summary>
        /// <remarks>
        /// Devuelve listado de transacciones realizadas en BioPortal, con su status y detalles de datos relacionados. 
        /// Se debe enviar como parámetro, una lista filtros indicados como pares "key","value". Los valores de filtros posibles son:
        /// <para>* datefrom: fecha desde cuando se desea el listado, con formato dd/MM/yyyy HH:mm:ss. [Ej: 01/09/2021 09:00:00]</para>
        /// <para>* datetill: fecha hasta cuando se desea el listado, con formato dd/MM/yyyy HH:mm:ss. [Ej: 01/09/2021 18:00:00]</para>
        /// <para>* trackid: id único de transacción entregado en el proceso de creación [bpweb/tx/create]</para>
        /// <para>* typeid: valor del tipo de documento por ejemplo RUT</para>
        /// <para>* valueid: valor del número de documento o pasaporte indicado a verificar.</para>
        /// <para>Las fechas indicadas, si superan los 3 meses, se ajusta a ese rango, tomando como base datefrom.</para>
        /// <para>Ejemplo de filtros enviados:</para> 
        /// <code>
        ///     
        ///     {
        ///         "filters": {
        ///                         "datefrom": "01/09/2021 09:00:00",
        ///                         "datetill": "01/09/2021 18:00:00",
        ///                         "valueid": "11111111-1"
        ///                    }
        ///     }
        ///     
        /// </code>
        /// </remarks>
        /// <param name="filters">Filtros para búsqueda en formato [key,value]</param>
        /// <response code="302">Found. Encontro una lista de transacciones con los filtros, y la retorna</response>
        /// <response code="400">Bad Request. Indica que faltan parámetros o bien los indicados son incorrectos</response>
        /// <response code="401">Unahutorized. Error con el token presentado</response>
        /// <response code="404">Not Found. NO Encontro ninguna transacción que cumpla con los filtros</response>
        /// <response code="500">Internal Server Error. Error inesperado</response>
        /// <returns></returns>
        [HttpPost]
        [Route("bpweb/tx/list")]
        [ProducesResponseType(StatusCodes.Status302Found, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(BSResponse))]
        public async Task<ActionResult<BSResponse>> GetBpTxList(FilterDTO filters)
        {
            BSResponse ret = new BSResponse(0, null, null);
            //List<RdCitxGridDTO> txList = null;
            try
            {
                if (filters == null)
                {
                    ret.code = 400;
                    ret.message = "Los filtros no pueden ser nulos";
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                }
                _logger.LogDebug("BpWebController.txlist IN...");
                ret = await _RepositoryBPWeb.ListBpTx(filters);//await _DbContext.Country.ToListAsync();
                //ret.response = txList;
            }
            catch (Exception ex)
            {
                ret.code = StatusCodes.Status500InternalServerError;
                ret.message = "BpWebController.Get Excp = " + ex.Message;
                ret.response = null;
                _logger.LogError("BpWebController.Get.CheckAcces - " + ret.code + " - " + ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }
            _logger.LogDebug("BpWebController.Get OUT!");
            return StatusCode(StatusCodes.Status200OK, ret);
        }

        /// <summary>
        /// Setea el valor de status manualmente, por cambios posteriores al estado automático final generado. 
        /// </summary>
        /// <remarks>
        /// <para>Los estados finales son: Reviewing / Rejected / Verified. Si el estado se intenta cambiar antes de llegar a
        /// alguno de estos estdos terminales, se rechaza la acción.</para>
        /// <para>Los status posibles a enviar son:</para>
        /// <para>* 0 - Created : Cuando se crea la Tx</para>
        /// <para>* 1 - Initialized : Cuando se comenzo a tomar las muestras y se inicio el wizard</para>
        /// <para>* 2 - Started : Cuando se comenzo a toamr las muestras o se inicio el metamapa</para>
        /// <para>* 3 - Processing : En estado de revision xq algun dato no es correcto (Cuando ya completo la captura de datos)</para>
        /// <para>* 4 - Postponed : Cuando se pospone por falta de algun servicio externo</para>
        /// <para>* 5 - Cancelled : Si presionan boton cancelar desde BpWeb</para>
        /// <para>* 6 - Expired : Se marca cuando expiro porque tiene fecha de expiracion </para>
        /// <para>* 7 - Reviewing: Marcado para revisión (ya sea automatico o manual)</para>
        /// <para>* 8 - Rejected : Si queda como rechazado (ya sea automatico o manual)</para>
        /// <para>* 9 - Verified : Verificado ok (ya sea automatico o manual)</para>
        /// </remarks>
        /// <param name="trackid">Id Unico de la transacción, generado al momento de su creación</param>
        /// <param name="status">Status de acuerdo a la lista antes indicada.</param>
        /// <response code="200">Success Updated. Funcionamiento ok. Devuelve objeto estándard de respuesta. En data devuelve 
        /// TrackId</response>
        /// <response code="400">Bad Request. Indica que faltan parámetros o bien los indicados son incorrectos</response>
        /// <response code="401">Unahutorized. Error con el token presentado</response>
        /// <response code="403">Forbidden. Rol del token presentado sin privilegios para esta operación</response>
        /// <response code="404">Not Found. Tipo de operacion pedida (Workflow) no encontrada para esa compañía</response>
        /// <response code="500">Internal Server Error. Error inesperado</response>
        /// <returns></returns>
        [HttpPut]
        [Route("bpweb/tx/status/set/{trackid}/{status:int}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status403Forbidden, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(BSResponse))]
        public async Task<ActionResult<BSResponse>> TxUpdatStatus(string trackid, int status)
        {
            BSResponse ret = new BSResponse(0, null, null);
            List<BPtxGridDTO> txList = null;
            try
            {
                //if (bpTxCreateDTO == null ||
                //    string.IsNullOrEmpty(bpTxCreateDTO.Typeid) || string.IsNullOrEmpty(bpTxCreateDTO.Valueid) ||
                //    string.IsNullOrEmpty(bpTxCreateDTO.Workflowname))
                //{
                //    ret.code = StatusCodes.Status400BadRequest;
                //    ret.message = "Los parámetros de entrada no pueden ser nulos";
                //    return StatusCode(StatusCodes.Status400BadRequest, ret);
                //}

                //Obtengo el User/Company para usar en la creacion de Tx
                var userid = HttpContext.User.Claims.Where(claim => claim.Type == "userid").FirstOrDefault();
                if (userid == null || string.IsNullOrEmpty(userid.Value))
                {
                    ret.code = StatusCodes.Status404NotFound;
                    ret.message = "Los filtros no pueden ser nulos";
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                }
                else
                {
                    _logger.LogDebug("BpWebController.TxCreate - CreateBpTx IN...");
                    ret = null; //await _RepositoryBPWeb.CreateBpTx(bpTxCreateDTO, userid.Value);
                }
            }
            catch (Exception ex)
            {
                ret.code = StatusCodes.Status500InternalServerError;
                ret.message = "BpWebController.TxCreate Excp = " + ex.Message;
                ret.response = null;
                _logger.LogError("BpWebController.TxCreate - " + ret.code + " - " + ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }
            _logger.LogDebug("BpWebController.TxCreate OUT! [ret.code = " + ret.code.ToString() + "]");
            return StatusCode(ret.code, ret);
        }


        #endregion Acceso con Token

        #region Acceso sin Token

        #endregion Acceso sin Token








        #region Private




        #endregion Private
    }
}
