﻿using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NuGet.DependencyResolver;

namespace Biometrika.Solutions.Server.Controllers.V1Private
{
    /// <summary>
    /// Controller para manejo interacción con BPWeb v1.  
    /// </summary>
    /// <remarks>
    /// Fecha: 07-2022
    /// </remarks>
    [ApiController]
    [Route("api/v1")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)] //, Policy = "SuperAdmin,Admin")]
    public class BpWebControllerPrivate : ControllerBase
    {
        private readonly ApplicationDbContext _DbContext;
        private readonly ILogger<BpWebControllerPrivate> _logger;
        private readonly IRepositoryBPWeb _RepositoryBPWeb;
        public BpWebControllerPrivate(ApplicationDbContext _ApplicationDbContext,
                                 ILogger<BpWebControllerPrivate> logger,
                                 IRepositoryBPWeb repositoryBPWeb)
        {
            _DbContext = _ApplicationDbContext;
            _logger = logger;
            _RepositoryBPWeb = repositoryBPWeb;
        }

        #region Acceso con Token

       
        #endregion Acceso con Token

        #region Acceso sin Token

        /// <summary>
        /// Se llama cuando se pide los datos para iniciar el proceso, cambiando el estado de 
        /// Created a Initialized en la transaccion, siempre que no este expirada y el estado sea Created 
        /// </summary>
        /// <param name="trackid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("bpweb/initialize/{trackid}")]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status304NotModified, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status412PreconditionFailed, Type = typeof(BSResponse))]
        [AllowAnonymous]
        public async Task<ActionResult<BSResponse>> InitBpTxByTrackId(string trackid)
        {
            BSResponse ret = new BSResponse(0, null, null);
            //List<RdCitxGridDTO> txList = null;
            try
            {
                if (string.IsNullOrEmpty(trackid) == null)
                {
                    ret.code = 400;
                    ret.message = "El trackid NO puede ser nulo";
                    _logger.LogDebug("BpWebController.InitBpTxByTrackId IN - " + ret.message);
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                }
                _logger.LogDebug("BpWebController.InitBpTxByTrackId IN...");
                ret = await _RepositoryBPWeb.InitializeBpTx(trackid);//await _DbContext.Country.ToListAsync();
                //ret.response = txList;
            }
            catch (Exception ex)
            {
                ret.code = StatusCodes.Status500InternalServerError;
                ret.message = "BpWebController.InitBpTxByTrackId Excp = " + ex.Message;
                ret.response = null;
                _logger.LogError("BpWebController.InitBpTxByTrackId - " + ret.code + " - " + ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }
            _logger.LogDebug("BpWebController.InitBpTxByTrackId OUT!");
            return StatusCode(ret.code, ret);
        }



        /// <summary>
        /// Se llama cuando se pide los datos para iniciar el proceso, cambiando el estado de 
        /// Created a Initialized en la transaccion, siempre que no este expirada y el estado sea Created 
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("bpweb/webhook")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status304NotModified, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status412PreconditionFailed, Type = typeof(BSResponse))]
        [AllowAnonymous]
        public async Task<ActionResult<BSResponse>> WebhookReceive([FromBody] object json)
        {

            /*
                verification_started	
                Sent at the beginning of the SDKs flow, when MetaMap is making a new verification record (usually at the upload of the first ID document)

                verification_inputs_completed	
                Sent when the user has uploaded all inputs via SDKs. You can use that webhook to know when to redirect the user after the metamap is complete on your website/App.

                verification_updated	
                Sent from your MetaMap dashboard manually after updating the user verification information.

                verification_postponed	
                When gov check service is postponed and awaiting until step timeout error according the established databases request timeout.

                verification_completed	
                Sent once MetaMap is done verifying a user entirely. When you get this webhook, you should GET the 'resource' URL to get the verification data about user.

                verification_signed	
                Based by base64NOMTimeStamp and is triggered only after verification_completed when digitalSignature service is finished processing.

                verification_expired	
                Sent when verification is not completed after 30 minutes. It means that the user probably did not finish the metamap.

                step_completed	
                webhook sent after each verification step is completed (liveness, face match, document-reading, alteration-detection, template-matching, watchlist)
            */
            BSResponse ret = new BSResponse(0, null, null);
            //List<RdCitxGridDTO> txList = null;
            try
            {
                if (json == null)
                {
                    ret.code = 400;
                    ret.message = "El json NO puede ser nulo";
                    _logger.LogDebug("BpWebController.WebhookReceive IN - " + ret.message);
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                }
                _logger.LogDebug("BpWebController.WebhookReceive IN...");

                MetamapResponseModel model = JsonConvert.DeserializeObject<MetamapResponseModel>(json.ToString());
                if (model == null)
                {
                    ret.code = 400;
                    ret.message = "Fallo la deserializacion";
                    _logger.LogDebug("BpWebController.WebhookReceive Deserializa nulo - " + ret.message);
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                }
                _logger.LogDebug("BpWebController.WebhookReceive - ProcesaWebhookMsgReceived call...");
                ret = await _RepositoryBPWeb.ProcesaWebhookMsgReceived(model);
                //System.IO.File.WriteAllText(@"d:\temp\AAWebHook\" + DateTime.Now.ToString("yyyyMMddHHmmsssffff") + ".json", json.ToString());
                ret.code = 200;
                ret.response = null;
                //ret = await _RepositoryBPWeb.InitializeBpTx(trackid);//await _DbContext.Country.ToListAsync();
                //ret.response = txList;
            }
            catch (Exception ex)
            {
                try
                {
                    System.IO.File.WriteAllText(@"d:\temp\AAWebHook\" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".error.json", json.ToString());

                }
                catch (Exception ex1)
                {
                    _logger.LogError("BpWebController.WebhookReceive - Excp Grabando en ex = " + ex.Message);
                }
                ret.code = StatusCodes.Status500InternalServerError;
                ret.message = "BpWebController.WebhookReceive Excp = " + ex.Message;
                ret.response = null;
                _logger.LogError("BpWebController.WebhookReceive - " + ret.code + " - " + ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }
            _logger.LogDebug("BpWebController.WebhookReceive OUT!");
            return StatusCode(ret.code, ret);
        }

        

        #endregion Acceso sin Token



        #region Private

       


        #endregion Private
    }
}
