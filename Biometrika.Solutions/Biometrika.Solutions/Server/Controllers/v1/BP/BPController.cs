﻿using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Biometrika.Solutions.Shared.Public;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Server.Controllers.V1
{
    [ApiController]
    [Route("api/v1")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BPController : ControllerBase
    {
        private readonly ApplicationDbContext _DbContext;
        private readonly ILogger<BPController> _logger;
        private readonly IRepositoryBP _RepositoryBP;
        public BPController(ApplicationDbContext _ApplicationDbContext,
                                 ILogger<BPController> logger,
                                 IRepositoryBP repositoryBP)
        {
            _DbContext = _ApplicationDbContext;
            _logger = logger;
            _RepositoryBP = repositoryBP;
        }

        /// <summary>
        /// Devuelve listado de transacciones realizadas en BioPortal, con sus status.
        /// </summary>
        /// <remarks>
        /// Devuelve listado de transacciones realizadas en BioPortal, con su status y detalles de datos relacionados. 
        /// Se debe enviar como parámetro, una lista filtros indicados como pares "key","value". Los valores de filtros posibles son:
        /// <para>* datefrom: fecha desde cuando se desea el listado, con formato dd/MM/yyyy HH:mm:ss. [Ej: 01/09/2021 09:00:00]</para>
        /// <para>* datetill: fecha hasta cuando se desea el listado, con formato dd/MM/yyyy HH:mm:ss. [Ej: 01/09/2021 18:00:00]</para>
        /// <para>* trackid: id único de transacción entregado en el proceso de creación [bpweb/tx/create]</para>
        /// <para>* typeid: valor del tipo de documento por ejemplo RUT</para>
        /// <para>* valueid: valor del número de documento o pasaporte indicado a verificar.</para>
        /// <para>Las fechas indicadas, si superan los 3 meses, se ajusta a ese rango, tomando como base datefrom.</para>
        /// <para>Ejemplo de filtros enviados:</para> 
        /// <code>
        ///     
        ///     {
        ///         "filters": {
        ///                         "datefrom": "01/09/2021 09:00:00",
        ///                         "datetill": "01/09/2021 18:00:00",
        ///                         "valueid": "11111111-1"
        ///                    }
        ///     }
        ///     
        /// </code>
        /// </remarks>
        /// <param name="filters">Filtros para búsqueda en formato [key,value]</param>
        /// <response code="302">Found. Encontro una lista de transacciones con los filtros, y la retorna</response>
        /// <response code="400">Bad Request. Indica que faltan parámetros o bien los indicados son incorrectos</response>
        /// <response code="401">Unahutorized. Error con el token presentado</response>
        /// <response code="404">Not Found. NO Encontro ninguna transacción que cumpla con los filtros</response>
        /// <response code="500">Internal Server Error. Error inesperado</response>
        /// <returns></returns>
        [HttpPost]
        [Route("bp/tx/list")]
        [ProducesResponseType(StatusCodes.Status302Found, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(BSResponse))]
        public async Task<ActionResult<BSResponse>> Get(FilterDTO filters)
        {
            BSResponse ret = new BSResponse(0, null, null);
            List<BPtxGridDTO> txList = null;
            try
            {
                _logger.LogDebug("BPController.bptxlist IN...");
                txList = await _RepositoryBP.bptxListToDTOGrid(filters);//await _DbContext.Country.ToListAsync();
                ret.response = txList;
            }
            catch (Exception ex)
            {
                ret.code = StatusCodes.Status500InternalServerError;
                ret.message = "BPController.bptxlist Excp = " + ex.Message;
                ret.response = null;
                _logger.LogError("CIController.bptxlist - " + ret.code + " - " + ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }
            _logger.LogDebug("CIController.bptxlist OUT!");
            return StatusCode(StatusCodes.Status200OK, ret); ;
        }

        /// <summary>
        /// Realiza acciones sincronicas en BioPortal Server Services
        /// </summary>
        /// <remarks>
        /// Dentro de los parámetros de ingreso, existe una propiedad llamada Action, cuyos valores posibles son:
        /// <para>* 1 - Verify: Realiza una verificación </para>
        /// <para>* 2 - Identify: </para>
        /// <para>* 3 - Enroll: </para>
        /// <para>* 4 - Get:</para>
        /// <para>* 5 - Modify</para>
        /// <para>* 6 - Delete</para>
        /// <para>* 7 - DeleteBir</para>
        /// <para>De acuerdo a la Actionid enviada, se controla que lleguen los otros parámetros correctos, de otra forma se devuelve error.</para>
        /// <para>La explicación de cada parámetro de entrada utilizado se puede revisar en el manual de la versión anterior.</para>
        /// <para>** NOTA IMORTANTE ** : En la versión 6 definitiva, algunos de estos valores pueden cambiar para ajustarlo a 
        /// las nuevas funcionalidades del sistema.</para>        
        /// <para>Los parámetros de entrada son</para> 
        /// <code>
        /// :
        /// </code>
        /// <code>
        /// 
        /// {
        ///  "actionid": 0,
        ///  "personalData": {
        ///    "id": 0,
        ///    "typeid": "string",
        ///    "valueid": "string",
        ///    "nick": "string",
        ///    "name": "string",
        ///    "patherlastname": "string",
        ///    "motherlastname": "string",
        ///    "sex": "string",
        ///    "documentseriesnumber": "string",
        ///    "documentexpirationdate": "2022-09-29T13:08:24.774Z",
        ///    "visatype": "string",
        ///    "birthdate": "2022-09-29T13:08:24.774Z",
        ///    "birthplace": "string",
        ///    "nationality": "string",
        ///    "photography": "string",
        ///    "signatureimage": "string",
        ///    "selfie": "string",
        ///    "docImageFront": "string",
        ///    "docImageBack": "string",
        ///    "profession": "string",
        ///    "dynamicdata": [
        ///      {
        ///        "key": "string",
        ///        "value": "string"
        ///      }
        ///    ],
        ///    "enrollinfo": "string",
        ///    "creation": "2022-09-29T13:08:24.774Z",
        ///    "verificationsource": "string",
        ///    "companyidenroll": 0,
        ///    "useridenroll": 0
        ///  },
        ///  "authenticationfactor": 0,
        ///  "bodypart": 0,
        ///  "minutiaetype": 0,
        ///  "matchingtype": 0,
        ///  "sampleCollection": [
        ///    {
        ///      "data": "string",
        ///      "minutiaetype": 0,
        ///      "additionaldata": "string",
        ///      "device": 0,
        ///      "bodyPart": 0
        ///    }
        ///  ],
        ///  "additionaldata": "string",
        ///  "threshold": 0,
        ///  "companyid": 0,
        ///  "userid": 0,
        ///  "origin": 0,
        ///  "clientid": "string",
        ///  "ipenduser": "string",
        ///  "enduser": "string",
        ///  "verifybyconnectorid": "string",
        ///  "operationOrder": 0,
        ///  "saveVerified": 0,
        ///  "insertOption": 0
        /// }
        /// 
        /// </code>
        /// </remarks>
        /// <param name="ParamIn">Parámetros de ingreso para el proceso</param>
        /// <response code="200">OK. En algunos procesos que ejecuta de forma correcta</response>
        /// <response code="201">Created. En procesos donde se crea una identidad</response>
        /// <response code="302">Found. Cuando encuentra un dato pedido. Especialemtne en Actionid = GET</response>
        /// <response code="400">Bad Request. Indica que faltan parámetros o bien los indicados son incorrectos</response>
        /// <response code="401">Unahutorized. Error con el token presentado</response>
        /// <response code="404">Not Found. NO Encontro ninguna transacción que cumpla con los filtros</response>
        /// <response code="500">Internal Server Error. Error inesperado</response>
        /// <returns></returns>
        [HttpPost]
        [Route("bp/process")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status302Found, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(BSResponse))]
        public async Task<ActionResult<BSResponse>> Process(BpParamIn ParamIn)
        {
            return null;
        }

    }
}
