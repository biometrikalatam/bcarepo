﻿using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Server.Controllers.V1
{
    /// <summary>
    /// Controller para manejo interacción con BPWeb v1.  
    /// </summary>
    /// <remarks>
    /// Fecha: 07-2022
    /// </remarks>
    [ApiController]
    [Route("api/v1")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)] //, Policy = "SuperAdmin,Admin")]
    public class BpInfoController : ControllerBase
    {
        private readonly ApplicationDbContext _DbContext;
        private readonly ILogger<BpInfoController> _logger;
        private readonly IRepositoryBPInfo _RepositoryBPInfo;
        public BpInfoController(ApplicationDbContext _ApplicationDbContext,
                                 ILogger<BpInfoController> logger,
                                 IRepositoryBPInfo repositoryBPInfo)
        {
            _DbContext = _ApplicationDbContext;
            _logger = logger;
            _RepositoryBPInfo = repositoryBPInfo;
        }

        #region Acceso con Token

        /// <summary>
        /// Permite la recuperación de datos relacionados a un typeid/valueid (ej.: rut 11111111-1) o placas patentes.
        /// </summary>
        /// <remarks>
        /// Dado los parámetros de entrada se pueden pedir informes relacionados a un rut son:
        /// <para>* Informe comercial : Score comercial, deudas, etc.</para>
        /// <para>* Informe de contactabilidad : mails, teléfonos, etc</para>
        /// <para>* Bloquedo de Cédula</para>
        /// <para>* Existencia del RUT en SRCeI</para>
        /// <para>* Existencia del RUT en SII</para>
        /// <para>* Estado de Licencia de Conducir en SRCeI</para>
        /// <para>Para placas patentes:</para>
        /// <para>* Infomre Automóviles: Con historial de eventos de seguro, multas, propietarios, etc.</para>
        /// </remarks>
        /// <param name="bpTxCreateDTO">Parámetros de entrada necesarios para operar</param>
        /// <response code="201">OK. Ejecución de recuperación de reporte OK</response>
        /// <response code="400">Bad Request. Indica que faltan parámetros o bien los indicados son incorrectos</response>
        /// <response code="401">Unahutorized. Error con el token presentado</response>
        /// <response code="403">Forbidden. Rol del token presentado sin privilegios para esta operación</response>
        /// <response code="404">Not Found. No se encontro reporte para los parámetros enviados</response>
        /// <response code="500">Internal Server Error. Error inesperado</response>
        /// <returns></returns>
        [HttpPost]
        [Route("info/report")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status403Forbidden, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(BSResponse))]
        public async Task<ActionResult<BSResponse>> TxInfoReport([FromBody] BpTxInfoParamDTO bpTxInfoDTO)
        {
            BSResponse ret = new BSResponse(0, null, null);
            List<BPtxGridDTO> txList = null;
            try
            {
                if (bpTxInfoDTO == null || 
                    (string.IsNullOrEmpty(bpTxInfoDTO.valueid) && string.IsNullOrEmpty(bpTxInfoDTO.licenseplate)) ||
                    (string.IsNullOrEmpty(bpTxInfoDTO.valueid) && string.IsNullOrEmpty(bpTxInfoDTO.name)) ||
                    (bpTxInfoDTO.report < 0 || bpTxInfoDTO.report > 2))
                {
                    ret.code = StatusCodes.Status400BadRequest;
                    ret.message = "Los parámetros de entrada no pueden ser nulos o estar fuera de rango";
                    _logger.LogError("BpInfoController.TxInfoReport - " + ret.code + " - " + ret.message);
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                }

                //Obtengo el User/Company para usar en la creacion de Tx
                var userid = HttpContext.User.Claims.Where(claim => claim.Type == "userid").FirstOrDefault();
                if (userid == null || string.IsNullOrEmpty(userid.Value))
                {
                    ret.code = StatusCodes.Status404NotFound;
                    ret.message = "No pudo recuperar usuario valido!";
                    _logger.LogError("BpInfoController.TxInfoReport - " + ret.code + " - " + ret.message);
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                } else
                {
                    _logger.LogDebug("BpInfoController.TxInfoReport - CreateBpTx IN...");
                    ret = await _RepositoryBPInfo.Report(bpTxInfoDTO, userid.Value);
                }
            }
            catch (Exception ex)
            {
                ret.code = StatusCodes.Status500InternalServerError;
                ret.message = "BpWebController.TxCreate Excp = " + ex.Message;
                ret.response = null;
                _logger.LogError("BpWebController.TxCreate - " + ret.code + " - " + ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }
            _logger.LogDebug("BpWebController.TxCreate OUT! [ret.code = " + ret.code.ToString() + "]");
            return StatusCode(ret.code, ret);
        }

        /// <summary>
        /// Devuelve listado de transacciones realizadas en el servicio, con sus status.
        /// </summary>
        /// <remarks>
        /// Devuelve listado de transacciones realizadas en BioPortal, con su status y detalles de datos relacionados. 
        /// Se debe enviar como parámetro, una lista filtros indicados como pares "key","value". Los valores de filtros posibles son:
        /// <para>* datefrom: fecha desde cuando se desea el listado, con formato dd/MM/yyyy HH:mm:ss. [Ej: 01/09/2021 09:00:00]</para>
        /// <para>* datetill: fecha hasta cuando se desea el listado, con formato dd/MM/yyyy HH:mm:ss. [Ej: 01/09/2021 18:00:00]</para>
        /// <para>* trackid: id único de transacción entregado en el proceso de creación [bpweb/tx/create]</para>
        /// <para>* typeid: valor del tipo de documento por ejemplo RUT</para>
        /// <para>* valueid: valor del número de documento o pasaporte indicado a verificar.</para>
        /// <para>* licenseplate: valor de placa patente para recuperar informe.</para>
        /// <para>Las fechas indicadas, si superan los 3 meses, se ajusta a ese rango, tomando como base datefrom.</para>
        /// <para>Ejemplo de filtros enviados:</para> 
        /// <code>
        ///     
        ///     {
        ///         "filters": {
        ///                         "datefrom": "01/09/2021 09:00:00",
        ///                         "datetill": "01/09/2021 18:00:00",
        ///                         "valueid": "11111111-1"
        ///                    }
        ///     }
        ///     
        /// </code>
        /// </remarks>
        /// <param name="filters">Filtros para búsqueda en formato [key,value]</param>
        /// <response code="302">Found. Encontro una lista de transacciones con los filtros, y la retorna</response>
        /// <response code="400">Bad Request. Indica que faltan parámetros o bien los indicados son incorrectos</response>
        /// <response code="401">Unahutorized. Error con el token presentado</response>
        /// <response code="404">Not Found. NO Encontro ninguna transacción que cumpla con los filtros</response>
        /// <response code="500">Internal Server Error. Error inesperado</response>
        /// <returns></returns>
        [HttpPost]
        [Route("info/get")]
        [ProducesResponseType(StatusCodes.Status302Found, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status403Forbidden, Type = typeof(BSResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BSResponse))]
        public async Task<ActionResult<BSResponse>> GetBpTxInfoByFilters(FilterDTO filters)
        {
            BSResponse ret = new BSResponse(0, null, null);
            //List<RdCitxGridDTO> txList = null;
            try
            {
                if (filters == null)
                {
                    ret.code = 400;
                    ret.message = "Los filtros no pueden ser nulos";
                    _logger.LogError("BpInfoController.GetBpTxInfoByFilters - " + ret.message);
                    return StatusCode(StatusCodes.Status400BadRequest, ret);
                }
                
                _logger.LogDebug("BpInfoController.GetBpTxInfoByFilters IN...");
                ret = await _RepositoryBPInfo.GetBpTxInfoByFilters(filters);//await _DbContext.Country.ToListAsync();
                //ret.response = txList;
            }
            catch (Exception ex)
            {
                ret.code = StatusCodes.Status500InternalServerError;
                ret.message = "BpInfoController.GetBpTxInfoByFilters Excp = " + ex.Message;
                ret.response = null;
                _logger.LogError("BpInfoController.GetBpTxInfoByFilters - " + ret.code + " - " + ret.message);
                return StatusCode(StatusCodes.Status500InternalServerError, ret);
            }
            _logger.LogDebug("BpInfoController.GetBpTxInfoByFilters OUT!");
            return StatusCode(ret.code, ret);
        }


        #endregion Acceso con Token

        #region Acceso sin Token


        #endregion Acceso sin Token
        



     
    }
}
