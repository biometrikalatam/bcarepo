﻿using Biometrika.Solutions.Server.Data;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.Model;
using Biometrika.Solutions.Shared.Model.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Server.Controllers.V1
{
    //[ApiController]
    //[Route("[controller]")]
    //public class CIController : ControllerBase
    //{
    //    private readonly ApplicationDbContext _DbContext;
    //    private readonly ILogger<CountryController> _logger;
    //    private readonly IRepositoryCI _RepositoryCI;
    //    public CIController(ApplicationDbContext _ApplicationDbContext,
    //                             ILogger<CountryController> logger,
    //                             IRepositoryCI repositoryCI)
    //    {
    //        _DbContext = _ApplicationDbContext;
    //        _logger = logger;
    //        _RepositoryCI = repositoryCI;
    //    }

    //    // public async Task<ActionResult<List<RdCitxGridDTO>>> Get(FilterDTO filters)
    //    [HttpPost]
    //    [Route("citxlist")]
    //    public async Task<ActionResult<BSResponse>> Get(FilterDTO filters)
    //    {
    //        BSResponse ret = new BSResponse(0, null, null);
    //        List<RdCitxGridDTO> txList = null;
    //        try
    //        {
    //            _logger.LogDebug("CIController.txlist IN...");
    //            txList = await _RepositoryCI.CItxListToDTOGrid(filters);//await _DbContext.Country.ToListAsync();
    //            ret.response = txList;
    //        }
    //        catch (Exception ex)
    //        {
    //            ret.code = StatusCodes.Status500InternalServerError;
    //            ret.message = "CIController.Get Excp = " + ex.Message;
    //            ret.response = null;
    //            _logger.LogError("CIController.Get.CheckAcces - " + ret.code + " - " + ret.message);
    //            return StatusCode(StatusCodes.Status500InternalServerError, ret);
    //        }
    //        _logger.LogDebug("CIController.Get OUT!");
    //        return StatusCode(StatusCodes.Status200OK, ret); ;
    //    }
    //}
}
