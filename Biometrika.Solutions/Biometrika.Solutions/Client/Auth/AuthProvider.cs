﻿using Microsoft.AspNetCore.Components.Authorization;
using System.Security.Claims;

namespace Biometrika.Solutions.Client.Auth
{
    public class AuthProvider : AuthenticationStateProvider
    {
        public async override Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var anonimo = new ClaimsIdentity(new List<Claim>() {
                new Claim("llave1", "valor1"),
                new Claim(ClaimTypes.Name, "Felipe")
                //new Claim(ClaimTypes.Role, "admin")
            });
            return await Task.FromResult(new AuthenticationState(new ClaimsPrincipal(anonimo)));
        }
    }
}
