﻿using Biometrika.Solutions.Client.Services.Interfaces;
using Biometrika.Solutions.Client;
using Biometrika.Solutions.Client.Auth;
using Biometrika.Solutions.Client.Helpers;
using Biometrika.Solutions.Client.Repositorios;
using Biometrika.Solutions.Client.Services;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Radzen;
using MudBlazor.Services;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");


builder.Services.AddHttpClient<HttpClientConToken>(
              cliente => cliente.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress))
              .AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>();

builder.Services.AddHttpClient<HttpClientSinToken>(
   cliente => cliente.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress));

builder.Services.AddHttpClient("Biometrika.Solutions.ServerAPI",
    client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress))
    .AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>();

// Supply HttpClient instances that include access tokens when making requests to the server project
builder.Services.AddScoped(sp => sp.GetRequiredService<IHttpClientFactory>().CreateClient("Biometrika.Solutions.ServerAPI"));

builder.Services.AddApiAuthorization();

//Service Biometrika
builder.Services.AddOptions();
builder.Services.AddScoped<IRepositorio, Repositorio>();
builder.Services.AddScoped<IMostrarMensajes, MostrarMensajes>();
builder.Services.AddScoped<IServiceCI, ServiceCI>();
builder.Services.AddScoped<IServiceBP, ServiceBP>();

//builder.Services.AddScoped<AuthenticationStateProvider, ProveedorAutenticacionJWT>();

//Radzen
builder.Services.AddScoped<ContextMenuService>();
builder.Services.AddScoped<DialogService>();
builder.Services.AddScoped<NotificationService>();
builder.Services.AddScoped<TooltipService>();

//MudBlazor
builder.Services.AddMudServices();

//Dictionary<string,string> d = new Dictionary<string, string>();
//d.Add("k1", "v1");
//string s = JsonConvert.



await builder.Build().RunAsync();

