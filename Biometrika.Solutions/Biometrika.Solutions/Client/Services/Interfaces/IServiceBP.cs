﻿using Biometrika.Solutions.Client.Services;
using Biometrika.Solutions.Shared;
using Biometrika.Solutions.Shared.DTOs;

namespace Biometrika.Solutions.Client.Services.Interfaces
{
    public interface IServiceBP
    {
        Task<ResponseDTO> BpTxGetList(FilterDTO filters);

    }
}
