﻿namespace Biometrika.Solutions.Client.Services
{
    public class ResponseDTO
    {

        public ResponseDTO(int code, string message, object response)
        {
            Code = code;
            Message = message;
            Response = response;    
        }

        public int Code { get; set; }
        public string Message { get; set; }
        public object Response { get; set; }
    }
}
