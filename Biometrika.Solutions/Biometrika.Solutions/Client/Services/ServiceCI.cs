﻿using Biometrika.Solutions.Client.Services.Interfaces;
using Biometrika.Solutions.Client.Repositorios;
using Biometrika.Solutions.Shared.DTOs;
using Newtonsoft.Json;

namespace Biometrika.Solutions.Client.Services
{
    public class ServiceCI : IServiceCI
    {

        private readonly IRepositorio _Repositorio;

        public ServiceCI(IRepositorio Repositorio)
        {
            _Repositorio = Repositorio;
        }
        public async Task<ResponseDTO> CIGetList(FilterDTO filters)
        {
            ResponseDTO response = new ResponseDTO(0, null, null);
            try
            {
                var responseHttp1 = await _Repositorio.Post<FilterDTO, BSResponse>("CI/citxlist", filters);
                if (responseHttp1 != null && responseHttp1.Response != null && responseHttp1.Response.response != null)
                {
                    response.Response = JsonConvert.DeserializeObject<List<RdCitxGridDTO>>(responseHttp1.Response.response.ToString());
                }
            }
            catch (Exception ex)
            {
                response = new ResponseDTO(-1, "Error recuperando lista de transacciones [" + ex.Message + "]", null);
            }
            return response;
        }
    }
}
