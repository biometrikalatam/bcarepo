﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("de_document")]
    public partial class DeDocument
    {
        public DeDocument()
        {
            DeDocumentTags = new HashSet<DeDocumentTag>();
            DeSignatories = new HashSet<DeSignatory>();
            DeTxes = new HashSet<DeTx>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("trackid")]
        [StringLength(50)]
        [Unicode(false)]
        public string Trackid { get; set; } = null!;
        [Column("companyid")]
        public int? Companyid { get; set; }
        [Column("detemplateid")]
        public int? Detemplateid { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("extension")]
        [StringLength(10)]
        [Unicode(false)]
        public string? Extension { get; set; }
        [Column("createdate", TypeName = "datetime")]
        public DateTime Createdate { get; set; }
        [Column("expirationdate", TypeName = "datetime")]
        public DateTime? Expirationdate { get; set; }
        [Column("enddate", TypeName = "datetime")]
        public DateTime? Enddate { get; set; }
        [Column("userid")]
        public int? Userid { get; set; }
        [Column("bytedocument", TypeName = "text")]
        public string? Bytedocument { get; set; }
        [Column("attachment", TypeName = "text")]
        public string? Attachment { get; set; }
        [Column("receipt", TypeName = "text")]
        public string? Receipt { get; set; }
        [Column("mailtonotify")]
        [StringLength(250)]
        [Unicode(false)]
        public string? Mailtonotify { get; set; }
        [Column("carregisterimagefront", TypeName = "text")]
        public string? Carregisterimagefront { get; set; }
        [Column("carregisterimageback", TypeName = "text")]
        public string? Carregisterimageback { get; set; }
        [Column("writingimage", TypeName = "text")]
        public string? Writingimage { get; set; }
        [Column("form", TypeName = "text")]
        public string? Form { get; set; }
        [Column("securitycode")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Securitycode { get; set; }
        [Column("dinamicparam", TypeName = "text")]
        public string? Dinamicparam { get; set; }
        [Column("callbackurl")]
        [StringLength(1024)]
        [Unicode(false)]
        public string? Callbackurl { get; set; }
        [Column("qrverify", TypeName = "text")]
        public string? Qrverify { get; set; }
        [Column("resume", TypeName = "text")]
        public string? Resume { get; set; }

        [ForeignKey("Companyid")]
        [InverseProperty("DeDocuments")]
        public virtual Company? Company { get; set; }
        [ForeignKey("Detemplateid")]
        [InverseProperty("DeDocuments")]
        public virtual DeTemplate? Detemplate { get; set; }
        [InverseProperty("Document")]
        public virtual ICollection<DeDocumentTag> DeDocumentTags { get; set; }
        [InverseProperty("Document")]
        public virtual ICollection<DeSignatory> DeSignatories { get; set; }
        [InverseProperty("Document")]
        public virtual ICollection<DeTx> DeTxes { get; set; }
    }
}
