﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("md_tipodocumentos")]
    public partial class MdTipodocumento
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("descripcion")]
        [StringLength(25)]
        [Unicode(false)]
        public string Descripcion { get; set; } = null!;
    }
}
