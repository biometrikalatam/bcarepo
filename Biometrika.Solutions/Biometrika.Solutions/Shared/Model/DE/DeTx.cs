﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("de_tx")]
    public partial class DeTx
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("documentid")]
        public int Documentid { get; set; }
        [Column("type")]
        public int Type { get; set; }
        [Column("actionid")]
        public int Actionid { get; set; }
        [Column("receipt", TypeName = "text")]
        public string? Receipt { get; set; }
        [Column("trackidbp")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Trackidbp { get; set; }
        [Column("trackidde")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Trackidde { get; set; }
        [Column("dategen", TypeName = "datetime")]
        public DateTime? Dategen { get; set; }
        [Column("resultcode")]
        public int Resultcode { get; set; }
        [Column("qrgenerated", TypeName = "text")]
        public string? Qrgenerated { get; set; }
        [Column("companyid")]
        public int Companyid { get; set; }
        [Column("lastmodify", TypeName = "datetime")]
        public DateTime? Lastmodify { get; set; }
        [Column("typeid")]
        [StringLength(10)]
        [Unicode(false)]
        public string? Typeid { get; set; }
        [Column("valueid")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Valueid { get; set; }
        [Column("score")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Score { get; set; }
        [Column("threshold")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Threshold { get; set; }
        [Column("username")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Username { get; set; }

        [ForeignKey("Documentid")]
        [InverseProperty("DeTxes")]
        public virtual DeDocument Document { get; set; } = null!;
    }
}
