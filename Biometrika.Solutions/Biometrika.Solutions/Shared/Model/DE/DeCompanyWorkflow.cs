﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("de_company_workflow")]
    public partial class DeCompanyWorkflow
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("companyid")]
        public int Companyid { get; set; }
        [Column("name")]
        [StringLength(100)]
        [Unicode(false)]
        public string Name { get; set; } = null!;
        [Column("onboardingmandatory")]
        public int Onboardingmandatory { get; set; }
        [Column("bpwebtypeonboarding")]
        [StringLength(20)]
        [Unicode(false)]
        public string Bpwebtypeonboarding { get; set; } = null!;
        [Column("bpwebtypeverify")]
        [StringLength(20)]
        [Unicode(false)]
        public string Bpwebtypeverify { get; set; } = null!;
        [Column("receiverlist")]
        [StringLength(1024)]
        [Unicode(false)]
        public string? Receiverlist { get; set; }
        [Column("threshold")]
        public double? Threshold { get; set; }
        [Column("datecreate", TypeName = "datetime")]
        public DateTime Datecreate { get; set; }
        [Column("dateupdate", TypeName = "datetime")]
        public DateTime Dateupdate { get; set; }
        [Column("autorization")]
        public int Autorization { get; set; }
        [Column("autorizationmessage", TypeName = "text")]
        public string? Autorizationmessage { get; set; }
        [Column("signerinclude")]
        public int Signerinclude { get; set; }
        [Column("checklocksrcei")]
        public int Checklocksrcei { get; set; }
        [Column("videoinclude")]
        public int Videoinclude { get; set; }
        [Column("videomessage", TypeName = "text")]
        public string? Videomessage { get; set; }
        [Column("theme")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Theme { get; set; }
        [Column("checkadult")]
        public int Checkadult { get; set; }
        [Column("checkexpired")]
        public int Checkexpired { get; set; }
        [Column("georefmandatory")]
        public int Georefmandatory { get; set; }
        [Column("redirecturl")]
        [StringLength(250)]
        [Unicode(false)]
        public string? Redirecturl { get; set; }
        [Column("successurl")]
        [StringLength(250)]
        [Unicode(false)]
        public string? Successurl { get; set; }
        [Column("carregisterinclude")]
        public int Carregisterinclude { get; set; }
        [Column("writinginclude")]
        public int Writinginclude { get; set; }
        [Column("forminclude")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Forminclude { get; set; }

        [ForeignKey("Companyid")]
        [InverseProperty("DeCompanyWorkflows")]
        public virtual Company Company { get; set; } = null!;
    }
}
