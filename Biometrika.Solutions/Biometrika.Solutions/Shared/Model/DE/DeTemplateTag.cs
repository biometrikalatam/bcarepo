﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("de_template_tag")]
    public partial class DeTemplateTag
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("templateid")]
        public int Templateid { get; set; }
        [Column("name")]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; } = null!;
        [Column("type")]
        public int Type { get; set; }
        [Column("parameters")]
        [StringLength(512)]
        [Unicode(false)]
        public string? Parameters { get; set; }
        [Column("translate")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Translate { get; set; }
        [Column("format")]
        [StringLength(128)]
        [Unicode(false)]
        public string? Format { get; set; }
        /// <summary>
        /// detx | dedocument | designatory | designatoryevidences | form | custom
        /// </summary>
        [Column("source")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Source { get; set; }
        [Column("signatoryorder")]
        public int Signatoryorder { get; set; }

        [ForeignKey("Templateid")]
        [InverseProperty("DeTemplateTags")]
        public virtual DeTemplate Template { get; set; } = null!;
    }
}
