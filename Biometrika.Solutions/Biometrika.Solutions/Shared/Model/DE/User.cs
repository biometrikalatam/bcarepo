﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("User")]
    public partial class User
    {
        [Key]
        public int Id { get; set; }
        [Column("username")]
        [StringLength(50)]
        public string Username { get; set; } = null!;
        [Column("email")]
        [StringLength(100)]
        public string Email { get; set; } = null!;
        [Column("password")]
        [StringLength(256)]
        public string Password { get; set; } = null!;
        [Column("passwordSalt")]
        [StringLength(64)]
        public string? PasswordSalt { get; set; }
        [Column("passwordFormat")]
        public int PasswordFormat { get; set; }
        [Column("passwordQuestion")]
        [StringLength(512)]
        public string PasswordQuestion { get; set; } = null!;
        [Column("passwordAnswer")]
        [StringLength(512)]
        public string PasswordAnswer { get; set; } = null!;
        [Column("failedPasswordAttemptCount")]
        public int FailedPasswordAttemptCount { get; set; }
        [Column("failedPasswordAttemptWindowStart", TypeName = "smalldatetime")]
        public DateTime? FailedPasswordAttemptWindowStart { get; set; }
        [Column("failedPasswordAnswerAttemptCount")]
        public int FailedPasswordAnswerAttemptCount { get; set; }
        [Column("failedPasswordAnswerAttemptWindowStart", TypeName = "smalldatetime")]
        public DateTime? FailedPasswordAnswerAttemptWindowStart { get; set; }
        [Column("lastPasswordChangedDate", TypeName = "smalldatetime")]
        public DateTime? LastPasswordChangedDate { get; set; }
        [Column("createDate", TypeName = "smalldatetime")]
        public DateTime CreateDate { get; set; }
        [Column("lastActivityDate", TypeName = "smalldatetime")]
        public DateTime? LastActivityDate { get; set; }
        [Column("isApproved")]
        public bool IsApproved { get; set; }
        [Column("isLockedOut")]
        public bool IsLockedOut { get; set; }
        [Column("lastLockOutDate", TypeName = "smalldatetime")]
        public DateTime? LastLockOutDate { get; set; }
        [Column("lastLoginDate", TypeName = "smalldatetime")]
        public DateTime? LastLoginDate { get; set; }
        [Column("comments")]
        public string? Comments { get; set; }
        [Column("name")]
        [StringLength(256)]
        public string? Name { get; set; }
        [Column("surname")]
        [StringLength(256)]
        public string? Surname { get; set; }
        [Column("phone")]
        [StringLength(16)]
        public string? Phone { get; set; }
        [Column("postalCode")]
        [StringLength(50)]
        public string? PostalCode { get; set; }
        [Column("updateDate", TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
        [Column("employee")]
        public int? Employee { get; set; }
    }
}
