﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("de_document_tag")]
    public partial class DeDocumentTag
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; } = null!;
        [Column("value", TypeName = "text")]
        public string? Value { get; set; }
        [Column("type")]
        public int? Type { get; set; }
        [Column("documentid")]
        public int Documentid { get; set; }
        [Column("templatetagid")]
        public int Templatetagid { get; set; }
        [Column("createdate", TypeName = "datetime")]
        public DateTime Createdate { get; set; }

        [ForeignKey("Documentid")]
        [InverseProperty("DeDocumentTags")]
        public virtual DeDocument Document { get; set; } = null!;
    }
}
