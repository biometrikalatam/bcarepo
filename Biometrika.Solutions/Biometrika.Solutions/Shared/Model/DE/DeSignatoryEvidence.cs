﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("de_signatory_evidences")]
    public partial class DeSignatoryEvidence
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("signatoreid")]
        public int Signatoreid { get; set; }
        [Column("name")]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; } = null!;
        [Column("value", TypeName = "text")]
        public string Value { get; set; } = null!;
        [Column("createdate", TypeName = "datetime")]
        public DateTime? Createdate { get; set; }
        [Column("typeimage")]
        public int Typeimage { get; set; }
        [Column("typeimageformat")]
        public int Typeimageformat { get; set; }

        [ForeignKey("Signatoreid")]
        [InverseProperty("DeSignatoryEvidences")]
        public virtual DeSignatory Signatore { get; set; } = null!;
    }
}
