﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("de_template")]
    public partial class DeTemplate
    {
        public DeTemplate()
        {
            DeDocuments = new HashSet<DeDocument>();
            DeTemplateSignatories = new HashSet<DeTemplateSignatory>();
            DeTemplateTags = new HashSet<DeTemplateTag>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("companyid")]
        public int Companyid { get; set; }
        [Column("name")]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; } = null!;
        [Column("description")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Description { get; set; }
        [Column("version")]
        [StringLength(10)]
        public string? Version { get; set; }
        [Column("bytetemplate", TypeName = "text")]
        public string? Bytetemplate { get; set; }
        [Column("createdate", TypeName = "datetime")]
        public DateTime? Createdate { get; set; }
        /// <summary>
        /// 0-Sin Firma - 1-FES Empresa - 2-FES PArticular - 3-FEACloud
        /// </summary>
        [Column("singnaturatype")]
        public int? Singnaturatype { get; set; }
        [Column("certificatepfx", TypeName = "text")]
        public string? Certificatepfx { get; set; }
        [Column("certificatepfxpassword")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Certificatepfxpassword { get; set; }
        [Column("certificatecer", TypeName = "text")]
        public string? Certificatecer { get; set; }
        [Column("imagesignaturetoshow", TypeName = "text")]
        public string? Imagesignaturetoshow { get; set; }
        [Column("fesshowtype")]
        public int? Fesshowtype { get; set; }
        [Column("fesx")]
        [StringLength(10)]
        public string? Fesx { get; set; }
        [Column("fesy")]
        [StringLength(10)]
        public string? Fesy { get; set; }
        [Column("fesw")]
        [StringLength(10)]
        public string? Fesw { get; set; }
        [Column("fesh")]
        [StringLength(10)]
        public string? Fesh { get; set; }
        [Column("fesmsgtoshow")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Fesmsgtoshow { get; set; }
        [Column("feaurl")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Feaurl { get; set; }
        [Column("feaapp")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Feaapp { get; set; }
        [Column("feauser")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Feauser { get; set; }
        [Column("listnotifymails")]
        [StringLength(250)]
        [Unicode(false)]
        public string? Listnotifymails { get; set; }
        [Column("mailtemplate", TypeName = "text")]
        public string? Mailtemplate { get; set; }
        [Column("assemblypostgenerate")]
        [StringLength(250)]
        [Unicode(false)]
        public string? Assemblypostgenerate { get; set; }
        [Column("resumedetemplateid")]
        public int? Resumedetemplateid { get; set; }

        [ForeignKey("Companyid")]
        [InverseProperty("DeTemplates")]
        public virtual Company Company { get; set; } = null!;
        [InverseProperty("Detemplate")]
        public virtual ICollection<DeDocument> DeDocuments { get; set; }
        [InverseProperty("Template")]
        public virtual ICollection<DeTemplateSignatory> DeTemplateSignatories { get; set; }
        [InverseProperty("Template")]
        public virtual ICollection<DeTemplateTag> DeTemplateTags { get; set; }
    }
}
