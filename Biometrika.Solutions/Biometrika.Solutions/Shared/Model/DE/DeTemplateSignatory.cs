﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("de_template_signatory")]
    public partial class DeTemplateSignatory
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("templateid")]
        public int Templateid { get; set; }
        [Column("signatoryorder")]
        public int Signatoryorder { get; set; }
        /// <summary>
        /// 0-SimpleBPWeb - 1-FES - 2-FEA - 3-FEACloud
        /// </summary>
        [Column("signaturetype")]
        public int Signaturetype { get; set; }
        [Column("workflowid")]
        public int Workflowid { get; set; }

        [ForeignKey("Templateid")]
        [InverseProperty("DeTemplateSignatories")]
        public virtual DeTemplate Template { get; set; } = null!;
    }
}
