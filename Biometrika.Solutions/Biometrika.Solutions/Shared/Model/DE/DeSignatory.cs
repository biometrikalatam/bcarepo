﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("de_signatory")]
    public partial class DeSignatory
    {
        public DeSignatory()
        {
            DeSignatoryEvidences = new HashSet<DeSignatoryEvidence>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("signatoryorder")]
        public int Signatoryorder { get; set; }
        [Column("createdate", TypeName = "datetime")]
        public DateTime? Createdate { get; set; }
        [Column("expirationdatesignature", TypeName = "datetime")]
        public DateTime? Expirationdatesignature { get; set; }
        [Column("lastmodify", TypeName = "datetime")]
        public DateTime? Lastmodify { get; set; }
        [Column("typeid")]
        [StringLength(10)]
        [Unicode(false)]
        public string? Typeid { get; set; }
        [Column("valueid")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Valueid { get; set; }
        [Column("name")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Name { get; set; }
        [Column("lastname")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Lastname { get; set; }
        [Column("sex")]
        [StringLength(1)]
        [Unicode(false)]
        public string? Sex { get; set; }
        [Column("birthdate", TypeName = "datetime")]
        public DateTime? Birthdate { get; set; }
        [Column("issuedate", TypeName = "datetime")]
        public DateTime? Issuedate { get; set; }
        [Column("serial")]
        [StringLength(25)]
        [Unicode(false)]
        public string? Serial { get; set; }
        [Column("expirationdate", TypeName = "datetime")]
        public DateTime? Expirationdate { get; set; }
        [Column("nacionality")]
        [StringLength(25)]
        [Unicode(false)]
        public string? Nacionality { get; set; }
        [Column("mail")]
        [StringLength(75)]
        [Unicode(false)]
        public string Mail { get; set; } = null!;
        [Column("phone")]
        [StringLength(20)]
        [Unicode(false)]
        public string? Phone { get; set; }
        [Column("documentid")]
        public int Documentid { get; set; }
        [Column("workstationid")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Workstationid { get; set; }
        [Column("georef")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Georef { get; set; }
        [Column("map", TypeName = "text")]
        public string? Map { get; set; }
        [Column("videourl")]
        [StringLength(512)]
        [Unicode(false)]
        public string? Videourl { get; set; }
        [Column("urlbpweb")]
        [StringLength(1024)]
        [Unicode(false)]
        public string? Urlbpweb { get; set; }
        [Column("trackidbp")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Trackidbp { get; set; }
        [Column("taxidcompany")]
        [StringLength(20)]
        [Unicode(false)]
        public string? Taxidcompany { get; set; }
        [Column("workflowid")]
        public int Workflowid { get; set; }
        [Column("score")]
        public double? Score { get; set; }
        [Column("threshold")]
        public double? Threshold { get; set; }
        /// <summary>
        /// 1-Positivo | 2-Negativo
        /// </summary>
        [Column("verifyresult")]
        [StringLength(20)]
        [Unicode(false)]
        public string? Verifyresult { get; set; }

        [ForeignKey("Documentid")]
        [InverseProperty("DeSignatories")]
        public virtual DeDocument Document { get; set; } = null!;
        [InverseProperty("Signatore")]
        public virtual ICollection<DeSignatoryEvidence> DeSignatoryEvidences { get; set; }
    }
}
