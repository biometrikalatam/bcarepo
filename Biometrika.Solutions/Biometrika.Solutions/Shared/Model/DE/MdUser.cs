﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("md_users")]
    public partial class MdUser
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("nombre")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Nombre { get; set; }
        [Column("apellidopaterno")]
        [StringLength(50)]
        [Unicode(false)]
        public string Apellidopaterno { get; set; } = null!;
        [Column("apellidomaterno")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Apellidomaterno { get; set; }
        [Column("rut")]
        [StringLength(50)]
        [Unicode(false)]
        public string Rut { get; set; } = null!;
        [Column("companyid")]
        public int? Companyid { get; set; }
        [Column("rol")]
        public int Rol { get; set; }
        [Column("activo", TypeName = "datetime")]
        public DateTime? Activo { get; set; }

        [ForeignKey("Companyid")]
        [InverseProperty("MdUsers")]
        public virtual MdCliente? Company { get; set; }
    }
}
