﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("Company")]
    public partial class Company
    {
        public Company()
        {
            DeCompanyWorkflows = new HashSet<DeCompanyWorkflow>();
            DeDocuments = new HashSet<DeDocument>();
            DeTemplates = new HashSet<DeTemplate>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("rut")]
        [StringLength(20)]
        [Unicode(false)]
        public string Rut { get; set; } = null!;
        [Column("address")]
        [StringLength(80)]
        [Unicode(false)]
        public string Address { get; set; } = null!;
        [Column("name")]
        [StringLength(100)]
        [Unicode(false)]
        public string Name { get; set; } = null!;
        [Column("phone")]
        [StringLength(20)]
        [Unicode(false)]
        public string Phone { get; set; } = null!;
        [Column("phone2")]
        [StringLength(20)]
        [Unicode(false)]
        public string? Phone2 { get; set; }
        [Column("fax")]
        [StringLength(20)]
        [Unicode(false)]
        public string? Fax { get; set; }
        [Column("createDate", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("endate", TypeName = "datetime")]
        public DateTime? Endate { get; set; }
        [Column("updateDate", TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
        [Column("domain")]
        [StringLength(15)]
        [Unicode(false)]
        public string Domain { get; set; } = null!;
        [Column("additionaldata", TypeName = "xml")]
        public string? Additionaldata { get; set; }
        [Column("contactname")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Contactname { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("holding")]
        public int? Holding { get; set; }
        [Column("accessname")]
        [StringLength(50)]
        [Unicode(false)]
        public string Accessname { get; set; } = null!;
        [Column("secretkey")]
        [StringLength(50)]
        [Unicode(false)]
        public string Secretkey { get; set; } = null!;
        [Column("companyidbp")]
        public int Companyidbp { get; set; }
        [Column("accessnamebp")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Accessnamebp { get; set; }
        [Column("secretkeybp")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Secretkeybp { get; set; }
        [Column("companyidnv")]
        public int Companyidnv { get; set; }
        [Column("accessnamenv")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Accessnamenv { get; set; }
        [Column("secretkeynv")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Secretkeynv { get; set; }
        [Column("mailtemplate", TypeName = "text")]
        public string? Mailtemplate { get; set; }

        [InverseProperty("Company")]
        public virtual ICollection<DeCompanyWorkflow> DeCompanyWorkflows { get; set; }
        [InverseProperty("Company")]
        public virtual ICollection<DeDocument> DeDocuments { get; set; }
        [InverseProperty("Company")]
        public virtual ICollection<DeTemplate> DeTemplates { get; set; }
    }
}
