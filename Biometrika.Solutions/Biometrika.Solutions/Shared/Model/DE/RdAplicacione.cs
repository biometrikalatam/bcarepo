﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("rd_aplicaciones")]
    public partial class RdAplicacione
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("nombre")]
        [StringLength(50)]
        [Unicode(false)]
        public string Nombre { get; set; } = null!;
        [Column("code")]
        [StringLength(20)]
        [Unicode(false)]
        public string Code { get; set; } = null!;
        [Column("qextensiones")]
        public int Qextensiones { get; set; }
        /// <summary>
        /// Indica si se chequean extensiones con tipo en generacion [0-Segun Programacion|1-Siempre|2-Nunca]
        /// </summary>
        [Column("checkingen")]
        public int Checkingen { get; set; }
        [Column("company")]
        public int Company { get; set; }
        [Column("signaturetype")]
        public int Signaturetype { get; set; }
        [Column("certificatepxf", TypeName = "text")]
        public string? Certificatepxf { get; set; }
        [Column("signaturepsw")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Signaturepsw { get; set; }
        [Column("certificatecer", TypeName = "text")]
        public string? Certificatecer { get; set; }
        [Column("urlfea")]
        [StringLength(150)]
        [Unicode(false)]
        public string? Urlfea { get; set; }
        [Column("appfea")]
        [StringLength(150)]
        [Unicode(false)]
        public string? Appfea { get; set; }
        [Column("userfea")]
        [StringLength(150)]
        [Unicode(false)]
        public string? Userfea { get; set; }
        [Column("deleted", TypeName = "datetime")]
        public DateTime? Deleted { get; set; }
        [Column("urlcallback")]
        [StringLength(250)]
        [Unicode(false)]
        public string? Urlcallback { get; set; }
        [Column("sendmailresult")]
        public int? Sendmailresult { get; set; }
    }
}
