﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.DE
{
    [Table("md_clientes")]
    public partial class MdCliente
    {
        public MdCliente()
        {
            MdUsers = new HashSet<MdUser>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("razonsocial")]
        [StringLength(50)]
        [Unicode(false)]
        public string Razonsocial { get; set; } = null!;
        [Column("rut")]
        [StringLength(10)]
        [Unicode(false)]
        public string Rut { get; set; } = null!;
        [Column("representante")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Representante { get; set; }
        [Column("rutrepresentante")]
        [StringLength(10)]
        [Unicode(false)]
        public string? Rutrepresentante { get; set; }
        [Column("direccion")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Direccion { get; set; }
        [Column("comuna")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Comuna { get; set; }
        [Column("region")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Region { get; set; }
        [Column("pais")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Pais { get; set; }
        [Column("disabled", TypeName = "datetime")]
        public DateTime? Disabled { get; set; }

        [InverseProperty("Company")]
        public virtual ICollection<MdUser> MdUsers { get; set; }
    }
}
