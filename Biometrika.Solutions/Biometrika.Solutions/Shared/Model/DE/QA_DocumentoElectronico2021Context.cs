﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Biometrika.Solutions.Shared.Model.DE
{
    public partial class QA_DocumentoElectronico2021Context : DbContext
    {
        public QA_DocumentoElectronico2021Context()
        {
        }

        public QA_DocumentoElectronico2021Context(DbContextOptions<QA_DocumentoElectronico2021Context> options)
            : base(options)
        {
        }

        public virtual DbSet<BpCompany> BpCompanies { get; set; } = null!;
        public virtual DbSet<Company> Companies { get; set; } = null!;
        public virtual DbSet<DeCompanyWorkflow> DeCompanyWorkflows { get; set; } = null!;
        public virtual DbSet<DeDocument> DeDocuments { get; set; } = null!;
        public virtual DbSet<DeDocumentTag> DeDocumentTags { get; set; } = null!;
        public virtual DbSet<DeSignatory> DeSignatories { get; set; } = null!;
        public virtual DbSet<DeSignatoryEvidence> DeSignatoryEvidences { get; set; } = null!;
        public virtual DbSet<DeTemplate> DeTemplates { get; set; } = null!;
        public virtual DbSet<DeTemplateSignatory> DeTemplateSignatories { get; set; } = null!;
        public virtual DbSet<DeTemplateTag> DeTemplateTags { get; set; } = null!;
        public virtual DbSet<DeTx> DeTxes { get; set; } = null!;
        public virtual DbSet<MdCliente> MdClientes { get; set; } = null!;
        public virtual DbSet<MdTipodocumento> MdTipodocumentos { get; set; } = null!;
        public virtual DbSet<MdUser> MdUsers { get; set; } = null!;
        public virtual DbSet<RdAplicacione> RdAplicaciones { get; set; } = null!;
        public virtual DbSet<Rol> Rols { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;
        public virtual DbSet<UserRol> UserRols { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("name=DefaultConnection");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DeCompanyWorkflow>(entity =>
            {
                entity.Property(e => e.Bpwebtypeonboarding).HasDefaultValueSql("('tv3d')");

                entity.Property(e => e.Bpwebtypeverify).HasDefaultValueSql("('tv3d')");

                entity.Property(e => e.Datecreate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Dateupdate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Theme).HasDefaultValueSql("('nv')");

                entity.Property(e => e.Threshold).HasDefaultValueSql("((50))");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.DeCompanyWorkflows)
                    .HasForeignKey(d => d.Companyid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_de_company_workflow_Company");
            });

            modelBuilder.Entity<DeDocument>(entity =>
            {
                entity.Property(e => e.Extension).HasDefaultValueSql("('ext')");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.DeDocuments)
                    .HasForeignKey(d => d.Companyid)
                    .HasConstraintName("FK_de_document_Company");

                entity.HasOne(d => d.Detemplate)
                    .WithMany(p => p.DeDocuments)
                    .HasForeignKey(d => d.Detemplateid)
                    .HasConstraintName("FK_de_document_de_template");
            });

            modelBuilder.Entity<DeDocumentTag>(entity =>
            {
                entity.Property(e => e.Type).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Document)
                    .WithMany(p => p.DeDocumentTags)
                    .HasForeignKey(d => d.Documentid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_de_document_tag_de_document");
            });

            modelBuilder.Entity<DeSignatory>(entity =>
            {
                entity.Property(e => e.Verifyresult)
                    .HasDefaultValueSql("('Negativo')")
                    .HasComment("1-Positivo | 2-Negativo");

                entity.HasOne(d => d.Document)
                    .WithMany(p => p.DeSignatories)
                    .HasForeignKey(d => d.Documentid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_de_signatory_de_document");
            });

            modelBuilder.Entity<DeSignatoryEvidence>(entity =>
            {
                entity.Property(e => e.Createdate).HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Signatore)
                    .WithMany(p => p.DeSignatoryEvidences)
                    .HasForeignKey(d => d.Signatoreid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_de_signatore_evidences_de_signatory");
            });

            modelBuilder.Entity<DeTemplate>(entity =>
            {
                entity.Property(e => e.Fesh).IsFixedLength();

                entity.Property(e => e.Fesw).IsFixedLength();

                entity.Property(e => e.Fesx).IsFixedLength();

                entity.Property(e => e.Fesy).IsFixedLength();

                entity.Property(e => e.Singnaturatype).HasComment("0-Sin Firma - 1-FES Empresa - 2-FES PArticular - 3-FEACloud");

                entity.Property(e => e.Version).IsFixedLength();

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.DeTemplates)
                    .HasForeignKey(d => d.Companyid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_de_template_Company");
            });

            modelBuilder.Entity<DeTemplateSignatory>(entity =>
            {
                entity.Property(e => e.Signaturetype).HasComment("0-SimpleBPWeb - 1-FES - 2-FEA - 3-FEACloud");

                entity.HasOne(d => d.Template)
                    .WithMany(p => p.DeTemplateSignatories)
                    .HasForeignKey(d => d.Templateid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_de_template_signatory_de_template");
            });

            modelBuilder.Entity<DeTemplateTag>(entity =>
            {
                entity.Property(e => e.Source).HasComment("detx | dedocument | designatory | designatoryevidences | form | custom");

                entity.HasOne(d => d.Template)
                    .WithMany(p => p.DeTemplateTags)
                    .HasForeignKey(d => d.Templateid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_de_template_tag_de_template");
            });

            modelBuilder.Entity<DeTx>(entity =>
            {
                entity.Property(e => e.Dategen).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Lastmodify).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Resultcode).HasDefaultValueSql("((2))");

                entity.HasOne(d => d.Document)
                    .WithMany(p => p.DeTxes)
                    .HasForeignKey(d => d.Documentid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_de_tx_de_document");
            });

            modelBuilder.Entity<MdUser>(entity =>
            {
                entity.HasOne(d => d.Company)
                    .WithMany(p => p.MdUsers)
                    .HasForeignKey(d => d.Companyid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_md_users_md_clientes");
            });

            modelBuilder.Entity<RdAplicacione>(entity =>
            {
                entity.Property(e => e.Checkingen).HasComment("Indica si se chequean extensiones con tipo en generacion [0-Segun Programacion|1-Siempre|2-Nunca]");

                entity.Property(e => e.Qextensiones).HasDefaultValueSql("((1))");

                entity.Property(e => e.Sendmailresult).HasDefaultValueSql("((0))");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
