﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace Biometrika.Solutions.Shared.Model
{ 
    public class ApplicationUser : IdentityUser
    {
        [Required]
        public int CompanyId { get; set; }
        public Company Company { get; set; }


    }
}