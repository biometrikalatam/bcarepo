﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.Model
{
    internal class Enums
    {
    }

    /*
            verification_started	
            Sent at the beginning of the SDKs flow, when MetaMap is making a new verification record (usually at the upload of the first ID document)

            verification_inputs_completed	
            Sent when the user has uploaded all inputs via SDKs. You can use that webhook to know when to redirect the user after the metamap is complete on your website/App.

            verification_updated	
            Sent from your MetaMap dashboard manually after updating the user verification information.

            verification_postponed	
            When gov check service is postponed and awaiting until step timeout error according the established databases request timeout.

            verification_completed	
            Sent once MetaMap is done verifying a user entirely. When you get this webhook, you should GET the 'resource' URL to get the verification data about user.

            verification_signed	
            Based by base64NOMTimeStamp and is triggered only after verification_completed when digitalSignature service is finished processing.

            verification_expired	
            Sent when verification is not completed after 30 minutes. It means that the user probably did not finish the metamap.

            step_completed	
            webhook sent after each verification step is completed (liveness, face match, document-reading, alteration-detection, template-matching, watchlist)
        */
    public enum TxStatus
    {
        Created = 0,          //Cuando se crea la Tx
        Initialized = 1,      //Inicializado el proceso de captura (cuando llama desde el link)
        Started = 2,          //Cuando se comenzo a toamr las muestras o se inicio el metamapa
        //Inputs_Completed = 3, //Cuando se completo la carga de muestras desde nuestro get manual o desde Metamap => Uso Processing
        Processing = 3,       //En estado de revision xq algun dato no es correcto (Cuando ya completo la captura de datos.
                              //  Aca se podra setear cuando se presiona boton de finalizar
        Postponed = 4,        //Cuando se pospone en metamap por falta de algun servicio  
        Cancelled = 5,        //Si presionan boton cancelar desde BpWeb
        Expired = 6,          //Se marca cunado expiro porque tiene fecha de expiracion
        Reviewing = 7,        //MArcado para revisión (ya sea automatico o manual)
        Rejected = 8,         //Si queda como rechazado (ya sea automatico o manual)
        Verified = 9          //Verificado ok (ya sea automatico o manual)
    }

    public enum TxStatusType
    {
        Automatic = 0,      //Cuando por default se realiza el chequeo de forma automatica, si todo se cumple)
        Hibrid = 1,         //Cuando da algun error en los chequeos y se coloca Review para revisar manual
        ManualForced = 2    //Cuando se fuerza a revision manual si o si, aunque esté Verified automatico
    }


}
