﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("bp_company_theme")]
    [Index(nameof(CompanyId), nameof(Name), Name = "bp_company_theme_name", IsUnique = true)]
    public partial class BpCompanyTheme
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("companyid")]
        public int CompanyId { get; set; }
        public Company Company { get; set; }


        [Required]
        [Column("name")]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; }
        
        [Required]
        [Column("jsonconfig", TypeName = "text")]
        public string JsonConfig { get; set; }

        //[ForeignKey(nameof(Companyid))]
        //[InverseProperty(nameof(BpCompany.BpCompanyTheme))]
        //public virtual BpCompany Company { get; set; }
    }
}
