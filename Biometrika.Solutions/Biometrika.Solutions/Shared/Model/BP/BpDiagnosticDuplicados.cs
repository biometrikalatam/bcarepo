﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.Models
{
    [Table("bp_diagnostic_duplicados")]
    public partial class BpDiagnosticDuplicados
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("ident_id_1")]
        public int IdentId1 { get; set; }
        [Required]
        [Column("typeid_1")]
        [StringLength(50)]
        [Unicode(false)]
        public string Typeid1 { get; set; }
        [Required]
        [Column("valueid_1")]
        [StringLength(50)]
        [Unicode(false)]
        public string Valueid1 { get; set; }
        [Column("name_1")]
        [StringLength(50)]
        [Unicode(false)]
        public string Name1 { get; set; }
        [Column("bir_id_1")]
        public int BirId1 { get; set; }
        [Column("bir_bodytype_1")]
        public int BirBodytype1 { get; set; }
        [Column("bir_creation_1", TypeName = "datetime")]
        public DateTime? BirCreation1 { get; set; }
        [Required]
        [Column("ident_id_2")]
        [StringLength(50)]
        [Unicode(false)]
        public string IdentId2 { get; set; }
        [Required]
        [Column("typeid_2")]
        [StringLength(50)]
        [Unicode(false)]
        public string Typeid2 { get; set; }
        [Required]
        [Column("valueid_2")]
        [StringLength(50)]
        [Unicode(false)]
        public string Valueid2 { get; set; }
        [Required]
        [Column("name_2")]
        [StringLength(50)]
        [Unicode(false)]
        public string Name2 { get; set; }
        [Column("bir_id_2")]
        public int BirId2 { get; set; }
        [Column("bir_bodytype_2")]
        public int BirBodytype2 { get; set; }
        [Column("bir_creation_2", TypeName = "datetime")]
        public DateTime? BirCreation2 { get; set; }
        [Column("umbral")]
        public int? Umbral { get; set; }
        [Column("score")]
        public int? Score { get; set; }
        [Column("created", TypeName = "datetime")]
        public DateTime? Created { get; set; }
    }
}
