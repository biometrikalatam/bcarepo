﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.Models
{
    [Table("bp_company_form")]
    public partial class BpCompanyForm
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        [Column("companyid")]
        public int Companyid { get; set; }
        public virtual Company Company { get; set; }

        [Required]
        [Column("formid")]
        [StringLength(50)]
        [Unicode(false)]
        public string Formid { get; set; }
        
        [Required]
        [Column("jsonschema", TypeName = "text")]
        public string Jsonschema { get; set; }

        //[ForeignKey(nameof(Companyid))]
        //[InverseProperty(nameof(BpCompany.BpCompanyForm))]
        
    }
}
