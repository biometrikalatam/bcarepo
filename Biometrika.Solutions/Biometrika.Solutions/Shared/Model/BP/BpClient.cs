﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("bp_client")]
    [Index(nameof(CompanyId), nameof(Clientid), Name = "bp_client_by_bp_company", IsUnique = true)]
    public partial class BpClient
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("companyid")]
        public int CompanyId { get; set; }

        public virtual Company Company { get; set; }

        [Required]
        [Column("clientid")]
        [StringLength(50)]
        [Unicode(false)]
        public string Clientid { get; set; }

        [Column("createddate", TypeName = "datetime")]
        public DateTime Createddate { get; set; }

        [Column("lastmodify", TypeName = "datetime")]
        public DateTime Lastmodify { get; set; }

        [Column("status")]
        public int Status { get; set; }
        [Column("userlastmodify")]
        
        public string UsuerId { get; set; }
        
        public virtual ApplicationUser User { get; set; }

        //[ForeignKey(nameof(Companyid))]
        //[InverseProperty(nameof(BpCompany.BpClient))]

    }
}
