﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.Models
{
    [Table("bp_log")]
    public partial class BpLog
    {
        [Key]
        public int Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Datel { get; set; }
        [Required]
        [StringLength(255)]
        [Unicode(false)]
        public string Threadl { get; set; }
        [Required]
        [StringLength(50)]
        [Unicode(false)]
        public string Levell { get; set; }
        [Required]
        [StringLength(255)]
        [Unicode(false)]
        public string Loggerl { get; set; }
        [Required]
        [StringLength(4000)]
        [Unicode(false)]
        public string Messagel { get; set; }
        [StringLength(2000)]
        [Unicode(false)]
        public string Exceptionl { get; set; }
    }
}
