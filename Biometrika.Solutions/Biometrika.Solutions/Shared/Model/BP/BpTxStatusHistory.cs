﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("bp_tx_status_history")]
    [Index(nameof(BpTxId), nameof(Update), Name = "bp_tx_status_history_txid_date")]
    public partial class BpTxStatusHistory
    {
        public BpTxStatusHistory()
        {
            
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("bptxid")]
        public virtual int BpTxId { get; set; }
        public virtual BpTx BpTx { get; set; }

        [Required]
        [StringLength(450)]
        [Column("userid")]
        public string UserId { get; set; }

        [Required]
        [Column("status")]
        public int Status { get; set; }

        [Required]
        [StringLength(1024)]
        [Column("reason")]
        public string Reason { get; set; }

        [Column("Update", TypeName = "datetime")]
        public DateTime Update { get; set; }

    }
}
