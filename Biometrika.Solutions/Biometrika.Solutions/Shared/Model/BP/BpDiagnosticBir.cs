﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.Models
{
    [Table("bp_diagnostic_bir")]
    public partial class BpDiagnosticBir
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("idOrg")]
        public int? IdOrg { get; set; }
        [Column("identid")]
        public int? Identid { get; set; }
        [Column("type")]
        public int? Type { get; set; }
        [Column("authenticationfactor")]
        public int? Authenticationfactor { get; set; }
        [Column("minutiaetype")]
        public int? Minutiaetype { get; set; }
        [Column("bodypart")]
        public int? Bodypart { get; set; }
        [Column("data", TypeName = "text")]
        public string Data { get; set; }
        [Column("additionaldata")]
        [StringLength(2048)]
        [Unicode(false)]
        public string Additionaldata { get; set; }
        [Column("timestamp", TypeName = "datetime")]
        public DateTime? Timestamp { get; set; }
        [Column("creation", TypeName = "datetime")]
        public DateTime? Creation { get; set; }
        [Column("signature", TypeName = "text")]
        public string Signature { get; set; }
        [Column("companyidenroll")]
        public int? Companyidenroll { get; set; }
        [Column("useridenroll")]
        public int? Useridenroll { get; set; }
    }
}
