﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("bp_tx_data")]
    [Index(nameof(Create), Name = "bp_tx_data_create")]
    public partial class BpTxData
    {
        public BpTxData()
        {
            
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("bptxid")]
        public virtual int BpTxId { get; set; }
        public virtual BpTx BpTx { get; set; }

        [Required]
        [Column("key")]
        public string Key { get; set; }


        [Column("create", TypeName = "datetime")]
        public DateTime Create { get; set; }

        [Required]
        [Column("dataraw", TypeName = "text")]
        public string DataRaw { get; set; }

        //TODO - Agregar DataType = json|text|image|xml para saber como tratarlo
    }
}
