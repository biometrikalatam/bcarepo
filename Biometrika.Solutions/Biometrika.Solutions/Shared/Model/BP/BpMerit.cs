﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("bp_merit")]
    [Index(nameof(Name), Name = "bp_merit_name", IsUnique = true)]
    [Index(nameof(UniqueMetamapGuid), Name = "bp_merit_uniquemetamapguid")]
    public class BpMerit
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        //[Column("companyid")]
        //public int Companyid { get; set; }
        //public Company Company { get; set; }


        [Required]
        [Column("name")]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; }

        [Column("description")]
        [StringLength(1024)]
        public string? Description { get; set; }

        /// <summary>
        /// Mensaje a mostrar para el mérito. Si esta nulo muestra un msg generico. Esto es para las capturas de video con msg x ejemplos, 
        /// o aprobacion de enrolamiento (aceptacion por parte del cliente)
        /// </summary>
        [Column("messagetoshow", TypeName = "text")]
        [Unicode(false)]
        public string? MessagegToShow;

        //[Required]
        [Column("uniquemetamapguid")]
        public string? UniqueMetamapGuid { get; set; }
    }
}
