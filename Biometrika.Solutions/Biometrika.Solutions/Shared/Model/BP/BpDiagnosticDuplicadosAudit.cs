﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.Models
{
    [Table("bp_diagnostic_duplicados_audit")]
    public partial class BpDiagnosticDuplicadosAudit
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("identid_eliminar")]
        public int? IdentidEliminar { get; set; }
        [Column("birid_eliminar")]
        public int? BiridEliminar { get; set; }
        [Column("createDate", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("identid_queda")]
        public int? IdentidQueda { get; set; }
        [Column("birid_queda")]
        public int? BiridQueda { get; set; }
        [Column("processed", TypeName = "datetime")]
        public DateTime? Processed { get; set; }
        [Column("tag")]
        [StringLength(100)]
        [Unicode(false)]
        public string Tag { get; set; }
    }
}
