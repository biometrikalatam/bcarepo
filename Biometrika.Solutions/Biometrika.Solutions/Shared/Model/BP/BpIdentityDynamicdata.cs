﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("bp_identity_dynamicdata")]
    [Index(nameof(BpIdentityId), Name = "Idx_Identity", IsUnique = false)]
    public partial class BpIdentityDynamicdata
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        [Column("bpidentityid")]
        public virtual int BpIdentityId { get; set; }
        public virtual BpIdentity BpIdentity { get; set; }

        [Required]
        [Column("ddkey")]
        [StringLength(50)]
        [Unicode(false)]
        public string Ddkey { get; set; }

        [Required]
        [Column("ddvalue", TypeName = "text")]
        public string Ddvalue { get; set; }

        [Column("creation", TypeName = "datetime")]
        public DateTime? Creation { get; set; }

        [Column("lastupdate", TypeName = "datetime")]
        public DateTime? Lastupdate { get; set; }

        //[ForeignKey(nameof(Identid))]
        //[InverseProperty(nameof(BpIdentity.BpIdentityDynamicdata))]
       
    }
}
