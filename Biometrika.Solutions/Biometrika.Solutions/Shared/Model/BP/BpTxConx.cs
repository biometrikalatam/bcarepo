﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("bp_tx_conx")]
    [Index(nameof(BpTxId), Name = "bp_tx_conx_bp_tx", IsUnique = false)]
    public partial class BpTxConx
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
 
        [Column("bptxid")]
        public virtual int BpTxId { get; set; }
        public virtual BpTx BpTx { get; set; }

        [Required]
        [Column("consultationtype")]
        [StringLength(50)]
        [Unicode(false)]
        public string Consultationtype { get; set; }
       
        [Column("connectorid")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Connectorid { get; set; }

        [Column("trackid")]
        [StringLength(100)]
        [Unicode(false)]
        public string Trackid { get; set; }

        [Column("status")]
        public int? Status { get; set; }
        
        [Column("result")]
        public int? Result { get; set; }
        
        [Column("score")]
        public double? Score { get; set; }
        
        [Column("threshold")]
        public double? Threshold { get; set; }
        
        [Column("timestamp")]
        [StringLength(25)]
        [Unicode(false)]
        public string Timestamp { get; set; }
        
        [Column("customertrackid")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Customertrackid { get; set; }

        [Column("trackid3ro")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Trackid3ro { get; set; }
        
        [Column("url3ro")]
        [StringLength(1024)]
        [Unicode(false)]
        public string? Url3ro { get; set; }
        
        [Column("sample3ro", TypeName = "text")]
        public string? Sample3ro { get; set; }
        
        //[Column("callbackurl")]
        //[StringLength(250)]
        //[Unicode(false)]
        
        //public string? Callbackurl { get; set; }
        //[Column("redirecturl")]
        //[StringLength(250)]
        //[Unicode(false)]
        
        //public string? Redirecturl { get; set; }
        //[Column("successurl")]
        //[StringLength(250)]
        //[Unicode(false)]
        
        //public string? Successurl { get; set; }
        [Column("onboardingmandatory")]
        
        public int? Onboardingmandatory { get; set; }
        [Column("onboardingtype")]
        public int? Onboardingtype { get; set; }
        
        [Column("signerinclude")]
        public int? Signerinclude { get; set; }
        
        [Column("videoinclude")]
        public int? Videoinclude { get; set; }
        
        [Column("videomessage", TypeName = "text")]
        public string? Videomessage { get; set; }
        
        [Column("videourl")]
        [StringLength(512)]
        [Unicode(false)]
        public string? Videourl { get; set; }
        
        [Column("theme")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Theme { get; set; }
        
        [Column("codereject")]
        public int? Codereject { get; set; }
        
        [Column("descriptionreject")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Descriptionreject { get; set; }
        
        [Column("session")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Session { get; set; }
        
        [Column("xuseragent")]
        [StringLength(1024)]
        [Unicode(false)]
        public string? Xuseragent { get; set; }
        
        [Column("sample3rosource", TypeName = "text")]
        public string? Sample3rosource { get; set; }
        
        [Column("sample3rosourcetype")]
        public int? Sample3rosourcetype { get; set; }
        
        [Column("sample3rotarget", TypeName = "text")]
        public string? Sample3rotarget { get; set; }
        
        [Column("sample3rotargettype")]
        public int? Sample3rotargettype { get; set; }
        
        [Column("audittrailimage", TypeName = "text")]
        public string? Audittrailimage { get; set; }
        
        [Column("lowqualityaudittrailimage", TypeName = "text")]
        public string? Lowqualityaudittrailimage { get; set; }
        
        [Column("enrollmentIdentifier")]
        [StringLength(100)]
        [Unicode(false)]
        public string? EnrollmentIdentifier { get; set; }
        
        [Column("idscan", TypeName = "text")]
        public string? Idscan { get; set; }
        
        [Column("idscanfrontimage", TypeName = "text")]
        public string? Idscanfrontimage { get; set; }
        
        [Column("idscanbackimage", TypeName = "text")]
        public string? Idscanbackimage { get; set; }
        
        [Column("idscanportrait", TypeName = "text")]
        public string? Idscanportrait { get; set; }
        
        [Column("idscansignature", TypeName = "text")]
        public string? Idscansignature { get; set; }
        
        [Column("responseocr", TypeName = "text")]
        public string? Responseocr { get; set; }
        
        [Column("response3ro", TypeName = "text")]
        public string? Response3ro { get; set; }
        
        [Column("workflow", TypeName = "text")]
        public string? Workflow { get; set; }
        
        [Column("autorizationinclude")]
        public int? Autorizationinclude { get; set; }
        
        [Column("autorization")]
        public int? Autorization { get; set; }
        
        [Column("autorizationmessage", TypeName = "text")]
        public string? Autorizationmessage { get; set; }
        
        [Column("autorizationdate", TypeName = "datetime")]
        public DateTime? Autorizationdate { get; set; }
        
        [Column("selfie2d", TypeName = "text")]
        public string? Selfie2d { get; set; }
        
        [Column("idcardfrontimage", TypeName = "text")]
        public string? Idcardfrontimage { get; set; }
        
        [Column("idcardbackimage", TypeName = "text")]
        public string? Idcardbackimage { get; set; }
        
        [Column("carregisterimageinclude")]
        public int? Carregisterimageinclude { get; set; }
        
        [Column("carregisterimagefront", TypeName = "text")]
        public string? Carregisterimagefront { get; set; }
        
        [Column("carregisterimageback", TypeName = "text")]
        public string? Carregisterimageback { get; set; }
        
        [Column("writingimageinclude")]
        public int? Writingimageinclude { get; set; }
        
        [Column("writingimage", TypeName = "text")]
        public string? Writingimage { get; set; }
        
        [Column("forminclude")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Forminclude { get; set; }
        
        [Column("form", TypeName = "text")]
        public string? Form { get; set; }

        
    }
}
