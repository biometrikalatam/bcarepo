﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.Models
{
    [Table("bp_diagnostic_blacklist")]
    public partial class BpDiagnosticBlacklist
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("ident_id")]
        public int IdentId { get; set; }
        [Required]
        [Column("typeid")]
        [StringLength(50)]
        [Unicode(false)]
        public string Typeid { get; set; }
        [Required]
        [Column("valueid")]
        [StringLength(50)]
        [Unicode(false)]
        public string Valueid { get; set; }
        [Column("name")]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; }
        [Column("bir_id")]
        public int BirId { get; set; }
        [Column("bir_bodytype")]
        public int BirBodytype { get; set; }
        [Column("bir_creation", TypeName = "datetime")]
        public DateTime BirCreation { get; set; }
        [Required]
        [Column("minutiae", TypeName = "text")]
        public string Minutiae { get; set; }
        [Column("bl_from", TypeName = "datetime")]
        public DateTime BlFrom { get; set; }
        [Required]
        [Column("auditor")]
        [StringLength(50)]
        [Unicode(false)]
        public string Auditor { get; set; }
    }
}
