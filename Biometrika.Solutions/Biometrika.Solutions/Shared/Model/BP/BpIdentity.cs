﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("bp_identity")]
    [Index(nameof(CompanyId), Name = "IX_bp_identity_companyidenroll")]
    [Index(nameof(CompanyId), nameof(Typeid), nameof(Valueid), 
           Name = "IX_bp_identity_companyidenroll_typeid_valueid", IsUnique = true)]
    [Index(nameof(Valueid), Name = "IX_bp_identity_valueid")]
    public partial class BpIdentity
    {
        public BpIdentity()
        {
            BpBir = new List<BpBir>();
            BpIdentity3ro = new List<BpIdentity3ro>();
            BpIdentityDynamicdata = new List<BpIdentityDynamicdata>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("nick")]
        [StringLength(80)]
        [Unicode(false)]
        public string? Nick { get; set; }
        
        [Required]
        [Column("typeid")]
        [StringLength(10)]
        [Unicode(false)]
        public string Typeid { get; set; }
        
        [Required]
        [Column("valueid")]
        [StringLength(50)]
        [Unicode(false)]
        public string Valueid { get; set; }
        
        [Column("name")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Name { get; set; }
        
        [Column("patherlastname")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Patherlastname { get; set; }
        
        [Column("motherlastname")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Motherlastname { get; set; }
        
        [Column("sex")]
        [StringLength(1)]
        [Unicode(false)]
        public string? Sex { get; set; }

        [Column("documentseriesnumber")]
        [StringLength(30)]
        [Unicode(false)]
        public string? Documentseriesnumber { get; set; }

        [Column("documentexpirationdate", TypeName = "datetime")]
        public DateTime? Documentexpirationdate { get; set; }

        [Column("visatype")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Visatype { get; set; }

        [Column("birthdate", TypeName = "datetime")]
        public DateTime? Birthdate { get; set; }

        [Column("birthplace")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Birthplace { get; set; }

        [Column("nationality")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Nationality { get; set; }

        [Column("photography", TypeName = "text")]
        public string? Photography { get; set; }

        [Column("signatureimage", TypeName = "text")]
        public string? Signatureimage { get; set; }

        [Column("profession")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Profession { get; set; }

        [Column("dynamicdata", TypeName = "text")]
        public string? Dynamicdata { get; set; }

        [Column("enrollinfo", TypeName = "text")]
        public string? Enrollinfo { get; set; }

        [Column("creation", TypeName = "datetime")]
        public DateTime? Creation { get; set; }

        [Column("verificationsource", TypeName = "text")]
        public string? Verificationsource { get; set; }

        [Column("companyid")]
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        [Column("userid")]
        public string? Userid { get; set; }
        public ApplicationUser User { get; set; }

        [Column("docimagefront", TypeName = "text")]
        public string? Docimagefront { get; set; }

        [Column("docimageback", TypeName = "text")]
        public string? Docimageback { get; set; }

        [Column("selfie", TypeName = "text")]
        public string? Selfie { get; set; }

        [Column("type3roservice")]
        [StringLength(25)]
        [Unicode(false)]
        public string? Type3roservice { get; set; }

        [Column("id3roservice")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Id3roservice { get; set; }

        //[InverseProperty("Ident")]
        public virtual List<BpBir> BpBir { get; set; }
        //[InverseProperty("Ident")]
        public virtual List<BpIdentity3ro> BpIdentity3ro { get; set; }
        //[InverseProperty("Ident")]
        public virtual List<BpIdentityDynamicdata> BpIdentityDynamicdata { get; set; }
    }
}
