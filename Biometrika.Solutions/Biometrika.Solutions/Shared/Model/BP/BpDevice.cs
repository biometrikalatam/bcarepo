﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.Models
{
    [Table("bp_device")]
    public partial class BpDevice
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("companyid")]
        public int Companyid { get; set; }
        [Required]
        [Column("serial")]
        [StringLength(50)]
        [Unicode(false)]
        public string Serial { get; set; }
        [Column("createddate", TypeName = "datetime")]
        public DateTime Createddate { get; set; }
        [Column("lastmodify", TypeName = "datetime")]
        public DateTime Lastmodify { get; set; }
        [Column("status")]
        public int Status { get; set; }
    }
}
