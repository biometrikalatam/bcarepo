﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Biometrika.Solutions.Shared.DTOs;
using Biometrika.Solutions.Shared.Model;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("bp_company_workflow_item")]
    [Index(nameof(BpCompanyWorkflowId))]
    public partial class BpCompanyWorkflowItem
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("bpcompanyworkflowid")]
        public int BpCompanyWorkflowId { get; set; }
        public virtual BpCompanyWorkflow BpCompanyWorkflow { get; set; }


        [Required]
        [Column("name")]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; }

        [Required]
        [Column("jsonconfig", TypeName = "text")]
        public string JsonConfig { get; set; }

        //[Required]
        [Column("bpcompanymetamapaid")]
        public int? BpCompanyMetamapId { get; set; }
        public virtual BpCompanyMetamap BpCompanyMetamap { get; set; }

        [Column("bpmeritid")]
        public int? BpMeritId { get; set; }
        public virtual BpMerit BpMerit { get; set; }

        //[ForeignKey(nameof(Companyid))]
        //[InverseProperty(nameof(BpCompany.BpCompanyTheme))]
        //public virtual BpCompany Company { get; set; }
    }
}
