﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.Models
{
    [Table("bp_tx_tonotarize")]
    public partial class BpTxTonotarize
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Required]
        [Column("trackid")]
        [StringLength(100)]
        [Unicode(false)]
        public string Trackid { get; set; }
        [Column("datenotarized", TypeName = "datetime")]
        public DateTime? Datenotarized { get; set; }
    }
}
