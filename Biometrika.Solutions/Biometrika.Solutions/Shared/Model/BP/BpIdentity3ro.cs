﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("bp_identity_3ro")]
    [Index(nameof(Type3roservice), nameof(BpIdentityId), Name = "IDX_IDENT_TYPE3RO", IsUnique = true)]
    public partial class BpIdentity3ro
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        //[Column("identid")]
        //public int Identid { get; set; }
        [Column("bpidentityid")]
        public virtual int BpIdentityId { get; set; }
        public virtual BpIdentity BpIdentity { get; set; }

        [Required]
        [Column("type3roservice")]
        public int Type3roservice { get; set; }
       
        [Required]
        [Column("trackid3ro", TypeName = "text")]
        public string Trackid3ro { get; set; }

        [Column("creation", TypeName = "datetime")]
        public DateTime Creation { get; set; }

        [Column("lastupdate", TypeName = "datetime")]
        public DateTime Lastupdate { get; set; }

        //[ForeignKey(nameof(Identid))]
        //[InverseProperty(nameof(BpIdentity.BpIdentity3ro))]
        //public virtual BpIdentity Ident { get; set; }
    }
}
