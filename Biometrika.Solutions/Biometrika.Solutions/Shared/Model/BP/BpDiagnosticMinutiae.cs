﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.Models
{
    [Table("bp_diagnostic_minutiae")]
    public partial class BpDiagnosticMinutiae
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("bir_id")]
        public int BirId { get; set; }
        [Column("ident_id")]
        public int IdentId { get; set; }
        [Required]
        [Column("minutiae", TypeName = "text")]
        public string Minutiae { get; set; }
        [Column("created", TypeName = "datetime")]
        public DateTime? Created { get; set; }
    }
}
