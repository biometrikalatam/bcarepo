﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.Models
{
    [Table("bp_diagnostic_identity")]
    public partial class BpDiagnosticIdentity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("idOriginal")]
        public int? IdOriginal { get; set; }
        [Column("nick")]
        [StringLength(80)]
        [Unicode(false)]
        public string Nick { get; set; }
        [Required]
        [Column("typeid")]
        [StringLength(10)]
        [Unicode(false)]
        public string Typeid { get; set; }
        [Required]
        [Column("valueid")]
        [StringLength(50)]
        [Unicode(false)]
        public string Valueid { get; set; }
        [Column("name")]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; }
        [Column("patherlastname")]
        [StringLength(50)]
        [Unicode(false)]
        public string Patherlastname { get; set; }
        [Column("motherlastname")]
        [StringLength(50)]
        [Unicode(false)]
        public string Motherlastname { get; set; }
        [Column("sex")]
        [StringLength(1)]
        [Unicode(false)]
        public string Sex { get; set; }
        [Column("documentseriesnumber")]
        [StringLength(30)]
        [Unicode(false)]
        public string Documentseriesnumber { get; set; }
        [Column("documentexpirationdate", TypeName = "datetime")]
        public DateTime? Documentexpirationdate { get; set; }
        [Column("visatype")]
        [StringLength(50)]
        [Unicode(false)]
        public string Visatype { get; set; }
        [Column("birthdate", TypeName = "datetime")]
        public DateTime? Birthdate { get; set; }
        [Column("birthplace")]
        [StringLength(50)]
        [Unicode(false)]
        public string Birthplace { get; set; }
        [Column("nationality")]
        [StringLength(50)]
        [Unicode(false)]
        public string Nationality { get; set; }
        [Column("photography", TypeName = "text")]
        public string Photography { get; set; }
        [Column("signatureimage", TypeName = "text")]
        public string Signatureimage { get; set; }
        [Column("profession")]
        [StringLength(50)]
        [Unicode(false)]
        public string Profession { get; set; }
        [Column("dynamicdata", TypeName = "text")]
        public string Dynamicdata { get; set; }
        [Column("enrollinfo", TypeName = "text")]
        public string Enrollinfo { get; set; }
        [Column("creation", TypeName = "datetime")]
        public DateTime? Creation { get; set; }
        [Column("verificationsource")]
        [StringLength(50)]
        [Unicode(false)]
        public string Verificationsource { get; set; }
        [Column("companyidenroll")]
        public int Companyidenroll { get; set; }
        [Column("useridenroll")]
        public int? Useridenroll { get; set; }
        [Column("dateCreate", TypeName = "date")]
        public DateTime? DateCreate { get; set; }
    }
}
