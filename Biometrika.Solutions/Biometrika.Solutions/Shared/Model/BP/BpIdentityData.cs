﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Biometrika.Solutions.Shared.Model.BP
{
   
    [Table("bp_identity_data")]
    [Index(nameof(BpIdentityId), Name = "Idx_Identity", IsUnique = false)]
    public partial class BpIdentityData
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("bpidentityid")]
        public virtual int BpIdentityId { get; set; }
        public virtual BpIdentity BpIdentity { get; set; }

        [Required]
        [Column("type")]
        [StringLength(50)]
        [Unicode(false)]
        public string Type { get; set; }  // phone | mail | address

        [Required]
        [StringLength(150)]
        [Column("value")]
        [Unicode(false)]
        public string Value { get; set; }

        [Column("creation", TypeName = "datetime")]
        public DateTime? Creation { get; set; }

        

    }
}
