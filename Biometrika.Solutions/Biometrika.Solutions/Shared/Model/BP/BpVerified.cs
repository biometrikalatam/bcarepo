﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model.Models
{
    [Table("bp_verified")]
    public partial class BpVerified
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("txid")]
        public int Txid { get; set; }
        [Column("datatype")]
        public int Datatype { get; set; }
        [Required]
        [Column("data", TypeName = "text")]
        public string Data { get; set; }
        [Column("additionaldata", TypeName = "text")]
        public string Additionaldata { get; set; }
        [Column("timestamp", TypeName = "datetime")]
        public DateTime Timestamp { get; set; }
        [Column("insertoption")]
        public int? Insertoption { get; set; }
        [Column("matchingtype")]
        public int? Matchingtype { get; set; }
    }
}
