﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("bp_company_workflow")]
    [Index(nameof(CompanyId), nameof(Name), Name = "bp_company_workflow_name", IsUnique = true)]
    public partial class BpCompanyWorkflow
    {
        public BpCompanyWorkflow()
        {
            BpCompanyWorkflowItem = new List<BpCompanyWorkflowItem>();
        }
    
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("companyid")]
        public int CompanyId { get; set; }
        public Company Company { get; set; }


        [Required]
        [Column("name")]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; }

        [Column("callbackurl")]
        [StringLength(1024)]
        public string? CallbackUrl { get; set; }  //Forma parte ahora de BpCompanyWorkflow

        [Column("redirecturl")]
        [StringLength(1024)]
        public string? RedirectUrl { get; set; }


        [Column("usefirstpage")]
        [DefaultValue(true)]
        public bool UseFirstPage { get; set; }  //Para habilitar la presentacion inicial de 

        [Column("usetheme")]
        [DefaultValue(true)]
        public bool UseTheme { get; set; }   //Por default true, pero si no es true, se piensa en que solo sea Metamapa =>
                                             //      iframe pantalla completa nomas, para evitar distorciones

        [Required]
        [Column("theme")]
        //[StringLength(50)]
        //[Unicode(false)]
        public int BpCompanyThemeId { get; set; }
        public BpCompanyTheme BpCompanyTheme { get; set; }

        [Column("expiration")]
        public int Expiration { get; set; }  //Si no se indica, se coloca expiracion por default del Configuration 

        
        [Column("statustype")]
        public int StatusType { get; set; }  //0 - Acepta Automatico | 1 - Coloca default para revisar

        public virtual List<BpCompanyWorkflowItem> BpCompanyWorkflowItem { get; set; }

        //[Column("successurl")]
        //[StringLength(1024)]
        //public string SuccessUrl;
        //[Column("jsonconfig", TypeName = "text")]
        //public string JsonConfig { get; set; }


        //[ForeignKey(nameof(Companyid))]
        //[InverseProperty(nameof(BpCompany.BpCompanyTheme))]
        //public virtual BpCompany Company { get; set; }
    }
}
