﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("bp_tx")]
    [Index(nameof(Timestampstart), Name = "bp_tx_startdate")]
    [Index(nameof(CompanyId), Name = "bp_tx_company")]
    public partial class BpTx
    {
        public BpTx()
        {
            BpTxConx = new List<BpTxConx>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("trackid")]
        [StringLength(100)]
        [Unicode(false)]
        public string Trackid { get; set; }

        [Column("statustype")]
        public int StatusType { get; set; }

        [Column("status")]
        public int Status { get; set; }

        [Column("expiration", TypeName = "datetime")]
        public DateTime? Expiration { get; set; }

        [Column("operationcode")]
        public int Operationcode { get; set; }

        [Required]
        [Column("typeid")]
        [StringLength(10)]
        [Unicode(false)]
        public string Typeid { get; set; }

        [Required]
        [Column("valueid")]
        [StringLength(50)]
        [Unicode(false)]
        public string Valueid { get; set; }

        /// <summary>
        /// RUT empresa a la que representa este ValueId (TTag)
        /// </summary>
        [Column("taxidcompany")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Taxidcompany { get; set; }

        [Column("result")]
        public int Result { get; set; }

        [Column("score")]
        public double Score { get; set; }

        [Column("threshold")]
        public double Threshold { get; set; }

        [Column("timestampstart", TypeName = "datetime")]
        public DateTime Timestampstart { get; set; }

        [Column("timestampend", TypeName = "datetime")]
        public DateTime Timestampend { get; set; }

        [Column("authenticationfactor")]
        public int Authenticationfactor { get; set; }

        [Column("minutiaetype")]
        public int Minutiaetype { get; set; }

        [Column("bodypart")]
        public int Bodypart { get; set; }

        [Column("actiontype")]
        public int Actiontype { get; set; }

        [Column("bporiginid")]
        public int BpOriginId { get; set; }
        public BpOrigin BpOrigin { get; set; }


        [Column("timestampclient", TypeName = "datetime")]
        public DateTime? Timestampclient { get; set; }

        [Column("clientid")]
        [StringLength(50)]
        [Unicode(false)]
        public string Clientid { get; set; }

        [Column("ipenduser")]
        [StringLength(15)]
        [Unicode(false)]
        public string? Ipenduser { get; set; }

        [Column("enduser")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Enduser { get; set; }

        [Column("dynamicdata", TypeName = "text")]
        public string? Dynamicdata { get; set; }

        [Column("companyid")]
        public int CompanyId { get; set; } //Si es un user desde portal se coloca la compañia sino será user de api (desde Token)
        public Company Company { get; set; }


        [Column("userid")]
        public string? UserId { get; set; } //Si es un user desde portal se coloca sinoserá user de api (desde Token)
        public ApplicationUser User { get; set; }


        [Column("operationsource")]
        public int Operationsource { get; set; }

        [Column("abs", TypeName = "text")]
        public string? Abs { get; set; }

        [Column("consumed", TypeName = "datetime")]
        public DateTime? Consumed { get; set; }

        [Column("checklock")]
        public int? Checklock { get; set; }

        [Column("estadolock")]
        [StringLength(10)]
        [Unicode(false)]
        public string? Estadolock { get; set; }

        [Column("razonlock")]
        [StringLength(30)]
        [Unicode(false)]
        public string? Razonlock { get; set; }

        [Column("vigenciafromlock")]
        [StringLength(20)]
        [Unicode(false)]
        public string? Vigenciafromlock { get; set; }

        [Column("signaturemanual", TypeName = "text")]
        public string? Signaturemanual { get; set; }

        [Column("geolocation")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Geolocation { get; set; }

        [Column("checkadult")]
        public int Checkadult { get; set; }

        [Column("checkexpired")]
        public int Checkexpired { get; set; }

        [Column("bpcompanyworkflowid")]
        public int? BpCompanyWorkflowId { get; set; }  //Si es 0 no hay Workflow asociado, es oparecion transaccional unica (Ej.: Verify de antes)
        public BpCompanyWorkflow BpCompanyWorkflow { get; set; }

        [Column("metadata")]
        public string? Metadata { get; set; }  //El user externo que integra coloca aqui su trackid o metadatos para enviarles con el resultado

        [Column("urlbpweb")]
        public string? URLBpWeb { get; set; } //Para mantener la url donde consultar

        [Column("callbackurl")]
        [StringLength(512)]
        [Unicode(false)]
        public string? Callbackurl { get; set; }

        [Column("redirecturl")]
        [StringLength(512)]
        [Unicode(false)]
        public string? Redirecturl { get; set; }

        //[Column("successurl")]
        //[StringLength(250)]
        //[Unicode(false)]
        //public string? Successurl { get; set; }

        //[InverseProperty("BpTxConx")]
        public virtual List<BpTxConx> BpTxConx { get; set; }

        public virtual List<BpTxData> BpTxData { get; set; }

        public virtual List<BpTxStatusHistory> BpTxStatusHistory { get; set; }
    }
}
