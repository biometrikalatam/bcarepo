﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("bp_company_metamap")]
    [Index(nameof(Companyid), nameof(UniqueMetamapGuid), Name = "bp_company_uniquemetamapguid", IsUnique = true)]
    public partial class BpCompanyMetamap
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("companyid")]
        public int Companyid { get; set; }
        public Company Company { get; set; }


        [Required]
        [Column("name")]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; }

        [Column("description")]
        [StringLength(1024)]
        public string? Description { get; set; }

        [Required]
        [Column("uniquemetamapguid")]
        public string UniqueMetamapGuid { get; set; }

        //[ForeignKey(nameof(Companyid))]
        //[InverseProperty(nameof(BpCompany.BpCompanyTheme))]
        //public virtual BpCompany Company { get; set; }
    }
}
