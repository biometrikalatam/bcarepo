﻿using Biometrika.Solutions.Shared.DTOs;

namespace Biometrika.Solutions.Shared.Model.Repositories.Interfaces
{
    public interface IRepositoryBPInfo
    {
        /// <summary>
        /// Devuelve una lista de Tx de BioPortal, completa o liviana, segun filtros 
        /// </summary>
        /// <param name="filters">Dictionary(key,value) con filtros y flag infdicando si es lista de datos completa o liviana</param>
        /// <returns></returns>
        Task<BSResponse> GetBpTxInfoByFilters(FilterDTO filters);
        Task<BSResponse> GetBpTxByTrackId(string trackid);

        Task<BSResponse> Report(BpTxInfoParamDTO bpTxCreateDTO, string userid);
    }
}