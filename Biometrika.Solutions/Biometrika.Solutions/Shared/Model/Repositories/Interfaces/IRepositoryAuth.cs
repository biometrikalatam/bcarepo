﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.Model.Repositories.Interfaces
{
    public interface IRepositoryAuth
    {
        Task<ApplicationUser> RetrieveUser(string userid);
    }
}
