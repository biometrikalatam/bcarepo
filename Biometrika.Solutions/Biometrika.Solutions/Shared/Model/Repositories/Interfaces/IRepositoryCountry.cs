﻿namespace Biometrika.Solutions.Shared.Model.Repositories.Interfaces
{
    public interface IRepositoryCountry
    {
        Task<List<Shared.Model.Country>> CountryList();
    }
}
