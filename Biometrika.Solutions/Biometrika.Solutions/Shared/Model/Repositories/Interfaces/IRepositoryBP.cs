﻿using Biometrika.Solutions.Shared.DTOs;

namespace Biometrika.Solutions.Shared.Model.Repositories.Interfaces
{
    public interface IRepositoryBP
    {
        Task<List<BPtxGridDTO>> bptxListToDTOGrid(FilterDTO filters);
        Task<BSResponse> EnrollOrUpdate(int companyid, BpIdentityDTO identities);
    }
}
