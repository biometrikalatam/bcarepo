﻿using Biometrika.Solutions.Shared.DTOs;

namespace Biometrika.Solutions.Shared.Model.Repositories.Interfaces
{
    public interface IRepositoryBPWeb
    {

        /// <summary>
        /// Crea la transaccion con los pa´rametros mínimos necesarios
        /// </summary>
        /// <param name="bpTxCreateDTO"></param>
        /// <param name="userid"></param>
        /// <returns>TrackId + URL de conexión por si queiren usarlo web</returns>
        Task<BSResponse> CreateBpTx(BpTxCreateDTO bpTxCreateDTO, string userid);

        /// <summary>
        /// Inicializa una transacción ya creada. Esto cambia el estado en la Tx como iniciada, y se envía 
        /// los datos del workflow que debe cumplir el BPWeb Client (Ahora Vue, luego será Blazor WebAssembly)
        /// </summary>
        /// <param name="trackId">TrackId de la transaccion que se desea iniciar</param>
        /// <returns></returns>
        Task<BSResponse> InitializeBpTx(string trackId);

        /// <summary>
        /// Dado el TrackId devuelve estado de la transaccion solo eso, para que sea rápido
        /// </summary>
        /// <param name="trackid"></param>
        /// <returns></returns>
        Task<BSResponse> GetStatusBpTx(string trackid);

        /// <summary>
        /// Dado el TrackId devuelve todos los datos relacionados a la Tx para que se pueda procesar en sistemas externos
        /// </summary>
        /// <param name="trackid"></param>
        /// <returns></returns>
        Task<BSResponse> GetDataBpTx(string trackid);

        /// <summary>
        /// Permitira desde el portal cambiar un estado de forma manual con un perfil de agente
        /// </summary>
        /// <param name="bpTxChangeStatusDTO"></param>
        /// <returns></returns>
        Task<BSResponse> ChangeStatusBpTx(BpTxChangeStatusDTO bpTxChangeStatusDTO);

        /// <summary>
        /// Procesara webhooks provenientes desde proveedores de terceros como Metamap, Transunion, Facetec, etc 
        /// </summary>
        /// <param name="jsonResponse">JSON a parsera y procesar</param>
        /// <returns></returns>
        Task<BSResponse> ProcessBpTxWebHook(string jsonResponse);

        /// <summary>
        /// Devuelve una lista de Tx de BioPortal, completa o liviana, segun filtros 
        /// </summary>
        /// <param name="filters">Dictionary(key,value) con filtros y flag infdicando si es lista de datos completa o liviana</param>
        /// <returns></returns>
        Task<BSResponse> ListBpTx(FilterDTO filters);
        Task<BSResponse> GetBpTxByTrackId(string trackid);
        Task<BSResponse> ProcesaWebhookMsgReceived(MetamapResponseModel model);
    }
}