﻿using Biometrika.Solutions.Shared.DTOs;

namespace Biometrika.Solutions.Shared.Model.Repositories.Interfaces
{
    public interface IRepositoryCI
    {
        Task<List<RdCitxGridDTO>> CItxListToDTOGrid(FilterDTO filters);
    }
}
