﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("rd_recibos")]
    public partial class RdRecibos
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Required]
        [Column("reciboid")]
        [StringLength(50)]
        [Unicode(false)]
        public string Reciboid { get; set; }
        [Column("fecha", TypeName = "datetime")]
        public DateTime Fecha { get; set; }
        [Column("idaplicacion")]
        public int Idaplicacion { get; set; }
        [Required]
        [Column("reciboxml", TypeName = "text")]
        public string Reciboxml { get; set; }
        [Column("version")]
        [StringLength(15)]
        [Unicode(false)]
        public string Version { get; set; }
        [Column("descripcion")]
        [StringLength(50)]
        [Unicode(false)]
        public string Descripcion { get; set; }
        [Column("iporigen")]
        [StringLength(15)]
        [Unicode(false)]
        public string Iporigen { get; set; }

        [ForeignKey("Idaplicacion")]
        [InverseProperty("RdRecibos")]
        public virtual RdAplicaciones IdaplicacionNavigation { get; set; }
    }
}