﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("nv_notaries")]
    [Index("Domain", Name = "IDX_Domain", IsUnique = true)]
    public partial class NvNotaries
    {
        public NvNotaries()
        {
            NvNotaryProcedures = new HashSet<NvNotaryProcedures>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Required]
        [Column("name")]
        [StringLength(100)]
        [Unicode(false)]
        public string Name { get; set; }
        [Required]
        [Column("rut")]
        [StringLength(12)]
        [Unicode(false)]
        public string Rut { get; set; }
        [Required]
        [Column("domain")]
        [StringLength(25)]
        [Unicode(false)]
        public string Domain { get; set; }
        [Column("notaryname")]
        [StringLength(100)]
        [Unicode(false)]
        public string Notaryname { get; set; }
        [Column("notarynamealternate")]
        [StringLength(100)]
        [Unicode(false)]
        public string Notarynamealternate { get; set; }
        [Column("phone")]
        [StringLength(20)]
        [Unicode(false)]
        public string Phone { get; set; }
        [Column("dateinit", TypeName = "datetime")]
        public DateTime Dateinit { get; set; }
        [Column("enddate", TypeName = "datetime")]
        public DateTime? Enddate { get; set; }
        [Column("status")]
        public int Status { get; set; }

        [InverseProperty("IdnotaryNavigation")]
        public virtual ICollection<NvNotaryProcedures> NvNotaryProcedures { get; set; }
    }
}