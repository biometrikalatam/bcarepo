﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.Model
{
    [Table("nv_procedures_evidences")]
    public partial class NvProceduresEvidences
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("idprocedure")]
        public int Idprocedure { get; set; }
        [Required]
        [Column("name")]
        [StringLength(100)]
        [Unicode(false)]
        public string Name { get; set; }
        /// <summary>
        /// 1-CI|2-Texto|3-JPG|4-PDF|5-XML
        /// </summary>
        [Column("type")]
        public int Type { get; set; }

        [ForeignKey("Idprocedure")]
        [InverseProperty("NvProceduresEvidences")]
        public virtual NvProcedures IdprocedureNavigation { get; set; }
    }
}