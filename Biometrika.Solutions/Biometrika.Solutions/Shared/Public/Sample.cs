﻿using System;

namespace Biometrika.Solutions.Shared.Public
{
    [Serializable]
    public class Sample
    {
#region Private
        private string _data;
        private int _minutiaetype;
        private string _additionaldata;
        private int _device;
        private int _bodypart;
#endregion Private

#region Public
        /// <summary>
        /// Contiene el sample en Base64
        /// </summary>
        public virtual string Data
        {
            get { return _data; }
            set { _data = value; }
        }

        /// <summary>
        /// Tipo de data contenido en Data
        /// </summary>
        public virtual int Minutiaetype
        {
            get { return _minutiaetype; }
            set { _minutiaetype = value; }
        }

        /// <summary>
        /// Reservado apra datos adicionales como w/h en WSQ o 
        /// coeficiente en NEC
        /// </summary>
        public virtual string Additionaldata
        {
            get { return _additionaldata; }
            set { _additionaldata = value; }
        }

        /// <summary>
        /// Id de device usado para obtener la muestra
        /// </summary>
        public int Device
        {
            get { return _device; }
            set { _device = value; }
        }

        /// <summary>
        /// Id de bodypart usado para obtener la muestra
        /// </summary>
        public int BodyPart
        {
            get { return _bodypart; }
            set { _bodypart = value; }
        }

        #endregion Public

    }
}
