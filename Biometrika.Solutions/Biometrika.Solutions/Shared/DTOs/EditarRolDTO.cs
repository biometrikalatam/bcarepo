﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class EditarRolDTO
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }
    }
}
