﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class CompanyDTO
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        [StringLength(20)]
        public string Rut { get; set; }
        
        [StringLength(80)]
        public string? Address { get; set; }
        
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        
        [StringLength(20)]
        public string? Phone { get; set; }
        
        [StringLength(20)]
        public string? Phone2 { get; set; }
        
        [StringLength(20)]
        public string? Fax { get; set; }
        
        public DateTime CreateDate { get; set; }
        
        public DateTime? Endate { get; set; }
        
        public DateTime? UpdateDate { get; set; }
        
        [Required]
        [StringLength(15)]
        public string Domain { get; set; }
        
        public string? Additionaldata { get; set; }
        
        [StringLength(50)]
        public string? Contactname { get; set; }
        
        public int Status { get; set; }
        
        public int? Holding { get; set; }
        
        //[InverseProperty("CompanyNavigation")]
        public virtual List<UserDTO> Users { get; set; }

        public virtual List<BpClientDTO> BpClient { get; set; }
        public virtual List<BpCompanyThemeDTO> BpCompanyTheme { get; set; }
        public virtual List<BpCompanyMetamapDTO> BpCompanyMetamap { get; set; }
        public virtual List<BpCompanyWorkflowDTO> BpCompanyWorkflow { get; set; }
    }
}
