﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class RdCitxGridDTO
    {
        public int Id { get; set; }
        public string Trackid { get; set; }
        public int Type { get; set; }
        public int Actionid { get; set; }
        public DateTime? Dategen { get; set; }
        public string Typeid { get; set; }
        public string Valueid { get; set; }
        public string NombreCompleto { get; set; }
        public string Score { get; set; }
     
    }
}
