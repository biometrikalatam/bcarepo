﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class UsuarioDTO
    {
        public string UserId { get; set; }
        public string Email { get; set; }
    }
}
