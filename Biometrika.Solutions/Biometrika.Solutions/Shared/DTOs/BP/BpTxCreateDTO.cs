﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class BpTxCreateDTO
    {

        //public BpTxCreateDTO(string typeid, string valueid, string metadata, string workflowname)
        //{
        //    Typeid = typeid;
        //    Valueid = valueid;
        //    Workflowname = workflowname;
        //    Metadata = metadata;
        //}

        /// <summary>
        /// Valor del tipo de documento a verificar, por ejempo RUT, PAS (Pasamorte), DNI, etc.
        /// </summary>
        [Required]
        public string Typeid { get; set; }

        /// <summary>
        /// Número de cédula o pasaporte a verificar. Si no se tiene ese valor y se desea reconocer los datos desde el documento 
        /// verificado, se debe enviar "NA". En caso de enviar un valor concreto diferente a NA, cuando se reconoce el documento
        /// se verifica que el número reconocido sea igual al pedido, de otra forma retorna error.
        /// Si el valor llega en nulo retorna error.
        /// </summary>
        [Required]
        public string Valueid { get; set; }

        /// <summary>
        /// Nombre único para cada compañía de uno de los workflows definidos de verificación. POr ejemplo: "BasciOnBoarding". 
        /// Si este valor llega en nulo se retonra error en la creación.
        /// </summary>
        [Required]
        public string Workflowname { get; set; }       //Id tabla BpCompanyWorkflow 

        /// <summary>
        /// Valor flexible para que cada cliente que crea una transacción envíe por ejemplo valores relevantes de su sistema, de tal
        /// forma de relacionar esta transacción con el workflow de verificación. 
        /// Este valor se retorna luego, en cada recuperación de los datos relacionados a la transacción. 
        /// Un ejemplo pdría ser un Id único de transacción de su sistema.
        /// </summary>
        public string Metadata { get; set; }  //Trackid o datos informado por el usuario que integra BPWeb (Json)

        //public string ClientId { get; set; }  //Para controld e clientes que se conectan



    }
}
