﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Biometrika.Solutions.Shared.DTOs;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.DTOs
{   
    public class BpCompanyMetamapDTO
    {
        public int Id { get; set; }

        //public int CompanyId { get; set; }
        public CompanyDTO Company { get; set; }

        [Required]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; }

        [StringLength(1024)]
        public string? Description { get; set; }

        [Required]
        public string UniqueMetamapGuid { get; set; }

        
    }
}
