﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class BpTxChangeStatusDTO
    {
        [Required]
        public string TrackId { get; set; }

        [Required]
        public int Status { get; set; }

        [Required]
        public string Reason { get; set; }

        //[Required]
        //public string UserId { get; set; } //Se toma del token

    }
}
