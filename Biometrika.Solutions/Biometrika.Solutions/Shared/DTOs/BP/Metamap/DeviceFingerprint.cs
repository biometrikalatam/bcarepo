﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs.BP.Metamap
{
    public class DeviceFingerprint
    {
        public string ua { get; set; }
        public Browser browser { get; set; }
        public Engine engine { get; set; }
        public Os os { get; set; }
        public Device device { get; set; }
        public App app { get; set; }
        public string ip { get; set; }
        public bool vpnDetectionEnabled { get; set; }
        public string ipRestrictionEnabled { get; set; }

    }

    public class Browser
    {
        public string name { get; set; }
        public string version { get; set; }
        public string major { get; set; }

    }
    public class Engine
    {
        public string name { get; set; }
        public string version { get; set; }

    }
    public class Os
    {
        public string name { get; set; }
        public string version { get; set; }

    }
    public class Device
    {
        public string vendor { get; set; }
        public string model { get; set; }
        public string type { get; set; }

    }
    public class App
    {
        public string platform { get; set; }
        public string version { get; set; }

    }
}
