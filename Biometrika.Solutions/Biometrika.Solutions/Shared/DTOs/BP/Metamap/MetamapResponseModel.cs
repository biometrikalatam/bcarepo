﻿using Biometrika.Solutions.Shared.DTOs.BP.Metamap;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Biometrika.Solutions.Shared.DTOs
{
    public class MetamapResponseModel
    {
        public string resource { get; set; }
        public string matiDashboardUrl { get; set; }
        public string status { get; set; }
        public string eventName { get; set; }
        public string flowId { get; set; }
        public DateTime timestamp { get; set; }
        public Step step { get; set; }
        public Computed computed { get; set; }
        public List<Documents> documents { get; set; }
        public bool expired { get; set; }
        public Flow flow { get; set; }
        public Identity identity { get; set; }
        public List<Step> steps { get; set; }
        public bool masJobToBePostpone { get; set; }
        public string id { get; set; }
        public DeviceFingerprint deviceFingerprint { get; set; }
        public bool hasProblem { get; set; }
        public string identityStatus { get; set; }
        //public CustomInputValues customInputValues { get; set; }
        public Metadata metadata { get; set; }

        public string base64NOMTimeStamp { get; set; }
        public string cid { get; set; }

    }

    public class Metadata
    {
        public string trackid { get; set; }
    }

    public class Error
    {
        public string type { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public object details { get; set; }
        public string reasonCode { get; set; }
        
    }
    
    public class Flow
    {
        public string id { get; set; }
        public string name { get; set; }

    }
    public class Identity
    {
        public string status { get; set; }

    }

    public class Step
    {
        public int status { get; set; }
        public string id { get; set; }
        public string phase { get; set; }
        public object data { get; set; }
        public Error error { get; set; }

    }



    public class AtomicFieldParams
    {
        public string type { get; set; }
        public bool value { get; set; }

    }
    public class Fields
    {
        public AtomicFieldParams atomicFieldParams { get; set; }
        public string _id { get; set; }
        public string name { get; set; }
        public string label { get; set; }
        public string type { get; set; }
        public bool isMandatory { get; set; }
        public List<object> children { get; set; }

    }
    public class CustomInputValues
    {
        public List<Fields> fields { get; set; }
        public string country { get; set; }

    }
       
}

