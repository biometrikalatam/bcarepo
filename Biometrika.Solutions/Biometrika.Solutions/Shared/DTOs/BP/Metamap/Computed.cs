﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs.BP.Metamap
{
    public class Computed
    {
        public Age age { get; set; }
        public IsDocumentExpired isDocumentExpired { get; set; }

    }
    public class Age
    {
        public int data { get; set; }

    }
    public class DataComputed
    {
        [JsonProperty("national-id")]
        public bool nationalid { get; set; }

    }
    public class IsDocumentExpired
    {
        public DataComputed data { get; set; }

    }
}
