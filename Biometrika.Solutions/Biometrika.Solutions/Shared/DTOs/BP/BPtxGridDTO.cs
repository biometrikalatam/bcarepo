﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class BPtxGridDTO
    {
        public int Id { get; set; }
        public string TrackId { get; set; }
        public int OperationCode { get; set; }
        public int ActionType { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string ClientId { get; set; }
        public string CompanyName { get; set; }
     
    }
}
