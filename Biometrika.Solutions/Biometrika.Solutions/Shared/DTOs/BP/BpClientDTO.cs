﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class BpClientDTO
    {
        public int Id { get; set; }

        public virtual CompanyDTO Company { get; set; }

        [Required]
        [StringLength(50)]
        public string Clientid { get; set; }

        public DateTime Createddate { get; set; }

        public DateTime Lastmodify { get; set; }

        public int Status { get; set; }

        public virtual UserDTO User { get; set; }
    }
}
