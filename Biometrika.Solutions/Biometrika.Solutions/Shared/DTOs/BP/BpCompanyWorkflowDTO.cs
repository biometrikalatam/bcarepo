﻿using Biometrika.Solutions.Shared.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class BpCompanyWorkflowDTO
    {
        public int Id { get; set; }
        
        public int CompanyId { get; set; }
        public Company Company { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(1024)]
        public string? CallbackUrl  { get; set; }    //Forma parte ahora de BpCompanyWorkflow

        [StringLength(1024)]
        public string? RedirectUrl { get; set; }

        public bool UseFirstPage { get; set; }
        public bool UseTheme { get; set; }

        [Required]
        [StringLength(50)]
        public BpCompanyThemeDTO BpCompanyTheme { get; set; }

        public int Expiration { get; set; }  //Si no se indica, se coloca expiracion por default del Configuration 

        public int StatusType { get; set; }  //0 - Acepta Automatico | 1 - Coloca default para revisar

        public virtual List<BpCompanyWorkflowItemDTO> BpCompanyWorkflowItem { get; set; }
    }
}
