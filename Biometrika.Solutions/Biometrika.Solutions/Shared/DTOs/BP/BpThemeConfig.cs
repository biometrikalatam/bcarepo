﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class BpThemeConfig
    {
        public string logoServiceCompany { get; set; }
        public string logoCustomer { get; set; }
        public bool dark { get; set; }
        public Colors colors { get; set; }
       
    }

    public class Colors
    {
        public string button { get; set; }
        public string textbutton { get; set; }
        public string background { get; set; }
        public string primary { get; set; }
        public string secondary { get; set; }
        public string accent { get; set; }
        public string error { get; set; }
        public string info { get; set; }
        public string success { get; set; }
        public string warning { get; set; }

    }

}
