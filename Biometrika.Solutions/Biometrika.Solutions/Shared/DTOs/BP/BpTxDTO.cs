﻿using Biometrika.Solutions.Shared.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class BpTxDTO
    {
        [Required]
        [StringLength(100)]
        public string Trackid { get; set; }

        public int Status { get; set; }
        public DateTime? Expiration { get; set; }

        public int Actiontype { get; set; }
        public int Operationcode { get; set; }

        [Required]
        public string Typeid { get; set; }
        [Required]
        [StringLength(50)]
        public string Valueid { get; set; }

        [StringLength(50)]
        public string? Taxidcompany { get; set; }

        public int Result { get; set; }
        public double Score { get; set; }
        public double Threshold { get; set; }

        public DateTime Timestampstart { get; set; }
        public DateTime Timestampend { get; set; }

        public int Authenticationfactor { get; set; }

        public int Minutiaetype { get; set; }

        public int Bodypart { get; set; }

        [StringLength(50)]
        public string Clientid { get; set; }

        [StringLength(15)]
        public string? Ipenduser { get; set; }

        [StringLength(50)]
        public string? Enduser { get; set; }

        public string? Dynamicdata { get; set; }

        //public int CompanyId { get; set; } //Si es un user desde portal se coloca la compañia sino será user de api (desde Token)
        public CompanyDTO Company { get; set; }

        //public string? UserId { get; set; } //Si es un user desde portal se coloca sinoserá user de api (desde Token)
        public UserDTO User { get; set; }

        public int Operationsource { get; set; }

        public string? Abs { get; set; }

        public DateTime? Consumed { get; set; }

        public int? Checklock { get; set; }

        [StringLength(10)]
        public string? Estadolock { get; set; }

        [StringLength(30)]
        public string? Razonlock { get; set; }

        [StringLength(20)]
        public string? Vigenciafromlock { get; set; }

        public string? Signaturemanual { get; set; }

        [StringLength(50)]
        public string? Geolocation { get; set; }

        public int Checkadult { get; set; }

        public int Checkexpired { get; set; }

        //public int? BpCompanyWorkflowId { get; set; }  //Si es 0 no hay Workflow asociado, es oparecion transaccional unica (Ej.: Verify de antes)
        public BpCompanyWorkflowDTO BpCompanyWorkflow { get; set; }

        public string? Metadata { get; set; }  //El user externo que integra coloca aqui su trackid o metadatos para enviarles con el resultado

        public string URLBpWeb { get; set; } //Para mantener la url donde consultar
        
        [StringLength(512)]
        public string? Callbackurl { get; set; }

        [StringLength(512)]
        public string? Redirecturl { get; set; }

        public virtual List<BpTxConx> BpTxConx { get; set; }

        public virtual List<BpTxData> BpTxData { get; set; }
    }
}
