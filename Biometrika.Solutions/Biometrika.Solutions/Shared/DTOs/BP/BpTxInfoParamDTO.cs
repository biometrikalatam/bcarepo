﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class BpTxInfoParamDTO
    {
        public string metadata { get; set; }

        public int report { get; set; }

        public string valueid { get; set; }

        public string serialcardid { get; set; }

        public string name { get; set; }

        public string birthdate { get; set; }

        public string  licenseplate { get; set; }

        public Include include { get; set; }

    }

    public class Include
    {
        public bool basicsrcei { get; set; }
        public bool blocksrcei { get; set; }
        public bool contactinfo { get; set; }
        public bool verifysii { get; set; }
        public bool commercial { get; set; }
        public bool law { get; set; }
    }
}
