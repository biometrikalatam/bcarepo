﻿using Biometrika.Solutions.Shared.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class BpCompanyWorkflowItemDTO
    {
        public int Id { get; set; }

        //[Required]
        //public int BpCompanyWorkflowId { get; set; }
        //public virtual BpCompanyWorkflow BpCompanyWorkflow { get; set; }


        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public string JsonConfig { get; set; }

        //[Required]
        //public int? BpCompanyMetamapId { get; set; }
        public virtual BpCompanyMetamapDTO BpCompanyMetamap { get; set; }

        //public int? BpMeritId { get; set; }
        public virtual BpMerit BpMerit { get; set; }
    }
}
