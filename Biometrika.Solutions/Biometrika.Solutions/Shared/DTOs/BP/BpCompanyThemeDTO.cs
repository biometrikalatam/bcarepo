﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Biometrika.Solutions.Shared.DTOs;
using Microsoft.EntityFrameworkCore;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class BpCompanyThemeDTO
    {
        public int Id { get; set; }

        //public int CompanyId { get; set; }
        public CompanyDTO Company { get; set; }


        [Required]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; }
        
        [Required]
        [Column("jsonconfig", TypeName = "text")]
        public string JsonConfig { get; set; }

        
    }
}
