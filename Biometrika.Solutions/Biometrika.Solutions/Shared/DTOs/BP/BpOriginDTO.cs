﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class BpOriginDTO
    {
        [Required]
        [Column("description")]
        [StringLength(50)]
        [Unicode(false)]
        public string Description { get; set; }
    }
}
