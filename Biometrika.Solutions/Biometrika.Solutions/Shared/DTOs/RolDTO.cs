﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class RolDTO
    {
        public string RoleId { get; set; }
        public string Nombre { get; set; }
    }
}
