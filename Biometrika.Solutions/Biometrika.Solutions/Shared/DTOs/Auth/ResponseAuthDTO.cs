﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs
{
    public class ResponseAuthDTO
    {
        /// <summary>
        /// Codigo de retorno de la operacion
        /// </summary>
        public int Code { get; set; }
        /// <summary>
        /// Mensaje explicativo del código anterior 
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Token generado si no hubo errores
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// Fecha de expiración del token generado si no hubo errores
        /// </summary>
        public DateTime Expiration { get; set; }

    }
}
