using Biometrika.Metamap.Api;
using RestSharp;

namespace TestsVarios
{
    public partial class Form1 : Form
    {

        MetamapClient _MMP_CLIENT = new MetamapClient();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            string msg;
            int ret = _MMP_CLIENT.Initialize("https://api.getmati.com/",30000, 
                                             "62c34849e98ff3001c9dac34",
                                             "Q7YUGFRUN6W5XNMJ4T09EDH46YJZ2VPH", out msg);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TestDownload();
        }

        private void TestDownload()
        {
            string msg;
            string selfie = _MMP_CLIENT.GetMedia(richTextBox1.Text.Trim(), "jpg", out msg);

            if (!string.IsNullOrEmpty(selfie))
            {
                byte[] data = Convert.FromBase64String(selfie);
                pictureBox1.Image = Image.FromStream(new System.IO.MemoryStream(data));
                this.Refresh();
            }

            //ANDA con version 106.1 de Restsharp
            //var client = new RestClient("https://media.getmati.com/file?location=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6IjYyZTk0ZTI3LWJjZGUtNGZhOC1hZGU3LTAwYTgxYmI5NTQ3OS53ZWJtIiwiZm9sZGVyIjoidmlkZW8iLCJpYXQiOjE2NjI3NTcwNDYsImV4cCI6MTY2Mjg0MzQ0NiwiYXVkIjoiYzkyNmQ1MDItYmEzYi00N2FhLTg0YjItZjJmZGNiODQxYjdlIn0.UTPOcwgx8k3S9l1-bGY_O0gIrhj7lHzrmHsL338kKuc");
            //client.Timeout = 30000;
            //var request = new RestRequest(Method.GET);
            //request.AddHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnQiOnsiX2lkIjoiNjJjMzQ4NDllOThmZjMwMDFjOWRhYzM0IiwibWVyY2hhbnQiOnsiX2lkIjoiNjJjMzQ4NDhkOGQ4ZmEwMDFjOTBmOGU1Iiwib3duZXIiOiI2MmMzNDg0ODA2ZWJlYTAwMWNhYzQ1YmUiLCJzdWJzY3JpcHRpb25TdGF0dXMiOnsidmFsdWUiOiJhY3RpdmUiLCJ1cGRhdGVkQXQiOiIyMDIyLTA3LTIwVDAwOjAwOjAwLjAxOFoifX19LCJ1c2VyIjp7Il9pZCI6IjYyYzM0ODQ4MDZlYmVhMDAxY2FjNDViZSJ9LCJzY29wZSI6InZlcmlmaWNhdGlvbl9mbG93IGlkZW50aXR5OnJlYWQgdmVyaWZpY2F0aW9uOnJlYWQiLCJpYXQiOjE2NjI3NTY0OTcsImV4cCI6MTY2Mjc2MDA5NywiaXNzIjoib2F1dGgyLXNlcnZlciJ9.I71D0KY4rZQpSeK7c2ugT62hC2r5xOwZolCFY4SeO3g");
            //IRestResponse response = client.Execute(request);

            //byte[] video = response.RawBytes;
            //System.IO.File.WriteAllBytes("d:\\tmp\\video.webm", video);

            //Console.WriteLine(response.Content);

            var request = new RestRequest("https://media.getmati.com/file?location=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6IjYyZTk0ZTI3LWJjZGUtNGZhOC1hZGU3LTAwYTgxYmI5NTQ3OS53ZWJtIiwiZm9sZGVyIjoidmlkZW8iLCJpYXQiOjE2NjI3NTcwNDYsImV4cCI6MTY2Mjg0MzQ0NiwiYXVkIjoiYzkyNmQ1MDItYmEzYi00N2FhLTg0YjItZjJmZGNiODQxYjdlIn0.UTPOcwgx8k3S9l1-bGY_O0gIrhj7lHzrmHsL338kKuc",
                                          Method.Get);
            request.AddHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnQiOnsiX2lkIjoiNjJjMzQ4NDllOThmZjMwMDFjOWRhYzM0IiwibWVyY2hhbnQiOnsiX2lkIjoiNjJjMzQ4NDhkOGQ4ZmEwMDFjOTBmOGU1Iiwib3duZXIiOiI2MmMzNDg0ODA2ZWJlYTAwMWNhYzQ1YmUiLCJzdWJzY3JpcHRpb25TdGF0dXMiOnsidmFsdWUiOiJhY3RpdmUiLCJ1cGRhdGVkQXQiOiIyMDIyLTA3LTIwVDAwOjAwOjAwLjAxOFoifX19LCJ1c2VyIjp7Il9pZCI6IjYyYzM0ODQ4MDZlYmVhMDAxY2FjNDViZSJ9LCJzY29wZSI6InZlcmlmaWNhdGlvbl9mbG93IGlkZW50aXR5OnJlYWQgdmVyaWZpY2F0aW9uOnJlYWQiLCJpYXQiOjE2NjI3NjI0ODksImV4cCI6MTY2Mjc2NjA4OSwiaXNzIjoib2F1dGgyLXNlcnZlciJ9.O_MawlCPkT4WpLTiLiRfpEUmvgMbVtXOy1w0uTWqsvk");
            var client = new RestClient(new RestClientOptions { Timeout = 300000 });
            var response = client.Execute(request);

            byte[] video = response.RawBytes;


            //HttpClient http = new HttpClient();
            //http.BaseAddress = new Uri("https://media.getmati.com");
            //http.Timeout = new TimeSpan(0,0,30);
            //var response1 = await http.GetAsync("/file?location=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6ImJmMDUzMGVmLTI4NTctNDQ2ZC1hNDljLWRhNWE4Y2JlNTllNC5qcGVnIiwiZm9sZGVyIjoic2VsZmllIiwiaWF0IjoxNjYyNzM4Nzk5LCJleHAiOjE2NjI4MjUxOTksImF1ZCI6ImVhYzE2MGZmLWEwZTUtNDNkMS1hZDU5LTUyMDIyNWJkNzliOCJ9.Uw0Ch_DtEF8hkiG3BlyEVeKgBd4ksunQdVwjs8axLXQ");

            //var request = new RestRequest("/file", Method.GET);
            //request.AddParameter("location", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6ImJmMDUzMGVmLTI4NTctNDQ2ZC1hNDljLWRhNWE4Y2JlNTllNC5qcGVnIiwiZm9sZGVyIjoic2VsZmllIiwiaWF0IjoxNjYyNzM4Nzk5LCJleHAiOjE2NjI4MjUxOTksImF1ZCI6ImVhYzE2MGZmLWEwZTUtNDNkMS1hZDU5LTUyMDIyNWJkNzliOCJ9.Uw0Ch_DtEF8hkiG3BlyEVeKgBd4ksunQdVwjs8axLXQ");
            //request.Timeout = 30000;
            ////var result = await ExecuteTaskAsync<DateTime?>(request);

            ////var client = new RestClient("https://media.getmati.com/file?location=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6ImJmMDUzMGVmLTI4NTctNDQ2ZC1hNDljLWRhNWE4Y2JlNTllNC5qcGVnIiwiZm9sZGVyIjoic2VsZmllIiwiaWF0IjoxNjYyNzM4Nzk5LCJleHAiOjE2NjI4MjUxOTksImF1ZCI6ImVhYzE2MGZmLWEwZTUtNDNkMS1hZDU5LTUyMDIyNWJkNzliOCJ9.Uw0Ch_DtEF8hkiG3BlyEVeKgBd4ksunQdVwjs8axLXQ");
            ////client.DefaultParametersTimeout = 30000;
            ////var request = new RestRequest(Method.GET);
            ////IRestResponse response = client.Execute(request);
            //var client = new RestClient("https://media.getmati.com"); // new RestClientOptions { Timeout = 300000 });
            ////client.AddDefaultParameter("location", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6ImJmMDUzMGVmLTI4NTctNDQ2ZC1hNDljLWRhNWE4Y2JlNTllNC5qcGVnIiwiZm9sZGVyIjoic2VsZmllIiwiaWF0IjoxNjYyNzM4Nzk5LCJleHAiOjE2NjI4MjUxOTksImF1ZCI6ImVhYzE2MGZmLWEwZTUtNDNkMS1hZDU5LTUyMDIyNWJkNzliOCJ9.Uw0Ch_DtEF8hkiG3BlyEVeKgBd4ksunQdVwjs8axLXQ");
            ////var client = new RestClient("https://media.getmati.com/file?location=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlTmFtZSI6ImJmMDUzMGVmLTI4NTctNDQ2ZC1hNDljLWRhNWE4Y2JlNTllNC5qcGVnIiwiZm9sZGVyIjoic2VsZmllIiwiaWF0IjoxNjYyNzM4Nzk5LCJleHAiOjE2NjI4MjUxOTksImF1ZCI6ImVhYzE2MGZmLWEwZTUtNDNkMS1hZDU5LTUyMDIyNWJkNzliOCJ9.Uw0Ch_DtEF8hkiG3BlyEVeKgBd4ksunQdVwjs8axLXQ")
            ////{
            ////    //Authenticator = new HttpBasicAuthenticator("username", "password")

            ////};
            ////var request = new RestRequest(Method.Get);
            //var response = client.Execute(request) ;//.DownloadData(request);

            //System.IO.File.WriteAllBytes("d:\\tmp\\imgdownloaded.jpg", response.Content);
            //Console.WriteLine(response.Content);

            //Thread.Sleep(5000);
            int i = 0;
        }
    }
}