﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs.BP.Metamap
{
    public class Datas
    {
    }

    //
    public class DataAgeCheck
    {
        public int age { get; set; }
        public int ageThreshold { get; set; }
        public bool underage { get; set; }
    }

    //Facematch
    public class DataFacematch
    {
        public double score { get; set; }
    }

    //DataPremiumAML
    public class DataPremiumAML
    {
        public string nameSearched { get; set; }
        public string profileUrl { get; set; }
        public DateTime searchedOn { get; set; }
        public int searchId { get; set; }
        public ErrorDataPremiumAML error { get; set; }

    }

    public class DetailsPremiumAML
    {
        public string matchStatus { get; set; }
        public string riskLevel { get; set; }
        public int numberOfBlacklists { get; set; }

    }
    public class ErrorDataPremiumAML
    {
        public string type { get; set; }
        public string code { get; set; }
        public DetailsPremiumAML details { get; set; }

    }

    //WatchList
    public class DataWatchList
    {
        public List<ItemWatchlist> data { get; set; }
    }
    public class ItemWatchlist
    {
        public Watchlist watchlist { get; set; }
        public SearchParams searchParams { get; set; }
        public List<object> searchResult { get; set; }
        public DateTime searchedAt { get; set; }

    }
    public class Watchlist
    {
        public int id { get; set; }
        public string name { get; set; }

    }
    public class SearchParams
    {
        public string fullName { get; set; }
        public string dateOfBirth { get; set; }

    }


    //email-risk-data
    public class DataEmailRisk
    {
        public string emailAddress { get; set; }
        public int riskScore { get; set; }
        public int riskThreshold { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
        public bool valid { get; set; }
        public bool disposable { get; set; }
        public int smtp_score { get; set; }
        public int overall_score { get; set; }
        public string first_name { get; set; }
        public bool generic { get; set; }
        public bool common { get; set; }
        public bool dns_valid { get; set; }
        public bool honeypot { get; set; }
        public string deliverability { get; set; }
        public bool frequent_complainer { get; set; }
        public string spam_trap_score { get; set; }
        public bool catch_all { get; set; }
        public bool timed_out { get; set; }
        public bool suspect { get; set; }
        public bool recent_abuse { get; set; }
        public string suggested_domain { get; set; }
        public bool leaked { get; set; }
        public Domain_age domain_age { get; set; }
        public First_seen first_seen { get; set; }
        public string sanitized_email { get; set; }
        public string domain_velocity { get; set; }
        public string user_activity { get; set; }
        public Associated_names associated_names { get; set; }
        public Associated_phone_numbers associated_phone_numbers { get; set; }
        public string request_id { get; set; }

    }
    public class Domain_age
    {
        public string human { get; set; }
        public int timestamp { get; set; }
        public DateTime iso { get; set; }

    }
    public class First_seen
    {
        public string human { get; set; }
        public int timestamp { get; set; }
        public DateTime iso { get; set; }

    }
    public class Associated_names
    {
        public string status { get; set; }
        public List<object> names { get; set; }

    }
    public class Associated_phone_numbers
    {
        public string status { get; set; }
        public List<object> phone_numbers { get; set; }

    }

    //email-owner validation
    public class DataMailOwnerValidation
    {
        public string emailAddress { get; set; }
    } 

    //phone-owner-validation
    public class DataPhoneOwnerValidation
    {
        public string phoneNumber { get; set; }
        public string countryCode { get; set; }
        public int dialingCode { get; set; }

    }

    //phone-risk
    public class DataPhoneRisk
    {
        public string phoneNumber { get; set; }
        public int dialingCode { get; set; }
        public string countryCode { get; set; }
        public string phoneType { get; set; }
        public string phoneTypeStatus { get; set; }
        public string riskLevel { get; set; }
        public List<RiskInsights> riskInsights { get; set; }
        public int riskScore { get; set; }
        public int maxPossibleRiskScore { get; set; }
        public string recommendation { get; set; }
        public string timezone { get; set; }
        public string lineType { get; set; }
        public string carrier { get; set; }
        public Location location { get; set; }

    }
    public class RiskInsights
    {
        public string name { get; set; }
        public string description { get; set; }
        public string impact { get; set; }
        public string type { get; set; }

    }
    public class Location
    {
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string metroCode { get; set; }
        public string county { get; set; }
        public string country { get; set; }

    }
   

    //geolocation
    public class DataGeoLocation
    {
        public double longitude { get; set; }
        public double latitude { get; set; }
        public bool vpnDetectionEnabled { get; set; }
        public string ipRestrictionEnabled { get; set; }
        public double accuracy { get; set; }
        public int timestamp { get; set; }
        public string device { get; set; }
        public string browser { get; set; }
        public string platform { get; set; }
        public string address { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public string region { get; set; }
        public string postalCode { get; set; }
        public string status { get; set; }

    }

    //Ip-Validation
    public class DataIPValitation
    {
        public string country { get; set; }
        public string countryCode { get; set; }
        public string region { get; set; }
        public string regionCode { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public bool safe { get; set; }
        public bool ipRestrictionEnabled { get; set; }
        public bool vpnDetectionEnabled { get; set; }
        public string platform { get; set; }

    }

    //Data-Liveness
    public class DataLiveness
    {
        public string videoUrl { get; set; }
        public string spriteUrl { get; set; }
        public string selfieUrl { get; set; }

    }

    //Data-Voice
    public class DataVoice
    {
        public string videoUrl { get; set; }
        public string text { get; set; }
        public string selfiePhotoUrl { get; set; }
        public string spriteUrl { get; set; }

    }

    //Data-Document-reading
    public class DataDocumetnReading
    {
        public FullNameDR fullName { get; set; }
        public DocumentNumberDR documentNumber { get; set; }
        public DateOfBirthDR dateOfBirth { get; set; }
        public ExpirationDateDR expirationDate { get; set; }
        public DocumentTypeDR documentType { get; set; }
        public FirstNameDR firstName { get; set; }
        public IssueCountryDR issueCountry { get; set; }
        public NationalityDR nationality { get; set; }
        public OptionalDR optional1 { get; set; }
        public OptionalDR optional2 { get; set; }
        public SurnameDR surname { get; set; }

    }
    public class FullNameDR
    {
        public string value { get; set; }
        public string label { get; set; }
        public bool required { get; set; }

    }
    public class DocumentNumberDR
    {
        public string value { get; set; }
        public string label { get; set; }
        public bool required { get; set; }

    }
    public class DateOfBirthDR
    {
        public string value { get; set; }
        public string label { get; set; }
        public bool required { get; set; }

    }
    public class ExpirationDateDR
    {
        public string value { get; set; }
        public string label { get; set; }
        public bool required { get; set; }

    }
    public class DocumentTypeDR
    {
        public string value { get; set; }
        public string label { get; set; }
        public bool required { get; set; }

    }
    public class FirstNameDR
    {
        public string value { get; set; }
        public string label { get; set; }
        public bool required { get; set; }

    }
    public class IssueCountryDR
    {
        public string value { get; set; }
        public string label { get; set; }
        public bool required { get; set; }

    }
    public class NationalityDR
    {
        public string value { get; set; }
        public string label { get; set; }
        public bool required { get; set; }

    }
    public class OptionalDR
    {
        public string value { get; set; }
        public string label { get; set; }
        public bool required { get; set; }

    }
    public class SurnameDR
    {
        public string value { get; set; }
        public string label { get; set; }
        public bool required { get; set; }

    }


  

}
