﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Solutions.Shared.DTOs.BP.Metamap
{
    public class Documents
    {
        public string country { get; set; }
        public string region { get; set; }
        public string type { get; set; }
        public List<Steps> steps { get; set; }
        public Fields fields { get; set; }
        public List<string> photos { get; set; }

    }
    public class DataDocuments
    {
        public int age { get; set; }
        public int ageThreshold { get; set; }
        public bool underage { get; set; }

    }
   
    public class DateOfBirth
    {
        public string value { get; set; }

    }
    public class DocumentNumber
    {
        public string value { get; set; }

    }
    public class DocumentType
    {
        public string value { get; set; }

    }
    public class EmissionDate
    {
        public string value { get; set; }

    }
    public class ExpirationDate
    {
        public string value { get; set; }

    }
    public class FirstName
    {
        public string value { get; set; }

    }
    public class FullName
    {
        public string value { get; set; }

    }
    public class IssueCountry
    {
        public string value { get; set; }

    }
    public class Nationality
    {
        public string value { get; set; }

    }
    public class Optional
    {
        public string value { get; set; }

    }
    public class RunNumber
    {
        public string value { get; set; }

    }
    public class Sex
    {
        public string value { get; set; }

    }
    public class Surname
    {
        public string value { get; set; }

    }
    public class Fields
    {
        public DateOfBirth dateOfBirth { get; set; }
        public DocumentNumber documentNumber { get; set; }
        public DocumentType documentType { get; set; }
        public EmissionDate emissionDate { get; set; }
        public ExpirationDate expirationDate { get; set; }
        public FirstName firstName { get; set; }
        public FullName fullName { get; set; }
        public IssueCountry issueCountry { get; set; }
        public Nationality nationality { get; set; }
        public Optional optional1 { get; set; }
        public Optional optional2 { get; set; }
        public RunNumber runNumber { get; set; }
        public Sex sex { get; set; }
        public Surname surname { get; set; }

    }
   
    
}
