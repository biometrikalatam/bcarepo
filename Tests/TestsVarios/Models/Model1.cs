﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsVarios.Models
{
    public class Model1
    {
        //public Computed computed { get; set; }
        public List<Steps> steps { get; set; }
    }
    public class Age
    {
        public int data { get; set; }

    }

    public class DataComputed
    {
        [JsonProperty("national-id")]
        public bool nationalid { get; set; }

    }
    public class IsDocumentExpired
    {
        [JsonProperty("data")]
        public DataComputed data { get; set; }

    }
    public class Computed
    {
        public Age age { get; set; }
        public IsDocumentExpired isDocumentExpired { get; set; }

    }

    public class Error
    {
        public string type { get; set; }
        public string code { get; set; }
        public string message { get; set; }
    }

    public class Steps
    {
        public int status { get; set; }
        public string id { get; set; }
        public string phase { get; set; }
        public object data { get; set; }
        public List<Error> error { get; set; }

    }
   

}
