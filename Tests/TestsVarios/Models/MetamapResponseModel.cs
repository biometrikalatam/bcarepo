﻿using Biometrika.Solutions.Shared.DTOs.BP.Metamap;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Biometrika.Solutions.Shared.DTOs
{
    public class MetamapResponseModel
    {
        public Computed computed { get; set; }
        public List<Documents> documents { get; set; }
        public bool expired { get; set; }
        public Flow flow { get; set; }
        public Identity identity { get; set; }
        public List<Steps> steps { get; set; }
        public bool masJobToBePostpone { get; set; }
        public string id { get; set; }
        public DeviceFingerprint deviceFingerprint { get; set; }
        public bool hasProblem { get; set; }
        //public CustomInputValues customInputValues { get; set; }
        public Metadata metadata { get; set; }

    }

    public class Metadata
    {
        public string trackid { get; set; }
    }

    public class Error
    {
        public string type { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public object details { get; set; }
    }
    
    public class Flow
    {
        public string id { get; set; }
        public string name { get; set; }

    }
    public class Identity
    {
        public string status { get; set; }

    }

    public class Steps
    {
        public int status { get; set; }
        public string id { get; set; }
        public string phase { get; set; }
        public object data { get; set; }
        public Error error { get; set; }

    }



    public class AtomicFieldParams
    {
        public string type { get; set; }
        public bool value { get; set; }

    }
    public class Fields
    {
        public AtomicFieldParams atomicFieldParams { get; set; }
        public string _id { get; set; }
        public string name { get; set; }
        public string label { get; set; }
        public string type { get; set; }
        public bool isMandatory { get; set; }
        public List<object> children { get; set; }

    }
    public class CustomInputValues
    {
        public List<Fields> fields { get; set; }
        public string country { get; set; }

    }
       
}

