﻿using BCR_AccessControl;
using BCR_WinPakProLite.Domain;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BCR_WinPakPro_Test
{
    public partial class Form1 : Form
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Form1));

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int idgen = 0;
            Card objCard;
            try
            {
                LOG.Debug("Form1.button1_Click IN...");
                objCard = new Card();
                objCard.CardHolderId = 11633;
                objCard.CardNumber = "21284415";
                objCard.RecordId = 42920;
                objCard.SpareStr1 = "2021-05-01 09:00:00";
                objCard.SpareStr2 = "2021-05-30 09:00:00";
                LOG.Debug("Form1.button1_Click - Ingresando a AccessControl.SaveAccess...");
                bool bRet = AccessControl.SaveAccess(1, objCard, textBox1.Text, out idgen);
                richTextBox1.Text = "Result = " + bRet.ToString() + "[idGen = " + idgen.ToString() + "]"; 
            }
            catch (Exception ex)
            {
                richTextBox1.Text = ex.Message;
                richTextBox1.Text = Environment.NewLine + ex.StackTrace;
                LOG.Error("Form1 Excp Error: " + ex.Message);
                LOG.Error("Form1 Excp Error: " + ex.StackTrace);
            }
            LOG.Debug("Form1.button1_Click OUT!");
        }
    }
}
