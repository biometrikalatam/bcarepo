﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BCR_WinPakPro_Test
{
    static class Program
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Program));

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            XmlConfigurator.Configure(
                    new FileInfo(Application.StartupPath + "\\Logger.cfg"));
            LOG.Info("Program.Main BCR_WinPakPro_Test Starting...");

            Application.Run(new Form1());
        }
    }
}
