﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Biometrika.VirtualPrinter.SimpleShow
{
    public partial class xfrmShow : DevExpress.XtraEditors.XtraForm
    {
        public xfrmShow()
        {
            InitializeComponent();
        }

        public void SetInfo(string xml)
        {
            this.rtbShow.Text = xml;
            Refresh();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}