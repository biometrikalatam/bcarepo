﻿// Libreria encargada de manejar el control de relés Arduino
// Se establecen funciones para:

// - Detectar puertos COM utilizados. getCOM()
// - Abrir o cerrar relés por separado. openClose(numero de relé para abrir o cerrar, estado true=abrir, false=cerrar)
// - Abrir o cerrar todos los relés. openCloseAll(true=abrir, false=cerrar)
// - Solicitar informacion a la placa. getInfoStatus(true=status, false=informacion de hardware)

// Desarrollado por Matias Arevalo
// Para Biometrika

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO.Ports;
using System.Management;
using log4net;


namespace BioArduinoRele
{
    public class ArduinoRele
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ArduinoRele));

        SerialPort ComPort = new SerialPort();

        /// <summary>
        /// Analiza los puertos COM informados para buscar el que sea de Arduino
        /// </summary>
        /// <returns>COM utilizado por la placa</returns>
        public string getCOM()
        {
            log.Debug("Analaizando puertos");
            ManagementScope connectionScope = new ManagementScope();
            log.Debug("Hacemos query al WMI");
            SelectQuery serialQuery = new SelectQuery("SELECT * FROM Win32_SerialPort"); // SELECT * FROM MSSerial_PortName
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(connectionScope, serialQuery);

            try
            {
                log.Debug("Query realizada, vemos su podemos recorrer los resultados");
                foreach (ManagementObject item in searcher.Get())
                {
                    string desc = item["Description"].ToString();
                    string deviceId = item["DeviceID"].ToString();
                    log.Debug(desc + ": " + deviceId);

                    if (desc.Contains("Arduino"))
                    {
                        log.Debug("Este contiene en la descripción Ardunio");
                        ComPort.PortName = deviceId;
                        log.Debug("Intentamos abrir el puerto: " + deviceId);

                        log.Debug("Checkeamos que el puerto este cerrado");
                        if (ComPort.IsOpen)
                        {
                            log.Debug("El puerto se encuentra abierto por otra aplicación");
                            continue;
                        }

                        ComPort.Open();
                        ComPort.Write("99");
                        System.Threading.Thread.Sleep(100);
                        string message = ComPort.ReadExisting();
                        log.Debug(message);
                        log.Debug("Formateamos la respuesta para hacerla generica");

                        string msgFormateado = message.Replace(Environment.NewLine, "");
                        log.Debug("Mensaje formateado: " + msgFormateado);

                        if (msgFormateado.CompareTo("Arduino UNO") == 0 || msgFormateado.CompareTo("Arduino RedBoard") == 0)
                        {
                            log.Debug("COM seleccionada: " + deviceId);
                            ComPort.Close();
                            return deviceId;
                        }
                        else
                        {
                            log.Debug("No se encuentra arduino");
                            log.Debug("Si arduino esta conectado y aparece en el administrador de dispositivos puede que este bugeado el puerto, para solucioanr desconecte arduino por 10 segundos o reinicie el pc donde esta conectado");
                            ComPort.Close();
                            return null;
                        }
                    }
                    else
                    {
                        log.Debug("La descripción no contiene Arduino");
                    }
                }
            }
            catch (ManagementException e)
            {
                log.Error(e.Message);
                log.Error("Si arduino esta conectado y aparece en el administrador de dispositivos puede que este bugeado el puerto, para solucioanr desconecte arduino por 10 segundos o reinicie el pc donde esta conectado");
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                log.Error(e.InnerException);
                log.Error(e);
                log.Error("Error no controlado");
            }

            return null;
        }

        /// <summary>
        /// Funcion de apertura, recive Rele a accionar y la accion a ejecutar(Abrir, cerrar)
        /// </summary>
        /// <param name="rele">Numero del rele a operar, ej: 01 para el rele 1 o 02 para el rele 2</param>
        /// <param name="accion">Accion a ejecutar, ej: 1 abrir, 0 cerrar</param>
        /// <param name="msgError">Msg de vuelta en el caso de errores</param>
        /// <returns>Retorna el msg entregado por la placa</returns>
        public string openClose(string rele, string accion, out string msgError)
        {
            msgError = "";
            string resultado = "";

            try
            {
                log.Debug("Intentamos abrir el puerto: " + ComPort.PortName);
                ComPort.Open();
                log.Debug("Puerto abierto, enviamos: " + rele + accion);
                ComPort.Write(rele + accion);
                System.Threading.Thread.Sleep(100);

                resultado = ComPort.ReadExisting();
                ComPort.Close();
                log.Debug("Escuchamos: " + resultado);
            }
            catch (Exception ex)
            {
                resultado = ex.Message;
                log.Error(ex.Message);
            }

            return resultado;
        }

        /// <summary>
        /// Funcion de apertura con timer, se le entrega el valor del rele y el tiempo que este estara abierto
        /// </summary>
        /// <param name="rele">Rele a operar, ej: 010 rele 1, 020 rele 2</param>
        /// <param name="timer">Tiempo de apertura en milisegundo</param>
        /// <param name="msgError">Msg de error si lo hubiera</param>
        /// <returns>Retorna msg de la placa arduino</returns>
        public string openDelay(string rele, string timer, out string msgError)
        {
            string resultado = "";
            msgError = "";

            try
            {
                log.Debug("Intentamos abrir el puerto: " + ComPort.PortName);
                ComPort.Open();
                log.Debug("Puerto abierto, enviamos pulso al rele: " + rele + ", tiempo: " + timer);
                ComPort.Write(rele + timer);
                System.Threading.Thread.Sleep(100);

                resultado = ComPort.ReadExisting();
                ComPort.Close();
                log.Debug("Escuchamos: " + resultado);
            }
            catch (Exception ex)
            {
                resultado = ex.Message;
                log.Error(ex.Message);
            }

            return resultado;
        }

        /// <summary>
        /// Funcion para abrir o cerrar todos los reles al mismo tiempo
        /// </summary>
        /// <param name="accion">Accion a realizar, ej: true para brir, false para cerrar</param>
        /// <param name="msgError">Msg de error si lo hubiera</param>
        /// <returns>Retorna msg de la placa arduino</returns>
        public string openCloseAll(bool accion, out string msgError)
        {
            string resultado = "";
            msgError = "";
            string accionAux = "";
            string que = "";

            if (accion)
            {
                que = "Abrir";
                accionAux = "981";
            }
            else
            {
                que = "Cerrar";
                accionAux = "980";
            }

            try
            {
                log.Debug("Intentamos abrir el puerto: " + ComPort.PortName);
                ComPort.Open();
                log.Debug("Puerto abierto, enviamos " + que + " todo: " + accionAux);
                ComPort.Write(accionAux);
                System.Threading.Thread.Sleep(100);

                resultado = ComPort.ReadExisting();
                ComPort.Close();
                log.Debug("Escuchamos: " + resultado);
            }
            catch (Exception ex)
            {
                resultado = ex.Message;
                log.Error(ex.Message);
            }

            return resultado;
        }

        /// <summary>
        /// Funcion que entrega la informacion del hardware de la placa o su estado
        /// </summary>
        /// <param name="accion">Accion a realizar, ej: true para informacion, false para status</param>
        /// <param name="msgError">Msg de error si lo hubiera</param>
        /// <returns>Retorna msg de la placa arduino</returns>
        public string getInfoStatus(bool accion, out string msgError)
        {
            string resultado = "";
            msgError = "";
            string accionAux = "";
            string que = "";
            int sleepTime = 0;

            if (accion)
            {
                que = "Info";
                accionAux = "86";
                sleepTime = 1000;
            }
            else
            {
                que = "Status";
                accionAux = "87";
                sleepTime = 100;
            }

            try
            {
                log.Debug("Intentamos abrir el puerto: " + ComPort.PortName);
                ComPort.Open();
                log.Debug("Puerto abierto, enviamos " + que + ": " + accionAux);
                ComPort.Write(accionAux);
                System.Threading.Thread.Sleep(sleepTime);

                resultado = ComPort.ReadExisting();
                ComPort.Close();
                log.Debug("Escuchamos: " + resultado);
            }
            catch (Exception ex)
            {
                resultado = ex.Message;
                log.Error(ex.Message);
            }

            return resultado;
        }
    }
}
