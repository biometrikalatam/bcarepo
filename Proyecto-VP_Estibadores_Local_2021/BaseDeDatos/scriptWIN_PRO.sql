USE [WIN-PAK PRO]
GO

/****** Object:  Table [dbo].[History]    Script Date: 21/05/2021 17:18:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[History](
	[RecordID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NULL CONSTRAINT [DF_History_AccountID]  DEFAULT (1),
	[TimeStamp] [datetime] NULL,
	[UserID] [int] NULL,
	[NodeID] [int] NULL,
	[Deleted] [tinyint] NULL CONSTRAINT [DF_History_Deleted]  DEFAULT (0),
	[UserPriority] [smallint] NULL,
	[RecvTime] [datetime] NULL,
	[GenTime] [datetime] NULL,
	[SeqID] [int] NULL,
	[Type1] [smallint] NULL,
	[Type2] [smallint] NULL,
	[Param1] [smallint] NULL,
	[Param2] [smallint] NULL,
	[Param3] [nvarchar](30) NULL,
	[Link1] [int] NULL,
	[Link2] [int] NULL,
	[Link3] [int] NULL,
	[Link4] [int] NULL,
	[SpareW1] [smallint] NULL CONSTRAINT [DF_History_SpareW1]  DEFAULT (0),
	[SpareW2] [smallint] NULL CONSTRAINT [DF_History_SpareW2]  DEFAULT (0),
	[SpareW3] [smallint] NULL CONSTRAINT [DF_History_SpareW3]  DEFAULT (0),
	[SpareW4] [smallint] NULL CONSTRAINT [DF_History_SpareW4]  DEFAULT (0),
	[SpareDW1] [int] NULL CONSTRAINT [DF_History_SpareDW1]  DEFAULT (0),
	[SpareDW2] [int] NULL CONSTRAINT [DF_History_SpareDW2]  DEFAULT (0),
	[SpareDW3] [int] NULL CONSTRAINT [DF_History_SpareDW3]  DEFAULT (0),
	[SpareDW4] [int] NULL CONSTRAINT [DF_History_SpareDW4]  DEFAULT (0),
	[SpareStr1] [nvarchar](30) NULL,
	[SpareStr2] [nvarchar](30) NULL,
	[Note] [image] NULL,
 CONSTRAINT [PK_History_1__10] PRIMARY KEY CLUSTERED 
(
	[RecordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


