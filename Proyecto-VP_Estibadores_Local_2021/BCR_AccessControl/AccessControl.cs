﻿using BCR_WinPakPro;
using BCR_WinPakPro.Domain;
using BCR_WinPakProLite;
//using Common.Logging;
using log4net;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Text;

namespace BCR_AccessControl
{
    public class AccessControl
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(AccessControl));

        public static BCR_WinPakProLite.Domain.Card giveAccessControl(string rut, string fecha, string[] format, string connectionStringLocal)
        {
            try
            {
                log.Debug("AccessControl.giveAccessControl IN..."); //- Primero analizamos la fecha de caducidad de la tarjeta: " + fecha);
                log.Debug("Formatos de fecha configurados: " + string.Join(", ", format));
                // primero vigencia
                //DateTime endDate = (DateTime)SqlDateTime.MinValue;
                //if (!DateTime.TryParseExact(fecha, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate))
                //    endDate = (DateTime)SqlDateTime.MinValue;

                //log.Debug("Fecha formateada: " + endDate.ToString("dd\\/MM\\/yyyy"));

                //if (endDate < DateTime.Now)
                //{
                //    log.Debug("La fecha de caducidad de la tarjeta es menor a la fecha de hoy");
                //    return null;
                //}

                log.Debug("AccessControl.giveAccessControl - OpenSession en BD...");
                using (var session = NHibernateHelperLocal.OpenSession(connectionStringLocal))
                {
                    log.Debug("AccessControl.giveAccessControl - Buscamos el rut en base de datos => " + rut);
                    BCR_WinPakProLite.Domain.Card card = session.CreateCriteria(typeof(BCR_WinPakProLite.Domain.Card))
                                .Add(Restrictions.Eq("CardNumber", rut))
                                .UniqueResult<BCR_WinPakProLite.Domain.Card>();

                    if (card == null)
                    {
                        log.Debug("AccessControl.giveAccessControl - El rut: " + rut + " no existe en base de datos");
                        return null;
                    }

                    DateTime fechasEntrada = Convert.ToDateTime(card.SpareStr1.Trim());
                    DateTime fechasSalida = Convert.ToDateTime(card.SpareStr2.Trim());

                    if (DateTime.Now >= fechasEntrada && DateTime.Now <= fechasSalida)
                    {
                        log.Debug("AccessControl.giveAccessControl - Rut encontrado en base de datos, se da acceso...");
                        log.Debug("AccessControl.giveAccessControl - Estibador dentro del rango de fechas, se permite el acceso => " +
                                    "FechaMin=" + fechasEntrada.ToString("ddMMyyyy") +
                                    "/Now=" + DateTime.Now.ToString("ddMMyyyy") +
                                    "/FechaMax=" + fechasSalida.ToString("ddMMyyyy"));
                        return card;
                    }
                    else
                    {
                        log.Info("AccessControl.giveAccessControl - Estibador fuera del rango de fechas, no se permite acceso => " +
                                    "FechaMin=" + fechasEntrada.ToString("ddMMyyyy") +
                                    "/Now=" + DateTime.Now.ToString("ddMMyyyy") +
                                    "/FechaMax=" + fechasSalida.ToString("ddMMyyyy"));
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("AccessControl.giveAccessControl - Excp:" + e.Message);
                return null;
            }
        }

        public static bool SaveAccess(short typeNumberMarc, BCR_WinPakProLite.Domain.Card objCard, string connectionStringDC, out int idgenerado)
        {
            bool res = false;
            DateTime fechaActual;
            //Iniciamos la fecha de marcación
            fechaActual = DateTime.Now;
            idgenerado = -1;
            
            try
            {
                log.Debug("AccessControl.SaveAccess IN...");
                log.Debug("AccessControl.SaveAccess - OpenSession en BD...");
                using (var session = NHibernateHelperDC.OpenSession(connectionStringDC))
                {
                    History dataToSave = new History();
                    //Validar esto.
                    dataToSave.Link1 = typeNumberMarc;
                    dataToSave.Link3 = objCard.CardHolderId;
                    dataToSave.Link2 = objCard.RecordId;
                    dataToSave.Param1 = 1;
                    dataToSave.Param2 = typeNumberMarc;
                    dataToSave.Param3 = objCard.CardNumber;
                    dataToSave.RecvTime = fechaActual;
                    dataToSave.Type1 = 9;
                    dataToSave.Type2 = 0;
                    dataToSave.UserPriority = 0;
                    dataToSave.NodeId = 0;
                    dataToSave.UserId = 0;
                    dataToSave.AccountId = 0;
                    dataToSave.Link4 = 0;
                    dataToSave.GenTime = fechaActual;
                    dataToSave.TimeStamp = fechaActual;
                    dataToSave.SpareStr1 = objCard.SpareStr1;
                    dataToSave.SpareStr2 = objCard.SpareStr2;
                    log.Debug("Entra a Grabar => " + HistoryToString(dataToSave));
                    int id = (int)session.Save(dataToSave);
                    session.Flush();
                    log.Info("AccessControl.SaveAccess Se graba el evento en history");
                    idgenerado = id;
                    log.Info("El id de history agregado es:" + idgenerado);
                    //Buscamos el último history y actualizamos la tabla.
                    dataToSave = session.CreateCriteria(typeof(History)).Add(Expression.Eq("RecordId", idgenerado)).UniqueResult<History>();
                    if (dataToSave != null)
                    {
                        dataToSave.SeqId = idgenerado;
                        log.Debug("AccessControl.SaveAccess - session.Update en BD...");
                        session.Update(dataToSave);
                        session.Flush();
                    }
                    res = true;
                }
            }
            catch (Exception exe)
            {
                res = false;
                log.Error("AccessControl.SaveAccess - Excp Se produjo error al grabar el history", exe);
            }
            log.Debug("AccessControl.SaveAccess OUT! => Resultado Save = " + res.ToString());

            return res;
        }

        public static string HistoryToString(History history)
        {
            string ret = "";
            try
            {
                ret = "RecordId=" + history.RecordId.ToString() + "|" +
                    "SeqId=" + history.SeqId.ToString() + "|" +
                    "Link1=" + history.Link1.ToString() + "|" +
                    "Link2=" + history.Link2.ToString() + "|" +
                    "Link3=" + history.Link3.ToString() + "|" +
                    "Link4=" + history.Link4.ToString() + "|" +
                    "Param1=" + history.Param1.ToString() + "|" +
                    "Param2=" + history.Param2.ToString() + "|" +
                    "Param3=" + history.Param3.ToString() + "|" +
                    "RecvTime=" + history.RecvTime.ToString("dd/MM/yyyy HH:mm:ss") + "|" +
                    "Type1=" + history.Type1.ToString() + "|" +
                    "Type2=" + history.Type2.ToString() + "|" +
                    "UserPriority=" + history.UserPriority.ToString() + "|" +
                    "NodeId=" + history.NodeId.ToString() + "|" +
                    "UserId=" + history.UserId.ToString() + "|" +
                    "AccountId=" + history.AccountId.ToString() + "|" +
                    "GenTime=" + history.GenTime.ToString("dd/MM/yyyy HH:mm:ss") + "|" +
                    "TimeStamp=" + history.TimeStamp.ToString("dd/MM/yyyy HH:mm:ss") + "|" +
                    "SpareStr1=" + history.SpareStr1.ToString() + "|" +
                    "SpareStr2=" + history.SpareStr2.ToString();
            }
            catch (Exception ex)
            {
                ret = "";
                //LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }
    }
}
