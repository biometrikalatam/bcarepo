/*

insert license info here

*/

using System;
using System.Data.SqlTypes;

namespace BCR.WebAssistance.Infraestructura
{
	/// <summary>
	///	Generated by MyGeneration using the Serdar's NHibernate Object Mapping 1.1 template (based on Gustavo's) - serdar@argelab.net
	/// </summary>
	[Serializable]
	public class IntentosAccesoProhibidosVehiculos 
	{
		#region Private Members
		private bool _isChanged;
		private bool _isDeleted;
		private int _id; 
		private string _patente; 
		private DateTime _fecha; 		
		#endregion
		
		#region Default ( Empty ) Class Constuctor
		/// <summary>
		/// default constructor
		/// </summary>
		public IntentosAccesoProhibidosVehiculos()
		{
			_id = 0; 
			_patente = null;
            _fecha = (DateTime)SqlDateTime.MinValue;
		}
		#endregion // End of Default ( Empty ) Class Constuctor

		#region Public Properties
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int Id
		{
			get { return _id; }
			set { _isChanged |= (_id != value); _id = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual string Patente
		{
			get { return _patente; }
			set	
			{
				if ( value != null )
					if( value.Length > 50)
						throw new ArgumentOutOfRangeException("Invalid value for Patente", value, value.ToString());
				
				_isChanged |= (_patente != value); _patente = value;
			}
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual DateTime Fecha
		{
			get { return _fecha; }
            set
            {
                if (value < (DateTime)SqlDateTime.MinValue)
                {
                    _isChanged |= (_fecha != value); _fecha = (DateTime)SqlDateTime.MinValue;
                }
                else
                {
                    _isChanged |= (_fecha != value); _fecha = value;
                }
            }
		}
			
		/// <summary>
		/// Returns whether or not the object has changed it's values.
		/// </summary>
		public virtual bool IsChanged
		{
			get { return _isChanged; }
		}
		
		/// <summary>
		/// Returns whether or not the object has changed it's values.
		/// </summary>
		public virtual bool IsDeleted
		{
			get { return _isDeleted; }
		}
		
		#endregion 
		
		
		#region Public Functions
		
		/// <summary>
		/// mark the item as deleted
		/// </summary>
		public virtual void MarkAsDeleted()
		{
			_isDeleted = true;
			_isChanged = true;
		}
		
		
		#endregion
		
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			IntentosAccesoProhibidosVehiculos castObj = (IntentosAccesoProhibidosVehiculos)obj; 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
}
