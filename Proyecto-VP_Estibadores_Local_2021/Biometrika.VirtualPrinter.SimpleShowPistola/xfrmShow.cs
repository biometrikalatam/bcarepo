﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Biometrika.VirtualPrinter.SimpleShow
{
    public partial class xfrmShow : Form //DevExpress.XtraEditors.XtraForm
    {
        public xfrmShow()
        {
            InitializeComponent();
        }

        public void SetInfo(string xml)
        {
            this.rtbShow.Text = xml;
            Refresh();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}