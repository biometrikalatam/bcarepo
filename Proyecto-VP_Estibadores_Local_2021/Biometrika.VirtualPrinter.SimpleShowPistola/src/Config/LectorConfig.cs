﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biometrika.VirtualPrinter.SimpleShow.src.Config
{
    public class LectorConfig
    {
        private bool _parseCedula = false;               
        private int _typemark = 0;       
        private String _numberelay = "";
        private int _timeoff = 10;
        private String _comport = "";
        private string _dateFormat = "";
        private string connectionString { get; set; }
        private string connectionStringDC { get; set; }
        private int _sentido = 1; //1-Entrada | 2-Salida
        private bool _checkBusinessRuleInOut = false; //true-mismo chequeo que en acceso | false-No chequea, solo abre y genera marca en BD

        public int Sentido
        {
            get { return _sentido; }
            set { _sentido = value; }
        }
        public bool CheckBusinessRuleInOut
        {
            get { return _checkBusinessRuleInOut; }
            set { _checkBusinessRuleInOut = value; }
        }

        public bool ParseCedula
        {
            get { return _parseCedula; }
            set { _parseCedula = value; }
        }
        public short TimeOff
        {
            get { return (short)_timeoff; }
            set { _timeoff = (short)value; }
        }
        public String NumberRelay
        {
            get { return _numberelay; }
            set { _numberelay = value; }
        }
        public int TypeMark
        {
            get { return _typemark; }
            set { _typemark = value; }
        }
        public String ComPort
        {
            get { return _comport; }
            set { _comport = value; }
        }

        public string DateFormat
        {
            get { return _dateFormat; }
            set { _dateFormat = value; }
        }

        public string ConnectionString
        {
            get { return connectionString; }
            set { connectionString = value; }
        }

        public string ConnectionStringDC
        {
            get { return connectionStringDC; }
            set { connectionStringDC = value; }
        }
    }
}
