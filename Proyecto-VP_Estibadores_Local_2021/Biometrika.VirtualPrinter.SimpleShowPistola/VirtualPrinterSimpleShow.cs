﻿using System;
using Biometrika.VirtualPrinter.Api;
using log4net;
//using NHibernate;
using Biometrika.VirtualPrinter.SimpleShow.src.Config;
using System.IO;
using System.Timers;
using System.Collections;
using BCR_AccessControl;
using Biometrika.VirtualPrinter.SimpleShowPistola.src.Helpers;

namespace Biometrika.VirtualPrinter.SimpleShow
{
    public class VirtualPrinterSimpleShow : IVirtualPrinter
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(VirtualPrinterSimpleShow));
        //internal LectorConfig CONFIG;
        private Timer _timer;
        //private  ISession _session;
        //private ISessionFactory sessionFactory;
        static private BioArduinoRele.ArduinoRele objArduinoRele;
        private short port;
        private short qRelay;
        private short milisec;
        private string idr;
        private short reti;

#region Implementation of IVirtualPrinter

        private string _name;
        private IList _cardExpress;
        private LectorConfig _Config;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public IList CardExpress
        {
            get { return _cardExpress; }
            set { _cardExpress = value; }
        }

        public object Config
        {
            get { return _Config; }
            set { _Config = (LectorConfig)value; }
        }

        public VirtualPrinterSimpleShow()
        {
            String comport = "";
            String msgErr = "";
            //Prodríamos tomar el archivo de configuración de virtual printer y leer los parámetros.
            //var uri = new UriBuilder(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
            //String pathassembly= Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
            //CONFIG =
            //        SerializeHelper.DeserializeFromFile<LectorConfig>(pathassembly + "\\LectorPistolaConfigOut.cfg");

            objArduinoRele = new BioArduinoRele.ArduinoRele();
            objArduinoRele.getCOM();

        }

        /// <summary>
        /// Inicializa la clase con los parametros que necesite para su ejecucion, de tal forma
        /// de acelerar los procesos. Puede leer conexion a BD, configuraciones expecificas,
        /// chequear conexiones, etc.
        /// </summary>
        /// <param name="xmlinput">xml con datos input - </param>
        /// <param name="xmloutput">xml con datos de output</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Initialization(string xmlinput, out string xmloutput)
        {
            xmloutput = null;
            try
            {
                LOG.Debug("VirtualPrinterSimpleShow.Initialization IN...");
                var uri = new UriBuilder(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
                string pathassembly = Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
                LOG.Debug("VirtualPrinterSimpleShow.Initialization Leyendo config en: " +
                            pathassembly + "\\" + xmlinput + "...");
                Config = BioCore.SerialComm.SerializeHelper.DeserializeFromFile<LectorConfig>(pathassembly + "\\" +
                        xmlinput); // LectorPistolaConfigOut.cfg");
            }
            catch (Exception ex)
            {
                LOG.Error("VirtualPrinterSimpleShow.Initialization- Excp Error: " + ex.Message);
                LOG.Debug("VirtualPrinterSimpleShow.Initialization OUT! => CONFIG == Null");
                return Errors.IERR_UNKNOWN;
            }
            LOG.Debug("VirtualPrinterSimpleShow.Initialization OUT! => CONFIG != null => " + 
                        (Config!= null).ToString());
            return Errors.IERR_OK;
        }

        /// <summary>
        /// Procesa la muestra serial entregada como parámetro
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="xmloutput">xml con datos de output</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Process(string xmlinput, out string xmloutput)
        {
            int ret = Errors.IERR_OK;
            xmloutput = null;
            Parameters paramin;
            String msgErr = "";
            bool res = false;
           
            try
            {
                LOG.Debug("VirtualPrinterSimpleShow.Process - Inicio de proceso de lectura por:" + _name);
                //0.- Chequo parametros entrada
                if (String.IsNullOrEmpty(xmlinput))
                {
                    LOG.Error("VirtualPrinterSimpleShow.Process Error - Prametro de entrada nulo");
                    return Errors.IERR_BAD_PARAMETER;
                }

                //1. Parseo parametros
                paramin = Parameters.DeserializeFromXml(xmlinput);
                if (paramin == null)
                {
                    LOG.Error("VirtualPrinterSimpleShow.Process Error - DeserializeFromXml retorna nulo");
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- TODO - Comienza el proceso especifico con la muestra
                if (paramin.GetValue("COM").ToString().Equals(((LectorConfig)Config).ComPort) == false) 
                {
                    LOG.Debug("VirtualPrinterSimpleShow.Process - Sale por diferencia de COM => " +
                        " COM Config = " + ((LectorConfig)Config).ComPort + " != COM Procesado = " + paramin.GetValue("COM").ToString());
                    ret = -1;
                }
                if (ret != -1)
                {
                    LOG.Debug("VirtualPrinterSimpleShow.Process - Recupera sample a procesar...");
                    byte[] sample = Convert.FromBase64String((string)paramin.GetValue("Sample"));
                    LOG.Debug("VirtualPrinterSimpleShow.Process - saple != null => " + (sample != null).ToString());
                   

                    /* Cambio por cedula - 21-05-2021
                             // Actualización 06-07-2018
                             // Leer codigos de terceros, el primer caracter es un #: #0888483516072019
                             // lectura = "0888483516072019";
                             LOG.Info("Se trata de un codigo 2D #: " + lectura);
                             string rut = lectura.Substring(0, 8);
                             if (rut.Substring(0, 1).CompareTo("0") == 0)
                             {
                                 LOG.Debug("Si el primer caracter es un 0 lo eliminamos");
                                 rut = rut.Substring(1, rut.Length - 1);
                             }
                             string[] format = CONFIG.DateFormat.Split(new string[] { "," }, StringSplitOptions.None);
                             string fecha = lectura.Substring(8, lectura.Length - 8);      
                    */

                    LOG.Info("VirtualPrinterSimpleShow.Process - Parsea codigo de barra leido...");
                    string rut = CedulaHelper.GetRUT(sample);
                    if (string.IsNullOrEmpty(rut))
                    {
                        LOG.Error("VirtualPrinterSimpleShow.Process - rut recuperado de lecutra == Null => Sale...");
                    }
                    else
                    {

                        string[] format = ((LectorConfig)Config).DateFormat.Split(new string[] { "," }, StringSplitOptions.None);
                        //Tomo la fecha como venia en la tarjeta ddmmyyyy
                        string fecha = DateTime.Now.ToString("ddMMyyyy"); // lectura.Substring(8, lectura.Length - 8);

                        LOG.Debug("VirtualPrinterSimpleShow.Process - Check " + 
                                    (((LectorConfig)Config).Sentido==1?"ENTRADA":"SALIDA") + " para RUT = " + rut +
                            " - Fecha = " + fecha + "...");
                        bool bOpenAndSave = false;
                        BCR_WinPakProLite.Domain.Card result = null;
                        if (((LectorConfig)Config).Sentido == 1 ||
                            (((LectorConfig)Config).Sentido == 2 && ((LectorConfig)Config).CheckBusinessRuleInOut))
                        {
                            result = AccessControl.giveAccessControl(rut, fecha, format,
                                                                    ((LectorConfig)Config).ConnectionString.ToString());
                            bOpenAndSave = false;
                            if (result == null)
                                LOG.Debug("VirtualPrinterSimpleShow.Process - Acceso denegado por result == null");
                            else
                            {
                                bOpenAndSave = true;
                            }
                        } else
                        {
                            //TODO  - Implemetar obtener esta card apra poder grabar la salida cuando no se chequea CheckBusinessRuleInOut
                            //result = AccessControl.GetCard(rut, ((LectorConfig)Config).ConnectionString.ToString());
                            //if (result == null)
                            //    LOG.Debug("VirtualPrinterSimpleShow.Process - Salida denegada por result == null");
                            //else
                            //{ 
                                bOpenAndSave = true;
                            //}
                        }
                        if (!bOpenAndSave)
                            LOG.Debug("VirtualPrinterSimpleShow.Process - Acceso denegado por result == null");
                        else
                        {
                            LOG.Debug("VirtualPrinterSimpleShow.Process - Acceso permitido a rut: " + rut + ", apertura por rele: " +
                                ((LectorConfig)Config).NumberRelay.ToString());
                            if (objArduinoRele == null)
                            {
                                LOG.Debug("VirtualPrinterSimpleShow.Process - Crea objArduino...");
                                objArduinoRele = new BioArduinoRele.ArduinoRele();
                                objArduinoRele.getCOM();
                            }
                            LOG.Debug("VirtualPrinterSimpleShow.Process - openDelay de " +
                                ((LectorConfig)Config).NumberRelay.ToString() + " por " + 
                                ((LectorConfig)Config).TimeOff.ToString() + " milisegundos...");
                                                                                    // "1000"
                            objArduinoRele.openDelay(((LectorConfig)Config).NumberRelay.ToString(), 
                                                     ((LectorConfig)Config).TimeOff.ToString(), out msgErr);
                            //DateTime CONFIG2.TypeMarkopenDate = DateTime.Now;

                            int idGenerado = 0;
                            LOG.Debug("VirtualPrinterSimpleShow.Process - Ingresa a grabar marca...");
                            bool resSave = AccessControl.SaveAccess((Int16)((LectorConfig)Config).TypeMark, result,
                                                                    ((LectorConfig)Config).ConnectionStringDC, out idGenerado);
                            LOG.Debug("VirtualPrinterSimpleShow.Process - Resultado grabacion => " + resSave.ToString() + 
                                      " - Id generado con la marca: " + idGenerado);
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                LOG.Error("VirtualPrinterSimpleShow.Process Error", ex);
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }
      
        private int OpenDoor()
        {
            int res = 0;
            int resrelay = 0;
            /*USBRelayAxNoGUI.USBRelayAxDllClass g_axUSBRelayAx = new USBRelayAxDllClass();
            
            string idr = "1234";
            short port = (short)CONFIG.ComPort;
            short qRelay = 4;
            short milisec = (short)CONFIG.TimeRelay;*/
            /*(try
            {
                short reti = g_axUSBRelayAx.InitComponent(ref idr, ref port, ref qRelay);
                if (reti == 0)
                {*/
                    
                    LOG.Info("USBRelay Initialized!:" + milisec.ToString());
                    res = 0;
                  
                /*}
                else
                {
                    LOG.Info("USBRelay Initialized ERROR [" + reti.ToString() + "]");
                    res = -2;
                }
            }
            catch (Exception exe)
            {
                LOG.Error("Se produjo un error al realizar la apertura de la barrera:" + exe.Message, exe);
                res = -1;
            }
            return res;*/
                    return res;
        }

        private String FormattingTaxid(string rut)
        {
            int cont = 0;
            String format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {

                    format = rut.Substring(i, 1) + format;

                    cont++;

                }
                return format;
            }
        }
        private void CloseRele()
        {
            try
            {
                LOG.Info("Mandamos a cerrar los relays");
              
                //Ojo que estamos cerrando solo 4 rele en la barrera de estacionamiento.
                /*g_axUSBRelayAx.OffRelay(ref idr, 3);
                g_axUSBRelayAx.OffRelay(ref idr, 4);*/
                LOG.Info("Cerrar el relay");
            }
            catch (Exception exe)
            {
                LOG.Error("Error en Cerrar Rele:" + exe.Message, exe);
            }
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //LOG.Info("Se activa el temporadizador del rele");
            //int res = 0;
            //String msgErr = "";
            //objArduinoRele.openCloseAll(false, out msgErr);
            ////objArduinoRele.openDelay(CONFIG.NumberRelay.ToString(), CONFIG.TimeOff.ToString(), out msgErr);
            //objArduinoRele.openDelay(CONFIG.NumberRelay.ToString(), "1000", out msgErr);  
            //_timer.Enabled = false;
            //LOG.Info("Se desactiva temporarizador del rele");
        }

        public string ConfigToString()
        {
            string ret = "";
            try
            {
                if (Config != null)
                {
                    LectorConfig conf = (LectorConfig)Config;
                    ret = "ComPort=" + conf.ComPort + "|" +
                          "TypeMark=" + conf.TypeMark + "|" +
                          "NumberRelay=" + conf.NumberRelay + "|" +
                          "TimeOff=" + conf.TimeOff + "|" +
                          "DateFormat=" + conf.DateFormat;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("VirtualPrinterSimpleShow.ConfigToString - Excp Error: " + ex.Message);
            }
            return ret;
        }
        #endregion Implementation of IVirtualPrinter



    }
}
