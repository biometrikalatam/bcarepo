﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biometrika.VirtualPrinter.SimpleShow.src.Config
{
    public class LectorConfig
    {
        private bool _parseCedula = false;             
        private int _typemark = 0;       
        private String _numberelay = "";
        private int _timeoff = 10;     
        private String _comport="";

        public bool ParseCedula
        {
            get { return _parseCedula; }
            set { _parseCedula = value; }
        }        
        public short TimeOff
        {
            get { return (short)_timeoff; }
            set { _timeoff = (short)value; }
        }
        public String NumberRelay
        {
            get { return _numberelay; }
            set { _numberelay = value; }
        }        
        public int TypeMark
        {
            get { return _typemark; }
            set { _typemark = value; }
        }
        public String ComPort
        {
            get { return _comport; }
            set { _comport = value; }
        }
    }
}
