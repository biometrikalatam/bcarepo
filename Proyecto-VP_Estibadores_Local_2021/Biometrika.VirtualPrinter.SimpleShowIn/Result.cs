﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biometrika.VirtualPrinter.SimpleShowIn
{
    public class Result
    {
        private String name;
        private String patherlastname;
        private String motherlastname;
        private String hasparking;
        private int parkincontract;
        private int gruoupparking;
        private int parkinginuse;
        private int status;
        private String mensaje;
        private int isvisit;
        private int block;
        private String valueid;
        private String titular;
        private String codetitular;
        private int initvisit = 0;
        private int endvisit = 0;
        private String replace = "-";

        public String Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public String MotherLastName
        {
            get
            {
                return motherlastname;
            }
            set
            {
                motherlastname = value;
            }
        }
        public String PatherLastName
        {
            get
            {
                return patherlastname;
            }
            set
            {
                patherlastname = value;
            }

        }
        public String HasParking
        {
            get
            {
                return hasparking;
            }
            set
            {
                hasparking = value;
            }

        }
        public int GroupParking
        {
            get
            {
                return gruoupparking;
            }
            set
            {
                gruoupparking = value;
            }
        }
        public int ParkingContract
        {
            get
            {
                return parkincontract;
            }
            set
            {
                parkincontract = value;
            }
        }
        public int ParkingInUse
        {
            get
            {
                return parkinginuse;
            }
            set
            {
                parkinginuse = value;
            }
        }
        public int Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        public String Mensaje
        {
            get
            {
                return mensaje;
            }
            set
            {
                mensaje = value;
            }

        }
        public int isVisit
        {
            get
            {
                return isvisit;
            }
            set
            {
                isvisit = value;
            }
        }
        public int isBlock
        {
            get
            {
                return block;
            }
            set
            {
                block = value;
            }
        }
        public String ValueId
        {
            get
            {
                return valueid;
            }
            set
            {
                valueid = value;
            }
        }
        public String Titular
        {
            get
            {
                return titular;
            }
            set
            {
                titular = value;
            }
        }
        public String CodeTitular
        {
            get
            {
                return codetitular;
            }
            set
            {
                codetitular = value;
            }
        }
        public int InitVisit
        {
            get
            {
                return initvisit;
            }
            set
            {
                initvisit = value;
            }
        }
        public int EndVisit
        {
            get
            {
                return endvisit;
            }
            set
            {
                endvisit = value;
            }
        }
        public String Replace
        {
            get
            {
                return replace;
            }
            set
            {
                replace = value;
            }
        }
    }
}
