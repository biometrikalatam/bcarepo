using System;
using System.Collections;
using System.Data;
using NHibernate;

namespace Bio.EPAustral.Core {
	/// <summary>
	/// Clase destinada al manejo de sessiones Hibernate. Esta clase es heredada
	/// por cada una de las clases Persistentes a utilizar (ej.: TB_IVR_OT).
	/// </summary>
	public class HPersistent : IDisposable {

		[ThreadStatic]
		public static ISession _session;

		//[ThreadStatic]
		//		public static IDbConnection _conx;


		static HPersistent() {
			//			if (_conx != null)
			//			{
			//				_session = HSessionFactory._sessionFactory.OpenSession(_conx);
			//			} else
			//			{
			_session = HSessionFactory._sessionFactory.OpenSession();
			//				_conx = _session.Connection; 
			//			}

			//_session.FlushMode = FlushMode.Never;
		}

		public HPersistent() {}


		// Track whether Dispose has been called.
		private bool disposed = false;

		// Implement IDisposable.
		// Do not make this method virtual.
		// A derived class should not be able to override this method.
		public void Dispose() {
			Dispose(true);
			// Take yourself off the Finalization queue 
			// to prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize(this);
		}


		public

			// Dispose(bool disposing) executes in two distinct scenarios.
			// If disposing equals true, the method has been called directly
			// or indirectly by a user's code. Managed and unmanaged resources
			// can be disposed.
			// If disposing equals false, the method has been called by the 
			// runtime from inside the finalizer and you should not reference 
			// other objects. Only unmanaged resources can be disposed.
			virtual void Dispose(bool disposing) {
			// Check to see if Dispose has already been called.
			if(!this.disposed) {
				// If disposing equals true, dispose all managed 
				// and unmanaged resources.
				if(disposing) {
					if (_session != null) {
						_session.Close();
					}
				}
				// Release unmanaged resources. If disposing is false, 
				// only the following code is executed.
				//				CloseHandle(handle);
				//				handle = IntPtr.Zero;
				// Note that this is not thread safe.
				// Another thread could start disposing the object
				// after the managed resources are disposed,
				// but before the disposed flag is set to true.
				// If thread safety is necessary, it must be
				// implemented by the client.

			}
			disposed = true;         
		}

		// Use C# destructor syntax for finalization code.
		// This destructor will run only if the Dispose method 
		// does not get called.
		// It gives your base class the opportunity to finalize.
		// Do not provide destructors in types derived from this class.
		~HPersistent() {
			if (_session != null) {
				_session.Close();
			}
			// Do not re-create Dispose clean-up code here.
			// Calling Dispose(false) is optimal in terms of
			// readability and maintainability.
			Dispose(false);
		}

		/* **************************************************************
		 * Funciones de ayuda para NHibernate. Queries, transformacion 
		 * a DataView o HashTable.
		***************************************************************** */
		public static DataView GetDataView(Type type, IList list, DictionaryEntry[] colOrder) {
			DataRow dr;
			DataTable dt = new DataTable();
			try {
				for (int i=0; i < colOrder.Length; i++)
					dt.Columns.Add( new DataColumn( (string) colOrder[i].Value ) );

				foreach (object obj in list) {
					dr = dt.NewRow();
					for (int i=0; i < colOrder.Length; i++) {
						dr[(string)colOrder[i].Value] = type.
							GetProperty((string)colOrder[i].Key)
							.GetValue(obj,null); 						
					}
					dt.Rows.Add(dr);
				}
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
				return null;
			}
			return new DataView( dt );
		}

		public static DataView GetDataView(string query, DictionaryEntry[] colOrder) {
			DataRow dr;
			DataTable dt = new DataTable();
			try {
				IQuery qy = _session.CreateQuery(query);
				IList lRes = qy.List();

				for (int i=0; i < colOrder.Length; i++)
					dt.Columns.Add( new DataColumn( (string) colOrder[i].Value ) );

				foreach (object obj in lRes) {
					dr = dt.NewRow();
					for (int i=0; i < colOrder.Length; i++) {
						dr[(string)colOrder[i].Value] = Convert.ToString(((Array)obj).GetValue((long)i)); 						
					}
					dt.Rows.Add(dr);
				}
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
				return null;
			}
			return new DataView( dt );
			
		}
	}
}
