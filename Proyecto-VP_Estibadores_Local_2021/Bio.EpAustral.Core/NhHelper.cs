﻿using System;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Tool.hbm2ddl;
using System.Reflection;

namespace Bio.EPAustral.Core
{
	public class NhHelper
	{
		private static ISessionFactory _sessionFactory;

		public static ISessionFactory GetSessionFactory()
		{
			if (_sessionFactory == null)
				BuildSessionFactory();
			return _sessionFactory;
		}

		private static void BuildSessionFactory()
		{
			var cfg = GetConfiguration();
			_sessionFactory = cfg.BuildSessionFactory();
		}

		public static void CreateDatabase()
		{
			var cfg = GetConfiguration();

			SchemaExport schemaExport = new SchemaExport(cfg);
			schemaExport.Create(false, true);
		}

		public static string GetSysInfo()
		{
			var session = _sessionFactory.GetCurrentSession();
			return session != null ? session.GetHashCode().ToString() : string.Empty;
		}

		public static Configuration GetConfiguration()
		{
            Configuration cfg = new Configuration();
            string sPathAssembly = Assembly.GetExecutingAssembly().CodeBase;
            cfg = new Configuration().Configure(sPathAssembly.Trim() + ".hibernate.cfg.xml");
            return cfg;
		}



		
	}
}
