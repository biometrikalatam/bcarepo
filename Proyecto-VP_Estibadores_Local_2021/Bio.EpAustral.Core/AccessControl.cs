﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Bio.EPAustral.Entities;
using NHibernate;
using NHibernate.Criterion;
using log4net;
using NHibernate.Transform;
using Bio.EpAustral.Entities;
using Bio.EPAustral.Core;

namespace Bio.EpAustral.Core
{
    public class AccessControl
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(AccessControl));
        static IList<Card> objCards = null;
        static IList<CardHolder> objCardHolder = null;
        static IList<History> objHistory = null;
        public static int nextHistoryID = 0;

        /// <summary>
        /// Funcion encargada de llenar el objeto card, la restriccion es que el campo CardNumber no puede estar nulo o vacio
        /// </summary>
        /// <param name="msgErr">msg de error generado por catch</param>
        /// <returns>0 = bien, -1 no se ejecuto</returns>
        public static IList getCards(out string msgErr)
        {
            msgErr = "";
            IList cardExprAux = null;
            try
            {
                log.Debug("Bio.EpAustral.Core.AccessControl.getCards IN...");
                ISession sess = HSessionFactory._sessionFactory.OpenSession();
                DateTime fechaActual = DateTime.Now;

                ICriteria crit = sess.CreateCriteria(typeof(Card))
                                    .Add(Restrictions.IsNotNull("CardNumber"))
                                    .Add(Restrictions.Gt("ExpirationDate", fechaActual))
                                    .Add(Restrictions.Eq("CardStatus", Convert.ToInt16(1)));

                ProjectionList p1 = Projections.ProjectionList();
                p1.Add(Projections.Property("CardNumber"), "CardNumber");
                p1.Add(Projections.Property("ExpirationDate"), "ExpirationDate");
                p1.Add(Projections.Property("CardStatus"), "CardStatus");
                p1.Add(Projections.Property("SpareStr1"), "SpareStr1");
                p1.Add(Projections.Property("SpareStr2"), "SpareStr2");
                p1.Add(Projections.Property("CardHolderid"), "CardHolderid");
                p1.Add(Projections.Property("Recordid"), "Recordid");

                crit.SetProjection(p1);
                crit.SetResultTransformer(Transformers.AliasToBean(typeof(CardExpress)));
                cardExprAux = crit.List();
                log.Info("Bio.EpAustral.Core.AccessControl.getCards - Se llena el objeto auxiliar cardExprAux, cantidad: " + (cardExprAux.Count - 1));
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                log.Error("Bio.EpAustral.Core.AccessControl.getCards - Excp Error al llenar objCards " + msgErr);
                return null;
            }
            log.Debug("Bio.EpAustral.Core.AccessControl.getCards OUT!");
            return cardExprAux;
        }

        /// <summary>
        /// Funcion encargada de llenar el objeto cardHolder
        /// </summary>
        /// <param name="msgErr">Msg de error generado por catch</param>
        /// <returns>0 = bien, -1 no se ejecuto</returns>
        public static int getCardHolder(out string msgErr)
        {
            msgErr = "";
            int res = 0;
            try
            {
                ISession sess = HSessionFactory._sessionFactory.OpenSession();
                objCardHolder = sess.CreateCriteria(typeof(CardHolder)).List<CardHolder>();
                log.Info("Se llena el objCardHolder, cantidad: " + (objCardHolder.Count - 1));
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                res = -1;
                log.Error("Error al llenar objCardHolder " + msgErr);
            }
            return res;
        }
        /// <summary>
        /// Funcion encargada de llenar el objeto history
        /// </summary>
        /// <param name="msgErr">Msg de error generado por catch</param>
        /// <returns>retorna la cantidad de marcas en el objecto history actual</returns>
        public static int getHistory(out string msgErr)
        {
            msgErr = "";
            int res = 0;
            try
            {
                ISession sess = HSessionFactory._sessionFactory.OpenSession();
                objHistory = sess.CreateCriteria(typeof(History))
                                    .AddOrder(Order.Asc("Recordid"))
                                    .List<History>();
                log.Info("Se llena el objHistory, cantidad: " + (objHistory.Count - 1));
                res = objHistory.Count;
                nextHistoryID = objHistory[res - 1].Recordid + 1;
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                res = -1;
                log.Error("Error al llenar objHistory " + msgErr);
            }
            return res;
        }

        //public int reloadHistory(out string msgErr)
        //{
        //    msgErr = "";
        //    int res = 0;
        //    try
        //    {
        //        ISession sess = HSessionFactory._sessionFactory.OpenSession();
        //        objHistory = sess.CreateCriteria(typeof(History))
        //                            .AddOrder(Order.Asc("Recordid"))
        //                            .List<History>();
        //        res = objHistory.Count;
        //        nextHistoryID = objHistory[res - 1].Recordid + 1;
        //        return res;
        //    }
        //    catch (Exception ex)
        //    {
        //        msgErr = ex.Message;
        //        res = -1;
        //        return res;
        //    }
        //}

        /// <summary>
        /// Funcion encargada de buscar el rut leido en la coleccion de cards actual
        /// </summary>
        /// <param name="rutLeido">ingreso de un rut</param>
        /// <param name="msgErr">Msg de error generado por catch</param>
        /// <param name="expiration">Devuelve la fecha de expiracion para el rut ingresado</param>
        /// <param name="status">devuelve el status para el rut ingresado</param>
        /// <returns>Retorna el CardHolder Id del rut leido, o -1 si hay algun error</returns>
        public static int matchCard(string rutLeido, out string msgErr, out string expiration, out string status)
        {
            msgErr = "";
            expiration = "";
            status = "";
            int res = 0;
            try
            {
                for (int x = 0; x < objCards.Count; x++)
                {
                    if (rutLeido == objCards[x].CardNumber.Trim())
                    {
                        res = Convert.ToInt32(objCards[x].CardHolderid);
                        expiration = objCards[x].ExpirationDate.ToString();
                        status = objCards[x].CardStatus.ToString();
                        log.Info("Hay coincidencia entre Rut leido: " + rutLeido + " y rut objCards: " + objCards[x].CardNumber.Trim());
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                res = -1;
                log.Error("Error al ejecutar la busqueda del rut ingresado " + msgErr);
            }
            return res;
        }
        /// <summary>
        /// Funcion encargada de buscar la informacion (CardHolder) de un rut
        /// </summary>
        /// <param name="CHID">cardholderid, valor devuelto por al funcion matchCard()</param>
        /// <param name="msgErr">Msg de error generdo por catch</param>
        /// <param name="nombres">nombre de la persona buscada</param>
        /// <param name="apellidos">apellidos de la persona buscada</param>
        /// <returns>0 = bien, -1 = error</returns>
        public static int getCardHolderData(int CHID, out string msgErr, out string nombres, out string apellidos)
        {
            msgErr = "";
            nombres = "";
            apellidos = "";
            int res = 0;
            try
            {
                for (int x = 0; x < objCardHolder.Count; x++)
                {
                    if (CHID == objCardHolder[x].Recordid)
                    {
                        nombres = objCardHolder[x].FirstName;
                        log.Info("Se encuentra coincidencia entre CHID: " + CHID + "y objCardHolder: " + objCards[x].CardNumber.Trim());
                        apellidos = objCardHolder[x].LastName;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                res = -1;
                log.Error("Error al ejecutar la busqueda del CHID ingresado " + msgErr);
            }
            return res;
        }

        /// <summary>
        /// Funcion encargada de dar o denegar el acceso, escribe en la base de datos
        /// </summary>
        /// <param name="RUT">rut leido para procesar</param>
        /// <param name="msgErr">Msg de error generado por catch</param>
        /// <param name="typeNumberMarc">valor del tipo de marca, tipo dato short</param>
        /// <returns>retorna true cuando se da acceso, retorna false cuando se niega el acceso</returns>
        public static bool giveAccess(string RUT, out string msgErr, short typeNumberMarc)
        {
            msgErr = "";

            int res = 0;

            ISession sess = HSessionFactory._sessionFactory.OpenSession();
            log.Info("Comienza proceso de control de acceso, para el rut: " + RUT);


            for (int x = 0; x < objCards.Count; x++)
            {
                if (RUT == objCards[x].CardNumber.Trim())
                {
                    log.Info("El rut: " + RUT + " existe en objCards: " + objCards[x].CardNumber.Trim());
                    if (objCards[x].CardStatus == 1)
                    {
                        log.Info("El rut: " + RUT + " tiene status: " + objCards[x].CardStatus);

                        DateTime fechaActual = DateTime.Now;
                        if (fechaActual < objCards[x].ExpirationDate)
                        {
                            log.Info("El rut: " + RUT + " tiene fecha vencimiento: " + objCards[x].ExpirationDate + " Hoy es: " + fechaActual);
                            try
                            {
                                log.Info("empesamos a llenar un objeto history");
                                History dataToSave = new History();
                                dataToSave.Recordid = objHistory[objHistory.Count - 1].Recordid + 1;
                                dataToSave.Link1 = 58;
                                dataToSave.Link3 = objCards[x].CardHolderid;
                                dataToSave.Link2 = objCards[x].Recordid;
                                dataToSave.Param1 = 1;
                                dataToSave.Param2 = typeNumberMarc;
                                dataToSave.Param3 = objCards[x].CardNumber;
                                fechaActual = DateTime.Now;
                                dataToSave.RecvTime = fechaActual;
                                dataToSave.Type1 = 9;
                                dataToSave.Type2 = 0;
                                dataToSave.UserPriority = 0;
                                dataToSave.Nodeid = 0;
                                dataToSave.Userid = 0;
                                dataToSave.Accountid = 0;
                                dataToSave.Link4 = 0;
                                dataToSave.Seqid = Convert.ToInt32(objCards[x].CardHolderid.ToString().Trim());
                                dataToSave.GenTime = fechaActual;
                                dataToSave.TimeStamp = fechaActual;

                                int otro = dataToSave.Recordid;
                                log.Info("Objeto hisory listo apra ser ingresado en la BD");
                                sess.Save(dataToSave);
                                sess.Flush();
                                nextHistoryID = nextHistoryID + 1;
                                dataToSave = null;
                                log.Info("Objeto hisory ingresado");
                                return true;
                            }
                            catch (Exception ex)
                            {
                                msgErr = ex.Message;
                                log.Error("Error en el ingreso: " + msgErr);
                                res = -1;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public static bool SaveAccessOutWithoutControl(string RUT, out string msgErr, short typeNumberMarc, IList cardsToSearch)
        {
            msgErr = "";
            //Definimos objeto tarjeta.
            IList<Card> objCard;
            //Definimos variable de respuesta
            bool res = false;
            bool exist = false;
            int indexRutEncontrado = 0;
            int idgenerado = 0;

            try
            {
                log.Debug("AccessControl.SaveAccessOutWithoutControl IN - Grabando salida para el rut: " + RUT);
                log.Debug("AccessControl.SaveAccessOutWithoutControl - Abre session BD...");
                ISession sess = Bio.EPAustral.Core.NhHelper.GetSessionFactory().OpenSession();
                log.Debug("AccessControl.SaveAccessOutWithoutControl - Busca RUT en lista de Cards en memoria...");
                for (int x = 0; x < cardsToSearch.Count; x++)
                {
                    string baseCompare = ((CardExpress)cardsToSearch[x]).CardNumber.Trim();
                    //log.Debug("Comparando con: " + baseCompare);
                    if (baseCompare.CompareTo(RUT) == 0)
                    {
                        log.Debug("Bio.EpAustral.Core.AccessControl.giveAccessControl - Encontrado: " + RUT + " = " + baseCompare);
                        exist = true;
                        indexRutEncontrado = x;
                        break;
                    }
                }

                log.Debug("AccessControl.giveAccessControl - Graba salida sin control de regla de negocio...");
                res = SaveAccess(RUT, typeNumberMarc, ((CardExpress)cardsToSearch[indexRutEncontrado]), out idgenerado);

            }
            catch (Exception ex)
            {
                res = false;
                log.Error("AccessControl.SaveAccessOutWithoutControl - Excp Error: " + ex.Message);
            }
            log.Debug("AccessControl.SaveAccessOutWithoutControl OUT!");
            return res;
        }

        /// <summary>
        /// Funcion encargada de eliminar el ultimo registro de la tabla history, usado cuando se da acceso pero el torniquete no abre
        /// </summary>
        /// <param name="msgErr">Msg de error generado por catch</param>
        /// <returns>retorna true cuando puede eliminar, y falso cuando hay error en la eliminacion</returns>
        public static bool deleteLastHistory(out string msgErr)
        {
            msgErr = "";
            int res = 0;
            try
            {
                ISession sess = HSessionFactory._sessionFactory.OpenSession();
                objHistory = sess.CreateCriteria(typeof(History))
                                    .AddOrder(Order.Asc("Recordid"))
                                    .List<History>();

                res = objHistory.Count;
                nextHistoryID = objHistory[res - 1].Recordid;
                log.Info("Se eliminara el registro para el id: " + nextHistoryID);

                IQuery query = sess.CreateQuery("FROM History WHERE RecordID = " + nextHistoryID + "");
                History pet = query.List<History>()[0];
                sess.Delete(pet);
                sess.Flush();
                log.Info("id eliminado: " + nextHistoryID);
                objHistory = sess.CreateCriteria(typeof(History))
                                    .AddOrder(Order.Asc("Recordid"))
                                    .List<History>();

                log.Info("Recarga de objHistory");

                res = objHistory.Count;
                nextHistoryID = objHistory[res - 1].Recordid;
                return true;
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                res = -1;
                log.Error("Error en la eliminacion" + msgErr);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RUT"></param>
        /// <param name="msgErr"></param>
        /// <param name="typeNumberMarc"></param>
        /// <param name="cardsToSearch"></param>
        /// <returns>true abre, false no abre</returns>
        public static bool giveAccessControl(string RUT, out string msgErr, short typeNumberMarc, IList cardsToSearch)
        {
            msgErr = "";
            //Definimos objeto tarjeta.
            IList<Card> objCard;
            //Definimos variable de respuesta
            bool res = false;
            bool exist = false;
            int indexRutEncontrado = 0;
            int idgenerado = 0;

            log.Debug("Bio.EpAustral.Core.AccessControl.giveAccessControl IN - " +
                        "Comienza proceso de control de acceso, para el rut: " + RUT);
            log.Debug("Bio.EpAustral.Core.AccessControl.giveAccessControl - Abre session BD...");
            ISession sess = Bio.EPAustral.Core.NhHelper.GetSessionFactory().OpenSession();
            

            log.Debug("Bio.EpAustral.Core.AccessControl.giveAccessControl - Busca RUT en lista de Cards en memoria...");
            for (int x = 0; x < cardsToSearch.Count; x++)
            {
                string baseCompare = ((CardExpress)cardsToSearch[x]).CardNumber.Trim();
                //log.Debug("Comparando con: " + baseCompare);
                if (baseCompare.CompareTo(RUT) == 0)
                {
                    log.Debug("Bio.EpAustral.Core.AccessControl.giveAccessControl - Encontrado: " + RUT + " = " + baseCompare);
                    exist = true;
                    indexRutEncontrado = x;
                    break;
                }
            }

            if (exist)
            {
                log.Debug("Bio.EpAustral.Core.AccessControl.giveAccessControl - El CardNumber: " + 
                          RUT + " Existe en la lista de CardNumber habilitados, comienza proceso de reglas de acceso...");
                if (((CardExpress)cardsToSearch[indexRutEncontrado]).SpareStr1 != null)
                {
                    //log.Info("Es un estibador");
                    if (((CardExpress)cardsToSearch[indexRutEncontrado]).SpareStr2 != null)
                    {
                        DateTime fechasEntrada = Convert.ToDateTime(((CardExpress)cardsToSearch[indexRutEncontrado]).SpareStr1.Trim());
                        DateTime fechasSalida = Convert.ToDateTime(((CardExpress)cardsToSearch[indexRutEncontrado]).SpareStr2.Trim());

                        DateTime fechaActualEst = DateTime.Now;

                        if (fechaActualEst > fechasEntrada && fechaActualEst < fechasSalida)
                        {
                            log.Debug("Bio.EpAustral.Core.AccessControl.giveAccessControl - " + 
                                        "Estibador dentro del rango de fechas, se permite el acceso");
                            SaveAccess(RUT, typeNumberMarc, ((CardExpress)cardsToSearch[indexRutEncontrado]), out idgenerado);
                            res = true;
                        }
                        else
                        {
                            log.Debug("Bio.EpAustral.Core.AccessControl.giveAccessControl - " + 
                                        "Estibador fuera del rango de fechas, no se permite acceso " +
                                        "[FechaLimite = " + fechasSalida.ToString("dd/MM/yyyy HH:mm:ss"));
                            saveNoAccess(RUT, typeNumberMarc, out idgenerado);
                            res = false;
                        }
                    }
                    else
                    {
                        log.Debug("Bio.EpAustral.Core.AccessControl.giveAccessControl - " + 
                                    "Error en el rango de tiempo, tiempo final nulo");
                        saveNoAccess(RUT, typeNumberMarc, out idgenerado);
                        res = false;
                    }
                }
                else
                {
                    log.Debug("Bio.EpAustral.Core.AccessControl.giveAccessControl - Es una persona normal");
                    SaveAccess(RUT, typeNumberMarc, ((CardExpress)cardsToSearch[indexRutEncontrado]), out idgenerado);
                    res = true;
                }
            }
            else
            {
                log.Debug("Bio.EpAustral.Core.AccessControl.giveAccessControl - El rut: " + RUT + 
                          " No existe en la lista de rut habilitados, se guarda el registro pero no se permite el acceso");
                saveNoAccess(RUT, typeNumberMarc, out idgenerado);
                res = false;
            }
            log.Debug("Bio.EpAustral.Core.AccessControl.giveAccessControl OUT!");
            return res;
        }

        private static bool SaveAccess(string RUT, short typeNumberMarc, CardExpress objCard, out int idgenerado)
        {
            bool res = false;
            DateTime fechaActual;
            //Iniciamos la fecha de marcación
            fechaActual = DateTime.Now;
            idgenerado = -1;
            log.Debug("Bio.EpAustral.Core.AccessControl.SaveAccess IN - Procedemos a guardar la marca");
            try
            {
                ISession sess = HSessionFactory._sessionFactory.OpenSession();
                History dataToSave = new History();
                //Validar esto.
                dataToSave.Link1 = typeNumberMarc;
                dataToSave.Link3 = objCard.CardHolderid;
                dataToSave.Link2 = objCard.Recordid;
                dataToSave.Param1 = 1;
                dataToSave.Param2 = typeNumberMarc;
                dataToSave.Param3 = objCard.CardNumber;
                dataToSave.RecvTime = fechaActual;
                dataToSave.Type1 = 9;
                dataToSave.Type2 = 0;
                dataToSave.UserPriority = 0;
                dataToSave.Nodeid = 0;
                dataToSave.Userid = 0;
                dataToSave.Accountid = 0;
                dataToSave.Link4 = 0;
                dataToSave.GenTime = fechaActual;
                dataToSave.TimeStamp = fechaActual;
                dataToSave.SpareStr1 = objCard.SpareStr1;
                dataToSave.SpareStr2 = objCard.SpareStr2;
                sess.Save(dataToSave);
                sess.Flush();
                log.Debug("Bio.EpAustral.Core.AccessControl.SaveAccess - Se graba el evento en history");
                idgenerado = Convert.ToInt32(sess.GetIdentifier(dataToSave));
                log.Debug("Bio.EpAustral.Core.AccessControl.SaveAccess - El id de history agregado es:" + idgenerado);
                //Buscamos el último history y actualizamos la tabla.
                dataToSave = sess.CreateCriteria(typeof(History)).Add(Expression.Eq("Recordid", idgenerado)).UniqueResult<History>();
                if (dataToSave != null)
                {
                    dataToSave.Seqid = idgenerado;
                    sess.Update(dataToSave);
                    sess.Flush();
                }
                res = true;
            }
            catch (Exception exe)
            {
                res = false;
                log.Error("Bio.EpAustral.Core.AccessControl.SaveAccess Excp Error => Se produjo error al grabar el history", exe);
            }
            log.Debug("Bio.EpAustral.Core.AccessControl.SaveAccess OUT!");

            return res;
        }

        public static bool saveNoAccess(string RUT, short typeNumberMarc, out int idgenerado)
        {
            bool res = false;
            idgenerado = -1;
            DateTime fechaActual = DateTime.Now;
            log.Debug("Bio.EpAustral.Core.AccessControl.saveNoAccess - IN El rut de la persona que no pudo marcar es:" + RUT);

            try
            {
                ISession sess = HSessionFactory._sessionFactory.OpenSession();
                LogTorniquetes logTorniquetesToSave = new LogTorniquetes();

                logTorniquetesToSave.Rut = RUT;
                logTorniquetesToSave.FechaIntento = fechaActual;
                logTorniquetesToSave.Torniquete = typeNumberMarc;
                sess.Save(logTorniquetesToSave);
                sess.Flush();
                idgenerado = Convert.ToInt32(sess.GetIdentifier(logTorniquetesToSave));
                log.Debug("Bio.EpAustral.Core.AccessControl.saveNoAccess - Se graba el evento en logTorniquetes");
                logTorniquetesToSave = sess.CreateCriteria(typeof(LogTorniquetes)).Add(Expression.Eq("Logid", idgenerado)).UniqueResult<LogTorniquetes>();
                if (logTorniquetesToSave != null)
                {
                    logTorniquetesToSave.Logid = idgenerado;
                    sess.Update(logTorniquetesToSave);
                    sess.Flush();
                }
                res = true;
            }
            catch (Exception ex)
            {
                res = false;
                log.Error("Bio.EpAustral.Core.AccessControl.saveNoAccess - Se produjo error al grabar el logTorniquetes - " + ex.Message);
            }
            log.Debug("Bio.EpAustral.Core.AccessControl.saveNoAccess OUT!");
            return res;
        }
    }
}
