﻿using System;
using System.Collections.Generic;
using System.Text;
using log4net;

using NHibernate;
using NHibernate.Context;
using Bio.EPAustral.Core;

namespace Bio.EpAustral.Core
{
    public class DBConn
    {
        static ISession _session;
        /// <summary>
        /// Esta funcion se encarga de llenar el objeto Cards
        /// </summary>
        /// <param name="msgErr">Mensage de error, este se genera en el catch</param>
        /// <returns>Retorno del objeto Card, si hay error retornara un objeto con valor null</returns>
        public static ISession Connection(out String msgErr)
        {
            msgErr = "S/C";
            try
            {
                var sessionFactory = NhHelper.GetSessionFactory();
                _session = sessionFactory.OpenSession();
                CurrentSessionContext.Bind(_session);
            }
            catch (Exception ex)
            {
                //LOG.Error("AdminBpCompany.Retrieve (id)", ex);
                msgErr = ex.Message;
            }
            return _session;
        }
    }
}
