﻿using BCR_WinPakProLite.Domain;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCR_WinPakProLite
{
    public class NHibernateHelperLocal
    {
        private static ISessionFactory _sessionFactory;

        private static ISessionFactory SessionFactory(string conn)
        {
            if (_sessionFactory == null)
                InitializeSessionFactory(conn);
            return _sessionFactory;
        }

        private static void InitializeSessionFactory(string conn)
        {
            _sessionFactory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008.ConnectionString(conn))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Card>())
                .BuildSessionFactory();
        }

        public static ISession OpenSession(string conn)
        {
            return SessionFactory(conn).OpenSession();
        }
    }
}
