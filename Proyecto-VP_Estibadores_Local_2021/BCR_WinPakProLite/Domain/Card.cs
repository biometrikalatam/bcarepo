﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCR_WinPakProLite.Domain
{
    public class Card
    {
        public virtual int Id { get; set; }
        public virtual string CardNumber { get; set; }
        public virtual string SpareStr1 { get; set; }
        public virtual string SpareStr2 { get; set; }
        public virtual int RecordId { get; set; }
        public virtual int CardHolderId { get; set; }
    }

    public class CardMap : ClassMap<Card>
    {
        public CardMap()
        {
            Table("[Card]");
            Id(x => x.Id).GeneratedBy.Identity().Column("[Id]");
            Map(x => x.CardNumber).Column("[CardNumber]");
            Map(x => x.SpareStr1).Column("[SpareStr1]");
            Map(x => x.SpareStr2).Column("[SpareStr2]");
            Map(x => x.RecordId).Column("[RecordID]");
            Map(x => x.CardHolderId).Column("[CardHolderID]");
        }
    }
}
