﻿using System.Collections;
using System.Collections.Generic;

namespace Biometrika.VirtualPrinter.Api
{
    public interface IVirtualPrinter
    {

        string Name { get; set; }

        object Config { get; set; }

        string ConfigToString();

        IList CardExpress { get; set; }
        /// <summary>
        /// Inicializa la clase con los parametros que necesite para su ejecucion, de tal forma
        /// de acelerar los procesos. Puede leer conexion a BD, configuraciones expecificas,
        /// chequear conexiones, etc.
        /// </summary>
        /// <param name="xmlinput">xml con datos input - </param>
        /// <param name="xmloutput">xml con datos de output</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        int Initialization(string xmlinput, out string xmloutput);

        /// <summary>
        /// Procesa la muestra serial entregada como parámetro
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="xmloutput">xml con datos de output</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        int Process(string xmlinput, out string xmloutput);


    }
}
