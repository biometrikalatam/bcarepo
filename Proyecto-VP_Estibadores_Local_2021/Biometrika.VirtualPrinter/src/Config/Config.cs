﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biometrika.VirtualPrinter.Config
{
    public class Config
    {

        private bool _parseCedula = false;
        private String _handkeycode = "";
        private String _readercode = "";
        private int _typemark = 0;
        private int _typemarknotfound=0;
        private int _typemarknotpay=0;
          
        private VirtualPrintersCofig[] _virtualPrintersCofigured;

        public bool ParseCedula
        {
            get { return _parseCedula; }
            set { _parseCedula = value; }
        }

        public String HandkeyCode
        {
            get{return _handkeycode;}
            set{_handkeycode = value;}
        }
        public String ReaderCode
        {
            get { return _readercode; }
            set { _readercode = value; }
        }
        public int TypeMark
        {
            get { return _typemark; }
            set { _typemark = value; }
        }
        public int TypeMarkNotFound
        {
            get { return _typemarknotfound; }
            set { _typemarknotfound = value; }
        }
        public int TypeMarkNotPay
        {
            get { return _typemarknotpay; }
            set { _typemarknotpay = value; }
        }


        public VirtualPrintersCofig[] VirtualPrintersCofigured
        {
            get { return _virtualPrintersCofigured; }
            set { _virtualPrintersCofigured = value; }
        }
    }

    public class VirtualPrintersCofig
    {
        private int _orderProcess;
        private string _name;
        private string _assemblyName;
        private string _configPath;

        public int OrderProcess
        {
            get { return _orderProcess; }
            set { _orderProcess = value; }
        }

        public string AssemblyName
        {
            get { return _assemblyName; }
            set { _assemblyName = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string ConfigPath
        {
            get { return _configPath; }
            set { _configPath = value; }
        }

    }
}
