﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Biometrika.VirtualPrinter.src.Utils
{
    /// <summary>
    /// Helper para serializar y deserializar objetos
    /// </summary>
    public class SerializeHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(SerializeHelper));

        #region Static Zone

        /// <summary>
        /// Serializa un objeto Parameters a disco
        /// </summary>
        /// <param name="obj">Onjeto a serializar</param>
        /// <param name="path">Path absoluto donde serializar</param>
        /// <returns>true o false en exito y fracaso respectivamente</returns>
        public static bool SerializeToFile(object obj, string path)
        {
            bool bRes = false;

            try
            {
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                TextWriter writer = new StreamWriter(path);
                serializer.Serialize(writer, obj);
                writer.Close();
                bRes = true;
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.SerializeHelper.SaveToFile", ex);
            }
            return bRes;
        }

        /// <summary>
        /// Deserializa un objeto Parameters desde disco
        /// </summary>
        /// <param name="path">Path absoluto donde serializar</param>
        /// <returns>Objeto deserializado o null de haber error</returns>
        public static T DeserializeFromFile<T>(string path)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                TextReader reader = new StreamReader(path);
                T cf = (T)serializer.Deserialize(reader);
                reader.Close();
                return cf;
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.SerializeHelper.SaveToFile", ex);
            }
            return default(T);
        }

        /// <summary>
        /// Serializa un objeto Parameters y lo devuelve en un string
        /// </summary>
        /// <param name="obj">objecto a serializar</param>
        /// <returns>true o false en exito y fracaso respectivamente</returns>
        public static string SerializeToXml(object obj)
        {
            string xmlRes = null;

            try
            {
                xmlRes = XmlUtils.SerializeObject(obj);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.SerializeHelper.SerializeToXml", ex);
            }
            return xmlRes;
        }

        /// <summary>
        /// Serializa un objeto Parameters y lo devuelve en un string
        /// </summary>
        /// <param name="xmlParam">Xml del objeto a deserializar</param>
        /// <returns>true o false en exito y fracaso respectivamente</returns>
        public static T DeserializeFromXml<T>(string xmlParam)
        {
            T olRes = default(T);

            try
            {
                olRes = XmlUtils.DeserializeObject<T>(xmlParam);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Serialize.DeserializeFromXml", ex);
            }
            return olRes;
        }


        #endregion Static Zone


    }
}
