﻿using System;
using System.Windows.Forms;
using BioCore.SerialComm;
using BioCore.SerialComm.BarCode;
using BioCore.SerialComm.Cedula;
using BioCore.SerialComm.SerialPort;
using Biometrika.VirtualPrinter.Api;
using Biometrika.VirtualPrinter.Config;
using log4net;
using System.Collections;
using System.Reflection;

namespace Biometrika.VirtualPrinter
{
    public partial class xfrmMain : Form 
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(xfrmMain));

        Broker broker;
        private bool bEnableRead = false;

        public xfrmMain()
        {
            try
            {
                xfrmMain.CheckForIllegalCrossThreadCalls = false;
                InitializeComponent();
                AddMsgLog("Creando Broker para lectura de barcode...", false); 
                this.broker = new Broker();
                AddMsgLog("Status Broker: " + (this.broker!=null?"CREADO":"NO CREADO"), false);
            }
            catch (Exception exe)
            {
                AddMsgLog("Excepcion creando broker " + exe.Message, false);
                LOG.Fatal(exe);
                MessageBox.Show(exe.Message);
            }
        }

        private void xfrmMain_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("Hola");
            xfrmMain.CheckForIllegalCrossThreadCalls = false;
            //Asigno eventos al broker
            AddMsgLog("Asociando eventos de Broker para lectura de barcode...", false);
            broker.SampleAvailable += new SampleAvailableEvent(OnSample);
            AddMsgLog("Eventos de Broker asociado...", false);
            bEnableRead = true;

            AddMsgLog("Biometrika Virtual Printer Started!", false);
            if (Program.CONFIG != null)
            {
                AddMsgLog("Config Read = OK", true);
                AddMsgLog("Virtual Printers Configured =>", true);
                foreach (var vp in Program.CONFIG.VirtualPrintersCofigured)
                {
                    VirtualPrintersCofig vpc = (VirtualPrintersCofig)vp;
                    AddMsgLog("     VP " + vpc.OrderProcess + "=>" + vpc.Name + " [" +
                                vpc.AssemblyName + "]", true);
                }

                Version version = Assembly.GetExecutingAssembly().GetName().Version;
                labVersion.Text = "v" + version.ToString();

                this.Text = "Biometrika Virtual Printer " + labVersion.Text + "...";
                AddMsgLog(this.Text + " => Iniciado!",true);

                //Llena info en ritch...
                rtbDetails.Text = "Biometrika Virtual Printer " + labVersion.Text + Environment.NewLine + 
                    "------------------------------------------------------------------------" + Environment.NewLine;

                rtbDetails.Text += "Nombre Control = " + Program.CONFIG.ReaderCode + Environment.NewLine + 
                    "------------------------------------------------------------------------" + Environment.NewLine;

                rtbDetails.Text += "Vitual Printers Configuradas = " + Program.CONFIG.VirtualPrintersCofigured.Length.ToString() + Environment.NewLine;
                for (int i = 0; i < Program.CONFIG.VirtualPrintersCofigured.Length ; i++)
                {
                    rtbDetails.Text += "  >> Orden Proceso:" + 
                        ((VirtualPrintersCofig)Program.CONFIG.VirtualPrintersCofigured[i]).OrderProcess.ToString() + Environment.NewLine;
                    rtbDetails.Text += "    >>>> Nombre:" + 
                        ((VirtualPrintersCofig)Program.CONFIG.VirtualPrintersCofigured[i]).Name.ToString() + Environment.NewLine;
                    rtbDetails.Text += "    >>>> Assembly:" + 
                        ((VirtualPrintersCofig)Program.CONFIG.VirtualPrintersCofigured[i]).AssemblyName.ToString() + Environment.NewLine;
                    rtbDetails.Text += "    >>>> Config:" + 
                        ((VirtualPrintersCofig)Program.CONFIG.VirtualPrintersCofigured[i]).ConfigPath.ToString() + Environment.NewLine;
                }
                rtbDetails.Text += Environment.NewLine;

                rtbDetails.Text += "Vitual Printers Instanciadas = " + Program.PRINTERS.Count.ToString() + Environment.NewLine;
                for (int i = 1; i <= Program.PRINTERS.Count; i++)
                {
                    IVirtualPrinter vp = (IVirtualPrinter)Program.PRINTERS[i];
                    rtbDetails.Text += "  >> Nombre:" + vp.Name + Environment.NewLine;
                    rtbDetails.Text += "     >> Condig:" + vp.ConfigToString() + Environment.NewLine;
                }
            } 
            else
            {
                AddMsgLog("Config Read = ERROR", true);
            }
        }

        private void AddMsgLog(string msg, bool append)
        {
            try
            {
                LOG.Debug("xfrmMain.AddMsgLog - " + msg);
                //if (rtbLog.Text.Length > 500)
                //    rtbLog.Text = "";

                //if (append) rtbLog.Text = rtbLog.Text + Environment.NewLine + msg;
                //else rtbLog.Text = msg;
            }
            catch (Exception ex)
            {
                LOG.Error("xfrmMain.AddMsgLog Error", ex);
            }
        }

        private void OnSample(ISensor sensor, ISample sample)
        {
            try
            {
                if (sample is SerialSample && bEnableRead)
                {
                    String com = "";
                    BarCodeReader reader = sensor as BarCodeReader;
                    com = reader.serialPort.PortName;
                    ISample sample1 = reader.GetSample(new BarCodeArguments(250));
                    DispatchSample(sample1, com);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("xfrmMainOnSample - Excp Error: " + ex.Message);
            }
        }

        private void DispatchSample(ISample sample, String com)
        {
            Parameters paramin = new Parameters();
            try
            {
                LOG.Debug("xfrmMain.DispatchSample - Entra a procesar muestra leida...");
                //Controlarmos que el sample no sea nulo
                if (sample != null)
                {
                    LOG.Debug("xfrmMain.DispatchSample - Sample no es nulo:" + sample);
                    
                    paramin.AddValue("Sample", Convert.ToBase64String((sample as SerialSample).Data));
                    paramin.AddValue("COM", com);
                    string xmlin = Parameters.SerializeToXml(paramin);
                    string xmlout = null;
                    int ret = 0;

                    //2.- Envio sample a cada VP
                    LOG.Debug("xfrmMain.DispatchSample - Entro a procesar la muestra en cada VP...");
                    for (int i = 1; i <= Program.PRINTERS.Count; i++)
                    {
                        IVirtualPrinter vp = (IVirtualPrinter)Program.PRINTERS[i];
                        
                        LOG.Debug("xfrmMain.DispatchSample - Process Sample en > " + vp.Name + "...");
                        
                        AddMsgLog("Antes de enviar a procesar:", true);
                        ret = vp.Process(xmlin, out xmlout);

                        LOG.Debug("xfrmMain.DispatchSample - Processed Sample en > " + vp.Name + " => " + ret.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("xfrmMain.DispatchSample Excp Error", ex);
            }
        }

        
        private void xfrmMain_Resize(object sender, EventArgs e)
        {
              if (WindowState == FormWindowState.Minimized) { 
                    this.Hide();
                notifyIconVP.BalloonTipTitle = "Biometrika Virtual Printer " + labVersion.Text + "...";
                notifyIconVP.Text = "Biometrika VP " + labVersion.Text + ". Doble click para maximizar...";
                notifyIconVP.BalloonTipText =
                            "Biometrika Virtual Printer Console Minimizado. " + 
                            "Haga doble click sobre el ícono para visualizarlo...";
                notifyIconVP.ShowBalloonTip(3000, "Biometrika Virtual Printer " + labVersion.Text + "...",
                            "Biometrika Virtual Printer Console Minimizado. Haga doble click sobre el ícono para visualizarlo.",
                            ToolTipIcon.Info);
            }
        }

        

        private void notifyIconVP_DoubleClick(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Maximized;
            this.Visible = true;
            this.TopMost = true;
            this.Show();
            this.Refresh();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}