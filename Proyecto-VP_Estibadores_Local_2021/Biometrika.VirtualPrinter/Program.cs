﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Biometrika.VirtualPrinter.Api;
using Biometrika.VirtualPrinter.Config;
using log4net;
using log4net.Config;
using BVPConfig = Biometrika.VirtualPrinter.Config.Config;

namespace Biometrika.VirtualPrinter
{
    static class Program 
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Program));

        public static BVPConfig CONFIG;

        public static Hashtable PRINTERS;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                //string s = "21284415-2";
                //s = s.Replace("-", "");
                //s = s.Substring(0, s.Length - 1);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                //0.- Inicializo LOG
                XmlConfigurator.Configure(
                    new FileInfo(Application.StartupPath + "\\Logger.cfg"));
                LOG.Info("Biometrika.VirtualPrinter Starting...");

                //1.- Leo Config
                //1.- Levanto configuración BioPortal
                CONFIG =
                    src.Utils.SerializeHelper.DeserializeFromFile<BVPConfig>(Application.StartupPath + "\\Config.cfg");
                LOG.Info("Biometrika.VirtualPrinter - " +  CONFIG != null
                             ? "   Biometrika Virtual Printer reading Config = OK"
                             : "   Biometrika Virtual Printer reading Config = ERROR");

                //2.- Levanto DLLs para Dispatch
                LOG.Info("Biometrika.VirtualPrinter - Inicializando Virtual Printers desde config...");
                int ret = InstanciaVirtualPrinters(CONFIG.VirtualPrintersCofigured);
                if (PRINTERS.Count == 0)
                {
                    LOG.Warn("Biometrika.VirtualPrinter - No existen Virtual Printers Configuradas!");
                }
                else
                {
                    for (int i = 1; i <= PRINTERS.Count; i++)
                    {
                        IVirtualPrinter vp = (IVirtualPrinter)PRINTERS[i];
                        //iret = vp.Initialization(null, out xmlout);
                        LOG.Info("    Virtual Printer Disponible: " + vp.Name + "...");
                    }
                }

                Application.Run(new xfrmMain());
            }
            catch (Exception exe)
            {
                MessageBox.Show(exe.Message);
            }
        }


        /// <summary>
        /// Instancia los objetos de las impresoras virtuales y las inicializa para leer los 
        /// archivos de config que son configurados en cada Virtual Printer implementada
        /// </summary>
        /// <param name="virtualPrintersCofigured">Config de cada VP</param>
        /// <returns></returns>
        private static int InstanciaVirtualPrinters(VirtualPrintersCofig[] virtualPrintersCofigured)
        {
            int ret = Errors.IERR_OK;
            string outP;
            try
            {
                LOG.Info("Biometrika.VirtualPrinter.InstanciaVirtualPrinters IN...");
                PRINTERS = new Hashtable();
                LOG.Info("Biometrika.VirtualPrinter.InstanciaVirtualPrinters - Procesando VirtualPrintersCofig => Len = " +
                            (virtualPrintersCofigured == null?"0": virtualPrintersCofigured.Length.ToString()));
                foreach (var virtualPrintersCofig in virtualPrintersCofigured)
                {
                    LOG.Info("Biometrika.VirtualPrinter.InstanciaVirtualPrinters -     >> Procesando: " +
                                virtualPrintersCofig.AssemblyName + "...");
                    Assembly asmLogin = Assembly.Load(virtualPrintersCofig.AssemblyName);
                    object instance;
                    Type[] types = null;
                    try
                    {
                         types = asmLogin.GetTypes();
                    } catch (Exception exloader)
                    {
                        LOG.Warn("Biometrika.VirtualPrinter.InstanciaVirtualPrinters Warn - Activator.CreateInstance(t)", exloader);
                    }
                    foreach (Type t in types)
                    {
                        try
                        {
                            instance = null;
                            try
                            {
                                instance = Activator.CreateInstance(t);
                            }
                            catch (Exception exC)
                            {
                                LOG.Warn("Biometrika.VirtualPrinter.Loader Warn - Activator.CreateInstance(t)", exC);
                                instance = null;
                            }

                            if (instance != null)
                            {
                                if (instance is IVirtualPrinter) //(t.Name.IndexOf("Matcher") > -1)
                                {
                                    LOG.Info("Biometrika.VirtualPrinter.InstanciaVirtualPrinters - Instanciando: " +
                                        virtualPrintersCofig.Name + " - " + virtualPrintersCofig.AssemblyName +
                                        virtualPrintersCofig.ConfigPath + "...");
                                    ((IVirtualPrinter)instance).Name = virtualPrintersCofig.Name;
                                    if (((IVirtualPrinter)instance).Initialization(virtualPrintersCofig.ConfigPath, out outP) == 0)
                                    {
                                        PRINTERS.Add(virtualPrintersCofig.OrderProcess, (IVirtualPrinter)instance);
                                    }
                                    break;
                                } else
                                {
                                    LOG.Info("Biometrika.VirtualPrinter.InstanciaVirtualPrinters - Descartando: " +
                                             virtualPrintersCofig.Name + "!");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LOG.Error("Biometrika.VirtualPrinter.Loader - Creando instancias de " +
                                      virtualPrintersCofig.AssemblyName, ex);
                        }
                    }
                }
                ret = PRINTERS.Count > 0 ? Errors.IERR_OK : Errors.IERR_PRINTER_NOT_CONFIGURED;
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("Biometrika.VirtualPrinter.Loader", ex);               
            }
            LOG.Info("Biometrika.VirtualPrinter.InstanciaVirtualPrinters OUT! => Cantidad PRINTERS = " + PRINTERS.Count.ToString());
            return ret;
        }
    }
}
