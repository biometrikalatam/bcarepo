﻿using System;
using System.Text;
using Biometrika.VirtualPrinter.Api;
using log4net;
using NHibernate.Context;
using NHibernate;
using BioCore.SerialComm.Cedula;
using BioCore.SerialComm.SerialPort;
using BioCore.SerialComm;
using Biometrika.VirtualPrinter.SimpleShow.src.Database;
using Biometrika.VirtualPrinter.SimpleShow.src.Config;
using Bio.Core.Utils;
using System.Xml.Serialization;
using System.IO;
using Bio.Core.Device.Handkey;
using System.Timers;
using WebAssistance.Core;
using System.Collections.Generic;
using System.Collections;
using Bio.Core.SerialComm.Cedula;




namespace Biometrika.VirtualPrinter.SimpleShow
{
    public class VirtualPrinterSimpleShow : IVirtualPrinter
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(VirtualPrinterSimpleShow));
        private static LectorConfig CONFIG;
        private Timer _timer;
        private  ISession _session;
        private ISessionFactory sessionFactory;
        private BioArduinoRele.ArduinoRele objArduinoRele;
        private short port;
        private short qRelay;
        private short milisec;
        private string idr;
        private short reti;

#region Implementation of IVirtualPrinter

        private string _name;
        private IList _cardExpress;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        
        public VirtualPrinterSimpleShow()
        {
            String comport = "";
            String msgErr = "";
            //Prodríamos tomar el archivo de configuración de virtual printer y leer los parámetros.
            var uri = new UriBuilder(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
            String pathassembly= Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
            CONFIG =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromFile<LectorConfig>(pathassembly + "\\LectorConfigOut.cfg");

            //LOG.Info("Configuramos la conexión a la base de datos");
            //_session = Bio.EpAustral.Core.DBConn.Connection(out msgErr);
            //if (_session != null)
            //{
            //    LOG.Info("La conexión a base de datos fue exitosa");
            //}
            //else
            //{
            //    LOG.Error("La conexión a la base de datos falló");
            //}
            //LOG.Info("Configuramos la conexión a Arduino");
            ////Definimos el objeto de relé.
            ////objArduinoRele = new BioArduinoRele.ArduinoRele();
            ////Obtenemos el COM de Arduino Relé.
            ////comport = objArduinoRele.getCOM();
            ////if (comport != null)
            ////{
            ////    LOG.Info("Se pudo conectar a Arduino sin inconvenientes en el puerto:" + comport);

            ////}
            ////else
            ////{
            ////    LOG.Error("No encontró Arduino, Favor verificar que Arduino este conectado");
            ////}
            //_timer = new Timer((double)CONFIG.TimeOff);
            //_timer.Elapsed += new ElapsedEventHandler(_timer_Elapsed);
            //_timer.Enabled = false; // Enable it
           
            
        }

        /// <summary>
        /// Inicializa la clase con los parametros que necesite para su ejecucion, de tal forma
        /// de acelerar los procesos. Puede leer conexion a BD, configuraciones expecificas,
        /// chequear conexiones, etc.
        /// </summary>
        /// <param name="xmlinput">xml con datos input - </param>
        /// <param name="xmloutput">xml con datos de output</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Initialization(string xmlinput, out string xmloutput)
        {
            xmloutput = null;
            return Errors.IERR_OK;
        }

        /// <summary>
        /// Procesa la muestra serial entregada como parámetro
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="xmloutput">xml con datos de output</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Process(string xmlinput, out string xmloutput)
        {
            int ret = Errors.IERR_OK;
            xmloutput = null;
            Parameters paramin;
            String msgErr = "";
            bool res = false;
           
            try
            {
                LOG.Info("Inicio de proceso de lectura por:" + _name);
                LOG.Info("Lectura OCR");
                //0.- Chequo parametros entrada
                if (String.IsNullOrEmpty(xmlinput))
                {
                    LOG.Info("VirtualPrinterHandkey Error - Prametro de entrada nulo");
                    return Errors.IERR_BAD_PARAMETER;
                }

                //1. Parseo parametros
                paramin = Parameters.DeserializeFromXml(xmlinput);
                if (paramin == null)
                {
                    LOG.Info("VirtualPrinterHandkey Error - DeserializeFromXml retorna nulo");
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- TODO - Comienza el proceso especifico con la muestra
                byte[] sample = Convert.FromBase64String((string)paramin.GetValue("Sample"));

                if (paramin.GetValue("COM").ToString().Equals(CONFIG.ComPort) == false) 
                {
                    ret = -1;
                }
                if (ret != -1)
                {
                    LOG.Error("Este sistema no esta configurado para leer OCR, solo se permiten tarjetas con codigos de barra leidas por pistola");
                    //string lectura = Encoding.UTF7.GetString(sample, 0, 94);

                    //CedulaTemplateLite objCedulaTemplate = new CedulaTemplateLite(lectura);
                    //if (objCedulaTemplate != null)
                    //{
                    //    _session.Clear();
                    //    CurrentSessionContext.Bind(_session);
                    //    //LOG.Debug("Se agrega el contexto de la sesión");
                    //    res = Bio.EpAustral.Core.AccessControl.giveAccessControl(objCedulaTemplate.Rut, out msgErr, (short)CONFIG.TypeMark, CardExpress);
                    //    if (res == true)
                    //    {
                    //        LOG.Info("Acceso permitido a: " + objCedulaTemplate.Rut + ", apertura por rele: " + CONFIG.NumberRelay.ToString());
                    //        objArduinoRele = new BioArduinoRele.ArduinoRele();
                    //        objArduinoRele.getCOM();
                    //        objArduinoRele.openDelay(CONFIG.NumberRelay.ToString(), "1000", out msgErr);
                    //    }
                    //    else
                    //    {
                    //        LOG.Info("Acceso denegado");
                    //    }
                    //    CurrentSessionContext.Unbind(_session.SessionFactory);
                    //}
                }

            }
            catch (Exception ex)
            {

                LOG.Error("VirtualPrinterHandkey.Process Error", ex);
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }
      
        private int OpenDoor()
        {
            int res = 0;
            int resrelay = 0;
            /*USBRelayAxNoGUI.USBRelayAxDllClass g_axUSBRelayAx = new USBRelayAxDllClass();
            
            string idr = "1234";
            short port = (short)CONFIG.ComPort;
            short qRelay = 4;
            short milisec = (short)CONFIG.TimeRelay;*/
            /*(try
            {
                short reti = g_axUSBRelayAx.InitComponent(ref idr, ref port, ref qRelay);
                if (reti == 0)
                {*/
                    
                    LOG.Info("USBRelay Initialized!:" + milisec.ToString());
                    res = 0;
                  
                /*}
                else
                {
                    LOG.Info("USBRelay Initialized ERROR [" + reti.ToString() + "]");
                    res = -2;
                }
            }
            catch (Exception exe)
            {
                LOG.Error("Se produjo un error al realizar la apertura de la barrera:" + exe.Message, exe);
                res = -1;
            }
            return res;*/
                    return res;
        }

        private String FormattingTaxid(string rut)
        {
            int cont = 0;
            String format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {

                    format = rut.Substring(i, 1) + format;

                    cont++;

                }
                return format;
            }
        }
        private void CloseRele()
        {
            try
            {
                LOG.Info("Mandamos a cerrar los relays");
              
                //Ojo que estamos cerrando solo 4 rele en la barrera de estacionamiento.
                /*g_axUSBRelayAx.OffRelay(ref idr, 3);
                g_axUSBRelayAx.OffRelay(ref idr, 4);*/
                LOG.Info("Cerrar el relay");
            }
            catch (Exception exe)
            {
                LOG.Error("Error en Cerrar Rele:" + exe.Message, exe);
            }
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //LOG.Info("Se activa el temporadizador del rele");
            //int res = 0;
            //String msgErr = "";
            //objArduinoRele.openCloseAll(false, out msgErr);
            ////objArduinoRele.openDelay(CONFIG.NumberRelay.ToString(), CONFIG.TimeOff.ToString(), out msgErr);
            //objArduinoRele.openDelay(CONFIG.NumberRelay.ToString(), "1000", out msgErr);  
            //_timer.Enabled = false;
            //LOG.Info("Se desactiva temporarizador del rele");
        }

        public string ConfigToString()
        {
            throw new NotImplementedException();
        }
        #endregion Implementation of IVirtualPrinter


        public IList CardExpress
        {
            get { return _cardExpress; }
            set { _cardExpress = value; }
        }

        public object Config { get; set; }
    }
}
