﻿using BCR_WinPakPro.Domain;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCR_WinPakPro
{
    public class NHibernateHelperDC
    {
        private static ISessionFactory _sessionFactory;

        private static ISessionFactory SessionFactory(string conn)
        {
            if (_sessionFactory == null)
                InitializeSessionFactory(conn);
            return _sessionFactory;
        }

        private static void InitializeSessionFactory(string conn)
        {
            //_sessionFactory = Fluently.Configure()
            //    .Database(MsSqlConfiguration.MsSql2008.ConnectionString(conn))
            //    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<History>())
            //    .BuildSessionFactory();
            
            //Se cambio en 2021, cuando se subio NHibernate de v4 a v5.3, de 2008 a 2000
            //sino daba error de parseo en datetime2 que se agego en versiones mas nuevas. 
            //Esto es porque EPAustral tiene SQLServer 2000  omo server central
            _sessionFactory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2000.ConnectionString(conn))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<History>())
                .BuildSessionFactory();
        }

        public static ISession OpenSession(string conn)
        {
            return SessionFactory(conn).OpenSession();
        }
    }
}
