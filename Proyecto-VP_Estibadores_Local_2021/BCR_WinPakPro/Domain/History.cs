﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCR_WinPakPro.Domain
{
    public class History
    {
        public virtual int RecordId { get; set; }
        public virtual int SeqId { get; set; }
        public virtual int Link1 { get; set; }
        public virtual int Link2 { get; set; }
        public virtual int Link3 { get; set; }
        public virtual int Link4 { get; set; }
        public virtual int Param1 { get; set; }
        public virtual short Param2 { get; set; }
        public virtual string Param3 { get; set; }
        public virtual DateTime RecvTime { get; set; }
        public virtual short Type1 { get; set; }
        public virtual short Type2 { get; set; }
        public virtual short UserPriority { get; set; }
        public virtual int NodeId { get; set; }
        public virtual int UserId { get; set; }
        public virtual int AccountId { get; set; }
        public virtual DateTime GenTime { get; set; }
        public virtual DateTime TimeStamp { get; set; }
        public virtual string SpareStr1 { get; set; }
        public virtual string SpareStr2 { get; set; }

        
    }

    public class HistoryMap : ClassMap<History>
    {
        public HistoryMap()
        {
            Table("[History]");
            Id(x => x.RecordId).Column("[RecordID]").GeneratedBy.Identity();
            Map(x => x.SeqId).Column("[SeqID]");
            Map(x => x.Link1).Column("[Link1]");
            Map(x => x.Link2).Column("[Link2]");
            Map(x => x.Link3).Column("[Link3]");
            Map(x => x.Link4).Column("[Link4]");
            Map(x => x.Param1).Column("[Param1]");
            Map(x => x.Param2).Column("[Param2]");
            Map(x => x.Param3).Column("[Param3]");
            Map(x => x.RecvTime).Column("[RecvTime]");
            Map(x => x.Type1).Column("[Type1]");
            Map(x => x.Type2).Column("[Type2]");
            Map(x => x.UserPriority).Column("[UserPriority]");
            Map(x => x.NodeId).Column("[NodeID]");
            Map(x => x.UserId).Column("[UserID]");
            Map(x => x.AccountId).Column("[AccountID]");
            Map(x => x.GenTime).Column("[GenTime]");
            Map(x => x.TimeStamp).Column("[TimeStamp]");
            Map(x => x.SpareStr1).Column("[SpareStr1]");
            Map(x => x.SpareStr2).Column("[SpareStr2]");
        }
    }
}
