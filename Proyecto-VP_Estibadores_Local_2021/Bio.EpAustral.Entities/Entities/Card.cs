/*

insert license info here

*/

using System;

namespace Bio.EPAustral.Entities
{
	/// <summary>
	///	Generated by MyGeneration using the Serdar's NHibernate Object Mapping 1.1 template (based on Gustavo's) - serdar@argelab.net
	/// </summary>
	[Serializable]
	public class Card 
	{
		#region Private Members
		private bool _isChanged;
		private bool _isDeleted;
		private int _recordid; 
		private int _accountid; 
		private DateTime _timestamp; 
		private int _userid; 
		private int _nodeid; 
		private byte _deleted; 
		private short _userpriority; 
		private string _cardnumber; 
		private short _issue; 
		private int _cardholderid; 
		private int _accesslevelid; 
		private DateTime _activationdate; 
		private DateTime _expirationdate; 
		private short _noofusesleft; 
		private string _pin1; 
		private string _pin2; 
		private int _cmdfileid; 
		private short _cardstatus; 
		private short _display; 
		private int _backdrop1id; 
		private int _backdrop2id; 
		private int _actiongroupid; 
		private int _lastreaderhid; 
		private DateTime _lastreaderdate; 
		private byte _printstatus; 
		private short _sparew1; 
		private short _sparew2; 
		private short _sparew3; 
		private short _sparew4; 
		private int _sparedw1; 
		private int _sparedw2; 
		private int _sparedw3; 
		private int _sparedw4; 
		private string _sparestr1; 
		private string _sparestr2; 
		private Guid _msrepl_tran_version; 		
		#endregion
		
		#region Default ( Empty ) Class Constuctor
		/// <summary>
		/// default constructor
		/// </summary>
		public Card()
		{
			_recordid = 0; 
			_accountid = 0; 
			_timestamp = DateTime.MinValue; 
			_userid = 0; 
			_nodeid = 0; 
			_deleted = 0; 
			_userpriority = 0; 
			_cardnumber = null; 
			_issue = 0; 
			_cardholderid = 0; 
			_accesslevelid = 0; 
			_activationdate = DateTime.MinValue; 
			_expirationdate = DateTime.MinValue; 
			_noofusesleft = 0; 
			_pin1 = null; 
			_pin2 = null; 
			_cmdfileid = 0; 
			_cardstatus = 0; 
			_display = 0; 
			_backdrop1id = 0; 
			_backdrop2id = 0; 
			_actiongroupid = 0; 
			_lastreaderhid = 0; 
			_lastreaderdate = DateTime.MinValue; 
			_printstatus = 0; 
			_sparew1 = 0; 
			_sparew2 = 0; 
			_sparew3 = 0; 
			_sparew4 = 0; 
			_sparedw1 = 0; 
			_sparedw2 = 0; 
			_sparedw3 = 0; 
			_sparedw4 = 0; 
			_sparestr1 = null; 
			_sparestr2 = null; 
			_msrepl_tran_version =  Guid.Empty; 
		}
		#endregion // End of Default ( Empty ) Class Constuctor

		#region Public Properties
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int Recordid
		{
			get { return _recordid; }
			set { _isChanged |= (_recordid != value); _recordid = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int Accountid
		{
			get { return _accountid; }
			set { _isChanged |= (_accountid != value); _accountid = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual DateTime TimeStamp
		{
			get { return _timestamp; }
			set { _isChanged |= (_timestamp != value); _timestamp = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int Userid
		{
			get { return _userid; }
			set { _isChanged |= (_userid != value); _userid = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int Nodeid
		{
			get { return _nodeid; }
			set { _isChanged |= (_nodeid != value); _nodeid = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual byte Deleted
		{
			get { return _deleted; }
			set { _isChanged |= (_deleted != value); _deleted = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual short UserPriority
		{
			get { return _userpriority; }
			set { _isChanged |= (_userpriority != value); _userpriority = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual string CardNumber
		{
			get { return _cardnumber; }
			set	
			{
				if ( value != null )
					if( value.Length > 30)
						throw new ArgumentOutOfRangeException("Invalid value for CardNumber", value, value.ToString());
				
				_isChanged |= (_cardnumber != value); _cardnumber = value;
			}
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual short Issue
		{
			get { return _issue; }
			set { _isChanged |= (_issue != value); _issue = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int CardHolderid
		{
			get { return _cardholderid; }
			set { _isChanged |= (_cardholderid != value); _cardholderid = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int AccessLevelid
		{
			get { return _accesslevelid; }
			set { _isChanged |= (_accesslevelid != value); _accesslevelid = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual DateTime ActivationDate
		{
			get { return _activationdate; }
			set { _isChanged |= (_activationdate != value); _activationdate = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual DateTime ExpirationDate
		{
			get { return _expirationdate; }
			set { _isChanged |= (_expirationdate != value); _expirationdate = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual short NoOfUsesLeft
		{
			get { return _noofusesleft; }
			set { _isChanged |= (_noofusesleft != value); _noofusesleft = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual string Pin1
		{
			get { return _pin1; }
			set	
			{
				if ( value != null )
					if( value.Length > 5)
						throw new ArgumentOutOfRangeException("Invalid value for Pin1", value, value.ToString());
				
				_isChanged |= (_pin1 != value); _pin1 = value;
			}
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual string Pin2
		{
			get { return _pin2; }
			set	
			{
				if ( value != null )
					if( value.Length > 5)
						throw new ArgumentOutOfRangeException("Invalid value for Pin2", value, value.ToString());
				
				_isChanged |= (_pin2 != value); _pin2 = value;
			}
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int CmdFileid
		{
			get { return _cmdfileid; }
			set { _isChanged |= (_cmdfileid != value); _cmdfileid = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual short CardStatus
		{
			get { return _cardstatus; }
			set { _isChanged |= (_cardstatus != value); _cardstatus = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual short Display
		{
			get { return _display; }
			set { _isChanged |= (_display != value); _display = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int BackDrop1id
		{
			get { return _backdrop1id; }
			set { _isChanged |= (_backdrop1id != value); _backdrop1id = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int BackDrop2id
		{
			get { return _backdrop2id; }
			set { _isChanged |= (_backdrop2id != value); _backdrop2id = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int ActionGroupid
		{
			get { return _actiongroupid; }
			set { _isChanged |= (_actiongroupid != value); _actiongroupid = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int LastReaderhid
		{
			get { return _lastreaderhid; }
			set { _isChanged |= (_lastreaderhid != value); _lastreaderhid = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual DateTime LastReaderDate
		{
			get { return _lastreaderdate; }
			set { _isChanged |= (_lastreaderdate != value); _lastreaderdate = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual byte PrintStatus
		{
			get { return _printstatus; }
			set { _isChanged |= (_printstatus != value); _printstatus = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual short Sparew1
		{
			get { return _sparew1; }
			set { _isChanged |= (_sparew1 != value); _sparew1 = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual short Sparew2
		{
			get { return _sparew2; }
			set { _isChanged |= (_sparew2 != value); _sparew2 = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual short Sparew3
		{
			get { return _sparew3; }
			set { _isChanged |= (_sparew3 != value); _sparew3 = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual short Sparew4
		{
			get { return _sparew4; }
			set { _isChanged |= (_sparew4 != value); _sparew4 = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int Sparedw1
		{
			get { return _sparedw1; }
			set { _isChanged |= (_sparedw1 != value); _sparedw1 = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int Sparedw2
		{
			get { return _sparedw2; }
			set { _isChanged |= (_sparedw2 != value); _sparedw2 = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int Sparedw3
		{
			get { return _sparedw3; }
			set { _isChanged |= (_sparedw3 != value); _sparedw3 = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual int Sparedw4
		{
			get { return _sparedw4; }
			set { _isChanged |= (_sparedw4 != value); _sparedw4 = value; }
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual string SpareStr1
		{
			get { return _sparestr1; }
			set	
			{
				if ( value != null )
					if( value.Length > 30)
						throw new ArgumentOutOfRangeException("Invalid value for SpareStr1", value, value.ToString());
				
				_isChanged |= (_sparestr1 != value); _sparestr1 = value;
			}
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual string SpareStr2
		{
			get { return _sparestr2; }
			set	
			{
				if ( value != null )
					if( value.Length > 30)
						throw new ArgumentOutOfRangeException("Invalid value for SpareStr2", value, value.ToString());
				
				_isChanged |= (_sparestr2 != value); _sparestr2 = value;
			}
		}
			
		/// <summary>
		/// 
		/// </summary>		
		public virtual Guid MsreplTranVersion
		{
			get { return _msrepl_tran_version; }
			set { _isChanged |= (_msrepl_tran_version != value); _msrepl_tran_version = value; }
		}
			
		/// <summary>
		/// Returns whether or not the object has changed it's values.
		/// </summary>
		public virtual bool IsChanged
		{
			get { return _isChanged; }
		}
		
		/// <summary>
		/// Returns whether or not the object has changed it's values.
		/// </summary>
		public virtual bool IsDeleted
		{
			get { return _isDeleted; }
		}
		
		#endregion 
		
		
		#region Public Functions
		
		/// <summary>
		/// mark the item as deleted
		/// </summary>
		public virtual void MarkAsDeleted()
		{
			_isDeleted = true;
			_isChanged = true;
		}
		
		
		#endregion
		
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			Card castObj = (Card)obj; 
			return ( castObj != null ) &&
				( this._recordid == castObj.Recordid );
		}
		
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			
			int hash = 57; 
			hash = 27 * hash * _recordid.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
}
