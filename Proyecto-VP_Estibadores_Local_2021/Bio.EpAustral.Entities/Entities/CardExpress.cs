﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bio.EpAustral.Entities
{
    public class CardExpress
    {
        #region Private Members
        private string _cardNumber;
        private DateTime _expirationDate;
        private short _cardStatus;
        private string _sparestr1;
        private string _sparestr2;
        private int _recordid;
        private int _cardholderid;
        #endregion

        #region Default ( Empty ) Class Constuctor
        public CardExpress()
		{
            _recordid = 0;
			_cardNumber = null; 
			_expirationDate = DateTime.MinValue; 
			_cardStatus = 0;
            _sparestr1 = null;
            _sparestr2 = null;
            _cardholderid = 0;
		}
		#endregion

        #region Public Properties

        /// <summary>
        /// 
        /// </summary>		
        public string CardNumber
        {
            get
            {
                return _cardNumber;
            }
            set
            {
                _cardNumber = value;
            }
        }

        public DateTime ExpirationDate
        {
            get
            {
                return _expirationDate;
            }
            set
            {
                _expirationDate = value;
            }
        }

        public short CardStatus
        {
            get
            {
                return _cardStatus;
            }
            set
            {
                _cardStatus = value;
            }
        }

        public virtual string SpareStr1
        {
            get { return _sparestr1; }
            set { _sparestr1 = value; }
        }

        public virtual string SpareStr2
        {
            get { return _sparestr2; }
            set { _sparestr2 = value; }
        }

        public virtual int Recordid
        {
            get { return _recordid; }
            set { _recordid = value; }
        }

        public virtual int CardHolderid
        {
            get { return _cardholderid; }
            set { _cardholderid = value; }
        }

        #endregion
    }
}
