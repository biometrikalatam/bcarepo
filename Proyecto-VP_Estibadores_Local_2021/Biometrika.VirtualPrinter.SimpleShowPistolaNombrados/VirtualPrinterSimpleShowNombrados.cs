﻿using System;
using Biometrika.VirtualPrinter.Api;
using log4net;
//using NHibernate;
using Biometrika.VirtualPrinter.SimpleShow.src.Config;
using System.IO;
using System.Timers;
using System.Collections;
using Biometrika.VirtualPrinter.SimpleShowPistola.src.Helpers;
using Bio.EpAustral.Core;
using NHibernate;
using NHibernate.Context;
using System.Collections.Generic;

namespace Biometrika.VirtualPrinter.SimpleShow
{
    public class VirtualPrinterSimpleShowNombrados : IVirtualPrinter
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(VirtualPrinterSimpleShowNombrados));
        //internal LectorConfig CONFIG;
        private Timer _timer;
        private ISession _SESSION;
        private ISessionFactory _SESSION_FACTORY;
        static private BioArduinoRele.ArduinoRele objArduinoRele;
        private short port;
        private short qRelay;
        private short milisec;
        private string idr;
        private short reti;


#region Implementation of IVirtualPrinter

        private string _name;
        private IList _cardExpress;
        private LectorConfig _Config;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public IList CardExpress
        {
            get { return _cardExpress; }
            set { _cardExpress = value; }
        }

        public object Config
        {
            get { return _Config; }
            set { _Config = (LectorConfig)value; }
        }

        public VirtualPrinterSimpleShowNombrados()
        {
            String comport = "";
            String msgErr = "";
            //Prodríamos tomar el archivo de configuración de virtual printer y leer los parámetros.
            //var uri = new UriBuilder(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
            //String pathassembly= Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
            //CONFIG =
            //        SerializeHelper.DeserializeFromFile<LectorConfig>(pathassembly + "\\LectorPistolaConfigOut.cfg");

            objArduinoRele = new BioArduinoRele.ArduinoRele();
            objArduinoRele.getCOM();

        }

        /// <summary>
        /// Inicializa la clase con los parametros que necesite para su ejecucion, de tal forma
        /// de acelerar los procesos. Puede leer conexion a BD, configuraciones expecificas,
        /// chequear conexiones, etc.
        /// </summary>
        /// <param name="xmlinput">xml con datos input - </param>
        /// <param name="xmloutput">xml con datos de output</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Initialization(string xmlinput, out string xmloutput)
        {
            xmloutput = null;
            try
            {
                LOG.Debug("VirtualPrinterSimpleShow.Initialization IN...");
                var uri = new UriBuilder(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
                string pathassembly = Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
                LOG.Debug("VirtualPrinterSimpleShow.Initialization Leyendo config en: " +
                            pathassembly + "\\" + xmlinput + "...");
                Config = BioCore.SerialComm.SerializeHelper.DeserializeFromFile<LectorConfig>(pathassembly + "\\" +
                        xmlinput); // LectorPistolaConfigOut.cfg");

                LOG.Debug("VirtualPrinterSimpleShow.Initialization - Set conexión...");
                string msgErr = null;
                _SESSION = Bio.EpAustral.Core.DBConn.Connection(out msgErr);
                if (_SESSION == null)
                {
                    LOG.Error("VirtualPrinterSimpleShow.Initialization - _SESSION = null!");
                    return Errors.IERR_DATABASE;
                } else
                {
                    LOG.Debug("VirtualPrinterSimpleShow.Initialization - Conexion establecida a : " +
                                _SESSION.Connection.ConnectionString);
                }

                LOG.Debug("VirtualPrinterSimpleShow.Initialization - Carga CardExpress...");
                CardExpress = AccessControl.getCards(out msgErr);
                if (CardExpress == null || CardExpress.Count == 0)
                {
                    LOG.Error("VirtualPrinterSimpleShow.Initialization - CardExpress = null o vacio!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("VirtualPrinterSimpleShow.Initialization- Excp Error: " + ex.Message);
                LOG.Debug("VirtualPrinterSimpleShow.Initialization OUT! => CONFIG == Null");
                return Errors.IERR_UNKNOWN;
            }
            LOG.Debug("VirtualPrinterSimpleShow.Initialization OUT! => CONFIG != null => " + 
                        (Config!= null).ToString());
            return Errors.IERR_OK;
        }

        /// <summary>
        /// Procesa la muestra serial entregada como parámetro
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="xmloutput">xml con datos de output</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Process(string xmlinput, out string xmloutput)
        {
            int ret = Errors.IERR_OK;
            bool res = false;
            xmloutput = null;
            Parameters paramin;

            String msgErr = "";

            try
            {
                LOG.Debug("VirtualPrinterSimpleShowNombrados.Process - IN => Inicio de proceso de lectura por:" + _name);
                //0.- Chequo parametros entrada
                if (String.IsNullOrEmpty(xmlinput))
                {
                    LOG.Error("VirtualPrinterSimpleShowNombrados.Process Error - Prametro de entrada nulo");
                    return Errors.IERR_BAD_PARAMETER;
                }

                //1. Parseo parametros
                paramin = Parameters.DeserializeFromXml(xmlinput);
                if (paramin == null)
                {
                    LOG.Error("VirtualPrinterSimpleShowNombrados.Process Error - DeserializeFromXml retorna nulo");
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- TODO - Comienza el proceso especifico con la muestra
                if (paramin.GetValue("COM").ToString().Equals(((LectorConfig)Config).ComPort) == false)
                {
                    LOG.Debug("VirtualPrinterSimpleShowNombrados.Process - Sale por diferencia de COM => " +
                        " COM Config = " + ((LectorConfig)Config).ComPort + " != COM Procesado = " + paramin.GetValue("COM").ToString());
                    ret = -1;
                }
                if (ret != -1)
                {
                    LOG.Debug("VirtualPrinterSimpleShowNombrados.Process - Recupera sample a procesar...");
                    byte[] sample = Convert.FromBase64String((string)paramin.GetValue("Sample"));
                    LOG.Debug("VirtualPrinterSimpleShowNombrados.Process - saple != null => " + (sample != null).ToString());

                    LOG.Info("VirtualPrinterSimpleShowNombrados.Process - Parsea codigo de barra leido...");
                    string rut = CedulaHelper.GetRUT(sample);
                    bool bOpenAndSave = false;
                    if (string.IsNullOrEmpty(rut))
                    {
                        LOG.Error("VirtualPrinterSimpleShowNombrados.Process - rut recuperado de lecutra == Null => Sale...");
                    }
                    else
                    {
                        LOG.Info("VirtualPrinterSimpleShowNombrados.Process - Verificando acceso apra RUT = " + rut + "...");
                        _SESSION.Clear();
                        CurrentSessionContext.Bind(_SESSION);
                        if (((LectorConfig)Config).Sentido == 1 ||
                            (((LectorConfig)Config).Sentido == 2 && ((LectorConfig)Config).CheckBusinessRuleInOut))
                        {
                            //LOG.Info("Se agrega el contexto de la sesión");
                            res = AccessControl.giveAccessControl(rut, out msgErr, (Int16)((LectorConfig)Config).TypeMark, CardExpress);
                            bOpenAndSave = false;
                            if (!res)
                                LOG.Debug("VirtualPrinterSimpleShowNombrados.Process - Acceso denegado a rut: " + rut);
                            else
                            {
                                bOpenAndSave = true;
                            }
                            
                        }
                        else
                        {
                            //result = AccessControl.GetCard(rut, ((LectorConfig)Config).ConnectionString.ToString());
                            //if (result == null)
                            //    LOG.Debug("VirtualPrinterSimpleShow.Process - Salida denegada por result == null");
                            //else
                            //{
                                bOpenAndSave = true;
                            //}
                        }

                        if (bOpenAndSave)
                        {
                            LOG.Debug("VirtualPrinterSimpleShowNombrados.Process - Acceso permitido a rut: " + rut + ", apertura por rele: " +
                                ((LectorConfig)Config).NumberRelay.ToString());
                            if (objArduinoRele == null)
                            {
                                LOG.Debug("VirtualPrinterSimpleShow.Process - Crea objArduino...");
                                objArduinoRele = new BioArduinoRele.ArduinoRele();
                                objArduinoRele.getCOM();
                            }
                            LOG.Debug("VirtualPrinterSimpleShowNombrados.Process - openDelay de " +
                                ((LectorConfig)Config).NumberRelay.ToString() + " por " +
                                ((LectorConfig)Config).TimeOff.ToString() + " milisegundos...");
                            // "1000"
                            objArduinoRele.openDelay(((LectorConfig)Config).NumberRelay.ToString(),
                                                     ((LectorConfig)Config).TimeOff.ToString(), out msgErr);

                            //Si es salida y sin chequeo debo grabar acceso
                            if (((LectorConfig)Config).Sentido == 2  && !((LectorConfig)Config).CheckBusinessRuleInOut)
                            {
                                LOG.Debug("VirtualPrinterSimpleShowNombrados.Process - Grabando salida sin chequeo de BR de rut: " + rut);
                                res = AccessControl.SaveAccessOutWithoutControl(rut, out msgErr,
                                                        (Int16)((LectorConfig)Config).TypeMark, CardExpress);
                            }
                            else
                            {
                                LOG.Debug("VirtualPrinterSimpleShowNombrados.Process - Acceso denegado a rut: " + rut);
                            }


                            //_SESSION.Clear();
                            //CurrentSessionContext.Bind(_SESSION);
                            ////LOG.Info("Se agrega el contexto de la sesión");
                            //res = AccessControl.giveAccessControl(rut, out msgErr, (Int16)((LectorConfig)Config).TypeMark, CardExpress);
                            //if (res == true)
                            //{
                            //    LOG.Debug("VirtualPrinterSimpleShowNombrados.Process - Acceso permitido a rut: " + rut + ", apertura por rele: " +
                            //        ((LectorConfig)Config).NumberRelay.ToString());
                            //    if (objArduinoRele == null)
                            //    {
                            //        LOG.Debug("VirtualPrinterSimpleShow.Process - Crea objArduino...");
                            //        objArduinoRele = new BioArduinoRele.ArduinoRele();
                            //        objArduinoRele.getCOM();
                            //    }
                            //    LOG.Debug("VirtualPrinterSimpleShowNombrados.Process - openDelay de " +
                            //        ((LectorConfig)Config).NumberRelay.ToString() + " por " +
                            //        ((LectorConfig)Config).TimeOff.ToString() + " milisegundos...");
                            //    // "1000"
                            //    objArduinoRele.openDelay(((LectorConfig)Config).NumberRelay.ToString(),
                            //                             ((LectorConfig)Config).TimeOff.ToString(), out msgErr);
                            //}
                            //else
                            //{
                            //    LOG.Debug("VirtualPrinterSimpleShowNombrados.Process - Acceso denegado a rut: " + rut);
                            //}
                            //CurrentSessionContext.Unbind(_SESSION.SessionFactory);
                        }
                        CurrentSessionContext.Unbind(_SESSION.SessionFactory);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("VirtualPrinterSimpleShowNombrados.Process Excp Error", ex);
                ret = Errors.IERR_UNKNOWN;
            }
            LOG.Debug("VirtualPrinterSimpleShowNombrados.Process - OUT! ret = " + ret.ToString());
            return ret;
        }

        private int OpenDoor()
        {
            int res = 0;
            int resrelay = 0;
            /*USBRelayAxNoGUI.USBRelayAxDllClass g_axUSBRelayAx = new USBRelayAxDllClass();
            
            string idr = "1234";
            short port = (short)CONFIG.ComPort;
            short qRelay = 4;
            short milisec = (short)CONFIG.TimeRelay;*/
            /*(try
            {
                short reti = g_axUSBRelayAx.InitComponent(ref idr, ref port, ref qRelay);
                if (reti == 0)
                {*/
                    
                    LOG.Info("USBRelay Initialized!:" + milisec.ToString());
                    res = 0;
                  
                /*}
                else
                {
                    LOG.Info("USBRelay Initialized ERROR [" + reti.ToString() + "]");
                    res = -2;
                }
            }
            catch (Exception exe)
            {
                LOG.Error("Se produjo un error al realizar la apertura de la barrera:" + exe.Message, exe);
                res = -1;
            }
            return res;*/
                    return res;
        }

        private String FormattingTaxid(string rut)
        {
            int cont = 0;
            String format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {

                    format = rut.Substring(i, 1) + format;

                    cont++;

                }
                return format;
            }
        }
        private void CloseRele()
        {
            try
            {
                LOG.Info("Mandamos a cerrar los relays");
              
                //Ojo que estamos cerrando solo 4 rele en la barrera de estacionamiento.
                /*g_axUSBRelayAx.OffRelay(ref idr, 3);
                g_axUSBRelayAx.OffRelay(ref idr, 4);*/
                LOG.Info("Cerrar el relay");
            }
            catch (Exception exe)
            {
                LOG.Error("Error en Cerrar Rele:" + exe.Message, exe);
            }
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //LOG.Info("Se activa el temporadizador del rele");
            //int res = 0;
            //String msgErr = "";
            //objArduinoRele.openCloseAll(false, out msgErr);
            ////objArduinoRele.openDelay(CONFIG.NumberRelay.ToString(), CONFIG.TimeOff.ToString(), out msgErr);
            //objArduinoRele.openDelay(CONFIG.NumberRelay.ToString(), "1000", out msgErr);  
            //_timer.Enabled = false;
            //LOG.Info("Se desactiva temporarizador del rele");
        }

        public string ConfigToString()
        {
            string ret = "";
            try
            {
                if (Config != null)
                {
                    LectorConfig conf = (LectorConfig)Config;
                    ret = "ComPort=" + conf.ComPort + "|" +
                          "TypeMark=" + conf.TypeMark + "|" +
                          "NumberRelay=" + conf.NumberRelay + "|" +
                          "TimeOff=" + conf.TimeOff + "|" +
                          "DateFormat=" + conf.DateFormat;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("VirtualPrinterSimpleShow.ConfigToString - Excp Error: " + ex.Message);
            }
            return ret;
        }
        #endregion Implementation of IVirtualPrinter



    }
}
