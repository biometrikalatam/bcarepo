﻿using log4net;
using System;
using System.Text;

namespace Biometrika.VirtualPrinter.SimpleShowPistola.src.Helpers
{
    internal class CedulaHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(CedulaHelper));

        internal static string GetRUT(byte[] sample)
        {
            string ret = null;
            //Bio.Core.SerialComm.Cedula.CedulaTemplate2 cedula = null;
            BioCore.SerialComm.Cedula.CedulaTemplate cedulabarcode = null;
            try
            {
                LOG.Debug("SimpleShowPistola.src.Helpers.GetRUT IN...");
                if (sample != null)
                {
                    LOG.Debug("frmMain.LeerCedula - Parsea cedula leida...");
                    cedulabarcode = new BioCore.SerialComm.Cedula.CedulaTemplate(sample);
                    if (cedulabarcode != null)
                    {
                        ret = cedulabarcode.Rut;
                        //ret = cedulabarcode.Rut.Substring(0, cedulabarcode.Rut.Length-2); //FormattingTaxid(cedulabarcode.Rut); 
                    }
                 }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("SimpleShowPistola.src.Helpers.GetRUT Excp Error: " + ex.Message);
            }
            LOG.Debug("SimpleShowPistola.src.Helpers.GetRUT OUT! ret = " + 
                        (string.IsNullOrEmpty(ret) ? "Null": ret));
            return ret;
        }

        /// <summary>
        /// Saca Mantisa de rut
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        private string FormattingTaxid(string rut)
        {
            int cont = 0;
            String format;
            if (rut.Length == 0)
            {
                return null;
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                return rut.Substring(0,rut.Length-1);
            }
        }
    }

    
}
