using System;
using System.Drawing;

namespace BioCore.SerialComm.Feedback
{
	/// <summary>
	/// Summary description for NullFeedbackProvider.
	/// </summary>
	public class NullFeedbackProvider : IFeedbackProvider
	{
		public NullFeedbackProvider()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// Forma generica de pasar datos a un Feedback provider
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="data"></param>
		public void SetData (IExecutor sender, object data)
		{
			
		}

		/// <summary>
		/// Coloca el estado del dispositivo, util para feedback por estado
		/// </summary>
		/// <param name="sender">dispositivo</param>
		/// <param name="state">estado</param>
		public void SetState (IExecutor sender, int state)
		{
			
		}

		/// <summary>
		/// Coloca un titulo en el feedback
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="caption"></param>
		/// <param name="args"></param>
		public void SetCaption (IExecutor sender, string caption, params object[] args)
		{
			
		}

		/// <summary>
		/// Despliega una imagen en el provider
		/// </summary>
		/// <param name="img">imagen que debe mostrar</param>
		public void SetImage (IExecutor sender, Image img)
		{
			
		}

		public void SetImage(IExecutor sender, object oSample)
		{
			
		}

		/// <summary>
		/// Permite desplegar en el provider la hora, esto es util para operaciones largas
		/// </summary>
		/// <param name="time"></param>
		public void ShowClock (IExecutor sender, DateTime time)
		{
			
		}

		/// <summary>
		/// Coloca en el Feedback provider el tiempo transcurrido de la operacion
		/// </summary>
		/// <param name="elapsed"></param>
		public void ShowElapsedTime (IExecutor sender, TimeSpan elapsed)
		{
			
		}

		/// <summary>
		/// Agrega un mensaje, notar que se usa String.Format para desplegar el texto, 
		/// </summary>
		/// <param name="format"></param>
		/// <param name="data"></param>
		public void AddMessage (IExecutor sender, string format, params object[] data)
		{
			
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender">Dispositivo que esta ejecutando</param>
		/// <param name="type"><see cref="PromptType"/></param>
		/// <param name="format"></param>
		/// <param name="data"></param>
		public PromptResult Prompt (IExecutor sender, PromptType type, string format, params object[] data)
		{
			return PromptResult.None;	
		}

		/// <summary>
		/// Inicia una barra de avance
		/// </summary>
		/// <param name="sender">Dispositivo que esta ejecutando</param>
		/// <param name="min"></param>
		/// <param name="max"></param>
		public void StartProgress (IExecutor sender, int min, int max)
		{
			
		}

		/// <summary>
		/// Hace avanzar la barra
		/// </summary>
		/// <param name="amount"></param>
		public void StepProgress (IExecutor sender, int amount)
		{
			
		}

		/// <summary>
		/// Detiene la barra de avance
		/// </summary>
		public void StopProgress (IExecutor sender)
		{
			
		}

		/// <summary>
		/// Devuelva la barra a su valor minimo
		/// </summary>
		/// <param name="sender"></param>
		public void ResetProgress (IExecutor sender)
		{
		
		}

		public void ErrorMessage (IExecutor sensor, string msg)
		{
			
		}

		public void Configure (IConfig config)
		{
			
		}
	}
}
