﻿using log4net;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bio.Core.SerialComm.Cedula
{
    public class CedulaTemplateLite
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(CedulaTemplateLite));
        private string _rut;
        private string dv;
        public CedulaTemplateLite(byte[] lectura)
        {
            LOG.Debug("Lectura byte[]");

            string type = "";
            type = Encoding.UTF7.GetString(lectura, 0, 1);
            LOG.Info("Tipo de cedula: " + type);

            if (type != "h")
            {
                LOG.Info("Cedula vieja");
                _rut = Encoding.UTF7.GetString(lectura, 0, 9).Replace('\0', ' ').TrimEnd();
                LOG.Debug("Rut leido: " + _rut);

                string rutAux = _rut.Substring(0, 7);
                string digVaux = _rut.Substring(7, 1);

                if (Dv(rutAux) == digVaux)
                {
                    LOG.Debug("Es menor a 10 millones");
                    _rut = rutAux;
                }
                else
                {
                    LOG.Debug("Es mayor a 10 millones");
                    _rut = Encoding.UTF7.GetString(lectura, 0, 8).Replace('\0', ' ').TrimEnd();
                    dv = Encoding.UTF7.GetString(lectura, 8, 1).Replace('\0', ' ').TrimEnd();
                }
            }
            else if (type == "h")
            {
                LOG.Info("Cedula nueva");

                string control = "RUN=";
                string controlFin = "&";

                string lecturaAux = Encoding.UTF7.GetString(lectura, 0, lectura.Length);

                int indexControl = lecturaAux.IndexOf(control);
                int indexControlFin = lecturaAux.IndexOf(controlFin);

                int controlInit = indexControl + control.Length;
                int controlEnd = (indexControlFin - indexControl) - control.Length;

                _rut = lecturaAux.Substring(controlInit, controlEnd);

                _rut = _rut.Substring(0, _rut.Length - 2);
            }
        }
        public CedulaTemplateLite(string lectura)
        {
            string[] nacionalidades = new string[] { "CHL", "ARG", "PER" };
            string pais = "";
            string type = "";

            LOG.Debug(lectura);
            String[] nuevaCedula = lectura.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries); //richTextBox1.Text.Split('\n', StringSplitOptions.RemoveEmptyEntries);

            LOG.Debug(nuevaCedula[0]);
            for (int i = 0; i < nacionalidades.Length; i++)
            {
                if (nuevaCedula[1].Contains(nacionalidades[i]))
                {
                    pais = nacionalidades[i];
                    break;
                }
            }

            type = nuevaCedula[0].Substring(2, 2);

            if (type.CompareTo("IN") == 0)
            {
                // cédula nueva.

                string[] datos = nuevaCedula[1].Split(new char[] { '<' }, StringSplitOptions.RemoveEmptyEntries);
                string[] separador = new string[] { pais };
                string[] subDatos = datos[0].Split(separador, StringSplitOptions.None);
                _rut = subDatos[1];
            }
            else if (type.CompareTo("ID") == 0)
            {
                // cedula antigua
                // rut
                int rutControl = nuevaCedula[0].Length - 17;

                if (nuevaCedula[0].Substring(rutControl, 1).CompareTo("<") == 0)
                {
                    rutControl -= 1;
                }

                _rut = nuevaCedula[0].Substring(7, rutControl - 7);
            }
        }

        public string Rut
        {
            get
            {
                return _rut;
            }
        }

        public string DV
        {
            get
            {
                return dv;
            }
        }

        private static string Dv(string rut)
        {
            int suma = 0;

            for (int x = rut.Length - 1; x >= 0; x--)
            {
                suma += int.Parse(char.IsDigit(rut[x]) ? rut[x].ToString() : "0") * (((rut.Length - (x + 1)) % 6) + 2);
            }

            int numericDigito = (11 - suma % 11);
            string digito = numericDigito == 11 ? "0" : numericDigito == 10 ? "K" : numericDigito.ToString();

            return digito;
        }
    }
}
