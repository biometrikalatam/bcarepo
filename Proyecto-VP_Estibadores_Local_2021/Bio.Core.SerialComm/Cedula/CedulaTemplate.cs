using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
//using BioCore.SerialComm.Fingerprints;
using log4net;

namespace BioCore.SerialComm.Cedula
{
	/// <summary>
	/// Summary description for CedulaTemplate.
	/// </summary>
	public class CedulaTemplate //: FingerTemplate
	{
		private static readonly ILog LOG = LogManager.GetLogger(typeof(CedulaTemplate));

		public CedulaTemplate(byte[] pdf) : base()
		{
			isValid = false;
			string[] nacionalidades = new string[]{ "CHL", "ARG", "PER" };
			try
			{
				String type = "";
				String type2 = "";

				type = Encoding.UTF7.GetString(pdf, 0, 1);
				type2 = Encoding.UTF7.GetString(pdf, 2, 2);

				if (type != "I" && type2 != "IN" && type2 != "ID")
				{
					if (type != "h")
					{
						rut = Encoding.UTF7.GetString(pdf, 0, 8).Replace('\0', ' ').TrimEnd();

						string rutAux = rut.Substring(0, rut.Length-1);
						string digVaux = rut.Substring(rut.Length - 1, 1);

						string test = Dv(rutAux);

						if (test == digVaux)
						{
							rut = rut.Substring(0, rut.Length - 1);
						}

						apellido = Encoding.UTF7.GetString(pdf, 19, 30).Replace('\0', ' ').TrimEnd();
						pais = Encoding.UTF7.GetString(pdf, 49, 3).Replace('\0', ' ').TrimEnd();
						numeroDeSerie = Encoding.UTF7.GetString(pdf, 58, 10).Replace('\0', ' ').TrimEnd();

						string aa = Encoding.UTF7.GetString(pdf, 52, 2);
						string mm = Encoding.UTF7.GetString(pdf, 54, 2);
						string dd = Encoding.UTF7.GetString(pdf, 56, 2);
						expiracion = new DateTime(Convert.ToInt32(aa) + 2000, Convert.ToInt32(mm), Convert.ToInt32(dd));

						FingerId = (pdf[73] << 24) + (pdf[72] << 16) + (pdf[71] << 8) + (pdf[70]);
						int pcLen = (pdf[77] << 24) + (pdf[76] << 16) + (pdf[75] << 8) + (pdf[74]);
						byte[] pc1 = new Byte[400];
						Buffer.BlockCopy(pdf, 78, pc1, 0, pcLen);
						SetMinutiae(pc1, pcLen);
						isValid = true;
					}
					else if (type == "h")
					{
						string control = "RUN=";
						string controlFin = "&";

						string lectura = Encoding.UTF7.GetString(pdf, 0, pdf.Length);

						int indexControl = lectura.IndexOf(control);
						int indexControlFin = lectura.IndexOf(controlFin);

						int controlInit = indexControl + control.Length;
						int controlEnd = (indexControlFin - indexControl) - control.Length;

						rut = lectura.Substring(controlInit, controlEnd);

						rut = rut.Substring(0, rut.Length - 2);

						//rut = Encoding.UTF7.GetString(pdf).Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[0] + Encoding.UTF7.GetString(pdf).Split('?')[1].Split('&')[0].Split('=')[1].Split('-')[1]; //Encoding.UTF7.GetString(pdf).Split('?')[1].Split('&')[0].Split('=')[1];
						isValid = true;
					}
				}
				else
				{
					string lectura = Encoding.UTF7.GetString(pdf, 0, 94);
					LOG.Debug(lectura);
					String[] nuevaCedula = lectura.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries); //richTextBox1.Text.Split('\n', StringSplitOptions.RemoveEmptyEntries);

					LOG.Debug(nuevaCedula[0]);
					for (int i = 0; i < nacionalidades.Length; i++)
					{
						if (nuevaCedula[1].Contains(nacionalidades[i]))
						{
							pais = nacionalidades[i];
							break;
						}
					}

					type = nuevaCedula[0].Substring(2, 2);

					if (type.CompareTo("IN") == 0)
					{
						// c�dula nueva.

						string[] datos = nuevaCedula[1].Split(new char[] { '<' }, StringSplitOptions.RemoveEmptyEntries);
						string[] separador = new string[] { pais };
						string[] subDatos = datos[0].Split(separador, StringSplitOptions.None);
						rut = subDatos[1];

						isValid = true;
					}
					else if (type.CompareTo("ID") == 0)
					{
						// cedula antigua
						// rut
						int rutControl = nuevaCedula[0].Length - 17;

						if (nuevaCedula[0].Substring(rutControl, 1).CompareTo("<") == 0)
						{
							rutControl -= 1;
						}

						rut = nuevaCedula[0].Substring(7, rutControl - 7);

						// nombre y apellidos
						//string[] nombreCompleto = nuevaCedula[2].Split('<');

						//apellido = nombreCompleto[0] + " " + nombreCompleto[1];
						//nombre = nombreCompleto[3];

						isValid = true;
					}
				}
			}
			catch (Exception ex)
			{
				isValid = false;
				LOG.Error("CedulaTemplate.CedulaTemplate",ex);
			}
		}

		private void SetMinutiae (byte[] pc1, int len)
		{
			minu = pc1;
			minuLen = len;
		}

		private int _fingerid;
		public int FingerId
		{
			get { return _fingerid; }
			set { _fingerid = value; }
		}

		public string Rut 
		{
			get { return rut; }
			set { rut = value; }
		}
		private string rut;

		public string Nombre
		{
			get { return nombre; }
			set { nombre = value; }
		}
		private string nombre;

		public string Apellido
		{
			get { return apellido; }
			set { apellido = value; }
		}
		private string apellido;

		public string Pais
		{
			get { return pais; }
			set { pais = value; }
		}
		private string pais;


		public string NumeroDeSerie
		{
			get { return numeroDeSerie; }
			set { numeroDeSerie = value; }
		}
		private string numeroDeSerie;

		
		public DateTime FechaExpiracion
		{
			get { return expiracion; }
			set { expiracion = value; }
		}
		private DateTime expiracion;
		private byte[] minu;
		private int minuLen;
		private bool isValid;


		public bool IsValid
		{
			get
			{
				return isValid;
			}
		}

		public int Quality
		{
			get { return isValid ? minu.Length : 0; }
		}

		/// <summary>
		/// Escribe a un stream la muestra
		/// </summary>
		/// <param name="stream">stream de salida</param>
		public void Serialize (StreamWriter stream)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(CedulaTemplate));
			serializer.Serialize(stream, this);
		}

		/// <summary>
		/// Lee desde un stream la muestra
		/// </summary>
		/// <param name="stream">stream de entrada</param>
		public void Deserialize (StreamReader stream)
		{
			
		}

		public Array Data
		{
			get { return minu; }
		}

		public int DataLen
		{
			get {  return minuLen; }
		}

		public string ToXML()
		{
			return "<Cedula>" +
						"<Rut>" + Rut +"</Rut>" +
						"<Apellido>" + Apellido +"</Apellido>" +
						"<Pais>" + Pais +"</Pais>" +
						"<NumeroDeSerie>" + NumeroDeSerie +"</NumeroDeSerie>" +
						"<Expiracion>" + FechaExpiracion.ToString("dd/MM/yyyy") +"</Expiracion>" +
						"<Finger>" + FingerId.ToString() +"</Finger>" +
						"<BiometricData>" + Convert.ToBase64String(minu) +"</BiometricData>" +
						"<LenBiometricData>" + minuLen.ToString() +"</LenBiometricData>" +
				   "</Cedula>";
		}
		private static string Dv(string rut)
		{
			int suma = 0;

			for (int x = rut.Length - 1; x >= 0; x--)
			{
				suma += int.Parse(char.IsDigit(rut[x]) ? rut[x].ToString() : "0") * (((rut.Length - (x + 1)) % 6) + 2);
			}

			int numericDigito = (11 - suma % 11);
			string digito = numericDigito == 11 ? "0" : numericDigito == 10 ? "K" : numericDigito.ToString();

			return digito;
		}
	}
}
