using System;

namespace BioCore.SerialComm
{
	/// <summary>
	/// Summary description for IExecutor.
	/// </summary>
	public interface IExecutor
	{
		string Name { get; }
		Type Driver { get; }

		IResult Execute(ICommand cmd, IArguments args);
	}
}
