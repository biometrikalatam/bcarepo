using System;

namespace Biokey.Fingerprints
{
	/// <summary>
	/// Summary description for NecTemplateMatcher.
	/// </summary>
	public class NecTemplateMatcher : IMatcher
	{
		
//		NECPID.MainClass nec = new NECPID.MainClass();
		public NecTemplateMatcher()
		{
		
		}

		public bool Match (ITemplate t1, ITemplate t2, out int score)
		{
//			score = 0;
//			try
//			{
//				int pc1_1_len = t1.DataLen, pc1_2_len = t2.DataLen;
//				Array pc1_1 = t1.Data, pc1_2 = t2.Data;
//				score = nec.match_1a1 (ref pc1_1, ref pc1_1_len, ref pc1_2, ref pc1_2_len);
//				return score > 600;
//			} catch (Exception ex)
//			{
//				BIM.MainForm.LOG.Error("NecTemplateMatcher.Match",ex);
//				return false;
//			}
			score = 0;
			bool bRes = false;
			try
			{				
				if (t1 == null || t2 == null) return false;
				
				Array arr = Array.CreateInstance(typeof(byte),t2.DataLen);
				Array.Copy(t2.Data,arr,t2.DataLen);
				byte[] NECtemplate_sample = (byte[])arr; //(byte[])arr;
				
				byte[] NECtemplate_ref = (byte[])t1.Data;

				//1.- Importo templates a BFT
				int bft_NECtemplate_ref =
					BFTWrapper.bft_load_template(NECtemplate_ref,
					NECtemplate_ref.Length);
				
				int bft_NECtemplate_sample =
					BFTWrapper.bft_load_template(NECtemplate_sample,
					NECtemplate_sample.Length);
				
				//2.- Si no da error hago match
				if (bft_NECtemplate_sample > 0 && bft_NECtemplate_ref > 0)
				{
					double scoreBFT = 0;
					int err = BFTWrapper.bft_match(bft_NECtemplate_ref,
						bft_NECtemplate_sample,
						500,4,out scoreBFT,null);
					//Es verificacion positiva, si da 0 no fallo pero
					//verificacion negativa
					if (err >= 0)
					{
						score = (int) scoreBFT;
						bRes = (err == 1);	//Si es match positivo, da 1 BFT
					}
				} 
				else
				{
					BIM.MainForm.LOG.Error("NecTemplateMatcher.Match.BFTWrapper.bft_load_template - " +
						"[Error bft_NECtemplate_sample = " +
						bft_NECtemplate_sample.ToString() + " - " +
						"Error bft_NECtemplate_ref = " +
						bft_NECtemplate_ref.ToString() + "]");
				}
				
				//3.- Libero memoria si existen templates
				if (bft_NECtemplate_sample > 0)
				{
					//4.- Libero memoria de template BFT
					BFTWrapper.bft_delete_template(bft_NECtemplate_sample);
				}
				if (bft_NECtemplate_ref > 0)
				{
					//4.- Libero memoria de template BFT
					BFTWrapper.bft_delete_template(bft_NECtemplate_ref);
				}
				
			} 
			catch (Exception ex)
			{
				BIM.MainForm.LOG.Error("NecTemplateMatcher.Match",ex);
				return false;
			}
			return bRes;
		}
	}
}
