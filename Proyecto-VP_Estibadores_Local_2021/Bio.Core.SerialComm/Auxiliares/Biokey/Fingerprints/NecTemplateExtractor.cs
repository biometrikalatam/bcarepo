namespace Biokey.Fingerprints
{
	/// <summary>
	/// Summary description for NecTemplateExtractor.
	/// </summary>
	public class NecTemplateExtractor : IFeatureExtractor
	{
		public NecTemplateExtractor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public bool ExtractFeature (ISample sample, out ITemplate template)
		{
			template = null;
			FingerSample fsample = sample as FingerSample;
			if (fsample == null)
				return false;
			NecFingerTemplate necTemplate = new NecFingerTemplate(fsample);
			template = necTemplate;
			return true;
		}
	}
}
