using System;
using System.IO;
using BIM;


namespace Biokey.Fingerprints
{
	/// <summary>
	/// Summary description for NecFingerTemplate.
	/// </summary>
	public class NecFingerTemplate : FingerTemplate
	{
		//private Nec.Quality quality;
		private int quality;
		private byte[] raw;
		//Nec.QualityCheck necQCheck = new Nec.QualityCheck();
		int pcLen;
		Array pc1;
		private int error;
		
//		NECPID.MainClass nec = new NECPID.MainClass();

		public NecFingerTemplate (FingerSample fsample)
		{
//			raw =  RawImageManipulator.Adjust(fsample.Data, fsample.Width, fsample.Height, 512, 512);
//			Array rawArray = raw;
//			quality = nec.Q_Check (ref rawArray);
//			pcLen = 400;
//			pc1 = new byte[400];
//			error = nec.raw_to_pc1 (ref rawArray, ref pc1, ref pcLen);
//			if (error != 0) 
//				BIM.MainForm.LOG.Error("NecFingerTemplate.NecFingerTemplate",
//										  "nec.raw_to_pc1 error [" + error.ToString() + "]");

			raw = fsample.Data; 
			try
			{
		
				int bft_NECtemplate = 0;
				//1.- Creo sample BFT desde RAW
				int bft_sample = 0;
				bft_sample = BFTWrapper.bfc_new_sample(raw, 512, 512, 8, 500, 500, 0);
			
				//2.-Si no da error, extraigo template VF
				//				int score = 0;				
				if (bft_sample > 0)
				{
					bft_NECtemplate = 
						BFTWrapper.bft_sample_to_template(bft_sample,4,10, 
						out quality, null);
				} 
				else
				{
					MainForm.LOG.Error("NecFingerTemplate.BFTWrapper.bfc_new_sample - [Error BFT = " +
						bft_sample.ToString() + "]");
					error = bft_sample;
				}
				
				//3.- Si no da error, extraigo template
				if (bft_NECtemplate > 0)
				{
					byte[] templateNEC = Convert.FromBase64String(
						BFTWrapper.bft_template_to_base64_str(bft_NECtemplate));
					pc1 = templateNEC;
					pcLen = templateNEC.Length;
					
					//Borro memoria de template BFT
					BFTWrapper.bft_delete_template(bft_NECtemplate);
				} 
				else
				{
					MainForm.LOG.Error("NecFingerTemplate.BFTWrapper.bft_sample_to_template - [Error BFT = " +
						bft_NECtemplate.ToString() + "]");
				}
				
				//5.- Si existe, libero memoria de BFT del smaple
				if (bft_sample > 0)
				{
					BFTWrapper.bfc_delete_sample(bft_sample);
				}
				
			} 
			catch (Exception ex)
			{
				MainForm.LOG.Error("NecFingerTemplate.NecFingerTemplate",ex);

			}

		}


		public int Error
		{
			get
			{
				return error;
			}
		}
		public override int Quality
		{
			get { return quality; }
		}

		/// <summary>
		/// Escribe a un stream la muestra
		/// </summary>
		/// <param name="stream">stream de salida</param>
		public override void Serialize (StreamWriter stream)
		{
			byte[] rawpc1 = pc1 as byte[];
			stream.WriteLine("{0}|{1}", pcLen, Convert.ToBase64String (rawpc1));
		}

		/// <summary>
		/// Lee desde un stream la muestra
		/// </summary>
		/// <param name="stream">stream de entrada</param>
		public override void Deserialize (StreamReader stream)
		{
			string[] data = stream.ReadToEnd ().Split ('|');
			pcLen = Convert.ToInt32 (data[0]);
			pc1 = Convert.FromBase64String (data[1]);

		}

		public override Array Data
		{
			get { return pc1;  }
		}

		public override int DataLen
		{
			get { return pcLen; }
		}

	}
}
