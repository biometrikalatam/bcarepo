using System;
using System.IO;

namespace Biokey.Fingerprints
{
	/// <summary>
	/// Summary description for FingerTemplate.
	/// </summary>
	public abstract class FingerTemplate : ITemplate
	{
		public FingerTemplate()
		{
			
		}

		public int FingerId
		{
			get { return fingerId; }
			set { fingerId = value; }
		}
		private int fingerId;


		public abstract int Quality { get; }
		/// <summary>
		/// Escribe a un stream la muestra
		/// </summary>
		/// <param name="stream">stream de salida</param>
		public abstract void Serialize (StreamWriter stream);

		/// <summary>
		/// Lee desde un stream la muestra
		/// </summary>
		/// <param name="stream">stream de entrada</param>
		public abstract void Deserialize (StreamReader stream);

		public abstract Array Data { get; }
		public abstract int DataLen { get; }

		
	}
}
