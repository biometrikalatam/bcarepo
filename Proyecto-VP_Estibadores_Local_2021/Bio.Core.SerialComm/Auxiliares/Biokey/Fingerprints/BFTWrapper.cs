using System.Runtime.InteropServices;
using stdole;

namespace Biokey.Fingerprints
{
	/// <summary>
	/// Summary description for BFTWrapper.
	/// </summary>
	public class BFTWrapper
	{
		public BFTWrapper() { }
		
		[DllImport("bokbft32.dll")]
		public static extern int bok_wsq_encode(int hraw_image, float factor);

		[DllImport("bokbft32.dll")]
		public static extern int bok_wsq_decode(int hraw_image);

		
		[DllImport("bokbft32.dll")]
		public static extern int bfc_resize(int sample, 
			int new_width,
			int new_height, 
			int back_color); 

		[DllImport("bokbft32.dll")]
		public static extern string bfc_sample_to_base64_str(int sample, 
			int include_extra_info); 
		
		[DllImport("bokbft32.dll")]
		public static extern int bfc_from_ipicture2(IPicture pic, int resX, 
			int resY, int resNewX, 
			int resNewY);

		[DllImport("bokbft32.dll")]
		public static extern int bfc_from_ipicture(IPicture pic, int resX, int resY);

		[DllImport("bokbft32.dll")]
		public static extern int bfc_new_sample(byte[] raw, int width, 
			int height, int depth, 
			int resolutionx, int resolutiony, 
			int stride);
        
		[DllImport("bokbft32.dll")]
		public static extern int bfc_delete_sample(int shandle);

		[DllImport("bokbft32.dll")]
		public static extern int bfc_sample_rawsize(int hsample);

		[DllImport("bokbft32.dll")]
		public static extern int bfc_copy_to_raw(int shandle, byte[] raw_dest, int raw_dest_size);

		[DllImport("bokbft32.dll")]
		public static extern int bfc_sample_width(int shandle);

		[DllImport("bokbft32.dll")]
		public static extern int bfc_sample_height(int shandle);

		[DllImport("bokbft32.dll")]
		public static extern int bfc_quality(int shandle);

		[DllImport("bokbft32.dll")]
		public static extern int bfc_quality_class(int shandle);

		[DllImport("bokbft32.dll")]
		public static extern void bok_bfc_startup();

		[DllImport("bokbft32.dll")]
		public static extern void bok_bfc_shutdown();

		[DllImport("bokbft32.dll")]
		public static extern int bft_sample_to_template(int handle, int format, 
			int threshold, out int score, 
			byte[] args);
        
		[DllImport("bokbft32.dll")]
		public static extern int bft_delete_template(int handle);
        
		[DllImport("bokbft32.dll")]
		public static extern int bft_load_from_file(byte[] path, int format);
        
		[DllImport("bokbft32.dll")]
		public static extern int bft_match(int template1, int template2, double threshold, int algorithm, out double score, byte[] args);
        
		[DllImport("bokbft32.dll")]
		public static extern int bfc_load_from_file(byte[] file, int format);

		[DllImport("bokbft32.dll")]
		public static extern int bft_load_template(byte[] byTemplate, int size);

		[DllImport("bokbft32.dll")]
		public static extern int bfc_sensor_find(int sensor_class, int model);

		[DllImport("bokbft32.dll")]
		public static extern int bft_sensor_open(int sensor_class, int sensor_model, byte[] args);

		[DllImport("bokbft32.dll")]
		public static extern int bft_sensor_close(int sensor_handle);

		[DllImport("bokbft32.dll")]
		public static extern int bft_get_sample(int sensor, int timeout, int normalize, int window);

		[DllImport("bokbft32.dll")]
		public static extern int bft_enroll(int sensor, int timeout, int normalize, int window);

		[DllImport("bokbft32.dll")]
		public static extern string bft_template_to_base64_str(int template);

	}
}
