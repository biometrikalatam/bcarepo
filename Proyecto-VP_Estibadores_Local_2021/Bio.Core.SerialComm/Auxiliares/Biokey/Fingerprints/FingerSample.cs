using System;
using System.IO;

namespace Biokey.Fingerprints
{
	/// <summary>
	/// Summary description for FingerImage.
	/// </summary>
	public abstract class FingerSample : ISample
	{
		/// <summary>
		/// Escribe a un stream la muestra
		/// </summary>
		/// <param name="stream">stream de salida</param>
		public abstract void Serialize (StreamWriter stream);

		/// <summary>
		/// Lee desde un stream la muestra
		/// </summary>
		/// <param name="stream">stream de entrada</param>
		public abstract void Deserialize (StreamReader stream);

		public DateTime TimeStamp 
		{ 
			get { return timeStamp; } 
		}
		private DateTime timeStamp = DateTime.Now;

		public abstract string Source { get; }
		
		public string AutentiCode
		{
			get { return Crypto.Hash.CalcMAC(TimeStamp, Source, Data); }
		}

		public abstract bool IsValid { get; }


		public abstract byte[] Data { get; }

		public abstract int Width { get; }
		public abstract int Height { get; }

		public float DpiX;
		public float DpiY;
		
	}
}
