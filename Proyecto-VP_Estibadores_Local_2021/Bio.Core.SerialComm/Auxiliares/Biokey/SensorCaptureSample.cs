namespace Biokey
{
	/// <summary>
	/// Delegado para eventos de captura de muestra en un sensor
	/// </summary>
	public delegate void SampleAvailableEvent(ISensor sensor, ISample sample);
}
