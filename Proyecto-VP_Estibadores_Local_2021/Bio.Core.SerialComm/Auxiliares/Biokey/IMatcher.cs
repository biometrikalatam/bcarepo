namespace Biokey
{
	/// <summary>
	/// Summary description for IMatcher.
	/// </summary>
	public interface IMatcher
	{
		bool Match(ITemplate t1, ITemplate t2, out int score);
	}
}
