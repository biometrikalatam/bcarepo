using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Xml;

namespace Biokey.Crypto
{
	/// <summary>
	/// A class for XML Digital Signature Handling
	/// Sign and Check XMLDSIG Documents.
	/// </summary>
	public sealed class XmlSigner
	{
		private static string keyStorage = "XmlSignerStorage";

		private XmlSigner()
		{
			
		}

		/// <summary>
		/// storage where to store the public/private key pairs for
		/// signing algorithm.
		/// </summary>
		public static string KeyStorage
		{
			get
			{
				return keyStorage;
			}
			set
			{
				keyStorage = value;
			}
		}

		/// <summary>
		/// Create a Cripto Service Provider.
		/// Uses KeyStorage property to determinate where to find the
		/// key pairs.
		/// </summary>
		/// <returns></returns>
		private static  RSACryptoServiceProvider GenCsp()
		{
			CspParameters cp = new CspParameters();
			cp.KeyContainerName = keyStorage;
			cp.Flags = CspProviderFlags.UseMachineKeyStore;
			cp.KeyNumber = 2;
			RSACryptoServiceProvider.UseMachineKeyStore = true;
			return new RSACryptoServiceProvider(cp);
		}

		/// <summary>
		/// Generate a XML Signature XML-DSIG
		/// </summary>
		/// <param name="fileName">fileName to sign</param>
		public static void SignXml (string fileName)
		{
			XmlDocument xmlDocument = new XmlDocument();
			using (FileStream fstrm = new FileStream(fileName, FileMode.Open, FileAccess.Read))
			{
				xmlDocument.Load(fstrm);
				fstrm.Close ();
			}
			
			RSACryptoServiceProvider csp = GenCsp();
			SignedXml sXml = new SignedXml(xmlDocument); 
			sXml.SigningKey = csp;
			sXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigCanonicalizationUrl;

			Reference reference = new Reference(String.Empty);
			reference.AddTransform(new XmlDsigEnvelopedSignatureTransform(false));
			sXml.AddReference (reference);

			sXml.ComputeSignature ();
			XmlElement sig = sXml.GetXml ();
			xmlDocument.DocumentElement.AppendChild (sig);
			using (FileStream fstrm = new FileStream(fileName, FileMode.Create, FileAccess.Write))
			{
				xmlDocument.Save (fstrm);
				fstrm.Close ();
			}
		}

		/// <summary>
		/// Check the digital signature of a file
		/// </summary>
		/// <param name="fileName">Name of signed file</param>
		/// <returns></returns>
		public static bool CheckSignXml (string fileName)
		{
			RSACryptoServiceProvider csp = GenCsp();
			
			XmlDocument xmldoc = new XmlDocument();
			using (FileStream fstrm = new FileStream(fileName, FileMode.Open, FileAccess.Read))
			{
				xmldoc.Load(fstrm);
				fstrm.Close ();
			}
			
			SignedXml sxml = new SignedXml(xmldoc);
			try
			{
				XmlNode dsig = xmldoc.GetElementsByTagName("Signature", SignedXml.XmlDsigNamespaceUrl)[0];
				sxml.LoadXml ((XmlElement) dsig);
			}
			catch
			{
				return false;
			}
			return sxml.CheckSignature (csp);
		}
	}
}
