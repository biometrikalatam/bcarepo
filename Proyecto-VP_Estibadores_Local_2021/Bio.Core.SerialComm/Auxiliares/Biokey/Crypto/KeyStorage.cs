using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Xml;

namespace Biokey.Crypto
{
	/// <summary>
	/// Summary description for KeyStorage.
	/// </summary>
	public sealed class KeyStorage
	{
		private static string keyRSA = "<RSAKeyValue><Modulus>l0V54ebqCm5qlHp2gr+mGLPihgq2YY+8LUTsq0tTjJrG3zDqjG6+ffwmTNIv2Hoq7xPCa6vWnhiJURALDauTrCEby/aIZrPYJOJkXMvhxNUqQ1mCQ/zp3sE2bf2X3Vewn3sT+otUNtbwo/ci8C0rcXRApzulI2VSq9GAD2Q6Rec=</Modulus><Exponent>AQAB</Exponent><P>xnXxU5upIynyJBqlXL1Kg2ecvdSRVnQPQF3EnFWtmtvsIUEEX2D/i3IhKVxte3lJtXZ0I/RamoaTzvpXFlqsDQ==</P><Q>wyER34NRSVq5U14fUIGKq9BPq2pC8oBK9gGhTenOGZlT07EEAiKWyqUqiUvzwrt/2mWjVIi1owzFcqRAJygYww==</Q><DP>s22mxiSSAszFs4azwukRGUTwHWC2FifQMi2UuJNt5fi3wEj8Hw+fev+wo6iBA4Vg8sMptegSkmrh79u+KOPJcQ==</DP><DQ>iKvOB/7DXZy098aSKmJBjawfUVHhSFdBqeNkZt0fZWC2jSyMQS6W7aMYEwhm2PtZRtAvwVLr3iNg3LZc1NBJ3w==</DQ><InverseQ>Z+mqJv81Bkm2HyLqKz03DksjorAxV2EfPqP5+2FwxnwgXBgst2CfAJzxZAQBxGmbsca42WLZ+MiT757wy+YzfA==</InverseQ><D>DvWW7QkZ4fgzYAWQTLPJKC4524mZqaI+OLfVM8daNtBjlFTRDRwyiGD0JKqwgpbqQgRROLagnbfNdI/CmBJ8ouUjA3uXpFaVYa96Juv4rvxZL9HIyXYhdH8KbskC93zd9T9dggYylHzJgZocCKCHEelbLIBfKS3IFNEXufsSMJk=</D></RSAKeyValue>";
		
		public static void LoadRSA(out RSACryptoServiceProvider rsa)
		{
			rsa = new RSACryptoServiceProvider();
			try
			{
				rsa.FromXmlString(KeyStorage.keyRSA);
			} catch (Exception ex)
			{
				BIM.MainForm.LOG.Error("KeyStorage.LoadRSA",ex);
				System.Windows.Forms.MessageBox.Show(
					"Error obteniendo CSP Criptogr�fico! " +
					" Comun�quese con el proveedor...",
					"Atenci�n",
					System.Windows.Forms.MessageBoxButtons.OK,
					System.Windows.Forms.MessageBoxIcon.Stop);
			}
		}
		
		public static  void GenerateToFile(string path)
		{
			RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
			string s = rsa.ToXmlString(true);
			StreamWriter sr = new StreamWriter(path);
			sr.Write(s);
			sr.Close();
		}
		
		public static  void Generate(string storageName, out RSACryptoServiceProvider rsa)
		{
			CspParameters cp = new CspParameters();
			cp.KeyContainerName = storageName;
			cp.Flags = CspProviderFlags.UseMachineKeyStore;
			cp.KeyNumber = 2;
		
			RSACryptoServiceProvider.UseMachineKeyStore = true;
			rsa = new RSACryptoServiceProvider(cp);
			
		}

		public static  void Generate(string storageName)
		{
			RSACryptoServiceProvider rsa;
			Generate(storageName, out rsa);
		}


		public static XmlDocument SignXml (string storageName, XmlDocument xmlDocument)
		{
			RSACryptoServiceProvider csp;
			//Generate(storageName, out csp);
			LoadRSA(out csp);
			SignedXml sXml = new SignedXml(xmlDocument); 
			sXml.SigningKey = csp;
			sXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigCanonicalizationUrl;

			Reference reference = new Reference(String.Empty);
			reference.AddTransform(new XmlDsigEnvelopedSignatureTransform(false));
			sXml.AddReference (reference);

			sXml.ComputeSignature ();
			XmlElement sig = sXml.GetXml ();
			xmlDocument.DocumentElement.AppendChild (sig);
			return xmlDocument;
		}

		public static void SignXml (string storageName, string fileName)
		{
			XmlDocument xmlDocument = new XmlDocument();
			using (FileStream fstrm = new FileStream(fileName, FileMode.Open, FileAccess.Read))
			{
				xmlDocument.Load(fstrm);
				fstrm.Close ();
			}
			
			RSACryptoServiceProvider csp;
			//Generate(storageName, out csp);
			LoadRSA(out csp);
			SignedXml sXml = new SignedXml(xmlDocument); 
			sXml.SigningKey = csp;
			sXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigCanonicalizationUrl;

			Reference reference = new Reference(String.Empty);
			reference.AddTransform(new XmlDsigEnvelopedSignatureTransform(false));
			sXml.AddReference (reference);

			sXml.ComputeSignature ();
			XmlElement sig = sXml.GetXml ();
			xmlDocument.DocumentElement.AppendChild (sig);
			using (FileStream fstrm = new FileStream(fileName, FileMode.Create, FileAccess.Write))
			{
				xmlDocument.Save (fstrm);
				fstrm.Close ();
			}
			
			
		}

		public static bool CheckSignXml (string storageName, XmlDocument xmldoc)
		{
			RSACryptoServiceProvider csp;
			//Generate(storageName, out csp);
			LoadRSA(out csp);
			SignedXml sxml = new SignedXml(xmldoc);
			try
			{
				XmlNode dsig = xmldoc.GetElementsByTagName("Signature", SignedXml.XmlDsigNamespaceUrl)[0];
				sxml.LoadXml ((XmlElement) dsig);
			}
			catch
			{
				return false;
			}
			return sxml.CheckSignature (csp);
		}

		public static bool CheckSignXml (string storageName, string fileName)
		{
			bool bres = false;
			
			try
			{
				RSACryptoServiceProvider csp;
				//Generate(storageName, out csp);
				LoadRSA(out csp);
			
				XmlDocument xmldoc = new XmlDocument();
				using (FileStream fstrm = new FileStream(fileName, FileMode.Open, FileAccess.Read))
				{
					xmldoc.Load(fstrm);
					fstrm.Close ();
				}
			
				SignedXml sxml = new SignedXml(xmldoc);
				try
				{
					XmlNode dsig = xmldoc.GetElementsByTagName("Signature", SignedXml.XmlDsigNamespaceUrl)[0];
					sxml.LoadXml ((XmlElement) dsig);
				}
				catch
				{
					return false;
				}
				bres = sxml.CheckSignature(csp);
			} catch(Exception ex)
			{
				BIM.MainForm.LOG.Error("KeyStorage.CheckSignXml",ex);
				System.Windows.Forms.MessageBox.Show(
				"Error verificando firma bit�cora [" + ex.Message + "]",
				"Atenci�n",
				System.Windows.Forms.MessageBoxButtons.OK,
				System.Windows.Forms.MessageBoxIcon.Stop);
				bres = false;
			}
			return bres;
		}
		
	}
}
