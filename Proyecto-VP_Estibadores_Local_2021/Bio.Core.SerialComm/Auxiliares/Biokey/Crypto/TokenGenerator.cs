using System;
using System.Security.Cryptography;

namespace Biokey.Crypto
{
	/// <summary>
	/// Summary description for TokenGenerator.
	/// </summary>
	public sealed class TokenGenerator
	{
		public static string CreateChallenge()
		{
			RandomNumberGenerator rng = RandomNumberGenerator.Create ();
			byte[] data = new byte[32];
			rng.GetNonZeroBytes (data);
			return Hash.CalcMAC (DateTime.Now, data);
		}

		public static string CreateResponse(string challenge)
		{
			return Cipher.Encrypt (Hash.CalcSHA(challenge,challenge), challenge);

		}

		public static bool CheckChallengeResponse (string response, string challenge)
		{
			string sha1 = Cipher.Decrypt (response, challenge);
			return sha1 == Hash.CalcSHA (challenge, challenge);
		}
	}
}
