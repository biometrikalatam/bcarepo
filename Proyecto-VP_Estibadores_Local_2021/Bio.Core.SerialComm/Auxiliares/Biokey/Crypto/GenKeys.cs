using System.Security.Cryptography;

namespace Biokey.Crypto
{

	public sealed class GenKeys
	{ 


		/// <summary>   
		/// Derive a key suitable for use in symetric algorithms.   
		///  </summary>   
		///  <param name="password">a password in plain text, perhaps a easy    
		///  to remember one  
		///  </param>   
		///  <param name="size">The size in BITS of the key</param>
		///  <returns>A key for use in encryption</returns>   
		public static byte[] DeriveKeyFromPassword(string password, int size)   
		{
				return DeriveKey(password, GenSalt(8), size/8);   
		}

		/// <summary>  
		/// Generate an IV (Initialization Vector) for use in Rijndael crypto  
		/// algorithm   
		/// </summary>  
		/// <param name="size">size in bits of the vector</param>
		/// <returns>a new array o bytes with a random generated IV</returns>   
		public byte[] GenIV(int size)
		{
			return GenSalt(size/8);
		}

		/// <summary>    
		/// Generate a salt array, this is a vector of bytes used for the    
		/// generation of strong keys.
		/// This method is used internally by DeriveKeyFromPassword()  
		/// </summary>    
		/// <param name="size">Size in bytes (watch out)</param>   
		/// <returns>an array of random bytes of size bytes</returns>    
		private static byte[] GenSalt(int size)    
		{   
			/// Use a cryptographic random number generator       
			RandomNumberGenerator rng = RandomNumberGenerator.Create();      
			// create the result array and fill it with non-zero bytes      
			byte[] result = new byte[size];      
			rng.GetNonZeroBytes (result);      
			return result;    
		}

 
		/// <summary>
		/// Derive a key form a password. This internally used by DeriveKeyFromPassword    
		/// This is an advanced implementation, when you know how to generate     
		/// a salt array    
		/// </summary>   
		/// <param name="password">A password, perhaps human readable and     
		/// easy to remember 
		/// </param>    
		/// <param name="salt">A salt array used to generate a password</param> 
		/// <param name="size">the size in bytes of the key</param>   
		/// <returns>a key of size bytes</returns>    
		private static byte[] DeriveKey(string password, byte[] salt, int size)
		{
			PasswordDeriveBytes pder = new PasswordDeriveBytes(password, salt);        
			pder.IterationCount = 100;        
			pder.HashName = "SHA1";        
			return pder.GetBytes (size);
		}
	}
	
}
