using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Biokey.Crypto
{
	/// <summary>
	/// Summary description for Hash.
	/// </summary>
	public sealed class Hash
	{
		public static string LicenseHash (int n, string pass, params object[] args)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append (pass);
			foreach (object arg in args)
			{
				sb.Append (arg);
			}
			MD5 md5 = MD5CryptoServiceProvider.Create ();
			byte[] dataMd5 = md5.ComputeHash (Encoding.UTF8.GetBytes (sb.ToString ()));

			sb = new StringBuilder();
			
			for (int i = 0; i < dataMd5.Length && i < n; i++)
			{
				sb.AppendFormat("{0:X2}", dataMd5[i]);
			}
			return sb.ToString ();	
		}

		public static string CalcSHA(string data1, string data2)
		{
			SHA1 sha1 = SHA1CryptoServiceProvider.Create();
			sha1.Initialize ();
			return Convert.ToBase64String (sha1.ComputeHash (Encoding.UTF8.GetBytes (data1+data2)));
		}

		public static string CalcMAC(DateTime timestamp, byte[] data)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append (timestamp);
			sb.Append (Convert.ToBase64String (data));
			using (MD5 md5 = MD5CryptoServiceProvider.Create ())
			{
				return Convert.ToBase64String (md5.ComputeHash (Encoding.UTF8.GetBytes (sb.ToString ())));
			}
		}

		public static string CalcMAC(DateTime timestamp, string source, byte[] data)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append (timestamp);
			sb.Append (source);
			sb.Append (Convert.ToBase64String (data));
			using (MD5 md5 = MD5CryptoServiceProvider.Create ())
			{
				return Convert.ToBase64String (md5.ComputeHash (Encoding.UTF8.GetBytes (sb.ToString ())));
			}
		}


		public static string CalcFileSHA (string path)
		{
			byte[] hash;
			using (MD5 md5 = MD5CryptoServiceProvider.Create ())
			{
				using (FileStream file = new FileStream(path, FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite))
				{
					hash = md5.ComputeHash (file);
				}
			}
			return Convert.ToBase64String (hash);
		}
	}
}
