using System;
using System.IO;
using System.IO.Ports;
using Biokey.Feedback;
using Biokey.SerialPort;

namespace Biokey.BarCode
{
	/// <summary>
	/// Summary description for Pdf417.
	/// </summary>
	public class BarCodeReader : ISensor
	{
		public string name;
		

		public BarCodeReader(string name, IConfig config, IFeedbackProvider feedback)
		{
			this.feedback = feedback;
			this.name = name;
			Configure(config);
		}




		public string Name
		{
			get { return name; }
		}

		public Type Driver
		{
			get { return typeof(Driver); }
		}


		public IResult Execute (ICommand cmd, IArguments args)
		{
			throw new NotImplementedException ();
		}

		SerialPortConfig scfg = null;
		

		public void Configure (IConfig config)
		{
			scfg = config as SerialPortConfig;
			
			
		}

	
		public void Open ()
		{
			try 
			{
				serialPort = new System.IO.Ports.SerialPort(scfg.Port,scfg.Bauds, scfg.Parity, scfg.Bits, scfg.StopBits);
				if (!serialPort.IsOpen)
					serialPort.Open ();
				serialPort.EventFilter = SerialEvents.All;
				serialPort.ReceivedEvent += new SerialEventHandler(serialPort_ReceivedEvent);
			}catch (Exception ex)
			{
				BIM.MainForm.LOG.Error("BarCodeReader.Open",ex);
				feedback.ErrorMessage (this, "Error al abrir lector de codigo de barras! ["+ex.Message+"]" );
			}
		}



		public void Close ()
		{
			if (serialPort.IsOpen)
				serialPort.Close();
		}

		public IStatus Status ()
		{
			throw new NotImplementedException ();
		}


		public IFeedbackProvider FeedbackProvider
		{
			get 
			{
				if (feedback == null)
					feedback = new NullFeedbackProvider();
				return feedback;
			}
			set 
			{ 
				feedback = value;
			}
		}
		IFeedbackProvider feedback;

	

		public string Id
		{
			get { return String.Empty; }
		}

		public void SetActive()
		{
			//No hace nada, solo para que se implemente y no falle en otro lado
		}

		public SampleAvailableEvent OnSampleAvailableEventAvailable
		{
			get { return onSampleAvailableEvent; }
			set { onSampleAvailableEvent = value; }
		}

		public void DoEvents ()
		{
			
			
		}

		System.IO.Ports.SerialPort serialPort;
		SampleAvailableEvent onSampleAvailableEvent;

		public ISample GetSample (IArguments arguments)
		{
			if (serialPort == null || !serialPort.IsOpen)
				return null;
			serialPort.ReadTimeout =  (arguments as BarCodeArguments).Timeout;
			
			try 
			{
			
					byte [] byteBuffer = new byte[1000];                  

				int numJustRead;
				int nread = 0;
				using (MemoryStream ms = new MemoryStream())
				{
					while ((numJustRead = serialPort.Read(byteBuffer, 0, 1000)) != 0)
					{
						ms.Write(byteBuffer, 0, numJustRead);
						nread += numJustRead;
					}
					ms.Close();
					return new SerialSample(this, ms.ToArray ());
				}
			}		
			catch (Exception ex)
			{
				FeedbackProvider.AddMessage (this, ex.Message);
				BIM.MainForm.LOG.Error("BarCodeReader.GetSample",ex);				
			}
			return null;
		}

		private void serialPort_ReceivedEvent(object source, SerialEventArgs e)
		{
			if (onSampleAvailableEvent != null)
				onSampleAvailableEvent(this, new SerialSample(this, null));
		}
	}
}
