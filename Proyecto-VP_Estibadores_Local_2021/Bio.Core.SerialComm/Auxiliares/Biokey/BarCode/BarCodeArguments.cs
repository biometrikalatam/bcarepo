namespace Biokey.BarCode
{
	/// <summary>
	/// Summary description for PdfArguments.
	/// </summary>
	public class BarCodeArguments : IArguments
	{

		public BarCodeArguments(int timeout)
		{
			this.timeout = timeout;
		}

		public int Timeout
		{
			get { return timeout; }
			set { timeout = value; }
		}

		int timeout;
	}
}
