using System;
using Biokey.Feedback;

namespace Biokey.BarCode
{
	/// <summary>
	/// Summary description for Driver.
	/// </summary>
	public class Driver : SensorDriver
	{
		public Driver()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public override IDevice Create (string name, IConfig configuration, IFeedbackProvider feedback)
		{
			return new BarCodeReader(name, configuration, feedback);
		}
	}
}
