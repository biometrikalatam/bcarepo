namespace Biokey.License
{
	/// <summary>
	/// Summary description for ILicenseGenerator.
	/// </summary>
	public interface ILicenseGenerator
	{
		string GetChallenge();
		
		int  GetLicenseKey(string ApplicationId, string HardwareId, string mac, string challenge, string response, out string licenseKey, out string expiryKey);
		
	}
}
