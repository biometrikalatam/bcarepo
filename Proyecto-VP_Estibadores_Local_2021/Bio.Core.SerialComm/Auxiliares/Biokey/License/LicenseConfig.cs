namespace Biokey.License
{
	/// <summary>
	/// Summary description for LicenseConfig.
	/// </summary>
	public class LicenseConfig : IConfig
	{
		public LicenseConfig()
		{
		
		}

		public string LicenseFile
		{
			get { return licenseFile; }
			set { licenseFile = value; }
		}

		public string HardwareId
		{
			get { return hardwareId; }
			set { hardwareId = value; }
		}
		public string ApplicationId
		{
			get { return applicationId; }
			set { applicationId = value; }
		}

		string applicationId;
		private string licenseFile;
		private string hardwareId;
	}
}
