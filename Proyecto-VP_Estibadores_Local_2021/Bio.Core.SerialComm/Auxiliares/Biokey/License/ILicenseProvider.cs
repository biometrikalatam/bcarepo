namespace Biokey.License
{
	/// <summary>
	/// Descripción breve de ILicenseProvider.
	/// </summary>
	public interface ILicenseProvider : IProvider
	{
		ILicenseGenerator LicenseGenerator { get; set; }

		bool CheckLicense();

		bool CheckVersion();

		bool CheckCPUs();

		bool CheckExpiry();

		bool CheckProduct();

	}
}