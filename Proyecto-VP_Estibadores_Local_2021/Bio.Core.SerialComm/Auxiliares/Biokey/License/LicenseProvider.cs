using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using Biokey.Crypto;

namespace Biokey.License
{
	/// <summary>
	/// Summary description for LicenseProvider.
	/// </summary>
	public class LicenseProvider : ILicenseProvider
	{
		private string applicationId;
		private string licenseFile;
		private string hardwareId;
		private ILicenseGenerator licenseGenerator;

		public LicenseProvider()
		{
			
		}

		public ILicenseGenerator LicenseGenerator
		{
			get { return licenseGenerator; }
			set { licenseGenerator = value; }
		}

		public bool CheckLicense ()
		{
			return true;

		}


		public bool CheckVersion ()
		{
			throw new NotImplementedException();
		}

		public bool CheckCPUs ()
		{
			throw new NotImplementedException ();
		}

		public bool CheckExpiry ()
		{
			return true;
		
		
		}

		public bool CheckProduct ()
		{
			throw new NotImplementedException ();
		}

		public void Configure (IConfig config)
		{
			if (config is LicenseConfig)
			{
				applicationId = (config as LicenseConfig).ApplicationId;
				licenseFile = (config as LicenseConfig).LicenseFile;
				hardwareId = (config as LicenseConfig).HardwareId;
			}
		}
	}
}
