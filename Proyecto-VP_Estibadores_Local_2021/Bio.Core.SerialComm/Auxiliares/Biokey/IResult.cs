namespace Biokey
{
	/// <summary>
	/// Summary description for IResult.
	/// </summary>
	public interface IResult
	{
		int Code { get; }
	}
}
