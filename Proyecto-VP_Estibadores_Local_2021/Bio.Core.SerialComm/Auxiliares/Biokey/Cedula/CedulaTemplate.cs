using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Biokey.Fingerprints;

namespace Biokey.Cedula
{
	/// <summary>
	/// Summary description for CedulaTemplate.
	/// </summary>
	public class CedulaTemplate : FingerTemplate
	{

		public CedulaTemplate(byte[] pdf) : base()
		{
			isValid = false;
			try
			{
				rut = Encoding.UTF7.GetString(pdf, 0, 9).Replace('\0', ' ').TrimEnd();
				apellido = Encoding.UTF7.GetString(pdf, 19, 30).Replace('\0',' ').TrimEnd();
				pais = Encoding.UTF7.GetString(pdf, 49, 3).Replace('\0', ' ').TrimEnd();
				numeroDeSerie = Encoding.UTF7.GetString(pdf, 58, 10).Replace('\0', ' ').TrimEnd();

				string aa = Encoding.UTF7.GetString(pdf, 52, 2);
				string mm = Encoding.UTF7.GetString(pdf, 54, 2);
				string dd = Encoding.UTF7.GetString(pdf, 56, 2);
				expiracion = new DateTime(Convert.ToInt32(aa)+2000, Convert.ToInt32(mm), Convert.ToInt32(dd));

				FingerId = (pdf[73] << 24)+ (pdf[72] << 16) + (pdf[71] << 8)+(pdf[70]);
				int pcLen = (pdf[77] << 24)+ (pdf[76] << 16) + (pdf[75] << 8)+(pdf[74]);
				byte[] pc1 = new Byte[400];
				Buffer.BlockCopy(pdf, 78, pc1, 0, pcLen);
				SetMinutiae(pc1, pcLen);
				isValid = true;
			}
			catch (Exception ex)
			{
				isValid = false;
				BIM.MainForm.LOG.Error("CedulaTemplate.CedulaTemplate",ex);
			}
		}

		private void SetMinutiae (byte[] pc1, int len)
		{
			minu = pc1;
			minuLen = len;
		}

		public string Rut 
		{
			get { return rut; }
			set { rut = value; }
		}
		private string rut;

		public string Apellido
		{
			get { return apellido; }
			set { apellido = value; }
		}
		private string apellido;

		public string Pais
		{
			get { return pais; }
			set { pais = value; }
		}
		private string pais;


		public string NumeroDeSerie
		{
			get { return numeroDeSerie; }
			set { numeroDeSerie = value; }
		}
		private string numeroDeSerie;

		
		public DateTime FechaExpiracion
		{
			get { return expiracion; }
			set { expiracion = value; }
		}
		private DateTime expiracion;
		private byte[] minu;
		private int minuLen;
		private bool isValid;


		public bool IsValid
		{
			get
			{
				return isValid;
			}
		}

		public override int Quality
		{
			get { return isValid ? minu.Length : 0; }
		}

		/// <summary>
		/// Escribe a un stream la muestra
		/// </summary>
		/// <param name="stream">stream de salida</param>
		public override void Serialize (StreamWriter stream)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(CedulaTemplate));
			serializer.Serialize(stream, this);
		}

		/// <summary>
		/// Lee desde un stream la muestra
		/// </summary>
		/// <param name="stream">stream de entrada</param>
		public override void Deserialize (StreamReader stream)
		{
			
		}

		public override Array Data
		{
			get { return minu; }
		}

		public override int DataLen
		{
			get {  return minuLen; }
		}

	}
}
