using Biokey.Feedback;

namespace Biokey
{
	/// <summary>
	/// Summary description for IDeviceDriver.
	/// </summary>
	public interface IDeviceDriver
	{
		IDevice Create(string name, IConfig config, IFeedbackProvider feedback);
	}
}
