using System;
using System.Collections;
using System.Configuration;
using System.Runtime.Remoting;
using Biokey.Cedula;
using Biokey.Feedback;
using Biokey.Fingerprints;
using Biokey.Log;

namespace Biokey
{
	/// <summary>
	/// Summary description for Broker.
	/// </summary>
	public class Broker : IDisposable
	{
		private Hashtable deviceList = new Hashtable();
		public IFeedbackProvider feedback;
		private NecTemplateExtractor featureExtractor = new NecTemplateExtractor();
		private NecTemplateMatcher matcher = new NecTemplateMatcher();

		public Broker(IFeedbackProvider feedback)
		{
			this.feedback = feedback;
			CreateDevices();

		}
		
		public IDevice GetDevice(string name)
		{
			return DeviceList[name] as IDevice;
		}
	
		/// <summary>
		/// Ide es la concatenacion de los id de todos los dispositivos definidos
		/// </summary>
		public string GetId(string name)
		{
			foreach (IDevice device in DeviceList.Values)
			{
				if (device.Name == name)
					return device.Id;
				
			}
			return null;
		}

		public bool ExtractFeature(ISample sample, out ITemplate template)
		{
			return featureExtractor.ExtractFeature (sample, out template);
		}
		
		private void CreateDevices()
		{
			BioControlsConfig config = (BioControlsConfig) ConfigurationSettings.GetConfig ("BioControlsConfig");
			UmbralReconocimientoVL = config.UmbralReconocimientoVL ;
			UmbralReconocimientoVR = config.UmbralReconocimientoVR;
			TimeoutWSBIS = config.TimeoutWSBIS;
			AutomaticWithGoodQuality = config.AutomaticWithGoodQuality;	
			TipoCheckLicencia = config.TipoCheckLicencia.Trim();
			EnrollInCheckPositive = config.EnrollInCheckPositive;
			AddBIREnroll = config.AddBIREnroll;
			RunAppInCheckPositive = config.RunAppInCheckPositive;
			AppInCheckPositive = config.AppInCheckPositive;
			ArgumentsForAppInCheckPositive = config.ArgumentsForAppInCheckPositive;
			WaitForExitAppInCheckPositive = config.WaitForExitAppInCheckPositive;
			ShowBigResult = config.ShowBigResult;
			Estacion = config.Estacion;
			Glosa = config.Glosa;
			foreach (DeviceConfig devConf in config.Devices)
			{
				AddDevice(CreateDevice(devConf));
			}
		}

		public int UmbralReconocimientoVL = 1600;
		public int UmbralReconocimientoVR = 1600;
		
		public int TimeoutWSBIS = 60000;
		public bool AutomaticWithGoodQuality = false;	
		public string TipoCheckLicencia = "KEY";	
		
		public bool EnrollInCheckPositive = false;
		public bool AddBIREnroll = false;
		
		public bool RunAppInCheckPositive = false;
		public string AppInCheckPositive = null;
		public string ArgumentsForAppInCheckPositive = null;		
		public int WaitForExitAppInCheckPositive = 10000;

		public bool ShowBigResult = false;
		public string Estacion = null;
		public string Glosa = null;
		
		private void AddDevice (IDevice device)
		{
			if (device != null)
			{
				BIM.MainForm.LOG.Debug("Broker.AddDevice - Adding " + device.Name);
				DeviceList.Add(device.Name, device);
				device.FeedbackProvider = feedback;
				device.Open ();
				device.OnSampleAvailableEventAvailable += new SampleAvailableEvent(OnSampleAvailable);
			} else
			{
				BIM.MainForm.LOG.Debug("Broker.AddDevice - Warn - Adding NULL Device!");			
			}
			BIM.MainForm.LOG.Debug("Broker.AddDevice - AddDevice End!");			
		}

		private void OnSampleAvailable (ISensor sensor, ISample sample)
		{
			if (SampleAvailable != null)						
				SampleAvailable(sensor, sample);
		}

		public SampleAvailableEvent SampleAvailable = null;

		private Hashtable DeviceList
		{
			get { return deviceList; }
		}


		private IDevice CreateDevice (DeviceConfig conf)
		{
			string className = "Biokey."+conf.Driver+".Driver";
			ObjectHandle handle = Activator.CreateInstance ("BIM", className);
			IDeviceDriver dev = handle.Unwrap () as IDeviceDriver;
			if (dev != null)
				return dev.Create(conf.Name, conf, feedback);
			return null;
		}

		public void Dispose ()
		{
			foreach (IDevice device in DeviceList.Values)
			{
				device.Close ();
			}
			deviceList = new Hashtable();
			LogProvider.Instance.Dispose();
		}



		public bool Match (FingerTemplate t1, CedulaTemplate t2, out int score)
		{
			return matcher.Match (t1, t2, out score);
		}
		
		public void SetDevicesActive()
		{
			ICollection colKeys = deviceList.Keys;
			foreach (string nameDev in colKeys)
			{
				IDevice dev = (IDevice)deviceList[nameDev];
				dev.SetActive();
			}
			
		}
		
		public string GetSerialNumberSensor()
		{
			ICollection colKeys = deviceList.Keys;
			foreach (string nameDev in colKeys)
			{
				if (nameDev.Equals("Sensor"))
				{
					IDevice dev = (IDevice)deviceList[nameDev];
					return dev.Id;
				}
			}
			return "BIM-SSN";
		}
	}
}
