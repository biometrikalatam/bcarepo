using System.Xml.Serialization;

namespace Biokey
{
	/// <summary>
	/// Summary description for DeviceConfig.
	/// </summary>
	public class DeviceConfig : IConfig
	{
		public DeviceConfig()
		{
			
		}

		[XmlAttribute("Name")]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		[XmlAttribute("Driver")]
		public string Driver
		{
			get { return driver; }
			set { driver = value; }
		}

		
		string name;
		string driver;
	}
}
