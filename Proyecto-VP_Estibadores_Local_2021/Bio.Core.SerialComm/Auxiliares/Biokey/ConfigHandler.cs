using System.Configuration;
using System.Xml;
using System.Xml.Serialization;

namespace Biokey
{
	/// <summary>
	/// Handler para manejar una seccion propia en el archivo de configuración
	/// de las aplicaciones.
	/// </summary>
	public class ConfigHandler : IConfigurationSectionHandler
	{
		public ConfigHandler()
		{
		
		}

		/// <summary>
		/// Crea un objeto de tipo BioControlsConfig que contiene toda la configuración necesaria
		/// para usar los controles
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="configContext"></param>
		/// <param name="section"></param>
		/// <returns></returns>
		public object Create (object parent, object configContext, XmlNode section)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(BioControlsConfig));
			XmlReader reader = new XmlNodeReader(section);
			return serializer.Deserialize (reader);
		}
	}
}
