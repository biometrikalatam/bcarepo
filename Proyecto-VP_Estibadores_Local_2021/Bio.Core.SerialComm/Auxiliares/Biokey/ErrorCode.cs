namespace Biokey
{
	/// <summary>
	/// Summary description for ConfigErrors.
	/// </summary>
	public enum ErrorCode
	{
		OK,
		BadConfigClass,
		BadConfigParameter,

	}
}
