
namespace Biokey
{
	/// <summary>
	/// Summary description for IFeatureMatcher.
	/// </summary>
	public interface IFeatureMatcher
	{
		bool Match(ITemplate t1, ITemplate t2, out double score);
	}
}
