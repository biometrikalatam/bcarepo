namespace Biokey.Feedback
{
	/// <summary>
	/// Resultado de hacer un prompt, es usado por IFeedbackProvider.Prompt
	/// </summary>
	public enum PromptResult
	{
		None,
		Ok,
		Cancel,
		Yes,
		No
	}
}