namespace Biokey.Wsq
{
	/// <summary>
	/// Descripción breve de WaveletTreeNode.
	/// </summary>
	public struct WaveletTreeNode
	{
		public int X;
		public int Y;
		public int LenX;
		public int LenY;
		public int OffSet;
		
	}
}
