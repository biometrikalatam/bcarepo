namespace Biokey.Wsq.Decoder
{
	/// <summary>
	/// Tabla de huffman para decodificar.
	/// </summary>
	internal sealed class HuffmanTable : Table
	{
	                  

		public HuffmanTable() : base(null)
		{
			huffBits = new byte[MAX_HUFFBITS+1];
			huffVal = new byte[MAX_HUFFCOUNTS_WSQ+1];
			maxCode = new int[MAX_HUFFBITS+1];
			minCode = new int[MAX_HUFFBITS+1];
			valuePointer = new int[MAX_HUFFBITS+1];

			huffSize = new int[MAX_HUFFCOUNTS_WSQ+1];
			huffCode = new int[MAX_HUFFCOUNTS_WSQ+1];
			
		}

		/// <summary>
		/// Lee la tabla de huffman desde la area de memoria que
		/// se esta pasando
		/// </summary>
		/// <param name="rdata"></param>
		/// <param name="pos"></param>
		/// <returns></returns>
		public int Parse(byte[] rdata, int start, int length, int pos)
		{
			SetData(rdata, start, length);
			th = GetByte(pos++);
			int nHuffVals = 0;
			for (int k = 0; k < 16; k++)
			{
				huffBits [k] = GetByte (pos++);
				nHuffVals += huffBits [k];
			}

			if (nHuffVals > MAX_HUFFCOUNTS_WSQ)
				throw new DecoderException ("Error al tratar de crear tabla de huffman, tama�o incorrecto: "+nHuffVals);

			for (int i = 0; i < nHuffVals; i++)
				huffVal [i] = GetByte (pos++);

			BuildHuffSizes ();
			BuildHuffCodes ();
			GenDecodeTable ();
			return pos;
		}
		

		private void BuildHuffSizes ()
		{
			int k = 0;
			int j = 1;
			for (int i = 0; i <= 16; i++)
			{
				j = 1;
				while (j <= huffBits[i])
				{
					huffSize [k] = i;
					++k;
					++j;
				}
			}
			huffSize [k] = 0;
		}

		private void GenDecodeTable ()
		{

			
			int i2 = 0;
			for (int i = 1; i <= MAX_HUFFBITS; i++)
			{
				if (huffBits [i - 1] == 0)
				{
					maxCode [i] = -1;
					continue;
				}
				valuePointer [i] = i2;
				minCode [i] = huffCode [i2];
				i2 = i2 + huffBits [i - 1] - 1;
				maxCode [i] = huffCode [i2];
				i2++;
			}
		}

		private void BuildHuffCodes ()
		{
			int k = 0;
			int code = 0;
			int si = huffSize [0];
			for (;; )
			{
				do
				{
					huffCode [k++] = code++;
				} while (huffSize [k] == si);
				
				if (huffSize [k] == 0)
					break;

				do
				{
					code <<= 1;
					++si;
				} while (huffSize [k] != si);	
			}
		}

		public byte TableIdentifier
		{
			get
			{
				return th;
			}
		}

		private byte th;
		
		private int[] maxCode;
		private int[] valuePointer;
		private int[] minCode;
		private byte[] huffVal;	
		private int[] huffCode;
		private int[] huffSize;
		private byte[] huffBits;
		private const int MAX_HUFFBITS = 17;
		private const int MAX_HUFFCOUNTS_WSQ = 257;
		
	
		public byte GetHuffmanValue(int pos)
		{
			return huffVal[pos];
		}
		
		public int[] MinCode
		{
			get { return minCode; }
		}

		internal int[] MaxCode
		{
			get { return maxCode; }
		}


		public int GetValuePointer (int inx)
		{
			return valuePointer[inx];
		}
	}
}