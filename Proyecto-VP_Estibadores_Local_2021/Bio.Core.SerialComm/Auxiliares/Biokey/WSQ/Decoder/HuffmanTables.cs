namespace Biokey.Wsq.Decoder
{
	/// <summary>
	/// Conjunto de Tablas de Huffman
	/// </summary>
	internal sealed class HuffmanTables : Table
	{
		const int MAXHUFFMANTABLES = 8;

		/// <summary>
		/// Retorna las tablas que hay dentro de este objeto
		/// </summary>
		internal HuffmanTable GetTable(int pos)
		{
			if (pos < 0 || pos >= tables.Length)
				throw new DecoderException("Trata de acceder a una tabla de huffman inexistente");
			HuffmanTable htable =  tables[pos];
			if (htable == null)
				throw new DecoderException("La tabla {0} no existe", pos);
			return htable;
		}

		/// <summary>
		/// Indica la cantidad de tablas que tiene este objeto
		/// </summary>
		public int Count
		{
			get { return count; }

		}

		/// <summary>
		/// Crea la lista de tablas de huffman
		/// </summary>
		/// <param name="dataLength">largo en bytes del area donde estan las tablas de huffman</param>
		/// <param name="data">datos en bytes de donde se extraeran las tablas</param>
		public HuffmanTables (byte[] data, int start, int dataLength) : base (data, start, dataLength)
		{
			int pos = 0;
			while (pos < dataLength)
			{
				HuffmanTable tbl = new HuffmanTable();
				pos = tbl.Parse (data, start, dataLength, pos);
				AddTable (tbl.TableIdentifier, tbl);
			}
		}


		
		/// <summary>
		/// Agrega tabla de huffman al conjunto
		/// </summary>
		/// <param name="tblId">identificadro de tabla</param>
		/// <param name="tbl">tabla</param>
		private void AddTable (int tblId, HuffmanTable tbl)
		{
			if (tbl == null)
				return;

			if (tblId <0 || tblId >= MAXHUFFMANTABLES)
				throw new DecoderException("Solo pueden haber 8 tablas de huffman en un archivo wsq");
			else
			{
				tables [tblId] = tbl;
				count++;
			}
		}

		private HuffmanTable[] tables = new HuffmanTable[MAXHUFFMANTABLES];

		private int count = 0;


	}
}