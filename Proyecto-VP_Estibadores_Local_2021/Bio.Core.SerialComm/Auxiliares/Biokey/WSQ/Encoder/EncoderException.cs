using System;

namespace Biokey.Wsq.Encoder
{
	/// <summary>
	/// Excepciones del codificador wsq.
	/// </summary>
	public class EncoderException : SystemException
	{

		/// <summary>
		/// constructor default
		/// </summary>
		public EncoderException() : base()
		{
		}

		/// <summary>
		/// Despliega un mensaje de error
		/// </summary>
		/// <param name="message">mensaje de la excepcions</param>
		public EncoderException(string message) : base(message)
		{
			
		}

		/// <summary>
		/// Mensaje y encadena una excepcion interna
		/// </summary>
		/// <param name="message">mensaje</param>
		/// <param name="exception">excepcion interna</param>
		public EncoderException(string message, Exception exception) : base(message, exception)
		{
			
		}

		/// <summary>
		/// Excepcion con un numero variable de argumentos
		/// </summary>
		/// <param name="message">mensaje de formato</param>
		/// <param name="args">parametros</param>
		public EncoderException(string message, params object[] args) : base(String.Format(message, args))
		{
			
		}

		/// <summary>
		/// Excepcion con numero variable de argumentos
		/// </summary>
		/// <param name="message">mensaje</param>
		/// <param name="exception">excepcion encadenada</param>
		/// <param name="args">parametros</param>
		public EncoderException(string message, Exception exception, params object[] args) : base(String.Format(message, args), exception)
		{
			
		}
	}
}
