using System;
using System.IO;

namespace Biokey.Wsq.Encoder
{
	/// <summary>
	/// Descripción breve de WsqWriter.
	/// </summary>
	internal sealed class WsqWriter : MarshalByRefObject, IDisposable
	{
		const ushort BIOKEYCODE = 42980;
		const int BIOKEYALGORITHM = 0xED;
		const float MAXSHORTF = 65535.0f;
            
		private BinaryWriter writer;

		internal WsqWriter(Stream stream) 
		{
			writer = new BinaryWriter(stream);
		}


		internal void PutSOI()
		{
			PutShort(SOI);
		}


		internal void PutTransformTable()
		{
			writer.Write(transformTable);
		}


		internal bool PutQuantizationTable(Quant vals)
		{
			if (vals == null)
				return false;

			PutShort(DQT);
			PutShort(389); // table size
			PutByte(2); // exponent scaling value 
			PutShort(44); // quantizer bin center parameter

			byte scale_ex, scale_ex2; /* exponent scaling parameters */
			ushort shrt_dat, shrt_dat2; /* temp variables */
			
			for (int sub = 0; sub < 64; sub++)
			{
				scale_ex = scale_ex2 = 0;
				shrt_dat = shrt_dat2 = 0;
				if (sub >= 0 && sub < 60)
				{
					if (vals.Qbss[sub] != 0.0)
					{
						float flt_tmp = vals.Qbss[sub];
						scale_ex = 0;
						if (flt_tmp >= MAXSHORTF)
							return false;
						while (flt_tmp < MAXSHORTF)
						{
							scale_ex++;
							flt_tmp *= 10.0f;
						}
						scale_ex--;
						shrt_dat = (ushort) (flt_tmp/10.0f+0.55f);


						flt_tmp = vals.Qzbs[sub];
						scale_ex2 = 0;
						if (flt_tmp >= MAXSHORTF)
							return false;

						while (flt_tmp < MAXSHORTF)
						{
							scale_ex2++;
							flt_tmp *= 10.0f;
						}
						scale_ex2--;
						shrt_dat2 = (ushort) (flt_tmp/10.0f+0.55f);

					}
				}

				PutByte(scale_ex);
				PutShort(shrt_dat);
				PutByte(scale_ex2);
				PutShort(shrt_dat2);
				
		
			}
			return true;
		}


		public void PutFrameHeader(ushort width, ushort height, float m, float r)
		{
			PutShort(SOF);
			PutShort(17); // Frame Length
			PutByte(0); // black pixel
			PutByte(255); // white pixel
			PutShort(height);
			PutShort(width);
			PutFloat(m);
			PutFloat(r);
			PutByte(BIOKEYALGORITHM);
			PutShort(BIOKEYCODE);
		}


		private void PutFloat(float m)
		{
			float flt = m;
			byte scale_ex = 0;
			ushort shrt_dat;
			if (flt == 0.0)
				shrt_dat = 0;
			else
			{
				while (flt < MAXSHORTF)
				{
					scale_ex += 1;
					flt *= 10;
				}
				scale_ex -= 1;
				shrt_dat = (ushort) (flt/10.0f+0.55f);
			}
			PutByte(scale_ex);
			PutShort(shrt_dat);
		}

		internal bool PutHuffmanTable(HuffmanTable table, byte id)
		{
			if (table == null)
				return false;
			PutShort(DHT);
			return table.WriteTo(this, id);
		}

		internal void PutHuffmanData(byte[] data, int size, byte id)
		{
			PutBlockHeader(id);
			PutBytes(data, size);
		}

		internal void PutBytes(byte[] data, int size)
		{
			writer.Write(data, 0, size);
		}

		internal void PutBlockHeader(byte id)
		{
			PutShort(SOB);
			PutShort(3);
			PutByte(id);
		}

		internal void PutEOI()
		{
			PutShort(EOI);
		}

		internal void PutShort(ushort data)
		{
			writer.Write((byte) ((data >> 8) & 0xff));
			writer.Write((byte) (data & 0xff)); 
			
		}

		internal void PutByte(byte b)
		{
			writer.Write(b);
		}

		internal void Close()
		{
			if (writer == null)
				return;
			writer.Close();
			writer = null;
		}

		
		private const ushort SOI = (ushort) 0xffa0u;
		private const ushort EOI = (ushort) 0xffa1u;
		private const ushort SOF = (ushort) 0xffa2u;
		private const ushort SOB = (ushort) 0xffa3u;
		//private const ushort DTT = (ushort) 0xffa4u;
		private const ushort DQT = (ushort) 0xffa5u;
		private const ushort DHT = (ushort) 0xffa6u;
		

		public void Dispose ()
		{
			Close();
		}

		static readonly byte[] transformTable = { 0xff, 0xa4, 0x0, 0x3a, 0x9, 0x7, 0x0, 0x9, 0x32, 0xd3, 0x25, 0xcd, 0x0, 0xa, 0xe0, 0xf3, 0x19, 0x9a, 0x1, 0xa, 0x41, 0xef, 0xf1, 0x9a, 0x1, 0xb, 0x8e, 0x27, 0x64, 0xcd, 0x0, 0xb, 0xe1, 0x79, 0xa3, 0x33, 0x0, 0x9, 0x2e, 0xff, 0x56, 0x0, 0x1, 0xa, 0xf9, 0x33, 0xd3, 0x33, 0x1, 0xb, 0xf2, 0x87, 0x21, 0x9a, 0x0, 0xa, 0x26, 0x77, 0xda, 0x33};
	}		
				
}