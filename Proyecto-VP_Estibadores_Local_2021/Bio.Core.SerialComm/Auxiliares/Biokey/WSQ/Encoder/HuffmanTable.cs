using System;

namespace Biokey.Wsq.Encoder
{
	/// <summary>
	/// Tabla de Huffman para codificar
	/// </summary>
	internal sealed class HuffmanTable
	{
		private const int MAXHUFFBITS = 16;
		private const int MAXHUFFCOEFF = 74;
		private const int MAXHUFFZRUN = 100;
		private const int MAXHUFFCOUNTSWSQ = 256;
                              		

		internal HuffmanTable()
		{
		}

		internal int Compress(byte[] obuf, short[] qdata, int start, int qsize1)
		{
			if (qdata == null)
				return -1;
			HUFFCODE[] codes = ohufftable;
			int loMaxCoeff; /* lower (negative) MaxCoeff limit */
			short pix; /* temp pixel pointer */
			uint rcnt = 0;
			State state; /* zero run count and if current pixel
                             is in a zero run or just a coefficient */

			int outbit, bytes; /* parameters used by WriteBits to */
			byte bits; /* output the "coded" image to the  */
			/* output buffer                    */

			loMaxCoeff = 1 - MAXHUFFCOEFF;
			int optr = 0;
			outbit = 7;
			bytes = 0;
			bits = 0;
			state = State.COEFF_CODE;
			for (int cnt = start; cnt < start + qsize1; cnt++)
			{
				pix = qdata[cnt];

				switch (state)
				{
					case State.COEFF_CODE:
						if (pix == 0)
						{
							state = State.RUN_CODE;
							rcnt = 1;
							break;
						}
						if (pix > MAXHUFFCOEFF)
						{
							if (pix > 255)
							{
								/* 16bit pos esc */
								WriteBits(obuf, ref optr, (ushort) codes[103].Code, codes[103].Size, ref outbit, ref bits, ref bytes);
								WriteBits(obuf, ref optr, (ushort) pix, MAXHUFFBITS, ref outbit, ref bits, ref bytes);
							}
							else
							{
								/* 8bit pos esc */
								WriteBits(obuf, ref optr, (ushort) codes[101].Code, codes[101].Size, ref outbit, ref bits, ref bytes);
								WriteBits(obuf, ref optr, (ushort) pix, 8, ref outbit, ref bits, ref bytes);
							}
						}
						else if (pix < loMaxCoeff)
						{
							if (pix < -255)
							{
								/* 16bit neg esc */
								WriteBits(obuf, ref optr, (ushort) codes[104].Code, codes[104].Size, ref outbit, ref bits, ref bytes);
								WriteBits(obuf, ref optr, (ushort) -pix, MAXHUFFBITS, ref outbit, ref bits, ref bytes);
							}
							else
							{
								/* 8bit neg esc */
								WriteBits(obuf, ref optr, (ushort) codes[102].Code, codes[102].Size, ref outbit, ref bits, ref bytes);
								WriteBits(obuf, ref optr, (ushort) -pix, 8, ref outbit, ref bits, ref bytes);
							}
						}
						else
						{
							/* within table */
							WriteBits(obuf, ref optr, (ushort) codes[pix + 180].Code, codes[pix + 180].Size, ref outbit, ref bits, ref bytes);
						}
						break;

					case State.RUN_CODE:
						if (pix == 0 && rcnt < 0xFFFF)
						{
							++rcnt;
							break;
						}
						if (rcnt <= (ulong) MAXHUFFZRUN)
						{
							/* log zero run length */
							WriteBits(obuf, ref optr, (ushort) codes[rcnt].Code, codes[rcnt].Size, ref outbit, ref bits, ref bytes);
						}
						else if (rcnt <= 0xFF)
						{
							/* 8bit zrun esc */
							WriteBits(obuf, ref optr, (ushort) codes[105].Code, codes[105].Size, ref outbit, ref bits, ref bytes);
							WriteBits(obuf, ref optr, (ushort) rcnt, 8, ref outbit, ref bits, ref bytes);
						}
						else if (rcnt <= 0xFFFF)
						{
							/* 16bit zrun esc */
							WriteBits(obuf, ref optr, (ushort) codes[106].Code, codes[106].Size, ref outbit, ref bits, ref bytes);
							WriteBits(obuf, ref optr, (ushort) rcnt, MAXHUFFBITS, ref outbit, ref bits, ref bytes);
						}
						else
						{
							return 0;
						}

						if (pix != 0)
						{
							if (pix > MAXHUFFCOEFF)
							{
								if (pix > 255)
								{
									/* 16bit pos esc */
									WriteBits(obuf, ref optr, (ushort) codes[103].Code, codes[103].Size, ref outbit, ref bits, ref bytes);
									WriteBits(obuf, ref optr, (ushort) pix, MAXHUFFBITS, ref outbit, ref bits, ref bytes);
								}
								else
								{
									/* 8bit pos esc */
									WriteBits(obuf, ref optr, (ushort) codes[101].Code, codes[101].Size, ref outbit, ref bits, ref bytes);
									WriteBits(obuf, ref optr, (ushort) pix, 8, ref outbit, ref bits, ref bytes);
								}
							}
							else if (pix < loMaxCoeff)
							{
								if (pix < -255)
								{
									/* 16bit neg esc */
									WriteBits(obuf, ref optr, (ushort) codes[104].Code, codes[104].Size, ref outbit, ref bits, ref bytes);
									WriteBits(obuf, ref optr, (ushort) -pix, MAXHUFFBITS, ref outbit, ref bits, ref bytes);
								}
								else
								{
									/* 8bit neg esc */
									WriteBits(obuf, ref optr, (ushort) codes[102].Code, codes[102].Size, ref outbit, ref bits, ref bytes);
									WriteBits(obuf, ref optr, (ushort) -pix, 8, ref outbit, ref bits, ref bytes);
								}
							}
							else
							{
								/* within table */
								WriteBits(obuf, ref optr, (ushort) codes[pix + 180].Code, codes[pix + 180].Size, ref outbit, ref bits, ref bytes);
							}
							state = State.COEFF_CODE;
						}
						else
						{
							rcnt = 1;
							state = State.RUN_CODE;
						}
						break;
				}
			}
			if (state == State.RUN_CODE)
			{
				if (rcnt <= (ulong) MAXHUFFZRUN)
				{
					WriteBits(obuf, ref optr, (ushort) codes[rcnt].Code, codes[rcnt].Size, ref outbit, ref bits, ref bytes);
				}
				else if (rcnt <= 0xFF)
				{
					WriteBits(obuf, ref optr, (ushort) codes[105].Code, codes[105].Size, ref outbit, ref bits, ref bytes);
					WriteBits(obuf, ref optr, (ushort) rcnt, 8, ref outbit, ref bits, ref bytes);
				}
				else if (rcnt <= 0xFFFF)
				{
					WriteBits(obuf, ref optr, (ushort) codes[106].Code, codes[106].Size, ref outbit, ref bits, ref bytes);
					WriteBits(obuf, ref optr, (ushort) rcnt, MAXHUFFBITS, ref outbit, ref bits, ref bytes);
				}
				else
				{
					return 0;
				}
			}

			FlushBits(obuf, ref optr, ref outbit, ref bits, ref bytes);
			return bytes;
		}


		private struct HUFFCODE
		{
			internal short Size;
			internal uint Code;
		} ;

		
		
		

		private HUFFCODE[] ohufftable;
		private byte[] ohuffbits;
		private byte[] ohuffvalues;

		/*************************************************************/
		/* Generate a Huffman code table for a quantized data block. */
		/*************************************************************/

		internal bool Generate(short[] sip, int start, params int[] block_sizes)
		{
			int[] huffcounts = CountBlock(sip, start, block_sizes[0]);
			if (huffcounts == null)
				return false;

			for (int i = 1; i < block_sizes.Length; i++)
			{
				int[] huffcounts2 = CountBlock(sip, start + block_sizes[i - 1], block_sizes[i]);
				if (huffcounts2 == null)
					return false;

				for (int j = 0; j < MAXHUFFCOUNTSWSQ; j++)
					huffcounts[j] += huffcounts2[j];

			}

			int[] codesize = FindHuffSizes(huffcounts, MAXHUFFCOUNTSWSQ);
			if (codesize == null)
				return false;

			bool adjust = false;
			byte[] huffbits = FindNumHuffSizes(ref adjust, codesize, MAXHUFFCOUNTSWSQ);
			if (huffbits == null)
				return false;

			if (adjust)
			{
				if (!SortHuffbits(huffbits))
					return false;
			}

			byte[] huffvalues = SortCodeSizes(codesize, MAXHUFFCOUNTSWSQ);

			if (huffvalues == null)
				return false;

			int last_size = 0; /* last huffvalue */
			HUFFCODE[] hufftable1 = BuildHuffSizes(ref last_size, huffbits, MAXHUFFCOUNTSWSQ);
			if (hufftable1 == null)
				return false;

			BuildHuffCodes(hufftable1);
			if (!CheckHuffCodesWsq(hufftable1, last_size))
				return false;

			HUFFCODE[] hufftable2 = BuildHuffCodeTable(hufftable1, last_size, huffvalues);
			if (hufftable2 == null)
				return false;

			ohuffbits = huffbits;
			ohuffvalues = huffvalues;
			ohufftable = hufftable2;
			return true;
		}

		private HUFFCODE[] BuildHuffCodeTable(HUFFCODE[] in_huffcode_table, int last_size, byte[] values)
		{
			HUFFCODE[] new_huffcode_table; /*pointer to a huffman code structure*/

			new_huffcode_table = new HUFFCODE[MAXHUFFCOUNTSWSQ + 1];
			for (int size = 0; size < last_size; size++)
			{
				new_huffcode_table[values[size]].Code = in_huffcode_table[size].Code;
				new_huffcode_table[values[size]].Size = in_huffcode_table[size].Size;
			}
			return new_huffcode_table;
		}


		private bool CheckHuffCodesWsq(HUFFCODE[] hufftable, int last_size)
		{
			for (int i = 0; i < last_size; i++)
			{
				bool all_ones = true;
				for (int k = 0; all_ones && k < hufftable[i].Size; k++)
					all_ones = (all_ones && (((hufftable[i].Code >> k) & 0x0001) == 0x0001));
				if (all_ones)
					return false;
			}
			return true;

		}

		private void BuildHuffCodes(HUFFCODE[] huffcode_table)
		{
			int pointer = 0; /*pointer to code word information*/
			if (pointer >= huffcode_table.Length)
				return;
			
			ushort temp_code = 0; /*used to construct code word*/
			short temp_size = huffcode_table[0].Size;

			do
			{
				do
				{
						huffcode_table[pointer].Code = temp_code;
					temp_code++;
					pointer++;
					
				} while (huffcode_table[pointer].Size == temp_size);

				if (huffcode_table[pointer].Size == 0)
					return;

				
				do
				{
					temp_code <<= 1;
					temp_size++;
				} while (huffcode_table[pointer].Size != temp_size);
			} while (huffcode_table[pointer].Size == temp_size);
		}


		private HUFFCODE[] BuildHuffSizes(ref int temp_size, byte[] huffbits, int max_huffcounts)
		{
			/**************************************************************************/
			/*This routine defines the huffman code sizes for each difference category*/
			/**************************************************************************/
			//int build_huffsizes(HUFFCODE **ohuffcode_table, int *temp_size,byte *huffbits, const int max_huffcounts)

			HUFFCODE[] huffcode_table; /*table of huffman codes and sizes*/
			short code_size; /*code sizes*/
			int number_of_codes = 1; /*the number codes for a given code size*/

			huffcode_table = new HUFFCODE[max_huffcounts + 1];

			temp_size = 0;

			for (code_size = 1; code_size <= MAXHUFFBITS; code_size++)
			{
				while (number_of_codes <= huffbits[code_size - 1])
				{
					huffcode_table[temp_size].Size = code_size;
					temp_size++;
					number_of_codes++;
				}
				number_of_codes = 1;
			}
			huffcode_table[temp_size].Size = 0;
			return huffcode_table;
		}


		private byte[] SortCodeSizes(int[] codesize, int max_huffcounts)
		{
			byte[] values = new byte[max_huffcounts + 1];
			int pvalues = 0;
			int max = 32;
			for (int i = 1; i <= max; i++)
			{
				for (int i3 = 0; i3 < max_huffcounts; i3++)
				{
					if (codesize[i3] == i)
						values[pvalues++] = (byte) i3;
				}
			}
			return values;
		}

		private bool SortHuffbits(byte[] bits)
		{
			short[] tbits = new short[32];

			int i;
			for (i = 0; i < 32; i++)
				tbits[i] = bits[i];

			for (i = 31; i > 15; i--)
			{
				while (tbits[i] > 0)
				{
					int j = i - 2;
					while (tbits[j] == 0)
						j--;
					tbits[i] -= 2;
					++tbits[i - 1];
					tbits[j + 1] += 2;
					--tbits[j];
				}
				tbits[i] = 0;
			}

			while (tbits[i] == 0)
				i--;

			--tbits[i];

			for (i = 0; i < 32; i++)
				bits[i] = (byte) tbits[i];

			for (i = MAXHUFFBITS; i < 32; i++)
				if (bits[i] > 0)
					return false;

			return true;
		}


		private byte[] FindNumHuffSizes(ref bool adjust, int[] codesize, int max_huffcounts)
		{
			adjust = false;

			/* Allocate 2X desired number of bits due to possible codesize. */
			byte[] bits = new byte[32];

			for (int i = 0; i < max_huffcounts; i++)
			{
				if (codesize[i] != 0)
					bits[(codesize[i] - 1)]++;
				if (codesize[i] > MAXHUFFBITS)
					adjust = true;
			}
			return bits;
		}

		private int[] FindHuffSizes(int[] freq, int max_huffcounts)
		{
			int[] codesize; /*codesizes for each category*/
			int[] others; /*pointer used to generate codesizes*/
			int value1; /*smallest and next smallest frequency*/
			int value2; /*of difference occurrence in the largest			  difference category*/

			codesize = new int[max_huffcounts + 1];
			others = new int[max_huffcounts + 1];

			for (int i = 0; i <= max_huffcounts; i++)
				others[i] = -1;

			while (true)
			{
				FindLeastFreq(out value1, out value2, freq, max_huffcounts);

				if (value2 == -1)
					break;

				freq[value1] += freq[value2];
				freq[value2] = 0;

				codesize[value1]++;

				while (others[value1] != -1)
				{
					value1 = others[value1];
					codesize[value1]++;
				}
				others[value1] = value2;
				codesize[value2]++;

				while (others[value2] != -1)
				{
					value2 = others[value2];
					codesize[value2]++;
				}
			}
			return codesize;
		}

		private void FindLeastFreq(out int value1, out int value2, int[] freq, int max_huffcounts)
		{
			int code_temp; /*store code*/
			int value_temp; /*store size*/
			int code2 = Int32.MaxValue; /*next smallest frequency in largest diff category*/
			int code1 = Int32.MaxValue; /*smallest frequency in largest difference category*/
			int set = 1; /*flag first two non-zero frequency values*/

			value1 = -1;
			value2 = -1;

			for (int i = 0; i <= max_huffcounts; i++)
			{
				if (freq[i] == 0)
					continue;
				if (set == 1)
				{
					code1 = freq[i];
					value1 = i;
					set++;
					continue;
				}
				if (set == 2)
				{
					code2 = freq[i];
					value2 = i;
					set++;
				}
				code_temp = freq[i];
				value_temp = i;
				if (code1 < code_temp && code2 < code_temp)
					continue;
				if ((code_temp < code1) || (code_temp == code1 && value_temp > value1))
				{
					code2 = code1;
					value2 = value1;
					code1 = code_temp;
					value1 = value_temp;
					continue;
				}
				if ((code_temp < code2) || (code_temp == code2 && value_temp > value2))
				{
					code2 = code_temp;
					value2 = value_temp;
				}
			}
		}

		private enum State
		{
			COEFF_CODE = 0,
			RUN_CODE = 1
		} ;

		/*****************************************************************/
		/* This routine counts the number of occurences of each category */
		/* in the huffman coding tables.                                 */
		/*****************************************************************/

		private int[] CountBlock(short[] sip, int start, long size)
		{
			int maxCoeff = MAXHUFFCOEFF;
			int maxZRun = MAXHUFFZRUN;
			int max_huffcounts = MAXHUFFCOUNTSWSQ;
			int[] counts; /* count for each huffman category */
			int loMaxCoeff; /* lower (negative) MaxCoeff limit */
			int rcnt = 0; /* zero run count and if current pixel
                             is in a zero run or just a coefficient */


			if (maxCoeff < 0 || maxCoeff > 0xffff)
				return null;

			if (maxZRun < 0 || maxZRun > 0xffff)
				return null;

			/* Ininitalize vector of counts to 0. */
			counts = new int[max_huffcounts + 1];

			/* Set last count to 1. */
			counts[max_huffcounts] = 1;

			loMaxCoeff = 1 - maxCoeff;

			State state = State.COEFF_CODE;
			for (int i = start; i < start + size; i++)
			{
				short pix = sip[i];
				switch (state)
				{
					case State.COEFF_CODE: /* for runs of zeros */
						if (pix == 0)
						{
							state = State.RUN_CODE;
							rcnt = 1;
							break;
						}
						if (pix > maxCoeff)
						{
							if (pix > 255)
								counts[103]++; /* 16bit pos esc */
							else
								counts[101]++; /* 8bit pos esc */
						}
						else if (pix < loMaxCoeff)
						{
							if (pix < -255)
								counts[104]++; /* 16bit neg esc */
							else
								counts[102]++; /* 8bit neg esc */
						}
						else
							counts[pix + 180]++; /* within table */
						break;

					case State.RUN_CODE: /* get length of zero run */
						if (pix == 0 && rcnt < 0xFFFF)
						{
							++rcnt;
							break;
						}
						else
						{
							/* limit rcnt to avoid EOF problem in bitio.c */
							if (rcnt <= maxZRun)
								counts[rcnt]++; 
							else if (rcnt <= 0xFF)
								counts[105]++;
							else if (rcnt <= 0xFFFF)
								counts[106]++; /* 16bit zrun esc */
							else
							{
								return null;
							}

							if (pix != 0)
							{
								if (pix > maxCoeff)
								{ 
									if (pix > 255)
										counts[103]++; /* 16bit pos esc */
									else
										counts[101]++; /* 8bit pos esc */
								}
								else if (pix < loMaxCoeff)
								{
									if (pix < -255)
										counts[104]++; /* 16bit neg esc */
									else
										counts[102]++; /* 8bit neg esc */
								}
								else
									counts[pix + 180]++; /* within table */
								state = State.COEFF_CODE;
							}
							else
							{
								rcnt = 1;
								state = State.RUN_CODE;
							}
						}
						break;
				}
			}
			if (state == State.RUN_CODE)
			{ 
				if (rcnt <= maxZRun)
					counts[rcnt]++;
				else if (rcnt <= 0xFF)
					counts[105]++;
				else if (rcnt <= 0xFFFF)
					counts[106]++; /* 16bit zrun esc */
				else
					return null;

			}
			return counts;
		}

		private void WriteBits(byte[] outbuf, ref int pos, ushort code, short size, ref int outbit, ref byte bits, ref int bytes)
		{
			for (--size; size >= 0; size--)
			{
				bits <<= 1;
				bits |= ((byte) ((code >> size) & 0x0001));

				if (--outbit < 0)
				{
					outbuf[pos] = bits;
					pos++;
					if (bits == 0xFF)
					{
						outbuf[pos] = 0;
						pos++;
						bytes++;
					}
					bytes++;
					outbit = 7;
					bits = 0;
				}
			}
		}

		/*********************************************/

		private void FlushBits(byte[] outbuf, ref int pos, ref int outbit, ref byte bits, ref int bytes)
		{
			if (outbit != 7)
			{
				for (int cnt = outbit; cnt >= 0; cnt--)
				{
					bits <<= 1;
					bits |= 0x01;
				}

				outbuf[pos] = bits;
				pos++;
				if (bits == 0xFF)
				{
					bits = 0;
					outbuf[pos] = 0;
					pos++;
					bytes++;
				}
				bytes++;
				outbit = 7;
				bits = 0;
			}
		}

		public bool WriteTo(WsqWriter data, byte tableId)
		{
			if (data == null)
				return false;

			//int i;
			ushort table_len, values_offset;	
			table_len = values_offset = 3 + MAXHUFFBITS;
			for (int i = 0; i < MAXHUFFBITS; i++)
				table_len += ohuffbits[i]; /* values size */


			/* Table Len */
			data.PutShort(table_len);

			/* Table ID */
			data.PutByte(tableId);
			/* Huffbits (MAX_HUFFBITS) */
			data.PutBytes (ohuffbits, MAXHUFFBITS);
			data.PutBytes (ohuffvalues, table_len - values_offset);
			return true;
		}



	}

}