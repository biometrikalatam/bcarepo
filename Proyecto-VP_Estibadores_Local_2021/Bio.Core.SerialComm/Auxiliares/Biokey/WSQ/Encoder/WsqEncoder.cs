using System.IO;

namespace Biokey.Wsq.Encoder
{
	/// <summary>
	/// Comprime en formato wsq.
	/// Implementación del formato wsq en C#
	/// </summary>
	public class WsqEncoder : IWsqEncoder
	{

		/// <summary>
		/// Constructor, configura la proporción de compresión en 0.75,
		/// es decir, 15 a 1.
		/// </summary>
		public WsqEncoder()
		{
		}

		/// <summary>
		/// Constructor, en este caso recibe la proporcion de compresión
		/// </summary>
		/// <param name="bitRate">Tasa de compresión, el valor default es 0.75</param>
		public WsqEncoder(float bitRate)
		{
			this.bitRate = bitRate;	
		}


		/// <summary>
		/// Factor de compresión, el valor default es 0.75, que es aproximadamente
		/// 15 a 1.
		/// </summary>
		/// <remarks>Una regla util, es si se quiere comprimir de X a 1, entonces
		/// bitRate = 8/X</remarks>
		public float BitRate
		{
			get
			{
				return bitRate;
			}
			set
			{
				bitRate = value;
			}
		}

		/// <summary>
		/// Codifica un stream en formato WSQ.
		/// </summary>
		/// <param name="inStream">stream que contiene la imagen a codificar</param>
		/// <param name="outStream">stream donde se escribirá la imagen codificada</param>
		/// <param name="width">ancho de la imagen</param>
		/// <param name="height">alto de la imagen</param>
		/// <returns>true, si pudo codificar, false si no pudo</returns>
		public bool EncodeStream(Stream inStream, Stream outStream, short width, short height)
		{
			byte[] raw = new byte[inStream.Length];
			inStream.Read(raw, 0, raw.Length);
			NormalizedData fdata = new NormalizedData(raw, width, height);

			WaveletTreeNode[] wtrees;
			QuantumTreeNode[] qtrees;
			TreeBuilder.BuildTrees(width, height, out wtrees, out qtrees);

			fdata.WsqDecompose(wtrees);

			Quant quantVals = new Quant(BitRate);

			int qsize;
			short[] qdata = quantVals.Quantize(out qsize, qtrees, wtrees, fdata.Data, width, height);
			if (qdata == null)
				return false;

			if (qsize != quantVals.Qsize1 + quantVals.Qsize2 + quantVals.Qsize3)
				return false;
			
			WsqWriter wsqData = new WsqWriter(outStream);


			wsqData.PutSOI();
			wsqData.PutTransformTable();
			wsqData.PutQuantizationTable(quantVals);
			wsqData.PutFrameHeader((ushort) width, (ushort) height, fdata.M, fdata.R);

			
			byte[] hufbuf = new byte[width*height];
			HuffmanTable huffTable1 = new HuffmanTable();


			/******************/
			/* ENCODE Block 1 */
			/******************/
			if (!huffTable1.Generate(qdata, 0, quantVals.Qsize1))
				return false;

			wsqData.PutHuffmanTable(huffTable1, 0);

			int hsize1 = huffTable1.Compress(hufbuf, qdata, 0, quantVals.Qsize1);
			int hsize = hsize1;
			wsqData.PutHuffmanData(hufbuf, hsize1, 0);


			/******************/
			/* ENCODE Block 2 */
			/******************/
			HuffmanTable huffTable2 = new HuffmanTable();

			if (!huffTable2.Generate(qdata, quantVals.Qsize1, quantVals.Qsize2, quantVals.Qsize3))
				return false;

			wsqData.PutHuffmanTable(huffTable2, 1);


			int hsize2 = huffTable2.Compress(hufbuf, qdata, quantVals.Qsize1, quantVals.Qsize2); 
			hsize += hsize2;
			wsqData.PutHuffmanData(hufbuf, hsize2, 1);

			/******************/
			/* ENCODE Block 3 */
			/******************/
			int hsize3 = huffTable2.Compress(hufbuf, qdata, quantVals.Qsize1 + quantVals.Qsize2, quantVals.Qsize3);
			hsize += hsize3;
			wsqData.PutHuffmanData(hufbuf, hsize3, 1);

			wsqData.PutEOI();
			return true;
		}

		private float bitRate = 0.75f;
		

		/// <summary>
		/// Codifica un segmento de memoria a formato wsq
		/// </summary>
		/// <param name="raw">imagen en formato raw</param>
		/// <param name="width">ancho de la imagen</param>
		/// <param name="height">alto de la imagen</param>
		/// <param name="wsq">wsq de salida</param>
		/// <returns>true, si pudo codificar en formato wsq, false si no</returns>
		public bool EncodeMemory(byte[] raw, short width, short height, out byte[] wsq)
		{
			wsq = null;
			using (MemoryStream msIn = new MemoryStream(raw))
			using (MemoryStream msOut = new MemoryStream())
			{
				if (!EncodeStream(msIn, msOut, width, height))
					return false;
				wsq = msOut.ToArray();
			}
			return true;
		}


	}
}