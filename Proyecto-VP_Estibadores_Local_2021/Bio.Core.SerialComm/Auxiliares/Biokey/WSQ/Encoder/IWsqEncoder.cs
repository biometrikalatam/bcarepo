using System.IO;

namespace Biokey.Wsq.Encoder
{
	/// <summary>
	/// Esta es la interfaz de un codificador WSQ
	/// </summary>
	public interface IWsqEncoder
	{
		/// <summary>
		/// Proporcion de la compresi�n, el valor recomendado por FBI esta entr� .61 y .75
		/// Por defaul, esta propiedad tiene el valor 0.75
		/// <remarks>Una regla util, es si se quiere comprimir de X a 1, entonces
		/// bitRate = 8/X</remarks>
		/// </summary>
		float BitRate { get; set; }

		/// <summary>
		/// Codifica un stream en formato WSQ.
		/// </summary>
		/// <param name="inStream">stream que contiene la imagen a codificar</param>
		/// <param name="outStream">stream donde se escribir� la imagen codificada</param>
		/// <param name="width">ancho de la imagen</param>
		/// <param name="height">alto de la imagen</param>
		/// <returns>true, si pudo codificar, false si no pudo</returns>
		bool EncodeStream(Stream inStream, Stream outStream, short width, short height);

		/// <summary>
		/// Codifica un segmento de memoria a formato wsq
		/// </summary>
		/// <param name="raw">imagen en formato raw</param>
		/// <param name="width">ancho de la imagen</param>
		/// <param name="height">alto de la imagen</param>
		/// <param name="wsq">wsq de salida</param>
		/// <returns>true, si pudo codificar en formato wsq, false si no</returns>
		bool EncodeMemory(byte[] raw, short width, short height, out byte[] wsq);
	}
}
