namespace Biokey.Wsq
{
	/// <summary>
	/// Descripción breve de QuantumTreeNode.
	/// </summary>
	public struct QuantumTreeNode
	{
		public int X;
		public int Y;
		public int LenX;
		public int LenY;
	}
}
