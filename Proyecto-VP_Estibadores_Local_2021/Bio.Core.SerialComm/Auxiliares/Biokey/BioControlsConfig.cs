using System.Collections;
using System.Configuration;
using System.IO;
using System.Xml.Serialization;
//using Biokey.DigitalPersona;
using Biokey.SerialPort;
using Biokey.License;
using Biokey.Log;

namespace Biokey
{
	/// <summary>
	/// Configuración de biocontrols
	/// </summary>
	public sealed class BioControlsConfig
	{
		public BioControlsConfig()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static BioControlsConfig Instance
		{
			get
			{
				return Nested.Instance;
			}
		}

		
			

		public override string ToString()
		{
			XmlSerializer ser = new XmlSerializer(typeof(BioControlsConfig));
			using (StringWriter wrt = new StringWriter())
			{
				ser.Serialize (wrt, this);
				wrt.Close ();
				return wrt.ToString ();
			}
		}


		public LicenseConfig LicenseConfig;

		public LogConfig LogConfig;

		public int UmbralReconocimientoVL = 1600;
		public int UmbralReconocimientoVR = 1600;
		
		public int TimeoutWSBIS = 60000;

		public bool EnrollInCheckPositive = false;
		public bool AddBIREnroll = false;
		
		public bool AutomaticWithGoodQuality = false;
		
		public string TipoCheckLicencia = "KEY";	
		
		
		public bool RunAppInCheckPositive = false;
		public string AppInCheckPositive = null;
		public string ArgumentsForAppInCheckPositive = null;
		public int WaitForExitAppInCheckPositive = 10000;
		
		public bool ShowBigResult = false;
		public string Estacion = null;
		public string Glosa = null;
		
		//[XmlArrayItem("Device", Type=typeof(DeviceConfig))]
		//[XmlArrayItem("DigitalPersona", Type=typeof(DigitalPersonaConfig))]
		[XmlArrayItem("SerialPort", Type=typeof(SerialPortConfig))]
		public ArrayList Devices;


		class Nested
		{
			private static BioControlsConfig config;
			// Explicit static constructor to tell C# compiler
			// not to mark type as beforefieldinit
			static Nested()
			{
			}

			internal static BioControlsConfig Instance
			{
				get
				{
					if (config == null)
						config = (BioControlsConfig) ConfigurationSettings.GetConfig ("BioControlsConfig");
					return config;
				}
			}
		}


	}
}
