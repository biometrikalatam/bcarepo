using System;

namespace Biokey
{
	/// <summary>
	/// Summary description for IQuality.
	/// </summary>
	public interface IQuality
	{
		int CompareWith(IQuality quality);
	}
}
