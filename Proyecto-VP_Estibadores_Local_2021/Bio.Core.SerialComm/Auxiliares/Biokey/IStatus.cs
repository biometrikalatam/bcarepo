namespace Biokey
{
	/// <summary>
	/// Summary description for IStatus.
	/// </summary>
	public interface IStatus
	{
		int Code { get; }
		string Message { get; }
	}
}
