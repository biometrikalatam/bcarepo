using System;

namespace Biokey
{
	/// <summary>
	/// Summary description for IProvider.
	/// </summary>
	public interface IProvider
	{
		void Configure(IConfig config);
	}
}
