using System;
using System.IO;

namespace Biokey.SerialPort
{
	/// <summary>
	/// Summary description for SerialSample.
	/// </summary>
	public class SerialSample : ISample
	{
		public SerialSample(IDevice device, byte[] data)
		{
			source = device.Name;
			isValid = data != null;
			if (isValid)
			{
				this.data = new byte[data.Length];
				Array.Copy(data, 0, this.data, 0, data.Length);
			}

		}

		/// <summary>
		/// Escribe a un stream la muestra
		/// </summary>
		/// <param name="stream">stream de salida</param>
		public void Serialize (StreamWriter stream)
		{
			stream.Write (Convert.ToBase64String(data));
		}

		/// <summary>
		/// Lee desde un stream la muestra
		/// </summary>
		/// <param name="stream">stream de entrada</param>
		public void Deserialize (StreamReader stream)
		{
			data = Convert.FromBase64String (stream.ReadToEnd ());
		}

		public DateTime TimeStamp
		{
			get { return tstamp; }
		}
		DateTime tstamp = DateTime.Now;

		public string Source
		{
			get { return source; }
		}

		public int Length
		{
			get
			{
				return data.Length;
			}
		}
		string source;
		private byte[] data;
		private bool isValid;


		public byte[] Data
		{
			get
			{
				return data;
			}
		}
		public string AutentiCode
		{
			get { return String.Empty; }
		}

		public bool IsValid
		{
			get { return isValid; }
		}

	}
}
