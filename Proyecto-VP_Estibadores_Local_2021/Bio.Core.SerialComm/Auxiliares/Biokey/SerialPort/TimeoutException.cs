// ==++==
// 
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  TimeoutException
**
** Purpose: Exception for a timeout.
**
** Date:  September 2002
**
===========================================================*/

using System;
using System.Runtime.Serialization;

namespace System
{
	// Exception supporting 
	public class TimeoutException : Exception
	{
		public TimeoutException() : base()
		{
		}
		
		public TimeoutException(string message) : base(message)
		{	
		}
	
		protected TimeoutException(SerializationInfo info, StreamingContext context) : base (info, context) 
		{
		}

	}
}
