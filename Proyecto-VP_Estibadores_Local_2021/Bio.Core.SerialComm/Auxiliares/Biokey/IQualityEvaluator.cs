using System;

namespace Biokey
{
	/// <summary>
	/// Summary description for IQualityEvaluator.
	/// </summary>
	public interface IQualityEvaluator
	{
		/// <summary>
		/// Obtiene la calidad de una muestra
		/// </summary>
		/// <param name="sample">la muestra</param>
		/// <returns>La calidad</returns>
		IQuality GetQuality(ISample sample);

		/// <summary>
		/// Chequea la calidad contra un umbral
		/// </summary>
		/// <param name="sample">muestra</param>
		/// <param name="threshold">umbral</param>
		/// <returns>true si acepta la calidad</returns>
		bool CheckQuality(ISample sample, IQuality threshold);
	}
}
