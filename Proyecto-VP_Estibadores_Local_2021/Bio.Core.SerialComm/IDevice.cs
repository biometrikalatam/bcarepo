using BioCore.SerialComm.Feedback;

namespace BioCore.SerialComm
{
	/// <summary>
	/// Summary description for IDevice.
	/// </summary>
	public interface IDevice : IExecutor
	{
		void Configure(IConfig config);
		void Open();
		void Close();
		IStatus Status();
		IFeedbackProvider FeedbackProvider { get; set; }
		SampleAvailableEvent OnSampleAvailableEventAvailable { get; set; }
		void DoEvents ();
		string Id { get; }
		void SetActive();
	}
}
