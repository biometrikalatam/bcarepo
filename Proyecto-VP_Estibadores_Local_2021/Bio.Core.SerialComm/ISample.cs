using System;
using System.IO;

namespace BioCore.SerialComm
{
	/// <summary>
	/// Una muestra obtenido desde alguna parte
	/// </summary>
	public interface ISample
	{
		/// <summary>
		/// Escribe a un stream la muestra
		/// </summary>
		/// <param name="stream">stream de salida</param>
		void Serialize(StreamWriter stream);

		/// <summary>
		/// Lee desde un stream la muestra
		/// </summary>
		/// <param name="stream">stream de entrada</param>
		void Deserialize(StreamReader stream);

		DateTime TimeStamp { get; }
		string   Source   { get; }
		string   AutentiCode { get; }
		bool     IsValid { get; }

	}
}
