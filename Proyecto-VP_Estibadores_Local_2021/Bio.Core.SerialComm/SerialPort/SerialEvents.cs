// ==++==
// 
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Type: SerialEvents
**
** Purpose: SerialEvents enum type defined.  
**
** Date:  August 2002
**
**
===========================================================*/


namespace System.IO.Ports
{
	
	
	// Note: old MSComm codes are no longer valid, and this enumeration should be used whenever possible.
	// The Send event is also gone, since its utility was dubious at best, and we do not collect material to be sent
	// in an explicit output buffer; i.e. we send all data availabe to the serial driver on every Write-flavored call.
	[Flags] public enum SerialEvents 
	{
		None = 0x0,
		ReceivedChars = 0x00000001,
		CtsChanged = 0x00000002,
		DsrChanged = 0x00000004,
		CDChanged = 0x0000008,
		Ring = 0x00000010,
		Break = 0x00000020, 
		TxFull = 0x00000040,
		RxOver = 0x00000080,
		Overrun = 0x00000100, 
		RxParity = 0x00000200,
		Frame = 0x00000400, 
		EofReceived = 0x00000800,
		All = 0x00000fff
	}
		

}