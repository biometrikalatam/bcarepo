// ==++==
// 
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  System.IO.Ports.NativeMethods
**
** Purpose: Wrapper for all SerialPort/SerialStream-related PInvoke methods
**		  : and hub for all constant definitions which are security-neutral to the OS.
**		  : This also contains all definitions unique to the SerialPort/SerialStream classes
**		  : such as RTS_CONTROL_HANDSHAKE, TWOSTOPBITS, etc.
** Date:  August, 2002
**
===========================================================*/


using System;
using System.Configuration.Assemblies;
using System.Runtime.Remoting;
using System.IO;
using System.Text;
using System.ComponentModel;
using System.Resources;
using System.Runtime;
using System.Security;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.Threading;

namespace System.IO.Ports
{
	
	internal sealed class NativeMethods
	{

		internal const String KERNEL32 = "kernel32.dll";

		internal const int GENERIC_READ  = unchecked((int) 0x80000000);
		internal const int GENERIC_WRITE  = 0x40000000;
		
		internal static readonly IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);
		internal static readonly IntPtr NULL = IntPtr.Zero;
		
		// All I/O error codes (esp. all System Error Codes) come through NativeMethods 
		internal const int ERROR_BROKEN_PIPE = 109;
		internal const int ERROR_NO_DATA = 232;
		internal const int ERROR_HANDLE_EOF = 38;
		internal const int ERROR_IO_PENDING = 997;
		internal const int ERROR_FILE_NOT_FOUND = 0x2;
		internal const int ERROR_PATH_NOT_FOUND = 0x3;
		internal const int ERROR_ACCESS_DENIED  = 0x5;
		internal const int ERROR_INVALID_HANDLE = 0x6;
		internal const int ERROR_SHARING_VIOLATION = 0x20;
		internal const int ERROR_FILE_EXISTS = 0x50;
		internal const int ERROR_INVALID_PARAMETER = 0x57;
		internal const int ERROR_FILENAME_EXCED_RANGE = 0xCE;  // filename too long.

		// The following are unique to the SerialPort/SerialStream classes
		internal const byte ONESTOPBIT = 0;
		internal const byte ONE5STOPBITS = 1;
		internal const byte TWOSTOPBITS = 2;

		internal const int DTR_CONTROL_DISABLE = 0x00;
		internal const int DTR_CONTROL_ENABLE = 0x01;
		internal const int DTR_CONTROL_HANDSHAKE = 0x02;

		internal const int RTS_CONTROL_DISABLE = 0x00;
		internal const int RTS_CONTROL_ENABLE = 0x01;
		internal const int RTS_CONTROL_HANDSHAKE = 0x02;
		internal const int RTS_CONTROL_TOGGLE = 0x03;
		
		internal const int  MS_CTS_ON = 0x10;
		internal const int  MS_DSR_ON = 0x20;
		internal const int  MS_RING_ON = 0x40;
		internal const int  MS_RLSD_ON  = 0x80;

		internal const byte EOFCHAR = (byte) 26;

		// Since C# does not provide access to bitfields and the native DCB structure contains
		// a very necessary one, these are the positional offsets (from the right) of areas 
		// of the 32-bit integer used in SerialStream's SetDcbFlag() and GetDcbFlag() methods.
		internal const int FBINARY = 0;
		internal const int FPARITY = 1; 
		internal const int FOUTXCTSFLOW = 2; 
		internal const int FOUTXDSRFLOW = 3; 
		internal const int FDTRCONTROL = 4; 
		internal const int FDSRSENSITIVITY = 6; 
		internal const int FTXCONTINUEONXOFF = 7; 
		internal const int FOUTX = 8; 
		internal const int FINX = 9; 
		internal const int FERRORCHAR = 10; 
		internal const int FNULL = 11; 
		internal const int FRTSCONTROL = 12; 
		internal const int FABORTONOERROR = 14; 
		internal const int FDUMMY2 = 15; 

		internal const int PURGE_TXABORT     =  0x0001;  // Kill the pending/current writes to the comm port.
		internal const int PURGE_RXABORT     =  0x0002;  // Kill the pending/current reads to the comm port.
		internal const int PURGE_TXCLEAR     =  0x0004;  // Kill the transmit queue if there.
		internal const int PURGE_RXCLEAR     =  0x0008;  // Kill the typeahead buffer if there.

		internal const byte DEFAULTXONCHAR = (byte) 17;
		internal const byte DEFAULTXOFFCHAR = (byte) 19;
		
		internal const int CLRDTR = 6;
		
		internal const int EV_RXCHAR = 0x01;
		internal const int EV_RXFLAG = 0x02;
		internal const int EV_CTS = 0x08;
		internal const int EV_DSR = 0x10;
		internal const int EV_RLSD = 0x20;
		internal const int EV_BREAK = 0x40;
		internal const int EV_ERR = 0x80;
		internal const int EV_RING = 0x100;
		internal const int ALL_EVENTS = 0x1fb;	// don't use EV_TXEMPTY
		
		internal const int CE_RXOVER = 0x01;
		internal const int CE_OVERRUN = 0x02;
		internal const int CE_PARITY = 0x04;
		internal const int CE_FRAME = 0x08;
		internal const int CE_BREAK = 0x10;
		internal const int CE_TXFULL = 0x100;
		
		internal const int MAXDWORD = -1;	// note this is 0xfffffff, or UInt32.MaxValue, here used as an int	
		
		[DllImport(NativeMethods.KERNEL32, SetLastError=true), SuppressUnmanagedCodeSecurityAttribute]
		internal static extern bool SetEvent(IntPtr eventHandle);

		[DllImport(NativeMethods.KERNEL32, SetLastError=true), SuppressUnmanagedCodeSecurityAttribute]
		internal static extern int SleepEx(
			int dwMilliseconds,  // time-out interval
			bool bAlertable        // early completion option
			);

		
	}
}
