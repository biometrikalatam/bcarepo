using System;
using System.Collections;
using System.Configuration;
using System.Runtime.Remoting;
using BioCore.SerialComm.Cedula;
using BioCore.SerialComm.Feedback;
//using BioCore.SerialComm.Fingerprints;
//using BioCore.SerialComm.Log;
using log4net;

namespace BioCore.SerialComm
{
	/// <summary>
	/// Summary description for Broker.
	/// </summary>
	public class Broker : IDisposable
	{
		private static readonly ILog LOG = LogManager.GetLogger(typeof(Broker));

		private Hashtable deviceList = new Hashtable();
		public IFeedbackProvider feedback;
		
		public Broker(IFeedbackProvider feedback)
		{
			this.feedback = feedback;
			CreateDevices();

		}

		public Broker()
		{
			this.feedback = feedback;
			CreateDevices();

		}
		public IDevice GetDevice(string name)
		{
			return DeviceList[name] as IDevice;
		}
	
		/// <summary>
		/// Ide es la concatenacion de los id de todos los dispositivos definidos
		/// </summary>
		public string GetId(string name)
		{
			foreach (IDevice device in DeviceList.Values)
			{
				if (device.Name == name)
					return device.Id;
				
			}
			return null;
		}

		//public bool ExtractFeature(ISample sample, out ITemplate template)
		//{
		//    return featureExtractor.ExtractFeature (sample, out template);
		//}
		
		private void CreateDevices()
		{
			BioControlsConfig config =
							SerializeHelper.
							DeserializeFromFile<BioControlsConfig>("Bio.Core.SerialComm.config.xml"); 
			//(BioControlsConfig) ConfigurationSettings.GetConfig ("BioControlsConfig");
			foreach (DeviceConfig devConf in config.Devices)
			{
				AddDevice(CreateDevice(devConf));
			}
		}

		private void AddDevice (IDevice device)
		{
			if (device != null)
			{
				LOG.Debug("Broker.AddDevice - Adding " + device.Name);
				DeviceList.Add(device.Name, device);
				device.FeedbackProvider = feedback;
				device.Open ();
				device.OnSampleAvailableEventAvailable += new SampleAvailableEvent(OnSampleAvailable);
			} else
			{
				LOG.Debug("Broker.AddDevice - Warn - Adding NULL Device!");			
			}
			LOG.Debug("Broker.AddDevice - AddDevice End!");			
		}

		private void OnSampleAvailable (ISensor sensor, ISample sample)
		{
			if (SampleAvailable != null)						
				SampleAvailable(sensor, sample);
		}

		public SampleAvailableEvent SampleAvailable = null;

		private Hashtable DeviceList
		{
			get { return deviceList; }
		}


		private IDevice CreateDevice (DeviceConfig conf)
		{
			string className = "BioCore.SerialComm."+conf.Driver+".Driver";
			ObjectHandle handle = Activator.CreateInstance("Bio.Core.SerialComm", className);
			IDeviceDriver dev = handle.Unwrap () as IDeviceDriver;
			if (dev != null)
				return dev.Create(conf.Name, conf, feedback);
			return null;
		}

		public void Dispose ()
		{
			foreach (IDevice device in DeviceList.Values)
			{
				device.Close ();
			}
			deviceList = new Hashtable();
			//LogProvider.Instance.Dispose();
		}



		//public bool Match (FingerTemplate t1, CedulaTemplate t2, out int score)
		//{
		//    return matcher.Match (t1, t2, out score);
		//}
		
		public void SetDevicesActive()
		{
			ICollection colKeys = deviceList.Keys;
			foreach (string nameDev in colKeys)
			{
				IDevice dev = (IDevice)deviceList[nameDev];
				dev.SetActive();
			}
			
		}
		
		public string GetSerialNumberSensor()
		{
			ICollection colKeys = deviceList.Keys;
			foreach (string nameDev in colKeys)
			{
				if (nameDev.Equals("Sensor"))
				{
					IDevice dev = (IDevice)deviceList[nameDev];
					return dev.Id;
				}
			}
			return "BIM-SSN";
		}
	}
}
