using System.Collections;
using System.Configuration;
using System.IO;
using System.Xml.Serialization;
//using BioCore.SerialComm.DigitalPersona;
using BioCore.SerialComm.SerialPort;
//using BioCore.SerialComm.License;
//using BioCore.SerialComm.Log;

namespace BioCore.SerialComm
{
	/// <summary>
	/// Configuración de biocontrols
	/// </summary>
	public sealed class BioControlsConfig
	{
		public BioControlsConfig()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static BioControlsConfig Instance
		{
			get
			{
				return Nested.Instance;
			}
		}

		
			

		public override string ToString()
		{
			XmlSerializer ser = new XmlSerializer(typeof(BioControlsConfig));
			using (StringWriter wrt = new StringWriter())
			{
				ser.Serialize (wrt, this);
				wrt.Close ();
				return wrt.ToString ();
			}
		}


        //public LicenseConfig LicenseConfig;

        //public LogConfig LogConfig;
		
		//[XmlArrayItem("Device", Type=typeof(DeviceConfig))]
		//[XmlArrayItem("DigitalPersona", Type=typeof(DigitalPersonaConfig))]
		[XmlArrayItem("SerialPort", Type=typeof(SerialPortConfig))]
		public ArrayList Devices;


		class Nested
		{
			private static BioControlsConfig config;
			// Explicit static constructor to tell C# compiler
			// not to mark type as beforefieldinit
			static Nested()
			{
			}

			internal static BioControlsConfig Instance
			{
				get
				{
                    if (config == null)

                        config = 
                            SerializeHelper.
                            DeserializeFromFile<BioControlsConfig>("Bio.Core.SerialComm.config.xml"); //(BioControlsConfig) ConfigurationSettings.GetConfig ("BioControlsConfig");
					return config;
				}
			}
		}


	}
}
