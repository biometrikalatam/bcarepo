namespace BioCore.SerialComm
{
	/// <summary>
	/// Summary description for ICommand.
	/// </summary>
	public interface ICommand
	{
		string Name { get; }
	}
}
