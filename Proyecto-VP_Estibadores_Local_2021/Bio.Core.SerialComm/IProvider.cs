using System;

namespace BioCore.SerialComm
{
	/// <summary>
	/// Summary description for IProvider.
	/// </summary>
	public interface IProvider
	{
		void Configure(IConfig config);
	}
}
