namespace BioCore.SerialComm
{
	/// <summary>
	/// Summary description for IFeatureExtractor.
	/// </summary>
	public interface IFeatureExtractor
	{
		bool ExtractFeature(ISample sample, out ITemplate template);
	}
}
