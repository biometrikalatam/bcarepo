﻿using System;
using System.Collections.Generic;
using Bio.Core.Matcher.Interface;
using log4net;

namespace Bio.Core.Matcher.Verifinger9
{
    /// <summary>
    /// Implementa el template de la tecnologia que se usa. Puedes ser template de huella, facial, etc. 
    /// </summary>
    [Serializable]
    public class Template : ITemplate
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Template));

        #region Implementation of ITemplate

        private int _authenticationFactor;

        private int _minutiaeType;

        private int _bodyPart;

        private int _type = Constant.BirType.PROCESSED_DATA;

        private byte[] _data;

        private string _additionalData;


        /// <summary>
        /// Tecnologia a la que corresponde el template
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia que contiene Data, incluidos WSQ, RAW, etc.
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Tipo de informacion (data) que contendra el template (desde BIR_TYPE)
        /// </summary>
        public int Type
        {
            get { return _type; }
            set { _type = value; }
        }

        /// <summary>
        /// Parte del cuerpo si es tecnologia biometrica
        /// </summary>
        public int BodyPart
        {
            get { return _bodyPart; }
            set { _bodyPart = value; }
        }

        /// <summary>
        /// Data propia del template o sample
        /// </summary>
        public byte[] Data
        {
            get { return _data; }
            set { _data = value; }
        }

        /// <summary>
        /// Datos adicionales segun sea la data. Por ejemplo si es WSQ, 
        /// se puede colocar las dimensiones de la forma w=512|h=512, o si
        /// es template NEC, el coeficiente coef=xxx. Si hay mas de uno separado 
        /// por pipe "|"
        /// </summary>
        public string AdditionalData
        {
            get { return _additionalData; }
            set { _additionalData = value; }
        }

        /// <summary>
        /// Obtiene la Data en base 64
        /// </summary>
        public string GetData
        {
            get { return (_data != null && _data.Length > 0) ? Convert.ToBase64String(_data) : null; }
        }

        /// <summary>
        /// Setea la data desde base 64
        /// </summary>
        public string SetData
        {
            set
            {
                try
                {
                    _data = Convert.FromBase64String(value);
                }
                catch (Exception ex)
                {
                    LOG.Error("Verifinger9.Template SetError", ex);
                    _data = null;
                }
            }
        }

        #endregion Implementation of ITemplate

        public void Dispose()
        {
            if (VfeUtils.IsLicenseConfigured)
            {
                VfeUtils.ReleaseLicenses();
            }
        }

        static public List<Template> ConvertItemplateToTemplate(List<ITemplate> listIN)
        {
            Template itemToAdd;
            List<Template> templateListOut = new List<Template>();
            LOG.Debug("Verifinger9.Template.ConvertItemplateToTemplate IN...");
            try
            {
                if (listIN != null)
                {
                    foreach (ITemplate item in listIN)
                    {
                        itemToAdd = new Template();
                        itemToAdd.AuthenticationFactor = item.AuthenticationFactor;
                        itemToAdd.BodyPart = item.BodyPart;
                        itemToAdd.AdditionalData = item.AdditionalData;
                        itemToAdd.MinutiaeType = item.MinutiaeType;
                        itemToAdd.Data = item.Data;
                        templateListOut.Add(itemToAdd);
                    }
                } else
                {
                    templateListOut = null;
                }
            }
            catch (Exception ex)
            {
                templateListOut = null;
                LOG.Error("Verifinger9.Template.ConvertItemplateToTemplate Excp ", ex);
            }
            LOG.Debug("Verifinger9.Template.ConvertItemplateToTemplate OUT! templateListOut!=null =>" + (templateListOut!=null).ToString());
            return templateListOut;
        }
    }
}
