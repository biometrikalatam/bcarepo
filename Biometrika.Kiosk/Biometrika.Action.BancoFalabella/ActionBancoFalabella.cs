﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Biometrika.Action.BancoFalabella.Printer;
using System.Drawing;
using Microsoft.SqlServer.Server;

namespace Biometrika.Action.BancoFalabella
{
    /// <summary>
    /// Implementa la interface de IAction para Autoclub Antofagasta. 
    /// Para eso utiliza un Arduino y mada a abrir por un cierto tiempo 
    /// los rele. 
    /// Si bien está para Autoclub, este esqueleto podria ser generico para cualqueir apertura en otros clientes. 
    /// Se deberia generalizar esta implementación para poder ser usada en otros clientes.
    /// </summary>
    public class ActionBancoFalabella : IAction 
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ActionBancoFalabella));

        System.Drawing.Image _LogoCredential = null;
        System.Drawing.Image _NoQRCredential = null;

        string _FooterMsg = null;
        string _PrinterName = "ZDesigner ZD220-203dpi ZPL";

        bool _Initialized;
        string _Name;
        DynamicData _Config;
        object _LockObject = new object();

        /// <summary>
        /// Constructor vacio 
        /// </summary>
        public ActionBancoFalabella() { 
        }


        /// <summary>
        /// Configuraicon especial si necesita
        /// </summary>
        public DynamicData Config
        {
            get {
                return _Config;
            }

            set {
                _Config = value;
            }
        }

        public bool Initialized
        {
            get {
                return this._Initialized;
            }

            set {
                this._Initialized = value;
            }
        }

        public string Name
        {
            get {
                return this._Name;
            }

            set {
                this._Name = value;
            }
        }

        /// <summary>
        /// En este caso crea el objeto de ayuda al Arduino  lo inicializa, para dejarlo listo 
        /// para su uso cuando se necesite accionar.
        /// Lee el archivo de config y si no existe lo crea.
        /// El puerto com si bien lo configura, la libreria lo setea solo, por lo que no es relevante.
        /// ArduinoType permite definir cantidad de reles a manejar. Y el Delay es el tiempo que se mantiene
        /// accionado hasta apagarlo. 
        /// </summary>
        /// <returns></returns>
        public int Intialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("ActionBancoFalabella.Intialize IN...");
                if (!System.IO.File.Exists("Biometrika.Action.BancoFalabella.cfg"))
                {
                    Config = new DynamicData();
                    //Config.AddItem("CredentialTitle", "VISITA BancoFalabella"); //No es necesaria en realidad, se autoconfigura
                    Config.AddItem("CredentialPrinterName", "PDFCreator"); //Cantidad de rele en la placa
                    Config.AddItem("CredentialLogo", "logo.jpg");
                    Config.AddItem("CredentialFooter", "Dirijase al Piso 17 para anunciarse!");
                    Config.AddItem("CredentialNoQR", "NOQR.jpg");

                    //_Parameters.Add("", "");
                    if (!SerializeHelper.SerializeToFile(Config, "Biometrika.Action.BancoFalabella.cfg"))
                    {
                        LOG.Warn("ActionBancoFalabella.Initialize - No grabo condif en disco (Biometrika.Action.BancoFalabella.cfg)");
                    }
                }
                else
                {
                    Config = SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.Action.BancoFalabella.cfg");
                    Config.Initialize();
                    if (Config.DynamicDataItems.ContainsKey("CredentialLogo"))
                    {
                        try
                        {
                            _LogoCredential = System.Drawing.Image.FromFile((string)Config.DynamicDataItems["CredentialLogo"]);
                        }
                        catch (Exception ex)
                        {
                            LOG.Warn("ActionBancoFalabella.Initialize - Error cargando logo para credencial! Path = " + 
                                        (string)Config.DynamicDataItems["CredentialLogo"]);
                            _LogoCredential = Image.FromStream(
                                new System.IO.MemoryStream(
                                    Convert.FromBase64String("/9j/4AAQSkZJRgABAQEAYABgAAD/4QAiRXhpZgAATU0AKgAAAAgAAQESAAMAAAABAAEAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAtAHgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/Ka7Yp1cF+0nomra98H9Yi0Wa4ivY0WYrA5SSZFILoCOeVz064x3ry87zCeAy+tjadN1HTjKXLHeXKm7LzZUY3djuEuEYnayt9DT93P41+ddprN5ZSrNb3l1DIpyrxzMrA+uQc16D4B/as8YeBrmPztQbWrNSN8F+xkZh7SffB9ySPY1/MmR/SsyivWVPNMJOin9qLU0vNq0X91/Q65YNpXTPtWml8Vj/D3xva/EbwhY61Y7vs99HvCv96M5wyHHcEEfhXxp/wAFePj/AKp8Hfir8DdJb4pa18JfB/ia61ceIta01VaSJIYrYwkhkc8O5XAH8ee3H9T4PGUcXQhicPJShNKUWtmmrpr1RxS03PuMygDvTq/PH/gnv+1N4g1/9pb4iaD4R+KPiL9oz4Y+G/Bi6zHeXsMMOqDV/O2rY2zOIjKHjVvmbEYZlBZSPn9n0b/gpPqXhf4p6f4T+J/wf8ZfDO98QaTqeraFJcalp+qJqiafbG5uoj9mmYRSrDghXOCWAyMgnpsB9T0V8dfDD/grVL4nt/h/rniv4P8AjPwT4B+KF9b6Z4d8VTajY31nJcXDbYBcRxSeZbq5B+ZxxjuMsOni/wCCjV/41+MOt6B8P/g/4++IXhvwnrp8N654m06Wzt7O1vVdUmSFJpVedYWYeYy42gE8rtLFgPp6ivkPxn/wVostBuPFWu6P8LvHXij4V+BdUfSPEHjewktBaWs0bhJmht3lE9xFExAd0XAwTgjBP1d4c8QWfivQbHVNPuY7zTtSt47u1uI/uTxOodHX2KkEfWkFy9RRRQAUUUUANMuK5P4w/HXwd8APCEmveNvEui+F9IjO37TqN0sKyNgnYgJy7nBwqgsccCukv3mi0+ZreNZLhY2MSMcK7AcAn0Jr+Y39r74v/Ej4zftA+Ir74sXWqN4w0+9mtbjT7wssei4cn7LBE3EcS/wheGGGJYsWPDjsZ9Xina7f3HxvGXFjyPDxnGm5yndLsrd3+S6n3h+2X/wVx+DWk+Nby8+FukeJvGAmLPcL5I0yyeTruhab97gktkNEBkZXcDivmnSP+C4GsXviVYZPhLH9j3YeKLXXa5j5wWLNbhePQhee4r5YzuFL5hb5WY4Xt6V+F4rwj4OxWJrYvEYFOdVtu0pxSb35UpJK++i3Py2n4u5xrzpX6WVvvvds/o2/4Je/tR+CP2m/2ZLG68I6hJJeaTI0Ws6bcALd6XcSM0m11HVGySkgyrgHGCGUcx/wUW+FXxO1n9oD4D/ET4beBY/iDL8NbzVri/0xtbt9JLi5ggjj/ezZwPlc5VWPygYGc1+dX/BuRr2raf8At36tYWLTnS9R8K3TaogJ8vEc0Bhdh0yHcgE9PMYDqc/qZ+2t43/sLxL8PtGvte1jw74c1q7un1a50uV47hkijQooaMF8Fn5ABz+HH7Rw7haOFy6jhMPHlhTioRW+kVZavysfrnCOfVc4y2OLrq0rtO2za6r1Plz42/sl/tJftifEHxh8TF8J6H8CfFEHgWfwtpNja+JotR1DXZJZld/Nu4UVI08rzI1LAMrbTwPmXmvBP7AXiu/+NXgHVfCf7OA+D9l4T8OeJbDXribxLZX8mvXd7o8lra7XErSSbZmcb3wMTliRjn7B+GGs+H/C+ma1rXw71vxJ42v7OOKC5tvEetT21paxu+RLuuIwoIKdsnBPrzyX7QP7SF98RPgL4+0O7srfRda0GTS5Xl0zUxeWt1BNdxDKSoB0wAynH3u/IHucx9NY4PxN+xv8Srz/AIJh/AX4f2/hvzPGPgnX9Av9Y0/7dbD7JFbTs8zeYZPLfapBwjEnsCeK0P2dPDXx0/YR1bxl4D0X4Mx/Ejwtq3i++1/RPENr4qstNWO0u5RIY7mOVfM82IZycHeflXgA19L/ABr+LPiX4c6rGml6DoF9Ytb+cbjUdfjsGkkBbMSIyksQADnp82OxryK1+Omu/E/48eDfEfhHRDqEmqeELhzpd7qP2WK3YXbRysWwQxDJgELkgg8c0rjsjwmD9m34+fBv9mL4jfs5+GvhrZeItF8XarqMWj+OJPENtBY22m38oZ3ubdiZxNEjuCqhtx+7uCjd97fBf4eR/B/4P+E/CcVw13F4W0az0dJiuDMLeBIg5HbOzP415in7Z8cfgy4a48NXUPjSDXF8OHQftSHdeMMq3nfdERUEl8cYI5HNcD+0/wDtBax4i+FHirwrrOlr4b8R6fHY6iv2DUhdQXdo90kZKyKFIw+AVI7ikCR9ZeZ04p1eKeD9TvP+G0PiFGrTXQtfDunm3t3mIj3fMcDOQuT3x3qHQP2rNYtfiPoOg+JfD+jWEfiK4azgl03XotQktZ8EqsyKo2g9Mg4BNAz3GikU5WigBhG8c14j+1r/AME7PhL+2xYr/wAJ14YhuNXgj8q21uxc2mqWyjOAJl5dRkkJIHTJztzXuIUAUu3mpnGMlyyV0c+KwlHE03RxEVKL3TV0fkD8a/8Ag2Y1qzuZrj4c/E7T9Qt2OYtP8Rae1vJEMDrcwllcnn/limPU14xZ/wDBvP8AtGTeII7OS38DwWrMN1+dcLQIO52iPzD9NlfvLtGaTYPSvPllOHbva3zPiMT4Z5FVnzqEo+Sk7fjc+W/+CZH/AATJ0D/gnf8AD/UFGoR+IvG3iNY/7Y1kQGGMomSltAhYlIkLMSSd0jfM2MKq+tfFf4V6p42+Nvw312zNqun+FZ72W9MkhDkSxIqBBj5iSDnkYr0vbijHNd1OnGnHkhoj7PAYChgqEcNho8sI7L+uvc8n/at+EesfFPSvDcmk2+m6snh/Vk1C50bUJjDbaqgUjYzYIBGeAwK8nPTB8k8S/soeL/G2h/EG5g8M+GPB83iK002DT9Hsr5ZIla2uVlkZnWJEXcqnHHUjOOTX1mRmk2L6Vodh82eIPg9401H4m6n4u1L4f+D/ABhJ4i063gGn6hqqn/hH5I1KskbvCyyIx+bKqpye2Dmn8Of2ffiF8Db7wfq+m6ToHiK60fRLrS7uy/tU2m1pbuScFJDEQQAwB4HIP1r6f2gCjaBQB8wa7+x74m8T+GrzXtQbw7eeMr7xIviGfTJizaZLEsZjFmXxk/KT85Ht/tVJ4r/Zr13xv8GvE+n2fgHwX4J1e8a1+xx2F6JpLpY5RJIksvlqFBwCo55HPY19N7RRsA7UAfOviL4NfELxZ8QfE2pLY6XpMfj7wwml3c8eqmRtDuERwAuEBmVjtBI24DE87cNkeDP2bvFD+Kfh8svgTwb4Tt/Bd5Hc3uqWGoLNNqwRNp+URB/mPzfOx619Q7RRsA7UACnK0Uo4ooA//9k=")));
                        }
                    }
                    if (Config.DynamicDataItems.ContainsKey("CredentialNoQR"))
                    {
                        try
                        {
                            _NoQRCredential = System.Drawing.Image.FromFile((string)Config.DynamicDataItems["CredentialNoQR"]);
                        }
                        catch (Exception ex)
                        {
                            LOG.Warn("ActionBancoFalabella.Initialize - Error cargando NoQR para credencial! Path = " +
                                        (string)Config.DynamicDataItems["CredentialNoQR"]);
                            _NoQRCredential = Image.FromStream(
                                new System.IO.MemoryStream(
                                    Convert.FromBase64String("/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCACPAJEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKACikZtoya8v/ar/AGw/An7Gfw3bxP471hNNsmJjtbaMeZe6nMBkQ28XWR/XoqjlmVQSM6tWFKDqVHaK3bOrA4HE43EQwmDpupUm7RjFNtt9Elqz09mwDgZPpXy1+1j/AMFhfgj+yPfXOm6r4kfxL4ktiUfRPDiLfXULjIKSvlYYWBHKySK3opr8pP28v+C13xO/a+uLzRPD9xc/D3wDLuiGm6dcbb/UIzxm6uVwx3DrFHtjwxDeZgNXxqFCKAqhQOAAMAV+bZz4gwg3Ty6N/wC89vkv8/uP7d8Mvoc4jGU4Y/jGs6SevsabTl/2/PVR81FN/wB5M/Rv49f8HJHxM8YSz2/w+8I+GvBdk4KpdagzatfgdmU/u4UP+yY5APU9a+WfiN/wU4/aD+Klw8mrfF/xvH5hyU0u+/siP6bLQRLj8K8KJxTJJ1ixuZRk45OOa+AxfEWZ4p/va0teidl9ysf17w94K8C5DTSweW0rr7U4+0l680+Z/czp9b+MnjHxNMZNS8XeKdRkJyXu9WuJ2J9y7k1DpPxU8U6BcedYeJvENjN/z0t9Smib81YGp/DvwX8Z+MIFl0jwf4r1aNxlXstIuLgN9CiGptb+A3jvwzbtLqXgfxlpsSDLPd6JdQKo9y6AVw2xj9/3vXU+p9pwvH/Z06C/u3h+R3fw/wD+Cifx5+GN0kukfF/4gDYQVivdYl1GAf8AbK5MkePbbivpv4Gf8HGPxl8ASxQ+NNG8LfECxXHmytD/AGTfyf8AbWEGEf8Afivz8E6mRk3LuQ4Zc8qfQinA5rswuf5lhZXpVpK3Ru6+56Hz2feEHA+fU2sbltGV/tRgoS+U4csvxP39/Za/4Lo/A79pO4ttNvtXuPh34gn2oLLxIEgtpXPGIrtSYTk8ASNG7Z4SvsmC9SeMOCCjAMrA5DA9x7V/JyRuFfS/7Df/AAVc+Kv7Dd7a2Gl6j/wk3gmNx5vhnVpS9tGncW0vL2rdfuZjyctG5r7vJ/EJtqlmMf8At5fqv8vuP5L8SvobezpyxvBtdtrX2NV7+UKnfsprXrNH9GtFeDfsQ/8ABRD4eft5+Cm1LwfqBg1exjVtV0C9Kx6jpjHjLKOHiJ+7KhKHoSGBUe7pIH6V+nUK9OvTVWjJSi9mj+F81yrG5Zi54DMKUqVWDtKMk00/R/090OooorU88KKKKACkZtopa87/AGov2k/Dv7JfwT1/x34qnaPSdDg3iKPHnXszYWK3iB4MkjlVUdOckhQSJnOMIuc3ZLVnRg8JXxdeGFw0XKpNqMYrVtt2SS7tnnn/AAUN/wCChHhT9gP4R/25rW3U/EGph4dB0GKYJPqcwAyxODsgTcDJLg7QQAGdlU/z9ftLftReNf2vPiteeMfHWrNqeqXP7uCFAUtNNhySsFvFkiONc+pZjlmZmJY2P2sf2pvFX7ZPxw1bx14tud97qBEVrZxuWt9KtFJMVrCD0RMnJxlmZnPLGvNycCvwjijiirmVX2VJ2pR2Xfzf6Lof6x+AvgPguCcBHMMxgp5hUXvy39mn/wAu4en2pbyf92wV6z+yl+w98Tf21PEzaf4A8N3GoW1vII7zVrhvs+maef8AprOeN2CD5ahpCOQhqX9lG+/Zk8F/FuRP2n/iv/wgNnpcdvdL4VTRtXk1HWI57eO5gkkmt7Z0htpIZonUo/mSK+R5Yw7fq14B/wCDiz/gn98KfB9j4e8NfGDQ9C0PTIxDaWNj4N1qC3t164VFssDkkk9SSSeTXqcO8DVcVFYjGtwg9l9p/wCS/E+F8ZvpW4PIas8n4WjHEYmOkqj1pwfZW+OS62aiu7d0YX7Lv/BuD4A8IWltffFPxHqXjbVOGk03THfTtLjPdS6n7RLj+8Hiz3SvuT4P/sbfC34AQxDwX4A8JeHJYl2i5s9MiW6f/enIMrn3ZjXkn7Iv/Bav9mP9u34vp4C+FHxOg8V+K5LOW/WwXQ9TsyYIseY++4to4+Nw43Z54FewftWftcfD39iP4MX3xC+KHiFfC/g/TZobe51B7K5uxE8ziOMeXBHJIcsQOFxzziv1PA5PgsHFLD00vO2v37n8C8VeJPE/ElR1M6xtSqn9nmagvSCtFfceiGJj/HQY23cNXwZ/xE8fsND/AJrpa/h4V13/AOQq+q/ih+2F8Ovgx+zBJ8ZvEviJdN+GkOl22sya19huZgtpceX5MvkxxtMd3mx8bNw3cgV6R8QaHxX/AGZPh/8AHWzaHxl4K8KeKVZdu7VNKhunQf7LspZfqCDXxd+01/wbrfCL4lWtxdeANR1r4b6uQWiiSRtT0t2/2oZm81c/7Eygf3T0r2n4/wD/AAWi/Zo/Zb8BfD/xP48+JtvoOh/FLS/7a8LXTaLqVwNWs9sbeaFhtnaMYljOJAp+YcUnwF/4LS/szftPfDv4geLPA3xNt9d8P/CzTl1fxTdjRNTt/wCyrVhIRKUltkeQERScRqx+Xp0rz8ZlWDxceXEUlL5a/fufX8Mcf8R8O1VVyTG1KNuik+V+sHeL+aZ+Kv7Zn/BM/wCLX7Dc7XPi7Q1vfDJkEcPiPSWNzpjk/dEjYDwMemJVTJ4UtXgVfurF/wAHGf7EHxSLaHb/ABftNbbVI2gewPhDW5VukYYZGRrLaVIyDu4wea/Gj9qHx34L+Jfx78S638PPCf8AwhPg2+uy+maQZjIYI+hYjJEZc5fykJSPdsUkKCfx7i7hvDZY4zw9TSX2Xv6+nqf6S/Ry8a8942p1cLnGD1orWvHSnJ/ytPadtfdurbqOl8L4T/FvxL8CPiHpnizwfrN74f8AEOjy+baXtq+GQ9CrA5V0YcMjAqwJBBFfvP8A8EtP+Cpeh/t+eBX0vVEs9C+JuhQB9V0mNsRX0YwpvLUEljESQGQktExAJIKO38+9dH8I/i54i+A3xL0bxh4T1OfR/EOgXAubK6i/hbBBVl6PGylldDwysyng153DfEdbLK3em91+q8z6/wAbvA/LeOstlOCVPG00/Z1O/wDcm+sH98Xquqf9VCtuFLXg/wDwTy/ba0P9uz9new8Y6esVhq0DCx13Sw+5tMvVUF0HcxsCHjbujDOGDAe8V++YfEU69KNak7xkrpn+RmbZVi8sxtXLsfBwq0pOMovdNaP/AIfruFFFFbHnjXbatfhP/wAF6f25pP2iP2i/+FcaHeNJ4O+G1w8E+xv3eoauAUnlOOogBMC5HDCcjIcV+s3/AAUd/akH7Hv7H/jPxvDJGmr2dl9k0ZXAbfqE7CK3+U/eCO3mMP7kb+lfzUyzyXUzyzSyzyyMXkklcu8jHkszHkknkk8kmvzfxBziVKjHAU3Zz1l6dF83+R/a30O/DenmOZ1uLMbG8MP7lK+3tGryl/25FpLzlfdDScCvvv8A4In/APBLuL9q/wAY/wDCyfHdh5vw68OXXl2VjOn7vxHepyVYH71tEcb+zv8Au+Qsgr5E/ZX/AGeNX/av/aE8LfD7RWMN54jvBBJc7NwsbdVLz3BHQiOJXfB6lQvUiv6Yfgv8H9C+A/wy0Twh4Zs1sdA8OWcdjYwjkqiDGWP8TscszHlmZieTXz3A/D8cZWeMrr3IPRd5f5I/ZfpV+MVbh7Lo8N5RPlxWJTcpLenS2du0pu6T6JSe9j8dtC+GXgjxx/wcy/ttXPjbwR4V8dWHhT4aaZrNpput6fFdQedBoujEbBIrBCRlcgcA18TeHv8Ag4l+F3iHX9KtX/YN/Z4Uajcx22fs0J+86px/o3bPevswfHT4b/BT/g5f/bXT4l/EHw58OdL8V/DnS9DtdR1i5WGN5ptF0cYTcQGZVy23PIWvjHw3/wAEOv2TNB1ywvv+Hh3wmkFlcxz7PstuN21w2B/pfGcelftJ/mIffPwu+E3hL4L/APB2PBo/gvw1oPgnQ2+DT3Rs9Hso7O3SRxlpNkYC7umTjnaK/PL/AIKCfsPftBftG6jqtp+z/wDFr4wftf8AwUJM+taol7LNp9jqscjySWRheUhniQwuCq9JVwBX6AfBD9obwH+0h/wdaQeJfh74t0Dx14cj+Dcln/aOk3a3Ns8sY+eMuuRuAIyO24V8feA/2lvH37X/AO0Pd638J9b179gn9mLTZbrTfEXiPQblk8MDXYyzCa4dPJi+1XAe3j5OcKnJ7gH46a3DPa6tPBdwm2urdzDNE0fltG6/KVK9iCMH3r9UP+Dcr4g6n+2R+294c8CfGT46a9deCNKs/K0r4fa9qE2o6Z4ykRCqWDWsjGExRxgyAMpGYRgV9F/tZf8ABGD4Y/skf8E7PiH8ONP8R+F/j3+1J8Wb618V+CZYdLVfE99ZPPAZzZxiR3kjKR3MjMDghnPYVwX7JP7Q3g74z/DhfCPhz9l/Sf2Zda+H6W/hzxN+0JaqPN+HWowoFku7h/LjMcszxtEQ0wINweeKAPtzXPh98Bv2Z/g3+178R7fxF4W/amn+EtzJdWvgLxJp6S2Xwx2SzL/ZFp5gcQQ9UxEoGLVeMg1wvwX+P2sftG/8Eh/2nfE91+yt4L/Zv8N+Lvh6D4a1bR/Jt4fGZuY5kiUEIhOGaPZkc+cMV4v45/bX0jwd4G1n9l/Xvg9F4F0X9oaxm0nUfj8+4j4gW9upL+J/IWINdecMy7RKSTcAAkkLXy5pfjTxd4f+Een/AA3ufiV428Z+BvDNwRodnq9w0Vtawx7kgKWgd0iIjJ+Xc+0uwB9fneIeIaOV0OaWs3tH9X5H7P4NeDWZ8eZn7OnenhabXtaltv7sejm1sum76Xp6PqaWfwc8A+Fk8PeFNGfwboqafPd6Xp0cV3q9wzF5bi5uAokmYkhVDHaqIuBkszQ0V0Pwx0Dwr4m1rU18WfELwf8AD/TtI0q41R5ta1GOGe/aNGMdtawk75ZZHAUEDaoJJOQqt+HSljM2xv8APUm/69Ej/VSnDhvw94at7uHwmHj9/wCspyfzkzW8C/s2eOfiZ8KPF3jnQvDeoaj4U8CpG+talGg8q03soxyQXKhg7hAfLT522rzXEA5r9vv+DaGX/ha//BEz4eXWvRQal/wkF34hGoRSxK0d0j6vexsjL0KbAFwf4QB0r8wP+CmH7Gkv7Dn7WWueErdZW8NXqrq3hyaQli+nyswSMserxOrxEnk+WGP3xX0PEfCMsswtPERlzdJeT6W8un/Dn4z4K/SMocbZ/jMoxFJUd5UF1lTVk1L+/wDa00s2vs3e7/wSc/bjl/Ye/aqsL/UbtovA/iox6T4mjY/u4oSx8q7xnrbu5YnBPltMoGWr+iu0m85M5yOx9fev5OG5QgjOa/oB/wCCHP7VMn7S37Dmk2d/dfafEfw9k/4RrUC7ZkliiRWtJT3OYGRCx+88Mh9a+g8PM4b5suqP+9H9V+v3n4/9Mnw2p0pUOMsFGzk1SrW9P3c356ODf+BH2XRRRX6kfwUfkl/wc0fHeQt8MvhnbTFYj9o8UajF/exm2tD9ObzI9lr8oK+wP+C7nxCfx5/wUs8ZWhcyQeFbHTtFgOeAotY7lwPpLcyg++a+P6/nvi3F/WM1rS7Pl+7Q/wBjfo68PQyfw+y6mlZ1Ye1l5uo+b8IuK+R+s3/BtL+zEj2Xjn4vX0OZXkHhbR2ZfuIAlxduPZibZQR/zzkHc1+s1fNf/BIv4Tx/CD/gnX8KbBUVJtR0VNduMDkyXzNd8+4EwX6KPSvpSv2rh7ArCZdSoreyb9Xqz/L3xi4pqcQ8Z5hmU3ePtJQh/gp+5G3qlf1bPw5/4LUftPfs7/s/ft4+L/8Aha37Bet/FDU9RvdM06Lx/LdTW1p4ouX0y1aOGFjGUd4kKwbUYnMB75FfLfjT/gob+xT8KvibD4P8Yf8ABNq78J+IHkhSSw1XVHtbqJZcbGMckatgg5HTPrX21/wd2fH7wt4T+A/wcgTXtI1DXPB3xO0/WNS0G11GI6jFElrLKC0O7cm5WXDMAP3inoa/OP8A4LUf8FIvhF/wUv8Ah9D8XfA/7O3xL8G/EP8AtiyE3xC1PdLpk1taxtGLTcjGHfu8rGAD8mD1r2j80P0z+M/jL4Xf8Efv2w0j+CP/AAT78aeKtUPh+OZPGHhK3uPs/lXSky22THIuQEAb5gfavz+8IeE/Gf8AwXP+MA+DX7PfgnxR+zR+zVrgmu/EdjJZS6n4ffX7XNw888iqoWdwLZAm8YMScV8/ftLf8FjP27rX4Q6bZePvil4p0XQPiFpv2izhSG1sbu6s2xiRTGizojjo2RuGcV0n/BC79sf9pC6m1r4DfCv41+DPg94e12eXxHdeIfFLQrDpc6qofymlBy8+yNCMHpnjrQB9q/8ABJf9sXR/AHwe+IHxN+Ktif2iv2lvgV4rk8F/D/SLe7QeKpdHjjS2kjsIAC7QIJbqRv3bkKJOau/sBfs0+I/jp/wTJ/b3svirZ61+z3pvxa8ZLr8Wo+L9IuEFhDJOLojyyqPOchYwI/mZ3UAEkA3v2a9L/Zh+Nf7O/wARPij8EL3wh+zh+0h8N9Yl0xPHmv6yb6S/uVRf7Qv7WzEknnR3UclyihIWOZeF7V8u/E79sr41/tEeBrLRPip8Utb+IENjdNdxrLDFaWgkwVVxDEigkKTgvkjc2MZNfPcQ8Q0cro80tZvaP6vyP2Xwb8Gc049zL2VG9PC02va1baL+7Ho5tbLZbvonY/aE/aQ1D45+Efhf4XngsV0L4O+F7fwnoMy2oS5uYo44kluZWJLK0xhRjEG2IFVRuIZ280ord+E3wy1v45/Fvw34I8N2V7d6x4o1K30yOeLT5rq10ozv5a3N2YgfKt1b7zEjocZwcfh0pY3N8b1nUn/XySP9VaVHhrw84b+zh8Jh4/f+spyfzkzzz4gfHHwn8FrvTJfFR1Oe0u5h5lnpbR/bpoQfnMZk+RDjgM4IBP3WwVr7++CnxI/Yu/ae/ZQ8N/FXw5+xFqXjeR/GEfgvxRA99PfXXhmJYFlGpXVwMhofLO5nIQDDZIxX5/8AxQ/4ILftL/Ev9qP4x+FbDTV8T6j8LGhkvNZnVtK0vVoZMEvazXOyIpGucjdwENfrj+wb478D/wDBJr/gmP8AC/4S6h4JtPi741+K2qS6L46074e6nb6xLZC8aRPtN6YWYiJYHSMuMDPANft3DnDdHKqNviqPeX6Ly/M/yz8avGnMePMyu708HTb9nT/9vnbRza+UVourf6W/sU+DfhB4A/Zy0TSvgR/wiv8AwrG2luv7L/4Ry9W804O1zK1x5cqu4P78y7sMcNkcYxXyL/wcUfs0J8Sf2RdP+IFrAP7X+HGoq8rgfM+n3bJBMvTnE32Z/YK/rX2f+y7+y54G/Yz+Cul/Dz4b6FH4b8IaLJPJZaek8s6wtNM88h3SMznMkjnk8ZwOKk/aa+FEPxy+APjXwbOFK+K9DvNLUkZ2PNCyKw9wxUj3Fepm2CWLwdTDv7Sf39PxPz/gHiarw7xHgs6ouzo1It+cb2mvnFtfM/lwr9Bf+Dcv46P4B/bE1zwRLNtsPH+huY48/fvLImaPH0ge7/IV+fMe4IA67HH3lP8ACe4/A17R/wAE5/iDJ8Lf29PhDrKSNEF8VWNlKynpFdSC0lz7eXO+a/AsgxTwuZ0anaST9Ho/zP8AXzxiyGnn3A2Y4O13KjKUf8UFzx/8mij+mLcaKbn/AGRRX9G2Z/inzxP5rP8Agptqzaz/AMFCfjHK3VPFV5APpE/lD9EFeD3b+XayH0Qn9K95/wCCnejPoP8AwUN+McD53P4ou7nkdpm85f0cV4PdLvt3HqpH5jFfzTmqtj6yl/PL82f7jeHvK+EMtdPb6vRt/wCC4n9UnwN0GPwr8HvCulwqEi0zRbK0RR0VY4EQD8hXV1x37P8A4jXxj8EvCGrxtvi1XQrG8RgfvCS3Rwf1rsa/pOnbkVtj/EPFqarzVT4ru/rfU/BX/g79/wCCdvgXw94M0z45eF/COtXnxT8c+J7ay1/UbWa4uY5LS20zyUzCNyRgJbQjcAv3STkmvJf+CPnwE/bL+Kn/AAS60J/h78XPgV4Z+Cl/rF/b22h+ONPhuM3C3DNLvMttIDmQMyruJHJwK7z/AILU/wDBdH9qD4E/8FS/if8As8fDbUfCqeEf+JTpFlDe6FHcSwjUdJspJTJMecebdSHcR8oI9K7n9jL/AIITfGay/ZVk+Bv7T+sfDvU/2cfCiX3iXRYPC+ttFqi68SzRs9z5abogktzlG7lf7tWc54r+2V4o+Afwe8cpF+3j4J8RfHb4vwWMEMHjH4aT/ZPDMemYYWdivlyW8azR4k3AR5wy/er4U/4JE/sz/Cn4x/8ABRePQfj1oeoaN8K00XVdcuYNTuLjTpYbaKF3t23JiRzkKqqmTIxCjJIFdV4e8L/tIeIP+CIbadpGoeC7v9n+X4jFYdKQJJ4hbVA4xIfl4gyASS36GvUfjH8d/HH7U3jXSvGfxPvdG1jxvYaFb6ALzT9PSzhgtIWZ0hRV6/M7FmPLHHQACvnuIeIaGV0eaWs3tH9X5H7N4N+DOaceZl7KlenhabXtKttF/dj0c2tlslq9N/Lvh/8AAPwx8IfEOtXGhRGb7bezm2uJ0xJDaGVjDEoJJXCbd3JJPXpXXdKK674Q+BtG8XXOvar4l1ebSfCXgjTBr/iN9PSO51f7AJ44T9jtWZTNKzyKoP3EG53OF2t+HTnjM2xl3edSf9fJI/1UoUOG/DzhrTlw+Ew8dX38+8pyfzkzD+GPhaX4v/HLwb8N9JvtPtPFXxCvzpOiC93/AGf7SUJUzFAWWIHbuIBIDDANcVo/7T37Yf8AwR8/ax+L3hfwRcXMWu3GpR6Tr95p3hk3+nX7We9YmtzNCcIBI+CAM7iTX6R3P7MfiHwv+wx4l/acuLfQEs/gLph8Y/s5waZj7bp9peESs+uW6oBcXbgWxcMT8yy9Og739mf/AIO5fg9pP7PHhL/hbWh/EmX4iQ6XGfE1xpfhaNLB7sD948WZRhOhHA61+28N8N0cro96j3f6Ly/M/wAtvGzxszLj3Mba08HTb9nTv/5PPo5tfKK0XVv8+vg9/wAF5/2jv2hovF2h/HX41eEPDfgTS7eOHxR4W1Tw/FZ6p4s06Z/LvLCz8uDctw0BcAlkxvBzXvf/AAbveLfhnqf/AAUb/ac1j9nq1uvhv4D1rwFHpvgn/hK7jd9l1Jmt9qSO7vvPnhpPL3s2z1r8rf8Agqp+0Von7cX/AAUS+LXxT8DWuszeGPFeqDULMXVp5dxFCIIo8yopYJyh7n619o/8G23gbwr+1rpHxF+HnxZt9Q1D4afBmP8A4XJY2uk5hvhqtr5cJbzE+eVPIDAQ5wWweDX0p+HH9G/7BPhH4xeBf2XdA0v49+JdC8XfFG3muzquq6PEI7O5RrqVrcIojjxtgMSn5Byp69T7C67iPY+leR/sLftoeFf+Cgf7MuhfFXwVaa3ZeHPEMt3DbQ6vbLb3atb3MttJvRWYDLxMR83Qjp0r1x5NhHHU4oA/lk/aE8PJ4S/aB8eaVEoWLS/EmpWaKOirHdyoB+S1k/DnV38P/EXw/qEZ2yWGp21yhHZklVh+orU+P/iGPxf8e/HWrw8xat4j1G9Q/wB5ZbqSQfo1Z/wv0aTxJ8TfDenRAmXUdVtbWMDu0kyIP1NfzNL/AH33f5tPvP8AczC/8kxH6z/z4XNf/Bqf1X0Um6iv6ZP8Mz+fr/gvN8OZPAX/AAUo8U3rJsh8XaZputQ/3WX7MtoxH/bS0k/Gvjkn5DxX62/8HMvwBMuh/DT4mwQg/Y5rjwzqUijnbIPtNrn0CmO6GemZQK/JKv594uwjw+bVV0k+ZfPX87n+xP0cuIY5x4fZfNP3qUPZS8nTbiv/ACVRfzP6Lv8Agjh8XE+MP/BOP4YXfnCS50XTP+EfuFzlo3spHtlB9zFHG30cV9PV+Q//AAbT/tNLp+r+OfhFf3O37bt8U6MjNgF1CQXiDPfaLVwo7LKexr9dYpfMzx0r9n4cxyxeW0qq3tZ+q0f+Z/mP408LVOHuNcwy6StF1HOH+Cp78beidvVM/IH9m3wVp3xI/wCDqH9tPQNVi8yy1j4baTZzFQBJGkmk6JGxRiDtfDHBpvxk/wCCOc/w6+HviP4KeOdc/sH9h/wHYyeMNG8QzeIFTxSPEDJgx3Eu077cvNOBGsW5iYgpLEVnftv/APBYH9pH4Sf8FSPjz8Mvgv8ADb4J6jpnwn0XTNZ1bX/EbfYLsWU+nafMxmuDcRiU+ddCNEUFiBGoBPX5C/a8/wCCnPxi/bz8NaHYfES90Ow0/TP9IOj+HoJbfTnueR5ziR3eV1U7VLHC/NtVSzE5cQ8QUMroc09Zv4Y9/wDgHo+Dvg5mnHmZ+xofu8NTa9rV6Jfyx7za2Wy3em/yT8Bvgja/Bjwz5fmG51W6B+03AkbyyN2QqKeABxzgEkZPpXd0V3Hwr+Fulax4f1Hxt471q48E/CTwrdW0HiTxUNPlvRZtO4WK2hijBaW5kJ4UDCL874GA34bKWMzfG9Z1J/18kj/VmnT4a8POGre7h8Jh479W+76ynJ/OTZ4j8b/jjp3wV8O/aJtl1qc6n7HZbsGUj+Jv7qDue/Qe2J/wR18AftE/tS/8FBLbxj8DbbwxrfxI8CWcuvND4juFi01rc/6MUZCQGUeeuEBByAw6Zr78nk/Zd/aY/YZ+Jv7MP7KfjbxH8XfjZ8ZdVt9X0keKtGFjcL9laCWaOO6kt4YoIktraRguRyWH8RqH/gkX8MvjD+1x4O8c/s86N8Dfh38K/hXZaungv4s+MvCd+bXxTBd2igs8css773aSBdwSNkw7cc8/t3DnDlHK6Peo/if6Ly/M/wAtfGzxszHjzMtL08HTf7unf/yedtHN/wDkq0XVv1f9mq5+PVl8QPiYn7G9n4Q1r4q3OrD/AIaE0/xxKDpGmeI90mItGXcg+yeYb0cFhtWLngAeQ/8ABZv/AIL2+KPCf7O1r8ArXSPBa/FuTTNR8K/GaFfDubKxu2jSPGnTb9pGTLhvmxhSMEZrl/8AgtB/wbp+LP8Agnp8NP8AhY/wF8S+P9c8IaXp13q3jzUtX8RQwT2pjeMQuiR+W8xYPJnhiMe9W/8Ag1BsfFHjP4Xftbax4b8MeHvH3xGtdG0248P23iVEmgvdQIvCiSyyHIVmVdx3Dp1HWvpD8OPDP+Ce/wC1F8HfgN4C+FXw2+EE2s6h8R/2i7m28CfGePxBp4ktbLT7mYw50qTCiKXbO+GO8ZCkjgV9C/tCfHj9nz/g3A/a8uPDP7N1/wCLr/4jPqdnoPxIh8XWv9p2NvoUgiunNoyLEDccx/3h94Y4rV/4OgdK8YaP/wAE9v2V9e+Ifgnwp8Ofipd6zqUniG08MQxxw2dwsSlFjljZiflCt99sEnk4r45/YL/4JieDviV4U8NfFr9rfxN4z8G/C34v3A0bwJ4g0e6S/vda1f7SYTFOmyaWNAsUvzuqjKfeoA/qQ/Ye/a48B/tzfs1aF8TPhn9rPgzX5LqOxNzY/YpCYLmW3lzF/D+8ifHqOe9a/wC1d8XovgH+zf468aSOiHwvoV5qMQc8PNHCzRJ9Wk2KPc1y3/BPX9h3w3/wTh/ZO8O/CHwlqms6x4f8NTXkttdaq0bXT/abuW5YMY1VcBpiBhRwBnJ5r5N/4ONv2o4fAH7MWi/DSxnA1b4hX63F4in5k060ZJWJ9N9x9nA9VSX0rzs3xscHg6mIl9lfj0/E+z8POFqnEfEuCyamrqrUipeUE7zfyimz8T4FMcShiWIGCx6mvcf+CaPw7f4qf8FAvg/pKIZAvie11GRQOsdmxvJM+2yBq8Qr9E/+DcH4Ef8ACc/tWeKfHlzBusfA2i/ZYGb+G9vWZEZfXEENyp9PNHqK/BOHsK8VmVGm9feTfotX+R/rl4z5/TyHgbMca3ZqlKEf8U1yR/GSP202n1FFLmiv6MP8WOVHj37ff7Msf7X37J/jPwCfLF7rNgZNLkfgQX0LLNbMT2XzUQN/sMw71/M/qemXWianc2V7bzWd7ZStBcW8yFJIJEJV0YHkMrAgjsQa/rEZdwr8QP8Agv8A/sKy/BX45R/FvQLPb4V+IM+zVhGvy2Gsbcszei3CKZAf+eiTZxuUH868QMndagsdSWsNJf4e/wAn+Z/aP0PfEqGV5tV4Vx07U8U+anfZVUrOP/b8UrecUt2fE/7O3x21v9mP44+GPH3h1h/a3he+W8ijZtqXSYKSwOf7ksTSRt/syHHNf0heDP2xfAHiz9mG0+MI121sPAd1pg1SW/unCiyQfK8cgGf3qSZiMYy3mAqATjP8xP3hXSN8XvFD/CZfAh17VP8AhDo9TbWV0fzz9k+2FBGZindtqgDsDkgAsSfiuGuKZ5VCpTceaMldLtL/ACfX0R/Tvjl4B4fj7E4PG0ansatJ8s5WvzUm7tL+9F6wvprK/Q639u34xaH+03+3j8V/iz4fstS020+IN1YIsN3J+8e3sbG2s4S6r8qlvs/m7edpkxk7Qa8uzRnNdf8ADn9nD4pfHPQtSvvhv8M/FnxGOkSRxXUWjRKqxs/IVpn+RG2gn+IjIO0g141SpjM2xl379Sb/AK9Ej9PwmF4b8POGuROOHwmHjq3u33fWU5P5t7HUfsLfs0L+2f8AtYeHvhkut2+iPqUFxqd5MV8yaOytlDzNGndzlUXdxlsnIBB8m/ZM/wCDhnxn+wh8KPGXwmu/hl4B+Kvhu98V3mreX4sMtwkJJRFiWH/VhV8lWHy9STX6Bf8ABJb9lX46v/wWI0H4meLP2Z9Z+BPgHRPhxd+FYIWu1vYTKAXEssxIeSaZ3csxXk1N/wAE9P8Ag3qm/aX/AOCYvxL+HXxo8GN8J/iBrnxGuNUsPElxottd6zHp6/Z3RYnLZETsJVxuHVuOa/buG+G6WV0f5qj3f6Ly/M/y28bPG3MePcx0vSwVN/u6V/8Ayedt5teqitF1b/Pv/gq98R/iH4z/AGg/hBrfjH4LXP7OvxP1aytxomm/DXybYazo9xI+LmMwuZBdl3eMKSPlABArn/8Agmv+0d4M/YMf4n/GG++K/jXSvi/8OvEzzeG/hpqj3As/HDNuhlOpmLIMqeZITlhh4xya+0f2L9F/bV/YR1LVrHX/ANjbVf2i9Y8Pa458KeNPF12kupaPYwgRQw2rv5jRQ4TzFVGXBkPHSvTP+Csn/BFDxP8A8FJP20v2a7zSPhT/AMKq8P8AjLw7NffEzWtA0y2P/CP6nODcSC5wU86USfJuPJ3E5r6U/DT5w13/AIL6aF+xZo134m+H+saZ+0DqX7RkX/CQ/EHwf4xN3LpPw/vSd502zVgFeDdcTL/ENsEdfOn7GPxB+C/7Wf7R/wAZ/H/xj+OXiP8AZKPiq9g1LStJ8ARzxWF0JDJ5kIWFWISLCbQQP9YfpX6j6l/wTm1nxb/wTc/aO+FJ/ZI8K6R4u+Guhp4X+Hfi2PSbT+2fiIY98X9phggaOZxEkh+brNjNfBvhD/gzz/aT8efDrwxr1pq/gvQrnWtKhvL/AEjXrma2vtJuWzvt3Eccikrgcg9/agD0bwv8Hf8Agn3F8V/B3iXxX+3l8UPiFH4N1i31eDS/Eun3V/ZXDRSI5jZZLdgEcLtbHUGvT/g5/wAFBPgj8L/+C1f7RE95rPhLXfAd/wCHLGP4TaBLatdaFNrbR2jQR2cCo0Vq8koIaRVTBkYlhk18c/Gr/g00/ay+Emr+FbWx0vwz41TxLqI0+efQr95ItEBKj7RdGREKw/MSWUMRtPHTPqn7AX/Bqv8AHPSv+ChEWm/E4jwp4M+Hlzaa7F4p01ReWWvTRyxSxwWu4q2Schi6jbtPFAH78/sf/H34i+I/2NLHx1+0L4S0j4S+MbWLULvxDpUV2stpo9rbzzBJjJ5kg2m3RJSd5xuPTpX4Kf8ABQj9r68/bh/an8QeOZRPDpBI07QbSU82mnQlvJUj+F3LPK47PM46AV9h/wDBcz/gqfB8Yr+6+DPw71JLjwxYXH/FUarayBo9WuI2yLKJgeYI3UM7Dh5FCj5Ubf8AmgPlFfjvHXEMcTP6hh37kfifd9vRfn6H+kn0T/Bytk+FfFuc0+WtXjalFrWFN7ya6Snpbqof4mkOwRCWIVQMknoBX9C3/BFr9k6X9lf9h3QIdTtWtfEnjJ/+El1eORcSQPOiiCFgRkFIEiDKejmT1r8n/wDgjl+wrL+2h+1PZ3WrWfneBPAskWq66zrmK7cMTb2R7HzXUlx/zyjkHBZc/wBCqLtHQZ74r0vDzJ3GMsxqLfSP6v8AT7z4j6ZHiVTxFahwbgp39m1UrW/mt+7g/RNza84C4opaK/UD+Egriv2gfgN4e/aW+EOveCPFVkt9oHiK2Ntcxg7ZI+QySxtg7ZI3VXRscMintXa0VM4RnFwkrpm2HxFXD1Y16EnGcWmmnZpp3TT6NPVH8xv7a/7HPif9hv48ah4K8SIZ4Rm50jVEj2Q6zZliEnQc4P8AC6ZJRwRyNrHyWv6Yf25f2GfB/wC3f8HZ/C3imEW9xAWuNI1eGMG60W5IwJY/7ynADxk7XXg4IVl/n1/bD/Yy8c/sP/FWXwv4208R+aXk0zU7cE2OswKcebC57jjdGfnQkbhgqT+G8VcLVMvqOvRV6T/Dyf6M/wBWfo/+PuE4xwUcrzSahmFNarZVUvtw8/5o9Hqvd28+8B2Wial420i38S3+oaV4emvIk1O8sbYXNza2xcCR44yQHcLkgZ69j0P9Jv7CukfCjQP2bdBs/gvdaVfeA4I9trPYS+YZpesjTsQH+0E8yCQBweCFwAP5nOorv/2d/wBqX4g/sn+M2174feKdT8OX8u0XCQsJLW+UdFngcGKUcnG9SVydpB5rDhXiKlldWXtafMpdV8S/4Hlp6nofSC8F8fx7gqTy/GunOjdqnL+DN95WV1Lope8ktOVXbP6jAOOlAXB6V+Uf7MP/AAcq6ddW9vp/xf8ABNzY3PCNrPhlvOt3/wBp7WZ98YHfZJIT2UdK+3/hF/wVB+AnxxgjOgfFTweLiXGyz1K8Gl3bH0ENz5chP0Br9jwOf5fi43oVV6PR/c9T/Nfivwj4w4cqOGa4CpGK+3GLnB/9vxuvk2n5Hvu0elAXHaqema/aa3aiezuILyFhkSQSrIhH1BIqW51GOyhMkxWFFGSzsFA/E16/MrXPzlpp8r3JyoPagnaPSvGfi3/wUJ+CnwOWX/hJvij4K06WH71pHqcd1eD6W8JeU/glfFn7Tf8AwcneCvDNtNY/Cnwpqni7UMFV1PWVOn6anoyxf8fEo/2WEX+9XmY3O8DhFevVS8r3f3LU+84X8L+K+IqihlGAqTT+1yuMF6zlaK+8/SLx3490b4aeEr7Xdf1TT9G0bTImnu76+nWC3tox1Z3YhVH1Nfjl/wAFSf8Agudd/Gqz1T4e/Bua80vwpcBrbVPErBoLzWYyCrRWynDQQEdXOJHBIAjXO/41/as/bq+KP7aXiBbzx/4oudQs4H8y00i2X7NpdiecGO3U43AEjzHLSYOC5FeRdK/L+IOOqmJi6GAvGD3f2n/l+Z/dng/9E7B5JVhm/Fko168bONNa0oPo5X/iST20UV2loxEQIoAAGBgAdq7H4CfAjxR+018W9G8EeDdOOp6/rkvlwoTtjgQcvPK38EUa5Zm9BgAkgF/wB/Z78X/tQ/FHT/B3gjRrjWtc1A7hGg2xW0QIDTzSH5Y4lyMu3qAMsQp/fr/gmr/wTS8M/wDBP74aSQwvBrfjjWo0Ou66YtpmI5FvADzHboeg6uRub+FV8bhnhmtmdbnnpSW77+S8/wAj9N8cvHTLuBsvlhcK1Ux1Rfu6e/Lf7c+0V0W8notLtd3+xD+xr4e/Yg/Z+0rwP4fxcGHN1qeovHsl1a9cDzbhx2zgKq5OxFRcnGT7JQOBRX7zRowpU1Spq0UrJeR/ktmWY4nMMXUx2Nm51ajcpSe7b1bCiiitDiCiiigAIzXCftDfs4+D/wBqL4bXfhPxvodnr2hXh3mGYFZIJACFlhkXDxSLk4dCGGTzgkHu6KipTjUi4zV0+hvhsVWw1aOIw83CcWmpRbTTWzTWqaPwr/bu/wCCC/xC/Z7vL3XvhmL34j+DFJl+yxRA65pqZ6PCoAuQOPnhXcecxKBk/BEsT208kUqtHLC5jkRxhkYHBUg8gg9Qa/rJMSn+Ee3HSvAv2q/+CZvwb/bDWa78Z+D7M67Iu0a5prGx1NSOAWmjx5uB0WUOo/u1+dZx4fUqr9pgJcj/AJXt8nuvx+R/aHhr9MPMsvpwwPF1J4iC09rCyqW/vRdoz9U4v1Z/NlSMu5cV+pPx8/4NoNb06eW5+GXxG0/UYmyYtO8TWzW0qY7faoFZXP8A2wT618jfGv8A4JL/AB8+AVrJd6/4Mtv7LQ4W/tdasZYZPcKZhL+aCvz/ABnC+aYa/tKLsuq1X4H9gcN+PnAWepLCZjTjN/ZqP2cvS07X+V0fONix05y0DPAT3iOw/pT7+6l1MYuJZbgD/nq5f+dO1DTptKu2huE2SJ1GQcflTLa3e8uEijGXc4UZ6mvH/ex9x39D9SUcFKHt0o23vp+ZGiBFwAAPQUte7fA3/gmh8bf2jkWTwj4MGo2pwWuH1exgSMHnJEk6v+Smvrr4G/8ABtP448RXSS/EPx/4e8NWoKs9pokEmp3TrnlfMkEUcZ9wJB7GvVwXDeZYuzo0nZ9XovvZ+b8SeOPA2Qxax2Y0+aP2YP2kv/AYczXzsj8zZHCISSAB1JOAK+xP2Fv+CLnxT/bClstY1e1m+H3gWbbIdV1O2YXd7GcH/RbY4dwQRiR9seDlS+NtfrP+yf8A8Ehfgh+yVe22p6L4X/4SDxJa7Wj1rxDIL+6jYcho1KiGFv8AaijRvc19PR2+0ncFP4V97k/h5CDVXMJX/urb5v8Ay+8/kXxI+mTicVCWD4OoOknp7apbm/7dgrpeTk3/AIUeS/sifsSeAP2KPhz/AMI94E0gWaz7X1DULlvOv9WlUHEk8uBuIycKAEXJ2qoOK9eAxS0V+lUqUKUFTpq0VskfxJmOY4rH4qeNx1SVSrN3lKTbbfdthRRRWhxhRRRQB//Z")));
                        }
                    }
                    if (Config.DynamicDataItems.ContainsKey("CredentialPrinterName"))
                    {
                        try
                        {
                            _PrinterName = (string)Config.DynamicDataItems["CredentialPrinterName"];
                        }
                        catch (Exception ex)
                        {
                            LOG.Warn("ActionBancoFalabella.Initialize - Error cargando nombre impresora!");
                            _PrinterName = "ZDesigner ZD220-203dpi ZPL";
                        }
                    }
                    if (Config.DynamicDataItems.ContainsKey("CredentialFooter"))
                    {
                        try
                        {
                            _FooterMsg = (string)Config.DynamicDataItems["CredentialFooter"];
                        }
                        catch (Exception ex)
                        {
                            LOG.Warn("ActionBancoFalabella.Initialize - Error cargando Msg Footer!");
                            _FooterMsg = "Dirijase al Piso 17 para anunciarse!";
                        }
                    }
                }

                if (Config == null)
                {
                    LOG.Fatal("ActionBancoFalabella.Initialize - Error leyendo BancoFalabella.cfg!");
                    return Errors.IERR_DESERIALIZING_DATA;
                } else //Si hay Config => Configuro Placa
                {
                    //string com = (Config.DynamicDataItems.ContainsKey("COMPort") ?
                    //                (string)Config.DynamicDataItems["COMPort"] : null);
                    //_ARDUINO_HELPER = new ArduinoHelper((string)Config.DynamicDataItems["ArduinoOpenDelay"], com);
                    ret = 0; // _ARDUINO_HELPER.Initialize(); 
                }
                _Initialized = true;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ActionBancoFalabella.Initialize - Excp Error: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// La accion en este caso es la impresión de la credencial de acceso
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="returns"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int DoAction(DynamicData parameters, out DynamicData returns, out string msg)
        {
            int ret = Errors.IERR_OK;
            returns = null;
            msg = null;
            try
            {
                //object o = (parameters != null) ? parameters.ParamsList["key"] : null;
                if (parameters == null)
                    LOG.Fatal("ActionBancoFalabella.DoAction - Parametros NULO!");
                else
                {
                    string rele = ""; ;
                    LOG.Debug("ActionBancoFalabella.DoAction - PrintEventType = " + 
                                    ((int)Config.DynamicDataItems["PrintEventType"]).ToString() + " / "   
                              + "EventType = " + ((int)parameters.DynamicDataItems["EventType"]).ToString() + "...");

                    /*
                        Los valores posibles de PrintEventType: 
                                0 - Deshabilitado siempre para imprimir
                                1 - Habilitado para imprimri solo si es el primer registro
                                2 - Imprime siempre sin importar que tipo de consulta es
                    */
                    if (((int)Config.DynamicDataItems["PrintEventType"]) == 2 ||
                        (((int)parameters.DynamicDataItems["EventType"]) == 1 &&
                         ((int)Config.DynamicDataItems["PrintEventType"]) == 1))
                    {
                        LOG.Debug("ActionBancoFalabella.DoAction - Printing...");
                        THPrint_Credential(parameters);
                        LOG.Debug("ActionBancoFalabella.DoAction - Impreso!");
                    } else
                    {
                        LOG.Debug("ActionBancoFalabella.DoAction - No imprime!");
                    } 
                    
                    //else if (((string)parameters.DynamicDataItems["EventType"]).Equals("0"))   //Si es primer registro de la visitas si o si imprime
                    //{
                    //    //Esto indica que esta habilitado a imprimir si es el registro de la visita
                    //    if (((string)Config.DynamicDataItems["PrintEventType"]).Equals("1")) 
                    //    {
                    //        LOG.Debug("ActionBancoFalabella.DoAction - Printing...");
                    //        THPrint_Credential(parameters);
                    //    } else
                    //    {
                    //        LOG.Debug("ActionBancoFalabella.DoAction - No imporme => PrintEventType = " + 
                    //                    (string)Config.DynamicDataItems["PrintEventType"]);
                    //    }
                    //} 
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msg = "ActionBancoFalabella.DoAction  Excp [" + ex.Message + "]";
                LOG.Error("ActionBancoFalabella.DoAction  - Excp Error: " + ex.Message);
            }
            LOG.Debug("ActionBancoFalabella.DoAction - OUT!");
            return ret;

        }

        private void THPrint_Credential(DynamicData parameters)
        {
            try
            {
                LOG.Debug("ActionBancoFalabella.THPrint_Credential IN...");
                lock (_LockObject)
                {
                    try
                    {
                        //LOG.Debug("ActionBancoFalabella.THPrint_Credential Printer = " + (string)Config.DynamicDataItems["CredentialPrinterName"]);
                        Credencial _Credential = new Credencial();
                        _Credential.FontSize = 10;
                        _Credential.HeaderImage = _LogoCredential;

                        //_Credential.PhotoImage = bmps[1];
                        //Titulo
                        string aux = ""; 
                        if (parameters.DynamicDataItems.ContainsKey("MeetName"))
                        {
                            aux = "Evento: " + (string)parameters.DynamicDataItems["MeetName"];
                        } else
                        {
                            aux = "Evento: Reunion";
                        }
                        _Credential.AddBodyLine(aux);
                        LOG.Debug(aux);
                        
                        //Fecha/Hora
                        aux = "";
                        if (parameters.DynamicDataItems.ContainsKey("MeetDate") &&
                            parameters.DynamicDataItems.ContainsKey("MeetStartTime"))
                        {
                            aux = "Inicio: " + (string)parameters.DynamicDataItems["MeetDate"] + " "
                                             + (string)parameters.DynamicDataItems["MeetStartTime"];
                        }
                        else
                        {
                            aux = "Inicio: Desconocido";
                        }
                        _Credential.AddBodyLine(aux);
                        LOG.Debug(aux);

                        //RUT
                        _Credential.AddBodyLine("Rut: " + (string)parameters.DynamicDataItems["idRecognized"]);
                        LOG.Debug("Rut: " + (string)parameters.DynamicDataItems["idRecognized"]);

                        //Nombre Invitado
                        aux = "";
                        if (parameters.DynamicDataItems.ContainsKey("Name"))
                        {
                            aux = "Nombre: " + (string)parameters.DynamicDataItems["Name"];
                        }
                        else
                        {
                            aux = "Nombre: Desconocido";
                        }
                        _Credential.AddBodyLine(aux);
                        LOG.Debug(aux);

                        //Autorizador
                        aux = "";
                        if (parameters.DynamicDataItems.ContainsKey("MeetAuthorizer"))
                        {
                            aux = "Autoriza: " + (string)parameters.DynamicDataItems["MeetAuthorizer"];
                        }
                        else
                        {
                            aux = "Autoriza: Desconocido";
                        }
                        _Credential.AddBodyLine(aux);
                        LOG.Debug(aux);

                        //Unidad
                        aux = "";
                        if (parameters.DynamicDataItems.ContainsKey("MeetUnity"))
                        {
                            aux = "Unidad: " + (string)parameters.DynamicDataItems["MeetUnity"];
                        }
                        else
                        {
                            aux = "Unidad: Desconocida";
                        }
                        _Credential.AddBodyLine(aux);
                        LOG.Debug(aux);

                        //Imagen QR
                        if (parameters.DynamicDataItems.ContainsKey("ImageQR"))
                        {
                            _Credential.FooterImage = 
                                Utils.SetImageFromB64((string)parameters.DynamicDataItems["ImageQR"]);
                        }
                        else
                        {
                            _Credential.FooterImage = _NoQRCredential;
                        }
                        
                        _Credential.AddFooterLine((string)Config.DynamicDataItems["CredentialFooter"]);
                        //LOG.Debug("Footer: " + (string)Config.DynamicDataItems["CredentialFooter"]);

                        //_Credential.FooterImage = Utils.SetImageFromB64((string)parameters.DynamicDataItems["QR"]);
                        
                        LOG.Debug("Printing en " + _PrinterName + "...");
                        _Credential.PrintCredencialBF(_PrinterName); //("Microsoft Print to PDF");
                        
                        _Credential = null;
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("ActionBancoFalabella.THPrint_Credential Error (lock) " + ex.Message);
                        //responsews = null;
                        //cedulaCurrent = null;
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("ActionBancoFalabella.THPrint_Credential Error " + ex.Message);
            }
            LOG.Debug("ActionBancoFalabella.THPrint_Credential OUT!");
        }
    }
}
