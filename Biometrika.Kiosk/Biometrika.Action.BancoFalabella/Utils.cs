﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Action.BancoFalabella
{
    internal class Utils
    {
        internal static Image SetImageFromB64(string base64Photo)
        {
            Image ret = null;
            try
            {
                if (!string.IsNullOrEmpty(base64Photo))
                {
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(base64Photo));
                    ret = Image.FromStream(ms);
                    ms.Close();
                }
            }
            catch (Exception ex)
            {
                ret = null;
                //LOG.Error("BusinessRuleSportlifeNorte.SetImageFromB64 Excp Error: " + ex.Message);
            }
            return ret;
        }
    }
}
