﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BusinessRule.ClubMedico
{
    /// <summary>
    /// Para parsear respuesta desde WAIS.
    /// </summary>
    public class AccessModelRL
    {
        public string Status { get; set; }
        public string Data { get; set; }
        public string Message { get; set; }
        public string ReadableMessage { get; set; }
    }

    public class AccessModelR
    {
        public string Status { get; set; }
        public Data Data { get; set; }
        public string Message { get; set; }
        public string ReadableMessage { get; set; }

    }

    public class Data
    {
        public int CanAccess { get; set; }
        public Identity Identity { get; set; }
        public string Event { get; set; }
    }

    public class Identity
    {
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string Nombres { get; set; }
        public string ApePat { get; set; }
        public string ApeMat { get; set; }
        public DateTime NacDate { get; set; }
        public string Foto { get; set; }
    }
    
   

    class Models
    {
    }
}
