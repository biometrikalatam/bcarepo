﻿using BioArduinoRele;
using Biometrika.Kiosk.Common.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Action.GrupoEsmeralda.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class ArduinoHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ArduinoHelper));

        bool _Initialized;
        ArduinoRele _ARDUINO;
        string _ArduinoDeviceId;
        string _ArduinoDelay;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_delay"></param>
        public ArduinoHelper(string _delay)
        {
            try
            {
                LOG.Debug("ArduinoHelper.Constructor IN...");
                _ArduinoDelay = _delay;
                LOG.Debug("ArduinoHelper.Constructor _ArduinoDelay = " + _ArduinoDelay.ToString());
                _Initialized = false;
            }
            catch (Exception ex)
            {
                _Initialized = false;
                LOG.Error("ArduinoHelper.Constructor Excp Error: " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int Initialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("ArduinoHelper.Initialize IN...");

                LOG.Debug("ArduinoHelper.Initialize Creo objeto...");
                _ARDUINO = new ArduinoRele();
                LOG.Debug("ArduinoHelper.Initialize - getCOM call...");
                _ArduinoDeviceId = _ARDUINO.getCOM();
                LOG.Debug("ArduinoHelper.Initialize - _ArduinoDeviceId = " +
                    (string.IsNullOrEmpty(_ArduinoDeviceId) ? "Null" : _ArduinoDeviceId));
                _Initialized = !string.IsNullOrEmpty(_ArduinoDeviceId);  //Si devuelve valor => configuro ok
                LOG.Debug("ArduinoHelper.Initialize -_Initialized = " + _Initialized.ToString());
                ret = _Initialized ? 0 : -2;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ArduinoHelper.Initialize Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.Initialize OUT! => ret = " + ret.ToString());
            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rele"></param>
        /// <param name="delay"></param>
        /// <returns></returns>
        public int OpenWithDelay(string rele, string delay)
        {
            int ret = Errors.IERR_OK;
            string msgErr;
            try
            {
                LOG.Debug("ArduinoHelper.OpenWithDelay IN...");

                LOG.Debug("ArduinoHelper.OpenWithDelay - Call OpenDelay method...");
                _ARDUINO.openDelay(rele, delay, out msgErr);
                if (string.IsNullOrEmpty(msgErr))
                {
                    LOG.Debug("ArduinoHelper.OpenWithDelay - Abrio rele = " + rele + " por " + delay + " milisegundos...");
                    ret = Errors.IERR_OK;
                } else
                {
                    LOG.Debug("ArduinoHelper.OpenWithDelay - NO Abrio rele = " + rele + " [" + msgErr + "]");
                    ret = Errors.IERR_HANDLE_RELE;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("ArduinoHelper.OpenWithDelay Excp Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.OpenWithDelay OUT! => ret = " + ret.ToString());
            return ret;
        }

        public int OpenAllWithDelay(int qReles, string delay)
        {
            int ret = 0;
            string strRele;
            string msgErr;
            try
            {
                LOG.Debug("ArduinoHelper.OpenAllWithDelay IN...");

                LOG.Debug("ArduinoHelper.OpenAllWithDelay Creo objeto...");
                //for (int i = 1; i <= qReles; i++)
                //{
                    msgErr = null;
                    //strRele = "0" + i.ToString() + "0";
                //_ARDUINO.openDelay(strRele, delay, out msgErr);
                _ARDUINO.openCloseAllDelay(delay, out msgErr);
                if (string.IsNullOrEmpty(msgErr))
                {
                    LOG.Debug("ArduinoHelper.OpenAllWithDelay - Abrio reles por " + delay + " milisegundos...");
                    ret = ret + Errors.IERR_OK;
                }
                else
                {
                    LOG.Debug("ArduinoHelper.OpenAllWithDelay - NO Abrio reles [" + msgErr + "]");
                    ret = ret + Errors.IERR_HANDLE_RELE;
                }
                //}
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ArduinoHelper.OpenAllWithDelay Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.OpenAllWithDelay OUT! => ret = " + ret.ToString());
            return ret;
        }


    }
}
