﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Kiosk.UI.Dummy
{
    public partial class frmDBDummy : Form
    {
        List<Persona> _LIST_PERSONAS = null;
        int _LAST_ID = 0;
        BindingSource bs = new BindingSource();

        public frmDBDummy()
        {
            InitializeComponent();
        }

        private void frmDBDummy_Load(object sender, EventArgs e)
        {
            try
            {
                System.IO.File.Copy(Application.StartupPath + @"\Data\DatabaseDummy.json",
                                    Application.StartupPath + @"\Data\DatabaseDummy.json." +
                                    DateTime.Now.ToString("yyyyMMddHHmmss"));

                string json = System.IO.File.ReadAllText(Application.StartupPath + @"\Data\DatabaseDummy.json");
                _LIST_PERSONAS = JsonConvert.DeserializeObject<List<Persona>>(json);
                bs.DataSource = _LIST_PERSONAS;
                dgView.DataSource = bs;
                dgView.EditMode = DataGridViewEditMode.EditOnEnter;
                _LAST_ID = _LIST_PERSONAS.Max<Persona>(p => p.Id);
                ClearForm();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                System.IO.File.Copy(Application.StartupPath + @"\Data\DatabaseDummy.json",
                                    Application.StartupPath + @"\Data\DatabaseDummy.json." +
                                    DateTime.Now.ToString("yyyyMMddHHmmss"));
                List<Persona> list = new List<Persona>();
                Persona pItem;
                foreach (DataGridViewRow item in dgView.Rows)
                {
                    if (!string.IsNullOrEmpty((string)item.Cells[1].Value))
                    {
                        pItem = new Persona(Convert.ToInt32(item.Cells[0].Value),
                                            (string)item.Cells[1].Value,
                                            (string)item.Cells[2].Value,
                                            (string)item.Cells[3].Value,
                                            (string)item.Cells[4].Value,
                                            (string)item.Cells[5].Value,
                                            Convert.ToBoolean(item.Cells[6].Value));
                        list.Add(pItem);
                    }
                }
                string json = JsonConvert.SerializeObject(list);
                System.IO.File.WriteAllText(Application.StartupPath + @"\Data\DatabaseDummy.json", json);

                _LIST_PERSONAS = list;
                bs.DataSource = null;
                bs.DataSource = _LIST_PERSONAS;
                dgView.DataSource = null;
                dgView.DataSource = bs;
                dgView.Update();
                dgView.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error grabando base de datos! [" + ex.Message + "]", "Atención...",
                                    MessageBoxButtons.OK,MessageBoxIcon.Hand);
                this.Close();
            }
        }


        int index = -1;

        private void dgView_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (index != -1)
            {
                DialogResult dr = MessageBox.Show(this, "Elimina el Rut = " + ((string)dgView.Rows[index].Cells[1].Value) + " ? ",
                                                  "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    foreach (Persona item in _LIST_PERSONAS)
                    {
                        if (item.Rut.Equals(((string)dgView.Rows[index].Cells[1].Value)))
                        {
                            _LIST_PERSONAS.Remove(item);
                            break;
                        }
                    }
                    bs.DataSource = null;
                    bs.DataSource = _LIST_PERSONAS;
                    dgView.DataSource = null;
                    dgView.DataSource = bs;
                    dgView.Update();
                    dgView.Refresh();
                    dgView.EditMode = DataGridViewEditMode.EditOnEnter;
                    _LAST_ID = _LIST_PERSONAS.Max<Persona>(p => p.Id);
                }
            }
        }


        private void btnBorrar_Click(object sender, EventArgs e)
        {
            ClearForm();
        }

        private void ClearForm()
        {
            txtId.Text = null;
            txtFoto.Text = null;
            txtName.Text = null;
            txtRut.Text = null;
            txtFN.Text = null;
            chkCode.Checked = false;
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            try
            {
                _LAST_ID++;
                Persona pItem = new Persona(_LAST_ID, "", "", "", "", "", false);
                _LIST_PERSONAS.Add(pItem);
                bs.DataSource = null;
                bs.DataSource = _LIST_PERSONAS;
                dgView.DataSource = null;
                dgView.DataSource = bs;
                dgView.Update();
                dgView.Refresh();
                dgView.EditMode = DataGridViewEditMode.EditOnEnter;
                _LAST_ID = _LIST_PERSONAS.Max<Persona>(p => p.Id);
                ClearForm();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
