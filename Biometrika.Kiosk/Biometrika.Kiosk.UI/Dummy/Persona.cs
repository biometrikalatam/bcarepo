﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.UI.Dummy
{
    internal class Persona
    {
        public Persona() { }

        public Persona(int _Id, string _Rut, string _Name, string _Photografy,
                        string _Finger, string _DateOfBirth, bool _Code)
        {
            Id = _Id;
            Rut = _Rut;
            Name = _Name;
            Photografy = _Photografy;
            Finger = _Finger;
            DateOfBirth = _DateOfBirth;
            Code = _Code;
        }


        public int Id { get; set; }
        public string Rut { get; set; }
        public string Name { get; set; }
        public string Photografy { get; set; }
        public string Finger { get; set; }
        public string DateOfBirth { get; set; }
        public bool Code { get; set; } //Puede acceder o no

    }
}
