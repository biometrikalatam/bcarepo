﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.UI.src.Helpers
{
    /// <summary>
    /// Parea datos leídos desde un lector de MRZ. Normalmente es cédula chilena. 
    /// Si fuera otra o pasaporte hay que revisar que la paresee bien.
    /// </summary>
    public class MRZHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(MRZHelper));

        public static int ParseMRZ(string MRZReaded, out CedulaTemplate2 cedula, out string id)
        {
            cedula = null;
            id = null;
            int ret = 0;
            try
            {
                cedula = new CedulaTemplate2(MRZReaded);
                if (cedula != null && cedula.IsValid)
                {
                    id = cedula.Rut;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }

    }

    public class CedulaTemplate2
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CedulaTemplate2));

        #region Variables privadas

        private string _MRZ;

        private string _tipoDocumento;
        private string _emision;
        private string _rut;
        private string _digV;
        private string _serie;
        private string _fechaNacimiento;
        private string _sexo;
        private DateTime _fechaVencimiento;
        private string _nacionalidad;
        private string _challa2;
        private string _challa3;
        private string _apePaterno;
        private string _apeMaterno;
        private string _nombre;
        private string _segNombre;
        private bool _esValida;
        private bool _vigente;

        #endregion

        #region Constructores

        /// <summary>
        /// Constructor de Cedula Template
        /// </summary>
        /// <param name="lectura">String de lectura OCR</param>
        public CedulaTemplate2(string lectura)
        {
            lectura = CleanMRZ(lectura);

            // Formateamos lo que llego para que quede con el largo y los caracteres correspondientes
            int maxLength = 92;

            log.Debug("CedulaTemplate2 IN...");
            log.Debug("MRZ = " + lectura);
            log.Debug("Largo: " + lectura.Length);

            try
            {
                _esValida = false;
                _vigente = false;

                if (lectura.Trim().Length < 92)
                {
                    return;
                }

                _MRZ = lectura;
                log.Debug("MRZ : " + Environment.NewLine + _MRZ);

                //Separo las lineas
                string[] lineas = lectura.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);


                string[] nacionalidadesArray = { "CHL", "ARG", "PER" };

                // primera linea
                string[] primeraLinea = lineas[0].Split(new char[] { '<' }, StringSplitOptions.RemoveEmptyEntries);

                //Tipo Documento
                if (primeraLinea[0].Substring(0, 1).Equals("P"))
                    _tipoDocumento = "Pasaporte";
                else if (primeraLinea[0].Substring(0, 1).Equals("I"))
                    _tipoDocumento = "Cedula";

                //Pais emisor
                _emision = primeraLinea[0].Substring(2, 3); //BuscarPais(nacionalidadesArray, primeraLinea[0]);

                //Serie Documento y RUT
                if (lineas[0].Substring(0, 2).Equals("IE") || lineas[0].Substring(0, 2).Equals("IN")) //Si es cedula nueva esta en la primera linea
                {
                    _serie = lineas[0].Substring(5, 9);
                    _rut = lineas[1].Substring(18, 10);
                }
                if (lineas[0].Substring(0, 2).Equals("ID"))
                { //Si es cedula vieja esta en la segunda linea
                    _serie = lineas[1].Substring(18, 10);
                    _rut = lineas[0].Substring(5, 9);
                }
                _rut = _rut.Replace("<", "");
                //Separo Rut y DV
                _digV = _rut.Substring(_rut.Length - 1);
                _rut = _rut.Substring(0, _rut.Length - 1);
                if (_digV.CompareTo(Dv(_rut)) == 0)
                {
                    _esValida = true;
                }
                else
                {
                    _esValida = false;
                }

                //fecha Nacimiento
                _fechaNacimiento = lineas[1].Substring(4, 2) + "/" + lineas[1].Substring(2, 2) + "/" + lineas[1].Substring(0, 2);
                //Sexo
                _sexo = lineas[1].Substring(7, 1);

                //fecha Venicimiento
                _fechaVencimiento = new DateTime(Convert.ToInt32(lineas[1].Substring(8, 2)) + 2000, Convert.ToInt32(lineas[1].Substring(10, 2)), Convert.ToInt32(lineas[1].Substring(12, 2)));

                if (DateTime.Now < _fechaVencimiento)
                {
                    _vigente = true;
                }

                //Nacionalidad
                _nacionalidad = lineas[1].Substring(15, 3); //BuscarPais(nacionalidadesArray, lecturaArray[1]);

                //int posNac = segundaLinea[0].IndexOf(_nacionalidad);

                //if (_tipoDocumento.CompareTo("ID") == 0 || _tipoDocumento.CompareTo("DC") == 0)
                //{
                //    #region Rut
                //    // primera linea
                //    if (primeraLinea.Length > 1)
                //    {
                //        // rut menor a 10millones
                //        string rutLeido = primeraLinea[0].Substring(5, 8);
                //        string digVLeido = rutLeido.Substring(rutLeido.Length - 1, 1);

                //        _rut = rutLeido.Substring(0, rutLeido.Length - 1);
                //        if (digVLeido.CompareTo(Dv(_rut)) == 0)
                //        {
                //            _digV = digVLeido;
                //            _esValida = true;
                //        }
                //        else
                //        {
                //            _digV = Dv(_rut);
                //            _esValida = false;
                //        }

                //        _challa1 = primeraLinea[1];
                //    }
                //    else if (primeraLinea.Length == 1)
                //    {
                //        string rutLeido = primeraLinea[0].Substring(5, primeraLinea[0].Length - 9);
                //        string digVLeido = rutLeido.Substring(rutLeido.Length - 1, 1);

                //        _rut = rutLeido.Substring(0, rutLeido.Length - 1);
                //        if (digVLeido.CompareTo(Dv(_rut)) == 0)
                //        {
                //            _digV = digVLeido;
                //            _esValida = true;
                //        }
                //        else
                //        {
                //            _digV = Dv(_rut);
                //            _esValida = false;
                //        }

                //        _challa1 = primeraLinea[0].Substring(primeraLinea[0].Length - 4, 4);
                //    }
                //    #endregion

                //    _challa2 = segundaLinea[0].Substring(posNac + 3, segundaLinea[0].Length - (posNac + 3));
                //    _challa3 = segundaLinea[1];
                //}
                //else if (_tipoDocumento.CompareTo("IN") == 0 || _tipoDocumento.CompareTo("NC") == 0 || _tipoDocumento.CompareTo("IE") == 0)
                //{
                //    // primera linea
                //    int posEmi = primeraLinea[0].IndexOf(_emision);
                //    _challa1 = primeraLinea[0].Substring(posEmi + 3, primeraLinea[0].Length - (posEmi + 3));

                //    // segunda linea
                //    int posNacIN = segundaLinea[0].IndexOf(_nacionalidad);

                //    _rut = segundaLinea[0].Substring(posNacIN + 3, segundaLinea[0].Length - (posNacIN + 3));
                //    string digVLeido = segundaLinea[1];

                //    if (digVLeido.CompareTo(Dv(_rut)) == 0)
                //    {
                //        _digV = digVLeido;
                //        _esValida = true;
                //    }
                //    else
                //    {
                //        _digV = Dv(_rut);
                //        _esValida = false;
                //    }

                //    _challa2 = segundaLinea[2];

                //    int algoo = 19;
                //}

                // tercera linea
                string[] ApyNom = lineas[2].Split(new string[] { "<<" }, StringSplitOptions.RemoveEmptyEntries);
                string[] Apellidos = ApyNom[0].Split(new string[] { "<" }, StringSplitOptions.RemoveEmptyEntries);
                _apePaterno = Apellidos[0];
                if (Apellidos.Length > 1)
                    _apeMaterno = Apellidos[1];

                string[] Nombres = ApyNom[1].Split(new string[] { "<" }, StringSplitOptions.RemoveEmptyEntries);
                _nombre = Nombres[0];
                if (Nombres.Length > 1)
                {
                    _segNombre = "";
                    bool first = true;
                    for (int i = 1; i < Nombres.Length; i++)
                    {
                        if (first)
                        {
                            _segNombre = Nombres[i];
                            first = false;
                        }
                        else
                        {
                            _segNombre = _segNombre + " " + Nombres[i];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }

        #endregion

        #region Get

        public string TipoDocumento
        {
            get { return _tipoDocumento; }
        }

        public string Emision
        {
            get { return _emision; }
        }

        public string Rut
        {
            get { return _rut + "-" + _digV; }
        }

        public string Mantisa
        {
            get { return _rut; }
        }

        public string DigV
        {
            get { return _digV; }
        }

        public string Serie
        {
            get { return _serie; }
        }

        public string FechaNacimiento
        {
            get { return _fechaNacimiento; }
        }

        public string Sexo
        {
            get { return _sexo; }
        }

        public DateTime FechaVencimiento
        {
            get { return _fechaVencimiento; }
        }

        public string Nacionalidad
        {
            get { return _nacionalidad; }
        }

        public string Challa2
        {
            get { return _challa2; }
        }

        public string Challa3
        {
            get { return _challa3; }
        }

        public string ApePaterno
        {
            get { return _apePaterno; }
        }

        public string ApeMaterno
        {
            get { return String.IsNullOrEmpty(_apeMaterno) ? "" : _apeMaterno; }
        }

        public string Nombre
        {
            get { return _nombre; }
        }

        public string SegNombre
        {
            get { return String.IsNullOrEmpty(_segNombre) ? "" : _segNombre; }
        }

        public bool Valida
        {
            get { return _esValida; }
        }

        public bool Vigente
        {
            get { return _vigente; }
        }

        #endregion

        private string BuscarPais(string[] nacionalidadToSearch, string stringToSearch)
        {
            string pais = "";

            for (int i = 0; i < nacionalidadToSearch.Length; i++)
            {
                if (stringToSearch.Contains(nacionalidadToSearch[i]))
                {
                    pais = nacionalidadToSearch[i];
                    break;
                }
            }

            return pais;
        }

        private static string Dv(string rut)
        {
            int suma = 0;

            for (int x = rut.Length - 1; x >= 0; x--)
            {
                suma += int.Parse(char.IsDigit(rut[x]) ? rut[x].ToString() : "0") * (((rut.Length - (x + 1)) % 6) + 2);
            }

            int numericDigito = (11 - suma % 11);
            string digito = numericDigito == 11 ? "0" : numericDigito == 10 ? "K" : numericDigito.ToString();

            return digito;
        }

        private string GARBAGESTART = "\u001c\u0002";
        private string GARBAGEEND = "\u0003\u001d";
        private string shit = "bOU0";

        private string CleanMRZ(string mrz)
        {
            mrz = mrz.Replace(GARBAGESTART, "");
            mrz = mrz.Replace(GARBAGEEND, "");
            mrz = mrz.Trim();

            mrz = mrz.Replace(" ", "");
            log.Info("MRZ sin limpiar: " + mrz);
            Regex rgx = new Regex("[^a-zA-Z0-9<]\r\n\r\n]");
            Regex rgx2 = new Regex("[\x00-\x09\x0B\x0C\x0E-\x1F\x7F]");
            mrz = rgx.Replace(mrz, "");
            mrz = rgx2.Replace(mrz, "");

            mrz = mrz.Replace(shit, "");
            log.Info("MRZ post limpieza: " + mrz);
            return mrz;
        }

        public bool IsValid
        {
            get {
                return _esValida;
            }
        }

        /*
         * 
        private string _serie;
        private string _fechaNacimiento;
        private string _sexo;
        private DateTime _fechaVencimiento;
        private string _nacionalidad;
        private string _challa2;
        private string _challa3;
        private string _apePaterno;
        private string _apeMaterno;
        private string _nombre;
        private string _segNombre;
        private bool _esValida;
        private bool _vigente;
         */
        internal string toXML()
        {
            return "<MRZ>" +
                       "<TipoDocumento>" + TipoDocumento + "</TipoDocumento>" +
                       "<Mantisa>" + Mantisa + "</Mantisa>" +
                       "<DV>" + DigV + "</Rut>" +
                       "<Rut>" + Rut + "</Rut>" +
                       "<Emisor>" + Emision + "</Emisor>" +
                       "<Serie>" + Serie + "</Serie>" +
                       "<FechaNacimiento>" + FechaNacimiento + "</FechaNacimiento>" +
                       "<Sexo>" + Sexo + "</Sexo>" +
                       "<FechaVencimiento>" + FechaVencimiento.ToString("dd/MM/yyyy") + "</FechaVencimiento>" +
                       "<Nacionbalidad>" + Nacionalidad + "</Nacionbalidad>" +
                       "<ApellidoPaterno>" + ApePaterno + "</ApellidoPaterno>" +
                       "<ApellidoMaterno>" + ApeMaterno + "</ApellidoMaterno>" +
                       "<Nombre>" + Nombre + "</Nombre>" +
                       "<SegundoNombre>" + SegNombre + "</SegundoNombre>" +
                "</MRZ>";
        }
    }
}
