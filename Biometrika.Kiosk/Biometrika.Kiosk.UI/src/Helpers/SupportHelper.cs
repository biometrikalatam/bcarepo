﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.UI.src.Helpers
{
    public class SupportHelper
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SupportHelper));

        static public string GetInfoKiosk()
        {
            string _SEP = "------------------------------------------" + Environment.NewLine;
            string ret = _SEP;

            try
            {
                ret += "Informacion General de Contexto" + Environment.NewLine + _SEP + _SEP;
                ret += FillDetails() + Environment.NewLine + _SEP + _SEP;
                ret += "Kiosk Config" + Environment.NewLine + _SEP;
                ret += ToStringFromProperty(Program._UOWKA._CONFIG) + _SEP;

                ret += "Action Engine Config" + Environment.NewLine + _SEP;
                ret += ToStringFromProperty(Program._UOWKA._ACTION_ENGINE) + _SEP;

                ret += "Business Rules Engine Config" + Environment.NewLine + _SEP;
                ret += ToStringFromProperty(Program._UOWKA._BUSINESSRULE_ENGINE) + _SEP;

                ret += "Mark Engine Config" + Environment.NewLine + _SEP;
                ret += ToStringFromProperty(Program._UOWKA._MARK_ENGINE) + _SEP;

            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }

        internal static string GetValueOfProperty(object obj, string property, string format = null)
        {
            string ret = null;
            try
            {
                log.Debug("Utils.GetValueOfPropert IN...");
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
                {
                    if (descriptor.Name.Equals(property))
                    {
                        object objValue = descriptor.GetValue(obj);
                        if (objValue is DateTime && !string.IsNullOrEmpty(format))
                        {
                            ret = ((DateTime)objValue).ToString(format);
                        }
                        else if (objValue is DateTime && !string.IsNullOrEmpty(format))
                        {
                            ret = ((double)objValue).ToString(format);
                        }
                        else
                        {
                            ret = descriptor.GetValue(obj).ToString();
                        }
                        log.Debug("Utils.GetValueOfPropert ret => property=" + property + " => value=" + ret);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                log.Error("Utils.GetValueOfProperty Excp Error: ", ex);
            }
            log.Debug("Utils.GetValueOfPropert OUT!");
            return ret;
        }

        internal static string ToStringFromProperty(object obj)
        {
            string ret = "";
            bool isFirst = true;
            object oAux = null;
            try
            {
                log.Debug("Utils.GetValueOfPropert IN...");
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
                {
                    oAux = descriptor.GetValue(obj);
                    if (oAux == null) oAux = "";
                    if (isFirst)
                    {
                        isFirst = false;
                        ret = descriptor.Name + " = " + oAux.ToString() + Environment.NewLine;
                    }
                    else {
                        ret += descriptor.Name + " = " + oAux.ToString() + Environment.NewLine;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = "Utils.GetValueOfProperty Excp Error: " + ex.Message;
                log.Error("Utils.GetValueOfProperty Excp Error: ", ex);
            }
            log.Debug("Utils.GetValueOfPropert OUT!");
            return ret;
        }

        internal static string FillDetails()
        {
            string sRet = "";
            int len = 0;
            try
            {
                Version version = Assembly.GetExecutingAssembly().GetName().Version;
                sRet = "Biometrika Kiosk: v" + version.ToString();
                sRet += Environment.NewLine + "-----------------------------------------------------------------------";
                sRet += Environment.NewLine;
                sRet += "Kiosk Name: " + Program._UOWKA._CONFIG.Name;
                sRet += Environment.NewLine;
                sRet += "Tecnologías Habilitadas:";
                sRet += Environment.NewLine;
                sRet += " > Fingerprint: " + (Program._UOWKA._CONFIG.AccessByFingerprintEnabled.ToString());
                sRet += Environment.NewLine;
                sRet += " > Barcode: " + (Program._UOWKA._CONFIG.AccessByBarcodeEnabled.ToString());
                sRet += Environment.NewLine;
                sRet += " > Facial: " + (Program._UOWKA._CONFIG.AccessByFacialEnabled.ToString());
                sRet += Environment.NewLine;
                sRet += " > MRZ: " + (Program._UOWKA._CONFIG.AccessByMRZEnabled.ToString());

                sRet += Environment.NewLine;
                sRet += "Kiosk Type: " + (Program._UOWKA._CONFIG.KioskType.ToString());

                sRet += Environment.NewLine;
                sRet += "Theme: " + (Program._UOWKA._CONFIG.Theme.ToString());
                sRet += "  Path Theme: " + Program._PATH_EXE + "\\Theme\\" + Program._UOWKA._CONFIG.Theme;

                sRet += Environment.NewLine;
                sRet += ("Path Logger Config: " + (Program._UOWKA._CONFIG.PathLoggerConfig.ToString()));

                sRet += Environment.NewLine;
                sRet += "Path Matcher Config: " + (Program._UOWKA._CONFIG.PathMatcherConfig.ToString());

                sRet += Environment.NewLine;
                sRet += "Time To Show Result: " + (Program._UOWKA._CONFIG.TimeToShowResult.ToString());

                sRet += Environment.NewLine;
                sRet += "Estados Engines:";
                sRet += Environment.NewLine;
                sRet += "  BusinessRules Engine: " + Program._UOWKA._BUSINESSRULE_ENGINE.Name;
                sRet += Environment.NewLine;
                sRet += "    Path: " + Program._UOWKA._BUSINESSRULE_ENGINE.Config.DynamicDataItems["BusinessRuleDllPath"].ToString();
                sRet += Environment.NewLine;
                sRet += "    Inicializado: " + Program._UOWKA._BUSINESSRULE_ENGINE.Initialized.ToString();

                sRet += Environment.NewLine;
                sRet += "  Action Engine: " + Program._UOWKA._ACTION_ENGINE.Name;
                sRet += Environment.NewLine;
                sRet += "    Path: " + Program._UOWKA._ACTION_ENGINE.Config.DynamicDataItems["ActionDllPath"].ToString();
                sRet += Environment.NewLine;
                sRet += "    Inicializado: " + Program._UOWKA._ACTION_ENGINE.Initialized.ToString();

                sRet += Environment.NewLine;
                sRet += "  Mark Engine: " + (Program._UOWKA._MARK_ENGINE != null ? Program._UOWKA._MARK_ENGINE.Name : "");
                sRet += Environment.NewLine;
                sRet += "    Path: " + (Program._UOWKA._MARK_ENGINE != null ? Program._UOWKA._MARK_ENGINE.Config.DynamicDataItems["MarkDllPath"].ToString() : "");
                sRet += Environment.NewLine;
                sRet += "    Inicializado: " + Program._UOWKA._MARK_ENGINE.Initialized.ToString();


            }
            catch (Exception ex)
            {
                sRet = "";
                log.Error("SupportHelper.FillDetails - Excp Error: " + ex.Message);
            }
            return sRet;
        }
    }
}
