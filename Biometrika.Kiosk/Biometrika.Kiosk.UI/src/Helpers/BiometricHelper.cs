﻿using Bio.Core.Api.Constant;
using Bio.Core.Matcher;
using Bio.Core.Matcher.Interface;
using Biometrika.BioApi20;
using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Utils;
using Biometrika.Kiosk.UI.src.Config;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Kiosk.UI.src.Helpers
{
    /// <summary>
    /// Inicaliza el Matcher y procesa las identificaciones de huella o facial segun se haya configurado.
    /// </summary>
    public class BiometricHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BiometricHelper));

        private static BiometricConfig _BIOMETRIC_CONFIG;

        internal IMatcher _MATCHER;

        public static List<ITemplate> objTemplates = new List<ITemplate>();

        internal static MatcherManager MATCHER_MANAGER;

        public static MatcherInstance matcherintance;
        public static IExtractor extractor;
        public static ITemplate templateCurrent;
        public static IMatcher matcher;

        /// <summary>
        /// Lle configuracion o crea archivo si no existe. Luego instancia y configura el 
        /// Matcher, y levanta los templates con el LoadBIR configurado.
        /// </summary>
        /// <returns></returns>
        public int Initalize()
        {
            int ret = 0;
            try
            {
                LOG.Info("BiometricHelper.Initalize - IN...");
                //Leo BiometricConfig
                if (!System.IO.File.Exists("BiometricConfig.cfg"))
                {
                    _BIOMETRIC_CONFIG = new BiometricConfig();
                    Common.Utils.SerializeHelper.SerializeToFile(_BIOMETRIC_CONFIG, "BiometricConfig.cfg");
                }
                else
                {
                    _BIOMETRIC_CONFIG = Common.Utils.SerializeHelper.DeserializeFromFile<BiometricConfig>("BiometricConfig.cfg");
                }

                if (_BIOMETRIC_CONFIG == null)
                {
                    LOG.Fatal("BiometricHelper.Initialization - Error leyendo BiometricConfig!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }

                //Cargo los Matchers configurados
                MATCHER_MANAGER = SerializeHelper.DeserializeFromFile<MatcherManager>(_BIOMETRIC_CONFIG.MatcherCfgPath); //("Matchers.cfg");
                if (MATCHER_MANAGER == null || MATCHER_MANAGER.MatchersConfigured.Count == 0)
                {
                    LOG.Info("BiometricHelper.Initalize - Revise la ruta del archivo de configuración Matchers");
                    LOG.Info("BiometricHelper.Initalize - Ruta de Matcher:" + Application.StartupPath + "\\Matcher.cfg");
                    LOG.Info("BiometricHelper.Initalize -    Reading Matchers = ERROR - No Matchers Configured!");
                    MessageBox.Show("No se han encontrado Matcher necesarios para que la aplicación funcione correctamente", "Biometrika", 
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Environment.Exit(0);
                }
                else
                {
                    LOG.Info("BiometricHelper.Initalize -    Reading Matchers = OK - Matchers Configured = " +
                             MATCHER_MANAGER.MatchersConfigured.Count.ToString());

                    //Si levanto matchers, inicializo creando las instancias
                    MATCHER_MANAGER.Initialization();

                    if (MATCHER_MANAGER.QuantityMatchersAvailables() == 0)
                    {
                        LOG.Info("BiometricHelper.Initalize -    Initializing Matchers = WARNING - No Matchers Availables!");
                        MessageBox.Show("No se han encontrado Matcher necesarios para que la aplicación funcione correctamente", "Biometrika", 
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Environment.Exit(0);
                    }
                    else
                    {
                        LOG.Info("BiometricHelper.Initalize -    Initializing Matchers = OK  - Matchers Availables = " +
                                       MATCHER_MANAGER.QuantityMatchersAvailables().ToString());

                        //1.- Genero Matcher, Extractor y Template correspondioente de acuerdo a AF y MT. 
                        //    Esto lo hago clonando desde el MatcherManager global. 
                        matcherintance = MATCHER_MANAGER.GetMatcherInstance(Bio.Core.Api.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                                                                            _BIOMETRIC_CONFIG.FingerMinutiaeType);
                        //(2, 7); // KioskSetting.GetAutenticationFactor(), KioskSetting.GetMinutiaeType());

                        extractor = matcherintance.Extractor;
                        templateCurrent = matcherintance.Template;
                        templateCurrent.AuthenticationFactor = Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT; //KioskSetting.GetAutenticationFactor();
                        templateCurrent.MinutiaeType = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW;
                        matcher = matcherintance.Matcher;
                        //Seteo los parametros de umbrales diferentes si hay diferencias
                        matcher.Threshold = matcher.Threshold;
                        LOG.Info("BiometricHelper.Initalize - Threshold configured = " + matcher.Threshold);
                        //KioskSetting.GetScore() != 0
                        //        ? KioskSetting.GetScore()
                        //        : matcher.Threshold;
                        matcher.MatchingType = matcher.MatchingType;
                        //KioskSetting.GetMatchingType() != 0
                        //                           ? KioskSetting.GetMatchingType()
                        //                           : matcher.MatchingType;
                        if (matcher.MatchingType != 1 && matcher.MatchingType != 2)
                        {
                            matcher.MatchingType = 1;
                        }
                        LOG.Info("BiometricHelper.Initalize - Procedemos a inicializar los componentes de identificación");
                        ret = matcher.Inicialize("");

                        LOG.Info("BiometricHelper.Initalize - Resultado del proceso de identificación:" + ret.ToString());
                        if (ret == 0)
                        {
                            LOG.Info("BiometricHelper.Initalize - Procedemos con las actividades de enrolamiento");
                            ret = LoadBIRs();
                            if (ret == 0)
                            {
                                ret = matcher.Enroll(objTemplates);
                                LOG.Info("BiometricHelper.Initalize - Resultado del proceso de enrolamiento" + ret.ToString());
                            } else
                            {
                                LOG.Warn("BiometricHelper.Initalize - Error recupeando BIRs! No podra identificar...[ret=" + ret + "]");
                                MessageBox.Show("Error recupeando BIRs! No podra identificar...[ret=" + ret + "]", "Biometrika",
                                        MessageBoxButtons.OK, MessageBoxIcon.Hand);
                                Environment.Exit(0);
                            }                            
                        }
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                LOG.Error("BiometricHelper.InitBiometricHelper", ex);
                return -1;
            }

        }

        private int LoadBIRs()
        {
            /*
                1.- Instancio dll de lectura de BD e inicializo
                2.- Recupero Lista y la seteo global
            */
            int ret = 0;
            try
            {
                LOG.Debug("BiometricHelper.LoadBIRs - IN...");
                if (_BIOMETRIC_CONFIG == null)
                    return Bio.Core.Api.Constant.Errors.IERR_BAD_SERIALIZER_CONFIG_FILE;

                //Instancio dll
                LOG.Debug("BiometricHelper.LoadBIRs - Assembly = " + _BIOMETRIC_CONFIG.LoadBIRPathDll);
                Assembly asmLogin = Assembly.Load(_BIOMETRIC_CONFIG.LoadBIRPathDll);
                Type[] types = null;
                LOG.Debug("BiometricHelper.LoadBIRs - GetTypes IN => " + asmLogin.FullName);
                types = asmLogin.GetTypes();
                
                foreach (Type t in types)
                {
                    try
                    {
                        object instance = null;
                        //ILoadBIRs oLoadBirs = null;
                        try
                        {
                            LOG.Debug("BiometricHelper.LoadBIRs -  Activator.CreateInstance IN: " + t.FullName);
                            instance = Activator.CreateInstance(t);
                            LOG.Debug("BiometricHelper.LoadBIRs - Activator.CreateInstance IN: OK!");
                        }
                        catch (Exception exC)
                        {
                            LOG.Warn("BiometricHelper.LoadBIRs - Activator.CreateInstance(t)", exC);
                            instance = null;
                        }

                        if (instance != null)
                        {
                            if (instance is ILoadBIRs)
                            {
                                if (!((ILoadBIRs)instance).Initialized)
                                {
                                    LOG.Debug("BiometricHelper.LoadBIRs - Inicializando instancia...");
                                    ret = ((ILoadBIRs)instance).Initialize();
                                    LOG.Debug("BiometricHelper.LoadBIRs - Incializado ret = " + ret.ToString());
                                }
                                if (ret == 0)
                                {
                                    ret = ((ILoadBIRs)instance).DoLoadBIRs(out objTemplates);
                                    if (ret < 0)
                                    {
                                        LOG.Warn("BiometricHelper.LoadBIRs - Error en la DoLoadBIRs [ret=" + ret.ToString() + "]");
                                    }
                                } else
                                {
                                    LOG.Warn("BiometricHelper.LoadBIRs - Error en la inicializacion de LoadBIRs [ret=" + ret.ToString() + "]");
                                }
                            }
                        }
                    } catch (Exception ex)
                    {
                        LOG.Error("BiometricHelper.LoadBIRs - Exc DoLoadBIRs [" + ex.Message + "]");
                    }
                }
            }
            catch (Exception ex)
            {

                LOG.Error("BiometricHelper.LoadBIRs - Error: " + ex.Message);
            }
            return ret;
        }

        internal int RefreshLoadBIRs()
        {
            int ret = 0;

            try
            {
                LOG.Info("BiometricHelper.RefreshLoadBIRs - Procedemos con las actividades de enrolamiento");
                lock (frmMain.OBJECT_TO_BLOCK)
                {
                    LOG.Info("BiometricHelper.RefreshLoadBIRs - Ingresando a LoadBIRs...");
                    ret = LoadBIRs();
                    if (ret == 0)
                    {
                        ret = matcher.Enroll(objTemplates);
                        LOG.Info("BiometricHelper.RefreshLoadBIRs - Resultado del proceso de enrolamiento" + ret.ToString());
                    }
                    else
                    {
                        LOG.Warn("BiometricHelper.RefreshLoadBIRs - Error recupeando BIRs! No podra identificar...[ret=" + ret + "]");
                        //MessageBox.Show("Error recupeando BIRs! No podra identificar...[ret=" + ret + "]", "Biometrika",
                        //        MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        //Environment.Exit(0);
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error(" Error: " + ex.Message);
            }
            LOG.Info("BiometricHelper.RefreshLoadBIRs OUT!");
            return ret;
        }

        public int GetTemplateVF(byte[] wsq, out ITemplate template)
        {
            try
            {
                template = null;
                LOG.Debug("BiometricHelper.Identify -- Starting...");

                LOG.Debug("BiometricHelper.Identify -- GetMatcherInstance - matcherintance != null : " +
                          (matcherintance != null).ToString());

                //    1.1.- Si no existe retorno con error.
                if (matcherintance == null)
                {
                    return Errors.IERR_MATCHER_NOT_AVAILABLE;
                }

                //2.- Genero template correspondiente desde el sample que viene en Input.
                //    2.1.- Genero template desde Input
                //    2.2.- Llamo a Extractor con el template generado y este retorna el mismo
                //          template si ya es minucia, o si es sample, lo transforma de acuerdo a
                //          como lo entiende cada algoritmo, y genera el template con las minucias 
                //          correspondientes a ese template.
                //IExtractor extractor = matcherintance.Extractor;
                //ITemplate templateCurrent = matcherintance.Template;
                //templateCurrent.AuthenticationFactor = Program.CONFIG.AuthenticationFactor;

                //templateCurrent.MinutiaeType = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW;
                //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ; //Program.CONFIG.MinutiaeType;
                templateCurrent.Data = wsq;
                templateCurrent.AdditionalData = null;
                ITemplate templateCurrentExtracted;
                //extractor.Threshold = extractor.Threshold != oparamin. No viene el THExtractor, ver si lo agrego
                int iErr = extractor.Extract(templateCurrent, TemplateDestination.TEMPLATEDESTINATION_TOVERIFY,
                                         out templateCurrentExtracted);
                //Si hay error retorno
                if (iErr != 0)
                {
                    return Errors.IERR_EXTRACTING;
                }
                else
                {
                    template = templateCurrentExtracted;
                    return Errors.IERR_OK;
                }
            }
            catch (Exception exe)
            {
                template = null;
                LOG.Error("Error en VF");
                return Errors.IERR_ACTION_NOT_SUPPORTED;
            }

        }

        public int Identify(byte[] wsq, out string idIdentified)
        {
            int ret = 0;
            idIdentified = null;
            try
            {
                LOG.Debug("BiometricHelper.Identify -- Starting...");

                LOG.Debug("BiometricHelper.Identify -- GetMatcherInstance - matcherintance != null : " +
                          (matcherintance != null).ToString());

                //    1.1.- Si no existe retorno con error.
                if (matcherintance == null)
                {
                    return Errors.IERR_MATCHER_NOT_AVAILABLE;
                }

                //2.- Genero template correspondiente desde el sample que viene en Input.
                //    2.1.- Genero template desde Input
                //    2.2.- Llamo a Extractor con el template generado y este retorna el mismo
                //          template si ya es minucia, o si es sample, lo transforma de acuerdo a
                //          como lo entiende cada algoritmo, y genera el template con las minucias 
                //          correspondientes a ese template.
                //IExtractor extractor = matcherintance.Extractor;
                //ITemplate templateCurrent = matcherintance.Template;
                //templateCurrent.AuthenticationFactor = Program.CONFIG.AuthenticationFactor;

                templateCurrent.MinutiaeType = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW;
                //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ; //Program.CONFIG.MinutiaeType;
                templateCurrent.Data = wsq;
                templateCurrent.AdditionalData = null;
                ITemplate templateCurrentExtracted;
                //extractor.Threshold = extractor.Threshold != oparamin. No viene el THExtractor, ver si lo agrego
                int iErr = extractor.Extract(templateCurrent, TemplateDestination.TEMPLATEDESTINATION_TOVERIFY,
                                         out templateCurrentExtracted);
                //Si hay error retorno
                if (iErr != 0)
                {
                    LOG.Error("Se produjo un error al extraer: " + iErr);
                    return Errors.IERR_EXTRACTING;
                }

                //Comienza parte de Identify
                //Instancio Matcher
                //IMatcher matcher = matcherintance.Matcher;
                ////Seteo los parametros de umbrales diferentes si hay diferencias
                //matcher.Threshold = Program.CONFIG.Score != 0
                //                        ? Program.CONFIG.Score
                //                        : matcher.Threshold;
                //matcher.MatchingType = Program.CONFIG.MatchingType != 0
                //                           ? Program.CONFIG.MatchingType
                //                           : matcher.MatchingType;
                //if (matcher.MatchingType != 1 && matcher.MatchingType != 2)
                //{
                //    matcher.MatchingType = 1;
                //}

                float score = 0;
                int idBir = 0;
                iErr = matcher.Identify(templateCurrentExtracted, objTemplates, out score, out idIdentified);

                if (iErr == 0) // > 0) //Reconoció => Lleno datos
                {
                    string msg;

                    if (!string.IsNullOrEmpty(idIdentified))
                    {
                        LOG.Debug("BiometricHelper.Identify - Idenfificado OK => idIdentified = " + idIdentified);
                    } else
                    {
                        LOG.Debug("BiometricHelper.Identify - Sin Error pero No Identificado!");
                        ret = Errors.IERR_IDENTITY_NOT_FOUND;
                    }
                    //WaBirSmall bir = AdminWaBirSmall.Retrieve(idBir, out msg);
                    //if (bir == null)
                    //{
                    //    LOG.Error("BiometricHelper.Identify Error recuperando idBir = " + idBir +
                    //                "[" + msg + "]");
                    //    ret = Errors.IERR_IDENTITY_NOT_FOUND;
                    //}
                    //else
                    //{
                    //    //Program.employeeCurrent = bir.EmpId; // database.AdminBpIdentity.Retrieve(bir.BpIdentity.Id, out msg);
                    //    //if (Program.employeeCurrent == null)
                    //    //{
                    //    //    LOG.Error("BiometricHelper.Identify Error recuperando idBir = " + idBir +
                    //    //              "[" + msg + "]");
                    //    //    ret = Errors.IERR_IDENTITY_NOT_FOUND;
                    //    //}
                    //}
                }
                else
                {
                    LOG.Error("BiometricHelper.Identify Error en matcher.Identify = " + iErr.ToString());
                    ret = iErr;
                }

                LOG.Debug("BiometricHelper.Identify -- End!");

            }
            catch (Exception ex)
            {
                LOG.Error("BiometricHelper.Identify Exception", ex);
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }

        internal byte[] GetSample(List<Sample> samplesCaptured)
        {
            byte[] wsq = null;
            byte[] rawOut = null;
            try
            {
                LOG.Debug("BiometricHelper.GetSample IN... samplesCaptured==null => " + (samplesCaptured == null).ToString());
                if (samplesCaptured == null)
                    return null;

                foreach (Sample item in samplesCaptured)
                {
                    if (item.MinutiaeType == Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                    {
                        wsq = (byte[])item.Data;
                    }

                    if (item.MinutiaeType == Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_RAW)
                    {
                        rawOut = (byte[])item.Data;
                    }
                }

                //Si no venia raw pero si wsq => Descomprimo wsq para generar raw
                if (rawOut == null && wsq != null)
                {
                    LOG.Debug("BiometricHelper.GetSample - Decode WSQ...");
                    Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                    short w, h;
                    dec.DecodeMemory(wsq, out w, out h, out rawOut);
                }
            }
            catch (Exception ex)
            {
                rawOut = null;
                LOG.Error("BiometricHelper.GetSample - Error: " + ex.Message);
            }
            LOG.Debug("BiometricHelper.GetSample OUT => Encontro? => rawOut != null => " + (rawOut != null));
            return rawOut;
        }

        public int Identify(byte[] wsq, List<Template> listTemplatePeople, int flag, out int idBir)
        {
            DateTime dtStart;
            DateTime dtEnd;
            idBir = 0;
            int ret = 0;
            try
            {
                //dtStart = DateTime.Now;
                //LOG.Debug("BiometricHelper.Identify -- Starting...");
                //LOG.Debug("Matcherinstance:" + matcherintance);
                ////LOG.Debug("BiometricHelper.Identify -- GetMatcherInstance - matcherintance != null : " +
                ////          (matcherintance != null).ToString());

                ////    1.1.- Si no existe retorno con error.
                //if (matcherintance == null)
                //{
                //    return Errors.IERR_MATCHER_NOT_AVAILABLE;
                //}

                ////2.- Genero template correspondiente desde el sample que viene en Input.
                ////    2.1.- Genero template desde Input
                ////    2.2.- Llamo a Extractor con el template generado y este retorna el mismo
                ////          template si ya es minucia, o si es sample, lo transforma de acuerdo a
                ////          como lo entiende cada algoritmo, y genera el template con las minucias 
                ////          correspondientes a ese template.
                ////IExtractor extractor = matcherintance.Extractor;
                ////ITemplate templateCurrent = matcherintance.Template;
                ////templateCurrent.AuthenticationFactor = Program.CONFIG.AuthenticationFactor;

                ////templateCurrent.MinutiaeType = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW;
                //templateCurrent.MinutiaeType = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ; //Program.CONFIG.MinutiaeType;
                //templateCurrent.Data = wsq;
                //templateCurrent.AdditionalData = null;
                //ITemplate templateCurrentExtracted;
                ////extractor.Threshold = extractor.Threshold != oparamin. No viene el THExtractor, ver si lo agrego
                ////LOG.Debug("BiometricHelper.Identify - Entrando a extractor.Extract [WSQ= " + wsq.Length.ToString() + "]");
                //int iErr = extractor.Extract(templateCurrent, TemplateDestination.TEMPLATEDESTINATION_TOVERIFY,
                //                         out templateCurrentExtracted);
                ////Si hay error retorno
                //if (iErr != 0)
                //{
                //    LOG.Debug("BiometricHelper.Identify - Saliendo de Extract con error [" + iErr.ToString() + "]");
                //    return Errors.IERR_EXTRACTING;
                //}

                ////Comienza parte de Identify
                ////Instancio Matcher
                ////IMatcher matcher = matcherintance.Matcher;
                //////Seteo los parametros de umbrales diferentes si hay diferencias
                ////matcher.Threshold = Program.CONFIG.Score != 0
                ////                        ? Program.CONFIG.Score
                ////                        : matcher.Threshold;
                ////matcher.MatchingType = Program.CONFIG.MatchingType != 0
                ////                           ? Program.CONFIG.MatchingType
                ////                           : matcher.MatchingType;
                ////if (matcher.MatchingType != 1 && matcher.MatchingType != 2)
                ////{
                ////    matcher.MatchingType = 1;
                ////}

                //float score = 0;

                ////int idEmpl = 0;
                ////iErr = matcher.Identify(templateCurrentExtracted, listTemplatePeople, out score, out idBir);
                //iErr = matcher.Identify(templateCurrentExtracted, listTemplatePeople, out score, out idBir);
                //LOG.Info("idBir:" + idBir);
                //dtEnd = DateTime.Now;
                //LOG.Info("fin identify = " + (dtEnd - dtStart).TotalSeconds.ToString());
                ////if (iErr == 0 && idBir > 0) //Reconoció => Lleno datos
                //if (iErr == 0 && idBir > 0) //Reconoció => Lleno datos
                //{
                //    string msg;
                //    int res = 0;
                //    //Recuperamos información del empleado.
                //    //WaEmployee objWaEmployee = DatabaseHelper.GetEmployee(idBir, out res, out msg);                    
                //    //if (objWaEmployee == null)
                //    //{
                //    //    LOG.Error("BiometricHelper.Identify Error recuperando idBir = " + idBir +
                //    //                "[" + msg + "]");
                //    //    ret = Errors.IERR_IDENTITY_NOT_FOUND;
                //    //}
                //    //else
                //    //{
                //    //    if (flag == 1)
                //    //    {
                //    //        Program.employeeCurrentT1 = objWaEmployee;
                //    //        //Program.employeeCurrentT1 = GetEmployee(idEmpl, out msg);
                //    //        //Program.employeeCurrentT1 = database.DatabaseHelper..AdminBpIdentity.Retrieve(bir.BpIdentity.Id, out msg);
                //    //        if (Program.employeeCurrentT1 == null)
                //    //        {
                //    //            //LOG.Error("BiometricHelper.Identify Error recuperando idBir = " + idBir +
                //    //            //          "[" + msg + "]");
                //    //            LOG.Error("BiometricHelper.Identify Error recuperando idBir = " + idBir +
                //    //                      "[" + msg + "]");
                //    //            ret = Errors.IERR_IDENTITY_NOT_FOUND;
                //    //        }
                //    //    }
                //    //    else
                //    //    {
                //    //        Program.employeeCurrentT2 = objWaEmployee;
                //    //        //Program.employeeCurrentT2 = GetEmployee(idEmpl, out msg);
                //    //        // database.AdminBpIdentity.Retrieve(bir.BpIdentity.Id, out msg);
                //    //        if (Program.employeeCurrentT2 == null)
                //    //        {
                //    //            //LOG.Error("BiometricHelper.Identify Error recuperando idBir = " + idBir +
                //    //            //          "[" + msg + "]");
                //    //            LOG.Error("BiometricHelper.Identify Error recuperando idBir = " + idBir +
                //    //                      "[" + msg + "]");
                //    //            ret = Errors.IERR_IDENTITY_NOT_FOUND;
                //    //        }
                //    //    }
                //    //}
                //}
                //else
                //{
                //    LOG.Error("BiometricHelper.Identify Error en matcher.Identify = " + iErr.ToString());
                //    ret = iErr;
                //}

                //dtEnd = DateTime.Now;
                //LOG.Info("BiometricHelper.Identify -- Identify Total Time = " + (dtEnd - dtStart).TotalSeconds.ToString());
                //LOG.Debug("BiometricHelper.Identify -- End!");

            }
            catch (Exception ex)
            {
                LOG.Error("BiometricHelper.Identify Exception", ex);
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }


        //public static void Identify(ITemplate obj, List<Template> objs, out float score, out int idBir)
        //{
        //    //matcher.Identify(obj, objs, out score, out idBir);
        //}

        //public static void Extract(byte[] by512, out ITemplate templatec)
        //{
        //    extractor.ExtractFromBy512(by512, out templatec);

        //}

        public int IdentifyExtractor(byte[] wsq, out string template)
        {
            DateTime dtStart;
            DateTime dtEnd;
            int ret = 0;
            try
            {
                dtStart = DateTime.Now;
                LOG.Debug("BiometricHelper.Identify -- Starting...");

                LOG.Debug("BiometricHelper.Identify -- GetMatcherInstance - matcherintance != null : " +
                          (matcherintance != null).ToString());

                //    1.1.- Si no existe retorno con error.
                if (matcherintance == null)
                {
                    ret = Errors.IERR_MATCHER_NOT_AVAILABLE;
                    template = null;
                    return ret;
                }

                //2.- Genero template correspondiente desde el sample que viene en Input.
                //    2.1.- Genero template desde Input
                //    2.2.- Llamo a Extractor con el template generado y este retorna el mismo
                //          template si ya es minucia, o si es sample, lo transforma de acuerdo a
                //          como lo entiende cada algoritmo, y genera el template con las minucias 
                //          correspondientes a ese template.
                //IExtractor extractor = matcherintance.Extractor;
                //ITemplate templateCurrent = matcherintance.Template;
                //templateCurrent.AuthenticationFactor = Program.CONFIG.AuthenticationFactor;

                //templateCurrent.MinutiaeType = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW;
                templateCurrent.MinutiaeType = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ; //Program.CONFIG.MinutiaeType;
                templateCurrent.Data = wsq;
                templateCurrent.AdditionalData = null;
                ITemplate templateCurrentExtracted;
                //extractor.Threshold = extractor.Threshold != oparamin. No viene el THExtractor, ver si lo agrego
                LOG.Debug("BiometricHelper.Identify - Entrando a extractor.Extract [WSQ= " + wsq.Length.ToString() + "]");
                int iErr = extractor.Extract(templateCurrent, TemplateDestination.TEMPLATEDESTINATION_TOVERIFY,
                         out templateCurrentExtracted);
                template = templateCurrentExtracted.GetData;

            }
            catch (Exception ex)
            {
                template = null;
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("BiometricHelper.Identify Exception", ex);
                ret = Errors.IERR_UNKNOWN;
            }

            return ret;

        }

        /*
        internal int Initalize()
        {
            int ret = 0;

            try
            {
                //Read Config

                //Load BIRs
                LoadBIRs();
            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }

            return ret;
        }

        internal int LoadBIRs()
        {
            int ret = 0;

            try
            {

            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }

            return ret;
        }

        internal int Identify(string raw, out string idIdentified)
        {
            int ret = 0;

            try
            {

            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }

            return ret;
        }
        */
    }
}
