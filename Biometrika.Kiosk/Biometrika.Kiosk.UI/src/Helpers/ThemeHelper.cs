﻿using Biometrika.Kiosk.UI.src.Themes;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.UI.src.Helpers
{
    /// <summary>
    /// Helper que permite manejar el thema configurado. 
    /// Inicaliza el theme, lee de disco las configuraciones customizadas, y luego mantiene en 
    /// memoria los datos para operar.
    /// </summary>
    public class ThemeHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ThemeHelper));

        public Theme _THEME;

        public ThemeHelper()
        {
            
        }

        /// <summary>
        /// Instancia Theme configurado y lo inicializa.
        /// </summary>
        /// <param name="themeName"></param>
        /// <returns></returns>
        internal int Initialize(string themeName)
        {
            int ret = 0;
            try
            {
                LOG.Debug("ThemeHelper.Initialize IN => themename = " + themeName);
                _THEME = new Theme(themeName);
                ret = _THEME.Initialize();
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ThemeHelper.Initialize - Excp Error: " + ex.Message);
            }
            LOG.Debug("ThemeHelper.Initialize OUT => ret = " + ret.ToString());
            return ret;
        }
    }
}
