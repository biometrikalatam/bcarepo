﻿using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Biometrika.Kiosk.UI.src.Themes
{
    /// <summary>
    /// Se inicia con el theme configurado. Creao o lee el ".cfg" con los paths de las imágenes
    /// cutomizadas por el Theme, y ciertos datos de coordenadas y tamaños para poder tener
    /// flexibilidad con las configuraciones de monitores y resoluciones.
    /// Si una imágen no la encuentra utiliza la de Default.
    /// Se maneja un DynamicData para las imágenes, uno para las coordenadas (StyleList)
    /// y otro para Mensajes personalizasos por código dependiendo el resultado 
    /// obtenido desde check de BusinessRule.
    /// </summary>
    public class Theme
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Theme));

        bool _Initialized;
        string _Name = "Default";
        string _BasePathTheme = "\\Themes\\Default\\";
        ThemeConfig _Config;
        

        public Theme() { }

        public Theme(string name) { //, bool initialize) {

            try
            {
                LOG.Debug("Theme.Constructor IN => name=" + name); // + " - initialize=" + initialize.ToString());
                Name = name;
                BasePathTheme = "\\Themes\\" + Name + "\\";
                Config = new ThemeConfig();
            }
            catch (Exception ex)
            {
                LOG.Error("Theme.Constructor - Excp Error: " + ex.Message);
            }
            LOG.Debug("Theme.Constructor OUT!");
        }

        public bool Initialized
        {
            get {
                return this._Initialized;
            }

            set {
                this._Initialized = value;
            }
        }

        public string Name
        {
            get {
                return this._Name;
            }

            set {
                this._Name = value;
            }
        }

        public string BasePathTheme
        {
            get {
                return this._BasePathTheme;
            }

            set {
                this._BasePathTheme = value;
            }
        }

        public ThemeConfig Config
        {
            get {
                return this._Config;
            }

            set {
                this._Config = value;
            }
        }

        public int Initialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("Theme.Initialize IN...");
                string archName = "Theme_" + Name + ".cfg";
                if (!System.IO.File.Exists(archName))
                {
                    LOG.Debug("Theme.Constructor - Inicia inicializacion manual...");
                    Config = new ThemeConfig(Name, BasePathTheme);
                    Config.FillDefault();
                    Config.Initialize();

                    if (!SerializeHelper.SerializeToFile(Config, archName))
                    {
                        LOG.Warn("BusinessRuleAutoclub.Initialize - No grabo condif en disco (" + archName + ")");
                    }
                }
                else
                {
                    Config = SerializeHelper.DeserializeFromFile<ThemeConfig>(archName);
                    Config.Initialize();
                }

                if (Config == null)
                {
                    LOG.Fatal("BusinessRuleAutoclub.Initialize - Error leyendo " + archName);
                    return Errors.IERR_DESERIALIZING_DATA;
                } else
                {
                    Initialized = true;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Theme.Initialize Error: " + ex.Message);
            }
            LOG.Debug("Theme.Initialize OUT!");
            return ret;
        }
    }

    public class ThemeConfig
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ThemeConfig));

        string _Name = "Default";
        string _BasePathTheme = "\\Themes\\Default\\";
        DynamicData _ImageList;
        DynamicData _StylesList;
        DynamicData _MsgFeedbackList;

        [XmlIgnoreAttribute]
        private Dictionary<string, object> imageDictionary;

        public string Name
        {
            get {
                return this._Name;
            }

            set {
                this._Name = value;
            }
        }

        public string BasePathTheme
        {
            get {
                return this._BasePathTheme;
            }

            set {
                this._BasePathTheme = value;
            }
        }

        public DynamicData ImageList
        {
            get {
                return this._ImageList;
            }

            set {
                this._ImageList = value;
            }
        }

        public DynamicData StylesList
        {
            get {
                return this._StylesList;
            }

            set {
                this._StylesList = value;
            }
        }

        public DynamicData MsgFeedbackList
        {
            get {
                return this._MsgFeedbackList;
            }

            set {
                this._MsgFeedbackList = value;
            }
        }

        [XmlIgnoreAttribute]
        public Dictionary<string, object> ImageDictionary
        {
            get {
                return this.imageDictionary;
            }

            set {
                this.imageDictionary = value;
            }
        }

        public ThemeConfig(string name, string basepaththeme)
        {
            Name = name;
            BasePathTheme = basepaththeme;
            ImageList = new DynamicData();
            StylesList = new DynamicData();
            MsgFeedbackList = new DynamicData();
        }

        public ThemeConfig()
        {
            ImageList = new DynamicData();
            StylesList = new DynamicData();
            MsgFeedbackList = new DynamicData();
        }

        public void FillDefault()
        {
            try
            {
                if (ImageList == null)
                    ImageList = new DynamicData();
                if (StylesList == null)
                    StylesList = new DynamicData();
                if (MsgFeedbackList == null)
                    MsgFeedbackList = new DynamicData();

                //LLeno Images Pantalla Principal
                    //Fondo
                ImageList.AddItem("ImageBackground", BasePathTheme + "ImageBackground.png");
                    //Logo CLiente. Si no está se usa nulo
                ImageList.AddItem("ImageLogoClient", BasePathTheme + "ImageLogoClient.png");
                    //Iconos de tecnologias para marcacion
                ImageList.AddItem("ImageTechFinger", BasePathTheme + "ImageTechFinger.png");
                ImageList.AddItem("ImageTechFacial", BasePathTheme + "ImageTechFacial.png");
                ImageList.AddItem("ImageTechLCB", BasePathTheme + "ImageTechLCB.png");
                ImageList.AddItem("ImageTechMRZ", BasePathTheme + "ImageTechMRZ.png");
                ImageList.AddItem("ImageTechPIN", BasePathTheme + "ImageTechPIN.png");
                    //Imagenes apra botones de angulo superior derecho
                ImageList.AddItem("ImageBtnPicClose", BasePathTheme + "ImageBtnPicClose.png");
                ImageList.AddItem("ImageBtnPicAcercaDe", BasePathTheme + "ImageBtnPicAcercaDe.png");
                ImageList.AddItem("ImageBtnPicSupport", BasePathTheme + "ImageBtnPicSupport.png");
                ImageList.AddItem("ImageBtnPicReinit", BasePathTheme + "ImageBtnPicReinit.png");

                //LLeno Images Pantalla ShowResult
                    //Fondo base
                ImageList.AddItem("ImageFondoResultEntrada", BasePathTheme + "ImageFondoResultEntrada.png");
                ImageList.AddItem("ImageFondoResultSalida", BasePathTheme + "ImageFondoResultSalida.png");
                ImageList.AddItem("ImageFondoResultNoFoto", BasePathTheme + "ImageFondoResultNoFoto.png");
                //Imagenes para mostrar con que tecnologia se marco. Solo Huella o Facial muestra sample real
                // el resto muestra imagen fija. Si no esta se usa la misma que el icono de pantalla principal 
                ImageList.AddItem("ImageTechLCBResult", BasePathTheme + "ImageTechLCB.png");
                ImageList.AddItem("ImageTechMRZResult", BasePathTheme + "ImageTechMRZ.png");
                ImageList.AddItem("ImageTechPINResult", BasePathTheme + "ImageTechPIN.png");
                    //Imagenes para mostrar resultado de accin, como acceso permitido, denegado, no identificado, etc
                ImageList.AddItem("ImagePicResultActionAccesoPermitido", BasePathTheme + "ImagePicResultActionAccesoPermitido.png");
                ImageList.AddItem("ImagePicResultActionAccesoDenegado", BasePathTheme + "ImagePicResultActionAccesoDenegado.png");
                ImageList.AddItem("ImagePicResultActionSalidaPermitido", BasePathTheme + "ImagePicResultActionSalidaPermitido.png");
                ImageList.AddItem("ImagePicResultActionSalidaDenegado", BasePathTheme + "ImagePicResultActionSalidaDenegado.png");
                    //Para indicar por ejemplo dirijase a administracion
                ImageList.AddItem("ImagePicResultActionInfo", BasePathTheme + "ImagePicResultActionInfo.png");
                    //Para mostrar en picFoto en lugar de una foto 
                ImageList.AddItem("ImagePicResultActionNoIdentify", BasePathTheme + "ImagePicResultActionNoIdentify.png");

                //ImageList.AddItem("", BasePathTheme + ".png");
                //ImageList.AddItem("ImageAccesoPermitido", BasePathTheme + "ImageAccesoPermitido.jpg");
                //ImageList.AddItem("ImageSalidaPermitido", BasePathTheme + "ImageSalidaPermitido.jpg");
                //ImageList.AddItem("ImageAccesoDenegado", BasePathTheme + "ImageAccesoDenegado.jpg");
                //ImageList.AddItem("ImageSalidaDenegado", BasePathTheme + "ImageSalidaDenegado.jpg");
                //ImageList.AddItem("ImageBackgroundNoIdentify", BasePathTheme + "ImageBackgroundNoIdentify.jpg");
                //ImageList.AddItem("ImageBackgroundNoIdentifySalida", BasePathTheme + "ImageBackgroundNoIdentifySalida.jpg");

                //LLeno Styles, cubriendo tamaños y posiciones de imágnes, y tipos|tamaño|color de letras
                //  - Para imagenes es x|y|w|h para posicion y luego tamaño
                //  - Para cuadros de mensajes escritos x|y|w|h|nombre tipo letra|tamaño letra|color en #AAAAAA
                //      Se usa => this.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffc0c0");
                    //Imagenes Elementos de Pantalla Principal
                StylesList.AddItem("StyleImageTechFinger", "274|434|330|400");
                StylesList.AddItem("StyleImageTechFacial", "274|968|330|400");
                StylesList.AddItem("StyleImageImageTechLCB", "-500|-500|1|1");
                StylesList.AddItem("StyleImageImageTechMRZ", "-500|-500|1|1");
                StylesList.AddItem("StyleImageImageTechPIN", "-500|-500|1|1");
                    //Botones Superiores
                StylesList.AddItem("StyleImageBtnPicClose", "825|30|25|25");
                StylesList.AddItem("StyleImageBtnPicAcercaDe", "795|30|25|25");
                StylesList.AddItem("StyleImageBtnPicSupport", "760|30|25|25");
                StylesList.AddItem("StyleImageBtnPicReinit", "725|30|25|25"); 
                    //Cuadros de texto Pantalla Principal
                StylesList.AddItem("StyleLabError", "10|130|88|70|Courier New|14|#ffc0c0");

                    //Imagenes Elementos de ShowResult
                StylesList.AddItem("StylePicFotoIdentified", "105|205|200|200");
                StylesList.AddItem("StylePicSample", "150|440|110|110");
                StylesList.AddItem("StylePicResultAction", "375|440|420|110");
                    //Cuadros de texto ShowResult
                StylesList.AddItem("StyleLabFeedbackName", "350|205|465|30|Arial|20|#e0e0e0");
                StylesList.AddItem("StyleLabFeedbackDetail", "350|250|470|180|Arial|24|#64dc64");
                StylesList.AddItem("StyleLabAntipassback", "520; 545|264|37|Arial|24|#ff8080");

                    //Windows Refresh HUellas Posicion y Tamaño
                StylesList.AddItem("StyleGrpRefresh", "200|600|555|160");
                

                
                //LLeno Mesages Feedback
                MsgFeedbackList.AddItem("-1", "Error Interno");
                MsgFeedbackList.AddItem("-2", "Antipassback");
                MsgFeedbackList.AddItem("-3", "Socio sin acceso (deuda o inactivo)");
                MsgFeedbackList.AddItem("-4", "Visita sin acceso (sin evento / invitación encontrado)");
                MsgFeedbackList.AddItem("-5", " Visita sin acceso (socio autorizador no tiene invitaciones disponibles)");
                MsgFeedbackList.AddItem("-6", "Identidad No Registrada!");
            }
            catch (Exception ex)
            {
                LOG.Error("Theme.FillDefault - Excp Error: " + ex.Message);
            }
        }

        internal void Initialize()
        {
            try
            {
                LOG.Debug("Theme.Initialize IN...");
                if (ImageList != null && ImageList.ListDynamicDataItems != null)
                {
                    if (ImageDictionary == null)
                        ImageDictionary = new Dictionary<string, object>();

                    LOG.Debug("Theme.Initialize - Cargando " + 
                                ImageList.ListDynamicDataItems.Count.ToString() + " imagenes..."); 
                    foreach (DynamicDataItem item in ImageList.ListDynamicDataItems)
                    {
                        LOG.Debug("Theme.Initialize -   >> Image Added => " + item.key + "-" + (string)item.value);
                        Image img = null;
                        try
                        {
                            img = Image.FromFile(Program._PATH_EXE + (string)item.value);
                        }
                        catch (Exception ex)
                        {
                            img = null;
                            LOG.Warn("Theme.Initialize Warn => Error levantando imagen: " + item.key + "[" + ex.Message + "]");
                        }
                        ImageDictionary.Add(item.key, img);
                    }
                } else
                {
                    LOG.Error("Theme.Initialize - Lista Imagenes no configurada!");
                }

                if (this.StylesList != null)
                {
                    this.StylesList.Initialize();
                }

                if (this.MsgFeedbackList != null)
                {
                    this.MsgFeedbackList.Initialize();
                }

            }
            catch (Exception ex)
            {
                LOG.Error("Theme.Initialize - Excp Error: " + ex.Message);
            }
            LOG.Debug("Theme.Initialize OUT!");
        }

        public string GetFeedbackMsg(string code)
        {
            string ret = "";
            try
            {
                if (MsgFeedbackList != null && MsgFeedbackList.DynamicDataItems.ContainsKey(code))
                {
                    ret = (string)MsgFeedbackList.DynamicDataItems[code];
                }
            }
            catch (Exception ex)
            {
                ret = "";
                LOG.Error("Theme.GetFeedbackMsg - Error: " + ex.Message);
            }
            return ret;
        }
    }
}
