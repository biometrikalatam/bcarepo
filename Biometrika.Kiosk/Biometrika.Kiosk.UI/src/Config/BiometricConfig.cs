﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.UI.src.Config
{
    /// <summary>
    /// Configuraciones de BiometrikHelper para operar
    /// </summary>
    public class BiometricConfig
    {
        public BiometricConfig() {
            _MatcherCfgPath = "Matchers.cfg";
            _FingerMinutiaeType = 7;
            _FingerThreshold = 75;
            _FacialMinutiaeType = 44;
            _FacialThreshold = 1.1;
            _LoadBIRPathDll = "Biometrika.Kiosk.LoadBIRDatabase";
        }

        private string _MatcherCfgPath = "Matchers.cfg";
        private int _FingerMinutiaeType = 7;
        private double _FingerThreshold = 75;
        private int _FacialMinutiaeType = 44;
        private double _FacialThreshold = 1.1;
        private string _LoadBIRPathDll = "Biometrika.Kiosk.LoadBIRDatabase"; //"Biometrika.Kiosk.LoadBIRAPIRest";

        public string MatcherCfgPath
        {
            get {
                return this._MatcherCfgPath;
            }

            set {
                this._MatcherCfgPath = value;
            }
        }

        public int FingerMinutiaeType
        {
            get {
                return this._FingerMinutiaeType;
            }

            set {
                this._FingerMinutiaeType = value;
            }
        }

        public double FingerThreshold
        {
            get {
                return this._FingerThreshold;
            }

            set {
                this._FingerThreshold = value;
            }
        }

        public int FacialMinutiaeType
        {
            get {
                return this._FacialMinutiaeType;
            }

            set {
                this._FacialMinutiaeType = value;
            }
        }

        public double FacialThreshold
        {
            get {
                return this._FacialThreshold;
            }

            set {
                this._FacialThreshold = value;
            }
        }

        public string LoadBIRPathDll
        {
            get {
                return this._LoadBIRPathDll;
            }

            set {
                this._LoadBIRPathDll = value;
            }
        }
    }
}
