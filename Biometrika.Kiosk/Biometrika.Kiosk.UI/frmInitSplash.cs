﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Kiosk.UI
{
    public partial class frmInitSplash : Form
    {
        public frmInitSplash()
        {
            InitializeComponent();
        }

        internal void SetStep(int step)
        {
            try
            {
                if (pbarInitialization.Value + 10 > 100)
                {
                    pbarInitialization.Value = 100;
                } else
                {
                    pbarInitialization.Value += 10;
                }
                pbarInitialization.Refresh();
                labStep.Text = step.ToString();
                labStep.Refresh();

                switch (step)
                {
                    case 3:
                        labStatus1.Text = "OK";
                        pic1.Image = Properties.Resources.check_OK_T;
                        break;
                    case 4:
                        pic2.Image = Properties.Resources.check_OK_T;
                        labStatus2.Text = "OK";
                        break;
                    default:
                        break;
                }
                this.Refresh();
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
           
        }

        internal void Finish()
        {
            try
            {
                picFinal.Image = Properties.Resources.check_OK_T;
                picFinal.Refresh();
                pbarInitialization.Value = pbarInitialization.Maximum; 
                pbarInitialization.Refresh();
                System.Threading.Thread.Sleep(4000);
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void frmInitSplash_Load(object sender, EventArgs e)
        {
            try
            {
                Version version = Assembly.GetExecutingAssembly().GetName().Version;
                labVersion.Text = "v" + version.ToString();
            }
            catch (Exception ex)
            {
                //LOG.Error(" Error: " + ex.Message);
            }
        }
    }
}
