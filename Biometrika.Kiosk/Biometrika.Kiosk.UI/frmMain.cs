﻿using BioCore.SerialComm;
using BioCore.SerialComm.BarCode;
using BioCore.SerialComm.Cedula;
using BioCore.SerialComm.SerialPort;
using Biometrika.BioApi20;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using Biometrika.Kiosk.UI.src.Helpers;
using log4net;
using SecuGen.FDxSDKPro.Windows;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using Emgu.CV;
using Capture = Emgu.CV.Capture;
using Emgu.CV.Structure;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
using Label = System.Windows.Forms.Label;
using System.IO;
using Bio.Core.Serialize;
using System.Drawing.Imaging;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Security.Cryptography;
using AForge.Video.DirectShow;

namespace Biometrika.Kiosk.UI
{

    /// <summary>
    /// Formulario principal del kiosko. Aqui se realiza la identificación y con este valor se procede a 
    /// realizar los chequeos de acceso, aciones y marcación, con la BusinessRule, Action y MArk configurados
    /// respectivamente. 
    /// Gráficamente utiliza el Theme configurado, que tiene un directorio asociado donde se almacenan los datos 
    /// customizados del mismo: Imágenes, mensajes, posiciones, etc.
    /// </summary>
    public partial class frmMain : Form
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(frmMain));

        //Added 01-2023 para soporte mas eficiente de Secugen
        private SGFingerPrintManager m_FPM1;
        private SGFingerPrintManager m_FPM2;
        private SGFPMDeviceList[] m_DevList; // Used for EnumerateDevice
        private Hashtable _HT_SENSOR_ENABLED = new Hashtable();
        public string SerialSensor1 { get; private set; }
        public int ImageWidth1 { get; private set; }
        public int ImageHeight1 { get; private set; }
        public string SerialSensor2 { get; private set; }
        public int ImageWidth2 { get; private set; }
        public int ImageHeight2 { get; private set; }

        //Added 01-2023 para soporte mas eficiente de Secugen



        //Agrego para tomar foto en marcacion si está habilitado
        private Capture _CaptureCam;
        private string _Image_TakeFoto = null;
        private Image _CurrentImage = null;
        private Capture _CaptureCamIN;
        private Capture _CaptureCamOUT;
        private int _CameraINIndex = 0;
        private int _CameraOUTIndex = 1;

        internal frmInitSplash _FRM_INIT_SPLASH;

        //Variables globales
        internal static object OBJECT_TO_BLOCK = new object();

        //PAra descartar nuevas lecturas hasta terminar de procesar una entrada o salida
        internal static bool _IS_PROCESSING = false;

        //Usado par aidentificar una persona y comenzar el proceso de control de acceso. 
        //Se mantiene en nulo hasta tanto se identifique a alguien por alguna de las tecnologias
        //configuradas. Luego se lo procesoa y veuvle a poner en Null.
        string _ID_RECOGNIZED = null;

        //Indica que serial id de lector o de COM (barcode) leyo el _ID_RECOGNIZED
        //para enviar de parámetro a BusinessRule/Action/Mark para procesar 
        string _ID_DEVICE = null;

        //Barcode Variable para manejo de lector de código de barras
        private Broker broker;
        //Agrego para eliminar lecturas repetidas. (Ver LeerCedula)
        private string _LAST_BARCODE_READED = null;

        //Thread Control. Ahora no se está usando, se debe revisar si es necesario que el proceso y las acciones 
        //se realicen en Threads aparte. Se verá mas adelante. Ahora está comentado.
        Thread threadProcessSample;

        //Thread para recarga de templates 
        Thread _ThreadToRefrechBIRs;
        bool _IsFirstLoad = true;

        //Biometric Variables

        //Variable par amanejo de eventos.
        bool _IdentifyRunning = false;
            //Lista de samples obtenidos, para mostrar e identificar. 
        List<Sample> _SamplesCaptured;
            //Helper para manejo de Matchers, inicialización, procesod e identificación, etc.
        static BiometricHelper _BIOMETRIC_HELPER;

        //Helper para manejo de Theme en general
        internal ThemeHelper _THEME_HELPER;

        //Aun no se sabe si se utilizará. 
        //public Persona _PERSONA { get; private set; }

        /// <summary>
        /// Constructor para inicializar controles 
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Evento de inicio, donde se debe:
        /// 1.- Deshabilitar control de threads sino falla.
        /// 2.- Toma version y la muestra en la pantalla.
        /// 3.- Inicializa el Theme, y reemplaza ´los valore spor default por el Theme establecido.
        /// 4.- Inicializa las tecnologías que se usarán para modificación (Finger, Facial, Código de barras, etc)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                //CheckIllegal
                CheckForIllegalCrossThreadCalls = false;

                //Tomo version
                Version version = Assembly.GetExecutingAssembly().GetName().Version;
                labVersion.Text = "v" + version.ToString();

                labError.Text = "";
                labError.Refresh();

                _FRM_INIT_SPLASH.SetStep(3);
                //Inicializo theme
                _THEME_HELPER = new ThemeHelper();
                int reth = _THEME_HELPER.Initialize(Program._UOWKA._CONFIG.Theme);
                if (reth != 0)
                {
                    LOG.Warn("frmMain.frmMain_Load - Theme Initialize Error = " + reth.ToString() + " => Sale!");
                    Program._UOWKA._SUPPORT.GenerateTicket(Program._UOWKA._SUPPORT.SUPPORT_LEVEL_ERROR, "Ticket Automatico - Theme Initialize Con Error [" + reth.ToString() + "]",
                                             "Error al procesar theme. Revisar archivo de config y recursos de imagenes y demas configurados."
                                             + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                                             true);
                    MessageBox.Show("Error inicializando el Theme [" + reth.ToString() + "]", "Atención",
                                     MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Environment.Exit(0);
                } else
                {
                    SetTheme();
                }

                _FRM_INIT_SPLASH.SetStep(4);
                //Inicializo los metodos de verificación
                if (Program._UOWKA._CONFIG.AccessByFingerprintEnabled || Program._UOWKA._CONFIG.AccessByFacialEnabled)
                {
                    try
                    {

                        int ret = InitBiometricsSensors();

                        /*
                        //if (Program._BSP == null)
                        //    InitBSP();

                        //Error err;
                        //LOG.Info("frmMain.frmMain_Load - Seteando AutoEvent en Biometric...");
                        //int ret = Program._BSP.EnableAutoEvent(Program._UOWKA._CONFIG.SerialSensor1, true, (int)this.Handle, out err);
                        //if (ret != 0)
                        //{
                        //    LOG.Warn("frmMain.frmMain_Load - EnableAutoEvent = " +
                        //              (err != null ? err.ErrorCode.ToString() + "-" + err.ErrorDescription : ret.ToString()));
                        //}
                        //Program._BSP.On(Program._UOWKA._CONFIG.SerialSensor1);

                        //ret = Program._BSP.CaptureContinue(Program._UOWKA._CONFIG.SerialSensor1, Program._UOWKA._CONFIG.QualityCapture, out err);
                        //if (ret != 0)
                        //{
                        //    LOG.Warn("frmMain.frmMain_Load - CaptureContinue = " +
                        //              (err != null ? err.ErrorCode.ToString() + "-" + err.ErrorDescription : ret.ToString()));
                        //    //Program._UOWKA._SUPPORT.GenerateTicket("ERROR Ticket Automatico - Fingerprint Reader Initialize Con Error [" + ret.ToString() + "]",
                        //    //                 "Error al configurar lector de huella. Revisar BPS20 config."
                        //    //                 + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                        //    //                 true);
                        //}
                        //else
                        //{
                        //    _IdentifyRunning = true;
                        //    LOG.Info("frmMain.frmMain_Load Biometric - Biometric OK!");
                        //}

                        //if (!string.IsNullOrEmpty(Program._UOWKA._CONFIG.SerialSensor2) &&
                        //    Program._UOWKA._CONFIG.SerialSensor2.Length > 0 &&
                        //    !Program._UOWKA._CONFIG.SerialSensor2.Equals("NA")) 
                        //{
                        //    ret = Program._BSP.EnableAutoEvent(Program._UOWKA._CONFIG.SerialSensor2, true, (int)this.Handle, out err);
                        //    if (ret != 0)
                        //    {
                        //        LOG.Warn("frmMain.frmMain_Load - EnableAutoEvent = " +
                        //                  (err != null ? err.ErrorCode.ToString() + "-" + err.ErrorDescription : ret.ToString()));
                        //    }
                        //    Program._BSP.On(Program._UOWKA._CONFIG.SerialSensor2);

                        //    ret = Program._BSP.CaptureContinue(Program._UOWKA._CONFIG.SerialSensor2, Program._UOWKA._CONFIG.QualityCapture, out err);
                        //    if (ret != 0)
                        //    {
                        //        LOG.Warn("frmMain.frmMain_Load - CaptureContinue = " +
                        //                  (err != null ? err.ErrorCode.ToString() + "-" + err.ErrorDescription : ret.ToString()));
                        //        //Program._UOWKA._SUPPORT.GenerateTicket("ERROR Ticket Automatico - Fingerprint Reader Initialize Con Error [" + ret.ToString() + "]",
                        //        //                 "Error al configurar lector de huella. Revisar BPS20 config."
                        //        //                 + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                        //        //                 true);
                        //    }
                        //    else
                        //    {
                        //        _IdentifyRunning = true;
                        //        LOG.Info("frmMain.frmMain_Load Biometric - Biometric OK!");
                        //    }
                        //}
                        */

                        if (ret == 0)
                        {
                            //Carga Templates
                            _BIOMETRIC_HELPER = new BiometricHelper();
                            _BIOMETRIC_HELPER.Initalize();

                            if (_BIOMETRIC_HELPER != null && BiometricHelper.objTemplates != null)
                            {
                                labQTemplates.Text = "#QH: " + BiometricHelper.objTemplates.Count().ToString();
                            }
                            else
                            {
                                Program._UOWKA._SUPPORT.GenerateTicket(Program._UOWKA._SUPPORT.SUPPORT_LEVEL_WARN, "Ticket Automatico - List Template Initialize Vacio!",
                                                 "No se lvantarn templates desde BD aunque esta configurado que se use acceso con huella. Revisar conectividad con server de BD."
                                                 + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                                                 true);
                                labQTemplates.Text = "#QH: 0";
                            }

                            //TODO - Dejar Thread si se setea RefreshAutomatico en Config
                            if (Program._UOWKA._CONFIG.TimeToRefreshAutomaticBIRs > 0)
                            {
                                _ThreadToRefrechBIRs = new Thread(new ThreadStart(_ThreadRefreshBIRs));
                                _ThreadToRefrechBIRs.Start();
                            }
                            _IdentifyRunning = true;
                            //picReinit_Click(null, null);
                        } else
                        {
                            //Error informar
                        }
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("frmMain.frmMain_Load Error: " + ex.Message);
                    }

                    //Added para redondear imagen de sample
                    System.Drawing.Drawing2D.GraphicsPath objDraw = new System.Drawing.Drawing2D.GraphicsPath();
                    objDraw.AddEllipse(0, 0, this.picSample.Width, this.picSample.Height);
                    this.picSample.Region = new Region(objDraw);
                } else
                {
                    //Invisibilzo el Refresh para evitar confusiones
                    picReinit.Visible = false;
                    picReinit.Refresh();
                }

                _FRM_INIT_SPLASH.SetStep(5);
                if (Program._UOWKA._CONFIG.AccessByBarcodeEnabled)
                {
                    LOG.Info("frmMain.frmMain_Load - Seteando Barcode por primera vez...");
                    if (InitializeBarCode() ==0)
                    {
                        if (Program._UOWKA._CONFIG.TimerToReinitLCB > 0) //=> Habilito timer para que reinicie el puerto cada xxxx milisegundos 
                        {
                            timer_Reinit_LCB.Interval = Program._UOWKA._CONFIG.TimerToReinitLCB < 300000 ?
                                                            300000 : Program._UOWKA._CONFIG.TimerToReinitLCB;
                            //timer_Reinit_LCB.Interval = Program._UOWKA._CONFIG.TimerToReinitLCB;
                            LOG.Info("frmMain.frmMain_Load - Habilitando Reiniti LCB cada " + timer_Reinit_LCB.Interval + " milisegundos...");
                            timer_Reinit_LCB.Enabled = true;
                        }
                    }
                    //try
                    //{
                    //    LOG.Info("frmMain.frmMain_Load - Seteando Barcode...");
                    //    this.broker = new Broker();
                    //    broker.SampleAvailable += new SampleAvailableEvent(OnSample);
                    //    _IdentifyRunning = true;
                    //    LOG.Info("frmMain.frmMain_Load - Barcode OK!");
                    //}
                    //catch (Exception ex)
                    //{
                    //    LOG.Error("frmMain.frmMain_Load Barcode - Error: " + ex.Message);
                    //}
                }

                _FRM_INIT_SPLASH.SetStep(6);
                LOG.Info("frmMain.frmMain_Load - Set label...");
                //Cargo tipo de kiosko nombre para identificar
                if (Program._UOWKA._CONFIG.KioskType == 1 && 
                    (Program._UOWKA._CONFIG.KioskType1 == 1 && Program._UOWKA._CONFIG.KioskType2 == 0))
                {
                    labKioskType.Text = Program._UOWKA._CONFIG.Name + " - " + "Entrada";
                } else if (Program._UOWKA._CONFIG.KioskType == 1 &&
                    (Program._UOWKA._CONFIG.KioskType1 == 0 && Program._UOWKA._CONFIG.KioskType == 1))
                {
                    labKioskType.Text = Program._UOWKA._CONFIG.Name + " - " + "Salida";
                } else
                {
                    labKioskType.Text = Program._UOWKA._CONFIG.Name + " - " + "E/S";
                }
                labKioskType.Refresh();

                //TODO => Ocultar Splash!!
                if (_FRM_INIT_SPLASH != null)
                {
                    _FRM_INIT_SPLASH.Finish();
                    _FRM_INIT_SPLASH.Close();
                }

                //Added para Dummy para demos y ferias
                labBtnDummy.Visible = Program._UOWKA._CONFIG.IsDummy;
                txtRutManual.Visible = Program._UOWKA._CONFIG.IsDummy;
                btnINRutManual.Visible = Program._UOWKA._CONFIG.IsDummy;
                rbSalidaManual.Visible = Program._UOWKA._CONFIG.IsDummy;
                rbEntradaManual.Visible = Program._UOWKA._CONFIG.IsDummy;
                labDebug1.Visible = Program._UOWKA._CONFIG.IsDummy;
                labDebug2.Visible = Program._UOWKA._CONFIG.IsDummy;
                labDebug3.Visible = Program._UOWKA._CONFIG.IsDummy;
                labDebug4.Visible = Program._UOWKA._CONFIG.IsDummy;

                //Added para tomar el Index de la camra de IN y OUT cuando es doble factor de verificacion              
                if (Program._UOWKA._CONFIG.MFAEnable && Program._UOWKA._CONFIG.MFAType == (int)Common.Model.Contants.MAFTypes.MAFType_BARCODE_FACIAL)
                {
                    string msgerr;
                    int ret = DetectIndexCameras(out msgerr);

                    if (ret != 0)
                    {
                        LOG.Warn("frmMain_Load - Error Detectando Indices de Camaras! => ret = " + ret.ToString() + ". Sale...");
                        Program._UOWKA._SUPPORT.GenerateTicket(Program._UOWKA._SUPPORT.SUPPORT_LEVEL_ERROR, "Ticket Automatico - Kiosk UI Load Error [Detect Cameras]",
                                                     "Error Detectando Indices de Camaras! [Ret = " + ret.ToString() 
                                                     + " -  MsgErr = " + (!string.IsNullOrEmpty(msgerr) ? msgerr : "Sin detalles, revise log...") + "]"
                                                     + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                                                     true);
                        MessageBox.Show(this, "Error detectando cámaras para reconocmiento facial. Revise configuración y reinicie...", "Atención...",
                                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }

                //if (Program._UOWKA._CONFIG.TakeFotoInMark)
                //{
                    //_CaptureCam = new Capture();
                    ////Application.Idle += ShowCamStream;
                    //_CaptureCam.Stop();
                //}

                this.Refresh();
            }
            catch (Exception ex)
            {
                LOG.Error("frmMain_Load Excp Error: ", ex);
                Program._UOWKA._SUPPORT.GenerateTicket(Program._UOWKA._SUPPORT.SUPPORT_LEVEL_ERROR, "Ticket Automatico - Kiosk UI Load Error [" + ex.Message + "]",
                                             "Hubo un error en el frm_Load del Kiosk UI. Revisar log para determinar problema."
                                             + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                                             true);
            }
            LOG.Info("frmMain.frmMain_Load - Kiosk [" + labKioskType.Text + "] Started OK!");

        }

      





        /// <summary>
        /// Inicializa el BArcode, abre el puerto y asocia evento para lectura
        /// </summary>
        internal int InitializeBarCode()
        {
            int ret = 0;
            try
            {
                LOG.Info("frmMain.InitializeBarCode - Seteando Barcode...");
                this.broker = new Broker();
                broker.SampleAvailable += new SampleAvailableEvent(OnSample);
                _IdentifyRunning = true;
                LOG.Info("frmMain.InitializeBarCode - Barcode OK!");
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("frmMain.InitializeBarCode Barcode - Error: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Timer que se habilita para reiniciar el BC, cerrando y abriendo el puerto por si se pierde y ya no 
        /// se reciben los eventos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        private void timer_Reinit_LCB_Tick(object sender, EventArgs e)
        {
            LOG.Debug("frmMain.timer_Reinit_LCB_Tick - IN...");
            try
            {
                _IdentifyRunning = false;
                if (this.broker != null) {
                    try
                    {
                        LOG.Debug("frmMain.timer_Reinit_LCB_Tick - Dispose broker...");
                        this.broker.Dispose();
                        LOG.Debug("frmMain.timer_Reinit_LCB_Tick - Free broker...");
                        this.broker = null;
                    }
                    catch (Exception ex)
                    {
                        LOG.Debug("frmMain.timer_Reinit_LCB_Tick - Free broker w/Excp...");
                        this.broker = null;
                    }
                }

                LOG.Debug("frmMain.timer_Reinit_LCB_Tick - Call InitializeBarCode...");
                int ret = InitializeBarCode();
                LOG.Debug("frmMain.timer_Reinit_LCB_Tick - InitializeBarCode ret = " + ret.ToString() +"!");
            }
            catch (Exception ex)
            {
                LOG.Error("frmMain.timer_Reinit_LCB_Tick Excp => " + ex.Message);
            }
            LOG.Debug("frmMain.timer_Reinit_LCB_Tick - OUT!");
        }

        #region Camera Zone

        FilterInfoCollection filter;
        VideoCaptureDevice device;
        /// <summary>
        /// Dada la configuracion de los cameraid seteados, se busca el indice que teine asignado para poder aegurar la captura en IN y OUT
        /// </summary>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        private int DetectIndexCameras(out string msgerr)
        {
            int ret = -1;
            int qCamaras = 0;
            msgerr = null;
            LOG.Debug("frmMain.DetectIndexCameras - IN...");
            try
            {
                filter = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                int index = 0;
                foreach (FilterInfo device in filter)
                {
                    var deviceN = new VideoCaptureDevice(filter[index].MonikerString);
                    string _id = deviceN.Source.Substring(deviceN.Source.IndexOf("usb#"),
                                                          deviceN.Source.LastIndexOf("#") - deviceN.Source.IndexOf("usb#"));
                    _id = _id.Replace("&", "-");
                    if (_id.Contains( Program._UOWKA._CONFIG.CameraId1) && Program._UOWKA._CONFIG.MFAControlType != 2) //Si debe controlar entrada
                    {
                        _CameraINIndex = index;
                        LOG.Debug("frmMain.DetectIndexCameras - CameraId1 = " + Program._UOWKA._CONFIG.CameraId1 + " => Camera IN Index = " + _CameraINIndex.ToString());
                        qCamaras++;
                    }
                    else if (_id.Contains(Program._UOWKA._CONFIG.CameraId2) && Program._UOWKA._CONFIG.MFAControlType > 1) //Si debe controlar salida
                    {
                        _CameraOUTIndex = index;
                        LOG.Debug("frmMain.DetectIndexCameras - CameraId2 = " + Program._UOWKA._CONFIG.CameraId2 + " => Camera OUT Index = " + _CameraOUTIndex.ToString());
                        qCamaras++;
                    }
                    index++;
                }
                if (qCamaras == 1 && Program._UOWKA._CONFIG.MFAControlType < 3)
                {
                    ret = 0;
                } else if (qCamaras == 2 && Program._UOWKA._CONFIG.MFAControlType == 3)
                {
                    ret = 0;
                } else
                {
                    ret = -2;
                }
                //ret = (qCamaras == 2) ? 0 : -2; //Para manejar el mesnaje 
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "DetectIndexCameras Excp [" + ex.Message + "]";
                LOG.Error("frmMain.DetectIndexCameras Excp => " + ex.Message);
            }
            LOG.Debug("frmMain.DetectIndexCameras - OUT! [ret = " + ret.ToString() + " - qCamaras =" + qCamaras.ToString() + "]");
            return ret;
        }


        Mat mat;
        Image<Bgr, Byte> img;
        private string TakeFoto(int flag)
        {
            try
            {

                switch (flag)
                {
                    case 0: //Captura foto para totem tipo falabella, solo una opcion
                        if (_CaptureCam == null)
                        {
                            _CaptureCam = new Capture(Program._UOWKA._CONFIG.CameraIndexToUse);
                            _CaptureCam.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 640);
                            _CaptureCam.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 480);
                        }
                        mat = _CaptureCam.QueryFrame();
                        mat = _CaptureCam.QueryFrame(); //Leo de nuevo porque sino se queda con la imagen anterior. NO SE PORQUE AUN!!
                        img = mat.ToImage<Bgr, Byte>();
                        _CurrentImage = img.ToBitmap(); // (Image)picVideo.Image.Clone();
                        _Image_TakeFoto = GetB64FromPictureBox(_CurrentImage);
                        mat.Dispose();
                        img.Dispose();
                        mat = null;
                        img = null;
                        break;
                    case 1: //Captura foto en IN
                        if (_CaptureCamIN == null)
                        {
                            _CaptureCamIN = new Capture(_CameraINIndex);
                            _CaptureCamIN.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 1280);
                            _CaptureCamIN.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 720);
                        }
                        mat = _CaptureCamIN.QueryFrame();
                        mat = _CaptureCamIN.QueryFrame(); //Leo de nuevo porque sino se queda con la imagen anterior. NO SE PORQUE AUN!!
                        img = mat.ToImage<Bgr, Byte>();
                        _CurrentImage = img.ToBitmap(); // (Image)picVideo.Image.Clone();
                        _Image_TakeFoto = GetB64FromPictureBox(_CurrentImage);
                        mat.Dispose();
                        img.Dispose();
                        mat = null;
                        img = null;
                        break;
                        break;
                    case 2: //Captura foto en OUT
                        if (_CaptureCamOUT == null)
                        {
                            _CaptureCamOUT = new Capture(_CameraOUTIndex);
                            _CaptureCamOUT.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 1280);
                            _CaptureCamOUT.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 720);
                        }
                        mat = _CaptureCamOUT.QueryFrame();
                        mat = _CaptureCamOUT.QueryFrame(); //Leo de nuevo porque sino se queda con la imagen anterior. NO SE PORQUE AUN!!
                        img = mat.ToImage<Bgr, Byte>();
                        _CurrentImage = img.ToBitmap(); // (Image)picVideo.Image.Clone();
                        _Image_TakeFoto = GetB64FromPictureBox(_CurrentImage);
                        mat.Dispose();
                        img.Dispose();
                        mat = null;
                        img = null;
                        break;
                        break;
                        break;

                    default:
                        break;
                }
            }
            catch (Exception)
            {
                _CurrentImage = null;
                _Image_TakeFoto = null;
            }
            return _Image_TakeFoto;
        }
    
        private void ShowCamStream(object sender, EventArgs e)
        {
            try
            {
                Mat mat = new Mat();
                _CaptureCam.Retrieve(mat);
                Image<Bgr, Byte> img = mat.ToImage<Bgr, Byte>();
                picVideo.Image = img.ToBitmap();
            }
            catch (Exception ex)
            {
                LOG.Info("frmMain.ShowCamStream - Excp [" + ex.Message + "]");
            }
        }

        private string GetB64FromPictureBox(Image image)
        {
            return ImageToBase64(image, System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        public string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        #endregion Camera Zone

        #region Theme Zone 

        /// <summary>
        /// En el momento de la inicialización del UI del Kiosko, se setea el Theme. Esto significa reemplazo de imágenes 
        /// customizadas, menajes customizados, y posiciones de acuerdo a la resolución configurada.
        /// De no encontrar un valor se utiliza el default.
        /// </summary>
        private void SetTheme()
        {
            try
            {
                LOG.Debug("frmMain.SetTheme IN...");

    #region Pantalla Principal

                LOG.Debug("frmMain.SetTheme - StyleFormManin...");
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleFormManin"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleFormManin"])).Split('|');
                        //this.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                    }
                    catch (Exception ex)
                    {
                        //this.picFotoIdentified.Size = new Size(200,200);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleFormManin...");
                }


                LOG.Debug("frmMain.SetTheme - ImageBackground...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageBackground"))
                {
                    this.BackgroundImage = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageBackground"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageBackground...");
                }

                LOG.Debug("frmMain.SetTheme - ImageTechFinger...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageTechFinger"))
                {
                    this.picFingerEnabled.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageTechFinger"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageTechFinger...");
                }
                //Style
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleImageTechFinger"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleImageTechFinger"])).Split('|');
                        this.picFingerEnabled.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picFingerEnabled.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));

                    }
                    catch (Exception ex)
                    {
                        //this.picFingerEnabled.Location = new Point(34, 241);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleImageTechFinger...");
                }

                LOG.Debug("frmMain.SetTheme - ImageTechFacial...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageTechFacial"))
                {
                    this.picFacialEnabled.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageTechFacial"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageTechFacial...");
                }
                //Style
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleImageTechFacial"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleImageTechFacial"])).Split('|');
                        this.picFacialEnabled.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picFacialEnabled.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                    }
                    catch (Exception ex)
                    {
                        //this.picFingerEnabled.Location = new Point(34, 241);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleImageTechFacial...");
                }

                LOG.Debug("frmMain.SetTheme - ImageTechLCB...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageTechLCB"))
                {
                    this.picLCBEnabled.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageTechLCB"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageTechLCB...");
                }
                //Style
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleImageImageTechLCB"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleImageImageTechLCB"])).Split('|');
                        this.picLCBEnabled.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picLCBEnabled.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                    }
                    catch (Exception ex)
                    {
                        //this.picFingerEnabled.Location = new Point(34, 241);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleImageImageTechLCB...");
                }


                LOG.Debug("frmMain.SetTheme - ImageTechMRZ...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageTechMRZ"))
                {
                    this.picMRZEnabled.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageTechMRZ"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageTechMRZ...");
                }
                //Style
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleImageImageTechMRZ"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleImageImageTechMRZ"])).Split('|');
                        this.picMRZEnabled.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picMRZEnabled.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                    }
                    catch (Exception ex)
                    {
                        //this.picFingerEnabled.Location = new Point(34, 241);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleImageImageTechMRZ...");
                }


                LOG.Debug("frmMain.SetTheme - ImageTechPIN...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageTechPIN"))
                {
                    this.picPINEnabled.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageTechPIN"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageTechPIN...");
                }
                //Style
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleImageImageTechPIN"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleImageImageTechPIN"])).Split('|');
                        this.picPINEnabled.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picPINEnabled.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                    }
                    catch (Exception ex)
                    {
                        //this.picFingerEnabled.Location = new Point(34, 241);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleImageImageTechPIN...");
                }

                //Botones angulo superior derecho
                LOG.Debug("frmMain.SetTheme - ImageBtnPicClose...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageBtnPicClose"))
                {
                    this.picClose.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageBtnPicClose"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageBtnPicClose...");
                }
                //Style
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleImageBtnPicClose"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleImageBtnPicClose"])).Split('|');
                        this.picClose.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picClose.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                    }
                    catch (Exception ex)
                    {
                        //this.picFingerEnabled.Location = new Point(34, 241);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleImageBtnPicClose...");
                }


                LOG.Debug("frmMain.SetTheme - ImageBtnPicAcercaDe...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageBtnPicAcercaDe"))
                {
                    this.picAcercaDe.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageBtnPicAcercaDe"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageBtnPicAcercaDe...");
                }
                //Style
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleImageBtnPicAcercaDe"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleImageBtnPicAcercaDe"])).Split('|');
                        this.picAcercaDe.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picAcercaDe.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                    }
                    catch (Exception ex)
                    {
                        //this.picFingerEnabled.Location = new Point(34, 241);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleImageBtnPicAcercaDe...");
                }

                LOG.Debug("frmMain.SetTheme - ImageBtnPicSupport...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageBtnPicSupport"))
                {
                    this.picSupport.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageBtnPicSupport"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageBtnPicSupport...");
                }
                //Style
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleImageBtnPicSupport"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleImageBtnPicSupport"])).Split('|');
                        this.picSupport.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picSupport.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                    }
                    catch (Exception ex)
                    {
                        //this.picFingerEnabled.Location = new Point(34, 241);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleImageBtnPicSupport...");
                }

                LOG.Debug("frmMain.SetTheme - ImageBtnPicReinit...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageBtnPicReinit"))
                {
                    this.picReinit.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageBtnPicReinit"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageBtnPicReinit...");
                }
                //Style
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleImageBtnPicReinit"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleImageBtnPicReinit"])).Split('|');
                        this.picReinit.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picReinit.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                    }
                    catch (Exception ex)
                    {
                        //this.picFingerEnabled.Location = new Point(34, 241);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleImageBtnPicReinit...");
                }

                if (Program._UOWKA._CONFIG.ShowReloj)
                {
                    //labReloj.Visible = true;
                    LOG.Debug("frmMain.SetTheme - Set IN Visible Reloj...");
                    if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleLabReloj"))
                    {
                        try
                        {
                            //10|130|88|70|Courier New|14|#ffc0c0
                            string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleLabReloj"])).Split('|');
                            this.labReloj.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                            this.labReloj.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                            float sizeFont = (float)Convert.ToDouble(xy[5]);
                            this.labReloj.Font = new Font(xy[4], sizeFont);
                            this.labReloj.ForeColor = System.Drawing.ColorTranslator.FromHtml(xy[6]);
                            labReloj.Text = DateTime.Now.ToString("HH:mm");
                            labReloj.Visible = true;
                            labReloj.Refresh();
                            timerReloj.Enabled = true;
                        }
                        catch (Exception ex)
                        {
                            //this.labKioskType.Location = new Point(235, 95);
                            //LOG.Error(" Error: " + ex.Message);
                        }
                        LOG.Debug("frmMain.SetTheme - Seted new StyleLabReloj...");
                    } else
                    {
                        LOG.Warn("frmMain.SetTheme - Falta Config en Theme!");
                    }
                } else
                {
                    labReloj.Visible = false;
                }

                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleGrpRefresh"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleGrpRefresh"])).Split('|');
                        this.grpRefreshLoadBIRs.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.grpRefreshLoadBIRs.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                    }
                    catch (Exception ex)
                    {
                        //this.grpRefreshLoadBIRs.Location = new Point(388, 344);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleGrpRefresh...");
                }

                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleLabError"))
                {
                    try
                    {
                        //10|130|88|70|Courier New|14|#ffc0c0
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleLabError"])).Split('|');
                        this.labError.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.labError.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                        float sizeFont = (float)Convert.ToDouble(xy[5]);
                        this.labError.Font = new Font(xy[4],sizeFont);
                        this.labError.ForeColor = System.Drawing.ColorTranslator.FromHtml(xy[6]);
                    }
                    catch (Exception ex)
                    {
                        this.labError.Location = new Point(235,95);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new PoslabError...");
                }

                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleLabKioskType"))
                {
                    try
                    {
                        //10|130|88|70|Courier New|14|#ffc0c0
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleLabKioskType"])).Split('|');
                        this.labKioskType.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.labKioskType.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                        float sizeFont = (float)Convert.ToDouble(xy[5]);
                        this.labKioskType.Font = new Font(xy[4], sizeFont);
                        this.labKioskType.ForeColor = System.Drawing.ColorTranslator.FromHtml(xy[6]);
                    }
                    catch (Exception ex)
                    {
                        //this.labKioskType.Location = new Point(235, 95);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleLabKioskType...");
                }
                 
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleLabQTemplates"))
                {
                    try
                    {
                        //10|130|88|70|Courier New|14|#ffc0c0
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleLabQTemplates"])).Split('|');
                        this.labQTemplates.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.labQTemplates.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                        float sizeFont = (float)Convert.ToDouble(xy[5]);
                        this.labQTemplates.Font = new Font(xy[4], sizeFont);
                        this.labQTemplates.ForeColor = System.Drawing.ColorTranslator.FromHtml(xy[6]);
                    }
                    catch (Exception ex)
                    {
                        //this.labKioskType.Location = new Point(235, 95);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleLabQTemplates...");
                }
                
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleLabVersion"))
                {
                    try
                    {
                        //10|130|88|70|Courier New|14|#ffc0c0
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleLabVersion"])).Split('|');
                        this.labVersion.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.labVersion.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                        float sizeFont = (float)Convert.ToDouble(xy[5]);
                        this.labVersion.Font = new Font(xy[4], sizeFont);
                        this.labVersion.ForeColor = System.Drawing.ColorTranslator.FromHtml(xy[6]);
                    }
                    catch (Exception ex)
                    {
                        //this.labKioskType.Location = new Point(235, 95);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleLabVersion...");
                }


                LOG.Debug("frmMain.SetTheme - ImageWorking...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageWorking"))
                {
                    this.panelWorking.BackgroundImage = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageWorking"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageWorking...");
                }
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleGrpWorking"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleGrpWorking"])).Split('|');
                        this.panelWorking.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.panelWorking.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                        float sizeFont = (float)Convert.ToDouble(xy[5]);
                        this.panelWorking.Font = new Font(xy[4], sizeFont);
                        this.panelWorking.ForeColor = System.Drawing.ColorTranslator.FromHtml(xy[6]);
                    }
                    catch (Exception ex)
                    {
                        //this.picFotoIdentified.Size = new Size(200,200);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleGrpWorking...");
                }

                //Muestro GrpPIN
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleGrpPIN"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleGrpPIN"])).Split('|');
                        this.grpPIN.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.grpPIN.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                        float sizeFont = (float)Convert.ToDouble(xy[5]);
                        this.grpPIN.Font = new Font(xy[4], sizeFont);
                        this.grpPIN.ForeColor = System.Drawing.ColorTranslator.FromHtml(xy[6]);
                    }
                    catch (Exception ex)
                    {
                        //this.picFotoIdentified.Size = new Size(200,200);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleGrpWorking...");
                }


                //Muestro Video de camara para captura de fotos en el moento de marcar si está habilitado sino oculto solo 
                //para operar pero sin mostrar imagen
                if (Program._UOWKA._CONFIG.TakeFotoInMark && 
                    _THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleShowVideo"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleShowVideo"])).Split('|');
                        //this.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picVideo.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picVideo.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                        this.picVideo.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        //this.picFotoIdentified.Size = new Size(200,200);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleFormManin...");
                } else
                {
                    this.picVideo.Visible = false;
                }
                this.Refresh();
                #endregion Pantalla Principal

                #region ShowResult

                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleGrpShowResult"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleGrpShowResult"])).Split('|');
                        this.grpShowResult.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.grpShowResult.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                    }
                    catch (Exception ex)
                    {
                        //this.picFotoIdentified.Size = new Size(200,200);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleGrpShowResult...");
                }


                //Cuadros de Textos
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleLabFeedbackName"))
                {
                    try
                    {
                        //10|130|88|70|Courier New|14|#ffc0c0
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleLabFeedbackName"])).Split('|');
                        this.labFeedbackName.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.labFeedbackName.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                        float sizeFont = (float)Convert.ToDouble(xy[5]);
                        this.labFeedbackName.Font = new Font(xy[4], sizeFont);
                        this.labFeedbackName.ForeColor = System.Drawing.ColorTranslator.FromHtml(xy[6]);
                    }
                    catch (Exception ex)
                    {
                        //this.labError.Location = new Point(235, 95);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleLabFeedbackName...");
                }

                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleLabFeedbackDetail"))
                {
                    try
                    {
                        //10|130|88|70|Courier New|14|#ffc0c0
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleLabFeedbackDetail"])).Split('|');
                        this.labFeedbackDetail.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.labFeedbackDetail.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                        float sizeFont = (float)Convert.ToDouble(xy[5]);
                        this.labFeedbackDetail.Font = new Font(xy[4], sizeFont);
                        this.labFeedbackDetail.ForeColor = System.Drawing.ColorTranslator.FromHtml(xy[6]);
                    }
                    catch (Exception ex)
                    {
                        //this.labError.Location = new Point(235, 95);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleLabFeedbackDetail...");
                }

                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StyleLabAntipassback"))
                {
                    try
                    {
                        //10|130|88|70|Courier New|14|#ffc0c0
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StyleLabAntipassback"])).Split('|');
                        this.labAntipassback.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.labAntipassback.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                        float sizeFont = (float)Convert.ToDouble(xy[5]);
                        this.labAntipassback.Font = new Font(xy[4], sizeFont);
                        this.labAntipassback.ForeColor = System.Drawing.ColorTranslator.FromHtml(xy[6]);
                    }
                    catch (Exception ex)
                    {
                        //this.labError.Location = new Point(235, 95);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StyleLabAntipassback...");
                }
                
                //imagenes
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StylePicFotoIdentified"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StylePicFotoIdentified"])).Split('|');
                        this.picFotoIdentified.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picFotoIdentified.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                        if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultNoFoto"))
                        {
                            this.picFotoIdentified.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultNoFoto"];
                            LOG.Debug("frmMain.SetTheme - Seted new ImageFondoResultNoFoto...");
                        }
                    }
                    catch (Exception ex)
                    {
                        //this.picFotoIdentified.Size = new Size(200,200);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new SizepicFotoIdentified...");
                }

                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StylePicSample"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StylePicSample"])).Split('|');
                        this.picSample.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picSample.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                    }
                    catch (Exception ex)
                    {
                        //this.picFotoIdentified.Size = new Size(200,200);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StylePicSample...");
                }

                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("StylePicResultAction"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["StylePicResultAction"])).Split('|');
                        this.picResultAction.Location = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                        this.picResultAction.Size = new Size(Convert.ToInt32(xy[2]), Convert.ToInt32(xy[3]));
                    }
                    catch (Exception ex)
                    {
                        //this.picFotoIdentified.Size = new Size(200,200);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new StylePicResultAction...");
                }



                grpShowResult.BackgroundImage = null;

    #endregion ShowResult
            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }
        }

        /*  **** SetTheme con POSICION HORIZONTAL ****
         private void SetTheme()
        {
            try
            {
                LOG.Debug("frmMain.SetTheme IN...");

                LOG.Debug("frmMain.SetTheme - ImageBackground...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageBackground"))
                {
                    this.BackgroundImage = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageBackground"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageBackground...");
                }

                LOG.Debug("frmMain.SetTheme - ImageTechFinger...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageTechFinger"))
                {
                    this.picFingerEnabled.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageTechFinger"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageTechFinger...");
                }

                LOG.Debug("frmMain.SetTheme - ImageTechFacial...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageTechFacial"))
                {
                    this.picFacialEnabled.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageTechFacial"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageTechFacial...");
                }

                LOG.Debug("frmMain.SetTheme - ImageTechLCB...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageTechLCB"))
                {
                    this.picLCBEnabled.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageTechLCB"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageTechLCB...");
                }
                LOG.Debug("frmMain.SetTheme - ImageTechMRZ...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageTechMRZ"))
                {
                    this.picMRZEnabled.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageTechMRZ"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageTechMRZ...");
                }
                LOG.Debug("frmMain.SetTheme - ImageTechPIN...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageTechPIN"))
                {
                    this.picPINEnabled.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageTechPIN"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageTechPIN...");
                }


                LOG.Debug("frmMain.SetTheme - ImageClose...");
                if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageClose"))
                {
                    this.picClose.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageClose"];
                    LOG.Debug("frmMain.SetTheme - Seted new ImageClose...");
                }

                //Added 13/08/2021 para cambios entre horizontal y vertical
                LOG.Debug("frmMain.SetTheme - Posicion Iconos...");
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("PosImageTechFinger"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["PosImageTechFinger"])).Split('|');
                        this.picFingerEnabled.Location = new Point(Convert.ToInt32(xy[0]),
                                                                   Convert.ToInt32(xy[1]));

                    }
                    catch (Exception ex)
                    {
                        this.picFingerEnabled.Location = new Point(34, 241);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new PosImageTechFinger...");
                }

                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("PosImageTechFacial"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["PosImageTechFacial"])).Split('|');
                        this.picFacialEnabled.Location = new Point(Convert.ToInt32(xy[0]),
                                                                   Convert.ToInt32(xy[1]));

                    }
                    catch (Exception ex)
                    {
                        this.picFacialEnabled.Location = new Point(278, 445);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new PosImageTechFinger...");
                }

                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("PosImageTechPIN"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["PosImageTechPIN"])).Split('|');
                        this.picPINEnabled.Location = new Point(Convert.ToInt32(xy[0]),
                                                                   Convert.ToInt32(xy[1]));

                    }
                    catch (Exception ex)
                    {
                        this.picPINEnabled.Location = new Point(779, 445);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new PosImageTechPIN...");
                }

                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("PosImageTechLCB"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["PosImageTechLCB"])).Split('|');
                        this.picLCBEnabled.Location = new Point(Convert.ToInt32(xy[0]),
                                                                   Convert.ToInt32(xy[1]));

                    }
                    catch (Exception ex)
                    {
                        this.picLCBEnabled.Location = new Point(528, 241);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new PosImageTechLCB...");
                }

                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("PosImageTechMRZ"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["PosImageTechMRZ"])).Split('|');
                        this.picMRZEnabled.Location = new Point(Convert.ToInt32(xy[0]),
                                                                   Convert.ToInt32(xy[1]));

                    }
                    catch (Exception ex)
                    {
                        this.picMRZEnabled.Location = new Point(1024, 241);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new PosImageTechMRZ...");
                }

                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("PosgrpRefreshLoadBIRsPos"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["PosgrpRefreshLoadBIRsPos"])).Split('|');
                        this.grpRefreshLoadBIRs.Location = new Point(Convert.ToInt32(xy[0]),
                                                                   Convert.ToInt32(xy[1]));

                    }
                    catch (Exception ex)
                    {
                        this.grpRefreshLoadBIRs.Location = new Point(388, 344);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Debug("frmMain.SetTheme - Seted new PosImageTechMRZ...");
                }
                

                grpShowResult.BackgroundImage = null;

                LOG.Debug("frmMain.SetTheme - ImageAccesoPermitido...");
                //if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageAccesoPermitido"))
                //{
                //    this.picLCBEnabled.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageAccesoPermitido"];
                //    LOG.Debug("frmMain.SetTheme - Seted new ImageAccesoPermitido...");
                //}
                //LOG.Debug("frmMain.SetTheme - ImageAccesoDenegado...");
                //if (_THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageAccesoDenegado"))
                //{
                //    this.picLCBEnabled.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageAccesoDenegado"];
                //    LOG.Debug("frmMain.SetTheme - Seted new ImageAccesoDenegado...");
                //}

                //Tamaño de Form y posicion
                if (_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems.ContainsKey("SizeFormManin"))
                {
                    try
                    {
                        string[] xy = ((string)(_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["SizeFormManin"])).Split('|');
                        this.Size = new Size(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));
                    }
                    catch (Exception ex)
                    {
                        this.Size = new Size(1280,800);
                        //LOG.Error(" Error: " + ex.Message);
                    }
                    this.Refresh();
                    LOG.Debug("frmMain.SetTheme - Seted Size FormMain...");
                }

                this.grpShowResult.Width = (int)_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["grpShowResultWidth"];

                this.grpShowResult.Width = (int)_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["grpShowResultWidth"];
                this.grpShowResult.Height = (int)_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["grpShowResultHeight"];
                this.grpShowResult.Location = new Point((int)_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["grpShowResultPosX"],
                                                        (int)_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["grpShowResultPosY"]);
                this.grpShowResult.Refresh();
                this.grpPIN.Width = (int)_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["grpShowResultWidth"];
                this.grpPIN.Height = (int)_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["grpShowResultHeight"];
                this.grpPIN.Location = new Point((int)_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["grpShowResultPosX"],
                                                        (int)_THEME_HELPER._THEME.Config.StylesList.DynamicDataItems["grpShowResultPosY"]);

                //ImageList.AddItem("ImageTechMRZ", BasePathTheme + "ImageTechMRZ.jpg");
                //ImageList.AddItem("ImageAccesoPermitido", BasePathTheme + "ImageAccesoPermitido.jpg");
                //ImageList.AddItem("ImageAccesoDenegado", BasePathTheme + "ImageAccesoDenegado.jpg");
            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }
        } 
         
        */
        #endregion Theme Zone 


        #region Biometric Zone

        bool _PROCESA_FINGERSAMPLE = false;

        /// <summary>
        /// Evento del lector de secugen, que permite capturar una huella. Esto es necesario solo por como 
        /// trabaja Secugen, en el modo de AutoEvent. 
        /// </summary>
        /// <param name="message"></param>
        protected override void WndProc(ref Message message)
        {
            try
            {
                int res = 0;
                if (_IdentifyRunning == true)
                {
                    if (message.Msg == (int)SGFPMMessages.DEV_AUTOONEVENT)
                    {
                        if (message.WParam.ToInt32() == (Int32)SGFPMAutoOnEvent.FINGER_ON)
                        {
                            CaptureContinue(50);
                            //Error err;
                            ////res = _SCUGEN_SENSOR.CaptureContinue("H54181019063", 50, out err);// bcrFpr.Capture();
                            //LOG.Debug("frmMain.WndProc - Entro a WndProc...");
                            //res = Program._BSP.CaptureContinue(Program._UOWKA._CONFIG.SerialSensor1, Program._UOWKA._CONFIG.QualityCapture, out err);// bcrFpr.Capture();
                            //if (!string.IsNullOrEmpty(Program._UOWKA._CONFIG.SerialSensor2) &&
                            //    Program._UOWKA._CONFIG.SerialSensor2.Length > 0 &&
                            //    !Program._UOWKA._CONFIG.SerialSensor2.Equals("NA"))
                            //{
                            //    res = Program._BSP.CaptureContinue(Program._UOWKA._CONFIG.SerialSensor2, Program._UOWKA._CONFIG.QualityCapture, out err);// bcrFpr.Capture();
                            //}


                        }
                        else if (message.WParam.ToInt32() == (Int32)SGFPMAutoOnEvent.FINGER_OFF)
                        {
                        }
                    }
                }
                base.WndProc(ref message);
            }
            catch (Exception)
            {

            }
        }

        #region Secugen Puro


        private int InitBiometricsSensors()
        {
            int ret = 0;
            try
            {
                LOG.Info("frmMain.InitBiometricsSensors IN...");
                m_FPM1 = new SGFingerPrintManager();
                if (!string.IsNullOrEmpty(Program._UOWKA._CONFIG.SerialSensor2) &&
                    Program._UOWKA._CONFIG.SerialSensor2.Length > 0 &&
                    !Program._UOWKA._CONFIG.SerialSensor2.Equals("NA"))
                {
                    m_FPM2 = new SGFingerPrintManager();
                }
                string enum_device;
                string auxSerial = null;

                // Enumerate Device
                SGFPMDeviceName device_name;
                Int32 device_id;
                int qSensors = m_FPM1.EnumerateDevice();
                int iError = 0;

                // Get enumeration info into SGFPMDeviceList
                m_DevList = new SGFPMDeviceList[m_FPM1.NumberOfDevice];
                LOG.Info("frmMain.InitBiometricsSensors - Cantidad de sensores conectados = " + m_DevList.Length);
                bool _Sensor1_OK = false;
                //Seteo en true si hay que configurar dispositivo de salida sino false
                bool _Sensor2_OK = (!string.IsNullOrEmpty(Program._UOWKA._CONFIG.SerialSensor2) &&
                                    Program._UOWKA._CONFIG.SerialSensor2.Length > 0 &&
                                    !Program._UOWKA._CONFIG.SerialSensor2.Equals("NA")) ? false : true;
                int i = 0;
                //for (int i = 0; i < m_DevList.Length; i++)
                while((!_Sensor1_OK || !_Sensor2_OK) && i < m_DevList.Length)
                {
                    auxSerial = null;
                    if (!_Sensor1_OK) //(i == 0)
                    {
                        LOG.Info("frmMain.InitBiometricsSensors - Procesando _CONFIG.SerialSensor1: " + Program._UOWKA._CONFIG.SerialSensor1 + "...");
                        m_DevList[i] = new SGFPMDeviceList();
                        m_FPM1.GetEnumDeviceInfo(i, m_DevList[i]);
                        enum_device = m_DevList[i].DevName.ToString() + " : " + m_DevList[i].DevID;
                        auxSerial = new ASCIIEncoding().GetString(m_DevList[i].DevSN);
                        auxSerial = auxSerial.Replace("\0", "").Trim();
                        if (auxSerial.Equals(Program._UOWKA._CONFIG.SerialSensor1))
                        {
                            device_name = m_DevList[i].DevName;
                            device_id = m_DevList[i].DevID;
                            iError = m_FPM1.Init(device_name);
                            iError = m_FPM1.OpenDevice(device_id);
                            m_FPM1.SetLedOn(true);
                            m_FPM1.EnableAutoOnEvent(true, (int)this.Handle);
                            if (iError == 0)
                            {
                                SGFPMDeviceInfoParam pInfo = new SGFPMDeviceInfoParam();
                                iError = m_FPM1.GetDeviceInfo(pInfo);
                                ImageWidth1 = pInfo.ImageWidth;
                                ImageHeight1 = pInfo.ImageHeight;
                                SerialSensor1 = auxSerial;
                                LOG.Info("frmMain.InitBiometricsSensors - Lector Iniciado: " + enum_device + " - " + SerialSensor1 + " - WxH = " +
                                            ImageWidth1 + "x" + ImageHeight1);
                                _HT_SENSOR_ENABLED.Add(auxSerial, m_DevList[i]);
                                _Sensor1_OK = true;
                            }
                            else
                            {
                                LOG.Error("frmMain.InitBiometricsSensors - Error abriendo Program._UOWKA._CONFIG.SerialSensor1 => " + iError);
                                return -2;
                            }
                            //i++;
                        }
                    }
                    
                    if (!string.IsNullOrEmpty(Program._UOWKA._CONFIG.SerialSensor2) &&
                            Program._UOWKA._CONFIG.SerialSensor2.Length > 0 &&
                            !Program._UOWKA._CONFIG.SerialSensor2.Equals("NA") &&
                            !_Sensor2_OK &&
                            (string.IsNullOrEmpty(auxSerial) || !auxSerial.Equals(Program._UOWKA._CONFIG.SerialSensor1)))
                    {
                        LOG.Info("frmMain.InitBiometricsSensors - Procesando _CONFIG.SerialSensor2: " + Program._UOWKA._CONFIG.SerialSensor2 + "...");
                        m_DevList[i] = new SGFPMDeviceList();
                        m_FPM1.GetEnumDeviceInfo(i, m_DevList[i]);
                        enum_device = m_DevList[i].DevName.ToString() + " : " + m_DevList[i].DevID;
                        auxSerial = new ASCIIEncoding().GetString(m_DevList[i].DevSN);
                        auxSerial = auxSerial.Replace("\0", "").Trim();
                        if (auxSerial.Equals(Program._UOWKA._CONFIG.SerialSensor2))
                        {
                            device_name = m_DevList[i].DevName;
                            device_id = m_DevList[i].DevID;
                            iError = m_FPM2.Init(device_name);
                            iError = m_FPM2.OpenDevice(device_id);
                            m_FPM2.SetLedOn(true);
                            m_FPM2.EnableAutoOnEvent(true, (int)this.Handle);
                            if (iError == 0)
                            {
                                SGFPMDeviceInfoParam pInfo = new SGFPMDeviceInfoParam();
                                iError = m_FPM2.GetDeviceInfo(pInfo);
                                ImageWidth2 = pInfo.ImageWidth;
                                ImageHeight2 = pInfo.ImageHeight;
                                SerialSensor2 = auxSerial;
                                LOG.Info("frmMain.InitBiometricsSensors - Lector Iniciado: " + enum_device + " - " + SerialSensor2 + " - WxH = " +
                                            ImageWidth2 + "x" + ImageHeight2);
                                _HT_SENSOR_ENABLED.Add(auxSerial, m_DevList[i]);
                                _Sensor2_OK = true;
                            }
                            else
                            {
                                LOG.Error("frmMain.InitBiometricsSensors - Error abriendo Program._UOWKA._CONFIG.SerialSensor2 => " + iError);
                                return -2;
                            }
                        }
                    }
                    i++;
                }
            }
            catch (Exception)
            {
                ret = -1;
            }
            LOG.Info("frmMain.InitBiometricsSensors OUT => ret = " + ret);
            return ret;
        }

        private int ReInitBiometricsSensors()
        {
            int ret = 0;
            try
            {
                LOG.Info("frmMain.ReInitBiometricsSensors IN...");
                //m_FPM1 = new SGFingerPrintManager();
                //if (!string.IsNullOrEmpty(Program._UOWKA._CONFIG.SerialSensor2) &&
                //    Program._UOWKA._CONFIG.SerialSensor2.Length > 0 &&
                //    !Program._UOWKA._CONFIG.SerialSensor2.Equals("NA"))
                //{
                //    m_FPM2 = new SGFingerPrintManager();
                //}
                string enum_device;
                string auxSerial;

                // Enumerate Device
                SGFPMDeviceName device_name;
                Int32 device_id;
                //int qSensors = m_FPM1.EnumerateDevice();
                int iError = 0;

                // Get enumeration info into SGFPMDeviceList
                //m_DevList = new SGFPMDeviceList[m_FPM1.NumberOfDevice];
                LOG.Info("frmMain.ReInitBiometricsSensors - Cantidad de sensores conectados = " + _HT_SENSOR_ENABLED.Count);
                //for (int i = 0; i < m_DevList.Length; i++)
                foreach (var key in _HT_SENSOR_ENABLED.Keys)
                {

                    SGFPMDeviceList item = (SGFPMDeviceList)_HT_SENSOR_ENABLED[key];
                    //enum_device = m_DevList[i].DevName.ToString() + " : " + m_DevList[i].DevID;
                    //auxSerial = new ASCIIEncoding().GetString(item.DevSN); // m_DevList[i].DevSN);
                    //auxSerial = auxSerial.Replace("\0", "").Trim();

                    if (key.Equals(Program._UOWKA._CONFIG.SerialSensor1))
                    {
                        device_name = item.DevName; // m_DevList[i].DevName;
                        device_id = item.DevID; // m_DevList[i].DevID;
                        iError = m_FPM1.Init(device_name);
                        iError = m_FPM1.OpenDevice(device_id);
                        m_FPM1.SetLedOn(true);
                        m_FPM1.EnableAutoOnEvent(true, (int)this.Handle);
                        LOG.Info("frmMain.ReInitBiometricsSensors - " + key + " lisening...");
                    }
                    else if (key.Equals(Program._UOWKA._CONFIG.SerialSensor2))
                    {
                        device_name = item.DevName; // m_DevList[i].DevName;
                        device_id = item.DevID; // m_DevList[i].DevID;
                        iError = m_FPM2.Init(device_name);
                        iError = m_FPM2.OpenDevice(device_id);
                        m_FPM2.SetLedOn(true);
                        m_FPM2.EnableAutoOnEvent(true, (int)this.Handle);
                        LOG.Info("frmMain.ReInitBiometricsSensors - " + key + " lisening...");
                    }

                    //if (auxSerial.Equals(Program._UOWKA._CONFIG.SerialSensor1) ||
                    //    auxSerial.Equals(Program._UOWKA._CONFIG.SerialSensor2))
                    //{
                    //    device_name = m_DevList[i].DevName;
                    //    device_id = m_DevList[i].DevID;
                    //    iError = m_FPM1.Init(device_name);
                    //    iError = m_FPM1.OpenDevice(device_id);
                    //    m_FPM1.SetLedOn(true);
                    //    m_FPM1.EnableAutoOnEvent(true, (int)this.Handle);
                    //    LOG.Info("frmMain.ReInitBiometricsSensors - " + auxSerial + " lisening...");
                    //}
                        
                       
                    //}
                    //else if (!string.IsNullOrEmpty(Program._UOWKA._CONFIG.SerialSensor2) &&
                    //        Program._UOWKA._CONFIG.SerialSensor2.Length > 0 &&
                    //        !Program._UOWKA._CONFIG.SerialSensor2.Equals("NA"))
                    //{
                    //    LOG.Info("frmMain.ReInitBiometricsSensors - Procesando _CONFIG.SerialSensor2: " + Program._UOWKA._CONFIG.SerialSensor2 + "...");
                    //    //m_DevList[i] = new SGFPMDeviceList();
                    //    //m_FPM1.GetEnumDeviceInfo(i, m_DevList[i]);
                    //    enum_device = m_DevList[i].DevName.ToString() + " : " + m_DevList[i].DevID;
                    //    //auxSerial = new ASCIIEncoding().GetString(m_DevList[i].DevSN);
                    //    //auxSerial = auxSerial.Replace("\0", "").Trim();
                    //    device_name = m_DevList[i].DevName;
                    //    device_id = m_DevList[i].DevID;
                    //    iError = m_FPM2.Init(device_name);
                    //    iError = m_FPM2.OpenDevice(device_id);
                    //    m_FPM2.SetLedOn(true);
                    //    m_FPM2.EnableAutoOnEvent(true, (int)this.Handle);
                    //}
                }
            }
            catch (Exception)
            {
                ret = -1;
            }
            LOG.Info("frmMain.ReInitBiometricsSensors OUT => ret = " + ret);
            return ret;
        }

        public int CaptureContinue(int qualityCapture) //, out Error error)
        {
            //error = null;
            int[] res = new int[2] { -1, -1 };
            string auxSerial = "";
            try
            {
                //if (CurrentSensor == null)
                //{
                //    if (InstanceSensor(sensorSerial) < 0)
                //    {
                //        error.ErrorCode = -3;
                //        error.ErrorDescription = "BFPSecugenSensor Error [No pudo instanciar Sensor " + sensorSerial + "]";
                //        return -3;
                //    }
                //}

                byte[] raw1 = new Byte[ImageWidth1 * ImageHeight1];
                byte[] raw2 = new Byte[ImageWidth2 * ImageHeight2];
                byte[] raw512x512 = null;
                Sample s = null;

                SGFPMDeviceName device_name;
                Int32 device_id;
                Int32 iError;
                string enum_device;


                int index = 0;


                //for (int i = 0; i < m_DevList.Length; i++)
                foreach (var key in _HT_SENSOR_ENABLED.Keys)
                {
                    //m_DevList[i] = new SGFPMDeviceList();
                    //m_FPM.GetEnumDeviceInfo(i, m_DevList[i]);
                    //enum_device = m_DevList[i].DevName.ToString() + " : " + m_DevList[i].DevID;

                    SGFPMDeviceList item = (SGFPMDeviceList)_HT_SENSOR_ENABLED[key];

                    auxSerial = new ASCIIEncoding().GetString(item.DevSN); // m_DevList[i].DevSN);
                    auxSerial = auxSerial.Replace("\0", "").Trim();

                    device_name = item.DevName; // m_DevList[i].DevName;
                    device_id = item.DevID; // m_DevList[i].DevID;
                    iError = m_FPM1.Init(device_name);
                    iError = m_FPM1.OpenDevice(device_id);
                    switch (index)
                    {
                        case 0:
                            res[index] = m_FPM1.GetImage(raw1);// this._SGPM.GetImage(this.raw);
                            break;
                        case 1:
                            res[index] = m_FPM1.GetImage(raw2);// this._SGPM.GetImage(this.raw);
                            break;
                    }
                    
                    //m_FPM.SetLedOn(true);
                    //m_FPM.EnableAutoOnEvent(true, (int)this.Handle);

                    if (res[index] == 0)
                        break;
                    else 
                        index++;
                }

                //log.Info("Tamaño de imagen Width:" + this._SGInfo.ImageWidth);
                //log.Info("Tamaño de imagen Height:" + this._SGInfo.ImageHeight);
                //imagewidth = ImageWidth; //this._SGInfo.ImageWidth;
                //imageheight = ImageHeight; //this._SGInfo.ImageHeight;
                //res = this._SGPM.GetImageEx(this.raw, _timeout, 0, _quality);
                //int res1 = m_FPM1.GetImage(raw1);// this._SGPM.GetImage(this.raw);
                //int res2 = m_FPM2.GetImage(raw2);// this._SGPM.GetImage(this.raw);
                //if (res2 == 0)
                //{
                //    res2 = m_FPM1.GetImage(raw2);// this._SGPM.GetImage(this.raw);
                //}
                List<Sample> listSampleCaptured = new List<Sample>();
                if (res[0] == 0)
                {
                    //log.Info("Imagen obtenida según calidad.");

                    //log.Info("Validamos la calidad de la imagen leída");
                    int _qualityex = 0;
                    res[0] = m_FPM1.GetImageQuality(ImageWidth1, ImageHeight1, raw1, ref _qualityex);

                    if (_qualityex >= qualityCapture)
                    {
                        //log.Info("Se cumple con la calidad requerida: q:" + _quality + ", calidad extendida:" + _qualityex);
                        //res = 0;

                        picSample.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw1, ImageWidth1, ImageHeight1);

                        raw512x512 = Bio.Core.Imaging.ImageProcessor.FillRaw(raw1, ImageWidth1, ImageHeight1, 512, 512);
                        //Bitmap bmapo = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw1, ImageWidth1, ImageHeight1);
                        //bmapo.Save(@"c:\tmp\aa\s1_orig_img_" + (DateTime.Now.ToString("ddMMyyhhmmss")) + ".jpg", ImageFormat.Jpeg);
                        //bmapo.Dispose();

                        //raw512x512 = Bio.Core.Imaging.ImageProcessor.FillRaw(raw1, ImageWidth1, ImageHeight1, 512, 512);
                        //s = new Sample(2, 22, 1, 1, 512, 512, raw512x512);
                        //listSampleCaptured.Add(s);
                        //Bitmap bmap = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw512x512, 512, 512);
                        //bmap.Save(@"c:\tmp\aa\s1_img_" + (DateTime.Now.ToString("ddMMyyhhmmss")) + ".jpg", ImageFormat.Jpeg);
                        //bmap.Dispose();
                        //byte[] raw512x512 = Bio.Core.Imaging.ImageProcessor.FillRaw(raw, ImageWidth, ImageHeight, 512, 512);
                        //Bio.Core.Wsq.Encoder.WsqEncoder enc = new Bio.Core.Wsq.Encoder.WsqEncoder();

                        //s = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                        //                          Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW,
                        //                          1, (int)Biometrika.Standards.Purpose.Verify, 512, 512, raw512x512);
                        //listSampleCaptured.Add(s);

                        ////BIR
                        //Standards.DataFormat dataFormat = new Standards.DataFormat(10, 0x02);

                        //byte[] wsq;
                        //if (enc.EncodeMemory(raw512x512, 512, 512, out wsq))
                        //{
                        //    s = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                        //                          Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ,
                        //                          1, (int)Biometrika.Standards.Purpose.Verify, 512, 512, wsq);
                        //    listSampleCaptured.Add(s);
                        //}

                        //s = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                        //                              Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW,
                        //                              1, (int)Biometrika.Standards.Purpose.Verify, ImageWidth, ImageHeight, raw);
                        //Sample sout;
                        //Error terr;
                        ////Toma imagen sin completar 512x512
                        //if (GetImage(s, 0, out sout, out terr) == 0)
                        //{
                        //    listSampleCaptured.Add(sout);
                        //}
                        //sout = null;
                        //terr = null;
                        ////Toma imagen y completa 512x512 si hace falta
                        //if (GetImage(s, 1, out sout, out terr) == 0)
                        //{
                        //    listSampleCaptured.Add(sout);
                        //}

                        //OnCapturedEvent(0, "Capture OK!", this.SerialSensor, null);


                    }
                    else
                    {
                        res[0] = -2;
                        LOG.Debug("Calidad menor a la calidad requerida: q:" + qualityCapture + ", calidad umbral:" + _qualityex);
                    }
                    //OnCaptureEvent(0, null, SerialSensor1, listSampleCaptured);
                    OnCaptureEvent(0, null, auxSerial, raw512x512);

                    //richTextBox1.Text += Environment.NewLine + "Evento del Sensor: " + SerialSensor + " - [res:" + res + "] - " +
                    //                                           " - Quality/Umbral: " + _qualityex + "/" + qualityCapture;
                }
                else
                {
                    //log.Info("No se puede capturar la imagen");
                    //richTextBox1.Text += Environment.NewLine + "Evento del Sensor: " + SerialSensor + " - Error Captura:" + res;
                    res[0] = -1;
                }

                if (res[1] == 0)
                {
                    //log.Info("Imagen obtenida según calidad.");

                    //log.Info("Validamos la calidad de la imagen leída");
                    int _qualityex = 0;
                    res[1] = m_FPM2.GetImageQuality(ImageWidth2, ImageHeight2, raw2, ref _qualityex);

                    if (_qualityex >= qualityCapture)
                    {
                        //log.Info("Se cumple con la calidad requerida: q:" + _quality + ", calidad extendida:" + _qualityex);
                        //res = 0;
                        //List<Sample> listSampleCaptured = new List<Sample>();
                        //Sample s = new Sample(2, 22, 1, 1, ImageWidth, ImageHeight, raw);
                        //listSampleCaptured.Add(s);
                        picSample.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw2, ImageWidth2, ImageHeight2);

                        raw512x512 = Bio.Core.Imaging.ImageProcessor.FillRaw(raw2, ImageWidth2, ImageHeight2, 512, 512);
                       

                        //Bitmap bmap = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw512x512, 512, 512);
                        //bmap.Save(@"c:\tmp\aa\s2_img_" + (DateTime.Now.ToString("ddMMyyhhmmss")) + ".jpg", ImageFormat.Jpeg);
                        //bmap.Dispose(); 
                        //s = new Sample(2, 22, 1, 1, 512, 512, raw512x512);
                        //listSampleCaptured.Add(s);

                        //byte[] raw512x512 = Bio.Core.Imaging.ImageProcessor.FillRaw(raw, ImageWidth, ImageHeight, 512, 512);
                        //Bio.Core.Wsq.Encoder.WsqEncoder enc = new Bio.Core.Wsq.Encoder.WsqEncoder();

                        //s = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                        //                          Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW,
                        //                          1, (int)Biometrika.Standards.Purpose.Verify, 512, 512, raw512x512);
                        //listSampleCaptured.Add(s);

                        ////BIR
                        //Standards.DataFormat dataFormat = new Standards.DataFormat(10, 0x02);

                        //byte[] wsq;
                        //if (enc.EncodeMemory(raw512x512, 512, 512, out wsq))
                        //{
                        //    s = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                        //                          Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ,
                        //                          1, (int)Biometrika.Standards.Purpose.Verify, 512, 512, wsq);
                        //    listSampleCaptured.Add(s);
                        //}

                        //s = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                        //                              Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW,
                        //                              1, (int)Biometrika.Standards.Purpose.Verify, ImageWidth, ImageHeight, raw);
                        //Sample sout;
                        //Error terr;
                        ////Toma imagen sin completar 512x512
                        //if (GetImage(s, 0, out sout, out terr) == 0)
                        //{
                        //    listSampleCaptured.Add(sout);
                        //}
                        //sout = null;
                        //terr = null;
                        ////Toma imagen y completa 512x512 si hace falta
                        //if (GetImage(s, 1, out sout, out terr) == 0)
                        //{
                        //    listSampleCaptured.Add(sout);
                        //}

                        //OnCapturedEvent(0, "Capture OK!", this.SerialSensor, null);

                        //OnCaptureEvent(0, null, SerialSensor2, listSampleCaptured);
                        OnCaptureEvent(0, null, auxSerial, raw512x512);
                    }
                    else
                    {
                        res[1] = -2;
                        LOG.Debug("Calidad menor a la calidad requerida: q:" + qualityCapture + ", calidad umbral:" + _qualityex);
                    }
                    //richTextBox1.Text += Environment.NewLine + "Evento del Sensor: " + SerialSensor + " - [res:" + res + "] - " +
                    //                                           " - Quality/Umbral: " + _qualityex + "/" + qualityCapture;
                }
                else
                {
                    //log.Info("No se puede capturar la imagen");
                    //richTextBox1.Text += Environment.NewLine + "Evento del Sensor: " + SerialSensor + " - Error Captura:" + res;
                    res[1] = -1;
                }
                ReInitBiometricsSensors();
            }
            catch (Exception ex)
            {
                res[0] = -1;
                res[1] = -1;
                //LOG.Error(" Error: " + ex.Message);
            }
            return (res[0]==0 || res[1]==0 ? 0 : res[0] + res[1]);
        }

        private void SetOnSensors()
        {
            try
            {
                if (m_FPM1!= null)
                {
                    m_FPM1.SetLedOn(true);
                }
                if (m_FPM2 != null)
                {
                    m_FPM2.SetLedOn(true);
                }
            }
            catch (Exception)
            {

            }
        }

        #endregion Secugen Puro

        /// <summary>
        /// Inicializa el Biometrika BioAPI Framework. 
        /// </summary>
        private void InitBSP()
        {
            try
            {
                //Crea BSP (Biometric Service Provider)
                Program._BSP = new Biometrika.BioApi20.BSP.BSPBiometrika();

                //Carga el BSP a partir del archivo XML en el mismo directorio llamado
                //Biometrika.BioApi20.BSP.SensorFactorySchema.xml
                Program._BSP.BSPAttach("2.0", true);

                //GetInfoFromBSP();

                //Asocio evento para tratar el evento de captura
            //Program._BSP.OnCapturedEvent += OnCaptureEvent;
                Error err;
                //Habilito el AutoEvent, esto significa que queda esperando el evento de la 
                //marcación con dedo.  
                Program._BSP.EnableAutoEvent(Program._UOWKA._CONFIG.SerialSensor1, true, (int)this.Handle, out err);
                //Prendo la luz del sensor si es que no lo tiene prendido por default (Ej.: Secugen)
                Program._BSP.On(Program._UOWKA._CONFIG.SerialSensor1);

                //Hablita captura continua, de tal forma que tome una huella, la procese, y luego
                //quede listo para recibir un nuevo evento de huella automáticamente.
                Program._BSP.CaptureContinue(Program._UOWKA._CONFIG.SerialSensor1, 50, out err);

                if (!string.IsNullOrEmpty(Program._UOWKA._CONFIG.SerialSensor2) &&
                    Program._UOWKA._CONFIG.SerialSensor2.Length > 0 &&
                   !Program._UOWKA._CONFIG.SerialSensor2.Equals("NA"))
                {
                    Program._BSP.EnableAutoEvent(Program._UOWKA._CONFIG.SerialSensor2, true, (int)this.Handle, out err);
                    //Prendo la luz del sensor si es que no lo tiene prendido por default (Ej.: Secugen)
                    Program._BSP.On(Program._UOWKA._CONFIG.SerialSensor2);

                    //Hablita captura continua, de tal forma que tome una huella, la procese, y luego
                    //quede listo para recibir un nuevo evento de huella automáticamente.
                    Program._BSP.CaptureContinue(Program._UOWKA._CONFIG.SerialSensor2, 50, out err);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("frmMain.InitBSP - Excp Error: " + ex.Message);
            }
            
            //Program._BSP.OnTimeoutEvent += OnTimeoutEvent;
        }

        //private void OnTimeoutEvent(int errCode, string errMessage, BioApi20.Interfaces.ISensor sensor)
        //{
        //    throw new NotImplementedException();
        //}

        /// <summary>
        /// Trata el evento de captura desde el BSP. Toma la lista de samples devueltos por el BSP, 
        /// de lo scuales toma la imágen real capturada para mostrar, y el WSQ para procesar.
        /// </summary>
        /// <param name="errCode"></param>
        /// <param name="errMessage"></param>
        /// <param name="samplesCaptured"></param>
        /// 
        string _LAST_ID_RECOGNIZED = null;
        //private void OnCaptureEvent(int errCode, string errMessage, string serialId, List<Sample> samplesCaptured)
        private void OnCaptureEvent(int errCode, string errMessage, string serialId, byte[] raw512x512)
        {
            LOG.Debug("frmMain.OnCaptureEvent - Entro a OnCaptureEvent...");
            lock (OBJECT_TO_BLOCK)
            {
                _IdentifyRunning = false;
                //_SamplesCaptured = samplesCaptured;
                _PROCESA_FINGERSAMPLE = true;
                //Si hubo una lista de samples devueltas, entre las que está el JPG real que sale del lector
                //y el WSQ, entonces entro a procesar.
                if (raw512x512 != null) //(_SamplesCaptured != null && _SamplesCaptured.Count > 0)
                {
                    LOG.Debug("frmMain.OnCaptureEvent - Entro a BiometricHelper.Identify...");
                    //Se realiza la identificación d ela huella capturada
                    //byte[] rawOut = (byte[])samplesCaptured[0].Data;
                    int ret = _BIOMETRIC_HELPER.Identify(raw512x512, out _ID_RECOGNIZED);
                    //int ret = _BIOMETRIC_HELPER.Identify(_BIOMETRIC_HELPER.GetSample(_SamplesCaptured), out _ID_RECOGNIZED);
                    LOG.Debug("frmMain.OnCaptureEvent - BiometricHelper.Identify ret = " + ret.ToString());
                    //if (ret == 0 || ret == -52) // && !string.IsNullOrEmpty(_ID_RECOGNIZED))
                    //{
                    if (!string.IsNullOrEmpty(_LAST_ID_RECOGNIZED) || 
                        (!string.IsNullOrEmpty(_ID_RECOGNIZED) && !_ID_RECOGNIZED.Equals(_LAST_ID_RECOGNIZED)))
                    {
                        _LAST_ID_RECOGNIZED = _ID_RECOGNIZED;
                        _ClearLastIdRecognized = true;
                        LOG.Debug("frmMain.OnCaptureEvent - ID Reconocido desde Huella => " +
                                    (string.IsNullOrEmpty(_ID_RECOGNIZED) ? "NULL => No Reconocio!" : _ID_RECOGNIZED));
                        _ID_DEVICE = serialId; //Program._UOWKA._CONFIG.SerialSensor1;
                    }
                    //Se muestra la huella capturada y reinicia el Sensor (prende ON)
                    ProcessFingerSample();
                        //Se procesa la huella, eso significa que se realiza chequeos de acceso si se identifico 
                        //se acciona el acceso, etc. Si no se identifico, solo se muestra NO Reconocido!
                    ProccessIdRecognized();
                        //threadProcessSample = new Thread(ProccessThreadSample);
                        //threadProcessSample.Name = "ProccessThreadSample";
                        //threadProcessSample.Start();
                        //}
                        //else
                        //{
                        //    LOG.Debug("frmMain.OnCaptureEvent - No Identificada Huella!");
                        //}

                }
            }
        }

        private void timerProcessFingerSampleCaptured_Tick(object sender, EventArgs e)
        {
            //if (_PROCESA_FINGERSAMPLE)
            //{
            //    LOG.Debug("frmMain.timerProcessFingerSampleCaptured_Tick - Entro a timerProcessFingerSampleCaptured_Tick...");
            //    _PROCESA_FINGERSAMPLE = false;
            //    ProcessFingerSample();
            //}
        }

       

        /// <summary>
        /// Toma la imágen normal devuelta peor el lector en formato JPG y la muestra.
        /// </summary>
        /// <returns></returns>
        public int ProcessFingerSample()
        {
            try
            {
                if (_SamplesCaptured != null && picSample.Image == null)
                {
                    LOG.Debug("frmMain.ProcessFingerSample - Entro a ProcessFingerSample...");
                    Sample s;
                    try
                    {
                        if (_SamplesCaptured != null && _SamplesCaptured.Count > 0)
                        {
                            s = _SamplesCaptured[_SamplesCaptured.Count - 2];

                            if (s.MinutiaeType == 41)
                            {
                                picSample.Image = (Bitmap)s.Data;
                                picSample.Refresh();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                //LOG.Error(" Error: " + ex.Message);
                Program._UOWKA._SUPPORT.GenerateTicket(Program._UOWKA._SUPPORT.SUPPORT_LEVEL_ERROR, "Ticket Automatico - ProcessFingerSample Error [" + ex.Message + "]",
                                            "Hubo un error al procesar huella capturada. Rebisar Log attachado"
                                            + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                                            true);
            }
            //System.Threading.Thread.Sleep(200);
            //picSample.Image = null;
            _SamplesCaptured = null;
            SetOnSensors();
            //Program._BSP.On(Program._UOWKA._CONFIG.SerialSensor1);
            //if (!string.IsNullOrEmpty(Program._UOWKA._CONFIG.SerialSensor2) &&
            //    Program._UOWKA._CONFIG.SerialSensor2.Length > 0 &&
            //    !Program._UOWKA._CONFIG.SerialSensor2.Equals("NA"))
            //{
            //    Program._BSP.On(Program._UOWKA._CONFIG.SerialSensor2);
            //}
            //_IdentifyRunning = true;
            //_PROCESA_FINGERSAMPLE = true;

            return 0;
        }

       

        internal void _ThreadRefreshBIRs()
        {
            bool bRunning = true;
            int milisec = 43200000; //12 hs
            try
            {
                LOG.Debug("frmMain._ThreadRefreshBIRs - IN...");
                milisec = Program._UOWKA._CONFIG.TimeToRefreshAutomaticBIRs * 60 * 1000;
                LOG.Debug("frmMain._ThreadRefreshBIRs - milisec = " + milisec.ToString());
                while (bRunning)
                {
                    LOG.Debug("frmMain._ThreadRefreshBIRs - Entra a dormir...");
                    Thread.Sleep(milisec);
                    try
                    {
                        LOG.Debug("frmMain._ThreadRefreshBIRs - Calling picReinit_Click...");
                        if (!_IsFirstLoad)
                        {
                            picReinit_Click(null, null);
                        }
                        else
                        {
                            _IsFirstLoad = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show("Error refrescando datos biometricos. Contacte con el administrador...", "Atención...",
                        //    MessageBoxButtons.OK, MessageBoxIcon.Error);
                        LOG.Error("frmMain._ThreadRefreshBIRs Error: " + ex.Message);
                    }


                    //Para test uso 1 minuto
                    //Thread.Sleep(60000);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("frmMain._ThreadRefreshBIRs - " + ex.Message);
            }
        }

        #endregion Biometric Zone

        #region Barcode Zone

        private BioCore.SerialComm.Cedula.CedulaTemplate2 cedula = null;
        private CedulaTemplate cedulabarcode = null;
        private string ComAux = null;
        private bool hayCedula = false;
        private string rut = "";
        private string accion = "";

        /// <summary>
        /// Formate rut en formato NNNNNNNN-N
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        private String FormattingTaxid(string rut)
        {
            int cont = 0;
            String format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {

                    format = rut.Substring(i, 1) + format;

                    cont++;

                }
                return format;
            }
        }


        /// <summary>
        /// Procesa el evento de captura de código de barras
        /// </summary>
        /// <param name="sensor"></param>
        /// <param name="sample"></param>
        private void OnSample(BioCore.SerialComm.ISensor sensor, ISample sample)
        {
            try
            {
                LOG.Debug("frmMain.OnSample - Entro a OnSample [_IdentifyRunning=" + 
                            _IdentifyRunning.ToString() + "]...");
                if (_IdentifyRunning)
                {
                    LOG.Debug("frmMain.OnSample - Entro a sample is sample: Sample!=null => + " +
                              (sample != null).ToString() + "...");
                    if (sample is SerialSample)
                    {
                        LOG.Debug("frmMain.OnSample - Parsea codigo de barras => sample.isValid = " 
                                  + sample.IsValid.ToString());
                        //Parsea código de barras leído y recupera datos existentes de la cédula a la 
                        //que pertenece el código de barras.
                        //hayCedula = true;
                        LeerCedula(sensor as BarCodeReader);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("frmMain.OnSample  Error: " + ex.Message);
                Program._UOWKA._SUPPORT.GenerateTicket(Program._UOWKA._SUPPORT.SUPPORT_LEVEL_ERROR, "Ticket Automatico - Barcode Error [" + ex.Message + "]",
                                            "Hubo un error al leer codigo de barras. Revisar Log attachado"
                                            + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                                            true);
            }

        }

        private void timerSerialLCB_Tick(object sender, EventArgs e)
        {
            try
            {
                timerSerialLCB.Enabled = false;
                if (hayCedula)
                {
                    hayCedula = false;
                    ProccessIdRecognized(); 
                }
                timerSerialLCB.Enabled = true;
            }
            catch (Exception ex)
            {
                LOG.Error("frmMain.timerSerialLCB_Tick Error Expc - ", ex);
            }
        }

        /// <summary>
        /// Este método solo esta leyendo lectura OCR.
        /// Toma lo leído desde el lector, para eso usa la librería Bi.Core.SerialComm
        /// </summary>
        /// <param name="reader"></param>
        private void LeerCedula(BarCodeReader reader)
        {
            int res = 0;
            string valueid = "";
            _ID_RECOGNIZED = null;
            try
            {
                LOG.Debug("frmMain.LeerCedula - IN...");
                _IdentifyRunning = false;
                ISample sample = reader.GetSample(new BarCodeArguments(250));
                ComAux = reader.serialPort.PortName;
                _ID_DEVICE = reader.serialPort.PortName;
                string type = reader.Name;

                if (sample is SerialSample)
                {
                    LOG.Debug("frmMain.LeerCedula - Parsea codigo de barras...");
                    string lectura = Convert.ToBase64String((sample as SerialSample).Data);

                    if (!lectura.Equals(_LAST_BARCODE_READED))
                    {
                        _LAST_BARCODE_READED = lectura;
                    } else
                    {
                        LOG.Debug("frmMain.LeerCedula - Descarta lectura de barcode por repetido...");
                        return;
                    }

                    byte[] rawSample = Convert.FromBase64String(lectura);
                    string lect = Encoding.UTF7.GetString(rawSample, 0, rawSample.Length - 1);

                    //ARREGLO TEMPORAL para lectores Symbol (se debe ver si se pued econfigurar para que no agregue estos datos iniciales
                    if (lect.StartsWith("]Q1"))
                    {
                        lect = lect.Substring(3);
                        rawSample = Encoding.UTF7.GetBytes(lect);
                    } else if (lect.StartsWith("]L"))
                    {
                        lect = lect.Substring(2);
                        rawSample = Encoding.UTF7.GetBytes(lect);
                    }

                    //cedula2 = new CedulaTemplate((sample as SerialSample).Data);
                    if (Program._UOWKA._CONFIG.ParseCedulaInBarcodeEnabled)
                    {
                        //if (lect.StartsWith("h")) //(type == "PDF")
                        //{
                            LOG.Debug("frmMain.LeerCedula - Parsea cedula..."); // nueva...");
                        //    cedula = new BioCore.SerialComm.Cedula.CedulaTemplate2(lect);
                        //}
                        //else
                        //{
                        //    LOG.Debug("frmMain.LeerCedula - Parsea cedula antigua...");
                            cedulabarcode = new CedulaTemplate(rawSample);
                        //}
                        if ((cedulabarcode != null)) //|| (cedula != null)
                        {
                            LOG.Debug("frmMain.LeerCedula - Parseo correcto...");
                            hayCedula = true;
                            //Se debe considerar que la lectura de cédula nueva es distinta a cédula antigua.
                            //if (cedula != null)
                            //{
                            //    _ID_RECOGNIZED = cedula.Rut;
                            //}
                            //else
                            //{
                                _ID_RECOGNIZED = FormattingTaxid(cedulabarcode.Rut);
                            //}
                            //ProccessIdRecognized();
                            //threadProcessSample = new Thread(ProccessThreadSample);
                            //threadProcessSample.Name = "ProccessThreadSample";
                            //threadProcessSample.Start();
                        }
                        else
                        {
                            _ID_RECOGNIZED = null;
                            LOG.Error("frmMain.LeerCedula - Credencial presentada Inválida");
                        }
                    }
                    else  
                    {
                        LOG.Debug("frmMain.LeerCedula - Se pasa el valor leido a _ID_RECOGNIZED para su proceso en BuisinessRule...");
                        _ID_RECOGNIZED = lect;
                    }
                    LOG.Debug("frmMain.LeerCedula - Inicia proceso de _ID_RECOGNIZED = " + _ID_RECOGNIZED);

                    picSample.Image = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageTechLCBResult"]; ;
                    picSample.Refresh();
                    //Una vez leido el RUT => se procesa el id reconocido, igual que con la huella
                    hayCedula = true;
                }
            }
            catch (Exception ex)
            {
                hayCedula = false;
                _IdentifyRunning = true;
                _ID_RECOGNIZED = null;
                _ID_DEVICE = null;
                LOG.Error("frmMain.LeerCedula - Se encuentra un error al leer la cédula", ex);
                Program._UOWKA._SUPPORT.GenerateTicket(Program._UOWKA._SUPPORT.SUPPORT_LEVEL_ERROR, "Ticket Automatico - Bacode Process Error [" + ex.Message + "]",
                                            "Hubo un error al procesar barcode leido. Revisar Log attachado"
                                            + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                                            true);
            }
            _IdentifyRunning = true;
        }

        #endregion Barcode Zone

        #region MRZ Modalidad Lectura Teclado
        bool isProcessing = false;

        
        /// <summary>
        /// En el evento de captura en un textbox de ingreo del MRZ en modo ingreso de teclado
        /// Si es del tamaño esperado, lo parsea para recuopera rl RUT, y si lo hace
        /// lo procesa como al leido desde código de barras o huella.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rtBoxInputMRZ_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(rtBoxInputMRZ.Text) && rtBoxInputMRZ.Text.Trim().Length == 92 
                    && _IdentifyRunning && !isProcessing)
                {
                    isProcessing = true;
                    LOG.Debug("frmMain.rtBoxInputMRZ_TextChanged MRZ Leido = " + rtBoxInputMRZ.Text);
                    timerMRZReset.Interval = 10000; //Properties.Settings.Default.TimerReset;
                    isProcessing = true;
                    src.Helpers.CedulaTemplate2 cedulaout;
                    int ret = MRZHelper.ParseMRZ(rtBoxInputMRZ.Text.Trim(), out cedulaout, out _ID_RECOGNIZED);
                    if (ret == 0 && !string.IsNullOrEmpty(_ID_RECOGNIZED))
                    {
                        LOG.Debug("frmMain.rtBoxInputMRZ_TextChanged - ID Reconocido desde MRZ => " + _ID_RECOGNIZED);
                        ProccessIdRecognized();
                        //threadProcessSample = new Thread(ProccessThreadSample);
                        //threadProcessSample.Name = "ProccessThreadSample";
                        //threadProcessSample.Start();
                    }
                    else
                    {
                        LOG.Debug("frmMain.rtBoxInputMRZ_TextChanged - ID NO Reconocido correctamente desde MRZ...");
                        rtBoxInputMRZ.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }
            isProcessing = false;
        }

        /// <summary>
        /// Para limpiar automatico el textbox donde se ingresa el MRZ leido
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerMRZReset_Tick(object sender, EventArgs e)
        {
            if (!isProcessing)
            {
                //LOG.Debug("frmMain.timerMRZReset_Tick IN...");
                rtBoxInputMRZ.Text = "";
                //LOG.Debug("frmMain.timerMRZReset_Tick OUT!");
            }
        }

        /// <summary>
        /// Para pruebas manuales, este boton se pone invisible en produccion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTestMRZ_Click(object sender, EventArgs e)
        {
            rtBoxInputMRZ.Text = "IECHL6010143104S48<<<<<<<<<<<<" + Environment.NewLine +
                                 "6909286M2208028ARG21284415<2<1" + Environment.NewLine +
                                 "SUHIT<<GUSTAVO<GERARDO<<<<<<<<";
        }
        #endregion MRZ Modalidad Lectura Teclado

        #region Process Operation

       
        /// <summary>
        /// Dado un ID Reconocido (POr ahora RUT) se realizan los procesos de chequeo de acceso y si aplica,
        /// se acciona el acceso y se registra la marca.
        /// </summary>
        private void ProccessIdRecognized()
        {
            bool _AddSleepShwoResult = false;
            bool accessresult = false;
            DynamicData paramsreturn;
            string msg;
            DynamicData parameters = new DynamicData();
            DateTime start, end, startstep, endstep;
            if (!_IS_PROCESSING)
            {
                if (!string.IsNullOrEmpty(_ID_RECOGNIZED))
                {
                    start = DateTime.Now;
                    startstep = DateTime.Now;
                    _IS_PROCESSING = true;
                    //this.BackgroundImage = Properties.Resources.
                    //grpShowResult.Visible = true;
                    //grpShowResult.Refresh();

                    if (Program._UOWKA._CONFIG.TakeFotoInMark ||
                        (Program._UOWKA._CONFIG.MFAEnable && Program._UOWKA._CONFIG.MFAType == (int)Common.Model.Contants.MAFTypes.MAFType_BARCODE_FACIAL))
                    {
                        try
                        {
                            if (Program._UOWKA._CONFIG.TakeFotoInMark)
                            {
                                _Image_TakeFoto = TakeFoto(0);
                            } else  if (Program._UOWKA._CONFIG.MFAEnable)
                            {
                                if (_ID_DEVICE.Equals(Program._UOWKA._CONFIG.PortCOMBarcode1) && Program._UOWKA._CONFIG.MFAControlType != 2) //Si debe controlar entrada
                                {
                                    _Image_TakeFoto = TakeFoto(1); //IN
                                } else if (_ID_DEVICE.Equals(Program._UOWKA._CONFIG.PortCOMBarcode2) && Program._UOWKA._CONFIG.MFAControlType > 1) //Si debe controlar salida
                                {
                                    _Image_TakeFoto = TakeFoto(2); //OUT
                                }
                            }
                        }
                        catch (Exception)
                        {
                            _CurrentImage = null;
                            _Image_TakeFoto = null;
                        }
                    } else if (Program._UOWKA._CONFIG.MFAEnable && Program._UOWKA._CONFIG.MFAType == (int)Common.Model.Contants.MAFTypes.MAFType_BARCODE_FINGERPRINT)
                    {
                        //TODO Hacer capture de huella con el serial correspondiente segun sea el Barcode
                    }
                    endstep = DateTime.Now;
                    LOG.Debug("frmMain.ProccessIdRecognized - Total Time captura foto = " + (endstep - startstep).TotalMilliseconds.ToString());
                    if (Program._UOWKA._CONFIG.IsDummy)
                    {
                        labDebug1.Text = "Foto: " + (endstep - startstep).TotalMilliseconds.ToString();
                    }
                    startstep = DateTime.Now;

                    labId.Text = _ID_RECOGNIZED;
                    labId.Refresh();

                    //Como se reconocio entro a verificar tema de BusinessRules
                    parameters.AddItem("idRecognized", _ID_RECOGNIZED);
                    parameters.AddItem("idDevice", _ID_DEVICE); //Indica com o serial sensor para indicar si es entrada o salida
                    if (Program._UOWKA._CONFIG.KioskType == 1)
                    {
                        parameters.AddItem("checktype", Program._UOWKA._CONFIG.KioskType1);
                    }
                    else
                    {
                        parameters.AddItem("checktype", Program._UOWKA._CONFIG.KioskType);
                    }
                    if (Program._UOWKA._CONFIG.TakeFotoInMark ||
                        (Program._UOWKA._CONFIG.MFAEnable && Program._UOWKA._CONFIG.MFAType == (int)Common.Model.Contants.MAFTypes.MAFType_BARCODE_FACIAL))
                    {
                        //Agrego la foto tomada en la captura recien realizada. Para los casos que se quiera mostrar esto 
                        //y para guardar en el server 
                        parameters.AddItem("photo", _Image_TakeFoto);
                    }
                    LOG.Debug("frmMain.ProccessIdRecognized - ID Procesada = " + _ID_RECOGNIZED);
                    LOG.Debug("frmMain.ProccessIdRecognized - ID Device = " + _ID_DEVICE);

                    this.labWorking.Text = "Consultando acceso o salida. Este proceso puede tardar unos segundos...";
                    this.panelWorking.Visible = true;
                    this.panelWorking.BringToFront();
                    this.panelWorking.Refresh(); 
                    //LLama al _BUSINESSRULE_ENGINE para chequear de acuerdo a como este configurado, si tiene o no acceso.
                    int ret = Program._UOWKA._BUSINESSRULE_ENGINE.CheckBusinessRule(parameters, out accessresult, out paramsreturn, out msg);
                    if (Program._UOWKA._CONFIG.IsDummy)
                    {
                        try
                        {
                            labDebug2.Text = "   ERP: " + paramsreturn.GetItem("TimeERPService");
                            labDebug3.Text = "Facial: " + (paramsreturn.DynamicDataItems.ContainsKey("TimeVerifyFacialService") ? 
                                                          paramsreturn.GetItem("TimeVerifyFacialService") : paramsreturn.GetItem("TimeVerifyWSQService"));
                            labDebug4.Text = " Total: " + paramsreturn.GetItem("TotalTimeCheckBR");
                        }
                        catch (Exception ex)
                        {
                             
                        }
                    }
                    endstep = DateTime.Now;
                    LOG.Debug("frmMain.ProccessIdRecognized - Total Time CheckRules = " + (endstep - startstep).TotalMilliseconds.ToString());
                    startstep = DateTime.Now;

                    this.panelWorking.Visible = false;
                    this.panelWorking.Refresh();
                    this.labWorking.Text = "";

                    //Muestro resultado positivo o negativo
                    ShowResult(ret, accessresult, paramsreturn, msg);
                    endstep = DateTime.Now;
                    LOG.Debug("frmMain.ProccessIdRecognized - Total Time ShowResult = " + (endstep - startstep).TotalMilliseconds.ToString());
                    
                    startstep = DateTime.Now;   
                    //Si tiene acceso => Se ejecutan las acciones definidas (ej: apertura de barrera, apertura de torniquete, etc)
                    // y generacion de marca
                    if (ret == 0 && accessresult)
                    {
                        LOG.Debug("frmMain.ProccessIdRecognized - Ingresa a Actions y Mark...");
                        //_PERSONA = Program._UOWKA._RETRIEVE_ENGINE.RetrievePersonaIdentified(parameters);
                        //Genero Actions y Marks (Hay que evaluar luego si se lleva estos llamados a Threads, para que sea mas eficiente o asi esta bien)

                        //Agrego parametros devueltos desde BusinessRules por si me sirven en Action y Mark
                        LOG.Debug("frmMain.ProccessIdRecognized - Agrega paramsreturns...");
                        foreach (var item in paramsreturn.DynamicDataItems)
                        {
                            try
                            {
                                parameters.AddItem(item.Key, item.Value);
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                
                        LOG.Debug("frmMain.ProccessIdRecognized - Excute Actions...");
                        //Agrego la info de los reles a abrir. 
                        if (_ID_DEVICE.Equals(Program._UOWKA._CONFIG.PortCOMBarcode1) ||
                               _ID_DEVICE.Equals(Program._UOWKA._CONFIG.SerialSensor1))
                        {
                            if (!string.IsNullOrEmpty(Program._UOWKA._CONFIG.Rele1))
                            {
                                parameters.AddItem("Rele1", Program._UOWKA._CONFIG.Rele1);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Program._UOWKA._CONFIG.Rele2))
                            {
                                parameters.AddItem("Rele2", Program._UOWKA._CONFIG.Rele2);
                            }
                        }
                        ExecuteActions(parameters);
                        endstep = DateTime.Now;
                        LOG.Debug("frmMain.ProccessIdRecognized - Total Time Execute Action = " + (endstep - startstep).TotalMilliseconds.ToString());

                        startstep = DateTime.Now;
                        LOG.Debug("frmMain.ProccessIdRecognized - Excute Marks...");
                        ExecuteMarks(parameters);
                        LOG.Debug("frmMain.ProccessIdRecognized - Total Time Execute Mark = " + (endstep - startstep).TotalMilliseconds.ToString());

                    }
                    else
                    {
                        _AddSleepShwoResult = true;
                        LOG.Warn("frmMain.ProccessIdRecognized - Check BusinessRule Negativa => ret = " + ret + " - msg = " +
                                 (!string.IsNullOrEmpty(msg) ? msg : "Null"));
                    }
                }
                else
                {
                    _AddSleepShwoResult = true;
                    //Muestra que no hubo identificación positiva del ID reconocido o huella leida.
                    //Esto es porque no identifico o porque no existe en la BD ese rut 
                    ShowResult(Errors.IERR_IDENTIFY, false, null,
                                (parameters.DynamicDataItems.ContainsKey("Msg") ? parameters.DynamicDataItems["Msg"].ToString() : 
                                                                                           "Persona NO Verificada!"));
                    //grpShowResult.Visible = true;
                    //grpShowResult.Refresh();

                    labId.Text = "No Identificado!";
                    labId.Refresh();
                    LOG.Debug("frmMain.ProccessIdRecognized - ID Procesada como No Identificada = " + _ID_RECOGNIZED);
                }
                this.Refresh();

                //Solo agrego este sleep si NO abrio => porue si abrio, el sleep lo hace el Action (x KMTronic)
                if (_AddSleepShwoResult)
                {
                    System.Threading.Thread.Sleep(Program._UOWKA._CONFIG.TimeToShowResult);
                }
                
                
                labError.Text = "";
                labError.Refresh();
                //this.BackgroundImage = Properties.Resources.ImageBackground1;
                //this.Refresh();
                //grpShowResult.BackgroundImage = null;
                labFeedbackName.Text = "";
                labFeedbackName.Refresh();
                picFotoIdentified.Image = null;
                picFotoIdentified.Refresh();
                labFeedbackDetail.Text = "";
                labFeedbackDetail.Visible = false;
                labFeedbackDetail.Refresh();
                labAntipassback.Visible = false;
                labAntipassback.Refresh();
                grpShowResult.Visible = false;
                grpShowResult.Refresh();

                picSample.Image = null;
                picSample.Refresh();
                rtBoxInputMRZ.Text = null;
                _ID_RECOGNIZED = null;
                _ID_DEVICE = null;
                labId.Text = "?";
                labId.Refresh();
                _IdentifyRunning = true;

                _CurrentImage = null;
                _Image_TakeFoto = null;

                _PROCESA_FINGERSAMPLE = true;
                _IS_PROCESSING = false;
            } else
            {
                LOG.Debug("ProccessIdRecognized - Descarta => " +
                            (!string.IsNullOrEmpty(_ID_RECOGNIZED) ? _ID_RECOGNIZED : "Null") +
                            " porque se esta procesando un reconocimeinto anterior...");
            }
            //Elimina esto para que pueda volver a probar el mismo QR
            _LAST_BARCODE_READED = null; 
        }

       

        /// <summary>
        /// Ejecuta el proceso de registro de marcas si aplica.
        /// En el caso de Autoclub el registro se hace junto con el BusinessRule asi que este metodo es dummy.
        /// Pero en otros casos puede ser diferente.
        /// </summary>
        /// <param name="parameters"></param>
        private void ExecuteMarks(DynamicData parameters)
        {
            try
            {
                DynamicData paramsreturn;
                string msg;
                int ret = Program._UOWKA._MARK_ENGINE.DoMark(parameters, out paramsreturn, out msg);
                if (ret < 0)
                {
                    LOG.Warn("frmMain.ExecuteMarks - Error generando marca! [code=" + ret.ToString() + "]");
                } else
                {
                    LOG.Warn("frmMain.ExecuteMarks - Marca Generada con exito para => [_ID_RECOGNIZED=" + _ID_RECOGNIZED + "]");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("frmMain.ExecuteMarks Excp Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Ejecuta acciones. En caso de Autoclub se envia a abrir todos los reles del Arduino configurado, para asi abrirl barrera 
        /// o torniquete y se prende luz verde si exta conectada en otro rele.
        /// </summary>
        /// <param name="parameters"></param>
        private void ExecuteActions(DynamicData parameters)
        {
            try
            {
                DynamicData paramsreturn;
                string msg;
                int ret = Program._UOWKA._ACTION_ENGINE.DoAction(parameters, out paramsreturn, out msg);
                if (ret < 0)
                {
                    Program._UOWKA._SUPPORT.GenerateTicket(Program._UOWKA._SUPPORT.SUPPORT_LEVEL_ERROR, "Ticket Automatico - ExecuteActions Error [" + ret.ToString() + "]",
                                            "Hubo un error al ejecutar Action. Revisar Log attachado"
                                            + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                                            true);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("frmMain.ExecuteActions Excp Error: " + ex.Message);
                Program._UOWKA._SUPPORT.GenerateTicket(Program._UOWKA._SUPPORT.SUPPORT_LEVEL_ERROR, "Ticket Automatico - ExecuteActions Error [" + ex.Message + "]",
                                            "Hubo una exception al ejecutar Action. Revisar Log attachado"
                                            + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                                            true);
            }
        }

        /// <summary>
        /// Muestra feedback del resultado en el kiosko.
        /// Dependiendo del código de resultado obtenido desde BusinessRule se muestra feedback 
        /// correspondiente. Estos mensajes pueden ser modificados en el Theme.
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="accessresult"></param>
        /// <param name="paramsreturn"></param>
        /// <param name="msg"></param>
        private void ShowResult(int ret, bool accessresult, DynamicData paramsreturn, string msg)
        {
            string _Nombre = "";
            Image _Foto = null;
            labFeedbackDetail.Text = "";
            bool _ReWritePhoto = true;
            try
            {
                LOG.Error("frmMain.ShowResult IN...");
                if (Program._UOWKA._CONFIG.ShowResultCustomized &&
                    Program._UOWKA._SCREEN_ENGINE != null &&
                    Program._UOWKA._SCREEN_ENGINE.Initialized &&
                    Program._UOWKA._SCREEN_ENGINE.Screen != null &&
                    Program._UOWKA._SCREEN_ENGINE.Screen.Initialized)
                {
                    int ret1 = Program._UOWKA._SCREEN_ENGINE.DoShow(this, paramsreturn, out paramsreturn, out msg);
                }
                else
                {
                    if (paramsreturn != null && !string.IsNullOrEmpty(_ID_DEVICE))
                    {

                        #region Datos Para Mostrar

                        LOG.Debug("frmMain.ShowResult - paramsreturn != null => muestro nombre, foto, etc...");
                        if (paramsreturn.DynamicDataItems.ContainsKey("Name"))
                        {
                            _Nombre = string.IsNullOrEmpty((string)paramsreturn.DynamicDataItems["Name"]) ? "" :
                                                                    (string)paramsreturn.DynamicDataItems["Name"];
                            LOG.Debug("frmMain.ShowResult - _Nombre = " + (string.IsNullOrEmpty(_Nombre) ? "Null" : _Nombre));
                        }
                        else
                        {
                            LOG.Debug("frmMain.ShowResult - No contiene paramsreturn.DynamicDataItems.ContainsKey(Name)");
                        }
                        if (paramsreturn.DynamicDataItems.ContainsKey("Photografy"))
                        {
                            _Foto = string.IsNullOrEmpty((string)paramsreturn.DynamicDataItems["Photografy"]) ?
                                            (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultNoFoto"] :
                                            Utils.SetImageFromB64((string)paramsreturn.DynamicDataItems["Photografy"]);
                            //Si llego imagen pero la conversion dio null => Seteo imagen por default de NoFoto
                            if (!string.IsNullOrEmpty((string)paramsreturn.DynamicDataItems["Photografy"]) &&
                                _Foto == null)
                            {
                                _Foto = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultNoFoto"];
                            }
                            LOG.Debug("frmMain.ShowResult - _Nombre = " + (string.IsNullOrEmpty(_Nombre) ? "Null" : "Foto OK"));
                        }
                        else
                        {
                            _Foto = (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultNoFoto"];
                            LOG.Debug("frmMain.ShowResult - No contiene paramsreturn.DynamicDataItems.ContainsKey(Photografy)");
                        }
                        //if (paramsreturn.DynamicDataItems.ContainsKey("DateOfBirth"))
                        //{
                        //    //TODO - Hay que ver como se muestra el Feliz Cumpleaños. Quiza una imagen en lugar de la selfie
                        //    //       y es aimagen en la lista de imagenes
                        //}

                        //Si no es acceso permitido => Seteo mensaje de acuerdo al codigo
                        //if (!accessresult)
                        //{

                        //}

                        #endregion Datos Para Mostrar
                    }

                    //this.BackgroundImage = Properties.Resources.ImageBackgroundAccesoPermitido;
                    //this.Refresh();
                    if (ret == Errors.IERR_IDENTITY_NOT_FOUND || ret == Errors.IERR_IDENTIFY)
                    {
                        LOG.Debug("frmMain.ShowResult - Process Identify...");
                        if (Program._UOWKA._CONFIG.KioskType == 1) //Es solo entrada
                        {
                            LOG.Debug("frmMain.ShowResult - Solo entrada config...");

                            if (Program._UOWKA._CONFIG.KioskType1 == 1)
                            {
                                grpShowResult.BackgroundImage =
                                    _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultEntrada") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultEntrada"] : null;
                                picSample.Image = picSample.Image == null ? GetImageFinger(_SamplesCaptured) : picSample.Image;
                                picFotoIdentified.Image = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionNoIdentify") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionNoIdentify"] : null;
                                _ReWritePhoto = false;
                                picResultAction.Image =
                                   (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionAccesoDenegado"];
                            }
                            else
                            {
                                grpShowResult.BackgroundImage = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultSalida") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultSalida"] : null;
                                picSample.Image = picSample.Image == null ? GetImageFinger(_SamplesCaptured) : picSample.Image;
                                picFotoIdentified.Image = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionNoIdentify") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionNoIdentify"] : null;
                                _ReWritePhoto = false;
                                picResultAction.Image =
                                   (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionSalidaDenegado"];
                            }
                            LOG.Debug("frmMain.ShowResult - grpShowResult.BackgroundImage ok!");
                        }
                        else
                        {
                            LOG.Debug("frmMain.ShowResult - E/S config...");
                            if (!string.IsNullOrEmpty(_ID_DEVICE))
                            {
                                if (_ID_DEVICE.Equals(Program._UOWKA._CONFIG.PortCOMBarcode1) ||
                                   _ID_DEVICE.Equals(Program._UOWKA._CONFIG.SerialSensor1))
                                {
                                    grpShowResult.BackgroundImage =
                                    _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultEntrada") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultEntrada"] : null;
                                    picSample.Image = null;
                                    picFotoIdentified.Image = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionNoIdentify") ?
                                        (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionNoIdentify"] : null;
                                    _ReWritePhoto = false;
                                    picResultAction.Image =
                                            (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionAccesoDenegado"];
                                }
                                else
                                {
                                    grpShowResult.BackgroundImage = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultSalida") ?
                                   (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultSalida"] : null;
                                    picSample.Image = null;
                                    picFotoIdentified.Image = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionNoIdentify") ?
                                        (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionNoIdentify"] : null;
                                    _ReWritePhoto = false;
                                    picResultAction.Image =
                                            (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionSalidaDenegado"];
                                }
                            }
                            else
                            {
                                grpShowResult.BackgroundImage = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultSalida") ?
                                  (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultSalida"] : null;
                                picSample.Image = null;
                                picFotoIdentified.Image = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionNoIdentify") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionNoIdentify"] : null;
                                _ReWritePhoto = false;
                                picResultAction.Image =
                                   (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionSalidaDenegado"];
                            }

                            LOG.Debug("frmMain.ShowResult - E/S grpShowResult.BackgroundImage ok!");
                        }
                    }
                    else if (ret < 0)
                    {
                        LOG.Debug("frmMain.ShowResult - Process NO Identify...");
                        if (Program._UOWKA._CONFIG.KioskType == 1) //Es solo entrada
                        {
                            if (Program._UOWKA._CONFIG.KioskType1 == 1)
                            {
                                grpShowResult.BackgroundImage =
                                       _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultEntrada") ?
                                       (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultEntrada"] : null;
                                picSample.Image = null;
                                picFotoIdentified.Image = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionNoIdentify") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionNoIdentify"] : null;
                                _ReWritePhoto = false;
                                picResultAction.Image =
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionAccesoDenegado"];
                            }
                            else
                            {
                                grpShowResult.BackgroundImage =
                                      _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultSalida") ?
                                      (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultSalida"] : null;
                                picSample.Image = null;
                                picFotoIdentified.Image = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionNoIdentify") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionNoIdentify"] : null;
                                _ReWritePhoto = false;
                                picResultAction.Image =
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionSalidaDenegado"];
                            }
                            LOG.Debug("frmMain.ShowResult - grpShowResult.BackgroundImage ok!");
                        }
                        else
                        {
                            if (_ID_DEVICE.Equals(Program._UOWKA._CONFIG.PortCOMBarcode1) ||
                               _ID_DEVICE.Equals(Program._UOWKA._CONFIG.SerialSensor1))
                            {
                                grpShowResult.BackgroundImage =
                                      _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultEntrada") ?
                                      (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultEntrada"] : null;
                                picSample.Image = null;
                                picFotoIdentified.Image = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionNoIdentify") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionNoIdentify"] : null;
                                _ReWritePhoto = false;
                                picResultAction.Image =
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionAccesoDenegado"];
                            }
                            else
                            {
                                grpShowResult.BackgroundImage =
                                      _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultSalida") ?
                                      (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultSalida"] : null;
                                picSample.Image = null;
                                picFotoIdentified.Image = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionNoIdentify") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionNoIdentify"] : null;
                                _ReWritePhoto = false;
                                picResultAction.Image =
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionSalidaDenegado"];
                            }
                            LOG.Debug("frmMain.ShowResult - E/S grpShowResult.BackgroundImage ok!");
                        }
                        labError.Visible = true;
                        labError.Text = "Error Chequeando Acceso [" + ret.ToString() + " - " +
                                        (string.IsNullOrEmpty(msg) ? "Desconocido" : msg) + "]";
                        labError.Refresh();
                    }
                    else if (ret == 0 && accessresult)
                    {
                        LOG.Debug("frmMain.ShowResult - Processing access premitido...");
                        if (Program._UOWKA._CONFIG.KioskType == 1)
                        { //Es solo entrada
                            if (Program._UOWKA._CONFIG.KioskType1 == 1)
                            {
                                grpShowResult.BackgroundImage = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageAccesoPermitido") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageAccesoPermitido"] : null;
                                grpShowResult.BackgroundImage =
                                      _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultEntrada") ?
                                      (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultEntrada"] : null;
                                //picSample.Image = null;
                                picResultAction.Image =
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionAccesoPermitido"];
                            }
                            else
                            {
                                grpShowResult.BackgroundImage = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageSalidaPermitido") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageSalidaPermitido"] : null;

                                grpShowResult.BackgroundImage =
                                     _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultSalida") ?
                                     (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultSalida"] : null;
                                //picSample.Image = null;
                                picResultAction.Image =
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionSalidaPermitido"];
                            }
                        }
                        else  //Si es entrada y salida => Debo determinar quien marco
                        {
                            if (_ID_DEVICE.Equals(Program._UOWKA._CONFIG.PortCOMBarcode1) ||
                                _ID_DEVICE.Equals(Program._UOWKA._CONFIG.SerialSensor1))
                            {
                                grpShowResult.BackgroundImage = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageAccesoPermitido") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageAccesoPermitido"] : null;
                                grpShowResult.BackgroundImage =
                                      _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultEntrada") ?
                                      (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultEntrada"] : null;
                                //picSample.Image = null;
                                picResultAction.Image =
                                    _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionAccesoPermitido") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionAccesoPermitido"] : null;
                                msg = "Ingreso registrado correctamente!";
                            }
                            else
                            {
                                grpShowResult.BackgroundImage = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageSalidaPermitido") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageSalidaPermitido"] : null;

                                grpShowResult.BackgroundImage =
                                     _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultSalida") ?
                                     (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultSalida"] : null;
                                //picSample.Image = null;
                                picResultAction.Image =
                                    _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionSalidaPermitido") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionSalidaPermitido"] : null;
                                msg = "Salida registrada correctamente!";
                            }
                        }
                        LOG.Debug("frmMain.ShowResult - Processed access premitido Ok!");
                    }
                    else
                    {
                        LOG.Debug("frmMain.ShowResult - Processing access denegado...");
                        if (Program._UOWKA._CONFIG.KioskType == 1)
                        { //Es solo entrada
                            if (Program._UOWKA._CONFIG.KioskType1 == 1)
                            {
                                grpShowResult.BackgroundImage =
                                      _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultEntrada") ?
                                      (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultEntrada"] : null;
                                picSample.Image = null;
                                picFotoIdentified.Image = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionNoIdentify") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionNoIdentify"] : null;
                                _ReWritePhoto = (ret == 0);
                                picResultAction.Image =
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionAccesoDenegado"];
                            }
                            else
                            {
                                grpShowResult.BackgroundImage =
                                      _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultSalida") ?
                                      (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultSalida"] : null;
                                picSample.Image = null;
                                picFotoIdentified.Image = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionNoIdentify") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionNoIdentify"] : null;
                                _ReWritePhoto = (ret == 0);
                                picResultAction.Image =
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionSalidaDenegado"];
                            }
                        }
                        else
                        {
                            if (_ID_DEVICE.Equals(Program._UOWKA._CONFIG.PortCOMBarcode1) ||
                                _ID_DEVICE.Equals(Program._UOWKA._CONFIG.SerialSensor1))
                            {
                                grpShowResult.BackgroundImage =
                                       _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultEntrada") ?
                                       (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultEntrada"] : null;
                                picSample.Image = null;
                                picFotoIdentified.Image = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionNoIdentify") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionNoIdentify"] : null;
                                //_ReWritePhoto = false;
                                picResultAction.Image =
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionInfo"];
                            }
                            else
                            {
                                grpShowResult.BackgroundImage =
                                       _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImageFondoResultSalida") ?
                                       (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImageFondoResultSalida"] : null;
                                picSample.Image = null;
                                picFotoIdentified.Image = _THEME_HELPER._THEME.Config.ImageDictionary.ContainsKey("ImagePicResultActionNoIdentify") ?
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionNoIdentify"] : null;
                                //_ReWritePhoto = false;
                                picResultAction.Image =
                                    (Image)_THEME_HELPER._THEME.Config.ImageDictionary["ImagePicResultActionInfo"];
                            }
                        }
                        LOG.Debug("frmMain.ShowResult - Processed access Denegado Ok!");
                        // Ejemplo Respuesta msg
                        //  "Persona realizó Ingreso a las : 03/03/2021 20:49:30"


                        if (paramsreturn.DynamicDataItems.ContainsKey("Code") &&
                            paramsreturn.DynamicDataItems["Code"].ToString().Equals("-2") &&
                            !string.IsNullOrEmpty(msg) && msg.Contains("Persona realizó"))
                        {
                            //labFeedbackDetail.Text = "Ultima Marca " + msg.Substring(msg.IndexOf(":")+1, msg.IndexOf(".") - 1 - msg.IndexOf(":") + 1); 
                            //labFeedbackDetail.Visible = true;
                            //labFeedbackDetail.BringToFront();
                            //labFeedbackDetail.Refresh();
                            LOG.Debug("frmMain.ShowResult - Processing especail Code...");
                            labAntipassback.Visible = true;
                            labAntipassback.Refresh();
                            string code = null;
                            string aux = msg;
                            if (paramsreturn.DynamicDataItems.ContainsKey("Code"))
                            {
                                code = paramsreturn.DynamicDataItems["Code"].ToString();
                                aux = _THEME_HELPER._THEME.Config.GetFeedbackMsg(code);
                            }
                            //if (string.IsNullOrEmpty(aux))
                            //{
                            //    aux = msg;
                            //}
                            labFeedbackDetail.Text = aux;

                        }
                        else
                        {
                            labFeedbackDetail.Text = !string.IsNullOrEmpty(msg) ? msg :
                                                    (paramsreturn.DynamicDataItems.ContainsKey("Msg") ?
                                                        paramsreturn.DynamicDataItems["Msg"].ToString() : "");
                        }


                        labFeedbackDetail.Visible = true;
                        labFeedbackDetail.BringToFront();
                        labFeedbackDetail.Refresh();
                        //}
                    }
                    labFeedbackName.Text = _Nombre;
                    labFeedbackName.Refresh();
                    if (_ReWritePhoto) picFotoIdentified.Image = _Foto;
                    if (!string.IsNullOrEmpty(msg))
                    {
                        labFeedbackDetail.Text = msg;
                        labFeedbackDetail.Visible = true;
                        labFeedbackDetail.BringToFront();
                        labFeedbackDetail.Refresh();
                    }
                    picFotoIdentified.Refresh();
                    grpShowResult.Refresh();
                    grpShowResult.BringToFront();
                    grpShowResult.Visible = true;
                    grpShowResult.Refresh();

                    labId.Text = ret.ToString() + "-" + _ID_RECOGNIZED + " [Accessresult = " + accessresult.ToString() +
                                    "|paramsreturn = " + (paramsreturn != null).ToString() + "|msg=" + msg;
                    labId.Refresh();
                    LOG.Debug("frmMain.ShowResult - Processed OK => " + (string.IsNullOrEmpty(labId.Text) ? "Feddabck Null" : labId.Text));
                }
            }
            catch (Exception ex)
            {
                labError.Text = "ShowResult Exception: " + ex.Message + ". Contacte al administrador..." ;
                labError.Refresh();
                LOG.Error("frmMain.ShowResult - Excp Error: " + ex.Message);
                Program._UOWKA._SUPPORT.GenerateTicket(Program._UOWKA._SUPPORT.SUPPORT_LEVEL_ERROR, "Ticket Automatico - ShowResult Error [" + ex.Message + "]",
                                            "Hubo un error al mostrar resultados. Revisar Log attachado"
                                            + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                                            true);
            }
            LOG.Debug("frmMain.ShowResult OUT!");
        }

        private Image GetImageFinger(List<Sample> samplesCaptured)
        {
            Image retImage = null;
            try
            {
                LOG.Debug("frmMain.GetImageFinger IN... samplesCaptured==null => " + (samplesCaptured == null).ToString());
                if (samplesCaptured == null)
                    return null;

                LOG.Debug("frmMain.GetImageFinger - Tomo imagen original...");
                retImage = (Bitmap)_SamplesCaptured[_SamplesCaptured.Count - 2].Data;
                //foreach (Sample item in samplesCaptured)
                //{
                //    if (item.MinutiaeType == Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_JPG)
                //    {
                //        wsq = (byte[])item.Data;
                //    }

                //    if (item.MinutiaeType == Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_RAW)
                //    {
                //        rawOut = (byte[])item.Data;
                //    }
                //}

                ////Si no venia raw pero si wsq => Descomprimo wsq para generar raw
                //if (rawOut == null && wsq != null)
                //{
                //    LOG.Debug("BiometricHelper.GetSample - Decode WSQ...");
                //    Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                //    short w, h;
                //    dec.DecodeMemory(wsq, out w, out h, out rawOut);
                //}
            }
            catch (Exception ex)
            {
                retImage = null;
                LOG.Error("frmMain.GetImageFinger - Error: " + ex.Message);
            }
            LOG.Debug("frmMain.GetImageFinger OUT => Encontro? => rawOut != null => " + (retImage != null));
            return retImage;
        }

        /// <summary>
        /// Cierra ventana de kiosko
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labClose_Click(object sender, EventArgs e)
        {
            this.Close();
            try
            {
                if (_ThreadToRefrechBIRs != null && _ThreadToRefrechBIRs.IsAlive)
                {
                    _ThreadToRefrechBIRs.Abort();
                    _ThreadToRefrechBIRs = null;
                }
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
            Application.Exit();
        }

        /// <summary>
        /// Abre ventana de AcercaDe y soporte.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picSupport_Click(object sender, EventArgs e)
        {
            try
            {
                frmSupport frmSup = new frmSupport();
                frmSup.Show(this);
            }
            catch (Exception ex)
            {
                LOG.Error(" Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Abre ventana de AcercaDe y soporte.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picAcercaDe_Click(object sender, EventArgs e)
        {
            try
            {
                frmSupport frmSup = new frmSupport();
                frmSup.Show(this);
            }
            catch (Exception ex)
            {
                LOG.Error(" Error: " + ex.Message);
            }
        }


        private void picReinit_Click(object sender, EventArgs e)
        {
            //TODO - Refresh manual de huellas...
            try
            {
                if (Program._UOWKA._CONFIG.AccessByFingerprintEnabled || Program._UOWKA._CONFIG.AccessByFacialEnabled)
                {
                    grpRefreshLoadBIRs.Visible = true;
                    prgBarRefreshLoadBIRs.Value = 25;
                    grpRefreshLoadBIRs.Refresh();

               
                    int ret = _BIOMETRIC_HELPER.RefreshLoadBIRs();
                    if (ret == 0)
                    {
                        prgBarRefreshLoadBIRs.Value = prgBarRefreshLoadBIRs.Maximum;
                        picRefreshLoadBIRs.Image = Properties.Resources.check_OK_T;
                        grpRefreshLoadBIRs.Refresh();
                        if (_BIOMETRIC_HELPER != null && BiometricHelper.objTemplates != null)
                        {
                            labQTemplates.Text = "#QH: " + BiometricHelper.objTemplates.Count().ToString();
                        }
                        else
                        {
                            labQTemplates.Text = "#QH: 0";
                        }
                        System.Threading.Thread.Sleep(1000);
                    }
                    else
                    {
                        LOG.Error("frmMain.picReinit_Click _BIOMETRIC_HELPER.RefreshLoadBIRs() ret = " + ret.ToString() +
                            "No se pudo resincronizar de forma automatica!");
                        Program._UOWKA._SUPPORT.GenerateTicket(Program._UOWKA._SUPPORT.SUPPORT_LEVEL_ERROR, "Ticket Automatico - Refresh huellas Con Ret = " + ret.ToString(),
                                                  "Error al refrescar automaticamente las huellas. Posible perdida de conexion con server. Revisar Log Attachado."
                                                  + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                                                  true);
                    }
                    grpRefreshLoadBIRs.Visible = false;
                    picRefreshLoadBIRs.Image = Properties.Resources.ImageReinit;
                    prgBarRefreshLoadBIRs.Value = 0;
                    grpRefreshLoadBIRs.Refresh();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(this, "Error refrescando datos biometricos. Contacte con el administrador...", "Atención...",
                //    MessageBoxButtons.OK, MessageBoxIcon.Error);
                LOG.Error("frmMain.picReinit_Click Excpt Error: ", ex);
                Program._UOWKA._SUPPORT.GenerateTicket(Program._UOWKA._SUPPORT.SUPPORT_LEVEL_ERROR, "Ticket Automatico - Refresh huellas Con Error [" + ex.Message + "]",
                                              "Error al refrescar automaticamente las huellas. Posible perdida de conexion con server. Revisar Log Attachado." 
                                              + Environment.NewLine + src.Helpers.SupportHelper.GetInfoKiosk(),
                                              true);
                
            }
        }

        private void timer_prgBarRefreshLoadBIRs_Tick(object sender, EventArgs e)
        {
            //timer_prgBarRefreshLoadBIRs;
            try
            {
                int index = 1;

                if (prgBarRefreshLoadBIRs.Value == prgBarRefreshLoadBIRs.Maximum)
                {

                }
            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }
        }

        #endregion Process Operation

        #region Zone PIN

        //Este PICTURE tiene evento porque permite ingresar datos a mano 
        private void picPINEnabled_Click(object sender, EventArgs e)
        {
            try
            {
                labTxtPIN.Text = "";
                grpPIN.Enabled = true;
                grpPIN.Visible = true;
                grpPIN.BringToFront();
                this.Refresh();
                //Inicio el timer para cerrar el panel de ingreso
                timer_PINPanel.Interval = Program._UOWKA._CONFIG.TimeToShowPINPanel;
                timer_PINPanel.Enabled = true;

            }
            catch (Exception ex)
            {
                LOG.Error("frmMain.picPINEnabled_Click - Excp [" + ex.Message + "]");
            }
        }
        private void timer_PINPanel_Tick(object sender, EventArgs e)
        {
            try
            {
                this.grpPIN.Visible = false;
                this.grpPIN.Enabled = false;
                this.Refresh();
                this.timer_PINPanel.Enabled = false;
            }
            catch (Exception ex)
            {
                LOG.Error("frmMain.timer_PINPanel_Tick - Excp [" + ex.Message + "]");
            }
        }

        private void labPIN_Click(object sender, EventArgs e)
        {
            if ((string.IsNullOrEmpty(labTxtPIN.Text)) || 
                (!string.IsNullOrEmpty(labTxtPIN.Text) && labTxtPIN.Text.Trim().Length < 9) ||
                (((Label)sender).Name.Equals("labPINDelete")) ||
                ((Label)sender).Name.Equals("labPINEnter"))
            {
                if (((Label)sender).Name.Equals("labPIN1"))
                {
                    //txtPIN.Text = txtPIN.Text + "1";
                    labTxtPIN.Text = labTxtPIN.Text + "1";
                } else if (((Label)sender).Name.Equals("labPIN2"))
                {
                    //txtPIN.Text = txtPIN.Text + "2";
                    labTxtPIN.Text = labTxtPIN.Text + "2";
                }
                else if (((Label)sender).Name.Equals("labPIN3"))
                {
                    //txtPIN.Text = txtPIN.Text + "3";
                    labTxtPIN.Text = labTxtPIN.Text + "3";
                }
                else if (((Label)sender).Name.Equals("labPIN4"))
                {
                    //txtPIN.Text = txtPIN.Text + "4";
                    labTxtPIN.Text = labTxtPIN.Text + "4";
                }
                else if (((Label)sender).Name.Equals("labPIN5"))
                {
                    //txtPIN.Text = txtPIN.Text + "5";
                    labTxtPIN.Text = labTxtPIN.Text + "5";
                }
                else if (((Label)sender).Name.Equals("labPIN6"))
                {
                    //txtPIN.Text = txtPIN.Text + "6";
                    labTxtPIN.Text = labTxtPIN.Text + "6";
                }
                else if (((Label)sender).Name.Equals("labPIN7"))
                {
                    //txtPIN.Text = txtPIN.Text + "7";
                    labTxtPIN.Text = labTxtPIN.Text + "7";
                }
                else if (((Label)sender).Name.Equals("labPIN8"))
                {
                    //txtPIN.Text = txtPIN.Text + "8";
                    labTxtPIN.Text = labTxtPIN.Text + "8";
                }
                else if (((Label)sender).Name.Equals("labPIN9"))
                {
                    //txtPIN.Text = txtPIN.Text + "9";
                    labTxtPIN.Text = labTxtPIN.Text + "9";
                }
                else if (((Label)sender).Name.Equals("labPIN0"))
                {
                    //txtPIN.Text = txtPIN.Text + "0";
                    labTxtPIN.Text = labTxtPIN.Text + "0";
                }
                else if (((Label)sender).Name.Equals("labPINK"))
                {
                    //txtPIN.Text = txtPIN.Text + "K";
                    labTxtPIN.Text = labTxtPIN.Text + "K";
                }
                else if (((Label)sender).Name.Equals("labPINDelete"))
                {
                    //txtPIN.Text = "";
                    labTxtPIN.Text = "";
                }
                else if (((Label)sender).Name.Equals("labPINEnter"))
                {
                    //txtPIN.Text = txtPIN.Text + "Enter";
                    txtPIN.Text = labTxtPIN.Text;
                    ProcesaPINInput(labTxtPIN.Text);
                    //timer_PINPanel.Enabled = false;
                    //this.grpPIN.Visible = false;
                    //this.grpPIN.Enabled = false;
                    //this.Refresh();
                }
                if (this.grpPIN.Enabled)
                {
                    timer_PINPanel.Interval = Program._UOWKA._CONFIG.TimeToShowPINPanel;
                    timer_PINPanel.Enabled = false;
                    timer_PINPanel.Enabled = true;
                }
            }
        }

        private void ProcesaPINInput(string input)
        {
            try
            {
                if (string.IsNullOrEmpty(input))
                {
                    timer_PINPanel.Enabled = false;
                    this.grpPIN.Visible = false;
                    this.grpPIN.Enabled = false;
                    this.Refresh();
                }

                Bio.Core.Utils.RUN run = Bio.Core.Utils.RUN.Convertir(input);

                if (run == null)
                {
                    //this.txtPIN.BackColor = Color.Red;
                    //this.labTxtPIN.BackColor = Color.Red;
                    this.labTxtPIN.ForeColor = Color.Red;
                    this.picVerifyInput.Image = Properties.Resources.nook;
                    this.picVerifyInput.Visible = true;
                    this.Refresh();
                    Thread.Sleep(1500);
                    timer_PINPanel.Enabled = false;
                    this.grpPIN.Visible = false;
                    this.grpPIN.Enabled = false;
                    //this.txtPIN.BackColor = Color.WhiteSmoke;
                    //this.labTxtPIN.BackColor = Color.Transparent;
                    this.labTxtPIN.ForeColor = Color.White;
                    this.txtPIN.Text = "";
                    this.picVerifyInput.Image = Properties.Resources.nook;
                    this.picVerifyInput.Visible = false;
                    this.Refresh();
                }
                else
                {
                    _ID_RECOGNIZED = input;
                    if (rbEntradaManual.Checked)
                    {
                        _ID_DEVICE = Program._UOWKA._CONFIG.PortCOMBarcode1;
                    }
                    else
                    {
                        _ID_DEVICE = Program._UOWKA._CONFIG.PortCOMBarcode2;
                    }

                    timer_PINPanel.Enabled = false;
                    this.grpPIN.Visible = false;
                    this.grpPIN.Enabled = false;
                    this.Refresh();
                    hayCedula = true;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("frmMain.ProcesaPINInput - Excp [" + ex.Message + "]");
            }
        }

        private void frmMain_KeyPress(object sender, KeyPressEventArgs e)
        {
            //string s = e.KeyChar.ToString();
            //if (s.Equals("m") || s.Equals("M"))
            //{
            //    if (Program._UOWKA._CONFIG.IsDummy)
            //    {
            //        txtRutManual.Visible = true;
            //        btnINRutManual.Visible = true;
            //        this.Refresh();
            //    }
            //}
            //if ((string.IsNullOrEmpty(txtPIN.Text)) ||
            //    (!string.IsNullOrEmpty(txtPIN.Text) && txtPIN.Text.Trim().Length < 4) ||
            //    (e.KeyChar.Equals('d')) || (e.KeyChar.Equals('d')))
            //{
            //    if (e.KeyChar.Equals('1'))
            //    {
            //        txtPIN.Text = txtPIN.Text + "1";
            //    }
            //    else if (((Label)sender).Name.Equals("labPIN2"))
            //    {
            //        txtPIN.Text = txtPIN.Text + "2";
            //    }
            //    else if (((Label)sender).Name.Equals("labPIN3"))
            //    {
            //        txtPIN.Text = txtPIN.Text + "3";
            //    }
            //    else if (((Label)sender).Name.Equals("labPIN4"))
            //    {
            //        txtPIN.Text = txtPIN.Text + "4";
            //    }
            //    else if (((Label)sender).Name.Equals("labPIN5"))
            //    {
            //        txtPIN.Text = txtPIN.Text + "5";
            //    }
            //    else if (((Label)sender).Name.Equals("labPIN6"))
            //    {
            //        txtPIN.Text = txtPIN.Text + "6";
            //    }
            //    else if (((Label)sender).Name.Equals("labPIN7"))
            //    {
            //        txtPIN.Text = txtPIN.Text + "7";
            //    }
            //    else if (((Label)sender).Name.Equals("labPIN8"))
            //    {
            //        txtPIN.Text = txtPIN.Text + "8";
            //    }
            //    else if (((Label)sender).Name.Equals("labPIN9"))
            //    {
            //        txtPIN.Text = txtPIN.Text + "9";
            //    }
            //    else if (((Label)sender).Name.Equals("labPIN0"))
            //    {
            //        txtPIN.Text = txtPIN.Text + "0";
            //    }
            //    else if (((Label)sender).Name.Equals("labPINDelete"))
            //    {
            //        txtPIN.Text = txtPIN.Text + "1";
            //    }
            //    else if (((Label)sender).Name.Equals("labPINEnter"))
            //    {
            //        txtPIN.Text = txtPIN.Text + "Enter";
            //    }
            //}
        }


        #endregion Zone PIN

  


        private void labWorking_Click(object sender, EventArgs e)
        {

        }

        private void timerReloj_Tick(object sender, EventArgs e)
        {
            try
            {
                if (!_IS_PROCESSING)
                {
                    labReloj.Text = DateTime.Now.ToString("HH:mm");
                    //labReloj.Visible = true;
                    labReloj.Refresh();
                } else
                {
                    LOG.Debug("frmMain.timerReloj_Tick - Skip actualizacion reloj porque estaba en proceso de apertura " +
                                "[" + DateTime.Now.ToString("HH:mm") + "]");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("frmMain.timerReloj_Tick Error: " + ex.Message);
            }
        }

        private void frmMain_Click(object sender, EventArgs e)
        {
            try
            {
                if (((MouseEventArgs)e).Button == System.Windows.Forms.MouseButtons.Right)
            {
                    if (MessageBox.Show(this, "Desea cerrar el kiosko?", "Atención...", 
                                          MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                                        
                    {
                        labClose_Click(null, null);
                    }
                }
            }
            catch (Exception)
            {
            }
            
        }

        /// <summary>
        /// Abre para editar database en json
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labBtnDummy_Click(object sender, EventArgs e)
        {
            try
            {
                Dummy.frmDBDummy frmDummy = new Dummy.frmDBDummy();
                frmDummy.Show(this);
            }
            catch (Exception ex)
            {

            }
        }

        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyData == Keys.F1)
            //{
            //    if (Program._UOWKA._CONFIG.IsDummy)
            //    {
            //        txtRutManual.Visible = true;
            //        btnINRutManual.Visible = true;
            //        this.Refresh();
            //    }
            //}
        }

        private void txtRutManual_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar.Equals('m'))
            //{
            //    if (Program._UOWKA._CONFIG.IsDummy)
            //    {
            //        txtRutManual.Visible = true;
            //        btnINRutManual.Enabled = true;
            //        this.Refresh();
            //    }
            //}
        }

        private void txtRutManual_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyData == Keys.F1)
            //{
            //    if (Program._UOWKA._CONFIG.IsDummy)
            //    {
            //        txtRutManual.Visible = true;
            //        btnINRutManual.Visible = true;
            //        this.Refresh();
            //    }
            //}
        }

        private void btnINRutManual_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtRutManual.Text))
            {
                //txtRutManual.Visible = false;
                //btnINRutManual.Visible = false;
                //this.Refresh();
                _ID_RECOGNIZED = txtRutManual.Text;
                if (rbEntradaManual.Checked)
                {
                    _ID_DEVICE = Program._UOWKA._CONFIG.PortCOMBarcode1;
                } else
                {
                    _ID_DEVICE = Program._UOWKA._CONFIG.PortCOMBarcode2;
                }
                

                hayCedula = true;
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (_CaptureCam != null)
                {
                    _CaptureCam.Stop();
                    _CaptureCam.Dispose();
                    _CaptureCam = null;
                }

                //Agregar depuracion de BusinessRules (por thread de sportlifenorte)
                Program._UOWKA._BUSINESSRULE_ENGINE.Dispose(); 

            }
            catch (Exception ex) 
            {

            }

        }

        private bool _ClearLastIdRecognized = false;
        private void timer_ClearIdRecognized_Tick(object sender, EventArgs e)
        {
            if (_ClearLastIdRecognized)
            {
                _LAST_ID_RECOGNIZED = null;
                _ClearLastIdRecognized = false;
            }
        }
    }
}
