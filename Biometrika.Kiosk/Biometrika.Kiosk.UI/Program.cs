﻿using Biometrika.BioApi20.BSP;
using Biometrika.Kiosk.Aplication;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace Biometrika.Kiosk.UI
{
    /// <summary>
    /// UI gráfica del Kiosko.
    /// Según el Theme cambia la gráfica.
    /// Aqui se resuelve la identificación de la persona, luego con el UOWKioskAplication
    /// del Kiosko se resuleven la reglas de negocio, marcas y acciones.
    /// </summary>
    static class Program
    {

        //Log generla del Program
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Program));
         
        internal static string _PATH_EXE;

        //internal static frmInitSplash _FRM_INIT_SPLASH;

        /// <summary>
        /// Unidad de trabajo de Kiosko Aplication para BR, Action y Mark 
        /// </summary>
        internal static UOWKioskAplication _UOWKA;

        internal static BSPBiometrika _BSP;


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            //src.Helpers.ThemeHelper _THEME_HELPER = new src.Helpers.ThemeHelper();
            //_THEME_HELPER._THEME = new src.Themes.Theme("SportlifeNorteVertical");
            //src.Themes.ThemeConfig Config = new src.Themes.ThemeConfig();
            //Config.FillDefault();
            //SerializeHelper.SerializeToFile(Config, "SportlifeNorteVertical.Theme.xml");

            XmlConfigurator.Configure(new FileInfo("Logger.cfg"));

            LOG.Info("Biometrika.Kiosk.UI.Main - Init Kiosko...");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Si ya esta ejecutando porque quedo pegado => Mata todos los que se llaman igual
            //para evitar que se buguee la lectura u otras cosas
            IsExecutingApplication(); 
            //if (IsExecutingApplication())
            //{
            //    LOG.Info("Biometrika.Kiosk.UI.Main - Aplicación ya en uso. No deja ejecutar!");
            //}
            //else
            //{

            //Add Splash
            frmMain _FRM_MAIN = new frmMain();
            _FRM_MAIN._FRM_INIT_SPLASH = new frmInitSplash();
            _FRM_MAIN._FRM_INIT_SPLASH.Show();
            _FRM_MAIN._FRM_INIT_SPLASH.SetStep(1);

            //_PATH_EXE = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            _PATH_EXE = Application.StartupPath;
            LOG.Info("Biometrika.Kiosk.UI.Main - Start Path = " + _PATH_EXE);

            LOG.Info("Biometrika.Kiosk.UI.Main - Creando UOWKioskAplication...");
            _UOWKA = new UOWKioskAplication();

            LOG.Info("Biometrika.Kiosk.UI.Main - Inicializando UOWKioskAplication...");
            string msgfeedback = null;
            int retini = _UOWKA.Initialization(out msgfeedback);
            if (retini == 0)
            {
                LOG.Info("Biometrika.Kiosk.UI.Main - UOWKioskAplication Inicializado OK! => " + msgfeedback);
                _UOWKA._SUPPORT.GenerateTicket(_UOWKA._SUPPORT.SUPPORT_LEVEL_INFO, "Ticket Automatico - Inicio de Kiosko sin problemas",
                                                src.Helpers.SupportHelper.GetInfoKiosk(), true);
                //_UOWKA._SUPPORT.GenerateTicket(_UOWKA._SUPPORT.SUPPORT_LEVEL_WARN, "Ticket Automatico - Inicio de Kiosko sin problemas",
                //                                src.Helpers.SupportHelper.GetInfoKiosk(), true);
                //_UOWKA._SUPPORT.GenerateTicket(_UOWKA._SUPPORT.SUPPORT_LEVEL_ERROR, "Ticket Automatico - Inicio de Kiosko sin problemas",
                //                                src.Helpers.SupportHelper.GetInfoKiosk(), true);
            }
            else
            {
                LOG.Fatal("Biometrika.Kiosk.UI.Main - UOWKioskAplication ERROR Inicializando! => Sale...");
                _UOWKA._SUPPORT.GenerateTicket(_UOWKA._SUPPORT.SUPPORT_LEVEL_FATAL, "Ticket Automatico - Inicio Con Error [" + retini.ToString() + "]",
                                               "Feedback Init Kiosk => " + msgfeedback + Environment.NewLine +
                                                src.Helpers.SupportHelper.GetInfoKiosk(), 
                                               true);
                //MessageBox.Show("Error inicializando el kiosko. Contacte con su soporte...");
                //MsgError
            }
            _FRM_MAIN._FRM_INIT_SPLASH.SetStep(2);
            Application.Run(_FRM_MAIN);
            //}
        }

        private static bool IsExecutingApplication()
        {
            bool ret = false;
            try
            {
                LOG.Info("Biometrika.Kiosk.UI.IsExecutingApplication IN - Entrando a controlar aplicacion ejecutando...");
                // Proceso actual
                Process currentProcess = Process.GetCurrentProcess();

                // Matriz de procesos
                Process[] processes = Process.GetProcesses();

                // Recorremos los procesos en ejecución
                foreach (Process p in processes)
                {
                    if (p.Id != currentProcess.Id)
                    {
                        if (p.ProcessName == currentProcess.ProcessName)
                        {
                            LOG.Info("Biometrika.Kiosk.UI.IsExecutingApplication - Mato p.Id = " + p.Id.ToString() +
                                     " - p.ProcessName = " + p.ProcessName + "!");
                            ret = true;
                            p.Kill();
                            //return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            { 
                ret = false;
                LOG.Error("Biometrika.Kiosk.UI.IsExecutingApplication - Excp Error: " + ex.Message);
            }
            LOG.Info("Biometrika.Kiosk.UI.IsExecutingApplication OUT!"); 
            return ret;
        }

    }
}
