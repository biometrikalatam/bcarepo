﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Kiosk.UI
{
    /// <summary>
    /// Formulario para mostrar configuraciones, estados, etc. También con datos de soporte.
    /// Se debe completar con mas operaciones como pedir soporte desde aqui adjuntando toda la información 
    /// relevantge para el soporte, como por ejemplo los .cfg y logs comprimidos y attachados, 
    /// asi como otra información relevante.
    /// </summary>
    public partial class frmSupport : Form
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(frmSupport));

        private int borderRadius = 25;
        private int borderSize = 0;
        private Color borderColor = Color.Gray; // Color.FromArgb(128, 128, 255);

        public frmSupport()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            this.Padding = new Padding(borderSize);
            //this.panelTitleBar.BackColor = borderColor;
            //this.BackColor = borderColor;
        }

        private void frmSupport_Load(object sender, EventArgs e)
        {
            try
            {
                Version version = Assembly.GetExecutingAssembly().GetName().Version;
                labVersion.Text = "v" + version.ToString();




                FillDetails();

            }
            catch (Exception ex)
            {
                LOG.Error("frmSupport.frmSupport_Load - Excp Error: " + ex.Message);
            }
        }

        private void FillDetails()
        {
            int len = 0;
            try
            {
                rtbDetails.Font = new Font("Arial", 10, FontStyle.Regular);

                rtbDetails.Text = "Biometrika Kiosk: " + labVersion.Text;
                rtbDetails.SelectionStart = 0;
                rtbDetails.SelectionLength = "Biometrika Kiosk:".Length;
                rtbDetails.SelectionFont = new Font("Arial", 12, FontStyle.Bold);
                rtbDetails.SelectionLength = 0;

                AddLine(Environment.NewLine + "-----------------------------------------------------------------------", 10, "");

                rtbDetails.AppendText(Environment.NewLine);
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("Kiosk Name: ", 11, Program._UOWKA._CONFIG.Name);

                rtbDetails.AppendText(Environment.NewLine);
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("Tecnologías Habilitadas:", 11, "");
                rtbDetails.AppendText(Environment.NewLine);
                AddLine(" > Fingerprint: ", 10, (Program._UOWKA._CONFIG.AccessByFingerprintEnabled.ToString()));
                rtbDetails.AppendText(Environment.NewLine);
                AddLine(" > Barcode: ", 10, (Program._UOWKA._CONFIG.AccessByBarcodeEnabled.ToString()));
                rtbDetails.AppendText(Environment.NewLine);
                AddLine(" > Facial: ", 10, (Program._UOWKA._CONFIG.AccessByFacialEnabled.ToString()));
                rtbDetails.AppendText(Environment.NewLine);
                AddLine(" > MRZ: ", 10, (Program._UOWKA._CONFIG.AccessByMRZEnabled.ToString()));

                rtbDetails.AppendText(Environment.NewLine);
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("Kiosk Type: ", 11, (Program._UOWKA._CONFIG.KioskType.ToString()));

                rtbDetails.AppendText(Environment.NewLine);
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("Theme: ", 11, (Program._UOWKA._CONFIG.Theme.ToString()));
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("  Path Theme: ", 10, Program._PATH_EXE + "\\Theme\\" + Program._UOWKA._CONFIG.Theme);



                rtbDetails.AppendText(Environment.NewLine);
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("Path Logger Config: ", 11, (Program._UOWKA._CONFIG.PathLoggerConfig.ToString()));

                rtbDetails.AppendText(Environment.NewLine);
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("Path Matcher Config: ", 11, (Program._UOWKA._CONFIG.PathMatcherConfig.ToString()));

                rtbDetails.AppendText(Environment.NewLine);
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("Time To Show Result: ", 11, (Program._UOWKA._CONFIG.TimeToShowResult.ToString()));

                rtbDetails.AppendText(Environment.NewLine);
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("Estados Engines:", 11, "");
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("  BusinessRules Engine: ", 10, Program._UOWKA._BUSINESSRULE_ENGINE.Name);
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("    Path: ", 10, Program._UOWKA._BUSINESSRULE_ENGINE.Config.DynamicDataItems["BusinessRuleDllPath"].ToString());
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("    Inicializado: ", 10, Program._UOWKA._BUSINESSRULE_ENGINE.Initialized.ToString());

                rtbDetails.AppendText(Environment.NewLine);
                AddLine("  Action Engine: ", 10, Program._UOWKA._ACTION_ENGINE.Name);
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("    Path: ", 10, Program._UOWKA._ACTION_ENGINE.Config.DynamicDataItems["ActionDllPath"].ToString());
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("    Inicializado: ", 10, Program._UOWKA._ACTION_ENGINE.Initialized.ToString());

                rtbDetails.AppendText(Environment.NewLine);
                AddLine("  Mark Engine: ", 10, Program._UOWKA._MARK_ENGINE.Name);
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("    Path: ", 10, Program._UOWKA._MARK_ENGINE.Config.DynamicDataItems["MarkDllPath"].ToString());
                rtbDetails.AppendText(Environment.NewLine);
                AddLine("    Inicializado: ", 10, Program._UOWKA._MARK_ENGINE.Initialized.ToString());


            }
            catch (Exception ex)
            {
                LOG.Error("frmSupport.FillDetails - Excp Error: " + ex.Message);
            }
        }

        private void AddLine(string title, int size, string body)
        {
            int len = rtbDetails.Text.Length;
            rtbDetails.AppendText(title + body);
            rtbDetails.SelectionStart = len;
            rtbDetails.SelectionLength = title.Length;
            rtbDetails.SelectionFont = new Font("Arial", size, FontStyle.Bold);
            rtbDetails.SelectionLength = 0;
        }

        private void SetText(string text, int size, int type)
        {
            try
            {
                rtbDetails.Text += text;
                rtbDetails.Select(rtbDetails.Text.IndexOf(text), rtbDetails.Text.IndexOf(text) + text.Length);
                if (type == 1)
                {
                    rtbDetails.SelectionFont = new Font("Verdana", size, FontStyle.Bold);
                } else
                {
                    rtbDetails.SelectionFont = new Font("Verdana", size, FontStyle.Regular);
                }
                
            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
           
        }

        private void labClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private GraphicsPath GetRoundedPath(Rectangle rect, float radius)
        {
            GraphicsPath path = new GraphicsPath();
            float curveSize = radius * 2F;
            path.StartFigure();
            path.AddArc(rect.X, rect.Y, curveSize, curveSize, 180, 90);
            path.AddArc(rect.Right - curveSize, rect.Y, curveSize, curveSize, 270, 90);
            path.AddArc(rect.Right - curveSize, rect.Bottom - curveSize, curveSize, curveSize, 0, 90);
            path.AddArc(rect.X, rect.Bottom - curveSize, curveSize, curveSize, 90, 90);
            path.CloseFigure();
            return path;
        }

        private void FormRegionAndBorder(Form form, float radius, Graphics graph, Color borderColor, float borderSize)
        {
            if (this.WindowState != FormWindowState.Minimized)
            {
                using (GraphicsPath roundPath = GetRoundedPath(form.ClientRectangle, radius))
                using (Pen penBorder = new Pen(borderColor, borderSize))
                using (Matrix transform = new Matrix())
                {
                    graph.SmoothingMode = SmoothingMode.AntiAlias;
                    form.Region = new Region(roundPath);
                    if (borderSize >= 1)
                    {
                        Rectangle rect = form.ClientRectangle;
                        float scaleX = 1.0F - ((borderSize + 1) / rect.Width);
                        float scaleY = 1.0F - ((borderSize + 1) / rect.Height);
                        transform.Scale(scaleX, scaleY);
                        transform.Translate(borderSize / 1.6F, borderSize / 1.6F);
                        graph.Transform = transform;
                        graph.DrawPath(penBorder, roundPath);
                    }
                }
            }
        }

        private void ControlRegionAndBorder(Control control, float radius, Graphics graph, Color borderColor)
        {
            using (GraphicsPath roundPath = GetRoundedPath(control.ClientRectangle, radius))
            using (Pen penBorder = new Pen(borderColor, 1))
            {
                graph.SmoothingMode = SmoothingMode.AntiAlias;
                control.Region = new Region(roundPath);
                graph.DrawPath(penBorder, roundPath);
            }
        }

        private void DrawPath(Rectangle rect, Graphics graph, Color color)
        {
            using (GraphicsPath roundPath = GetRoundedPath(rect, borderRadius))
            using (Pen penBorder = new Pen(color, 3))
            {
                graph.DrawPath(penBorder, roundPath);
            }
        }

        private struct FormBoundsColors
        {
            public Color TopLeftColor;
            public Color TopRightColor;
            public Color BottomLeftColor;
            public Color BottomRightColor;
        }
        private FormBoundsColors GetFormBoundsColors()
        {
            var fbColor = new FormBoundsColors();
            using (var bmp = new Bitmap(1, 1))
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle rectBmp = new Rectangle(0, 0, 1, 1);
                //Top Left
                rectBmp.X = this.Bounds.X - 1;
                rectBmp.Y = this.Bounds.Y;
                graph.CopyFromScreen(rectBmp.Location, Point.Empty, rectBmp.Size);
                fbColor.TopLeftColor = bmp.GetPixel(0, 0);
                //Top Right
                rectBmp.X = this.Bounds.Right;
                rectBmp.Y = this.Bounds.Y;
                graph.CopyFromScreen(rectBmp.Location, Point.Empty, rectBmp.Size);
                fbColor.TopRightColor = bmp.GetPixel(0, 0);
                //Bottom Left
                rectBmp.X = this.Bounds.X;
                rectBmp.Y = this.Bounds.Bottom;
                graph.CopyFromScreen(rectBmp.Location, Point.Empty, rectBmp.Size);
                fbColor.BottomLeftColor = bmp.GetPixel(0, 0);
                //Bottom Right
                rectBmp.X = this.Bounds.Right;
                rectBmp.Y = this.Bounds.Bottom;
                graph.CopyFromScreen(rectBmp.Location, Point.Empty, rectBmp.Size);
                fbColor.BottomRightColor = bmp.GetPixel(0, 0);
            }
            return fbColor;
        }


        private void frmSupport_Paint(object sender, PaintEventArgs e)
        {
            //-> SMOOTH OUTER BORDER
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            Rectangle rectForm = this.ClientRectangle;
            int mWidht = rectForm.Width / 2;
            int mHeight = rectForm.Height / 2;
            var fbColors = GetFormBoundsColors();
            ////Top Left
            //DrawPath(rectForm, e.Graphics, fbColors.TopLeftColor);
            ////Top Right
            //Rectangle rectTopRight = new Rectangle(mWidht, rectForm.Y, mWidht, mHeight);
            //DrawPath(rectTopRight, e.Graphics, fbColors.TopRightColor);
            ////Bottom Left
            //Rectangle rectBottomLeft = new Rectangle(rectForm.X, rectForm.X + mHeight, mWidht, mHeight);
            //DrawPath(rectBottomLeft, e.Graphics, fbColors.BottomLeftColor);
            ////Bottom Right
            //Rectangle rectBottomRight = new Rectangle(mWidht, rectForm.Y + mHeight, mWidht, mHeight);
            //DrawPath(rectBottomRight, e.Graphics, fbColors.BottomRightColor);
            //-> SET ROUNDED REGION AND BORDER
            FormRegionAndBorder(this, borderRadius, e.Graphics, borderColor, borderSize);
        }
    }
}
