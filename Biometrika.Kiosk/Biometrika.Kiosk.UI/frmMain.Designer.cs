﻿namespace Biometrika.Kiosk.UI
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.timerProcessFingerSampleCaptured = new System.Windows.Forms.Timer(this.components);
            this.timerMRZReset = new System.Windows.Forms.Timer(this.components);
            this.rtBoxInputMRZ = new System.Windows.Forms.RichTextBox();
            this.btnTestMRZ = new System.Windows.Forms.Button();
            this.grpShowResult = new System.Windows.Forms.GroupBox();
            this.picSample = new System.Windows.Forms.PictureBox();
            this.picFotoIdentified = new System.Windows.Forms.PictureBox();
            this.labAntipassback = new System.Windows.Forms.Label();
            this.picResultAction = new System.Windows.Forms.PictureBox();
            this.grpPIN = new System.Windows.Forms.GroupBox();
            this.picVerifyInput = new System.Windows.Forms.PictureBox();
            this.labTxtPIN = new System.Windows.Forms.Label();
            this.labPINK = new System.Windows.Forms.Label();
            this.labPINDelete = new System.Windows.Forms.Label();
            this.labPINEnter = new System.Windows.Forms.Label();
            this.labPIN0 = new System.Windows.Forms.Label();
            this.labPIN9 = new System.Windows.Forms.Label();
            this.labPIN8 = new System.Windows.Forms.Label();
            this.labPIN6 = new System.Windows.Forms.Label();
            this.labPIN5 = new System.Windows.Forms.Label();
            this.labPIN3 = new System.Windows.Forms.Label();
            this.labPIN2 = new System.Windows.Forms.Label();
            this.labPIN7 = new System.Windows.Forms.Label();
            this.labPIN4 = new System.Windows.Forms.Label();
            this.labPIN1 = new System.Windows.Forms.Label();
            this.txtPIN = new System.Windows.Forms.TextBox();
            this.labFeedbackName = new System.Windows.Forms.Label();
            this.labFeedbackDetail = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labId = new System.Windows.Forms.Label();
            this.grpRefreshLoadBIRs = new System.Windows.Forms.GroupBox();
            this.prgBarRefreshLoadBIRs = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.picRefreshLoadBIRs = new System.Windows.Forms.PictureBox();
            this.panelWorking = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.labWorking = new System.Windows.Forms.Label();
            this.picPINEnabled = new System.Windows.Forms.PictureBox();
            this.picFacialEnabled = new System.Windows.Forms.PictureBox();
            this.picFingerEnabled = new System.Windows.Forms.PictureBox();
            this.picMRZEnabled = new System.Windows.Forms.PictureBox();
            this.picLCBEnabled = new System.Windows.Forms.PictureBox();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.picClose = new System.Windows.Forms.PictureBox();
            this.picAcercaDe = new System.Windows.Forms.PictureBox();
            this.labVersion = new System.Windows.Forms.Label();
            this.picSupport = new System.Windows.Forms.PictureBox();
            this.picReinit = new System.Windows.Forms.PictureBox();
            this.labQTemplates = new System.Windows.Forms.Label();
            this.labKioskType = new System.Windows.Forms.Label();
            this.timerSerialLCB = new System.Windows.Forms.Timer(this.components);
            this.labError = new System.Windows.Forms.Label();
            this.timer_prgBarRefreshLoadBIRs = new System.Windows.Forms.Timer(this.components);
            this.labReloj = new System.Windows.Forms.Label();
            this.timerReloj = new System.Windows.Forms.Timer(this.components);
            this.timer_Reinit_LCB = new System.Windows.Forms.Timer(this.components);
            this.labBtnDummy = new System.Windows.Forms.Label();
            this.txtRutManual = new System.Windows.Forms.TextBox();
            this.btnINRutManual = new System.Windows.Forms.Button();
            this.rbEntradaManual = new System.Windows.Forms.RadioButton();
            this.rbSalidaManual = new System.Windows.Forms.RadioButton();
            this.timer_PINPanel = new System.Windows.Forms.Timer(this.components);
            this.picVideo = new System.Windows.Forms.PictureBox();
            this.timer_ClearIdRecognized = new System.Windows.Forms.Timer(this.components);
            this.labDebug1 = new System.Windows.Forms.Label();
            this.labDebug2 = new System.Windows.Forms.Label();
            this.labDebug3 = new System.Windows.Forms.Label();
            this.labDebug4 = new System.Windows.Forms.Label();
            this.grpShowResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoIdentified)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResultAction)).BeginInit();
            this.grpPIN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVerifyInput)).BeginInit();
            this.grpRefreshLoadBIRs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRefreshLoadBIRs)).BeginInit();
            this.panelWorking.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPINEnabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFacialEnabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFingerEnabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMRZEnabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLCBEnabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAcercaDe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSupport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReinit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVideo)).BeginInit();
            this.SuspendLayout();
            // 
            // timerProcessFingerSampleCaptured
            // 
            this.timerProcessFingerSampleCaptured.Enabled = true;
            this.timerProcessFingerSampleCaptured.Interval = 250;
            this.timerProcessFingerSampleCaptured.Tick += new System.EventHandler(this.timerProcessFingerSampleCaptured_Tick);
            // 
            // timerMRZReset
            // 
            this.timerMRZReset.Enabled = true;
            this.timerMRZReset.Interval = 10000;
            this.timerMRZReset.Tick += new System.EventHandler(this.timerMRZReset_Tick);
            // 
            // rtBoxInputMRZ
            // 
            this.rtBoxInputMRZ.Location = new System.Drawing.Point(2, -2);
            this.rtBoxInputMRZ.Name = "rtBoxInputMRZ";
            this.rtBoxInputMRZ.Size = new System.Drawing.Size(0, 0);
            this.rtBoxInputMRZ.TabIndex = 2;
            this.rtBoxInputMRZ.Text = "";
            this.rtBoxInputMRZ.TextChanged += new System.EventHandler(this.rtBoxInputMRZ_TextChanged);
            // 
            // btnTestMRZ
            // 
            this.btnTestMRZ.Location = new System.Drawing.Point(112, 14);
            this.btnTestMRZ.Name = "btnTestMRZ";
            this.btnTestMRZ.Size = new System.Drawing.Size(75, 23);
            this.btnTestMRZ.TabIndex = 3;
            this.btnTestMRZ.Text = "TestMRZ";
            this.btnTestMRZ.UseVisualStyleBackColor = true;
            this.btnTestMRZ.Visible = false;
            this.btnTestMRZ.Click += new System.EventHandler(this.btnTestMRZ_Click);
            // 
            // grpShowResult
            // 
            this.grpShowResult.BackColor = System.Drawing.Color.Transparent;
            this.grpShowResult.BackgroundImage = global::Biometrika.Kiosk.UI.Properties.Resources.ImageFondoResultEntrada;
            this.grpShowResult.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.grpShowResult.Controls.Add(this.picSample);
            this.grpShowResult.Controls.Add(this.picFotoIdentified);
            this.grpShowResult.Controls.Add(this.labAntipassback);
            this.grpShowResult.Controls.Add(this.picResultAction);
            this.grpShowResult.Controls.Add(this.grpPIN);
            this.grpShowResult.Controls.Add(this.labFeedbackName);
            this.grpShowResult.Controls.Add(this.labFeedbackDetail);
            this.grpShowResult.Controls.Add(this.label1);
            this.grpShowResult.Controls.Add(this.labId);
            this.grpShowResult.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpShowResult.Location = new System.Drawing.Point(0, 100);
            this.grpShowResult.Margin = new System.Windows.Forms.Padding(0);
            this.grpShowResult.Name = "grpShowResult";
            this.grpShowResult.Size = new System.Drawing.Size(1280, 676);
            this.grpShowResult.TabIndex = 5;
            this.grpShowResult.TabStop = false;
            this.grpShowResult.Visible = false;
            // 
            // picSample
            // 
            this.picSample.BackColor = System.Drawing.Color.Transparent;
            this.picSample.Location = new System.Drawing.Point(57, 246);
            this.picSample.Name = "picSample";
            this.picSample.Size = new System.Drawing.Size(108, 116);
            this.picSample.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSample.TabIndex = 5;
            this.picSample.TabStop = false;
            // 
            // picFotoIdentified
            // 
            this.picFotoIdentified.BackColor = System.Drawing.Color.Transparent;
            this.picFotoIdentified.Location = new System.Drawing.Point(186, 180);
            this.picFotoIdentified.Name = "picFotoIdentified";
            this.picFotoIdentified.Size = new System.Drawing.Size(160, 160);
            this.picFotoIdentified.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFotoIdentified.TabIndex = 8;
            this.picFotoIdentified.TabStop = false;
            // 
            // labAntipassback
            // 
            this.labAntipassback.AutoSize = true;
            this.labAntipassback.BackColor = System.Drawing.Color.Transparent;
            this.labAntipassback.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labAntipassback.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.labAntipassback.Location = new System.Drawing.Point(778, 180);
            this.labAntipassback.Name = "labAntipassback";
            this.labAntipassback.Size = new System.Drawing.Size(229, 32);
            this.labAntipassback.TabIndex = 17;
            this.labAntipassback.Text = "ANTIPASSBACK";
            this.labAntipassback.Visible = false;
            // 
            // picResultAction
            // 
            this.picResultAction.BackColor = System.Drawing.Color.Transparent;
            this.picResultAction.Location = new System.Drawing.Point(487, 594);
            this.picResultAction.Name = "picResultAction";
            this.picResultAction.Size = new System.Drawing.Size(415, 112);
            this.picResultAction.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResultAction.TabIndex = 29;
            this.picResultAction.TabStop = false;
            // 
            // grpPIN
            // 
            this.grpPIN.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grpPIN.BackgroundImage")));
            this.grpPIN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.grpPIN.Controls.Add(this.picVerifyInput);
            this.grpPIN.Controls.Add(this.labTxtPIN);
            this.grpPIN.Controls.Add(this.labPINK);
            this.grpPIN.Controls.Add(this.labPINDelete);
            this.grpPIN.Controls.Add(this.labPINEnter);
            this.grpPIN.Controls.Add(this.labPIN0);
            this.grpPIN.Controls.Add(this.labPIN9);
            this.grpPIN.Controls.Add(this.labPIN8);
            this.grpPIN.Controls.Add(this.labPIN6);
            this.grpPIN.Controls.Add(this.labPIN5);
            this.grpPIN.Controls.Add(this.labPIN3);
            this.grpPIN.Controls.Add(this.labPIN2);
            this.grpPIN.Controls.Add(this.labPIN7);
            this.grpPIN.Controls.Add(this.labPIN4);
            this.grpPIN.Controls.Add(this.labPIN1);
            this.grpPIN.Controls.Add(this.txtPIN);
            this.grpPIN.Location = new System.Drawing.Point(75, 215);
            this.grpPIN.Name = "grpPIN";
            this.grpPIN.Size = new System.Drawing.Size(900, 1600);
            this.grpPIN.TabIndex = 16;
            this.grpPIN.TabStop = false;
            this.grpPIN.Visible = false;
            // 
            // picVerifyInput
            // 
            this.picVerifyInput.BackColor = System.Drawing.Color.Transparent;
            this.picVerifyInput.Image = global::Biometrika.Kiosk.UI.Properties.Resources.nook;
            this.picVerifyInput.Location = new System.Drawing.Point(395, 523);
            this.picVerifyInput.Name = "picVerifyInput";
            this.picVerifyInput.Size = new System.Drawing.Size(114, 116);
            this.picVerifyInput.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picVerifyInput.TabIndex = 14;
            this.picVerifyInput.TabStop = false;
            this.picVerifyInput.Visible = false;
            // 
            // labTxtPIN
            // 
            this.labTxtPIN.BackColor = System.Drawing.Color.Transparent;
            this.labTxtPIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 86.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTxtPIN.ForeColor = System.Drawing.Color.White;
            this.labTxtPIN.Location = new System.Drawing.Point(97, 672);
            this.labTxtPIN.Name = "labTxtPIN";
            this.labTxtPIN.Size = new System.Drawing.Size(710, 135);
            this.labTxtPIN.TabIndex = 15;
            this.labTxtPIN.Text = "212844152";
            this.labTxtPIN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labPINK
            // 
            this.labPINK.BackColor = System.Drawing.Color.Transparent;
            this.labPINK.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPINK.Location = new System.Drawing.Point(685, 1090);
            this.labPINK.Name = "labPINK";
            this.labPINK.Size = new System.Drawing.Size(120, 180);
            this.labPINK.TabIndex = 13;
            this.labPINK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labPINK.Click += new System.EventHandler(this.labPIN_Click);
            // 
            // labPINDelete
            // 
            this.labPINDelete.BackColor = System.Drawing.Color.Transparent;
            this.labPINDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPINDelete.Location = new System.Drawing.Point(89, 1297);
            this.labPINDelete.Name = "labPINDelete";
            this.labPINDelete.Size = new System.Drawing.Size(230, 185);
            this.labPINDelete.TabIndex = 12;
            this.labPINDelete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labPINDelete.Click += new System.EventHandler(this.labPIN_Click);
            // 
            // labPINEnter
            // 
            this.labPINEnter.BackColor = System.Drawing.Color.Transparent;
            this.labPINEnter.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPINEnter.Location = new System.Drawing.Point(584, 1297);
            this.labPINEnter.Name = "labPINEnter";
            this.labPINEnter.Size = new System.Drawing.Size(224, 185);
            this.labPINEnter.TabIndex = 11;
            this.labPINEnter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labPINEnter.Click += new System.EventHandler(this.labPIN_Click);
            // 
            // labPIN0
            // 
            this.labPIN0.BackColor = System.Drawing.Color.Transparent;
            this.labPIN0.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPIN0.Location = new System.Drawing.Point(334, 1297);
            this.labPIN0.Name = "labPIN0";
            this.labPIN0.Size = new System.Drawing.Size(230, 185);
            this.labPIN0.TabIndex = 10;
            this.labPIN0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labPIN0.Click += new System.EventHandler(this.labPIN_Click);
            // 
            // labPIN9
            // 
            this.labPIN9.BackColor = System.Drawing.Color.Transparent;
            this.labPIN9.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPIN9.Location = new System.Drawing.Point(538, 1090);
            this.labPIN9.Name = "labPIN9";
            this.labPIN9.Size = new System.Drawing.Size(120, 180);
            this.labPIN9.TabIndex = 9;
            this.labPIN9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labPIN9.Click += new System.EventHandler(this.labPIN_Click);
            // 
            // labPIN8
            // 
            this.labPIN8.BackColor = System.Drawing.Color.Transparent;
            this.labPIN8.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPIN8.Location = new System.Drawing.Point(389, 1090);
            this.labPIN8.Name = "labPIN8";
            this.labPIN8.Size = new System.Drawing.Size(120, 180);
            this.labPIN8.TabIndex = 8;
            this.labPIN8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labPIN8.Click += new System.EventHandler(this.labPIN_Click);
            // 
            // labPIN6
            // 
            this.labPIN6.BackColor = System.Drawing.Color.Transparent;
            this.labPIN6.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPIN6.Location = new System.Drawing.Point(95, 1090);
            this.labPIN6.Name = "labPIN6";
            this.labPIN6.Size = new System.Drawing.Size(120, 180);
            this.labPIN6.TabIndex = 7;
            this.labPIN6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labPIN6.Click += new System.EventHandler(this.labPIN_Click);
            // 
            // labPIN5
            // 
            this.labPIN5.BackColor = System.Drawing.Color.Transparent;
            this.labPIN5.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPIN5.Location = new System.Drawing.Point(685, 877);
            this.labPIN5.Name = "labPIN5";
            this.labPIN5.Size = new System.Drawing.Size(120, 180);
            this.labPIN5.TabIndex = 6;
            this.labPIN5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labPIN5.Click += new System.EventHandler(this.labPIN_Click);
            // 
            // labPIN3
            // 
            this.labPIN3.BackColor = System.Drawing.Color.Transparent;
            this.labPIN3.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPIN3.Location = new System.Drawing.Point(389, 877);
            this.labPIN3.Name = "labPIN3";
            this.labPIN3.Size = new System.Drawing.Size(120, 180);
            this.labPIN3.TabIndex = 5;
            this.labPIN3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labPIN3.Click += new System.EventHandler(this.labPIN_Click);
            // 
            // labPIN2
            // 
            this.labPIN2.BackColor = System.Drawing.Color.Transparent;
            this.labPIN2.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPIN2.Location = new System.Drawing.Point(242, 877);
            this.labPIN2.Name = "labPIN2";
            this.labPIN2.Size = new System.Drawing.Size(120, 180);
            this.labPIN2.TabIndex = 4;
            this.labPIN2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labPIN2.Click += new System.EventHandler(this.labPIN_Click);
            // 
            // labPIN7
            // 
            this.labPIN7.BackColor = System.Drawing.Color.Transparent;
            this.labPIN7.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPIN7.Location = new System.Drawing.Point(242, 1090);
            this.labPIN7.Name = "labPIN7";
            this.labPIN7.Size = new System.Drawing.Size(120, 180);
            this.labPIN7.TabIndex = 3;
            this.labPIN7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labPIN7.Click += new System.EventHandler(this.labPIN_Click);
            // 
            // labPIN4
            // 
            this.labPIN4.BackColor = System.Drawing.Color.Transparent;
            this.labPIN4.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPIN4.Location = new System.Drawing.Point(538, 877);
            this.labPIN4.Name = "labPIN4";
            this.labPIN4.Size = new System.Drawing.Size(120, 180);
            this.labPIN4.TabIndex = 2;
            this.labPIN4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labPIN4.Click += new System.EventHandler(this.labPIN_Click);
            // 
            // labPIN1
            // 
            this.labPIN1.BackColor = System.Drawing.Color.Transparent;
            this.labPIN1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPIN1.Location = new System.Drawing.Point(95, 877);
            this.labPIN1.Name = "labPIN1";
            this.labPIN1.Size = new System.Drawing.Size(120, 180);
            this.labPIN1.TabIndex = 1;
            this.labPIN1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labPIN1.Click += new System.EventHandler(this.labPIN_Click);
            // 
            // txtPIN
            // 
            this.txtPIN.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtPIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 86.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPIN.Location = new System.Drawing.Point(97, 516);
            this.txtPIN.Name = "txtPIN";
            this.txtPIN.ReadOnly = true;
            this.txtPIN.Size = new System.Drawing.Size(711, 138);
            this.txtPIN.TabIndex = 0;
            this.txtPIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPIN.Visible = false;
            this.txtPIN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmMain_KeyPress);
            // 
            // labFeedbackName
            // 
            this.labFeedbackName.BackColor = System.Drawing.Color.Transparent;
            this.labFeedbackName.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labFeedbackName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labFeedbackName.Location = new System.Drawing.Point(106, 365);
            this.labFeedbackName.Name = "labFeedbackName";
            this.labFeedbackName.Size = new System.Drawing.Size(333, 131);
            this.labFeedbackName.TabIndex = 19;
            this.labFeedbackName.Text = "Suhit Gallucci, Gustavo Gerardo ";
            this.labFeedbackName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labFeedbackDetail
            // 
            this.labFeedbackDetail.BackColor = System.Drawing.Color.Transparent;
            this.labFeedbackDetail.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labFeedbackDetail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(220)))), ((int)(((byte)(100)))));
            this.labFeedbackDetail.Location = new System.Drawing.Point(105, 496);
            this.labFeedbackDetail.Name = "labFeedbackDetail";
            this.labFeedbackDetail.Size = new System.Drawing.Size(319, 196);
            this.labFeedbackDetail.TabIndex = 18;
            this.labFeedbackDetail.Text = "Ultima Marca 25/02/2021 18:06:23";
            this.labFeedbackDetail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labFeedbackDetail.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 734);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "ID Reconocido";
            this.label1.Visible = false;
            // 
            // labId
            // 
            this.labId.AutoSize = true;
            this.labId.Location = new System.Drawing.Point(1222, 734);
            this.labId.Name = "labId";
            this.labId.Size = new System.Drawing.Size(19, 13);
            this.labId.TabIndex = 6;
            this.labId.Text = "??";
            this.labId.Visible = false;
            // 
            // grpRefreshLoadBIRs
            // 
            this.grpRefreshLoadBIRs.BackColor = System.Drawing.Color.Transparent;
            this.grpRefreshLoadBIRs.Controls.Add(this.prgBarRefreshLoadBIRs);
            this.grpRefreshLoadBIRs.Controls.Add(this.label2);
            this.grpRefreshLoadBIRs.Controls.Add(this.picRefreshLoadBIRs);
            this.grpRefreshLoadBIRs.Location = new System.Drawing.Point(355, 360);
            this.grpRefreshLoadBIRs.Name = "grpRefreshLoadBIRs";
            this.grpRefreshLoadBIRs.Size = new System.Drawing.Size(555, 160);
            this.grpRefreshLoadBIRs.TabIndex = 28;
            this.grpRefreshLoadBIRs.TabStop = false;
            this.grpRefreshLoadBIRs.Visible = false;
            // 
            // prgBarRefreshLoadBIRs
            // 
            this.prgBarRefreshLoadBIRs.BackColor = System.Drawing.Color.White;
            this.prgBarRefreshLoadBIRs.Location = new System.Drawing.Point(135, 106);
            this.prgBarRefreshLoadBIRs.Maximum = 50;
            this.prgBarRefreshLoadBIRs.Name = "prgBarRefreshLoadBIRs";
            this.prgBarRefreshLoadBIRs.Size = new System.Drawing.Size(392, 23);
            this.prgBarRefreshLoadBIRs.Step = 1;
            this.prgBarRefreshLoadBIRs.TabIndex = 1;
            this.prgBarRefreshLoadBIRs.Tag = "";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(128, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(411, 80);
            this.label2.TabIndex = 20;
            this.label2.Text = "Refrescando valores de acceso...Espere por favor...";
            // 
            // picRefreshLoadBIRs
            // 
            this.picRefreshLoadBIRs.Image = global::Biometrika.Kiosk.UI.Properties.Resources.ImageReinit;
            this.picRefreshLoadBIRs.Location = new System.Drawing.Point(31, 34);
            this.picRefreshLoadBIRs.Name = "picRefreshLoadBIRs";
            this.picRefreshLoadBIRs.Size = new System.Drawing.Size(91, 95);
            this.picRefreshLoadBIRs.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picRefreshLoadBIRs.TabIndex = 0;
            this.picRefreshLoadBIRs.TabStop = false;
            // 
            // panelWorking
            // 
            this.panelWorking.BackColor = System.Drawing.Color.Transparent;
            this.panelWorking.BackgroundImage = global::Biometrika.Kiosk.UI.Properties.Resources.ImageWorking;
            this.panelWorking.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelWorking.Controls.Add(this.label4);
            this.panelWorking.Controls.Add(this.labWorking);
            this.panelWorking.Location = new System.Drawing.Point(160, 850);
            this.panelWorking.Name = "panelWorking";
            this.panelWorking.Size = new System.Drawing.Size(776, 383);
            this.panelWorking.TabIndex = 30;
            this.panelWorking.Visible = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(307, 316);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(411, 40);
            this.label4.TabIndex = 23;
            this.label4.Text = "Espere por favor...";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labWorking
            // 
            this.labWorking.BackColor = System.Drawing.Color.Transparent;
            this.labWorking.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labWorking.ForeColor = System.Drawing.Color.White;
            this.labWorking.Location = new System.Drawing.Point(322, 55);
            this.labWorking.Name = "labWorking";
            this.labWorking.Size = new System.Drawing.Size(411, 228);
            this.labWorking.TabIndex = 22;
            this.labWorking.Text = "Procesando...";
            this.labWorking.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picPINEnabled
            // 
            this.picPINEnabled.BackColor = System.Drawing.Color.Transparent;
            this.picPINEnabled.Image = global::Biometrika.Kiosk.UI.Properties.Resources.ImageTechPIN1;
            this.picPINEnabled.InitialImage = null;
            this.picPINEnabled.Location = new System.Drawing.Point(375, 1260);
            this.picPINEnabled.Name = "picPINEnabled";
            this.picPINEnabled.Size = new System.Drawing.Size(307, 330);
            this.picPINEnabled.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPINEnabled.TabIndex = 10;
            this.picPINEnabled.TabStop = false;
            this.picPINEnabled.Click += new System.EventHandler(this.picPINEnabled_Click);
            // 
            // picFacialEnabled
            // 
            this.picFacialEnabled.BackColor = System.Drawing.Color.Transparent;
            this.picFacialEnabled.Image = global::Biometrika.Kiosk.UI.Properties.Resources.CedulaEnabled;
            this.picFacialEnabled.InitialImage = null;
            this.picFacialEnabled.Location = new System.Drawing.Point(17, 1404);
            this.picFacialEnabled.Name = "picFacialEnabled";
            this.picFacialEnabled.Size = new System.Drawing.Size(330, 400);
            this.picFacialEnabled.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFacialEnabled.TabIndex = 9;
            this.picFacialEnabled.TabStop = false;
            // 
            // picFingerEnabled
            // 
            this.picFingerEnabled.BackColor = System.Drawing.Color.Transparent;
            this.picFingerEnabled.Image = global::Biometrika.Kiosk.UI.Properties.Resources.ImageTechFinger1;
            this.picFingerEnabled.Location = new System.Drawing.Point(558, 14);
            this.picFingerEnabled.Name = "picFingerEnabled";
            this.picFingerEnabled.Size = new System.Drawing.Size(320, 380);
            this.picFingerEnabled.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFingerEnabled.TabIndex = 8;
            this.picFingerEnabled.TabStop = false;
            // 
            // picMRZEnabled
            // 
            this.picMRZEnabled.BackColor = System.Drawing.Color.Transparent;
            this.picMRZEnabled.Image = global::Biometrika.Kiosk.UI.Properties.Resources.ImageTechMRZ1;
            this.picMRZEnabled.InitialImage = null;
            this.picMRZEnabled.Location = new System.Drawing.Point(765, 290);
            this.picMRZEnabled.Name = "picMRZEnabled";
            this.picMRZEnabled.Size = new System.Drawing.Size(320, 380);
            this.picMRZEnabled.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picMRZEnabled.TabIndex = 7;
            this.picMRZEnabled.TabStop = false;
            // 
            // picLCBEnabled
            // 
            this.picLCBEnabled.BackColor = System.Drawing.Color.Transparent;
            this.picLCBEnabled.Image = global::Biometrika.Kiosk.UI.Properties.Resources.ImageTechLCB1;
            this.picLCBEnabled.InitialImage = null;
            this.picLCBEnabled.Location = new System.Drawing.Point(232, 19);
            this.picLCBEnabled.Name = "picLCBEnabled";
            this.picLCBEnabled.Size = new System.Drawing.Size(320, 380);
            this.picLCBEnabled.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLCBEnabled.TabIndex = 6;
            this.picLCBEnabled.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 16);
            this.label3.TabIndex = 19;
            this.label3.Text = "ID Reconocido";
            this.label3.Visible = false;
            // 
            // picClose
            // 
            this.picClose.BackColor = System.Drawing.Color.Transparent;
            this.picClose.Image = global::Biometrika.Kiosk.UI.Properties.Resources.ImageClose;
            this.picClose.InitialImage = null;
            this.picClose.Location = new System.Drawing.Point(1203, 60);
            this.picClose.Name = "picClose";
            this.picClose.Size = new System.Drawing.Size(25, 25);
            this.picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picClose.TabIndex = 20;
            this.picClose.TabStop = false;
            this.picClose.Click += new System.EventHandler(this.labClose_Click);
            // 
            // picAcercaDe
            // 
            this.picAcercaDe.BackColor = System.Drawing.Color.Transparent;
            this.picAcercaDe.Image = global::Biometrika.Kiosk.UI.Properties.Resources.ImageAcercaDe;
            this.picAcercaDe.InitialImage = null;
            this.picAcercaDe.Location = new System.Drawing.Point(1173, 60);
            this.picAcercaDe.Name = "picAcercaDe";
            this.picAcercaDe.Size = new System.Drawing.Size(22, 25);
            this.picAcercaDe.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picAcercaDe.TabIndex = 21;
            this.picAcercaDe.TabStop = false;
            this.picAcercaDe.Click += new System.EventHandler(this.picAcercaDe_Click);
            // 
            // labVersion
            // 
            this.labVersion.BackColor = System.Drawing.Color.Transparent;
            this.labVersion.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labVersion.ForeColor = System.Drawing.Color.White;
            this.labVersion.Location = new System.Drawing.Point(880, 720);
            this.labVersion.Name = "labVersion";
            this.labVersion.Size = new System.Drawing.Size(375, 20);
            this.labVersion.TabIndex = 22;
            this.labVersion.Text = "v7.5.111.2345";
            this.labVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // picSupport
            // 
            this.picSupport.BackColor = System.Drawing.Color.Transparent;
            this.picSupport.Image = global::Biometrika.Kiosk.UI.Properties.Resources.ImageSupport;
            this.picSupport.InitialImage = null;
            this.picSupport.Location = new System.Drawing.Point(1143, 60);
            this.picSupport.Name = "picSupport";
            this.picSupport.Size = new System.Drawing.Size(25, 25);
            this.picSupport.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSupport.TabIndex = 23;
            this.picSupport.TabStop = false;
            this.picSupport.Click += new System.EventHandler(this.picSupport_Click);
            // 
            // picReinit
            // 
            this.picReinit.BackColor = System.Drawing.Color.Transparent;
            this.picReinit.Image = global::Biometrika.Kiosk.UI.Properties.Resources.ImageReinit;
            this.picReinit.InitialImage = null;
            this.picReinit.Location = new System.Drawing.Point(1112, 60);
            this.picReinit.Name = "picReinit";
            this.picReinit.Size = new System.Drawing.Size(25, 25);
            this.picReinit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picReinit.TabIndex = 24;
            this.picReinit.TabStop = false;
            this.picReinit.Click += new System.EventHandler(this.picReinit_Click);
            // 
            // labQTemplates
            // 
            this.labQTemplates.BackColor = System.Drawing.Color.Transparent;
            this.labQTemplates.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labQTemplates.ForeColor = System.Drawing.Color.White;
            this.labQTemplates.Location = new System.Drawing.Point(880, 700);
            this.labQTemplates.Name = "labQTemplates";
            this.labQTemplates.Size = new System.Drawing.Size(375, 25);
            this.labQTemplates.TabIndex = 25;
            this.labQTemplates.Text = "#QH: -1";
            this.labQTemplates.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labKioskType
            // 
            this.labKioskType.BackColor = System.Drawing.Color.Transparent;
            this.labKioskType.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labKioskType.ForeColor = System.Drawing.Color.White;
            this.labKioskType.Location = new System.Drawing.Point(880, 700);
            this.labKioskType.Name = "labKioskType";
            this.labKioskType.Size = new System.Drawing.Size(375, 19);
            this.labKioskType.TabIndex = 26;
            this.labKioskType.Text = "Entrada";
            this.labKioskType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // timerSerialLCB
            // 
            this.timerSerialLCB.Enabled = true;
            this.timerSerialLCB.Interval = 500;
            this.timerSerialLCB.Tick += new System.EventHandler(this.timerSerialLCB_Tick);
            // 
            // labError
            // 
            this.labError.BackColor = System.Drawing.Color.Transparent;
            this.labError.Font = new System.Drawing.Font("Courier New", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.labError.Location = new System.Drawing.Point(148, 93);
            this.labError.Name = "labError";
            this.labError.Size = new System.Drawing.Size(746, 70);
            this.labError.TabIndex = 27;
            this.labError.Text = "Error [-2 - Error de conexión a web service...]";
            this.labError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer_prgBarRefreshLoadBIRs
            // 
            this.timer_prgBarRefreshLoadBIRs.Interval = 1000;
            this.timer_prgBarRefreshLoadBIRs.Tick += new System.EventHandler(this.timer_prgBarRefreshLoadBIRs_Tick);
            // 
            // labReloj
            // 
            this.labReloj.BackColor = System.Drawing.Color.Transparent;
            this.labReloj.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labReloj.ForeColor = System.Drawing.Color.White;
            this.labReloj.Location = new System.Drawing.Point(780, 130);
            this.labReloj.Name = "labReloj";
            this.labReloj.Size = new System.Drawing.Size(253, 33);
            this.labReloj.TabIndex = 28;
            this.labReloj.Text = "15:45";
            this.labReloj.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labReloj.Visible = false;
            // 
            // timerReloj
            // 
            this.timerReloj.Interval = 60000;
            this.timerReloj.Tick += new System.EventHandler(this.timerReloj_Tick);
            // 
            // timer_Reinit_LCB
            // 
            this.timer_Reinit_LCB.Interval = 300000;
            this.timer_Reinit_LCB.Tick += new System.EventHandler(this.timer_Reinit_LCB_Tick);
            // 
            // labBtnDummy
            // 
            this.labBtnDummy.BackColor = System.Drawing.Color.Red;
            this.labBtnDummy.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labBtnDummy.ForeColor = System.Drawing.Color.White;
            this.labBtnDummy.Location = new System.Drawing.Point(12, 20);
            this.labBtnDummy.Name = "labBtnDummy";
            this.labBtnDummy.Size = new System.Drawing.Size(28, 47);
            this.labBtnDummy.TabIndex = 31;
            this.labBtnDummy.Text = "D";
            this.labBtnDummy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labBtnDummy.Visible = false;
            this.labBtnDummy.Click += new System.EventHandler(this.labBtnDummy_Click);
            // 
            // txtRutManual
            // 
            this.txtRutManual.Location = new System.Drawing.Point(46, 27);
            this.txtRutManual.Name = "txtRutManual";
            this.txtRutManual.Size = new System.Drawing.Size(100, 20);
            this.txtRutManual.TabIndex = 32;
            this.txtRutManual.Text = "21284415-2";
            this.txtRutManual.Visible = false;
            this.txtRutManual.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRutManual_KeyDown);
            this.txtRutManual.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRutManual_KeyPress);
            // 
            // btnINRutManual
            // 
            this.btnINRutManual.BackColor = System.Drawing.Color.RosyBrown;
            this.btnINRutManual.Location = new System.Drawing.Point(152, 20);
            this.btnINRutManual.Name = "btnINRutManual";
            this.btnINRutManual.Size = new System.Drawing.Size(35, 47);
            this.btnINRutManual.TabIndex = 33;
            this.btnINRutManual.Text = "IN";
            this.btnINRutManual.UseVisualStyleBackColor = false;
            this.btnINRutManual.Visible = false;
            this.btnINRutManual.Click += new System.EventHandler(this.btnINRutManual_Click);
            // 
            // rbEntradaManual
            // 
            this.rbEntradaManual.AutoSize = true;
            this.rbEntradaManual.BackColor = System.Drawing.Color.Transparent;
            this.rbEntradaManual.Checked = true;
            this.rbEntradaManual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbEntradaManual.ForeColor = System.Drawing.Color.White;
            this.rbEntradaManual.Location = new System.Drawing.Point(57, 50);
            this.rbEntradaManual.Name = "rbEntradaManual";
            this.rbEntradaManual.Size = new System.Drawing.Size(33, 17);
            this.rbEntradaManual.TabIndex = 34;
            this.rbEntradaManual.TabStop = true;
            this.rbEntradaManual.Text = "E";
            this.rbEntradaManual.UseVisualStyleBackColor = false;
            // 
            // rbSalidaManual
            // 
            this.rbSalidaManual.AutoSize = true;
            this.rbSalidaManual.BackColor = System.Drawing.Color.Transparent;
            this.rbSalidaManual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSalidaManual.ForeColor = System.Drawing.Color.White;
            this.rbSalidaManual.Location = new System.Drawing.Point(96, 50);
            this.rbSalidaManual.Name = "rbSalidaManual";
            this.rbSalidaManual.Size = new System.Drawing.Size(33, 17);
            this.rbSalidaManual.TabIndex = 35;
            this.rbSalidaManual.Text = "S";
            this.rbSalidaManual.UseVisualStyleBackColor = false;
            // 
            // timer_PINPanel
            // 
            this.timer_PINPanel.Interval = 5000;
            this.timer_PINPanel.Tick += new System.EventHandler(this.timer_PINPanel_Tick);
            // 
            // picVideo
            // 
            this.picVideo.Location = new System.Drawing.Point(375, 425);
            this.picVideo.Name = "picVideo";
            this.picVideo.Size = new System.Drawing.Size(320, 190);
            this.picVideo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picVideo.TabIndex = 36;
            this.picVideo.TabStop = false;
            // 
            // timer_ClearIdRecognized
            // 
            this.timer_ClearIdRecognized.Enabled = true;
            this.timer_ClearIdRecognized.Interval = 2000;
            this.timer_ClearIdRecognized.Tick += new System.EventHandler(this.timer_ClearIdRecognized_Tick);
            // 
            // labDebug1
            // 
            this.labDebug1.AutoSize = true;
            this.labDebug1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDebug1.Location = new System.Drawing.Point(264, 19);
            this.labDebug1.Name = "labDebug1";
            this.labDebug1.Size = new System.Drawing.Size(46, 18);
            this.labDebug1.TabIndex = 37;
            this.labDebug1.Text = "label5";
            this.labDebug1.Visible = false;
            // 
            // labDebug2
            // 
            this.labDebug2.AutoSize = true;
            this.labDebug2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDebug2.Location = new System.Drawing.Point(264, 39);
            this.labDebug2.Name = "labDebug2";
            this.labDebug2.Size = new System.Drawing.Size(46, 18);
            this.labDebug2.TabIndex = 38;
            this.labDebug2.Text = "label5";
            this.labDebug2.Visible = false;
            // 
            // labDebug3
            // 
            this.labDebug3.AutoSize = true;
            this.labDebug3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDebug3.Location = new System.Drawing.Point(264, 60);
            this.labDebug3.Name = "labDebug3";
            this.labDebug3.Size = new System.Drawing.Size(46, 18);
            this.labDebug3.TabIndex = 39;
            this.labDebug3.Text = "label5";
            this.labDebug3.Visible = false;
            // 
            // labDebug4
            // 
            this.labDebug4.AutoSize = true;
            this.labDebug4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDebug4.Location = new System.Drawing.Point(264, 80);
            this.labDebug4.Name = "labDebug4";
            this.labDebug4.Size = new System.Drawing.Size(46, 18);
            this.labDebug4.TabIndex = 40;
            this.labDebug4.Text = "label5";
            this.labDebug4.Visible = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1280, 800);
            this.Controls.Add(this.grpRefreshLoadBIRs);
            this.Controls.Add(this.picLCBEnabled);
            this.Controls.Add(this.picFingerEnabled);
            this.Controls.Add(this.labDebug4);
            this.Controls.Add(this.labDebug3);
            this.Controls.Add(this.labDebug2);
            this.Controls.Add(this.labDebug1);
            this.Controls.Add(this.picVideo);
            this.Controls.Add(this.panelWorking);
            this.Controls.Add(this.rbSalidaManual);
            this.Controls.Add(this.rbEntradaManual);
            this.Controls.Add(this.btnINRutManual);
            this.Controls.Add(this.txtRutManual);
            this.Controls.Add(this.labBtnDummy);
            this.Controls.Add(this.labReloj);
            this.Controls.Add(this.grpShowResult);
            this.Controls.Add(this.picClose);
            this.Controls.Add(this.picFacialEnabled);
            this.Controls.Add(this.labError);
            this.Controls.Add(this.labQTemplates);
            this.Controls.Add(this.picReinit);
            this.Controls.Add(this.picSupport);
            this.Controls.Add(this.labVersion);
            this.Controls.Add(this.picAcercaDe);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.picMRZEnabled);
            this.Controls.Add(this.btnTestMRZ);
            this.Controls.Add(this.rtBoxInputMRZ);
            this.Controls.Add(this.labKioskType);
            this.Controls.Add(this.picPINEnabled);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Biometrika Kiosk";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Click += new System.EventHandler(this.frmMain_Click);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmMain_KeyPress);
            this.grpShowResult.ResumeLayout(false);
            this.grpShowResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoIdentified)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResultAction)).EndInit();
            this.grpPIN.ResumeLayout(false);
            this.grpPIN.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVerifyInput)).EndInit();
            this.grpRefreshLoadBIRs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picRefreshLoadBIRs)).EndInit();
            this.panelWorking.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picPINEnabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFacialEnabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFingerEnabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMRZEnabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLCBEnabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAcercaDe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSupport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReinit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVideo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timerProcessFingerSampleCaptured;
        private System.Windows.Forms.Timer timerMRZReset;
        private System.Windows.Forms.RichTextBox rtBoxInputMRZ;
        private System.Windows.Forms.Button btnTestMRZ;
        private System.Windows.Forms.GroupBox grpShowResult;
        private System.Windows.Forms.PictureBox picFotoIdentified;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labId;
        private System.Windows.Forms.PictureBox picSample;
        private System.Windows.Forms.PictureBox picLCBEnabled;
        private System.Windows.Forms.PictureBox picMRZEnabled;
        private System.Windows.Forms.PictureBox picFingerEnabled;
        private System.Windows.Forms.PictureBox picFacialEnabled;
        private System.Windows.Forms.PictureBox picPINEnabled;
        private System.Windows.Forms.GroupBox grpPIN;
        private System.Windows.Forms.Label labPINEnter;
        private System.Windows.Forms.Label labPIN0;
        private System.Windows.Forms.Label labPIN9;
        private System.Windows.Forms.Label labPIN8;
        private System.Windows.Forms.Label labPIN6;
        private System.Windows.Forms.Label labPIN5;
        private System.Windows.Forms.Label labPIN3;
        private System.Windows.Forms.Label labPIN2;
        private System.Windows.Forms.Label labPIN7;
        private System.Windows.Forms.Label labPIN4;
        private System.Windows.Forms.Label labPIN1;
        private System.Windows.Forms.TextBox txtPIN;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.Label labPINDelete;
        private System.Windows.Forms.Label labAntipassback;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox picClose;
        private System.Windows.Forms.PictureBox picAcercaDe;
        private System.Windows.Forms.Label labVersion;
        private System.Windows.Forms.PictureBox picSupport;
        private System.Windows.Forms.PictureBox picReinit;
        private System.Windows.Forms.Label labFeedbackDetail;
        private System.Windows.Forms.Label labQTemplates;
        private System.Windows.Forms.Label labFeedbackName;
        private System.Windows.Forms.Label labKioskType;
        private System.Windows.Forms.Timer timerSerialLCB;
        private System.Windows.Forms.Label labError;
        private System.Windows.Forms.GroupBox grpRefreshLoadBIRs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar prgBarRefreshLoadBIRs;
        private System.Windows.Forms.PictureBox picRefreshLoadBIRs;
        private System.Windows.Forms.Timer timer_prgBarRefreshLoadBIRs;
        private System.Windows.Forms.PictureBox picResultAction;
        private System.Windows.Forms.Panel panelWorking;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labWorking;
        private System.Windows.Forms.Label labReloj;
        private System.Windows.Forms.Timer timerReloj;
        private System.Windows.Forms.Timer timer_Reinit_LCB;
        private System.Windows.Forms.Label labBtnDummy;
        private System.Windows.Forms.TextBox txtRutManual;
        private System.Windows.Forms.Button btnINRutManual;
        private System.Windows.Forms.RadioButton rbEntradaManual;
        private System.Windows.Forms.RadioButton rbSalidaManual;
        private System.Windows.Forms.Label labPINK;
        private System.Windows.Forms.Timer timer_PINPanel;
        private System.Windows.Forms.PictureBox picVerifyInput;
        private System.Windows.Forms.PictureBox picVideo;
        private System.Windows.Forms.Label labTxtPIN;
        private System.Windows.Forms.Timer timer_ClearIdRecognized;
        private System.Windows.Forms.Label labDebug1;
        private System.Windows.Forms.Label labDebug2;
        private System.Windows.Forms.Label labDebug3;
        private System.Windows.Forms.Label labDebug4;
    }
}

