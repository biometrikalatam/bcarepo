﻿namespace Biometrika.Kiosk.UI
{
    partial class frmInitSplash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInitSplash));
            this.label1 = new System.Windows.Forms.Label();
            this.labStep = new System.Windows.Forms.Label();
            this.labStatus1 = new System.Windows.Forms.Label();
            this.lab1 = new System.Windows.Forms.Label();
            this.labStatus2 = new System.Windows.Forms.Label();
            this.lab2 = new System.Windows.Forms.Label();
            this.labVersion = new System.Windows.Forms.Label();
            this.pbarInitialization = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pic1 = new System.Windows.Forms.PictureBox();
            this.pic2 = new System.Windows.Forms.PictureBox();
            this.picFinal = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFinal)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(153, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(503, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "Iniciando Biometrika Kiosk...";
            // 
            // labStep
            // 
            this.labStep.AutoSize = true;
            this.labStep.BackColor = System.Drawing.Color.Transparent;
            this.labStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labStep.Location = new System.Drawing.Point(678, 87);
            this.labStep.Name = "labStep";
            this.labStep.Size = new System.Drawing.Size(40, 42);
            this.labStep.TabIndex = 1;
            this.labStep.Text = "0";
            this.labStep.Visible = false;
            // 
            // labStatus1
            // 
            this.labStatus1.AutoSize = true;
            this.labStatus1.BackColor = System.Drawing.Color.Transparent;
            this.labStatus1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labStatus1.Location = new System.Drawing.Point(68, 144);
            this.labStatus1.Name = "labStatus1";
            this.labStatus1.Size = new System.Drawing.Size(51, 42);
            this.labStatus1.TabIndex = 3;
            this.labStatus1.Text = "...";
            this.labStatus1.Visible = false;
            // 
            // lab1
            // 
            this.lab1.AutoSize = true;
            this.lab1.BackColor = System.Drawing.Color.Transparent;
            this.lab1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 21.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab1.Location = new System.Drawing.Point(253, 152);
            this.lab1.Name = "lab1";
            this.lab1.Size = new System.Drawing.Size(249, 33);
            this.lab1.TabIndex = 2;
            this.lab1.Text = "Iniciando Theme";
            // 
            // labStatus2
            // 
            this.labStatus2.AutoSize = true;
            this.labStatus2.BackColor = System.Drawing.Color.Transparent;
            this.labStatus2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labStatus2.Location = new System.Drawing.Point(68, 198);
            this.labStatus2.Name = "labStatus2";
            this.labStatus2.Size = new System.Drawing.Size(51, 42);
            this.labStatus2.TabIndex = 5;
            this.labStatus2.Text = "...";
            this.labStatus2.Visible = false;
            // 
            // lab2
            // 
            this.lab2.AutoSize = true;
            this.lab2.BackColor = System.Drawing.Color.Transparent;
            this.lab2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 21.75F, System.Drawing.FontStyle.Italic);
            this.lab2.Location = new System.Drawing.Point(253, 207);
            this.lab2.Name = "lab2";
            this.lab2.Size = new System.Drawing.Size(291, 33);
            this.lab2.TabIndex = 4;
            this.lab2.Text = "Iniciando Biometric";
            // 
            // labVersion
            // 
            this.labVersion.AutoSize = true;
            this.labVersion.BackColor = System.Drawing.Color.Transparent;
            this.labVersion.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labVersion.Location = new System.Drawing.Point(396, 362);
            this.labVersion.Name = "labVersion";
            this.labVersion.Size = new System.Drawing.Size(105, 20);
            this.labVersion.TabIndex = 6;
            this.labVersion.Text = "v7.5.234.456784";
            // 
            // pbarInitialization
            // 
            this.pbarInitialization.Location = new System.Drawing.Point(57, 438);
            this.pbarInitialization.Name = "pbarInitialization";
            this.pbarInitialization.Size = new System.Drawing.Size(444, 10);
            this.pbarInitialization.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 448);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "0%";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(255, 451);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "50%";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(484, 451);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "100%";
            // 
            // pic1
            // 
            this.pic1.BackColor = System.Drawing.Color.Transparent;
            this.pic1.Image = global::Biometrika.Kiosk.UI.Properties.Resources.check_T;
            this.pic1.Location = new System.Drawing.Point(201, 144);
            this.pic1.Name = "pic1";
            this.pic1.Size = new System.Drawing.Size(46, 49);
            this.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic1.TabIndex = 11;
            this.pic1.TabStop = false;
            // 
            // pic2
            // 
            this.pic2.BackColor = System.Drawing.Color.Transparent;
            this.pic2.Image = global::Biometrika.Kiosk.UI.Properties.Resources.check_T;
            this.pic2.Location = new System.Drawing.Point(201, 197);
            this.pic2.Name = "pic2";
            this.pic2.Size = new System.Drawing.Size(46, 49);
            this.pic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic2.TabIndex = 12;
            this.pic2.TabStop = false;
            // 
            // picFinal
            // 
            this.picFinal.BackColor = System.Drawing.Color.Transparent;
            this.picFinal.Image = global::Biometrika.Kiosk.UI.Properties.Resources.check_T;
            this.picFinal.Location = new System.Drawing.Point(549, 392);
            this.picFinal.Name = "picFinal";
            this.picFinal.Size = new System.Drawing.Size(76, 79);
            this.picFinal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFinal.TabIndex = 13;
            this.picFinal.TabStop = false;
            // 
            // frmInitSplash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Biometrika.Kiosk.UI.Properties.Resources.Splash;
            this.ClientSize = new System.Drawing.Size(1007, 526);
            this.Controls.Add(this.picFinal);
            this.Controls.Add(this.pic2);
            this.Controls.Add(this.pic1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pbarInitialization);
            this.Controls.Add(this.labVersion);
            this.Controls.Add(this.labStatus2);
            this.Controls.Add(this.lab2);
            this.Controls.Add(this.labStatus1);
            this.Controls.Add(this.lab1);
            this.Controls.Add(this.labStep);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmInitSplash";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmInitSplash";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmInitSplash_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFinal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labStep;
        private System.Windows.Forms.Label labStatus1;
        private System.Windows.Forms.Label lab1;
        private System.Windows.Forms.Label labStatus2;
        private System.Windows.Forms.Label lab2;
        private System.Windows.Forms.Label labVersion;
        private System.Windows.Forms.ProgressBar pbarInitialization;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pic1;
        private System.Windows.Forms.PictureBox pic2;
        private System.Windows.Forms.PictureBox picFinal;
    }
}