﻿//using BioArduinoRele;
//using Biometrika.Action.Fcb.Helpers;
using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Action.Dummy
{
    /// <summary>
    /// Implementa la interface de IAction para Autoclub Antofagasta. 
    /// Para eso utiliza un Arduino y mada a abrir por un cierto tiempo 
    /// los rele. 
    /// Si bien está para Autoclub, este esqueleto podria ser generico para cualqueir apertura en otros clientes. 
    /// Se deberia generalizar esta implementación para poder ser usada en otros clientes.
    /// </summary>
    public class ActionDummy : IAction
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ActionDummy));

        /// <summary>
        /// Permite manejar la placa Arduino
        /// </summary>
        //ArduinoHelper _ARDUINO_HELPER;

        bool _Initialized;
        string _Name;
        DynamicData _Config; 

        /// <summary>
        /// Constructor vacio 
        /// </summary>
        public ActionDummy() { 
        }


        /// <summary>
        /// Configuraicon especial si necesita
        /// </summary>
        public DynamicData Config
        {
            get {
                return _Config;
            }

            set {
                _Config = value;
            }
        }

        public bool Initialized
        {
            get {
                return this._Initialized;
            }

            set {
                this._Initialized = value;
            }
        }

        public string Name
        {
            get {
                return this._Name;
            }

            set {
                this._Name = value;
            }
        }

        /// <summary>
        /// En este caso crea el objeto de ayuda al Arduino  lo inicializa, para dejarlo listo 
        /// para su uso cuando se necesite accionar.
        /// Lee el archivo de config y si no existe lo crea.
        /// El puerto com si bien lo configura, la libreria lo setea solo, por lo que no es relevante.
        /// ArduinoType permite definir cantidad de reles a manejar. Y el Delay es el tiempo que se mantiene
        /// accionado hasta apagarlo. 
        /// </summary>
        /// <returns></returns>
        public int Intialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("ActionDummy.Intialize IN...");
                if (!System.IO.File.Exists("Biometrika.Action.FCB.cfg"))
                {
                    Config = new DynamicData();
                    Config.AddItem("COMPort", "COM8"); //No es necesaria en realidad, se autoconfigura
                    Config.AddItem("ArduinoType", 2); //Cantidad de rele en la placa
                    Config.AddItem("ArduinoOpenDelay", "30000");
                    //_Parameters.Add("", "");
                    if (!SerializeHelper.SerializeToFile(Config, "Biometrika.Action.FCB.cfg"))
                    {
                        LOG.Warn("ActionDummy.Initialize - No grabo condif en disco (Biometrika.Action.FCB.cfg)");
                    }
                }
                else
                {
                    Config = SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.Action.FCB.cfg");
                    Config.Initialize();
                }

                if (Config == null)
                {
                    LOG.Fatal("ActionDummy.Initialize - Error leyendo ActionDummy.cfg!");
                    return Errors.IERR_DESERIALIZING_DATA;
                } else //Si hay Config => Configuro Placa
                {
                    string com = (Config.DynamicDataItems.ContainsKey("COMPort") ?
                                    (string)Config.DynamicDataItems["COMPort"] : null);
                    //_ARDUINO_HELPER = new ArduinoHelper((string)Config.DynamicDataItems["ArduinoOpenDelay"], com);
                    ret = 0; // _ARDUINO_HELPER.Initialize(); 
                }
                _Initialized = true;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ActionDummy.Initialize - Excp Error: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Realiza la acción en este caso de enviar a abrir todos los rele (POr ejemplo en
        /// la barrera para abrirla y para prender una luz verde).
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="returns"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int DoAction(DynamicData parameters, out DynamicData returns, out string msg)
        {
            int ret = Errors.IERR_OK;
            returns = null;
            msg = null;
            try
            {
                //object o = (parameters != null) ? parameters.ParamsList["key"] : null;
                if (parameters == null)
                    LOG.Fatal("ActionDummy.DoAction - Parametros NULO!");
                else
                {
                    string rele = ""; ;
                    if (parameters.DynamicDataItems.ContainsKey("Rele1"))
                    {
                        rele = (string)parameters.DynamicDataItems["Rele1"];
                        LOG.Debug("ActionSportlifeNorte.DoAction - Set Rele1!");
                    }
                    else if (parameters.DynamicDataItems.ContainsKey("Rele2"))
                    {
                        rele = (string)parameters.DynamicDataItems["Rele2"];
                        LOG.Debug("ActionSportlifeNorte.DoAction - Set Rele2!");
                    }
                    //if (_ARDUINO_HELPER.OpenAllWithDelay((int)Config.DynamicDataItems["ArduinoType"],
                    //                                 (string)Config.DynamicDataItems["ArduinoOpenDelay"]) == 0)
                    LOG.Debug("ActionDummy.DoAction - OpenWithDelay => rele = " + rele +
                                                    " - Delay =  " + (string)Config.DynamicDataItems["ArduinoOpenDelay"]);
                    //if (_ARDUINO_HELPER.OpenWithDelay(rele, (string)Config.DynamicDataItems["ArduinoOpenDelay"]) == 0)
                    //{
                        msg = "Acciones Correctas!";
                        LOG.Debug("ActionDummy.DoAction - OpenWithDelay OK!");
                        ret = Errors.IERR_OK;
                    //} else
                    //{
                    //    LOG.Warn("ActionDummy.DoAction - OpenWithDelay NOOK!");
                    //    msg = "Atención! Alguna/s accion/es Incorrectas!";
                    //    ret = Errors.IERR_HANDLE_RELE;
                    //}
                    LOG.Debug("ActionDummy.DoAction - OpenWithDelay msg = " + msg);
                    //foreach (DynamicDataItem item in parameters.ListDynamicDataItems)
                    //{
                    //    LOG.Debug("ActionDummy.DoAction - key=" + item.key + " => value = " + (item.value.ToString()));
                    //}
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msg = "ActionDummy.DoAction  Excp [" + ex.Message + "]";
                LOG.Error("ActionDummy.DoAction  - Excp Error: " + ex.Message);
            }
            return ret;


        }
    }
}
