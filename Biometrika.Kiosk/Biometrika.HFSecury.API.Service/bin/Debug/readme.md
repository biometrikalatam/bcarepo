﻿# NEC Service

Servicio para la extracción templates y comparación de huellas utilizando las librerías de NEC.

## Descripción

La aplicación es un servicio HTTP auto hosteado (no se necesita IIS o Apache) que actualmente tiene un único verbo POST: '/nec/match' que recibe como entrada una lista con dos strings:
la primera la huella viva en formato RAW de 512x512 y la segunda la extraída de la cédula de identidad chilena antigua. El cuerpo debe ir como una lista de strings serializado a JSON, ejemplo:
    [
      "datosRAW",
      "datosTemplateNEC"
    ]

La respuesta corresponde al score obtenido por la comparación de las huellas, ejemplo:
642

## Dependencias

* .NET Framework 4.0 (No se puede subir la versión porque las librerías de NEC actualmente presentan un problema con cualquier versión superior a la 4.5)
* Librerías NEC instaladas con el runtime/sdk de NEC 2.0(C:\Program Files (x86)\Nec Argentina S.A\NEC Biometric SDK PC1 2.0 for .NET - Definitive\lib)
    * Bimcot.dll
    * ConfigFileReader.dll
    * FELE.dll
    * Fis.dll
    * fmatch.dll
    * MatchPC1.dll
    * MatLight.dll
    * MinutiasPC1.dll
    * nec_wsq.dll
    * NECImage.dll
    * pcgw32.dll
    * PID2.dll
    * pidwrapper.dll
    * SDKBiometrikException.dll
    * SDKImageTools.dll
    * SDKPID.dll
* Archivo de configuración instalado con el runtime/sdk de NEC (C:\Program Files (x86)\Nec Argentina S.A\NEC Biometric SDK PC1 2.0 for .NET - Definitive\config\SDKPID.config)
* Nancy v1.4.3
* Nanc.Hosting.Self v1.4.1
* TopShelf v3.3.1
* NLog v.4.4.9
* TopShelf.Nlog v3.3.1

Nota: Las Librerías TopShelf y Nancy no pueden ser actualizadas debido a que las versiones actualizadas de las librerías no soportan un framework inferior al .NET Framework 4.5.

## Instalación

1. Configrurar el puerto deseado en NecService.config, fuera del rango de puertos del sistema (>1023).
2. Instale la librería NEC 2.0 runtime
3. Copie las librerías y archivo de configuración instalados con el runtime de NEC (vea dependencias) a la misma carpeta donde se encuentre necservice.exe. Reemplace todo.
4. Abrir símbolo del sistema en modo administrador.
5. Dirigirse a donde se encuentre la aplicación (ejemplo: 'cd C:\Biometrika\NecService\')
6. Introducir el siguiente comando:
    necservice2021.exe install --delayed
7. La primera vez que corra el servicio se solicitarán permisos para asociar el puerto escogido a la aplicación. Elija aceptar (éste comportamiento se repetirá solo si es que se cambia el puerto en la configuración).

Notas: Los logs quedan ubicados dentro de la misma carpeta donde corre el servicio.

## Ejecutar sin instalar

Repita los pasos 1, 2 y 3 de la sección instalación y ejecute necservice.exe.