﻿using log4net;
using log4net.Config;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace NecService2021
{
    public class Program
    { 
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Program));

        static internal string  _bioIdClient = "1023";
        static internal string _bioIdUser = "bioka";
        static internal string _bioIdPsw = "biokatest";
        static internal string _bioIdObs = "sc";
        static internal BidCedChk.BidCedChk  _BIOID;

        static internal object _OBJECT_TO_BLOCK = new object();

        static internal TxLogHelper _TXLOG_HELPER;

        static internal string _VERSION = "7.5";

        static void Main()
        {
            try
            {
                //test();

                XmlConfigurator.Configure(new FileInfo(Properties.Settings.Default.ServiceLogPathConfig));
                LOG.Info("NecService2021.Program - Inicializando el servicio NEC 2021...");
                //Tomo version
                Version version = Assembly.GetExecutingAssembly().GetName().Version;
                _VERSION = version.ToString();
                LOG.Info("NecService2021.Program - v" + _VERSION);

                if (Properties.Settings.Default.ServiceGenerateTxLog)
                {
                    _TXLOG_HELPER = new TxLogHelper();
                    if (!_TXLOG_HELPER.Initialize())
                    {
                        LOG.Info("NecService2021.Program - LogTx Inicializado!");
                    } else
                    {
                        LOG.Warn("NecService2021.Program - LogTx Initialize ERROR!");
                    }
                }

                //Creando objeto para uso en verificaciones y login...
                _BIOID = new BidCedChk.BidCedChk();
                _bioIdClient = Properties.Settings.Default.WSIdClient != null ? Properties.Settings.Default.WSIdClient : _bioIdClient;
                _bioIdUser = Properties.Settings.Default.WSIdUser != null ? Properties.Settings.Default.WSIdUser : _bioIdUser;
                _bioIdPsw = Properties.Settings.Default.WSPsw != null ? Properties.Settings.Default.WSPsw : _bioIdPsw;

                LOG.Debug("NecService2021.Program - _BIOID object <> null => " + (_BIOID != null).ToString());
                LOG.Debug("NecService2021.Program - Seteando cliente => IdClient = " + _bioIdClient + "/" +
                          "IdUser=" + _bioIdUser + "/Psw=" + _bioIdPsw.Substring(0, 2) + ".../Obs=" + _bioIdObs);
                string res = _BIOID.SetCliente(_bioIdClient, _bioIdUser, _bioIdPsw, _bioIdObs);

                if (string.IsNullOrEmpty(res) || !res.StartsWith("ok"))
                {
                    LOG.Fatal("NecService2021.Program - Error seteando cliente en servicio remoto = " + res);
                    System.Diagnostics.Debugger.Break();
                    Environment.Exit(0);
                }
                else
                {
                    LOG.Debug("NecService2021.Program - Iniciando servicio...");
                    HostFactory.Run(x =>
                    {
                        x.Service<NecService2021>(s =>
                        {
                            s.ConstructUsing(name => new NecService2021());
                            s.WhenStarted(nh => nh.Start());
                            s.WhenStopped(nh => nh.Stop());
                        });
                        x.RunAsLocalSystem();
                        x.UseLog4Net();

                        x.SetDescription("Servicio de ayuda de NEC 2021");
                        x.SetDisplayName("NECService2021");
                        x.SetServiceName("NECService2021");
                        x.EnableServiceRecovery(rc =>
                        {
                            rc.RestartService(1);
                        });
                    });
                }
                System.Diagnostics.Debugger.Break();
            }
            catch (Exception ex)
            {
                LOG.Error("NecService2021.Program - Error al inicializar el servicio de comparación de huellas de NEC 2021", ex);
                System.Diagnostics.Debug.WriteLine(ex);
                System.Diagnostics.Debugger.Break();
            }

        }

        private static void test()
        {
            try
            {

                TxLog  _LOG_TX = JsonConvert.DeserializeObject<TxLog>
                                    (System.IO.Directory.GetCurrentDirectory() + "\\TxLog\\20210608_TxLog.db.log");
                _LOG_TX = JsonConvert.DeserializeObject<TxLog>
                                    (System.IO.File.ReadAllText(
                                            System.IO.Directory.GetCurrentDirectory() + "\\TxLog\\20210608_TxLog.db.log"));



                Tx transaction = new Tx();
                transaction.id = Guid.NewGuid().ToString("N");
                transaction.date = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                transaction.minutiaeTypeNecData = 1;
                transaction.sampleNec = "NecData";
                transaction.wsq = "wsq";
                transaction.timeToResponse = "500";
                transaction.rut = "21284415-2";
                transaction.name = "SUHIT";
                transaction.pais = "ARG";
                transaction.numeroDeSerie = "A123456789";
                transaction.expiracion = "15/10/2022";
                transaction.score = "3179";
                TxLog oTxLog = new TxLog();
                oTxLog.Transactions = new List<Tx>();
                oTxLog.Transactions.Add(transaction);

                string s = JsonConvert.SerializeObject(oTxLog);


            }
            catch (Exception ex)
            {

                LOG.Error(" Error: " + ex.Message);
            }
        }
    }
}
