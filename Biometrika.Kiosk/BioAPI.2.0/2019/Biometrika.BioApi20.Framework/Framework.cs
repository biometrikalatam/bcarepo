﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Biometrika.BioApi20.Interfaces;

namespace Biometrika.BioApi20.Framework
{
    public class Framework : IFramework
    {
        public IFrameworkSchema FrameworkSchema => throw new NotImplementedException();

        public List<AttachedSession> AttachedSessionList => throw new NotImplementedException();

        public AttachedSession BSPAttach(UUID bspUUID, string bioAPIVersion, ACBioParameters acBioParameters, List<UnitListElement> unitList, List<SecurityProfileType> securityProfileList)
        {
            throw new NotImplementedException();
        }

        public void BSPDetach(AttachedSession sessionAttached)
        {
            throw new NotImplementedException();
        }

        public void BSPLoad(UUID bspID, BFPEventCallback notifyCallback, BFPEnumerationCallback bfpEnumeration, string context)
        {
            throw new NotImplementedException();
        }

        public void BSPUnload(UUID bspID, BFPEventCallback notifyCallback, BFPEnumerationCallback bfpEnumeration, string context)
        {
            throw new NotImplementedException();
        }

        public void EnableEventNotifications(UUID bspID, List<EventKind> events)
        {
            throw new NotImplementedException();
        }

        public List<BFPSchema> EnumBFPs()
        {
            throw new NotImplementedException();
        }

        public List<BSPSchema> EnumBSPs()
        {
            throw new NotImplementedException();
        }

        public void Init(string version)
        {
            throw new NotImplementedException();
        }

        public List<BFPListElement> QueryBFPs(UUID bspUUID)
        {
            throw new NotImplementedException();
        }

        public List<UnitSchema> QueryUnits(UUID bspUUID)
        {
            throw new NotImplementedException();
        }

        public void Terminate()
        {
            throw new NotImplementedException();
        }
    }
}
