﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Biometrika.BioApi20.Interfaces;
using Biometrika.Standards;

namespace Biometrika.BioApi20
{
    public class AttachedSession
    {
        //public IBSP bsp { get; set; }

        //public AttachedSession(IBSP bspAttached)
        //{
        //    bsp = bspAttached;
        //}

        //public BSPSchema Schema
        //{
        //    get
        //    {
        //        return bsp.Schema;
        //    }
        //}

        //public BIR BiometricReference
        //{
        //    get
        //    {
        //        return bsp.BiometricReference;
        //    }
        //}

        ////public byte[] ACBioInstance
        ////{
        ////    get
        ////    {
        ////        return bsp.ACBioInstance;
        ////    }
        ////}

        ////public void BSPAttach(string bioAPIVersion, ACBioParameters acBioParameters, List<UnitListElement> unitList,
        ////    List<SecurityProfileType> securityProfileList)
        ////{
        ////    bsp.BSPAttach(bioAPIVersion, acBioParameters, unitList, securityProfileList);
        ////}

        //public void BSPAttach(string bioAPIVersion)
        //{
        //    bsp.BSPAttach(bioAPIVersion);
        //}

        //public void BSPDetach()
        //{
        //    bsp.BSPDetach();
        //}

        //public void BSPLoad(BFPEventCallback bioAPINotifyCallback, BFPEnumerationCallback bfpEnumeration)
        //{
        //    bsp.BSPLoad(bioAPINotifyCallback, bfpEnumeration);
        //}

        //public void BSPUnload()
        //{
        //    bsp.BSPUnload();
        //}

        //public byte CheckQuality(BIR inputBIR, DataFormat qualityAlgorithmID)
        //{
        //    return bsp.CheckQuality(inputBIR, qualityAlgorithmID);
        //}

        //public byte[] ControlUnit(int unitID, int controlCode, byte[] inputData)
        //{
        //    return bsp.ControlUnit(unitID, controlCode, inputData);
        //}

        //public int Enrol(BIR capturedBIR, BIR referenceTemplate, List<Purpose> purpose, BiometricSubtype subtype, DataFormat outputFormat,
        //    byte[] payload, int timeout, List<ResultOptions> options)
        //{
        //    return bsp.Enrol(capturedBIR, referenceTemplate, purpose, subtype, outputFormat, payload, timeout, options);
        //}

        //public int Enrol(List<BIR> capturedBIRs, BIR referenceTemplate, List<Purpose> purpose, BiometricSubtype subtype,
        //    DataFormat outputFormat, byte[] payload, int timeout, List<ResultOptions> options)
        //{
        //    return bsp.Enrol(capturedBIRs, referenceTemplate, purpose, subtype, outputFormat, payload, timeout, options);
        //}

        //public int Enrol(int numberOfSamples, BIR referenceTemplate, List<Purpose> purpose, BiometricSubtype subtype, DataFormat outputFormat,
        //    byte[] payload, int timeout, List<ResultOptions> options)
        //{
        //    return bsp.Enrol(numberOfSamples, referenceTemplate, purpose, subtype, outputFormat, payload, timeout, options);
        //}

        //public int Enrol(BIR capturedBIR, string referenceID, List<Purpose> purpose, BiometricSubtype subtype, DataFormat outputFormat,
        //    byte[] payload, int timeout, List<ResultOptions> options)
        //{
        //    return bsp.Enrol(capturedBIR, referenceID, purpose, subtype, outputFormat, payload, timeout, options);
        //}

        //public int Enrol(List<BIR> capturedBIRs, string referenceID, List<Purpose> purpose, BiometricSubtype subtype, DataFormat outputFormat,
        //    byte[] payload, int timeout, List<ResultOptions> options)
        //{
        //    return bsp.Enrol(capturedBIRs, referenceID, purpose, subtype, outputFormat, payload, timeout, options);
        //}

        //public int Enrol(int numberOfSamples, string referenceID, List<Purpose> listPurpose, BiometricSubtype subtype, DataFormat outputFormat,
        //    byte[] payload, int timeout, List<ResultOptions> options)
        //{
        //    return bsp.Enrol(numberOfSamples, referenceID, listPurpose, subtype, outputFormat, payload, timeout, options);
        //}

        ////public List<Candidate> IdentifyAggregated(int maxFMRrequested, BiometricSubtype subtype, bool binning, int maxResults, int timeout,
        ////    List<ResultOptions> options)
        ////{
        ////    return bsp.IdentifyAggregated(maxFMRrequested, subtype, binning, maxResults, timeout, options);
        ////}

        //public List<BFPListElement> QueryBFPs()
        //{
        //    return bsp.QueryBFPs();
        //}

        //public List<BFPListElement> QueryBFPs(List<UnitCategoryType> unitCategories)
        //{
        //    return bsp.QueryBFPs(unitCategories);
        //}

        ////public List<UnitSchema> QueryUnits()
        ////{
        ////    return bsp.QueryUnits();
        ////}

        ////public List<UnitSchema> QueryUnits(List<UnitCategoryType> unitCategories)
        ////{
        ////    return bsp.QueryUnits(unitCategories);
        ////}

        //public bool VerifyAggregated(int maxFMRrequested, BIR referenceTemplate, BiometricSubtype subtype, int timeout,
        //    List<ResultOptions> options)
        //{
        //    return bsp.VerifyAggregated(maxFMRrequested, referenceTemplate, subtype, timeout, options);
        //}

        //public bool VerifyAggregated(int maxFMRrequested, BIR processedBIR, string referenceKey, BiometricSubtype subtype, int timeout,
        //    List<ResultOptions> options)
        //{
        //    return bsp.VerifyAggregated(maxFMRrequested, processedBIR, referenceKey, subtype, timeout, options);
        //}

        //public bool VerifyAggregated(int maxFMRrequested, string referenceKey, BiometricSubtype subtype, int timeout,
        //    List<ResultOptions> options)
        //{
        //    return bsp.VerifyAggregated(maxFMRrequested, referenceKey, subtype, timeout, options);
        //}

        //public void SubscribeToGUIEvents(GUISelectEventCallback guiSelectEventCallbackFunction, GUIStateEventCallback
        //    guiStateEventCallbackFunction, GUIProgressEventCallback guiProgressEventCallbackFunction)
        //{
        //    bsp.SubscribeToGUIEvents(guiSelectEventCallbackFunction, guiStateEventCallbackFunction, guiProgressEventCallbackFunction);
        //}

        //public void UnsubscribeFromGUIEvents()
        //{
        //    bsp.UnsubscribeFromGUIEvents();
        //}

        //////////////////

        //public BIR Capture(List<Purpose> purpose, BiometricSubtype subtype, DataFormat outputFormat, int timeout, List<ResultOptions> options)
        //{
        //    return bsp.Capture(purpose, subtype, outputFormat, timeout, options);
        //}

        //public void Calibrate(int timeout)
        //{
        //    bsp.Calibrate(timeout);
        //}

        //public void SetIndicatorStatus(UnitIndicatorStatus indicatorStatus)
        //{
        //    bsp.SetIndicatorStatus(indicatorStatus);
        //}

        //public void SetPowerMode(UnitPowerMode powerMode)
        //{
        //    bsp.SetPowerMode(powerMode);
        //}

        //////////

        //public BIR CreateTemplate(BIR capturedBIR, BIR referenceTemplate, DataFormat outputFormat, byte[] payload)
        //{
        //    return bsp.CreateTemplate(capturedBIR, referenceTemplate, outputFormat, payload);
        //}

        //public BIR CreateTemplate(List<BIR> capturedBIRs, BIR referenceTemplate, DataFormat outputFormat, byte[] payload)
        //{
        //    return bsp.CreateTemplate(capturedBIRs, referenceTemplate, outputFormat, payload);
        //}

        //public BIR Process(BIR capturedBIR, DataFormat outputFormat)
        //{
        //    return bsp.Process(capturedBIR, outputFormat);
        //}

        //public BIR Process(BIR capturedBIR, List<BIR> auxiliaryBIRs, DataFormat outputFormat)
        //{
        //    return bsp.Process(capturedBIR, auxiliaryBIRs, outputFormat);
        //}

        //////////

        //public List<Candidate> Identify(int maxFMRrequested, BIR processedBIR, bool binning, int maxResults, int timeout)
        //{
        //    return bsp.Identify(maxFMRrequested, processedBIR, binning, maxResults, timeout);
        //}

        //public List<Candidate> Identify(int maxFMRrequested, BIR processedBIR, List<BIR> auxiliaryBIRs, bool binning, int maxResults, int timeout)
        //{
        //    return bsp.Identify(maxFMRrequested, processedBIR, auxiliaryBIRs, binning, maxResults, timeout);
        //}

        //public void PresetIdentifyPopulation(IdentifyPopulation population)
        //{
        //    bsp.PresetIdentifyPopulation(population);
        //}

        //public bool Verify(int maxFMRrequested, BIR processedBIR, BIR referenceTemplate, List<ResultOptions> options)
        //{
        //    return bsp.Verify(maxFMRrequested, processedBIR, referenceTemplate, options);
        //}

        //public bool Verify(int maxFMRrequested, BIR processedBIR, BIR referenceTemplate, List<BIR> auxiliaryBIRs, List<ResultOptions> options)
        //{
        //    return bsp.Verify(maxFMRrequested, processedBIR, referenceTemplate, auxiliaryBIRs, options);
        //}

        //////////

        //public void CloseDatabase()
        //{
        //    bsp.CloseDatabase();
        //}

        //public void DeleteBIR(UUID key)
        //{
        //    bsp.DeleteBIR(key);
        //}

        //public BIR GetSingleBIR(UUID key)
        //{
        //    return bsp.GetSingleBIR(key);
        //}

        //public List<UUID> ListUUIDs()
        //{
        //    return bsp.ListUUIDs();
        //}

        //public IdentifyPopulation NewIdentifyPopulation()
        //{
        //    return bsp.NewIdentifyPopulation();
        //}

        //public IdentifyPopulation NewIdentifyPopulation(List<UUID> uuidList)
        //{
        //    return bsp.NewIdentifyPopulation(uuidList);
        //}

        //public IdentifyPopulation NewIdentifyPopulation(byte[] query)
        //{
        //    return bsp.NewIdentifyPopulation(query);
        //}

        //public void OpenDatabase(byte[] databaseID, BIRDatabaseAccess access)
        //{
        //    bsp.OpenDatabase(databaseID, access);
        //}

        //public UUID StoreBIR(BIR biometricReference)
        //{
        //    return bsp.StoreBIR(biometricReference);
        //}

        //public void StoreBIR(BIR biometricReference, UUID key)
        //{
        //    bsp.StoreBIR(biometricReference, key);
        //}

        //public UUID StoreBIR(BIR biometricReference, byte[] auxiliaryData)
        //{
        //    return bsp.StoreBIR(biometricReference, auxiliaryData);
        //}

        //public void StoreBIR(BIR biometricReference, byte[] auxiliaryData, UUID key)
        //{
        //    bsp.StoreBIR(biometricReference, auxiliaryData, key);
        //}
    }
}
