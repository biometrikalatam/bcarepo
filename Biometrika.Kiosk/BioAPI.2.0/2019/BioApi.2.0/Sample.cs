﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BioApi20
{
    public class Sample
    {
        public int AuthenticationFactor { get; set; }
        public int MinutiaeType { get; set; }
        public int BodyPart { get; set; }
        public int Operation { get; set; }  //0-Verify | 2-Enroll
        public int SampleWith { get; set; }
        public int SampleHeight{ get; set; }
        public object Data { get; set; }

        public Sample()
        {

        }
        public Sample(int authenticationFactor, int minutiaeType, int bodyPart, int operation, int sampleWith, int sampleHeight, object data)
        {
            AuthenticationFactor = authenticationFactor;
            MinutiaeType = minutiaeType;
            BodyPart = bodyPart;
            Operation = operation;
            SampleWith = sampleWith;
            SampleHeight = sampleHeight;
            Data = data;
    }

        //public string GetData()
        //{
        //    if (typeof(Data).Equals())

        //    return (Data != null ? Convert.ToBase64String(Data) : null);
        //}
    }
}
