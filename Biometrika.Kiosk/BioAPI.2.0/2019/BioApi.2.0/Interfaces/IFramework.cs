﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BioApi20.Interfaces
{
    public interface IFramework
    {

        /// <summary>
        /// Retrieves the information about the biometric system. 
        /// the FrameworkSchema object that identifies the implementation of the BioAPI framework.        
        /// </summary>
        IFrameworkSchema FrameworkSchema { get; }
        List<AttachedSession> AttachedSessionList { get; }

        //AttachedSession BSPAttach(UUID bspUUID, string bioAPIVersion, ACBioParameters acBioParameters, List<UnitListElement> unitList,
        //    List<SecurityProfileType> securityProfileList);
        //void BSPDetach(AttachedSession sessionAttached);
        //void BSPLoad(UUID bspID, BFPEventCallback notifyCallback, BFPEnumerationCallback bfpEnumeration, string context);

        //void BSPUnload(UUID bspID, BFPEventCallback notifyCallback, BFPEnumerationCallback bfpEnumeration, string context);

        //void EnableEventNotifications(UUID bspID, List<EventKind> events);
        //List<BFPSchema> EnumBFPs();
        //List<BSPSchema> EnumBSPs();
        //void Init(string version);
        //List<BFPListElement> QueryBFPs(UUID bspUUID);
        //List<UnitSchema> QueryUnits(UUID bspUUID);
        void Terminate();
    }
}
