﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BioApi20.Interfaces
{
    public interface IProcessingFactory
    {
        /* EVENTS */
        event ErrorCallbackDelegate OnErrorEvent; // (int errCode, string errMessage, ISensor sensor);
        event TimeoutCallbackDelegate OnTimeoutEvent;  //(int errCode, string errMessage, ISensor sensor);
        event TaskDoneCallbackDelegate OnTaskDoneEvent;  //(int errCode, string errMessage, ISensor sensor);

        /// <summary>
        /// Configraucion para levantar lo correcto
        /// </summary>
        ProcessingFactorySchema Schema { get; set; }

        /// <summary>
        /// Lista de matchers configurados en Schema
        /// </summary>
        Hashtable HT_MATCHER { get; set; }

        /// <summary>
        /// Levanta el schema y carga las listas para posterior procesos
        /// </summary>
        /// <returns></returns>
        int Initialization();

        /// <summary>
        /// Cierra todos los sensores y libera recursos
        /// </summary>
        /// <returns></returns>
        int Close();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sample"></param>
        /// <param name="typeImage">0-JPG desde RAW sin Completar | 1-JPG desde RAW 512x512</param>
        /// <param name="typeReturn">0-Como Drawing.Image | 1- Como byte[]</param>
        /// <param name="sampleout"></param>
        /// <returns></returns>
        int GetImage(Sample sample, int typeImage, int typeReturn, out Sample sampleout, out Error error);

        /// <summary>
        /// Dado RAW => Retorna WSQ. Si el RAW no es de 512x512 => Lo rellena
        /// </summary>
        /// <param name="sample"></param>
        /// <param name="sampleout"></param>
        /// <returns></returns>
        int GetWSQ(Sample sample, bool complete512, out Sample sampleout, out Error error);

        /// <summary>
        /// Dado una minutiaetype, ve si es factible la MT de out, y lo realiza o da error.
        /// </summary>
        /// <param name="sample"></param>
        /// <param name="mtout"></param>
        /// <param name="purpose">0-Verify | 2-Enroll</param>
        /// <param name="sampleout"></param>
        /// <returns></returns>
        int GetMinutiae(Sample sample, Bio.Core.Matcher.Constant.MinutiaeType mtout, int purpose, out Sample sampleout, out Error error);

        
    }
}
