﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Biometrika.Standards;

namespace Biometrika.BioApi20.Interfaces
{
    public interface IBSP 
    {
        /* BASE */
        bool IsLoaded { get; set; }
        //Configuraciones en XML para inicio
        BSPSchema Schema { get; set; }
        ISensorFactory SENSOR_FACTORY { get; set; }
        IProcessingFactory PROCESSING_FACTORY { get; set; }
        //        IComparisionFactory COMPARISION_FACTORY { get; set; }

        /* EVENTS */
        event CaptureCallbackDelegate OnCapturedEvent; //(int errCode, string errMessage, List<Sample> samplesCaptured);
        event ErrorCallbackDelegate OnErrorEvent; // (int errCode, string errMessage, ISensor sensor);
        event ConnectCallbackDelegate OnConnectEvent; // (int errCode, string errMessage, ISensor sensor);
        event DisconnectCallbackDelegate OnDisconnectEvent; // (int errCode, string errMessage, ISensor sensor);
        event TimeoutCallbackDelegate OnTimeoutEvent;  //(int errCode, string errMessage, ISensor sensor);

        //Crea objetos, inicializa y carga los BFP si bspload = true
        void BSPAttach(string bioAPIVersion, bool bspLoad);
        //Libera todo
        void BSPDetach();
        //Recorre el Schema, levantando los BFP configurados e inicializa Factorys
        void BSPLoad(); // BFPEventCallback bioAPINotifyCallback, BFPEnumerationCallback bfpEnumeration);
        //Libera todo lo que se creo con Load
        void BSPUnload();

        /* OPERATIONS */
        /// <summary>
        /// Captura de aceurdo a lo indicado en parametros
        /// </summary>
        /// <param name="SensorType">Tipo de device, </param>
        /// <param name="SensorSerial">Si serial es != null => se toma desde ese serial si existe</param>
        /// <param name="timeout">Tempo en milisegundos para la captura sino manda evento de timeout. 
        /// Si va 0 se fija en el default del BSP</param>
        /// <param name="CaptureType">Valores Posibles:
        ///     0 - Todos
        ///     1 - Solo WSQ (Para Fingerprint)
        ///     2 - Solo Minucias
        ///     3 - Solo JPG (Para Facial)
        /// Estos se devuelven planos o como BIR si la configuracion de BCP es BioApi20Enabled = true
        /// </param>
        /// <returns></returns>
        List<Sample> Capture(int sensorType, int bodypart, string sensorSerial, int timeout, int captureType, int qualityCapture, out Error error);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listSamples"></param>
        /// <param name="AuthenticationFactor"></param>
        /// <param name="MinutiaeType"></param>
        /// <param name="thershold"></param>
        /// <param name="score"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        bool Verify(List<Sample> listSamples, int AuthenticationFactor, int MinutiaeType, double thershold, out double score, out Error error);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sample"></param>
        /// <param name="listSamples"></param>
        /// <param name="AuthenticationFactor"></param>
        /// <param name="MinutiaeType"></param>
        /// <param name="typeIdentify"></param>
        /// <param name="thershold"></param>
        /// <param name="score"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        List<Sample> Identify(Sample sample, List<Sample> listSamples, int AuthenticationFactor, int MinutiaeType, int typeIdentify,
            double thershold, out double score, out Error error);

        /*  QUERIES */
        List<BFPListElement> QueryBFPs();
        List<BFPListElement> QueryBFPs(List<UnitCategoryType> unitCategories);

        /// <summary>
        /// Habilita el sensor para captura constante con eventos, para situaciones de control de acceso. 
        /// Se retorna el evento de captura cuando se produce el evento de captura
        /// </summary>
        /// <param name="enable"></param>
        /// <param name="hwnd"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        int EnableAutoEvent(string sensorSerial, bool enable, int hwnd, out Error error);

        /// <summary>
        /// Habilita la captura continua. Para uso de kioskos con toma continua.
        /// </summary>
        /// <param name="sensorSerial"></param>
        /// <param name="qualityCapture"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        int CaptureContinue(string sensorSerial, int qualityCapture, out Error error);

        /// <summary>
        /// Permite prender led lector
        /// </summary>
        void On(string sensorSerial);

        /// <summary>
        /// Permite apagar el lector
        /// </summary>
        void Off(string sensorSerial);

        /*  EVENTS */
        //void SubscribeToGUIEvents(GUISelectEventCallback guiSelectEventCallback, GUIStateEventCallback
        //   guiStateEventCallback, GUIProgressEventCallback guiProgressEventCallback);
        //void UnsubscribeFromGUIEvents();


        //BIR BiometricReference { get; }
        //byte[] ACBioInstance { get; }

        //void BSPAttach(string bioAPIVersion, ACBioParameters acBioParameters, List<UnitListElement> unitList,
        //    List<SecurityProfileType> securityProfileList);
        //void BSPAttach(string bioAPIVersion);
        //void BSPDetach();
        //void BSPLoad(BFPEventCallback bioAPINotifyCallback, BFPEnumerationCallback bfpEnumeration);
        //void BSPUnload();
        //byte CheckQuality(BIR inputBIR, DataFormat qualityAlgorithmID);
        //byte[] ControlUnit(int unitID, int controlCode, byte[] inputData);

        //int Enrol(BIR capturedBIR, BIR referenceTemplate, List<Purpose> purpose, BiometricSubtype subtype, DataFormat outputFormat,
        //    byte[] payload, int timeout, List<ResultOptions> options);
        //int Enrol(List<BIR> capturedBIRs, BIR referenceTemplate, List<Purpose> purpose, BiometricSubtype subtype, DataFormat outputFormat,
        //    byte[] payload, int timeout, List<ResultOptions> options);
        //int Enrol(int NumberOfSamples, BIR referenceTemplate, List<Purpose> purpose, BiometricSubtype subtype, DataFormat outputFormat,
        //    byte[] payload, int timeout, List<ResultOptions> options);
        //int Enrol(BIR capturedBIR, string referenceID, List<Purpose> purpose, BiometricSubtype subtype, DataFormat outputFormat,
        //    byte[] payload, int timeout, List<ResultOptions> options);
        //int Enrol(List<BIR> capturedBIRs, string referenceID, List<Purpose> purpose, BiometricSubtype subtype, DataFormat outputFormat,
        //    byte[] payload, int timeout, List<ResultOptions> options);
        //int Enrol(int NumberOfSamples, string referenceID, List<Purpose> purpose, BiometricSubtype subtype, DataFormat outputFormat,
        //    byte[] payload, int timeout, List<ResultOptions> options);

        //List<Candidate> IdentifyAggregated(int maxFMRrequested, BiometricSubtype subtype, bool binning, int maxResults, int timeout,
        //    List<ResultOptions> options);

        //bool VerifyAggregated(int maxFMRrequested, BIR referenceTemplate, BiometricSubtype subtype, int timeout, List<ResultOptions> options);
        //bool VerifyAggregated(int maxFMRrequested, BIR processedBIR, string referenceKey, BiometricSubtype subtype, int timeout,
        //    List<ResultOptions> options);
        //bool VerifyAggregated(int maxFMRrequested, string referenceKey, BiometricSubtype subtype, int timeout, List<ResultOptions> options);


    }
}
