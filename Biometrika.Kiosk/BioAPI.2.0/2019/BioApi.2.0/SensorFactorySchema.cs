﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BioApi20
{
    public class SensorFactorySchema
    {
        public List<SensorFactorySchemaItem> listSensors;
    }

    public class SensorFactorySchemaItem
    {
        /// <summary>
        /// Sensor de que es (Ej: 2 = Fingerprint | 4 = Facial)
        /// </summary>
        public int AuthenticationFactor;

        /// <summary>
        /// Marca
        /// </summary>
        public int Brand;

        /// <summary>
        /// Default en milisegundos para captura
        /// </summary>
        public int TimeoutCapture = 5000;

        /// <summary>
        /// Lista de seriales que deben ser configurados
        /// </summary>
        public List<string> ListSeriales;

        /// <summary>
        /// Path dll de Biometric Function Providers para instanciar 
        /// </summary>
        public string PathBFP;

        /// <summary>
        /// Parametros si necesita. Con formato:
        ///            param1=value1|param2=value2
        /// </summary>
        public string ParametersBFP;

        /// <summary>
        /// Para chequear calidad en la captura
        /// </summary>
        public double QualityCapture = 70;
    }
}
