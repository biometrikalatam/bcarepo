﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Biometrika.BioApi20.Interfaces;

namespace Biometrika.BioApi20
{
    //public delegate bool GUISelectEventCallback(UUID bspUUID, int unitID, GUIEnrolType enrolType, GUIOperation operation,
    //       GUIMoment moment, int resultCode, int maxNumEnrollSamples, List<BiometricSubtype> selectableInstances,
    //       List<BiometricSubtype> selectedInstances, List<BiometricSubtype> capturedInstances, string text, GUIResponse response);

    //public delegate bool GUIStateEventCallback(UUID bspUUID, int unitID, GUIOperation operation, GUISuboperation suboperation,
    //    Purpose purpose, GUIMoment moment, int resultCode, int enrolSampleIndex, List<GUIBitmap> bitmaps, string text,
    //    GUIResponse response, int enrolSampleIndexToRecapture);

    //public delegate bool GUIProgressEventCallback(UUID bspUUID, int unitID, GUIOperation operation, GUISuboperation suboperation,
    //    Purpose purpose, GUIMoment moment, byte suboperationProgress, List<GUIBitmap> bitmaps, string text, GUIResponse response);

    //public delegate void BFPEventCallback(string bfpUUID, int unitID, UnitSchema unitSchema, EventKind eventKind);

    //public delegate void BFPGUIProgressCallback(int unitID, string context, List<GUIBitmap> bitmaps, byte response);

    //public delegate List<BFPSchema> BFPEnumerationCallback();

    public delegate void CaptureCallbackDelegate(int errCode, string errMessage, List<Sample> samplesCaptured);
    public delegate void ErrorCallbackDelegate(int errCode, string errMessage, object sensor);
    public delegate void ConnectCallbackDelegate(int errCode, string errMessage, ISensor sensor);
    public delegate void DisconnectCallbackDelegate(int errCode, string errMessage, ISensor sensor);
    public delegate void TimeoutCallbackDelegate(int errCode, string errMessage, ISensor sensor);
    public delegate void TaskDoneCallbackDelegate(int errCode, string errMessage, object objectReturn);


}
