﻿using System;
using System.Drawing;

namespace BioAPI
{
    /// <summary>
    ///
    /// </summary>
    public class FingerCaptured : IFingerCaptured
    {
        public ReaderFactory.CaptureQuality CaptureQuality { get; set; }

        public ReaderFactory.CaptureResultStatus CaptureResultStatus { get; set; }

        public ReaderImage ReaderImage { get; set; }

        public int Finger { get; set; }

        public Image Image { get; set; }

        public Byte[] TemplateANSI { get; set; }

        public string TemplateANSI64 { get; set; }

        public Byte[] TemplateISO { get; set; }

        public string TemplateISO64 { get; set; }

        public Byte[] TemplateRegistry { get; set; }

        public string TemplateRegistry64 { get; set; }

        public Byte[] TemplateVerify { get; set; }

        public string TemplateVerify64 { get; set; }

        public string WSQ64 { get; set; }

        public Guid Guid { get; set; }

        public object Clone()
        {
            FingerCaptured newFingerCaptured = (FingerCaptured)this.MemberwiseClone();

            //newFingerCaptured.ReaderImage = (ReaderImage)this.ReaderImage.Clone();

            //newFingerCaptured.Image = (Image)this.Image.Clone();

            //newFingerCaptured.TemplateANSI = (Byte[])this.TemplateANSI.Clone();

            //newFingerCaptured.TemplateANSI64 = String.Copy(TemplateANSI64);

            //newFingerCaptured.TemplateISO = (Byte[])this.TemplateISO.Clone();

            //newFingerCaptured.TemplateISO64 = String.Copy(TemplateISO64);

            //newFingerCaptured.TemplateRegistry = (Byte[])this.TemplateRegistry.Clone();

            //newFingerCaptured.TemplateRegistry64 = String.Copy(TemplateRegistry64);

            //newFingerCaptured.TemplateVerify = (Byte[])this.TemplateVerify.Clone();

            //newFingerCaptured.TemplateVerify64 = String.Copy(TemplateVerify64);

            //newFingerCaptured.WSQ64 = String.Copy(WSQ64);

            //newFingerCaptured.Guid = Guid.NewGuid();
              
            return newFingerCaptured;
        }
    }
}