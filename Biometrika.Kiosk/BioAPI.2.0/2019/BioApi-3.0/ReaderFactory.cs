﻿using System.Diagnostics;
using System.Management;

namespace BioAPI
{
    /// <summary>
    ///
    /// </summary>
    public static class ReaderFactory
    {
        public delegate void CaptureCallbackDelegate(IFingerCaptured result);

        public enum CaptureQuality
        {
            QUALITY_GOOD,
            QUALITY_POOR
        }

        public enum CaptureResultStatus
        {
            SUCCESS,
            FAIL
        }

        public enum ComparisionResult
        {
            MATCH,
            NOT_MATCH,
            ERROR
        }

        /// <summary>
        ///
        /// </summary>
        public enum ReaderBrand
        {
            DIGITAL_PERSONA,
            SECUGEN,
            NONE
        }

        /// <summary>
        /// Enum de resultados
        /// </summary>
        public enum ReaderResultCode
        {
            DEVICE_BUSY,
            DEVICE_READY,
            DEVICE_FAILURE,
            INVALID_DEVICE,
            SUCCESS,
            MORETHANONE_DEVICE
        }

        private static ReaderStatus ReaderStatus { get; set; }

        public static bool ReaderConnected { get; set; }

        private static AbstractReader reader = null;

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public static AbstractReader GetReader()
        {
            ReaderConnected = false;
            if (IsSecugenConnected)
                reader = new SecugenReader();

            if (IsDigitalPersonaConnected)
                reader = new DigitalPersonaReader();

            if (reader != null)
            {
                ReaderStatus = new ReaderStatus() { IsOpen = true };
                ReaderConnected = true;
            }

            return reader;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        public static AbstractReader GetReader(ReaderBrand brand)
        {
            ELog(string.Concat("Inicializando Reader {0}", brand.ToString()));

            ReaderConnected = false;

            switch (brand)
            {
                case ReaderFactory.ReaderBrand.DIGITAL_PERSONA:
                    reader = new DigitalPersonaReader();
                    break;
                case ReaderFactory.ReaderBrand.SECUGEN:
                    reader = new SecugenReader();
                    break;
                case ReaderFactory.ReaderBrand.NONE:
                    reader = null;
                    break;
            }


            if (reader != null)
            {
                ReaderStatus = new ReaderStatus() { IsOpen = true };
                ReaderConnected = true;
            }

            return reader;
        }

        public static ComparisionResult Compare(IFingerCaptured fingerCaptured1, IFingerCaptured fingerCaptured2)
        {
            return FingerCaptureProcessor.Compare(fingerCaptured1, fingerCaptured2);
        }

        private static bool IsDigitalPersonaConnected
        {
            get
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(@"SELECT * FROM WIN32_PNPENTITY  where  DEVICEID LIKE '%VID_05BA%'");
                return searcher.Get().Count > 0;
            }
        }

        private static bool IsSecugenConnected
        {
            get
            {
                var searcher = new ManagementObjectSearcher(@"SELECT * FROM WIN32_PNPENTITY  where  DEVICEID LIKE '%VID_1162%'");
                var collection = searcher.Get();

                return collection.Count > 0;
            }
        }

        private static void ELog(string p)
        {
            //EventLog log = new EventLog() { Source = "Bioenroll" };
            //log.WriteEntry(p, EventLogEntryType.Information);
        }

        /// <summary>
        /// Reader base
        /// </summary>
        public static void Dispose()
        {
            if (reader != null)
            {
                reader.CloseReader();
                reader.Dispose();
                reader = null;
            }
        }
    }
}