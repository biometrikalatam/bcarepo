using System;
using System.Collections.Generic;

namespace BioAPI
{
    public interface IAbstractReader
    {
        event ReaderFactory.CaptureCallbackDelegate OnCapturedEvent;
        ReaderStatus ReaderStatus { get; set; }
        ReaderInfo ReaderInfo { get; set; }
        void Capture(int finger);
        BioAPI.ReaderFactory.ReaderResultCode Status();
        void CloseReader();
        void CancelCapture();
        bool IsEventHandlerCaptureRegistered();
    }
}
