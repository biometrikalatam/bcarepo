﻿using System;
using System.Collections.Generic;

namespace BioAPI
{
    public class ReaderInfo
    {
        public int ImageWidth { get; set; }

        public int ImageHeight { get; set; }

        public string DeviceID { get; set; }

        public string SerialNumber { get; set; }
    }
}
