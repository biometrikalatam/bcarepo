﻿using System;
using System.Collections.Generic;

namespace BioAPI
{
    public enum ReaderError
    {
        ERROR_CREATE_VERIFY_TEMPLATE,
        ERROR_IN_MATCHING
    }

    public class ReaderException : Exception
    {
        private ReaderError readerError;

        public ReaderException()
        {
        }

        public ReaderException(string message)
            : base(message)
        {
        }

        public ReaderException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public ReaderException(ReaderError readerError)
            : this()
        {
            this.readerError = readerError;
        }
    }
}
