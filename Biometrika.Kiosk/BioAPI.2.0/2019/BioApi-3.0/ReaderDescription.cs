﻿using System;
using System.Collections.Generic;

namespace BioAPI
{
    /// <summary>
    /// Descripción del lector
    /// </summary>
    public class ReaderDescription
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string SerialNumber { get; set; }

        public string Technology { get; set; }

        public string Version { get; set; }
    }
}
