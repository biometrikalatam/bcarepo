﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("BioAPI")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Biometrika")]
[assembly: AssemblyProduct("BioAPI")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("32687aef-4f55-4bc5-827e-08469a878e01")]

[assembly: AssemblyVersion("3.0.1.*")]
[assembly: AssemblyFileVersion("3.0.1.*")]