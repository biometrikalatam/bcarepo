﻿#if DIGITALPERSONA
  using DPUruNet;
#endif
#if SECUGEN
using SecuGen.FDxSDKPro.Windows;
#endif
using Bio.Core.Imaging;
using Bio.Core.Wsq.Encoder;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;

namespace BioAPI
{
    public static class FingerCaptureProcessor
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(FingerCaptureProcessor));

        private const int QUALITY_LIMMIT = 40;
        private static int ImageWidth = 512;
        private static int ImageHeight = 512;
#if DIGITALPERSONA
        private static Reader readerDP;
        private static CaptureResult captureResult;
#endif        
#if SECUGEN
        private static SGFingerPrintManager readerSGF = new SGFingerPrintManager();
#endif
        private static ReaderFactory.ReaderBrand readerBrand;
        private static byte[] rawImage = new Byte[ImageWidth * ImageHeight];

        private static ReaderFactory.CaptureQuality CaptureQuality { get; set; }

        private static TemplateFormat registryDefault = TemplateFormat.BASICRegistryTemplate;

        /// <summary>
        ///
        /// </summary>
        /// <param name="fingerCaptured1"></param>
        /// <param name="fingerCaptured2"></param>
        /// <returns></returns>
        public static ReaderFactory.ComparisionResult Compare(IFingerCaptured fingerCaptured1, IFingerCaptured fingerCaptured2)
        {
            switch (readerBrand)
            {
                case ReaderFactory.ReaderBrand.DIGITAL_PERSONA:
                    break;

                case ReaderFactory.ReaderBrand.SECUGEN:
#if SECUGEN
                    if (registryDefault == TemplateFormat.ANSITemplate)
                    {
                        var sample_info = new SGFPMANSITemplateInfo();
                        readerSGF.GetAnsiTemplateInfo(fingerCaptured1.TemplateRegistry, sample_info);

                        for (var i = 0; i < sample_info.TotalSamples; i++)
                        {
                            var matched = false;
                            readerSGF.MatchAnsiTemplate(fingerCaptured1.TemplateRegistry, i, fingerCaptured1.TemplateVerify, 0, SGFPMSecurityLevel.NORMAL, ref matched);
                            if (matched)
                            {
                                return ReaderFactory.ComparisionResult.MATCH;
                            }
                        }
                        return ReaderFactory.ComparisionResult.NOT_MATCH;
                    }

                    if (registryDefault == TemplateFormat.BASICRegistryTemplate)
                    {
                        var matched = false;
                        ELog("FingerCaptureProcessor -> ComparisionResult() -> BASICRegistryTemplate");
                        //ELog("fingerCaptured1.TemplateRegistry $$$" + DumpBytes(fingerCaptured1.TemplateRegistry) + "$$$");

                        //ELog("fingerCaptured2.TemplateVerify $$$" + DumpBytes(fingerCaptured2.TemplateVerify) + "$$$");

                        var error = readerSGF.MatchTemplate(fingerCaptured1.TemplateRegistry, fingerCaptured2.TemplateVerify, SGFPMSecurityLevel.NORMAL, ref matched);

                        if (error != (int)SGFPMError.ERROR_NONE)
                        {
                            ELog("FingerCaptureProcessor -> ComparisionResult() -> BASICRegistryTemplate -> Error" + error.ToString());

                            return ReaderFactory.ComparisionResult.ERROR;
                        }
                        if (matched)
                        {
                            return ReaderFactory.ComparisionResult.MATCH;
                        }
                        else
                        {
                            return ReaderFactory.ComparisionResult.NOT_MATCH;
                        }
                    }
#endif
                    break;

                case ReaderFactory.ReaderBrand.NONE:
                    break;
            }

            return ReaderFactory.ComparisionResult.ERROR;
        }

#if DIGITALPERSONA
        /// <summary>
        /// Assign reader brand specific
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static int InitializeDigitalPersona(DPUruNet.Reader reader)
        {
            readerDP = reader;
            readerBrand = ReaderFactory.ReaderBrand.DIGITAL_PERSONA;
            return 0;
        }

        public static int SetCapture(CaptureResult capResult)
        {
            captureResult = capResult;
            var fiv = capResult.Data.Views[0];
            rawImage = fiv.RawImage;

            ImageWidth = fiv.Width;
            ImageHeight = fiv.Height;

            CaptureResultStatus = ReaderFactory.CaptureResultStatus.SUCCESS;

            if (capResult.Quality == Constants.CaptureQuality.DP_QUALITY_GOOD)
            {
                CaptureQuality = ReaderFactory.CaptureQuality.QUALITY_GOOD;
            }
            if (capResult.Quality != Constants.CaptureQuality.DP_QUALITY_GOOD)
            {
                CaptureQuality = ReaderFactory.CaptureQuality.QUALITY_POOR;
            }
            return 0;
        }


#endif

#if SECUGEN
        /// <summary>
        /// Assign reader brand specific
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static int InitializeSecugen(SGFingerPrintManager reader, int imageWidth, int imageHeight)
        {
            ImageWidth = imageWidth;
            ImageHeight = imageHeight;
            readerSGF = reader;
            readerBrand = ReaderFactory.ReaderBrand.SECUGEN;
            return 0;
        }



        /// <summary>
        /// Get  image array in bytes
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static int SetCapture(byte[] image)
        {
            rawImage = image;
            var img_qlty = 0;
            LOG.Debug("SetCapture IN...");
            var response = readerSGF.GetImageQuality(ImageWidth, ImageHeight, rawImage, ref img_qlty);
            LOG.Debug("SetCapture readerSGF.GetImageQuality responce == 0 => " + (response == 0).ToString());
            CaptureResultStatus = ReaderFactory.CaptureResultStatus.SUCCESS;

            if (img_qlty > QUALITY_LIMMIT)
            {
                CaptureQuality = ReaderFactory.CaptureQuality.QUALITY_GOOD;
            }
            if (img_qlty <= QUALITY_LIMMIT)
            {
                CaptureQuality = ReaderFactory.CaptureQuality.QUALITY_POOR;
            }
            LOG.Debug("SetCapture Image Quality = " + img_qlty.ToString());
            return response;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        private static SGFPMFingerInfo FingerInfo()
        {
            return new SGFPMFingerInfo()
            {
                FingerNumber = (SGFPMFingerPosition)0,
                ImageQuality = (Int16)80,
                ImpressionType = (Int16)SGFPMImpressionType.IMPTYPE_LP,
                ViewNumber = 1
            };
        }

        private static Byte[] TemplateInit(TemplateFormat format)
        {
            int error;
            if (format == TemplateFormat.ANSITemplate)
            {
                error = readerSGF.SetTemplateFormat(SGFPMTemplateFormat.ANSI378);
            }
            else
            {
                if (format == TemplateFormat.ISOTemplate)
                {
                    error = readerSGF.SetTemplateFormat(SGFPMTemplateFormat.ISO19794);
                }
                else
                {
                    error = readerSGF.SetTemplateFormat(SGFPMTemplateFormat.SG400);
                }
            }

            var max_template_size = 800;
            error = readerSGF.GetMaxTemplateSize(ref max_template_size);

            return new Byte[max_template_size];
        }
#endif

        /// <summary>
        /// Get ANSI Byte array Template
        /// </summary>
        /// <returns></returns>
        public static byte[] GetANSITemplate()
        {
            if (readerBrand == ReaderFactory.ReaderBrand.SECUGEN)
            {
#if SECUGEN
                var templateANSI = TemplateInit(TemplateFormat.ANSITemplate);
                readerSGF.CreateTemplate(FingerInfo(), rawImage, templateANSI);
                LOG.Debug("GetANSITemplate From Secugen ANSITEmplate = " + Convert.ToBase64String(templateANSI));
                return templateANSI;
#else
                return null;
#endif

            }
            else
            {
#if DIGITALPERSONA
                var fmdANSI = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.ANSI);
                LOG.Debug("GetANSITemplate From DP ANSITEmplate = " + Convert.ToBase64String(fmdANSI.Data.Bytes));
                return fmdANSI.Data.Bytes;
#else
                return null;                 
#endif
            }
        }

        /// <summary>
        /// Get ISO Byte array Template
        /// </summary>
        /// <returns></returns>
        public static byte[] GetISOTemplate()
        {
            if (readerBrand == ReaderFactory.ReaderBrand.SECUGEN)
            {
#if SECUGEN
                var templateISO = TemplateInit(TemplateFormat.ISOTemplate);
                readerSGF.CreateTemplate(FingerInfo(), rawImage, templateISO);
                LOG.Debug("GetISOTemplate From Secugen ISOTEmplate = " + Convert.ToBase64String(templateISO));
                return templateISO;
#else
                return null;
#endif
            }
            else
            {
#if DIGITALPERSONA
                var fmdISO = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.ISO);
                LOG.Debug("GetISOTemplate From DP ISOTEmplate = " + Convert.ToBase64String(fmdISO.Data.Bytes));
                return fmdISO.Data.Bytes;
#else
                return null;
#endif
            }
        }

        /// <summary>
        /// Get propietarie verify template
        /// </summary>
        /// <returns></returns>
        public static byte[] GetVerifyTemplate()
        {
            if (readerBrand == ReaderFactory.ReaderBrand.SECUGEN)
            {
#if SECUGEN
                var template = new Byte[400];
                var error = readerSGF.CreateTemplate(null, rawImage, template);

                //if (error != (int)SGFPMError.ERROR_NONE)
                //{
                //    throw new ReaderException(ReaderError.ERROR_CREATE_VERIFY_TEMPLATE);
                //}
                return template;
#endif
            }
#if DIGITALPERSONA
            if (readerBrand == ReaderFactory.ReaderBrand.DIGITAL_PERSONA)
            {
                var fmdVerify = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.DP_VERIFICATION);
                if (fmdVerify.Data == null)
                {
                    throw new ReaderException(ReaderError.ERROR_CREATE_VERIFY_TEMPLATE);
                }
                return fmdVerify.Data.Bytes;
            }
#endif
            return null;
        }

        /// <summary>
        /// Get default registry Template
        /// </summary>
        /// <returns></returns>
        public static byte[] GetRegistryTemplate()
        {
#if SECUGEN
            if (readerBrand == ReaderFactory.ReaderBrand.SECUGEN)
            {
                if (registryDefault == TemplateFormat.BASICRegistryTemplate)
                {
                    var template = TemplateInit(registryDefault);
                    readerSGF.CreateTemplate(rawImage, template);
                    return template;
                }

                if (registryDefault == TemplateFormat.ANSITemplate)
                {
                    Byte[] template1 = TemplateInit(registryDefault);
                    ELog("" + template1.Length);
                    readerSGF.CreateTemplate(null, rawImage, template1);

                    var matched = false;
                    var match = readerSGF.MatchTemplate(template1, template1, SGFPMSecurityLevel.NORMAL, ref matched);
                    if (match != (Int32)SGFPMError.ERROR_NONE)
                    {
                        throw new ReaderException(ReaderError.ERROR_IN_MATCHING);
                    }
                    if (matched)
                    {
                        var buf_size = 0;

                        readerSGF.GetTemplateSizeAfterMerge(template1, template1, ref buf_size);
                        var merged_template = new Byte[buf_size];

                        readerSGF.MergeAnsiTemplate(template1, template1, merged_template);
                        return merged_template;
                    }
                    else
                    {
                        throw new ReaderException(ReaderError.ERROR_IN_MATCHING);
                    }
                }
            }
#endif
#if DIGITALPERSONA
            if (readerBrand == ReaderFactory.ReaderBrand.DIGITAL_PERSONA)
            {
                var resultConversion = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.DP_PRE_REGISTRATION);

                if (resultConversion.ResultCode == Constants.ResultCode.DP_SUCCESS)
                {
                    var gallery = new List<Fmd>();
                    for (var i = 0; i < 4; i++)
                    {
                        gallery.Add(resultConversion.Data);
                    }
                    var fmdEnroll = Enrollment.CreateEnrollmentFmd(Constants.Formats.Fmd.DP_REGISTRATION, gallery);
                    return fmdEnroll.Data.Bytes;
                }
            }
#endif
            return null;
        }

        /// <summary>
        /// Get Bitmap image
        /// </summary>
        /// <returns></returns>
        public static Image GetBitmapImage()
        {
            return ImageProcessor.RawToBitmap(rawImage, ImageWidth, ImageHeight);
        }

        /// <summary>
        /// Get Sampled base 54 raw image
        /// </summary>
        /// <returns></returns>
        public static string GetSampleJPG()
        {
            return String.Format("{0}~{1}~{2}", ImageWidth, ImageHeight, Convert.ToBase64String(rawImage));
        }

        public static string GetWSQImage64()
        {
            var raw512x512 = ImageProcessor.FillRaw(rawImage, ImageWidth, ImageHeight, 512, 512);
            var encoder = new WsqEncoder();
            byte[] wsqCaptured;

            if (encoder.EncodeMemory(raw512x512, 512, 512, out wsqCaptured))
            {
                return Convert.ToBase64String(wsqCaptured);
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get default Verify template
        /// </summary>
        /// <returns></returns>
        public static string GetVerifyTemplate64()
        {
            return Convert.ToBase64String(GetVerifyTemplate());
        }

        /// <summary>
        /// Get default registry template base 64
        /// </summary>
        /// <returns></returns>
        public static string GetRegistryTemplate64()
        {
            return Convert.ToBase64String(GetRegistryTemplate());
        }

        public static string GetANSITemplate64()
        {
            return Convert.ToBase64String(GetANSITemplate());
        }

        public static string GetISOTemplate64()
        {
            return Convert.ToBase64String(GetISOTemplate());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="fingerID"></param>
        /// <returns></returns>
        public static FingerCaptured GetFingerCaptured(int fingerID)
        {
            var fingerCaptured = new FingerCaptured()
            {
                Finger = fingerID,
                CaptureResultStatus = CaptureResultStatus,
                CaptureQuality = CaptureQuality
            };

            try
            {
                LOG.Debug(" FingerCaptureProcessor IN...");
                if (CaptureQuality == ReaderFactory.CaptureQuality.QUALITY_GOOD)
                {
                    fingerCaptured.Guid = Guid.NewGuid();

                    fingerCaptured.ReaderImage = new ReaderImage() { Image = rawImage, Height = ImageHeight, Width = ImageWidth };

                    fingerCaptured.Image = GetBitmapImage();

                    fingerCaptured.WSQ64 = GetWSQImage64();
                    LOG.Debug(" FingerCaptureProcessor fingerCaptured.WSQ64 = " + fingerCaptured.WSQ64);
                    fingerCaptured.TemplateISO64 = GetISOTemplate64();
                    LOG.Debug(" FingerCaptureProcessor fingerCaptured.TemplateISO64 = " + fingerCaptured.TemplateISO64);
                    fingerCaptured.TemplateANSI64 = GetANSITemplate64();
                    LOG.Debug(" FingerCaptureProcessor fingerCaptured.TemplateANSI64 = " + fingerCaptured.TemplateANSI64);
                    fingerCaptured.TemplateRegistry64 = GetRegistryTemplate64();
                    LOG.Debug(" FingerCaptureProcessor fingerCaptured.TemplateRegistry64 = " + fingerCaptured.TemplateRegistry64);
                    fingerCaptured.TemplateVerify64 = GetVerifyTemplate64();
                    LOG.Debug(" FingerCaptureProcessor fingerCaptured.TemplateISO64 = " + fingerCaptured.TemplateISO64);

                    fingerCaptured.TemplateISO = GetISOTemplate();
                    fingerCaptured.TemplateANSI = GetANSITemplate();
                    fingerCaptured.TemplateRegistry = GetRegistryTemplate();
                    fingerCaptured.TemplateVerify = GetVerifyTemplate();
                }
            }
            catch (Exception e)
            {
                ELog(string.Format(" FingerCaptureProcessor -> GetFingerCaptured", e.Message));
                LOG.Error(" FingerCaptureProcessor -> GetFingerCaptured => " + e.Message);
            }

            return fingerCaptured;
        }

        public static ReaderFactory.CaptureResultStatus CaptureResultStatus { get; set; }

        private static string DumpBytes(Byte[] byteArray)
        {
            return System.Text.Encoding.UTF8.GetString(byteArray);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="p"></param>
        private static void ELog(string p)
        {
            new EventLog() { Source = "FingerReader" }.WriteEntry(p, EventLogEntryType.Information);
        }
    }
}