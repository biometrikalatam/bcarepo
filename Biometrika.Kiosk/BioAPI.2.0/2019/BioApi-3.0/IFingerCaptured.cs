﻿using System;
using System.Drawing;

namespace BioAPI
{
    public interface IFingerCaptured : ICloneable
    {
        Guid Guid { get; set;  }

        ReaderImage ReaderImage { get; set; }

        ReaderFactory.CaptureResultStatus CaptureResultStatus { get; set; }

        ReaderFactory.CaptureQuality CaptureQuality { get; set; }

        Image Image { get; set; }

        Byte[] TemplateANSI { get; set; }

        Byte[] TemplateISO { get; set; }

        Byte[] TemplateRegistry { get; set; }

        Byte[] TemplateVerify { get; set; }

        string TemplateANSI64 { get; set; }

        string TemplateISO64 { get; set; }

        string TemplateRegistry64 { get; set; }

        string TemplateVerify64 { get; set; }

        string WSQ64 { get; set; }

        int Finger { get; set; }
    }
}