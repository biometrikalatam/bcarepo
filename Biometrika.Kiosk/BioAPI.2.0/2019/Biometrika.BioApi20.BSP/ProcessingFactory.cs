﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bio.Core.Matcher.Constant;
using Bio.Core.Wsq.Encoder;
using Biometrika.BioApi20.Interfaces;
using log4net;

namespace Biometrika.BioApi20.BSP
{
    public class ProcessingFactory : IProcessingFactory
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ProcessingFactory));

        public ProcessingFactorySchema Schema { get; set; }
        public Hashtable HT_MATCHER { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public event ErrorCallbackDelegate OnErrorEvent;
        public event TimeoutCallbackDelegate OnTimeoutEvent;
        public event TaskDoneCallbackDelegate OnTaskDoneEvent;

        public int Close()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sample"></param>
        /// <param name="typeImage">0-JPG desde RAW sin Completar | 1-JPG desde RAW 512x512</param>
        /// <param name="sampleout"></param>
        /// <returns></returns>
        public int GetImage(Sample sample, int typeImage, out Sample sampleout, out Error error)
        {
            int ret = 0;
            sampleout = null;
            error = new Error(0, null);
            byte[] rawToImage;
            LOG.Debug("ProcessingFactory.GetImage IN...");
            try
            {
                LOG.Debug("ProcessingFactory.GetImage sample != null => " + (sample != null).ToString());
                if (sample == null || sample.Data == null || 
                    sample.MinutiaeType != Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW ||
                    sample.MinutiaeType != Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                {
                    LOG.Debug("ProcessingFactory.GetImage sample.Data != null => " + (sample.Data != null).ToString());
                    LOG.Debug("ProcessingFactory.GetImage sample.MinutiaeType = " + sample.MinutiaeType.ToString());
                    ret = Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA;
                    LOG.Error("ProcessingFactory.GetImage Error - Sample o Data == null");
                    sampleout = null;
                    error = new Error(Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA, "ProcessingFactory.GetImage Error - Sample o Data == null");
                    return ret;
                }

                //Si es WSQ => Descomprimo para usear el RAW
                short sw, sh;
                int w, h;
                if (sample.MinutiaeType == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                {
                    LOG.Error("ProcessingFactory.GetImage Decomprime WSQ...");
                    Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                    if (!dec.DecodeMemory((byte[])sample.Data, out sw, out sh, out rawToImage)) {
                        ret = Bio.Core.Constant.Errors.IERR_WSQ_DECOMPRESSING;
                        LOG.Error("ProcessingFactory.GetImage Error - Descomprimiendo WSQ");
                        sampleout = null;
                        error = new Error(Bio.Core.Constant.Errors.IERR_WSQ_DECOMPRESSING, "ProcessingFactory.GetImage Error - Descomprimiendo WSQ");
                        return ret;
                    } else
                    {
                        w = (int)sw;
                        h = (int)sh;
                        LOG.Error("ProcessingFactory.GetImage RAW Obtenido desde WSQ size => w x h = " + w.ToString() + " x " + h.ToString());
                    }
                } else
                {
                    rawToImage = (byte[])sample.Data;
                    w = sample.SampleWith;
                    h = sample.SampleHeight;
                    LOG.Error("ProcessingFactory.GetImage RAW Recibido size => w x h = " + w.ToString() + " x " + h.ToString());
                }

                LOG.Debug("ProcessingFactory.GetImage typeImage = " + typeImage + " [" + (typeImage==0?"Sin Completar":"Completa 512x512") + "]");
                 if (typeImage == 1)
                {
                    rawToImage = Bio.Core.Imaging.ImageProcessor.FillRaw((byte[])sample.Data, sample.SampleWith, sample.SampleHeight, 512, 512);
                    w = 512;
                    h = 512;
                } 

                Bitmap bmpImage = Bio.Core.Imaging.ImageProcessor.RawToBitmap(rawToImage, w, h);
                byte[] bmpByte;
                if (bmpImage != null)
                {
                    System.Drawing.Image img = (System.Drawing.Image)(((System.Drawing.Image)bmpImage).Clone());
                    using (var ms = new MemoryStream())
                    {
                        img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        bmpByte = ms.ToArray();
                    }
                    sampleout = new Sample(sample.AuthenticationFactor, MinutiaeType.MINUTIAETYPE_JPG, sample.BodyPart, (int)Biometrika.Standards.Purpose.Verify, w, h, bmpByte);
                }
                else
                {
                    LOG.Debug("ProcessingFactory.GetImage Error ImageProcessor.RawToBitmap generating JPG");
                    ret = Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA;
                    LOG.Error("ProcessingFactory.GetImage Error ImageProcessor.RawToBitmap generating JPG"); 
                    sampleout = null;
                    error = new Error(Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA,
                        "ProcessingFactory.GetImage Error ImageProcessor.RawToBitmap generating JPG");
                    return ret;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ProcessingFactory.GetImage Error " + ex.Message);
                sampleout = null;
                error = new Error(-1, "ProcessingFactory.GetImage Error = " + ex.Message);
            }
            LOG.Debug("ProcessingFactory.GetImage OUT!");
            return ret;
        }

        public int GetImage(Sample sample, int typeImage, int typeReturn, out Sample sampleout, out Error error)
        {
            throw new NotImplementedException();
        }

        public int GetMinutiae(Sample sample, MinutiaeType mtout, out Sample sampleout, out Error error)
        {
            throw new NotImplementedException();
        }

        public int GetMinutiae(Sample sample, MinutiaeType mtout, int purpose, out Sample sampleout, out Error error)
        {
            throw new NotImplementedException();
        }

        public int GetWSQ(Sample sample, out Sample sampleout, out Error error)
        {
            int ret = 0;
            sampleout = null;
            error = new Error(0, null);
            byte[] rawToWSQ;
            LOG.Debug("ProcessingFactory.GetImage IN...");
            try
            {
                LOG.Debug("ProcessingFactory.GetImage sample != null => " + (sample != null).ToString());
                if (sample == null || sample.Data == null || sample.MinutiaeType != Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW)
                {
                    LOG.Debug("ProcessingFactory.GetImage sample.Data != null => " + (sample.Data != null).ToString());
                    LOG.Debug("ProcessingFactory.GetImage sample.MinutiaeType = " + sample.MinutiaeType.ToString());
                    ret = Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA;
                    LOG.Error("ProcessingFactory.GetImage Error - Sample o Data == null");
                    sampleout = null;
                    error = new Error(Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA, "ProcessingFactory.GetImage Error - Sample o Data == null");
                    return ret;
                }

                LOG.Debug("ProcessingFactory.GetImage RAW Recibido size => w x h = " + 
                          sample.SampleWith.ToString() + " x " + sample.SampleHeight.ToString());
                if (sample.SampleWith < 512 || sample.SampleHeight < 512)
                {
                    rawToWSQ = Bio.Core.Imaging.ImageProcessor.FillRaw((byte[])sample.Data, sample.SampleWith, sample.SampleHeight, 512, 512);
                }
                else
                {
                    rawToWSQ = (byte[])sample.Data;
                }
                LOG.Debug("ProcessingFactory.GetImage RAWtoWSQ Length = " + rawToWSQ.Length.ToString());

                var encoder = new WsqEncoder();
                byte[] wsqCaptured;
                LOG.Debug("ProcessingFactory.GetImage Encoding WSQ...");
                if (encoder.EncodeMemory(rawToWSQ, 512, 512, out wsqCaptured))
                {
                    sampleout = new Sample(sample.AuthenticationFactor, MinutiaeType.MINUTIAETYPE_JPG, sample.BodyPart, (int)Biometrika.Standards.Purpose.Verify, 512, 512, wsqCaptured);
                    LOG.Debug("ProcessingFactory.GetImage WSQ GEnerado!");
                }
                else
                {
                    LOG.Debug("ProcessingFactory.GetImage Error encoder.EncodeMemory generating WSQ");
                    ret = Bio.Core.Constant.Errors.IERR_WSQ_COMPRESSING;
                    LOG.Error("ProcessingFactory.GetImage Error encoder.EncodeMemory generating WSQ");
                    sampleout = null;
                    error = new Error(Bio.Core.Constant.Errors.IERR_WSQ_COMPRESSING, "ProcessingFactory.GetImage Error encoder.EncodeMemory generating WSQ");
                    return ret;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ProcessingFactory.GetImage Error " + ex.Message);
                sampleout = null;
                error = new Error(-1, "ProcessingFactory.GetImage Error = " + ex.Message);
            }
            LOG.Debug("ProcessingFactory.GetImage OUT!");
            return ret;
        }

        public int GetWSQ(Sample sample, bool complete512, out Sample sampleout, out Error error)
        {
            throw new NotImplementedException();
        }

        public int Initialization()
        {
            throw new NotImplementedException();
        }
    }
}
