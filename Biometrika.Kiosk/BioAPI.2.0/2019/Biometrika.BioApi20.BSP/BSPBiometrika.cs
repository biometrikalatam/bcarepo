﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Biometrika.BioApi20.Interfaces;
using Biometrika.BioApi20;
using log4net.Config;
using System.IO;
using Biometrika.Standards;
using System.Collections;
using log4net;

namespace Biometrika.BioApi20.BSP
{
    public class BSPBiometrika : IBSP
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BSPBiometrika));

        public bool IsLoaded { get; set; }
        //Configuraciones en XML para inicio
        public BSPSchema Schema { get; set; }
        public ISensorFactory SENSOR_FACTORY { get; set; }
        public IProcessingFactory PROCESSING_FACTORY { get; set; }

        public event CaptureCallbackDelegate OnCapturedEvent;
        public event ErrorCallbackDelegate OnErrorEvent;
        public event ConnectCallbackDelegate OnConnectEvent;
        public event DisconnectCallbackDelegate OnDisconnectEvent;
        public event TimeoutCallbackDelegate OnTimeoutEvent;

        //Crea objetos, inicializa y carga los BFP si bspload = true
        public void BSPAttach(string bioAPIVersion, bool bspLoad)
        {
            try
            {
                XmlConfigurator.Configure(new FileInfo("logger.cfg"));

                if (bspLoad) BSPLoad();
            }
            catch (Exception ex)
            {
                LOG.Error("BSPBiometrika.BSPAttach Error = " + ex.Message);
            }
        }

        public void BSPDetach()
        {
            BSPUnload();
        }

        public void BSPLoad() // BFPEventCallback bioAPINotifyCallback, BFPEnumerationCallback bfpEnumeration)
        {
            try
            {
                SENSOR_FACTORY = new SensorFactory();
                int ret = SENSOR_FACTORY.Initialization();
                IsLoaded = (ret == 0);
            }
            catch (Exception ex)
            {
                LOG.Error("BSPBiometrika.BSPLoad Error = " + ex.Message);
            }
        }

        public void BSPUnload()
        {
            try
            {
                SENSOR_FACTORY.Close();
                SENSOR_FACTORY = null;
            }
            catch (Exception ex)
            {
                SENSOR_FACTORY = null;
            }
        }

        public List<Sample> Capture(int sensorType, int bodypart, string sensorSerial, int timeout, int captureType, int qualityCapture, out Error error)
        {
            if (SENSOR_FACTORY == null)
            {
                error = new Error(-4, "SENSOR_FACTORY No inicializado. Re inicialize el BSP!");
                return null;
            }
            SENSOR_FACTORY.OnCapturedEvent += OnCaptureEventMethod;
            SENSOR_FACTORY.OnTimeoutEvent += OnTimeoutEventMethod;
            List<Sample> sampleout = SENSOR_FACTORY.Capture(sensorType, bodypart, sensorSerial, timeout, captureType, qualityCapture, out error);
            SENSOR_FACTORY.OnCapturedEvent -= OnCaptureEventMethod;
            SENSOR_FACTORY.OnTimeoutEvent -= OnTimeoutEventMethod;
            return sampleout;
        }

        private void OnTimeoutEventMethod(int errCode, string errMessage, ISensor sensor)
        {
            OnTimeoutEvent(errCode, errMessage, sensor);
        }

        private void OnCaptureEventMethod(int errCode, string errMessage, List<Sample> samplesCaptured)
        {
            OnCapturedEvent(errCode, errMessage, samplesCaptured);
        }

        List<Sample> IBSP.Identify(Sample sample, List<Sample> listSamples, int AuthenticationFactor, int MinutiaeType, int typeIdentify, double thershold, out double score, out Error error)
        {
            throw new NotImplementedException();
        }

        public List<BFPListElement> QueryBFPs()
        {
            BFPListElement item;
            List<BFPListElement> listBFP = new List<BFPListElement>();
            try
            {
                if (SENSOR_FACTORY == null) return null;

                List<string> listSerial = new List<string>();
                foreach (DictionaryEntry sitem in SENSOR_FACTORY.htSensor)
                {
                    item = new BFPListElement(((ISensor)sitem.Value).AuthenticationFactor, 0, Standards.UnitCategoryType.Sensor,
                                              ((ISensor)sitem.Value).Vendor, "",
                                              ((ISensor)sitem.Value).ImageWidth,
                                              ((ISensor)sitem.Value).ImageHeight,
                                              (string)sitem.Key);
                    listBFP.Add(item);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BSPBiometrika.QueryBFPs Error = " + ex.Message);
            }
            return listBFP;
        }

        public List<BFPListElement> QueryBFPs(List<Biometrika.Standards.UnitCategoryType> unitCategories)
        {
            BFPListElement item;
            List<BFPListElement> listBFP = new List<BFPListElement>();
            try
            {
                if (SENSOR_FACTORY == null) return null;

                if (IncludeUnitCategoryType(Standards.UnitCategoryType.Sensor, unitCategories))
                {
                    List<string> listSerial = new List<string>();
                    foreach (DictionaryEntry sitem in SENSOR_FACTORY.htSensor)
                    {
                        item = new BFPListElement(((ISensor)sitem.Value).AuthenticationFactor, 0, Standards.UnitCategoryType.Sensor,
                                                  ((ISensor)sitem.Value).Vendor, "",
                                                  ((ISensor)sitem.Value).ImageWidth,
                                                  ((ISensor)sitem.Value).ImageHeight, 
                                                  (string)sitem.Key);
                        listBFP.Add(item);
                    }
                    //foreach (ISensor sitem in SENSOR_FACTORY.listSensor)
                    //{
                    //    item = new BFPListElement(sitem.AuthenticationFactor, 0, Standards.UnitCategoryType.Sensor, sitem.Vendor,
                    //                              "", sitem.ImageWidth, sitem.ImageHeight, sitem.SerialSensor);
                    //    listBFP.Add(item);
                    //}
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BSPBiometrika.QueryBFPs Error = " + ex.Message);
            }
            return listBFP;
        }

        private bool IncludeUnitCategoryType(UnitCategoryType unittype, List<UnitCategoryType> unitCategories)
        {
            bool ret = false;
            try
            {
                foreach (UnitCategoryType item in unitCategories)
                {
                    if (item.Equals(unittype)) return true;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BSPBiometrika.IncludeUnitCategoryType Error = " + ex.Message);
                ret = false;
            }
            return ret;
        }

        //void IBSP.SubscribeToGUIEvents(GUISelectEventCallback guiSelectEventCallback, GUIStateEventCallback guiStateEventCallback, GUIProgressEventCallback guiProgressEventCallback)
        //{
        //    throw new NotImplementedException();
        //}

        //void IBSP.UnsubscribeFromGUIEvents()
        //{
        //    throw new NotImplementedException();
        //}

        bool IBSP.Verify(List<Sample> listSamples, int AuthenticationFactor, int MinutiaeType, double thershold, out double score, out Error error)
        {
            score = 0;
            error = null;
            return false;
        }

        public int EnableAutoEvent(string sensorSerial, bool enable, int hwnd, out Error error)
        {
            if (SENSOR_FACTORY == null)
            {
                error = new Error(-4, "SENSOR_FACTORY No inicializado. Re inicialize el BSP!");
                return -4;
            }
            //SENSOR_FACTORY.OnCapturedEvent += OnCaptureEventMethod;
            //SENSOR_FACTORY.OnTimeoutEvent += OnTimeoutEventMethod;
            return SENSOR_FACTORY.EnableAutoEvent(sensorSerial, enable, hwnd, out error);
            //SENSOR_FACTORY.OnCapturedEvent -= OnCaptureEventMethod;
            //SENSOR_FACTORY.OnTimeoutEvent -= OnTimeoutEventMethod;
            //return sampleout;
        }

        public int CaptureContinue(string sensorSerial, int qualityCapture, out Error error)
        {
            if (SENSOR_FACTORY == null)
            {
                error = new Error(-4, "SENSOR_FACTORY No inicializado. Re inicialize el BSP!");
                return -4;
            }
            SENSOR_FACTORY.OnCapturedEvent += OnCaptureEventMethod;
            //SENSOR_FACTORY.OnTimeoutEvent += OnTimeoutEventMethod;
            int ret = SENSOR_FACTORY.CaptureContinue(sensorSerial, qualityCapture, out error);
            LOG.Error("BSPBiometrika.CaptureContinue -  SENSOR_FACTORY.CaptureContinue ret = " + ret.ToString() +
                       " | error=" + ((error!=null)?error.ErrorCode+"-"+error.ErrorDescription:"Null"));
            SENSOR_FACTORY.OnCapturedEvent -= OnCaptureEventMethod;
            //SENSOR_FACTORY.OnTimeoutEvent -= OnTimeoutEventMethod;
            return ret;
        }

        public void On(string sensorSerial)
        {
            if (SENSOR_FACTORY == null)
            {
                //error = new Error(-4, "SENSOR_FACTORY No inicializado. Re inicialize el BSP!");
                return; // -4;
            }
            SENSOR_FACTORY.On(sensorSerial);

            return; 
        }

        public void Off(string sensorSerial)
        {
            if (SENSOR_FACTORY == null)
            {
                //error = new Error(-4, "SENSOR_FACTORY No inicializado. Re inicialize el BSP!");
                return; // -4;
            }
            SENSOR_FACTORY.Off(sensorSerial);

            return;
        }
    }
}
