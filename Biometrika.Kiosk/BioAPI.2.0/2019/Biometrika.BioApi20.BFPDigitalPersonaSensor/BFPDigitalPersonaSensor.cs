﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Bio.Core.Matcher.Constant;
using Biometrika.BioApi20.Interfaces;
using Biometrika.Standards;
using DPUruNet;
using log4net;
using static DPUruNet.Reader;

namespace Biometrika.BioApi20.BFPDigitalPersonaSensor
{
    public class BFPDigitalPersonaSensor: ISensor
    {
        Timer _TIMER_FOR_EVENT = new Timer(200);
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BFPDigitalPersonaSensor));

        //private DPUruNet.Reader _readerDP;
        
        //Para usar en el evento de captura pasado desde el llamado a Capture.
        int _CaptureType = 0;
        int _BodyPart = 1;
        public event CaptureCallbackDelegate _OnCapturedEvent;
        public event TimeoutCallbackDelegate _OnTimeoutEvent;

        public event CaptureCallbackDelegate OnCapturedEvent;
        public event ErrorCallbackDelegate OnErrorEvent;
        public event ConnectCallbackDelegate OnConnectEvent;
        public event DisconnectCallbackDelegate OnDisconnectEvent;
        public event TimeoutCallbackDelegate OnTimeoutEvent;

        public string Vendor { get; private set; }

        public int AuthenticationFactor { get; private set; }

        public object CurrentSensor { get; private set; }

        public string SerialSensor { get; private set; }

        public UnitIndicatorStatus IndicatorStatus { get; private set; }

        public int ImageWidth { get; private set; }

        public int ImageHeight { get; private set; }

        public string DeviceID { get; private set; }

        public int SensorType { get; private set; }

        public void Calibrate(int timeout)
        {
            
        }

        public List<Sample> Capture(int sensorType, int bodypart, string sensorSerial, int timeout, int captureType,
                                    double qualityCapture, out Error error)
        {
            _ListSampleCaptured = null;
            error = new Error(0, null);
            LOG.Debug("BFPSecugenSensor.Capture IN...");
            _CaptureType = captureType;
            _BodyPart = bodypart;
            try
            {
                //Chequeo que este abierto sensor sino abro
                CurrentSensor = null;
                if (CurrentSensor == null)
                {
                    if (InstanceSensor(sensorSerial) < 0)
                    {
                        error.ErrorCode = -3;
                        error.ErrorDescription = "BFPDigitalPErsonaSensor Error [SerialSensor=" + sensorSerial +
                                                " - Error = No pudo instanciar Sensor]";
                        return null;
                    }
                }

                var resultCode = ((Reader)CurrentSensor).Open(Constants.CapturePriority.DP_PRIORITY_COOPERATIVE);
                if (resultCode != Constants.ResultCode.DP_SUCCESS)
                {
                    resultCode = ((Reader)CurrentSensor).Open(Constants.CapturePriority.DP_PRIORITY_EXCLUSIVE);
                }
                if (resultCode == Constants.ResultCode.DP_SUCCESS)
                {
                    ((Reader)CurrentSensor).On_Captured += new DPUruNet.Reader.CaptureCallback(OnCaptureMethodDP);
                    SetTimer(timeout);
                    ((Reader)CurrentSensor).GetStatus();


                    resultCode = ((Reader)CurrentSensor).CaptureAsync(
                                           DPUruNet.Constants.Formats.Fid.ANSI,
                                           DPUruNet.Constants.CaptureProcessing.DP_IMG_PROC_DEFAULT,
                                           ((Reader)CurrentSensor).Capabilities.Resolutions[0]
                                           );

                    _OnCapturedEvent = OnCapturedEvent;
                    _OnTimeoutEvent = OnTimeoutEvent;
                    if (resultCode != Constants.ResultCode.DP_SUCCESS)
                    {
                        error.ErrorCode = -1;
                        error.ErrorDescription = "BFPSecugenSensor.Capture Error [SerialSensor=" + sensorSerial +
                                                " - Error = resultCode = DEVICE_FAILURE]";
                        OnErrorEvent(error.ErrorCode, error.ErrorDescription, null);
                        //((Reader)CurrentSensor).On_Captured -= new DPUruNet.Reader.CaptureCallback(OnCaptureMethodDP);
                    }
                }
                else
                {
                    //ReaderStatus.ReaderResultCode = ReaderFactory.ReaderResultCode.DEVICE_FAILURE;
                }
            }
            catch (Exception ex)
            {
                error.ErrorCode = -1;
                error.ErrorDescription = "BFPSecugenSensor.Capture Error [SerialSensor=" + sensorSerial +
                                                " - Error = " + ex.Message + "]";
                OnErrorEvent(error.ErrorCode, error.ErrorDescription, null);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="captureResult"></param>
        public void OnCaptureMethodDP(CaptureResult captureResult)
        {
            List<Sample> listSampleCaptured = new List<Sample>();
            Sample sample;
            Error error = new Error(0,null);


            _TIMER_FOR_EVENT.Enabled = false;
            var fiv = captureResult.Data.Views[0];
            byte[] rawImage = fiv.RawImage;

            ImageWidth = fiv.Width;
            ImageHeight = fiv.Height;
            
            if (captureResult.Quality != Constants.CaptureQuality.DP_QUALITY_GOOD)
            {
                error.ErrorCode = Biometrika.BioApi20.Error.IERR_BAD_QUALITY;
                error.ErrorDescription = "BFPDigitalPersonaSensor.Capture Error [SerialSensor=" + SerialSensor +
                                                " - Error = Imagen baja calidad]";
                OnErrorEvent(error.ErrorCode, error.ErrorDescription, null);
            } else { 

           
                LOG.Debug("BFPDigitalPersonaSensor.Capture Capture OnCaptureMethodDP ok...");

                byte[] birArray = rawImage;
                ImageWidth = fiv.Width;
                ImageHeight = fiv.Height;

                //Armo lista de samples de salida de acuerdo a captureType
                //      0 - All | 1 - Solo WSQ | 2 - Solo Minucias | 3 - Solo Image
                if (_CaptureType < 0 || _CaptureType > 4)
                {
                    _CaptureType = 0; //All
                }
                //      0 - All | 1 - Solo WSQ
                byte[] raw512x512 = null;
                if (_CaptureType == 0 || _CaptureType == 1)
                {
                    raw512x512 = Bio.Core.Imaging.ImageProcessor.FillRaw(rawImage, ImageWidth, ImageHeight, 512, 512);
                    Bio.Core.Wsq.Encoder.WsqEncoder enc = new Bio.Core.Wsq.Encoder.WsqEncoder();
                    sample = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                                                Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW,
                                                _BodyPart, (int)Biometrika.Standards.Purpose.Verify, 512, 512, raw512x512);
                    listSampleCaptured.Add(sample);
                    byte[] wsq;
                    if (enc.EncodeMemory(raw512x512, 512, 512, out wsq))
                    {
                        sample = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                                                Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ,
                                                _BodyPart, (int)Biometrika.Standards.Purpose.Verify, 512, 512, wsq);
                        listSampleCaptured.Add(sample);
                    }
           
                }

                //      0 - All  | 2 - Solo Minucias
                if (_CaptureType == 0 || _CaptureType == 2)
                {
                    Error terror;
                    List<Sample> sout;
                    //Sample sin = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                    //                            Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW,
                    //                            _BodyPart, (int)Biometrika.Standards.Purpose.Verify, ImageWidth, ImageHeight, rawImage);
                    if (Extract(captureResult, Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004, out sout, out terror) == 0)
                    {
                        foreach (Sample item in sout)
                        {
                            listSampleCaptured.Add(item);
                        }
                    }
                    else
                    {
                        LOG.Debug("BFPDigitalPersonaSensor.Capture Error generando Minucias ANSI [SerialSensor=" + SerialSensor +
                                                " - Error = " +
                            (terror != null ? terror.ErrorCode + "-" + terror.ErrorDescription : "Error null") + "]");
                    }

                    sout = null;
                    terror = null;
                    if (Extract(captureResult, Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005, out sout, out terror) == 0)
                    {
                        foreach (Sample item in sout)
                        {
                            listSampleCaptured.Add(item);
                        }
                    }
                    else
                    {
                        LOG.Debug("BFPDigitalPersonaSensor.Capture Error generando Minucias ISO [SerialSensor=" + SerialSensor +
                                                " - Error = " +
                            (terror != null ? terror.ErrorCode + "-" + terror.ErrorDescription : "Error null") + "]");
                    }

                    sout = null;
                    terror = null;
                    if (Extract(captureResult, Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONA, out sout, out terror) == 0)
                    {
                        foreach (Sample item in sout)
                        {
                            listSampleCaptured.Add(item);
                        }
                    }
                    else
                    {
                        LOG.Debug("BFPDigitalPersonaSensor.Capture Error generando Minucias DP [SerialSensor=" + SerialSensor +
                                                " - Error = " +
                            (terror != null ? terror.ErrorCode + "-" + terror.ErrorDescription : "Error null") + "]");
                    }
                }

                //      0 - All | 3 - Solo Image
                if (_CaptureType == 0 || _CaptureType == 3)
                {
                    sample = new Sample(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                                                Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW,
                                                _BodyPart, (int)Biometrika.Standards.Purpose.Verify, ImageWidth, ImageHeight, rawImage);
                    Sample sout;
                    Error terr;
                    //Toma imagen sin completar 512x512
                    if (GetImage(sample, 0, out sout, out terr) == 0)
                    {
                        listSampleCaptured.Add(sout);
                    }
                    sout = null;
                    terr = null;
                    //Toma imagen y completa 512x512 si hace falta
                    if (GetImage(sample, 1, out sout, out terr) == 0)
                    {
                        listSampleCaptured.Add(sout);
                    }
                    
                    
                    //BIR bir = new BIR(null, 0, 0, outputFormat, false, false, BiometricType.Finger, subtype, null, null, null,
                    //     ProcessedLevel.Raw, null, 0, 0, null, null, null, null, null, null, null, birArray, null);

                    //return bir;
                }
                //OnCapturedEvent(0, null, _ListSampleCaptured);

                //_OnCapturedEvent(0, "Capture OK!", listSampleCaptured);
                OnCapturedEventMethod(0, "Capture OK!", listSampleCaptured);
            }
        }

        private void SetTimer(int timeout)
        {
            // Create a timer with a two second interval.
            _TIMER_FOR_EVENT = new System.Timers.Timer(timeout);
            // Hook up the Elapsed event for the timer. 
            //_TIMER_FOR_EVENT.Interval = timeout;
            _TIMER_FOR_EVENT.Elapsed += OnTimedEvent;
            _TIMER_FOR_EVENT.AutoReset = true;
            _TIMER_FOR_EVENT.Enabled = true;
        }

        public List<Sample> _ListSampleCaptured;
        public void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            _TIMER_FOR_EVENT.Enabled = false;
            //OnCapturedEvent(0, null, _ListSampleCaptured);
            try
            {
                if (((Reader)CurrentSensor) != null)
                {
                    ((Reader)CurrentSensor).CancelCapture();
                    ((Reader)CurrentSensor).Dispose();
                }
            }
            catch (Exception ex)
            {

            }
            _OnTimeoutEvent(Error.IERR_TIMEOUT, "Timeout de la operacion", null);
        }

        public void OnCapturedEventMethod(int err, string errdesc, List<Sample> listSampleCaptured)
        {
            //_ListSampleCaptured = listSampleCaptured;
            //if (OnCapturedEvent != null)
            //    OnCapturedEvent(err, errdesc, listSampleCaptured);

            try
            {
                if (((Reader)CurrentSensor) != null)
                {
                    ((Reader)CurrentSensor).CancelCapture();
                    ((Reader)CurrentSensor).Dispose();
                }
            }
            catch (Exception ex)
            {

            }

            _OnCapturedEvent?.Invoke(err, errdesc, listSampleCaptured);
            //_TIMER_FOR_EVENT.Enabled = true;
        }


        /// <summary>
        /// Dado un RAW devuelto por el sensor, extrae minucias de los tipos que soporta
        /// </summary>
        /// <param name="sampleIn"></param>
        /// <param name="minutiaeTypeOut"></param>
        /// <param name="sampleOut"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public int Extract(object captureResultP, int minutiaeTypeOut, out List<Sample> sampleOut, out Error error)
        {
            int ret = 0;
            sampleOut = null;
            error = new Error(0, null);
            byte[] rawToImage;

            try
            {

                CaptureResult captureResult = (CaptureResult)captureResultP;
                //Chequeo que este abierto sensor sino abro
                if (CurrentSensor == null)
                {
                    if (InstanceSensor(null) < 0)
                    {
                        error.ErrorCode = -3;
                        error.ErrorDescription = "BFPDigitalPersonaSensor.Extract Error [No pudo instanciar Sensor]";
                        return -3;
                    }
                }

                 int ierror;

                byte[] templateGenerated;
                if (minutiaeTypeOut == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
                {
                    var fmdANSI = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.ANSI);
                    if (fmdANSI != null && fmdANSI.ResultCode == Constants.ResultCode.DP_SUCCESS)
                    {
                        LOG.Debug("BFPDigitalPersonaSensor.Extract From DP ANSITEmplate = " + Convert.ToBase64String(fmdANSI.Data.Bytes));
                        templateGenerated = fmdANSI.Data.Bytes;
                        sampleOut = new List<Sample>();
                        sampleOut.Add(new Sample(2, minutiaeTypeOut, _BodyPart, (int)Biometrika.Standards.Purpose.Verify, 0, 0, templateGenerated));
                    } else
                    {
                        LOG.Error("BFPDigitalPersonaSensor.Extract From DP ANSITEmplate");
                        error = new Error(Bio.Core.Constant.Errors.IERR_EXTRACTING, "BFPDigitalPersonaSensor.Extract Error - Extrayendo ANSI");
                    }
                    //return fmdANSI.Data.Bytes;
                    //ierror = ((SGFingerPrintManager)CurrentSensor).SetTemplateFormat(SGFPMTemplateFormat.ANSI378);
                }
                else
                {
                    if (minutiaeTypeOut == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
                    {
                        var fmdISO = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.ISO);
                        LOG.Debug("BFPDigitalPersonaSensor.Extract From DP ISOTEmplate = " + Convert.ToBase64String(fmdISO.Data.Bytes));
                        if (fmdISO != null && fmdISO.ResultCode == Constants.ResultCode.DP_SUCCESS)
                        {
                            LOG.Debug("BFPDigitalPersonaSensor.Extract From DP ISOTEmplate = " + Convert.ToBase64String(fmdISO.Data.Bytes));
                            templateGenerated = fmdISO.Data.Bytes;
                            sampleOut = new List<Sample>();
                            sampleOut.Add(new Sample(2, minutiaeTypeOut, _BodyPart, (int)Biometrika.Standards.Purpose.Verify, 0, 0, templateGenerated));
                        }
                        else
                        {
                            LOG.Error("BFPDigitalPersonaSensor.Extract From DP ISOTEmplate");
                            error = new Error(Bio.Core.Constant.Errors.IERR_EXTRACTING, "BFPDigitalPersonaSensor.Extract Error - Extrayendo ISO");
                        }
                        //ierror = ((SGFingerPrintManager)CurrentSensor).SetTemplateFormat(SGFPMTemplateFormat.ISO19794);
                    }
                    else
                    {
                        //Creo Verify 1ro y Enroll Despues
                        var fmdVerify = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.DP_VERIFICATION);
                        if (fmdVerify != null && fmdVerify.ResultCode == Constants.ResultCode.DP_SUCCESS)
                        {
                            LOG.Debug("BFPDigitalPersonaSensor.Extract From DP Verify Template = " + Convert.ToBase64String(fmdVerify.Data.Bytes));
                            templateGenerated = fmdVerify.Data.Bytes;
                            sampleOut = new List<Sample>();
                            sampleOut.Add(new Sample(2, minutiaeTypeOut, _BodyPart, (int)Biometrika.Standards.Purpose.Verify, 0, 0, templateGenerated));
                        }
                        else
                        {
                            LOG.Error("BFPDigitalPersonaSensor.Extract From DP Verify Template");
                            error = new Error(Bio.Core.Constant.Errors.IERR_EXTRACTING, "BFPDigitalPersonaSensor.Extract Error - Extrayendo DP Verify Template");
                        }

                        var resultConversion = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.DP_PRE_REGISTRATION);
                        if (resultConversion.ResultCode == Constants.ResultCode.DP_SUCCESS)
                        {
                            var gallery = new List<Fmd>();
                            for (var i = 0; i < 4; i++)
                            {
                                gallery.Add(resultConversion.Data);
                            }
                            var fmdEnroll = Enrollment.CreateEnrollmentFmd(Constants.Formats.Fmd.DP_REGISTRATION, gallery);
                            if (fmdEnroll != null && fmdEnroll.ResultCode == Constants.ResultCode.DP_SUCCESS)
                            {
                                LOG.Debug("BFPDigitalPersonaSensor.Extract From DP Enroll Template = " + Convert.ToBase64String(fmdEnroll.Data.Bytes));
                                templateGenerated = fmdEnroll.Data.Bytes;
                                if (sampleOut==null) sampleOut = new List<Sample>();
                                sampleOut.Add(new Sample(2, minutiaeTypeOut, _BodyPart, (int)Biometrika.Standards.Purpose.Enrol, 0, 0, templateGenerated));
                            }
                            else
                            {
                                LOG.Error("BFPDigitalPersonaSensor.Extract From DP Enroll Template");
                                error = new Error(Bio.Core.Constant.Errors.IERR_EXTRACTING, "BFPDigitalPersonaSensor.Extract Error - Extrayendo DP Enroll Template");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                error.ErrorCode = -1;
                error.ErrorDescription = "BFPDigitalPersonaSensor.Extract Error [" + ex.Message + "]";
                sampleOut = null;
            }
            return ret;
        }

        public int Close()
        {
            int ret = 0;
            try
            {
                if (CurrentSensor != null)
                {
                    //ret = ((SGFingerPrintManager)CurrentSensor).CloseDevice();
                    CurrentSensor = null;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
            }
            return ret;
        }


        /// <summary>
        /// Retorna lista de seriales conectados de esta marca
        /// </summary>
        /// <param name="listSerial"></param>
        /// <returns></returns>
        public List<string> GetSensorConnected(List<string> listSerial)
        {
            List<string> listDepured = new List<string>();
            string auxSerial;
            try
            {
                AuthenticationFactor = Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;
                Vendor = "DigitalPersona Inc";
                SensorType = Bio.Core.Matcher.Constant.Devices.DEVICE_DIGITALPERSONA;
                ReaderCollection readerCollection = DPUruNet.ReaderCollection.GetReaders();
                if (readerCollection == null) return null;
                else
                {
                    foreach (Reader item in readerCollection)
                    {
                        SerialSensor = item.Description.SerialNumber;
                        Vendor = item.Description.Id.VendorName;
                        auxSerial = item.Description.SerialNumber;
                        if (MustInclude(auxSerial, listSerial))
                        {
                            listDepured.Add(auxSerial);
                        }
                    }
                }
            }
            catch (Exception)
            {
                listDepured = null;
            }
            return listDepured;
        }

        public int InstanceSensor(string serial)
        {
            List<string> listDepured = new List<string>();
            string auxSerial;
            int ret = 0;
            try
            {

                ReaderCollection readerCollection = DPUruNet.ReaderCollection.GetReaders();
                if (readerCollection == null) return Error.IERR_NO_SENSOR_CONNECTED;
                else
                {
                    foreach (Reader item in readerCollection)
                    {
                        auxSerial = item.Description.SerialNumber;
                        if (auxSerial.Equals(serial))
                        {
                            //_readerDP = item;
                            CurrentSensor = item;
                            SerialSensor = ((Reader)CurrentSensor).Description.SerialNumber;
                            Vendor = ((Reader)CurrentSensor).Description.Id.VendorName;
                            break;
                        }
                    }
                }

                        
            }
            catch (Exception)
            {
                ret = Error.IERR_UNKNOWN;
            }
            return ret;
        }

        public void SetIndicatorStatus(UnitIndicatorStatus indicatorStatus)
        {
            throw new NotImplementedException();
        }

        public void SetPowerMode(UnitPowerMode powerMode)
        {
            throw new NotImplementedException();
        }

        public int EnableAutoEvent(string serialSensor, bool enable, int hwnd)
        {
            CurrentSensor = null;
            if (CurrentSensor == null)
            {
                if (InstanceSensor(serialSensor) < 0)
                {
                    //error.ErrorCode = -3;
                    //error.ErrorDescription = "BFPDigitalPErsonaSensor Error [SerialSensor=" + sensorSerial +
                    //                        " - Error = No pudo instanciar Sensor]";
                    return -1;
                }
            }

            //var resultCode = ((Reader)CurrentSensor).Open(Constants.CapturePriority.DP_PRIORITY_COOPERATIVE);
            return 0;
        }
        
        public int CaptureContinue(string sensorSerial, int qualityCapture, out Error error)
        {
            _ListSampleCaptured = null;
            error = new Error(0, null);
            LOG.Debug("BFPSecugenSensor.Capture IN...");
            //_CaptureType = captureType;
            //_BodyPart = bodypart;
            try
            {
                //Chequeo que este abierto sensor sino abro
                CurrentSensor = null;
                if (CurrentSensor == null)
                {
                    if (InstanceSensor(sensorSerial) < 0)
                    {
                        error.ErrorCode = -3;
                        error.ErrorDescription = "BFPDigitalPErsonaSensor Error [SerialSensor=" + sensorSerial +
                                                " - Error = No pudo instanciar Sensor]";
                        return -3;
                    }
                }

                var resultCode = ((Reader)CurrentSensor).Open(Constants.CapturePriority.DP_PRIORITY_COOPERATIVE);
                if (resultCode != Constants.ResultCode.DP_SUCCESS)
                {
                    resultCode = ((Reader)CurrentSensor).Open(Constants.CapturePriority.DP_PRIORITY_EXCLUSIVE);
                }
                if (resultCode == Constants.ResultCode.DP_SUCCESS)
                {
                    ((Reader)CurrentSensor).On_Captured += new DPUruNet.Reader.CaptureCallback(OnCaptureMethodDP);
                    //SetTimer(timeout);
                    ((Reader)CurrentSensor).GetStatus();


                    //resultCode = ((Reader)CurrentSensor).CaptureAsync(
                    //                       DPUruNet.Constants.Formats.Fid.ANSI,
                    //                       DPUruNet.Constants.CaptureProcessing.DP_IMG_PROC_DEFAULT,
                    //                       ((Reader)CurrentSensor).Capabilities.Resolutions[0]
                    //                       );
                    CaptureResult captureResultCode = ((Reader)CurrentSensor).Capture(
                                           DPUruNet.Constants.Formats.Fid.ANSI,
                                           DPUruNet.Constants.CaptureProcessing.DP_IMG_PROC_DEFAULT,
                                           5000, ((Reader)CurrentSensor).Capabilities.Resolutions[0]
                                           );

                    _OnCapturedEvent = OnCapturedEvent;
                    //_OnTimeoutEvent = OnTimeoutEvent;
                    if (captureResultCode.ResultCode != Constants.ResultCode.DP_SUCCESS)
                    {
                        error.ErrorCode = -1;
                        error.ErrorDescription = "BFPSecugenSensor.Capture Error [SerialSensor=" + sensorSerial +
                                                " - Error = resultCode = DEVICE_FAILURE]";
                        OnErrorEvent(error.ErrorCode, error.ErrorDescription, null);
                        //((Reader)CurrentSensor).On_Captured -= new DPUruNet.Reader.CaptureCallback(OnCaptureMethodDP);
                    }
                }
                else
                {
                    //ReaderStatus.ReaderResultCode = ReaderFactory.ReaderResultCode.DEVICE_FAILURE;
                }
            }
            catch (Exception ex)
            {
                error.ErrorCode = -1;
                error.ErrorDescription = "BFPSecugenSensor.Capture Error [SerialSensor=" + sensorSerial +
                                                " - Error = " + ex.Message + "]";
                OnErrorEvent(error.ErrorCode, error.ErrorDescription, null);
            }
            return 0;
        }

        public void On()
        {
            
        }

        public void Off()
        {
            
        }

        #region Private

        private bool MustInclude(string auxSerial, List<string> listSerial)
        {
            bool ret = false;
            try
            {
                if (listSerial == null || listSerial.Count == 0) return true;

                foreach (string item in listSerial)
                {
                    if (item.Equals(auxSerial)) return true;
                }
            }
            catch (Exception)
            {
                ret = false;
            }
            return ret;
        }

        //private static SGFPMFingerInfo FingerInfo()
        //{
        //    return new SGFPMFingerInfo()
        //    {
        //        FingerNumber = (SGFPMFingerPosition)0,
        //        ImageQuality = (Int16)70,
        //        ImpressionType = (Int16)SGFPMImpressionType.IMPTYPE_LP,
        //        ViewNumber = 1
        //    };
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sample"></param>
        /// <param name="typeImage">0-JPG desde RAW sin Completar | 1-JPG desde RAW 512x512</param>
        /// <param name="sampleout"></param>
        /// <returns></returns>
        internal int GetImage(Sample sample, int typeImage, out Sample sampleout, out Error error)
        {
            int ret = 0;
            sampleout = null;
            error = new Error(0, null);
            byte[] rawToImage;
            LOG.Debug("BFPSecugenSensor.GetImage IN...");
            try
            {
                LOG.Debug("BFPSecugenSensor.GetImage sample != null => " + (sample != null).ToString());
                if (sample == null || sample.Data == null ||
                   (sample.MinutiaeType != Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW &&
                   sample.MinutiaeType != Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ))
                {
                    LOG.Debug("BFPSecugenSensor.GetImage sample.Data != null => " + (sample.Data != null).ToString());
                    LOG.Debug("BFPSecugenSensor.GetImage sample.MinutiaeType = " + sample.MinutiaeType.ToString());
                    ret = Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA;
                    LOG.Error("BFPSecugenSensor.GetImage Error - Sample o Data == null");
                    sampleout = null;
                    error = new Error(Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA, "ProcessingFactory.GetImage Error - Sample o Data == null");
                    return ret;
                }

                //Si es WSQ => Descomprimo para usear el RAW
                short sw, sh;
                int w, h;
                if (sample.MinutiaeType == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                {
                    LOG.Error("BFPSecugenSensor.GetImage Decomprime WSQ...");
                    Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                    if (!dec.DecodeMemory((byte[])sample.Data, out sw, out sh, out rawToImage))
                    {
                        ret = Bio.Core.Constant.Errors.IERR_WSQ_DECOMPRESSING;
                        LOG.Error("BFPSecugenSensor.GetImage Error - Descomprimiendo WSQ");
                        sampleout = null;
                        error = new Error(Bio.Core.Constant.Errors.IERR_WSQ_DECOMPRESSING, "BFPSecugenSensor.GetImage Error - Descomprimiendo WSQ");
                        return ret;
                    }
                    else
                    {
                        w = (int)sw;
                        h = (int)sh;
                        LOG.Error("BFPSecugenSensor.GetImage RAW Obtenido desde WSQ size => w x h = " + w.ToString() + " x " + h.ToString());
                    }
                }
                else
                {
                    rawToImage = (byte[])sample.Data;
                    w = sample.SampleWith;
                    h = sample.SampleHeight;
                    LOG.Error("BFPSecugenSensor.GetImage RAW Recibido size => w x h = " + w.ToString() + " x " + h.ToString());
                }

                LOG.Debug("BFPSecugenSensor.GetImage typeImage = " + typeImage + " [" + (typeImage == 0 ? "Sin Completar" : "Completa 512x512") + "]");
                if (typeImage == 1)
                {
                    rawToImage = Bio.Core.Imaging.ImageProcessor.FillRaw((byte[])sample.Data, sample.SampleWith, sample.SampleHeight, 512, 512);
                    w = 512;
                    h = 512;
                }

                Bitmap bmpImage = Bio.Core.Imaging.ImageProcessor.RawToBitmap(rawToImage, w, h);
                byte[] bmpByte;
                if (bmpImage != null)
                {
                    //System.Drawing.Image img = (System.Drawing.Image)(((System.Drawing.Image)bmpImage).Clone());
                    //using (var ms = new MemoryStream())
                    //{
                    //    img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    //    bmpByte = ms.ToArray();
                    //}
                    sampleout = new Sample(sample.AuthenticationFactor, MinutiaeType.MINUTIAETYPE_JPG, sample.BodyPart, 
                                            (int)Biometrika.Standards.Purpose.Verify,  w, h, bmpImage); // bmpByte);
                }
                else
                {
                    LOG.Debug("BFPSecugenSensor.GetImage Error ImageProcessor.RawToBitmap generating JPG");
                    ret = Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA;
                    LOG.Error("BFPSecugenSensor.GetImage Error ImageProcessor.RawToBitmap generating JPG");
                    sampleout = null;
                    error = new Error(Bio.Core.Constant.Errors.IERR_NORMALIZING_DATA,
                        "BFPSecugenSensor.GetImage Error ImageProcessor.RawToBitmap generating JPG");
                    return ret;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BFPSecugenSensor.GetImage Error " + ex.Message);
                sampleout = null;
                error = new Error(-1, "BFPSecugenSensor.GetImage Error = " + ex.Message);
            }
            LOG.Debug("BFPSecugenSensor.GetImage OUT!");
            return ret;
        }



        #endregion Private
    }
}
