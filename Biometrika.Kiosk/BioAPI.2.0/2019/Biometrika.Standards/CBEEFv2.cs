﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Standards
{
    public class CBEEFv2
    {

        private SBH_CBEEFv2 _SBH_CBEEFv2;
        private BSMB_CBEEFv2 _BSMB_CBEEFv2;
        private SB_CBEEFv2 _SB_CBEEFv2;

        private int _CBEEFType = 0;  //0-Simple | 1-Complex
        private CBEEFv2[] _CBEEFList = null;

        public CBEEFv2()
        {
            _CBEEFType = 0;
            _CBEEFList = new CBEEFv2[1];
        }

        public CBEEFv2(int quantity)
        {
            _CBEEFType = 0;
            _CBEEFList = new CBEEFv2[quantity];
        }



    }

    public class SBH_CBEEFv2
    {
        byte[] _SBHSecurity;
        byte[] _IntegrityOptions;
        byte[] _CBEFFHeader;
        byte[] _PatronHeaderVersion;
        byte[] _BiometricType;
        byte[] _RecordDataType;
        byte[] _RecordPurpose;
        byte[] _RecordDataQuality;
        byte[] _CreationDate;
        byte[] _Creator; 


        public SBH_CBEEFv2()
        {
            _SBHSecurity = new byte[1];
            _IntegrityOptions = new byte[1];
            _CBEFFHeader = new byte[2];
            _PatronHeaderVersion = new byte[2];
            _BiometricType = new byte[3];
            _RecordDataType = new byte[1];
            _RecordPurpose = new byte[1];
            _RecordDataQuality = new byte[1];
            _CreationDate = new byte[7];
            _Creator = new byte[16];
        }

    }

    public class BSMB_CBEEFv2
    {
        byte[] _BSMBFormatOwner;
        byte[] _BSMBFormatType;
        byte[] _MemoryBlockLength;
        byte[] _BiometricSpecificMemoryBlockData;

        public BSMB_CBEEFv2()
        {
            _BSMBFormatOwner = new byte[2];
            _BSMBFormatType = new byte[2];
            _MemoryBlockLength = new byte[4];
        }

    }

    public class SB_CBEEFv2
    {
        byte[] SignatureOrMAC;

        public SB_CBEEFv2()
        {
        }

    }
}
