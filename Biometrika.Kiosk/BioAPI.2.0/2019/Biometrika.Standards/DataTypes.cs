﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Standards
{
    public enum BiometricType
    {
        NoValueAvailable,
        MultipleBiometricTypes,
        Face,
        Voice,
        Finger,
        Iris,
        Retina,
        HandGeometry,
        SignatureOrSign,
        Keystroke,
        LipMovement,
        Gait,
        Vein,
        DNA,
        Ear,
        Foot,
        Scent
    }

    public enum BiometricSubtype
    {
        NoValueAvailable,
        Left,
        Right,
        LeftThumb,
        LeftIndexFinger,
        LeftMiddleFinger,
        LeftRingFinger,
        LeftLittleFinger,
        RightThumb,
        RightIndexFinger,
        RightMiddleFinger,
        RightRingFinger,
        RightLittleFinger,
        LeftPalm,
        LeftBackOfHand,
        LeftWrist,
        RightPalm,
        RightBackOfHand,
        RightWrist
    }

    /// <summary>
    /// Defines access modes to the database.
    /// -Read: The access mode that allows to retrieve records but probiits any modifications of the databse.
    /// -ReadWrite: The access mode that allows bot the record retrieval and that modifications or the database.
    /// -Write: The access mode that allows t add and delete records from the databse but prohibits retrieval of records.
    /// </summary>
    public enum BIRDatabaseAccess
    {
        Read,
        ReadWrite,
        Write
    }

    /// <summary>
    /// Defines the purpose for which the BioAPI BIR is intended (when used as an input to a BioAPI function) or 
    /// is suitable (when used as an output from a BioAPI function or within the BIR header).
    /// </summary>
    public enum Purpose
    {
        Verify,
        Identify,
        Enrol,
        EnrolForVerificationOnly,
        EnrolForIdentificacionOnly,
        Audit,
        Decide,
        NoPurposeAvailable
    }

    public enum BSPSchemaOperations
    {
        CalibrateSensor,// (0x00020000)
        Capture,// (0x00000004)
        CheckQuality,// (0x00080000) (typo in 19784-1)
        ControlUnit,// ###### (0x00400000)
        CreateTemplate,// (0x00000008)
        CreateTemplateWithAuxBIR,// (0x00000020)
        DatabaseOperations,// ###### (0x00002000)
        Decide,// ###### (0x04000000)
        EnableEvents,// (0x00000001)
        Enrol,// (0x00000100)
        Export,// ###### (0x20000000) (typo in 19784-1)
        Fuse,// ###### (0x08000000)
        GetIndicatorStatus,// (0x00010000)
        Identify,// (0x00000080)
        IdentifyAggregate,// (0x00000400)
        Import,//  ###### (0x00000800)
        PresetIdentifyPopulation,// (0x00001000)
        Process,// (0x00000010)
        ProcessWithAuxBIR,// (0x01000000)
        QueryBFPs,// (0x00200000)
        QueryUnits,// (0x00100000)
        Security,// (0x10000000)
        SetIndicatorStatus,// (0x00008000)
        SetPowerMode,// (0x00004000)
        Transform,// ####### (0x00800000)
        Utilities,// ######## (0x00040000) 
        Verify,// (0x00000040)
        VerifyAggregated,// (0x00000200)
        VerifyWithAuxBIR// (0x02000000)

    }

    public enum BSPSchemaOptions
    {
        Adaptation,// (0x00000800)
        AppGUI,// (0x00000010)
        AchiveBFP,// (0x00020000)
        Binning,// (0x00001000)
        BirEncrypt,// (0x00000200)
        BirSign,// (0x00000100)
        CaptureMultiple,// (0x00400000)
        CoarseScores,// (0x00100000)
        ComparisonBFP,// (0x00040000)
        GUIProgressEvents,// (0x00000020)
        IdentifyIndicator,// (0x00200000)
        OCC,// (0x00004000) (formerly known as MOC)
        Payload,// (0x00000080)
        ProcessingBFP,// (0x00080000)
        ProcessMultiple,// (0x00800000)
        QualityIntermediate,// (0x00000004)
        QualityProcessed,// (0x00000008)
        QualityRaw,// (0x00000002)
        Raw,// (0x00000001)
        SelfContainedDevice,// (0x00002000)
        SensorBFP,// (0x00010000)
        SourcePresent,// (0x00000040)
        SubtypeToCapture,// (0x00008000)
        TemplateUpdate// (0x00000400)
    }

    public enum EventKind
    {
        Insert,// (0x00000001)
        Remove,// (0x00000002)
        Fault,// (0x00000004)
        SourcePresent,// (0x00000008)
        SourceRemoved// (0x00000010) 
    }

    /// <summary>
    /// Defines originator of the error in a exception (see 8.1)
    /// </summary>
    public enum Facility
    {
        Framework,
        BSP,
        Unit
    }

    /// <summary>
    /// Indicates the enrol type of a BSP (see 8.2)
    /// </summary>
    public enum GUIEnrolType
    {
        TestVerify,
        MultipleCapture
    }

    /// <summary>
    /// Determines the moment when the processing of an operation is at the time of calling a GUI callback function (see 8.2)
    /// </summary>
    public enum GUIMoment
    {
        BeforeStart,
        AfterEnd
    }

    /// <summary>
    /// Determines the operation being performed when using GUI callback functions (see 8.2)
    /// </summary>
    public enum GUIOperation
    {
        Capture,
        Enrol,
        Identify,
        Verify
    }

    /// <summary>
    /// Enumeration of the possible actions to be performed bya BSP after a GUI event notification callback made by the BSP
    /// has returned control to the BSP (see 8.2)
    /// </summary>
    public enum GUIResponse
    {
        CycleStart,
        CycleRestart,
        Default,
        OpComplete,
        OpCancel,
        ProgressContinue,
        ProgressAbort,
        Recapture,
        SubOpStart,
        SubOpNext
    }

    /// <summary>
    /// Determines the operation being performed when using GUI callback functions (see 8.2)
    /// </summary>
    public enum GUISuboperation
    {
        Capture,
        CreateTemplate,
        Identify,
        Process,
        Verify
    }

    /// <summary>
    /// This data type identifies the type of biometric sample (raw, intermediate, or processed) that is contained
    /// in the BDB.
    /// </summary>
    public enum ProcessedLevel
    {
        Intermediate,
        Processed,
        Raw
    }

    /// <summary>
    /// Defines the request to some BioAPI methods, to provide additional results to the originally defined (e.g. see 4.2.3).
    /// 
    /// -RequestAdaptedBir: Requests that a BIR be constructed by adapting the reference template using the processed BIR that is the input to
    /// the biometric verification.
    /// -RequestAuditData: Requests that BSP should collect audit data. This data may be used to provide human-identifiable data of the person
    /// at the sensor unit. Not all BSPs support the collection of audit data.
    /// -RequestPayload: If a payload is associated with the reference template, requests that the payload should returned upon successful
    /// verification if the achieved FMR is sufficiently stringent; this is controlled by the policy of the BSP and specified in its schema.
    /// </summary>
    public enum ResultOptions
    {
        RequestAdaptedBIR,
        RequestAuditData,
        RequestPayload
    }

    /// <summary>
    /// Defines which security options are supported by the BioAPI_Unit
    /// </summary>
    public enum SecurityOptionsType
    {
        Encryption,// (0x00000001) 
        MAC,// (0x00000002) 
        DigitalSignature,// (0x00000004) 
        ACBioGenerationWithMAC,// (0x00000010) 
        ACBioGenerationWithDigitalSignature// (0x00000020) 
    }

    /// <summary>
    /// Defines possible unit categories.
    /// -Archive: The unit manages BSP's BIR database.
    /// -Comparison: The unit is the collection of comparison algorithms. 
    /// -Processing: The unit is the collection of processing algorithms.
    /// -Sensor: The unit manages hardware sensor.
    /// </summary>
    public enum UnitCategoryType
    {
        Archive,
        Comparison,
        Processing,
        Sensor
    }

    /// <summary>
    /// Defines possible values of indicator status.
    /// </summary>
    public enum UnitIndicatorStatus
    {
        Accept,
        Busy,
        Failure,
        Ready,
        Reject
    }

    /// <summary>
    /// Defines unit power modes.
    /// -Detect: The mode when the unit is able to detect insertion/finger on/person present type of events.
    /// -Normal: The mode when all functions are available.
    /// -Sleep: Minimum mode. All functions off.
    /// </summary>
    public enum UnitPowerMode
    {
        Detect,
        Normal,
        Sleep
    }
}
