﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Standards
{
    public class Constant
    {


        #region ERROR

        //public static int ERROR_ = -1;
        public static int ERROR_OK = 0;
        public static int ERROR_UNKNOW = -1;
        public static int ERROR_INVALID_LICENSE = -2;
        public static int ERROR_CONVERTION_NOT_SUPPORTED = -3;
        public static int ERROR_CONVERTION_NOT_PROCESSED = -4;


        #endregion ERROR

        #region SampleFormat

        public static int SAMPLE_FORMAT_IMAGE_RAW = 1;
        public static int SAMPLE_FORMAT_IMAGE_WSQ = 2;
        public static int SAMPLE_FORMAT_MINUTIAE_ISO = 3;
        public static int SAMPLE_FORMAT_MINUTIAE_ANSI = 4;
        public static int SAMPLE_FORMAT_MINUTIAE_VERIFINGER = 5;

        #endregion SampleFormat


        #region DestinationFormat

        public static int DESTINATION_FORMAT_FIRECORD = 1;
        public static int DESTINATION_FORMAT_NTEMPLATE = 2;
        public static int DESTINATION_FORMAT_CBEEF_ISO = 3;
        public static int DESTINATION_FORMAT_CBEEF_ANSI = 4;


        #endregion DestinationFormat

        public enum SBH_SecutiryOptions
        {
            None = 0x00,
            WithPrivacyEncryption = 0x10,
            WithIntegritySignedOrMACed = 0x20,
            WithIntegrityAndPrivacyEncryption = 0x30
        }

        public enum SBH_IntegrityOptions
        {
            None = 0x00,
            MACed = 0x01,
            Signed = 0x02
        }

        public enum CBEFF_HeaderVersion
        {
            Major = 0x01,
            Minor = 0x00
        }

        public enum PatronHeaderVersion
        {

        }

        /*
            INCITS Technical Committee M1	001B	27	0401	1025	Finger Image format	ANSI/INCITS 381-2004
            INCITS Technical Committee M1	001B	27	0201	513	Finger Minutiae - no extended data	ANSI/INCITS 378-2004
            NCITS Technical Committee M1	001B	27	0501	1281	Face Image Format	ANSI/INCITS 385-2004
            INCITS Technical Committee M1	001B	27	8001	32769	WSQ compressed fingerprint image	IAFIS-IC-0010 (V3) - WSQ Gray-scale fingerprint image compression specification; 1997
            INCITS Technical Committee M1	001B	27	8003	32771	JPEG image	ISO/IEC 10918 - Information Technology - Digital Compression and coding of continuous-tone still images (JPEG)
            INCITS Technical Committee M1	001B	27	8004	32772	JPEG2000 image	ISO/IEC 15444 - Information Technology - JPEG 2000 Image Coding System
            INCITS Technical Committee M1	001B	27	8005	32773	TIFF image	ISO/IEC 12639 - Tag image file format for image technology (TIFF/IT)
            ISO/IEC JTC 1 SC 37-Biometrics	0101	257	0001	1	finger-minutiae-record-n	ISO/IEC 19794-2 {iso registration-authority cbeff(19785) biometric-organization(0) jtc1-sc37(257) bdbs(0) finger-minutiae-record-n (1)} See ISO/IEC 19794-2:2005/Cor.1:2007 Table 16 (Finger minutiae, record format, no extended data)
        */
        public enum BDBOwner
        {

        }

        public enum BDBFormat
        {

        }

        /*
        000A	10	Secugen Corporation	Dan Riley	USA	N
        */

    }

}
