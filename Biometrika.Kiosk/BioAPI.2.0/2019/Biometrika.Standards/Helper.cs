﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Standards
{
    public static class Helper
    {

        public static BiometricSubtype GetBiometricSubtype(int bodypart)
        {
            switch (bodypart)
            {
                case 1:
                    return BiometricSubtype.RightThumb;
                case 2:
                    return BiometricSubtype.RightIndexFinger;
                case 3:
                    return BiometricSubtype.RightMiddleFinger;
                case 4:
                    return BiometricSubtype.RightRingFinger;
                case 5:
                    return BiometricSubtype.RightLittleFinger;
                case 6:
                    return BiometricSubtype.LeftThumb;
                case 7:
                    return BiometricSubtype.LeftIndexFinger;
                case 8:
                    return BiometricSubtype.LeftMiddleFinger;
                case 9:
                    return BiometricSubtype.LeftRingFinger;
                case 10:
                    return BiometricSubtype.LeftLittleFinger;
                //case 16:
                //    return BiometricSubtype.;
                default:
                    return BiometricSubtype.RightThumb;
            }
        }

    }
}
