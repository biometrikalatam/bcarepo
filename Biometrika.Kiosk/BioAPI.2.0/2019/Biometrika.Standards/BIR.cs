﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Standards
{
    /// <summary>
    /// Represents BIR. No methods to modify the existing BIR object are provided. An application should use
    /// the corresponding method of the DataFactory to create new BIR containing modified data.
    /// Depending on the way the BIR object was created, the BIR is either bound to the BSP or is unbound.
    /// Output of biometric operation normally creates the BIR bound to the BSP. Bound BIR is the BIR 
    /// encapsulated in the BSP with the lifetime limited by the lifetime of the AttachSession that created it.
    /// The bound BIR can be extracted from a BSP:
    /// -implicitly, by calling either of properties BiometricData or SecurityBlock.
    /// -explicitly, by calling the Unbind() method.
    /// </summary>
    /// <author>
    /// See CREDITS.txt
    /// </author>
    public class BIR
    {

        public short SHSecurityOption { get; set; }
        public short PatronHeaderVersion { get; set; }
        public BiometricType BiometricType { get; set; }
        //public BiometricSubtype BiometricSubtype { get; set; }
        public ProcessedLevel DataBIRType { get; set; }
        public Purpose Purpose { get; set; }
        public short Quality { get; set; }
        public DataFormat BDBFormat { get; set; }
        public byte[] BDBData;

        //public byte PatronHeaderVersion { get; set; }
        //public DataFormat PatronFormat { get; set; }
        //public byte CBEFFVersion { get; set; }
        //public DataFormat BDBFormat { get; set; }
        //public bool BDBEncription { get; set; }
        //public bool BDBIntegrity { get; set; }
        //public BiometricType BDBBiometricType { get; set; }
        //public BiometricSubtype BDBBiometricSubtype { get; set; }
        //public byte[] BDBChallengeResponse { get; set; }
        //public Date BDBCreationDate { get; set; }
        //public byte[] BDBIndex { get; set; }
        //public ProcessedLevel BDBProcessedLevel { get; set; }
        //public DataFormat BDBProduct { get; set; }
        //public Purpose BDBPurpose { get; set; }
        //public byte BDBQuality { get; set; }
        //public List<Date> BDBValidityPeriod { get; set; }
        //public Date BIRCreationDate { get; set; }
        //public byte[] BIRCreator { get; set; }
        //public byte[] BIRIndex { get; set; }
        //public byte[] BIRPayload { get; set; }
        //public List<Date> BIRValidityPeriod { get; set; }
        //public DataFormat SBFormat { get; set; }
        //public byte[] BDBData { get; set; }
        //public byte[] SBData { get; set; }


        //public BIR
        //(DataFormat patronFormat, byte cbeffVersion, byte patronHeaderVersion, DataFormat bdbFormat, bool bdbEncription, bool bdbIntegrity,
        // BiometricType bdbBiometricType, BiometricSubtype bdbBiometricSubtype, byte[] bdbChallengeResponse, Date bdbCreationDate,
        //     byte[] bdbIndex, ProcessedLevel bdbProcessedLevel, DataFormat bdbProduct, Purpose bdbPurpose, byte bdbQuality,
        //     List<Date> bdbValidityPeriod, Date birCreationDate, byte[] birCreator, byte[] birIndex, byte[] birPayload,
        //     List<Date> birValidityPeriod, DataFormat sbFormat, byte[] bdbData, byte[] sbData)
        //{
        //    PatronFormat = patronFormat;
        //    CBEFFVersion = cbeffVersion;
        //    PatronHeaderVersion = patronHeaderVersion;
        //    BDBFormat = bdbFormat;
        //    BDBEncription = bdbEncription;
        //    BDBIntegrity = bdbIntegrity;
        //    BDBBiometricType = bdbBiometricType;
        //    BDBBiometricSubtype = bdbBiometricSubtype;
        //    BDBChallengeResponse = bdbChallengeResponse;
        //    BDBCreationDate = bdbCreationDate;
        //    BDBIndex = bdbIndex;
        //    BDBProcessedLevel = bdbProcessedLevel;
        //    BDBProduct = bdbProduct;
        //    BDBPurpose = bdbPurpose;
        //    BDBQuality = bdbQuality;
        //    BDBValidityPeriod = bdbValidityPeriod;
        //    BIRCreationDate = birCreationDate;
        //    BIRCreator = birCreator;
        //    BIRIndex = birIndex;
        //    BIRPayload = birPayload;
        //    BIRValidityPeriod = birValidityPeriod;
        //    SBFormat = sbFormat;
        //    BDBData = bdbData;
        //    SBData = sbData;
        //}

        public BIR (short _sHSecurityOption, short _patronHeaderVersion, BiometricType _bdbBiometricType,
                    ProcessedLevel _dataBIRtype, Purpose _purpose, short _quality, DataFormat _bDBFormat, byte[] _bDBData)
        {
            SHSecurityOption = _sHSecurityOption;
            PatronHeaderVersion = _patronHeaderVersion;
            BiometricType = _bdbBiometricType;
            DataBIRType = _dataBIRtype;
            Purpose = _purpose;
            Quality = _quality;
            BDBFormat = _bDBFormat;
            BDBData = _bDBData;
    }
        /// <summary>
        /// Fills in the BIR data from a byte array coded as a ISO/IEC 19785 record
        /// </summary>
        /// <param name="record">
        /// the byte array containing the CBEFF record
        /// </param>
        public void BIRFromByteArray(byte[] record)
        {

        }

        /// <summary>
        /// Serializes a BIR record so as to provide it as a byte array representing the CBEFF information
        /// </summary>
        /// <returns>
        /// The byte array containing the CBEFF information
        /// </returns>
        public byte[] BIRToByteArray()
        {
            List<byte[]> listArrays = new List<byte[]>();

            //int size = 52 + BDBData.Length;
            //byte[] bir = new byte[size];

            //byte[] lenBIR = new byte[4];

            /*
                0x00 = plain Biometric
                0x10 = with Privacy (Encryption)
                0x20 = with Integrity (Signed or MACed)
                0x30 = with Privacy and Integrity
            */
            byte[] bySHSecurityOption = new byte[1];
            bySHSecurityOption = BitConverter.GetBytes(SHSecurityOption);
            listArrays.Add(bySHSecurityOption);

            byte[] byPatronHeaderVersion = new byte[1];
            byPatronHeaderVersion = BitConverter.GetBytes(PatronHeaderVersion); ;
            listArrays.Add(byPatronHeaderVersion);

            #region BiometricType 

            byte[] bdbBiometricType = new byte[3];

            switch (BiometricType)
            {
                case (BiometricType.NoValueAvailable):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 0;
                    bdbBiometricType[2] = 0;
                    break;
                case (BiometricType.MultipleBiometricTypes):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 0;
                    bdbBiometricType[2] = 1;
                    break;
                case (BiometricType.Face):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 0;
                    bdbBiometricType[2] = 2;
                    break;
                case (BiometricType.Voice):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 0;
                    bdbBiometricType[2] = 4;
                    break;
                case (BiometricType.Finger):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 0;
                    bdbBiometricType[2] = 8;
                    break;
                case (BiometricType.Iris):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 0;
                    bdbBiometricType[2] = 16;
                    break;
                case (BiometricType.Retina):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 0;
                    bdbBiometricType[2] = 32;
                    break;
                case (BiometricType.HandGeometry):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 0;
                    bdbBiometricType[2] = 64;
                    break;
                case (BiometricType.SignatureOrSign):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 0;
                    bdbBiometricType[2] = 128;
                    break;
                case (BiometricType.Keystroke):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 1;
                    bdbBiometricType[2] = 0;
                    break;
                case (BiometricType.LipMovement):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 2;
                    bdbBiometricType[2] = 0;
                    break;
                case (BiometricType.Gait):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 16;
                    bdbBiometricType[2] = 0;
                    break;
                case (BiometricType.Vein):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 32;
                    bdbBiometricType[2] = 0;
                    break;
                case (BiometricType.DNA):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 64;
                    bdbBiometricType[2] = 0;
                    break;
                case (BiometricType.Ear):
                    bdbBiometricType[0] = 0;
                    bdbBiometricType[1] = 128;
                    bdbBiometricType[2] = 0;
                    break;
                case (BiometricType.Foot):
                    bdbBiometricType[0] = 1;
                    bdbBiometricType[1] = 0;
                    bdbBiometricType[2] = 0;
                    break;
                case (BiometricType.Scent):
                    bdbBiometricType[0] = 2;
                    bdbBiometricType[1] = 0;
                    bdbBiometricType[2] = 0;
                    break;
            }
            listArrays.Add(bdbBiometricType);
            #endregion

            //BDBBiometricSubtype 
            #region DataBIRtype

            byte[] byDataBIRtype = new byte[1];
            
            switch (DataBIRType)
            {
                case ProcessedLevel.Raw:
                    byDataBIRtype[0] = 0;
                    break;
                case ProcessedLevel.Intermediate:
                    byDataBIRtype[0] = 1;
                    break;
                case ProcessedLevel.Processed:
                    byDataBIRtype[0] = 2;
                    break;
            }
            listArrays.Add(byDataBIRtype);
            #endregion

            byte[] byPurpose = new byte[1];

            switch (Purpose)
            {
                case Standards.Purpose.NoPurposeAvailable:
                    byPurpose[0] = 0;
                    break;
                case Standards.Purpose.Verify:
                    byPurpose[0] = 1;
                    break;
                case Standards.Purpose.Identify:
                    byPurpose[0] = 2;
                    break;
                case Standards.Purpose.Enrol:
                    byPurpose[0] = 3;
                    break;
                case Standards.Purpose.EnrolForVerificationOnly:
                    byPurpose[0] = 4;
                    break;
                case Standards.Purpose.EnrolForIdentificacionOnly:
                    byPurpose[0] = 5;
                    break;
                case Standards.Purpose.Audit:
                    byPurpose[0] = 6;
                    break;
                default:
                    byPurpose[0] = 1;
                    break;
            }
            listArrays.Add(byPurpose);
            //Purpose[0] = 1;
            //Buffer.BlockCopy(Purpose, 0, bir, 6, Purpose.Length);

            byte[] byQuality = new byte[1];
            if (Quality == -2)
            {
                byQuality[0] = 254;
            }
            else if (Quality == -1)
            {
                byQuality[0] = 255;
            }
            else if ((Quality < -2 || Quality > 100))
            {
                byQuality[0] = 255;
            } else { 
                byQuality = BitConverter.GetBytes(Quality);
            }
            listArrays.Add(byQuality);

            /*
             
              000A	10	Secugen Corporation	Dan Riley	USA	N
              0011	17	NEC Solutions America, Inc.	Theresa Giordano	USA	N
              0012	18	UPEK, Inc.	Vito Fabbrizio	USA	N
              0017	23	Cogent Systems, Inc.	James Jasinski	USA	N
              0018	24	Cross Match Technologies, Inc.	Greg Cannon	USA	N
              0019	25	Recognition Systems, Inc.	Frank Perry	USA	N
              001D	29	Sagem Morpho	Creed Jones	USA	N
              0031	49	Neurotechnologija	Irmantas Naujikas	Lithuania	N
              0033	51	DigitalPersona, Inc.	Brian Farley	USA	N
              0035	53	Innovatrics	Jan Lunter	France	N
              003A	58	Griaule Tecnologia LTDA	Iron Calil Daher	Brazil	software
              003B	59	Aware, Inc.	David Benini	USA	fingerprint template
              0044	68	Suprema, Inc.	Brian Song	Korea	fingerprint
              004E	78	IriTech, Inc.	Joyce Y. Kim	USA	iris
              0103	259	Vendor Unknown	Lisa Rajchel	USA	Standards Development Organization

              INCITS Technical Committee M1	001B	27	0201	513	    Finger Minutiae - no extended data	ANSI/INCITS 378-2004
              INCITS Technical Committee M1	001B	27	0401	1025	Finger Image format                 ANSI/INCITS 381-2004
              INCITS Technical Committee M1	001B	27	0501	1281	Face Image Format	                ANSI/INCITS 385-2004
              INCITS Technical Committee M1	001B	27	8003	32771	JPEG image	                        ISO/IEC 10918 - Information Technology - Digital Compression and coding of continuous-tone still images (JPEG)
              INCITS Technical Committee M1	001B	27	8004	32772	JPEG2000 image	                    ISO/IEC 15444 - Information Technology - JPEG 2000 Image Coding System
              INCITS Technical Committee M1	001B	27	8005	32773	TIFF image	                        ISO/IEC 12639 - Tag image file format for image technology (TIFF/IT)
             ISO/IEC JTC 1 SC 37-Biometrics	0101	257	0001	1	    finger-minutiae-record-n	        ISO/IEC 19794-2 {iso registration-authority cbeff(19785) biometric-organization(0) jtc1-sc37(257) bdbs(0) finger-minutiae-record-n (1)} See ISO/IEC 19794-2:2005/Cor.1:2007 Table 16 (Finger minutiae, record format, no extended data)
             ISO/IEC JTC 1 SC 37-Biometrics	0101	257	0007	7	    finger-image	                    ISO/IEC 19794-4 {iso registration-authority cbeff(19785) biometric-organization(0) 257 bdbs(0) finger-image (7)}
             ISO/IEC JTC 1 SC 37-Biometrics	0101	257	0008	8	    face-image	face-image              ISO/IEC 19794-5 {iso registration-authority cbeff(19785) biometric-organization(0) 257 bdbs(0) face-image (8)}
             ISO/IEC JTC 1 SC 37-Biometrics	0101	257	001D	29	    finger-minutiae-record-format	    ISO/IEC 19794-2 {iso registration-authority cbeff(19785)biometric-organization(0) 257 bdbs(0) finger-minutiae-record-format (29)}
             ISO/IEC JTC 1 SC 37-Biometrics	0101	257	0005	5	    finger-minutiae-card-compact-v-nh	ISO/IEC 19794-2 with Cor. 1 {iso registration-authority cbeff(19785) biometric-organization(0) 257 bdbs(0) finger-minutiae-card-compact-v-nh (5)} See ISO/IEC 19794-2:2005/Cor.1:2007 Table 16 (Finger minutiae, card compact size format, valley bifurcations with no headers) 
             INCITS Technical Committee M1	001B	27	8001	32769	WSQ compressed fingerprint image	IAFIS-IC-0010 (V3) - WSQ Gray-scale fingerprint image compression specification; 1997
            */
            byte[] byBSMBFormatOwner = new byte[4];
            byBSMBFormatOwner = BitConverter.GetBytes(BDBFormat.Owner);
            listArrays.Add(byBSMBFormatOwner);
            //BSMBFormatOwner[0] = 0;
            //BSMBFormatOwner[1] = 0;
            //Buffer.BlockCopy(BSMBFormatOwner, 0, bir, 8, BSMBFormatOwner.Length);


            byte[] byBSMBFormatType = new byte[4];
            byBSMBFormatType = BitConverter.GetBytes(BDBFormat.Type);
            listArrays.Add(byBSMBFormatType);
            //BSMBFormatType[0] = 0;
            //BSMBFormatType[1] = 1;

            byte[] BDBDataLen = BitConverter.GetBytes(BDBData.Length);
            listArrays.Add(BDBDataLen);
            listArrays.Add(BDBData);
           

            byte[] SecurityBlockLen = null;
            byte[] bySecurityBlock = CreateSecurityBlock(SHSecurityOption, out SecurityBlockLen);
            if (bySecurityBlock != null && SecurityBlockLen != null)
            {
                listArrays.Add(bySecurityBlock);
                listArrays.Add(SecurityBlockLen);
            } 

            int totalLength = 0;
            for (int i = 0; i < listArrays.Count; i++)
            {
                if (listArrays[i] != null)
                {
                    totalLength += listArrays[i].Length;
                }
            }
            byte[] BIRLen = BitConverter.GetBytes(totalLength);
            byte[] birToByteArray = new byte[totalLength + BIRLen.Length];

            int dstOffset = 0;
            try
            {

                for (int i = 0; i < listArrays.Count; i++)
                {
                    if (listArrays[i] != null)
                    {
                        Buffer.BlockCopy(listArrays[i], 0, birToByteArray, dstOffset, listArrays[i].Length);
                        dstOffset = dstOffset + listArrays[i].Length;
                    }
                }
            }
            catch (Exception ex)
            {

                string s = ex.Message;
            }


            return birToByteArray;
        }

        private byte[] CreateSecurityBlock(short sHSecurityOption, out byte[] securityBlockLen)
        {
            securityBlockLen = null;
            return null;
        }

        //public byte[] BIRToByteArray()
        //{
        //    return null;
        //}

        /// <summary>
        /// Removes all the information in the current BIR, leaving it empty for a next use
        /// </summary>
        public void Destroy()
        {

        }

        private static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}
