namespace Bio.Core.Wsq
{
	/// <summary>
	/// Creador de WaveletTrees
	/// </summary>
	public class TreeBuilder
	{
		private const int WTREELEN = 20;
		private const int QTREELEN = 64;

		/// <summary>
		/// Privado para no crear instancias de esta clase
		/// </summary>
		private TreeBuilder()
		{
			
		}

		/// <summary>
		/// Construye los arboles de division para las dimensiones dadas
		/// </summary>
		/// <param name="width">ancho</param>
		/// <param name="height">alto</param>
		/// <param name="wTreeNodes">WaveleTrees de salida</param>
		/// <param name="qTreeNodes">QuantumTrees de salida</param>
		public static void BuildTrees(int width, int height, out WaveletTreeNode[] wTreeNodes, out QuantumTreeNode[] qTreeNodes)
		{
			wTreeNodes = new WaveletTreeNode[WTREELEN];
			qTreeNodes = new QuantumTreeNode[QTREELEN];
			BuildWTree(width, height, wTreeNodes);
			BuildQTree(wTreeNodes, qTreeNodes);
			for (int i = 0; i < WTREELEN; i++)
				wTreeNodes[i].OffSet = wTreeNodes[i].Y*width + wTreeNodes[i].X;
				
		}

		private static void BuildWTree(int width, int height, WaveletTreeNode[] wTree)
		{
			/* starting lengths of sections of
									 the image being split into subbands */

			WTree4(wTree, 0, 1, width, height, 0, 0, 1);

			int lenMod2 = (wTree[1].LenX % 2);	
			int lenx = (wTree[1].LenX + lenMod2) / 2;
			int lenx2 = lenx - lenMod2;
			

			lenMod2 = (wTree[1].LenY % 2);
			int leny = (wTree[1].LenY + lenMod2) / 2;
			int leny2 = leny - lenMod2;
			

			WTree4(wTree, 4, 6, lenx2, leny, lenx, 0, 0);
			WTree4(wTree, 5, 10, lenx, leny2, 0, leny, 0);
			WTree4(wTree, 14, 15, lenx, leny, 0, 0, 0);

			wTree[19].X = 0;
			wTree[19].Y = 0;
			wTree[19].LenX = (wTree[15].LenX + (wTree[15].LenX % 2)) / 2;
			wTree[19].LenY = (wTree[15].LenY + (wTree[15].LenY % 2)) / 2;

		}

		static  void WTree4(WaveletTreeNode[] w_tree,    /* wavelet tree structure                      */
			int start1,   /* w_tree locations to start calculating       */
			int start2,   /*    subband split locations and sizes        */
			int lenx,     /* (temp) subband split location and sizes     */
			int leny,
			int x,
			int y,
			int stop1)   
                      
		{
			int evenx, eveny;   /* Check length of subband for even or odd */
			int p1, p2;         /* w_tree locations for storing subband sizes and
                          locations */
   
			p1 = start1;
			p2 = start2;

			evenx = lenx % 2;
			eveny = leny % 2;

			w_tree[p1].X = x;
			w_tree[p1].Y = y;
			w_tree[p1].LenX = lenx;
			w_tree[p1].LenY = leny;
   
			w_tree[p2].X = x;
			w_tree[p2+2].X = x;
			w_tree[p2].Y = y;
			w_tree[p2+1].Y = y;

			if(evenx == 0) 
			{
				w_tree[p2].LenX = lenx / 2;
				w_tree[p2+1].LenX = w_tree[p2].LenX;
			}
			else 
			{
				if(p1 == 4) 
				{
					w_tree[p2].LenX = (lenx - 1) / 2;
					w_tree[p2+1].LenX = w_tree[p2].LenX + 1;
				}
				else 
				{
					w_tree[p2].LenX = (lenx + 1) / 2;
					w_tree[p2+1].LenX = w_tree[p2].LenX - 1;
				}
			}
			w_tree[p2+1].X = w_tree[p2].LenX + x;
			if(stop1 == 0) 
			{
				w_tree[p2+3].LenX = w_tree[p2+1].LenX;
				w_tree[p2+3].X = w_tree[p2+1].X;
			}
			w_tree[p2+2].LenX = w_tree[p2].LenX;


			if(eveny == 0) 
			{
				w_tree[p2].LenY = leny / 2;
				w_tree[p2+2].LenY = w_tree[p2].LenY;
			}
			else 
			{
				if(p1 == 5) 
				{
					w_tree[p2].LenY = (leny - 1) / 2;
					w_tree[p2+2].LenY = w_tree[p2].LenY + 1;
				}
				else 
				{
					w_tree[p2].LenY = (leny + 1) / 2;
					w_tree[p2+2].LenY = w_tree[p2].LenY - 1;
				}
			}
			w_tree[p2+2].Y = w_tree[p2].LenY + y;
			if(stop1 == 0) 
			{
				w_tree[p2+3].LenY = w_tree[p2+2].LenY;
				w_tree[p2+3].Y = w_tree[p2+2].Y;
			}
			w_tree[p2+1].LenY = w_tree[p2].LenY;
		}



		private static void BuildQTree(WaveletTreeNode[] wTreeNodes, QuantumTreeNode[] qTreeNodes)
		{
			QTree16(qTreeNodes,3,wTreeNodes[14].LenX,wTreeNodes[14].LenY,
				wTreeNodes[14].X,wTreeNodes[14].Y, false, false);
			QTree16(qTreeNodes,19,wTreeNodes[4].LenX,wTreeNodes[4].LenY,
				wTreeNodes[4].X,wTreeNodes[4].Y, false, true);
			QTree16(qTreeNodes,48,wTreeNodes[0].LenX,wTreeNodes[0].LenY,
				wTreeNodes[0].X,wTreeNodes[0].Y, false, false);
			QTree16(qTreeNodes,35,wTreeNodes[5].LenX,wTreeNodes[5].LenY,
				wTreeNodes[5].X,wTreeNodes[5].Y, true, false);
			QTree4(qTreeNodes,0,wTreeNodes[19].LenX,wTreeNodes[19].LenY,
				wTreeNodes[19].X,wTreeNodes[19].Y);

			
			
		}

		/********************************************************************/
		/* Routine to obtain subband "x-y locations" for creating wavelets. */
		/********************************************************************/
		static void QTree16(QuantumTreeNode[] qTree, int p, int lenx, int leny, int x, int y, bool rw, bool cl)
		{
			int tempx, temp2x;   /* temporary x values */
			int tempy, temp2y;   /* temporary y values */
			int evenx, eveny;    /* Check length of subband for even or odd */
			
			evenx = lenx % 2;
			eveny = leny % 2;

			if(evenx == 0) 
			{
				tempx = lenx / 2;
				temp2x = tempx;
			}
			else 
			{
				if(cl) 
				{
					temp2x = (lenx + 1) / 2;
					tempx = temp2x - 1;
				}
				else  
				{
					tempx = (lenx + 1) / 2;
					temp2x = tempx - 1;
				}
			}

			if(eveny == 0) 
			{
				tempy = leny / 2;
				temp2y = tempy;
			}
			else 
			{
				if(rw) 
				{
					temp2y = (leny + 1) / 2;
					tempy = temp2y - 1;
				}
				else 
				{
					tempy = (leny + 1) / 2;
					temp2y = tempy - 1;
				}
			}

			evenx = tempx % 2;
			eveny = tempy % 2;

			qTree[p].X = x;
			qTree[p+2].X = x;
			qTree[p].Y = y;
			qTree[p+1].Y = y;
			if(evenx == 0) 
			{
				qTree[p].LenX = tempx / 2;
				qTree[p+1].LenX = qTree[p].LenX;
				qTree[p+2].LenX = qTree[p].LenX;
				qTree[p+3].LenX = qTree[p].LenX;
			}
			else 
			{
				qTree[p].LenX = (tempx + 1) / 2;
				qTree[p+1].LenX = qTree[p].LenX - 1;
				qTree[p+2].LenX = qTree[p].LenX;
				qTree[p+3].LenX = qTree[p+1].LenX;
			}
			qTree[p+1].X = x + qTree[p].LenX;
			qTree[p+3].X = qTree[p+1].X;
			if(eveny == 0) 
			{
				qTree[p].LenY = tempy / 2;
				qTree[p+1].LenY = qTree[p].LenY;
				qTree[p+2].LenY = qTree[p].LenY;
				qTree[p+3].LenY = qTree[p].LenY;
			}
			else 
			{
				qTree[p].LenY = (tempy + 1) / 2;
				qTree[p+1].LenY = qTree[p].LenY;
				qTree[p+2].LenY = qTree[p].LenY - 1;
				qTree[p+3].LenY = qTree[p+2].LenY;
			}
			qTree[p+2].Y = y + qTree[p].LenY;
			qTree[p+3].Y = qTree[p+2].Y;


			evenx = temp2x % 2;

			qTree[p+4].X = x + tempx;
			qTree[p+6].X = qTree[p+4].X;
			qTree[p+4].Y = y;
			qTree[p+5].Y = y;
			qTree[p+6].Y = qTree[p+2].Y;
			qTree[p+7].Y = qTree[p+2].Y;
			qTree[p+4].LenY = qTree[p].LenY;
			qTree[p+5].LenY = qTree[p].LenY;
			qTree[p+6].LenY = qTree[p+2].LenY;
			qTree[p+7].LenY = qTree[p+2].LenY;
			if(evenx == 0) 
			{
				qTree[p+4].LenX = temp2x / 2;
				qTree[p+5].LenX = qTree[p+4].LenX;
				qTree[p+6].LenX = qTree[p+4].LenX;
				qTree[p+7].LenX = qTree[p+4].LenX;
			}
			else 
			{
				qTree[p+5].LenX = (temp2x + 1) / 2;
				qTree[p+4].LenX = qTree[p+5].LenX - 1;
				qTree[p+6].LenX = qTree[p+4].LenX;
				qTree[p+7].LenX = qTree[p+5].LenX;
			}
			qTree[p+5].X = qTree[p+4].X + qTree[p+4].LenX;
			qTree[p+7].X = qTree[p+5].X;


			eveny = temp2y % 2;

			qTree[p+8].X = x;
			qTree[p+9].X = qTree[p+1].X;
			qTree[p+10].X = x;
			qTree[p+11].X = qTree[p+1].X;
			qTree[p+8].Y = y + tempy;
			qTree[p+9].Y = qTree[p+8].Y;
			qTree[p+8].LenX = qTree[p].LenX;
			qTree[p+9].LenX = qTree[p+1].LenX;
			qTree[p+10].LenX = qTree[p].LenX;
			qTree[p+11].LenX = qTree[p+1].LenX;
			if(eveny == 0) 
			{
				qTree[p+8].LenY = temp2y / 2;
				qTree[p+9].LenY = qTree[p+8].LenY;
				qTree[p+10].LenY = qTree[p+8].LenY;
				qTree[p+11].LenY = qTree[p+8].LenY;
			}
			else 
			{
				qTree[p+10].LenY = (temp2y + 1) / 2;
				qTree[p+11].LenY = qTree[p+10].LenY;
				qTree[p+8].LenY = qTree[p+10].LenY - 1;
				qTree[p+9].LenY = qTree[p+8].LenY;
			}
			qTree[p+10].Y = qTree[p+8].Y + qTree[p+8].LenY;
			qTree[p+11].Y = qTree[p+10].Y;


			qTree[p+12].X = qTree[p+4].X;
			qTree[p+13].X = qTree[p+5].X;
			qTree[p+14].X = qTree[p+4].X;
			qTree[p+15].X = qTree[p+5].X;
			qTree[p+12].Y = qTree[p+8].Y;
			qTree[p+13].Y = qTree[p+8].Y;
			qTree[p+14].Y = qTree[p+10].Y;
			qTree[p+15].Y = qTree[p+10].Y;
			qTree[p+12].LenX = qTree[p+4].LenX;
			qTree[p+13].LenX = qTree[p+5].LenX;
			qTree[p+14].LenX = qTree[p+4].LenX;
			qTree[p+15].LenX = qTree[p+5].LenX;
			qTree[p+12].LenY = qTree[p+8].LenY;
			qTree[p+13].LenY = qTree[p+8].LenY;
			qTree[p+14].LenY = qTree[p+10].LenY;
			qTree[p+15].LenY = qTree[p+10].LenY;
		}

/********************************************************************/
		static void QTree4(
			QuantumTreeNode[] QTree,   /* quantization tree structure */
			int p,  /* q_tree location of first subband         */
			/*    in the subband group being calculated */
			int lenx,   /* (temp) subband location and sizes */
			int leny,
			int x,
			int  y)        
		{
			/* Check length of subband for even or odd */		
		
			QTree[p].X = x;
			QTree[p+2].X = x;
			QTree[p].Y = y;
			QTree[p+1].Y = y;
			int evenx = lenx % 2;
			QTree[p].LenX = (lenx + evenx) / 2;
			QTree[p+1].LenX = QTree[p].LenX - evenx;
			QTree[p+2].LenX = QTree[p].LenX;
			QTree[p+3].LenX = QTree[p+evenx].LenX;
			
			QTree[p+1].X = x + QTree[p].LenX;
			QTree[p+3].X = QTree[p+1].X;
			
			
			int eveny = leny % 2;
			QTree[p].LenY = (leny + eveny) / 2;
			QTree[p+1].LenY = QTree[p].LenY;
			QTree[p+2].LenY = QTree[p].LenY - eveny;
			QTree[p+3].LenY = QTree[p+2*eveny].LenY;
		
			QTree[p+2].Y = y + QTree[p].LenY;
			QTree[p+3].Y = QTree[p+2].Y;
		}


	}
}