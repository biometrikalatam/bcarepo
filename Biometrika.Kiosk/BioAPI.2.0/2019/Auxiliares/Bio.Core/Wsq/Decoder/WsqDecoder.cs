using System.IO;

namespace Bio.Core.Wsq.Decoder
{
	/// <summary>
	/// Decodificador, o descompresor de formato Wsq.
	/// </summary>
	public sealed class WsqDecoder : IWsqDecoder
	{
		
		/// <summary>
		/// Constructor default
		/// </summary>
		public WsqDecoder()
		{
		}


		/// <summary>
		/// Decodifica un segmento de memoria codificado en formato wsq
		/// </summary>
		/// <param name="wsq">El arreglo en memoria que contiene la imagen codificada en formato wsq</param>
		/// <param name="width">ancho de la imagen reconstruida</param>
		/// <param name="height">alto de la imagen reconstruida</param>
		/// <param name="raw">imagen en formato raw</param>
		/// <returns>true si el decodificador pudo descomprimir la imagen, false en caso de falla</returns>				
		public bool DecodeMemory (byte[] wsq, out short width, out short height, out byte[] raw)
		{
			width = height = 0;
			raw = null;
			if (wsq == null)
				return false;

			WsqReader reader = new WsqReader(new MemoryStream(wsq));
			Frame frame = reader.ReadFrame();
			width = frame.Width;
			height = frame.Height;
			raw = new byte[frame.ImageSize];
			width = frame.Width;
			height = frame.Height;
			return frame.Decode(raw);
		}

		/// <summary>
		/// Decodifica un stream que viene en formato wsq
		/// </summary>
		/// <param name="inStream">stream de entrada que contiene la imagen wsq</param>
		/// <param name="outStream">stream donde quedar� la imagen en formato raw</param>
		/// <param name="width">ancho de la imagen descomprimida</param>
		/// <param name="height">alto de la imagen descomprimida</param>
		/// <returns>true si pudo decodificar, false si hubo alg�n problema</returns>
		public bool DecodeStream (Stream inStream, Stream outStream, out short width, out short height)
		{
			width = height = 0;
			if (inStream == null || outStream == null)
				return false;
			WsqReader reader = new WsqReader(inStream);
			Frame frame = reader.ReadFrame();
			byte[] raw = new byte[frame.ImageSize];
			if (!frame.Decode(raw))
				return false;
			width = frame.Width;
			height = frame.Height;
			outStream.Write(raw, 0, raw.Length);
			return true;
		}


	}
}
