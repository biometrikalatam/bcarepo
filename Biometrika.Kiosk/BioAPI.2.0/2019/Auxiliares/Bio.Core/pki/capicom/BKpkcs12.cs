//using log4net;
//using System;
//using System.Collections;
//using System.Security.Cryptography;
//using System.Security.Cryptography.X509Certificates;
////using CAPICOM;

//namespace Bio.Core.pki.capicom
//{
//    public class BKpkcs12
//    {
//        private static readonly ILog LOG = LogManager.GetLogger(typeof(BKpkcs12));

//        #region Constructores		

//        public BKpkcs12() { }

//        public BKpkcs12(string path, string psw)
//        {
//            this.certificate = BKpkcs12.BKOpenPFX(path, psw);
//        }

//        #endregion Constructores		

//        #region Public Methods		

//        /// <summary>
//        /// retorna clave para firma
//        /// </summary>
//        /// <param name="path"></param>
//        /// <param name="psw"></param>
//        /// <returns></returns>
//        public static AsymmetricAlgorithm GetPrivateKeyFromPFX(string path, string psw)
//        {
//            X509Certificate2 cRet = null;
//            AsymmetricAlgorithm cAux = null;
//            try
//            {
//                cRet = new X509Certificate2();
//                cRet.Import(path, psw, X509KeyStorageFlags.PersistKeySet);
//                cAux = cRet.PrivateKey;
//            }
//            catch (Exception ex)
//            {
//                LOG.Error("BKpkcs12.GetPrivateKeyFromPFX - " + ex.Message);
//                cAux = null;
//            }
//            return cAux;
//        }

//        /// <summary>
//        /// Retorna certificado
//        /// </summary>
//        /// <param name="path"></param>
//        /// <param name="psw"></param>
//        /// <returns></returns>
//        public static X509Certificate2 BKOpenPFX(string path, string psw)
//        {
//            X509Certificate2 cRet = null;
//            X509Certificate2 cAux = null;
//            try
//            {
//                cRet = new X509Certificate2();
//                cRet.Import(path, psw, X509KeyStorageFlags.PersistKeySet);
               
//            }
//            catch (Exception ex)
//            {
//                LOG.Error("BKpkcs12.GetPrivateKeyFromPFX - " + ex.Message);
//                cRet = null;
//            }
//            return cRet;
//        }


//#endregion Public Methods		

//#region Public Properties

//        public X509Certificate2 Certificate
//        {
//            get { return certificate; }
//            set { certificate = value; }
//        }

//#endregion Public Properties

//#region Private

//        X509Certificate2 certificate;

//#endregion Private


//        //#region Public Methods		
//        //        public static ICertificate BKOpenPFX(string path, string psw)
//        //        {
//        //            ICertificate cRet = null;
//        //            ICertificate cAux = null;
//        //            try
//        //            {
//        //                //Creo un storage de certificado
//        //                CAPICOM.StoreClass sc = new StoreClass();
//        //                //Lo abro en UBICACION = MEMORIA
//        //                sc.Open(CAPICOM.CAPICOM_STORE_LOCATION.CAPICOM_MEMORY_STORE, null, CAPICOM.CAPICOM_STORE_OPEN_MODE.CAPICOM_STORE_OPEN_READ_ONLY);
//        //                //Cago en el Storage el certificado .pfx o .p12
//        //                sc.Load(path.Trim(), psw.Trim(), CAPICOM.CAPICOM_KEY_STORAGE_FLAG.CAPICOM_KEY_STORAGE_DEFAULT);

//        //                //Tomo el Enumerator de certificados del storage
//        //                IEnumerator e = sc.Certificates.GetEnumerator();
//        //                bool bSigue = true;
//        //                while (e.MoveNext() && bSigue)
//        //                {
//        //                    cAux = (ICertificate)e.Current;
//        //                    if (cAux.HasPrivateKey())
//        //                    {
//        //                        cRet = cAux;
//        //                        bSigue = false;
//        //                    }
//        //                }
//        //                sc.Close();
//        //                sc = null;
//        //            }
//        //            catch (Exception ex)
//        //            {
//        //                Console.WriteLine(ex.Message);
//        //                cRet = null;
//        //            }
//        //            return cRet;
//        //        }


//        //#endregion Public Methods		

//        //#region Public Properties
//        //        public ICertificate Certificate
//        //        {
//        //            get { return certificate; }
//        //            set { certificate = value; }
//        //        }
//        //#endregion Public Properties

//        //#region Private
//        //        ICertificate certificate;
//        //#endregion Private
//    }
//}
