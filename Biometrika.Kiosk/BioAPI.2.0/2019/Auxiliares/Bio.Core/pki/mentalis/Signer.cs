using System;
using System.Security.Cryptography;
using System.Text;
using log4net;

namespace Bio.Core.pki.mentalis
{
	/// <summary>
	/// Descripción breve de Signer.
	/// </summary>
	public class Signer
	{
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Signer));

		#region Constructores		
    		public Signer() {}

    		public Signer(string path, string psw)
    		{
    			this.pkcs12 = new Pkcs12(path,psw);
    		}

    		public Signer(Pkcs12 oPKCS12) {
    			this.pkcs12 = oPKCS12;
    		}
		#endregion Constructores		

		#region Methods
    		public static bool GetSignature(ref object signature, string data, string hashMethod,
    									bool enBase64, string path, string psw) {
    			try {
    				// Create a UnicodeEncoder to convert between byte array and string.
    				ASCIIEncoding ByteConverter = new ASCIIEncoding();

    				// Create byte arrays to hold original, encrypted, and decrypted data.
    				byte[] originalData = ByteConverter.GetBytes(data);
    				byte[] signedData = null;

    				AsymmetricAlgorithm alg = Pkcs12.GetPrivateKeyFromPFX(path,psw);
    				if (hashMethod == null || hashMethod.Trim().Equals("SHA1"))
    				{
    					signedData = HashAndSignBytes(originalData, alg, 
    												  new SHA1CryptoServiceProvider(),	
    											      0, originalData.Length );				
    				} else
    				{
    					signedData = HashAndSignBytes(originalData, alg, 
    												  new MD5CryptoServiceProvider(),	
    						                          0, originalData.Length );
    				} 
    				if (enBase64)
    				{
    					signature = Convert.ToBase64String(signedData);
    				} else
    				{
    					signature = signedData;
    				}
    			}
    			catch (Exception ex) {
                    LOG.Error("Signer.GetSignature", ex);
    				return false;
    			}
    			return true;
    		}

            public bool GetSignature(ref object signature, string data, 
    							 string hashMethod,bool enBase64) {
    			try {
    				// Create a UnicodeEncoder to convert between byte array and string.
    				ASCIIEncoding ByteConverter = new ASCIIEncoding();

    				// Create byte arrays to hold original, encrypted, and decrypted data.
    				byte[] originalData = ByteConverter.GetBytes(data);
    				byte[] signedData = null;

    				AsymmetricAlgorithm alg = pkcs12.GetPrivateKeyFromPFX();
    				if (hashMethod == null || hashMethod.Trim().Equals("SHA1")) {
    					signedData = HashAndSignBytes(originalData,alg,
    												  new SHA1CryptoServiceProvider(),	
    												  0, originalData.Length );				
    				} else {
    					signedData = HashAndSignBytes(originalData, alg,
    						  						  new MD5CryptoServiceProvider(),	
    												  0, originalData.Length );
    				} 
    				if (enBase64) {
    					signature = Convert.ToBase64String(signedData);
    				} else {
    					signature = signedData;
    				}
    			}
    			catch (Exception ex) {
                    LOG.Error("Signer.GetSignature", ex);
    				return false;
    			}
    			return true;
    		} 


    		public static byte[] HashAndSignBytes(byte[] DataToSign, AsymmetricAlgorithm Key,  
    											  HashAlgorithm halg, int Index, int Length) {
    			try {   

    				if (Key.SignatureAlgorithm.IndexOf("RSA") >= 0 || Key.SignatureAlgorithm.IndexOf("rsa") >= 0) {
    					RSACryptoServiceProvider RSACryp = (RSACryptoServiceProvider)Key;
    					// Hash and sign the data. Pass a new instance of SHA1CryptoServiceProvider
    					// to specify the use of SHA1 for hashing.
    					return RSACryp.SignData(DataToSign,Index,Length, halg);
    				} else {
    					DSACryptoServiceProvider DSACryp = (DSACryptoServiceProvider)Key;
    					// Hash and sign the data. Pass a new instance of SHA1CryptoServiceProvider
    					// to specify the use of SHA1 for hashing.
    					return DSACryp.SignData(DataToSign,Index,Length);
    				}

    			}
    			catch(Exception ex) {
                    LOG.Error("Signer.HashAndSignBytes", ex);
    				return null;
    			}
    		}

    		public static bool VerifySignature(object signature, string data,
    									   bool enBase64, string hashMethod,
    									   string path, string psw) {
    			try {
    				AsymmetricAlgorithm alg = Pkcs12.GetPublicKeyFromPFX(path,psw);

    				// Create a UnicodeEncoder to convert between byte array and string.
    				ASCIIEncoding ByteConverter = new ASCIIEncoding();

    				// Create byte arrays to hold original, encrypted, and decrypted data.
    				byte[] originalData = ByteConverter.GetBytes(data);
    				byte[] signedData = null;
    				if (enBase64)
    				{
    					signedData = Convert.FromBase64String((string)signature);
    				} else
    				{
    					signedData = (byte[])signature;
    				}
    				
    				if (hashMethod == null || hashMethod.Trim().Equals("SHA1")) {
    					return VerifySignedHash(originalData,signedData,alg,
    									new SHA1CryptoServiceProvider());				
    				} else {
    					return VerifySignedHash(originalData,signedData,alg,
    									new MD5CryptoServiceProvider());				
    				} 
    			} catch (Exception ex) {
                    LOG.Error("Signer.VerifySignature", ex);
    			}
    			return false;
    		}

            public bool VerifySignature(object signature, string data,
    							     bool enBase64, string hashMethod) {
    			try {
    				AsymmetricAlgorithm alg = this.pkcs12.GetPublicKeyFromPFX();

    				// Create a UnicodeEncoder to convert between byte array and string.
    				ASCIIEncoding ByteConverter = new ASCIIEncoding();

    				// Create byte arrays to hold original, encrypted, and decrypted data.
    				byte[] originalData = ByteConverter.GetBytes(data);
    				byte[] signedData = null;
    				if (enBase64) {
    					signedData = Convert.FromBase64String((string)signature);
    				} else {
    					signedData = (byte[])signature;
    				}
    				
    				if (hashMethod == null || hashMethod.Trim().Equals("SHA1")) {
    					return VerifySignedHash(originalData,signedData,alg,
    						new SHA1CryptoServiceProvider());				
    				} else {
    					return VerifySignedHash(originalData,signedData,alg,
    						new MD5CryptoServiceProvider());				
    				} 
    			} catch (Exception ex) {
                    LOG.Error("Signer.VerifySignature", ex);
    			}
    			return false;
    		}

    		public static bool VerifySignedHash(byte[] DataToVerify, byte[] SignedData, 
    									AsymmetricAlgorithm Key, HashAlgorithm halg) {
    			try {
    				if (Key.SignatureAlgorithm.IndexOf("RSA") >= 0 || Key.SignatureAlgorithm.IndexOf("rsa") >= 0) {
    					RSACryptoServiceProvider RSACryp = (RSACryptoServiceProvider)Key;
    					// Hash and sign the data. Pass a new instance of SHA1CryptoServiceProvider
    					// to specify the use of SHA1 for hashing.
    					return RSACryp.VerifyData(DataToVerify, halg, SignedData); 
    				} else {
    					DSACryptoServiceProvider DSACryp = (DSACryptoServiceProvider)Key;
    					// Hash and sign the data. Pass a new instance of SHA1CryptoServiceProvider
    					// to specify the use of SHA1 for hashing.
    					return DSACryp.VerifyData(DataToVerify, SignedData); 
    				}
    			}
    			catch(CryptographicException e) {
    				Console.WriteLine(e.Message);

    				return false;
    			}
    		}


		#endregion Methods

		#region Private
		    Pkcs12 pkcs12;
		#endregion Private

	}
}
