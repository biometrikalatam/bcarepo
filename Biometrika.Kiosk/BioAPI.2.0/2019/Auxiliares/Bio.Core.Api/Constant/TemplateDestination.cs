﻿namespace Bio.Core.Api.Constant
{
    ///<summary>
    /// Para definir el destino de un template. En algunos algoritmos se distingue el template
    /// para enrolamiento del template para verificación.
    ///</summary>
    public class TemplateDestination
    {
        static public int TEMPLATEDESTINATION_ANY = -2;
        static public string STEMPLATEDESTINATION_ANY = "Cualquiera";
        static public int TEMPLATEDESTINATION_All = -1;
        static public string STEMPLATEDESTINATION_All = "Todos";
        static public int TEMPLATEDESTINATION_DEFAULT = 0;
        static public string STEMPLATEDESTINATION_DEFAULT = "Default";

        static public int TEMPLATEDESTINATION_TOVERIFY = 1;
        static public string STEMPLATEDESTINATION_TOVERIFY = "TO_VERIFY";
        static public int TEMPLATEDESTINATION_TOENROLL = 2;
        static public string STEMPLATEDESTINATION_TOENROLL = "TO_ENROLL";
        static public int TEMPLATEDESTINATION_TOIDENTIFY = 3;
        static public string STEMPLATEDESTINATION_TOIDENTIFY = "TO_IDENTIFY";

        static public string GetName(int src)
        {
            switch (src)
            {
                case -2:
                    return STEMPLATEDESTINATION_ANY;
                case -1:
                    return STEMPLATEDESTINATION_All;
                case 0:
                    return STEMPLATEDESTINATION_DEFAULT;
                case 1:
                    return STEMPLATEDESTINATION_TOVERIFY;
                case 2:
                    return STEMPLATEDESTINATION_TOENROLL;
                case 3:
                    return STEMPLATEDESTINATION_TOIDENTIFY;
                default:
                    return "Template Destination no documentado";
            }
        }
    }
}
