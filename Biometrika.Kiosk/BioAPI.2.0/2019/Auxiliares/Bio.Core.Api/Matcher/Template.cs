﻿using System;
using System.Text;
using Bio.Core.Matcher.Interface;
using log4net;

namespace Bio.Core.Matcher
{   
    [Serializable]
    public class Template : ITemplate
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Template));

#region Implementation of ITemplate

        private int _authenticationFactor;

        private int _minutiaeType;

        private int _bodyPart;

        private int _type;

        private byte[] _data;

        private string _additionalData;

        /// <summary>
        /// Tecnologia a la que corresponde el template
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia que contiene Data, incluidos WSQ, RAW, etc.
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Tipo de informacion (data) que contendra el template (desde BIR_TYPE)
        /// </summary>
        public int Type
        {
            get { return _type; }
            set { _type = value; }
        }

        /// <summary>
        /// Parte del cuerpo si es tecnologia biometrica
        /// </summary>
        public int BodyPart
        {
            get { return _bodyPart; }
            set { _bodyPart = value; }
        }

        /// <summary>
        /// Data propia del template o sample
        /// </summary>
        public byte[] Data
        {
            get { return _data; }
            set { _data = value; }
        }

        /// <summary>
        /// Datos adicionales segun sea la data. Por ejemplo si es WSQ, 
        /// se puede colocar las dimensiones de la forma w=512|h=512, o si
        /// es template NEC, el coeficiente coef=xxx. Si hay mas de uno separado 
        /// por pipe "|"
        /// </summary>
        public string AdditionalData
        {
            get { return _additionalData; }
            set { _additionalData = value; }
        }

        /// <summary>
        /// Obtiene la Data en base 64
        /// </summary> 
        public string GetData
        {
            get { return (_data != null && _data.Length > 0) ? Convert.ToBase64String(_data) : null; }
        }

        /// <summary>
        /// Setea la data desde base 64
        /// </summary>
        public string SetData
        {
            set
            {
                try
                {
                    //Hago esto para hacer compatible con versiones anteriores. Luego sacarlo
                    //y hacer conversor para que sea mas rápido
                    string[] part = value.Split('|');

                    if (part.Length >= 2)
                    {
                        byte[] aux = Convert.FromBase64String(part.Length == 2 ? part[1] : part[0]);

                        if (Encoding.ASCII.GetString(aux, 0, 3).Equals("VF5"))
                         {
                             _data = new byte[aux.Length - 8];
                             for (int i = 8; i < aux.Length; i++)
                             {
                                 _data[i - 8] = aux[i];
                             }
                         }
                         else
                         {
                             _data = aux;
                         }

                        if (part.Length > 2) _additionalData = part[0];
                    } else
                    {
                        _data = Convert.FromBase64String(value);                        
                    }
                }
                catch (Exception ex)
                {
                    LOG.Error("Bio.Core.Matcher.Template SetError", ex);
                    _data = null;
                }
            }
        }

#endregion Implementation of ITemplate

#region Added sobre ITemplate para controles internos

        /// <summary>
        /// Agregado apra que identifique el bir y con eso saber la 
        /// Identity para los casos de Identify
        /// </summary>
        private int _id;

        /// <summary>
        /// Id Company que enrolo el template utilizado
        /// </summary>
        private int _companyidenroll;

        /// <summary>
        /// Id user que enrolo el template utilizado
        /// </summary>
        private int _useridenroll;

        /// <summary>
        /// Fuente de verificacion del enrolamiento (Ej: SRCeI)
        /// </summary>
        private string _verificationsource;

        /// <summary>
        /// Agregado apra que identifique el bir y con eso saber la 
        /// Identity para los casos de Identify
        /// </summary>
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// Id Company que enrolo el template utilizado
        /// </summary>
        public int Companyidenroll
        {
            get { return _companyidenroll; }
            set { _companyidenroll = value; }
        }

        /// <summary>
        /// Id user que enrolo el template utilizado
        /// </summary>
        public int Useridenroll
        {
            get { return _useridenroll; }
            set { _useridenroll = value; }
        }

        /// <summary>
        /// Fuente de verificacion del enrolamiento (Ej: SRCeI)
        /// </summary>
        public string Verificationsource
        {
            get { return _verificationsource; }
            set { _verificationsource = value; }
        }

        #endregion Added sobre ITemplate para controles internos

        public void Dispose()
        {
          
        }
    }
}
