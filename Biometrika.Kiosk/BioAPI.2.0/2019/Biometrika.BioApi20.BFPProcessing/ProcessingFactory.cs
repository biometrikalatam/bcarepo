﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Bio.Core.Matcher.Constant;
using Bio.Core.Utils;
using Biometrika.BioApi20.Interfaces;
using log4net;

namespace Biometrika.BioApi20.BFPProcessing
{
    public class ProcessingFactory : IProcessingFactory
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ProcessingFactory));

        /* PROPERTIES */
        public ProcessingFactorySchema Schema { get ; set; }
        public Hashtable HT_MATCHER { get; set; }

        /* EVENTS */
        public event ErrorCallbackDelegate OnErrorEvent;
        public event TimeoutCallbackDelegate OnTimeoutEvent;
        public event TaskDoneCallbackDelegate OnTaskDoneEvent;

        public int Close()
        {
            int ret = 0;
            try
            {

                ret = 0;
            }
            catch (Exception ex)
            {
                LOG.Error("ProcessingFactory.Close Error" + ex.Message);
                ret = -1;
            }
            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sample"></param>
        /// <param name="typeImage">0-JPG desde RAW sin Completar | 1-JPG desde RAW 512x512</param>
        /// <param name="typeReturn">0-Como Drawing.Image | 1- Como byte[]</param>
        /// <param name="sampleout"></param>
        /// <returns></returns>
        public int GetImage(Sample sample, int typeImage, int typeReturn, out Sample sampleout, out Error error)
        {
            error = null;
            sampleout = null;
            int ret = 0;
            byte[] _DATA_FROM_SAMPLE;
            short _W;
            short _H;
            try
            {
                LOG.Debug("ProcessingFactory.GetImage IN...");
                if (sample == null ||
                    (sample.MinutiaeType != MinutiaeType.MINUTIAETYPE_WSQ &&
                    sample.MinutiaeType != MinutiaeType.MINUTIAETYPE_RAW) ||
                    sample.Data == null)
                {
                    LOG.Error("ProcessingFactory.GetImage Error Bad Parameter => " +
                                ((sample != null) ? "MT=" + sample.MinutiaeType.ToString() + "-Data<>NULL => " +
                                    (sample.Data != null).ToString() : "sample NULL"));
                    error = new Error(Error.IERR_BAD_PARAMETER,
                                        "ProcessingFactory.GetImage Sample de input nulo o Data Incorrecta!");
                    OnErrorEvent(Error.IERR_BAD_PARAMETER,
                                        "ProcessingFactory.GetMinutiae Sample de input nulo o Data Incorrecta!", null);
                    return Error.IERR_BAD_PARAMETER;
                }

                LOG.Debug("ProcessingFactory.GetImage Si es WSQ => Descomprimo sino asigno RAW...");
                if (sample.MinutiaeType != MinutiaeType.MINUTIAETYPE_WSQ)
                {
                    LOG.Debug("ProcessingFactory.GetImage Decode WSQ...");
                    Bio.Core.Wsq.Decoder.WsqDecoder decoder = new Bio.Core.Wsq.Decoder.WsqDecoder();
                    bool res = decoder.DecodeMemory((byte[])sample.Data, out _W, out _H, out _DATA_FROM_SAMPLE);
                    LOG.Debug("ProcessingFactory.GetImage RAW (WxH) = (" + _W + "x" + _H + ")");
                }
                else
                {
                    LOG.Debug("ProcessingFactory.GetImage Asiganndo RAW original...");
                    _DATA_FROM_SAMPLE = (byte[])sample.Data;
                    _W = (short)sample.SampleWith;
                    _H = (short)sample.SampleHeight;
                    LOG.Debug("ProcessingFactory.GetImage RAW (WxH) = (" + _W + "x" + _H + ")");
                }

                //Chequea si debe ser completo 512x512, lo completa si hace falta
                LOG.Debug("ProcessingFactory.GetImage Check si completa...");
                if (typeImage == 1)
                {
                    if (_W < 512 || _H < 512)
                    {
                        LOG.Debug("ProcessingFactory.GetImage Completando...");
                        _DATA_FROM_SAMPLE = Bio.Core.Imaging.ImageProcessor.FillRaw(_DATA_FROM_SAMPLE, _W, _H, 512, 512);
                    }
                }

                //Devuelvo Imagen desde el data generada
                object objReturn;
                LOG.Debug("ProcessingFactory.GetImage Creando Image...");
                Bitmap _Jpg = Bio.Core.Imaging.ImageProcessor.RawToBitmap(_DATA_FROM_SAMPLE, _W, _H);
                LOG.Debug("ProcessingFactory.GetImage Evaluando tipo de retorno..."); 
                if (typeReturn == 0) //Retorna jpg como esta
                {
                    LOG.Debug("ProcessingFactory.GetImage Return Image");
                    objReturn = _Jpg;
                } else //Pasa jpg a byte[]
                {
                    LOG.Debug("ProcessingFactory.GetImage Return byte[] de jpg");
                    byte[] byJpg;
                    using (var ms = new System.IO.MemoryStream())
                    {
                        _Jpg.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        byJpg = ms.ToArray();
                    }
                    objReturn = byJpg;
                }

                LOG.Debug("ProcessingFactory.GetImage Creando Sample de retorno...");
                sampleout = new Sample(sample.AuthenticationFactor, MinutiaeType.MINUTIAETYPE_JPG,
                                       sample.BodyPart, 1, _W, _H, objReturn);

            }
            catch (Exception ex)
            {
                error = new Error(-1, "ProcessingFactory.Close Error" + ex.Message);
                LOG.Error("ProcessingFactory.Close Error" + ex.Message);
                ret = -1;
                OnErrorEvent(-1, "ProcessingFactory.Close Error" + ex.Message, null);
            }
            LOG.Debug("ProcessingFactory.GetImage OUT!");
            return ret;
        }

        /// <summary>
        /// Dado un sample, si puede, extrae las minucias
        /// </summary>
        /// <param name="sample"></param>
        /// <param name="mtout"></param>
        /// <param name="purpose">0-Verify | 2-Enroll</param>
        /// <param name="sampleout"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public int GetMinutiae(Sample sample, MinutiaeType mtout, int purpose, out Sample sampleout, out Error error)
        {
            error = null;
            sampleout = null;
            int ret = 0;
            byte[] _DATA_FROM_SAMPLE;
            try
            {
                LOG.Debug("ProcessingFactory.GetMinutiae IN...");
                if (sample == null ||
                    (sample.MinutiaeType != MinutiaeType.MINUTIAETYPE_WSQ &&
                    sample.MinutiaeType != MinutiaeType.MINUTIAETYPE_RAW &&
                    sample.MinutiaeType != MinutiaeType.MINUTIAETYPE_JPG) ||
                    sample.Data == null)
                {
                    LOG.Error("ProcessingFactory.GetMinutiae Error Bad Parameter => " +
                                ((sample != null) ? "MT=" + sample.MinutiaeType.ToString() + "-Data<>NULL => " + 
                                 (sample.Data!=null).ToString() : "sample NULL"));
                    error = new Error(Error.IERR_BAD_PARAMETER,
                                      "ProcessingFactory.GetMinutiae Sample de input nulo o Data Incorrecta!");
                    OnErrorEvent(Error.IERR_BAD_PARAMETER,
                                       "ProcessingFactory.GetMinutiae Sample de input nulo o Data Incorrecta!", null);
                    return Error.IERR_BAD_PARAMETER;
                }

                if (sample.MinutiaeType != MinutiaeType.MINUTIAETYPE_WSQ)
                {
                    Bio.Core.Wsq.Decoder.WsqDecoder decoder = new Bio.Core.Wsq.Decoder.WsqDecoder();
                    short _ws, _hs;
                    bool res = decoder.DecodeMemory((byte[])sample.Data,out _ws, out _hs, out _DATA_FROM_SAMPLE);
                } else
                {
                    _DATA_FROM_SAMPLE = (byte[])sample.Data;
                }

                LOG.Debug("ProcessingFactory.GetMinutiae CRecuperando Matcher con MT=" + mtout + "...");
                IExtractor _Extractor = (IExtractor)HT_MATCHER[mtout.ToString()];
                LOG.Debug("ProcessingFactory.GetMinutiae MAtcher Recuperado _Extractor != null? => " + (_Extractor != null).ToString());

                if (_Extractor == null)
                {
                    ret = Error.IERR_EXTRACTING;
                    error = new Error(ret, "ProcessingFactory.GetMinutiae Error Getting Extractor [_Extractor == null]");
                    LOG.Error("ProcessingFactory.GetMinutiae Error Getting Extractor [_Extractor == null]");
                    OnErrorEvent(ret, "ProcessingFactory.GetMinutiae Error Getting Extractor [_Extractor == null]", null);
                } else //Extraigo
                {
                    LOG.Debug("ProcessingFactory.GetMinutiae _Extractor.Extract IN...");
                    ret = _Extractor.Extract(sample, purpose, out sampleout);
                    LOG.Debug("ProcessingFactory.GetMinutiae _Extractor.Extract Out => ret = " + ret); 
                    if (ret == 0)
                    {
                        OnTaskDoneEvent(0, null, sampleout);
                        ret = 0;
                    } else
                    {
                        error = new Error(ret, "ProcessingFactory.GetMinutiae Error Extracting");
                        LOG.Error("ProcessingFactory.GetMinutiae Error Extracting [" + ret + "]");
                        OnErrorEvent(ret, "ProcessingFactory.GetMinutiae Error Extracting", null);
                    }
                }
            }
            catch (Exception ex)
            {
                error = new Error(-1, "ProcessingFactory.GetMinutiae Error" + ex.Message);
                LOG.Error("ProcessingFactory.GetMinutiae Error" + ex.Message);
                ret = -1;
                OnErrorEvent(-1, "ProcessingFactory.GetMinutiae Error" + ex.Message, null);
            }
            return ret;
        }

        /// <summary>
        /// Dado un RAW lo comeplta o no a 512x512 y convierte a WSQ
        /// </summary>
        /// <param name="sample"></param>
        /// <param name="complete512"></param>
        /// <param name="sampleout"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public int GetWSQ(Sample sample, bool complete512, out Sample sampleout, out Error error)
        {
            error = null;
            sampleout = null;
            int ret = 0;
            byte[] _RAW;
            short _W, _H;
            try
            {
                LOG.Debug("ProcessingFactory.GetWSQ IN...");
                if (sample == null)
                {
                    error = new Error(Error.IERR_BAD_PARAMETER,
                                      "ProcessingFactory.GetWSQ Sample de input nulo!");
                    OnErrorEvent(Error.IERR_BAD_PARAMETER, 
                                       "ProcessingFactory.GetWSQ Sample de input nulo!", null);
                    return Error.IERR_BAD_PARAMETER;
                }

                LOG.Debug("ProcessingFactory.GetWSQ Check si completa imagen => Size IN (w x h) = " +
                          "(" + sample.SampleWith + " x " + sample.SampleHeight + ") y " +
                          " complete512=" + complete512.ToString());
                if (complete512 && (sample.SampleWith < 512 || sample.SampleHeight < 512))
                {
                    LOG.Debug("ProcessingFactory.GetWSQ Completando 512x512...");
                    _RAW = Bio.Core.Imaging.ImageProcessor.FillRaw((byte[])sample.Data,
                                        sample.SampleWith, sample.SampleHeight, 512, 512);
                    _W = 512;
                    _H = 512;
                    if (_RAW == null)
                    {
                        error = new Error(Error.IERR_NORMALIZING_DATA,
                                      "ProcessingFactory.GetWSQ Error Completando 512x512");
                        OnErrorEvent(Error.IERR_NORMALIZING_DATA, "ProcessingFactory.GetWSQ Error Completando 512x512!", null);
                        return Error.IERR_NORMALIZING_DATA;
                    }
                } else
                {
                    _RAW = (byte[])sample.Data;
                    _W = (short)sample.SampleWith;
                    _H = (short)sample.SampleHeight;
                }

                LOG.Debug("ProcessingFactory.GetWSQ GEt WSQ...");
                Bio.Core.Wsq.Encoder.WsqEncoder encoder = new Bio.Core.Wsq.Encoder.WsqEncoder();
                byte[] _WSQ;
                if (!encoder.EncodeMemory(_RAW, _W, _H, out _WSQ))
                {
                    error = new Error(Error.IERR_NORMALIZING_DATA,
                                      "ProcessingFactory.GetWSQ Error Getting WSQ");
                    OnErrorEvent(Error.IERR_NORMALIZING_DATA, "ProcessingFactory.GetWSQ Error Getting WSQ!", null);
                    return Error.IERR_NORMALIZING_DATA;
                } else {
                    sampleout = new Sample(sample.AuthenticationFactor, MinutiaeType.MINUTIAETYPE_WSQ, sample.BodyPart,
                                           sample.Operation, _W, _H, _WSQ);
                }
                OnTaskDoneEvent(0, null, sampleout);
                ret = 0;
            }
            catch (Exception ex)
            {
                error = new Error(-1, "ProcessingFactory.GetWSQ Error" + ex.Message);
                LOG.Error("ProcessingFactory.GetWSQ Error" + ex.Message);
                ret = -1;
                OnErrorEvent(-1, "ProcessingFactory.GetWSQ Error" + ex.Message, null);
            }
            LOG.Debug("ProcessingFactory.GetWSQ OUT!");
            return ret;
        }

        public int Initialization()
        {
            int ret = 0;
            try
            {
                //1.- LEvanta y procesa el Schema, sino lo crea.
                ret = ProcesaSchema();

                if (ret < 0)
                {
                    LOG.Error("ProcessingFactory.Initialization Error Procesando Schema = " + ret.ToString());
                    OnErrorEvent(ret, "ProcessingFactory.Initialization Error Procesando Schema = " + ret.ToString(), null);
                    return ret;
                }

                //2.- Instancia los BFP configurados y los carga en la lista y Dictionary/Hashtable
                ret = LoadExtractors();

                if (ret < 0)
                {
                    LOG.Error("ProcessingFactory.Initialization Error Cargando Extractors = " + ret.ToString());
                    OnErrorEvent(ret, "ProcessingFactory.Initialization Error Cargando BFPs = " + ret.ToString(), null);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("ProcessingFactory.Initialization Error" + ex.Message);
                ret = -1;
                OnErrorEvent(-1, "ProcessingFactory.Initialization Error" + ex.Message, null);
            }
            return ret;
        }

        #region Private

        /// <summary>
        /// Lee Schema o lo crea si no existe. Lo caraga en Propiedad
        /// </summary>
        /// <returns></returns>
        private int ProcesaSchema()
        {
            int ret = 0;
            try
            {
                LOG.Debug("ProcessingFactory.ProcesaSchema IN. Chequeando Biometrika.BioApi20.BSP.ProcessingFactorySchema.xml...");
                if (!System.IO.File.Exists("Biometrika.BioApi20.BSP.ProcessingFactorySchema.xml"))
                {
                    LOG.Debug("Biometrika.BioApi20.BSP.ProcessingFactory.ProcesaSchema Archivo no existe! Creando...");
                    this.Schema = new ProcessingFactorySchema();
                    this.Schema.TimeoutProcessing = 5000;
                    ExtractorItem item = new ExtractorItem();
                    item.AuthenticationFactor = 2; //Fingerprint
                    item.MinutiaeType = 14; //ISO
                    item.Threshold = 70; //Umbral
                    item.TimeoutProcessing = 0;
                    item.PathExtractor = "Biometrika.BioApi20.BFPProcessingExtractor.ISO";
                    item.Parameters = "Param1=Value1|PAram2=Value2";
                    this.Schema.listExtractors = new List<ExtractorItem>();
                    this.Schema.listExtractors.Add(item);
                    if (SerializeHelper.SerializeToFile(this.Schema, "Biometrika.BioApi20.BSP.ProcessingFactorySchema.xml"))
                    {
                        LOG.Debug("Biometrika.BioApi20.BSP.ProcessingFactorySchema.ProcesaSchema Creado!");
                    }
                    else
                    {
                        LOG.Error("Biometrika.BioApi20.BSP.ProcessingFactorySchema.ProcesaSchema Error Creando Biometrika.BioApi20.BSP.ProcessingFactorySchema.xml!");
                    }

                }
                else
                {
                    this.Schema = SerializeHelper.DeserializeFromFile<ProcessingFactorySchema>("Biometrika.BioApi20.BSP.ProcessingFactorySchema.xml");
                    LOG.Debug("Biometrika.BioApi20.BSP.ProcessingFactorySchema.ProcesaSchema Leido! Schema!=null => " + (this.Schema != null).ToString());
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Biometrika.BioApi20.BSP.ProcessingFactorySchema.ProcesaSchema Error = " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Dado el Schema levantado, se instancian los sensores configurados y se levantan a memoria.
        /// </summary>
        /// <returns></returns>
        private int LoadExtractors()
        {
            int ret = 0;
            try
            {
                LOG.Debug("Biometrika.BioApi20.BSP.ProcessingFactory.LoadExtractors IN. Procesando Schema...");
                if (this.Schema == null)
                {
                    ret = Bio.Core.Constant.Errors.IERR_BAD_SERIALIZER_CONFIG_FILE;
                    LOG.Error("Biometrika.BioApi20.BSP.ProcessingFactory.LoadExtractors Schema nulo! Aborta [Err=" +
                        Bio.Core.Constant.Errors.IERR_BAD_SERIALIZER_CONFIG_FILE.ToString() + "]");
                }
                else
                {
                    LOG.Debug("Biometrika.BioApi20.BSP.ProcessingFactory.LoadExtractors - Proceso Schema: listExtractors.length=" +
                        ((Schema.listExtractors != null) ? Schema.listExtractors.Count.ToString() : "NULL"));

                    this.HT_MATCHER = new Hashtable();
                    //this.htSensor = new Hashtable();
                    foreach (ExtractorItem item in Schema.listExtractors)
                    {
                        LOG.Debug("Biometrika.BioApi20.BSP.ProcessingFactory.LoadExtractors Cargando: " + item.PathExtractor);
                        try
                        {
                            Assembly a = Assembly.Load(item.PathExtractor);
                            foreach (Type type in a.GetTypes())
                            {
                                if (type.IsClass && typeof(IExtractor).IsAssignableFrom(type)) // && type.IsNested)
                                {
                                    IExtractor currentExtractor = (IExtractor)Activator.CreateInstance(type);
                                    this.HT_MATCHER.Add(item.MinutiaeType, currentExtractor);
                                    LOG.Debug("Biometrika.BioApi20.BSP.ProcessingFactory.LoadExtractors    ==> Agregado Extractor de MT=" + item.MinutiaeType);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LOG.Error("Biometrika.BioApi20.BSP.ProcessingFactory.LoadExtractors Error loading " + item.PathExtractor + " => " + ex.Message);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Biometrika.BioApi20.BSP.ProcessingFactory.LoadExtractors Error = " + ex.Message);
            }
            LOG.Debug("Biometrika.BioApi20.BSP.ProcessingFactory.LoadExtractors OUT!");
            return ret;
        }

        #endregion Private
    }
}
