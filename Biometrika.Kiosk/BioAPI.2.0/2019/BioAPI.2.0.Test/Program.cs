﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biometrika.Standards;

namespace BioAPI._2._0.Test
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //byte[] bdbBiometricType = new byte[3];
            //bdbBiometricType[0] = 0;
            //bdbBiometricType[1] = 0;
            //bdbBiometricType[2] = 8;
            //UInt64 ix1 = BitConverter.ToUInt64(bdbBiometricType, 2);

            //ix1 = 0;
            //bdbBiometricType[0] = 8;
            //bdbBiometricType[1] = 0;
            //bdbBiometricType[2] = 0;
            //ix1 = BitConverter.ToUInt64(bdbBiometricType, 0);

            //byte[] bySHSecurityOption = new byte[1];
            //bySHSecurityOption = BitConverter.GetBytes(0);
            //bySHSecurityOption = BitConverter.GetBytes(1);
            //bySHSecurityOption = BitConverter.GetBytes(2);
            //bySHSecurityOption = BitConverter.GetBytes(3);


            //byte[] by = BitConverter.GetBytes((int)BDBTypes.ISO_WSQ);
            //int ix = BitConverter.ToInt32(by,0);
            //by = BitConverter.GetBytes((int)(BDBTypes.ISO_MINUTIAE_19794_2));
            //ix = BitConverter.ToInt32(by, 0);
            //by = BitConverter.GetBytes((int)(BDBTypes.ANSI_MINUTIAE_378));
            //ix = BitConverter.ToInt32(by, 0);
            //by = BitConverter.GetBytes(Convert.ToInt32(Owners.ANSI));
            //ix = BitConverter.ToInt32(by, 0);


            //by = BitConverter.GetBytes(450000);
            //int i = BitConverter.ToInt32(by, 0);
            //by = BitConverter.GetBytes(8);
            //i = BitConverter.ToInt32(by, 0);
            //short s = 127;
            //by = BitConverter.GetBytes(s);
            //i = BitConverter.ToInt16(by, 0);

            //uint patronFormat = uint.Parse("0x0101", System.Globalization.NumberStyles.HexNumber);

            //int i = 0;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
