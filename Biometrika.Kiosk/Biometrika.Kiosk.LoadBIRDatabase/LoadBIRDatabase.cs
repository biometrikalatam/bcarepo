﻿using Bio.Core.Api.Constant;
using Bio.Core.Matcher;
using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.LoadBIRDatabase.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using Dapper;
using System.Text;
using System.Threading.Tasks;
using Biometrika.Kiosk.Common.Model;

namespace Biometrika.Kiosk.LoadBIRDatabase
{
    /// <summary>
    /// Implementa ILoadBIRs, para cargar los templates biometricos para identificaciones. 
    /// Por ahora está implementado solo templates verifinger, conectado directo a la BD.
    /// En _Parameters están los datos de conexión y demás datos para operar.
    /// Siempre por definicion los archivos de configuracion de estas implementaciones
    /// tienen el mismo nombre del Assembly ".cfg".
    /// </summary>
    public class LoadBIRDatabase : ILoadBIRs 
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(LoadBIRDatabase));

        SqlConnection _sqlDatabase;

        private bool _Initialized = false;
        private DynamicData _Parameters;

        public DynamicData Parameters
        {
            get {
                return _Parameters;
            }

            set {
                _Parameters = value;
            }
        }

        public bool Initialized
        {
            get {
                return this._Initialized;
            }

            set {
                this._Initialized = value;
            }
        }

        /// <summary>
        /// Dado el string de conexión y la sentencia SQL definida, se recuperan los datos 
        /// y se llena una List<Bio.Core.Matcher.Interface.ITemplate> templates para ser 
        /// utulizados en el Matcher correspondiente, al inicio del kiosko, si está habilitada7
        /// esta tecnología de identificación.
        /// </summary>
        /// <param name="templates"></param>
        /// <returns></returns>
        public int DoLoadBIRs(out List<Bio.Core.Matcher.Interface.ITemplate> templates)
        {
            templates = null;
            int ret = 0;
            try
            {
                LOG.Debug("LoadBIRDatabase.DoLoadBIRs - IN...");
                List<Bio.Core.Matcher.Interface.ITemplate> objTemplates = new List<Bio.Core.Matcher.Interface.ITemplate>();

                IList<DatabaseRow> DatabaseRowList = null;

                string sql = (string)_Parameters.GetItem("QueryCmd");
                /*
                    public string id { get; set; }
                    public int authenticationfactor { get; set; }
                    public int minutiaetype { get; set; }
                    public int bodypart { get; set; }
                    public string data { get; set; }
                */
                //sql = "  select [id],[authenticationfactor],[minutiaetype],[bodypart],[data]" +
                //            " from[dbo].[bp_bir] where minutiaetype = 7";


                LOG.Debug("LoadBIRDatabase.DoLoadBIRs - ConectionString = " + (string)_Parameters.GetItem("ConectionString"));
                _sqlDatabase = new SqlConnection((string)_Parameters.GetItem("ConectionString"));
                LOG.Debug("LoadBIRDatabase.DoLoadBIRs - Query para getBirs" + sql);
                DatabaseRowList = _sqlDatabase.Query<DatabaseRow>(sql).ToList();

                if (DatabaseRowList == null)
                {
                    LOG.Warn("LoadBIRDatabase.DoLoadBIRs - NO Se han recuperado huella! => No funcionará identificación con huella!");
                }
                else
                {
                    LOG.Info("LoadBIRDatabase.DoLoadBIRs - Se ha recuperado huellas => Cantidad = " + DatabaseRowList.Count.ToString());
                    for (int i = 0; i < DatabaseRowList.Count; i++)
                    {
                        Template objTemplate = new Template();
                        objTemplate.Companyidenroll = 1;
                        objTemplate.SetData = DatabaseRowList[i].data;
                        objTemplate.MinutiaeType = DatabaseRowList[i].minutiaetype;
                        objTemplate.BodyPart = DatabaseRowList[i].bodypart;
                        objTemplate.AuthenticationFactor = DatabaseRowList[i].authenticationfactor;
                        objTemplate.Id = DatabaseRowList[i].id;
                        objTemplate.AdditionalData = DatabaseRowList[i].valueid;
                        objTemplates.Add(objTemplate);
                    }
                    templates = objTemplates;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("LoadBIRDatabase.DoLoadBIRs - Ex Error: " + ex.Message);
            }
            LOG.Debug("LoadBIRDatabase.DoLoadBIRs - OUT! - Q templates = " + (templates==null?"Null":templates.Count.ToString()));
            return ret;
        }

        /// <summary>
        /// Lee el archivo de configuración, y si no esxiste crea uno de ejemplo. 
        /// Dentro de los parametros existentes deben incluirse:
        ///   - ConectionString: String de conexion a la BD
        ///   - QueryCmd: Sentencia SQL para recuperar la info, devolviendo los datos necesarios en el modelo
        ///     de clase DatabaseRow, para ser parseado por Dapper. Esto puede cambiar segun la fuente desde
        ///     donde se consigan los datos. Aqui se considera la recolección desde BioPortal
        /// </summary>
        /// <returns></returns>
        public int Initialize()
        {
            int ret = 0;
            try
            {
                if (!System.IO.File.Exists("Biometrika.Kiosk.LoadBIRDatabase.cfg"))
                {
                    _Parameters = new DynamicData();
                    //_Parameters.Add("ConectionString", "data source=DESKTOP-EVPUDI7;initial catalog=bp_v5.9;persist security info=False;user id=sa;password=Sa.admin;workstation id=DESKTOP-EVPUDI7;packet size=4096");
                    //_Parameters.Add("QueryCmd",
                    //        "SELECT TOP (200) bpiden.id AS id, bpiden.valueid, bpr.authenticationfactor, bpr.minutiaetype, bpr.bodypart, bpr.data " + 
                    //        "  FROM bp_bir AS bpr INNER JOIN bp_identity AS bpiden ON bpr.identid = bpiden.id " +
                    //        " WHERE (bpr.minutiaetype = 7) AND (bpiden.companyidenroll = 7) AND bpiden.valueid = '21284415-2'");
                    _Parameters.AddItem("ConectionString", "data source=DESKTOP-EVPUDI7;initial catalog=WEBASSISTANCEEIM;persist security info=False;user id=sa;password=Sa.admin;workstation id=DESKTOP-EVPUDI7;packet size=4096");
                    _Parameters.AddItem("QueryCmd",
                            "SELECT id, additionaldata as valueid, authenticationfactor, minutiaetype, bodypart, data " +
                            "  FROM wa_BirSmall " +
                            " WHERE (minutiaetype = 7)");

                    //_Parameters.Add("", "");
                    //_Parameters.Add("", "");
                    //_Parameters.Add("", "");
                    if (!Common.Utils.SerializeHelper.SerializeToFile(_Parameters, "Biometrika.Kiosk.LoadBIRDatabase.cfg"))
                    {
                        LOG.Warn("LoadBIRDatabase.Initialize - No grabo condif en disco (Biometrika.Kiosk.LoadBIRDatabase.cfg)");
                    }
                }
                else
                {
                    _Parameters = Common.Utils.SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.Kiosk.LoadBIRDatabase.cfg");
                    _Parameters.Initialize();
                }

                if (_Parameters == null)
                {
                    LOG.Fatal("LoadBIRDatabase.Initialize - Error leyendo LoadBIRDatabase!");
                    return Bio.Core.Api.Constant.Errors.IERR_DESERIALIZING_DATA;
                } else
                {
                    _Initialized = true;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }
    }
}
