﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.LoadBIRDatabase.Models
{
    /// <summary>
    /// Modelo para recolección de datos en BD. Puede variar en cada implementacion, pero si 
    /// lo hace hay que revisar si afecta al kiosko.
    /// </summary>
    public class DatabaseRow
    {
        public int id { get; set; }
        public string valueid { get; set; }
        public int authenticationfactor { get; set; }
        public int minutiaetype { get; set; }
        public int bodypart { get; set; }
        public string data { get; set; }
    }

    public class Models
    {
    }
}
