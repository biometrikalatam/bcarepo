﻿namespace Biometrika.Screen.BancoFalabella
{
    partial class frmScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) 
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labAutorizador = new System.Windows.Forms.Label();
            this.labUnidad = new System.Windows.Forms.Label();
            this.labDateEnd = new System.Windows.Forms.Label();
            this.labDateInit = new System.Windows.Forms.Label();
            this.labMeetingName = new System.Windows.Forms.Label();
            this.labNombre = new System.Windows.Forms.Label();
            this.labMessage = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.picFoto = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.picResultAccess = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResultAccess)).BeginInit();
            this.SuspendLayout();
            // 
            // labAutorizador
            // 
            this.labAutorizador.BackColor = System.Drawing.Color.Transparent;
            this.labAutorizador.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labAutorizador.ForeColor = System.Drawing.Color.White;
            this.labAutorizador.Location = new System.Drawing.Point(92, 1286);
            this.labAutorizador.Name = "labAutorizador";
            this.labAutorizador.Size = new System.Drawing.Size(693, 73);
            this.labAutorizador.TabIndex = 25;
            this.labAutorizador.Text = "Jose Perez";
            // 
            // labUnidad
            // 
            this.labUnidad.BackColor = System.Drawing.Color.Transparent;
            this.labUnidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUnidad.ForeColor = System.Drawing.Color.White;
            this.labUnidad.Location = new System.Drawing.Point(92, 1142);
            this.labUnidad.Name = "labUnidad";
            this.labUnidad.Size = new System.Drawing.Size(693, 73);
            this.labUnidad.TabIndex = 24;
            this.labUnidad.Text = "Cafetería";
            // 
            // labDateEnd
            // 
            this.labDateEnd.BackColor = System.Drawing.Color.Transparent;
            this.labDateEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDateEnd.ForeColor = System.Drawing.Color.White;
            this.labDateEnd.Location = new System.Drawing.Point(89, 988);
            this.labDateEnd.Name = "labDateEnd";
            this.labDateEnd.Size = new System.Drawing.Size(730, 73);
            this.labDateEnd.TabIndex = 23;
            this.labDateEnd.Text = "Fin: 22/10/2022 11:35";
            // 
            // labDateInit
            // 
            this.labDateInit.BackColor = System.Drawing.Color.Transparent;
            this.labDateInit.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDateInit.ForeColor = System.Drawing.Color.White;
            this.labDateInit.Location = new System.Drawing.Point(89, 915);
            this.labDateInit.Name = "labDateInit";
            this.labDateInit.Size = new System.Drawing.Size(730, 73);
            this.labDateInit.TabIndex = 22;
            this.labDateInit.Text = "Ini.: 22/10/2022 10:35";
            // 
            // labMeetingName
            // 
            this.labMeetingName.BackColor = System.Drawing.Color.Transparent;
            this.labMeetingName.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMeetingName.ForeColor = System.Drawing.Color.White;
            this.labMeetingName.Location = new System.Drawing.Point(92, 772);
            this.labMeetingName.Name = "labMeetingName";
            this.labMeetingName.Size = new System.Drawing.Size(705, 73);
            this.labMeetingName.TabIndex = 21;
            this.labMeetingName.Text = "Proyecto Totem";
            // 
            // labNombre
            // 
            this.labNombre.BackColor = System.Drawing.Color.Transparent;
            this.labNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNombre.ForeColor = System.Drawing.Color.White;
            this.labNombre.Location = new System.Drawing.Point(92, 552);
            this.labNombre.Name = "labNombre";
            this.labNombre.Size = new System.Drawing.Size(705, 151);
            this.labNombre.TabIndex = 20;
            this.labNombre.Text = "Gustavo Gerardo Suhit Gallucci";
            // 
            // labMessage
            // 
            this.labMessage.BackColor = System.Drawing.Color.Transparent;
            this.labMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMessage.ForeColor = System.Drawing.Color.Yellow;
            this.labMessage.Location = new System.Drawing.Point(312, 270);
            this.labMessage.Name = "labMessage";
            this.labMessage.Size = new System.Drawing.Size(473, 256);
            this.labMessage.TabIndex = 19;
            this.labMessage.Text = "Acceso Correcto!";
            this.labMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gainsboro;
            this.label5.Location = new System.Drawing.Point(95, 1231);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(295, 55);
            this.label5.TabIndex = 18;
            this.label5.Text = "Autorizador:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gainsboro;
            this.label4.Location = new System.Drawing.Point(95, 1087);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(197, 55);
            this.label4.TabIndex = 17;
            this.label4.Text = "Unidad:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gainsboro;
            this.label3.Location = new System.Drawing.Point(95, 860);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(202, 55);
            this.label3.TabIndex = 16;
            this.label3.Text = "Fechas:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gainsboro;
            this.label2.Location = new System.Drawing.Point(92, 717);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(225, 55);
            this.label2.TabIndex = 15;
            this.label2.Text = "Reunión:";
            // 
            // picFoto
            // 
            this.picFoto.BackColor = System.Drawing.Color.Transparent;
            this.picFoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFoto.Image = global::Biometrika.Screen.BancoFalabella.Properties.Resources.ImageFondoResultNoFoto;
            this.picFoto.Location = new System.Drawing.Point(105, 270);
            this.picFoto.Name = "picFoto";
            this.picFoto.Size = new System.Drawing.Size(201, 199);
            this.picFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFoto.TabIndex = 14;
            this.picFoto.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gainsboro;
            this.label1.Location = new System.Drawing.Point(92, 497);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 55);
            this.label1.TabIndex = 13;
            this.label1.Text = "Nombre:";
            // 
            // picResultAccess
            // 
            this.picResultAccess.BackColor = System.Drawing.Color.Transparent;
            this.picResultAccess.Image = global::Biometrika.Screen.BancoFalabella.Properties.Resources.ImagePicResultActionInfo;
            this.picResultAccess.Location = new System.Drawing.Point(349, 203);
            this.picResultAccess.Name = "picResultAccess";
            this.picResultAccess.Size = new System.Drawing.Size(357, 79);
            this.picResultAccess.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResultAccess.TabIndex = 26;
            this.picResultAccess.TabStop = false;
            // 
            // frmScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Biometrika.Screen.BancoFalabella.Properties.Resources.feedback_cliente_25;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(900, 1600);
            this.Controls.Add(this.picResultAccess);
            this.Controls.Add(this.labAutorizador);
            this.Controls.Add(this.labUnidad);
            this.Controls.Add(this.labDateEnd);
            this.Controls.Add(this.labDateInit);
            this.Controls.Add(this.labMeetingName);
            this.Controls.Add(this.labNombre);
            this.Controls.Add(this.labMessage);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.picFoto);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmScreen";
            this.Text = "frmScreen";
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResultAccess)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labAutorizador;
        private System.Windows.Forms.Label labUnidad;
        private System.Windows.Forms.Label labDateEnd;
        private System.Windows.Forms.Label labDateInit;
        private System.Windows.Forms.Label labMeetingName;
        private System.Windows.Forms.Label labNombre;
        private System.Windows.Forms.Label labMessage;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox picFoto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picResultAccess;
    }
}