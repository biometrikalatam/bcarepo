﻿using Biometrika.Kiosk.Common.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Screen.BancoFalabella
{
    public partial class frmScreen : Form
    {
        public frmScreen()
        {
            InitializeComponent();
        }
        public void SetParameters(DynamicData parameters)
        {
            try
            {
                /* 
                 
                returns.AddItem("DateOfBirth", oResponse.Data.Identity.NacDate.ToString());
                returns.AddItem("Code", oResponse.Data.CanAccess);
                returns.AddItem("EventType", oResponse.Data.EvntType);
                //Datos Meet
                returns.AddItem("MeetDate", oResponse.Data.Meet.StartDate.ToString("dd/MM/yyyy"));
                returns.AddItem("MeetStartTime", oResponse.Data.Meet.StartTime);
                returns.AddItem("MeetEndTime", oResponse.Data.Meet.EndTime);
                returns.AddItem("MeetAuthorizer", oResponse.Data.Meet.Authorizer);
                returns.AddItem("MeetUnity", oResponse.Data.Meet.Unity);
                returns.AddItem("ImageQR", oResponse.Data.ImageQR);

                returns.AddItem("Message", oResponse.Message);
                 */

                if (parameters == null || parameters.DynamicDataItems == null || parameters.DynamicDataItems.Count == 0)
                {
                    picResultAccess.Image = Properties.Resources.ImagePicResultActionInfo;
                    labMessage.ForeColor = Color.Red;
                    labMessage.Text = "Error Inesperado! Reintente...";
                }


                if (parameters.DynamicDataItems.ContainsKey("Code"))
                {
                    if ((int)parameters.DynamicDataItems["Code"] == 1)
                    {
                        picResultAccess.Image = Properties.Resources.ImagePicResultActionAccesoPermitido;
                        labMessage.ForeColor = Color.LightGreen;
                        labMessage.Text = "Acceso Correcto!";
                    }
                    else
                    {
                        picResultAccess.Image = Properties.Resources.ImagePicResultActionInfo;
                        if (parameters.DynamicDataItems.ContainsKey("Message"))
                        {
                            labMessage.ForeColor = Color.Yellow;
                            labMessage.Text = (string)parameters.DynamicDataItems["Message"];
                        }
                    }
                }

                if (parameters.DynamicDataItems.ContainsKey("Name"))
                {
                    labNombre.Text = (string)parameters.DynamicDataItems["Name"];
                } else
                {
                    labNombre.Text = "Desconocido";
                }

                if (parameters.DynamicDataItems.ContainsKey("Photografy"))
                {
                    this.picFoto.Image = SetImageFromB64((string)parameters.DynamicDataItems["Photografy"]); 
                }
                else
                {
                    this.picFoto.Image = Properties.Resources.ImageFondoResultNoFoto;
                }

                if (parameters.DynamicDataItems.ContainsKey("MeetName"))
                {
                    labMeetingName.Text = (string)parameters.DynamicDataItems["MeetName"];
                }
                else
                {
                    labMeetingName.Text = "N/A";
                }

                if (parameters.DynamicDataItems.ContainsKey("MeetStartTime") && parameters.DynamicDataItems.ContainsKey("MeetDate"))
                {
                    labDateInit.Text = "Ini.:" + (string)parameters.DynamicDataItems["MeetDate"] + 
                                                 " " + ((string)parameters.DynamicDataItems["MeetStartTime"]).Substring(0, 5);
                }
                else
                {
                    labDateInit.Text = "N/A";
                }

                if (parameters.DynamicDataItems.ContainsKey("MeetEndTime"))
                {
                    //labDateEnd.Text = (string)parameters.DynamicDataItems["MeetEndTime"];
                    labDateEnd.Text = "Fin:" + (string)parameters.DynamicDataItems["MeetDate"] +
                                                " " + ((string)parameters.DynamicDataItems["MeetEndTime"]).Substring(0,5);
                }
                else
                {
                    labDateEnd.Text = "N/A";
                }

                if (parameters.DynamicDataItems.ContainsKey("MeetUnity"))
                {
                    labUnidad.Text = (string)parameters.DynamicDataItems["MeetUnity"];
                }
                else
                {
                    labUnidad.Text = "N/A";
                }

                if (parameters.DynamicDataItems.ContainsKey("MeetAuthorizer"))
                {
                    labAutorizador.Text = (string)parameters.DynamicDataItems["MeetAuthorizer"];
                }
                else
                {
                    labAutorizador.Text = "N/A";
                }
                


                this.Refresh();

            }
            catch (Exception ex)
            {

            }

      
        }

        public Image SetImageFromB64(string base64Photo)
        {
            Image ret = null;
            try
            {
                if (!string.IsNullOrEmpty(base64Photo))
                {
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(base64Photo));
                    ret = Image.FromStream(ms);
                    ms.Close();
                }
            }
            catch (Exception ex)
            {
                ret = null;
                //LOG.Error("Utils.SetImageFromB64 Excp Error: " + ex.Message);
            }
            return ret;
        }
    }
}
