﻿namespace Biometrika.HFSecury.API.Admin
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDevicePassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnDesconectar = new System.Windows.Forms.Button();
            this.btnConectar = new System.Windows.Forms.Button();
            this.txtDevicePort = new System.Windows.Forms.TextBox();
            this.txtDeviceIP = new System.Windows.Forms.TextBox();
            this.txtDeviceName = new System.Windows.Forms.TextBox();
            this.labSNCurrent = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabDeviceHandle = new System.Windows.Forms.TabControl();
            this.tabAdmin = new System.Windows.Forms.TabPage();
            this.btnSetCallbackHeartbeat = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.txtHeartBeatCallback = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.txtUrlCallbackIdentify = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnRestart = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSelectLogo = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.tabOperation = new System.Windows.Forms.TabPage();
            this.btnOpOpenRele = new System.Windows.Forms.Button();
            this.tabPersona = new System.Windows.Forms.TabPage();
            this.button15 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.picFace = new System.Windows.Forms.PictureBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.txtPersonCardId = new System.Windows.Forms.TextBox();
            this.txtPerdonName = new System.Windows.Forms.TextBox();
            this.txtPerdonId = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabMarks = new System.Windows.Forms.TabPage();
            this.button13 = new System.Windows.Forms.Button();
            this.txtMarkOrder = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtMarkModel = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtMarkTill = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtMarkFrom = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.txtMarkIndex = new System.Windows.Forms.TextBox();
            this.txtMarkLength = new System.Windows.Forms.TextBox();
            this.txtMarkPersonId = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button14 = new System.Windows.Forms.Button();
            this.btnSetCallbackIdentify = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.tabDeviceHandle.SuspendLayout();
            this.tabAdmin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.tabOperation.SuspendLayout();
            this.tabPersona.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFace)).BeginInit();
            this.tabMarks.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDevicePassword);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.btnDesconectar);
            this.groupBox1.Controls.Add(this.btnConectar);
            this.groupBox1.Controls.Add(this.txtDevicePort);
            this.groupBox1.Controls.Add(this.txtDeviceIP);
            this.groupBox1.Controls.Add(this.txtDeviceName);
            this.groupBox1.Controls.Add(this.labSNCurrent);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(413, 221);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Device";
            // 
            // txtDevicePassword
            // 
            this.txtDevicePassword.Location = new System.Drawing.Point(79, 105);
            this.txtDevicePassword.Name = "txtDevicePassword";
            this.txtDevicePassword.Size = new System.Drawing.Size(93, 20);
            this.txtDevicePassword.TabIndex = 11;
            this.txtDevicePassword.Text = "12345678";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Password";
            // 
            // btnDesconectar
            // 
            this.btnDesconectar.Location = new System.Drawing.Point(181, 138);
            this.btnDesconectar.Name = "btnDesconectar";
            this.btnDesconectar.Size = new System.Drawing.Size(90, 23);
            this.btnDesconectar.TabIndex = 9;
            this.btnDesconectar.Text = "Desconectar";
            this.btnDesconectar.UseVisualStyleBackColor = true;
            this.btnDesconectar.Click += new System.EventHandler(this.btnDesconectar_Click);
            // 
            // btnConectar
            // 
            this.btnConectar.Location = new System.Drawing.Point(79, 138);
            this.btnConectar.Name = "btnConectar";
            this.btnConectar.Size = new System.Drawing.Size(93, 23);
            this.btnConectar.TabIndex = 8;
            this.btnConectar.Text = "Conectar";
            this.btnConectar.UseVisualStyleBackColor = true;
            this.btnConectar.Click += new System.EventHandler(this.btnConectar_Click);
            // 
            // txtDevicePort
            // 
            this.txtDevicePort.Location = new System.Drawing.Point(79, 79);
            this.txtDevicePort.Name = "txtDevicePort";
            this.txtDevicePort.Size = new System.Drawing.Size(93, 20);
            this.txtDevicePort.TabIndex = 7;
            this.txtDevicePort.Text = "8090";
            // 
            // txtDeviceIP
            // 
            this.txtDeviceIP.Location = new System.Drawing.Point(79, 54);
            this.txtDeviceIP.Name = "txtDeviceIP";
            this.txtDeviceIP.Size = new System.Drawing.Size(210, 20);
            this.txtDeviceIP.TabIndex = 6;
            this.txtDeviceIP.Text = "192.168.1.113";
            // 
            // txtDeviceName
            // 
            this.txtDeviceName.Location = new System.Drawing.Point(79, 28);
            this.txtDeviceName.Name = "txtDeviceName";
            this.txtDeviceName.Size = new System.Drawing.Size(210, 20);
            this.txtDeviceName.TabIndex = 5;
            this.txtDeviceName.Text = "DEV_DEVICE";
            // 
            // labSNCurrent
            // 
            this.labSNCurrent.AutoSize = true;
            this.labSNCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSNCurrent.ForeColor = System.Drawing.Color.Blue;
            this.labSNCurrent.Location = new System.Drawing.Point(31, 180);
            this.labSNCurrent.Name = "labSNCurrent";
            this.labSNCurrent.Size = new System.Drawing.Size(32, 24);
            this.labSNCurrent.TabIndex = 4;
            this.labSNCurrent.Text = "??";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(20, 167);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "SN";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Port";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "IP";
            // 
            // tabDeviceHandle
            // 
            this.tabDeviceHandle.Controls.Add(this.tabAdmin);
            this.tabDeviceHandle.Controls.Add(this.tabOperation);
            this.tabDeviceHandle.Controls.Add(this.tabPersona);
            this.tabDeviceHandle.Controls.Add(this.tabMarks);
            this.tabDeviceHandle.Location = new System.Drawing.Point(12, 263);
            this.tabDeviceHandle.Name = "tabDeviceHandle";
            this.tabDeviceHandle.SelectedIndex = 0;
            this.tabDeviceHandle.Size = new System.Drawing.Size(413, 495);
            this.tabDeviceHandle.TabIndex = 2;
            // 
            // tabAdmin
            // 
            this.tabAdmin.BackColor = System.Drawing.SystemColors.Control;
            this.tabAdmin.Controls.Add(this.btnSetCallbackHeartbeat);
            this.tabAdmin.Controls.Add(this.button16);
            this.tabAdmin.Controls.Add(this.button11);
            this.tabAdmin.Controls.Add(this.button4);
            this.tabAdmin.Controls.Add(this.txtHeartBeatCallback);
            this.tabAdmin.Controls.Add(this.label9);
            this.tabAdmin.Controls.Add(this.button3);
            this.tabAdmin.Controls.Add(this.txtUrlCallbackIdentify);
            this.tabAdmin.Controls.Add(this.label8);
            this.tabAdmin.Controls.Add(this.btnRestart);
            this.tabAdmin.Controls.Add(this.button2);
            this.tabAdmin.Controls.Add(this.button1);
            this.tabAdmin.Controls.Add(this.btnSelectLogo);
            this.tabAdmin.Controls.Add(this.label6);
            this.tabAdmin.Controls.Add(this.picLogo);
            this.tabAdmin.Location = new System.Drawing.Point(4, 22);
            this.tabAdmin.Name = "tabAdmin";
            this.tabAdmin.Padding = new System.Windows.Forms.Padding(3);
            this.tabAdmin.Size = new System.Drawing.Size(405, 469);
            this.tabAdmin.TabIndex = 0;
            this.tabAdmin.Text = "Admin";
            // 
            // btnSetCallbackHeartbeat
            // 
            this.btnSetCallbackHeartbeat.Location = new System.Drawing.Point(227, 405);
            this.btnSetCallbackHeartbeat.Name = "btnSetCallbackHeartbeat";
            this.btnSetCallbackHeartbeat.Size = new System.Drawing.Size(111, 23);
            this.btnSetCallbackHeartbeat.TabIndex = 23;
            this.btnSetCallbackHeartbeat.Text = "Set ";
            this.btnSetCallbackHeartbeat.UseVisualStyleBackColor = true;
            this.btnSetCallbackHeartbeat.Click += new System.EventHandler(this.btnSetCallbackHeartbeat_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(227, 309);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(111, 23);
            this.button16.TabIndex = 22;
            this.button16.Text = "Set ";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(37, 83);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(111, 23);
            this.button11.TabIndex = 21;
            this.button11.Text = "Set Time";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(669, 116);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(111, 23);
            this.button4.TabIndex = 20;
            this.button4.Text = "Set en Device";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtHeartBeatCallback
            // 
            this.txtHeartBeatCallback.Location = new System.Drawing.Point(57, 379);
            this.txtHeartBeatCallback.Name = "txtHeartBeatCallback";
            this.txtHeartBeatCallback.Size = new System.Drawing.Size(281, 20);
            this.txtHeartBeatCallback.TabIndex = 19;
            this.txtHeartBeatCallback.Text = "http://192.168.1.84/Callback/CallbackDeviceHeartBeat";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(58, 363);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "URL HearthBeat ";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(669, 56);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(111, 23);
            this.button3.TabIndex = 17;
            this.button3.Text = "Set en Device";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtUrlCallbackIdentify
            // 
            this.txtUrlCallbackIdentify.Location = new System.Drawing.Point(57, 283);
            this.txtUrlCallbackIdentify.Name = "txtUrlCallbackIdentify";
            this.txtUrlCallbackIdentify.Size = new System.Drawing.Size(281, 20);
            this.txtUrlCallbackIdentify.TabIndex = 16;
            this.txtUrlCallbackIdentify.Text = "http://192.168.1.84/Callback/CallbackIdentify";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(58, 267);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "URL Callback Identity";
            // 
            // btnRestart
            // 
            this.btnRestart.Location = new System.Drawing.Point(37, 38);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(111, 23);
            this.btnRestart.TabIndex = 14;
            this.btnRestart.Text = "Restart Device";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(189, 206);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(111, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "Clear Logo";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(189, 177);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Set Logo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSelectLogo
            // 
            this.btnSelectLogo.Location = new System.Drawing.Point(273, 33);
            this.btnSelectLogo.Name = "btnSelectLogo";
            this.btnSelectLogo.Size = new System.Drawing.Size(27, 23);
            this.btnSelectLogo.TabIndex = 12;
            this.btnSelectLogo.Text = "...";
            this.btnSelectLogo.UseVisualStyleBackColor = true;
            this.btnSelectLogo.Click += new System.EventHandler(this.btnSelectLogo_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(186, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Selecione Logo";
            // 
            // picLogo
            // 
            this.picLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picLogo.Location = new System.Drawing.Point(189, 59);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(111, 112);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 0;
            this.picLogo.TabStop = false;
            // 
            // tabOperation
            // 
            this.tabOperation.BackColor = System.Drawing.SystemColors.Control;
            this.tabOperation.Controls.Add(this.btnOpOpenRele);
            this.tabOperation.Location = new System.Drawing.Point(4, 22);
            this.tabOperation.Name = "tabOperation";
            this.tabOperation.Padding = new System.Windows.Forms.Padding(3);
            this.tabOperation.Size = new System.Drawing.Size(405, 469);
            this.tabOperation.TabIndex = 1;
            this.tabOperation.Text = "Operación";
            // 
            // btnOpOpenRele
            // 
            this.btnOpOpenRele.Location = new System.Drawing.Point(46, 25);
            this.btnOpOpenRele.Name = "btnOpOpenRele";
            this.btnOpOpenRele.Size = new System.Drawing.Size(75, 23);
            this.btnOpOpenRele.TabIndex = 1;
            this.btnOpOpenRele.Text = "Open Rele";
            this.btnOpOpenRele.UseVisualStyleBackColor = true;
            this.btnOpOpenRele.Click += new System.EventHandler(this.btnOpOpenRele_Click);
            // 
            // tabPersona
            // 
            this.tabPersona.BackColor = System.Drawing.SystemColors.Control;
            this.tabPersona.Controls.Add(this.button15);
            this.tabPersona.Controls.Add(this.button8);
            this.tabPersona.Controls.Add(this.button9);
            this.tabPersona.Controls.Add(this.button10);
            this.tabPersona.Controls.Add(this.label13);
            this.tabPersona.Controls.Add(this.picFace);
            this.tabPersona.Controls.Add(this.button7);
            this.tabPersona.Controls.Add(this.button6);
            this.tabPersona.Controls.Add(this.button5);
            this.tabPersona.Controls.Add(this.txtPersonCardId);
            this.tabPersona.Controls.Add(this.txtPerdonName);
            this.tabPersona.Controls.Add(this.txtPerdonId);
            this.tabPersona.Controls.Add(this.label10);
            this.tabPersona.Controls.Add(this.label11);
            this.tabPersona.Controls.Add(this.label12);
            this.tabPersona.Location = new System.Drawing.Point(4, 22);
            this.tabPersona.Name = "tabPersona";
            this.tabPersona.Padding = new System.Windows.Forms.Padding(3);
            this.tabPersona.Size = new System.Drawing.Size(405, 469);
            this.tabPersona.TabIndex = 2;
            this.tabPersona.Text = "Personas";
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(134, 300);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(93, 23);
            this.button15.TabIndex = 22;
            this.button15.Text = "Enroll Persona";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(191, 171);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(111, 23);
            this.button8.TabIndex = 21;
            this.button8.Text = "Delete Logo";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(191, 142);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(111, 23);
            this.button9.TabIndex = 20;
            this.button9.Text = "Set Face";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(134, 113);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(27, 23);
            this.button10.TabIndex = 19;
            this.button10.Text = "...";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(47, 118);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 18;
            this.label13.Text = "Selecione Face";
            // 
            // picFace
            // 
            this.picFace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFace.Location = new System.Drawing.Point(44, 137);
            this.picFace.Name = "picFace";
            this.picFace.Size = new System.Drawing.Size(140, 142);
            this.picFace.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFace.TabIndex = 17;
            this.picFace.TabStop = false;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(301, 85);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(93, 23);
            this.button7.TabIndex = 16;
            this.button7.Text = "Delete Persona";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(301, 59);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(93, 23);
            this.button6.TabIndex = 15;
            this.button6.Text = "Update Persona";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(301, 32);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(93, 23);
            this.button5.TabIndex = 14;
            this.button5.Text = "Add Persona";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txtPersonCardId
            // 
            this.txtPersonCardId.Location = new System.Drawing.Point(73, 85);
            this.txtPersonCardId.Name = "txtPersonCardId";
            this.txtPersonCardId.Size = new System.Drawing.Size(210, 20);
            this.txtPersonCardId.TabIndex = 13;
            this.txtPersonCardId.Text = "123456789";
            // 
            // txtPerdonName
            // 
            this.txtPerdonName.Location = new System.Drawing.Point(73, 60);
            this.txtPerdonName.Name = "txtPerdonName";
            this.txtPerdonName.Size = new System.Drawing.Size(210, 20);
            this.txtPerdonName.TabIndex = 12;
            this.txtPerdonName.Text = "TestPerson";
            // 
            // txtPerdonId
            // 
            this.txtPerdonId.Location = new System.Drawing.Point(73, 34);
            this.txtPerdonId.Name = "txtPerdonId";
            this.txtPerdonId.Size = new System.Drawing.Size(210, 20);
            this.txtPerdonId.TabIndex = 11;
            this.txtPerdonId.Text = "111111111";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Name";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(28, 88);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "card id";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(41, 37);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "id";
            // 
            // tabMarks
            // 
            this.tabMarks.BackColor = System.Drawing.SystemColors.Control;
            this.tabMarks.Controls.Add(this.button13);
            this.tabMarks.Controls.Add(this.txtMarkOrder);
            this.tabMarks.Controls.Add(this.label20);
            this.tabMarks.Controls.Add(this.txtMarkModel);
            this.tabMarks.Controls.Add(this.label19);
            this.tabMarks.Controls.Add(this.txtMarkTill);
            this.tabMarks.Controls.Add(this.label18);
            this.tabMarks.Controls.Add(this.txtMarkFrom);
            this.tabMarks.Controls.Add(this.label14);
            this.tabMarks.Controls.Add(this.button12);
            this.tabMarks.Controls.Add(this.txtMarkIndex);
            this.tabMarks.Controls.Add(this.txtMarkLength);
            this.tabMarks.Controls.Add(this.txtMarkPersonId);
            this.tabMarks.Controls.Add(this.label15);
            this.tabMarks.Controls.Add(this.label16);
            this.tabMarks.Controls.Add(this.label17);
            this.tabMarks.Location = new System.Drawing.Point(4, 22);
            this.tabMarks.Name = "tabMarks";
            this.tabMarks.Padding = new System.Windows.Forms.Padding(3);
            this.tabMarks.Size = new System.Drawing.Size(405, 469);
            this.tabMarks.TabIndex = 3;
            this.tabMarks.Text = "Marks";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(192, 227);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(93, 23);
            this.button13.TabIndex = 27;
            this.button13.Text = "Delete Marks";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // txtMarkOrder
            // 
            this.txtMarkOrder.Location = new System.Drawing.Point(82, 196);
            this.txtMarkOrder.Name = "txtMarkOrder";
            this.txtMarkOrder.Size = new System.Drawing.Size(93, 20);
            this.txtMarkOrder.TabIndex = 26;
            this.txtMarkOrder.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(28, 199);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(33, 13);
            this.label20.TabIndex = 25;
            this.label20.Text = "Order";
            // 
            // txtMarkModel
            // 
            this.txtMarkModel.Location = new System.Drawing.Point(82, 168);
            this.txtMarkModel.Name = "txtMarkModel";
            this.txtMarkModel.Size = new System.Drawing.Size(93, 20);
            this.txtMarkModel.TabIndex = 24;
            this.txtMarkModel.Text = "-1";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(28, 171);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 13);
            this.label19.TabIndex = 23;
            this.label19.Text = "Model";
            // 
            // txtMarkTill
            // 
            this.txtMarkTill.Location = new System.Drawing.Point(82, 138);
            this.txtMarkTill.Name = "txtMarkTill";
            this.txtMarkTill.Size = new System.Drawing.Size(124, 20);
            this.txtMarkTill.TabIndex = 22;
            this.txtMarkTill.Text = "2021-08-19 18:04:49";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(26, 138);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 13);
            this.label18.TabIndex = 21;
            this.label18.Text = "Hasta";
            // 
            // txtMarkFrom
            // 
            this.txtMarkFrom.Location = new System.Drawing.Point(82, 105);
            this.txtMarkFrom.Name = "txtMarkFrom";
            this.txtMarkFrom.Size = new System.Drawing.Size(124, 20);
            this.txtMarkFrom.TabIndex = 20;
            this.txtMarkFrom.Text = "2021-06-16 18:04:49";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 108);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "Desde";
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(82, 227);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(93, 23);
            this.button12.TabIndex = 18;
            this.button12.Text = "Get Marks";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // txtMarkIndex
            // 
            this.txtMarkIndex.Location = new System.Drawing.Point(82, 79);
            this.txtMarkIndex.Name = "txtMarkIndex";
            this.txtMarkIndex.Size = new System.Drawing.Size(93, 20);
            this.txtMarkIndex.TabIndex = 17;
            this.txtMarkIndex.Text = "0";
            // 
            // txtMarkLength
            // 
            this.txtMarkLength.Location = new System.Drawing.Point(82, 54);
            this.txtMarkLength.Name = "txtMarkLength";
            this.txtMarkLength.Size = new System.Drawing.Size(210, 20);
            this.txtMarkLength.TabIndex = 16;
            this.txtMarkLength.Text = "-1";
            // 
            // txtMarkPersonId
            // 
            this.txtMarkPersonId.Location = new System.Drawing.Point(82, 28);
            this.txtMarkPersonId.Name = "txtMarkPersonId";
            this.txtMarkPersonId.Size = new System.Drawing.Size(210, 20);
            this.txtMarkPersonId.TabIndex = 15;
            this.txtMarkPersonId.Text = "-1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 31);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "Person Id";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(28, 82);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(33, 13);
            this.label16.TabIndex = 13;
            this.label16.Text = "Index";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(23, 57);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 13);
            this.label17.TabIndex = 12;
            this.label17.Text = "Length";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button14);
            this.groupBox2.Controls.Add(this.btnSetCallbackIdentify);
            this.groupBox2.Location = new System.Drawing.Point(445, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1061, 759);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "LOG...";
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(951, 19);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(93, 23);
            this.button14.TabIndex = 12;
            this.button14.Text = "Borrar";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // btnSetCallbackIdentify
            // 
            this.btnSetCallbackIdentify.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.btnSetCallbackIdentify.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnSetCallbackIdentify.Location = new System.Drawing.Point(17, 31);
            this.btnSetCallbackIdentify.Name = "btnSetCallbackIdentify";
            this.btnSetCallbackIdentify.ReadOnly = true;
            this.btnSetCallbackIdentify.Size = new System.Drawing.Size(1027, 706);
            this.btnSetCallbackIdentify.TabIndex = 0;
            this.btnSetCallbackIdentify.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1518, 792);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tabDeviceHandle);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Biometrika Device Admin";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabDeviceHandle.ResumeLayout(false);
            this.tabAdmin.ResumeLayout(false);
            this.tabAdmin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.tabOperation.ResumeLayout(false);
            this.tabPersona.ResumeLayout(false);
            this.tabPersona.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFace)).EndInit();
            this.tabMarks.ResumeLayout(false);
            this.tabMarks.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDesconectar;
        private System.Windows.Forms.Button btnConectar;
        private System.Windows.Forms.TextBox txtDevicePort;
        private System.Windows.Forms.TextBox txtDeviceIP;
        private System.Windows.Forms.TextBox txtDeviceName;
        private System.Windows.Forms.Label labSNCurrent;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDevicePassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabControl tabDeviceHandle;
        private System.Windows.Forms.TabPage tabAdmin;
        private System.Windows.Forms.TabPage tabOperation;
        private System.Windows.Forms.Button btnOpOpenRele;
        private System.Windows.Forms.Button btnSelectLogo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox btnSetCallbackIdentify;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtUrlCallbackIdentify;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtHeartBeatCallback;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPersona;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txtPersonCardId;
        private System.Windows.Forms.TextBox txtPerdonName;
        private System.Windows.Forms.TextBox txtPerdonId;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox picFace;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TabPage tabMarks;
        private System.Windows.Forms.TextBox txtMarkFrom;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TextBox txtMarkIndex;
        private System.Windows.Forms.TextBox txtMarkLength;
        private System.Windows.Forms.TextBox txtMarkPersonId;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtMarkOrder;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtMarkModel;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtMarkTill;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button btnSetCallbackHeartbeat;
    }
}

