﻿using Biometrika.HFSecury.API.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.HFSecury.API.Admin
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string s = Encrypt("B1ometrika2021", "Biometrik@", "SaltBiometrika2021");
            string s1 = Decrypt(s, "Biometrik@", "SaltBiometrika2021");
            int i = 0;

            //Config c = new Config();

            //string s = c.ToString();
            //foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(c))
            //{
            //    string name = descriptor.Name;
            //    object value = descriptor.GetValue(c);
            //    Console.WriteLine("{0}={1}", name, value);
            //}

            //Type t = typeof(Config);
            //// Get the public properties.
            //PropertyInfo[] propInfos = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            //DisplayPropertyInfo(propInfos);



            //string s1 = "删除识别记录数量：15";
            //string s = System.Text.Encoding.UTF8.GetString(System.Text.ASCIIEncoding.ASCII.GetBytes(s1));
            //bool isDigit = false;
            //string sout = "";
            //string sc;
            //int auxi = -1;
            //foreach (char item in s)
            //{
            //    auxi = -1;
            //    char c = item;
            //    //if (c.c.Equals(':'))
            //    sc = item.ToString();

            //    try
            //    {
            //        auxi = Convert.ToInt16(sc);
            //    }
            //    catch (Exception ex)
            //    {
            //        auxi = -1;
            //    }
            //    if (auxi > 0) { 
            //        isDigit = true;
            //    }
            //    if (isDigit)
            //    {
            //        sout = sout + sc.ToString();
            //    }
            //}
            //sout = sout.Trim();
            //int p = Convert.ToInt32(sout);
            ////string[] ss = s.Split(new char);
            //int pos = s.IndexOf(":");

            //string s = GetTimeStamp();

            //string s1 = GetTimeStamp();
            //string s2 = GetTimeStamp();


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        /// <summary>
        /// Get a 13-bit timestamp
        /// </summary>
        /// <returns></returns>
        public static string GetTimeStamp()
        {
            System.DateTime time = System.DateTime.Now;
            long ts = ConvertDateTimeToInt(time);
            return ts.ToString();
        }
        /// <summary>  
        /// Convert c# DateTime time format to Unix timestamp format
                /// </summary>  
        /// <param name="time">time</param>
                /// <returns>long</returns>  
        private static long ConvertDateTimeToInt(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            long t = (time.Ticks - startTime.Ticks) / 10000; // except 10000 is adjusted to 13 bits
            return t;
        }

        public static void DisplayPropertyInfo(PropertyInfo[] propInfos)
        {
            // Display information for all properties.
            foreach (var propInfo in propInfos)
            {
                bool readable = propInfo.CanRead;
                bool writable = propInfo.CanWrite;

                Console.WriteLine("   Property name: {0}", propInfo.Name);
                Console.WriteLine("   Property type: {0}", propInfo.PropertyType);
                Console.WriteLine("   Read-Write:    {0}", readable & writable);
                if (readable)
                {
                    MethodInfo getAccessor = propInfo.GetMethod;
                    Console.WriteLine("   Visibility:    {0}",
                                      GetVisibility(getAccessor));
                }
                if (writable)
                {
                    MethodInfo setAccessor = propInfo.SetMethod;
                    Console.WriteLine("   Visibility:    {0}",
                                      GetVisibility(setAccessor));
                }
                Console.WriteLine();
            }
        }

        public static String GetVisibility(MethodInfo accessor)
        {
            if (accessor.IsPublic)
                return "Public";
            else if (accessor.IsPrivate)
                return "Private";
            else if (accessor.IsFamily)
                return "Protected";
            else if (accessor.IsAssembly)
                return "Internal/Friend";
            else
                return "Protected Internal/Friend";
        }

        public static string Encrypt(string value, string password, string salt)
        {
            DeriveBytes rgb = new Rfc2898DeriveBytes(password, Encoding.Unicode.GetBytes(salt));
            SymmetricAlgorithm algorithm = new TripleDESCryptoServiceProvider();
            byte[] rgbKey = rgb.GetBytes(algorithm.KeySize >> 3);
            byte[] rgbIV = rgb.GetBytes(algorithm.BlockSize >> 3);
            ICryptoTransform transform = algorithm.CreateEncryptor(rgbKey, rgbIV);
            using (MemoryStream buffer = new MemoryStream())
            {
                using (CryptoStream stream = new CryptoStream(buffer, transform, CryptoStreamMode.Write))
                {
                    using (StreamWriter writer = new StreamWriter(stream, Encoding.Unicode))
                    {
                        writer.Write(value);
                    }
                }
                return Convert.ToBase64String(buffer.ToArray());
            }
        }

        public static string Decrypt(string text, string password, string salt)
        {
            DeriveBytes rgb = new Rfc2898DeriveBytes(password, Encoding.Unicode.GetBytes(salt));
            SymmetricAlgorithm algorithm = new TripleDESCryptoServiceProvider();
            byte[] rgbKey = rgb.GetBytes(algorithm.KeySize >> 3);
            byte[] rgbIV = rgb.GetBytes(algorithm.BlockSize >> 3);
            ICryptoTransform transform = algorithm.CreateDecryptor(rgbKey, rgbIV);
            using (MemoryStream buffer = new MemoryStream(Convert.FromBase64String(text)))
            {
                using (CryptoStream stream = new CryptoStream(buffer, transform, CryptoStreamMode.Read))
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.Unicode))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }
    }
}
