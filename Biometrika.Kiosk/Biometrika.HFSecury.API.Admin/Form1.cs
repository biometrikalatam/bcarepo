﻿using Biometrika.HFSecury.API.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.HFSecury.API.Admin
{
    public partial class Form1 : Form
    {

        Device _CURRENT_DEVICE;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            try
            {
                _CURRENT_DEVICE = new Device();
                if (_CURRENT_DEVICE.Initialize(txtDeviceName.Text, txtDeviceIP.Text, 
                                               txtDevicePort.Text, txtDevicePassword.Text) == 0)
                {
                    labSNCurrent.Text = _CURRENT_DEVICE.SN;
                }
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void btnDesconectar_Click(object sender, EventArgs e)
        {
            try
            {
                _CURRENT_DEVICE = null;
                labSNCurrent.Text = "??";
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void btnOpOpenRele_Click(object sender, EventArgs e)
        {
            try
            {
                int ret = _CURRENT_DEVICE.OpenRele();
                
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        string _STR_BASE64_LOGO = null;
        private void btnSelectLogo_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Seleccione sample en Base 64 o jpg...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    byte[] byimage1 = System.IO.File.ReadAllBytes(openFileDialog1.FileName);
                    _STR_BASE64_LOGO = Convert.ToBase64String(byimage1);
                    picLogo.Image = Image.FromFile(openFileDialog1.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                picLogo.Image = null;
                _STR_BASE64_LOGO = null;
                string msgerr;
                int iret = _CURRENT_DEVICE.SetLogo("-1", out msgerr);
                AddLog("Set Logo - iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(_STR_BASE64_LOGO))
                {
                    AddLog("Set Logo - Debe seleccionar un logo primero...");
                } else
                {
                    string msgerr;
                    int iret = _CURRENT_DEVICE.SetLogo(_STR_BASE64_LOGO, out msgerr);
                    AddLog("Set Logo - iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr)?"":msgerr));
                }
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void AddLog(string msg)
        {
            try
            {
                btnSetCallbackIdentify.Text = msg + Environment.NewLine + btnSetCallbackIdentify.Text;
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            try
            {
                string msgerr;
                int iret = _CURRENT_DEVICE.Restart(out msgerr);
                AddLog("Restart - iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                string msgerr;
                int iret = _CURRENT_DEVICE.SetCallbackIdentifyUrl(txtUrlCallbackIdentify.Text, out msgerr);
                AddLog("Sel Callback URL => iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                string msgerr;
                int iret = _CURRENT_DEVICE.SetDeviceHeartBeatUrl(txtHeartBeatCallback.Text, out msgerr);
                AddLog("Sel Callback URL Herat Beat => iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                string msgerr;
                Person persona = new Person();
                persona.id = txtPerdonId.Text;
                persona.name = txtPerdonName.Text;
                persona.idcardNum = txtPersonCardId.Text;
                int iret = _CURRENT_DEVICE.AddPersona(persona, out msgerr);
                AddLog("Add Persona => iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                string msgerr;
                Person persona = new Person();
                persona.id = txtPerdonId.Text;
                persona.name = txtPerdonName.Text;
                persona.idcardNum = txtPersonCardId.Text;
                int iret = _CURRENT_DEVICE.UpdatePersona(persona, out msgerr);
                AddLog("UpdatePersona => iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                string msgerr;
                int iret = _CURRENT_DEVICE.DeletePersona(txtPerdonId.Text, out msgerr);
                AddLog("DeletePersona => iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
            }
            catch (Exception ex)
            {
                
                //LOG.Error(" Error: " + ex.Message);
            }
        }

        string _STR_BASE64_FACE = null; 
        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Seleccione sample en Base 64 o jpg...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    byte[] byimage1 = System.IO.File.ReadAllBytes(openFileDialog1.FileName);
                    _STR_BASE64_FACE = Convert.ToBase64String(byimage1);
                    picFace.Image = Image.FromFile(openFileDialog1.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                string msgerr;

                if (!string.IsNullOrEmpty(_STR_BASE64_FACE) || !string.IsNullOrEmpty(txtPerdonId.Text))
                {
                    int iret = _CURRENT_DEVICE.AddPhoto(txtPerdonId.Text,
                                            txtPerdonId.Text + Guid.NewGuid().ToString("N"),
                                            _STR_BASE64_FACE, out msgerr);
                    AddLog("AddPhoto => iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
                } else
                {
                    AddLog("AddPhoto => Seleccione face y personId primero...");
                }
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                string msgerr;

                if (!string.IsNullOrEmpty(_STR_BASE64_FACE) || !string.IsNullOrEmpty(txtPerdonId.Text))
                {
                    int iret = _CURRENT_DEVICE.DeletePhoto(txtPerdonId.Text, out msgerr);
                    AddLog("DeleteFoto => iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
                }
                else
                {
                    AddLog("DeleteFoto => Seleccione face y personId primero...");
                }
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                string msgerr;
                //DateTime foo = DateTime.Now;
                //long unixTime = ((DateTimeOffset)foo).ToUnixTimeSeconds();
                //long l = DateTimeOffset.Now.ToUnixTimeSeconds();
                var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
                long unixTime1 = (long)timeSpan.TotalSeconds;
                var timeSpan2 = (DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0));
                long unixTime2 = (long)timeSpan2.TotalSeconds;

                int entero = 
                    (int)Math.Truncate((DateTime.Now.ToUniversalTime().
                    Subtract(new DateTime(1970, 1, 1))).TotalSeconds);

                var dateTime2 = Helpers.HelperTime.ToDateTimeOffsetFromEpoch(1623967489383/1000);
                    //DateTimeOffset.FromUnixTimeSeconds(1623967489383).LocalDateTime;

                DateTime dt = Helpers.HelperTime.UnixTimestampToDateTime(1623967489383); 
                // DateTimeOffset.FromUnixTimeSeconds(1623967489383).LocalDateTime;

                //DateTime dt = Helpers.HelperTime.UnixTimestampToDateTime(1623967489383);

                long epochTicks = new DateTime(1970, 1, 1).Ticks;
                long unixTime = ((DateTime.UtcNow.Ticks - epochTicks) / TimeSpan.TicksPerSecond);
                int iret = _CURRENT_DEVICE.SetTime(Helpers.HelperTime.GetTimeStamp(), out msgerr);
                AddLog("Set Time => iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            try
            {
                string msgerr;
                MarkSearchModelIn param = new MarkSearchModelIn();
                param.startTime = txtMarkFrom.Text;
                param.endTime = txtMarkTill.Text;
                param.index = Convert.ToInt32(txtMarkIndex.Text);
                param.model = Convert.ToInt32(txtMarkModel.Text);
                param.length = Convert.ToInt32(txtMarkLength.Text);
                param.personId = txtMarkPersonId.Text;
                param.order = Convert.ToInt32(txtMarkOrder.Text);

                IList<MarkSearchModelOut> markListOut = null;
                int iret = _CURRENT_DEVICE.GetMarks(param, out markListOut, out msgerr);
                AddLog("Get Marks => iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
                if (iret == 0)
                {
                    if (markListOut != null)
                    {
                        MarkSearchModelOut mark;
                        AddLog("Get Marks => #Marks = " + markListOut.Count.ToString());
                        foreach (var item in markListOut)
                        {
                            AddLog("     Mark => " + ((MarkSearchModelOut)item).ToString()); 
                        }
                    } else 
                    {
                        AddLog("Get Marks => #Marks = 0!");
                    }
                }
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            try
            {
                string msgerr;
                MarkDeleteModelIn param = new MarkDeleteModelIn();
                param.startTime = txtMarkFrom.Text;
                param.endTime = txtMarkTill.Text;
                param.model = Convert.ToInt32(txtMarkModel.Text);
                param.personId = txtMarkPersonId.Text;
                IList<MarkSearchModelOut> markListOut = null;

                int qdeleted = 0;
                int iret = _CURRENT_DEVICE.DeleteMarks(param, out qdeleted, out msgerr);
                AddLog("Delete Marks => iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
                if (iret == 0)
                {
                    AddLog("Delete Marks => #Marks Deleted = " + qdeleted.ToString());
                }
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            btnSetCallbackIdentify.Text = "";
        }

        private void button15_Click(object sender, EventArgs e)
        {
            try
            {
                string msgerr;
                Person persona = new Person();
                persona.id = txtPerdonId.Text;
                persona.name = txtPerdonName.Text;
                persona.idcardNum = txtPersonCardId.Text;
                int iret = _CURRENT_DEVICE.EnrollPersona(persona,
                                txtPerdonId.Text + Guid.NewGuid().ToString("N"),
                                _STR_BASE64_FACE, out msgerr);
                AddLog("Enroll Persona => iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void btnSetCallbackHeartbeat_Click(object sender, EventArgs e)
        {
            try
            {
                string msgerr;
                Person persona = new Person();
                persona.id = txtPerdonId.Text;
                persona.name = txtPerdonName.Text;
                persona.idcardNum = txtPersonCardId.Text;
                int iret = _CURRENT_DEVICE.SetDeviceHeartBeatUrl(txtHeartBeatCallback.Text,
                                                                 out msgerr);
                AddLog("Enroll Persona => iret = " + iret + " - msgerr=" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr));
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //this.label5.Text = "Serial Test";
            ////this.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffc0c0");
            //this.label5.Location = new Point(21, 150);
            //this.label5.Size = new Size(300, 100);
            //float sizeFont = (float)Convert.ToDouble(20);
            //this.label5.Font = new Font("Courier New", sizeFont);
            //this.label5.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ffc0c0");
        }
    }
}
