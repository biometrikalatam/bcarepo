﻿using Biometrika.ReleHandle.Web.src.Config;
using log4net;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Biometrika.ReleHandle.Web
{
    /// <summary>
    /// 
    /// </summary>
    public class AccessController : ApiController
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(AccessController));

        [Route("api/access/action")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "ReleHandleResponse", typeof(ReleHandleResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "ReleHandleResponse", typeof(ReleHandleResponse))]
        public object Action(string origin, string accessname, string type)
        {
            int res = 0;
            ReleHandleResponse RESPONSE = new ReleHandleResponse();
            try
            {
                LOG.Debug("AccessController.Action IN...");
                /*
                    1.- Obtengo el AccessPoint
                    2.- Verifico que tenga el type definido
                    3.- Mando a ejecutar la accion
                */

                string msgerr;
                RESPONSE.code = Global._ACCESS_CONTROLLER_UoW.Action(origin, accessname, type, out msgerr);
                RESPONSE.message = msgerr;

                if (RESPONSE.code < 0)
                {
                    LOG.Warn("AccessController.Action - Error ejecutando action [" + RESPONSE.code + "-" + RESPONSE.message + "]");
                    if (RESPONSE.code == 1001) return Request.CreateResponse(HttpStatusCode.NotFound, RESPONSE);
                    else return Request.CreateResponse(HttpStatusCode.InternalServerError, RESPONSE);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AccessController.Action Excp Error:", ex);
                RESPONSE.code = -1;
                RESPONSE.message = ex.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, RESPONSE);
            }
            return Request.CreateResponse(HttpStatusCode.OK, RESPONSE);
        }

        [Route("api/access/config")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "ReleHandleResponse", typeof(ReleHandleResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "ReleHandleResponse", typeof(ReleHandleResponse))]
        public object Config()
        {
            int res = 0;
            ReleHandleResponse RESPONSE = new ReleHandleResponse();
            try
            {
                LOG.Debug("AccessController.Action IN...");
                string msgerr;
                if (Global._ACCESS_CONTROLLER_UoW == null || Global._ACCESS_CONTROLLER_UoW._CONFIG == null ||
                    Global._ACCESS_CONTROLLER_UoW._CONFIG.AccessPointList == null ||
                    Global._ACCESS_CONTROLLER_UoW._CONFIG.AccessPointList.Count == 0)
                {
                    RESPONSE.code = -999;
                    RESPONSE.message = "AccessPoints No Configurados [Vacio]";
                    return Request.CreateResponse(HttpStatusCode.NotImplemented, RESPONSE);
                }
                else
                {
                    RESPONSE.code = 0;
                    RESPONSE.message = Global._ACCESS_CONTROLLER_UoW._CONFIG.ToString();
                }

            }
            catch (Exception ex)
            {
                LOG.Error("AccessController.Action Excp Error:", ex);
                RESPONSE.code = -1;
                RESPONSE.message = ex.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, RESPONSE);
            }
            return Request.CreateResponse(HttpStatusCode.OK, RESPONSE);
        }

        // GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}