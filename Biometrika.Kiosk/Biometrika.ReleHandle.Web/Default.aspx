﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biometrika.ReleHandle.Web._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            font-family: Arial, Helvetica, sans-serif;
            color: #006600;
        }
        .style2
        {
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="style1">
        <strong><span class="style2">Biometrika ReleHandle Web Resume</span> </strong>
    </div>
    <br />
    <asp:Label ID="Label2" runat="server" Text="Type Relay"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="txtTypeRelay" runat="server" Width="211px"></asp:TextBox>
&nbsp;&nbsp;
    <br />
    <asp:Label ID="Label1" runat="server" Text="IP Relay"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="txtIPRelay" runat="server" Width="211px"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="labEstado" runat="server" Text="CONECTED!"></asp:Label>
    <br />
    <asp:Label ID="Label3" runat="server" Text="Port Relay"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="txtPortRelay" runat="server" Width="211px"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnReinit" runat="server" onclick="btnReinit_Click" 
        Text="Reiniciar" />
    <br />
    <asp:Label ID="Label4" runat="server" Text="Module Id Relay"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="txtModuleId" runat="server" Width="211px"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="btnGetState" runat="server" onclick="btnGetState_Click" 
        Text="Get Estados" />
&nbsp;
    <asp:TextBox ID="txtEstados" runat="server" Width="211px"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="btnViewLog" runat="server" onclick="btnViewLog_Click" 
        Text="View Log" />
    <br />
    <br />
    </form>
</body>
</html>
