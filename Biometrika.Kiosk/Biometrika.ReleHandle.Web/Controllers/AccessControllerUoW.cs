﻿using Bio.Core.Utils;
using Biometrika.Kiosk.Common.Model;
using Biometrika.ReleHandle.Web.src.Config;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Biometrika.ReleHandle.Web.Controllers
{
    public class AccessControllerUoW : ApiController
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(AccessControllerUoW));

        internal AccessConfig _CONFIG;

        public int Initialize()
        {
            int ret = 0;

            try
            {
                LOG.Debug("AccessControllerUoW.Initialize IN...");
                if (!System.IO.File.Exists(Properties.Settings.Default.PathAccessConfig))
                {
                    LOG.Debug("AccessControllerUoW.Initialize - Creando AccessConfig en " + Properties.Settings.Default.PathAccessConfig + "...");
                    _CONFIG = new AccessConfig();
                    _CONFIG.AccessPointList = new List<AccessPoint>();
                    AccessPoint _AP = new AccessPoint("Torniquete1", "E", "1", "3000");
                    _CONFIG.AccessPointList.Add(_AP);
                    _AP = new AccessPoint("Torniquete1", "S", "2", "3000");
                    _CONFIG.AccessPointList.Add(_AP);
                    _AP = new AccessPoint("Torniquete2", "E", "1", "3000");
                    _CONFIG.AccessPointList.Add(_AP);
                    _AP = new AccessPoint("Puerta1", "E", "1", "3000");
                    _CONFIG.AccessPointList.Add(_AP);
                    //System.IO.File.WriteAllText(
                    //        Properties.Settings.Default.PathAccessConfig, Bio.Core.Utils.SerializeHelper.SerializeToXml(_CONFIG));
                    if (!SerializeHelper.SerializeToFile(_CONFIG, Properties.Settings.Default.PathAccessConfig))
                    {
                        LOG.Warn("AccessControllerUoW.Initialize - Error creando AccessConfig!");
                    }
                    else
                    {
                        LOG.Debug("AccessControllerUoW.Initialize - Creado AccessConfig!");
                    }
                } else
                {
                    LOG.Debug("AccessControllerUoW.Initialize - Deserializando AccessConfig en " + Properties.Settings.Default.PathAccessConfig + "...");
                    //_CONFIG = Bio.Core.Utils.XmlUtils.DeserializeObject<AccessConfig>(
                    //            System.IO.File.ReadAllText(Properties.Settings.Default.PathAccessConfig));
                    _CONFIG = SerializeHelper.DeserializeFromFile<AccessConfig>(Properties.Settings.Default.PathAccessConfig);
                    LOG.Debug("AccessControllerUoW.Initialize - AccessConfig != null => " + (_CONFIG!=null).ToString());
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("AccessControllerUoW.Initialize Excp Error: ", ex);
            }
            return ret;
        }

        public int Action(string origin, string accessname, string type, out string msgerr)
        {
            msgerr = "";
            int ret = 0;
            HttpResponseMessage HRM = new HttpResponseMessage();
            try
            {
                LOG.Debug("AccessControllerUoW.Action IN...");

                ReleHandleController RHC = new ReleHandleController();
                AccessPoint _AP = Global._ACCESS_CONTROLLER_UoW._CONFIG.GetAP(accessname, type, out msgerr);
                if (_AP == null)
                {
                    LOG.Warn("AccessControllerUoW.Action - AccessPoint no configurado! GetAP Ret = null");
                    msgerr = "No existe AccessPoint configurado como " + accessname + "/" + type;
                    return -1001;
                }
                LOG.Debug("AccessController.Action - AccessPoint obtenido!");

                //if (type.Equals("ES") || type.Equals("E"))
                //{
                    ReleHandleResponse RHR = (ReleHandleResponse)RHC.OpRelePulso(origin, Convert.ToInt32(_AP.ReleNumber), 
                                                                                 Convert.ToInt32(_AP.TimerRele));
                    //if (HRM != null && HRM.StatusCode == HttpStatusCode.OK)
                    if (RHR != null && RHR.code == 0)
                    {
                        LOG.Debug("AccessController.Action - Action ejecutada OK! " +
                                   "[AccessPointName = " + _AP.Name + " - Type = " + type + " - Timer = " + _AP.TimerRele);
                        ret = 0;
                    } else
                    {
                        //ReleHandleResponse RHR = JsonConvert.DeserializeObject<ReleHandleResponse>(HRM.Content.ToString());
                        LOG.Warn("AccessController.Action - Action ejecutada NO OK! " +
                                   "[AccessPointName = " + _AP.Name + " - Type = " + type + " - Timer = " + _AP.TimerRele +
                                   " => ERROR = " + RHR.code + "-" + RHR.message);
                        ret = RHR.code;
                        msgerr = RHR.message;
                    }
                //} else //Es salida
                //{
                //    ReleHandleResponse RHR = (ReleHandleResponse)RHC.OpRelePulso(origin, Convert.ToInt32(_AP.ReleNumber),
                //                                                                 Convert.ToInt32(_AP.TimerRele));
                //    //if (HRM != null && HRM.StatusCode == HttpStatusCode.OK)
                //    if (RHR != null && RHR.code == 0)
                //    {
                //        LOG.Debug("AccessController.Action - Action ejecutada OK! " +
                //                   "[AccessPointName = " + _AP.Name + " - Type = " + type + " - Timer = " + _AP.TimerRele);
                //        ret = 0;
                //    }
                //    else
                //    {
                //        //ReleHandleResponse RHR = JsonConvert.DeserializeObject<ReleHandleResponse>(HRM.Content.ToString());
                //        LOG.Warn("AccessController.Action - Action ejecutada NO OK! " +
                //                   "[AccessPointName = " + _AP.Name + " - Type = " + type + " - Timer = " + _AP.TimerRele +
                //                   " => ERROR = " + RHR.code + "-" + RHR.message);
                //        ret = RHR.code;
                //        msgerr = RHR.message;
                //    }
                //}
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "AccessControllerUoW.Action Excp: " + ex.Message; 
                LOG.Error("AccessControllerUoW.Action Excp Error: ", ex);
            }
            return ret;
        }

    }
}