﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;

namespace Biometrika.ReleHandle.Web
{
    public partial class _Default : System.Web.UI.Page
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(_Default));

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //txtIPRelay.Text = Global.OBJ_RELE_DEVICE.Ip;
                //txtTypeRelay.Text = Global.OBJ_RELE_DEVICE.ModuleType;
                //txtModuleId.Text = Global.OBJ_RELE_DEVICE.ModuleId.ToString();
                //txtPortRelay.Text = Global.OBJ_RELE_DEVICE.Port.ToString();
                //if (Global.OBJ_RELE_DEVICE.Connected)
                //{
                //    labEstado.ForeColor = Color.Green;
                //    labEstado.Font.Bold = true;
                //    labEstado.Text = "CONNECTED! From " + Global.OBJ_RELE_DEVICE.ConnectedFrom.ToString("dd/MM/yyyy HH:mm:ss");
                //} else
                //{
                //    labEstado.ForeColor = Color.Red;
                //    labEstado.Font.Bold = true;
                //    labEstado.Text = "DICONNECTED!";

                //}

            }
            catch (Exception ex)
            {
                LOG.Error("Default.Page_Load", ex);
            }
        }

        protected void btnGetState_Click(object sender, EventArgs e)
        {
            try
            {
                //string estados = "";
                //int[] res = Global.OBJ_RELE_DEVICE.GetStates();
                //bool isFirst = true;
                //for (int i = 0; i < res.Length; i++)
                //{
                //    if (isFirst)
                //    {
                //        txtEstados.Text = res[i].ToString();
                //        isFirst = false;
                //    }
                //    else txtEstados.Text = txtEstados.Text + " | " + res[i].ToString(); 
                //}
                 

            }
            catch (Exception ex)
            {
                LOG.Error("Default.Page_Load", ex);
            }
        }

        protected void btnReinit_Click(object sender, EventArgs e)
        {
            try
            {
                Global.InitializationRelayDevice();
           }
            catch (Exception ex)
            {
                LOG.Error("Default.btnReinit_Click", ex);
            }
        }

        protected void btnViewLog_Click(object sender, EventArgs e)
        {
            try
            {
                //string tag = "<table><tr><td>11</td></tr></table>";
                //Response.Write(this.Page + "<HR>" +  tag);
                Response.Redirect("ViewLog.aspx");
            }
            catch (Exception ex)
            {
                LOG.Error("Default.btnViewLog_Click", ex);
            }
        }
    }
}