﻿using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Biometrika.ReleHandle.Web.src.Config
{
    public class AccessConfig
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(AccessConfig));

        public List<AccessPoint> AccessPointList { get; set; }

        [XmlIgnore]
        public Hashtable HTAccessPointList;

        public AccessPoint GetAP(string name, string type, out string msgerr)
        {
            AccessPoint _APRet = null;
            msgerr = "";

            try
            {
                foreach (AccessPoint item in AccessPointList)
                {
                    if (item.Name.Equals(name) && item.Type.Equals(type))
                    {
                        _APRet = item;
                        break;
                    }
                }        
            }
            catch (Exception ex)
            {
                _APRet = null;
                msgerr = "Error obteniendo AccessPoint = " + ex.Message;
                LOG.Error("AccessConfig.GetAP Excp Error: ", ex);
            }
            return _APRet;
        }

        public string ToString()
        {
            string ret = "";
            if (AccessPointList != null && AccessPointList.Count > 0)
            {
                bool isFirst = true;
                foreach (AccessPoint item in AccessPointList)
                {
                    if (isFirst)
                    {
                        ret = "Point => Name=" + item.Name + " | Type=" + item.Type + " |Relenumber=" + item.ReleNumber + " | TimerRele=" + item.TimerRele;
                        isFirst = false;
                    } else
                    {
                        ret += Environment.NewLine + "Point => Name=" + item.Name + " | Type=" + item.Type + " |Relenumber=" + item.ReleNumber + " | TimerRele=" + item.TimerRele;
                    }
                }
                return ret;
            } else
            {
                return "AccessPoints NO Configurados!";
            }
        }

        public AccessConfig() { }
    }

    public class AccessPoint
    {
        //Valor unico
        public string Name { get; set; }
        //Valores posibles: ES - Entrada/Salida | E-Entrada | S-Salida
        public string Type { get; set; }
        //Numero de rele asignado
        public string ReleNumber { get; set; }
        //Tiempo de apertura en milisegundos
        public string TimerRele { get; set; }

        public AccessPoint() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="numberele"></param>
        /// <param name="reletimer"></param>
        public AccessPoint(string name, string type, string numberele, string reletimer) {
            Name = name;
            Type = type;
            ReleNumber = numberele;
            TimerRele = reletimer;
        }

    }
}