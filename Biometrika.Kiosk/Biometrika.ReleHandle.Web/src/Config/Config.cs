﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Biometrika.ReleHandle.Web.src.Config
{
    public class Config
    {
        
#region Private

        private string _pathToRquest = @"c:\Biometrika\Rele.Handle\Request";
        private string _pathResponse = @"c:\Biometrika\Rele.Handle\Response";


        private int _typeRelayDevice = 1; //1-USB Orginal | 2-TCP-IP
        
        private int _timerRelay = 2000;

        private string _ipRele = "192.168.100.139";
        private int _portRele = 17494;

        private int _portComUsbRelay = 6;
        private int _qRelay = 4;

        private int _qTxLogRecords = 20;
        private bool _enableEventViewer = false;
        
#endregion Private


#region Public

        public string PathToRquest
        {
            get { return _pathToRquest; }
            set { _pathToRquest = value; }
        }

        public string PathResponse
        {
            get { return _pathResponse; }
            set { _pathResponse = value; }
        }

        public int TypeRelayDevice
        {
            get { return _typeRelayDevice; }
            set { _typeRelayDevice = value; }
        }

        public int TimerRelay
        {
            get { return _timerRelay; }
            set { _timerRelay = value; }
        }

        public string IpRele
        {
            get { return _ipRele; }
            set { _ipRele = value; }
        }

        public int PortRele
        {
            get { return _portRele; }
            set { _portRele = value; }
        }

        public int PortComUsbRelay
        {
            get { return _portComUsbRelay; }
            set { _portComUsbRelay = value; }
        }

        public int QRelay
        {
            get { return _qRelay; }
            set { _qRelay = value; }
        }

        public int QTxLogRecords
        {
            get { return _qTxLogRecords; }
            set { _qTxLogRecords = value; }
        }

        public bool EnableEventViewer
        {
            get { return _enableEventViewer; }
            set { _enableEventViewer = value; }
        }
        

        /// <summary>
        /// Dato necesario para envío de notificacione spor SMTP
        /// </summary>
        public string _SMTPSendTo = "gsuhit@biometrika.cl";
        /// <summary>
        /// Dato necesario para envío de notificacione spor SMTP
        /// </summary>
        public string _SMTPFromTo = "info@biometrika.cl";
        /// <summary>
        /// Dato necesario para envío de notificacione spor SMTP
        /// </summary>
        public string _SMTPSubject = "Aviso ReleHandle WebServer";
        /// <summary>
        /// Dato necesario para envío de notificacione spor SMTP
        /// </summary>
        public string _SMTPServer = "mail.biometrika.cl";
        /// <summary>
        /// Dato necesario para envío de notificacione spor SMTP
        /// </summary>
        public int _SMTPServerPort = 25;
        /// <summary>
        /// Dato necesario para envío de notificacione spor SMTP
        /// </summary>
        public string _SMTPUser = "info@biometrika.cl";
        /// <summary>
        /// Dato necesario para envío de notificacione spor SMTP
        /// </summary>
        public string _SMTPPassword = "B1ometrika";

#endregion Public

        public string ToString()
        {
            string sret = "";
            return sret;
            //    "_pathToEnrollBatch=" + _pathToEnrollBatch + "|" +
            //                  "_pathEnrolledBatch = @"c:\Biometrika\WebAssistance.Kiosk\DataEnroll\EnrolledBatch";

            //private int _authenticationFactor = 2; //Fingerprint default
            //private int _minutiaeType = 7; //Verifinager Default

            //private string _pathLoggerConfig;
            //private string _pathBDConfig;

            //private string _pathMatcherConfig;
            //private string _pathConnectorConfig;

            //private string _assemblyLogin = "WebAssistance.Kiosk.Login.NativeLogin";
            //private string _assemblyLoginParam = "";

            //private int _timerShowResult = 3000;
            //private int _timerRelay = 2000;  //Si es > 0 => Abre, sino no hace nada (para solo admin no hace falta abrir acceso)
            //private int _portComUsbRelay = 6;
            //private bool _portComUsbRelayOpenDoor = false;
            //private int _daystocheckvenc = 30;

            //private float _threshold = 10; //Default para Verifinger
            //private int _matchingType = 1; //1 - First Ok | 2 - Best Ok
            //private float _score = 47; //Default para Verifinger

            //private int _companyId = 6; //Id Empresa en WA
            //private int _companyIdRemote = 6; //Id Empresa en WA en datacenter Central

            //private int _markPoint = 2; //Numero de punto de marcacion de WA
            //private string _codePoint = "SedeSportlife"; //Numero de punto de marcacion de WA
            //private int _companyIdBP = 9; //Id Empresa en BioPortal
            //private int _timeoutWS = 30000; //Timeout para llamados a WS

            //private int _wwoIdDefault = 1;
            //private int _cocIdDefault = 5;

            //private int _withPhoto = 170;
            //private int _heightPhoto = 170;

            //private string _pathPubTop;
            //private string _pathPubLeft;
            //private string _pathFondoKiosk;

            ////Info de Torniquetes
            //private int _qKioskAccessPoints = 1; //Indica cantidad de troniquetes en el acceso
            ////private WAKioskAccessPointConfig WAKioskP1 = null;
            ////private WAKioskAccessPointConfig WAKioskP2 = null;
            //private WAKioskAccessPoint APConfig1;
            //private WAKioskAccessPoint APConfig2;

            //private int _hourToFlash = 8;
            //private int _timeToSleepRefreshBIRs = 60000;
        }
    }
}