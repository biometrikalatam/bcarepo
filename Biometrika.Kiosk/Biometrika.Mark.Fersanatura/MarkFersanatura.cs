﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using System;

namespace Biometrika.Mark.Fersanatura
{
    /// <summary>
    /// Implementacion de IMark para AUtoclub. POr ahora es Dummy porque el registro se hace en el mismo momento 
    /// del chequeo den BusinessRule.
    /// </summary>
    public class MarkFersanatura : IMark
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(MarkFersanatura));

        bool _Initialized; 
        string _Name;
        DynamicData _Config;

        public DynamicData Config
        {
            get
            {
                return _Config;
            }

            set
            {
                _Config = value;
            }
        }

        public bool Initialized
        {
            get
            {
                return this._Initialized;
            }

            set
            {
                this._Initialized = value;
            }
        } 

        public string Name
        {
            get
            {
                return this._Name;
            }

            set
            {
                this._Name = value;
            }
        }

        public int DoMark(DynamicData parameters, out DynamicData returns, out string msg)
        {
            int ret = 0;
            returns = null;
            msg = null;
            try
            {
                LOG.Debug("MarkFersanatura.DoMark IN...");
                //object o = (parameters != null) ? parameters.ParamsList["key"] : null;
                if (parameters == null)
                    LOG.Fatal("MarkFersanatura.DoMark - Parametros NULO!");
                else
                {
                    LOG.Debug("MarkFersanatura.DoMark IN! - parameters.length = " + 
                                parameters.DynamicDataItems.Count.ToString());
                    //foreach (DynamicDataItem item in parameters.ListDynamicDataItems)
                    //{
                    //    LOG.Debug("MarkFersanatura.DoMark - key=" + item.key + " => value = " + (item.value.ToString()));
                    //}
                    if (Config != null) 
                    {
                        if (Config.DynamicDataItems.ContainsKey("TypeMark") &&
                            Config.DynamicDataItems["TypeMark"].Equals("2"))  //Si es API
                        {
                            LOG.Debug("MarkFersanatura.DoMark - Ignora porque es API [ya marco]");
                            //TODO - Mark via API
                        } else
                        {
                            LOG.Fatal("MarkFersanatura.DoMark - Graba en BD Local");
                            ret = DatabaseHelper.SaveMark(parameters);
                            
                        }
                    } else
                    {
                        LOG.Warn("MarkFersanatura.DoMark - Config == null => No graba marca para idRecognized = " +
                                    (string)Config.DynamicDataItems["idRecognized"]);
                    }


                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "MarkFersanatura.Excp [" + ex.Message + "]";
                LOG.Error("MarkFersanatura.DoMark - Excp Error: " + ex.Message);
            }
            return ret;
        }

        public int Intialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("MarkFersanatura.Intialize IN...");
                if (!System.IO.File.Exists("Biometrika.Mark.Fersanatura.cfg"))
                {
                    Config = new DynamicData();
                    Config.AddItem("TypeMark", 1); //1 - Local BD | 2 - API Remota
                    Config.AddItem("URLService", "http://www.dominio.cl/v1/mark");
                    Config.AddItem("URLTimeout", 30000);
                    Config.AddItem("ConectionString", "data source=ServerName;initial catalog=BiometrikaKiosk;user id=sa;password=password;");
                    //_Parameters.Add("", "");
                    if (!SerializeHelper.SerializeToFile(Config, "Biometrika.Mark.Fersanatura.cfg"))
                    {
                        LOG.Warn("MarkFersanatura.Initialize - No grabo condif en disco (Biometrika.Mark.Fersanatura.cfg)");
                    }
                }
                else
                {
                    Config = SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.Mark.Fersanatura.cfg");
                    Config.Initialize();
                }

                if (Config == null)
                {
                    LOG.Fatal("MarkFersanatura.Initialize - Biometrika.Mark.Fersanatura.cfg!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                _Initialized = true;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("MarkFersanatura.Initialize - Excp Error: " + ex.Message);
            }
            return ret;
        }
    }
}
