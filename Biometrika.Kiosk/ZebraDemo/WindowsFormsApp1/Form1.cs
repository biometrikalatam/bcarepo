﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Device;
using Zebra.Sdk.Graphics;
using Zebra.Sdk.Printer;
using Zebra.Sdk.Printer.Discovery;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetUsbPrintersAndAddToList();
        }


        private void GetUsbPrintersAndAddToList()
        {
            try
            {
                //viewModel.UsbDevices.Clear();

                List<DiscoveredPrinterDriver> discoPrinters = UsbDiscoverer.GetZebraDriverPrinters();
                foreach (DiscoveredPrinterDriver printer in discoPrinters)
                {
                    //viewModel.UsbDevices.Add(printer);
                    richTextBox1.Text = printer.PrinterName + Environment.NewLine;
                }
                XmlPrintingExample();
            }
            catch (ConnectionException)
            {
            //    viewModel.UsbDevices.Clear();
            //    viewModel.UsbDevices.Add(new DiscoveredPrinterDriver("OS not supported", "", new List<string>()));
            //    usbPrintersDropdown.SelectedIndex = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        /// These examples demonstrate how to use the one-line printing capability of the XmlPrinter class.
        ///
        /// They assume that a ZPL template containing variable fields appropriate to the XML data
        /// specified exists on the host device. In this case, a PC with the file named XmlPrinterExampleTemplate.zpl
        /// saved at "c:\XmlPrinterExampleTemplate.zpl". The contents of this file should be...
        /// 
        /// ^XA^DFXmlExamp.zpl^FS
        /// ^A0N,100,100^FO100,100^FN1"Name"^FS
        /// ^A0N,100,100^FO100,200^FN2"Street"^FS
        /// ^A0N,100,100^FO100,300^FN3"City"^FS
        /// ^A0N,100,100^FO100,400^FN4"State"^FS
        /// ^A0N,100,100^FO100,500^FN5"Zip"^FS
        /// ^XZ
        private static void XmlPrintingExample()
        {
            // The possible inputs to the one-line XML printing function(s)
            //string destinationDevice = "ZDesigner ZD220-203dpi ZPL"; //"192.168.1.32";
            string destinationDevice = "Microsoft Print to PDF"; //"192.168.1.32";
            string templateFilename = "D:\\tmp\\Installers\\ZebraZD220\\xml1.prn"; //"c:\\XmlPrinterExampleTemplate.zpl";
            string defaultQuantityString = "1";
            bool verbose = true;

            // If the destination device argument is 'null' then any data that would have been
            // sent to a destination device, had one been specified, is captured in 'outputDataStream'.
            // This provides a way to test your output and configuration without having an actual
            // printer available or without wasting media even if a printer is available.
            Console.WriteLine("\nThe destinationDevice connection string argument is null:");
            try
            {
                using (MemoryStream outputDataStream = new MemoryStream())
                {
                    using (Stream sourceStream = GetSampleXmlData())
                    {
                        XmlPrinter.Print(null, sourceStream, templateFilename, defaultQuantityString, outputDataStream, verbose);
                        Console.WriteLine(Encoding.UTF8.GetString(outputDataStream.ToArray()));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            // The outputDataStream argument may be null, in which case the data generated by the XmlPrinter class will
            // not be logged but will be sent to the destination device.
            Console.WriteLine("\nThe outputDataStream argument is null:");
            //try
            //{
            //    using (Stream sourceStream = GetSampleXmlData())
            //    {
            //        XmlPrinter.Print(destinationDevice, sourceStream, templateFilename, defaultQuantityString, null, verbose);
            //    }
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.ToString());
            //}

            // Both destinationDevice connection string AND outputDataStream arguments may be specified, in which case the
            // data generated by the XmlPrinter class will be sent to the destination device and logged to the outputDataStream.
            Console.WriteLine("\nBoth destinationDevice connection string and outputDataStream arguments are specified:");
            try
            {
                using (MemoryStream outputDataStream = new MemoryStream())
                {
                    using (Stream sourceStream = GetSampleXmlData())
                    {
                        XmlPrinter.Print(destinationDevice, sourceStream, templateFilename, defaultQuantityString, outputDataStream, verbose);
                        Console.WriteLine(Encoding.UTF8.GetString(outputDataStream.ToArray()));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            // At least one of these two (destinationDevice connection string and outputDataStream) arguments should be specified.
        }

        private static Stream GetSampleXmlData()
        {
            string sampleXmlData =
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                    + "<file _FORMAT=\"XmlExamp.zpl\">"
                        + " <label>\n"
                            + "     <variable name=\"Name\">John Smith</variable>"
                            + "  </label>\n"
                            + "</file>";

            return new MemoryStream(Encoding.UTF8.GetBytes(sampleXmlData));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //DriverPrinterConnection(printer.PrinterName);
            Connection printerConnection = new DriverPrinterConnection("ZDesigner ZD220-203dpi ZPL");
            //Task.Run(() => {
                try
                {
                    //printerConnection = connectionSelector.GetConnection();
                    printerConnection.Open();
                    ZebraImageI image = ZebraImageFactory.GetImage(@"C:\Users\gsuhi\Pictures\gus.jpg");
                    //if (viewModel.ShouldStoreImage)
                    //{
                    //    ZebraPrinterFactory.GetInstance(printerConnection).StoreImage(viewModel.StoredFileName, image, 540, 412);
                    //}
                    ZebraPrinterFactory.GetInstance(printerConnection).PrintImage(image, 5, 5, 25, 25, false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                //catch (ConnectionException e)
                //{
                //    //MessageBoxCreator.ShowError(e.Message, "Connection Error");
                //}
                //catch (ZebraPrinterLanguageUnknownException e)
                //{
                //    //MessageBoxCreator.ShowError(e.Message, "Connection Error");
                //}
                //catch (IOException e)
                //{
                //    //MessageBoxCreator.ShowError(e.Message, "Image Error");
                //}
                //catch (ZebraIllegalArgumentException e)
                //{
                //    MessageBoxCreator.ShowError(e.Message, "Illegal Arguments");
                //}
                //catch (ArgumentException e)
                //{
                //    MessageBoxCreator.ShowError(e.Message, "Invalid File Path");
                //}
                finally
                {
                    try
                    {
                        if (printerConnection != null)
                        {
                            printerConnection.Close();
                        }
                    }
                    catch (ConnectionException)
                    {
                    }
                    finally
                    {
                        //SetPrintButtonState(true);
                    }
                }
            //});
        }
    }
}
