﻿using log4net;
using log4net.Config;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Relay_Test
{
    static class Program
    {
        //Comentario para probar git
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Program));
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Dictionary<string, string> dic = new Dictionary<string, string>();
            //dic.Add("k1", "v1");
            //dic.Add("k2", "v2");
            //string s = JsonConvert.SerializeObject(dic);

            XmlConfigurator.Configure(
                    new FileInfo(Application.StartupPath + "\\Logger.cfg"));

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Relay_Test());
        }
    }
}
