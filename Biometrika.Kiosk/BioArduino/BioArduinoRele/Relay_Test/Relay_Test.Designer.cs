﻿namespace Relay_Test
{
    partial class Relay_Test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Relay_Test));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tituloCOM = new System.Windows.Forms.Label();
            this.tituloPuerto = new System.Windows.Forms.Label();
            this.detectCom = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.ledRele4 = new System.Windows.Forms.PictureBox();
            this.closeRele4 = new System.Windows.Forms.Button();
            this.openRele4 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.ledRele3 = new System.Windows.Forms.PictureBox();
            this.closeRele3 = new System.Windows.Forms.Button();
            this.openRele3 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.ledRele2 = new System.Windows.Forms.PictureBox();
            this.closeRele2 = new System.Windows.Forms.Button();
            this.openRele2 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.intervalBox = new System.Windows.Forms.TextBox();
            this.intervalCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ledRele1 = new System.Windows.Forms.PictureBox();
            this.closeRele1 = new System.Windows.Forms.Button();
            this.openRele1 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.closeAll = new System.Windows.Forms.Button();
            this.openAll = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.cleanLog = new System.Windows.Forms.Button();
            this.statusBox = new System.Windows.Forms.TextBox();
            this.getStatus = new System.Windows.Forms.Button();
            this.getInfo = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.txtManualCom = new System.Windows.Forms.TextBox();
            this.btnSetManual = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledRele4)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledRele3)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledRele2)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledRele1)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSetManual);
            this.groupBox1.Controls.Add(this.txtManualCom);
            this.groupBox1.Controls.Add(this.tituloCOM);
            this.groupBox1.Controls.Add(this.tituloPuerto);
            this.groupBox1.Controls.Add(this.detectCom);
            this.groupBox1.Location = new System.Drawing.Point(73, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(175, 105);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detectar COM";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // tituloCOM
            // 
            this.tituloCOM.AutoSize = true;
            this.tituloCOM.Location = new System.Drawing.Point(127, 25);
            this.tituloCOM.Name = "tituloCOM";
            this.tituloCOM.Size = new System.Drawing.Size(0, 13);
            this.tituloCOM.TabIndex = 2;
            // 
            // tituloPuerto
            // 
            this.tituloPuerto.AutoSize = true;
            this.tituloPuerto.Location = new System.Drawing.Point(88, 25);
            this.tituloPuerto.Name = "tituloPuerto";
            this.tituloPuerto.Size = new System.Drawing.Size(41, 13);
            this.tituloPuerto.TabIndex = 1;
            this.tituloPuerto.Text = "Puerto:";
            // 
            // detectCom
            // 
            this.detectCom.Location = new System.Drawing.Point(7, 20);
            this.detectCom.Name = "detectCom";
            this.detectCom.Size = new System.Drawing.Size(75, 23);
            this.detectCom.TabIndex = 0;
            this.detectCom.Text = "Detectar";
            this.detectCom.UseVisualStyleBackColor = true;
            this.detectCom.Click += new System.EventHandler(this.detectCom_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox7);
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(13, 123);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(235, 333);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Menu 4 Relé";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.ledRele4);
            this.groupBox7.Controls.Add(this.closeRele4);
            this.groupBox7.Controls.Add(this.openRele4);
            this.groupBox7.Location = new System.Drawing.Point(7, 266);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(216, 57);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Relé 4";
            // 
            // ledRele4
            // 
            this.ledRele4.Image = global::Relay_Test.Properties.Resources.LedOff;
            this.ledRele4.Location = new System.Drawing.Point(176, 16);
            this.ledRele4.Name = "ledRele4";
            this.ledRele4.Size = new System.Drawing.Size(30, 30);
            this.ledRele4.TabIndex = 2;
            this.ledRele4.TabStop = false;
            // 
            // closeRele4
            // 
            this.closeRele4.Enabled = false;
            this.closeRele4.Location = new System.Drawing.Point(93, 20);
            this.closeRele4.Name = "closeRele4";
            this.closeRele4.Size = new System.Drawing.Size(75, 23);
            this.closeRele4.TabIndex = 1;
            this.closeRele4.Text = "Cerrar";
            this.closeRele4.UseVisualStyleBackColor = true;
            this.closeRele4.Click += new System.EventHandler(this.closeRele4_Click);
            // 
            // openRele4
            // 
            this.openRele4.Enabled = false;
            this.openRele4.Location = new System.Drawing.Point(7, 20);
            this.openRele4.Name = "openRele4";
            this.openRele4.Size = new System.Drawing.Size(75, 23);
            this.openRele4.TabIndex = 0;
            this.openRele4.Text = "Abrir";
            this.openRele4.UseVisualStyleBackColor = true;
            this.openRele4.Click += new System.EventHandler(this.openRele4_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.ledRele3);
            this.groupBox6.Controls.Add(this.closeRele3);
            this.groupBox6.Controls.Add(this.openRele3);
            this.groupBox6.Location = new System.Drawing.Point(7, 203);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(216, 57);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Relé 3";
            // 
            // ledRele3
            // 
            this.ledRele3.Image = global::Relay_Test.Properties.Resources.LedOff;
            this.ledRele3.Location = new System.Drawing.Point(176, 16);
            this.ledRele3.Name = "ledRele3";
            this.ledRele3.Size = new System.Drawing.Size(30, 30);
            this.ledRele3.TabIndex = 2;
            this.ledRele3.TabStop = false;
            // 
            // closeRele3
            // 
            this.closeRele3.Enabled = false;
            this.closeRele3.Location = new System.Drawing.Point(93, 20);
            this.closeRele3.Name = "closeRele3";
            this.closeRele3.Size = new System.Drawing.Size(75, 23);
            this.closeRele3.TabIndex = 1;
            this.closeRele3.Text = "Cerrar";
            this.closeRele3.UseVisualStyleBackColor = true;
            this.closeRele3.Click += new System.EventHandler(this.closeRele3_Click);
            // 
            // openRele3
            // 
            this.openRele3.Enabled = false;
            this.openRele3.Location = new System.Drawing.Point(7, 20);
            this.openRele3.Name = "openRele3";
            this.openRele3.Size = new System.Drawing.Size(75, 23);
            this.openRele3.TabIndex = 0;
            this.openRele3.Text = "Abrir";
            this.openRele3.UseVisualStyleBackColor = true;
            this.openRele3.Click += new System.EventHandler(this.openRele3_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.ledRele2);
            this.groupBox5.Controls.Add(this.closeRele2);
            this.groupBox5.Controls.Add(this.openRele2);
            this.groupBox5.Location = new System.Drawing.Point(7, 140);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(216, 57);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Relé 2";
            // 
            // ledRele2
            // 
            this.ledRele2.Image = global::Relay_Test.Properties.Resources.LedOff;
            this.ledRele2.Location = new System.Drawing.Point(176, 16);
            this.ledRele2.Name = "ledRele2";
            this.ledRele2.Size = new System.Drawing.Size(30, 30);
            this.ledRele2.TabIndex = 2;
            this.ledRele2.TabStop = false;
            // 
            // closeRele2
            // 
            this.closeRele2.Enabled = false;
            this.closeRele2.Location = new System.Drawing.Point(93, 20);
            this.closeRele2.Name = "closeRele2";
            this.closeRele2.Size = new System.Drawing.Size(75, 23);
            this.closeRele2.TabIndex = 1;
            this.closeRele2.Text = "Cerrar";
            this.closeRele2.UseVisualStyleBackColor = true;
            this.closeRele2.Click += new System.EventHandler(this.closeRele2_Click);
            // 
            // openRele2
            // 
            this.openRele2.Enabled = false;
            this.openRele2.Location = new System.Drawing.Point(7, 20);
            this.openRele2.Name = "openRele2";
            this.openRele2.Size = new System.Drawing.Size(75, 23);
            this.openRele2.TabIndex = 0;
            this.openRele2.Text = "Abrir";
            this.openRele2.UseVisualStyleBackColor = true;
            this.openRele2.Click += new System.EventHandler(this.openRele2_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.intervalBox);
            this.groupBox4.Controls.Add(this.intervalCheckBox);
            this.groupBox4.Location = new System.Drawing.Point(39, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(154, 51);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Intervalo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(125, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "ms.";
            // 
            // intervalBox
            // 
            this.intervalBox.Enabled = false;
            this.intervalBox.Location = new System.Drawing.Point(80, 18);
            this.intervalBox.MaxLength = 5;
            this.intervalBox.Name = "intervalBox";
            this.intervalBox.Size = new System.Drawing.Size(42, 20);
            this.intervalBox.TabIndex = 1;
            this.intervalBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.intervalBox_KeyPress);
            // 
            // intervalCheckBox
            // 
            this.intervalCheckBox.AutoSize = true;
            this.intervalCheckBox.Enabled = false;
            this.intervalCheckBox.Location = new System.Drawing.Point(7, 20);
            this.intervalCheckBox.Name = "intervalCheckBox";
            this.intervalCheckBox.Size = new System.Drawing.Size(67, 17);
            this.intervalCheckBox.TabIndex = 0;
            this.intervalCheckBox.Text = "Intervalo";
            this.intervalCheckBox.UseVisualStyleBackColor = true;
            this.intervalCheckBox.CheckedChanged += new System.EventHandler(this.intervalCheckBox_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ledRele1);
            this.groupBox3.Controls.Add(this.closeRele1);
            this.groupBox3.Controls.Add(this.openRele1);
            this.groupBox3.Location = new System.Drawing.Point(7, 77);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(216, 57);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Relé 1";
            // 
            // ledRele1
            // 
            this.ledRele1.Image = global::Relay_Test.Properties.Resources.LedOff;
            this.ledRele1.InitialImage = global::Relay_Test.Properties.Resources.LedOff;
            this.ledRele1.Location = new System.Drawing.Point(176, 16);
            this.ledRele1.Name = "ledRele1";
            this.ledRele1.Size = new System.Drawing.Size(30, 30);
            this.ledRele1.TabIndex = 2;
            this.ledRele1.TabStop = false;
            // 
            // closeRele1
            // 
            this.closeRele1.Enabled = false;
            this.closeRele1.Location = new System.Drawing.Point(93, 20);
            this.closeRele1.Name = "closeRele1";
            this.closeRele1.Size = new System.Drawing.Size(75, 23);
            this.closeRele1.TabIndex = 1;
            this.closeRele1.Text = "Cerrar";
            this.closeRele1.UseVisualStyleBackColor = true;
            this.closeRele1.Click += new System.EventHandler(this.closeRele1_Click);
            // 
            // openRele1
            // 
            this.openRele1.Enabled = false;
            this.openRele1.Location = new System.Drawing.Point(7, 20);
            this.openRele1.Name = "openRele1";
            this.openRele1.Size = new System.Drawing.Size(75, 23);
            this.openRele1.TabIndex = 0;
            this.openRele1.Text = "Abrir";
            this.openRele1.UseVisualStyleBackColor = true;
            this.openRele1.Click += new System.EventHandler(this.openRele1_Click_1);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.closeAll);
            this.groupBox8.Controls.Add(this.openAll);
            this.groupBox8.Location = new System.Drawing.Point(254, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(196, 105);
            this.groupBox8.TabIndex = 2;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Opciones Globales";
            // 
            // closeAll
            // 
            this.closeAll.Enabled = false;
            this.closeAll.Location = new System.Drawing.Point(105, 19);
            this.closeAll.Name = "closeAll";
            this.closeAll.Size = new System.Drawing.Size(83, 23);
            this.closeAll.TabIndex = 1;
            this.closeAll.Text = "Cerrar todo";
            this.closeAll.UseVisualStyleBackColor = true;
            this.closeAll.Click += new System.EventHandler(this.closeAll_Click);
            // 
            // openAll
            // 
            this.openAll.Enabled = false;
            this.openAll.Location = new System.Drawing.Point(8, 19);
            this.openAll.Name = "openAll";
            this.openAll.Size = new System.Drawing.Size(83, 23);
            this.openAll.TabIndex = 0;
            this.openAll.Text = "Abrir todo";
            this.openAll.UseVisualStyleBackColor = true;
            this.openAll.Click += new System.EventHandler(this.openAll_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.cleanLog);
            this.groupBox9.Controls.Add(this.statusBox);
            this.groupBox9.Controls.Add(this.getStatus);
            this.groupBox9.Controls.Add(this.getInfo);
            this.groupBox9.Location = new System.Drawing.Point(254, 123);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(198, 334);
            this.groupBox9.TabIndex = 3;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "FeedBack";
            // 
            // cleanLog
            // 
            this.cleanLog.Location = new System.Drawing.Point(8, 305);
            this.cleanLog.Name = "cleanLog";
            this.cleanLog.Size = new System.Drawing.Size(180, 23);
            this.cleanLog.TabIndex = 3;
            this.cleanLog.Text = "Limpiar Log";
            this.cleanLog.UseVisualStyleBackColor = true;
            this.cleanLog.Click += new System.EventHandler(this.cleanLog_Click);
            // 
            // statusBox
            // 
            this.statusBox.Location = new System.Drawing.Point(7, 50);
            this.statusBox.Multiline = true;
            this.statusBox.Name = "statusBox";
            this.statusBox.ReadOnly = true;
            this.statusBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.statusBox.Size = new System.Drawing.Size(183, 249);
            this.statusBox.TabIndex = 2;
            // 
            // getStatus
            // 
            this.getStatus.Enabled = false;
            this.getStatus.Location = new System.Drawing.Point(115, 20);
            this.getStatus.Name = "getStatus";
            this.getStatus.Size = new System.Drawing.Size(75, 23);
            this.getStatus.TabIndex = 1;
            this.getStatus.Text = "Status Placa";
            this.getStatus.UseVisualStyleBackColor = true;
            this.getStatus.Click += new System.EventHandler(this.getStatus_Click);
            // 
            // getInfo
            // 
            this.getInfo.Enabled = false;
            this.getInfo.Location = new System.Drawing.Point(7, 20);
            this.getInfo.Name = "getInfo";
            this.getInfo.Size = new System.Drawing.Size(102, 23);
            this.getInfo.TabIndex = 0;
            this.getInfo.Text = "Información Placa";
            this.getInfo.UseVisualStyleBackColor = true;
            this.getInfo.Click += new System.EventHandler(this.getInfo_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Relay_Test.Properties.Resources.icono_48x48;
            this.pictureBox1.InitialImage = global::Relay_Test.Properties.Resources.icono_48x48;
            this.pictureBox1.Location = new System.Drawing.Point(15, 14);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 48);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // txtManualCom
            // 
            this.txtManualCom.Location = new System.Drawing.Point(91, 64);
            this.txtManualCom.MaxLength = 5;
            this.txtManualCom.Name = "txtManualCom";
            this.txtManualCom.Size = new System.Drawing.Size(42, 20);
            this.txtManualCom.TabIndex = 3;
            // 
            // btnSetManual
            // 
            this.btnSetManual.Location = new System.Drawing.Point(7, 61);
            this.btnSetManual.Name = "btnSetManual";
            this.btnSetManual.Size = new System.Drawing.Size(75, 23);
            this.btnSetManual.TabIndex = 4;
            this.btnSetManual.Text = "Set Manual";
            this.btnSetManual.UseVisualStyleBackColor = true;
            this.btnSetManual.Click += new System.EventHandler(this.btnSetManual_Click);
            // 
            // Relay_Test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 475);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Relay_Test";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Biometrika - Relay Test";
            this.Load += new System.EventHandler(this.Relay_Test_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ledRele4)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ledRele3)).EndInit();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ledRele2)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ledRele1)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label tituloCOM;
        private System.Windows.Forms.Label tituloPuerto;
        private System.Windows.Forms.Button detectCom;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.PictureBox ledRele4;
        private System.Windows.Forms.Button closeRele4;
        private System.Windows.Forms.Button openRele4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.PictureBox ledRele3;
        private System.Windows.Forms.Button closeRele3;
        private System.Windows.Forms.Button openRele3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.PictureBox ledRele2;
        private System.Windows.Forms.Button closeRele2;
        private System.Windows.Forms.Button openRele2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox intervalBox;
        private System.Windows.Forms.CheckBox intervalCheckBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox ledRele1;
        private System.Windows.Forms.Button closeRele1;
        private System.Windows.Forms.Button openRele1;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button closeAll;
        private System.Windows.Forms.Button openAll;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox statusBox;
        private System.Windows.Forms.Button getStatus;
        private System.Windows.Forms.Button getInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button cleanLog;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnSetManual;
        private System.Windows.Forms.TextBox txtManualCom;
    }
}

