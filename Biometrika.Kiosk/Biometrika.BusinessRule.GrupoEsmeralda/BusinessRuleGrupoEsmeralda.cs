﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BusinessRule.GrupoEsmeralda
{
    /// <summary>
    /// Implementacion de BusinessRule para Autoclub Antofagasta. 
    /// Lo realiza conecgtandose a la API de WAIS. 
    /// Usa URL: 
    ///   http://jano.biometrika.cl/AutoClub_Test/API/api/AutoClubRules/CanAccess/RUT/21284415-2/{{registerAccess}}/{{isExit}}/{{token}}
    /// Tambien realiza login y lo deja en el Config para poder trabajar. El token del login no vence, asi que solo 
    /// deberia hacerlo una vez, pero si se elimina, lo realiza de nuevo y lo deja grabado en el config.
    /// </summary>
    public class BusinessRuleGrupoEsmeralda : IBusinessRule
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BusinessRuleGrupoEsmeralda));

        int _Q = 0;

        bool _Initialized;
        DynamicData _Config;

        public DynamicData Config
        {
            get {
                return _Config;
            }

            set {
                _Config = value;
            }
        }

        public bool Initialized
        {
            get {
                return _Initialized;
            }

            set {
                _Initialized = value;
            }
        }
      
        public int CheckBusinessRule(DynamicData parameters, out bool accessresult, out DynamicData returns, out string msg)
        {
            int ret = 0;
            accessresult = false;
            returns = null;
            msg = null;
            string typeid, valueid;
            bool isExit;
            bool registerAccess;
            /*
                1.- Tomo parametro de entrada y chequeo integridad
                2.- Consumo servicio
                    2.1.- Si es Check y Marca segun config => Chequeo y registro.
                    2.2.- Si es colo check segun config => cehqeo e informo
                3.- Retorno resultado
                              
                    -1 Error interno
                    -2 APB
                    -3 Socio sin acceso (deuda o inactivo)
                    -4 Visita sin acceso (sin evento/invitación encontrado)
                    -5 Visita sin acceso (socio autorizador no tiene invitaciones disponibles)
                    -6 Identidad no existe
            */
            try
            {
                LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule IN...");
                //1.- Tomo parametro de entrada y chequeo integridad
                //if (parameters == null || parameters.DynamicDataItems == null ||
                //    (!parameters.DynamicDataItems.ContainsKey("typeid") ||
                //     !parameters.DynamicDataItems.ContainsKey("valueid")) ||
                //     !parameters.DynamicDataItems.ContainsKey("checktype"))
                if (parameters == null || parameters.DynamicDataItems == null ||
                    !parameters.DynamicDataItems.ContainsKey("idRecognized") || 
                     !parameters.DynamicDataItems.ContainsKey("checktype"))
                {
                    ret = Errors.IERR_BAD_PARAMETER;
                    msg = "Error chequeando BusinessRule - Parametros nulos o faltantes...";
                    LOG.Warn("BusinessRuleGrupoEsmeralda.CheckBusinessRule - " + msg);
                    return ret;
                } else //Estamso ok => Tomo valores
                {
                    typeid = "RUT"; //(string)parameters.DynamicDataItems["typeid"];
                    valueid = (string)parameters.DynamicDataItems["idRecognized"];
                    //isExit = ((int)parameters.DynamicDataItems["checktype"] == 2);
                    LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule - Chequeando BusinessRule de typeid/valueid => " +
                                typeid + "/" + valueid + " - CheckingType=" + 
                                parameters.DynamicDataItems["checktype"].ToString());
                }
                //Detecto sentido de marca
                if (((string)parameters.DynamicDataItems["idDevice"]).Equals(Config.DynamicDataItems["PortCOMBarcodeIn"]) ||
                    ((string)parameters.DynamicDataItems["idDevice"]).Equals(Config.DynamicDataItems["SerialSensorIn"]))
                {
                    isExit = false;
                } else
                {
                    isExit = true;
                }

                //2.- Consumo servicio {registerAccess}}/{{isExit}}/{{token}}
                LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule - Setting ServiceType => " + 
                          ((int)Config.DynamicDataItems["ServiceType"]).ToString());
                registerAccess = ((int)Config.DynamicDataItems["ServiceType"]) == 1;
                LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule - Setting Checking Type (IN/OUT) => isExit = " +
                          isExit.ToString());

                //Consumo servicio + "api/AutoClubRules/CanAccess/" +
                string sURL = !((string)Config.DynamicDataItems["ServiceURL"]).EndsWith("/") ?
                                ((string)Config.DynamicDataItems["ServiceURL"]) + "/" :
                                ((string)Config.DynamicDataItems["ServiceURL"]);
                sURL = sURL + typeid + "/" + valueid + "/" + registerAccess.ToString() + "/" +
                             isExit.ToString() + "/" + Config.DynamicDataItems["ServiceAccessToken"];
                LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule - LLama a " + sURL + "...");
                var client = new RestClient(sURL);
                client.Timeout = (int)Config.DynamicDataItems["ServiceTimeout"];
                var request = new RestRequest(Method.GET);
                
                LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule - call execute...");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    AccessModelR oResponse = JsonConvert.DeserializeObject<AccessModelR>(response.Content);
                    if (oResponse != null)
                    {
                        //&& oResponse.Message.Equals("Identidad no registrada")
                        LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule - Parseando resultado...");
                        if (oResponse.Status.Equals("Error") && oResponse.Data.CanAccess.Equals("-6") )
                        {
                            ret = Errors.IERR_IDENTITY_NOT_FOUND;
                            msg = "No Registrado!";
                            LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule - Response No Registrado! [ret = " + ret + "]");
                        }
                        else
                        {
                            //TODO: Parsear nombre y foto para poder mostrar en el kiosko
                            returns = new DynamicData();
                            //if (oResponse.Status.Equals("Success") && oResponse.Data.Equals("1"))  
                            //{
                            //    accessresult = true;
                            //}

                            accessresult = (oResponse.Status.Equals("Success") && oResponse.Data.CanAccess.ToString().Equals("1"));
                            msg = oResponse.Message;
                            if (oResponse.Data != null && oResponse.Data.Identity != null)
                            {
                                string nombre = (string.IsNullOrEmpty(oResponse.Data.Identity.Nombres) ? "" : oResponse.Data.Identity.Nombres) + " "
                                      + (string.IsNullOrEmpty(oResponse.Data.Identity.ApePat) ? "" : oResponse.Data.Identity.ApePat) + " "
                                      + (string.IsNullOrEmpty(oResponse.Data.Identity.ApeMat) ? "" : oResponse.Data.Identity.ApeMat);
                                LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule - CanAccess ="  + accessresult.ToString() + 
                                          " - RUT= " + valueid + " - Nombre=" + (string.IsNullOrEmpty(nombre)?"":nombre));
                                returns.AddItem("Name", nombre);
                                returns.AddItem("Photografy", oResponse.Data.Identity.Foto);
                                returns.AddItem("DateOfBirth", oResponse.Data.Identity.NacDate.ToString());
                                returns.AddItem("Code", oResponse.Data.CanAccess);
                            }
                            else
                            {
                                LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule - CanAccess =" + accessresult.ToString() +
                                          " - RUT= " + valueid + " - Datos Vacios");
                                returns.AddItem("Name",null);
                                returns.AddItem("Photografy", null);
                                returns.AddItem("DateOfBirth", null);
                                returns.AddItem("Code", oResponse.Data.CanAccess);
                            }

                            LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule - Response OK");
                            ret = 0;
                        }
                    } else
                    {
                        LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule - Error parseando respuesta del servicio...");
                        ret = Errors.IERR_DESERIALIZING_DATA;
                        accessresult = false;
                        msg = "Error chequeando acceso! [Respuesta Nula, consulte a su adminsitrador...]";
                    }
                }
                else
                {
                    LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule - Response Error");
                    msg = "Error del servicio: " + (response != null ? response.StatusCode.ToString() : "Null");
                    ret = Errors.IERR_CONX_WS;
                }

                //if (_Q % 2 == 0)
                //{
                //    accessresult = true;
                //    msg = "Acceso Permitido!";
                //    returns = new DynamicData();
                //    returns.AddItem("ParamReturnKey", _Q);
                //} else
                //{
                //    accessresult = false;
                //    msg = "Acceso NO Permitido [_Q=" + _Q.ToString() + "]";
                //}
                //_Q++;
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "BusinessRuleGrupoEsmeralda Excp [" + ex.Message + "]";
                LOG.Error("BusinessRuleGrupoEsmeralda.CheckBusinessRule - Excp Error: " + ex.Message);
            }
            LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule OUT! - ret = " + ret.ToString() + 
                      " - accessresult=" + accessresult.ToString());
            return ret;
        }


        public int Initialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("CheckBusinessRule.Intialize IN...");
                if (!System.IO.File.Exists("Biometrika.BusinessRule.GrupoEsmeralda.cfg"))
                {
                    Config = new DynamicData();
                    //Config.ConfigItems.Add("BusinessRuleDllPath", "Biometrika.BusinessRule.AutoclubAntofagasta");
                    //Parameters.Add("QueryCmd",
                    //        "SELECT TOP (200) bpiden.id AS id, bpiden.valueid, bpr.authenticationfactor, bpr.minutiaetype, bpr.bodypart, bpr.data " +
                    //        "  FROM bp_bir AS bpr INNER JOIN bp_identity AS bpiden ON bpr.identid = bpiden.id " +
                    //        " WHERE (bpr.minutiaetype = 7) AND (bpiden.companyidenroll = 7) AND bpiden.valueid = '21284415-2'");
                    //Config.ConfigItems.Add("URLService", "http://localhost/BS");
                    //Config.ConfigItems.Add("URLTimeout", 30000);
                    Config.AddItem("ServiceURL", "http://jano.biometrika.cl/AutoClub_Test/API/api/");
                    Config.AddItem("ServiceTimeout", 30000);
                    Config.AddItem("ServiceAccessToken", "794C0A6E5CE5480886E9F6CBC523094AC0535C58FA79EB81C6367902B5E204EC7EFF765F76DA693764A9EEFCE16A090F");
                    Config.AddItem("ServiceUser", "totemvehicular@autoclub");
                    Config.AddItem("ServicePassword", "fdbfeec3c613d7d0fcf6aecaf5c5be9a");
                    Config.AddItem("ServicePointName", "TotemVehicular");
                    Config.AddItem("ServiceType", 1); //0-Solo Check | 1-Check y Register
                    if (!SerializeHelper.SerializeToFile(Config, "Biometrika.BusinessRule.GrupoEsmeralda.cfg"))
                    {
                        LOG.Warn("BusinessRuleGrupoEsmeralda.Initialize - No grabo condig en disco (Biometrika.BusinessRule.GrupoEsmeralda.cfg)");
                    }
                }  
                else
                {
                    Config = SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.BusinessRule.GrupoEsmeralda.cfg");
                    if (Config != null) //Check nulo
                    {
                        Config.Initialize();
                        if (!Config.DynamicDataItems.ContainsKey("ServiceAccessToken") ||
                             string.IsNullOrEmpty((string)Config.DynamicDataItems["ServiceAccessToken"]))
                        {
                            ActualizeAccessToken();
                        }
                        Config.Initialize();
                        if (!SerializeHelper.SerializeToFile(Config, "Biometrika.BusinessRule.GrupoEsmeralda.cfg"))
                        {
                            LOG.Warn("BusinessRuleGrupoEsmeralda.Initialize - No grabo condig en disco (Biometrika.BusinessRule.GrupoEsmeralda.cfg)");
                        }
                    } else
                    {
                        LOG.Fatal("BusinessRuleGrupoEsmeralda.Initialize - No parseo la config. Elimine archivo Biometrika.BusinessRule.GrupoEsmeralda.cfg y reintente...");
                    }
                }

                if (Config == null)
                {
                    LOG.Fatal("BusinessRuleGrupoEsmeralda.Initialize - Error leyendo BusinessRuleGrupoEsmeralda!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                _Initialized = true;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BusinessRuleGrupoEsmeralda.Initialize Excp Error: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Revisa 
        /// </summary>
        private void ActualizeAccessToken()
        {
            try
            {
                LOG.Debug("BusinessRuleGrupoEsmeralda.ActualizeAccessToken IN...");

                //Consumo servicio
                //http://jano.biometrika.cl/AutoClub_Test/API/api/Account/Login/
                //              totemvehicular @autoclub/fdbfeec3c613d7d0fcf6aecaf5c5be9a/TotemVehicular
                string sURL = Config.DynamicDataItems["ServiceURL"] + "api/Account/Login/" +
                                Config.DynamicDataItems["ServiceUser"] + "/" +
                                Config.DynamicDataItems["ServicePassword"] + "/" +
                                Config.DynamicDataItems["ServicePointName"];
                LOG.Debug("BusinessRuleGrupoEsmeralda.CheckBusinessRule - LLama a " + sURL + "...");
                var client = new RestClient(sURL);
                client.Timeout = (int)Config.DynamicDataItems["ServiceTimeout"];
                var request = new RestRequest(Method.GET);

                LOG.Debug("BusinessRuleGrupoEsmeralda.ActualizeAccessToken - call execute...");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    AccessModelRL oResponse = JsonConvert.DeserializeObject<AccessModelRL>(response.Content);
                    if (oResponse != null)
                    {
                        LOG.Debug("BusinessRuleGrupoEsmeralda.ActualizeAccessToken - Parseando resultado...");
                        if (oResponse.Status.Equals("Success") && !string.IsNullOrEmpty(oResponse.Data))
                        {
                            Config.UpdateItem("ServiceAccessToken", oResponse.Data);
                        }
                        LOG.Debug("BusinessRuleGrupoEsmeralda.ActualizeAccessToken - Response OK");
                    }
                    else
                    {
                        LOG.Debug("BusinessRuleGrupoEsmeralda.ActualizeAccessToken - Error parseando respuesta del servicio...");
                    }
                }
                else
                {
                    LOG.Debug("BusinessRuleGrupoEsmeralda.ActualizeAccessToken - Response Error");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BusinessRuleGrupoEsmeralda.ActualizeAccessToken Excp Error: " + ex.Message);
            }
        }

        private Image SetImageFromB64(string base64Photo)
        {
            Image ret = null;
            try
            {
                if (!string.IsNullOrEmpty(base64Photo))
                {
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(base64Photo));
                    ret = Image.FromStream(ms);
                    ms.Close();
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("BusinessRuleGrupoEsmeralda.SetImageFromB64 Excp Error: " + ex.Message);
            }
            return ret;
        }
    }
}
