using System;

namespace Bio.Core.Wsq.Decoder
{
	/// <summary>
	/// Descripción breve de EntropyDecoder.
	/// </summary>
	public sealed class EntropyDecoder
	{
		private HuffmanTable huffmanTable;
		private EntropyEncodedBlock ecs;

		internal EntropyDecoder (EntropyEncodedBlock ecs)
		{
			if (ecs == null)
				throw new DecoderException("EntropyEncodedBlock no puede ser nulo");
			this.ecs = ecs;
			huffmanTable = ecs.GetHuffmanTable ();
			if (huffmanTable == null)
				throw new DecoderException("Huffmantable no puede ser nulo");
			
		}


		internal bool Decode (short[] image, ref int imgPos)
		{
			
			code = code2 = 0;
			int bitCount = 0;
			ushort marker = 0;
			ushort tbits = 0;
			int nodePointer = 0;
			int pos = 0;
			while (imgPos < image.Length && DecodeData (ref nodePointer, ref pos, ref bitCount, ref marker, image.Length))
			{
				if (nodePointer == -1)
					continue;

				if (nodePointer > 0 && nodePointer <= 100)
					ZeroFill((ushort) nodePointer, image, ref imgPos);
				else if (nodePointer > 106 && nodePointer < 255)
					image [imgPos++] = (short) (nodePointer - 180);
				else if(nodePointer == 101 || nodePointer == 102)
				{
					if (!GetNextBits (ref tbits, ref marker, ref pos, ref bitCount, 8, image.Length))
						return false;
					image [imgPos++] = nodePointer == 101 ? (short) tbits : (short) (-tbits);
				}
				else if (nodePointer == 103 || nodePointer == 104)
				{
					if (!GetNextBits (ref tbits, ref marker, ref pos, ref bitCount, 16, image.Length))
						return false;
					image [imgPos++] = nodePointer == 103 ? (short) tbits : (short) (-tbits);
				}
				else if (nodePointer == 105 || nodePointer == 106)
				{
					if (!GetNextBits (ref tbits, ref marker, ref pos, ref bitCount, nodePointer == 105 ? 8 : 16, image.Length))
						return false;
					ZeroFill (tbits, image, ref imgPos);
				}
				else
					image[imgPos++] = 0;
			}
			return true;
		}

		private static void ZeroFill (ushort tbits, short[] image, ref int imgPos)
		{
			Array.Clear(image, imgPos, tbits);
			imgPos += tbits;
		}

		private bool DecodeData (ref int outputNode, ref int pos, ref int bitCount, ref ushort marker, int imageLen)
		{
			ushort code = 0, tbits = 0;
			outputNode = 0;
			if (!GetNextBits (ref code, ref marker, ref pos, ref bitCount, 1, imageLen))
				return false;

			if (marker != 0)
			{
				outputNode = -1;
				return true;
			}
			int inx, inx2;
			for (inx = 1; (int) code > huffmanTable.MaxCode [inx]; inx++)
			{
				if (!GetNextBits (ref tbits, ref marker, ref pos, ref bitCount, 1, imageLen))
					return false;
				code = (ushort) ((code << 1) + tbits);
				if (marker != 0)
				{
					outputNode = -1;
					return true;
				}
			}
			inx2 = huffmanTable.GetValuePointer(inx);
			inx2 = inx2 + code - huffmanTable.MinCode [inx];
			outputNode = huffmanTable.GetHuffmanValue(inx2);
			return true;
		}

		private bool GetNextBits (ref ushort obits, ref ushort marker, ref int pos, ref int bitCount, int bitsRequired, int imageLen)
		{
			ushort bits;
			ushort tbits = 0;
			obits = marker = 0;

			if (bitCount == 0)
			{
				if (!ecs.GetByte (pos++, out code))
					return false;

				bitCount = 8;
				if (code == 0xFF)
				{
					if (!ecs.GetByte (pos++, out code2))
						return false;
					if (code2 != 0x00 && bitsRequired == 1)
					{
						marker = (ushort) ((code << 8) | code2);
						obits = 1;
						return true;
					}
					if (code2 != 0x00)
						return false;
				}
			}
			if (bitsRequired > bitCount)
			{
				int bitsNeeded = bitsRequired - bitCount;
				bits = (ushort) (code << bitsNeeded);
				bitCount = 0;
				ushort mmarker = 0;
				if (!GetNextBits (ref tbits, ref mmarker, ref pos, ref bitCount, bitsNeeded, imageLen))
					return false;
				bits |= tbits;
			}
			else
			{
				bits = (ushort) ((code >> (bitCount - bitsRequired)) & BitMask [bitsRequired]);
				bitCount -= bitsRequired;
				code &= BitMask [bitCount];
			}
			obits = bits;
			return true;
		}

		private byte code;
		private byte code2;
		private static readonly byte[] BitMask = {0x00, 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff};

	}
}