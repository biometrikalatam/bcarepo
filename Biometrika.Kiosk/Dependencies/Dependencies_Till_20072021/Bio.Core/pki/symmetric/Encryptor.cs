using System;
using System.Security.Cryptography;
using Org.Mentalis.Security.Cryptography;
using log4net;

namespace Bio.Core.pki.symmetric
{
	/// <summary>
	/// Descripción breve de Encryptor.
	/// </summary>
	public class Encryptor
	{
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Encryptor));

		//Tipos de algoritmos posibles
		public const int ALG_TripleDES = 1;
		public const int ALG_RC4 = 2;
		public const int ALG_ARCFour = 3;
		public const int ALG_Rijndael = 4;

		public byte[] key;
		public byte[] IV;
		public SymmetricAlgorithm symalg;

		ICryptoTransform encryptor;
		ICryptoTransform decryptor;

		public Encryptor()
		{
			symalg = new TripleDESCryptoServiceProvider();
			symalg.GenerateKey();
			symalg.GenerateIV();
			encryptor = symalg.CreateEncryptor();
			decryptor = symalg.CreateDecryptor();
		}

		public Encryptor(byte[] key, byte[] IV, int algoritmo)
		{
			this.key = key;
			this.IV = IV;
			symalg = Encryptor.GetAlgoritmo(algoritmo);
			symalg.Key = key;
			symalg.IV = IV;
			encryptor = symalg.CreateEncryptor();
			decryptor = symalg.CreateDecryptor();
		}

		public Encryptor(int algoritmo) {
			symalg = Encryptor.GetAlgoritmo(algoritmo);
			symalg.GenerateKey();
			symalg.GenerateIV();
			encryptor = symalg.CreateEncryptor();
			decryptor = symalg.CreateDecryptor();
			this.key = symalg.Key;
			this.IV = symalg.IV;
		}

		public bool GenerateKey(int algoritmo)
		{
			SymmetricAlgorithm alg = null;
			try
			{

				if (symalg == null) {
					alg = Encryptor.GetAlgoritmo(algoritmo);
				} else
				{
					alg = symalg;
				}

				alg.GenerateKey();
				alg.GenerateIV();
				this.key = alg.Key;
				this.IV = alg.IV;
				
			} catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				return false;
			}
			return true;
		}

		public bool LoadKey(string path)
		{
			return true;
		}

		public static bool Encrypt(object dataToEncrypt, 
									bool dataToEncryptInBase64, 
								    int algoritmo, 
									ref object dataEncrypted, 
									bool dataEncryptedInBase64,
									byte[] key, byte[] IV)
		{
			ICryptoTransform encryptor;
			SymmetricAlgorithm algorithm;
			byte[] plaintext;
			byte[] encrypted;
			try {
				
				// initialize the selected symmetric algorithm
				algorithm = Encryptor.GetAlgoritmo(algoritmo);
				
				if (dataToEncryptInBase64)
				{
					plaintext = Convert.FromBase64String((string)dataToEncrypt);				
				} else
				{
					plaintext = (byte[])dataToEncrypt;
				}
				// generate an IV that consists of bytes with the value zero
				// in real life applications, the IV should not be set to
				// an array of bytes with the value zero!
				algorithm.IV = IV;
				// generate a new key
				algorithm.Key = key;
				// create the encryption and decryption objects
				encryptor = algorithm.CreateEncryptor();
				// encrypt the bytes
				encrypted = encryptor.TransformFinalBlock(plaintext, 0, plaintext.Length);

				if (dataEncryptedInBase64)
				{
					dataEncrypted = Convert.ToBase64String(encrypted);
				} else
				{
					dataEncrypted = encrypted;
				}				
				
				// dispose of the resources
				algorithm.Clear();
				encryptor.Dispose();
				
			} catch (Exception ex) {
                LOG.Error("Encryptor.Encrypt", ex);
				return false;
			}
			return true;
		}

		public bool Encrypt(object dataToEncrypt, 
							bool dataToEncryptInBase64, 
							ref object dataEncrypted, 
							bool dataEncryptedInBase64) {
			byte[] plaintext;
			byte[] encrypted;
			try {

				if (this.symalg == null)
				{
					return false;
				}
			
				if (dataToEncryptInBase64) {
					plaintext = Convert.FromBase64String((string)dataToEncrypt);				
				} else {
					plaintext = (byte[])dataToEncrypt;
				}

				encrypted = encryptor.TransformFinalBlock(plaintext, 0, plaintext.Length);

				if (dataEncryptedInBase64) {
					dataEncrypted = Convert.ToBase64String(encrypted);
				} else {
					dataEncrypted = encrypted;
				}				
							
			} catch (Exception ex) {
                LOG.Error("Encryptor.Encrypt", ex);
				return false;
			}
			return true;
		}

		public static bool Decrypt(object dataToDecrypt, 
									bool dataToDecryptInBase64, 
									int algoritmo, 
									ref object dataDecrypted, 
									bool dataDecryptedInBase64,
									byte[] key, byte[] IV) {
			ICryptoTransform decryptor;
			SymmetricAlgorithm algorithm;
			byte[] encrypted;
			byte[] decrypted;
			try {
				
				// initialize the selected symmetric algorithm
				algorithm = Encryptor.GetAlgoritmo(algoritmo);
				
				if (dataToDecryptInBase64) {
					encrypted = Convert.FromBase64String((string)dataToDecrypt);				
				} else {
					encrypted = (byte[])dataToDecrypt;
				}
				// generate an IV that consists of bytes with the value zero
				// in real life applications, the IV should not be set to
				// an array of bytes with the value zero!
				algorithm.IV = IV;
				// generate a new key
				algorithm.Key = key;
				
				if (algoritmo == ALG_TripleDES)
				{
					algorithm.Mode = CipherMode.ECB;	
				}
				
				// create the encryption and decryption objects
				decryptor = algorithm.CreateDecryptor(key,IV);

				
				// encrypt the bytes
				decrypted = decryptor.TransformFinalBlock(encrypted, 0, encrypted.Length);

				if (dataDecryptedInBase64) {
					dataDecrypted = Convert.ToBase64String(decrypted);
				} else {
					dataDecrypted = decrypted;
				}				
				
				// dispose of the resources
				algorithm.Clear();
				decryptor.Dispose();
				
			} catch (Exception ex) {
                LOG.Error("Encryptor.Decrypt", ex);
				return false;
			}
			return true;
		}

		public bool Decrypt(object dataToDecrypt, 
			 				 bool dataToDecryptInBase64, 
							 ref object dataDecrypted, 
							 bool dataDecryptedInBase64) {
			byte[] encrypted;
			byte[] decrypted;
			try {
				
				if (this.symalg == null) {
					return false;
				}

				if (dataToDecryptInBase64) {
					encrypted = Convert.FromBase64String((string)dataToDecrypt);				
				} else {
					encrypted = (byte[])dataToDecrypt;
				}
				
				decrypted = decryptor.TransformFinalBlock(encrypted, 0, encrypted.Length);

				if (dataDecryptedInBase64) {
					dataDecrypted = Convert.ToBase64String(decrypted);
				} else {
					dataDecrypted = decrypted;
				}				
				
			} catch (Exception ex) {
                LOG.Error("Encryptor.Decrypt", ex);
				return false;
			}
			return true;
		}


		public void Clear()
		{
			this.key = null;
			this.IV = null;
			this.symalg.Clear();
			this.encryptor.Dispose();
			this.decryptor.Dispose();
		}

		~Encryptor()
		{
			this.key = null;
			this.IV = null;
			this.symalg.Clear();
			this.encryptor.Dispose();
			this.decryptor.Dispose();		
		}

		public static SymmetricAlgorithm GetAlgoritmo(int algoritmo)
		{
			// initialize the selected symmetric algorithm
			switch(algoritmo) {
				case Encryptor.ALG_ARCFour:
					return (new ARCFourManaged());
				case Encryptor.ALG_RC4:
					return (new RC4CryptoServiceProvider());
				case Encryptor.ALG_Rijndael:
					return (new RijndaelCryptoServiceProvider());
				case Encryptor.ALG_TripleDES:
					return (new TripleDESCryptoServiceProvider());
				default:
					return null;
			}
		}

	}
}
