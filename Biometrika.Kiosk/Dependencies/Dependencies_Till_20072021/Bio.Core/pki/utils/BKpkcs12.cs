using log4net;
using System;
using System.Collections;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
//using CAPICOM;

namespace Bio.Core.pki.utils
{
    public class BKpkcs12
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BKpkcs12));

        #region Constructores		

        /// <summary>
        /// 
        /// </summary>
        public BKpkcs12() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="psw"></param>
        public BKpkcs12(string path, string psw)
        {
            this.certificate = BKpkcs12.BKOpenPFX(path, psw);
        }

        #endregion Constructores		

        #region Public Methods		

        /// <summary>
        /// retorna clave para firma
        /// </summary>
        /// <param name="path"></param>
        /// <param name="psw"></param>
        /// <returns></returns>
        public static AsymmetricAlgorithm GetPrivateKeyFromPFX(string path, string psw)
        {
            X509Certificate2 cRet = null;
            AsymmetricAlgorithm cAux = null;
            try
            {
                cRet = new X509Certificate2();
                LOG.Debug("BKpkcs12.GetPrivateKeyFromPFX.Import " + path);
                cRet.Import(path, psw, X509KeyStorageFlags.PersistKeySet);
                cAux = cRet.PrivateKey;
            }
            catch (Exception ex)
            {
                LOG.Error("BKpkcs12.GetPrivateKeyFromPFX - " + ex.Message);
                cAux = null;
            }
            return cAux;
        }

        /// <summary>
        /// Retorna certificado
        /// </summary>
        /// <param name="path"></param>
        /// <param name="psw"></param>
        /// <returns></returns>
        public static X509Certificate2 BKOpenPFX(string path, string psw)
        {
            X509Certificate2 cRet = null;
            X509Certificate2 cAux = null;
            try
            {
                cRet = new X509Certificate2();
                LOG.Debug("BKpkcs12.BKOpenPFX.Import " + path);
                cRet.Import(path, psw, X509KeyStorageFlags.PersistKeySet);

            }
            catch (Exception ex)
            {
                LOG.Error("BKpkcs12.BKOpenPFX - " + ex.Message);
                cRet = null;
            }
            return cRet;
        }


        #endregion Public Methods		

        #region Public Properties

        public X509Certificate2 Certificate
        {
            get { return certificate; }
            set { certificate = value; }
        }

        #endregion Public Properties

        #region Private

        X509Certificate2 certificate;

        #endregion Private
        
    }
}
