﻿namespace Bio.Core.Matcher.Constant
{

    ///<summary>
    ///</summary>
    public class MinutiaeType
    {

        static public int MINUTIAETYPE_ANY = -2;
        static public string SMINUTIAETYPE_ANY = "Cualquiera";

        static public int MINUTIAETYPE_ALL = -1;
        static public string SMINUTIAETYPE_ALL = "Todas";

        static public int MINUTIAETYPE_NONE = 0;
        static public string SMINUTIAETYPE_NONE = "Ninguna";

		static public int MINUTIAETYPE_NEC = 1;
        static public string SMINUTIAETYPE_NEC = "NEC";

        static public int MINUTIAETYPE_DIGITALPERSONA = 2;
        static public string SMINUTIAETYPE_DIGITALPERSONA = "Digital Persona";

        static public int MINUTIAETYPE_DIGITALPERSONAGOLD = 3;
        static public string SMINUTIAETYPE_DIGITALPERSONAGOLD = "Digital Persona Gold";

        static public int MINUTIAETYPE_DIGITALPERSONAOTW = 4;
        static public string SMINUTIAETYPE_DIGITALPERSONAOTW = "Digital Persona";

        static public int MINUTIAETYPE_SECUGEN = 5;
        static public string SMINUTIAETYPE_SECUGEN = "Secugen";

        static public int MINUTIAETYPE_TESTECH = 6;
        static public string SMINUTIAETYPE_TESTECH = "Testech";

        static public int MINUTIAETYPE_VERIFINGER = 7;
        static public string SMINUTIAETYPE_VERIFINGER = "Verifinger";

        static public int MINUTIAETYPE_SAGEM = 8;
        static public string SMINUTIAETYPE_SAGEM = "Sagem";

        static public int MINUTIAETYPE_IDENTIX = 9;
        static public string SMINUTIAETYPE_IDENTIX = "Identix";

        static public int MINUTIAETYPE_UPEK = 10;
        static public string SMINUTIAETYPE_UPEK = "Upek";

        static public int MINUTIAETYPE_COGENT = 11;
        static public string SMINUTIAETYPE_COGENT = "Cogent";

        static public int MINUTIAETYPE_CROSSMATCH = 12;
        static public string SMINUTIAETYPE_CROSSMATCH = "Crossmatch";
		
		static public int MINUTIAETYPE_ANSI_INSITS_378_2004 = 13;
        static public string SMINUTIAETYPE_ANSI_INSITS_378_2004 = "ANSI INSITS 378-2004"; //ANSI Fingerprint
		
		static public int MINUTIAETYPE_ISO_IEC_19794_2_2005 = 14; //ISO Fingerprint
        static public string SMINUTIAETYPE_ISO_IEC_19794_2_2005 = "ISO/IEC 19794-2:2005";
	
        static public int MINUTIAETYPE_PASSWORD = 20;
        static public string SMINUTIAETYPE_PASSWORD = "Clave";

        static public int MINUTIAETYPE_WSQ = 21;
        static public string SMINUTIAETYPE_WSQ = "WSQ";

        static public int MINUTIAETYPE_RAW = 22;
        static public string SMINUTIAETYPE_RAW = "RAW";

        static public int MINUTIAETYPE_INCITS_381_2004 = 23; //ANSI IMAGE
        static public string SMINUTIAETYPE_INCITS_381_2004 = "INCITS 381-2004";    
		
		static public int MINUTIAETYPE_TOKEN = 24;
        static public string SMINUTIAETYPE_TOKEN = "TOKEN";

        static public int MINUTIAETYPE_PDF417CEDULA = 25;
        static public string SMINUTIAETYPE_PDF417CEDULA = "PDF417CEDULA";
		
        static public int MINUTIAETYPE_HANDKEY = 30;
        static public string SMINUTIAETYPE_HANDKEY = "Handkey";

        static public int MINUTIAETYPE_FACIAL = 40;     //Si es Facial => Es Imagen
        static public string SMINUTIAETYPE_FACIAL = "Facial";
                          
        static public int MINUTIAETYPE_JPG = 41;     //Si es Facial => Es Imagen
        static public string SMINUTIAETYPE_JPG = "JPG";

        static public int MINUTIAETYPE_FACIAL_F7 = 42;
        static public string SMINUTIAETYPE_FACIAL_F7 = "F7 Facial";

        static public int MINUTIAETYPE_FACIAL_VERILOOK = 43;
        static public string SMINUTIAETYPE_FACIAL_VERILOOK = "VeriLook Facial";

        static public int MINUTIAETYPE_FACIAL_NAMKU = 44;
        static public string SMINUTIAETYPE_FACIAL_NAMKU = "Namku";

        static public int MINUTIAETYPE_FACIAL_FACEMAP = 45;
        static public string SMINUTIAETYPE_FACIAL_FACEMAP = "FaceMap 3D";

        static public int MINUTIAETYPE_IRIS = 50;
        static public string SMINUTIAETYPE_IRIS = "Iris";

        static public int MINUTIAETYPE_RETINA = 60;
        static public string SMINUTIAETYPE_RETINA = "Retina";

        static public int MINUTIAETYPE_X509 = 70;
        static public string SMINUTIAETYPE_X509 = "Certificado Digital x509";

        static public string GetDescription(int minutiaetype)
        {
            switch (minutiaetype)
            {
                case -2:
                    return MinutiaeType.SMINUTIAETYPE_ANY;

                case -1:
                    return MinutiaeType.SMINUTIAETYPE_ALL;

                case 0:
                    return MinutiaeType.SMINUTIAETYPE_NONE;

                case 1:
                    return MinutiaeType.SMINUTIAETYPE_NEC;

                case 2:
                    return MinutiaeType.SMINUTIAETYPE_DIGITALPERSONA;

                case 3:
                    return MinutiaeType.SMINUTIAETYPE_DIGITALPERSONAGOLD;

                case 4:
                    return MinutiaeType.SMINUTIAETYPE_DIGITALPERSONAOTW;

                case 5:
                    return MinutiaeType.SMINUTIAETYPE_SECUGEN;

                case 6:
                    return MinutiaeType.SMINUTIAETYPE_TESTECH;

                case 7:
                    return MinutiaeType.SMINUTIAETYPE_VERIFINGER;

                case 8:
                    return MinutiaeType.SMINUTIAETYPE_SAGEM;

                case 9:
                    return MinutiaeType.SMINUTIAETYPE_IDENTIX;

                case 10:
                    return MinutiaeType.SMINUTIAETYPE_UPEK;

                case 11:
                    return MinutiaeType.SMINUTIAETYPE_COGENT;

                case 13:
                    return MinutiaeType.SMINUTIAETYPE_ANSI_INSITS_378_2004;

                case 14:
                    return MinutiaeType.SMINUTIAETYPE_ISO_IEC_19794_2_2005;

                case 12:
                    return MinutiaeType.SMINUTIAETYPE_CROSSMATCH;

                case 20:
                    return MinutiaeType.SMINUTIAETYPE_PASSWORD;

                case 21:
                    return MinutiaeType.SMINUTIAETYPE_WSQ;

                case 22:
                    return MinutiaeType.SMINUTIAETYPE_RAW;

                case 23:
                    return MinutiaeType.SMINUTIAETYPE_INCITS_381_2004;

                case 24:
                    return MinutiaeType.SMINUTIAETYPE_TOKEN;

                case 30:
                    return MinutiaeType.SMINUTIAETYPE_HANDKEY;

                case 40:
                    return MinutiaeType.SMINUTIAETYPE_FACIAL;

                case 41:
                    return MinutiaeType.SMINUTIAETYPE_JPG;

                case 42:
                    return MinutiaeType.SMINUTIAETYPE_FACIAL_F7;

                case 43:
                    return MinutiaeType.SMINUTIAETYPE_FACIAL_VERILOOK;

                case 44:
                    return MinutiaeType.SMINUTIAETYPE_FACIAL_NAMKU;

                case 50:
                    return MinutiaeType.SMINUTIAETYPE_IRIS;

                case 60:
                    return MinutiaeType.SMINUTIAETYPE_RETINA;

                case 70:
                    return MinutiaeType.SMINUTIAETYPE_X509;

                default:
                    return "Minutiae no documentada";
            }
        }
    }
}
