using System;
using System.Web.Mail;

namespace Bio.Core.Notify
{
	/// <summary>
	/// Descripci�n breve de Class1.
	/// </summary>
	public class BKNotifyByMail
	{

		public static MailFormat FORMAT_TEXT = MailFormat.Text;
		public static MailFormat FORMAT_HTML = MailFormat.Html;

		public BKNotifyByMail()
		{
			//
			// TODO: agregar aqu� la l�gica del constructor
			//
		}

		public static void SendMail(string to, string from, string subject,
						string body, string smtpserver, 
						string user, string password,
						MailFormat format)
		{
			try {
				MailMessage oMsg = new MailMessage();

				oMsg.To = to;
				oMsg.From = from;

				oMsg.Subject = subject;

				// SEND IN format FORMAT 
				//(if comment this line to send plain text).
				oMsg.BodyFormat = format;

				// HTML Body (remove HTML tags for plain text).
				oMsg.Body = body;

//				// ADD AN ATTACHMENT.
//				String sFile = @"C:\temp\Hello.txt";
//				MailAttachment oAttch = new MailAttachment(sFile, MailEncoding.Base64);
//
//				oMsg.Attachments.Add(oAttch);

				//Agrego user/password si el SMTP tiene Login obligatorio
				if (user != null && password != null) 
				{
					oMsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					oMsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", user); //set your username here
					oMsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", password);	//set your password here
				}

				// TODO: Replace with the name of your remote SMTP server.
				SmtpMail.SmtpServer = smtpserver;
				SmtpMail.Send(oMsg);

				oMsg = null;
//				oAttch = null;
			}
			catch (Exception e) {
				Console.WriteLine("Error enviando mail de notificacion : {0}", e);
			}

		}
	}
}
