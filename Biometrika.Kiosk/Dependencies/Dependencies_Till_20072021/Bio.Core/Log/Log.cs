using System;
using System.Collections;
using System.Data;
using NHibernate;
using NHibernate.Criterion;

namespace Bio.Core.Log
{
	/// <summary>
	/// Descripción breve de Log.
	/// </summary>
	public class Log
	{

#region Constructores

		public Log() { }

#endregion Constructores

#region properties private
		int id;
		DateTime date;
		string thread;
		string level;
		string logger;
		string message;
		string exception;
#endregion  properties private 

#region properties public

		public int Id
		{
			get { return id; }
			set { id = value; }
		}

		public DateTime Date
		{
			get { return date; }
			set { date = value; }
		}

		public string Thread
		{
			get { return thread; }
			set { thread = value; }
		}

		public string Level
		{
			get { return level; }
			set { level = value; }
		}

		public string Logger
		{
			get { return logger; }
			set { logger = value; }
		}

		public string Message
		{
			get { return message; }
			set { message = value; }
		}

		public string Exception
		{
			get { return exception; }
			set { exception = value; }
		}

#endregion  properties public 

#region Static Methods
		public static Log Retrieve(int id, out string msgErr) { 
			msgErr = "S/C";
			Log transaccion;
			ISession sess = HSessionFactory._sessionFactory.OpenSession();

			try {
				transaccion = (Log) sess.Load(typeof(Log),id);
			}
			catch (Exception ex) {
				msgErr = ex.Message;
				transaccion = null;
			} finally {
				if (sess != null && sess.IsOpen) sess.Close();
			}
			return transaccion;

		}

		public static IList RetrieveByFilter(string level, DateTime desde, 
			DateTime hasta, out string msgErr) { 
			msgErr = "S/C";
			ISession sess = HSessionFactory._sessionFactory.OpenSession();

			try {
				//Aplico filtros para BISTransaccion
				ICriteria crit = sess.CreateCriteria(typeof(Log));

				if (level != null && level.Trim().Length > 0) {
					crit.Add(Expression.Eq("Level",level));
				} 

				crit.Add(Expression.Between("Date",desde,hasta));
//				crit.Add(Expression.Ge("Date",desde));
//				crit.Add(Expression.Le("Date",hasta));

				return crit.List();
			} catch (Exception ex) {
				msgErr = ex.Message;
				return null;
			} finally {
				if (sess != null && sess.IsOpen) sess.Close();
			}
		}

		public static IList ListAll {
			get {
				return HSessionFactory._sessionFactory.OpenSession().
					CreateCriteria(typeof(Log)).List(); 
			}
		}

		public static DataView GetAllDataView(DictionaryEntry[] columns, 
			out string msgErr) {
			msgErr = "S/C";
			IList l = null;
			DataView dw = null;

			try {
				l = Log.ListAll;

				dw = HPersistent.GetDataView(typeof(Log),l,columns);
			} catch (Exception ex) {
				msgErr = ex.Message;
			}
			return dw;
		}

		public static DataView GetByFiltroDataView(string level, DateTime desde, 
			DateTime hasta, DictionaryEntry[] columns, out string msgErr) {
			msgErr = "S/C";
			IList l = null;
			DataView dw = null;

			try {
				l = Log.RetrieveByFilter(level, desde, hasta, out msgErr);

				dw = HPersistent.GetDataView(typeof(Log),l,columns);
			} catch (Exception ex) {
				msgErr = ex.Message;
			}
			return dw;
		}

#endregion Static Methods

	
	}
}
