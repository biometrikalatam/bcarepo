using System.Reflection;
using NHibernate;
using NHibernate.Cfg;

namespace Bio.Core.Log
{
	/// <summary>
	/// Clase base para crear session de conexion a Base de Datos. Primero
	/// configura el SessionFactoiry, mapeando lo necesario, y configurando.
	/// </summary>
	public class HSessionFactory {

		//Variable estatica para todo el proyecto. Se inicializa 
		// una sola vez en el constructor estatico
		public static ISessionFactory _sessionFactory; 

		//Metodo estatico solo para ejecutar el constructor 
		public static void Init() {}

		//Metodo estatico que setea los valores de Hibernate. Conexiones
		// y Mapeos de clases (Estos ultimos incrustados en el Assembly)
		static HSessionFactory() {
			if (_sessionFactory == null) {
				string sPathAssembly = Assembly.GetExecutingAssembly().CodeBase;
				Configuration cfg = new Configuration().Configure(sPathAssembly.Trim() + ".hibernate.cfg.xml");

				_sessionFactory = cfg.BuildSessionFactory();
			}
		}

		//Constructor no estatico
		public HSessionFactory() {
			Configure();
		}

		//Destructor
		~HSessionFactory() {
			if (_sessionFactory != null) {
				_sessionFactory.Close();	
				_sessionFactory = null;
			}
		}

		public static void Close() {
			if (_sessionFactory != null) {
				_sessionFactory.Close();	
				_sessionFactory = null;
			}			
		}

		public void Configure() {
			if (_sessionFactory == null) {
				string sPathAssembly = Assembly.GetExecutingAssembly().CodeBase;
				Configuration cfg = new Configuration().Configure(sPathAssembly.Trim() + ".hibernate.cfg.xml");

				_sessionFactory = cfg.BuildSessionFactory();
			}
		}
	}
}


