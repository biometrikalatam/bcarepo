﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Bio.Core.Utils
{
    /// <summary>
    /// 
    /// </summary>
    public enum StorageType
    {
        /// <summary>
        /// Si es local = 0 => storageConnectionString contiene path de HDD
        /// </summary>
        local = 0,
        /// <summary>
        /// Si es azure = 1 => storageConnectionString contiene string de conexión a azure blob 
        /// </summary>
        azure = 1  
    }

    /// <summary>
    /// Permite grabar en un storage local o azure blob un archivo y devuelve el path completo
    /// </summary>
    public class StorageHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(StorageHelper));

        public static int UploadToStorage(byte[] arrVideo, string company, string filename, string extension,
                                                      StorageType type, string storageConnectionString, 
                                                      string container,
                                                      out string urlfileinstorage, out string msgerr)
        {
            int ret = 0;

            LOG.Debug("Bio.Core.Utils.StorageHelper.UploadToStorageFEMInAzure IN...");
            //urlvideoinstorage = "http://storage.biometrikalatam.com/femstorage/Company7/2018/11/05/20181105100520_212844152_fem3d.avi";

            //byte[] byVideo = System.IO.File.ReadAllBytes(@"c:\tmp\_FE3d_tmp.avi.compressed");

            string folderfile = company + "/" + GetFolderDate() + filename + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "." + extension;
            LOG.Debug("Bio.Core.Utils.StorageHelper.UploadToStorageFEMInAzure folderfile => " + folderfile);
            ret = ProcessSync(type, storageConnectionString, container, arrVideo, folderfile, out urlfileinstorage, out msgerr); //.GetAwaiter().GetResult();
            LOG.Debug("Bio.Core.Utils.StorageHelper.UploadToStorageFEMInAzure Upload = " + ret + " - Url = " + urlfileinstorage);
            return ret;
        }

        private static string GetFolderDate()
        {
            string ret = "";
            try
            {
                LOG.Debug("Bio.Core.Utils.StorageHelper.GetFolderDate IN...");
                DateTime now = DateTime.Now;
                string year = now.Year.ToString();
                string month = now.Month.ToString();
                if (month.Length == 1)
                    month = "0" + month;
                string day = now.Day.ToString();
                if (day.Length == 1)
                    day = "0" + day;
                ret = year + "/" + month + "/" + day + "/";
                LOG.Debug("Bio.Core.Utils.StorageHelper.GetFolderDate FolderDate => " + ret);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Utils.StorageHelper.GetFolderDate Error " + ex.Message);
            }
            return ret;
        }


        internal static int ProcessSync(StorageType type, string storageConnectionString, string container, byte[] video, string folderfile, 
                                         out string urlvideoinstorage, out string msgerr)
        {
            int ret = 0;
            msgerr = "";
            urlvideoinstorage = "";
            CloudStorageAccount storageAccount = null;
            CloudBlobContainer cloudBlobContainer = null;
            string sourceFile = null;
            string destinationFile = null;

            LOG.Debug("Bio.Core.Utils.StorageHelper.ProcessSync IN...");

            try
            {
                if (type == StorageType.azure)
                {
                    // Retrieve the connection string for use with the application. The storage connection string is stored
                    // in an environment variable on the machine running the application called storageconnectionstring.
                    // If the environment variable is created after the application is launched in a console or with Visual
                    // Studio, the shell needs to be closed and reloaded to take the environment variable into account.
                    //string storageConnectionString = Properties.Settings.Default.StorageConnectionString; // "DefaultEndpointsProtocol=https;AccountName=femstorage;AccountKey=gMksxXzIPz8O+KuCN3aFdPd+s9iydHoMSCmQ5SMNJdNP/vn2nGhru7YUxdm74G2w3Tbdqm8f/zzCLf1QWYrXdA==;EndpointSuffix=core.windows.net"; // Environment.GetEnvironmentVariable("storageconnectionstring");
                    LOG.Debug("StorageHelper.ProcessSync storageConnectionString = " + storageConnectionString);
                    // Check whether the connection string can be parsed.
                    if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
                    {
                        try
                        {
                            LOG.Debug("Bio.Core.Utils.StorageHelper.ProcessSync storageAccount parsed = " + storageAccount.Credentials.AccountName);
                            // Create the CloudBlobClient that represents the Blob storage endpoint for the storage account.
                            CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                            LOG.Debug("Bio.Core.Utils.StorageHelper.ProcessSync - GetContainerReference(" + container + ")..."); 
                            cloudBlobContainer = cloudBlobClient.GetContainerReference(container); // "femstoragedev");
                            LOG.Debug("Bio.Core.Utils.StorageHelper.ProcessSync - SetPermissions...");
                            cloudBlobContainer.SetPermissions(new BlobContainerPermissions
                                                            { PublicAccess = BlobContainerPublicAccessType.Blob });
                            LOG.Debug("Bio.Core.Utils.StorageHelper.ProcessSync - CreateIfNotExists...");
                            bool created = cloudBlobContainer.CreateIfNotExists();
                            LOG.Debug("Bio.Core.Utils.StorageHelper.ProcessSync - CreateIfNotExists ret created = " + created.ToString());
                            LOG.Debug("Bio.Core.Utils.StorageHelper.ProcessSync Container = " + cloudBlobContainer.Name);

                            // Get a reference to the blob address, then upload the file to the blob.
                            // Use the value of localFileName for the blob name.
                            LOG.Debug("Bio.Core.Utils.StorageHelper.ProcessSync Getting reference to = " + folderfile);
                            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(folderfile);
                            LOG.Debug("Bio.Core.Utils.StorageHelper.ProcessSync Uploading file length = " + video.Length.ToString());
                            cloudBlockBlob.UploadFromByteArray(video, 0, video.Length);
                            urlvideoinstorage = cloudBlockBlob.Uri.AbsoluteUri;
                            LOG.Debug("Bio.Core.Utils.StorageHelper.ProcessSync urlvideoinstorage = " + urlvideoinstorage);
                        }
                        catch (Exception ex)
                        {
                            ret = -1;
                            msgerr = ex.Message;
                            LOG.Error("Bio.Core.Utils.StorageHelper.ProcessSync Error = " + msgerr);
                        }
                    }
                    else
                    {
                        ret = -2;
                        LOG.Error("Bio.Core.Utils.StorageHelper.ProcessSync Error parseando storageConnectionString...");
                    }
                } else //Es local => Disco
                {
                    //TODO - Save en disco local y retornar URL
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Bio.Core.Utils.StorageHelper.ProcessSync Excp: " + ex.Message);
            }
            LOG.Debug("Bio.Core.Utils.StorageHelper.ProcessAsync OUT! ret = " + ret.ToString());
            return ret;
        }
    }
    
}
