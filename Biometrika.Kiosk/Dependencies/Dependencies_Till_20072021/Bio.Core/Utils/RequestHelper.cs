﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using log4net;

namespace Bio.Core.Utils
{
    public class RequestHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(RequestHelper));

        public static int GetBrowserAndOS(HttpContext context, out string browser, out string so)
        {
            int ret = 0;
            browser = "Unknown";
            so = "Unknown";
            try
            {
                System.Web.HttpBrowserCapabilities cap = context.Request.Browser;
                if (cap != null)
                {
                    browser = cap["Browser"] + " " + cap["Version"];
                    so = cap["Platform"];
                    string sAux = cap[""];
                    if (!string.IsNullOrEmpty(sAux))
                    {
                        so = so + " " + (sAux.Contains("64")?"64":"32");
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("RequestHelper.GetBrowserAndOS Error: " + ex.Message);
            }
            return ret;
        }
    }
}
