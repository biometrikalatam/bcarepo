﻿

using System;

namespace Bio.Core.Matcher.Interface
{
    /// <summary>
    /// Interface para definir extractores de minucas de diferentes tecnologias.
    /// </summary>
    public interface IExtractor : IDisposable
    {
        /// <summary>
        /// Tecnologia a utilizar para extraccion
        /// </summary>
        int AuthenticationFactor { get; set; }

        /// <summary>
        /// Tipo d eminucia dentro de la tecnologia utilziada
        /// </summary>
        int MinutiaeType { get; set; }

        /// <summary>
        /// Umbral de extracción considerada aceptable
        /// </summary>
        double Threshold { get; set; }

        /// <summary>
        /// Se envian parametros si el Extractor lo necesita, de la forma key1=value1|key2=value2|...|keyN=valueN
        /// </summary>
        string Parameters { get; set; }

        /// <summary>
        /// Extrae desde el template ingresado en el parametro inicial, de acuerdo a 
        /// los adtos de tecnologia y minucias seteados, el template resultante. 
        /// Estos parámetros están serializados en xml, por lo que primero se deserializa.
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="xmloutput">template generado serializado en xml</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        int Extract(string xmlinput, out string xmloutput);

        /// <summary>
        /// Idem anterior pero entrega un ITemplate
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        int Extract(string xmlinput, out ITemplate templateout);

        /// <summary>
        /// Idem anterior pero ingresa un ITemplate y entrega un ITemplate
        /// </summary>
        /// <param name="templatebase">Template base para extraccion</param>
        /// <param name="destination">Determina si es template para 1-Verify | 2-Enroll </param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        int Extract(ITemplate templatebase, int destination, out ITemplate templateout);

    }
}
