﻿using System;

namespace Bio.Core.Api
{
    [Serializable]
    public class PersonalData
    {
#region Private
        private int _id; //Para recuperaciones o enroll
        private string _nick;
        private string _typeid;
        private string _valueid;
        private string _name;
        private string _patherlastname;
        private string _motherlastname;
        private string _sex;
        private string _documentseriesnumber;
        private DateTime _documentexpirationdate;
        private string _visatype;
        private DateTime _birthdate;
        private string _birthplace;
        private string _nationality;
        private string _photography;
        private string _signatureimage;
        private string _selfie;
        private string _docimagefront;
        private string _docimageback;
        private string _profession;
        private DynamicDataItem[] _dynamicdata;
        private string _enrollinfo;
        private DateTime _creation;
        private string _verificationsource;
        private int _companyidenroll;
        private int _useridenroll;
#endregion Private

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Typeid
        {
            get { return _typeid; }
            set { _typeid = value; }
        }

        public string Valueid
        {
            get { return _valueid; }
            set { _valueid = value; }
        }

        public string Nick
        {
            get { return _nick; }
            set { _nick = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Patherlastname
        {
            get { return _patherlastname; }
            set { _patherlastname = value; }
        }

        public string Motherlastname
        {
            get { return _motherlastname; }
            set { _motherlastname = value; }
        }

        public string Sex
        {
            get { return _sex; }
            set { _sex = value; }
        }

        public string Documentseriesnumber
        {
            get { return _documentseriesnumber; }
            set { _documentseriesnumber = value; }
        }

        public DateTime Documentexpirationdate
        {
            get { return _documentexpirationdate; }
            set { _documentexpirationdate = value; }
        }

        public string Visatype
        {
            get { return _visatype; }
            set { _visatype = value; }
        }

        public DateTime Birthdate
        {
            get { return _birthdate; }
            set { _birthdate = value; }
        }

        public string Birthplace
        {
            get { return _birthplace; }
            set { _birthplace = value; }
        }

        public string Nationality
        {
            get { return _nationality; }
            set { _nationality = value; }
        }

        public string Photography
        {
            get { return _photography; }
            set { _photography = value; }
        }

        public string Signatureimage
        {
            get { return _signatureimage; }
            set { _signatureimage = value; }
        }

        public string Selfie
        {
            get { return _selfie; }
            set { _selfie = value; }
        }

        public string DocImageFront
        {
            get { return _docimagefront; }
            set { _docimagefront = value; }
        }

        public string DocImageBack
        {
            get { return _docimageback; }
            set { _docimageback = value; }
        }

        public string Profession
        {
            get { return _profession; }
            set { _profession = value; }
        }

        public DynamicDataItem[] Dynamicdata
        {
            get { return _dynamicdata; }
            set { _dynamicdata = value; }
        }

        public string Enrollinfo
        {
            get { return _enrollinfo; }
            set { _enrollinfo = value; }
        }

        public DateTime Creation
        {
            get { return _creation; }
            set { _creation = value; }
        }

        public string Verificationsource
        {
            get { return _verificationsource; }
            set { _verificationsource = value; }
        }

        public int Companyidenroll
        {
            get { return _companyidenroll; }
            set { _companyidenroll = value; }
        }

        public int Useridenroll
        {
            get { return _useridenroll; }
            set { _useridenroll = value; }
        }
    }
}
