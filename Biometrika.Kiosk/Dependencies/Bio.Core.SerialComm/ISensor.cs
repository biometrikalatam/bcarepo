namespace BioCore.SerialComm
{
	/// <summary>
	/// Un Sensor actua por polling o por eventos
	/// </summary>
	public interface ISensor : IDevice
	{
		ISample GetSample(IArguments arguments);
		
	}
}
