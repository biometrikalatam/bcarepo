using System.Xml.Serialization;
using System.IO.Ports;

namespace BioCore.SerialComm.SerialPort
{
	/// <summary>
	/// Summary description for Pdf417Config.
	/// </summary>
	public class SerialPortConfig : DeviceConfig
	{
		public SerialPortConfig()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		[XmlAttribute("Port")]
		public string Port;

		[XmlAttribute("Bauds")]
		public int Bauds;

		[XmlAttribute("Bits")]
		public int Bits;

		[XmlAttribute("Parity")]
		public Parity Parity;

		[XmlAttribute("StopBits")]
		public StopBits StopBits;

	}
}
