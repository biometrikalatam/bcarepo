// ==++==
// 
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  SerialPort
**
** Purpose: SerialPort wraps an internal SerialStream class,
**		  : providing a high but complete level of Serial Port I/O functionality
**		  : over the handle/Win32 object level of the SerialStream.
**		 
**		  
** Date:  August 2002
**
===========================================================*/


using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Text;
using log4net;


namespace System.IO.Ports
{

	public class SerialPort : System.ComponentModel.Component
	{
		public const int InfiniteTimeout = -1;
		
		// ---------- default values -------------*
		
		private const int defaultDataBits = 8;
		private const Parity defaultParity = Parity.None;
		private const StopBits defaultStopBits = StopBits.One;
		private const Handshake defaultHandshake = Handshake.None;
		private const int defaultBufferSize = 1024;
		private const string defaultPortName = "COM1";
		private const int defaultBaudRate = 9600;
		private const bool defaultDtrEnable = false;
		private const bool defaultRtsEnable = false;
		private const bool defaultDiscardNull = false;
		private const byte defaultParityReplace = (byte) '?';
		private const SerialEvents defaultEventFilter = SerialEvents.All; 
		private const int defaultReceivedBytesThreshold = 1;
		private const int defaultReadTimeout = SerialPort.InfiniteTimeout;
		private const int defaultWriteTimeout = SerialPort.InfiniteTimeout;
		private const int maxDataBits = 8;
		private const int minDataBits = 5;
		// --------- members supporting exposed properties ------------*
		
		private int baudRate = defaultBaudRate;
		private int dataBits = defaultDataBits;
		private Parity parity = defaultParity;
		private StopBits stopBits = defaultStopBits;
		private string portName = defaultPortName;
		private Encoding encoding = new ASCIIEncoding(); // ASCII is default encoding for modem communication, etc.
		private Handshake handshake = defaultHandshake;
		private int readTimeout = defaultReadTimeout;
		private int writeTimeout = defaultWriteTimeout;
		private SerialEvents eventFilter = defaultEventFilter;
		private int receivedBytesThreshold = defaultReceivedBytesThreshold;
		private bool discardNull = defaultDiscardNull;
		private bool isOpen = false;
		private bool inBreak = false;
		private bool dtrEnable = defaultDtrEnable;
		private bool rtsEnable = defaultRtsEnable;
		private byte parityReplace = defaultParityReplace;
		
		// ---------- members for internal support ---------*
		private SerialStream internalSerialStream = null;
		private byte[] inBuffer = new byte[defaultBufferSize];
		private int readPos = 0;	// position of next byte to read in the read buffer.  readPos <= readLen
		private int readLen = 0;	// position of first unreadable byte => readLen - readPos is the number of readable bytes left.
		private char[] oneChar = new char[1];
		
		// ------ event members ------------------*
		//public event EventHandler Disposed;
		public event SerialEventHandler ErrorEvent;
		public event SerialEventHandler PinChangedEvent;
		public event SerialEventHandler ReceivedEvent;

        private static readonly ILog LOG = LogManager.GetLogger(typeof(SerialPort));
		//--- component properties---------------*

		// ---- SECTION: public properties --------------*
		// Note: information about port properties passes in ONE direction: from SerialPort to 
		// its underlying Stream.  No changes are able to be made in the important properties of 
		// the stream and its behavior, so no reflection back to SerialPort is necessary.

		// Gets the internal SerialStream object.  Used to pass essence of SerialPort to another Stream wrapper.
		public Stream BaseStream 
		{
			get { return internalSerialStream; }
		}
				
		[Browsable(true), 
		DefaultValue(defaultBaudRate),
		Description("The maximum baud rate at which to set the serial driver.")]
		public int BaudRate 
		{ 
			get { return baudRate;	}
			set { 
				if (isOpen)
					internalSerialStream.BaudRate = value;
				baudRate = value; 
			}
		}

	
		public bool CDHolding 
		{
			get 
			{ 
				if (!isOpen)
					throw new InvalidOperationException("CDHolding - port not open");
				return internalSerialStream.CDHolding;
			}
		}
	
		public bool CtsHolding 
		{
			get 
			{ 
				if (!isOpen)
					throw new InvalidOperationException("CtsHolding - port not open");
				return internalSerialStream.CtsHolding;
			}
		}
	

		[Browsable(true), 
		DefaultValue(defaultDataBits),
		Description("The number of data bits per transmitted/received byte.")]
		public int DataBits 
		{ 
			get 
			{ return dataBits;	}
			set 
			{
				if (isOpen) 
					internalSerialStream.DataBits = value;
				dataBits = value;
			}
		}
		
		[Browsable(true), 
		DefaultValue(defaultDiscardNull),
		Description("Whether to discard null bytes received on the port before adding to serial buffer.")]
		public bool DiscardNull
		{
			get 
			{ 
				return discardNull;
			}
			set 
			{
				if (isOpen)
					internalSerialStream.DiscardNull = value;
				discardNull = value;
			}
		}
		
		public bool DsrHolding 
		{
			get 
			{ 
				if (!isOpen)
					throw new InvalidOperationException("DsrHolding - port not open");
				return internalSerialStream.DsrHolding;
			}
		}
		
		[Browsable(true), 
		DefaultValue(defaultDtrEnable),
		Description("Whether to enable the Data Terminal Ready (DTR) line during communications.")]
		public bool DtrEnable 
		{ 
			get { return dtrEnable;	}
			set 
			{
				if (isOpen) 
					internalSerialStream.DtrEnable = value;
				dtrEnable = value;
			}
		}
		
		// Allows specification of an arbitrary encoding for the reading and writing functions of the port
		// which deal with chars and strings.  Set by default in the code to System.Text.ASCIIEncoding(), which 
		// is the standard text encoding for modem commands and most of serial communication.
		// Clearly not designable. 
		public Encoding Encoding
		{
			get { return encoding; }
			set { encoding = value; }
		}
		
		// Indicates which event-triggers we choose to acknowledge in the event-driven model provided.
		[Browsable(true), 
		DefaultValue(defaultEventFilter),
		Description("Flag enum of SerialEvents indicating the selected conditions on which to fire events.")]	
		public SerialEvents EventFilter
		{
			get { return eventFilter; }
			set { eventFilter = value; }
		}
		
		[Browsable(true), 
		DefaultValue(defaultHandshake),
		Description("The handshaking protocol for flow control in data exchange, which can be None.")]
		public Handshake Handshake 
		{
			get 
			{ 
				return handshake;
			}
			set 
			{
				if (isOpen) 
					internalSerialStream.Handshake = value;
				handshake = value;
			}	
		}
		
		public bool InBreak
		{
			get 
			{				
				return inBreak;
			}
		}
		
		
		// includes all bytes available on serial driver's input buffer as well as bytes internally buffered int the SerialPort class.
		public int InBufferBytes 
		{ 
			get 
			{
				if (!isOpen)
					throw new InvalidOperationException("InBufferBytes - port not open");
				return internalSerialStream.InBufferBytes + readLen - readPos; // count the number of bytes we have in the internal buffer too.				
			}
		}

		// true only if the Open() method successfully called on this SerialPort object, without Close() being called more recently.
		public bool IsOpen
		{
			get { return isOpen; }
		}

		// includes all bytes available on serial driver's output buffer.  Note that we do not internally buffer output bytes in SerialPort.
		public int OutBufferBytes 
		{ 
			get 
			{
				if (!isOpen)
					throw new InvalidOperationException("OutBufferBytes - port not open");
				return internalSerialStream.OutBufferBytes;
			}
		} 			
		
		
		[Browsable(true), 
		DefaultValue(defaultParity),
		Description("The scheme for parity checking each received byte and marking each transmitted byte.")]
		public Parity Parity 
		{  
			get 
			{ 
				
				return parity;
			}
			set 
			{
				if (isOpen) 
					internalSerialStream.Parity = parity;
				parity = value;
			}
		} 
		
		[Browsable(true), 
		DefaultValue(defaultParityReplace),
		Description("Byte with which to replace bytes received with parity errors.")]
		public byte ParityReplace
		{
			get { 	return parityReplace;	}
			set
			{
				if (isOpen) 
					internalSerialStream.ParityReplace = value;
				parityReplace = value;
			}
		}
	
	
		
		// Note that the communications port cannot be meaningfully re-set when the port is open,
		// and so once set by the constructor becomes read-only.
		[Browsable(true), 
		DefaultValue(defaultPortName),
		Description("The string indicating the communications resource to open, e.g. \"COM2\".")]
		public string PortName 
		{ 
			get 
			{ return portName; }
			set 
			{
				if (isOpen)
					throw new InvalidOperationException("PortName - port open");
				portName = value;
			}
		}
		
		// timeout for all read operations.  May be set to SerialPort.InfiniteTimeout, 0, or any positive value
		[Browsable(true), 
		DefaultValue(SerialPort.InfiniteTimeout),
		Description("Milliseconds after any read operation starts before timeout if no data received.")]
		public int ReadTimeout  
		{ 
			get 
			{		
			
				return readTimeout;
			} 

			set 
			{
				if (isOpen) 
					internalSerialStream.ReadTimeout = value;
				readTimeout = value;
			}
		}

		[Browsable(true), 
		DefaultValue(defaultReceivedBytesThreshold),
		Description("Number of bytes required to be available before the Read event is fired.")]
		// If we have the ReceivedChars event set, this property indicates the number of bytes necessary 
		// to exist in our buffers before the event is thrown.  This is useful if we expect to receive n-byte
		// packets and can only act when we have this many, etc.
		public int ReceivedBytesThreshold 
		{
			
			get 
			{				
				return receivedBytesThreshold;
			} 

			set 
			{ 
				if (value <= 0) 
					throw new ArgumentOutOfRangeException("receivedBytesThreshold", 
						InternalResources.GetResourceString("ArgumentOutOfRange_NeedPosNum"));
				receivedBytesThreshold = value;
			}
		}	
		
		
		[Browsable(true), 
		DefaultValue(defaultRtsEnable),	
		Description("Whether to enable the Request To Send (RTS) line during communications.")]
		public bool RtsEnable 
		{ 
			get 
			{ 
				return rtsEnable;
			}
			set 
			{
				if (isOpen) 
					internalSerialStream.RtsEnable = value;
				rtsEnable = value;
			}
		}
		
		// StopBits represented in C# as StopBits enum type and in Win32 as an integer 1, 2, or 3.
		[Browsable(true), 
		DefaultValue(defaultStopBits),
		Description("The number of stop bits per transmitted/received byte.")]
		public StopBits StopBits 
		{ 
			get 
			{ 
				return stopBits;
			}
			set 
			{
				if (isOpen) 
					internalSerialStream.StopBits = value;
				stopBits = value;
			}
		}
		
		// timeout for all write operations.  May be set to SerialPort.InfiniteTimeout or any positive value
		[Browsable(true), 
		DefaultValue(defaultWriteTimeout),
		Description("Milliseconds after any write operation starts before timeout if no data transmitted.")]
		public int WriteTimeout
		{ 
			get 
			{ 
				return writeTimeout;
			}
			set 
			{
				if (isOpen) 
					internalSerialStream.WriteTimeout = value;
				writeTimeout = value;
			}
		}
		
		
	
		// -------- SECTION: constructors -----------------*
		public SerialPort(System.ComponentModel.IContainer container)
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			container.Add(this);
		}
		
		public SerialPort()
		{
		}

		// Non-design SerialPort constructors here chain, using default values for members left unspecified by parameters 
		// Note: Calling SerialPort() does not open a port connection but merely instantiates an object.
		//     : A connection must be made using SerialPort's Open() method.
		public SerialPort(string resource) : this (resource, defaultBaudRate, defaultParity, defaultDataBits, defaultStopBits) 
		{
		}
		
		public SerialPort(string resource, int baudRate) : this (resource, baudRate, defaultParity, defaultDataBits, defaultStopBits) 
		{
		}

		public SerialPort(string resource, int baudRate, Parity parity) : this (resource, baudRate, parity, defaultDataBits, defaultStopBits) 
		{
		}

		public SerialPort(string resource, int baudRate, Parity parity, int dataBits) : this (resource, baudRate, parity, dataBits, defaultStopBits) 
		{
		}
		
		// all the magic happens in the call to the instance's .Open() method.
		// Internally, the SerialStream constructor opens the file handle, sets the device
		// control block and associated Win32 structures, and begins the event-watching cycle.
		public SerialPort(string resource, int baudRate, Parity parity, int dataBits, StopBits stopBits) 
		{
			// RAD paradigm allows us to set these to ANYTHING, so long as .Open() checks for 
			// invalid parameters.
			this.portName = resource;
			this.baudRate = baudRate;
			this.parity = parity;
			this.dataBits = dataBits;
			this.StopBits = stopBits;
            this.DtrEnable = true;
            //this.RtsEnable = true;
		}
		
		// DEFAULT DESTRUCTOR used here.
		
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				
				if (internalSerialStream != null)
					internalSerialStream.Flush();
				
			}
			if (isOpen == false) Close();
			base.Dispose( disposing );
		}

	
	
	
		// ----- SECTION: public methods		
		
		public void ClearBreak()
		{
			if (!isOpen)
				throw new InvalidOperationException("ClearBreak - port not open");
			internalSerialStream.ClearBreak();
			inBreak = false;
		}
		
		
		// Calls internal Serial Stream's Close() method on the internal Serial Stream.
		public void Close() 
		{
			if (!isOpen)
				return;//throw new InvalidOperationException("Serial Port - port already closed!");
			if (internalSerialStream != null) 
			{
				internalSerialStream.Close();
				internalSerialStream = null;
			}
			isOpen = false;
		}	

		public void DiscardInBuffer()
		{
			if (!isOpen)
				throw new InvalidOperationException("DiscardInBuffer - port not open");
			internalSerialStream.DiscardInBuffer();
		}
		
		public void DiscardOutBuffer()
		{
			if (!isOpen)
				throw new InvalidOperationException("DiscardOutBuffer - port not open");
			internalSerialStream.DiscardOutBuffer();
		}
		
		// SerialPort is open <=> SerialPort has an associated SerialStream.
		// The two statements are functionally equivalent here, so this method basically calls underlying Stream's
		// constructor from the main properties specified in SerialPort: baud, stopBits, parity, dataBits, 
		// comm resource, handshaking, and timeouts.
		public void Open() 
		{
            try
            {
                LOG.Info("Entre a Opener");
                if (isOpen)
                    throw new InvalidOperationException("Serial Port - port already open!");
                // Equivalent error checking done here and in properties individually.				
                LOG.Info("Pase is Open");
                if (parity < Parity.None || parity > Parity.Space)
                    throw new ArgumentOutOfRangeException("parity", InternalResources.GetResourceString("ArgumentOutOfRange_Enum"));
                LOG.Info("Pase Parity Node");
                if (dataBits < minDataBits || dataBits > maxDataBits)
                    throw new ArgumentOutOfRangeException("dataBits",
                        InternalResources.GetResourceString("ArgumentOutOfRange_Bounds_Lower_Upper", minDataBits, maxDataBits));
                LOG.Info("Pase Stop dataBits");
                if (stopBits < StopBits.One || stopBits > StopBits.OnePointFive)
                    throw new ArgumentOutOfRangeException("stopBits", InternalResources.GetResourceString("ArgumentOutOfRange_Enum"));
                LOG.Info("Pase stopBits");
                if (baudRate <= 0)
                    throw new ArgumentOutOfRangeException("baudRate", InternalResources.GetResourceString("ArgumentOutOfRange_NeedPosNum"));
                LOG.Info("Pase baudrates");
                if (portName == null)
                    throw new ArgumentNullException("resource", InternalResources.GetResourceString("ArgumentNull_String"));
                LOG.Info("Pase PortNmae");
                if (handshake < Handshake.None || handshake > Handshake.RequestToSendXOnXOff)
                    throw new ArgumentOutOfRangeException("handshake", InternalResources.GetResourceString("ArgumentOutOfRange_Enum"));
                LOG.Info("Pase HandShake");
                if (readTimeout < 0 && readTimeout != SerialPort.InfiniteTimeout)
                    throw new ArgumentOutOfRangeException("readTimeout", InternalResources.GetResourceString("ArgumentOutOfRange_NeedNonNegNum"));
                LOG.Info("Pase readTimeout");
                if (writeTimeout <= 0 && writeTimeout != SerialPort.InfiniteTimeout)
                    throw new ArgumentOutOfRangeException("writeTimeout", InternalResources.GetResourceString("ArgumentOutOfRange_NeedPosNum"));
                // disallow access to device resources beginning with @"\\", instead requiring "COM2:", etc.
                // Note that this still allows freedom in mapping names to ports, etc., but blocks security leaks.
                LOG.Info("Pase writeTimeOut");
                if (portName.StartsWith("\\\\"))
                    throw new ArgumentException("resource", InternalResources.GetResourceString("Arg_SecurityException"));

                LOG.Info("Voy a previo general InternalSerialStram");
                internalSerialStream = new SerialStream(portName, baudRate, parity, dataBits, stopBits, readTimeout,
                    writeTimeout, handshake, dtrEnable, rtsEnable, discardNull, parityReplace);
                LOG.Info("Pase InternalSerialStram");
                internalSerialStream.ErrorEvent += new SerialEventHandler(CatchErrorEvents);
                internalSerialStream.PinChangedEvent += new SerialEventHandler(CatchPinChangedEvents);
                internalSerialStream.ReceivedEvent += new SerialEventHandler(CatchReceivedEvents);

                isOpen = true;
            }
            catch (Exception exe)
            {
                
            }
		}

		// Read Design pattern:
		//	: Read() returns -1 on timeout, or the first available full char if found before.
		//  : Read(byte[] buffer..., int count) returns all data available before read timeout expires up to *count* bytes
		//  : Read(char[] buffer..., int count) returns all data available before read timeout expires up to *count* chars.
		//	:									Note, this does not return "half-characters".
		//  : ReadByte() is the binary analogue of the first one.
		//  : ReadLine(): returns null string on timeout, saves received data in buffer
		//  : ReadAvailable(): returns all full characters which are IMMEDIATELY available.
			
		public int Read(byte[] buffer, int offset, int count) 
		{
			if (!isOpen) 
				throw new InvalidOperationException("Serial Port Read - port not open");
			if (buffer==null)
				throw new ArgumentNullException("buffer", InternalResources.GetResourceString("ArgumentNull_Buffer"));
			if (offset < 0)
				throw new ArgumentOutOfRangeException("offset", InternalResources.GetResourceString("ArgumentOutOfRange_NeedNonNegNum"));
			if (count < 0)
				throw new ArgumentOutOfRangeException("count", InternalResources.GetResourceString("ArgumentOutOfRange_NeedNonNegNum"));
			if (buffer.Length - offset < count)
				throw new ArgumentException(InternalResources.GetResourceString("Argument_InvalidOffLen"));
			int beginReadPos = readPos;
			
			byte [] tempReturnBuffer = new byte[count];
			
			// if any bytes available in internal buffer, return those without calling any read ops.
			if (readLen - readPos >= 1)
			{
				int min = (readLen - readPos < count) ? readLen - readPos : count;
				Buffer.BlockCopy(inBuffer, readPos, buffer, 0, min);
				readPos += min;
				if (min == count) {
					if (readPos == readLen) readPos = readLen = 0;	// just a check to see if we can reset buffer
					return count;
				}
				
				// if we have read some bytes but there's none immediately available, return.
				if (InBufferBytes == 0) return min;
			}
			
			int bytesLeftToRead = count - (readPos - beginReadPos);
			
			// check that internal buffer is big enough to hold incoming bytes before requesting.	
			if (bytesLeftToRead + readLen >= inBuffer.Length) ResizeBuffer();
			
			// request to read the requested number of bytes to fulfill the contract,
			// doesn't matter if we time out.  We still return all the data we have available.
			int returnCount = internalSerialStream.Read(inBuffer, readLen, bytesLeftToRead);
	
			// Return all we have.  This will leave nothing left in the inbuffer, and will not be more than *count*.
			Buffer.BlockCopy(inBuffer, beginReadPos, buffer, offset, returnCount + (readPos - beginReadPos));
			readLen = readPos = 0; 
			return returnCount + readPos - beginReadPos; // return the number of bytes we threw into the buffer plus what we had.
		}

		// publicly exposed "ReadOneChar"-type: Read()
		// reads one full character from the stream
		public int Read() 
		{
			return ReadOneChar(readTimeout);
		}

		// gets next available full character, which may be from the buffer, the stream, or both.
		// this takes size^2 time at most, where *size* is the maximum size of any one character in an encoding.
		// The user can call Read(1) to mimic this functionality.
		
		private int ReadOneChar(int timeout) 
		{	
			int beginReadPos = readPos;
			int nextByte;
			int timeUsed = 0;
			Debug.Assert(isOpen, "ReadOneChar - port not open");
			
			// simply return if we have no time rather than throw exception, since we have 
			// to use this multiple times for one ReadLine() call, and our toes might end up
			// just over the edge.
			
			if (timeout < 0 && timeout != SerialPort.InfiniteTimeout) return -1;
			
			// case 1: we have >= 1 character in the internal buffer.
			if (encoding.GetCharCount(inBuffer, readPos, readLen - readPos) != 0) 
			{
				// get characters from buffer.
				do 
				{
					nextByte = (int) inBuffer[readPos++];
				} while (encoding.GetCharCount(inBuffer, beginReadPos, readPos - beginReadPos) < 1);
				encoding.GetChars(inBuffer, beginReadPos, readPos - beginReadPos, oneChar, 0);
				return oneChar[0];
			} 
			else 
			{
				
				// need to return immediately.
				if (timeout == 0) {
					// read all bytes in the serial driver in here.
					if (InBufferBytes + readPos >= inBuffer.Length) ResizeBuffer();
					int bytesRead = internalSerialStream.Read(inBuffer, readLen, InBufferBytes - (readLen - readPos)); // read all immediately avail.
					readLen += bytesRead;
					
					// check all we have in the buffer - if not enough, return -1
					if (ReadBufferIntoChars(oneChar, 0, 1) == 0) return -1;
					else return oneChar[0];
				}
				
				// case 2: we need to read from outside to find this.
				// timeout is either infinite or positive.
				int startTicks = SafeNativeMethods.GetTickCount();
				do 
				{
					timeUsed = SafeNativeMethods.GetTickCount() - startTicks;
					if (timeout != SerialPort.InfiniteTimeout && (timeout - timeUsed <= 0)) 
					{
						nextByte = -1;
						break;	// break for timeouts
					}
					nextByte = internalSerialStream.ReadByte((timeout == InfiniteTimeout) ? InfiniteTimeout : timeout - timeUsed);
					if (nextByte == -1) break;		// timed out
					if (readLen >= inBuffer.Length) ResizeBuffer();
					inBuffer[readLen++] = (byte) nextByte;	// we must add to the end of the buffer
					readPos++;
				} while (encoding.GetCharCount(inBuffer, beginReadPos, readPos - beginReadPos) < 1);
			}
			
			if (nextByte == -1) return -1;		// timed out, return -1
			
			encoding.GetChars(inBuffer, beginReadPos, readPos - beginReadPos, oneChar, 0);
			return oneChar[0];
		}
		

		
		// return value is either a valid byte from the buffer or stream, or -1 if a timeout occurs.
		// reads as many full characters as available
		public int Read(char[] buffer, int offset, int count)
		{
			if (buffer==null)
				throw new ArgumentNullException("buffer", InternalResources.GetResourceString("ArgumentNull_Buffer"));
			if (offset < 0)
				throw new ArgumentOutOfRangeException("offset", InternalResources.GetResourceString("ArgumentOutOfRange_NeedNonNegNum"));
			if (count < 0)
				throw new ArgumentOutOfRangeException("count", InternalResources.GetResourceString("ArgumentOutOfRange_NeedNonNegNum"));
			if (buffer.Length - offset < count)
				throw new ArgumentException(InternalResources.GetResourceString("Argument_InvalidOffLen"));
			if (!isOpen) 
				throw new InvalidOperationException("Serial Port Read - port not open");
				
			if (count == 0) return 0;	// immediately return on zero chars desired.  This simplifies things later.
			
			// read everything else into internal buffer, which we know we can do instantly, and see if we NOW have enough.
			int bytesInDriver = InBufferBytes - (readLen - readPos);
			if (readLen + bytesInDriver >= inBuffer.Length) ResizeBuffer();
			internalSerialStream.Read(inBuffer, readLen, bytesInDriver);	// should execute instantaneously.
			readLen += bytesInDriver;
			
			int charsWeAlreadyHave = encoding.GetCharCount(inBuffer, readPos, readLen - readPos); // full chars already in our buffer	
			if (charsWeAlreadyHave >= count) 
			{
				return ReadBufferIntoChars(buffer, offset, count);
			}
			
			// else: we need to do incremental reads from the stream.
			// -----
			// our internal algorithm for finding exactly n characters is a bit complicated, but must overcome the 
			// hurdle of NEVER READING TOO MANY BYTES from the Stream, since we can time out.  A variable-length encoding
			// allows anywhere between minimum and maximum bytes per char times number of chars to be the exactly correct
			// target, and we have to take care not to overuse GetCharCount().  The problem is that GetCharCount() will never tell
			// us if we've read "half" a character in our current set of collected bytes; it underestimates.    
			// size = maximum bytes per character in the encoding.  n = number of characters requested.
			// Solution I: Use ReadOneChar() to read successive characters until we get to n. 
			// Read calls: size * n; GetCharCount calls: size * n; each byte "counted": size times.
			// Solution II: Use a binary reduction and backtracking to reduce the number of calls.
			// Read calls: size * log n; GetCharCount calls: size * log n; each byte "counted": size * (log n) / n times.
			// We use the second, more complicated solution here.  Note log is actually log_(size/size - 1)...
		
			
			// we need to read some from the stream
			// read *up to* the maximum number of bytes from the stream
			// we can read more since we receive everything instantaneously, and we don't have enough,
			// so when we do receive any data, it will be necessary and sufficient.
			
			if (readTimeout == 0) return ReadBufferIntoChars(buffer, offset, count);
			 
			int startTicks = SafeNativeMethods.GetTickCount();
			int justRead;
			do {
				internalSerialStream.Read(inBuffer, readLen, Encoding.GetMaxByteCount(count - charsWeAlreadyHave));
				justRead = ReadBufferIntoChars(buffer, offset, count);
				if (justRead > 0) return justRead;
			} while (readTimeout == SerialPort.InfiniteTimeout || readTimeout - (SafeNativeMethods.GetTickCount() - startTicks) > 0);
			
			// must've timed out w/o getting a character.
			return 0;
		}

		
		// ReadBufferIntoChars reads from Serial Port's inBuffer up to *count* chars and 
		// places them in *buffer* starting at *offset*.
		// This does not call any stream Reads, and so takes "no time".
		private int ReadBufferIntoChars(char[] buffer, int offset, int count) 
		{
			if (buffer==null)
				throw new ArgumentNullException("buffer", InternalResources.GetResourceString("ArgumentNull_Buffer"));
			if (offset < 0)
				throw new ArgumentOutOfRangeException("offset", InternalResources.GetResourceString("ArgumentOutOfRange_NeedNonNegNum"));
			if (count < 0)
				throw new ArgumentOutOfRangeException("count", InternalResources.GetResourceString("ArgumentOutOfRange_NeedNonNegNum"));
			if (buffer.Length - offset < count)
				throw new ArgumentException(InternalResources.GetResourceString("Argument_InvalidOffLen"));
			if (!isOpen) 
				throw new InvalidOperationException("Serial Port Read - port not open");
			if (count == 0) return 0;
			
			// variables required for Binary-Reduce-Read algorithm developed below.
			int totalBytesRead = 0;	// total Bytes read from outside the buffer, i.e. the stream 
			int totalCharsRead = 0;	// total chars read from outside the stream, => totalCharsRead <= totalBytesRead
			int totalBytesJustRead; // total bytes read on any one internalSerialStream.Read call
			int totalCharsJustRead; // total chars COMPLETED by any one such Read call, => totalCharsJustRead <= totalBytesJustRead
			int lastFullCharPos = readPos; // first index AFTER last full char read, capped at ReadLen.
			int backtrack = 0;	// for backtracking part of the algorithm, temporary index walking back from the end of what we've read.
			
		
			if (encoding.GetMaxByteCount(1) == 1) 
			{		// kill ASCII/ANSI encoding easily.
				// read at least one and at most *count* characters
				int bytesToRead = (count < (readLen - readPos) ? count : readLen - readPos);
				 
				encoding.GetChars(inBuffer, readPos, bytesToRead, buffer, offset);
								
				readPos += bytesToRead;
				if (readPos == readLen) readPos = readLen = 0;
				return bytesToRead;
			} 
			else 
			{
				do 
				{
					backtrack = 0;
					// "read" count - totalCharsRead, one byte for each expected char.
					totalBytesJustRead = count - totalCharsRead;	// if there are count - totalChars Read chars left, we can safely read that many bytes.
					totalBytesRead += totalBytesJustRead;	// here totalBytesRead means total examined in inBuffer.
					totalCharsJustRead = encoding.GetCharCount(inBuffer, lastFullCharPos, readPos + totalBytesRead - lastFullCharPos);
					if (totalCharsJustRead > 0) 
					{
						// go backwards until we know we have a full set of totalCharsJustRead bytes with no extra lead-bytes.
						do 
						{
							backtrack += 1;
						} while (encoding.GetCharCount(inBuffer, lastFullCharPos, readPos + totalBytesRead - lastFullCharPos - backtrack) == totalCharsJustRead);
						lastFullCharPos = readPos + totalBytesRead - backtrack + 1;	// go back to starting position of last known char.
						totalCharsRead += totalCharsJustRead;
					}
				} while (totalCharsRead < count); 
				
				// fill into destination buffer all the COMPLETE characters we've read.		
				int numCharsRead = encoding.GetChars(inBuffer, readPos, lastFullCharPos - readPos, buffer, offset);
				readPos = lastFullCharPos;
				
				if (readPos == readLen) readPos = readLen = 0;
				return numCharsRead;
			}
		}
		
		public int ReadByte() 
		{
			if (!isOpen) 
				throw new InvalidOperationException("Serial Port Read - port not open");
			if (readLen != readPos) 		// stuff left in buffer, so we can read from it
				return inBuffer[readPos++];	
			
			return internalSerialStream.ReadByte(); // otherwise, ask the stream.
		}	
	
		public string ReadAvailable() 
		{
			byte [] bytesReceived = new byte[InBufferBytes];

			if (readPos < readLen)	
			{			// stuff in internal buffer
				Buffer.BlockCopy(inBuffer, readPos, bytesReceived, 0, readLen - readPos);
			}
			internalSerialStream.Read(bytesReceived, readLen - readPos, bytesReceived.Length - (readLen - readPos));	// get everything
			int numCharsReceived = Encoding.GetCharCount(bytesReceived);	
			int lastFullCharIndex = bytesReceived.Length;
			
			if (numCharsReceived == 0) 
			{
				Buffer.BlockCopy(bytesReceived, 0, inBuffer, 0, bytesReceived.Length); // put it all back!
				// don't change readPos. --> readPos == 0?
				readPos = 0;
				readLen = bytesReceived.Length;
				return "";
			}
				
			do 
			{
				lastFullCharIndex--;
			} while (Encoding.GetCharCount(bytesReceived, 0, lastFullCharIndex) == numCharsReceived);
			
		
			// we cleared the buffers above, so we can reset, but we needn't resize, since it was originally
			// big enough to fit our trailing characters AND MORE.
			readPos = readLen = 0;
			
			Buffer.BlockCopy(bytesReceived, lastFullCharIndex + 1, inBuffer, 0, bytesReceived.Length - (lastFullCharIndex + 1));
			return Encoding.GetString(bytesReceived, 0, lastFullCharIndex + 1);
		}

	
		//  Design discussion:
		//  Problem: if '\r' occurs at the end of available stream data, should we wait for the potential '\n'?	
		//	Solution: We have chosen to NOT wait for \n if \r occurs at the end of a ReadLine().
		//	Additionally, we have chosen to essentially "forget" about the potential '\n' following any terminating \r,
		//	NOT remembering through internal state that '\r' occurred as the most-recently read char, and in a ReadLine() call.
		//	Cons: 1. This means we may drop '\n' onto another read, possibly binary, if it follows in a subsequent transmission but
		//			was intended to be read by this ReadLine().
		//		2. We thus give users the feeling we're corrupting their data, which may be happening.
		//	Pros: 1. We don't hang until timeout if we get "xxx\r"
		//		2. We don't remove any '\n' text patterns or 0x0a (= 10 = '\n') from tranmissions to which a
		//			leading '\n' should be grouped.
		//		3. We do not clutter an otherwise intuitive, self-explanatory SerialPort API with a property requiring users to read documentation.
		//		4. We recongize all flavors of newline: '\r', '\n', "\r\n".
		//		5. We follow the pattern of the StreamReader class in Pro #4, and in disregarding an absent line feed char.
		 
		 
		public string ReadLine() 
		{
			if (!isOpen) 
				throw new InvalidOperationException("Serial Port Read - port not open");
            	
			string inBufferString;
			bool carriageReturnFlag = false;
			int startTicks = SafeNativeMethods.GetTickCount();
			int lastChar;	// holds return of ReadOneChar(), which may be a timeout indicator, so we need an int.
			int beginReadPos = readPos;
			
			// store encoding-based byte lengths of carraige return, line feed characters.
			char [] charTestBuf = new char[1];
			charTestBuf[0] = '\r';
			int crLength = encoding.GetByteCount(charTestBuf);
			charTestBuf[0] = '\n'; 
			int lfLength = encoding.GetByteCount(charTestBuf);					
			int timeUsed = 0;
			int timeNow;
			
			// for timeout issues, best to read everything already on the stream into our buffers.
			readLen += internalSerialStream.Read(inBuffer, readLen, InBufferBytes - (readLen - readPos));
			
			// read through the buffer one *character* at a time to find '\r', '\n', or '\r\n'
			while (true) 
			{
				timeNow = SafeNativeMethods.GetTickCount();
				lastChar = ReadOneChar((readTimeout == InfiniteTimeout) ? InfiniteTimeout : readTimeout - timeUsed);
				timeUsed += SafeNativeMethods.GetTickCount() - timeNow;
				
				if (lastChar == -1) break;	// we timed out.
				
				// note, we assume this for all encodings, true for Unicode, ASCII, UTF7, UTF8.
				
				if ((char) lastChar == '\r') 
				{	
					if (InBufferBytes == 0) 
					{
						// return string representation of all characters UP TO '\r'
						inBufferString = encoding.GetString(inBuffer, beginReadPos, readPos - beginReadPos - crLength);
						readPos = readLen = 0;	// reset read buffer, since we're at the very end
						return inBufferString;
					} 
					else if (carriageReturnFlag == true) 
					{
						inBufferString = encoding.GetString(inBuffer, beginReadPos, readPos - beginReadPos - 2 * crLength);
						// "unread" last non-linefeed character, which means we can't set it to zero beforehand in ReadOneChar().
						readPos -= crLength;	
						return inBufferString;
					} 
					else 
					{
						// wait to look at next char, to see if it's '\n'.  We're returning after next pass-through.  
						carriageReturnFlag = true; 
					} 
				} 
				else if ((char) lastChar == '\n') 
				{
					// case: we found '\r\n'.  Return everything up to, not including, those characters.
					if (carriageReturnFlag == true) 
					{
						inBufferString = encoding.GetString(inBuffer, beginReadPos, readPos - beginReadPos - crLength - lfLength);
						// readPos incremented in ReadOneChar(), so do not do it here.
						if (readPos == readLen) readPos = readLen = 0;
						return inBufferString;
					} 
					else 
					{
						// case: we found '\n'.  Return everything up to, not including, that character.
						inBufferString = encoding.GetString(inBuffer, beginReadPos, readPos - beginReadPos  - lfLength);
						// readPos incremented in ReadOneChar(), so don't do it here.
						if (readPos == readLen) readPos = readLen = 0;
						return inBufferString;
					}
				} 
				else 
				{
					// here we have "peek"ed beyond '\r' to see if '\n' next, and we just found '\rX...', char x != '\n'.
					// Put X "back in the stream", return everything up to '\r'.
					if (carriageReturnFlag == true) 
					{
						charTestBuf[0] = (char) lastChar; 
						int lastCharLength = encoding.GetByteCount(charTestBuf);
						inBufferString = encoding.GetString(inBuffer, beginReadPos, readPos - beginReadPos - crLength - lastCharLength);
						// "unread" last non-linefeed character, which means we can't set it to zero beforehand in ReadOneChar().
						readPos -= lastCharLength;	
						return inBufferString;
					}
				}
				
			}
			// we broke out due to timeout.
			
			// need to reset read position, since ReadOneChar() advances it.
			readPos = beginReadPos;
			
			// Important note: we are necessarily in a time-out mode here, because even if "ABC\nDEF" occurs at the end of the stream, we
			// can only return ABC as full line, and then wait for whatever will complete DEF, since we have no reason to expect
			// that no more data exists.  We then save DEF in our buffer, which can be read char-by-char, or however the user desires.
			
			// we haven't found a line before time ran out, but we've already tossed everything into the read buffer
			return (string) null; 
		}
		
		
		public void SetBreak()
		{
			if (!isOpen)
				throw new InvalidOperationException("SetBreak - port not open");
			internalSerialStream.SetBreak();
			inBreak = true;
		}
		
			
		// Writes string to output, no matter string's length.
		public void Write(string str) 
		{
			if (!isOpen)
				throw new InvalidOperationException("Serial Port Write - port not open!");
			if (str == null) 
				throw new ArgumentNullException("write buffer", InternalResources.GetResourceString("ArgumentNull_String"));
			if (str.Length == 0) return;	
			byte [] bytesToWrite;
		
			bytesToWrite = encoding.GetBytes(str);
		
			internalSerialStream.Write(bytesToWrite, 0, bytesToWrite.Length, writeTimeout);
		}
		
		// encoding-dependent Write-chars method.
		// Probably as performant as direct conversion from ASCII to bytes, since we have to cast anyway (we can just call GetBytes)
		public void Write(char[] buffer, int offset, int count) {
			if (!isOpen)
				throw new InvalidOperationException("Serial Port Write - port not open!");
			if (buffer == null) 
				throw new ArgumentNullException("write buffer", InternalResources.GetResourceString("ArgumentNull_String"));
			if (buffer.Length == 0) return;
				
			byte [] byteArray = Encoding.GetBytes(buffer,offset, count);
			Write(byteArray, 0, byteArray.Length);
			
		}
		
		// Writes a specified section of a byte buffer to output.
		public void Write(byte[] buffer, int offset, int count)
		{
			if (!isOpen)
				throw new InvalidOperationException("Serial Port Write - port not open!");
			if (buffer==null)
				throw new ArgumentNullException("buffer", InternalResources.GetResourceString("ArgumentNull_Buffer"));
			if (offset < 0)
				throw new ArgumentOutOfRangeException("offset", InternalResources.GetResourceString("ArgumentOutOfRange_NeedNonNegNum"));
			if (count < 0)
				throw new ArgumentOutOfRangeException("count", InternalResources.GetResourceString("ArgumentOutOfRange_NeedNonNegNum"));
			if (buffer.Length - offset < count)
				throw new ArgumentException(InternalResources.GetResourceString("Argument_InvalidOffLen"));
			if (buffer.Length == 0) return;
			
			internalSerialStream.Write(buffer, offset, count, writeTimeout);
		}


		// ----- SECTION: internal utility methods ----------------*
		
		// included here just to use the event filter to block unwanted invocations of the Serial Port's events.
		// Plus, this enforces the requirement on the received event that the number of buffered bytes >= receivedBytesThreshold
		private void CatchErrorEvents(object src, SerialEventArgs e) 
		{
            try
            {
                int eventsCaught = (int)e.EventType & (int)eventFilter; // nix any unwanted events.
                if ((eventsCaught & (int)(SerialEvents.Frame | SerialEvents.Overrun | SerialEvents.RxOver
                    | SerialEvents.RxParity | SerialEvents.TxFull)) != 0)
                {
                    ErrorEvent(src, e);
                }
            }
            catch (Exception exe)
            {
                LOG.Info("Error en CatchError:" + exe.Message);
            }
		}
		
		private void CatchPinChangedEvents(object src, SerialEventArgs e) 
		{
            try
            {
                int eventsCaught = (int)e.EventType & (int)eventFilter; // nix any unwanted events.
                if (((eventsCaught & (int)(SerialEvents.CDChanged | SerialEvents.CtsChanged | SerialEvents.DsrChanged | SerialEvents.Ring | SerialEvents.Break)) != 0))
                {
                    PinChangedEvent(src, e);
                }
            }
            catch (Exception exe)
            {
                LOG.Info("Error en CatchError:" + exe.Message);
            }

		}
		
		private void CatchReceivedEvents(object src, SerialEventArgs e)
		{
			int eventsCaught = (int) e.EventType & (int) eventFilter; // nix any unwanted events.
			int inBufferBytes = InBufferBytes;
			
			if (((eventsCaught & (int) (SerialEvents.ReceivedChars | SerialEvents.EofReceived)) != 0) 
				&& (InBufferBytes >= receivedBytesThreshold))
				ReceivedEvent(src, e);	// here, do your reading, etc.
		}

		
		// doubles size of buffer, copying data resident in old buffer to new buffer, like a C realloc() call.
		private void ResizeBuffer() 
		{
			Debug.Assert(inBuffer.Length >= readLen, "ResizeBuffer - readLen > inBuffer.Length");
			byte[] newBuffer = new byte[inBuffer.Length * 2];
			Buffer.BlockCopy(inBuffer, 0, newBuffer, 0, inBuffer.Length);
			inBuffer = newBuffer;
		}
			
		
	}
}
