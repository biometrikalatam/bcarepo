// ==++==
// 
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  System.IO.Ports.SafeNativeMethods

** Purpose: Wrapper for all SerialPort/SerialStream-related PInvoke methods
**		  : and hub for all constant definitions considered to be safe to the OS.
**
** Date:  August 2002
**
===========================================================*/


using System;
using System.Configuration.Assemblies;
using System.Runtime.Remoting;
using System.IO;
using System.Text;
using System.ComponentModel;
using System.Resources;
using System.Runtime;
using System.Security;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.Threading;

namespace System.IO.Ports
{
	internal sealed class SafeNativeMethods
	{

		[DllImport(NativeMethods.KERNEL32, CharSet=CharSet.Auto)]
		internal static extern int FormatMessage(int dwFlags, IntPtr lpSource,
			int dwMessageId, int dwLanguageId, StringBuilder lpBuffer,
			int nSize, IntPtr va_list_arguments);
		
		// Use this to translate error codes like the above into HRESULTs like
		// 0x80070006 for ERROR_INVALID_HANDLE
		internal static int MakeHRFromErrorCode(int errorCode)
		{
			
			return unchecked(((int)0x80070000) | errorCode);
		}

		[DllImport(NativeMethods.KERNEL32, SetLastError=true, CharSet=CharSet.Auto)]
		internal static extern int GetLastError();
	
		[DllImport(NativeMethods.KERNEL32, SetLastError=true, CharSet=CharSet.Auto)]
		internal static extern int GetTickCount();
		
	
	}

	
}
