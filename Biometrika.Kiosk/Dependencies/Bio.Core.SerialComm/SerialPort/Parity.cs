// ==++==
// 
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Type: Parity
**
** Purpose: Parity enum type defined here.
**
** Date:  August 2002
**
===========================================================*/

namespace System.IO.Ports
{
	public enum Parity  
	{
		None,
		Even,
		Odd,
		Mark,
		Space
	};	
}
	