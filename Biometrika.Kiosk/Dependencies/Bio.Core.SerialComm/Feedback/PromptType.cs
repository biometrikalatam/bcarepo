namespace BioCore.SerialComm.Feedback
{
	/// <summary>
	/// Tipo de prompt, usado por IFeedbackProvider.Prompt
	/// </summary>
	public enum PromptType
	{
		Info,
		Warn,
		Error,
		Question,
	}
}