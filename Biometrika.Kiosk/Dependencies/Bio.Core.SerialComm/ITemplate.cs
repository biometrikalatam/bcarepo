using System;
using System.IO;

namespace BioCore.SerialComm
{
	/// <summary>
	/// Summary description for ITemplate.
	/// </summary>
	public interface ITemplate
	{
		/// <summary>
		/// Escribe a un stream la muestra
		/// </summary>
		/// <param name="stream">stream de salida</param>
		void Serialize(StreamWriter stream);

		/// <summary>
		/// Lee desde un stream la muestra
		/// </summary>
		/// <param name="stream">stream de entrada</param>
		void Deserialize(StreamReader stream);

		Array Data { get; }
		int   DataLen { get; }
	}
}
