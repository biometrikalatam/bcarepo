using BioCore.SerialComm.Feedback;

namespace BioCore.SerialComm
{
	/// <summary>
	/// Summary description for SensorDriver.
	/// </summary>
	public abstract class SensorDriver : IDeviceDriver
	{
		public abstract IDevice Create (string name, IConfig config, IFeedbackProvider feedback);
		
	}
}
