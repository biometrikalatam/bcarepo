﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bio.Core.Matcher.Constant
{
    public class AuthenticationFactor
    {
        static public int AUTHENTICATIONFACTOR_ANY = -2;
        static public string SAUTHENTICATIONFACTOR_ANY = "Cualquiera";
        static public int AUTHENTICATIONFACTOR_All = -1;
        static public string SAUTHENTICATIONFACTOR_All = "Todos";
        static public int AUTHENTICATIONFACTOR_NONE = 0;
        static public string SAUTHENTICATIONFACTOR_NONE = "Ninguno";

        static public int AUTHENTICATIONFACTOR_PASSWORD = 1;
        static public string SAUTHENTICATIONFACTOR_PASSWORD = "Clave";

        static public int AUTHENTICATIONFACTOR_FINGERPRINT = 2;
        static public string SAUTHENTICATIONFACTOR_FINGERPRINT = "Huella Digital";

        static public int AUTHENTICATIONFACTOR_HANDGEOMETRY = 3;
        static public string SAUTHENTICATIONFACTOR_HANDGEOMETRY = "Geometria de la Mano";


        static public int AUTHENTICATIONFACTOR_FACIAL = 4;
        static public string SAUTHENTICATIONFACTOR_FACIAL = "Facial";
        static public int AUTHENTICATIONFACTOR_VOICE = 5;
        static public string SAUTHENTICATIONFACTOR_VOICE = "Voz";
        static public int AUTHENTICATIONFACTOR_IRIS = 6;
        static public string SAUTHENTICATIONFACTOR_IRIS = "Iris";
        static public int AUTHENTICATIONFACTOR_RETINA = 7;
        static public string SAUTHENTICATIONFACTOR_RETINA = "Retina";

        static public int AUTHENTICATIONFACTOR_THERMALFACEIMAGE = 8;
        static public string SAUTHENTICATIONFACTOR_THERMALFACEIMAGE = "Imagen termal facial";
        static public int AUTHENTICATIONFACTOR_THERMALHANDIMAGE = 9;
        static public string SAUTHENTICATIONFACTOR_THERMALHANDIMAGE = "Imagen termal de mano";

        static public int AUTHENTICATIONFACTOR_HANDVEINSMAP = 10;
        static public string SAUTHENTICATIONFACTOR_HANDVEINSMAP = "Mapa de venas de manos";

        static public int AUTHENTICATIONFACTOR_PKI = 11;
        static public string SAUTHENTICATIONFACTOR_PKI = "PKI";

        static public string GetName(int af)
        {
            switch (af)
            {
                case -2:
                    return SAUTHENTICATIONFACTOR_ANY;
                case -1:
                    return SAUTHENTICATIONFACTOR_All;
                case 0:
                    return SAUTHENTICATIONFACTOR_NONE;
                case 1:
                    return SAUTHENTICATIONFACTOR_PASSWORD;
                case 2:
                    return SAUTHENTICATIONFACTOR_FINGERPRINT;
                case 3:
                    return SAUTHENTICATIONFACTOR_HANDGEOMETRY;
                case 4:
                    return SAUTHENTICATIONFACTOR_FACIAL;
                case 5:
                    return SAUTHENTICATIONFACTOR_VOICE;
                case 6:
                    return SAUTHENTICATIONFACTOR_IRIS;
                case 7:
                    return SAUTHENTICATIONFACTOR_RETINA;
                case 8:
                    return SAUTHENTICATIONFACTOR_THERMALFACEIMAGE;
                case 9:
                    return SAUTHENTICATIONFACTOR_THERMALHANDIMAGE;
                case 10:
                    return SAUTHENTICATIONFACTOR_HANDVEINSMAP;
                case 11:
                    return SAUTHENTICATIONFACTOR_PKI;
                default:
                    return "AuthenticationFactor no documentado";
            }
        }
    }


}
