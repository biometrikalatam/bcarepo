using System;

// Import log4net classes.
using System.Collections;
using System.Globalization;
using log4net;
using log4net.Config;
using System.Diagnostics;
using System.Data;
using System.Xml;
using System.IO;

namespace Bio.Core.Log {
	/// <summary>
	/// La clase BOKLogger sirve para englobar las funcionaolidades
	/// de Log4net. Ademas, agrega funcionalidades de visualizacion 
	/// del LOG
	/// </summary>
	public class BioLog {
        public BioLog() { }

		//ID del Logger, de Log4net 
        public static readonly ILog log = LogManager.GetLogger("BioLog");

		#region Properties

		#region General
		//Archivo de configuracion Log4net = "log4net.xml"
		private string sPathConf;
		public string PathConf {
			get { return sPathConf; }
			set {sPathConf = value;
				this.ParseXMLConfig();
			}
		}

		public void SetConf() {
			XmlConfigurator.Configure(new System.IO.FileInfo(this.PathConf));
		}

		public void SetConf(string p_sPathConf) {
			XmlConfigurator.Configure(new System.IO.FileInfo(p_sPathConf));
		}
		#endregion General
	
		#region Mask Fechas
		private string sMaskFechaShort;
		public string MaskFechaShort {
			get { return sMaskFechaShort;}
			set { sMaskFechaShort = value;}
		}

		private string sMaskFechaLong;
		public string MaskFechaLong {
			get { return sMaskFechaLong; }
			set { sMaskFechaLong = value;}
		}

		private string sDelimiterHora;
		public string DelimiterHora {
			get { return sDelimiterHora; }
			set { sDelimiterHora = value; }
		}

		#endregion Mask Fechas

		//Variables de configuracion para recuperacion de log
		#region DataBase
		//Data Base
		private bool bDBEnabled = false;
		public bool DBEnabled {
			get { return bDBEnabled; }
			set { bDBEnabled = value; }
		}
		#endregion DataBase

		#region ASCII
		//MSSQL
		private bool bASCIIFileEnabled = false;
		public bool ASCIIFileEnabled {
			get { return bASCIIFileEnabled; }
			set { bASCIIFileEnabled = value; }
		}
		private string sPathASCII = null;
		public string PathASCII {
			get { return sPathASCII; }
			set { sPathASCII = value; }
		}
		#endregion ASCII


		#endregion Properties

        #region Sobreescibo rutinas log4net standard [Deprecated]

        //Sobreescribo rutinas de Log4net - [Deprecated]

		//DEBUG
		public  void Debug(string p_sNameMethodORmsg,object messageOEException) {
			if (log.IsDebugEnabled) {
				if (messageOEException.GetType().FullName.IndexOf("Exception",0) > 0) {
					log.Debug(p_sNameMethodORmsg, (System.Exception)messageOEException);
				} else {
					log.Debug("["+p_sNameMethodORmsg+"] - " + messageOEException);
				}
			}
		}
		public  void Debug(object message) {
			if (log.IsDebugEnabled) {
				log.Debug(message);
			}
		}
		public  void Debug(string p_sNameMethod,object message, System.Exception t) {
			if (log.IsDebugEnabled) {
				log.Debug("["+p_sNameMethod+"] - " + message, t);
			}
		}

		//INFO
		public  void Info(string p_sNameMethodORmsg,object messageOEException) {
			if (log.IsInfoEnabled) {
				if (messageOEException.GetType().FullName.IndexOf("Exception",0) > 0) {
					log.Info(p_sNameMethodORmsg, (System.Exception)messageOEException);
				} else {
					log.Info("["+p_sNameMethodORmsg+"] - " + messageOEException);
				}
			}
		}
		public  void Info(object message){
			if (log.IsInfoEnabled) {
				log.Info(message);
			}
		}
		public  void Info(string p_sNameMethod,object message,System.Exception t) {
			if (log.IsInfoEnabled) {
				log.Info("["+p_sNameMethod+"] - " + message,t);
			}
		}

		//WARN	
		public  void Warn(object p_sNameMethodORmsg,object messageOEException) {
			if (log.IsWarnEnabled) {
				if (messageOEException.GetType().FullName.IndexOf("Exception",0) > 0) {
					log.Warn(p_sNameMethodORmsg, (System.Exception) messageOEException);
				} else {
					log.Warn("["+p_sNameMethodORmsg+"] - " + messageOEException);
				}
			}
		}
		public  void Warn(object message) {
			if (log.IsWarnEnabled) {
				log.Warn(message);
			}
		}
		public  void Warn(string p_sNameMethod,object message, System.Exception t) {
			if (log.IsWarnEnabled) {
				log.Warn("["+p_sNameMethod+"] - " + message, t);
			}
		}


		//ERROR	
		public  void Error(object p_sNameMethodORmsg,object messageOEException) {
			if (log.IsErrorEnabled) {
				if (messageOEException.GetType().FullName.IndexOf("Exception",0) > 0) {
					log.Error(p_sNameMethodORmsg, (System.Exception) messageOEException);
				} else {
					log.Error("["+p_sNameMethodORmsg+"] - " + messageOEException);
				}
			}
		}
		public  void Error(object message) {
			if (log.IsErrorEnabled) {
				log.Error(message);
			}
		}
		public  void Error(string p_sNameMethod,object message, System.Exception t) {
			if (log.IsErrorEnabled) {
				log.Error("["+p_sNameMethod+"] - " + message, t);
			}
		}

		//FATAL	
		public  void Fatal(object p_sNameMethodORmsg,object messageOEException) {
			if (log.IsFatalEnabled) {
				if (messageOEException.GetType().FullName.IndexOf("Exception",0) > 0) {
					log.Fatal(p_sNameMethodORmsg, (System.Exception) messageOEException);
				} else {
					log.Fatal("["+p_sNameMethodORmsg+"] - " + messageOEException);
				}
			}
		}
		public  void Fatal(object message) {
			if (log.IsFatalEnabled) {
				log.Fatal(message);
			}
		}
		public  void Fatal(string p_sNameMethod,object message, System.Exception t) {
			if (log.IsFatalEnabled) {
				log.Fatal("["+p_sNameMethod+"] - " + message, t);
			}
        }


        #endregion Sobreescibo rutinas log4net standard [Deprecated]

        #region Rutinas de recovery de Log

#region Recovery Data Base
        public DataView LeeLogConFiltro(string level, string desde, string hasta)
		{
			DictionaryEntry[] sColumns = {new DictionaryEntry("Id","ID"),
								 new DictionaryEntry("Date","Fecha"),
								 new DictionaryEntry("Thread","Thread"),
								 new DictionaryEntry("Level","Nivel"),
								 new DictionaryEntry("Logger","Origen"),
								 new DictionaryEntry("Message","Mensaje"),
								 new DictionaryEntry("Exception","Excepcion")};

			DateTime dtD = new DateTime();
			DateTime dtH = new DateTime();
			
			CultureInfo ci = new CultureInfo("es-CL");
			ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
			ci.DateTimeFormat.ShortTimePattern = "HH:mm:ss";

			//Si no son nulas las dos, parseo por cada una
			if (desde != null && desde.Trim().Length > 0) {
				try {
					dtD = DateTime.Parse(desde, ci);
				} catch (Exception de) {
					dtD = DateTime.Parse("01/01/1800", ci);
				}
			} else
			{
				dtD = DateTime.Parse("01/01/1800", ci);
			}
			//Si no son nulas las dos, parseo por cada una
			if (hasta != null && hasta.Trim().Length > 0) {
				try {
					dtH = DateTime.Parse(hasta, ci);
				} catch (Exception de) {
					dtH = DateTime.Parse("31/12/9999", ci);
				}
			} else
			{
				dtH = DateTime.Parse("31/12/9999", ci);
			}

			string msgErr = "S/C";
			return Log.GetByFiltroDataView(level,dtD,dtH, sColumns, out msgErr);
		}

#endregion Recovery Data Base

#region Recovery ASCIIFile
		public DataSet LeeAllLogASCIIFile(DataSet p_oDS) {
			if (this.ASCIIFileEnabled) {
				string sLinea;
			
				try {
					//Preparo para leer el file
					//Copio a temporal para evitar error de que esta tomado por otra
					//aplicacion. El log4net lo toma exclusivo.
					string sSemilla = System.String.Format("{0:yyyyMMddHHmmss}",DateTime.Now);
					if (File.Exists(this.PathASCII + "." + sSemilla)) {
						File.Delete(this.PathASCII + "." +  sSemilla);
					}
					File.Copy(this.PathASCII,this.PathASCII + "." +  sSemilla);
					System.IO.FileStream fs = new FileStream(this.PathASCII + "." +  sSemilla,
						System.IO.FileMode.Open,
						System.IO.FileAccess.Read,
						System.IO.FileShare.Read);
					System.IO.StreamReader file = 
						new System.IO.StreamReader(fs);

					//Creo a mano el DataSet
					System.Data.DataTable Log = p_oDS.Tables.Add("Log");
					Log.Columns.Add("Date", Type.GetType("System.String"));
					Log.Columns.Add("Level", Type.GetType("System.String"));
					Log.Columns.Add("Mensaje", Type.GetType("System.String"));

					//Recorro el file y lleno el DataSet a mano	
					while((sLinea = file.ReadLine()) != null) {
						string[] aLinea = sLinea.Split(new Char [] {'|'});
						if ((aLinea.Length > 3)) { 
							System.Data.DataRow oRow;
							oRow = Log.NewRow();
							oRow["Date"] = aLinea[0];
							oRow["Level"] = aLinea[2];       
							oRow["Mensaje"] = aLinea[5];
							//							Log.Rows.Add(oRow);
							Log.Rows.InsertAt(oRow,0);
						}
					}
					file.Close();
					//Borro temporal
					if (File.Exists(this.PathASCII + "." + sSemilla)) {
						File.Delete(this.PathASCII + "." +  sSemilla);
					}
				} catch (Exception e) {
					Console.WriteLine(e.ToString());
					p_oDS = null; 
				}
				return p_oDS;
			} else {
				return null;
			}
		}

		public DataSet LeeAllLogASCIIFile(DataSet p_oDS, int p_iCant) {
			if (this.ASCIIFileEnabled) {
				string sLinea;
				int iContador = 1;

				try {
					//Preparo para leer el file
					//Copio a temporal para evitar error de que esta tomado por otra
					//aplicacion. El log4net lo toma exclusivo.
					string sSemilla = System.String.Format("{0:yyyyMMddHHmmss}",DateTime.Now);
					if (File.Exists(this.PathASCII + "." + sSemilla)) {
						File.Delete(this.PathASCII + "." +  sSemilla);
					}
					File.Copy(this.PathASCII,this.PathASCII + "." +  sSemilla);
					System.IO.FileStream fs = new FileStream(this.PathASCII + "." +  sSemilla,
						System.IO.FileMode.Open,
						System.IO.FileAccess.Read,
						System.IO.FileShare.Read);
					System.IO.StreamReader file = 
						new System.IO.StreamReader(fs);

					//Creo a mano el DataSet
					System.Data.DataTable Log = p_oDS.Tables.Add("Log");
					Log.Columns.Add("Date", Type.GetType("System.String"));
					Log.Columns.Add("Level", Type.GetType("System.String"));
					Log.Columns.Add("Mensaje", Type.GetType("System.String"));
					
					//Recorro el file y lleno el DataSet a mano	
					while ((sLinea = file.ReadLine()) != null && iContador < p_iCant) {
						iContador++;
						string[] aLinea = sLinea.Split(new Char [] {'|'});
						if ((aLinea.Length > 3)) { 
							System.Data.DataRow oRow;
							oRow = Log.NewRow();
							oRow["Date"] = aLinea[0];
							oRow["Level"] = aLinea[2];       
							oRow["Mensaje"] = aLinea[5];
							//Log.Rows.Add(oRow);
							Log.Rows.InsertAt(oRow,0);
						}
					}
					file.Close();
					//Borro temporal
					if (File.Exists(this.PathASCII + "." + sSemilla)) {
						File.Delete(this.PathASCII + "." +  sSemilla);
					}
				} catch (Exception e) {
					Console.WriteLine(e.ToString());
					p_oDS = null; 
				}
				return p_oDS;
			} else {
				return null;
			}
		}

		public DataSet LeeConFiltroLogASCII(DataSet p_oDS, string p_level, string p_fDesde, string p_fHasta) {
			if (this.ASCIIFileEnabled) {
				string sLinea;

			
				try {
					//Preparo para leer el file
					//Copio a temporal para evitar error de que esta tomado por otra
					//aplicacion. El log4net lo toma exclusivo.
					string sSemilla = System.String.Format("{0:yyyyMMddHHmmss}",DateTime.Now);
					if (File.Exists(this.PathASCII + "." + sSemilla)) {
						File.Delete(this.PathASCII + "." +  sSemilla);
					}
					File.Copy(this.PathASCII,this.PathASCII + "." +  sSemilla);
					System.IO.FileStream fs = new FileStream(this.PathASCII + "." +  sSemilla,
						System.IO.FileMode.Open,
						System.IO.FileAccess.Read,
						System.IO.FileShare.Read);
					System.IO.StreamReader file = 
						new System.IO.StreamReader(fs);

					//Creo a mano el DataSet
					System.Data.DataTable Log = p_oDS.Tables.Add("Log");
					Log.Columns.Add("Date", Type.GetType("System.String"));
					Log.Columns.Add("Level", Type.GetType("System.String"));
					Log.Columns.Add("Mensaje", Type.GetType("System.String"));

					//Recorro el file y lleno el DataSet a mano	
					while((sLinea = file.ReadLine()) != null) {
						string[] aLinea = sLinea.Split(new Char [] {'|'});
						if ((aLinea.Length > 3)) { 
							if (CumpleFiltro(aLinea,p_level,p_fDesde,p_fHasta)) {
								System.Data.DataRow oRow;
								oRow = Log.NewRow();
								oRow["Date"] = aLinea[0];
								oRow["Level"] = aLinea[2];       
								oRow["Mensaje"] = aLinea[5];
								//								Log.Rows.Add(oRow);
								Log.Rows.InsertAt(oRow,0);
							
							}
						}
					}
					file.Close();
					//Borro temporal
					if (File.Exists(this.PathASCII + "." + sSemilla)) {
						File.Delete(this.PathASCII + "." +  sSemilla);
					}
				} catch (Exception e) {
					Console.WriteLine(e.ToString());
					p_oDS = null;
				}
				return p_oDS;
			} else {
				return null;
			}
		}

#endregion Recovery ASCIIFile

	
		#endregion Rutinas de recovery de Log

		#region Helpers para BioLog
		//Get info del Thread
		public static string GetNameThread(StackTrace p_st) {
			string s = "";
			//				StackTrace p_st1 = new StackTrace(true);
			//				p_st1.GetFrame(p_st1.FrameCount 
			s = p_st.GetFrame(0).GetFileName() + ":";
			s = s + p_st.GetFrame(0).GetMethod() + ":";
			s = s + p_st.GetFrame(0).GetFileLineNumber().ToString();
			return s;
		}

		//Parseo XML de configuracion para llenar variables 
		public bool ParseXMLConfig() {
			XmlDocument dom = new XmlDocument();
			dom.PreserveWhitespace = true;
			XmlTextReader r = new XmlTextReader(this.PathConf);
			try {
				dom.Load(r);
			} catch (Exception e) { Console.WriteLine(e.ToString()); }
			r.Close();

			XmlNodeList cAppendersRef = dom.GetElementsByTagName("appender-ref");
			for (int i = 0; i < cAppendersRef.Count; i++) {
				if (cAppendersRef.Item(i).Attributes.Item(0).Value.ToString().IndexOf("ADONetAppender") > -1) {
					this.DBEnabled = true;
				}

				if (cAppendersRef.Item(i).Attributes.Item(0).Value.ToString().Equals("ASCIIFileAppender")) {
					this.ASCIIFileEnabled = true;
					ExtraeInfoASCIIFile(dom);
				}
			}
			return true;
		}

//		private void ExtraeInfoMSSQL(XmlDocument p_DOM) {
//			XmlNode nMSSQL = null;
//
//			XmlNodeList nList = p_DOM.GetElementsByTagName("appender");
//			for (int i = 0; i < nList.Count; i++) {
//				if (nList.Item(i).Attributes.Item(0).Value.ToString().Equals("ADONetAppenderMSSQL")) {
//					nMSSQL = nList[i]; 	
//					break;
//				}
//			}
//			//				XmlDocument xdom = (XmlDocument)nMSSQL;
//			XmlNodeList cParams = nMSSQL.ChildNodes; //.GetElementsByTagName("param");
//			for (int j = 0; j < cParams.Count;j++) {
//				if (cParams.Item(j).NodeType.Equals(XmlNodeType.Element)) {
//					if (cParams.Item(j).Attributes.Item(0).Value.ToString().Equals("ConnectionString")) {
//						this.MSSQLConx = cParams.Item(j).Attributes.Item(1).Value.ToString();
//					}
//					if (cParams.Item(j).Attributes.Item(0).Value.ToString().Equals("CommandTextGet")) {
//						this.MSSQLCmdGet = cParams.Item(j).Attributes.Item(1).Value.ToString();
//					}
//				}
//			}			
//		}
//		private void ExtraeInfoACCESS(XmlDocument p_DOM) {
//		}


//		private void ExtraeInfoORACLE(XmlDocument p_DOM) {
//			XmlNode nOracleSQL = null;
//
//			XmlNodeList nList = p_DOM.GetElementsByTagName("appender");
//			for (int i = 0; i < nList.Count; i++) {
//				if (nList.Item(i).Attributes.Item(0).Value.ToString().Equals("ADONetAppenderORACLE")) {
//					nOracleSQL = nList[i]; 	
//					break;
//				}
//			}
//			//				XmlDocument xdom = (XmlDocument)nMSSQL;
//			XmlNodeList cParams = nOracleSQL.ChildNodes; //.GetElementsByTagName("param");
//			for (int j = 0; j < cParams.Count;j++) {
//				if (cParams.Item(j).NodeType.Equals(XmlNodeType.Element)) {
//					if (cParams.Item(j).LocalName.Equals("connectionString")) {
//						this.ORASQLConx = cParams.Item(j).Attributes.Item(0).Value.ToString();
//					}
//					//					if (cParams.Item(j).Attributes.Item(0).Value.ToString().Equals("ConnectionString")) {
//					//						this.ORASQLConx = cParams.Item(j).Attributes.Item(1).Value.ToString();
//					//					}
//
//					if (cParams.Item(j).LocalName.Equals("commandText")) {
//						this.ORASQLCmdGet = cParams.Item(j).Attributes.Item(0).Value.ToString();
//					}
//					//					if (cParams.Item(j).Attributes.Item(0).Value.ToString().Equals("CommandTextGet")) {
//					//						this.ORASQLCmdGet = cParams.Item(j).Attributes.Item(1).Value.ToString();
//					//					}
//				}
//			}			
//		}

			

		private void ExtraeInfoASCIIFile(XmlDocument p_DOM) {
			XmlNode nMSSQL = null;

			XmlNodeList nList = p_DOM.GetElementsByTagName("appender");
			for (int i = 0; i < nList.Count; i++) {
				if (nList.Item(i).Attributes.Item(0).Value.ToString().Equals("ASCIIFileAppender")) {
					nMSSQL = nList[i]; 	
					break;
				}
			}
			//				XmlDocument xdom = (XmlDocument)nMSSQL;
			XmlNodeList cParams = nMSSQL.ChildNodes; //.GetElementsByTagName("param");
			for (int j = 0; j < cParams.Count;j++) {
				if (cParams.Item(j).NodeType.Equals(XmlNodeType.Element)) {
					if (cParams.Item(j).Attributes.Item(0).Value.ToString().Equals("File")) {
						this.PathASCII = cParams.Item(j).Attributes.Item(1).Value.ToString();
						break;
					}
				}
			}			
		}

		private bool CumpleFiltro(string[] aLinea, string p_level,
			string p_fDesde, string p_fHasta) {
			bool bRes = true;

			if (p_fDesde == null || p_fHasta == null) {
				return false;
			} else {
				string[] sData = aLinea[0].Split(new Char [] {','});
				System.DateTime dt = System.Convert.ToDateTime(sData[0]);
				System.DateTime dtd = System.Convert.ToDateTime(p_fDesde + " 00:00:00");
				System.DateTime dth = System.Convert.ToDateTime(p_fHasta + " 23:59:59");
				//Console.WriteLine(dtd.ToString() + " <= " + dt.ToString() + " <= " + dth.ToString());
				bRes = bRes && (dtd <= dt) && (dt <= dth);
			}
			if (p_level != null && bRes) {
				if (!p_level.Equals(aLinea[2].Trim())) {
					bRes = false;
				}
			}
			return bRes;
		}
		#endregion Helpers para BioLog

	}
}
