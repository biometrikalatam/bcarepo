using System;
using System.Collections;

namespace Bio.Core.Common
{
	/// <summary>
	/// Descripción breve de ByteArray.
	/// </summary>
	[Serializable]
	public class ByteArray 
	{
		public ByteArray(byte[] data)
		{
			this.data = data;
		}

		public Array Bytes
		{
			get { return data; }
		}

		public int Length
		{
			get { return data == null ? 0 : data.Length; }
		}

		private byte[] data;
	}
}
