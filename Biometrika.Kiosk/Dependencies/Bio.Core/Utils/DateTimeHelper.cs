﻿using System;
using log4net;

namespace Bio.Core.Utils
{
    ///<summary>
    /// Clase destinada a generar metodos de ayuda con tipo de datos DateTime
    ///</summary>
    public class DateTimeHelper
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(DateTimeHelper));


        ///<summary>
        /// Minima fecha considerada por Bio.Core
        ///</summary>
        public static DateTime DTMinimal = new DateTime(1900,1,1,0,0,0,1);

        /// <summary>
        /// Chequea que la variable ingresada no sea menor que el mínimo permitido
        /// en la base de datos, para evitar problemas en la grabación.
        /// </summary>
        /// <param name="param">Variable del tipo DateTime</param>
        /// <returns></returns>
        public static DateTime CheckDateTimeMinimal(DateTime? param)
        {
            DateTime dtRet = new DateTime();
            try {
                if (!param.HasValue) return DTMinimal;
                if (param < DTMinimal) return DTMinimal;
                dtRet = param.Value;
            } catch(Exception ex)
            {
                LOG.Error("DateTimeHelper.CheckDateTimeMinimal", ex);
            }
            return dtRet;
        }

        /// <summary>
        /// Parsea una fecha con diferentes formatos. Sino devuelve MinValue
        /// Formatos de fecha procesados:
        ///     yyyy/MM/dd
        ///     yyyy/M/dd
        ///     dd/MM/yyyy
        ///     dd/M/yyyy
        ///     M/dd/yyyy
        ///     MM/dd/yyyy
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static DateTime Parse(string strDate)
        {
            DateTime dRet = new DateTime(1900,1,1);
            bool mustReturn = false;
            try
            {
                LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse IN - Fecha = " + (string.IsNullOrEmpty(strDate)?"Null":strDate));
                if (string.IsNullOrEmpty(strDate)) return dRet;

                //Reemplazo - por /
                strDate = strDate.Replace("-", "/");
                LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse IN - Fecha = " + strDate);
                try
                {
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try yyyy/MM/dd...");
                    dRet = DateTime.ParseExact(strDate, "yyyy/MM/dd", null);
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try yyyy/MM/dd OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse Ex: " + ex1.Message);
                }

                if (mustReturn) return dRet;

                try
                {
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try yyyy/M/dd...");
                    dRet = DateTime.ParseExact(strDate, "yyyy/M/dd", null);
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try yyyy/M/dd OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse Ex: " + ex1.Message);
                }

                if (mustReturn) return dRet;
                //------------------
                try
                {
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try dd/MM/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try dd/MM/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse Ex: " + ex1.Message);
                }

                if (mustReturn) return dRet;

                try
                {
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try dd/M/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "dd/M/yyyy", null);
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try dd/M/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse Ex: " + ex1.Message);
                }

                if (mustReturn) return dRet;

                try
                {
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try M/dd/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "M/dd/yyyy", null);
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try M/dd/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse Ex: " + ex1.Message);
                }

                if (mustReturn) return dRet;

                try
                {
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try MM/dd/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "MM/dd/yyyy", null);
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try MM/dd/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse Ex: " + ex1.Message);
                }

                if (mustReturn)
                    return dRet;

                try
                {
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try M/d/yyyy...");
                    dRet = DateTime.ParseExact(strDate, "M/d/yyyy", null);
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse try M/d/yyyy OK!");
                    mustReturn = true;
                }
                catch (Exception ex1)
                {
                    mustReturn = false;
                    LOG.Debug("Bio.Core.Utils.DateTimeHelper.Parse Ex: " + ex1.Message);
                }

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Utils.DateTimeHelper.Parse Error: " + ex.Message);
            }
            return dRet;
        }

        public static int CalcularEdad(DateTime fechaNacimiento)
        {
            // Obtiene la fecha actual:
            DateTime fechaActual = DateTime.Today;

            // Comprueba que la se haya introducido una fecha válida; si 
            // la fecha de nacimiento es mayor a la fecha actual se muestra mensaje 
            // de advertencia:
            if (fechaNacimiento > fechaActual)
            {
                Console.WriteLine("La fecha de nacimiento es mayor que la actual.");
                return -1;
            }
            else
            {
                int edad = fechaActual.Year - fechaNacimiento.Year;

                // Comprueba que el mes de la fecha de nacimiento es mayor 
                // que el mes de la fecha actual:
                if (fechaNacimiento.Month >= fechaActual.Month)
                {
                    if (fechaNacimiento.Day > fechaActual.Day)
                    {
                        --edad;
                    }
                }

                return edad;
            }
        }
    }
}
