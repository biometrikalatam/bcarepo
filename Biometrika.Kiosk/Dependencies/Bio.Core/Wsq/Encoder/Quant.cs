using System;
using Bio.Core.Wsq;
namespace Bio.Core.Wsq.Encoder
{
	/// <summary>
	/// Descripción breve de QuantVals.
	/// </summary>
	internal class Quant
	{
		public const int NUM_SUBBANDS = 60;
		private const float VARIANCE_THRESH =  1.01f;
		private const int STRT_SUBBAND_2 = 19;
		private const int STRT_SUBBAND_3 = 52;
		private const int STRT_SUBBAND_DEL = 60;

		public Quant(float r)
		{
			this.r = r;
		}

		

		public short[] Quantize(out int sptr, QuantumTreeNode[] qtrees, WaveletTreeNode[] wtrees, float[] fip, int width, int height)
		{
			float[] var = new float[NUM_SUBBANDS];
		
			CalcVariance(qtrees, fip, width, var);
			Qbss = new float[NUM_SUBBANDS];
			Qzbs = new float[NUM_SUBBANDS];
			sptr = 0;
			if (var[0] >= VARIANCE_THRESH) Qbss[0] = 1.0f;
			if (var[1] >= VARIANCE_THRESH) Qbss[1] = 1.0f;
			if (var[2] >= VARIANCE_THRESH) Qbss[2] = 1.0f;
			if (var[3] >= VARIANCE_THRESH) Qbss[3] = 1.0f;

			for (int cnt = 4; cnt < NUM_SUBBANDS; cnt++)
			{
				if (var[cnt] >= VARIANCE_THRESH)
					Qbss[cnt] =  (10.0f/(A[cnt]*(float)Math.Log(var[cnt])));
				
			}


			int k0Len = 0; /* number of subbands in K0 */
			int[] k0 = new int[NUM_SUBBANDS]; /* initial list of subbands w/variance >= thresh */
			int[] k1 = new int[NUM_SUBBANDS]; /* working list of subbands */

			// Desde este punto var[i] se convierte en sigma[i], es decir la desviacion estandar
			// esto lo hacemos para ahorrar memoria
			for (int cnt = 0; cnt < NUM_SUBBANDS; cnt++)
			{
				if (var[cnt] >= VARIANCE_THRESH)
				{
					k0[k0Len] = cnt;
					k1[k0Len++] = cnt;
					var[cnt] = (float) Math.Sqrt(var[cnt]);
				}
			}

			int npLen; /* number of subbands flagged in NP */
			float s, p, q;
			int kLen, nKlen; /* number of subbands in other subband lists */
			int[] k = k1;
			kLen = k0Len;

			bool[] np = new bool[NUM_SUBBANDS]; /* current subbounds with nonpositive bit rates. */

			int[] nK;
			while (true)
			{
				s = 0.0f;
				/* Compute new 'S' */
				for (int i = 0; i < kLen; i++)
					s += M[k[i]];

				/* Compute product 'P' */
				p = 1.0f;
				for (int i = 0; i < kLen; i++)
				{
					int ki = k[i];
					p *= (float) (Math.Pow((var[ki]/Qbss[ki]), M[ki]));
				}

				q =  (((float)Math.Pow(2.0f, ((r/s) - 1.0f))/2.5f)/(float)Math.Pow(p, 1.0f/s));


				/* Flag subbands with non-positive bitrate. */
				npLen = 0;
				for (int i = 0; i < kLen; i++)
				{
					int ki = k[i];
					if ((Qbss[ki]/q) >= (5.0f*var[ki]))
					{
						np[ki] = true;
						npLen++;
					}
				}

				/* If list of subbands with non-positive bitrate is empty ... */
				if (npLen == 0)
					break;

				/* Assign new subband set to previous set K minus subbands in set NP. */
				nK = k1;
				nKlen = 0;
				for (int i = 0; i < kLen; i++)
				{
					int ki = k[i];
					if (!np[ki])
						nK[nKlen++] = ki;
				}

				/* Assign new set as K. */
				k = nK;
				kLen = nKlen;

			}

			/* Flag subbands that are in set 'K0' (the very first set). */
			nK = new int[k1.Length];

			for (int i = 0; i < k0Len; i++)
				nK[k0[i]] = 1;


			/* Set 'Q' values. */
			for (int cnt = 0; cnt < NUM_SUBBANDS; cnt++)
			{
				if (nK[cnt] == 1)
					Qbss[cnt] /= q;
				else
					Qbss[cnt] = 0.0f;
				Qzbs[cnt] = 1.2f*Qbss[cnt];
			}

			/* Now ready to compute and store bin widths for subbands. */
			short[] sip = new short[width*height];
			
			
			for (int cnt = 0; cnt < NUM_SUBBANDS; cnt++)
			{
				QuantumTreeNode qtree = qtrees[cnt];
				int qty = qtree.Y;
				int qtx = qtree.X;
				int fp = (qty*width) + qtx;

				if (Qbss[cnt] != 0.0)
				{
					float zbin = Qzbs[cnt]/2.0f;
					float nzbin = -zbin;
					int qtlenx = qtree.LenX;
					int delta = width - qtlenx;
					int qtleny = qtree.LenY;
					for (int row = 0; row < qtleny; row++, fp += delta)
					{
						for (int col = 0; col < qtlenx; col++)
						{
							if (nzbin <= fip[fp] && fip[fp] <= zbin)
								sip[sptr] = 0;
							else if (fip[fp] > zbin)
								sip[sptr] = (short) (((fip[fp] - zbin)/Qbss[cnt]) + 1.0);
							else
								sip[sptr] = (short) (((fip[fp] + zbin)/Qbss[cnt]) - 1.0);
							sptr++;
							fp++;
						}
					}
				}
			}

			if (sptr == 0)
				return null;
			QuantBlockSizes(wtrees, qtrees);
	
			return sip;
		}

		private void QuantBlockSizes(WaveletTreeNode[] wtrees, QuantumTreeNode[]   qtrees)
		{
			/* Compute temporary sizes of 3 WSQ subband blocks. */			
			qsize1 = wtrees[14].LenX*wtrees[14].LenY;
			qsize2 = (wtrees[5].LenY*wtrees[1].LenX) + (wtrees[4].LenX*wtrees[4].LenY);
			qsize3 = (wtrees[2].LenX*wtrees[2].LenY) + (wtrees[3].LenX*wtrees[3].LenY);

			/* Adjust size of quantized WSQ subband blocks. */
			
			for (int node = 0; node < STRT_SUBBAND_2; node++)
			{
				QuantumTreeNode qtrees1 = qtrees[node];
				if (Qbss[node] == 0.0)
					qsize1 -= (qtrees1.LenX*qtrees1.LenY);
			}
			for (int node = STRT_SUBBAND_2; node < STRT_SUBBAND_3; node++)
			{
				QuantumTreeNode qtrees1 = qtrees[node];
				if (Qbss[node] == 0.0f)
					qsize2 -= (qtrees1.LenX*qtrees1.LenY);
			}
			for (int node = STRT_SUBBAND_3; node < STRT_SUBBAND_DEL; node++)
			{
				QuantumTreeNode qtrees1 = qtrees[node];
				if (Qbss[node] == 0.0)
					qsize3 -= (qtrees1.LenX*qtrees1.LenY);
			}
		}

		private void CalcVariance(QuantumTreeNode[] qTreeNodes, float[] fdata, int width, float[] var)
		{
			for (int cvr = 0, treeidx = 0; cvr < NUM_SUBBANDS; cvr++, treeidx++)
			{
				QuantumTreeNode qt = qTreeNodes[treeidx];
				int fp = (qt.Y*width) + qt.X;
				float ssq =  0.0f;
				float sum_pix = 0.0f;


				int lenX = qt.LenX;
				int skipx = lenX>>3;
				int lenY = qt.LenY;
				int skipy = (9*lenY)>>5;

				int lenx = (3*lenX)>>2;
				int leny = (7*lenY)>>4;

				fp += (skipy*width) + skipx;
				int delta = width - lenx;
				for (int row = 0; row < leny; row++, fp += delta)
				{
					for (int col = 0; col < lenx; col++)
					{
						float value = fdata[fp];
						sum_pix += value;
						ssq += value * value;
						fp++;
					}
				}
				int lenxy = (lenx*leny);
				float sum2 = (sum_pix*sum_pix)/lenxy;
				var[cvr] = (ssq - sum2)/(lenxy - 1.0f);
			}
		}


		private float r; /* compression bitrate */
		internal float[] Qbss;
		internal float[] Qzbs;
		private int qsize1;
		private int qsize2;
		private int qsize3;
		
		public int Qsize1
		{
			get { return qsize1; }
		}

		public int Qsize2
		{
			get { return qsize2; }
		}

		public int Qsize3
		{
			get { return qsize3; }
		}




		/* subband "weights" for quantization */
		private static float[] A = {
									   1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f,
									   1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f,
									   1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f,
									   1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f,
									   1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f, 1.00f,
									   1.00f, 1.00f, 1.32f, 1.08f, 1.42f, 1.08f, 1.32f, 1.42f, 1.08f, 1.08f

								   };

		private static float[] M = { 
									   1.0f/1024.0f, 
									   1.0f/1024.0f,
									   1.0f/1024.0f,
									   1.0f/1024.0f,
									   1.0f/256.0f, 
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f, 
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f, 
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f, 
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f, 
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f, 
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f, 
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/256.0f,
									   1.0f/16.0f,
									   1.0f/16.0f,
									   1.0f/16.0f,
									   1.0f/16.0f,
									   1.0f/16.0f,
									   1.0f/16.0f,
									   1.0f/16.0f,
									   1.0f/16.0f,
									   1.0f/16.0f,
									   1.0f/16.0f
								   };

		
	}
}