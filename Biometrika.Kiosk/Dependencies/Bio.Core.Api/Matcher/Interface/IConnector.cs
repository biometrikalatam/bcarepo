﻿using System;

namespace Bio.Core.Api.Matcher.Interface
{
    public interface IConnector : IDisposable
    {

        /// <summary>
        /// Id identificador del conector
        /// </summary>
        string ConnectorId { get; set; }

        /// <summary>
        /// Pares de key/value de configuracion para el conector
        /// </summary>
        DynamicData Config { get; set; }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        int Verify(string xmlinput, out string xmloutput);

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        int Identify(string xmlinput, out string xmloutput);

        /// <summary>
        /// Acción de Recuperación. Ingresa información recuperar, 
        /// y se realiza la operacion.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        int Get(string xmlinput, out string xmloutput);

    }
}
