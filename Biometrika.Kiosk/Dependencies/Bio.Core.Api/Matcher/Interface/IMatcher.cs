﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bio.Core.Matcher.Interface
{
    ///<summary>
    /// Interface que deberán implementar los Matcher de cada marca
    ///</summary>
    public interface IMatcher : IDisposable
    {

        List<Template> listTemplatePeople { get; set; }

        /// <summary>
        /// Tecnologia a utilizar para matching
        /// </summary>
        int AuthenticationFactor { get; set; }

        /// <summary>
        /// Tipo de minucia dentro de la tecnologia utilziada
        /// </summary>
        int MinutiaeType { get; set; }

        /// <summary>
        /// Umbral de verificación considerada aceptable
        /// </summary>
        double Threshold { get; set; }

        /// <summary>
        /// Dos posibles valores:
        ///     0 - FIRST   -> El primer matching positivo
        ///     1 - BEST    -> El mejor matching positivo
        /// </summary>
        int MatchingType { get; set; }

        /// <summary>
        /// Se envian parametros si el Matcher lo necesita, de la forma key1=value1|key2=value2|...|keyN=valueN
        /// </summary>
        string Parameters { get; set; }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// y nuevo, parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        int Verify(string xmlinput, out string xmloutput);

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// y nuevo, parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        int Verify(ITemplate templateCurrent1, ITemplate templateCurrent2, out float score);

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// y lista contra que verificar, parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        int Identify(string xmlinput, out string xmloutput);

        /// <summary>
        /// Creado para WA Kiosk para no tener que deserializar tod o el tiempo la lista de todas
        /// las personas par aidentificar
        /// </summary>
        /// <param name="templateCurrent">Template actual capturado</param>
        /// <param name="listTemplatePeople">Lista de templates contra que chequear</param>
        /// <param name="score">Score obtenido</param>
        /// <param name="idPersona">id persona identificada (Que esta en AdditionalData)</param>
        /// <returns></returns>
        int Identify(ITemplate templateCurrent, List<ITemplate> listTemplatePeople, out float score, out string idBir);

        /// <summary>
        /// Creado para WA Kiosk para no tener que deserializar todo el tiempo la lista de todas
        /// las personas para identificar, y usar el enroll una sola vez
        /// </summary>
        /// <param name="templateCurrent"></param>
        /// <param name="score"></param>
        /// <param name="idBir"></param>
        /// <returns></returns>
        int Identify(ITemplate templateCurrent, out float score, out int idBir);

        /// <summary>
        /// Relaiza inicializaciones en el matcher para optimizar la operacion
        /// </summary>
        /// <param name="xmlparam"></param>
        /// <returns></returns>
        int Inicialize(string xmlparam);

        /// <summary>
        /// Setea la lista de templates contra las que se realiza la identificacion, para hacer mas eficiente
        /// la operación, y optimizar el proceso en los casos que se puede como en Verifinger.
        /// </summary>
        /// <param name="listTemplatePeople"></param>
        /// <returns></returns>
        int Enroll(List<ITemplate> listTemplatePeople);
    }
}
