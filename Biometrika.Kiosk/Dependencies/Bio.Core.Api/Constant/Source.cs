﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bio.Core.Api.Constant
{
    public class Source
    {
        static public int SOURCE_ANY = -2;
        static public string SSOURCE_ANY = "Cualquiera";
        static public int SOURCE_All = -1;
        static public string SSOURCE_All = "Todos";
        static public int SOURCE_DEFAULT = 0;
        static public string SSOURCE_DEFAULT = "Default";

        static public int SOURCE_LOCAL = 1;
        static public string SSOURCE_LOCAL = "Local";
        static public int SOURCE_REMOTE = 2;
        static public string SSOURCE_REMOTE = "Remoto";

        static public string GetName(int src)
        {
            switch (src)
            {
                case -2:
                    return SSOURCE_ANY;
                case -1:
                    return SSOURCE_All;
                case 0:
                    return SSOURCE_DEFAULT;
                case 1:
                    return SSOURCE_LOCAL;
                case 2:
                    return SSOURCE_REMOTE;
                default:
                    return "Source no documentado";
            }
        }
    }
}
