﻿namespace Biometrika.HFSecury.API
{
    public class Person
    {
        public string id { get; set; }
        public string name { get; set; }
        public string idcardNum { get; set; }
    }
}