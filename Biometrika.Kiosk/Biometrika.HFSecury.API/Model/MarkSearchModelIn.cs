﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Biometrika.HFSecury.API.Helpers;

namespace Biometrika.HFSecury.API.Model
{
    public class MarkSearchModelIn
    {
        public string personId { get; set; }
        public int length { get; set; }
        public int index { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public int model { get; set; }
        public int order { get; set; }

        public string ToString()
        {
            return "personId=" + personId + "|length=" +
                length.ToString() + "|index=" +
                index.ToString() + "|startTime=" +
                startTime + "|endTime=" +
                endTime + "|model=" +
                model.ToString() + "|order=" +
                order.ToString();
        }
    }

    public class MarkSearchModelOut
    {
        public int id { get; set; }
        public int mask { get; set; }
        public string idcard { get; set; }
        public string path { get; set; }
        public string personId { get; set; }
        public string temperature { get; set; }
        public string temperatureState { get; set; }
        public string time { get; set; }
        public int type { get; set; }

        public string ToString()
        {
            string strH = "";
            try
            {
                //strH = HelperTime.UnixTimestampToDateTime(Convert.ToDouble(time) / 1000).ToString("yyyy-MM-dd HH:mm:ss");
                strH = HelperTime.UnixTimeStampToDateTime2(Convert.ToInt64(time) / 1000).ToString("yyyy-MM-dd HH:mm:ss");
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }

            return "id=" + id.ToString() + "|mask=" +
                mask.ToString() + "|personId=" +
                personId + "|idcard=" +
                (string.IsNullOrEmpty(idcard)?"":idcard) + "|path=" +
                path + "|temperature=" +
                temperature + "|temperatureState=" +
                temperatureState + "|time=" +
                time.ToString() + " [" + strH + "]" +  
                "|type=" + type.ToString();
        }

    }

    public class MarkDeleteModelIn
    {
        public string personId { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public int model { get; set; }

        public string ToString()
        {
            return "personId=" + personId + "|startTime=" +
                startTime + "|endTime=" +
                endTime + "|model=" +
                model.ToString();
        }
    }
        //public class Application
        //{
        //    public string code { get; set; }
        //    public IList<Data> data { get; set; }
        //    public string msg { get; set; }
        //    public int result { get; set; }
        //    public bool success { get; set; }

        //}
    }
