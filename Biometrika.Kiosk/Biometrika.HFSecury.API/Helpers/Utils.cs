﻿using System;
using System.ComponentModel;

namespace Biometrika.HFSecury.API.Helpers
{
    public class Utils
    {
        public static string ToString(object obj)
        {
            string ret = "";
            string name;
            object value;
            try
            {
                bool isFirst = true;
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
                {
                    name = descriptor.Name;
                    value = descriptor.GetValue(obj);

                    if (isFirst)
                    {
                        isFirst = false;
                        ret = name + "=" + value.ToString();
                    } else
                    {
                        ret = ret + "|" + name + "=" + (value!=null?value.ToString():"null");
                    }
                }
            }
            catch (Exception ex)
            {
                ret = "";
                //LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }
    }
}
