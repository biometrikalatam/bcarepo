﻿using Biometrika.HFSecury.API.Model;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.HFSecury.API.Helpers
{
    public class HelperAPIRest
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(HelperAPIRest));

        public static bool DebugExtendido { get; set; } = true; //Graba salidas desde response, pueden ser grandes
                                                                //por eso se pude deshabilitar
        public static int Timeout { get; set; } = 30000;

        public static Hashtable DevicesList;
        
        /// <summary>
        /// Llamada al API Rest del device generica. 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="method"></param>
        /// <param name="result">PArsea resultado. Luego en data segun llamado se procesa en cada 
        /// llamante</param>
        /// <returns>codigp de error => 0-OK y negativo error</returns>
        public static int HRequest(string url, Method method, out Result result)
        {
            int ret = 0;
            result = null;
            try
            {
                LOG.Debug("HRequest IN...");

                LOG.Debug("HRequest URL => " + url + " - Method= " + method.ToString() +
                            " - Timeout= " + Timeout.ToString());
                var client = new RestClient(url);
                client.Timeout = Timeout;
                var request = new RestRequest(method);
                LOG.Debug("HRequest Calling...");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (DebugExtendido) LOG.Debug("HRequest response.Content = " + response.Content);
                    else LOG.Debug("HRequest response.Content!=null => " + (!string.IsNullOrEmpty(response.Content)));
                    result = JsonConvert.DeserializeObject<Result>(response.Content);
                } else
                {
                    LOG.Debug("HRequest response.Content = NULL");
                    ret = -2;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("HRequest Excp Error: " + ex.Message);
            }
            LOG.Debug("HRequest OUT! => ret = " + ret.ToString());
            return ret;
        }

        internal static int HRequestForm(string url, Dictionary<string,object> parametros, Method method, 
                                         out Result result)
        {
            int ret = 0;
            result = null;
            try
            {
                LOG.Debug("HRequestForm IN...");

                LOG.Debug("HRequestForm URL => " + url + " - Method= " + method.ToString() +
                            " - Timeout= " + Timeout.ToString());
                var client = new RestClient(url);
                client.Timeout = Timeout;
                var request = new RestRequest(method);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                foreach (var item in parametros)
                {
                    request.AddParameter(item.Key, item.Value);
                }
                
                LOG.Debug("HRequestForm Calling...");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (DebugExtendido) LOG.Debug("HRequestForm response.Content = " + response.Content);
                    else LOG.Debug("HRequestForm response.Content!=null => " + (!string.IsNullOrEmpty(response.Content)));
                    result = JsonConvert.DeserializeObject<Result>(response.Content);
                }
                else
                {
                    LOG.Debug("HRequestForm response.Content = NULL");
                    ret = -2;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("HRequestForm Excp Error: " + ex.Message);
            }
            LOG.Debug("HRequestForm OUT! => ret = " + ret.ToString());
            return ret;
        }
    }
}
