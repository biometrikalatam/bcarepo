﻿using Biometrika.HFSecury.API.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace Biometrika.HFSecury.Callback.Test.Controllers
{
    public class DeviceController : ApiController
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(DeviceController));

        [Route("CallbackIdentify")]
        [HttpPost]
        public object callbackidentify([FromBody] CallbackModel param)
        {
            string msgerr;
            try
            {
                LOG.Info("DeviceController.callbackidentify IN...");

                if (param != null)
                {
                    LOG.Info("DeviceController.callbackidentify - Recibido Type 1 = " + param.GetType().FullName.ToString());
                    try
                    {
                        LOG.Info("DeviceController.callbackidentify - Recibido PersonId => id:" + 
                            param.personId + " - name:" + param.personName + " - time:" + param.time 
                            + " - temperature:" + param.temperature + " - CodigoBarras:" + 
                            (string.IsNullOrEmpty(param.data)?"NULL": param.data));
                    }
                    catch (Exception ex)
                    {

                        LOG.Error(" Error: " + ex.Message);
                    }

                } else
                {
                    LOG.Info("DeviceController.callbackidentify PARAM = Null");
                }
          
                System.Web.HttpContext.Current.Request.SaveAs("c:/tmp/request_IdentifyPost_" +
                                    DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt", true);


            }
            catch (Exception ex)
            {

                LOG.Error("DeviceController.callbackidentify 1 Excp Error: " + ex.Message);
            }
            string ret = "{\"result\":1,\"success\":\"ture\"}";
            LOG.Info("DeviceController.callbackidentify Response => " + ret);
            LOG.Info("DeviceController.callbackidentify OUT!");
            //return ret; // Request.CreateResponse(HttpStatusCode.OK, "{\"result\":1,\"success\":ture}");
            return Request.CreateResponse(HttpStatusCode.OK, "{\"result\":1,\"success\":\"ture\"}");
        }

        [Route("CallbackDeviceHeartBeat")]
        [HttpPost]
        public object DeviceHeartBeat([FromBody] CallbackHeartBeat param)
        {
            string msgerr;
            try
            {
                LOG.Info("DeviceController.CallbackDeviceHeartBeat IN...");

                if (param != null)
                {
                    try
                    {
                        LOG.Info("DeviceController.CallbackDeviceHeartBeat - Recibido => deviceKey:" +
                            param.deviceKey + " - time:" + param.time + " - ip:" + param.ip + 
                            " - personCount:" + param.personCount +
                            " - faceCount:" + param.faceCount + " - Version:" + param.version);
                    }
                    catch (Exception ex)
                    {

                        LOG.Error(" Error: " + ex.Message);
                    }

                }
                else
                {
                    LOG.Info("DeviceController.CallbackQREvent PARAM = Null");
                }

                System.Web.HttpContext.Current.Request.SaveAs("c:/tmp/request_HeartBeat_" + 
                                        DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt", true);


            }
            catch (Exception ex)
            {

                LOG.Error("DeviceController.callbackidentify 1 Excp Error: " + ex.Message);
            }
            LOG.Info("DeviceController.callbackidentify OUT!");
            string ret = "{\"result\":1,\"success\":ture}";
            LOG.Info("DeviceController.callbackidentify Response => " + ret); 
            return ret; // Request.CreateResponse(HttpStatusCode.OK, "{\"result\":1,\"success\":ture}");
        }


        [Route("CallbackQREvent")]
        [HttpPost]
        public object CallbackQR([FromBody] CallbackModel param)
        {
            string msgerr;
            try
            {
                LOG.Info("DeviceController.CallbackQR IN...");

                if (param != null)
                {
                    LOG.Info("DeviceController.CallbackQR - Recibido Type 1 = " + param.GetType().FullName.ToString());
                    try
                    {
                        //LOG.Info("DeviceController.callbackidentify - Recibido PersonId => id:" +
                        //    param.personId + " - name:" + param.personName + " - time:" + param.time
                        //    + " - temperature:" + param.temperature + " - CodigoBarras:" +
                        //    (string.IsNullOrEmpty(param.data) ? "NULL" : param.data));
                    }
                    catch (Exception ex)
                    {

                        LOG.Error(" Error: " + ex.Message);
                    }

                }
                else
                {
                    LOG.Info("DeviceController.CallbackQR PARAM = Null");
                }

                System.Web.HttpContext.Current.Request.SaveAs("c:/tmp/request_QREvent_" +
                                    DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt", true);


            }
            catch (Exception ex)
            {

                LOG.Error("DeviceController.CallbackQR 1 Excp Error: " + ex.Message);
            }
            LOG.Info("DeviceController.CallbackQR OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, "{\"result\":1,\"success\":ture}");
        }

        // GET: api/Device
        [Route("api/v1/Ping")]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Device/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Device
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Device/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Device/5
        public void Delete(int id)
        {
        }

        
    }

    
}
