﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Biometrika.HFSecury.Callback.Test
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(WebApiApplication));

        protected void Application_Start()
        {
            string root = HttpContext.Current.Server.MapPath(".");
            //string urlbaseservice = HttpContext.Current.Request.Url.AbsoluteUri;

             //0.- Inicializo LOG
            XmlConfigurator.Configure(
                new FileInfo(root + "\\bin\\Logger.cfg"));
            LOG.Info("Biometrika.HFSecury.Callback.Test Starting...");

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
