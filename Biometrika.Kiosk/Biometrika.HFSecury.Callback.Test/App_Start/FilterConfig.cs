﻿using System.Web;
using System.Web.Mvc;

namespace Biometrika.HFSecury.Callback.Test
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
