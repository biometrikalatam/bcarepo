﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using System;

namespace Biometrika.Mark.GrupoEsmeralda
{
    /// <summary>
    /// Implementacion de IMark para AUtoclub. POr ahora es Dummy porque el registro se hace en el mismo momento 
    /// del chequeo den BusinessRule.
    /// </summary>
    public class MarkGrupoEsmeralda : IMark
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(MarkGrupoEsmeralda));

        bool _Initialized;
        string _Name;
        DynamicData _Config;

        public DynamicData Config
        {
            get {
                return _Config;
            }

            set {
                _Config = value;
            }
        }

        public bool Initialized
        {
            get {
                return this._Initialized;
            }

            set {
                this._Initialized = value;
            }
        }

        public string Name
        {
            get {
                return this._Name;
            }

            set {
                this._Name = value;
            }
        }

        public int DoMark(DynamicData parameters, out DynamicData returns, out string msg)
        {
            int ret = 0;
            returns = null;
            msg = null;
            try
            {
                //object o = (parameters != null) ? parameters.ParamsList["key"] : null;
                if (parameters == null)
                    LOG.Fatal("MarkGrupoEsmeralda.DoMark - Parametros NULO!");
                else
                {
                    foreach (DynamicDataItem item in parameters.ListDynamicDataItems)
                    {
                        LOG.Debug("MarkGrupoEsmeralda.DoMark - key=" + item.key + " => value = " + 
                                    (item.value!=null?item.value.ToString():"NULL"));
                    }
                }
            }
            catch (Exception ex) 
            {
                ret = -1;
                msg = "MarkGrupoEsmeralda Excp [" + ex.Message + "]";
                LOG.Error("MarkGrupoEsmeralda.DoMark - Excp Error: " + ex.Message);
            }
            return ret;
        }

        public int Intialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("MarkGrupoEsmeralda.Intialize IN...");
                if (!System.IO.File.Exists("Biometrika.Mark.GrupoEsmeralda.cfg"))
                {
                    Config = new DynamicData();
                    Config.AddItem("URLService", "http://localhost/BS");
                    Config.AddItem("URLTimeout", 30000);
                    //_Parameters.Add("", "");
                    if (!SerializeHelper.SerializeToFile(Config, "Biometrika.Mark.GrupoEsmeralda.cfg"))
                    {
                        LOG.Warn("MarkGrupoEsmeralda.Initialize - No grabo condif en disco (Biometrika.Mark.GrupoEsmeralda.cfg)");
                    }
                }
                else
                {
                    Config = SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.Mark.GrupoEsmeralda.cfg");
                    Config.Initialize();
                }

                if (Config == null)
                {
                    LOG.Fatal("MarkGrupoEsmeralda.Initialize - Error leyendo BusinessRuleGrupoEsmeralda!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                _Initialized = true;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("MarkGrupoEsmeralda.Initialize - Excp Error: " + ex.Message);
            }
            LOG.Debug("MarkGrupoEsmeralda.Intialize OUT!");
            return ret;
        }
    }
}
