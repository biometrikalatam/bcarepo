﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using System;

namespace Biometrika.Mark.Fersanatura
{
    /// <summary>
    /// Implementacion de IMark para AUtoclub. POr ahora es Dummy porque el registro se hace en el mismo momento 
    /// del chequeo den BusinessRule.
    /// </summary>
    public class MarkSportlifeNorte : IMark
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(MarkSportlifeNorte));

        bool _Initialized;
        string _Name;
        DynamicData _Config;

        public DynamicData Config
        {
            get
            {
                return _Config;
            }

            set
            {
                _Config = value;
            }
        }

        public bool Initialized
        {
            get
            {
                return this._Initialized;
            }

            set
            {
                this._Initialized = value;
            }
        }

        public string Name
        {
            get
            {
                return this._Name;
            }

            set
            {
                this._Name = value;
            }
        }

        public int DoMark(DynamicData parameters, out DynamicData returns, out string msg)
        {
            int ret = 0;
            returns = null;
            msg = null;
            try
            {
                LOG.Debug("MarkSportlifeNorte.DoMark IN...");
                //object o = (parameters != null) ? parameters.ParamsList["key"] : null;
                if (parameters == null)
                    LOG.Fatal("MarkSportlifeNorte.DoMark - Parametros NULO!");
                else
                {
                    LOG.Debug("MarkSportlifeNorte.DoMark IN! - parameters.length = " + 
                                parameters.DynamicDataItems.Count.ToString());
                    //foreach (DynamicDataItem item in parameters.ListDynamicDataItems)
                    //{
                    //    LOG.Debug("MarkSportlifeNorte.DoMark - key=" + item.key + " => value = " + (item.value.ToString()));
                    //}
                    if (Config != null) 
                    {
                        if (Config.DynamicDataItems.ContainsKey("TypeMark") &&
                            Config.DynamicDataItems["TypeMark"].Equals("2"))  //Si es API
                        {
                            LOG.Debug("MarkSportlifeNorte.DoMark - Ignora porque es API [ya marco]");
                            //TODO - Mark via API
                        } else
                        {
                            LOG.Fatal("MarkSportlifeNorte.DoMark - Graba en BD Local");
                            ret = DatabaseHelper.SaveMark(parameters);
                            
                        }
                    } else
                    {
                        LOG.Warn("MarkSportlifeNorte.DoMark - Config == null => No graba marca para idRecognized = " +
                                    (string)Config.DynamicDataItems["idRecognized"]);
                    }


                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "MarkSportlifeNorte.Excp [" + ex.Message + "]";
                LOG.Error("MarkSportlifeNorte.DoMark - Excp Error: " + ex.Message);
            }
            return ret;
        }

        public int Intialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("MarkSportlifeNorte.Intialize IN...");
                if (!System.IO.File.Exists("Biometrika.Mark.SportlifeNorte.cfg"))
                {
                    Config = new DynamicData();
                    Config.AddItem("TypeMark", 1); //1 - Local BD | 2 - API Remota
                    Config.AddItem("URLService", "http://www.dominio.cl/v1/mark");
                    Config.AddItem("URLTimeout", 30000);
                    Config.AddItem("ConectionString", "data source=ServerName;initial catalog=BiometrikaKiosk;user id=sa;password=password;");
                    //_Parameters.Add("", "");
                    if (!SerializeHelper.SerializeToFile(Config, "Biometrika.Mark.SportlifeNorte.cfg"))
                    {
                        LOG.Warn("MarkSportlifeNorte.Initialize - No grabo condif en disco (Biometrika.Mark.SportlifeNorte.cfg)");
                    }
                }
                else
                {
                    Config = SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.Mark.SportlifeNorte.cfg");
                    Config.Initialize();
                }

                if (Config == null)
                {
                    LOG.Fatal("MarkSportlifeNorte.Initialize - Biometrika.Mark.SportlifeNorte.cfg!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                _Initialized = true;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("MarkSportlifeNorte.Initialize - Excp Error: " + ex.Message);
            }
            return ret;
        }
    }
}
