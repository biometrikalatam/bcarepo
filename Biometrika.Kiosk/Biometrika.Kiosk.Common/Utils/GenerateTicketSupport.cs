﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Common.Utils
{
    public class GenerateTicketSupport
    {
        //ALL, DEBUG, INFO, WARN, ERROR, FATAL, OFF
        public int SUPPORT_LEVEL_OFF = -1; 
        public int SUPPORT_LEVEL_ALL = 0;
        public int SUPPORT_LEVEL_DEBUG = 1;
        public int SUPPORT_LEVEL_INFO = 2;
        public int SUPPORT_LEVEL_WARN = 3;
        public int SUPPORT_LEVEL_ERROR = 4;
        public int SUPPORT_LEVEL_FATAL = 5;


        public Dictionary<string,string> Config { get; set; } 
        public bool Initialized { get; set; }
        internal int _LEVEL = 0;
        internal bool _IS_ROLLING_LOG = false; //Si es true, se agrega al nombre la fecha _'yyyyMMdd'.log sin solo .log

        private static readonly ILog LOG = LogManager.GetLogger(typeof(GenerateTicketSupport));

        public bool Initialize(string config)
        {
            bool ret = false;
            try
            {
                if (string.IsNullOrEmpty(config)) return false;
                Config = JsonConvert.DeserializeObject<Dictionary<string, string>>
                              (System.IO.File.ReadAllText("Support.cfg"));
                if (Config != null)
                {
                    Config["SmtpClave"] = Utils.Decrypt(Config["SmtpClave"], "Biometrik@", "SaltBiometrika2021");
                    try
                    {
                        _LEVEL = Convert.ToInt32(Config["Level"]);
                    }
                    catch (Exception ex)
                    {
                        _LEVEL = 0;
                    }

                    try
                    {
                        if (Config.ContainsKey("IsRollingLog"))
                        {
                            _IS_ROLLING_LOG = (!string.IsNullOrEmpty(Config["IsRollingLog"]) &&
                                                Config["IsRollingLog"].Equals("1")) ? true : false;
                        } else
                        {
                            _IS_ROLLING_LOG = false;
                        }   
                    }
                    catch (Exception ex)
                    {
                        _IS_ROLLING_LOG = false;
                    }
                }

                Initialized = (Config != null);
                ret = Initialized;
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("GenerateTicketSupport.Initialize Excp Error: " + ex.Message);
            }
            return ret;
        }

        public bool GenerateTicket(string subject, string body, byte[] attach, 
                                   string extensionattach, string nameattach)
        {
            bool ret = false;

            try
            {
                if (!Initialized)
                {
                    ret = false;
                    LOG.Warn("GenerateTicketSupport.GenerateTicket - No inicializado clase GenerateTicketSupport!"); 
                 } else
                {
                    string[] listDestinataries = Config["DestinatariesList"].Split(',');


                    ret = Bio.Core.Notify.NotifyByMail.SendMail(Config["SmtpServer"],
                                                      Config["SmtpUser"], Config["SmtpClave"],
                                                      Config["SmtpDisplayName"], listDestinataries, subject,
                                                      body, attach, extensionattach, nameattach);
                }
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("GenerateTicketSupport.GenerateTicket Excp Error: ", ex);
            }
            return ret;
        }

        public bool GenerateTicket(int levelmsg, string subject, string body, bool attachlog)
        {
            bool ret = false;
            string auxLogName = null;
            try
            {
                //Filtro por nivel, asi puedo manejar varios niveles
                if (_LEVEL == -1) return true;  //Esta configurado OFF
                if (levelmsg < _LEVEL) return true; //Filtro los que estan debajo del level indicado para enviar mail
                                                    //Si es _LEVEL = 4 => Error y Mando un levelmsg = 2 Info => No lo envia

                if (!Initialized)
                {
                    ret = false;
                    LOG.Warn("GenerateTicketSupport.GenerateTicket - No inicializado clase GenerateTicketSupport!");
                }
                else
                {
                    string[] listDestinataries = Config["DestinatariesList"].Split(',');

                    if (attachlog)
                    {
                        auxLogName = _IS_ROLLING_LOG ?
                                        Config["CurrentLog"] + "_" + DateTime.Now.ToString("yyyyMMdd") + ".log" :
                                        Config["CurrentLog"] + ".log";
                        //System.IO.File.Copy(Config["CurrentLog"], Config["CurrentLog"] + ".copy");
                        //byte[] byLog = System.IO.File.ReadAllBytes(Config["CurrentLog"] + ".copy");
                        //System.IO.File.Delete(Config["CurrentLog"] + ".copy");
                        //string namelog = Path.GetFileName(Config["CurrentLog"]);

                        System.IO.File.Copy(auxLogName, auxLogName + ".copy");
                        byte[] byLog = System.IO.File.ReadAllBytes(auxLogName + ".copy");
                        System.IO.File.Delete(auxLogName + ".copy");
                        string namelog = Path.GetFileName(auxLogName);


                        ret = Bio.Core.Notify.NotifyByMail.SendMail(Config["SmtpServer"],
                                                          Config["SmtpUser"], Config["SmtpClave"],
                                                          Config["SmtpDisplayName"], listDestinataries, 
                                                          "[" + GetLevel(levelmsg) + "] " + subject,
                                                          Config["Point"] + Environment.NewLine + Environment.NewLine + body,
                                                          byLog, "txt", namelog);
                    } else
                    {
                        ret = Bio.Core.Notify.NotifyByMail.SendMail(Config["SmtpServer"],
                                                         Config["SmtpUser"], Config["SmtpClave"],
                                                         Config["SmtpDisplayName"], listDestinataries, subject,
                                                         body, null, null, null);
                    }
                    LOG.Debug("GenerateTicketSupport.GenerateTicket - Ticket generado => " + ret.ToString());
                }
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("GenerateTicketSupport.GenerateTicket Excp Error: ", ex);
            }
            return ret;
        }

        private string GetLevel(int levelmsg)
        {
            /*
                public int SUPPORT_LEVEL_OFF = -1; 
                public int SUPPORT_LEVEL_ALL = 0;
                public int SUPPORT_LEVEL_DEBUG = 1;
                public int SUPPORT_LEVEL_INFO = 2;
                public int SUPPORT_LEVEL_WARN = 3;
                public int SUPPORT_LEVEL_ERROR = 4;
                public int SUPPORT_LEVEL_FATAL = 5;
            */
            switch (levelmsg)
            {
                case 1:
                    return "DEBUG";
                case 2:
                    return "INFO";
                case 3:
                    return "WARN";
                case 4:
                    return "ERROR";
                case 5:
                    return "FATAL";
                default:
                    return "INFO";
            }
        }
    }
}
