﻿using log4net;
using Svg;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Kiosk.Common.Utils
{

    /// <summary>
    /// Se deben agregar aqui funciones genericas reutilizables si son especificas de
    /// los kioskos y sus componentes.
    /// </summary>
    public class Utils
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Utils));

        static public Image SetImageFromB64(string base64Photo)
        {
            Image ret = null;
            System.IO.MemoryStream ms;  
            try
            {
                byte[] byImage = Convert.FromBase64String(base64Photo);
                string s = Encoding.ASCII.GetString(byImage);
                if (Encoding.ASCII.GetString(byImage).StartsWith("<")) {  //Es SVG
                    using (var stream = new MemoryStream(byImage))
                    {
                        var svgDocument = SvgDocument.Open<SvgDocument>(stream);
                        var bitmap = svgDocument.Draw();
                        ret = bitmap;
                        //bitmap.Save(path, ImageFormat.Png);
                    }

                } else //Asume que es JPG u otro formato Bitmap directo
                {
                    //if (!string.IsNullOrEmpty(base64Photo))
                    //{
                        ms = new System.IO.MemoryStream(byImage);
                        ret = Image.FromStream(ms);
                        ms.Close();
                    //}
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("Utils.SetImageFromB64 Excp Error: " + ex.Message);
            }
            return ret;
        }

        public static string Encrypt(string value, string password, string salt)
        {
            DeriveBytes rgb = new Rfc2898DeriveBytes(password, Encoding.Unicode.GetBytes(salt));
            SymmetricAlgorithm algorithm = new TripleDESCryptoServiceProvider();
            byte[] rgbKey = rgb.GetBytes(algorithm.KeySize >> 3);
            byte[] rgbIV = rgb.GetBytes(algorithm.BlockSize >> 3);
            ICryptoTransform transform = algorithm.CreateEncryptor(rgbKey, rgbIV);
            using (MemoryStream buffer = new MemoryStream())
            {
                using (CryptoStream stream = new CryptoStream(buffer, transform, CryptoStreamMode.Write))
                {
                    using (StreamWriter writer = new StreamWriter(stream, Encoding.Unicode))
                    {
                        writer.Write(value);
                    }
                }
                return Convert.ToBase64String(buffer.ToArray());
            }
        }

        public static string Decrypt(string text, string password, string salt)
        {
            DeriveBytes rgb = new Rfc2898DeriveBytes(password, Encoding.Unicode.GetBytes(salt));
            SymmetricAlgorithm algorithm = new TripleDESCryptoServiceProvider();
            byte[] rgbKey = rgb.GetBytes(algorithm.KeySize >> 3);
            byte[] rgbIV = rgb.GetBytes(algorithm.BlockSize >> 3);
            ICryptoTransform transform = algorithm.CreateDecryptor(rgbKey, rgbIV);
            using (MemoryStream buffer = new MemoryStream(Convert.FromBase64String(text)))
            {
                using (CryptoStream stream = new CryptoStream(buffer, transform, CryptoStreamMode.Read))
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.Unicode))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }
    }
}
