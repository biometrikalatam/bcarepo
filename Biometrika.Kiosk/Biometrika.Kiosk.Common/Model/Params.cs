﻿using Biometrika.Kiosk.Common.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Common.Model
{
    /// <summary>
    /// No usado por ahora
    /// </summary>
    public class Params : IParams
    {
        Dictionary<string, object> _ParamsList;
        public Dictionary<string, object> ParamsList
        {
            get {
                return _ParamsList;
            }

            set {
                _ParamsList = value;
            }
        }

        public Params()
        {
            ParamsList = new Dictionary<string, object>();
        }
    }
}
