﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Biometrika.Kiosk.Common.Model
{
    /// <summary>
    /// Clase generica para manejar de forma facil parametros variables. Se utiliza en archivos
    /// de configuracion, para agregar valores en formato par key/value, de tal forma
    /// de tener flexibilidad. 
    /// Además se utiliza para enviar o recibir parámetros de formato generico key/value.
    /// Se utiliza una lista de DynamicDataItem para serializar hacia y desde archivos,
    /// y se maneja internamente un Dictionary<string, object> para acceso mas rápido 
    /// en runtime 
    /// </summary>
    public class DynamicData
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(DynamicData));

        Dictionary<string, object> _DynamicDataItems;
        List<DynamicDataItem> _ListDynamicDataItems;
        

        [XmlIgnoreAttribute]
        public Dictionary<string, object> DynamicDataItems
        {
            get {
                return _DynamicDataItems;
            }

            set {
                _DynamicDataItems = value;
            }
        }

        public List<DynamicDataItem> ListDynamicDataItems
        {
            get {
                return _ListDynamicDataItems;
            }

            set {
                _ListDynamicDataItems = value;
            }
        }

        public DynamicData()
        {
            _DynamicDataItems = new Dictionary<string, object>();
            _ListDynamicDataItems = new List<DynamicDataItem>();
        }

        /// <summary>
        /// Agrega nuevo par key/value en lista y en diccionario
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddItem(string key, object value)
        {
            try
            {
                if (_DynamicDataItems == null)
                    _DynamicDataItems = new Dictionary<string, object>();

                if (_ListDynamicDataItems == null)
                    _ListDynamicDataItems = new List<DynamicDataItem>();

                _ListDynamicDataItems.Add(new DynamicDataItem(key, value));
                _DynamicDataItems.Add(key, value);
            }
            catch (Exception ex)
            {
                LOG.Error("DynamicData.AddItem Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Update par key/value en lista y en diccionario
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void UpdateItem(string key, object value)
        {
            try
            {
                if (_DynamicDataItems == null)
                    _DynamicDataItems = new Dictionary<string, object>();

                if (_ListDynamicDataItems == null)
                    _ListDynamicDataItems = new List<DynamicDataItem>();

                //Remove in list
                foreach (DynamicDataItem item in _ListDynamicDataItems)
                {
                    if (item.key.Equals(key))
                    {
                        _ListDynamicDataItems.Remove(item);
                        break;
                    }
                }

                //Remove en IDictionary
                if (DynamicDataItems.ContainsKey(key))
                {
                    DynamicDataItems.Remove(key);
                }

                //Agrego de nuevo en ambos, con nuevos valores
                _ListDynamicDataItems.Add(new DynamicDataItem(key, value));
                _DynamicDataItems.Add(key, value);
            }
            catch (Exception ex)
            {
                LOG.Error("DynamicData.AddItem Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Toma un valor utilizando la key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object GetItem(string key)
        {
            try
            {
                if (_DynamicDataItems == null || _ListDynamicDataItems == null)
                    return null;

                return (_DynamicDataItems.ContainsKey(key) ? _DynamicDataItems[key] : null);
            }
            catch (Exception ex)
            {
                LOG.Error("DynamicData.GetItem Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Se utiliza cunado le lee un archivo desde disco y se deserializa, y se carga la lista. 
        /// Luego se ejecuta este metodo que inicaliza el Diccionario con los mismos valores.
        /// </summary>
        public void Initialize()
        {
            try
            {
                if (_DynamicDataItems == null)
                    _DynamicDataItems = new Dictionary<string, object>();

                if (_ListDynamicDataItems != null)
                {
                    foreach (DynamicDataItem item in _ListDynamicDataItems)
                    {
                        _DynamicDataItems.Add(item.key, item.value);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DynamicData.Initialize - Error : " + ex.Message);
            }
        }
    }

    /// <summary>
    /// Clase que manteine el par key/value, utilizado en DynamicData
    /// </summary>
    public class DynamicDataItem
    {
        public string key { get; set; }
        public object value { get; set; }

        public DynamicDataItem() { }
        public DynamicDataItem(string _key, object _value)
        {
            key = _key;
            value = _value;
        }

    }
}
