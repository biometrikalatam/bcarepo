﻿using Bio.Core.Matcher.Interface;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Common.Model
{
    /// <summary>
    /// Contiene los datos de una persona, cuando es identificada, para poder mostrar los datos en 
    /// el kiosko, si asi se necesita. Se completa luego del chequeo de BusinessRule.
    /// </summary>
    public class Persona
    {

        string _TypeId;
        string _ValueId;
        string _Name;
        Image _Photografy;
        ITemplate _Template;

        public Persona() { }

        public Persona(string typeId, string valueId, string name, string photografy, ITemplate template)
        {
            _TypeId = typeId;
            _ValueId = valueId;
            _Name = name;
            _Photografy = SetImageFromB64(photografy);
            _Template = template;
        }

        //public Persona(string typeId, string valueId, string name, string photografy, 
        //                ITemplate template)
        //{
        //    _TypeId = typeId;
        //    _ValueId = valueId;
        //    _Name = name;
        //    _Photografy = SetImageFromB64(photografy);
        //    _Template = template;
        //}

        private Image SetImageFromB64(string photografy)
        {
            throw new NotImplementedException();
        }
    }
}
