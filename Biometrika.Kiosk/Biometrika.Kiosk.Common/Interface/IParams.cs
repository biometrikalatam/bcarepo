﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Common.Interface
{
    /// <summary>
    /// No usado por ahora
    /// </summary>
    public interface IParams
    {
        Dictionary<string, object> ParamsList { get; set; }
    }
}
