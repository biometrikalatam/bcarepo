﻿using Biometrika.Kiosk.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Common.Interface
{
    /// <summary>
    /// Para interpretacion de códigos de barras leidos. POr ahiora no lo usamos, dado
    /// que se parsea en el mismo Bio.Core.SerialComm, se lee la cédula o el RUT desde 
    /// la credencial del Autoclub. Pero de la misma forma que se trabaja con 
    /// ActionEngine, MarkEngine, etc, se podria trabajar con BarcodeEngine y dada una
    /// lectura se puede procesar con varios Barcode implementados y retornar todos los 
    /// datos leidos en un DynamicData 
    /// </summary>
    public interface IBarcode
    {
        /// <summary>
        /// Indica si fue inicializada o no la clase.
        /// </summary>
        bool Initialized { get; set; }

        /// <summary>
        /// Nombre para identificar la marca a realizar
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Lee archivo de configuracion llamado Barcodes.cfg e instancia lo necesario 
        /// </summary>
        /// <returns></returns>
        int Intialize();

        /// <summary>
        /// Realiza el parseo de acuerdo a la configuracion establecida, con los datos recibidos desde los 
        /// parametros. 
        /// </summary>
        /// <param name="parameters">Lista de parametros key/value que viene desde el reconoicimiento</param>
        /// <param name="returns">Parametros relevantes retornados que puedan ser necesarios. Sino no es necesario solo null</param>
        /// <param name="msg">Mensaje a desplegar en el kiosko si hace falta</param>
        /// <returns>0-Funciono ok | menor a 1 - Código de error (en msg detalle del error indicado)</returns>
        int Parse(DynamicData parameters, out DynamicData returns, out string msg);
    }
}
