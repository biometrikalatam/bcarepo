﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Common.Interface
{
    /// <summary>
    /// Interface para instanciar un Kiosko, y configurar los Engines, para darle
    /// servicios a la UI del Kiosko. 
    /// Implementa su propio Config para manejo de datos de operación básicos.
    /// Por ahora no se manejan eventos, pero es factible agregar eventos para 
    /// independizar el manejo de eventos de la UI.
    /// </summary>
    public interface IKiosk
    {
        /* BASE */
        bool IsLoaded { get; set; }
        /// <summary>
        /// Configuraciones del kiosko
        /// </summary>
        IKioskConfig Config { get; set; }
        /// <summary>
        /// Motor de chequeo de reglas de negocio
        /// </summary>
        IBusinessRulesEngine BusinessRulesEngines { get; set; }
        /// <summary>
        /// Lista de Acciones a realizar cunado da posivo el chequeo de reglas de negocio
        /// </summary>
        IList<IActionEngine> ListActions { get; set; }
        /// <summary>
        /// Lista de motroes de marcas para generar marca cuando es correcta la regla de negocio
        /// </summary>
        IList<IMarkEngine> ListMarkEngines { get; set; }


        ///* EVENTS */
        ////event CaptureCallbackDelegate OnCapturedEvent; //(int errCode, string errMessage, List<Sample> samplesCaptured);
        //event IdentifyCallbackDelegate OnIdentifyEvent;
        //event ErrorCallbackDelegate OnErrorEvent; // (int errCode, string errMessage, ISensor sensor);
        ////event ConnectCallbackDelegate OnConnectEvent; // (int errCode, string errMessage, ISensor sensor);
        ////event DisconnectCallbackDelegate OnDisconnectEvent; // (int errCode, string errMessage, ISensor sensor);
        ////event TimeoutCallbackDelegate OnTimeoutEvent;  //(int errCode, string errMessage, ISensor sensor);

        ////Crea objetos, inicializa y carga los BFP si bspload = true
        //void BSPAttach(string bioAPIVersion, bool bspLoad);
    }
}
