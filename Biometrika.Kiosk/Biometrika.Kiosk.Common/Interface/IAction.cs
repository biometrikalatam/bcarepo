﻿using Biometrika.Kiosk.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Common.Interface
{
    /// <summary>
    /// Interface apra implementar las acciones que se realizan una vez identificada la persona
    /// y chequeado que pueda acceder. Una acción puede ser mandar a abrir un rele o todos.
    /// Se debe implementar cada accion por separado o en una única dll todas las acciones.
    /// Cada accion podra tener sus propios datos de config segun necesite.
    /// </summary>
    public interface IAction
    {
        /// <summary>
        /// Indica si fue inicializada o no la clase.
        /// </summary>
        bool Initialized { get; set; }

        /// <summary>
        /// Nombre para identificar la  a realizar
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Lee archivo de configuracion llamado ActionEngine.cfg e instancia todas las BR configuradas, si son 
        /// implementeaciones de IBusinessRule, y agrega en caso positivo, en BusinessRulesList 
        /// </summary>
        /// <returns></returns>
        int Intialize();

        /// <summary>
        /// Realiza la accion de acuerdo a la configuracion establecida, con los datos recibidos desde los 
        /// parametros. Cada IAction puede tener logica diferente, y configuracion diferente.
        /// </summary>
        /// <param name="parameters">Lista de parametros key/value que viene desde el reconoicimiento</param>
        /// <param name="returns">Parametros relevantes retornados que puedan ser necesarios. Sino no es necesario solo null</param>
        /// <param name="msg">Mensaje a desplegar en el kiosko si hace falta</param>
        /// <returns>0-Funciono ok | menor a 1 - Código de error (en msg detalle del error indicado)</returns>
        int DoAction(DynamicData parameters, out DynamicData returns, out string msg);
    }
}
