﻿using Biometrika.Kiosk.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Common.Interface
{
    /// <summary>
    /// Idem de IAction, pero para marcas. Una vez permitido el acceso se genera la marca.
    /// En el caso de Autoclub en el momento de hacer el chequeo de BusinessRules, se realiza la marca,
    /// por lo que la implementacion de marca para Autoclub es Dummy. No hace nada. En otras ocaciones
    /// posdria implementarse para hacer despues de accionar el acceso y generando marcas en diversos 
    /// lugares.
    /// </summary>
    public interface IMark
    {
        /// <summary>
        /// Indica si fue inicializada o no la clase.
        /// </summary>
        bool Initialized { get; set; }

        /// <summary>
        /// Nombre para identificar la marca a realizar
        /// </summary>
        string Name { get; set; }

         /// <summary>
        /// Lee archivo de configuracion llamado Mark.cfg e instancia lo necesario 
        /// </summary>
        /// <returns></returns>
        int Intialize();

        /// <summary>
        /// Realiza la marca de acuerdo a la configuracion establecida, con los datos recibidos desde los 
        /// parametros. 
        /// </summary>
        /// <param name="parameters">Lista de parametros key/value que viene desde el reconoicimiento</param>
        /// <param name="returns">Parametros relevantes retornados que puedan ser necesarios. Sino no es necesario solo null</param>
        /// <param name="msg">Mensaje a desplegar en el kiosko si hace falta</param>
        /// <returns>0-Funciono ok | menor a 1 - Código de error (en msg detalle del error indicado)</returns>
        int DoMark(DynamicData parameters, out DynamicData returns, out string msg);
    }
}
