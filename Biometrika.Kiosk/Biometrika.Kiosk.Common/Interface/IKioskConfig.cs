﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Common.Interface
{
    /// <summary>
    /// Interface para implementar el Config de las implementaciones de Kioskos
    /// </summary>
    public interface IKioskConfig
    {
        string Name { get; set; }

        //Logger
        string PathLoggerConfig { get; set; }

        //Tecnologias habilitadas para ingreso
        bool AccessByFingerprintEnabled { get; set; }
        string SerialSensor1 { get; set; }
        string SerialSensor2 { get; set; }
        int QualityCapture { get; set; }
        bool AccessByFacialEnabled { get; set; }
        string PathMatcherConfig { get; set; }

        bool AccessByMRZEnabled { get; set; }
        bool AccessByBarcodeEnabled { get; set; }
        string PortCOMBarcode1 { get; set; }
        string PortCOMBarcode2 { get; set; }

        string Rele1 { get; set; }
        string Rele2 { get; set; }
        int KioskType { get; set; }
        int KioskType1 { get; set; }
        int KioskType2 { get; set; }
        //private bool _enableSaveXMLToEnroll = true;
        //private string _pathToEnrollBatch = @"c:\Biometrika\WebAssistance.Kiosk\DataEnroll\ToEnrollBatch";
        //private string _pathEnrolledBatch = @"c:\Biometrika\WebAssistance.Kiosk\DataEnroll\EnrolledBatch";

        //private int _authenticationFactor = 2; //Fingerprint default
        //private int _minutiaeType = 7; //Verifinager Default

        ////private string _pathBDConfig;

        //private string _pathMatcherConfig;
        //private string _pathConnectorConfig;

        //private string _assemblyLogin = "WebAssistance.Kiosk.Login.NativeLogin";
        //private string _assemblyLoginParam = "";

        //private int _timerShowResult = 3000;
        //private int _timerRelay = 2000;  //Si es > 0 => Abre, sino no hace nada (para solo admin no hace falta abrir acceso)

        //private int _typeReleConnection = 1; //1-USB Rele directo | 2-Web Service 
        //private int _portComUsbRelay = 6;
        //private bool _portComUsbRelayOpenDoor = false;
        //private int _releNumberDoor1 = 2;
        //private bool _portComUsbRelayOpenDoor2 = false;
        //private int _releNumberDoor2 = 3;
        //private int _daystocheckvenc = 30;

        //private float _threshold = 10; //Default para Verifinger
        //private int _matchingType = 1; //1 - First Ok | 2 - Best Ok
        //private float _score = 47; //Default para Verifinger

        //private int _companyId = 6; //Id Empresa en WA
        //private int _companyIdRemote = 6; //Id Empresa en WA en datacenter Central

        //private int _markPoint = 2; //Numero de punto de marcacion de WA
        //private string _codePoint = "SedeSportlife"; //Numero de punto de marcacion de WA
        //private int _companyIdBP = 9; //Id Empresa en BioPortal
        //private int _timeoutWS = 30000; //Timeout para llamados a WS

        //private int _wwoIdDefault = 1;
        //private int _cocIdDefault = 5;

        //private int _withPhoto = 170;
        //private int _heightPhoto = 170;

        //private string _pathPubTop;
        //private string _pathPubLeft;
        //private string _pathFondoKiosk;

        ////Info de Torniquetes
        //private int _qKioskAccessPoints = 1; //Indica cantidad de troniquetes en el acceso
        ////private WAKioskAccessPointConfig WAKioskP1 = null;
        ////private WAKioskAccessPointConfig WAKioskP2 = null;
        //private WAKioskAccessPoint APConfig1;
        //private WAKioskAccessPoint APConfig2;

        //private int _hourToFlash = 8;
        //private int _timeToSleepRefreshBIRs = 60000;

        //private bool _enableTimerNoMark = false;
        //private int _timerNoMark = 1200000; //20 minutos
        //private bool _enableSerial = false;
        //private int _elapsedReportDays = 60;

        ////Config para OCR
        //private bool _enableOCR = false;
        //private bool _OCRLogEnable = true;
        //private int _OCRLogLevel = 3;
        //private int _OCRLogLogMask = -1;
        //private string _OCRLogPathFile = "log\\SwipeReader.Net.log";

        //private string _maskDate = "yyyy-dd-MM";

    }
}
