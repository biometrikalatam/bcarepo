﻿using Biometrika.Kiosk.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Common.Interface
{
    /// <summary>
    /// IScreenEngine maneja una ventana para visualizar datos de forma customizada. 
    /// Por ejemplo BancoFalabella, que debe mostrar datos especificos de reunion, no es dato solo de personas
    /// Luego podria extenderse esto apra manejar todas las ventanas del flujo que se defina, por ahora maneja solo una pantalla
    /// Y la muestra. Debe tener sus configuraciones como el Theme en el kiosko, dentro del config.
    /// </summary>
    public interface IScreenEngine
    {
        /// <summary>
        /// Indica si fue inicializada o no la clase.
        /// </summary>
        bool Initialized { get; set; }

        /// <summary>
        /// Nombre para identificar la pantalla a mostrar
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Screen configurado en este caso. Luego se deberia extender a  List<IScreen> ScreenList
        /// para manejar varios tipos de pantallas independientes cada una con su logica. Y agregar
        /// un workflow de transiciones de pantallas.
        /// </summary>
        IScreen Screen { get; set; }

        /// <summary>
        /// Lee archivo de configuracion llamado ScreenEngine.cfg.
        /// Aqui debe configurarse imagenes y posiciones como en Theme.
        /// Para la primera bversion solo manejaremso lo mínimo indispensable que es posición relativa
        /// con respecto a la ventana padre y tamaño. (A priori (x,y,w,h) = (0,0,900,1600) para tapar toda
        /// la ventana como en la pantalla d eingreso de RUT (BancoFalabella).
        /// Crea la ventana (Form) para luego utilizarlo en al Show
        /// </summary>
        /// <returns></returns>
        int Intialize();

        /// <summary>
        /// Crea la ventana y la muestra
        /// </summary>
        /// <param name="parameters">Lista de parametros key/value que viene desde el reconoicimiento</param>
        /// <param name="returns">Parametros relevantes retornados que puedan ser necesarios. Sino no es necesario solo null</param>
        /// <param name="msg">Mensaje a desplegar en el kiosko si hace falta</param>
        /// <returns>0-Funciono ok | menor a 1 - Código de error (en msg detalle del error indicado)</returns>
        int DoShow(System.Windows.Forms.Form formParent, DynamicData parameters, out DynamicData returns, out string msg);
    }
}
