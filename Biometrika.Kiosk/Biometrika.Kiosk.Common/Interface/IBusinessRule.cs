﻿using Biometrika.Kiosk.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Common.Interface
{
    /// <summary>
    /// Idem que IAction, pero para BusinessRules. LA BusinessRule es la que determina
    /// que un Id reconocido (RUT, UID, o lo que se defina) puede o no acceder.
    /// Cada BuissinesRule puede tener su propio archivo de config, y ejecutar una o mas
    /// acciones para responder.
    /// </summary>
    public interface IBusinessRule
    {

        /// <summary>
        /// Indica si fue inicializada o no la clase.
        /// </summary>
        bool Initialized { get; set; }

        /// <summary>
        /// Parametros de funcionamiento key/value list, para tener flexibilidad para cada regla
        /// </summary>
        DynamicData Config { get; set; }

        /// <summary>
        /// Lee archivo de configuracion llamado BusinessRules.cfg e instancia todas las BR configuradas, si son 
        /// implementeaciones de IBusinessRule, y agrega en caso positivo, en BusinessRulesList 
        /// </summary>
        /// <returns></returns>
        int Initialize();

        /// <summary>
        /// Chequea la business rules y si da ok, retorna los valores necesarios para luego pasarlos a 
        /// IMarkEngine y a IActionEngine por si son necesarios, y mensjae a desplegar en el kiosko. 
        /// </summary>
        /// <param name="parameters">Lista de parametros key/value que viene desde el reconoicimiento</param>
        /// <param name="accessresult">INdica true/false si puede acceder</param>
        /// <param name="returns">Parametros relevantes retornados que puedan ser necesarios en IMarkEngine y a IActionEngine
        /// cmo key/value. Sino no es necesario solo null</param>
        /// <param name="msg">Mensaje a desplegar en el kiosko</param>
        /// <returns>0-Funciono ok | menor a 1 - Código de error (en msg detalle del error indicado)</returns>
        int CheckBusinessRule(DynamicData parameters, out bool accessresult, out DynamicData returns, out string msg);

        /// <summary>
        /// Se agrega por si se necesita liberar recursos como el thread de check en linea de SportlifeNorte
        /// </summary>
        /// <returns></returns>
        int Dispose();
    }
}
