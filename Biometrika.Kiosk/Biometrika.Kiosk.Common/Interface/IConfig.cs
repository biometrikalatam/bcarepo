﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Biometrika.Kiosk.Common.Interface
{
    /// <summary>
    /// Por ahora no se usa. Se usa directamente DynamicData para
    /// para configs genericos.
    /// </summary>
    public interface IConfig
    {
        [XmlIgnoreAttribute]
        Dictionary<string, object> ConfigItems { get; set; }

        List<ConfigItem> ListConfigItems { get; set; }

        void AddItem(string key, object value);
        object GetItem(string key);


        void Initialize();
    }

    public class ConfigItem
    {
        public string key { get; set; }
        public object value { get; set; }

        public ConfigItem() { }
        public ConfigItem(string _key, object _value)
        {
            key = _key;
            value = _value;
        }

    }
}
