﻿using Biometrika.Kiosk.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Common.Interface
{
    /// <summary>
    /// Motor de ejecucion de la acción para ejecutar cuando el acceso es correcto.
    /// Ahora esta implementado con una única acción. Pero se podría cambiar la propiedad
    /// IAction Action { get; set; } por List<IAction> Actions { get; set; } para configurar
    /// mas de una acción, y luego cambiar el DoAction para ejecutar todas als acciones
    /// configuradas.
    /// </summary>
    public interface IActionEngine
    {
        /// <summary>
        /// Indica si fue inicializada o no la clase.
        /// </summary>
        bool Initialized { get; set; }

        /// <summary>
        /// Nombre para identificar la  a realizar
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Action configurado en este caso. Luego se deberia extender a  List<IAction> ActionsList
        /// para manejar varios tipos d emarcas independientes cada una con su logica
        /// </summary>
        IAction Action { get; set; }

        /// <summary>
        /// Lee archivo de configuracion llamado ActionEngine.cfg e instancia todas las BR configuradas, si son 
        /// implementeaciones de IBusinessRule, y agrega en caso positivo, en BusinessRulesList 
        /// </summary>
        /// <returns></returns>
        int Intialize();

        /// <summary>
        /// Realiza la accion de acuerdo a la configuracion establecida, con los datos recibidos desde los 
        /// parametros. Cada IAction puede tener logica diferente, y configuracion diferente.
        /// </summary>
        /// <param name="parameters">Lista de parametros key/value que viene desde el reconoicimiento</param>
        /// <param name="returns">Parametros relevantes retornados que puedan ser necesarios. Sino no es necesario solo null</param>
        /// <param name="msg">Mensaje a desplegar en el kiosko si hace falta</param>
        /// <returns>0-Funciono ok | menor a 1 - Código de error (en msg detalle del error indicado)</returns>
        int DoAction(DynamicData parameters, out DynamicData returns, out string msg);
    }
}
