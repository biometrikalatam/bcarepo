﻿using Bio.Core.Matcher.Interface;
using Biometrika.Kiosk.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Common.Interface
{
    /// <summary>
    /// Interface para levantar a memoria los templates para verificación biométrica.
    /// Ahora está solo implementada la de huellas con Verifinger (Biometrika.Kiosk.LoadBIRDatabase), 
    /// pero se puede implementar varias, algunas genericas de nuestros productos u otras fuentes.
    /// Además es posible levantar caras, patrones de iris, etc.
    /// LA actual implementacion toma los datos directo desde base de datos, pero es factible
    /// hacerlo via WS o APIRest, para tener mas felxibilidad.
    /// </summary>
    public interface ILoadBIRs
    {
        /// <summary>
        /// Indica si está o no inicializada
        /// </summary>
        bool Initialized { get; set; }

        /// <summary>
        /// Lista de parametros de funcionamiento 
        /// </summary>
        DynamicData Parameters { get; set; }

        /// <summary>
        /// Lee desde disco el config de la dll, que es mismo nombre .cfg. Ej: Biometrika.Kiosk.LoadBIRDatabase.cfg
        /// y deserializo en Parameters
        /// </summary>
        /// <returns></returns>
        int Initialize();

        /// <summary>
        /// Hace el LOadBIRs y devuelve la lista de personas con sus temaplates y datos personales
        /// para mostrar en el kiosko cuando se lo identifica.
        /// </summary>
        /// <param name="birs"></param>
        /// <returns></returns>
        int DoLoadBIRs(out List<ITemplate> birs);

    }
}
