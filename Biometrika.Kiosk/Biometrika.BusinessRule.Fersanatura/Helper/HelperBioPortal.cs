﻿using Bio.Core.Api.Constant;
using Bio.Core.Log;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BusinessRule.Fersanatura.Helper
{
    internal class HelperBioPortal
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(HelperBioPortal));
        //Constantes
        string _URLService_Base = "https://localhost:7160";
        string _API_KEY_Service = "username1";
        string _SECRET_KEY_Service = "secretkey1";
        int _AF_Verify = Bio.Core.Api.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL;
        int _MT_Verify = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_JPG;
        int _AF_Sample = Bio.Core.Api.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL;
        int _MT_Sample = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_JPG;
        int _THRESHOLD = 50;
        int _Timeout = 10000;

        VerifyRequestParam _PARAM;
        VerifyResponse _RESPONSE;

        string _JWT_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImVlNDdhYWYxLTVhYTItNGE3Ni04NTlhLTQ3NTI5ZjM1ZjZiOSIsIkNJZCI6IjAiLCJuYW1lIjoiYXBpdXNlckBiaW9tZXRyaWthIiwiZW1haWwiOiJpbmZvQGJpb21ldHJpa2EuY2wiLCJyb2xlIjoiQWRtaW4iLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL2V4cGlyYXRpb24iOiIzMS8wMS8yMDIzIDE1OjA4OjQ4IiwibmJmIjoxNjc1MTc0MTI4LCJleHAiOjE2NzUxNzc3MjgsImlhdCI6MTY3NTE3NDEyOH0.cUNUAmn9H_szooSXLwkwVi_ZYCC0ti1XA_PEcphP7QM";
        DateTime _JWT_TOKEN_VENCIMIENTO = DateTime.Now.AddMinutes(-1); 
        DateTime _JWT_TOKEN_VENCIMIENTO_INTERNO = DateTime.Now.AddMinutes(-1); //Added x si hay diferencia horaria de server y pc cliente => Seteo 1h por ahora


        public HelperBioPortal(string url, string apikey, string secretkey, int afverify, int mtverify, int afsample, int mtsample, int threshold, int timeout)
        {
            _URLService_Base = url;
            _API_KEY_Service = apikey;
            _SECRET_KEY_Service = secretkey;
            _AF_Verify = afverify;
            _MT_Verify = mtverify;
            _AF_Sample = afsample;
            _MT_Sample = mtsample;
            _THRESHOLD = threshold;
            _Timeout = timeout; 
        }

        internal string GetExpirationToken()
        {
            string ret = "";
            try
            {
                ret = _JWT_TOKEN_VENCIMIENTO_INTERNO.ToString("dd/MM/yyyy HH:mm:ss.fff");
            }
            catch (Exception ex)
            {
                ret = "";
                LOG.Debug("HelperBioPortal.GetExpirationToken Excp [" + ex.Message + "]");
            }
            return ret;
        }

        internal string GetToken()
        {
            try
            {
                LOG.Debug("HelperBioPortal.GetToken IN...");
                if (_JWT_TOKEN_VENCIMIENTO_INTERNO < DateTime.Now)
                {
                    string uri = "/api/v1/getToken"; 
                    var client = new RestClient(_URLService_Base + uri);
                    client.Timeout = _Timeout;
                    var request = new RestRequest(Method.GET);
                    string _BasicKey = _API_KEY_Service + ":" + _SECRET_KEY_Service;
                    byte[] byBasicKey = Encoding.ASCII.GetBytes(_BasicKey);
                    string _BasicKeyEncripted = Convert.ToBase64String(byBasicKey);
                    request.AddHeader("Authorization", "Basic " + _BasicKeyEncripted); // dXNlcm5hbWUxOnNlY3JldGtleTE=");
                    LOG.Debug("HelperBioPortal.GetToken - Calling " + _URLService_Base + uri);
                    IRestResponse response = client.Execute(request);
                    if (response != null)
                    {
                        TokenResponse _TR = JsonConvert.DeserializeObject<TokenResponse>(response.Content);
                        if (_TR != null)
                        {
                            _JWT_TOKEN = _TR.token;
                            _JWT_TOKEN_VENCIMIENTO = _TR.expiration;
                            _JWT_TOKEN_VENCIMIENTO_INTERNO = DateTime.Now.AddMinutes(55); //
                            LOG.Debug("HelperBioPortal.GetToken - Token OK! RENEW - Expiration Server = " + 
                                        _JWT_TOKEN_VENCIMIENTO.ToString("dd/MM/yyyy HH:mm:ss.fff") + 
                                        " [Expiration Local = " + _JWT_TOKEN_VENCIMIENTO_INTERNO.ToString("dd/MM/yyyy HH:mm:ss.fff"));
                        } else
                        {
                            _JWT_TOKEN = null;
                            _JWT_TOKEN_VENCIMIENTO = DateTime.Now.AddMinutes(-1);
                            _JWT_TOKEN_VENCIMIENTO_INTERNO = DateTime.Now.AddMinutes(-1);
                            LOG.Warn("HelperBioPortal.GetToken - Error deserializando salida de servicio...");
                        }
                    }
                    else
                    {
                        _JWT_TOKEN = null;
                        _JWT_TOKEN_VENCIMIENTO = DateTime.Now.AddMinutes(-1);
                        _JWT_TOKEN_VENCIMIENTO_INTERNO = DateTime.Now.AddMinutes(-1);
                        LOG.Warn("HelperBioPortal.GetToken - Error servicio salida nula...");
                    } 
                } else
                {
                    LOG.Debug("HelperBioPortal.GetToken - Token OK! No Renew - Expiration Server = " 
                                + _JWT_TOKEN_VENCIMIENTO.ToString("dd/MM/yyyy HH:mm:ss") +
                                 " [Expiration Local = " + _JWT_TOKEN_VENCIMIENTO_INTERNO.ToString("dd/MM/yyyy HH:mm:ss.fff"));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("HelperBioPortal.GetToken Excp [" + ex.Message + "]");
                return null;
            }
            LOG.Debug("HelperBioPortal.GetToken OUT! Token OK = " + ((!string.IsNullOrEmpty(_JWT_TOKEN)).ToString()));
            return _JWT_TOKEN;
        }

        public VerifyResponse Verify(string valueid, string sedeid, string sample)
        {
            DateTime dtStart = DateTime.Now;
            DateTime dtEnd = DateTime.Now;
            try
            {
                LOG.Debug("HelperBioPortal.Verify IN...");
                _RESPONSE = new VerifyResponse();
                _PARAM = new VerifyRequestParam();
                _PARAM.ActionId = Bio.Core.Api.Constant.Action.ACTION_VERIFY;
                _PARAM.BodyPart = -1;
                _PARAM.AuthenticationFactor = _AF_Verify;
                _PARAM.MinutiaeType = _MT_Verify;
                _PARAM.SaveAudit = 0;
                _PARAM.Threshold = _THRESHOLD;
                _PARAM.ClientId = sedeid;
                _PARAM.TypeId = "RUT";
                _PARAM.ValueId = valueid;
                _PARAM.OperationOrder = 1;
                _PARAM.ConnectorId = "NONE";
                _PARAM.EnrollOption = 2; //No enrola
                _PARAM.MatchingType= 1;
                _PARAM.Origin = 1;
                _PARAM.IpEndUser = "127.0.0.1";
                Sample _Sample = new Sample();
                _Sample.AuthenticationFactor = _AF_Sample;
                _Sample.MinutiaeType = _MT_Sample;
                _Sample.BodyPart = -1;
                _Sample.Data = sample;
                _Sample.AdditionalData = null;
                _PARAM.Samples = new List<Sample> { _Sample };
                string _STR_PARAM = JsonConvert.SerializeObject(_PARAM);

                var client = new RestClient(_URLService_Base + "/api/v1/bp/verify");
                client.Timeout = _Timeout;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "Bearer " + GetToken());
                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("application/json", _STR_PARAM, ParameterType.RequestBody);
                LOG.Debug("HelperBioPortal.Verify - Calling " + _URLService_Base + "/api/v1/bp/verify => " + valueid + "...");
                IRestResponse response = client.Execute(request);
                dtEnd = DateTime.Now;
                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    _RESPONSE = JsonConvert.DeserializeObject<VerifyResponse>(response.Content);
                    if (_RESPONSE != null)
                    {
                        LOG.Debug("HelperBioPortal.Verify - Respuesta deserialziada ok...");
                        if (_RESPONSE.data!= null)
                        {
                            LOG.Debug("HelperBioPortal.Verify - result = " + _RESPONSE.data.result + " | score = " + _RESPONSE.data.score.ToString()
                                + " / threshold = " + _RESPONSE.data.threshold.ToString());
                        } else
                        {

                        }
                    }
                } else
                {
                    _RESPONSE.code = Errors.IERR_CONNECTOR_REMOTE_ERROR;
                    _RESPONSE.message = "Retorno BioPortal servicio nulo! [StatusCode = " + (response == null ? "Null": response.StatusCode.ToString()) + "]";
                    LOG.Debug("HelperBioPortal.Verify - respuesta verifiy null...");
                }
            }
            catch (Exception ex)
            {
                _RESPONSE.code = Errors.IERR_UNKNOWN;
                _RESPONSE.message = "Error BioPortal Verify Excp [" + ex.Message + "]";
                LOG.Error("HelperBioPortal.Verify Excp [" + ex.Message + "]");
            }
            

            LOG.Debug("HelperBioPortal.Verify OUT! Time to Verify = " + (dtEnd-dtStart).Milliseconds.ToString() + " milisegundos!");
            return _RESPONSE;
        }


    }

    //TOKEN Response
    public class TokenResponse
    {
        public string token { get; set; }
        public DateTime expiration { get; set; }
        public object message { get; set; }
    }

    //REQUEST
    public class VerifyRequestParam
    {
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public int ActionId { get; set; }
        public int AuthenticationFactor { get; set; }
        public int MinutiaeType { get; set; }
        public int BodyPart { get; set; }
        public List<Sample> Samples { get; set; }
        public int OperationOrder { get; set; }
        public int MatchingType { get; set; }
        public int Origin { get; set; }
        public int Threshold { get; set; }
        public int SaveAudit { get; set; }
        public int EnrollOption { get; set; }
        public object ConnectorId { get; set; }
        public string ClientId { get; set; }
        public object IpEndUser { get; set; }
    }

    public class Sample
    {
        public int Id { get; set; }
        public int AuthenticationFactor { get; set; }
        public int MinutiaeType { get; set; }
        public int BodyPart { get; set; }
        public string Data { get; set; }
        public object AdditionalData { get; set; }
    }

    //RESPONSE
    public class VerifyResponse
    {
        public int code { get; set; }
        public string message { get; set; }
        public Data data { get; set; }
    }
    public class Data
    {
        //public DateTime start { get; set; }
        //public DateTime end { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public object message { get; set; }
        public object error { get; set; }
        public double score { get; set; }
        public int result { get; set; }
        public double threshold { get; set; }
        public int bodypart { get; set; }
        public int companyidenroll { get; set; }
        public int useridenroll { get; set; }
        public object verificationsource { get; set; }
    }
    //var body = @"{" + "\n" +
    //                                @"  ""TypeId"": ""RUT"",
    //                " + "\n" +
    //                                @"  ""ValueId"": ""21284415-2"",  
    //                " + "\n" +
    //                                @"  ""ActionId"": 1,
    //                " + "\n" +
    //                                @"  ""AuthenticationFactor"": 4,
    //                " + "\n" +
    //                                @"  ""MinutiaeType"": 44,
    //                " + "\n" +
    //                                @"  ""BodyPart"": 16,
    //                " + "\n" +
    //                                @"  ""Samples"": [
    //                " + "\n" +
    //                                @"    {
    //                " + "\n" +
    //                                @"      ""Id"": 1,
    //                " + "\n" +
    //                                @"      ""AuthenticationFactor"": 4,
    //                " + "\n" +
    //                                @"      ""MinutiaeType"": 41,
    //                " + "\n" +
    //                                @"      ""BodyPart"": 16,
    //                " + "\n" +
    //                                @"      ""Data"": ""/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAHgAoADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDod1LuqPIo3VYEu6jNR5pc0mwJc0Z96jzSg0XAkzRmmbqXNMB+aXNMzS5pAPBNOzUeaAaQEuaWow1LuoAkB9aXNMDUuadx3H7jTgajzS5pNiJKKZmjNFx6kmaXNR5pc0XGiSimZpc0XGOpc0zNLmkA/NLmmUUASAilzUeaXdQBJmjNMzS5p3GPpc1HmlzSBMfmnA1HRQBJmlzUeaXNA7klGajzS5oC5JmlBqPNLk0ASZpM03NGaAuPzSg1HmlzQHUkzRn3qOjNBVyXNFR5pc0XE0PzRmmbqXdQK47NLmmbqN1AXH5ozTN1G6i4XH5ozTN1G6ncLj80Zpm6jdRcB+TSZpm+jdQIkzS7h6VDuo3UXC9iXNGai3UbqdwuS7vek3VHupN1IRLu96N3vUO6jcBQFyXdSF6iLikL07ktku+k31Fv4600vQmIm30haod9NL1QE2+mlqhL00yYouBMXzTd9Q76aZKdwMbNLmod1KGqAJQ1LuqLcKUNSAmDUu6od1ODU7AS5pQai3Uu6kBLmnA1Dupd1AE2admod1KDQBLmlzUQNOzSAkBpc1HmlzQMlBpc1FmlBoAlzRmmBqXPNAD80uTTM0ZoBD8+9Ln3plGaCh+6nBqjzS0ASZpc1GDS5oAfmlzTM0uaB2H0U3NLmgBc07NMzS5oAfuo3UyigCTNLmmbqN2aAH5pc0zNGaBj80u4+tMzRSuCJN3rRmmUuaAHZpQ1MzRTAkzRmmZozQNMkzS7qjzRmgbZJuozTM0bqBEmaM1Hmk3UCJd1Jmo91G6gCQmk3UzdSZoAk3Ck3UzNJmgRJuo3VHmjNAMk3Cjd71HmjNAiTdSbqZmkzQIfupN1M3Ubs0xXH7qN1Rk0hNANjy1NLUwtTS1Ah+40hb3qPdTC9AEu6mmSoy5qMvQBMZKYXqIsTTc0wJDJTS5pmaSmBlbqXdUG/wB6N1ICxupc1AGpwekBNml3Gog1O3UAShqcGqDdTg1AEuaXdUQanBqLgSA07dUWaXNICYNS5qIGn7qAJQaUGogadmgZJmnZqMGnZoAdmlBplLQA/NLmmUtA0PBpc0yjNAySlzUeaXNAElGaaDS5oAdmlzTM0ooKH5pc0ylzQA/NFNzS5oAdmlz703NGRSuA+im5opj6Ds0ZpuaM0hD804Go6XNAD80tR5pc0hj80ZpmaXdTEx+aXNR5pc0DQ/NGaZn3pN1MZJmjNMzSZNAD80ZpmaM0Ejs0ZpuaQmgB+aTNMopDH5ozTKKZI/NGaZS5oEOzRkUyigQ4tSZpM03NAMfmkJphPGabuNAh5akLmmbqbmmIeWphamlqaWoAcWphY03NJnNACkmkzSZooAKQmkbimE0AOLU0tSZpDTYGHu96XcfWogcUu6gCXeacHqHNLuoAsbqXdUAY04PSAmDU/dUG6l3UAThqeDVYGnhqQE+acGqINTgaYE2aXNRA08GkMkBp4NRg0oNAEgp1MpQaAHg04GmUooGh9FJS0DFpaSigB1GaSloAcKUU3NLzSGkOoptLQMcDS5ptLQgFp1NopgOzRkUlLjNJjHA+9Lmm496PxpXAdRmkpaAYuaWkFFAh1JSUtAxaKKKVxhRRRTuAUUUuDSTASilwfSkwaYBzRS4PpSYNMmwUlKQaSmAlLSUUkAuaKSkpgOozTM0b6BDs0mfem5FNJoExxNN3U0tTd1AmP3U0tTS1NJ96AY7caTJpmaM0CHU00ZppIpiFpM0m6mk0gHbqQk03NJmgBSabRRmmAUmfekLU0mkBhFcdqSrnlx+9IIk7nPpTAqjNOANWRHHjoacI4/Q5oArCnYzU/lx9waeEjPGygCsKXBq15a/3KcFX+6KQysMkU4CrIUelOAH90UCIFFPFTDj+EUv4UDIwKeB7VIM08Z9BQIiAPpTgD6VKM0ozQMYAfSnBT6VJg+tLzQAwITzTtlO5pcGgBApx0pQpNLinAUFDdhpfLPrTsUuKAGiPjrSiL/ap4FOAFAXIxEP71O8sf3qfgUtA0xnlj1o8setSYFLxSYyPYKUItPGKKQCbVo2rT+KOKoBu1fQ0bV9KdRSYCYHpS/L6UcU7jFIY0AZ6U4KPSgECnAigEN2j0pcD0p2aM0DG4HpS4HpS5ozQ0Kwm0HtSgAdqXNGaTQw49KPwoBFOBosAmPal4/u0uaTNOwB+FJ36UuaMigBM0mfal4pOKYAfpSUvFFMkafcUnHpTqKQDMD0pMD0qSkpiI8D0pCo9KeabQK4zaPSk2j0p9IaBtkRA9KbgZ6VKfrTDQSxm0Z6Um0elPpKBEewelIQPSpM004oAjwKCB6U4000CGFR6GkwPSn0maAG4FJgZp2eaQimA3AppFOpM0gGFc1G0bdqnoqWMzMKKPlptJ+NaMRKNtLlBUGR6ilyo/iH50gJ8il3Cq4dem4fnTtwx1FAE+4U4MKrh/ejzR60AWtwpQ4qp5o9aUTL60AXN4pQ4qn5o9aXzaQFzfT9wqmsuasI2aAJg1OBNNFOGKBjgTS0gIp1AAM0vNJS0ALz60oBoBpwNAwwfWlozS0DAU4UlKMUALThSA0tABSgGkz7UtIoXFLikzRmmAtKB6U3IprSqoyWAA7mkBJijjFY9x4js4c7W8zHXFc3r/jENaeTaZVmPLCmF0d0XQdWUfjSoVY8EH6GvCrjVrozuWnkOPVjVyx8UXlo6hJmyepJzRyiuz2vj2pRivKW8a3+w5k3HHcCnWvje8i+ZzuzjilYdz1bj1FFect4/m80ARR5I/Orh8dMEyYkZsdFJo1Hc7ujFZmkaomp2STqQGP3gO1aW7NAx2BRikpc0gClpKKAFxRSc0ZNMBaKbmloAXFGKbzRmmAtFNzS5oFYWm0ZppagVh1IRSbqaTQAuKSkzTcmgVhTSUhJppagQpxTaTNJmgVhTTaQsaYWoEPIptNLGmljQA402kLe1NLmgQ6mmmFjSbzQA+io95pC5pgSE03NRFznNMMhpAWO3WnCqu805XIFIDHL5HU1GzDHWq/m0nmVYiYlePamlh1qLfQSTSGSxkB81ZD1msXH3TijMp6yNQBp+ZgdKTzKzv3gHMh/Ojcc58z9aANINS5rM3oT/AK1f++qT7TbL1uFyPegDXVqeGGcZrEOo2K43XSZP+1Sf2zpqH/j5U/SgDoFYZqxG/HUVy/8AwkmmL1nPH+yasW/inTXdY1diSeuKGB1CtTwaqQzJJGHRgVPQ1YU0hkoIp4NRA09TQA+lpKKAHU4GmU4UFDgadTRTqAFpRSUooAeMilpKKBoWlpKXNJjCkJqOSURjJOKxtT1yO2hYKfnoC5fvdSitombIODiuH1zxNLcM8cTFYxWNqGtTTNhnI+YnFY8k+8ls8nrTCzZd/tB2jZF61lTTl0DHseafCfnbPcVWKHkHimOwSENnvmoDlCG9KmCsOMU/yT0IpCsNaRim7Oe+KjMjEcGrCQnBAFNMBAyFoHZiRSH5STmrkbtyoPaqscPb3qyqlGye9A0dR4f1mXTZFUElD1Ga9OsbyO7tkmjOcjmvGLYn5T0INdr4a1ZoJPJkJ2N09jSY2d9zS1DDMJEDA9alzUiFoopaYxKKXFGKBCUUtJQAlFLxRQAlJTqKLgNxSYp34UlK4DcUypaYetMTGUGlopoQzFNIqSm0MTGGmEVIabTERkU361IaaR70CYwimkU8000CIyKaakNMIoAZTTmnmm0CGUU7FNIoAYaYQakNNNADKeBxSYpwFIZ5Eviu76bVA9KQ+Krw8fIB9Otc2G9afu9KdzX2aN8+Jr4g4cD8KjPiHUGzmXr7VjA+1G6k2WqSNU6xfN1uGpDqV2xObhzn3rPU0/NS5Mr2MS0b64Of37/nTDcynrK/5mq5akzSUh+ziWDK3dm/Ok3kj1/GoRmnZ+tPmKVOI/PHWpE6VFlaki4FJzY1Ti2K1WLH/j6TPuarnrVvThuul47Gp52aKmj0nw8f+JUnPO45/OtpWrC0VtunRDp1NbEbZrSLujiqfEW1NSrUCGp1qjOw8ClxSUtACindaSlFAxQKdxSCnUAKKUUlKKQDselKKQUuaLlC4phYAU4kVn304iUHOBihgU9YvRCqDPymuA1S9eW5dQcgHNauqahJcP5Q5wcCqEemqfnlyWovYqMbs5+WF5DuA60qWLHnBroTbRp0UUhQAdKhzOmNJGQlhnlhilOnAnK9/WtUKB6Um3mjnNfZxMttOyM45qdNOVlGeoq+AMU5etPnJdOJSWwAOcDj2obTUbkd/UVoYOaeMdBU8zGoIyjpQ+8B0p39mF1xjpWsKmXFCkwdOJkRae0a4x3q1Gslu27kAGtSIKauJDG67SByOarnMpUjQ0fUy8KozYYCuhM+F69RmuGmtntWEkWdg/StHT9UeZ9khPHHWq31OdpxdmdivIzS4qG3fdEpz2qekAY96KKKAEpKdS4pXAZS0UUwCilpKLAJSYpaKAEphHvTzTCKBMaaSlxSUxCUhp1JQA00ynmmmmSMIppFONJQJjSBTDTzmmmgQymsKfTTQBGR7U2nkU2gQlNNOpD0oAjNNNPppoASnAUlOFIZ88qRTgRUYyD0p3PpRc6kh+6jdTcHGaUA+lJsrUkBI5qZTu+tQkEDmpoR69KllIa4I5xTd1WmAZMHsaqlVzgCkgY4SYp6sCeTTPKPVTTeVPINFkNFoxrjlsGnoMDiqgerEcgYcdqllLckNXNLBNzn0WqPWtHS/wDXP/u0jQ7/AEo4sIR7Vrx1k6fhbWIAdAK1Iq1hojgqbsuxmrC1XjqdaozJBS0gp1MBaWkFOApAKKcKaKdQAtOGKQUoFIB1FFITgUFDW6HmuT168KMYTnnkVvahfLaxED5mNcbcTG5nLyDv60DSu7FeCIZ8xhz2zT5JAookkwMCqjOT1NZuR1wikhzNu9qbmmMTRjIFI2SFyPSj6Um3mnYAoRQig85p6n6UhHFOUL9aYh+R9KbuApQopG96BDt+O9OEhz1qLK+lOJBHFAFlJRn0rQgn4BrIB44qeFiOhoFY3gVdMHkGsq5jexk3JnaTwaswz8DtVi4Rbi3KEc9RTTsYzhc3dAvBPagM2WxW5kHtXCaRO1nOAcjkda7eNt6Buxq2c1rEnFGKMUc1ICUUuKOtNgJSUppKaAKKKKAEooooASmkU6mmgBtJRmimSJSUtIaYhKYetOJ4ptAmNpppxNNNAmNNNNONNoENOKSlNJQIYRTaeTTaAGmmkU40hoAYabTjTaAClpKUUAfPZQqcUoxSuTmgVJ2hj2qSNQTTKch5oGh03BwKkhHGagfr1qxbgmolsUtwP3TUQNTyDbGar85pRBomTrxTiwwc80kS8daVhihsdhm0HsKegAFN5pydOvehrQqKJABWppY4kJ9BWXWrpX3X+oqGWd7Z/wCojHX5a1IhWdbriNfpWlF0reOx589y3HVharpVhaZBJSikpRxQwHUopKdihALThTadQAop2KbS54pAh1RyttjLe1DE1k6pd+Wm3n5vSgowNVu2mnZQTj1rO+6KsXLDfz3qq3NKWxrSjqMc5qAjmpH+U9ajIJPNYnXEadvbNO57cU7GQKMUyxvUc5p4FJgmnY4zTAQ4FOGOtNpQeKYrDyeOlMxmnHJHSlCnGTQFiNUwaXHOKmGDS7ATmkrgRqvpUq5DU4KFHSlFMCZHORWlE3y9azFUZ61fh+7QRIZNv83KHHtXXaPd/aLYKTyB61zLoDg1p6S3kzr82Fari9DmqLU6eikByM0uKLGYUUUUwEooooASiijNABSUUlABSGlpDTAYaKM0lAmLTTS000yRDTDTjTDQDCmmlpDQQxhNJmnGmGgBDSUU00CENNpaSgAppNLmmmgBpNNzSmm0ALRTaWkB8/kHNGKP4qWkdtgp8Y5ptTwAbvwpXGiGQfN1qzB8v0NV3PzZq1AAV9KhlRQTn5OKrAc1PNwuM9aiUc4pIb3JkHHvSMOakVcLTW60upQm3ilRfloOABn0p0X+rHFMaFPStfSV3KRz94Vk8CtzRwMR98tUvQrodzBjA9q0YqoQjgcVoRfSt47HnSLUYqwvSoEqdelMkeKcKaKdSAXFOGKbThQA6lpKXigBaXtTadigoZJnFc7qwzKuTwOa6F/umud1dsTKPaga3MCdxu96h7ZqRwd/40xsZqJHRTIiCTnrTSKc0qRjLnFVJ7+GM/fU/jWdje9ix2xmnheOtZ41CBjncB704arbD/lqPpVqIcyNDbwKMY6jNVEvoHPyyVaWVXAx3p8oKaF27iTSbfwp3Q0ox1NPlKvcReRUgGQOaYp9KjeYRE5PSnyicifAU560/clY8+sxxjpj3NUzrxwSFxntT5TOVRI6LetTx4IrkjrbY4XJqSPXGJAxg0nEXtEdaFO7IGRV+FMgCubsNbQyCOVsA9DXUWrgtwQQRxUuLByJTEDHU9phcD0PWnhcpx3qNQVNVFGM2dNbsDCpqaszSZd0DIc5U960qpmYtITRSUIAoopM0ALSUUUgCkoooAKacUuaQkYpiY00lFFAmwNNNKaaTTENJpppTTaBXEzSUpphoJA000UhoASmmlJphNAhKQ0ZpN1ABTTQWppNACGm0hJpM0AOpaZmlyKAPAaWkA5pcVN7nagPtViDOCfaq5zUycKaTGRsSZKuQVTx8/Jq7bHYCfWplsXEjm7YqOMHcBUk55FNj+9Up6DkWeccmoj19Kk/h9aiOd3WpuUK/T8Keg+QZ9Ka/wDSnqPlAp9BoCM8V0GiLnyB/tVz5yGFdLoa5e3HTnNSEnodnB0q/F0FUIRyK0IuldK2PPk9S0lTr0qFKnWhkjhTqSloQCinCmgZ704cUALS5pKKQ0OFOpop2aBkcg4rmdaYJOD6iumkyAa5LxGcSpj8aOoXMbduJJ61FcTeTGW4/GkRupzWfqTu42Ck1dnTDSJjXd5JNIzEnA6YrNcSy8gk+lbsVkCuCmc9atpaoicKBVpIh8zOUNvPtBKNUYQsR611zxgD0qtJArHkA46cVRNn1Mi1STd34rdtnbcvOMVWTYjfdxVuKRexyaTLjc0FlwMGpo23dDUCKGAJ/KrEQwe1TobLRCkFeRVS6QyDI4atP5KqT7BljjimZyZz9xYO7ks36VXk0+UcIM1syTrzVcXkStgnFUmZtXK1toruuZJNp9AKsf2AcErL+lWIb2NjjcpH5VpxSRsARke/WqeocpkrpLxLktk9jW5odxLExikbK54p3lboyQc+lJawsJNxU8VDGtDrImyo96V12nJGc1Hbf6te3FSzPtTI9aRMti9pTfO3HBGa1s1gaVKpu9oPat0dKGjOI6kpM0UFBSUlGaQC5ozTc0ZoAdmm5pKKAFpKKKZIlJQaaTQSwJphNBNNzQAtNoLCmlqBMCaaaN1NJpkgaSkJppYUgA000E0wkUwAmm0hpM0ALmmmikPSgBpNNoNITSAWlP1puaKAPBRThSDNLms76HaB60vRc038alx+7/GlcoaMFutW0yB1qqn3iKuDhaUmUiCQ5PWiPrSTff4NLH9KS2GWONtRDls5qXPHSo1ODSLsEh5JqQH5aY/NBzQJMdkE11Oh4M1t9DXKrXW6GMTxjuFqXuOT91nWwduK0IugrPtxwM1oxcV0LY86W5aSplqFKmWmxDxS0DGKKYCinfSm04UgHUCl7UlDGh4p3SmCnc4pDI5Dwa4vxTLslxntXaPyDmuC8Y5W4U/wntQtwZnwfNEpPU1BMhaQHjin2TBrReeQSM05wM9al3udK2EChVySMVmz34JIhTdg/eqTUZisaxlsBjz9KzI/Nu5CkPyxjv61auO9h8l1IByyrxVR7qUgnzd2KoXKOtxIjsflbGDURRC0YXOSeQa1UGYSrJ7Gh9tJGG/OpYrlkdTnIqtLAsD/ACtlSOlPSMyDK9O9KWhUG2dFaXAZsZ6ir6nPNYlgCCOeVrWV2+tc7dnodijdEzyBR3rLvbosdi9qvOC61juN0rEnvyRWidzOcChPI7sevFRRhmOFUnHJxVucEkhUOyrVoqCJl4BIxk1rFJmDutTOjvoo+NrH1yK07W/trggLIY2+tYRBhkMbnawPWtTR9Ohv70vJOsMKLycdT7VfszL2zOv04TRuA/KN0PrWgYtkuVOFNYCXTWcTwRF3hz+7dhit62uUuYEkLAHHzA+tYtGl7mzbA+WMnpTb4lbZmHYikspwUINOvQZLWRVHOM0hEGiSk6imRweK60GuL0glNSiwCc12QNNmcR+abSZopFC0lJmjNAC0UmaTNBLY6kpM0maAuLmjNNzSE0CAmm5pCaaaAYpNRseaUmmE0EiGk4ophpiHFqaWppphagQ4tTS1ITTCadgHFqYTSE03NFgHbqTdTM0maLASZJ5ppNNzRmiwAabRn3ptSA6ikpc0AeEDmnZFNHvUihcZHWsbHaiPvUwB8vJHWourcirDH/Rkp2KRFH1/GrnQVVj6jNWmPy4/KpaKWxUk5lNSRZqNs7zzU0IoGiYn5aYOTzTiaYDUmgHjtQTg0E8/jSH3piaHJyw+tdjoi/vlxyAtchEAWA967TQ1/etxg7aVtSZfAzpoa0I+1UIPpV+LpW5wstJ0qZagSp1qiR4pabTqAFpwpoxTgRQA6iiikUhwNOzTRS0gGOeDXEeMoDMqbPvLz9a7eQZHSuK8TT/vzFxkAZzQNK5kWcBitEU9TyabMoHOcVcj4UDHQVXnTPTpSTOpLQxrm2e6cEdBSJE1sdyk8dRitaJEximyW5PYVSYNHO6hAt3L5mNjEckDrVNLOOM5BZmHQ10clqrDkVWe3UDAGK052Z+xRkFSxyR9asRR5GFGM9as/ZznpVmKBVPSokzenT1Ftowi49epq4uM8UgVeOKkwAR/KsGjpVloSouRjtVO/sWimEgHyvzWhBjeM9M1qahElzYrtGWXpVxRnN6WOKkRzxiiKFzwD+daTW4HBFPSFehFao57FE6aJvmeNWq3a2HlOB5Y2jtV+OEAYVsCrKpgVbkzJpXIHj80KpUADpSLvhcYP1xVoYUZNRMN8mc8VBSRpWc5DqSetbSkMnTIrCt0+YVv2qZUDtSE1Yz9OkW11eRHQsT90+ldWrbgD61y+o4tL4XA6smBXRxt+7T/AHRQZuKWpPmkzTN1JuoEPz703dTCaM0CZJmgmo91LmgkdupuaQmm7qAsOzQTTc0hNAAWppamk02gTFJpuaQmmk0CaFJphNBamFqZIrEUw0maQmmgFpDSZoNMBCaZmlJ96bQAHNNzSk0wmqQC5ozTM00mkJDi1Jmoy1AbmoYyXNOzUW7NKD70gPDqcKbTsiotodgn8XFWH/1aD2qsDzVhvuqPQVBothIhyKlkJ2EjtTIhzU7INjA9xSbH0KIb5jnmrEXWoMYarEXTrTsJJ3HkDr2pB7U5hxmkHTioLGjrQwOaUdaUrzVD6DoeHXjPNdvogO+TjsK4uD/WJ7mu20QHbIfUip1uKfwHRQdq0I6z4a0Iq3OFllKmWoUqYGncgdS0lLTGOFFJSjHepAeKWm5paAQ4U6minUyhD0rjPGlqfMt7hf4jtau0JyOlYfieISaOxI+64IoRUHqcsoLd+Ka6A0oIFP6iod0daKm0o1KW/CpWHNQNnvVDsQvyetQFQW4oLSFsY4NSMBgY4Ip3KUSMRjPBqZIxSDAHvTg+KTNFoOOF69BQCCfWkI38849KU444qN2XHUmRttaUD7kwOlYxZs+1bOmR+YuD1q0E0ralK6iHmFgO9Qbcdq0Lz5J2jI6VT4JqjDRjVLA8VOJecdajCU9UFFyJQJfvcZzT44gxwaFTkYFWokGRxSJtYuWcHAB5rcgj2xg4rNtFGRWyg+TAqjKT1MXXYy0DMBnan9a1NPmEunwP3K4NU9Vl8lRuGUcbTmqtvq9jp9tHb3M/lsASMqemaQP4LnQbhSbhWdBqtjc/6m7ibPbdVrdQYslL0m6od9JuoFcn30b6g3Ub6BXJi9M3e9RF/emF6Blnf70F/equ+jfQBYLCm7qg30b6BEpNMJpu+mlqAFJppamlqbmgmw7dSbqbmjNMQ8HJoPSmUtUgAmmk0pphqgEJpmc0rUzNNCYH60006o2NHQBhNJmg0zPNZtDJdxpc5qMNS5pAeLUuOM0AdgKU9MVB2oZUx5qMdakJqGNEkR5qzLxGfpVeEZP41au8bcZ6LUPc1itDOHWrUeAKrKDmrCDFU9hLce3ShehprE9BQM4qSmNH3/pTyfamL98+wpxJzQxomt8mZBjPzV3GiACFz3JFcTaZ+0R8d67jRR/o5/3qS3Jq/Cb8Iq/HVCHNX4vetzgZZSpVFQpmphQIfRRRQA6lptOoAWlzTaUUDQ8UtNpRQMdWX4gGdFn9sH9a065zxPrEFtB/Z4IaaYYI9BTKW5zSuGQNnqKdv96rRghcc8VICcc0mdS0JCc0w4PWkyD0NKM574xTsaIi2DpUbDirDYqFgAOTRYtMhLEL0p0TBgWJ6dqikOe/FReb5fB4zQwuaCuDxSjaeorBkvblLpDH8yZ5FbVvIHXcOhpJWG5WLSW6s+fSt/SLf99/sgZNYsBBcY5rUub5tPtFigI82Tlm9BVGM5tkniSBY4oZ1+9u21gRsGz696nv7576JEL5C8596pQE7jmnYmBcWMnvU6xjFNj7VciTccEUi3IhVccVahUkgHpTxAQOnFPC468UGTZetAM4BrZRcLWVZgFvQVrgjFMxkYuv82+z+JiMCvNvGE5j1nywfuxrx+FenX6GXUolxlQleReMZS3iS6/2SF/SmtxydqZSjvXVgQ5GK29P8WahZFQs5ZAfutyK5HcacJD3NUc1z1Sy8dW8pC3UOwn+JTxXTxXEc8SyxOHRuhFeFJMfWuy8I68bWYW0znyZOBn+E0nHsO56Nuo3e9Qb/el3GoGSFqbmmZ96TNA0P3e9BamZpM0CHbjRuqMmjdQBLupC1R7qaWoAkJpuaZuppPvQBJuoyaizTsn1oJJc07OahDU7dVJiHE00tTd5ppNUAE0wkUMaZmqQDyajanUwk/SgkjY802lY02oZQuadk0ynfQ1IHjaHPSgg4zmmqMU7jHWs7ncIOtO9RSDrS4+br3pSAtQEFlHPWpL1sSsT6Uy3+8O1NupA7nHTpWdm2a/ZIEqzH16VVTrVlKbuShX4NC8imyD2pU+5Uoq4oHJpp696cOtNIOaZXQt2OPtSD3rudGUi1H+9XDWI/wBJX0Fd5o+fsa59TQtzOq/dNuHmrsZqjFV2P3rZHGy0lTA1ApqUUEkmaWmg0tADs0UlLSAWlGO9JRQgQ+lpoNL1ouUVNU1GPS9Olu5SMIPlHqa8Wu9VlvdW+1ysSWfP0FdT8QtZ826XTo2/dxDL47mvPi+TW8I2jczctT0FSMfKcipMErxWXpc/n2UTDkgYNaKsRzmsj0E7q4Rx7fxqQDmm79xGKlxTGtxhGRVaUYXJq2elU7g5bbQi7lYDJyB0qpeNzjPNXmdY1+Y4rKnO6TKEHJ7UA5WRnySTGX90+McVoWN3NGdrEEVXlRY5gu4Ekc0ixhHzk9OmapK5yuq7nRR6ktsu5AC5HSs65v7+7nG4lVzz6VTTcykK5zn0q9ETt2Hhh0NXyaC9pfU0LY/LtPUdam24ORWUt46SAPwPUVpxyhhwajU2jJMv2/QVsWoGRnvWLbtk4xWtbEgjmkEmaXlgx1WMTHpVpTmPimYNFjG5YtVCuB0rQySeKyoJiHw4IHrWupAjLHoBmghlOYbHluXbAjXv24rwXVLlrrUJ53bJdyc16n408QCx0IwKf391x9Frx9j3q4LqTUelgzzThz3qMnNA61RkSg4PFWbeYo4Oe9Us09TSA9Y8Mav9vshDK376IYHuK3w2a8j0TUZLO6R1PzKePcV6lb3aXMCTIcqwqZxsUn0LWaTdURajfUFku6k3VFupC1AEu6k3VHuo3CgQ/dSE0zdTS1AiTNITUe6kzQBIGpQaizShqCSajNRhqdTQh2aaTTSRnig5NUAjGkpM0lUmA7NNNFIx4pkkbYpmaVjUeazZRJS0wEUoNIDx4c0Uq8DpQenBrM7gUc0vOaRTzR/FR0C7LScLzVeUnpiplzmo51+bPTipi1ctjYRzVpKqx9e9W1O2lMIiN6Z4pwACHFIx5zRu+SpLQgzkkGkOSaVec/WkxzQDLdjkTD6V3elcWcee4zXDWAJm57DrXeacmy2jX0FNbmdX4TWi61eTtVGE4q+nStTjZOtSiol+tSCgRIDTs0ylFAD6KbmlzQwHZopKKQ0OzUVzOttayzucLGhapO1c542v/sXh2RQcNMQg+nemld2B7Hk+pXTXd5LO7ZZ2JNZxPPWpJmyxFVya6mZGvpGp/YZdj5MT9R6V1u4EAg8MOK87Dc11mj3vn2QUnMkfB5rGcTpoz+yzdRyBxU6niqUUh71aUmoubq45z8prFu7oiTCH8a1rh8RMcVzF1lWJCmmtRylyoSS5JB3c++agjuhGGPLZpDvkQKq1Nb2K/flPHpWiSW5ztykym8jPL5zHr1qVblVOAp56mtPZbY2+UCv0qeO20+UEFQpxxV3SB0pGTDcbJS6gmrjXe/kADI7ir8VnZWzllPOKl2WmT3J9qfMhezkZuQ6CZvvAdKntr8AZYkc9WqYWCSoSGx9Kp3dgyQ742BHXaTUuzEuaJvWtxuYbsfhW7bkYBri9KyZMHOR3rsrU7olrNmqldGrEMp14pjFg2M0QZEWM0uCZfWhGcnqT2q+a3I+tVvFerjRPD0kiNtmf5I60LcqoIxg15V8Rte+36mtpE37mA4GO5ppXdiG9Dm9Rv7m/dZZ5WkbHU1n55p0j4gBqASjNa2sZslxTsECohKKd5wpgLg0oJpBIh60uQemKGgLFvKUkDdK6pvEd5pdlCbcI0bf3hnFceD71q7xc6Q6DlozkChpNWYGkfHmqY+7CP+A03/hPNU64h/75rlCTTCaxcEPmZ0M3jHWJJvMFztA/hUDFWIvHerBAC0LEdyvJrk6aDg0rILs7D/hPdWx/yxz/ALtJ/wAJ7q3rD/3xXJ59qM07RDmZ6L4b8V3uq6n9muRHtKEjaMGuv3cZry3wY2PEEYx1Rq9M3e9TJJbDTCaYxRM/XA6VzFxr96ZmjDKq5/hFb1+wFhOf9iuFacbsBfxrNs78JRjO7aPTvDo+0eC55nbc5lxvPXqKkFjp3R7t92Oxqz8OESfwqVkUMvmtkGuqOk6e3P2SL64q7EudOnNxaOFFtbRyoY7wyneo2Y6813n9m2WObZPyqA6dpcTBhbRhgcjAqZrvP3RVxiYV6kJW5RsulWTKcQIp9apPpFuDzEn4VZaZmPLmozIsoIJzV2Oa5UGio7cRx7ferB0SxVMmPoOeatQMApA6CpS9VYVzn2i0UfL84b05pps9Ob7vmfnW8wTH3F/KoWWM/wAC/lSaA5rUbG3htFnhLctjmsUnmum1/aLBAoA+euWJ5rNlIdmnBveo6UHHapA8lppNG6kzWaO4cnvS/wAVCYo5Le9DAtxZJqO5H61JbtiQA1HcnOPWs9mX0IkxnrVgZ71XTrU4zinLUSF708n5egqIk5604txUWKQ5eh7U3v1/SnR8r9aTvTKbL2nkeaxz0Fd7YEm3iJ6la4GyOHJ/Cu+s+IYx6KKI7mVX4UakP0q8hqjCelXErY5GWFqVaiWpFoESUUmaKVwHA0uabS5oYD6Wmg06kMWvN/iRfK9zb2qPzECWHucV6K7BFZ2OAoyTXhmv3zX+rXFwTkOxx9K1pb3JkZLZY8mojH71IaYTWtyRmw561oaXdNZ3asx+QnDCqG405X5pvVDTs7nexSb2DD7uMirqPnisHRrrzrMLnLx8VpliSMcGuex2xd1cnul3xbR3rn7ojey5wFroATsJIzWUdNLSFsnk5qo6Ey1ZmIly5zCgx05qVbW6Y5djn2rZjtjGgwOlNMwjJ3U+Y0hFLczvs9wOC4P4U5YZxyuPxFXTexgE9cd6gN+T8wxtp7mnNFbsiENyedwB9MVYhgnPDSDj0FWI70ldrKKuW00W4ZAB7GqvZC5olI2Fw8fySMD69KqXGlXSRGTzWdh1UmutjeOUYH6UjWo56EVDkYzakcfZTFZArZVu1drayqYFOfrWNLpyJN5mOSa0rZVZFT0p3TMdUbVtJt56jFSIfMUEHvzUdtCEh56mpIysZIxz2oIuZ/iTWhpGlysjAzsPlHp714teyvNMXcksxyTW74o1aa51+dJDkIdmKwLoYwR0PStYxsrkN3AjfCKh8n3qdD+5qPcabENEHvThBz1pQTTsn1qbgIIBjrThCvrRupd1F2A4RJnv+damlwAyMoJGRg1mA1paZIEuFz3PJqoq7AyrmJoZnjPVTiq/WtzxBb+XcrKo+WQZ/EVhHI7UpRAQ0wHDU401h3rJoB+aM0lL1oA6Dwi2PEVt7hh+hr0nPFeYeFzt8Q2p/wB7+Rr0vdSmVEjvctZTqBklcAVxUtjcxx+Y0ZCepNdrKf3TD2qhfhTpsvGcL1rNx1O7DYh03budj8NpRH4XO7p5zVteJNRurLS2urVh8mMqw4xXJeBbgp4dZd2P3rGumvGW802WBj99CK6NOpy4h3qtnLW/jqZHP2u1RkHUocGup07V4NUs1ubfIUnBB6ivHb2VreVopFIZTg5rqPAuoMIbm35OGDKK1cFa6Od3PRDKPWkWQbgv6VRTewy521YV1QYFITLUl6IWCiJmB9DUbangkGB+KqvO393P40jS4GcE0CFefzQ0gaVM9RVc3p5USSf981IJQexFMkkFFh3K2qzB9PjG4nL9TXP7sGtbU5d9qgGeXrHJrGa1KWw7NPBzUWacDUDPJNwHWl3jHWo91BYVlY7LlhSNtA/1lQI3NSoctSKTLkX3s+1RXPDCp4AMk5xgVXuGV24ORU7lPYYh5qTdUAwDUigdSasSZJnPNOOMcVX4zxUinK1LiNSLEfKCkxzSxL8vXrTGDBqk0ZoWSglsnniu9tP9Wo9q4KwJ3eozXfW2Qq564poyrPQ0ouoq5H0qlF0FXEq0zkRYQ1KKhU1IKYiTPFLUeaXJpASZopoNLmgdh4pwNR5pc56UMDH8V332Hw/cOp+d/kXHvXiszZY16R8Qrp5Ft7SJhgEsw/lXnptDn55AD7DNb04vluZyd2U2pnzO2FBJPYVpR20RYKFLsenNdloWhQwRm6uQoCDceOFrVRJcjkrXQJPJ+037/Zbcc5Iyx/Corm/t44ngsYFSM8F25Zqm8R64+q3hCZW2Q4jT+tYTMcVXKK5s6BcFLqRRyCua65GVkDY5NcZ4ewb1we6V11u4U7T+Fc1T4jto/CWh8qnmmc569O1PABHPem+URyDUmqDdxWZcwyuw2jjvWksbdT0pJF+WhDexjpANpVmwcZ5q3b2QeAAEbXyc1Wu4nL7lPygc4p8TSCOP5iAnatoHJO6ZfaxRFzu5PTmqYZ4nCkZyetDie+fcAVx05rUtLHcA7nJFVKyWooti6cZUdSc1t78rVZYsdBip1QkVg9TZETgknilgjYyDCnnrirSR7vlIq5BAI1oE0RbjtCtn5adbOWlbPIB4pL2YIojUfM/6VHCdidOgOTTuZ8p4xrr7tbvGzn96386WztZtTK20ADTH7oJxmq2puZL+aQ/xOTT9Mu5LO7iniOHjbINdEOxjK6J5rC5sQ0V1C8TjqGFVCOa9ktJLDxrpLxyKsd9GuWUd/cVwmreFZ7CU5XCZ4dRkVr7O+xmqnc5alq3Pp1xCN2zcv94VUwQazcWjS6ClzTaWoGSKatWz7XHOOapjOetTI2Ka3EdPdQi80zcclk5FYMtqoGMVv6JIs0TQtnkYFUruMKWUjleCK6VFNGUpWZzMi4JqNulTz/eNV2PFc01Y1i7oB0604YpgFOBrIZseHMDXrXn+I/yNelZrzDQWxrdof9v+hr0K4lkEbeUuWPA9qUi4K7Jp50SNizDgc80+O2ivrBWLMySj+AVzcq/vBlvmPDZrV0zVWs4ha8GMcg+lQ07aG3Ko63Ok0+xTSrMQQFiudx3GrK3LE4LGshdQ844EoPHrUct+IUJLrntk1w16s4m8VGepZv7WwdWnuoY2CjJZhTbPV9Ksof3EscaDsq1i3eom7heEchxgkGsf7DKUCI8mM/3aKFao1qzOdKKO/g8S2U8nlxXKO5HC9Kurqav3/KvObOxWyuDP87yEdTWk2pS5wqEVrLEzi9NTONBPc7Nr9CeXxn1qT7SjKAJB+FeY63f3U4iVELKuSdtY0V7eQlmxOrdtrGuqlUnON2Yzgoux7E0371SZgFGcgnrTHuFBys6fTIryUa/qkUYP2iUN6MoP9KePE1+77ZBFIuP4krTnmmRyo9JvJi0KrvDHd2NZ+ea5zSJTdTJOIxGcc4zit7fxUt3GTg+lOBqENUi/SkB5KOR96kP1pgzS/hUWOpEijJ5NSqCPeoA2KdvJqGrFIn8w+tJwaiHJ5zThgdc0rIol2j1pwVT3qE8+tAOPWq6BcsLGCehp4iweDUKy4XGTTQ3Pela5VkXciNeTwBUaypK+1TUN03ycetV4WKtxTUbkyq8rsbVqxSaNB0ZhXoNvnaox2rzmyf8A02Et93PNei2/3Vx6VNrE1Hc1Ie1WkqnEc4q2hqkYE6mpQaoz39raLmedE9s81lT+MtOhyI1klPsMU9WJ2OlBpa4ebx3LnEFqg92OazLjxnqUo/1yxD0XFUqcmTdHpZ4GTx9TVebUbO2UtNcxqB715Lc67eTE+dduc+9Z0uoFj8zs3pVqjIXOemX3jiytyVtkaVuxPArmL3xrqd1lUkESHjCiuSN1nkCo2uCxwABWioxRLkzRuLqWZy0jszHuTVVpOeT+tUpJWJ5algQzzpGOSxxW0URc6vw9ZiQi4bkscIK2PGN//Z2iR2ULYkn+9j0qbRYAjqAPkiFcV4q1I3+tSnOUjOxfwrSSVyIu5iMeaaxOOtJnNIazZqkaegNt1Ic4ytdaN2etcXpLlNQj9ziuwUk89q5Z/EehhtYl1J+OeoqzHKHGOBWX347VEbpoHBxn1NQXJcpvKVA61FNk/T1qrBeFtoJ5foMVYKNkcnjmixDkkQJB5jkDNKtuWlCHAUVdXdGm3I3sKjgAJbJwV61SIbTH29qASeMVowxKOByDUCgIOuc1eRfLhbjJ61T1JukKPLAo3qG4NHyugZQCGpgQBCentU2DmRZiXLBjyB6VO90qrgVkJPJJK6rlcdx3pxJA5NIUXzEztmTcTk1DdXIitJmzjahOaiabBrK1678rS3VeTIQtBpy6HnF2Nzs3qagi4artwgywqiOH4rem9TlnE6fStXn0m7t7yA4ePqP7w7ivV5Xt9Z0uPULcBopV+dfSvEkb9yK7DwL4k/s+8/s25b/Rbg/KT0Vv/r12RZzSjoGrwPpU2WXdbv8AdbHT2rNa0sb8ZIwx/iU4NeiazpkUkckEo3Qyjg/3TXld/aS6VfPBJncv3WHcVrKKkrmcJO9mFzoM0fMDiVfToazWgkjbbIjIfcVoxalLGB82fY1pRatDKmy4hDKe+M1zuknsbczRzODSqea6aTStOvRut5BEx7Dp+VZs+g3kB+RPNX1X/CodJopTTJtEnMV2mPp1rU16AwXb+jAMK5+13QXK71ZWB6EV1/iXEunWdyMcoVP1raCstTKpucBccOaq9WzVi4OTmq+K5aj1No7BS0lFYlGjoxxq1qR2evR8815ppZxqdsf9uvQ9/tSY0LLBFKAWUGmLbQx4wi/U04v6d6jZ+M1NzRPucvqerXcd1JFEwiVCR8o5NY7XdwzZaV2PqTW3qGkXFxdSTRsuGOcZqkfD9/1VEI/3qfs4yVyXN3I7LU7uCZdkpA+ma2l1u/Iz5/H+6KzY/Derb/ktS2OeGFRmZo/ldCGXgirhSjbYlyZs/wBt35x++6dflFO/tu9/56j/AL5FYn2oehpftYPatfZQfQnmZtf2zef31/75FH9s3mc5Q/8AAaxvtS46Gk+2IOtUqcQubH9sXTEFhGSD3Snf2vN3hg/74rH+1pjp1oN4mSBz+NDjEVzci1uePhYogPYVL/btwB/q4vyrnftnGVA/OnLdr1kBOOwNHJENTof7fnwP3Uf61ZTWL9yPLsgQfQGsW01i1tDuFqGb1Zq0v+Ex4A+yr/31UtRWyFqcIMntS84pTwetH41yM7U7jeacM+lLThSY0AJHanBhjkHNFKGpaFBvFT20TXdwkMWC7nAzUPX3p6vtwRwR0IofkM2pvC2owruKxn6OKy5LaS3lCSrhqQ3c56zSH6sajLs7AliT6k1KcuoDZwWAxUcXDjNLc8beaZABurVbHPN++bNgC90kYwN3GTXoNsCkSDPQYrgdI51OA+ldbd6tHZRYUhnPT0FQ029C5uyNyW7htIjJNIFUdvWuZ1HxbK+5Lb91GP4u5rm7/VpLmQsX3N61mM7SNuY5rohRtrI5ZT6I0LnU3lcli7nPUmqrXsp4XAqE4x1pm4V0JIm48yu3VzTOp5NNLUmQaAuOPtSE4pmaTNFhClqTNIcU3IosAMcmtXw/CJNVjY9EBb9KyG5NdD4WTM1w+OkePzNVDcTeh2JuVsNFnuSQDtOP6V5dI5dixOSTmu08YXixadb2aH7/ACR7CuGJ5qpsUEPBpGpF60pzWRoPtn2XEbZ6NXcQENGDnrXBqcMD6V2tg4e3Q57Vz1Nztwr3RdwPWka3WRMZ/Sjv1qVXHes7nXJXQWcIhYFuWzxWvFAfNwe45qggHfmrKzkHcDyKqNmcFROJakVRMG9BUKxoCzEfe/WhWEjfMQOeTVlhE0bFDnZ2rSxipMWCElueAMYrTlGxMHv6VmxuWAHAHfNXcr5W0vnA4PrTSG5MswWO21dlbgkEZqp5oeXgDauR9aVZphCsZbA9KaoCjtSk0hRTYFV5KgCqkz4NTSS7VyTis+abe2BWR1QjYiuHIPB6dTWBrM3nKEz0Oa2bmQRxnjPFc7M4lkY44oN7KxiXQ2sfpWa3D1s36cjjHFY7jD1pDc46qsW4zmOm7yrgg4IOQfSmo2EppIrrRzM9j8KayviHw/5U5zcwfK+e/oa5/wAU6cbqyeXb+/tufqtct4U1ltI1uOQn9zL8kg9q9I1MJ9qRiAY5htb3BreEjnqRtqeRZwaekmOlTajamz1Ce2PWNytVKh3TNU7otpcOpGDV+DVpofuuce/NZCmng8U0xcp066va3AAuYVPvitKSey1HTDZpOFYHKk9q4kPinCQ+pq9CeUmv/D2oQ5ZIxKueChz+lYzxvE211KkdiK3rfUbmDGyZgPQ81d/tWG6AW9s0lH97oaxnST2KjNrRnIn6UoFdPLoumXuWs7gwOf4HGRWHfabc6e+2ZMA9GByDXLKlJGqmmJp5xfwf74rvg2O9eeWjhbqIscAMM12yXUUjYBPPQkdaydyi2ZPemb+KjL8cHNML46GlYu5KMZJqaIgHFU1clhirKN0qiDb02dYpwzdNpBrL1rwndapcm70yAyl/vovY1JBLgiuz8IXapdMhNVDcTdjzM+A/EY/5hU35U3/hBvEQ66Tcf9819DtqdrDKsMlzGkjdFLcmnT6jbW7qk1xHGzdAzda3TI5mfOh8EeIgf+QTcf8AfNQt4J8QZydLucf7lfSc1/BbBfOnSPd0ycZqUTAgENkEZBo0E5M+aP8AhDdeBx/ZdyMf7FNbwhrgU5025H/AK+mfN96TzBTSXYXOfMX/AAiuuJnOm3P/AH7obwzrI66Zc4/3K+nfMHt+VVrnUYLYqJWALdAFyaNOwc7Pmj/hHdWA5064/wC+DTToOqjrp9xj/cNfS9tqNvdgmIjI6grU5dO4U/hTshe0Z8iEnvRu9qTnrxRXAzvTHBjmnA03n0pR9KVih+aKTn0oFKxSY8GnZBpn4UtMEOIpyfeWoqki++KVh3GXR+YD2otUMjgAVZ+xtcS8nC8ZNWcR2w2RrmtYQbRy1JWlclib7Kd38Y6VUubp5T8zE0krEcn7xqmzEnmuiFNIylNyY/dmlFNB496TdzVtWJJGPFQk1Ix+WoieeBQtAFzRmm0UbgKabS0000AhNIPekzRSYwNdd4QhJtrlsdWUfzrkM89K73wWoOnSdOZeaqC1InojnvFdx5usyJ/DGoUD8K589a1dfbfrV2f+mhrJNKoOGw4Gn1FmnisrmiDvXS6PcboVQnkVzWea0tMm8uQc45rOojehLlkdcjcdaXdzkVWiclQRip1fHBrFnoluNulWFOR0qjGwq3GxxxSTMpxuMlD7hgcVMvnEbUyMnmlDDOKswy5OABVp2OeUB8UEhwzdavhDtxTEYcZqRpMDqKbZHIh3Cjmo5HC8k1A8/wA3WoJpweKkqMRk0xZ8ZwBVOWQq+AKHlBY81E5UAkmg6IqxWvJf3WB396ycEN7VeuH3nGMfjVNgSelNDkZ9+o2qR19KxZPv1tXpy3PGBWLIf3lXDc46rHL0phNPJAWos11MwHA4PpXo+k6mdU0CMuf3sPyt+HSvN66LwfO/9pPZjnz14HuKqLM5xui14whxqyzYx58Kv+OMGub5rqvGzBdUhtxz5MKqf51ytaSVzOnsKPrT1NNApQPWkkaD804HAqInmnA0wJFIqVSagp6kdKpCZajJyMGtGC6cxGGZVkiPBUispWwauW7fNV2T3M2iO60cB/OtFBjxyO4otrhkdI2Pyg8NW5aAAhlOD6VLe6Z9qCzxYBQfMuOtc1egkro0pzu+Uqb8gHtSFs1OtvAbclUKSDvmlutMuLa0juWKlHGeO1cFzrcHHcpmdIhvY8Cqx16MNiOEsPUtWdqkhG1N3B54rNU+9bU4825nNWOoi145/wBV+Ga6fw14jt479PPzFu4yTxXm0b1pxZKgjiuulQiznnJnoGuajv8AGMJV8rvj5B+lTeM74t4htlDnaoX+dc94Z0canefapbpgYHVtuM7q6rWPD8erajDdvcmPy8ZULnOPeicVBpMmLvqJ42vz51moOB5Z/mK7mxvP9CgBP/LMfyridb0JdZkgY3Bj8sYPy5yKv6hrlvoOmrLPudFwgA6ms01Yb1OuN3j+IUn273rzJvifpZ/5YzdfSj/hZmlnH7u4x9KLisz0/wC2A/xc1iaq0jXqTKpdSNvHauO/4WbpAwGE3P8As0//AIWZo3Qif/vg0tR8rOz053E+8LtQDHPetcTZrzhfiZoYP35QP9w1MvxN0Dp5s3/fs1cWTys8Uoz7U7ilwPSvPPSQ0E04UfL3NPVV9aWhSQ3NLmnlBSeXx1qbjAEUvvSeWaXYaQxwwangjJYMR8oqKKPJyegqVpuCF/CtoQ5jKdRRJ5bjHyoOelRgEKWY81HF13HmllfjGa64RsjkbbZFI2Sc1AetKx5pmaoEPGcdaQ9aBSNU9QHdRTTQpytIR70wFpMUY96PxoSADTT6elBPvSGgBKTvSmkHWpATvXf+B/8AjwnHpKMflXA967DwTd+XcTWzH/WAMo9xVxFPY57XcnWbvP8Az1b+dZeOa1teGNZu/wDrq386yiKKgQ2Gng04ZptOWsDQdU8J2sKhxVlI9yA0pK5UW7m7Y3OUAJrRX5u9c7buYz1rctZQR68Vg0ejTndal1DjrVlH44qjuPXPFSByp5osablt3NLHcMjcGoA/mCmkFXosZOJsJeDrkU57pW4DdfSsfYT3qwmVUZoJ5UWzIc9e9RuxNM+btRsIOf5mguMRjLznvVeVs8HpVxgM1VkTJzmgq6RQcFjxTSnFWGiLNgdqUwHp1q7Iyb1OdveHY5rFOS+fetjU2wW55rGU81cNzkqvUc3A6Uwc096jBxWzZkh6qWYKoJYnAA71674D8G/2ci392N13Ivyr/wA8x/jXktozfa4SDht459K+kGuLbRvDD6jM6lhF8vua6aHLa7ObEOWkUeH+KZfO8Q3r5yPNIH0HFYgqxeSme4kkY5LtuNVqc9GVBWQ8CjvSDFLxU3LCnD8aTPFKOe9IBwp209aQfWn8AcmqQhV+tTxuarryepxUyqc1oiGbNnPtGM84rbsroAgg8jqPWuVjYoR7VqQykAMK05eZWMWranRNa2S5nnmxG/3VxVtWtb7TfswcFRwDXNT4vbbyHbaTyh9DWENSu7HNqcq6tzu5ry8Th3HWJ3UK/N8Rs63YLLaS26AB4+VIA5riWRlOGUg+9dBeavJIufOV2I+6EwBWbLfzXB/foj4HAxWFGUlua1LPYpoSK0reVRFzWVsbPFKDKoxXoU6vK7nNOFz0LwbPte5bnbgD8a683nHU15BY6rfWKFbeUICcnitE+KdVZQPOUY7hRUVZ80rkwhY9NN1x3rnvF0pm0gIo3AOCQa5IeJ9VBybgH6qKr3mt6hfQmKaVSp9BWN2aKNim0G77kK/99UC2cDmIf99CoEMkbDDcVpR6nHGgVrVX9zVIGrFI2r/88h/31U0WmXE/CW+T6lgKsnVYe9ilIdUiP/Ltj6GmkAf8I9ejkwp7YkFNOg3w6wL/AN9inf2pERnyGx/vU7+04sYEDfXdVoRjeXjvSqDUO4+tOBPrXnI7USMoPNAT0pm6gORQx3H7Go2tSCRqXzGosMUbhUiKzMB3NMV8mr0YCJuIw1VGN9CZz5UQyfINuaiFJI5ZzQtdcI8uiONtt3Jgdqe9RMwY0OcCogea0RIHpTCOal4qM4oBBTGpw5Pamt0pMYqYIpTTU+7SkYPegABxQT2oo6jpTACOOtN7U7HHWjH0oYDDSDrTjRikxoQ1e0rUP7O1KC6xkRtlh6jvVIjiomPNTsDNXXrm3vNYuLi2J8qRtwzxWZxSA5pau90EVYYcUDrSn6UAVnbUoeOlX7QgoRwcGqANXLJgJcHuMUNaDT1LDR4ORwKuWkpUY/OmsoZMdKZGNj1zyR0x0NiNs89jVlRkfSs6CTHFaMWSM1B1RldEkaHPHFSbcHnmhARTmGBmmTIVWAPSpEYH3qqxPYHFKhOaBF9PwpSMckU2Ne+akbAHXikURHOOKiZCRzUxwccYFAUs3Q4ppEsqGPAPalZNsDSE4wM1aMQA9WqhqUhjtnB47fWrukTY4/U2DSHGPWs1BzVu8O52PqarJitobHHN+8wfrxUeKkI5pNhxVtECRsqtzXS6l4ov9U0+2s5pcQwKFCjvXMFcGrKD5RV05W0Ikr6jyeaDSU7tWj1EIKdim96eCMUAL9aSlAo5oEOQZ705sDgGiMd6RvWrSExUYg1ZUHOaqKeauJjiqRLHE4PWtTT8yQsPSsiRhmtLR2zIwHUit4voQ9iSZiBkfLt6UXdqNTsDcIv+kxDkD+IU64QgsKbp1wbe4BJwCeaVSCkrEptO6OdI9aMZre17TVRxe24zDJy2P4TWDg15U4crOyEuZXExSbRmnDOKWlcoaBilxS0UhWExS4FFFMYYpdvFHFO9/agBAF70bRnrTgR6ClAX61QrCADpS7R9KXFGMDqKpMVjMFOGaZhh2pcnuK4bHWnYkGfSnAH0qNWyakG48AUD0GkGlApGJBwQc06EGR9oFANli3hydxHSpZnIXb1p3CgIo6VBIcnFddKCSuclSXMyEnJzTl5PSlwPShc4PFaWMxkp5xmoe9Oc5NIAfSmCHjkUjfSkBpT7CmAymtxTjTWOR0pDHIMrTu1Nj+7S0JAJntSg0hNA+lMB3am80tAzmkAmD6UY5pefpQBzQMVh8tV3XmrJqJhntSsFyEZBpwOaUr3pMYpbDClA5pM8dKcPakAVLE20g+nNRU9aaA6CBfNhEg70xkIfpTtJcNE6MRgdKtSRYI6/lXPNWdjrh7yuRoNoyK0LVyeKrLHhBVm3XHNRY2iaaAbRxmkZcnmmxnGCal4I5zikNogZacir6GpeMYqN8g8GgSLKngUNkkURtxyakA3GgYkcRY4xUyx7Pc1Ii4GFpxU554pkNsgdcDOK53X5RHEFzya6KVmxiuE1+8FxqJRD8icU4rmYpSUY3MafPGe9MRSBTpfvd6aK6krHEwI5pW4HFL+Bo2k1QhiqWNTAUirin4x2qoolsMUoUetHJ7U7AxWiEMI9DQB70pFJgikBKOlIRz0oU5p1NK5DHoCFJqJs+tTHhOhqBiTVvRAOTk1bXO3j86qRjHbNWl4FVBCYxslsGr+lZ+1Dms5utXtMP+lJ65rSL1Ilsa9yuJDgdapiP5s9KuXZxKMUNFlN3tmtbGSHWToxa1uDmOYYwfWuev7F7C8aB+QPun1FTTTnzt6kgjpz0rTvMappHnAZmg647iuXEU7q6NoScWc4eBSUtNrzrHWLS0lFAC0cn60e1GaAFA96XBoGKCfQUwFwMUoHHTmmhiKdvx+FNXEOAOOakjt5J38uJdx6+lQFie1OhmkhlDIxVhSeg42vqUOtOKDaeaaetL/DXMdA0cHipUOOahHXrUinikxpjuWbNXI4xFFuI5NRQr3OOKmYkjFa0ablqzGrPohmTjNVnJzUzcKarsea6rWOcevShjtWlTGM0yRu1AELHJ705QSKaTzT16U9UA3oetL+NNYcU5T8tMBrVGT71KenSomPNDGSJ90U7vQvSihXEIaMVPDbSzn5F4HU1qweH5HALTKM+1XGnKWwnOMdzEowc106eF1YfNcH/vmpV8MW4I3SMc9a1WFmzJ14HJ05FJOBzXVnw/Zx87XYe5qS00+CO4/doAPpT+qSvqJ4iKWhyRikP8LflUbIV6givSBEgTG0flWB4jjUQoQoHNaPCWjczhi1J2OTwaNvHNP70cVxNHYmQlTSqPSnkUq9elTYq5FIhHNOgOW2npUj/dNVskNkVL0JTN/S2Edyo7Nwa6JYtwz1rlLOX5QR94V01lcGVAMc9zU1I31R00H0HtCATnODTok2tir6ruiwB1pogJOQKwSZ1pjogCMccetPK54FJGpB65q3HGpOeQRSsNlJo2QZyaTYXI6ir7ICOajWMAnkCnYLkAjO70q5Eg/GmeWCwxg1OgOeeKLCkyYAAUx/rT/MGOBUbyrtJJwB1osZGPr98LKxJ3fvX4UV58hLSFic1ra/qR1C+OMeXHwtZKna2K3pwsrnPVnd2GTD56aqlulWyvmDAHQUxV21vymHMQ7WHelApzEk0goSFccPenUAcUoGK1S0FcO1FFKKLCuIenFMOal6daYcZ5pNBcFqUAY96iBqVTmnETFc/Liq5PNSzt0FQDrRLUEWYqnqGPgdKeWHrVxvYTBqt6bzdKKpmrmnf8fC1rT3Jlsac5ZpOuCOKtwMJIVz1bg1RlYlvrTba5ZrlUX7oOBWxiZt3H5Vw6eh4rU0GcLcCJ+Vfg0zXIDHejgYZQaXSI90vyj5hyKzcbl8y5TJ1C3NrfTQ9lbj6VUyTXReJ4R9phnCkeYpBPuK5+vMqxtI6qcrxEzRzS49KO9ZGgZopQPWlpgJilGad04xS0AN2n1pdhNLgZ6U8nFNMQgGB1q3a3McAw0YOT1zVIsc5x1pYQskqiRiFzSnsaU5OMrooEU44Ef400U5z8grmNSJevNWYV3HFVl69K0bSMKhkY/QYoSbdhNpK4rgLhQaTPHWk3Avk+tD9M5rugrI5JO7I5WzUGeae5qLvTJJV+pqN+ven9BmoyeaEMYce9PU8Uzj6U9cYoe4xGpFPOKVqb0NU0CHMeMVF1bFSnG3NRry9SBN0FIOtHbFHSrixF1Jnjt0CErzzj6111g2YkJ6kVxoGbcdTz0rrrD/AFCfSu+g9Djr7GsACtNNVr6UxWTuHK+9crJfgkh5pmH1rrc4x3OOMJSZ1M0iKpyw/Oks13NuB4rlois0qKgbLHuc12FpGI4lHtUqak9CpLlRYIFc74nK+RGM85rozjHWuH128+03rhfurwKdSSjG4qEbzMjvRiijpxXjy1Z6yEI/GgdaWm9DUsoc54qsw54qw3IqEis2BdtmXGR1NaNtfNBKBk7T1rFtWxKFzwauN1zWkEnGzGpOLujurOdZYg0bAir2wlcg1xWk6k1nIFY5jPUeldrbyCVFKHKkZBFYTi4s7IVFJXI9hBztqwg9M0pXA4GaWNM+ue9ZNGnMLjPfHsaQoc84qQpz0Jz60pjpBcYkdSH0pv3RyTUU9zHDGXd8AdSaCWSSNhDXM65rCJC1vE2ZG4JHaqWqeI3uGaO2ysfTd3Nc875OCa1hTb1ZjOpZWQ1yOaaAetKQM0ighsdRmug5izDwwOKJk2k46Gli45qaba0Bbpit1sZvczieaetJ1OaUVFh3FpaCeKQVSELSik/CloAcBTTT1xQwFMREKlSmECpENJDIZ8bqZHyadO3zelJGKnqBZXjvSEjPNA6UhrXoSLnnGav2XEoPpVBfvCrtudvStKe4pbF26cLHkfSqVrcbJlqW6ztHtVBeG5NW5amaR0mvruitpQPvLiqWiTiLUYwejHBrQuiLrw1C/VkIBrEtiUuUI4INUiV8Njo/FluF0uJwPuy/zriz3r0LxCgn8LGQfwlT+teenGTXnYltSOnDu8QBpy4603jFGa5ToFyOwoz3pM0vGeaYC5z0pfxOKTilJG3GaYhaBg03cAOKTJJ4oQD9w6ZpueaTHvTh05qtwKQ605/uAU0DmnMpJGMk9q4kjq2Q63iLv0471fkbZHj8BViXTW0+yheXAdxlhnpVKQ56muqlBLVnJOd9ERZOelK2QKSkY8VsQQtnNIPzpxHFNpgOPC1Gc08nIphpAMNPXpTKlXpQ9xjWPFMNSGmN6U0CE6ikQZpKfH9aWjGPx70DrRR1qkIvW08KRbZAcg8YGalF2FcbbmUL9KzKM1vGs4qxlKnc0by+eUbFkYp7nrVONd74pnWrFuMc9+gpObmyVFRWhtaJa+ZMzn+HpXVIoC9s1n6RbeVaJnqa0iMV6FKNkedWldmfq94LWybB+ZuBXCyksxPrW74iuvNuhEDwgxgetYLdawxNT7J04WFldkZoApTRiuFnaGDTCOal28elMapGgPK1ERUwHFIy0WGV+VYEdjWjHIs0QIqkyZFMjkaJ80k+Vg0aIGG6VtaPrDWcmyQkxH9KxI5o5F6gH0qTA6g1q1GSFGTi7npUMyTxrIh3KehFTDrXDaPrMlg/luxMJPI9K7O3uUmjDDv3zXHKDi9TshUUldFjbxnPNR7iD6U7eAM9qhlk3ZwKhmlyO6n8qFm3AAcmuE1LVJb6UrnEKngDvWlr+pcm2jJ/2jXNM2OBWlOF9TGrUtogZuwoC4570IOMmngZ6V0I5bjcc5pyr6CnHavLGmBz/DwK0SJbJ12Ac9e9RSylht7elJ2+tM78U2yRoBzT+lAApQKBiUmaesZdtqqWPoBmr0Wh6nOu6OxmYHvtxQ2kBn5pcGtX/hG9XUf8eMv6VVmsLu1OJ7aWLP8AfXFJNMVysvA6U4g4zigD0IqULla0QmV8GnqOKUgZpQOKSQyrOP3mKfGMCmzD97Tx8oqOoyXnFMpUYH3pH+9xWlyRyCrcRwMmqqYqWM7mA7Crg9RMt3BP2bdnvVAdRitCY/6MBWeD81XJiR1Nh++8Nzoe3NYA+SQc8A810GjHdo9ynqKxHX52HTmtEYrqdpMBceCrgAEsEz+tecHk16tocHneGpEPJYEV5dLEUkYEYwSK4MUtTbDPcixRilIxSHgVx6HYGDQaUDjrijFMBM8UlLinKmRz3ouA3BpR6VJt+bAp23B/nTAZgdKdjA6Uh9AaQ800IrwwPIc4wB3rTs4kFzGPL3YOaYWCjAXHpTrKUpfRHcR82KUKNtWOU2yfWluRdL9oBwwyvpWU5Oa6fxEuYLWUk85AB/CuWc8+1b2VtDCIZppzS8YzTSaRQhBpuOafjimUgFOMVHTiRSHpTQEZ608dKZjmnLx3pNalD+1Rtmnk0w0yWRZ5qZOlQHrU69Khbj6DvpRz3pBj1pc1YMMUoGaTinCqsIeiEtgVq2Ft592kY5VOTVC3Tqx7V0mg2pVDO/Vj3rejC7OetKyOghXaowDikuZRDC8h/hXNOU4FYviG7EVuIR9569Je7HU8uzlKxzFzIZZmkPUnNVmB7CpSc0w8d68uq+aVz1YKysMP0FIKU80maxsaIUk0xvpT+o60xs0DQJyKk7dKijxmpTQtyhOD6VFJCG5HWpRz2p2PSr5UxXKJRlqzbTlRhjT2QEdKrSREHiocWth3uaQdXPBGa3/D+pmKf7NM3ysPlJPSuLV2Rsg4NaENzv74YUpPmVmVBuLuj0svxwRWRrOpi1h2qfnbsDWZZ+IAlmUmz5qjj3rDu7uS6mLsTyeKwUHzHS5pK5DNKzuWY5YnmmIpY8inKnc9afuVPrXQo2RyybbFC45PAphl5+UUxmLHmjHvVpIi4nLNzzUijFNXGak6VSQMRs0wUp5OBW5p2mW9vaf2lqf+r/5ZQ93NTOSSuxFSx0a8vgHRNkP/AD0fgVpR2eiacym6le8lH8CcLVK/1e4vmwzeVCOFjXoBUVgIZr+CK5YrCzYYiseac9tCnFJXka58QSIRHpllFbg8AKuTUyyeJL4th5QB97GFxWpcppGiHfE6h8Z2feNV08RzEypZWjESfxMOlWqK3epDq22RWNjrccUkxvGAQZ4kzmq9t4i1WFV3sJ4zxiVOtd1o+kvqGjpcTOFmkB+Urx+VZ1xLaQSLa6rbiHySQhC/Kwp+xiL2r6o543OiaugjurX7FP2kToao6l4eubBfOiIuLXqJE7fWul1nw3ZTW32uykEQK5EarkN9OeKwLe+1HQbjy5Y32H70Mg7UWnDValWjLYwCtAFdXf6Pa6tZnUdHADDmW37j6VyzoyE5BHrW8ZKSuidim/Mx+tEhwvpSuf3xpk54zUuyKFiNSd6rwtU5OBSTuDQ7dgYqWEnIqsp55qzB9/61pDcTWhcuCRCq1RUc9asXLZKgdAKrqMtz0rRvUlHVaGcaXcDr+FZTLmc/71a2kMFsHTrx2qiseZwMdWrVHO+p6PoMITS4VAxnJxXmmsQ+VqcsZHAkbj8a9W0zEMVuuOAtea+IEI8Q3K4/5aZH5152PTSujfAq82jY07wdp+qW0ElwZkZh/A2K0z8ONGAyJLnH+/W1oaYtLfjGFNa56V4vtZLqem6aOKPgPSY2VgJm56O/FM1fwtpOmsubTcJB13Hiuz2hpUX1arGtaNJepbhIyyrkkCh1Z20FGCvqeYxeHtIlcALIOeRv4rqYvB2iCMA2QbjqWNKfDW25QiRkAP3WU810W0Lx2qadapbVl1qUVsc6/gzRH/5cwOOzGs678DaYsZaNZRj0auzqO44if6Vqqsl1MOQ8M121/snWLi0VTtTBUnqQRWYbnj7tdN8QQR4lJPeFOlcmQBzXVGTauJqx/9k="",
    //                " + "\n" +
    //                                @"      ""AdditionalData"": null
    //                " + "\n" +
    //                                @"    }
    //                " + "\n" +
    //                                @"  ],
    //                " + "\n" +
    //                                @"  ""OperationOrder"": 1,
    //                " + "\n" +
    //                                @"  ""MatchingType"": 0,
    //                " + "\n" +
    //                                @"  ""Origin"": 1,
    //                " + "\n" +
    //                                @"  ""Threshold"": 0.9,
    //                " + "\n" +
    //                                @"  ""SaveAudit"": 0,
    //                " + "\n" +
    //                                @"  ""EnrollOption"": 0,
    //                " + "\n" +
    //                                @"  ""ConnectorId"": null,
    //                " + "\n" +
    //                                @"  ""ClientId"": ""testclientid"",
    //                " + "\n" +
    //                                @"  ""IpEndUser"": null
    //                " + "\n" +
    //@"}";

}
