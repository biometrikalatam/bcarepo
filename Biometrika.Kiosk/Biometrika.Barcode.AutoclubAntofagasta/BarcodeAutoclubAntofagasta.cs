﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Barcode.AutoclubAntofagasta
{
    /// <summary>
    /// Implementa IBarcode para AUtoclub, aunque por ahora no lo estamos usando, este proceso lo
    /// resolvemos directo con Bio.Core.SerialComm
    /// </summary>
    public class BarcodeAutoclubAntofagasta : IBarcode
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BarcodeAutoclubAntofagasta));

        bool _Initialized;
        string _Name;

        public bool Initialized
        {
            get {
                return this._Initialized;
            }

            set {
                this._Initialized = value;
            }
        }

        public string Name
        {
            get {
                return this._Name;
            }

            set {
                this._Name = value;
            }
        }

        public int Intialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("BarcodeCedulaChilena.Intialize IN...");
                /*
                    if (!System.IO.File.Exists("Biometrika.Action.AutoclubAntofagasta.cfg"))
                    {
                        Config = new DynamicData();
                        Config.AddItem("COMPort", "COM8"); //No es necesaria en realidad, se autoconfigura
                        Config.AddItem("ArduinoType", 2); //Cantidad de rele en la placa
                        Config.AddItem("ArduinoOpenDelay", "30000");
                        //_Parameters.Add("", "");
                        if (!SerializeHelper.SerializeToFile(Config, "Biometrika.Action.AutoclubAntofagasta.cfg"))
                        {
                            LOG.Warn("ActionAutoclubAntofagasta.Initialize - No grabo condif en disco (Biometrika.Action.AutoclubAntofagasta.cfg)");
                        }
                    }
                    else
                    {
                        Config = SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.Action.AutoclubAntofagasta.cfg");
                        Config.Initialize();
                    }

                    if (Config == null)
                    {
                        LOG.Fatal("ActionAutoclubAntofagasta.Initialize - Error leyendo ActionAutoclubAntofagasta.cfg!");
                        return Errors.IERR_DESERIALIZING_DATA;
                    }
                    else //Si hay Config => Configuro Placa
                    {
                        _ARDUINO_HELPER = new ArduinoHelper((string)Config.DynamicDataItems["ArduinoOpenDelay"]);
                        _ARDUINO_HELPER.Initialize();
                    }
                */
                _Initialized = true;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BarcodeCedulaChilena.Initialize - Excp Error: " + ex.Message);
            }
            return ret;
        }

        public int Parse(DynamicData parameters, out DynamicData returns, out string msg)
        {
            int ret = Errors.IERR_OK;
            returns = null;
            msg = null;
            try
            {
                LOG.Debug("BarcodeAutoclubAntofagasta.Parse IN...");
                //object o = (parameters != null) ? parameters.ParamsList["key"] : null;
                if (parameters == null)
                    LOG.Fatal("BarcodeAutoclubAntofagasta.Parse - Parametros NULO!");
                else
                {
                    if (parameters.DynamicDataItems.ContainsKey("BarcodeToParse"))
                    {
                        string strBarcodeToParse = (string)parameters.DynamicDataItems["BarcodeToParse"];
                        if (string.IsNullOrEmpty(strBarcodeToParse))
                        {
                            ret = Errors.IERR_BAD_PARAMETER;
                            msg = "Llego barcode nulo para interpretar!";
                        }
                        else
                        {
                            string rut;
                            if (strBarcodeToParse.StartsWith("0"))
                            {
                                rut = strBarcodeToParse.Substring(1);
                            } else
                            {
                                rut = strBarcodeToParse;
                            }
                            LOG.Debug("BarcodeAutoclubAntofagasta.Parse - Rut recognized = " + rut);
                            returns = new DynamicData();
                            returns.AddItem("IdRecognized", FormattingTaxid(rut));
                            
                        }
                    }
                    else
                    {
                        ret = Errors.IERR_BAD_PARAMETER;
                        msg = "No llego barcode para interpretar!";
                    }
                    //foreach (DynamicDataItem item in parameters.ListDynamicDataItems)
                    //{
                    //    LOG.Debug("ActionAutoclubAntofagasta.DoAction - key=" + item.key + " => value = " + (item.value.ToString()));
                    //}
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msg = "BarcodeAutoclubAntofagasta.Parse  Excp [" + ex.Message + "]";
                LOG.Error("BarcodeAutoclubAntofagasta.Parse  - Excp Error: " + ex.Message);
            }
            LOG.Debug("BarcodeAutoclubAntofagasta.Parse OUT! ret = " + ret.ToString());
            return ret;
        }

        private String FormattingTaxid(string rut)
        {
            int cont = 0;
            String format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {

                    format = rut.Substring(i, 1) + format;

                    cont++;

                }
                return format;
            }
        }
    }
}
