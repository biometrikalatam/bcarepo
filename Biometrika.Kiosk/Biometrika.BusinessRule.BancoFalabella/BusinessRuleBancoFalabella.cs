﻿using BioCore.SerialComm.Cedula;
using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BusinessRule.BancoFalabella
{
    /// <summary>
    /// Implementacion de BusinessRule para Autoclub Antofagasta.  
    /// Lo realiza conecgtandose a la API de WAIS. 
    /// Usa URL: 
    ///   http://jano.biometrika.cl/AutoClub_Test/API/api/AutoClubRules/CanAccess/RUT/21284415-2/{{registerAccess}}/{{isExit}}/{{token}}
    /// Tambien realiza login y lo deja en el Config para poder trabajar. El token del login no vence, asi que solo 
    /// deberia hacerlo una vez, pero si se elimina, lo realiza de nuevo y lo deja grabado en el config.
    /// </summary>
    public class BusinessRuleBancoFalabella : IBusinessRule
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BusinessRuleBancoFalabella));

        int _Q = 0;

        bool _Initialized;
        DynamicData _Config;

        public DynamicData Config
        {
            get {
                return _Config;
            }

            set {
                _Config = value;
            }
        }

        public bool Initialized
        {
            get {
                return _Initialized;
            }

            set {
                _Initialized = value;
            }
        }
      
        public int CheckBusinessRule(DynamicData parameters, out bool accessresult, out DynamicData returns, out string msg)
        {
            int ret = 0;
            accessresult = false;
            returns = null;
            msg = null;
            string typeid, valueid;
            bool isExit;
            bool registerAccess;
            string image_from_kiosk = null;
            string tokenQR = null;
            /*
                1.- Tomo parametro de entrada y chequeo integridad
                    1.1.- Verifico si es CB desde cedula o desde credencial y recupero el RUT de acuerdo a definicion
                2.- Consumo servicio
                    2.1.- Si es Check y Marca segun config => Chequeo y registro.
                    2.2.- Si es colo check segun config => cehqeo e informo
                    2.3.- Si es solo consulta porque ya está registrado consumo y devuelve info de la cita ya agendada
                          (Para controld e credenciales)
                3.- Retorno resultado   
                    Vienen datos d ela cita. Se mostrar adatos generales de la cita, organizador y de la persona, no de otros.
            */
            try
            { 
                LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule IN...");
                //1.- Tomo parametro de entrada y chequeo integridad
               if (parameters == null || parameters.DynamicDataItems == null ||
                    !parameters.DynamicDataItems.ContainsKey("idRecognized") || 
                     !parameters.DynamicDataItems.ContainsKey("checktype"))
                {
                    ret = Errors.IERR_BAD_PARAMETER;
                    msg = "Error chequeando BusinessRule - Parametros nulos o faltantes...";
                    LOG.Warn("BusinessRuleBancoFalabella.CheckBusinessRule - " + msg);
                    return ret;
                } else //Estamso ok => Tomo valores
                {
                    bool isRUT;
                    typeid = "RUT"; //(string)parameters.DynamicDataItems["typeid"];
                    valueid = RetrieveRUT((string)parameters.DynamicDataItems["idRecognized"], out isRUT);

                    //Si no recupero rut => es un QR desde la credencial
                    if (!isRUT)
                    {
                        tokenQR = valueid;
                        valueid = null;
                    }

                    if (parameters.DynamicDataItems.ContainsKey("photo"))
                    {
                        image_from_kiosk = (string)parameters.DynamicDataItems["photo"];
                    }

                    //isExit = ((int)parameters.DynamicDataItems["checktype"] == 2);
                    LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule - Chequeando BusinessRule de typeid/valueid => " +
                                typeid + "/" + valueid + " - CheckingType=" + 
                                parameters.DynamicDataItems["checktype"].ToString());
                }
                //Detecto sentido de marca
                if (((string)parameters.DynamicDataItems["idDevice"]).Equals(Config.DynamicDataItems["PortCOMBarcodeIn"]) ||
                    ((string)parameters.DynamicDataItems["idDevice"]).Equals(Config.DynamicDataItems["SerialSensorIn"]))
                {
                    isExit = false;
                } else
                {
                    isExit = true;
                }

                //2.- Consumo servicio {registerAccess}}/{{isExit}}/{{token}}
                LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule - Setting ServiceType => " + 
                          ((int)Config.DynamicDataItems["ServiceType"]).ToString());
                registerAccess = ((int)Config.DynamicDataItems["ServiceType"]) == 1;
                LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule - Setting Checking Type (IN/OUT) => isExit = " +
                          isExit.ToString());

                //Consumo servicio + "api/AutoClubRules/CanAccess/" +
                string sURL = !((string)Config.DynamicDataItems["ServiceURL"]).EndsWith("/") ?
                                ((string)Config.DynamicDataItems["ServiceURL"]) + "/" :
                                ((string)Config.DynamicDataItems["ServiceURL"]);
                //sURL = sURL + typeid + "/" + valueid + "/" + registerAccess.ToString() + "/" +
                //             isExit.ToString() + "/" + Config.DynamicDataItems["ServiceAccessToken"];
                sURL = sURL + Config.DynamicDataItems["ServiceAccessToken"];
                LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule - LLama a " + sURL + "...");

                AccessModelIn _ParamIn = new AccessModelIn();
                _ParamIn.typeId = typeid;
                _ParamIn.valueId = valueid;

                if (_ParamIn.valueId.IndexOf("-") > 0) //Si viene el DV con - => lo saco
                {
                    _ParamIn.valueId = _ParamIn.valueId.Substring(0, _ParamIn.valueId.IndexOf("-"));
                }

                _ParamIn.registerAccess = registerAccess;
                _ParamIn.isExit = false; //isExit;
                _ParamIn.photography = image_from_kiosk;
                _ParamIn.tokenQR = tokenQR; //string.IsNullOrEmpty(tokenQR) ? "BkNA" : tokenQR;

               IRestClient client = new RestClient(sURL);
                client.Timeout = (int)Config.DynamicDataItems["ServiceTimeout"];
                var request = new RestRequest(Method.POST);
                request.AddJsonBody(_ParamIn);
                LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule - call execute...");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    AccessModelR oResponse = Config.DynamicDataItems.ContainsKey("DevelopEnabled") && 
                                             (bool)Config.DynamicDataItems["DevelopEnabled"] ?
                                                JsonConvert.DeserializeObject<AccessModelR>(System.IO.File.ReadAllText("json_response_bancofalabella.json")) :
                                                JsonConvert.DeserializeObject<AccessModelR>(response.Content);
                    if (oResponse != null)
                    {
                        //&& oResponse.Message.Equals("Identidad no registrada")
                        LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule - Parseando resultado...");
                        if (oResponse.Status.Equals("Error") && oResponse.Data != null && oResponse.Data.CanAccess.Equals("-4") )
                        {
                            ret = Errors.IERR_IDENTITY_NOT_FOUND;
                            msg = "No Registrado!";
                            LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule - Response No Registrado! [ret = " + ret + "]");
                        }
                        else
                        {
                            //TODO: Parsear nombre y foto para poder mostrar en el kiosko
                            returns = new DynamicData();
                            //if (oResponse.Status.Equals("Success") && oResponse.Data.Equals("1"))  
                            //{
                            //    accessresult = true;
                            //}

                            accessresult = (oResponse.Status.Equals("Success") && oResponse.Data.CanAccess.ToString().Equals("1"));
                            msg = oResponse.Message;
                            if (oResponse.Data != null && oResponse.Data.Identity != null && oResponse.Data.Meet != null)
                            {
                                string nombre = (string.IsNullOrEmpty(oResponse.Data.Identity.Nombres) ? "" : oResponse.Data.Identity.Nombres) + " "
                                      + (string.IsNullOrEmpty(oResponse.Data.Identity.ApePat) ? "" : oResponse.Data.Identity.ApePat) + " "
                                      + (string.IsNullOrEmpty(oResponse.Data.Identity.ApeMat) ? "" : oResponse.Data.Identity.ApeMat);
                                LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule - CanAccess ="  + accessresult.ToString() + 
                                          " - RUT= " + valueid + " - Nombre=" + (string.IsNullOrEmpty(nombre)?"":nombre));
                                returns.AddItem("Name", nombre);
                                if (string.IsNullOrEmpty(image_from_kiosk))
                                {
                                    returns.AddItem("Photografy", oResponse.Data.Identity.Foto);
                                } else
                                {
                                    returns.AddItem("Photografy", image_from_kiosk);
                                }
                                returns.AddItem("DateOfBirth", oResponse.Data.Identity.NacDate.ToString());
                                returns.AddItem("Code", oResponse.Data.CanAccess);
                                returns.AddItem("EventType", oResponse.Data.EvntType);
                                //Datos Meet
                                returns.AddItem("MeetName", oResponse.Data.Meet.EventName);
                                returns.AddItem("MeetDate", oResponse.Data.Meet.StartDate.ToString("dd/MM/yyyy"));
                                returns.AddItem("MeetStartTime", oResponse.Data.Meet.StartTime);
                                returns.AddItem("MeetEndTime", oResponse.Data.Meet.EndTime);
                                returns.AddItem("MeetAuthorizer", oResponse.Data.Meet.Authorizer);
                                returns.AddItem("MeetUnity", oResponse.Data.Meet.Unity);
                                returns.AddItem("ImageQR", oResponse.Data.ImageQR);

                                returns.AddItem("Message", oResponse.Message);

                            }
                            else
                            {
                                LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule - CanAccess =" + accessresult.ToString() +
                                          " - RUT= " + valueid + " - Datos Vacios");
                                returns.AddItem("Name",null);
                                returns.AddItem("Photografy", image_from_kiosk);
                                returns.AddItem("DateOfBirth", null);
                                returns.AddItem("Code", -1);
                                returns.AddItem("EventType", 0);
                                if (oResponse != null && !string.IsNullOrEmpty(oResponse.Message))
                                {
                                    returns.AddItem("Message", oResponse.Message);
                                }
                                else
                                {
                                    returns.AddItem("Message", "Respuesta del servicio Nula!");
                                } 
                            }

                            LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule - Response OK");
                            ret = 0;
                        }
                    } else
                    {
                        LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule - Error parseando respuesta del servicio...");
                        ret = Errors.IERR_DESERIALIZING_DATA;
                        accessresult = false;
                        msg = "Error chequeando acceso! [Respuesta Nula, consulte a su adminsitrador...]";
                    }
                }
                else
                {
                    LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule - Response Error");
                    msg = "Error del servicio: " + (response != null ? response.StatusCode.ToString() : "Null");
                    ret = Errors.IERR_CONX_WS;
                }

                //if (_Q % 2 == 0)
                //{
                //    accessresult = true;
                //    msg = "Acceso Permitido!";
                //    returns = new DynamicData();
                //    returns.AddItem("ParamReturnKey", _Q);
                //} else
                //{
                //    accessresult = false;
                //    msg = "Acceso NO Permitido [_Q=" + _Q.ToString() + "]";
                //}
                //_Q++;
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "BusinessRuleBancoFalabella Excp [" + ex.Message + "]";
                LOG.Error("BusinessRuleBancoFalabella.CheckBusinessRule - Excp Error: " + ex.Message);
            }
            LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule OUT! - ret = " + ret.ToString() + 
                      " - accessresult=" + accessresult.ToString());
            return ret;
        }

        //TODO - Ver si es de cedula => parsear sino desencriptar el QR de la credencial
        private string RetrieveRUT(string token, out bool isRUT)
        {
            string rutret = token;
            isRUT = false;
            try
            {
                if (!string.IsNullOrEmpty(token)) 
                {
                    //Si no es un token encriptado => parseo cedula.
                    if (!token.StartsWith("BK"))
                    {
                        if (token.Length <= 10)
                        {
                            rutret = token; //FormattingTaxid(token);  => xq debe llegar rut sin -
                        }
                        else
                        {
                            byte[] byLect = ASCIIEncoding.ASCII.GetBytes(token);
                            CedulaTemplate cedulabarcode = new CedulaTemplate(byLect); // Convert.FromBase64String(token));
                            if (cedulabarcode != null)
                            {
                                //rutret = FormattingTaxid(cedulabarcode.Rut);
                                rutret = cedulabarcode.Rut; // cedulabarcode.Rut.Substring(0, cedulabarcode.Rut.Length-1);=> xq debe llegar rut sin -
                            }
                        }
                        isRUT = true;
                    }
                } 
            }
            catch (Exception ex)
            {
                rutret = token;
                LOG.Error("BusinessRuleBancoFalabella.RetrieveRUT - Excp Error: " + ex.Message);
            }
            LOG.Debug("BusinessRuleBancoFalabella.RetrieveRUT OUT! - rutret = " + rutret);
            return rutret;
        }

        public int Initialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("CheckBusinessRule.Intialize IN...");
                if (!System.IO.File.Exists("Biometrika.BusinessRule.BancoFalabella.cfg"))
                {
                    Config = new DynamicData();
                    //Config.ConfigItems.Add("BusinessRuleDllPath", "Biometrika.BusinessRule.AutoclubAntofagasta");
                    //Parameters.Add("QueryCmd",
                    //        "SELECT TOP (200) bpiden.id AS id, bpiden.valueid, bpr.authenticationfactor, bpr.minutiaetype, bpr.bodypart, bpr.data " +
                    //        "  FROM bp_bir AS bpr INNER JOIN bp_identity AS bpiden ON bpr.identid = bpiden.id " +
                    //        " WHERE (bpr.minutiaetype = 7) AND (bpiden.companyidenroll = 7) AND bpiden.valueid = '21284415-2'");
                    //Config.ConfigItems.Add("URLService", "http://localhost/BS");
                    //Config.ConfigItems.Add("URLTimeout", 30000);
                    Config.AddItem("ServiceURL", "http://jano.biometrika.cl/AutoClub_Test/API/api/");
                    Config.AddItem("ServiceTimeout", 30000);
                    Config.AddItem("ServiceAccessToken", "794C0A6E5CE5480886E9F6CBC523094AC0535C58FA79EB81C6367902B5E204EC7EFF765F76DA693764A9EEFCE16A090F");
                    Config.AddItem("ServiceUser", "totemvehicular@autoclub");
                    Config.AddItem("ServicePassword", "fdbfeec3c613d7d0fcf6aecaf5c5be9a");
                    Config.AddItem("ServicePointName", "TotemVehicular");
                    Config.AddItem("ServiceType", 1); //0-Solo Check | 1-Check y Register
                    Config.AddItem("DevelopEnabled", true);
                    if (!SerializeHelper.SerializeToFile(Config, "Biometrika.BusinessRule.BancoFalabella.cfg"))
                    {
                        LOG.Warn("BusinessRuleBancoFalabella.Initialize - No grabo condig en disco (Biometrika.BusinessRule.BancoFalabella.cfg)");
                    }
                }  
                else
                {
                    Config = SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.BusinessRule.BancoFalabella.cfg");
                    if (Config != null) //Check nulo
                    {
                        Config.Initialize();
                        if (!Config.DynamicDataItems.ContainsKey("ServiceAccessToken") ||
                             string.IsNullOrEmpty((string)Config.DynamicDataItems["ServiceAccessToken"]))
                        {
                            ActualizeAccessToken();
                        }
                        Config.Initialize();
                        if (!SerializeHelper.SerializeToFile(Config, "Biometrika.BusinessRule.BancoFalabella.cfg"))
                        {
                            LOG.Warn("BusinessRuleBancoFalabella.Initialize - No grabo condig en disco (Biometrika.BusinessRule.BancoFalabella.cfg)");
                        }
                    } else
                    {
                        LOG.Fatal("BusinessRuleBancoFalabella.Initialize - No parseo la config. Elimine archivo Biometrika.BusinessRule.BancoFalabella.cfg y reintente...");
                    }
                }

                if (Config == null)
                {
                    LOG.Fatal("BusinessRuleBancoFalabella.Initialize - Error leyendo BusinessRuleBancoFalabella!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                _Initialized = true;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BusinessRuleBancoFalabella.Initialize Excp Error: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Revisa 
        /// </summary>
        private void ActualizeAccessToken()
        {
            try
            {
                LOG.Debug("BusinessRuleBancoFalabella.ActualizeAccessToken IN...");

                //Consumo servicio
                //http://jano.biometrika.cl/AutoClub_Test/API/api/Account/Login/
                //              totemvehicular @autoclub/fdbfeec3c613d7d0fcf6aecaf5c5be9a/TotemVehicular
                string sURL = Config.DynamicDataItems["ServiceURL"] + "api/Account/Login/" +
                                Config.DynamicDataItems["ServiceUser"] + "/" +
                                Config.DynamicDataItems["ServicePassword"] + "/" +
                                Config.DynamicDataItems["ServicePointName"];
                LOG.Debug("BusinessRuleBancoFalabella.CheckBusinessRule - LLama a " + sURL + "...");
                var client = new RestClient(sURL);
                client.Timeout = (int)Config.DynamicDataItems["ServiceTimeout"];
                var request = new RestRequest(Method.GET);

                LOG.Debug("BusinessRuleBancoFalabella.ActualizeAccessToken - call execute...");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    AccessModelRL oResponse = JsonConvert.DeserializeObject<AccessModelRL>(response.Content);
                    if (oResponse != null)
                    {
                        LOG.Debug("BusinessRuleBancoFalabella.ActualizeAccessToken - Parseando resultado...");
                        if (oResponse.Status.Equals("Success") && !string.IsNullOrEmpty(oResponse.Data))
                        {
                            Config.UpdateItem("ServiceAccessToken", oResponse.Data);
                        }
                        LOG.Debug("BusinessRuleBancoFalabella.ActualizeAccessToken - Response OK");
                    }
                    else
                    {
                        LOG.Debug("BusinessRuleBancoFalabella.ActualizeAccessToken - Error parseando respuesta del servicio...");
                    }
                }
                else
                {
                    LOG.Debug("BusinessRuleBancoFalabella.ActualizeAccessToken - Response Error");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BusinessRuleBancoFalabella.ActualizeAccessToken Excp Error: " + ex.Message);
            }
        }

        private Image SetImageFromB64(string base64Photo)
        {
            Image ret = null;
            try
            {
                if (!string.IsNullOrEmpty(base64Photo))
                {
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(base64Photo));
                    ret = Image.FromStream(ms);
                    ms.Close();
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("BusinessRuleBancoFalabella.SetImageFromB64 Excp Error: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Formate rut en formato NNNNNNNN-N
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        private String FormattingTaxid(string rut)
        {
            int cont = 0;
            String format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {
                    format = rut.Substring(i, 1) + format;
                    cont++;
                }
                return format;
            }
        }

        public int Dispose()
        {
            return 0;
        }
    }
}
