﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BusinessRule.BancoFalabella
{

    public class AccessModelIn
    {
        public string typeId { get; set; }
        public string valueId { get; set; }
        public bool registerAccess { get; set; }
        public bool isExit { get; set; }
        public string photography { get; set; }
        public string tokenQR { get; set; }
    }

    /// <summary>
    /// Para parsear respuesta desde WAIS.
    /// </summary>
    public class AccessModelRL
    {
        public string Status { get; set; }
        public string Data { get; set; }
        public string Message { get; set; }
        public string ReadableMessage { get; set; }
    }

    public class AccessModelR
    {
        public string Status { get; set; }
        public Data Data { get; set; }
        public string Message { get; set; }
        public string ReadableMessage { get; set; }

    }

    public class Meet
    {
        public string EventName { get; set; }
        public DateTime StartDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Authorizer { get; set; }
        public string Unity { get; set; }

    }

    public class Data
    {
        public int CanAccess { get; set; }
        public Identity Identity { get; set; }
        public Meet Meet { get; set; }
        public int EvntType { get; set; }
        public string ImageQR { get; set; }
        public string Event { get; set; }

    }

    public class Identity
    {
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string Nombres { get; set; }
        public string ApePat { get; set; }
        public string ApeMat { get; set; }
        public DateTime NacDate { get; set; }
        public string Foto { get; set; }
    }
    
   

    class Models
    {
    }
}
