﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Biometrika.BusinessRule.BancoFalabella")]
[assembly: AssemblyDescription("Reglas de Negocio para Banco Falabella")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Bioemtrika Latam")]
[assembly: AssemblyProduct("Biometrika.BusinessRule.BancoFalabella")]
[assembly: AssemblyCopyright("Copyright ©  2022")]
[assembly: AssemblyTrademark("Biometrika")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3359c4bc-80d4-42c2-a904-ac3b16ccaf57")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("7.5.*")]
[assembly: AssemblyFileVersion("7.5.0.0")]
