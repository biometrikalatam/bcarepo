﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Barcode.CedulaChile.Cedula
{
    public static class CedulaHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(CedulaHelper));

        internal static int ParseCedulaChilena(string strBarcodeToParse, out string rut)
        {
            int ret = 0;
            string type;
            rut = null;
            CedulaTemplate cedulaAntigua = null;
            CedulaTemplate2 cedulaNueva = null;
            try
            {
                LOG.Debug("CedulaHelper.ParseCedulaChilena IN...");
                byte[] rawSample = Convert.FromBase64String(strBarcodeToParse);
                string lect = Encoding.UTF7.GetString(rawSample, 0, rawSample.Length - 2);

                type = Encoding.UTF7.GetString(rawSample, 0, 1);

                if (!type.Equals("h"))
                {
                    LOG.Debug("CedulaHelper.ParseCedulaChilena - Parsing cedula antigua...");
                    cedulaAntigua = new CedulaTemplate(rawSample);
                }
                else
                {
                    LOG.Debug("CedulaHelper.ParseCedulaChilena - Parsing cedula nueva...");
                    cedulaNueva = new CedulaTemplate2(lect);
                }
                if ((cedulaAntigua != null) || (cedulaNueva != null))
                {
                    if (cedulaNueva != null)
                    {
                        rut = cedulaNueva.Rut;
                        LOG.Debug("CedulaHelper.ParseCedulaChilena - RUT desde cedula nueva => " + rut);
                    }
                    else
                    {
                        rut = FormattingTaxid(cedulaAntigua.Rut);
                        LOG.Debug("CedulaHelper.ParseCedulaChilena - RUT desde cedula antigua => " + rut);
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("CedulaHelper.ParseCedulaChilena - Excp Error: " + ex.Message);
            }
            LOG.Debug("CedulaHelper.ParseCedulaChilena OUT! ret = " + ret.ToString());
            return ret;
        }

        internal static String FormattingTaxid(string rut)
        {
            int cont = 0;
            String format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {

                    format = rut.Substring(i, 1) + format;

                    cont++;

                }
                return format;
            }
        }
    }
}
