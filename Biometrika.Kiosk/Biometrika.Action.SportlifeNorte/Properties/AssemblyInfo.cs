﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Biometrika.Action.SportlifeNorte")]
[assembly: AssemblyDescription("Implementacion de IAction para Sportlife Norte")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Biometrika")]
[assembly: AssemblyProduct("Biometrika.Action.SportlifeNorte")]
[assembly: AssemblyCopyright("Copyright ©Biometrika  2021")]
[assembly: AssemblyTrademark("Biometrika")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("dcf51543-b4ce-440a-ac0a-eec1b25a8c29")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("7.5.0.0")]
[assembly: AssemblyFileVersion("7.5.0.0")]
