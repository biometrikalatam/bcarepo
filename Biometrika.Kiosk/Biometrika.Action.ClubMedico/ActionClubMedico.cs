﻿using Biometrika.Action.ClubMedico.Helpers;
using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using System;

namespace Biometrika.Action.ClubMedico
{
    /// <summary>
    /// Implementa la interface de IAction para Sportlife Norte. 
    /// Para eso utiliza un Arduino y mada a abrir por un cierto tiempo 
    /// los rele. 
    /// Si viene un rele especifico, abre ese, sino abre todos.
    /// </summary>
    public class ActionClubMedico : IAction
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ActionClubMedico));

        /// <summary>
        /// Permite manejar la placa Arduino
        /// </summary>
        ArduinoHelper _ARDUINO_HELPER;

        bool _Initialized;
        string _Name;
        DynamicData _Config;

        /// <summary>
        /// Constructor vacio 
        /// </summary>
        public ActionClubMedico() { 
        }


        /// <summary>
        /// Configuraicon especial si necesita
        /// </summary>
        public DynamicData Config
        {
            get {
                return _Config;
            }

            set {
                _Config = value;
            }
        }

        public bool Initialized
        {
            get {
                return this._Initialized;
            }

            set {
                this._Initialized = value;
            }
        }

        public string Name
        {
            get {
                return this._Name;
            }

            set {
                this._Name = value;
            }
        }

        /// <summary>
        /// En este caso crea el objeto de ayuda al Arduino  lo inicializa, para dejarlo listo 
        /// para su uso cuando se necesite accionar.
        /// Lee el archivo de config y si no existe lo crea.
        /// El puerto com si bien lo configura, la libreria lo setea solo, por lo que no es relevante.
        /// ArduinoType permite definir cantidad de reles a manejar. Y el Delay es el tiempo que se mantiene
        /// accionado hasta apagarlo. 
        /// </summary>
        /// <returns></returns>
        public int Intialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("ActionClubMedico.Intialize IN...");
                if (!System.IO.File.Exists("Biometrika.Action.Fersanatura.cfg"))
                {
                    Config = new DynamicData();
                    Config.AddItem("COMPort", "COM8"); //No es necesaria en realidad, se autoconfigura
                    Config.AddItem("ArduinoType", 2); //Cantidad de rele en la placa
                    Config.AddItem("ArduinoOpenDelay", "3000");
                    //_Parameters.Add("", "");
                    if (!SerializeHelper.SerializeToFile(Config, "Biometrika.Action.ActionClubMedico.cfg"))
                    {
                        LOG.Warn("ActionClubMedico.Initialize - No grabo condif en disco (Biometrika.Action.ActionClubMedico.cfg)");
                    }
                }
                else
                {
                    Config = SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.Action.ActionClubMedico.cfg");
                    Config.Initialize();
                }

                if (Config == null)
                {
                    LOG.Fatal("ActionClubMedico.Initialize - Error leyendo Biometrika.Action.ActionClubMedico.cfg!");
                    return Errors.IERR_DESERIALIZING_DATA;
                } else //Si hay Config => Configuro Placa
                {
                    _ARDUINO_HELPER = new ArduinoHelper(Config.DynamicDataItems["ArduinoOpenDelay"].ToString(),
                                                        Config.DynamicDataItems["ServiceType"].ToString(),
                                                        (string)Config.DynamicDataItems["UrlServiceWebRele"]);
                    _ARDUINO_HELPER.Initialize();
                }
                //_Initialized = true;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ActionClubMedico.Initialize - Excp Error: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Realiza la acción en este caso de enviar a abrir todos los rele (POr ejemplo en
        /// la barrera para abrirla y para prender una luz verde).
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="returns"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int DoAction(DynamicData parameters, out DynamicData returns, out string msg)
        {
            int ret = Errors.IERR_OK;
            returns = null;
            msg = null;
            try
            {
                //object o = (parameters != null) ? parameters.ParamsList["key"] : null;
                if (parameters == null)
                    LOG.Fatal("ActionClubMedico.DoAction - Parametros NULO!");
                else
                {
                    string rele = "000";
                    if (parameters.DynamicDataItems.ContainsKey("Rele1"))
                    {
                        rele = (string)parameters.DynamicDataItems["Rele1"];
                        LOG.Debug("ActionClubMedico.DoAction - Set Rele1!");
                    } else if (parameters.DynamicDataItems.ContainsKey("Rele2"))
                    {
                        rele = (string)parameters.DynamicDataItems["Rele2"];
                        LOG.Debug("ActionClubMedico.DoAction - Set Rele2!");
                    }

                    if (rele.Equals("000"))
                    {
                        LOG.Debug("ActionClubMedico.DoAction - Open All rele [rele=000]");
                        if (_ARDUINO_HELPER.OpenAllWithDelay((int)Config.DynamicDataItems["ArduinoType"],
                                                         Config.DynamicDataItems["ArduinoOpenDelay"].ToString()) == 0)
                        {
                            msg = "Acciones Correctas!";
                            ret = Errors.IERR_OK;
                        }
                        else
                        {
                            msg = "Atención! Alguna/s accion/es Incorrectas!";
                            ret = Errors.IERR_HANDLE_RELE;
                        }
                    }
                    else 
                    {
                        LOG.Debug("ActionClubMedico.DoAction - Open rele = " + rele);
                        if (_ARDUINO_HELPER.OpenWithDelay(rele, Config.DynamicDataItems["ArduinoOpenDelay"].ToString()) == 0)
                        {
                            msg = "Acciones Correctas!";
                            ret = Errors.IERR_OK;
                        }
                        else
                        {
                            msg = "Atención! Alguna/s accion/es Incorrectas!";
                            ret = Errors.IERR_HANDLE_RELE;
                        }
                    }
                    //foreach (DynamicDataItem item in parameters.ListDynamicDataItems)
                    //{
                    //    LOG.Debug("ActionAutoclubAntofagasta.DoAction - key=" + item.key + " => value = " + (item.value.ToString()));
                    //}
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msg = "ActionClubMedico.DoAction  Excp [" + ex.Message + "]";
                LOG.Error("ActionClubMedico.DoAction  - Excp Error: " + ex.Message);
            }
            return ret;


        }
    }
}
