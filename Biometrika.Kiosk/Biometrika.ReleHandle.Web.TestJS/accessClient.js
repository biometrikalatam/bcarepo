﻿function BiometrikaActionClient(host) {
    this.BaseUrl = host;
    this.origin = "TestWeb";
    this.accessname = "Torniquete1";
    this.type = "ES";

   this.ExecuteAction = function () {
        var returnData = new BiometrikaClientResponse();

        var finalUrl = this.BaseUrl +
            "?origin=" + this.origin + "&accessname=" + this.accessname + "&type=" + this.type;
        //alert(finalUrl);
        //var finalUrl = this.BaseUrl +
        //    "?origin=" + this.origin + "&relenumber=" + this.relenumber + "&operation=1"; // + this.timeout;

        var a = function (callback) {
            $.ajax({
                type: "POST",
                url: finalUrl,
                async: false,
                crossDomain: true,
                processData: false,
                error: function (request, status, error) {
                    var errMsg = "Error al llamar a BiometrikaReleHandle, Retorno=" + request.responseText;
                    alert(errMsg);
                    //alert(status);
                    returnData.Status = false;
                    returnData.Message = errMsg;
                    console.log(errMsg);
                },
                success: callback
            });
        };

        a(function (msg) {
            returnData.Status = true;
            returnData.Data = msg;
        });

        return returnData;
    };

    this.GetConfig = function () {
        var returnData = new BiometrikaClientResponse();

        var finalUrl = this.BaseUrl;
        //alert(finalUrl);
        //var finalUrl = this.BaseUrl +
        //    "?origin=" + this.origin + "&relenumber=" + this.relenumber + "&operation=1"; // + this.timeout;

        var a = function (callback) {
            $.ajax({
                type: "GET",
                url: finalUrl,
                async: false,
                crossDomain: true,
                processData: false,
                error: function (request, status, error) {
                    var errMsg = "Error al llamar a BiometrikaReleHandle, Retorno=" + request.responseText;
                    alert(errMsg);
                    //alert(status);
                    returnData.Status = false;
                    returnData.Message = errMsg;
                    console.log(errMsg);
                },
                success: callback
            });
        };

        a(function (data) {
            returnData.Status = true;
            returnData.Message = data.message;
        });

        return returnData;
    };
}

function BiometrikaClientResponse() {
    this.Status = false;
    this.Message = "";
    this.Data = "";
}