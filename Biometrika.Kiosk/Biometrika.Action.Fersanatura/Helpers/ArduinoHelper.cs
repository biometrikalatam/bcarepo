﻿using BioArduinoRele;
using Biometrika.Kiosk.Common.Model;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Action.Fersanatura.Helpers
{
    public class ArduinoHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ArduinoHelper));

        bool _Initialized;
        ArduinoRele _ARDUINO;
        string _ArduinoDeviceId;
        string _ArduinoDelay;
        string _ServiceType = "1"; //1-Directo | 2-Web Rele
        string _UrlServiceWebRele = "http://localhost/";

        public ArduinoHelper(string _delay, string _servicetype, string _urlservice)
        {
            try
            {
                LOG.Debug("ArduinoHelper.Constructor IN...");
                _ArduinoDelay = _delay;
                _ServiceType = _servicetype;
                _UrlServiceWebRele = _urlservice;
                LOG.Debug("ArduinoHelper.Constructor _ArduinoDelay = " + _ArduinoDelay.ToString() +
                            " - ServiceType = " + (string.IsNullOrEmpty(_servicetype) ? "Null" : _servicetype) +
                            " - URLService = " + (string.IsNullOrEmpty(_urlservice)?"Null":_urlservice));
                _Initialized = true;
            }
            catch (Exception ex)
            {
                _Initialized = false;
                LOG.Error("ArduinoHelper.Constructor Excp Error: " + ex.Message);
            }
        }

        public int Initialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("ArduinoHelper.Initialize IN...");

                if (_ServiceType.Equals("1"))
                {
                    LOG.Debug("ArduinoHelper.Initialize - Servicetype = 1 (Directo)...");
                    LOG.Debug("ArduinoHelper.Initialize Creo objeto...");
                    _ARDUINO = new ArduinoRele();
                    LOG.Debug("ArduinoHelper.Initialize - getCOM call...");
                    _ArduinoDeviceId = _ARDUINO.getCOM();
                    LOG.Debug("ArduinoHelper.Initialize - _ArduinoDeviceId = " +
                        (string.IsNullOrEmpty(_ArduinoDeviceId) ? "Null" : _ArduinoDeviceId));
                    _Initialized = !string.IsNullOrEmpty(_ArduinoDeviceId);  //Si devuelve valor => configuro ok
                } else
                {
                    LOG.Debug("ArduinoHelper.Initialize - Servicetype = 2 (Web)...");
                    string sRet = PingToService();
                    LOG.Debug("ArduinoHelper.Initialize - Ping Result => " + sRet);
                    _Initialized = sRet.StartsWith("OK");
                }
                LOG.Debug("ArduinoHelper.Initialize -_Initialized = " + _Initialized.ToString());
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ArduinoHelper.Initialize Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.Initialize OUT! => ret = " + ret.ToString());
            return ret;
        }

       

        public int OpenWithDelay(string rele, string delay)
        {
            int ret = Errors.IERR_OK;
            string msgErr;
            try
            {
                LOG.Debug("ArduinoHelper.OpenWithDelay IN...");

                LOG.Debug("ArduinoHelper.OpenWithDelay - Call OpenDelay method...");
                if (_ServiceType.Equals("1"))
                {
                    _ARDUINO.openDelay(rele, delay, out msgErr);
                }
                else
                {
                    msgErr = WebReleOpenDelay(rele, delay);
                }
                if (string.IsNullOrEmpty(msgErr))
                {
                    LOG.Debug("ArduinoHelper.OpenWithDelay - Abrio rele = " + rele + " por " + delay + " milisegundos...");
                    ret = Errors.IERR_OK;
                } else
                {
                    LOG.Debug("ArduinoHelper.OpenWithDelay - NO Abrio rele = " + rele + " [" + msgErr + "]");
                    ret = Errors.IERR_HANDLE_RELE;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("ArduinoHelper.OpenWithDelay Excp Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.OpenWithDelay OUT! => ret = " + ret.ToString());
            return ret;
        }

       
        public int OpenAllWithDelay(int qReles, string delay)
        {
            int ret = 0;
            string strRele;
            string msgErr;
            try
            {
                LOG.Debug("ArduinoHelper.OpenAllWithDelay IN...");

                LOG.Debug("ArduinoHelper.OpenAllWithDelay Creo objeto...");
                //for (int i = 1; i <= qReles; i++)
                //{
                    msgErr = null;
                //strRele = "0" + i.ToString() + "0";
                //_ARDUINO.openDelay(strRele, delay, out msgErr);
                if (_ServiceType.Equals("1"))
                {
                    _ARDUINO.openCloseAllDelay(delay, out msgErr);
                }
                else
                {
                    msgErr = WebReleOpenCloseAllDelay(delay);
                }
                if (string.IsNullOrEmpty(msgErr))
                {
                    LOG.Debug("ArduinoHelper.OpenAllWithDelay - Abrio reles por " + delay + " milisegundos...");
                    ret = ret + Errors.IERR_OK;
                }
                else
                {
                    LOG.Debug("ArduinoHelper.OpenAllWithDelay - NO Abrio reles [" + msgErr + "]");
                    ret = ret + Errors.IERR_HANDLE_RELE;
                }
                //}
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ArduinoHelper.OpenAllWithDelay Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.OpenAllWithDelay OUT! => ret = " + ret.ToString());
            return ret;
        }

       


        #region Service API Web Rele

        private string PingToService()
        {
            string ret = "ERROR";
            LOG.Debug("ArduinoHelper.PingToService IN...");
            try
            {
                var client = new RestClient(_UrlServiceWebRele + "/api/ping");
                client.Timeout = 30000;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                ReleHandleResponse objResponse = JsonConvert.DeserializeObject<ReleHandleResponse>(response.Content);
                if (objResponse != null && objResponse.code == 0)
                {
                    ret = "OK - " + (string)objResponse.data;
                } else
                {
                    ret = "ERROR - " + (objResponse != null ? objResponse.message : "objResponse=null");
                }

            }
            catch (Exception ex)
            {
                ret = "ERROR - " + ex.Message;
                LOG.Error("ArduinoHelper.PingToService Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.PingToService OUT!");
            return ret;
        }

        private string WebReleOpenDelay(string rele, string delay)
        {
            string ret = "ERROR";
            LOG.Debug("ArduinoHelper.WebReleOpenDelay IN...");
            try
            {
                //Depuro numero de rele para normalizar con Arduino
                if (rele.Equals("010")) {
                    rele = "1";
                }
                else if (rele.Equals("020")) {
                    rele = "2";
                }
                else if (rele.Equals("030"))
                {
                    rele = "3";
                }
                else if (rele.Equals("040"))
                {
                    rele = "4";
                }


                string param = "origin=1&relenumber=" + rele + "&timeout=" + delay;
                var client = new RestClient(_UrlServiceWebRele + "/api/OpRelePulso?" + param);
                client.Timeout = 30000;
                var request = new RestRequest(Method.POST);
                IRestResponse response = client.Execute(request);
                ReleHandleResponse objResponse = JsonConvert.DeserializeObject<ReleHandleResponse>(response.Content);
                if (objResponse != null && objResponse.code == 0)
                {
                    ret = null; // "OK - " + (string)objResponse.data;
                }
                else
                {
                    ret = "ERROR - " + (objResponse != null  ? objResponse.code + "-" + objResponse.message : "objResponse=NULL => WebRelay No Repsonde!");
                }

            }
            catch (Exception ex)
            {
                ret = "ERROR - " + ex.Message;
                LOG.Error("ArduinoHelper.WebReleOpenDelay Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.WebReleOpenDelay OUT!");
            return ret;
        }

        private string WebReleOpenCloseAllDelay(string delay)
        {
            string ret = "ERROR";
            LOG.Debug("ArduinoHelper.WebReleOpenCloseAllDelay IN...");
            try
            {
                string param = "origin=1&timeout=" + delay;
                var client = new RestClient(_UrlServiceWebRele + "/api/OpReleAllDelay?" + param);
                client.Timeout = 30000;
                var request = new RestRequest(Method.POST);
                IRestResponse response = client.Execute(request);
                ReleHandleResponse objResponse = JsonConvert.DeserializeObject<ReleHandleResponse>(response.Content);
                if (objResponse != null && objResponse.code == 0)
                {
                    ret = null; // "OK - " + (string)objResponse.data;
                }
                else
                {
                    ret = "ERROR - " + objResponse.code + "-" + objResponse.message;
                }

            }
            catch (Exception ex)
            {
                ret = "ERROR - " + ex.Message;
                LOG.Error("ArduinoHelper.WebReleOpenCloseAllDelay Error: " + ex.Message);
            }
            LOG.Debug("ArduinoHelper.WebReleOpenCloseAllDelay OUT!");
            return ret;
        }

        #endregion Service API Web Rele

    }
}

public class ReleHandleResponse
{
    public int code;
    public string message;
    public object data;
}
