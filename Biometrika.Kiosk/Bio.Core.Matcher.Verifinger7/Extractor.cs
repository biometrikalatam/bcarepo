﻿using System;
using System.Drawing;
using Bio.Core.Constant;
using Bio.Core.Wsq.Decoder;
using log4net;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Images;
using Bio.Core.Matcher.Interface;
//using Neurotec.Images;

namespace Bio.Core.Matcher.Verifinger7
{
	public class Extractor : IExtractor
	{
		private static readonly ILog LOG = LogManager.GetLogger(typeof(Extractor));

		


#region Implementation of IExtractor

		private int _authenticationFactor;

		private int _minutiaeType;

		private double _threshold;

		private string _parameters;

        private NBiometricClient _biometricClient=new NBiometricClient();

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
		{
			get { return _parameters; }
			set { _parameters = value; }
		}

		/// <summary>
		/// Tecnologia a utilizar para extraccion
		/// </summary>
		public int AuthenticationFactor
		{
			get { return _authenticationFactor; }
			set { _authenticationFactor = value; }
		}

		/// <summary>
		/// Tipo d eminucia dentro de la tecnologia utilziada
		/// </summary>
		public int MinutiaeType
		{
			get { return _minutiaeType; }
			set { _minutiaeType = value; }
		}

		/// <summary>
		/// Umbral de extracción considerada aceptable
		/// </summary>
		public double Threshold
		{
			get { return _threshold; }
			set { _threshold = value; }
		}

		/// <summary>
		/// Extrae desde el template ingresado en el parametro inicial, de acuerdo a 
		/// los adtos de tecnologia y minucias seteados, el template resultante. 
		/// Estos parámetros están serializados en xml, por lo que primero se deserializa.
		/// </summary>
		/// <param name="xmlinput">xml con datos input</param>
		/// <param name="xmloutput">template generado serializado en xml</param>
		/// <returns>coidgo de error si existe o 0 si genero ok</returns>
		public int Extract(string xmlinput, out string xmloutput)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Idem anterior pero entrega un ITemplate
		/// </summary>
		/// <param name="xmlinput">xml con datos input</param>
		/// <param name="templateout">template generado</param>
		/// <returns>coidgo de error si existe o 0 si genero ok</returns>
		public int Extract(string xmlinput, out ITemplate templateout)
		{
			throw new NotImplementedException();
		}

		public void TestVF(String path, out ITemplate templateout)
		{
			byte[] raw;
			NFRecord nfeRecord;
			NBiometricClient _biometricClient = new NBiometricClient() ;
			NSubject _subject;
			int res = Errors.IERR_OK;
			templateout=null;
			
			int a = 0;
			if (!VfeUtils.IsLicenseConfigured)
			{
				if (!VfeUtils.ConfigureLicense(_parameters)) 
					a=Errors.IERR_LICENSE;
			}

			raw = Convert.FromBase64String(System.IO.File.ReadAllText(path));
			//2.- Extraigo  
			NFExtractor _extractor = new NFExtractor();

			//Creo Bitmap desde RAW
			Bitmap imgBMP = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw, 512, 512);

			//Creo NImgae (VF) desde el BMP
			NImage imageRAWtoBMP = NImage.FromBitmap(imgBMP);
			LOG.Error("Verifinger7.Extractor.Extract - Crea NGrayscaleImage...");
			NGrayscaleImage NGImage = (NGrayscaleImage)NImage.FromImage(NPixelFormat.Grayscale8U, 0, imageRAWtoBMP);
			//    (NPixelFormat.Grayscale8S,0, 500, 500, imageRAWtoBMP);

			NfeExtractionStatus extractionStatus = new NfeExtractionStatus();
			LOG.Error("Verifinger7.Extractor.Extract - Crea nfeRecord (Extract Func)...");
			nfeRecord = _extractor.Extract(NGImage, NFPosition.Unknown,
													NFImpressionType.NonliveScanPlain,
													out extractionStatus);
			if (extractionStatus != NfeExtractionStatus.TemplateCreated)
			{
				res = Errors.IERR_EXTRACTING;
				templateout = null;
				LOG.Error("Verifinger7.Extractor.Extract Error [extractionStatus=" + extractionStatus.ToString() + "]");
			}
			else
			{

				_biometricClient = new NBiometricClient { UseDeviceManager = false };
				NFinger finger2 = new NFinger();
				_subject = new NSubject();
				_subject.Fingers.Add(finger2);
				NImage nimagen = NImage.FromBitmap(imgBMP);
				nimagen.HorzResolution = (float)500.0;
				nimagen.VertResolution = (float)500.0;
				NGrayscaleImage NGImage2 = (NGrayscaleImage)NImage.FromImage(NPixelFormat.Grayscale8U, 0, nimagen);
				_subject.Fingers[0].Image = NGImage2;
				LOG.Info("Antes de extraer");
				_biometricClient.FingersTemplateSize = NTemplateSize.Large;
				NBiometricStatus status = _biometricClient.CreateTemplate(_subject);
				LOG.Info("TEST:" + Convert.ToBase64String(_subject.GetTemplate().Fingers.Records[0].Save().ToArray()));
				NTemplate objNTemplate = _subject.GetTemplate();
				//Neurotec.IO.NBuffer nBuf2 = 

			   
				byte[] data = _subject.GetTemplate().Fingers.Records[0].Save().ToArray();// _subject.GetTemplateBuffer().ToArray();
				templateout = new Template
				{
					//Data = _subject.GetTemplateBuffer().ToArray()
					Data = data

				};
				//System.IO.File.WriteAllText(@"C:\paso\verifinger\verifingernicolededoindicederecho.txt", Convert.ToBase64String(data));
			   /* templateout.AdditionalData = "SizeData=" + templateout.Data.Length.ToString() +
										"|Quality=" + _subject.Fingers[0].Objects[0].NfiqQuality;
				templateout.AuthenticationFactor = templatebase.AuthenticationFactor;
				templateout.BodyPart = templatebase.BodyPart;
				templateout.MinutiaeType = Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER;
				templateout.Type = Constant.BirType.PROCESSED_DATA;*/
				//imageRAWtoBMP.Dispose();
				//NGImage.Dispose();
			}
		   
						
		}

        public void ExtractFromBy512(byte[] raw, out ITemplate templateout)
        {

            NFRecord nfeRecord;
            NBiometricClient _biometricClient = new NBiometricClient();
            NSubject _subject;
            int res = Errors.IERR_OK;
            templateout = null;

            int a = 0;
            if (!VfeUtils.IsLicenseConfigured)
            {
                if (!VfeUtils.ConfigureLicense(_parameters))
                    a = Errors.IERR_LICENSE;
            }

            //2.- Extraigo  
            NFExtractor _extractor = new NFExtractor();

            //Creo Bitmap desde RAW
            Bitmap imgBMP = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw, 512, 512);

            //Creo NImgae (VF) desde el BMP
            NImage imageRAWtoBMP = NImage.FromBitmap(imgBMP);
            LOG.Error("Verifinger7.Extractor.Extract - Crea NGrayscaleImage...");
            NGrayscaleImage NGImage = (NGrayscaleImage)NImage.FromImage(NPixelFormat.Grayscale8U, 0, imageRAWtoBMP);
            //    (NPixelFormat.Grayscale8S,0, 500, 500, imageRAWtoBMP);

            NfeExtractionStatus extractionStatus = new NfeExtractionStatus(); 
            
			LOG.Error("Verifinger7.Extractor.Extract - Crea nfeRecord (Extract Func)...");
			nfeRecord = _extractor.Extract(NGImage, NFPosition.Unknown,
													NFImpressionType.NonliveScanPlain,
													out extractionStatus);
			if (extractionStatus != NfeExtractionStatus.TemplateCreated)
			{
				res = Errors.IERR_EXTRACTING;
				templateout = null;
				LOG.Error("Verifinger7.Extractor.Extract Error [extractionStatus=" + extractionStatus.ToString() + "]");
			}
			else
			{

				_biometricClient = new NBiometricClient { UseDeviceManager = false };
				NFinger finger2 = new NFinger();
				_subject = new NSubject();
				_subject.Fingers.Add(finger2);
				NImage nimagen = NImage.FromBitmap(imgBMP);
				nimagen.HorzResolution = (float)500.0;
				nimagen.VertResolution = (float)500.0;
				NGrayscaleImage NGImage2 = (NGrayscaleImage)NImage.FromImage(NPixelFormat.Grayscale8U, 0, nimagen);
				_subject.Fingers[0].Image = NGImage2;
				LOG.Info("Antes de extraer");
				_biometricClient.FingersTemplateSize = NTemplateSize.Large;
				NBiometricStatus status = _biometricClient.CreateTemplate(_subject);
				LOG.Info("TEST:" + Convert.ToBase64String(_subject.GetTemplate().Fingers.Records[0].Save().ToArray()));
				NTemplate objNTemplate = _subject.GetTemplate();
				//Neurotec.IO.NBuffer nBuf2 = 


				byte[] data = _subject.GetTemplate().Fingers.Records[0].Save().ToArray();// _subject.GetTemplateBuffer().ToArray();
				templateout = new Template
				{
					//Data = _subject.GetTemplateBuffer().ToArray()
					Data = data

				};
				//System.IO.File.WriteAllText(@"C:\paso\verifinger\verifingernicolededoindicederecho.txt", Convert.ToBase64String(data));
				/* templateout.AdditionalData = "SizeData=" + templateout.Data.Length.ToString() +
										 "|Quality=" + _subject.Fingers[0].Objects[0].NfiqQuality;
				 templateout.AuthenticationFactor = templatebase.AuthenticationFactor;
				 templateout.BodyPart = templatebase.BodyPart;
				 templateout.MinutiaeType = Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER;
				 templateout.Type = Constant.BirType.PROCESSED_DATA;*/
				//imageRAWtoBMP.Dispose();
				//NGImage.Dispose();
			}


		}

		/// <summary>
		/// Idem anterior pero ingresa un ITemplate y entrega un ITemplate
		/// </summary>
		/// <param name="templatebase">Template base para extraccion</param>
		/// <param name="destination">Determina si es template para 1-Verify | 2-Enroll </param>
		/// <param name="templateout">template generado</param>
		/// <returns>coidgo de error si existe o 0 si genero ok</returns>
		public int Extract(ITemplate templatebase, int destination, out ITemplate templateout)
		{
			int res = Errors.IERR_OK;
			templateout = null;
            DateTime dtStart;
            DateTime dtEnd;
            try
			{
                dtStart = DateTime.Now;
                LOG.Info("inicio del Extractor:" + dtStart);
                if (!VfeUtils.IsLicenseConfigured)
                {
                    if (!VfeUtils.ConfigureLicense(_parameters)) return Errors.IERR_LICENSE;
                }

                if (templatebase == null) return Errors.IERR_NULL_TEMPLATE;
				if (templatebase.Data == null) return Errors.IERR_NULL_TEMPLATE;

				if (templatebase.AuthenticationFactor !=
					Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT ||
					(templatebase.MinutiaeType !=
					Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER && 
					templatebase.MinutiaeType !=
					Constant.MinutiaeType.MINUTIAETYPE_WSQ &&
					templatebase.MinutiaeType !=
					Constant.MinutiaeType.MINUTIAETYPE_RAW))
				{
					return Errors.IERR_INVALID_TEMPLATE;
				}

				//Si lo anterior esta ok, y la minucia es Verifinger solo asigno porque 
				if (templatebase.MinutiaeType == Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER)
				{
					LOG.Error("Verifinger7.Extractor.Extract - In Minutiae VF...");
					templateout = templatebase;
				}
				else //es Constant.MinutiaeType.MINUTIAETYPE_WSQ
				{
					NFRecord nfeRecord;
					//NBiometricClient _biometricClient;
					NSubject _subject;
					byte[] raw;
					bool rawOK = false;
					if (templatebase.MinutiaeType == Constant.MinutiaeType.MINUTIAETYPE_WSQ)
					{
						LOG.Error("Verifinger7.Extractor.Extract - In WSQ...");
						//1.- Descomprimo WSQ
						WsqDecoder decoder = new WsqDecoder();
						short w, h;
						if (!decoder.DecodeMemory(templatebase.Data, out w, out h, out raw))
						{
							res = Errors.IERR_WSQ_DECOMPRESSING;
							LOG.Error("Verifinger7.Extractor.Extract Error [decoder.DecodeMemory]");
						} else
						{
							rawOK = true;
						}
                        dtEnd = DateTime.Now;
                        LOG.Info("FIN WSQ = " + (dtEnd - dtStart).TotalSeconds.ToString());
                    } else
					{
						//LOG.Error("Verifinger7.Extractor.Extract - In RAW...");
						raw = templatebase.Data;
						rawOK = true;
					}
					
					if (rawOK)
					{
						
						NFExtractor _extractor = new NFExtractor();
						
						//Creo Bitmap desde RAW
						Bitmap imgBMP = Imaging.ImageProcessor.RawToBitmap(raw,512,512);
                    	//Creo NImgae (VF) desde el BMP
						NImage imageRAWtoBMP = NImage.FromBitmap(imgBMP);
						NGrayscaleImage NGImage = (NGrayscaleImage)NImage.FromImage(NPixelFormat.Grayscale8U, 0, imageRAWtoBMP);
						NfeExtractionStatus extractionStatus = new NfeExtractionStatus();
						
						nfeRecord = _extractor.Extract(NGImage, NFPosition.Unknown,
																NFImpressionType.NonliveScanPlain,
																out extractionStatus);

                        dtEnd = DateTime.Now;
                        LOG.Info("FIN EXTRACTOR 2 = " + (dtEnd - dtStart).TotalSeconds.ToString());

                        if (extractionStatus != NfeExtractionStatus.TemplateCreated)
						{
							res = Errors.IERR_EXTRACTING;
							templateout = null;
							LOG.Error("Verifinger7.Extractor.Extract Error [extractionStatus=" + extractionStatus.ToString() + "]");
						} else
						{

							//_biometricClient = new NBiometricClient { UseDeviceManager = false };
							NFinger finger2 = new NFinger();
							_subject = new NSubject();
							_subject.Fingers.Add(finger2);
							NImage nimagen = NImage.FromBitmap(imgBMP);
							nimagen.HorzResolution = (float)500.0;
							nimagen.VertResolution = (float)500.0;
							NGrayscaleImage NGImage2 = (NGrayscaleImage)NImage.FromImage(NPixelFormat.Grayscale8U, 0, nimagen);
							_subject.Fingers[0].Image = NGImage2;
							//LOG.Info("Antes de extraer");
							//_biometricClient.FingersTemplateSize = NTemplateSize.Large;
							NBiometricStatus status = _biometricClient.CreateTemplate(_subject);
							//LOG.Info("Template generado:"  + Convert.ToBase64String(_subject.GetTemplate().Fingers.Records[0].Save().ToArray()));
							NTemplate objNTemplate = _subject.GetTemplate();
							byte[] data = _subject.GetTemplate().Fingers.Records[0].Save().ToArray();// _subject.GetTemplateBuffer().ToArray();
							templateout = new Template
							{
								//Data = _subject.GetTemplateBuffer().ToArray()
								Data = data

							};
						   
							templateout.AdditionalData = "SizeData=" + templateout.Data.Length.ToString() +
													"|Quality=" + _subject.Fingers[0].Objects[0].NfiqQuality;
							templateout.AuthenticationFactor = templatebase.AuthenticationFactor;
							templateout.BodyPart = templatebase.BodyPart;
							templateout.MinutiaeType = Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER;
							templateout.Type = Constant.BirType.PROCESSED_DATA;
							
						}
					} 
				}
                dtEnd = DateTime.Now;
                LOG.Info("FIN EXTRACTOR = " + (dtEnd - dtStart).TotalSeconds.ToString());
            }
			catch (Exception ex)
			{
				res = Errors.IERR_UNKNOWN;
				LOG.Error("Bio.Core.Matcher.Verifinger7.Extract(T, T) Error", ex);
			}
			return res;
		}

#endregion Implementation of IExtractor

		public void Dispose()
		{
			if (VfeUtils.IsLicenseConfigured)
			{
				VfeUtils.ReleaseLicenses();
                _biometricClient.Dispose();
			}
		}

        public void TestExtractor(string path)
        {
            throw new NotImplementedException();
        }

        public int Inicialize(string xmlparam)
        {
            int res = 0;
            String msg = "";
            if (!VfeUtils.IsLicenseConfigured)
            {
                if (!VfeUtils.ConfigureLicense(_parameters))
                {
                    msg = "Bio.Core.Matcher.Verifinger7.Matcher Error Chequeando Licencia Verfinger";
                    LOG.Fatal(msg);
                 
                    return Errors.IERR_LICENSE;
                }
            }
            _biometricClient = new NBiometricClient();
            _biometricClient.MatchingWithDetails = true;
            _biometricClient.UseDeviceManager = false;
            _biometricClient.FingersMatchingSpeed = NMatchingSpeed.High;
            _biometricClient.MatchingThreshold = Convert.ToInt32(Convert.ToInt32(_threshold));
            _biometricClient.FingersTemplateSize = NTemplateSize.Large;

            return res;
        }
    }
}
