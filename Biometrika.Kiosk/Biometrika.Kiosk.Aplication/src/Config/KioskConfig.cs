﻿using Biometrika.Kiosk.Common.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Aplication.src.Config
{
    /// <summary>
    /// Implementa el IKioskConfig para manejar los parámetros de ejecución.
    /// Aqui se habilitan o deshabilitan las tecnologías que operarán en el kiosko, 
    /// las cuales son, identificación por: Finger, código de barras (cedula y cualqueir otro 
    /// que empiece con RUT), MRZ. Falta completar PIN y Facial 
    /// Una de las configuraciones es el Theme. Este tiene asociado un directorio
    /// en <PATH\Theme\<NombreTheme> donde están las imágenes y datos necesarios para 
    /// establecer el UI customizado para ese Theme. Si alguno de los datos falta se usa
    /// el existente en Default.
    /// </summary>
    public class KioskConfig : IKioskConfig
    {

        private string _Name;
        private string _PathLoggerConfig;

        private bool _AccessByFingerprintEnabled;
        
        private int _QualityCapture;
        private string _PathMatcherConfig;
        private int _TimeToRefreshAutomaticBIRs = 720;  //En Minutos => Default 12hs = 720 minutos = 43200 segundos = 43200000 milisegundos
        private string _SerialSensor1;
        private string _SerialSensor2;

        private bool _AccessByFacialEnabled;
        private bool _AccessByMRZEnabled;

        private bool _AccessByBarcodeEnabled;
        private bool _ParseCedulaInBarcodeEnabled = true;
        private string _PortCOMBarcode1;
        private string _PortCOMBarcode2;

        private bool _AccessByPINEnabled = false;
        private int _TimeToShowPINPanel = 5000;


        private string _Rele1; //000-Todos | 010-Rele 1 | 020-Rele2 | 030-Rele3 | 040-Rele4
        private string _Rele2; //000-Todos | 010-Rele 1 | 020-Rele2 | 030-Rele3 | 040-Rele4

        //1-Un solo sentido (solo considerar todos los 1 => Ej. _Rele1) | 2- Entrada y Salida (Los 1 son entrada y 2 salida)
        private int _KioskType = 2; 
        private int _KioskType1 = 1; //0-Manual | 1-Entrada | 2-Salida
        private int _KioskType2 = 2; //0-Manual | 1-Entrada | 2-Salida
        private int _TimeToShowResult = 2000;
        //Theme
        private string _Theme;

        private bool _ShowReloj = false;

        private int _TimerToReinitLCB = 0; //5 minutos = 300000 milisegundos => Default Disabled

        private bool _IsDummy = false; //Se agrega para ferias o demos donde queremos mostrar BusinessRules Dumy con Enroll 

        private bool _TakeFotoInMark = false; //Toma una foto en el momento de consulta para dejar como evidencia a auditorias
        private int _CameraIndexToUse = 0; //POr si hay mas de una webcam conectada, selecciona la camara para la foto si _TakeFotoInMark = true
        private bool _ShowVideoToTakeFotoInMark = false; //Toma una foto en el momento de consulta para dejar como evidencia a auditorias

        private string _CameraId1 = "usb#vid_1bcf&pid_28c1&mi_00#6&269dc56c&0&0000";
        private string _CameraId2 = "usb#vid_045e&pid_0812&mi_00#7&783613d&0&0000";

        private bool _ShowResultCustomized = false; //Si para mostrar resultados usa una IScreen de IScreenEngine (BancoFalabella)

        //Multi factor Enabled
        private bool _MFAEnable = false;
        //indica que primero se lee el barcode, y luego toma o huella o cara para verificacion
        //Esta verificacion es copntra un servicio, pero NO es identificacion. Para evitar BD, sincronizaciones y licnecias para identificacion.
        //Si MFAEnable = false => Se usa _AccessByFingerprintEnabled o _AccessByFacialEnabled para identificacion
        private int _MFAType = 2;  //1-Barcode + Fingerprint | 2-Brcode + Facial
        private int _MFAControlType = 1;  //1-Solo Entrada | 2-Solo Salida | 3- E/S

        #region Public Properties

        public bool MFAEnable
        {
            get
            {
                return this._MFAEnable;
            }

            set
            {
                this._MFAEnable = value;
            }
        }

        public int MFAType
        {
            get
            {
                return this._MFAType;
            }

            set
            {
                this._MFAType = value;
            }
        }

        public int MFAControlType
        {
            get
            {
                return this._MFAControlType;
            }

            set
            {
                this._MFAControlType = value;
            }
        }
        public string Name
        {
            get {
                return this._Name;
            }

            set {
                this._Name = value;
            }
        }

        public string PathLoggerConfig
        {
            get {
                return this._PathLoggerConfig;
            }

            set {
                this._PathLoggerConfig = value;
            }
        }

        public bool AccessByFingerprintEnabled
        {
            get {
                return this._AccessByFingerprintEnabled;
            }

            set {
                this._AccessByFingerprintEnabled = value;
            }
        }

        public string SerialSensor1
        {
            get {
                return this._SerialSensor1;
            }

            set {
                this._SerialSensor1 = value;
            }
        }

        public string SerialSensor2
        {
            get
            {
                return this._SerialSensor2;
            }

            set
            {
                this._SerialSensor2 = value;
            }
        }

        public bool AccessByFacialEnabled
        {
            get {
                return this._AccessByFacialEnabled;
            }

            set {
                this._AccessByFacialEnabled = value;
            }
        }

        public string PathMatcherConfig
        {
            get {
                return this._PathMatcherConfig;
            }

            set {
                this._PathMatcherConfig = value;
            }
        }

        public bool AccessByMRZEnabled
        {
            get {
                return this._AccessByMRZEnabled;
            }

            set {
                this._AccessByMRZEnabled = value;
            }
        }

        public bool AccessByBarcodeEnabled
        {
            get {
                return this._AccessByBarcodeEnabled;
            }

            set {
                this._AccessByBarcodeEnabled = value;
            }
        }

        public string PortCOMBarcode1
        {
            get {
                return this._PortCOMBarcode1;
            }

            set {
                this._PortCOMBarcode1 = value;
            }
        }

        public string PortCOMBarcode2
        {
            get
            {
                return this._PortCOMBarcode2;
            }

            set
            {
                this._PortCOMBarcode2 = value;
            }
        }

        public string Rele1
        {
            get
            {
                return this._Rele1;
            }

            set
            {
                this._Rele1 = value;
            }
        }

        public string Rele2
        {
            get
            {
                return this._Rele2;
            }

            set
            {
                this._Rele2 = value;
            }
        }

        public int QualityCapture
        {
            get {
                return _QualityCapture;
            }

            set {
                _QualityCapture = value;
            }
        }

        public string Theme
        {
            get {
                return this._Theme;
            }

            set {
                this._Theme = value;
            }
        }

        public int KioskType
        {
            get {
                return this._KioskType;
            }

            set {
                this._KioskType = value;
            }
        }

        public int KioskType1
        {
            get
            {
                return this._KioskType1;
            }

            set
            {
                this._KioskType1 = value;
            }
        }

        public int KioskType2
        {
            get
            {
                return this._KioskType2;
            }

            set
            {
                this._KioskType2 = value;
            }
        }

        public int TimeToShowResult
        {
            get {
                return this._TimeToShowResult;
            }

            set {
                this._TimeToShowResult = value;
            }
        }

        public int TimeToRefreshAutomaticBIRs
        {
            get
            {
                return this._TimeToRefreshAutomaticBIRs;
            }

            set
            {
                this._TimeToRefreshAutomaticBIRs = value;
            }
        }

        public bool ParseCedulaInBarcodeEnabled
        {
            get
            {
                return this._ParseCedulaInBarcodeEnabled;
            }

            set
            {
                this._ParseCedulaInBarcodeEnabled = value;
            }
        }

        public bool ShowReloj {
            get => _ShowReloj;
            set => _ShowReloj = value;
        }

        public int TimerToReinitLCB
        {
            get
            {
                return this._TimerToReinitLCB;
            }

            set
            {
                this._TimerToReinitLCB = value;
            }
        }

        public bool IsDummy 
        { 
            get => _IsDummy; 
            set => _IsDummy = value; 
        }
        public bool AccessByPINEnabled { get => _AccessByPINEnabled; set => _AccessByPINEnabled = value; }
        public int TimeToShowPINPanel { get => _TimeToShowPINPanel; set => _TimeToShowPINPanel = value; }
        public bool TakeFotoInMark { get => _TakeFotoInMark; set => _TakeFotoInMark = value; }
        public bool ShowVideoToTakeFotoInMark { get => _ShowVideoToTakeFotoInMark; set => _ShowVideoToTakeFotoInMark = value; }
        public bool ShowResultCustomized { get => _ShowResultCustomized; set => _ShowResultCustomized = value; }
        public int CameraIndexToUse { get => _CameraIndexToUse; set => _CameraIndexToUse = value; }
        public string CameraId1 { get => _CameraId1; set => _CameraId1 = value; }
        public string CameraId2 { get => _CameraId2; set => _CameraId2 = value; }


        #endregion Public Properties

        public KioskConfig()
        {
            _Name = "KioskDefault";
            _PathLoggerConfig = "Logger.cfg";
            _AccessByFingerprintEnabled = true;
            _SerialSensor1 = "H39150402301";
            _SerialSensor2 = "H39150402302";
            _QualityCapture = 50;
            _AccessByFacialEnabled = false;
            _PathMatcherConfig = "Matchers.cfg";
            _AccessByMRZEnabled = false;
            _AccessByBarcodeEnabled = true;
            _ParseCedulaInBarcodeEnabled = true;
            _PortCOMBarcode1 = "COM8";
            _PortCOMBarcode2 = "COM9";
            _Rele1 = "010";
            _Rele2 = "020";
            _KioskType = 2;
            _KioskType1 = 1;
            _KioskType2 = 2;
            _TimeToShowResult = 2000;
            _Theme = "Default";
            _TimeToRefreshAutomaticBIRs = 720; //12hs = 720 minutos => Si es 0 => No refresh automatico
            _ShowReloj = false;
            _TimerToReinitLCB = 0;
            _IsDummy = false;
            _AccessByPINEnabled = false;
            _TimeToShowPINPanel = 5000;
            _TakeFotoInMark = false;
            _ShowVideoToTakeFotoInMark = false;
            _CameraIndexToUse = 0;
            _CameraId1 = "usb#vid_1bcf&pid_28c1&mi_00#6&269dc56c&0&0000";
            _CameraId2 = "usb#vid_045e&pid_0812&mi_00#7&783613d&0&0000";
    }
    }
}
