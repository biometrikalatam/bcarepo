﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Aplication.src.Engines
{
    public class BarcodeEngine : IBarcodeEngine
    {
        public bool Initialized
        {
            get {
                throw new NotImplementedException();
            }

            set {
                throw new NotImplementedException();
            }
        }

        public string Name
        {
            get {
                throw new NotImplementedException();
            }

            set {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Lista de IBarcode implementados y configurados en BarcodeEngine.cfg, instanciados
        /// y que se recorreran en Parse con el codigo de barras leido para tratar de conseguir el IdRecognized 
        /// que será el Rut inicialmente.  
        /// </summary>
        public List<IBarcode> ListBarcodes
        {
            get {
                throw new NotImplementedException();
            }

            set {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Debe leer el archivo de BarcodeEngine.cfg e instanciar todas las dlls
        /// que implementen IBarcode y agregarlas aqui
        /// </summary>
        /// <returns></returns>
        public int Intialize()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Se debe implementar la forma de recorre la lista de Barcodes y tratar de parsear el codigo
        /// de barras pasado de parametro hasta encontrar un id valido.
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="returns"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int Parse(DynamicData parameters, out DynamicData returns, out string msg)
        {
            throw new NotImplementedException();
        }
    }
}
