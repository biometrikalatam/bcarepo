﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Kiosk.Aplication.src.Engines
{
    /// <summary>
    /// Implementacion de IMAskEngine. Similar a IActionEngine (Ver documentacion alli)
    /// Ahora es Dummy porque la marca se registra en el mismo BusinessRule en Autoclub
    /// </summary>
    public class ScreenEngine : IScreenEngine
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ScreenEngine)); 

        bool _Initialized;
        string _Name;
        IScreen _Screen;
        DynamicData _Config; 

        public bool Initialized
        {
            get {
                return this._Initialized;
            }

            set {
                this._Initialized = value;
            }
        }

        public string Name
        {
            get {
                return this._Name;
            }

            set {
                this._Name = value;
            }
        }

        public DynamicData Config
        {
            get {
                return this._Config;
            }

            set {
                this._Config = value;
            }
        }

        public IScreen Screen {
            get
            {
                return this._Screen;
            }

            set
            {
                this._Screen = value;
            }
        }


        
        public int DoShow(Form formParent, DynamicData parameters, out DynamicData returns, out string msg)
        {
            int ret = 0;
            returns = null;
            msg = null;
            try
            {
                if (!_Initialized)
                {
                    ret = Intialize();
                }

                if (ret == 0 && Screen != null)
                {
                    ret = Screen.DoShow(formParent, parameters, out returns, out msg);
                }
                else
                {
                    ret = Errors.IERR_INITIALIZING_MARKENGINE;
                    msg = "Politicas de Marcas no configurado [" + ret + "]";
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "DoShow Excp [" + ex.Message + "]";
                LOG.Error("ScreenEngine.DoShow Error: " + ex.Message);
            }
            return ret; ;
        }

        public int Intialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("ScreenEngine.Intialize IN...");
                if (!System.IO.File.Exists("ScreenEngine.cfg"))
                {
                    _Config = new DynamicData();
                    _Config.AddItem("ScreenDllPath", "Biometrika.Screen.BancoFalabella");
                    //_Parameters.Add("", "");

                    if (!Common.Utils.SerializeHelper.SerializeToFile(_Config, "ScreenEngine.cfg"))
                    {
                        LOG.Warn("MarkEngine.Initialize - No grabo condif en disco (ScreenEngine.cfg)");
                    }
                }
                else
                {
                    _Config = Common.Utils.SerializeHelper.DeserializeFromFile<DynamicData>("ScreenEngine.cfg");
                    _Config.Initialize();
                }

                if (_Config == null)
                {
                    LOG.Fatal("ScreenEngine.Initialize - Error leyendo ScreenEngine.cfg!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                else
                {
                    //Instancio dll
                    LOG.Debug("ScreenEngine.Initialize - Assembly = " + ((string)_Config.GetItem("ScreenDllPath")));
                    Assembly asmLogin = Assembly.Load((string)_Config.GetItem("ScreenDllPath"));
                    Type[] types = null;
                    LOG.Debug("ScreenEngine.Initialize - GetTypes IN => " + asmLogin.FullName);
                    types = asmLogin.GetTypes();

                    foreach (Type t in types)
                    {
                        try
                        {
                            object instance = null;
                            //ILoadBIRs oLoadBirs = null;
                            try
                            {
                                LOG.Debug("ScreenEngine.Initialize -  Activator.CreateInstance IN: " + t.FullName);
                                instance = Activator.CreateInstance(t);
                                LOG.Debug("ScreenEngine.Initialize Activator.CreateInstance IN: OK!");
                            }
                            catch (Exception exC)
                            {
                                LOG.Warn("ScreenEngine.Initialize Warn - Activator.CreateInstance(t)", exC);
                                instance = null;
                            }

                            if (instance != null)
                            {
                                if (instance is IScreen)
                                {
                                    if (!((IScreen)instance).Initialized)
                                    {
                                        ((IScreen)instance).Intialize();
                                    }
                                    Screen = (IScreen)instance;
                                    ret = 0;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LOG.Error("ScreenEngine.Initialize - Exc Initialize [" + ex.Message + "]");
                        }
                    }
                    Initialized = true;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ScreenEngine.Initialize Error: " + ex.Message);
            }
            LOG.Debug("ScreenEngine.Initialize OUT! ret = " + ret.ToString());
            return ret;
        }
    }
}
