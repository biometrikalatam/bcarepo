﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Aplication.src.Engines
{
    /// <summary>
    /// Implementa IActionEngine, definiendo que dll se instanciará para el manejo en 
    /// cada kiosko. En la key de configuracion ActionDllPath se indica el nombre de la DLL
    /// que implementa el IActionEngine para cada kiosko.
    /// </summary>
    public class ActionEngine : IActionEngine
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ActionEngine));
        bool _Initialized;
        string _Name;
        IAction _Action;
        DynamicData _Config;

        public bool Initialized
        {
            get {
                return this._Initialized;
            }

            set {
                this._Initialized = value;
            }
        }

        public string Name
        {
            get {
                return this._Name;
            }

            set {
                this._Name = value;
            }
        }

        public IAction Action
        {
            get {
                return this._Action;
            }

            set {
                this._Action = value;
            }
        }

        public DynamicData Config
        {
            get {
                return this._Config;
            }

            set {
                this._Config = value;
            }
        }

        public int DoAction(DynamicData parameters, out DynamicData returns, out string msg)
        {
            int ret = 0;
            returns = null;
            msg = null;
            try
            {
                LOG.Debug("ActionEngine.DoAction IN..."); 
                if (!_Initialized)
                {
                    ret = Intialize();
                }

                if (ret == 0 && Action != null)
                {
                    ret = Action.DoAction(parameters, out returns, out msg);
                }
                else
                {
                    ret = Errors.IERR_INITIALIZING_ACTIONENGINE;
                    msg = "Politicas de Actions no configurado [" + ret + "]";
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "DoAction Excp [" + ex.Message + "]";
                LOG.Error("ActionEngine.DoAction Error: " + ex.Message);
            }
            LOG.Debug("ActionEngine.DoAction Out => ret = " + ret);
            return ret;
        }

        /// <summary>
        /// Lee o crea el archivo de configuracion, y luego instancia la DLL configurada
        /// para que quede listo para operar. 
        /// </summary>
        /// <returns></returns>
        public int Intialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("ActionEngine.Intialize IN...");
                if (!System.IO.File.Exists("ActionEngine.cfg"))
                {
                    _Config = new DynamicData();
                    _Config.AddItem("ActionDllPath", "Biometrika.Action.AutoclubAntofagasta");
                    //_Parameters.Add("", "");

                    if (!Common.Utils.SerializeHelper.SerializeToFile(_Config, "ActionEngine.cfg"))
                    {
                        LOG.Warn("ActionEngine.Initialize - No grabo condif en disco (ActionEngine.cfg)");
                    }
                }
                else
                {
                    _Config = Common.Utils.SerializeHelper.DeserializeFromFile<DynamicData>("ActionEngine.cfg");
                    _Config.Initialize();
                }

                if (_Config == null)
                {
                    LOG.Fatal("ActionEngine.Initialize - Error leyendo MarkEngine.cfg!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                else
                {
                    //Instancio dll
                    LOG.Debug("ActionEngine.Initialize - Assembly = " + ((string)_Config.GetItem("ActionDllPath")));
                    Assembly asmLogin = Assembly.Load((string)_Config.GetItem("ActionDllPath"));
                    Type[] types = null;
                    LOG.Debug("ActionEngine.Initialize - GetTypes IN => " + asmLogin.FullName);
                    types = asmLogin.GetTypes();

                    //Intancia todos los tipos encontrados en la dll levantada
                    foreach (Type t in types)
                    {
                        try
                        {
                            object instance = null;
                            //ILoadBIRs oLoadBirs = null;
                            try
                            {
                                LOG.Debug("ActionEngine.Initialize -  Activator.CreateInstance IN: " + t.FullName);
                                instance = Activator.CreateInstance(t);
                                LOG.Debug("ActionEngine.Initialize Activator.CreateInstance IN: OK!");
                            }
                            catch (Exception exC)
                            {
                                LOG.Warn("ActionEngine.Initialize Warn - Activator.CreateInstance(t)", exC);
                                instance = null;
                            }

                            //Descarta los que no son implementacione sde IAction.
                            //Es similar en todos las implementaciones (IMark, IBusinessRule, etc).
                            if (instance != null)
                            {
                                if (instance is IAction)
                                {
                                    if (!((IAction)instance).Initialized)
                                    {
                                        ret = ((IAction)instance).Intialize();
                                    }
                                    if (ret == 0)
                                    {
                                        Action = (IAction)instance;
                                    } else
                                    {
                                        LOG.Warn("ActionEngine.Initialize Warn -((IAction)instance).Intialize() = " 
                                                  + ret.ToString());
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LOG.Error("ActionEngine.Initialize - Exc Initialize [" + ex.Message + "]");
                        }
                    }
                    Initialized = true;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ActionEngine.Initialize Error: " + ex.Message);
            }
            LOG.Debug("ActionEngine.Initialize OUT! ret = " + ret.ToString());
            return ret;
        }
    }
}
