﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Aplication.src.Engines
{
    /// <summary>
    /// Implementacion de IBuisessRuleEngine. Similar a IActionEngine (Ver documentacion alli)
    /// </summary>
    public class BusinessRuleEngine : IBusinessRulesEngine
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BusinessRuleEngine));

        bool _Initialized = false;
        string _Name;
        IBusinessRule _BusinessRule;
        private DynamicData _Config;

        public string Name
        {
            get {
                return _Name;
            }

            set {
                _Name = value;
            }
        }

        public IBusinessRule BusinessRule
        {
            get {
                return _BusinessRule;
            }

            set {
                _BusinessRule = value;
            }
        }

        public DynamicData Config
        {
            get {
                return this._Config;
            }

            set {
                this._Config = value;
            }
        }

        public bool Initialized
        {
            get {
                return this._Initialized;
            }

            set {
                this._Initialized = value;
            }
        }

        /// <summary>
        /// Dado los paramteros entregados, verifica que tiene acceso de acuerdo a la DLL configurada, y retorna
        /// resultado con parámetros de salida si aplica, o bien mensaje de error o de NO Acceso (Motivo)
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="accessresult"></param>
        /// <param name="returns"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int CheckBusinessRule(DynamicData parameters, out bool accessresult, out DynamicData returns, out string msg)
        {
            int ret = 0;
            accessresult = false;
            returns = null;
            msg = null;
            try
            {
                if (!_Initialized)
                {
                    ret = Intialize();
                }

                if (ret == 0 && BusinessRule != null)
                {
                    ret = BusinessRule.CheckBusinessRule(parameters, out accessresult, out returns, out msg);
                } else
                {
                    ret = Errors.IERR_INITIALIZING_BUSINESSRULEENGINE;
                    msg = "Politicas de control de acceso no configurado [" + ret + "]";
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "CheckBusinessRule Excp [" + ex.Message + "]";
                LOG.Error("BusinessRuleEngine.CheckBusinessRule Error: " + ex.Message);
            }
            return ret;
        }

        public int Dispose()
        {
            int ret = 0;
            try
            {
                BusinessRule.Dispose();
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BusinessRuleEngine.Dispose Error: " + ex.Message);
            }
            return ret;
        }

        public int Intialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("BusinessRuleEngine.Intialize IN...");
                if (!System.IO.File.Exists("BusinessRuleEngine.cfg"))
                {
                    _Config = new DynamicData();
                    _Config.AddItem("BusinessRuleDllPath", "Biometrika.BusinessRule.AutoclubAntofagasta");
                    //Parameters.Add("QueryCmd",
                    //        "SELECT TOP (200) bpiden.id AS id, bpiden.valueid, bpr.authenticationfactor, bpr.minutiaetype, bpr.bodypart, bpr.data " +
                    //        "  FROM bp_bir AS bpr INNER JOIN bp_identity AS bpiden ON bpr.identid = bpiden.id " +
                    //        " WHERE (bpr.minutiaetype = 7) AND (bpiden.companyidenroll = 7) AND bpiden.valueid = '21284415-2'");
                    //_Parameters.Add("URLService", "http://localhost/BS");
                    //_Parameters.Add("URLTimeout", 30000);

                    //_Parameters.Add("", "");
                    //_Parameters.Add("", "");
                    //_Parameters.Add("", "");
                    if (!Common.Utils.SerializeHelper.SerializeToFile(_Config, "BusinessRuleEngine.cfg"))
                    {
                        LOG.Warn("BusinessRuleEngine.Initialize - No grabo condif en disco (BusinessRuleEngine.cfg)");
                    }
                }
                else
                {
                    _Config = Common.Utils.SerializeHelper.DeserializeFromFile<DynamicData>("BusinessRuleEngine.cfg");
                    _Config.Initialize();
                }

                if (_Config == null)
                {
                    LOG.Fatal("BusinessRuleEngine.Initialize - Error leyendo BusinessRuleEngine.cfg!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                else
                {
                    //Instancio dll
                    LOG.Debug("BusinessRuleEngine.Intialize - Assembly = " + ((string)_Config.GetItem("BusinessRuleDllPath")));
                    Assembly asmLogin = Assembly.Load((string)_Config.GetItem("BusinessRuleDllPath"));
                    Type[] types = null;
                    LOG.Debug("BusinessRuleEngine.Intialize - GetTypes IN => " + asmLogin.FullName);
                    types = asmLogin.GetTypes();

                    foreach (Type t in types)
                    {
                        try
                        {
                            object instance = null;
                            //ILoadBIRs oLoadBirs = null;
                            try
                            {
                                LOG.Debug("BusinessRuleEngine.Intialize -  Activator.CreateInstance IN: " + t.FullName);
                                instance = Activator.CreateInstance(t);
                                LOG.Debug("BusinessRuleEngine.Intialize Activator.CreateInstance IN: OK!");
                            }
                            catch (Exception exC)
                            {
                                LOG.Warn("BusinessRuleEngine.Intialize Warn - Activator.CreateInstance(t)", exC);
                                instance = null;
                            }

                            if (instance != null)
                            {
                                if (instance is IBusinessRule)
                                {
                                    if (!((IBusinessRule)instance).Initialized)
                                    {
                                        ((IBusinessRule)instance).Initialize();
                                    }
                                    BusinessRule = (IBusinessRule)instance;
                                    ret = 0;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LOG.Error("BusinessRuleEngine.Intialize - Exc Initialize [" + ex.Message + "]");
                        }
                    }
                    Initialized = true;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BusinessRuleEngine.Intialize Error: " + ex.Message);
            }
            LOG.Debug("BusinessRuleEngine.Intialize OUT! ret = " + ret.ToString());
            return ret;
        }
    }
}
