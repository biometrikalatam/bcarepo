﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Aplication.src.Engines
{
    /// <summary>
    /// Implementacion de IMAskEngine. Similar a IActionEngine (Ver documentacion alli)
    /// Ahora es Dummy porque la marca se registra en el mismo BusinessRule en Autoclub
    /// </summary>
    public class MarkEngine : IMarkEngine
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(MarkEngine)); 

        bool _Initialized;
        string _Name;
        IMark _Mark;
        DynamicData _Config; 

        public bool Initialized
        {
            get {
                return this._Initialized;
            }

            set {
                this._Initialized = value;
            }
        }

        public string Name
        {
            get {
                return this._Name;
            }

            set {
                this._Name = value;
            }
        }

        public IMark Mark
        {
            get {
                return this._Mark;
            }

            set {
                this._Mark = value;
            }
        }

        public DynamicData Config
        {
            get {
                return this._Config;
            }

            set {
                this._Config = value;
            }
        }

        public int DoMark(DynamicData parameters, out DynamicData returns, out string msg)
        {
            int ret = 0;
            returns = null;
            msg = null;
            try
            {
                if (!_Initialized)
                {
                    ret = Intialize();
                }

                if (ret == 0 && Mark != null)
                {
                    ret = Mark.DoMark(parameters, out returns, out msg);
                }
                else
                {
                    ret = Errors.IERR_INITIALIZING_MARKENGINE;
                    msg = "Politicas de Marcas no configurado [" + ret + "]";
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "DoMark Excp [" + ex.Message + "]";
                LOG.Error("MarkEngine.DoMark Error: " + ex.Message);
            }
            return ret;
        }

        public int Intialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("MarkEngine.Intialize IN...");
                if (!System.IO.File.Exists("MarkEngine.cfg"))
                {
                    _Config = new DynamicData();
                    _Config.AddItem("MarkDllPath", "Biometrika.Mark.AutoclubAntofagasta");
                    //_Parameters.Add("", "");

                    if (!Common.Utils.SerializeHelper.SerializeToFile(_Config, "MarkEngine.cfg"))
                    {
                        LOG.Warn("MarkEngine.Initialize - No grabo condif en disco (MarkEngine.cfg)");
                    }
                }
                else
                {
                    _Config = Common.Utils.SerializeHelper.DeserializeFromFile<DynamicData>("MarkEngine.cfg");
                    _Config.Initialize();
                }

                if (_Config == null)
                {
                    LOG.Fatal("MarkEngine.Initialize - Error leyendo MarkEngine.cfg!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                else
                {
                    //Instancio dll
                    LOG.Debug("MarkEngine.Initialize - Assembly = " + ((string)_Config.GetItem("MarkDllPath")));
                    Assembly asmLogin = Assembly.Load((string)_Config.GetItem("MarkDllPath"));
                    Type[] types = null;
                    LOG.Debug("MarkEngine.Initialize - GetTypes IN => " + asmLogin.FullName);
                    types = asmLogin.GetTypes();

                    foreach (Type t in types)
                    {
                        try
                        {
                            object instance = null;
                            //ILoadBIRs oLoadBirs = null;
                            try
                            {
                                LOG.Debug("MarkEngine.Initialize -  Activator.CreateInstance IN: " + t.FullName);
                                instance = Activator.CreateInstance(t);
                                LOG.Debug("MarkEngine.Initialize Activator.CreateInstance IN: OK!");
                            }
                            catch (Exception exC)
                            {
                                LOG.Warn("MarkEngine.Initialize Warn - Activator.CreateInstance(t)", exC);
                                instance = null;
                            }

                            if (instance != null)
                            {
                                if (instance is IMark)
                                {
                                    if (!((IMark)instance).Initialized)
                                    {
                                        ((IMark)instance).Intialize();
                                    }
                                    Mark = (IMark)instance;
                                    ret = 0;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LOG.Error("MarkEngine.Initialize - Exc Initialize [" + ex.Message + "]");
                        }
                    }
                    Initialized = true;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("MarkEngine.Initialize Error: " + ex.Message);
            }
            LOG.Debug("MarkEngine.Initialize OUT! ret = " + ret.ToString());
            return ret;
        }
    }
}
