﻿using Biometrika.Kiosk.Aplication.src.Config;
using Biometrika.Kiosk.Aplication.src.Engines;
using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Kiosk.Aplication
{
    /// <summary>
    /// Unidad de trabajo que se utiliza desde UI para inicalizar todas las componentes
    /// y se realizan las acciones mas relevantes.
    /// </summary>
    public class UOWKioskAplication
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(UOWKioskAplication));

        //Globales
        public KioskConfig _CONFIG;
        public BusinessRuleEngine _BUSINESSRULE_ENGINE;
        public ActionEngine _ACTION_ENGINE;
        public MarkEngine _MARK_ENGINE;
        public ScreenEngine _SCREEN_ENGINE;
        public GenerateTicketSupport _SUPPORT;
        
        /// <summary>
        /// Inicializa el Kiosko, la unidad de Trabajo, e inicializa todos los Engines
        /// y queda preparado para operar.
        /// Lee la configuracion desde KioskConfig.cfg que debe estar en mismo directorio 
        /// que la dll Biometrika.Kiosk.Aplication.dll. Si no está el archivo entonces
        /// lo crea con parametros por default.
        /// </summary>
        /// <returns></returns>
        public int Initialization(out string msgfeedback)
        {
            int ret = 0;
            msgfeedback = "Status UOWKioskAplication.Initialization => ";
            try
            {
                LOG.Info("UOWKioskAplication.Initialization - IN...");

                if (!System.IO.File.Exists("Support.cfg"))
                {
                    LOG.Info("UOWKioskAplication.Initialization - Support.cfg no existe => Deshabilita envio automatico de soporte!");
                    msgfeedback += "SUPPORT=No Configurado!"; 
                }
                else
                {
                    LOG.Info("UOWKioskAplication.Initialization - Support.cfg deserializando..."); 
                    _SUPPORT = new GenerateTicketSupport();
                    bool bret = _SUPPORT.Initialize("Support.cfg");
                    LOG.Info("UOWKioskAplication.Initialization - _SUPPORT Inicializado => " + bret.ToString());
                    msgfeedback += "SUPPORT=Configurado OK!";
                }

                if (!System.IO.File.Exists("KioskConfig.cfg"))
                {
                    LOG.Info("UOWKioskAplication.Initialization - KioskConfig.cfg no existe => Lo genero y grabo!");
                    _CONFIG = new KioskConfig();
                    Common.Utils.SerializeHelper.SerializeToFile(_CONFIG, "KioskConfig.cfg");
                    msgfeedback += "|CONFIG=Creado OK!";
                } else
                {
                    LOG.Info("UOWKioskAplication.Initialization - KioskConfig.cfg deserializando...");
                    _CONFIG = Common.Utils.SerializeHelper.DeserializeFromFile<KioskConfig>("KioskConfig.cfg");
                    
                }

                if (_CONFIG == null)
                {
                    LOG.Fatal("UOWKioskAplication.Initialization - Error leyendo KioskConfig!");
                    msgfeedback += "|CONFIG=Leido NOOK!";
                    return Errors.IERR_DESERIALIZING_DATA;
                } else
                {
                    LOG.Info("UOWKioskAplication.Initialization - KioskConfig.cfg parseado OK!");
                    msgfeedback += "|CONFIG=Leido OK!";
                }

        //TODO - Control de lciencia!!

                LOG.Info("UOWKioskAplication.Initialization - Configurando BusinessRuleEngine...");
                _BUSINESSRULE_ENGINE = new BusinessRuleEngine();
                ret = _BUSINESSRULE_ENGINE.Intialize();
                if (ret < 0)
                {
                    LOG.Fatal("UOWKioskAplication.Initialization - Error inicializando BusinessRulesEngine!");
                    msgfeedback += "|BUSINESS_RULE=Init NOOK!";
                    return Errors.IERR_INITIALIZING_BUSINESSRULEENGINE;
                } else
                {
                    msgfeedback += "|BUSINESS_RULE=Init OK!";
                    LOG.Info("UOWKioskAplication.Initialization - BusinessRulesEngine Configured OK!");
                }

                LOG.Info("UOWKioskAplication.Initialization - Configurando ActionEngine...");
                _ACTION_ENGINE = new ActionEngine();
                ret = _ACTION_ENGINE.Intialize();
                if (ret < 0)
                {
                    LOG.Fatal("UOWKioskAplication.Initialization - Error inicializando ActionEngine!");
                    msgfeedback += "|ACTION_ENGINE=Init NOOK!";
                    return Errors.IERR_INITIALIZING_ACTIONENGINE;
                }
                else
                {
                    msgfeedback += "|ACTION_ENGINE=Init OK!";
                    LOG.Info("UOWKioskAplication.Initialization - ActionEngine Configured OK!");
                }

                LOG.Info("UOWKioskAplication.Initialization - Configurando MarkEngine...");
                _MARK_ENGINE = new MarkEngine();
                ret = _MARK_ENGINE.Intialize();
                if (ret < 0)
                {
                    LOG.Fatal("UOWKioskAplication.Initialization - Error inicializando MarkEngine!");
                    msgfeedback += "|MARK_ENGINE=Leido NOOK!";
                    return Errors.IERR_INITIALIZING_MARKENGINE;
                }
                else
                {
                    msgfeedback += "|MARK_ENGINE=Leido OK!";
                    LOG.Info("UOWKioskAplication.Initialization - MarkEngine Configured OK!");
                }

                //Added 11/2022 - BancoFalabella
                if (_CONFIG.ShowResultCustomized)
                {
                    LOG.Info("UOWKioskAplication.Initialization - Configurando ScreenEngine...");
                    _SCREEN_ENGINE = new ScreenEngine();
                    ret = _SCREEN_ENGINE.Intialize();
                    if (ret < 0)
                    {
                        LOG.Fatal("UOWKioskAplication.Initialization - Error inicializando MarkEngine!");
                        msgfeedback += "|SCREEN_ENGINE=Leido NOOK!";
                        return Errors.IERR_INITIALIZING_MARKENGINE;
                    }
                    else
                    {
                        msgfeedback += "|SCREEN_ENGINE=Leido OK!";
                        LOG.Info("UOWKioskAplication.Initialization - ScreenEngine Configured OK!");
                    }
                } else
                {
                    LOG.Info("UOWKioskAplication.Initialization - Skeep ScreenEngine OK!");
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msgfeedback += "|Excpetion INIT [" + ex.Message + "]";
                LOG.Error("UOWKioskAplication.Initialization - Error: " + ex.Message);
            }
            LOG.Info("UOWKioskAplication.Initialization - OUT! ret = " + ret.ToString());
            return ret;
        }

    }
}
