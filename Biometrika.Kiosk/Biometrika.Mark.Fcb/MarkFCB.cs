﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Mark.AutoclubAntofagasta
{
    /// <summary>
    /// Implementacion de IMark para FCB. POr ahora es Dummy porque el registro se hace en el mismo momento 
    /// del chequeo den BusinessRule.
    /// </summary>
    public class MarkFCB : IMark
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(MarkFCB));

        bool _Initialized; 
        string _Name;
        DynamicData _Config;

        public DynamicData Config
        {
            get {
                return _Config;
            }

            set {
                _Config = value;
            }
        }

        public bool Initialized
        {
            get {
                return this._Initialized;
            }

            set {
                this._Initialized = value;
            }
        }

        public string Name
        {
            get {
                return this._Name;
            }

            set {
                this._Name = value;
            }
        }

        public int DoMark(DynamicData parameters, out DynamicData returns, out string msg)
        {
            int ret = 0;
            returns = null;
            msg = null;
            try
            {
                //object o = (parameters != null) ? parameters.ParamsList["key"] : null;
                if (parameters == null)
                    LOG.Warn("MarckFCB.DoMark - Parametros NULO!");
                else
                {
                    if (parameters.ListDynamicDataItems != null)
                    foreach (DynamicDataItem item in parameters.ListDynamicDataItems)
                    {
                        if (item != null && !string.IsNullOrEmpty(item.key))
                        {
                                try
                                {
                                    LOG.Debug("MarckFCB.DoMark - key=" + item.key + " => value = " +
                                        (item.value != null ? item.value.ToString() : "NULL"));
                                }
                                catch (Exception)
                                {
                                }
                            
                        }
                    }
                    LOG.Debug("MarckFCB.DoMark - DONE! Sin acción real dado que marca se relaiza en BusinessRule!");
                }
            }
            catch (Exception ex) 
            {
                ret = -1;
                msg = "MarckFCB Excp [" + ex.Message + "]";
                LOG.Error("MarckFCB.DoMark - Excp Error: " + ex.Message);
            }
            return ret;
        }

        public int Intialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("MarckFCB.Intialize IN...");
                if (!System.IO.File.Exists("Biometrika.Mark.FCB.cfg"))
                {
                    Config = new DynamicData();
                    Config.AddItem("URLService", "http://localhost/BS");
                    Config.AddItem("URLTimeout", 30000);
                    //_Parameters.Add("", "");
                    if (!SerializeHelper.SerializeToFile(Config, "Biometrika.Mark.FCB.cfg"))
                    {
                        LOG.Warn("MarckFCB.Initialize - No grabo condif en disco (Biometrika.Mark.FCB.cfg)");
                    }
                }
                else
                {
                    Config = SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.Mark.FCB.cfg");
                    Config.Initialize();
                }

                if (Config == null)
                {
                    LOG.Fatal("MarckFCB.Initialize - Error leyendo MarckFCB!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                _Initialized = true;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("MarckFCB.Initialize - Excp Error: " + ex.Message);
            }
            return ret;
        }
    }
}
