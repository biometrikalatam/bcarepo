﻿using Bio.Core.Serialize;
using BioCore.SerialComm.Cedula;
using Biometrika.BusinessRule.SportlifeNorte.Helper;
using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Biometrika.BusinessRule.SportlifeNorte
{
    /// <summary>
    /// Implementacion de BusinessRule para Sportlife Norte. 
    /// Lo realiza conecgtandose a la API de IdealSur. 
    /// Usa URL: 
    ///   http://cercayrapido.com/chsportlife_dev/api/reservas.php?RESERVAID=14&SEDEID=101&FLUJO=SALIDA
    /// </summary>
    public class BusinessRuleSportlifeNorte : IBusinessRule
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BusinessRuleSportlifeNorte));

        Helper.HelperBioPortal _HELPER;

        int _Q = 0;

        bool _Initialized;
        DynamicData _Config;

        //Agrego esto para habilitar o no, un envio via thread de verificacion para mantener activos los servicios levantados
        //en BS7, sino l aprimera consulta despues de un periodo largo, tarfa mucho (7 segundos aprox, porque levanta el MatcherEngine y demas).
        //Lo dejo configrable proque si luego mejoramos eso en el mismo BS7 esto se puede deshabilitar.
        bool _EnablePingVerifyService = false;
        int _TimeThreadPingVerifyService = 3000000;
        Thread _tPingVerifyService = null;

        public DynamicData Config
        {
            get
            {
                return _Config;
            }

            set
            {
                _Config = value;
            }
        }

        public bool Initialized
        {
            get
            {
                return _Initialized;
            }

            set
            {
                _Initialized = value;
            }
        }

        /// <summary>
        /// Controla acceso en Sportlife Norte contra ERP de ellos      
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="accessresult"></param>
        /// <param name="returns"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int CheckBusinessRule(DynamicData parameters, out bool accessresult, out DynamicData returns, out string msg)
        {
            int ret = 0;
            accessresult = false;
            returns = null;
            msg = null;
            string typeid, valueid, reservaId, sedeId;
            bool isExit;
            bool registerAccess;
            DateTime dtStart, dtEnd, startstep, endstep;
            dtStart = DateTime.Now;
            /* 
                1.- Tomo parametro de entrada y chequeo integridad
                2.- Consumo servicio
                    2.1.- Chequeo y registro.
                3.- Retorno resultado
                 mensaje ingreso
                   {"ERROR":false,"RUT":"9999911-k","NOMBRE COMPLETO":"MONARDES VERSALOVIC HERNAN JESUS","FOTO":"","MENSAJE":"BIENVENIDO AL GIMNASIO MONARDES VERSALOVIC HERNAN JESUS"}
                   {"ERROR":false,"MENSAJE":"BIENVENIDO AL GIMNASIO PEREZ BANDERA LUIS","RUT":"1648975-1","NOMBRE_COMPLETO":"PEREZ BANDERA LUIS","FOTO":"data:image\/jpg;base64,\/9j\/4AAQSkZJRgABAQEAYABgAAD\/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL\/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL\/wAARCACqAKoDASIAAhEBAxEB\/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL\/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6\/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL\/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6\/9oADAMBAAIRAxEAPwD1zT4rAaVZZsrYk28Z\/wBSv90e1SMlj\/z5Wv8A35X\/AArFsbr\/AIltnz\/y7x\/+gintcnPWs1c00L8iWR\/5crT\/AL8J\/hVOa1syP+PO1\/78r\/hUP2g+tHm5FaJEspz2Vp\/z6W3\/AH5X\/Csyeztef9Ft\/wDv0v8AhWzKcis+cVdjNmPJa23\/AD7Qf9+l\/wAKqva2\/wDz7Qf9+l\/wrRlGKpyUrCKLW9v\/AM+8P\/ftf8Kia3g\/594f+\/Y\/wq01QtSsBXa3g\/594f8Av2KjNvB\/zwh\/79irDcVE1ICAwQf88Iv+\/Ypvkwf88Iv++BUd3fW9oMyyAHrtHU1nx+ILWSTaVdAT95hRYZp+RB\/zwi\/79ik8iD\/nhF\/37FO3ZHHfpRTsIb5EH\/PCL\/vgUCCD\/nhF\/wB8CnZpR1pDEEEH\/PCL\/v2KeLeD\/nhF\/wB+xSipBRYYz7PB\/wA8If8Av2Ksra2+0f6PD0\/55j\/CmgVaVfkX6UmirnVWU3\/EutOf+WEf\/oIqQy1m2cn+gWvP\/LGP\/wBBFTeZUobZbEvPWpFlqgr1Mr1pEllsvkVWlpwbio5DxWq1IZRmFUZK0JapSjrUsRTeoWqeSq71AyJqqXlwlrbtK5AA6e5q21YWsRS308drEM4Bc49hk\/pSGkc1dzvcTvISSzHmofKlxnYT+FbUOnBMFkP41bSNVOMVLkaKnch0XUd4+yyH5lHy59PStrNZd1pUiW\/9qQoQIGG8juK0w25Qw7jNUnciUbMdThTKcKZI9amUVEtWI1NIZIiVeSM7F47UyCInHFaqWx8teOwqWUQWj\/6Ba\/8AXFP\/AEEVMGqpZt\/oVt\/1xT\/0EVZWgZMpqZaijTJq5HESOlUmKw0dKH6VZ8lsdKikiIq1IlooSVSlFX5gRmqMg60NiKT1XerMnWqz1IELUnhdYZdXnvLhDKkT7Qm4rkEEYyO1DVHostvZQyefMsYd+M8k\/QDk1nNtR0NKa97Un8Sgm5SQxJEpGAqDAFc+CN1S+IfEMl\/fZjhKwR\/Kgx2Hf61kyagvk\/IwLnoKST6mvMjr7YmTRLiA8pIm1hWNpzE2MYb7y5U\/gcVn6Zf38cmQ6tG33kY4H6Ves3O+6jKhTHKc7Tkc88GrjoZTdy7SimU8VRmTIOavW6ZIqlH1rUs0ywpNjNWwtDIRxXVRaOxhQ7f4R2qHw9p\/nuvHHeu4VFVAoAwBip3KPFLM\/wCh23\/XFP8A0EVejGaz7Lmytv8Arkn\/AKCK07dcsKYF+1gLEcVvWmms4GFqLSLMyuvFdhDCsKBVFJDMT+xmx92qF5prID8tddUcsSyoVYVQjze6gKk57VkTLg12OsWPlO3FcpdLhjQmS0Zco5qq9WpuOKqPTAjVd8irkLk4LN0HuaqQQeTM1z5Z\/drJCsq8qxYEqQe3erYI8wZOAeM1t2ipJol1DLH5brMGKqNuByFI\/LP41lUlY2pRTVzzi9s7hboRHGeOAc80s2kFNPF0cKzgyA+q7tv\/ANetDUSi3bRpK7QhsZIAOO\/QU7UdXsbqRPLhMcMcXl7dxOcf56VV+xSiupm2GnTJcDLEA9SDxitZYBDPM6MGjlbcPXHQH8cGsu1VFnXJ3JnIB6Gt25ZXYSA\/NtVMewH\/ANemnd2JkrIipwqPNKGqjAtRda2tPGXGawo2xWzYSAOvNJoaPUfC6AQMfauhrlvC14mPLJ+8OK6mkho8OsT\/AKHbD\/pkn\/oIrZsuXFYVi3+iW\/8A1yT\/ANBFbVnJhxzQB6BoEY8ot7Vt1h+Hpg0RX2rcoQwooopgZGuxBoA1ef3y4Zq9B12UJagV55qEmWNTfUDHm61UerMrcmqjtVEkbVYfVJmZ5JpM\/IqfgOBVVjVS5DSQSKOTtNKSutSoyaZhXc5N0VB6+3Wq32SRjkRtt9xU2VeT5x1FRyT4YR7m+maSNVqJG5juVUA4X7xrZgkLxBieTWNNIPlRRg4zWvBGYoUQ9QMmmjOfYnzShqjzSg1ZmWEatC2m2sKylarMUmKTQI7XSNRMLqd2MV3kWvR+UmcZ2jNeQ2t0V71tJqD+WvzHoKh3KMSzbFpb\/wDXJP8A0EVqW0mGBrGs2\/0S3\/65J\/IVehkwetWI7rRL\/wAp15rtIpVmQMpzmvJ7O7MZBzXT2GstGB81RsWdpTJJFiQsxwBWF\/b\/AMvUVm3ustICC3FHMINd1ESsQDx2rjLuXJNX726LknNYlxJ1OaEJlaVveqztVeW9YsdqfTLYqnJeSZ+9Gv61Qi8zZotiPtcWeQXAI9jxWZHdO0wBlyPTGBV6GRTcQqpyxcYAHvSY1uc\/qUX2K7ePPAY4NUDMC244yO9dHr9ssk8hYZ+Y1yslod2A5xUwkmtTWSaehPC\/n3Kj06mumfnawHVR\/hXO2kQiwo9ck1c1S+e3srfy3w28jPqKpaysiWvduaZGOc0m5R\/GPwrmDq7llaaMOvIGOMVZTWQRnbge1a8jMbo3vNQHufwqRblF9B+NYC6mjclm\/KrMMyTp8jZx1zT9n3FzG4t+o6H9KtLqjbR16VhIOQAa1E026KKQo5HrR7NBzFu1Y\/ZLf\/rkv8hVtGPpWTBeEWsABA\/dr0HsKcbuRuAxqUirnQQyEd6uR3qxjmRR9TXLRq78lmORzk1diTH\/AOqpcSkzoDqqY4fP0FQvqJborn68VUgt95AOTXX6BoMVw4LoMDrxS5Quck80rjiM\/hk1Qn80g5ib8eK9nfQLHySqxAHHWuH12ytLNm8ySNMf3mA\/nTUbibseY3ML+YfkOPTNVzbv2AFb19dWCscXCN\/u8\/yrIkvocnYrsPXGBWigiXIiSBg2Watjw2bNtRkLPuuIh+6U9yeCfw\/rXM318ZGVEyFznr1qG2vZrO7iuYX2yxtlT2+lE6V4tIcZ2kmzqtejKXLL\/eGcVy8kW2TkV2lvd6d4mdX8wQXIA3wk9\/VfUVn6po3l3O23IfjkE1xRvF8r3Ox+9qjm0GCT6d6yNRuTPLsz8qcCtXWHbTlMJK+c3RQenvXOZJrqpR6nPVlbQN2UK+nNMRyDkGkBw\/1pNpHHoa3sYlpZDtyKu207RzDB7c+9Zi5BxVyHJlzxTitSWdPbr5qhh0P6V1cEzi3jBTOEHf2rP8EaR\/bV\/HZeYqM+SGbkcDJ\/SvVl+HsIUD7UvA\/55n\/GulQpRX7yVrmDlUfwK55Jb2yi0g\/65r\/IVYWFEGcCrtpYubK3ODzEn\/oIqQ2DY6GpVJ22G6iKLSLAI9ysSy5AVc0rXjxJ5htJdvqxArSnge2jt5Amd0XGfrWLf3k7xtEwQIfRaxlSadzWNRPYnXxDPER5dvCv+8S1XIfGeuKNkV4sA\/6ZRKD+ZzXJu5pY5SDmsrWLuetw+FvFOrWayXmpyJvUEJLcN0PqF4rzTxBZzaZqlxZzsjTQttZkbcPXr+NXF8TamtqIVvrgRgYCiU4x+dYF3K0rMzHJPWhi0IU4DSSE7OgGepqvLNnJGMAdBTbmUqqrnoKz5JTgj2q4qwXFkmL4J9cUhY9c1WDExd\/vVITxVLUQ7zWU5BII6EHpUy61qcLEx3THjHz\/ADfzqkzYFRluM1MknuUpNbCSSSzTPJK5d2OSxOSajYccU0NnJz3pSaEhXIWzuBpxOQD601l603260NFXQ8E5q3C\/zZqlyKerkEdRSWgnY7Xw7rEmnXMU8TlXjIKkHoa9GHj7USAftT889a8Tt5yp4JrZS\/YIo3dq64VdLNXOeVJN6n0RpfhZW0uzcugzBGen+yKvf8IpAR80xH0WqNl4oSLSrNFC8W8ff\/ZFVbrxbNg7HC\/SlFYl9bGTdFPZs1J\/CdhMFSedgsa4GCAT9a8w8ZWOm2OoGDT3eRVHzsxyCfb9a6DXdalawspfMOXV8nPXmuB1C5Mzkk5qndR9+Vyo76KxlyEZqLcRTpDzUJrkZ0E6ktxV6bQtSWxa8On3X2dRlpTCwUD1zis+2neCdJUOHRgynHQiui1fxvresWZtbm9cwuMOigKG+uOtJJDOJvWxMw7VmSPVy8fMrH3NZkjc035Au49CfLIBx81SZwABUKnC7fTrTi3FCBgx5znp0qGR8DFOY+lV5H7UxoEb3pSwxwc1Crc0\/OKIvQY8sNtRkjNITTd3NFyWyUNxSM3IpF6ClVGlmCopZj0ApPYNWyxA3vWgG+UfSrFh4fkcKZpAmf4V5NdCnhez2LmR84\/vVm60I7mypSaOlttQY2duM9IkH\/joq5FBPcwebv2qfu5HWub0udJTaRTSiGJgitLt3bBgc4716beWejR+H7V7C\/Scp+7Z143kdTjtXS6jSOOUbK6OO1mRhomnhuGBkU\/mK5eV8k1ueIrnzfDdpcRH5ftMqg+2f\/rVw0105z8xqZSLijRkdQOWA\/GoGuIx\/EKymmY0zexrNsqxqi7TPANaGk2N9rl59l0+BZJQpchpFQBR1JLEDuK5xQ7Gp4hLFkq2N3B57VcUr6iZDqCNFdTRtjcjlTg5GQazJGrRvMtM575Pas2UHqRUtauxaHqflz60FuKhjYlcHtSucDOaa2uFhHkqBie9DMajJ4qW7gOyQeKfk1Dk5qQGheYncUnio6Uk4pozn6UAiVHNaGluBde5NZoyBVqwfbdr71M3dWKh8R29tIcCtZZTsX6etYVq+VFaSy\/KPpXFNXO6OxBbROtvCR02L\/Kup0XQbm\/0ySVLvyo5CV27MkEcZ61TtkT7FD8q\/wCqXt7Cu+8JIn\/CPj5F\/wBY3avVlGyueVzHD+I9LGm+CktRIX8i6+8RjO6vOpAua9h8dqv\/AAjlx8o\/4+V7V5ReKon+6OnpWcloXFlA4HanLz0FPwPSpogMdB0pRQ2zY0TREvo2muZDFGOF2jJY0moaSbOUFWLRE5BIr0HwpbwN4dhZoYyfs7nJUdd1M8YxxjQ7UhFB89uQP9gV6fsKfs1pqcSqTdR66HlEllcXDF0hcqTkE8VVm0q7AP7oD\/gQr1BYYvscf7tPuj+EVz2oqoY4UD8K8WVaXM0erGkrXPO2R4Jisi7TUcjZPFaesAfaulZbdR9atN2M5aMic802pJPvU3tVMi42ng8Uz0qRaaVwY0+wpF608\/epvanLsPoLx1qS2fbcJ9aYfu0sf+tT\/eqWtConYWUgKjntWmrDaOR0rIsQNg4rWUDaOB0rhlud0dj\/2Q=="}

                 mensaje salida
                    {"ERROR":false,"RUT":"9999911-k","NOMBRE COMPLETO":"MONARDES VERSALOVIC HERNAN JESUS","FOTO":"","MENSAJE":"GRACIAS POR VISITARNOS MONARDES VERSALOVIC HERNAN JESUS"}
                    {"ERROR":false,"MENSAJE":"GRACIAS POR VISITARNOS PEREZ BANDERA LUIS","RUT":"1648975-1","NOMBRE_COMPLETO":"PEREZ BANDERA LUIS","FOTO":"data:image\/jpg;base64,\/9j\/4AAQSkZJRgABAQEAYABgAAD\/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL\/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL\/wAARCACqAKoDASIAAhEBAxEB\/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL\/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6\/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL\/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6\/9oADAMBAAIRAxEAPwD1zT4rAaVZZsrYk28Z\/wBSv90e1SMlj\/z5Wv8A35X\/AArFsbr\/AIltnz\/y7x\/+gintcnPWs1c00L8iWR\/5crT\/AL8J\/hVOa1syP+PO1\/78r\/hUP2g+tHm5FaJEspz2Vp\/z6W3\/AH5X\/Csyeztef9Ft\/wDv0v8AhWzKcis+cVdjNmPJa23\/AD7Qf9+l\/wAKqva2\/wDz7Qf9+l\/wrRlGKpyUrCKLW9v\/AM+8P\/ftf8Kia3g\/594f+\/Y\/wq01QtSsBXa3g\/594f8Av2KjNvB\/zwh\/79irDcVE1ICAwQf88Iv+\/Ypvkwf88Iv++BUd3fW9oMyyAHrtHU1nx+ILWSTaVdAT95hRYZp+RB\/zwi\/79ik8iD\/nhF\/37FO3ZHHfpRTsIb5EH\/PCL\/vgUCCD\/nhF\/wB8CnZpR1pDEEEH\/PCL\/v2KeLeD\/nhF\/wB+xSipBRYYz7PB\/wA8If8Av2Ksra2+0f6PD0\/55j\/CmgVaVfkX6UmirnVWU3\/EutOf+WEf\/oIqQy1m2cn+gWvP\/LGP\/wBBFTeZUobZbEvPWpFlqgr1Mr1pEllsvkVWlpwbio5DxWq1IZRmFUZK0JapSjrUsRTeoWqeSq71AyJqqXlwlrbtK5AA6e5q21YWsRS308drEM4Bc49hk\/pSGkc1dzvcTvISSzHmofKlxnYT+FbUOnBMFkP41bSNVOMVLkaKnch0XUd4+yyH5lHy59PStrNZd1pUiW\/9qQoQIGG8juK0w25Qw7jNUnciUbMdThTKcKZI9amUVEtWI1NIZIiVeSM7F47UyCInHFaqWx8teOwqWUQWj\/6Ba\/8AXFP\/AEEVMGqpZt\/oVt\/1xT\/0EVZWgZMpqZaijTJq5HESOlUmKw0dKH6VZ8lsdKikiIq1IlooSVSlFX5gRmqMg60NiKT1XerMnWqz1IELUnhdYZdXnvLhDKkT7Qm4rkEEYyO1DVHostvZQyefMsYd+M8k\/QDk1nNtR0NKa97Un8Sgm5SQxJEpGAqDAFc+CN1S+IfEMl\/fZjhKwR\/Kgx2Hf61kyagvk\/IwLnoKST6mvMjr7YmTRLiA8pIm1hWNpzE2MYb7y5U\/gcVn6Zf38cmQ6tG33kY4H6Ves3O+6jKhTHKc7Tkc88GrjoZTdy7SimU8VRmTIOavW6ZIqlH1rUs0ywpNjNWwtDIRxXVRaOxhQ7f4R2qHw9p\/nuvHHeu4VFVAoAwBip3KPFLM\/wCh23\/XFP8A0EVejGaz7Lmytv8Arkn\/AKCK07dcsKYF+1gLEcVvWmms4GFqLSLMyuvFdhDCsKBVFJDMT+xmx92qF5prID8tddUcsSyoVYVQjze6gKk57VkTLg12OsWPlO3FcpdLhjQmS0Zco5qq9WpuOKqPTAjVd8irkLk4LN0HuaqQQeTM1z5Z\/drJCsq8qxYEqQe3erYI8wZOAeM1t2ipJol1DLH5brMGKqNuByFI\/LP41lUlY2pRTVzzi9s7hboRHGeOAc80s2kFNPF0cKzgyA+q7tv\/ANetDUSi3bRpK7QhsZIAOO\/QU7UdXsbqRPLhMcMcXl7dxOcf56VV+xSiupm2GnTJcDLEA9SDxitZYBDPM6MGjlbcPXHQH8cGsu1VFnXJ3JnIB6Gt25ZXYSA\/NtVMewH\/ANemnd2JkrIipwqPNKGqjAtRda2tPGXGawo2xWzYSAOvNJoaPUfC6AQMfauhrlvC14mPLJ+8OK6mkho8OsT\/AKHbD\/pkn\/oIrZsuXFYVi3+iW\/8A1yT\/ANBFbVnJhxzQB6BoEY8ot7Vt1h+Hpg0RX2rcoQwooopgZGuxBoA1ef3y4Zq9B12UJagV55qEmWNTfUDHm61UerMrcmqjtVEkbVYfVJmZ5JpM\/IqfgOBVVjVS5DSQSKOTtNKSutSoyaZhXc5N0VB6+3Wq32SRjkRtt9xU2VeT5x1FRyT4YR7m+maSNVqJG5juVUA4X7xrZgkLxBieTWNNIPlRRg4zWvBGYoUQ9QMmmjOfYnzShqjzSg1ZmWEatC2m2sKylarMUmKTQI7XSNRMLqd2MV3kWvR+UmcZ2jNeQ2t0V71tJqD+WvzHoKh3KMSzbFpb\/wDXJP8A0EVqW0mGBrGs2\/0S3\/65J\/IVehkwetWI7rRL\/wAp15rtIpVmQMpzmvJ7O7MZBzXT2GstGB81RsWdpTJJFiQsxwBWF\/b\/AMvUVm3ustICC3FHMINd1ESsQDx2rjLuXJNX726LknNYlxJ1OaEJlaVveqztVeW9YsdqfTLYqnJeSZ+9Gv61Qi8zZotiPtcWeQXAI9jxWZHdO0wBlyPTGBV6GRTcQqpyxcYAHvSY1uc\/qUX2K7ePPAY4NUDMC244yO9dHr9ssk8hYZ+Y1yslod2A5xUwkmtTWSaehPC\/n3Kj06mumfnawHVR\/hXO2kQiwo9ck1c1S+e3srfy3w28jPqKpaysiWvduaZGOc0m5R\/GPwrmDq7llaaMOvIGOMVZTWQRnbge1a8jMbo3vNQHufwqRblF9B+NYC6mjclm\/KrMMyTp8jZx1zT9n3FzG4t+o6H9KtLqjbR16VhIOQAa1E026KKQo5HrR7NBzFu1Y\/ZLf\/rkv8hVtGPpWTBeEWsABA\/dr0HsKcbuRuAxqUirnQQyEd6uR3qxjmRR9TXLRq78lmORzk1diTH\/AOqpcSkzoDqqY4fP0FQvqJborn68VUgt95AOTXX6BoMVw4LoMDrxS5Quck80rjiM\/hk1Qn80g5ib8eK9nfQLHySqxAHHWuH12ytLNm8ySNMf3mA\/nTUbibseY3ML+YfkOPTNVzbv2AFb19dWCscXCN\/u8\/yrIkvocnYrsPXGBWigiXIiSBg2Watjw2bNtRkLPuuIh+6U9yeCfw\/rXM318ZGVEyFznr1qG2vZrO7iuYX2yxtlT2+lE6V4tIcZ2kmzqtejKXLL\/eGcVy8kW2TkV2lvd6d4mdX8wQXIA3wk9\/VfUVn6po3l3O23IfjkE1xRvF8r3Ox+9qjm0GCT6d6yNRuTPLsz8qcCtXWHbTlMJK+c3RQenvXOZJrqpR6nPVlbQN2UK+nNMRyDkGkBw\/1pNpHHoa3sYlpZDtyKu207RzDB7c+9Zi5BxVyHJlzxTitSWdPbr5qhh0P6V1cEzi3jBTOEHf2rP8EaR\/bV\/HZeYqM+SGbkcDJ\/SvVl+HsIUD7UvA\/55n\/GulQpRX7yVrmDlUfwK55Jb2yi0g\/65r\/IVYWFEGcCrtpYubK3ODzEn\/oIqQ2DY6GpVJ22G6iKLSLAI9ysSy5AVc0rXjxJ5htJdvqxArSnge2jt5Amd0XGfrWLf3k7xtEwQIfRaxlSadzWNRPYnXxDPER5dvCv+8S1XIfGeuKNkV4sA\/6ZRKD+ZzXJu5pY5SDmsrWLuetw+FvFOrWayXmpyJvUEJLcN0PqF4rzTxBZzaZqlxZzsjTQttZkbcPXr+NXF8TamtqIVvrgRgYCiU4x+dYF3K0rMzHJPWhi0IU4DSSE7OgGepqvLNnJGMAdBTbmUqqrnoKz5JTgj2q4qwXFkmL4J9cUhY9c1WDExd\/vVITxVLUQ7zWU5BII6EHpUy61qcLEx3THjHz\/ADfzqkzYFRluM1MknuUpNbCSSSzTPJK5d2OSxOSajYccU0NnJz3pSaEhXIWzuBpxOQD601l603260NFXQ8E5q3C\/zZqlyKerkEdRSWgnY7Xw7rEmnXMU8TlXjIKkHoa9GHj7USAftT889a8Tt5yp4JrZS\/YIo3dq64VdLNXOeVJN6n0RpfhZW0uzcugzBGen+yKvf8IpAR80xH0WqNl4oSLSrNFC8W8ff\/ZFVbrxbNg7HC\/SlFYl9bGTdFPZs1J\/CdhMFSedgsa4GCAT9a8w8ZWOm2OoGDT3eRVHzsxyCfb9a6DXdalawspfMOXV8nPXmuB1C5Mzkk5qndR9+Vyo76KxlyEZqLcRTpDzUJrkZ0E6ktxV6bQtSWxa8On3X2dRlpTCwUD1zis+2neCdJUOHRgynHQiui1fxvresWZtbm9cwuMOigKG+uOtJJDOJvWxMw7VmSPVy8fMrH3NZkjc035Au49CfLIBx81SZwABUKnC7fTrTi3FCBgx5znp0qGR8DFOY+lV5H7UxoEb3pSwxwc1Crc0\/OKIvQY8sNtRkjNITTd3NFyWyUNxSM3IpF6ClVGlmCopZj0ApPYNWyxA3vWgG+UfSrFh4fkcKZpAmf4V5NdCnhez2LmR84\/vVm60I7mypSaOlttQY2duM9IkH\/joq5FBPcwebv2qfu5HWub0udJTaRTSiGJgitLt3bBgc4716beWejR+H7V7C\/Scp+7Z143kdTjtXS6jSOOUbK6OO1mRhomnhuGBkU\/mK5eV8k1ueIrnzfDdpcRH5ftMqg+2f\/rVw0105z8xqZSLijRkdQOWA\/GoGuIx\/EKymmY0zexrNsqxqi7TPANaGk2N9rl59l0+BZJQpchpFQBR1JLEDuK5xQ7Gp4hLFkq2N3B57VcUr6iZDqCNFdTRtjcjlTg5GQazJGrRvMtM575Pas2UHqRUtauxaHqflz60FuKhjYlcHtSucDOaa2uFhHkqBie9DMajJ4qW7gOyQeKfk1Dk5qQGheYncUnio6Uk4pozn6UAiVHNaGluBde5NZoyBVqwfbdr71M3dWKh8R29tIcCtZZTsX6etYVq+VFaSy\/KPpXFNXO6OxBbROtvCR02L\/Kup0XQbm\/0ySVLvyo5CV27MkEcZ61TtkT7FD8q\/wCqXt7Cu+8JIn\/CPj5F\/wBY3avVlGyueVzHD+I9LGm+CktRIX8i6+8RjO6vOpAua9h8dqv\/AAjlx8o\/4+V7V5ReKon+6OnpWcloXFlA4HanLz0FPwPSpogMdB0pRQ2zY0TREvo2muZDFGOF2jJY0moaSbOUFWLRE5BIr0HwpbwN4dhZoYyfs7nJUdd1M8YxxjQ7UhFB89uQP9gV6fsKfs1pqcSqTdR66HlEllcXDF0hcqTkE8VVm0q7AP7oD\/gQr1BYYvscf7tPuj+EVz2oqoY4UD8K8WVaXM0erGkrXPO2R4Jisi7TUcjZPFaesAfaulZbdR9atN2M5aMic802pJPvU3tVMi42ng8Uz0qRaaVwY0+wpF608\/epvanLsPoLx1qS2fbcJ9aYfu0sf+tT\/eqWtConYWUgKjntWmrDaOR0rIsQNg4rWUDaOB0rhlud0dj\/2Q=="}
                
                 error
                    {"ERROR":true,,"MENSAJE":"PERSONA YA SALIO"}  3
                    {"ERROR":true,"MENSAJE":"LA RESERVA NO CORRESPONDE A ESTA SEDE"}
            */
            try
            {
                returns = new DynamicData();
                startstep = DateTime.Now;
                LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule IN...");
                //1.- Tomo parametro de entrada y chequeo integridad
                if (parameters == null || parameters.DynamicDataItems == null ||
                    !parameters.DynamicDataItems.ContainsKey("idRecognized") ||
                     !parameters.DynamicDataItems.ContainsKey("checktype") ||
                     !parameters.DynamicDataItems.ContainsKey("idDevice"))
                {
                    ret = Errors.IERR_BAD_PARAMETER;
                    msg = "Error chequeando BusinessRule - Parametros nulos o faltantes...";
                    LOG.Warn("BusinessRuleSportlifeNorte.CheckBusinessRule - " + msg);
                    return ret;
                }
                else //Estamso ok => Tomo valores
                {
                    LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Parseando idRecognized=" + (string)parameters.DynamicDataItems["idRecognized"]);
                    LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Parseando idDevice=" + (string)parameters.DynamicDataItems["idDevice"]);
                    typeid = "RUT"; //(string)parameters.DynamicDataItems["typeid"];

                    string sAux = null;
                    //Verifico que no venga en Base64, si viene lo decodifico sino tomo lo que viene
                    try
                    {
                        sAux = Encoding.UTF8.GetString(
                                        Convert.FromBase64String(((string)parameters.DynamicDataItems["idRecognized"])));

                    }
                    catch (Exception ex)
                    {
                        sAux = ((string)parameters.DynamicDataItems["idRecognized"]);
                    }
                    //string sAux = (string)parameters.DynamicDataItems["idRecognized"];
                    valueid = null;
                    if (!string.IsNullOrEmpty(sAux) && sAux.Length > 15) //Es codigo de barras desde cedula
                    {
                        LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Parsea cédula recibida...");
                        //if (sAux.StartsWith("h")) //Es QR cedula nueva
                        //{
                        //    LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Parsea cedula nueva...");
                        //    byte[] rawSample = Convert.FromBase64String(((string)parameters.DynamicDataItems["idRecognized"]));
                        //    string lect = Encoding.UTF7.GetString(rawSample, 0, rawSample.Length - 2);
                        //    CedulaTemplate2 cedula = new BioCore.SerialComm.Cedula.CedulaTemplate2(lect);
                        //    if (cedula != null)
                        //    {
                        //        valueid = cedula.Rut;
                        //    }
                        //}
                        //else //Es PDF417 cedula vieja
                        //{
                        //    LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Parsea cedula antigua...");
                        //    CedulaTemplate cedulabarcode = 
                        //        new CedulaTemplate(Convert.FromBase64String(((string)parameters.DynamicDataItems["idRecognized"])));
                        //    if (cedulabarcode != null)
                        //    {
                        //        valueid = cedulabarcode.Rut;
                        //    }
                        //}
                        //Verifico que no venga en Base64, si viene lo decodifico sino tomo lo que viene
                        CedulaTemplate cedulabarcode = null;
                        try
                        {
                            cedulabarcode = new CedulaTemplate(Convert.FromBase64String(((string)parameters.DynamicDataItems["idRecognized"])));
                        }
                        catch (Exception ex)
                        {
                            cedulabarcode = new CedulaTemplate(Encoding.UTF7.GetBytes(((string)parameters.DynamicDataItems["idRecognized"])));
                        }
                        if (cedulabarcode != null)
                        {
                            valueid = FormattingTaxid(cedulabarcode.Rut);
                        }
                    }
                    else //Es QR desde celular (de la reserva)
                    {
                        LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Usa reserva recibida => " + sAux.Trim());
                        valueid = sAux.Trim(); // info[0].Trim();
                    }

                    LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Setted valueid = " + 
                                (string.IsNullOrEmpty(valueid)?"NULL":valueid));
                    //string[] info = sAux.Split('_');
                    //if (info == null || info.Length < 2)
                    //{
                    //    ret = Errors.IERR_PARSE_CEDULA;
                    //    msg = "Error chequeando BusinessRule - Error parseando QR leido...";
                    //    LOG.Warn("BusinessRuleSportlifeNorte.CheckBusinessRule - " + msg);
                    //    return ret;
                    //}
                    //valueid = sAux.Trim(); // info[0].Trim();
                    //reservaId = info[1].Trim();

                    if (string.IsNullOrEmpty(valueid)) //(!IsNumber(reservaId))
                    {
                        ret = Errors.IERR_PARSE_CEDULA;
                        msg = "QR de formato Incorrecto!";
                        LOG.Warn("BusinessRuleSportlifeNorte.CheckBusinessRule - " + msg);
                        return ret;
                    }

                    isExit = ((int)parameters.DynamicDataItems["checktype"] == 2);
                    LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Chequeando BusinessRule de typeid/valueid => " +
                                typeid + "/" + valueid + " - CheckingType=" +
                                parameters.DynamicDataItems["checktype"].ToString() + " - rut = " + valueid + 
                                " - SedeId = " + (string)Config.DynamicDataItems["ServicePointName"]);
                }

                //2.- Consumo servicio
                //Detecto sentido de marca
                string sentido = "SALIDA";
                if (((string)parameters.DynamicDataItems["idDevice"]).Equals(Config.DynamicDataItems["PortCOMBarcodeIn"]) ||
                    ((string)parameters.DynamicDataItems["idDevice"]).Equals(Config.DynamicDataItems["SerialSensorIn"]))
                {
                    sentido = "INGRESO";
                }

                string sURL = Config.DynamicDataItems["ServiceURL"] + "?RUT=" + valueid +
                    "&SEDEID=" + (string)Config.DynamicDataItems["ServicePointName"] + "&FLUJO=" +
                    sentido;
                //(parameters.DynamicDataItems["checktype"].ToString().Equals("1") ? "INGRESO" : "SALIDA");

                LOG.Debug("BusinessRuleFersanatura.CheckBusinessRule - LLama a " + sURL + "...");
                var client = new RestClient(sURL);
                client.Timeout = (int)Config.DynamicDataItems["ServiceTimeout"];
                var request = new RestRequest(Method.GET);
                LOG.Debug("BusinessRuleFersanatura.CheckBusinessRule - call execute...");
                IRestResponse response = client.Execute(request);

                //Consumo servicio


                endstep = DateTime.Now;
                LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Total Time ERP Service = " + (endstep - startstep).TotalMilliseconds.ToString());
                returns.AddItem("TimeERPService", (endstep - startstep).TotalMilliseconds.ToString());

                startstep = DateTime.Now;
                
                
                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Parseando resultado...");
                    AccessModelR oResponse = JsonConvert.DeserializeObject<AccessModelR>(response.Content);
                    if (oResponse != null)
                    {
                        //Si es true => Hubo error significa que no puede acceder => Solo mostramos el msg 
                        if (oResponse.ERROR)
                        {
                            ret = 0; // Errors.IERR_IDENTITY_NOT_FOUND;
                            msg = oResponse.MENSAJE;
                            returns.AddItem("Name", "RUT " + valueid);
                            //returns.AddItem("Msg", oResponse.MENSAJE);
                            returns.AddItem("Code", 0);
                            LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Response No Registrado! [ret = " +
                                ret + " - " + msg + "]");
                        }
                        else
                        {
                            returns.AddItem("Msg", oResponse.MENSAJE);
                            //En este punto por default es 1 => Puede acceder. Ahora chequea si esta habilitado MAFType = 0-No Verify | 1-Verify Finger | 2-Verify Face
                            int _CanAccess = 1;
                            //OPCION 01-2023
                            // Si entra aca y del ERP indica que si o si debe ser biometrico => voy a BioPortal a verificar con RUT + Foto que saco.
                            //  - Si es negativo, pide reintente
                            //  - Si es positivo => Muestra foto de persona verificada en lugar de la tomada (O ambas?) y sigue con el ok
                            //Si es doble factor y aprobado desde regla de negocio y desde ERP dice que no puede marcar SOLO con QR 
                            //   Desde ERP viene TIPO_ACCESO = SOLO QR | SOLO BIOMETRIA
                            if (Config.DynamicDataItems.ContainsKey("MFAType") && 
                                _CanAccess == 1 && 
                                (!string.IsNullOrEmpty(oResponse.TIPO_ACCESO) && !oResponse.TIPO_ACCESO.Equals("2")) &&
                                (Config.DynamicDataItems.ContainsKey("MFAControlType") && 
                                MustControlMFA(sentido, (int)Config.DynamicDataItems["MFAControlType"])))
                            {
                                VerifyResponse responseV = null;
                                int _MAFType = Convert.ToInt32(Config.DynamicDataItems["MFAType"].ToString());
                                switch (_MAFType)
                                {
                                    case 1:
                                        if (parameters.DynamicDataItems.ContainsKey("wsq"))
                                        {
                                            responseV = _HELPER.Verify(valueid, Config.DynamicDataItems["ServicePointName"].ToString(), 
                                                                       parameters.DynamicDataItems["wsq"].ToString());
                                            endstep = DateTime.Now;
                                            LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Total Time Verify WSQ Service = " + (endstep - startstep).TotalMilliseconds.ToString());
                                            returns.AddItem("TimeVerifyWSQService", (endstep - startstep).TotalMilliseconds.ToString());
                                            startstep = DateTime.Now;
                                            if (responseV != null && responseV.data != null)
                                            {
                                                if (responseV.data.result == 1)
                                                {
                                                    returns.AddItem("Code", _CanAccess); // oResponse.Data.CanAccess);
                                                    accessresult = true;
                                                } else
                                                {
                                                    returns.AddItem("Code", Errors.IERR_IDENTITY_NOT_FOUND);
                                                    returns.UpdateItem("Msg", "Persona No Identificada Biometricametne! Reintente...");
                                                    accessresult = false;
                                                }
                                            }
                                            else
                                            {
                                                returns.AddItem("Code", Errors.IERR_IDENTITY_NOT_FOUND);
                                                returns.UpdateItem("Msg", "Persona No Identificada Biometricametne! Respuesta Nula!");
                                                accessresult = false;
                                            } 
                                        }
                                        else
                                        {
                                            returns.AddItem("Code", Errors.IERR_IDENTITY_NOT_FOUND);
                                            returns.UpdateItem("Msg", "Falta Muestra para verificar! Reintente...");
                                            accessresult = false;
                                        } 
                                        break;
                                    case 2:
                                        if (parameters.DynamicDataItems.ContainsKey("photo"))
                                        {
                                            responseV = _HELPER.Verify(valueid, Config.DynamicDataItems["ServicePointName"].ToString(),
                                                                       parameters.DynamicDataItems["photo"].ToString());
                                            endstep = DateTime.Now;
                                            LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Total Time Verify Facial Service = " + (endstep - startstep).TotalMilliseconds.ToString());
                                            returns.AddItem("TimeVerifyFacialService", (endstep - startstep).TotalMilliseconds.ToString());
                                            startstep = DateTime.Now;

                                            if (responseV != null && responseV.data != null)
                                            {
                                                if (responseV.data.result == 1)
                                                {
                                                    returns.AddItem("Code", _CanAccess); // oResponse.Data.CanAccess);
                                                    accessresult = true;
                                                }
                                                else
                                                {
                                                    returns.AddItem("Code", Errors.IERR_IDENTITY_NOT_FOUND);
                                                    returns.UpdateItem("Msg", "Persona No Identificada Biometricamente! Reintente...");
                                                    accessresult = false;
                                                }
                                            }
                                            else
                                            {
                                                returns.AddItem("Code", Errors.IERR_IDENTITY_NOT_FOUND);
                                                returns.UpdateItem("Msg", "Persona No Identificada Biometricamente! Respuesta Nula! [Code=" +
                                                                    (responseV != null ? responseV.code.ToString():"Null") +
                                                                    (string.IsNullOrEmpty(responseV.message) ? "" : "/" + responseV.message) +
                                                                    "]");
                                                accessresult = false;
                                            }
                                        }
                                        else
                                        {
                                            returns.AddItem("Code", Errors.IERR_IDENTITY_NOT_FOUND);
                                            returns.UpdateItem("Msg", "Falta Muestra para verificar! Reintente...");
                                            accessresult = false;
                                        }
                                        break;
                                    default:
                                        break;
                                } 
                            } else
                            {
                                //Es oResponse.TIPO_ACCESO = "2" => Puede acceder sin biometria
                                returns.AddItem("Code", _CanAccess); // oResponse.Data.CanAccess);
                                accessresult = true;
                            }

                            //Si ERROR == false => Dio acceso o salida ok => Parseo datos y muestro  
                            LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - CanAccess = true" +
                                      " - RUT= " + oResponse.RUT + " - Nombre=" + oResponse.NOMBRE_COMPLETO);
                            
                            returns.AddItem("Name", oResponse.NOMBRE_COMPLETO);
                            returns.AddItem("ValueId", oResponse.RUT); 

                            string _foto = null;
                            if (parameters.DynamicDataItems.ContainsKey("photo"))
                            {
                                _foto = (string)parameters.DynamicDataItems["photo"];
                            } else
                            {
                                _foto = (string)GetB64FromFoto(oResponse.FOTO);   
                            }

                            returns.AddItem("Photografy", _foto);
                            returns.AddItem("DateOfBirth", null); // oResponse.Data.Identity.NacDate.ToString());
                            
                           
                            ret = 0;
                        }
                        
                        returns.AddItem("CompleteResponse", response.Content);
                        LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Response OK");

                    } else
                    {
                        ret = Errors.IERR_DESERIALIZING_DATA;
                        msg = "ERROR PROCESANDO RESPUESTA";
                        LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Response Error = Null Parsing");
                    }
                }
                else
                {
                    LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Error Nulo Respuesta Servicio");
                    ret = Errors.IERR_CONNECTOR_REMOTE_ERROR;
                    accessresult = false;
                    msg = "Error chequeando acceso! [Respuesta Nula, consulte a su adminsitrador...]";
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "BusinessRuleSportlifeNorte Excp [" + ex.Message + "]";
                LOG.Error("BusinessRuleSportlifeNorte.CheckBusinessRule - Excp Error: " + ex.Message);
            }
            LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule OUT! - ret = " + ret.ToString() +
                      " - accessresult=" + accessresult.ToString());
            dtEnd = DateTime.Now;
            LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - Total Time CheckBusinessRules = " + (dtEnd - dtStart).TotalMilliseconds.ToString());
            returns.AddItem("TotalTimeCheckBR", (dtEnd - dtStart).TotalMilliseconds.ToString());
            startstep = DateTime.Now;

            return ret;
        }

        /// <summary>
        /// Debe controlar que dada la direccion de marcacion, se deba controla con el segundo factor.
        /// Si MFAControlType => //1-Solo Entrada | 2-Solo Salida | 3- E/S
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        private bool MustControlMFA(string _sentido, int _mfaControlType)
        {
            try
            {
                LOG.Debug("BusinessRuleSportlifeNorte.MustControlMFA - _Sentido = " + _sentido + " / _mfaControlType = " + _mfaControlType);
                if (_mfaControlType == 3)
                {
                    LOG.Debug("BusinessRuleSportlifeNorte.MustControlMFA - MustControlMFA = true");
                    return true;
                }

                if (_mfaControlType == 2 && _sentido.Equals("SALIDA"))
                {
                    LOG.Debug("BusinessRuleSportlifeNorte.MustControlMFA - MustControlMFA = true");
                    return true;
                }

                if (_mfaControlType == 1 && _sentido.Equals("INGRESO"))
                {
                    LOG.Debug("BusinessRuleSportlifeNorte.MustControlMFA - MustControlMFA = true");
                    return true;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BusinessRuleSportlifeNorte.MustControlMFA Excp [" + ex.Message + "]");
            }
            LOG.Debug("BusinessRuleSportlifeNorte.MustControlMFA - MustControlMFA = false");
            return false;
        }

        private bool IsNumber(string reservaId)
        {
            bool ret = false;
            try
            {
                int i = Convert.ToInt32(reservaId);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Extrae base64 de la foto desde el estandard
        // "FOTO":"data:image\/jpg;base64,\/9j\/4AAQSkZJRgA...."}
        /// </summary>
        /// <param name="foto"></param>
        /// <returns></returns>
        // "FOTO":"data:image\/jpg;base64,\/9j\/4AAQSkZJRgABAQEAYABgAAD\/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL\/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL\/wAARCACqAKoDASIAAhEBAxEB\/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL\/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6\/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL\/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6\/9oADAMBAAIRAxEAPwD1zT4rAaVZZsrYk28Z\/wBSv90e1SMlj\/z5Wv8A35X\/AArFsbr\/AIltnz\/y7x\/+gintcnPWs1c00L8iWR\/5crT\/AL8J\/hVOa1syP+PO1\/78r\/hUP2g+tHm5FaJEspz2Vp\/z6W3\/AH5X\/Csyeztef9Ft\/wDv0v8AhWzKcis+cVdjNmPJa23\/AD7Qf9+l\/wAKqva2\/wDz7Qf9+l\/wrRlGKpyUrCKLW9v\/AM+8P\/ftf8Kia3g\/594f+\/Y\/wq01QtSsBXa3g\/594f8Av2KjNvB\/zwh\/79irDcVE1ICAwQf88Iv+\/Ypvkwf88Iv++BUd3fW9oMyyAHrtHU1nx+ILWSTaVdAT95hRYZp+RB\/zwi\/79ik8iD\/nhF\/37FO3ZHHfpRTsIb5EH\/PCL\/vgUCCD\/nhF\/wB8CnZpR1pDEEEH\/PCL\/v2KeLeD\/nhF\/wB+xSipBRYYz7PB\/wA8If8Av2Ksra2+0f6PD0\/55j\/CmgVaVfkX6UmirnVWU3\/EutOf+WEf\/oIqQy1m2cn+gWvP\/LGP\/wBBFTeZUobZbEvPWpFlqgr1Mr1pEllsvkVWlpwbio5DxWq1IZRmFUZK0JapSjrUsRTeoWqeSq71AyJqqXlwlrbtK5AA6e5q21YWsRS308drEM4Bc49hk\/pSGkc1dzvcTvISSzHmofKlxnYT+FbUOnBMFkP41bSNVOMVLkaKnch0XUd4+yyH5lHy59PStrNZd1pUiW\/9qQoQIGG8juK0w25Qw7jNUnciUbMdThTKcKZI9amUVEtWI1NIZIiVeSM7F47UyCInHFaqWx8teOwqWUQWj\/6Ba\/8AXFP\/AEEVMGqpZt\/oVt\/1xT\/0EVZWgZMpqZaijTJq5HESOlUmKw0dKH6VZ8lsdKikiIq1IlooSVSlFX5gRmqMg60NiKT1XerMnWqz1IELUnhdYZdXnvLhDKkT7Qm4rkEEYyO1DVHostvZQyefMsYd+M8k\/QDk1nNtR0NKa97Un8Sgm5SQxJEpGAqDAFc+CN1S+IfEMl\/fZjhKwR\/Kgx2Hf61kyagvk\/IwLnoKST6mvMjr7YmTRLiA8pIm1hWNpzE2MYb7y5U\/gcVn6Zf38cmQ6tG33kY4H6Ves3O+6jKhTHKc7Tkc88GrjoZTdy7SimU8VRmTIOavW6ZIqlH1rUs0ywpNjNWwtDIRxXVRaOxhQ7f4R2qHw9p\/nuvHHeu4VFVAoAwBip3KPFLM\/wCh23\/XFP8A0EVejGaz7Lmytv8Arkn\/AKCK07dcsKYF+1gLEcVvWmms4GFqLSLMyuvFdhDCsKBVFJDMT+xmx92qF5prID8tddUcsSyoVYVQjze6gKk57VkTLg12OsWPlO3FcpdLhjQmS0Zco5qq9WpuOKqPTAjVd8irkLk4LN0HuaqQQeTM1z5Z\/drJCsq8qxYEqQe3erYI8wZOAeM1t2ipJol1DLH5brMGKqNuByFI\/LP41lUlY2pRTVzzi9s7hboRHGeOAc80s2kFNPF0cKzgyA+q7tv\/ANetDUSi3bRpK7QhsZIAOO\/QU7UdXsbqRPLhMcMcXl7dxOcf56VV+xSiupm2GnTJcDLEA9SDxitZYBDPM6MGjlbcPXHQH8cGsu1VFnXJ3JnIB6Gt25ZXYSA\/NtVMewH\/ANemnd2JkrIipwqPNKGqjAtRda2tPGXGawo2xWzYSAOvNJoaPUfC6AQMfauhrlvC14mPLJ+8OK6mkho8OsT\/AKHbD\/pkn\/oIrZsuXFYVi3+iW\/8A1yT\/ANBFbVnJhxzQB6BoEY8ot7Vt1h+Hpg0RX2rcoQwooopgZGuxBoA1ef3y4Zq9B12UJagV55qEmWNTfUDHm61UerMrcmqjtVEkbVYfVJmZ5JpM\/IqfgOBVVjVS5DSQSKOTtNKSutSoyaZhXc5N0VB6+3Wq32SRjkRtt9xU2VeT5x1FRyT4YR7m+maSNVqJG5juVUA4X7xrZgkLxBieTWNNIPlRRg4zWvBGYoUQ9QMmmjOfYnzShqjzSg1ZmWEatC2m2sKylarMUmKTQI7XSNRMLqd2MV3kWvR+UmcZ2jNeQ2t0V71tJqD+WvzHoKh3KMSzbFpb\/wDXJP8A0EVqW0mGBrGs2\/0S3\/65J\/IVehkwetWI7rRL\/wAp15rtIpVmQMpzmvJ7O7MZBzXT2GstGB81RsWdpTJJFiQsxwBWF\/b\/AMvUVm3ustICC3FHMINd1ESsQDx2rjLuXJNX726LknNYlxJ1OaEJlaVveqztVeW9YsdqfTLYqnJeSZ+9Gv61Qi8zZotiPtcWeQXAI9jxWZHdO0wBlyPTGBV6GRTcQqpyxcYAHvSY1uc\/qUX2K7ePPAY4NUDMC244yO9dHr9ssk8hYZ+Y1yslod2A5xUwkmtTWSaehPC\/n3Kj06mumfnawHVR\/hXO2kQiwo9ck1c1S+e3srfy3w28jPqKpaysiWvduaZGOc0m5R\/GPwrmDq7llaaMOvIGOMVZTWQRnbge1a8jMbo3vNQHufwqRblF9B+NYC6mjclm\/KrMMyTp8jZx1zT9n3FzG4t+o6H9KtLqjbR16VhIOQAa1E026KKQo5HrR7NBzFu1Y\/ZLf\/rkv8hVtGPpWTBeEWsABA\/dr0HsKcbuRuAxqUirnQQyEd6uR3qxjmRR9TXLRq78lmORzk1diTH\/AOqpcSkzoDqqY4fP0FQvqJborn68VUgt95AOTXX6BoMVw4LoMDrxS5Quck80rjiM\/hk1Qn80g5ib8eK9nfQLHySqxAHHWuH12ytLNm8ySNMf3mA\/nTUbibseY3ML+YfkOPTNVzbv2AFb19dWCscXCN\/u8\/yrIkvocnYrsPXGBWigiXIiSBg2Watjw2bNtRkLPuuIh+6U9yeCfw\/rXM318ZGVEyFznr1qG2vZrO7iuYX2yxtlT2+lE6V4tIcZ2kmzqtejKXLL\/eGcVy8kW2TkV2lvd6d4mdX8wQXIA3wk9\/VfUVn6po3l3O23IfjkE1xRvF8r3Ox+9qjm0GCT6d6yNRuTPLsz8qcCtXWHbTlMJK+c3RQenvXOZJrqpR6nPVlbQN2UK+nNMRyDkGkBw\/1pNpHHoa3sYlpZDtyKu207RzDB7c+9Zi5BxVyHJlzxTitSWdPbr5qhh0P6V1cEzi3jBTOEHf2rP8EaR\/bV\/HZeYqM+SGbkcDJ\/SvVl+HsIUD7UvA\/55n\/GulQpRX7yVrmDlUfwK55Jb2yi0g\/65r\/IVYWFEGcCrtpYubK3ODzEn\/oIqQ2DY6GpVJ22G6iKLSLAI9ysSy5AVc0rXjxJ5htJdvqxArSnge2jt5Amd0XGfrWLf3k7xtEwQIfRaxlSadzWNRPYnXxDPER5dvCv+8S1XIfGeuKNkV4sA\/6ZRKD+ZzXJu5pY5SDmsrWLuetw+FvFOrWayXmpyJvUEJLcN0PqF4rzTxBZzaZqlxZzsjTQttZkbcPXr+NXF8TamtqIVvrgRgYCiU4x+dYF3K0rMzHJPWhi0IU4DSSE7OgGepqvLNnJGMAdBTbmUqqrnoKz5JTgj2q4qwXFkmL4J9cUhY9c1WDExd\/vVITxVLUQ7zWU5BII6EHpUy61qcLEx3THjHz\/ADfzqkzYFRluM1MknuUpNbCSSSzTPJK5d2OSxOSajYccU0NnJz3pSaEhXIWzuBpxOQD601l603260NFXQ8E5q3C\/zZqlyKerkEdRSWgnY7Xw7rEmnXMU8TlXjIKkHoa9GHj7USAftT889a8Tt5yp4JrZS\/YIo3dq64VdLNXOeVJN6n0RpfhZW0uzcugzBGen+yKvf8IpAR80xH0WqNl4oSLSrNFC8W8ff\/ZFVbrxbNg7HC\/SlFYl9bGTdFPZs1J\/CdhMFSedgsa4GCAT9a8w8ZWOm2OoGDT3eRVHzsxyCfb9a6DXdalawspfMOXV8nPXmuB1C5Mzkk5qndR9+Vyo76KxlyEZqLcRTpDzUJrkZ0E6ktxV6bQtSWxa8On3X2dRlpTCwUD1zis+2neCdJUOHRgynHQiui1fxvresWZtbm9cwuMOigKG+uOtJJDOJvWxMw7VmSPVy8fMrH3NZkjc035Au49CfLIBx81SZwABUKnC7fTrTi3FCBgx5znp0qGR8DFOY+lV5H7UxoEb3pSwxwc1Crc0\/OKIvQY8sNtRkjNITTd3NFyWyUNxSM3IpF6ClVGlmCopZj0ApPYNWyxA3vWgG+UfSrFh4fkcKZpAmf4V5NdCnhez2LmR84\/vVm60I7mypSaOlttQY2duM9IkH\/joq5FBPcwebv2qfu5HWub0udJTaRTSiGJgitLt3bBgc4716beWejR+H7V7C\/Scp+7Z143kdTjtXS6jSOOUbK6OO1mRhomnhuGBkU\/mK5eV8k1ueIrnzfDdpcRH5ftMqg+2f\/rVw0105z8xqZSLijRkdQOWA\/GoGuIx\/EKymmY0zexrNsqxqi7TPANaGk2N9rl59l0+BZJQpchpFQBR1JLEDuK5xQ7Gp4hLFkq2N3B57VcUr6iZDqCNFdTRtjcjlTg5GQazJGrRvMtM575Pas2UHqRUtauxaHqflz60FuKhjYlcHtSucDOaa2uFhHkqBie9DMajJ4qW7gOyQeKfk1Dk5qQGheYncUnio6Uk4pozn6UAiVHNaGluBde5NZoyBVqwfbdr71M3dWKh8R29tIcCtZZTsX6etYVq+VFaSy\/KPpXFNXO6OxBbROtvCR02L\/Kup0XQbm\/0ySVLvyo5CV27MkEcZ61TtkT7FD8q\/wCqXt7Cu+8JIn\/CPj5F\/wBY3avVlGyueVzHD+I9LGm+CktRIX8i6+8RjO6vOpAua9h8dqv\/AAjlx8o\/4+V7V5ReKon+6OnpWcloXFlA4HanLz0FPwPSpogMdB0pRQ2zY0TREvo2muZDFGOF2jJY0moaSbOUFWLRE5BIr0HwpbwN4dhZoYyfs7nJUdd1M8YxxjQ7UhFB89uQP9gV6fsKfs1pqcSqTdR66HlEllcXDF0hcqTkE8VVm0q7AP7oD\/gQr1BYYvscf7tPuj+EVz2oqoY4UD8K8WVaXM0erGkrXPO2R4Jisi7TUcjZPFaesAfaulZbdR9atN2M5aMic802pJPvU3tVMi42ng8Uz0qRaaVwY0+wpF608\/epvanLsPoLx1qS2fbcJ9aYfu0sf+tT\/eqWtConYWUgKjntWmrDaOR0rIsQNg4rWUDaOB0rhlud0dj\/2Q=="}
        private object GetB64FromFoto(string foto)
        {
            string ret = null;
            try
            {
                LOG.Debug("BusinessRuleSportlifeNorte.GetB64FromFoto IN..."); 

                if (string.IsNullOrEmpty(foto))
                {
                    ret = null;
                    LOG.Debug("BusinessRuleSportlifeNorte.GetB64FromFoto - Param IN foto == null => Sale en null");
                } else
                {
                    string[] sAux = foto.Split(',');
                    if (sAux.Length == 2)
                    {
                        ret = sAux[1];
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("BusinessRuleSportlifeNorte.GetB64FromFoto - Excp Error: " + ex.Message);
            }
            LOG.Debug("BusinessRuleSportlifeNorte.GetB64FromFoto OUT! ret == null => " +
                        (string.IsNullOrEmpty(ret)).ToString());
            return ret;
        }

        public int Initialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("BusinessRuleSportlifeNorte.Intialize IN...");
                if (!System.IO.File.Exists("Biometrika.BusinessRule.SportlifeNorte.cfg"))
                {
                    Config = new DynamicData();
                    Config.AddItem("ServiceURL", "http://cercayrapido.com/chsportlife_dev/api/reservas.php");
                    Config.AddItem("ServiceTimeout", 30000);
                    Config.AddItem("ServiceAccessToken", "794C0A6E5CE5480886E9F6CBC523094AC0535C58FA79EB81C6367902B5E204EC7EFF765F76DA693764A9EEFCE16A090F");
                    Config.AddItem("ServiceUser", "totemvehicular@autoclub");
                    Config.AddItem("ServicePassword", "fdbfeec3c613d7d0fcf6aecaf5c5be9a");
                    Config.AddItem("ServicePointName", "Sede107");
                    Config.AddItem("ServiceType", 1); //0-Solo Check | 1-Check y Register
                    Config.AddItem("PortCOMBarcodeIn", "COM8");
                    Config.AddItem("PortCOMBarcodeOut", "COM4");
                    Config.AddItem("SerialSensorIn", "H39150402301");
                    Config.AddItem("SerialSensorOut", "H39150402302");
                    if (!SerializeHelper.SerializeToFile(Config, "Biometrika.BusinessRule.SportlifeNorte.cfg"))
                    {
                        LOG.Warn("BusinessRuleSportlifeNorte.Initialize - No grabo condig en disco (Biometrika.BusinessRule.SportlifeNorte.cfg)");
                    }
                }
                else
                {
                    Config = SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.BusinessRule.SportlifeNorte.cfg");
                    if (Config != null) //Check nulo
                    {
                        //Config.Initialize();
                        //if (!Config.DynamicDataItems.ContainsKey("ServiceAccessToken") ||
                        //     string.IsNullOrEmpty((string)Config.DynamicDataItems["ServiceAccessToken"]))
                        //{
                        //    ActualizeAccessToken();
                        //}
                        Config.Initialize();
                        if (!SerializeHelper.SerializeToFile(Config, "Biometrika.BusinessRule.SportlifeNorte.cfg"))
                        {
                            LOG.Warn("BusinessRuleSportlifeNorte.Initialize - No grabo condig en disco (Biometrika.BusinessRule.SportlifeNorte.cfg)");
                        }
                        //Inicializo Helper con parámetros necesarios para la operacion si esta MFAType = 1 o 2
                        if (Config != null && Config.DynamicDataItems.ContainsKey("MFAType") && 
                            (Config.DynamicDataItems["MFAType"].ToString().Equals("1") ||
                             Config.DynamicDataItems["MFAType"].ToString().Equals("2")))
                        {

                            _EnablePingVerifyService = Config.DynamicDataItems.ContainsKey("EnablePingVerifyService") ?
                                            Convert.ToBoolean(Config.DynamicDataItems["EnablePingVerifyService"].ToString()) : false;
                            _TimeThreadPingVerifyService = Config.DynamicDataItems.ContainsKey("TimeThreadPingVerifyService") ?
                                            Convert.ToInt32(Config.DynamicDataItems["TimeThreadPingVerifyService"].ToString()) : 300000;

                            string url = Config.DynamicDataItems.ContainsKey("URLServiceVerifyBase") ? 
                                            Config.DynamicDataItems["URLServiceVerifyBase"].ToString() : "https://localhost:7160";
                            string apikey = Config.DynamicDataItems.ContainsKey("APIKEYServiceVerify") ?
                                            Config.DynamicDataItems["APIKEYServiceVerify"].ToString() : "username1";
                            string secretkey = Config.DynamicDataItems.ContainsKey("SECRETKEYServiceVerify") ?
                                            Config.DynamicDataItems["SECRETKEYServiceVerify"].ToString() : "secretkey1";
                            int afverify = Config.DynamicDataItems.ContainsKey("AFVerify") ?
                                            Convert.ToInt32(Config.DynamicDataItems["AFVerify"].ToString()) :
                                            Bio.Core.Api.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL; 
                            int mtverify = Config.DynamicDataItems.ContainsKey("MTVerify") ?
                                            Convert.ToInt32(Config.DynamicDataItems["MTVerify"].ToString()) : 
                                            Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_JPG;
                            int afsample = Config.DynamicDataItems.ContainsKey("AFVAFVerifySampleerify") ?
                                            Convert.ToInt32(Config.DynamicDataItems["AFVerifySample"].ToString()) : 
                                            Bio.Core.Api.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL;
                            int mtsample = Config.DynamicDataItems.ContainsKey("MTVerifySample") ?
                                            Convert.ToInt32(Config.DynamicDataItems["MTVerifySample"].ToString()) : 
                                            Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_JPG;
                            int threshold = Config.DynamicDataItems.ContainsKey("VerifyThreshold") ?
                                            Convert.ToInt32(Config.DynamicDataItems["VerifyThreshold"].ToString()) : 50;
                            int timeout = Config.DynamicDataItems.ContainsKey("VerifyTimeout") ?
                                            Convert.ToInt32(Config.DynamicDataItems["VerifyTimeout"].ToString()) : 10000;
                            _HELPER = new HelperBioPortal(url, apikey, secretkey, afverify, mtverify, afsample, mtsample, threshold, timeout); //Add parametros

                            if (_EnablePingVerifyService)
                            {
                                LOG.Debug("BusinessRuleSportlifeNorte.Intialize - Inicio StartPingVerifyService cada " + _TimeThreadPingVerifyService.ToString() +
                                                    " milisegundos...");
                                StartPingVerifyService();
                            }
                        }
                    }
                    else
                    {
                        LOG.Fatal("BusinessRuleSportlifeNorte.Initialize - No parseo la config. Elimine archivo Biometrika.BusinessRule.SportlifeNorte.cfg y reintente...");
                    }
                }

                if (Config == null)
                {
                    LOG.Fatal("BusinessRuleSportlifeNorte.Initialize - Error leyendo BusinessRuleAutoclub!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                _Initialized = true;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BusinessRuleSportlifeNorte.Initialize Excp Error: " + ex.Message);
            }
            LOG.Debug("BusinessRuleSportlifeNorte.Intialize OUT! ret = " + ret.ToString());
            return ret;
        }

#region Thread Verify Service

        private void StartPingVerifyService()
        {
            try
            {
                LOG.Debug("BusinessRuleSportlifeNorte.StartPingVerifyService IN...");

               _tPingVerifyService = new Thread(new ThreadStart(PingVerifyService));
               _tPingVerifyService.Start();
            }
            catch (Exception ex)
            {
                LOG.Error("BusinessRuleSportlifeNorte.StartPingVerifyService Excp Error: " + ex.Message);
            }
            LOG.Debug("BusinessRuleSportlifeNorte.StartPingVerifyService OUT!");
        }


        private void PingVerifyService()
        {
            string sToken = null;
            bool bRunning = true;
            try
            {
                //string photo = "/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAHgAoADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDod1LuqPIo3VYEu6jNR5pc0mwJc0Z96jzSg0XAkzRmmbqXNMB+aXNMzS5pAPBNOzUeaAaQEuaWow1LuoAkB9aXNMDUuadx3H7jTgajzS5pNiJKKZmjNFx6kmaXNR5pc0XGiSimZpc0XGOpc0zNLmkA/NLmmUUASAilzUeaXdQBJmjNMzS5p3GPpc1HmlzSBMfmnA1HRQBJmlzUeaXNA7klGajzS5oC5JmlBqPNLk0ASZpM03NGaAuPzSg1HmlzQHUkzRn3qOjNBVyXNFR5pc0XE0PzRmmbqXdQK47NLmmbqN1AXH5ozTN1G6i4XH5ozTN1G6ncLj80Zpm6jdRcB+TSZpm+jdQIkzS7h6VDuo3UXC9iXNGai3UbqdwuS7vek3VHupN1IRLu96N3vUO6jcBQFyXdSF6iLikL07ktku+k31Fv4600vQmIm30haod9NL1QE2+mlqhL00yYouBMXzTd9Q76aZKdwMbNLmod1KGqAJQ1LuqLcKUNSAmDUu6od1ODU7AS5pQai3Uu6kBLmnA1Dupd1AE2admod1KDQBLmlzUQNOzSAkBpc1HmlzQMlBpc1FmlBoAlzRmmBqXPNAD80uTTM0ZoBD8+9Ln3plGaCh+6nBqjzS0ASZpc1GDS5oAfmlzTM0uaB2H0U3NLmgBc07NMzS5oAfuo3UyigCTNLmmbqN2aAH5pc0zNGaBj80u4+tMzRSuCJN3rRmmUuaAHZpQ1MzRTAkzRmmZozQNMkzS7qjzRmgbZJuozTM0bqBEmaM1Hmk3UCJd1Jmo91G6gCQmk3UzdSZoAk3Ck3UzNJmgRJuo3VHmjNAMk3Cjd71HmjNAiTdSbqZmkzQIfupN1M3Ubs0xXH7qN1Rk0hNANjy1NLUwtTS1Ah+40hb3qPdTC9AEu6mmSoy5qMvQBMZKYXqIsTTc0wJDJTS5pmaSmBlbqXdUG/wB6N1ICxupc1AGpwekBNml3Gog1O3UAShqcGqDdTg1AEuaXdUQanBqLgSA07dUWaXNICYNS5qIGn7qAJQaUGogadmgZJmnZqMGnZoAdmlBplLQA/NLmmUtA0PBpc0yjNAySlzUeaXNAElGaaDS5oAdmlzTM0ooKH5pc0ylzQA/NFNzS5oAdmlz703NGRSuA+im5opj6Ds0ZpuaM0hD804Go6XNAD80tR5pc0hj80ZpmaXdTEx+aXNR5pc0DQ/NGaZn3pN1MZJmjNMzSZNAD80ZpmaM0Ejs0ZpuaQmgB+aTNMopDH5ozTKKZI/NGaZS5oEOzRkUyigQ4tSZpM03NAMfmkJphPGabuNAh5akLmmbqbmmIeWphamlqaWoAcWphY03NJnNACkmkzSZooAKQmkbimE0AOLU0tSZpDTYGHu96XcfWogcUu6gCXeacHqHNLuoAsbqXdUAY04PSAmDU/dUG6l3UAThqeDVYGnhqQE+acGqINTgaYE2aXNRA08GkMkBp4NRg0oNAEgp1MpQaAHg04GmUooGh9FJS0DFpaSigB1GaSloAcKUU3NLzSGkOoptLQMcDS5ptLQgFp1NopgOzRkUlLjNJjHA+9Lmm496PxpXAdRmkpaAYuaWkFFAh1JSUtAxaKKKVxhRRRTuAUUUuDSTASilwfSkwaYBzRS4PpSYNMmwUlKQaSmAlLSUUkAuaKSkpgOozTM0b6BDs0mfem5FNJoExxNN3U0tTd1AmP3U0tTS1NJ96AY7caTJpmaM0CHU00ZppIpiFpM0m6mk0gHbqQk03NJmgBSabRRmmAUmfekLU0mkBhFcdqSrnlx+9IIk7nPpTAqjNOANWRHHjoacI4/Q5oArCnYzU/lx9waeEjPGygCsKXBq15a/3KcFX+6KQysMkU4CrIUelOAH90UCIFFPFTDj+EUv4UDIwKeB7VIM08Z9BQIiAPpTgD6VKM0ozQMYAfSnBT6VJg+tLzQAwITzTtlO5pcGgBApx0pQpNLinAUFDdhpfLPrTsUuKAGiPjrSiL/ap4FOAFAXIxEP71O8sf3qfgUtA0xnlj1o8setSYFLxSYyPYKUItPGKKQCbVo2rT+KOKoBu1fQ0bV9KdRSYCYHpS/L6UcU7jFIY0AZ6U4KPSgECnAigEN2j0pcD0p2aM0DG4HpS4HpS5ozQ0Kwm0HtSgAdqXNGaTQw49KPwoBFOBosAmPal4/u0uaTNOwB+FJ36UuaMigBM0mfal4pOKYAfpSUvFFMkafcUnHpTqKQDMD0pMD0qSkpiI8D0pCo9KeabQK4zaPSk2j0p9IaBtkRA9KbgZ6VKfrTDQSxm0Z6Um0elPpKBEewelIQPSpM004oAjwKCB6U4000CGFR6GkwPSn0maAG4FJgZp2eaQimA3AppFOpM0gGFc1G0bdqnoqWMzMKKPlptJ+NaMRKNtLlBUGR6ilyo/iH50gJ8il3Cq4dem4fnTtwx1FAE+4U4MKrh/ejzR60AWtwpQ4qp5o9aUTL60AXN4pQ4qn5o9aXzaQFzfT9wqmsuasI2aAJg1OBNNFOGKBjgTS0gIp1AAM0vNJS0ALz60oBoBpwNAwwfWlozS0DAU4UlKMUALThSA0tABSgGkz7UtIoXFLikzRmmAtKB6U3IprSqoyWAA7mkBJijjFY9x4js4c7W8zHXFc3r/jENaeTaZVmPLCmF0d0XQdWUfjSoVY8EH6GvCrjVrozuWnkOPVjVyx8UXlo6hJmyepJzRyiuz2vj2pRivKW8a3+w5k3HHcCnWvje8i+ZzuzjilYdz1bj1FFect4/m80ARR5I/Orh8dMEyYkZsdFJo1Hc7ujFZmkaomp2STqQGP3gO1aW7NAx2BRikpc0gClpKKAFxRSc0ZNMBaKbmloAXFGKbzRmmAtFNzS5oFYWm0ZppagVh1IRSbqaTQAuKSkzTcmgVhTSUhJppagQpxTaTNJmgVhTTaQsaYWoEPIptNLGmljQA402kLe1NLmgQ6mmmFjSbzQA+io95pC5pgSE03NRFznNMMhpAWO3WnCqu805XIFIDHL5HU1GzDHWq/m0nmVYiYlePamlh1qLfQSTSGSxkB81ZD1msXH3TijMp6yNQBp+ZgdKTzKzv3gHMh/Ojcc58z9aANINS5rM3oT/AK1f++qT7TbL1uFyPegDXVqeGGcZrEOo2K43XSZP+1Sf2zpqH/j5U/SgDoFYZqxG/HUVy/8AwkmmL1nPH+yasW/inTXdY1diSeuKGB1CtTwaqQzJJGHRgVPQ1YU0hkoIp4NRA09TQA+lpKKAHU4GmU4UFDgadTRTqAFpRSUooAeMilpKKBoWlpKXNJjCkJqOSURjJOKxtT1yO2hYKfnoC5fvdSitombIODiuH1zxNLcM8cTFYxWNqGtTTNhnI+YnFY8k+8ls8nrTCzZd/tB2jZF61lTTl0DHseafCfnbPcVWKHkHimOwSENnvmoDlCG9KmCsOMU/yT0IpCsNaRim7Oe+KjMjEcGrCQnBAFNMBAyFoHZiRSH5STmrkbtyoPaqscPb3qyqlGye9A0dR4f1mXTZFUElD1Ga9OsbyO7tkmjOcjmvGLYn5T0INdr4a1ZoJPJkJ2N09jSY2d9zS1DDMJEDA9alzUiFoopaYxKKXFGKBCUUtJQAlFLxRQAlJTqKLgNxSYp34UlK4DcUypaYetMTGUGlopoQzFNIqSm0MTGGmEVIabTERkU361IaaR70CYwimkU8000CIyKaakNMIoAZTTmnmm0CGUU7FNIoAYaYQakNNNADKeBxSYpwFIZ5Eviu76bVA9KQ+Krw8fIB9Otc2G9afu9KdzX2aN8+Jr4g4cD8KjPiHUGzmXr7VjA+1G6k2WqSNU6xfN1uGpDqV2xObhzn3rPU0/NS5Mr2MS0b64Of37/nTDcynrK/5mq5akzSUh+ziWDK3dm/Ok3kj1/GoRmnZ+tPmKVOI/PHWpE6VFlaki4FJzY1Ti2K1WLH/j6TPuarnrVvThuul47Gp52aKmj0nw8f+JUnPO45/OtpWrC0VtunRDp1NbEbZrSLujiqfEW1NSrUCGp1qjOw8ClxSUtACindaSlFAxQKdxSCnUAKKUUlKKQDselKKQUuaLlC4phYAU4kVn304iUHOBihgU9YvRCqDPymuA1S9eW5dQcgHNauqahJcP5Q5wcCqEemqfnlyWovYqMbs5+WF5DuA60qWLHnBroTbRp0UUhQAdKhzOmNJGQlhnlhilOnAnK9/WtUKB6Um3mjnNfZxMttOyM45qdNOVlGeoq+AMU5etPnJdOJSWwAOcDj2obTUbkd/UVoYOaeMdBU8zGoIyjpQ+8B0p39mF1xjpWsKmXFCkwdOJkRae0a4x3q1Gslu27kAGtSIKauJDG67SByOarnMpUjQ0fUy8KozYYCuhM+F69RmuGmtntWEkWdg/StHT9UeZ9khPHHWq31OdpxdmdivIzS4qG3fdEpz2qekAY96KKKAEpKdS4pXAZS0UUwCilpKLAJSYpaKAEphHvTzTCKBMaaSlxSUxCUhp1JQA00ynmmmmSMIppFONJQJjSBTDTzmmmgQymsKfTTQBGR7U2nkU2gQlNNOpD0oAjNNNPppoASnAUlOFIZ88qRTgRUYyD0p3PpRc6kh+6jdTcHGaUA+lJsrUkBI5qZTu+tQkEDmpoR69KllIa4I5xTd1WmAZMHsaqlVzgCkgY4SYp6sCeTTPKPVTTeVPINFkNFoxrjlsGnoMDiqgerEcgYcdqllLckNXNLBNzn0WqPWtHS/wDXP/u0jQ7/AEo4sIR7Vrx1k6fhbWIAdAK1Iq1hojgqbsuxmrC1XjqdaozJBS0gp1MBaWkFOApAKKcKaKdQAtOGKQUoFIB1FFITgUFDW6HmuT168KMYTnnkVvahfLaxED5mNcbcTG5nLyDv60DSu7FeCIZ8xhz2zT5JAookkwMCqjOT1NZuR1wikhzNu9qbmmMTRjIFI2SFyPSj6Um3mnYAoRQig85p6n6UhHFOUL9aYh+R9KbuApQopG96BDt+O9OEhz1qLK+lOJBHFAFlJRn0rQgn4BrIB44qeFiOhoFY3gVdMHkGsq5jexk3JnaTwaswz8DtVi4Rbi3KEc9RTTsYzhc3dAvBPagM2WxW5kHtXCaRO1nOAcjkda7eNt6Buxq2c1rEnFGKMUc1ICUUuKOtNgJSUppKaAKKKKAEooooASmkU6mmgBtJRmimSJSUtIaYhKYetOJ4ptAmNpppxNNNAmNNNNONNoENOKSlNJQIYRTaeTTaAGmmkU40hoAYabTjTaAClpKUUAfPZQqcUoxSuTmgVJ2hj2qSNQTTKch5oGh03BwKkhHGagfr1qxbgmolsUtwP3TUQNTyDbGar85pRBomTrxTiwwc80kS8daVhihsdhm0HsKegAFN5pydOvehrQqKJABWppY4kJ9BWXWrpX3X+oqGWd7Z/wCojHX5a1IhWdbriNfpWlF0reOx589y3HVharpVhaZBJSikpRxQwHUopKdihALThTadQAop2KbS54pAh1RyttjLe1DE1k6pd+Wm3n5vSgowNVu2mnZQTj1rO+6KsXLDfz3qq3NKWxrSjqMc5qAjmpH+U9ajIJPNYnXEadvbNO57cU7GQKMUyxvUc5p4FJgmnY4zTAQ4FOGOtNpQeKYrDyeOlMxmnHJHSlCnGTQFiNUwaXHOKmGDS7ATmkrgRqvpUq5DU4KFHSlFMCZHORWlE3y9azFUZ61fh+7QRIZNv83KHHtXXaPd/aLYKTyB61zLoDg1p6S3kzr82Fari9DmqLU6eikByM0uKLGYUUUUwEooooASiijNABSUUlABSGlpDTAYaKM0lAmLTTS000yRDTDTjTDQDCmmlpDQQxhNJmnGmGgBDSUU00CENNpaSgAppNLmmmgBpNNzSmm0ALRTaWkB8/kHNGKP4qWkdtgp8Y5ptTwAbvwpXGiGQfN1qzB8v0NV3PzZq1AAV9KhlRQTn5OKrAc1PNwuM9aiUc4pIb3JkHHvSMOakVcLTW60upQm3ilRfloOABn0p0X+rHFMaFPStfSV3KRz94Vk8CtzRwMR98tUvQrodzBjA9q0YqoQjgcVoRfSt47HnSLUYqwvSoEqdelMkeKcKaKdSAXFOGKbThQA6lpKXigBaXtTadigoZJnFc7qwzKuTwOa6F/umud1dsTKPaga3MCdxu96h7ZqRwd/40xsZqJHRTIiCTnrTSKc0qRjLnFVJ7+GM/fU/jWdje9ix2xmnheOtZ41CBjncB704arbD/lqPpVqIcyNDbwKMY6jNVEvoHPyyVaWVXAx3p8oKaF27iTSbfwp3Q0ox1NPlKvcReRUgGQOaYp9KjeYRE5PSnyicifAU560/clY8+sxxjpj3NUzrxwSFxntT5TOVRI6LetTx4IrkjrbY4XJqSPXGJAxg0nEXtEdaFO7IGRV+FMgCubsNbQyCOVsA9DXUWrgtwQQRxUuLByJTEDHU9phcD0PWnhcpx3qNQVNVFGM2dNbsDCpqaszSZd0DIc5U960qpmYtITRSUIAoopM0ALSUUUgCkoooAKacUuaQkYpiY00lFFAmwNNNKaaTTENJpppTTaBXEzSUpphoJA000UhoASmmlJphNAhKQ0ZpN1ABTTQWppNACGm0hJpM0AOpaZmlyKAPAaWkA5pcVN7nagPtViDOCfaq5zUycKaTGRsSZKuQVTx8/Jq7bHYCfWplsXEjm7YqOMHcBUk55FNj+9Up6DkWeccmoj19Kk/h9aiOd3WpuUK/T8Keg+QZ9Ka/wDSnqPlAp9BoCM8V0GiLnyB/tVz5yGFdLoa5e3HTnNSEnodnB0q/F0FUIRyK0IuldK2PPk9S0lTr0qFKnWhkjhTqSloQCinCmgZ704cUALS5pKKQ0OFOpop2aBkcg4rmdaYJOD6iumkyAa5LxGcSpj8aOoXMbduJJ61FcTeTGW4/GkRupzWfqTu42Ck1dnTDSJjXd5JNIzEnA6YrNcSy8gk+lbsVkCuCmc9atpaoicKBVpIh8zOUNvPtBKNUYQsR611zxgD0qtJArHkA46cVRNn1Mi1STd34rdtnbcvOMVWTYjfdxVuKRexyaTLjc0FlwMGpo23dDUCKGAJ/KrEQwe1TobLRCkFeRVS6QyDI4atP5KqT7BljjimZyZz9xYO7ks36VXk0+UcIM1syTrzVcXkStgnFUmZtXK1toruuZJNp9AKsf2AcErL+lWIb2NjjcpH5VpxSRsARke/WqeocpkrpLxLktk9jW5odxLExikbK54p3lboyQc+lJawsJNxU8VDGtDrImyo96V12nJGc1Hbf6te3FSzPtTI9aRMti9pTfO3HBGa1s1gaVKpu9oPat0dKGjOI6kpM0UFBSUlGaQC5ozTc0ZoAdmm5pKKAFpKKKZIlJQaaTQSwJphNBNNzQAtNoLCmlqBMCaaaN1NJpkgaSkJppYUgA000E0wkUwAmm0hpM0ALmmmikPSgBpNNoNITSAWlP1puaKAPBRThSDNLms76HaB60vRc038alx+7/GlcoaMFutW0yB1qqn3iKuDhaUmUiCQ5PWiPrSTff4NLH9KS2GWONtRDls5qXPHSo1ODSLsEh5JqQH5aY/NBzQJMdkE11Oh4M1t9DXKrXW6GMTxjuFqXuOT91nWwduK0IugrPtxwM1oxcV0LY86W5aSplqFKmWmxDxS0DGKKYCinfSm04UgHUCl7UlDGh4p3SmCnc4pDI5Dwa4vxTLslxntXaPyDmuC8Y5W4U/wntQtwZnwfNEpPU1BMhaQHjin2TBrReeQSM05wM9al3udK2EChVySMVmz34JIhTdg/eqTUZisaxlsBjz9KzI/Nu5CkPyxjv61auO9h8l1IByyrxVR7qUgnzd2KoXKOtxIjsflbGDURRC0YXOSeQa1UGYSrJ7Gh9tJGG/OpYrlkdTnIqtLAsD/ACtlSOlPSMyDK9O9KWhUG2dFaXAZsZ6ir6nPNYlgCCOeVrWV2+tc7dnodijdEzyBR3rLvbosdi9qvOC61juN0rEnvyRWidzOcChPI7sevFRRhmOFUnHJxVucEkhUOyrVoqCJl4BIxk1rFJmDutTOjvoo+NrH1yK07W/trggLIY2+tYRBhkMbnawPWtTR9Ohv70vJOsMKLycdT7VfszL2zOv04TRuA/KN0PrWgYtkuVOFNYCXTWcTwRF3hz+7dhit62uUuYEkLAHHzA+tYtGl7mzbA+WMnpTb4lbZmHYikspwUINOvQZLWRVHOM0hEGiSk6imRweK60GuL0glNSiwCc12QNNmcR+abSZopFC0lJmjNAC0UmaTNBLY6kpM0maAuLmjNNzSE0CAmm5pCaaaAYpNRseaUmmE0EiGk4ophpiHFqaWppphagQ4tTS1ITTCadgHFqYTSE03NFgHbqTdTM0maLASZJ5ppNNzRmiwAabRn3ptSA6ikpc0AeEDmnZFNHvUihcZHWsbHaiPvUwB8vJHWourcirDH/Rkp2KRFH1/GrnQVVj6jNWmPy4/KpaKWxUk5lNSRZqNs7zzU0IoGiYn5aYOTzTiaYDUmgHjtQTg0E8/jSH3piaHJyw+tdjoi/vlxyAtchEAWA967TQ1/etxg7aVtSZfAzpoa0I+1UIPpV+LpW5wstJ0qZagSp1qiR4pabTqAFpwpoxTgRQA6iiikUhwNOzTRS0gGOeDXEeMoDMqbPvLz9a7eQZHSuK8TT/vzFxkAZzQNK5kWcBitEU9TyabMoHOcVcj4UDHQVXnTPTpSTOpLQxrm2e6cEdBSJE1sdyk8dRitaJEximyW5PYVSYNHO6hAt3L5mNjEckDrVNLOOM5BZmHQ10clqrDkVWe3UDAGK052Z+xRkFSxyR9asRR5GFGM9as/ZznpVmKBVPSokzenT1Ftowi49epq4uM8UgVeOKkwAR/KsGjpVloSouRjtVO/sWimEgHyvzWhBjeM9M1qahElzYrtGWXpVxRnN6WOKkRzxiiKFzwD+daTW4HBFPSFehFao57FE6aJvmeNWq3a2HlOB5Y2jtV+OEAYVsCrKpgVbkzJpXIHj80KpUADpSLvhcYP1xVoYUZNRMN8mc8VBSRpWc5DqSetbSkMnTIrCt0+YVv2qZUDtSE1Yz9OkW11eRHQsT90+ldWrbgD61y+o4tL4XA6smBXRxt+7T/AHRQZuKWpPmkzTN1JuoEPz703dTCaM0CZJmgmo91LmgkdupuaQmm7qAsOzQTTc0hNAAWppamk02gTFJpuaQmmk0CaFJphNBamFqZIrEUw0maQmmgFpDSZoNMBCaZmlJ96bQAHNNzSk0wmqQC5ozTM00mkJDi1Jmoy1AbmoYyXNOzUW7NKD70gPDqcKbTsiotodgn8XFWH/1aD2qsDzVhvuqPQVBothIhyKlkJ2EjtTIhzU7INjA9xSbH0KIb5jnmrEXWoMYarEXTrTsJJ3HkDr2pB7U5hxmkHTioLGjrQwOaUdaUrzVD6DoeHXjPNdvogO+TjsK4uD/WJ7mu20QHbIfUip1uKfwHRQdq0I6z4a0Iq3OFllKmWoUqYGncgdS0lLTGOFFJSjHepAeKWm5paAQ4U6minUyhD0rjPGlqfMt7hf4jtau0JyOlYfieISaOxI+64IoRUHqcsoLd+Ka6A0oIFP6iod0daKm0o1KW/CpWHNQNnvVDsQvyetQFQW4oLSFsY4NSMBgY4Ip3KUSMRjPBqZIxSDAHvTg+KTNFoOOF69BQCCfWkI38849KU444qN2XHUmRttaUD7kwOlYxZs+1bOmR+YuD1q0E0ralK6iHmFgO9Qbcdq0Lz5J2jI6VT4JqjDRjVLA8VOJecdajCU9UFFyJQJfvcZzT44gxwaFTkYFWokGRxSJtYuWcHAB5rcgj2xg4rNtFGRWyg+TAqjKT1MXXYy0DMBnan9a1NPmEunwP3K4NU9Vl8lRuGUcbTmqtvq9jp9tHb3M/lsASMqemaQP4LnQbhSbhWdBqtjc/6m7ibPbdVrdQYslL0m6od9JuoFcn30b6g3Ub6BXJi9M3e9RF/emF6Blnf70F/equ+jfQBYLCm7qg30b6BEpNMJpu+mlqAFJppamlqbmgmw7dSbqbmjNMQ8HJoPSmUtUgAmmk0pphqgEJpmc0rUzNNCYH60006o2NHQBhNJmg0zPNZtDJdxpc5qMNS5pAeLUuOM0AdgKU9MVB2oZUx5qMdakJqGNEkR5qzLxGfpVeEZP41au8bcZ6LUPc1itDOHWrUeAKrKDmrCDFU9hLce3ShehprE9BQM4qSmNH3/pTyfamL98+wpxJzQxomt8mZBjPzV3GiACFz3JFcTaZ+0R8d67jRR/o5/3qS3Jq/Cb8Iq/HVCHNX4vetzgZZSpVFQpmphQIfRRRQA6lptOoAWlzTaUUDQ8UtNpRQMdWX4gGdFn9sH9a065zxPrEFtB/Z4IaaYYI9BTKW5zSuGQNnqKdv96rRghcc8VICcc0mdS0JCc0w4PWkyD0NKM574xTsaIi2DpUbDirDYqFgAOTRYtMhLEL0p0TBgWJ6dqikOe/FReb5fB4zQwuaCuDxSjaeorBkvblLpDH8yZ5FbVvIHXcOhpJWG5WLSW6s+fSt/SLf99/sgZNYsBBcY5rUub5tPtFigI82Tlm9BVGM5tkniSBY4oZ1+9u21gRsGz696nv7576JEL5C8596pQE7jmnYmBcWMnvU6xjFNj7VciTccEUi3IhVccVahUkgHpTxAQOnFPC468UGTZetAM4BrZRcLWVZgFvQVrgjFMxkYuv82+z+JiMCvNvGE5j1nywfuxrx+FenX6GXUolxlQleReMZS3iS6/2SF/SmtxydqZSjvXVgQ5GK29P8WahZFQs5ZAfutyK5HcacJD3NUc1z1Sy8dW8pC3UOwn+JTxXTxXEc8SyxOHRuhFeFJMfWuy8I68bWYW0znyZOBn+E0nHsO56Nuo3e9Qb/el3GoGSFqbmmZ96TNA0P3e9BamZpM0CHbjRuqMmjdQBLupC1R7qaWoAkJpuaZuppPvQBJuoyaizTsn1oJJc07OahDU7dVJiHE00tTd5ppNUAE0wkUMaZmqQDyajanUwk/SgkjY802lY02oZQuadk0ynfQ1IHjaHPSgg4zmmqMU7jHWs7ncIOtO9RSDrS4+br3pSAtQEFlHPWpL1sSsT6Uy3+8O1NupA7nHTpWdm2a/ZIEqzH16VVTrVlKbuShX4NC8imyD2pU+5Uoq4oHJpp696cOtNIOaZXQt2OPtSD3rudGUi1H+9XDWI/wBJX0Fd5o+fsa59TQtzOq/dNuHmrsZqjFV2P3rZHGy0lTA1ApqUUEkmaWmg0tADs0UlLSAWlGO9JRQgQ+lpoNL1ouUVNU1GPS9Olu5SMIPlHqa8Wu9VlvdW+1ysSWfP0FdT8QtZ826XTo2/dxDL47mvPi+TW8I2jczctT0FSMfKcipMErxWXpc/n2UTDkgYNaKsRzmsj0E7q4Rx7fxqQDmm79xGKlxTGtxhGRVaUYXJq2elU7g5bbQi7lYDJyB0qpeNzjPNXmdY1+Y4rKnO6TKEHJ7UA5WRnySTGX90+McVoWN3NGdrEEVXlRY5gu4Ekc0ixhHzk9OmapK5yuq7nRR6ktsu5AC5HSs65v7+7nG4lVzz6VTTcykK5zn0q9ETt2Hhh0NXyaC9pfU0LY/LtPUdam24ORWUt46SAPwPUVpxyhhwajU2jJMv2/QVsWoGRnvWLbtk4xWtbEgjmkEmaXlgx1WMTHpVpTmPimYNFjG5YtVCuB0rQySeKyoJiHw4IHrWupAjLHoBmghlOYbHluXbAjXv24rwXVLlrrUJ53bJdyc16n408QCx0IwKf391x9Frx9j3q4LqTUelgzzThz3qMnNA61RkSg4PFWbeYo4Oe9Us09TSA9Y8Mav9vshDK376IYHuK3w2a8j0TUZLO6R1PzKePcV6lb3aXMCTIcqwqZxsUn0LWaTdURajfUFku6k3VFupC1AEu6k3VHuo3CgQ/dSE0zdTS1AiTNITUe6kzQBIGpQaizShqCSajNRhqdTQh2aaTTSRnig5NUAjGkpM0lUmA7NNNFIx4pkkbYpmaVjUeazZRJS0wEUoNIDx4c0Uq8DpQenBrM7gUc0vOaRTzR/FR0C7LScLzVeUnpiplzmo51+bPTipi1ctjYRzVpKqx9e9W1O2lMIiN6Z4pwACHFIx5zRu+SpLQgzkkGkOSaVec/WkxzQDLdjkTD6V3elcWcee4zXDWAJm57DrXeacmy2jX0FNbmdX4TWi61eTtVGE4q+nStTjZOtSiol+tSCgRIDTs0ylFAD6KbmlzQwHZopKKQ0OzUVzOttayzucLGhapO1c542v/sXh2RQcNMQg+nemld2B7Hk+pXTXd5LO7ZZ2JNZxPPWpJmyxFVya6mZGvpGp/YZdj5MT9R6V1u4EAg8MOK87Dc11mj3vn2QUnMkfB5rGcTpoz+yzdRyBxU6niqUUh71aUmoubq45z8prFu7oiTCH8a1rh8RMcVzF1lWJCmmtRylyoSS5JB3c++agjuhGGPLZpDvkQKq1Nb2K/flPHpWiSW5ztykym8jPL5zHr1qVblVOAp56mtPZbY2+UCv0qeO20+UEFQpxxV3SB0pGTDcbJS6gmrjXe/kADI7ir8VnZWzllPOKl2WmT3J9qfMhezkZuQ6CZvvAdKntr8AZYkc9WqYWCSoSGx9Kp3dgyQ742BHXaTUuzEuaJvWtxuYbsfhW7bkYBri9KyZMHOR3rsrU7olrNmqldGrEMp14pjFg2M0QZEWM0uCZfWhGcnqT2q+a3I+tVvFerjRPD0kiNtmf5I60LcqoIxg15V8Rte+36mtpE37mA4GO5ppXdiG9Dm9Rv7m/dZZ5WkbHU1n55p0j4gBqASjNa2sZslxTsECohKKd5wpgLg0oJpBIh60uQemKGgLFvKUkDdK6pvEd5pdlCbcI0bf3hnFceD71q7xc6Q6DlozkChpNWYGkfHmqY+7CP+A03/hPNU64h/75rlCTTCaxcEPmZ0M3jHWJJvMFztA/hUDFWIvHerBAC0LEdyvJrk6aDg0rILs7D/hPdWx/yxz/ALtJ/wAJ7q3rD/3xXJ59qM07RDmZ6L4b8V3uq6n9muRHtKEjaMGuv3cZry3wY2PEEYx1Rq9M3e9TJJbDTCaYxRM/XA6VzFxr96ZmjDKq5/hFb1+wFhOf9iuFacbsBfxrNs78JRjO7aPTvDo+0eC55nbc5lxvPXqKkFjp3R7t92Oxqz8OESfwqVkUMvmtkGuqOk6e3P2SL64q7EudOnNxaOFFtbRyoY7wyneo2Y6813n9m2WObZPyqA6dpcTBhbRhgcjAqZrvP3RVxiYV6kJW5RsulWTKcQIp9apPpFuDzEn4VZaZmPLmozIsoIJzV2Oa5UGio7cRx7ferB0SxVMmPoOeatQMApA6CpS9VYVzn2i0UfL84b05pps9Ob7vmfnW8wTH3F/KoWWM/wAC/lSaA5rUbG3htFnhLctjmsUnmum1/aLBAoA+euWJ5rNlIdmnBveo6UHHapA8lppNG6kzWaO4cnvS/wAVCYo5Le9DAtxZJqO5H61JbtiQA1HcnOPWs9mX0IkxnrVgZ71XTrU4zinLUSF708n5egqIk5604txUWKQ5eh7U3v1/SnR8r9aTvTKbL2nkeaxz0Fd7YEm3iJ6la4GyOHJ/Cu+s+IYx6KKI7mVX4UakP0q8hqjCelXErY5GWFqVaiWpFoESUUmaKVwHA0uabS5oYD6Wmg06kMWvN/iRfK9zb2qPzECWHucV6K7BFZ2OAoyTXhmv3zX+rXFwTkOxx9K1pb3JkZLZY8mojH71IaYTWtyRmw561oaXdNZ3asx+QnDCqG405X5pvVDTs7nexSb2DD7uMirqPnisHRrrzrMLnLx8VpliSMcGuex2xd1cnul3xbR3rn7ojey5wFroATsJIzWUdNLSFsnk5qo6Ey1ZmIly5zCgx05qVbW6Y5djn2rZjtjGgwOlNMwjJ3U+Y0hFLczvs9wOC4P4U5YZxyuPxFXTexgE9cd6gN+T8wxtp7mnNFbsiENyedwB9MVYhgnPDSDj0FWI70ldrKKuW00W4ZAB7GqvZC5olI2Fw8fySMD69KqXGlXSRGTzWdh1UmutjeOUYH6UjWo56EVDkYzakcfZTFZArZVu1drayqYFOfrWNLpyJN5mOSa0rZVZFT0p3TMdUbVtJt56jFSIfMUEHvzUdtCEh56mpIysZIxz2oIuZ/iTWhpGlysjAzsPlHp714teyvNMXcksxyTW74o1aa51+dJDkIdmKwLoYwR0PStYxsrkN3AjfCKh8n3qdD+5qPcabENEHvThBz1pQTTsn1qbgIIBjrThCvrRupd1F2A4RJnv+damlwAyMoJGRg1mA1paZIEuFz3PJqoq7AyrmJoZnjPVTiq/WtzxBb+XcrKo+WQZ/EVhHI7UpRAQ0wHDU401h3rJoB+aM0lL1oA6Dwi2PEVt7hh+hr0nPFeYeFzt8Q2p/wB7+Rr0vdSmVEjvctZTqBklcAVxUtjcxx+Y0ZCepNdrKf3TD2qhfhTpsvGcL1rNx1O7DYh03budj8NpRH4XO7p5zVteJNRurLS2urVh8mMqw4xXJeBbgp4dZd2P3rGumvGW802WBj99CK6NOpy4h3qtnLW/jqZHP2u1RkHUocGup07V4NUs1ubfIUnBB6ivHb2VreVopFIZTg5rqPAuoMIbm35OGDKK1cFa6Od3PRDKPWkWQbgv6VRTewy521YV1QYFITLUl6IWCiJmB9DUbangkGB+KqvO393P40jS4GcE0CFefzQ0gaVM9RVc3p5USSf981IJQexFMkkFFh3K2qzB9PjG4nL9TXP7sGtbU5d9qgGeXrHJrGa1KWw7NPBzUWacDUDPJNwHWl3jHWo91BYVlY7LlhSNtA/1lQI3NSoctSKTLkX3s+1RXPDCp4AMk5xgVXuGV24ORU7lPYYh5qTdUAwDUigdSasSZJnPNOOMcVX4zxUinK1LiNSLEfKCkxzSxL8vXrTGDBqk0ZoWSglsnniu9tP9Wo9q4KwJ3eozXfW2Qq564poyrPQ0ouoq5H0qlF0FXEq0zkRYQ1KKhU1IKYiTPFLUeaXJpASZopoNLmgdh4pwNR5pc56UMDH8V332Hw/cOp+d/kXHvXiszZY16R8Qrp5Ft7SJhgEsw/lXnptDn55AD7DNb04vluZyd2U2pnzO2FBJPYVpR20RYKFLsenNdloWhQwRm6uQoCDceOFrVRJcjkrXQJPJ+037/Zbcc5Iyx/Corm/t44ngsYFSM8F25Zqm8R64+q3hCZW2Q4jT+tYTMcVXKK5s6BcFLqRRyCua65GVkDY5NcZ4ewb1we6V11u4U7T+Fc1T4jto/CWh8qnmmc569O1PABHPem+URyDUmqDdxWZcwyuw2jjvWksbdT0pJF+WhDexjpANpVmwcZ5q3b2QeAAEbXyc1Wu4nL7lPygc4p8TSCOP5iAnatoHJO6ZfaxRFzu5PTmqYZ4nCkZyetDie+fcAVx05rUtLHcA7nJFVKyWooti6cZUdSc1t78rVZYsdBip1QkVg9TZETgknilgjYyDCnnrirSR7vlIq5BAI1oE0RbjtCtn5adbOWlbPIB4pL2YIojUfM/6VHCdidOgOTTuZ8p4xrr7tbvGzn96386WztZtTK20ADTH7oJxmq2puZL+aQ/xOTT9Mu5LO7iniOHjbINdEOxjK6J5rC5sQ0V1C8TjqGFVCOa9ktJLDxrpLxyKsd9GuWUd/cVwmreFZ7CU5XCZ4dRkVr7O+xmqnc5alq3Pp1xCN2zcv94VUwQazcWjS6ClzTaWoGSKatWz7XHOOapjOetTI2Ka3EdPdQi80zcclk5FYMtqoGMVv6JIs0TQtnkYFUruMKWUjleCK6VFNGUpWZzMi4JqNulTz/eNV2PFc01Y1i7oB0604YpgFOBrIZseHMDXrXn+I/yNelZrzDQWxrdof9v+hr0K4lkEbeUuWPA9qUi4K7Jp50SNizDgc80+O2ivrBWLMySj+AVzcq/vBlvmPDZrV0zVWs4ha8GMcg+lQ07aG3Ko63Ok0+xTSrMQQFiudx3GrK3LE4LGshdQ844EoPHrUct+IUJLrntk1w16s4m8VGepZv7WwdWnuoY2CjJZhTbPV9Ksof3EscaDsq1i3eom7heEchxgkGsf7DKUCI8mM/3aKFao1qzOdKKO/g8S2U8nlxXKO5HC9Kurqav3/KvObOxWyuDP87yEdTWk2pS5wqEVrLEzi9NTONBPc7Nr9CeXxn1qT7SjKAJB+FeY63f3U4iVELKuSdtY0V7eQlmxOrdtrGuqlUnON2Yzgoux7E0371SZgFGcgnrTHuFBys6fTIryUa/qkUYP2iUN6MoP9KePE1+77ZBFIuP4krTnmmRyo9JvJi0KrvDHd2NZ+ea5zSJTdTJOIxGcc4zit7fxUt3GTg+lOBqENUi/SkB5KOR96kP1pgzS/hUWOpEijJ5NSqCPeoA2KdvJqGrFIn8w+tJwaiHJ5zThgdc0rIol2j1pwVT3qE8+tAOPWq6BcsLGCehp4iweDUKy4XGTTQ3Pela5VkXciNeTwBUaypK+1TUN03ycetV4WKtxTUbkyq8rsbVqxSaNB0ZhXoNvnaox2rzmyf8A02Et93PNei2/3Vx6VNrE1Hc1Ie1WkqnEc4q2hqkYE6mpQaoz39raLmedE9s81lT+MtOhyI1klPsMU9WJ2OlBpa4ebx3LnEFqg92OazLjxnqUo/1yxD0XFUqcmTdHpZ4GTx9TVebUbO2UtNcxqB715Lc67eTE+dduc+9Z0uoFj8zs3pVqjIXOemX3jiytyVtkaVuxPArmL3xrqd1lUkESHjCiuSN1nkCo2uCxwABWioxRLkzRuLqWZy0jszHuTVVpOeT+tUpJWJ5algQzzpGOSxxW0URc6vw9ZiQi4bkscIK2PGN//Z2iR2ULYkn+9j0qbRYAjqAPkiFcV4q1I3+tSnOUjOxfwrSSVyIu5iMeaaxOOtJnNIazZqkaegNt1Ic4ytdaN2etcXpLlNQj9ziuwUk89q5Z/EehhtYl1J+OeoqzHKHGOBWX347VEbpoHBxn1NQXJcpvKVA61FNk/T1qrBeFtoJ5foMVYKNkcnjmixDkkQJB5jkDNKtuWlCHAUVdXdGm3I3sKjgAJbJwV61SIbTH29qASeMVowxKOByDUCgIOuc1eRfLhbjJ61T1JukKPLAo3qG4NHyugZQCGpgQBCentU2DmRZiXLBjyB6VO90qrgVkJPJJK6rlcdx3pxJA5NIUXzEztmTcTk1DdXIitJmzjahOaiabBrK1678rS3VeTIQtBpy6HnF2Nzs3qagi4artwgywqiOH4rem9TlnE6fStXn0m7t7yA4ePqP7w7ivV5Xt9Z0uPULcBopV+dfSvEkb9yK7DwL4k/s+8/s25b/Rbg/KT0Vv/r12RZzSjoGrwPpU2WXdbv8AdbHT2rNa0sb8ZIwx/iU4NeiazpkUkckEo3Qyjg/3TXld/aS6VfPBJncv3WHcVrKKkrmcJO9mFzoM0fMDiVfToazWgkjbbIjIfcVoxalLGB82fY1pRatDKmy4hDKe+M1zuknsbczRzODSqea6aTStOvRut5BEx7Dp+VZs+g3kB+RPNX1X/CodJopTTJtEnMV2mPp1rU16AwXb+jAMK5+13QXK71ZWB6EV1/iXEunWdyMcoVP1raCstTKpucBccOaq9WzVi4OTmq+K5aj1No7BS0lFYlGjoxxq1qR2evR8815ppZxqdsf9uvQ9/tSY0LLBFKAWUGmLbQx4wi/U04v6d6jZ+M1NzRPucvqerXcd1JFEwiVCR8o5NY7XdwzZaV2PqTW3qGkXFxdSTRsuGOcZqkfD9/1VEI/3qfs4yVyXN3I7LU7uCZdkpA+ma2l1u/Iz5/H+6KzY/Derb/ktS2OeGFRmZo/ldCGXgirhSjbYlyZs/wBt35x++6dflFO/tu9/56j/AL5FYn2oehpftYPatfZQfQnmZtf2zef31/75FH9s3mc5Q/8AAaxvtS46Gk+2IOtUqcQubH9sXTEFhGSD3Snf2vN3hg/74rH+1pjp1oN4mSBz+NDjEVzci1uePhYogPYVL/btwB/q4vyrnftnGVA/OnLdr1kBOOwNHJENTof7fnwP3Uf61ZTWL9yPLsgQfQGsW01i1tDuFqGb1Zq0v+Ex4A+yr/31UtRWyFqcIMntS84pTwetH41yM7U7jeacM+lLThSY0AJHanBhjkHNFKGpaFBvFT20TXdwkMWC7nAzUPX3p6vtwRwR0IofkM2pvC2owruKxn6OKy5LaS3lCSrhqQ3c56zSH6sajLs7AliT6k1KcuoDZwWAxUcXDjNLc8beaZABurVbHPN++bNgC90kYwN3GTXoNsCkSDPQYrgdI51OA+ldbd6tHZRYUhnPT0FQ029C5uyNyW7htIjJNIFUdvWuZ1HxbK+5Lb91GP4u5rm7/VpLmQsX3N61mM7SNuY5rohRtrI5ZT6I0LnU3lcli7nPUmqrXsp4XAqE4x1pm4V0JIm48yu3VzTOp5NNLUmQaAuOPtSE4pmaTNFhClqTNIcU3IosAMcmtXw/CJNVjY9EBb9KyG5NdD4WTM1w+OkePzNVDcTeh2JuVsNFnuSQDtOP6V5dI5dixOSTmu08YXixadb2aH7/ACR7CuGJ5qpsUEPBpGpF60pzWRoPtn2XEbZ6NXcQENGDnrXBqcMD6V2tg4e3Q57Vz1Nztwr3RdwPWka3WRMZ/Sjv1qVXHes7nXJXQWcIhYFuWzxWvFAfNwe45qggHfmrKzkHcDyKqNmcFROJakVRMG9BUKxoCzEfe/WhWEjfMQOeTVlhE0bFDnZ2rSxipMWCElueAMYrTlGxMHv6VmxuWAHAHfNXcr5W0vnA4PrTSG5MswWO21dlbgkEZqp5oeXgDauR9aVZphCsZbA9KaoCjtSk0hRTYFV5KgCqkz4NTSS7VyTis+abe2BWR1QjYiuHIPB6dTWBrM3nKEz0Oa2bmQRxnjPFc7M4lkY44oN7KxiXQ2sfpWa3D1s36cjjHFY7jD1pDc46qsW4zmOm7yrgg4IOQfSmo2EppIrrRzM9j8KayviHw/5U5zcwfK+e/oa5/wAU6cbqyeXb+/tufqtct4U1ltI1uOQn9zL8kg9q9I1MJ9qRiAY5htb3BreEjnqRtqeRZwaekmOlTajamz1Ce2PWNytVKh3TNU7otpcOpGDV+DVpofuuce/NZCmng8U0xcp066va3AAuYVPvitKSey1HTDZpOFYHKk9q4kPinCQ+pq9CeUmv/D2oQ5ZIxKueChz+lYzxvE211KkdiK3rfUbmDGyZgPQ81d/tWG6AW9s0lH97oaxnST2KjNrRnIn6UoFdPLoumXuWs7gwOf4HGRWHfabc6e+2ZMA9GByDXLKlJGqmmJp5xfwf74rvg2O9eeWjhbqIscAMM12yXUUjYBPPQkdaydyi2ZPemb+KjL8cHNML46GlYu5KMZJqaIgHFU1clhirKN0qiDb02dYpwzdNpBrL1rwndapcm70yAyl/vovY1JBLgiuz8IXapdMhNVDcTdjzM+A/EY/5hU35U3/hBvEQ66Tcf9819DtqdrDKsMlzGkjdFLcmnT6jbW7qk1xHGzdAzda3TI5mfOh8EeIgf+QTcf8AfNQt4J8QZydLucf7lfSc1/BbBfOnSPd0ycZqUTAgENkEZBo0E5M+aP8AhDdeBx/ZdyMf7FNbwhrgU5025H/AK+mfN96TzBTSXYXOfMX/AAiuuJnOm3P/AH7obwzrI66Zc4/3K+nfMHt+VVrnUYLYqJWALdAFyaNOwc7Pmj/hHdWA5064/wC+DTToOqjrp9xj/cNfS9tqNvdgmIjI6grU5dO4U/hTshe0Z8iEnvRu9qTnrxRXAzvTHBjmnA03n0pR9KVih+aKTn0oFKxSY8GnZBpn4UtMEOIpyfeWoqki++KVh3GXR+YD2otUMjgAVZ+xtcS8nC8ZNWcR2w2RrmtYQbRy1JWlclib7Kd38Y6VUubp5T8zE0krEcn7xqmzEnmuiFNIylNyY/dmlFNB496TdzVtWJJGPFQk1Ix+WoieeBQtAFzRmm0UbgKabS0000AhNIPekzRSYwNdd4QhJtrlsdWUfzrkM89K73wWoOnSdOZeaqC1InojnvFdx5usyJ/DGoUD8K589a1dfbfrV2f+mhrJNKoOGw4Gn1FmnisrmiDvXS6PcboVQnkVzWea0tMm8uQc45rOojehLlkdcjcdaXdzkVWiclQRip1fHBrFnoluNulWFOR0qjGwq3GxxxSTMpxuMlD7hgcVMvnEbUyMnmlDDOKswy5OABVp2OeUB8UEhwzdavhDtxTEYcZqRpMDqKbZHIh3Cjmo5HC8k1A8/wA3WoJpweKkqMRk0xZ8ZwBVOWQq+AKHlBY81E5UAkmg6IqxWvJf3WB396ycEN7VeuH3nGMfjVNgSelNDkZ9+o2qR19KxZPv1tXpy3PGBWLIf3lXDc46rHL0phNPJAWos11MwHA4PpXo+k6mdU0CMuf3sPyt+HSvN66LwfO/9pPZjnz14HuKqLM5xui14whxqyzYx58Kv+OMGub5rqvGzBdUhtxz5MKqf51ytaSVzOnsKPrT1NNApQPWkkaD804HAqInmnA0wJFIqVSagp6kdKpCZajJyMGtGC6cxGGZVkiPBUispWwauW7fNV2T3M2iO60cB/OtFBjxyO4otrhkdI2Pyg8NW5aAAhlOD6VLe6Z9qCzxYBQfMuOtc1egkro0pzu+Uqb8gHtSFs1OtvAbclUKSDvmlutMuLa0juWKlHGeO1cFzrcHHcpmdIhvY8Cqx16MNiOEsPUtWdqkhG1N3B54rNU+9bU4825nNWOoi145/wBV+Ga6fw14jt479PPzFu4yTxXm0b1pxZKgjiuulQiznnJnoGuajv8AGMJV8rvj5B+lTeM74t4htlDnaoX+dc94Z0canefapbpgYHVtuM7q6rWPD8erajDdvcmPy8ZULnOPeicVBpMmLvqJ42vz51moOB5Z/mK7mxvP9CgBP/LMfyridb0JdZkgY3Bj8sYPy5yKv6hrlvoOmrLPudFwgA6ms01Yb1OuN3j+IUn273rzJvifpZ/5YzdfSj/hZmlnH7u4x9KLisz0/wC2A/xc1iaq0jXqTKpdSNvHauO/4WbpAwGE3P8As0//AIWZo3Qif/vg0tR8rOz053E+8LtQDHPetcTZrzhfiZoYP35QP9w1MvxN0Dp5s3/fs1cWTys8Uoz7U7ilwPSvPPSQ0E04UfL3NPVV9aWhSQ3NLmnlBSeXx1qbjAEUvvSeWaXYaQxwwangjJYMR8oqKKPJyegqVpuCF/CtoQ5jKdRRJ5bjHyoOelRgEKWY81HF13HmllfjGa64RsjkbbZFI2Sc1AetKx5pmaoEPGcdaQ9aBSNU9QHdRTTQpytIR70wFpMUY96PxoSADTT6elBPvSGgBKTvSmkHWpATvXf+B/8AjwnHpKMflXA967DwTd+XcTWzH/WAMo9xVxFPY57XcnWbvP8Az1b+dZeOa1teGNZu/wDrq386yiKKgQ2Gng04ZptOWsDQdU8J2sKhxVlI9yA0pK5UW7m7Y3OUAJrRX5u9c7buYz1rctZQR68Vg0ejTndal1DjrVlH44qjuPXPFSByp5osablt3NLHcMjcGoA/mCmkFXosZOJsJeDrkU57pW4DdfSsfYT3qwmVUZoJ5UWzIc9e9RuxNM+btRsIOf5mguMRjLznvVeVs8HpVxgM1VkTJzmgq6RQcFjxTSnFWGiLNgdqUwHp1q7Iyb1OdveHY5rFOS+fetjU2wW55rGU81cNzkqvUc3A6Uwc096jBxWzZkh6qWYKoJYnAA71674D8G/2ci392N13Ivyr/wA8x/jXktozfa4SDht459K+kGuLbRvDD6jM6lhF8vua6aHLa7ObEOWkUeH+KZfO8Q3r5yPNIH0HFYgqxeSme4kkY5LtuNVqc9GVBWQ8CjvSDFLxU3LCnD8aTPFKOe9IBwp209aQfWn8AcmqQhV+tTxuarryepxUyqc1oiGbNnPtGM84rbsroAgg8jqPWuVjYoR7VqQykAMK05eZWMWranRNa2S5nnmxG/3VxVtWtb7TfswcFRwDXNT4vbbyHbaTyh9DWENSu7HNqcq6tzu5ry8Th3HWJ3UK/N8Rs63YLLaS26AB4+VIA5riWRlOGUg+9dBeavJIufOV2I+6EwBWbLfzXB/foj4HAxWFGUlua1LPYpoSK0reVRFzWVsbPFKDKoxXoU6vK7nNOFz0LwbPte5bnbgD8a683nHU15BY6rfWKFbeUICcnitE+KdVZQPOUY7hRUVZ80rkwhY9NN1x3rnvF0pm0gIo3AOCQa5IeJ9VBybgH6qKr3mt6hfQmKaVSp9BWN2aKNim0G77kK/99UC2cDmIf99CoEMkbDDcVpR6nHGgVrVX9zVIGrFI2r/88h/31U0WmXE/CW+T6lgKsnVYe9ilIdUiP/Ltj6GmkAf8I9ejkwp7YkFNOg3w6wL/AN9inf2pERnyGx/vU7+04sYEDfXdVoRjeXjvSqDUO4+tOBPrXnI7USMoPNAT0pm6gORQx3H7Go2tSCRqXzGosMUbhUiKzMB3NMV8mr0YCJuIw1VGN9CZz5UQyfINuaiFJI5ZzQtdcI8uiONtt3Jgdqe9RMwY0OcCogea0RIHpTCOal4qM4oBBTGpw5Pamt0pMYqYIpTTU+7SkYPegABxQT2oo6jpTACOOtN7U7HHWjH0oYDDSDrTjRikxoQ1e0rUP7O1KC6xkRtlh6jvVIjiomPNTsDNXXrm3vNYuLi2J8qRtwzxWZxSA5pau90EVYYcUDrSn6UAVnbUoeOlX7QgoRwcGqANXLJgJcHuMUNaDT1LDR4ORwKuWkpUY/OmsoZMdKZGNj1zyR0x0NiNs89jVlRkfSs6CTHFaMWSM1B1RldEkaHPHFSbcHnmhARTmGBmmTIVWAPSpEYH3qqxPYHFKhOaBF9PwpSMckU2Ne+akbAHXikURHOOKiZCRzUxwccYFAUs3Q4ppEsqGPAPalZNsDSE4wM1aMQA9WqhqUhjtnB47fWrukTY4/U2DSHGPWs1BzVu8O52PqarJitobHHN+8wfrxUeKkI5pNhxVtECRsqtzXS6l4ov9U0+2s5pcQwKFCjvXMFcGrKD5RV05W0Ikr6jyeaDSU7tWj1EIKdim96eCMUAL9aSlAo5oEOQZ705sDgGiMd6RvWrSExUYg1ZUHOaqKeauJjiqRLHE4PWtTT8yQsPSsiRhmtLR2zIwHUit4voQ9iSZiBkfLt6UXdqNTsDcIv+kxDkD+IU64QgsKbp1wbe4BJwCeaVSCkrEptO6OdI9aMZre17TVRxe24zDJy2P4TWDg15U4crOyEuZXExSbRmnDOKWlcoaBilxS0UhWExS4FFFMYYpdvFHFO9/agBAF70bRnrTgR6ClAX61QrCADpS7R9KXFGMDqKpMVjMFOGaZhh2pcnuK4bHWnYkGfSnAH0qNWyakG48AUD0GkGlApGJBwQc06EGR9oFANli3hydxHSpZnIXb1p3CgIo6VBIcnFddKCSuclSXMyEnJzTl5PSlwPShc4PFaWMxkp5xmoe9Oc5NIAfSmCHjkUjfSkBpT7CmAymtxTjTWOR0pDHIMrTu1Nj+7S0JAJntSg0hNA+lMB3am80tAzmkAmD6UY5pefpQBzQMVh8tV3XmrJqJhntSsFyEZBpwOaUr3pMYpbDClA5pM8dKcPakAVLE20g+nNRU9aaA6CBfNhEg70xkIfpTtJcNE6MRgdKtSRYI6/lXPNWdjrh7yuRoNoyK0LVyeKrLHhBVm3XHNRY2iaaAbRxmkZcnmmxnGCal4I5zikNogZacir6GpeMYqN8g8GgSLKngUNkkURtxyakA3GgYkcRY4xUyx7Pc1Ii4GFpxU554pkNsgdcDOK53X5RHEFzya6KVmxiuE1+8FxqJRD8icU4rmYpSUY3MafPGe9MRSBTpfvd6aK6krHEwI5pW4HFL+Bo2k1QhiqWNTAUirin4x2qoolsMUoUetHJ7U7AxWiEMI9DQB70pFJgikBKOlIRz0oU5p1NK5DHoCFJqJs+tTHhOhqBiTVvRAOTk1bXO3j86qRjHbNWl4FVBCYxslsGr+lZ+1Dms5utXtMP+lJ65rSL1Ilsa9yuJDgdapiP5s9KuXZxKMUNFlN3tmtbGSHWToxa1uDmOYYwfWuev7F7C8aB+QPun1FTTTnzt6kgjpz0rTvMappHnAZmg647iuXEU7q6NoScWc4eBSUtNrzrHWLS0lFAC0cn60e1GaAFA96XBoGKCfQUwFwMUoHHTmmhiKdvx+FNXEOAOOakjt5J38uJdx6+lQFie1OhmkhlDIxVhSeg42vqUOtOKDaeaaetL/DXMdA0cHipUOOahHXrUinikxpjuWbNXI4xFFuI5NRQr3OOKmYkjFa0ablqzGrPohmTjNVnJzUzcKarsea6rWOcevShjtWlTGM0yRu1AELHJ705QSKaTzT16U9UA3oetL+NNYcU5T8tMBrVGT71KenSomPNDGSJ90U7vQvSihXEIaMVPDbSzn5F4HU1qweH5HALTKM+1XGnKWwnOMdzEowc106eF1YfNcH/vmpV8MW4I3SMc9a1WFmzJ14HJ05FJOBzXVnw/Zx87XYe5qS00+CO4/doAPpT+qSvqJ4iKWhyRikP8LflUbIV6givSBEgTG0flWB4jjUQoQoHNaPCWjczhi1J2OTwaNvHNP70cVxNHYmQlTSqPSnkUq9elTYq5FIhHNOgOW2npUj/dNVskNkVL0JTN/S2Edyo7Nwa6JYtwz1rlLOX5QR94V01lcGVAMc9zU1I31R00H0HtCATnODTok2tir6ruiwB1pogJOQKwSZ1pjogCMccetPK54FJGpB65q3HGpOeQRSsNlJo2QZyaTYXI6ir7ICOajWMAnkCnYLkAjO70q5Eg/GmeWCwxg1OgOeeKLCkyYAAUx/rT/MGOBUbyrtJJwB1osZGPr98LKxJ3fvX4UV58hLSFic1ra/qR1C+OMeXHwtZKna2K3pwsrnPVnd2GTD56aqlulWyvmDAHQUxV21vymHMQ7WHelApzEk0goSFccPenUAcUoGK1S0FcO1FFKKLCuIenFMOal6daYcZ5pNBcFqUAY96iBqVTmnETFc/Liq5PNSzt0FQDrRLUEWYqnqGPgdKeWHrVxvYTBqt6bzdKKpmrmnf8fC1rT3Jlsac5ZpOuCOKtwMJIVz1bg1RlYlvrTba5ZrlUX7oOBWxiZt3H5Vw6eh4rU0GcLcCJ+Vfg0zXIDHejgYZQaXSI90vyj5hyKzcbl8y5TJ1C3NrfTQ9lbj6VUyTXReJ4R9phnCkeYpBPuK5+vMqxtI6qcrxEzRzS49KO9ZGgZopQPWlpgJilGad04xS0AN2n1pdhNLgZ6U8nFNMQgGB1q3a3McAw0YOT1zVIsc5x1pYQskqiRiFzSnsaU5OMrooEU44Ef400U5z8grmNSJevNWYV3HFVl69K0bSMKhkY/QYoSbdhNpK4rgLhQaTPHWk3Avk+tD9M5rugrI5JO7I5WzUGeae5qLvTJJV+pqN+ven9BmoyeaEMYce9PU8Uzj6U9cYoe4xGpFPOKVqb0NU0CHMeMVF1bFSnG3NRry9SBN0FIOtHbFHSrixF1Jnjt0CErzzj6111g2YkJ6kVxoGbcdTz0rrrD/AFCfSu+g9Djr7GsACtNNVr6UxWTuHK+9crJfgkh5pmH1rrc4x3OOMJSZ1M0iKpyw/Oks13NuB4rlois0qKgbLHuc12FpGI4lHtUqak9CpLlRYIFc74nK+RGM85rozjHWuH128+03rhfurwKdSSjG4qEbzMjvRiijpxXjy1Z6yEI/GgdaWm9DUsoc54qsw54qw3IqEis2BdtmXGR1NaNtfNBKBk7T1rFtWxKFzwauN1zWkEnGzGpOLujurOdZYg0bAir2wlcg1xWk6k1nIFY5jPUeldrbyCVFKHKkZBFYTi4s7IVFJXI9hBztqwg9M0pXA4GaWNM+ue9ZNGnMLjPfHsaQoc84qQpz0Jz60pjpBcYkdSH0pv3RyTUU9zHDGXd8AdSaCWSSNhDXM65rCJC1vE2ZG4JHaqWqeI3uGaO2ysfTd3Nc875OCa1hTb1ZjOpZWQ1yOaaAetKQM0ighsdRmug5izDwwOKJk2k46Gli45qaba0Bbpit1sZvczieaetJ1OaUVFh3FpaCeKQVSELSik/CloAcBTTT1xQwFMREKlSmECpENJDIZ8bqZHyadO3zelJGKnqBZXjvSEjPNA6UhrXoSLnnGav2XEoPpVBfvCrtudvStKe4pbF26cLHkfSqVrcbJlqW6ztHtVBeG5NW5amaR0mvruitpQPvLiqWiTiLUYwejHBrQuiLrw1C/VkIBrEtiUuUI4INUiV8Njo/FluF0uJwPuy/zriz3r0LxCgn8LGQfwlT+teenGTXnYltSOnDu8QBpy4603jFGa5ToFyOwoz3pM0vGeaYC5z0pfxOKTilJG3GaYhaBg03cAOKTJJ4oQD9w6ZpueaTHvTh05qtwKQ605/uAU0DmnMpJGMk9q4kjq2Q63iLv0471fkbZHj8BViXTW0+yheXAdxlhnpVKQ56muqlBLVnJOd9ERZOelK2QKSkY8VsQQtnNIPzpxHFNpgOPC1Gc08nIphpAMNPXpTKlXpQ9xjWPFMNSGmN6U0CE6ikQZpKfH9aWjGPx70DrRR1qkIvW08KRbZAcg8YGalF2FcbbmUL9KzKM1vGs4qxlKnc0by+eUbFkYp7nrVONd74pnWrFuMc9+gpObmyVFRWhtaJa+ZMzn+HpXVIoC9s1n6RbeVaJnqa0iMV6FKNkedWldmfq94LWybB+ZuBXCyksxPrW74iuvNuhEDwgxgetYLdawxNT7J04WFldkZoApTRiuFnaGDTCOal28elMapGgPK1ERUwHFIy0WGV+VYEdjWjHIs0QIqkyZFMjkaJ80k+Vg0aIGG6VtaPrDWcmyQkxH9KxI5o5F6gH0qTA6g1q1GSFGTi7npUMyTxrIh3KehFTDrXDaPrMlg/luxMJPI9K7O3uUmjDDv3zXHKDi9TshUUldFjbxnPNR7iD6U7eAM9qhlk3ZwKhmlyO6n8qFm3AAcmuE1LVJb6UrnEKngDvWlr+pcm2jJ/2jXNM2OBWlOF9TGrUtogZuwoC4570IOMmngZ6V0I5bjcc5pyr6CnHavLGmBz/DwK0SJbJ12Ac9e9RSylht7elJ2+tM78U2yRoBzT+lAApQKBiUmaesZdtqqWPoBmr0Wh6nOu6OxmYHvtxQ2kBn5pcGtX/hG9XUf8eMv6VVmsLu1OJ7aWLP8AfXFJNMVysvA6U4g4zigD0IqULla0QmV8GnqOKUgZpQOKSQyrOP3mKfGMCmzD97Tx8oqOoyXnFMpUYH3pH+9xWlyRyCrcRwMmqqYqWM7mA7Crg9RMt3BP2bdnvVAdRitCY/6MBWeD81XJiR1Nh++8Nzoe3NYA+SQc8A810GjHdo9ynqKxHX52HTmtEYrqdpMBceCrgAEsEz+tecHk16tocHneGpEPJYEV5dLEUkYEYwSK4MUtTbDPcixRilIxSHgVx6HYGDQaUDjrijFMBM8UlLinKmRz3ouA3BpR6VJt+bAp23B/nTAZgdKdjA6Uh9AaQ800IrwwPIc4wB3rTs4kFzGPL3YOaYWCjAXHpTrKUpfRHcR82KUKNtWOU2yfWluRdL9oBwwyvpWU5Oa6fxEuYLWUk85AB/CuWc8+1b2VtDCIZppzS8YzTSaRQhBpuOafjimUgFOMVHTiRSHpTQEZ608dKZjmnLx3pNalD+1Rtmnk0w0yWRZ5qZOlQHrU69Khbj6DvpRz3pBj1pc1YMMUoGaTinCqsIeiEtgVq2Ft592kY5VOTVC3Tqx7V0mg2pVDO/Vj3rejC7OetKyOghXaowDikuZRDC8h/hXNOU4FYviG7EVuIR9569Je7HU8uzlKxzFzIZZmkPUnNVmB7CpSc0w8d68uq+aVz1YKysMP0FIKU80maxsaIUk0xvpT+o60xs0DQJyKk7dKijxmpTQtyhOD6VFJCG5HWpRz2p2PSr5UxXKJRlqzbTlRhjT2QEdKrSREHiocWth3uaQdXPBGa3/D+pmKf7NM3ysPlJPSuLV2Rsg4NaENzv74YUpPmVmVBuLuj0svxwRWRrOpi1h2qfnbsDWZZ+IAlmUmz5qjj3rDu7uS6mLsTyeKwUHzHS5pK5DNKzuWY5YnmmIpY8inKnc9afuVPrXQo2RyybbFC45PAphl5+UUxmLHmjHvVpIi4nLNzzUijFNXGak6VSQMRs0wUp5OBW5p2mW9vaf2lqf+r/5ZQ93NTOSSuxFSx0a8vgHRNkP/AD0fgVpR2eiacym6le8lH8CcLVK/1e4vmwzeVCOFjXoBUVgIZr+CK5YrCzYYiseac9tCnFJXka58QSIRHpllFbg8AKuTUyyeJL4th5QB97GFxWpcppGiHfE6h8Z2feNV08RzEypZWjESfxMOlWqK3epDq22RWNjrccUkxvGAQZ4kzmq9t4i1WFV3sJ4zxiVOtd1o+kvqGjpcTOFmkB+Urx+VZ1xLaQSLa6rbiHySQhC/Kwp+xiL2r6o543OiaugjurX7FP2kToao6l4eubBfOiIuLXqJE7fWul1nw3ZTW32uykEQK5EarkN9OeKwLe+1HQbjy5Y32H70Mg7UWnDValWjLYwCtAFdXf6Pa6tZnUdHADDmW37j6VyzoyE5BHrW8ZKSuidim/Mx+tEhwvpSuf3xpk54zUuyKFiNSd6rwtU5OBSTuDQ7dgYqWEnIqsp55qzB9/61pDcTWhcuCRCq1RUc9asXLZKgdAKrqMtz0rRvUlHVaGcaXcDr+FZTLmc/71a2kMFsHTrx2qiseZwMdWrVHO+p6PoMITS4VAxnJxXmmsQ+VqcsZHAkbj8a9W0zEMVuuOAtea+IEI8Q3K4/5aZH5152PTSujfAq82jY07wdp+qW0ElwZkZh/A2K0z8ONGAyJLnH+/W1oaYtLfjGFNa56V4vtZLqem6aOKPgPSY2VgJm56O/FM1fwtpOmsubTcJB13Hiuz2hpUX1arGtaNJepbhIyyrkkCh1Z20FGCvqeYxeHtIlcALIOeRv4rqYvB2iCMA2QbjqWNKfDW25QiRkAP3WU810W0Lx2qadapbVl1qUVsc6/gzRH/5cwOOzGs678DaYsZaNZRj0auzqO44if6Vqqsl1MOQ8M121/snWLi0VTtTBUnqQRWYbnj7tdN8QQR4lJPeFOlcmQBzXVGTauJqx/9k=";
                VerifyResponse responseV = null;
                while (bRunning)
                {
                    LOG.Info("BusinessRuleSportlifeNorte.PingVerifyService IN...");

                    sToken = _HELPER.GetToken();
                    //responseV = _HELPER.Verify("21284415-2", Config.DynamicDataItems["ServicePointName"].ToString(), photo);

                    //Hace Ping y verifica que de resultado ok, sino para
                    //bRunning = (responseV != null && responseV.data != null);
                    if (string.IsNullOrEmpty(sToken))
                    {
                        LOG.Fatal("BusinessRuleSportlifeNorte.PingVerifyService - NO Pudo obtener Token valido! Revisar...");
                    }
                    else
                    {
                        LOG.Info("BusinessRuleSportlifeNorte.PingVerifyService - Ping continue => [Expiration = " + _HELPER.GetExpirationToken() + "]");
                    } 
                    
                    //Duerme el timepo configurado o 5 minuto sdefault
                    //(1000 miliseg x 60 seg x VerifyTimeout minutos = default => 
                    //				1000 x 60 x 5  milisegundos = 300000 es 5 minutos)
                    Thread.Sleep(_TimeThreadPingVerifyService);

                    //Para test uso 1 minuto
                    //Thread.Sleep(60000);
                }

            }
            catch (Exception ex)
            {
                LOG.Error("BusinessRuleSportlifeNorte.PingVerifyService - " + ex.Message);
            }
        }

        #endregion Thread Verify Service

        /// <summary>
        /// Revisa 
        /// </summary>
        private void ActualizeAccessToken()
        {
            try
            {
                LOG.Debug("BusinessRuleSportlifeNorte.ActualizeAccessToken IN...");
                Config.UpdateItem("ServiceAccessToken", "794C0A6E5CE5480886E9F6CBC523094AC0535C58FA79EB81C6367902B5E204EC7EFF765F76DA693764A9EEFCE16A090F");
                //Consumo servicio
                //http://jano.biometrika.cl/AutoClub_Test/API/api/Account/Login/
                //              totemvehicular @autoclub/fdbfeec3c613d7d0fcf6aecaf5c5be9a/TotemVehicular
                //string sURL = Config.DynamicDataItems["ServiceURL"] + "api/Account/Login/" +
                //                Config.DynamicDataItems["ServiceUser"] + "/" +
                //                Config.DynamicDataItems["ServicePassword"] + "/" +
                //                Config.DynamicDataItems["ServicePointName"];
                //LOG.Debug("BusinessRuleSportlifeNorte.CheckBusinessRule - LLama a " + sURL + "...");
                //var client = new RestClient(sURL);
                //client.Timeout = (int)Config.DynamicDataItems["ServiceTimeout"];
                //var request = new RestRequest(Method.GET);

                //LOG.Debug("BusinessRuleSportlifeNorte.ActualizeAccessToken - call execute...");
                //IRestResponse response = client.Execute(request);

                //if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                //{
                //    AccessModelRL oResponse = JsonConvert.DeserializeObject<AccessModelRL>(response.Content);
                //    if (oResponse != null)
                //    {
                //        LOG.Debug("BusinessRuleSportlifeNorte.ActualizeAccessToken - Parseando resultado...");
                //        if (oResponse.Status.Equals("Success") && !string.IsNullOrEmpty(oResponse.Data))
                //        {
                //            Config.UpdateItem("ServiceAccessToken", oResponse.Data);
                //        }
                //        LOG.Debug("BusinessRuleSportlifeNorte.ActualizeAccessToken - Response OK");
                //    }
                //    else
                //    {
                //        LOG.Debug("BusinessRuleSportlifeNorte.ActualizeAccessToken - Error parseando respuesta del servicio...");
                //    }
                //}
                //else
                //{
                //    LOG.Debug("BusinessRuleSportlifeNorte.ActualizeAccessToken - Response Error");
                //}
            }
            catch (Exception ex)
            {
                LOG.Error("BusinessRuleSportlifeNorte.ActualizeAccessToken Excp Error: " + ex.Message);
            }
        }

        private Image SetImageFromB64(string base64Photo)
        {
            Image ret = null;
            try
            {
                if (!string.IsNullOrEmpty(base64Photo))
                {
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(base64Photo));
                    ret = Image.FromStream(ms);
                    ms.Close();
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("BusinessRuleSportlifeNorte.SetImageFromB64 Excp Error: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Formate rut en formato NNNNNNNN-N
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        private String FormattingTaxid(string rut)
        {
            int cont = 0;
            String format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {
                    format = rut.Substring(i, 1) + format;
                    cont++;
                }
                return format;
            }
        }

        public int Dispose()
        {
            int ret = 0;
            try
            {
                if (_tPingVerifyService != null && _tPingVerifyService.IsAlive)
                {
                    _tPingVerifyService.Abort();
                }
                _tPingVerifyService = null;
                LOG.Error("BusinessRuleSportlifeNorte.Dispose - Rescursos liberados!");
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BusinessRuleSportlifeNorte.Dispose - " + ex.Message);
            }
            return ret;
        }
    }

}
