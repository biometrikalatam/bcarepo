﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BusinessRule.SportlifeNorte
{
    /// <summary>
    /// Para parsear respuesta desde Servicio desde Idealsur
    /// </summary>
    public class AccessModelR
    {
        public bool ERROR { get; set; }
        public string ACCESO { get; set; }
        public string RUT { get; set; }
        public string NOMBRE_COMPLETO { get; set; }
        public string FOTO { get; set; }
        public string MENSAJE { get; set; }
        public string TIPO_ACCESO { get; set; }

    }

    class Models
    {
    }
}
