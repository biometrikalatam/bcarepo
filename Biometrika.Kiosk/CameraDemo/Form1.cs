﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using Emgu.CV;
//using Emgu.CV.CvEnum;
//using Emgu.CV.Structure;
//using Emgu.Util;
using System.IO;
using Svg;
using AForge.Video;
using AForge.Video.DirectShow;
//using DirectShowLib;
//using Emgu.CV.Reg;
using System.Text.RegularExpressions;
//using Emgu.CV.DepthAI;
using Capture = Emgu.CV.Capture;
using Emgu.CV.Structure;

namespace CameraDemo
{
    public partial class Form1 : Form
    {
        //private VideoCapture _capture = null;
        private bool _captureInProgress;
        private Mat _frame;
        private Mat _grayFrame;
        private Mat _smallGrayFrame;
        private Mat _smoothedGrayFrame;
        private Mat _cannyFrame;

        FilterInfoCollection filter;
        VideoCaptureDevice device;

        int CameraIndexIN = 0;
        int CameraIndexOUT = 1;

        public Form1()
        {
            InitializeComponent();
            //CvInvoke.UseOpenCL = false;
            //try
            //{
            //    _capture = new VideoCapture();
            //    _capture.ImageGrabbed += ProcessFrame;
            //}
            //catch (NullReferenceException excpt)
            //{
            //    MessageBox.Show(excpt.Message);
            //}
            //_frame = new Mat();
            //_grayFrame = new Mat();
            //_smallGrayFrame = new Mat();
            //_smoothedGrayFrame = new Mat();
            //_cannyFrame = new Mat();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //string imgsvg = System.IO.File.ReadAllText(@"c:\acode\img_sgg.svg");
            //var byteArray = Encoding.ASCII.GetBytes(imgsvg);
            ////MemoryStream ms = new MemoryStream(byteArray);
            //using (var stream = new MemoryStream(byteArray))
            //{
            //    var svgDocument = SvgDocument.Open<SvgDocument>(stream);
            //    var bitmap = svgDocument.Draw();
            //    pictureBox1.Image = bitmap;
            //    //bitmap.Save(path, ImageFormat.Png);
            //}

            filter = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            int index = 0;
            int selectedIndex = 0;
            foreach (FilterInfo device in filter)
            {
                cbCamaras.Items.Add(device.Name);
                var deviceN = new VideoCaptureDevice(filter[index].MonikerString);
                //if (deviceN.Source.Equals(@"@device:pnp:\\\\?\\usb#vid_045e&pid_0812&mi_00#7&1daac9fa&0&0000#{65e8773d-8f56-11d0-a3b9-00a0c9223196}\\global"))
                //0->@device:pnp:\\?\usb#vid_1bcf&pid_28c1&mi_00#6&269dc56c&0&0000#{65e8773d-8f56-11d0-a3b9-00a0c9223196}\global
                //1->@device:pnp:\\?\usb#vid_045e&pid_0812&mi_00#7&1daac9fa&0&0000#{65e8773d-8f56-11d0-a3b9-00a0c9223196}\global
                //

                //
                //0->usb#vid_1bcf&pid_28c1&mi_00#6&269dc56c&0&0000
                //1->usb#vid_045e&pid_0812&mi_00#7&783613d&0&0000

                if ((deviceN.Source.LastIndexOf("#") - deviceN.Source.IndexOf("usb#")).Equals("usb#vid_1bcf&pid_28c1&mi_00#6&269dc56c&0&0000"))
                {
                    CameraIndexIN = index;
                } else if ((deviceN.Source.LastIndexOf("#") - deviceN.Source.IndexOf("usb#")).Equals("usb#vid_045e&pid_0812&mi_00#7&783613d&0&0000"))
                {
                    CameraIndexOUT = index;
                }

                    richTextBox1.Text += index + " -> " + deviceN.Source + Environment.NewLine;
                richTextBox1.Text += index + " -> " + deviceN.Source.Substring(deviceN.Source.IndexOf("usb#"),
                                                                               deviceN.Source.LastIndexOf("#")- deviceN.Source.IndexOf("usb#")) 
                                                    + Environment.NewLine;
                //                                  usb#vid_1bcf&pid_28c1&mi_00#6&269dc56c&0&0000 
                if (deviceN.Source.Contains(@"usb#vid_045e&pid_0812&mi_00#7&1daac9fa&0&0000"))
                        selectedIndex = index;
                index++;
            }
            cbCamaras.SelectedIndex = selectedIndex;
            device = new VideoCaptureDevice();

        //    DsDevice[] directShowCameras = DsDevice.GetDevicesOfCat(DirectShowLib.FilterCategory.VideoInputDevice);

        ////private int getCameraIndexForName(string name)
        ////{
        //    for (int i=0; i < directShowCameras.Count(); i++)
        //    {
        //        //richTextBox1.Text += i + " -> " + directShowCameras[i].Name + " | " + directShowCameras[i].DevicePath + Environment.NewLine;
        //        richTextBox1.Text += i + " -> " + directShowCameras[i].DevicePath + Environment.NewLine;
        //        listBox1.Items.Add(i + "-" + directShowCameras[i].Name);
        //    }
                //if (directShowCameras[i].Name.ToLower().Contains(name.ToLower()))
                //{
                //    return i;
                //}
        //    }
        //    return -1;
        //}

        }

        private void ProcessFrame(object sender, EventArgs arg)
        {
            //if (_capture != null && _capture.Ptr != IntPtr.Zero)
            //{
            //    _capture.Retrieve(_frame, 0);

            //    CvInvoke.CvtColor(_frame, _grayFrame, ColorConversion.Bgr2Gray);

            //    CvInvoke.PyrDown(_grayFrame, _smallGrayFrame);

            //    CvInvoke.PyrUp(_smallGrayFrame, _smoothedGrayFrame);

            //    CvInvoke.Canny(_smoothedGrayFrame, _cannyFrame, 100, 60);

            //    Image<Bgr, byte> image = _frame.ToImage<Bgr, byte>();
            //    captureImageBox.Image = new Bitmap(new System.IO.MemoryStream(image.ToJpegData()));
            //    //grayscaleImageBox.Image = _grayFrame;
            //    //smoothedGrayscaleImageBox.Image = _smoothedGrayFrame;
            //    //cannyImageBox.Image = _cannyFrame;
            //}
        }

        private void captureButtonClick(object sender, EventArgs e)
        {
            //if (_capture != null)
            //{
            //    if (_captureInProgress)
            //    {  //stop the capture
            //        btnStart.Text = "Start Capture";
            //        _capture.Pause();
            //    }
            //    else
            //    {
            //        //start the capture
            //        btnStart.Text = "Stop";
            //        _capture.Start();
            //    }

            //    _captureInProgress = !_captureInProgress;
            //}
        }

        private void ReleaseData()
        {
            //if (_capture != null)
            //    _capture.Dispose();
        }

        private void FlipHorizontalButtonClick(object sender, EventArgs e)
        {
            //if (_capture != null) _capture.FlipHorizontal = !_capture.FlipHorizontal;
        }

        private void FlipVerticalButtonClick(object sender, EventArgs e)
        {
            //if (_capture != null) _capture.FlipVertical = !_capture.FlipVertical;
        }

       

        private void btnStart_Click(object sender, EventArgs e)
        {
            device = new VideoCaptureDevice(filter[cbCamaras.SelectedIndex].MonikerString);
            device.NewFrame += Device_Frame;
            device.Start();
        }

        private void Device_Frame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap bitmap = (Bitmap)eventArgs.Frame.Clone();
            pictureBox1.Image = bitmap;
            
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            device.Stop();
        }


        private void btnTakeFoto_Click(object sender, EventArgs e)
        {
            int cameraid = 0;
            Bitmap imageTook = TakeFoto(Convert.ToInt32(txtIndexCamera.Text));
            if (Convert.ToInt32(txtIndexCamera.Text)  == 0)
            {
                pic1.Image= imageTook;
            } else
            {
                pic2.Image = imageTook;
            }

        }

        Mat mat;
        Image<Bgr, Byte> img;
        private Capture _CaptureCam;
        Bitmap _CurrentImage;
        private Bitmap TakeFoto(int cameraid)
        {
            //cameraid = 0;
            try
            {
                _CurrentImage = null;
                if (_CaptureCam == null)
                    _CaptureCam = new Capture(cameraid);
                cameraid = listBox1.SelectedIndex;
                mat = _CaptureCam.QueryFrame();
                mat = _CaptureCam.QueryFrame(); //Leo de nuevo porque sino se queda con la imagen anterior. NO SE PORQUE AUN!!
                img = mat.ToImage<Bgr, Byte>();
                _CurrentImage = img.ToBitmap(); // (Image)picVideo.Image.Clone();
                //_Image_TakeFoto = GetB64FromPictureBox(_CurrentImage);
                mat.Dispose();
                img.Dispose();
                mat = null;
                img = null;
                _CaptureCam.Stop();
                _CaptureCam = null;

                //_CaptureCam.Stop();
            }
            catch (Exception ex)
            {
                //_CurrentImage = null;
                //_Image_TakeFoto = null;
            }
            return _CurrentImage;
        }
    }
}
