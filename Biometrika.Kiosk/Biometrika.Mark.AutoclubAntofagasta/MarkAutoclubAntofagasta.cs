﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Mark.AutoclubAntofagasta
{
    /// <summary>
    /// Implementacion de IMark para AUtoclub. POr ahora es Dummy porque el registro se hace en el mismo momento 
    /// del chequeo den BusinessRule.
    /// </summary>
    public class MarkAutoclubAntofagasta : IMark
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(MarkAutoclubAntofagasta));

        bool _Initialized;
        string _Name;
        DynamicData _Config;

        public DynamicData Config
        {
            get {
                return _Config;
            }

            set {
                _Config = value;
            }
        }

        public bool Initialized
        {
            get {
                return this._Initialized;
            }

            set {
                this._Initialized = value;
            }
        }

        public string Name
        {
            get {
                return this._Name;
            }

            set {
                this._Name = value;
            }
        }

        public int DoMark(DynamicData parameters, out DynamicData returns, out string msg)
        {
            int ret = 0;
            returns = null;
            msg = null;
            try
            {
                //object o = (parameters != null) ? parameters.ParamsList["key"] : null;
                if (parameters == null)
                    LOG.Fatal("MarkAutoclubAntofagasta.DoMark - Parametros NULO!");
                else
                {
                    foreach (DynamicDataItem item in parameters.ListDynamicDataItems)
                    {
                        if (item != null && !string.IsNullOrEmpty(item.key))
                        {
                            LOG.Debug("MarkAutoclubAntofagasta.DoMark - key=" + item.key + " => value = " +
                                        (item.value != null ? item.value.ToString() : "NULL"));
                        }
                    }
                }
            }
            catch (Exception ex) 
            {
                ret = -1;
                msg = "MarkAutoclubAntofagasta Excp [" + ex.Message + "]";
                LOG.Error("MarkAutoclubAntofagasta.DoMark - Excp Error: " + ex.Message);
            }
            return ret;
        }

        public int Intialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("MarkAutoclubAntofagasta.Intialize IN...");
                if (!System.IO.File.Exists("Biometrika.Mark.AutoclubAntofagasta.cfg"))
                {
                    Config = new DynamicData();
                    Config.AddItem("URLService", "http://localhost/BS");
                    Config.AddItem("URLTimeout", 30000);
                    //_Parameters.Add("", "");
                    if (!SerializeHelper.SerializeToFile(Config, "Biometrika.Mark.AutoclubAntofagasta.cfg"))
                    {
                        LOG.Warn("MarkAutoclubAntofagasta.Initialize - No grabo condif en disco (Biometrika.Mark.AutoclubAntofagasta.cfg)");
                    }
                }
                else
                {
                    Config = SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.Mark.AutoclubAntofagasta.cfg");
                    Config.Initialize();
                }

                if (Config == null)
                {
                    LOG.Fatal("MarkAutoclubAntofagasta.Initialize - Error leyendo BusinessRuleAutoclub!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                _Initialized = true;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("MarkAutoclubAntofagasta.Initialize - Excp Error: " + ex.Message);
            }
            return ret;
        }
    }
}
