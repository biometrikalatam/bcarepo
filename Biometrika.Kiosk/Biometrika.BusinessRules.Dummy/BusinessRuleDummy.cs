﻿using Biometrika.Kiosk.Common.Interface;
using Biometrika.Kiosk.Common.Model;
using Biometrika.Kiosk.Common.Utils;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using Biometrika.BusinessRules.Dummy.Models;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BusinessRules.Dummy
{
   
    /// <summary>
    /// Implementacion de BusinessRule Dummy para demos de kioskos (Exponor). 
    /// Levanta a memoria datos para verificaciones, y con eso acciona
    /// Los datos se almacenan en un JSON llamado DatabaseDummy.json en el path configurado en config del BusinessRule 
    /// </summary>
    public class BusinessRuleDummy : IBusinessRule
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BusinessRuleDummy));

        Hashtable _HT_PERSONAS = new Hashtable();

        int _Q = 0;

        bool _Initialized;
        DynamicData _Config;

        public DynamicData Config
        {
            get
            {
                return _Config;
            }

            set
            {
                _Config = value;
            }
        }

        public bool Initialized
        {
            get
            {
                return _Initialized;
            }

            set
            {
                _Initialized = value;
            }
        }

        public int Initialize()
        {
            int ret = 0;
            try
            {
                LOG.Debug("CheckBusinessRule.Intialize IN...");
                if (!System.IO.File.Exists("Biometrika.BusinessRule.Dummy.cfg"))
                {
                    Config = new DynamicData();
                    Config.AddItem("PathDatabase", @"\Data\DatabaseDummy.json");
                    //Config.AddItem("Enable", 30000);
                    //Config.AddItem("ServiceAccessToken", "794C0A6E5CE5480886E9F6CBC523094AC0535C58FA79EB81C6367902B5E204EC7EFF765F76DA693764A9EEFCE16A090F");
                    //Config.AddItem("ServiceUser", "totemvehicular@autoclub");
                    //Config.AddItem("ServicePassword", "fdbfeec3c613d7d0fcf6aecaf5c5be9a");
                    //Config.AddItem("ServicePointName", "TotemVehicular");
                    //Config.AddItem("ServiceType", 1); //0-Solo Check | 1-Check y Register

                    if (!SerializeHelper.SerializeToFile(Config, "Biometrika.BusinessRule.Dummy.cfg"))
                    {
                        LOG.Warn("BusinessRuleDummy.Initialize - No grabo condig en disco (Biometrika.BusinessRule.Dummy.cfg)");
                    }
                }
                else
                {
                    Config = SerializeHelper.DeserializeFromFile<DynamicData>("Biometrika.BusinessRule.Dummy.cfg");
                    if (Config != null) //Check nulo
                    {
                        Config.Initialize();
                        if (!Config.DynamicDataItems.ContainsKey("ServiceAccessToken") ||
                             string.IsNullOrEmpty((string)Config.DynamicDataItems["ServiceAccessToken"]))
                        {
                            ActualizeAccessToken();
                            Config.DynamicDataItems = null;
                            Config.Initialize();
                        }

                        if (!SerializeHelper.SerializeToFile(Config, "Biometrika.BusinessRule.Dummy.cfg"))
                        {
                            LOG.Warn("BusinessRuleDummy.Initialize - No grabo condig en disco (Biometrika.BusinessRule.Dummy.cfg)");
                        }
                        FillDatabaseDummy();
                    }
                    else
                    {
                        LOG.Fatal("BusinessRuleDummy.Initialize - No parseo la config. Elimine archivo Biometrika.BusinessRule.FCB.cfg y reintente...");
                    }
                }

                if (Config == null)
                {
                    LOG.Fatal("BusinessRuleDummy.Initialize - Error leyendo BusinessRuleDummy!");
                    return Errors.IERR_DESERIALIZING_DATA;
                }
                _Initialized = true;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BusinessRuleDummy.Initialize Excp Error: " + ex.Message);
            }
            return ret;
        }

        private void FillDatabaseDummy()
        {
            try
            {
                _HT_PERSONAS = new Hashtable();
                LOG.Debug("FillDatabaseDummy - Leyendo json en => " + (string)Config.DynamicDataItems["PathDatabase"]);
                string JsonDB = System.IO.File.ReadAllText((string)Config.DynamicDataItems["PathDatabase"]);
                if (string.IsNullOrEmpty(JsonDB))
                {
                    LOG.Fatal("FillDatabaseDummy - JsonDB nulo!");
                } else
                {
                    LOG.Debug("FillDatabaseDummy - Deserializando JsonDB...");
                    List <Models.Persona> list = JsonConvert.DeserializeObject<List<Models.Persona>>(JsonDB);
                    LOG.Debug("FillDatabaseDummy - Cantidad Personas => " + (list==null?"Null":list.Count.ToString()));

                    foreach (Models.Persona item in list)
                    {
                        if (!_HT_PERSONAS.ContainsKey(item.Rut))
                        {
                            _HT_PERSONAS.Add(item.Rut, item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BusinessRuleDummy.FillDatabaseDummy Excp Error: " + ex.Message);
            }
        }

        public int CheckBusinessRule(DynamicData parameters, out bool accessresult, out DynamicData returns, out string msg)
        {
            int ret = 0;
            accessresult = false;
            returns = null;
            msg = null;
            string typeid, valueid;
            bool isExit;
            bool registerAccess;
            /*
                1.- Tomo parametro de entrada y chequeo integridad
                2.- Consumo servicio
                    2.1.- Si es Check y Marca segun config => Chequeo y registro.
                    2.2.- Si es colo check segun config => cehqeo e informo
                3.- Retorno resultado
                              
                    -1 Error interno
                    -2 APB
                    -3 Socio sin acceso (deuda o inactivo)
                    -4 Visita sin acceso (sin evento/invitación encontrado)
                    -5 Visita sin acceso (socio autorizador no tiene invitaciones disponibles)
                    -6 Identidad no existe
            */
            try
            {
                LOG.Debug("BusinessRuleDummy.CheckBusinessRule IN...");
                if (parameters == null || parameters.DynamicDataItems == null ||
                    !parameters.DynamicDataItems.ContainsKey("idRecognized") ||
                     !parameters.DynamicDataItems.ContainsKey("checktype"))
                {
                    ret = Errors.IERR_BAD_PARAMETER;
                    msg = "Error chequeando BusinessRule - Parametros nulos o faltantes...";
                    LOG.Warn("BusinessRuleDummy.CheckBusinessRule - " + msg);
                    return ret;
                }
                else //Estamso ok => Tomo valores
                {
                    typeid = "RUT"; //(string)parameters.DynamicDataItems["typeid"];
                    valueid = (string)parameters.DynamicDataItems["idRecognized"];

                    //2.- Consumo servicio
                    //Detecto sentido de marca
                    string sentido = "SALIDA";
                    if (((string)parameters.DynamicDataItems["idDevice"]).Equals(Config.DynamicDataItems["PortCOMBarcodeIn"]) ||
                        ((string)parameters.DynamicDataItems["idDevice"]).Equals(Config.DynamicDataItems["SerialSensorIn"]))
                    {
                        sentido = "INGRESO";
                    }

                    isExit = sentido.Equals("SALIDA"); // ((int)parameters.DynamicDataItems["checktype"] == 2);
                    LOG.Debug("BusinessRuleDummy.CheckBusinessRule - Chequeando BusinessRule de typeid/valueid => " +
                                typeid + "/" + valueid + " - CheckingType=" +
                                parameters.DynamicDataItems["checktype"].ToString());
                }

                //2.- Consumo servicio {registerAccess}}/{{isExit}}/{{token}}
                LOG.Debug("BusinessRuleDummy.CheckBusinessRule - Setting ServiceType => " +
                          ((int)Config.DynamicDataItems["ServiceType"]).ToString());
                registerAccess = ((int)Config.DynamicDataItems["ServiceType"]) == 1;
                LOG.Debug("BusinessRuleDummy.CheckBusinessRule - Setting Checking Type (IN/OUT) => isExit = " +
                          isExit.ToString());

                Models.Persona _PERSONA = null;
                ret = IdentifyDumy(valueid, out _PERSONA, out msg);


                if (_PERSONA != null && ret == 0)
                {   
                    returns = new DynamicData();
                    accessresult =_PERSONA.Code;
                    msg = (string.IsNullOrEmpty(msg) && !accessresult?"Acceso deshabilitado para " + _PERSONA.Name : msg);
                    
                    LOG.Debug("BusinessRuleDummy.CheckBusinessRule - CanAccess =" + accessresult.ToString() +
                                " - RUT= " + valueid + " - Nombre=" + (string.IsNullOrEmpty(_PERSONA.Name) ? "" : _PERSONA.Name));
                    returns.AddItem("Name", _PERSONA.Name);
                    returns.AddItem("Photografy", _PERSONA.Photografy);
                    returns.AddItem("DateOfBirth", _PERSONA.DateOfBirth);
                    returns.AddItem("Code", _PERSONA.Code?"1":"2");
                   
                    LOG.Debug("BusinessRuleDummy.CheckBusinessRule - Response OK");
                    ret = 0;
                }
                else
                {
                    if (ret == Errors.IERR_IDENTITY_NOT_FOUND)
                    {
                        ret = Errors.IERR_IDENTITY_NOT_FOUND;
                        msg = "Persona con Id=" + valueid + " no Registrada!";
                        LOG.Debug("BusinessRuleFCB.CheckBusinessRule - Response No Registrado! [ret = " + ret + "]");
                    } else
                    {
                        ret = Errors.IERR_UNKNOWN;
                        msg = "Error en proceso de identificacion!";
                        LOG.Debug("BusinessRuleFCB.CheckBusinessRule - Response [ret = " + ret + "]");
                    }
                }

                //if (_Q % 2 == 0)
                //{
                //    accessresult = true;
                //    msg = "Acceso Permitido!";
                //    returns = new DynamicData();
                //    returns.AddItem("ParamReturnKey", _Q);
                //} else
                //{
                //    accessresult = false;
                //    msg = "Acceso NO Permitido [_Q=" + _Q.ToString() + "]";
                //}
                //_Q++;
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "BusinessRuleDummy Excp [" + ex.Message + "]";
                LOG.Error("BusinessRuleDummy.CheckBusinessRule - Excp Error: " + ex.Message);
            }
            LOG.Debug("BusinessRuleDummy.CheckBusinessRule OUT! - ret = " + ret.ToString() +
                      " - accessresult=" + accessresult.ToString());
            return ret;
        }

        private int IdentifyDumy(string valueid, out Models.Persona _PersonaOut, out string msg)
        {
            int ret = 0;
            msg = null;
            _PersonaOut = null;
            try
            {
                if (_HT_PERSONAS == null)
                {
                    msg = "No existe DataBase!";
                    return Errors.IERR_DATABASE;
                }

                if (_HT_PERSONAS.ContainsKey(valueid))
                {
                    _PersonaOut = (Models.Persona)_HT_PERSONAS[valueid];
                } else
                {
                    msg = "No existe el id=" + valueid + " en la base de datos!";
                    return Errors.IERR_IDENTITY_NOT_FOUND;
                }

            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "IdentifyDumy Error [" + ex.Message + "]";
            }
            return ret;
        }




        /// <summary>
        /// Revisa 
        /// </summary>
        private void ActualizeAccessToken()
        {
            //try
            //{
            //    LOG.Debug("BusinessRuleDummy.ActualizeAccessToken IN...");

            //    Consumo servicio
            //    http://jano.biometrika.cl/AutoClub_Test/API/api/Account/Login/
            //    totemvehicular @autoclub/ fdbfeec3c613d7d0fcf6aecaf5c5be9a / TotemVehicular
            //    string sURL = Config.DynamicDataItems["ServiceURL"] + "api/Account/Login/" +
            //                    Config.DynamicDataItems["ServiceUser"] + "/" +
            //                    Config.DynamicDataItems["ServicePassword"] + "/" +
            //                    Config.DynamicDataItems["ServicePointName"];
            //    LOG.Debug("BusinessRuleDummy.CheckBusinessRule - LLama a " + sURL + "...");
            //    var client = new RestClient(sURL);
            //    client.Timeout = (int)Config.DynamicDataItems["ServiceTimeout"];
            //    var request = new RestRequest(Method.GET);

            //    LOG.Debug("BusinessRuleDummy.ActualizeAccessToken - call execute...");
            //    IRestResponse response = client.Execute(request);

            //    if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
            //    {
            //        AccessModelRL oResponse = JsonConvert.DeserializeObject<AccessModelRL>(response.Content);
            //        if (oResponse != null)
            //        {
            //            LOG.Debug("BusinessRuleDummy.ActualizeAccessToken - Parseando resultado...");
            //            if (oResponse.Status.Equals("Success") && !string.IsNullOrEmpty(oResponse.Data))
            //            {
            //                Config.UpdateItem("ServiceAccessToken", oResponse.Data);
            //            }
            //            LOG.Debug("BusinessRuleDummy.ActualizeAccessToken - Response OK");
            //        }
            //        else
            //        {
            //            LOG.Debug("BusinessRuleDummy.ActualizeAccessToken - Error parseando respuesta del servicio...");
            //        }
            //    }
            //    else
            //    {
            //        LOG.Debug("BusinessRuleDummy.ActualizeAccessToken - Response Error");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LOG.Error("BusinessRuleDummy.ActualizeAccessToken Excp Error: " + ex.Message);
            //}
        }

        private Image SetImageFromB64(string base64Photo)
        {
            Image ret = null;
            try
            {
                if (!string.IsNullOrEmpty(base64Photo))
                {
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(base64Photo));
                    ret = Image.FromStream(ms);
                    ms.Close();
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("BusinessRuleDummy.SetImageFromB64 Excp Error: " + ex.Message);
            }
            return ret;
        }
    }
}
