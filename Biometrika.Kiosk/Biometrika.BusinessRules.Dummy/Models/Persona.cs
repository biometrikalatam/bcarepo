﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.BusinessRules.Dummy.Models
{
    internal class Persona
    {
        public int Id { get; set; }
        public string Rut { get; set; }
        public string Name { get; set; }
        public string Photografy { get; set; }
        public string Finger { get; set; }
        public string DateOfBirth { get; set; }
        public bool Code { get; set; } //Puede acceder o no

    }
}
