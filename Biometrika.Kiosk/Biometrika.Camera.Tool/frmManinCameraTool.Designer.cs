﻿namespace Biometrika.Camera.Tool
{
    partial class frmManinCameraTool
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmManinCameraTool));
            this.label2 = new System.Windows.Forms.Label();
            this.txtIndexCamera = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbCamaras = new System.Windows.Forms.ComboBox();
            this.btnTakeFoto = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.pic1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnTakeId = new System.Windows.Forms.Button();
            this.txtCameraId = new System.Windows.Forms.TextBox();
            this.btnCopiar = new System.Windows.Forms.Button();
            this.labFotoCameraId = new System.Windows.Forms.Label();
            this.labFotoName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(689, 342);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Index Camera (IN = 0 / OUT = 1)";
            this.label2.Visible = false;
            // 
            // txtIndexCamera
            // 
            this.txtIndexCamera.Location = new System.Drawing.Point(689, 361);
            this.txtIndexCamera.Name = "txtIndexCamera";
            this.txtIndexCamera.Size = new System.Drawing.Size(100, 20);
            this.txtIndexCamera.TabIndex = 25;
            this.txtIndexCamera.Text = "0";
            this.txtIndexCamera.Visible = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(7, 485);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(1255, 166);
            this.richTextBox1.TabIndex = 23;
            this.richTextBox1.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Camaras";
            // 
            // cbCamaras
            // 
            this.cbCamaras.FormattingEnabled = true;
            this.cbCamaras.Location = new System.Drawing.Point(71, 36);
            this.cbCamaras.Name = "cbCamaras";
            this.cbCamaras.Size = new System.Drawing.Size(602, 21);
            this.cbCamaras.TabIndex = 21;
            // 
            // btnTakeFoto
            // 
            this.btnTakeFoto.Location = new System.Drawing.Point(679, 34);
            this.btnTakeFoto.Name = "btnTakeFoto";
            this.btnTakeFoto.Size = new System.Drawing.Size(75, 23);
            this.btnTakeFoto.TabIndex = 20;
            this.btnTakeFoto.Text = "Take Foto";
            this.btnTakeFoto.UseVisualStyleBackColor = true;
            this.btnTakeFoto.Click += new System.EventHandler(this.btnTakeFoto_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(610, 299);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 19;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Visible = false;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(610, 270);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 18;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Visible = false;
            // 
            // pic1
            // 
            this.pic1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic1.Location = new System.Drawing.Point(953, 92);
            this.pic1.Name = "pic1";
            this.pic1.Size = new System.Drawing.Size(309, 312);
            this.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic1.TabIndex = 16;
            this.pic1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(7, 65);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(518, 321);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(795, 358);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 27;
            this.button1.Text = "Take Foto";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(950, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "FOTO Tomada por...";
            // 
            // btnTakeId
            // 
            this.btnTakeId.Location = new System.Drawing.Point(461, 392);
            this.btnTakeId.Name = "btnTakeId";
            this.btnTakeId.Size = new System.Drawing.Size(75, 23);
            this.btnTakeId.TabIndex = 29;
            this.btnTakeId.Text = "Take Id";
            this.btnTakeId.UseVisualStyleBackColor = true;
            this.btnTakeId.Click += new System.EventHandler(this.btnTakeId_Click);
            // 
            // txtCameraId
            // 
            this.txtCameraId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCameraId.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCameraId.Location = new System.Drawing.Point(461, 421);
            this.txtCameraId.Name = "txtCameraId";
            this.txtCameraId.ReadOnly = true;
            this.txtCameraId.Size = new System.Drawing.Size(460, 29);
            this.txtCameraId.TabIndex = 30;
            this.txtCameraId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnCopiar
            // 
            this.btnCopiar.Location = new System.Drawing.Point(738, 456);
            this.btnCopiar.Name = "btnCopiar";
            this.btnCopiar.Size = new System.Drawing.Size(183, 23);
            this.btnCopiar.TabIndex = 31;
            this.btnCopiar.Text = "Copiar Portapapeles";
            this.btnCopiar.UseVisualStyleBackColor = true;
            this.btnCopiar.Click += new System.EventHandler(this.btnCopiar_Click);
            // 
            // labFotoCameraId
            // 
            this.labFotoCameraId.AutoSize = true;
            this.labFotoCameraId.Location = new System.Drawing.Point(950, 63);
            this.labFotoCameraId.Name = "labFotoCameraId";
            this.labFotoCameraId.Size = new System.Drawing.Size(58, 13);
            this.labFotoCameraId.TabIndex = 32;
            this.labFotoCameraId.Text = "Camara Id:";
            // 
            // labFotoName
            // 
            this.labFotoName.AutoSize = true;
            this.labFotoName.Location = new System.Drawing.Point(950, 36);
            this.labFotoName.Name = "labFotoName";
            this.labFotoName.Size = new System.Drawing.Size(46, 13);
            this.labFotoName.TabIndex = 33;
            this.labFotoName.Text = "Camara:";
            // 
            // frmManinCameraTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1279, 686);
            this.Controls.Add(this.labFotoName);
            this.Controls.Add(this.labFotoCameraId);
            this.Controls.Add(this.btnCopiar);
            this.Controls.Add(this.txtCameraId);
            this.Controls.Add(this.btnTakeId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtIndexCamera);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbCamaras);
            this.Controls.Add(this.btnTakeFoto);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.pic1);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmManinCameraTool";
            this.Text = "Biometrika Camera Tool";
            this.Load += new System.EventHandler(this.frmManinCameraTool_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIndexCamera;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbCamaras;
        private System.Windows.Forms.Button btnTakeFoto;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.PictureBox pic1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnTakeId;
        private System.Windows.Forms.TextBox txtCameraId;
        private System.Windows.Forms.Button btnCopiar;
        private System.Windows.Forms.Label labFotoCameraId;
        private System.Windows.Forms.Label labFotoName;
    }
}

