﻿using AForge.Video.DirectShow;
using Emgu.CV;
using Emgu.CV.Flann;
using Emgu.CV.Structure;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Biometrika.Camera.Tool
{
    public partial class frmManinCameraTool : Form
    {
        //private VideoCapture _capture = null;
        private bool _captureInProgress;
        private Mat _frame;
        private Mat _grayFrame;
        private Mat _smallGrayFrame;
        private Mat _smoothedGrayFrame;
        private Mat _cannyFrame;

        FilterInfoCollection filter;
        VideoCaptureDevice device;

        int CameraIndexIN = 0;
        int CameraIndexOUT = 1;

        Hashtable htCameras = new Hashtable();
        public frmManinCameraTool()
        {
            InitializeComponent();
        }

        private void frmManinCameraTool_Load(object sender, EventArgs e)
        {
            FillCameras();
        }

        private void FillCameras()
        {
            BKCamera item;
            try
            {
                filter = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                int index = 0;
                int selectedIndex = 0;
                foreach (FilterInfo device in filter)
                {
                    
                    var deviceN = new VideoCaptureDevice(filter[index].MonikerString);
                    cbCamaras.Items.Add(device.Name + " [" + deviceN.Source.Substring(deviceN.Source.IndexOf("usb#"),
                                                          deviceN.Source.LastIndexOf("#") - deviceN.Source.IndexOf("usb#")) + "]");
                    item = new BKCamera(index, device.Name, deviceN.Source,
                                        deviceN.Source.Substring(deviceN.Source.IndexOf("usb#"),
                                                          deviceN.Source.LastIndexOf("#") - deviceN.Source.IndexOf("usb#")));
                    htCameras.Add(index.ToString(), item);
                    richTextBox1.Text += index + " -> " + item.source + Environment.NewLine;
                    richTextBox1.Text += index + " -> " + item.source.Substring(item.source.IndexOf("usb#"),
                                                                                   item.source.LastIndexOf("#") - deviceN.Source.IndexOf("usb#"))
                                                        + Environment.NewLine;
                    index++;
                }
                cbCamaras.SelectedIndex = selectedIndex;
            }
            catch (Exception ex)
            {
                richTextBox1.Text = "Excepcion Form_Load [" + ex.Message + "]";
            }
        }

        private void btnTakeId_Click(object sender, EventArgs e)
        {
            if (htCameras != null && htCameras.ContainsKey(cbCamaras.SelectedIndex.ToString()) )
            {
                richTextBox1.Text = "Camara Seleccionada: ";
                BKCamera item = (BKCamera)htCameras[cbCamaras.SelectedIndex.ToString()];
                richTextBox1.Text += Environment.NewLine + "Id = " + item.id;
                richTextBox1.Text += Environment.NewLine + "Name = " + item.name;
                richTextBox1.Text += Environment.NewLine + "Source = " + item.source;
                richTextBox1.Text += Environment.NewLine + "CameraId = " + item.cameraid.Replace("&", "-");
                txtCameraId.Text = item.cameraid.Replace("&", "-");
            }
        }

        private void btnCopiar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCameraId.Text))
            {
                Clipboard.SetDataObject(txtCameraId.Text);
            }
            else
            {
                MessageBox.Show("Debe Tomar un CameraId primero!");
            } 
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnTakeFoto_Click(object sender, EventArgs e)
        {
            int cameraid = 0;
            Bitmap imageTook = TakeFoto(cbCamaras.SelectedIndex);
            if (imageTook != null)
            {
                pic1.Image = imageTook;
                BKCamera item = (BKCamera)htCameras[cbCamaras.SelectedIndex.ToString()];
                labFotoName.Text = "Camera: " + item.id.ToString() + " - " + item.name;
                labFotoCameraId.Text = "Camera Id: " + item.cameraid;
                txtCameraId.Text = item.cameraid.Replace("&", "-");
            }
            else
            {
                MessageBox.Show("No pudo obtener imagen!");
            }

        }

        Mat mat;
        Image<Bgr, Byte> img;
        private Capture _CaptureCam;
        Bitmap _CurrentImage;
        private Bitmap TakeFoto(int cameraid)
        {
            //cameraid = 0;
            try
            {
                _CurrentImage = null;
                if (_CaptureCam == null)
                    _CaptureCam = new Capture(cameraid);
                cameraid = cbCamaras.SelectedIndex;
                mat = _CaptureCam.QueryFrame();
                mat = _CaptureCam.QueryFrame(); //Leo de nuevo porque sino se queda con la imagen anterior. NO SE PORQUE AUN!!
                img = mat.ToImage<Bgr, Byte>();
                _CurrentImage = img.ToBitmap(); // (Image)picVideo.Image.Clone();
                //_Image_TakeFoto = GetB64FromPictureBox(_CurrentImage);
                mat.Dispose();
                img.Dispose();
                mat = null;
                img = null;
                _CaptureCam.Stop();
                _CaptureCam = null;

                //_CaptureCam.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error capturando foto [" + ex.Message + "]");
                MessageBox.Show(ex.InnerException.Message);
                MessageBox.Show(ex.StackTrace);
            }
            return _CurrentImage;
        }
    }

    internal class BKCamera
    {
        public BKCamera(int _id, string _name, string _source, string _cameraid) { 
            id = _id;
            name = _name;
            source = _source;
            cameraid = _cameraid;
        }
        public int id { get; set; }
        public string name { get; set; }
        public string source { get; set; }
        public string cameraid { get; set; }

    }
}
