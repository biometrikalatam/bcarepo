﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Camera.Tool
{
    internal static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string s = "usb#vid_045e-pid_0812-mi_00#7-2b6e0a05-0-0000";
            bool b = s.Contains("usb#vid_045e-pid_0812-mi_00#7-2b6e0a05-0-0000");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmManinCameraTool());
        }
    }
}
