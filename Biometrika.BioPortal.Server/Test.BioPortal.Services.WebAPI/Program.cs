﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.BioPortal.Services.WebAPI;

namespace Test.BioPortal.Services.WebAPI
{
    public class Program
    {
        static void Main(string[] args)
        {

            Test.BioPortal.Services.WebAPI.WSTest ws = new WSTest();
            ws.Setup();
            ws.WSVerify();
        }

    }
}
