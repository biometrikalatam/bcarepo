﻿using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.BioPortal.Services.WebAPI
{
    public class BaseTest
    {
        protected IRestClient Client;
        string host = "http://localhost:58512/";

        [SetUp]
        public void Setup()
        {
            Client = new RestClient(host);
        }
    }
}
