﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using log4net;
using Regula.DocumentReader.WebClient.Api;
using Regula.DocumentReader.WebClient.Model;
using Regula.DocumentReader.WebClient.Model.Ext;
using Regula.DocumentReader.WebClient.Model.Ext.Autheticity;

namespace Biometrika.Regula.WebAPI.Api._2021
{
    public class ApiFunctions
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ApiFunctions));

        //internal static string _URLAPI = "http://localhost:8089/WebApi/";
        //internal static string _USER = "testuser";
        //internal static string _PASSWORD = "Regul@SdkTest";

        internal string _API_BASE_PATH = "https://api.regulaforensics.com/"; //"http://localhost:8089";
        internal string _TEST_LICENSE = "LICENSE"; //"TEST_LICENSE";
        internal string _LICENSE_FILE_NAME = "regula.license"; //@"D:\Biometrika\BioPartners\Regula\SW\2021\OL79677\regula.license";
        internal int PROCESS_TYPE = 0; //Para resolver bug v6.2 de Regula, se procesa ambos lados de cedula por separado para
                                       //resolver la extracción de signature

        private DocumentReaderApi _API_DOCUMENTREADER = null;

        internal bool _IsInitialized = false;
        internal byte[] _LicenseFromFile;

        public int GetImageDataToFixv6 { get; private set; }

        public ApiFunctions() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlbaseservice"></param>
        /// <param name="pathlicense"></param>
        /// <param name="processType">0|1 => 
        ///                         0-Normal  
        ///                         1-Para resolver bug v6.2 de Regula, se procesa ambos lados de cedula por separado para
        ///                           resolver la extracción de signature
        /// </param>
        /// <returns></returns>
        public bool Initialize(string urlbaseservice, string pathlicense, int processType = 0)
        {
            try
            {
                _IsInitialized = false;
                LOG.Debug("ApiFunctions.Initialize IN...");
                if (!string.IsNullOrEmpty(pathlicense))
                {
                    _LICENSE_FILE_NAME = pathlicense;
                }
                if (!string.IsNullOrEmpty(urlbaseservice))
                {
                    _API_BASE_PATH = urlbaseservice;
                }
                LOG.Debug("ApiFunctions.Initialize - URL Base Service = " + _API_BASE_PATH);
                LOG.Debug("ApiFunctions.Initialize - Load License File from = " + _LICENSE_FILE_NAME);
                _LicenseFromFile = File.Exists(_LICENSE_FILE_NAME)
                    ? File.ReadAllBytes(_LICENSE_FILE_NAME)
                    : null;
                if (_LicenseFromFile != null)
                {
                    LOG.Debug("ApiFunctions.Initialize - Load License File OK! [Size=" + _LicenseFromFile.Length.ToString() + "] " + 
                              "=> Inicializa DocumentReaderApi...");
                    _API_DOCUMENTREADER = new DocumentReaderApi(urlbaseservice).WithLicense(_LicenseFromFile);
                    
                    _IsInitialized = (_API_DOCUMENTREADER != null);
                    LOG.Debug("ApiFunctions.Initialize - Status DocumentReaderApi Initialize => " + _IsInitialized.ToString());
                    if (_IsInitialized)
                    {
                        if (urlbaseservice.StartsWith("https"))
                        {
                            LOG.Debug("ApiFunctions.Initialize - Habilita SSL...");
                            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        }
                        LOG.Debug("ApiFunctions.Initialize - Realiza Ping...");
                        DeviceInfo info = _API_DOCUMENTREADER.Ping();
                        if (info != null)
                        {
                            LOG.Info("ApiFunctions.Initialize -------- Info Service OCR ---------------");
                            LOG.Info("ApiFunctions.Initialize >> Id=" + info.LicenseId);
                            LOG.Info("ApiFunctions.Initialize >> Serial=" + info.LicenseSerial);
                            LOG.Info("ApiFunctions.Initialize >> Valida Hasta=" + info.ValidUntil.ToString("dd/MM/yyyy"));
                            LOG.Info("ApiFunctions.Initialize >> Server Time=" + info.ServerTime.ToString("dd/MM/yyyy"));
                            LOG.Info("ApiFunctions.Initialize >> Version=" + info.Version);
                            LOG.Info("ApiFunctions.Initialize -------- Info Service OCR ---------------");

                            PROCESS_TYPE = processType;
                            LOG.Info("ApiFunctions.Initialize - Process_Type = " + PROCESS_TYPE.ToString());
                        } else
                        {
                            LOG.Warn("ApiFunctions.Initialize - Retorno Info Ping = NULL..."); 
                        }
                    }
                } else
                {
                    LOG.Warn("ApiFunctions.Initialize - NOT Load License File! => No Inicializado el servicio!");
                    _IsInitialized = false;
                }
                    
            }
            catch (Exception ex)
            {
                _IsInitialized = false;
                _LicenseFromFile = null;
                LOG.Error("ApiFunctions.Initialize Excp Error: " + ex.Message);
                LOG.Error("StackTrace => " + ex.StackTrace);
                LOG.Error("InnerException => " + ex.InnerException);
            }
            LOG.Debug("ApiFunctions.Initialize - OUT! _IsInitialized = " + _IsInitialized.ToString());
            return _IsInitialized;
        }

        public int Process(List<Picture> pictureList, out Document document) //, int capabilities, int authenticity)
        {
            int ret = 0;
            document = null;
            try
            {
                LOG.Debug("ApiFunctions.Process IN...");

                if (!_IsInitialized)
                {
                    ret = -2;
                    LOG.Error("ApiFunctions.Process - API no inicializada!");
                    return ret;
                }

                if (pictureList == null || pictureList.Count == 0)
                {
                    ret = -6;
                    LOG.Error("ApiFunctions.Process - Parametros erroneos");
                    return ret;
                }

                var requestParams = new RecognitionParams()
                    .WithScenario(Scenario.FULL_PROCESS)
                    .WithResultTypeOutput(new List<int>
                    {
                        // actual results
                        Result.STATUS,  Result.TEXT, Result.IMAGES,
                        Result.DOCUMENT_TYPE, Result.DOCUMENT_TYPE_CANDIDATES,
                        // legacy results
                        Result.MRZ_TEXT, Result.VISUAL_TEXT, Result.BARCODE_TEXT, 
                        Result.VISUAL_GRAPHICS, Result.BARCODE_GRAPHICS 
                        

                        //// actual results
                        //Result.STATUS, Result.TEXT, Result.IMAGES, Result.AUTHENTICITY,
                        //Result.DOCUMENT_TYPE, Result.RFID_TEXT, Result.RFID_GRAPHICS,
                        //// legacy results
                        //Result.MRZ_TEXT, Result.VISUAL_TEXT,
                        //Result.VISUAL_GRAPHICS,
                        //Result.LEXICAL_ANALYSIS, Result.IMAGE_QUALITY
                    });
                requestParams.DoublePageSpread = true;

                List<ProcessRequestImage> _listProcessRequestImage = new List<ProcessRequestImage>();
                _listProcessRequestImage.Add(new ProcessRequestImage(
                                                    new ImageData(Convert.FromBase64String(pictureList[0].Base64ImageString)),
                                                    Light.WHITE, 0));

                if (pictureList.Count > 1)
                {
                    _listProcessRequestImage.Add(new ProcessRequestImage(
                                                    new ImageData(Convert.FromBase64String(pictureList[1].Base64ImageString)),
                                                    Light.WHITE, 1));
                    requestParams.DoublePageSpread = true;
                }

                var request = new RecognitionRequest(requestParams, _listProcessRequestImage);
                
                var response = _API_DOCUMENTREADER.Process(request);

                //Si respuesta no es nula pero ademas hay data reconocida, sino sale sin reconocer
                if (response != null && response.Text()!=null)
                {
                    document = new Document();

                    int isGetOk = GetTextData(response, document, out document);

                    if (isGetOk == 0)
                    {
                        isGetOk = GetImageData(response, document, out document);
                        if (PROCESS_TYPE == 1 && pictureList.Count > 1) //Solo si son dos lados sino no lo hace
                        {
                            //Proceso el frente solo para obtener las imagenes que en el bug de v6.2 de Regula no devuelve
                            // Signature + Gost Photo
                            isGetOk = GetImageDataToFixv6_2(pictureList[0], document, out document);
                        }
                    } else
                    {
                        LOG.Warn("ApiFunctions.Process - Error recuperando datos desde API [err=" + isGetOk + "]");
                    }
                } else
                {
                    ret = -5; //No reoconocio data pero rentrego alguna respuesta
                    LOG.Warn("ApiFunctions.Process - Sale con Err = -5 => Proceso, no dio error pero no reconocio data!");
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ApiFunctions.Process Excp Error: " + ex.Message);
            }
            return ret;
        }

        private int GetImageDataToFixv6_2(Picture front, Document documentIn, out Document documentOut)
        {
            int ret = 0;
            documentOut = null;
            try
            {
                LOG.Debug("ApiFunctions.GetImageDataToFixv6_2 IN...");

                if (!_IsInitialized)
                {
                    ret = -2;
                    LOG.Error("ApiFunctions.GetImageDataToFixv6_2 - API no inicializada!");
                    return ret;
                }

                if (front == null)
                {
                    ret = -6;
                    LOG.Error("ApiFunctions.GetImageDataToFixv6_2 - Parametros erroneos");
                    return ret;
                }

                var requestParams = new RecognitionParams()
                    .WithScenario(Scenario.FULL_PROCESS)
                    .WithResultTypeOutput(new List<int>
                    {
                        // actual results
                        Result.STATUS,
                        Result.IMAGES,
                        Result.VISUAL_GRAPHICS 
                        //Result.TEXT, 
                        //Result.DOCUMENT_TYPE, Result.DOCUMENT_TYPE_CANDIDATES,
                        // legacy results
                        //Result.MRZ_TEXT,
                        //Result.VISUAL_TEXT, Result.BARCODE_TEXT,
                        //Result.BARCODE_GRAPHICS 
                        //// actual results
                        //Result.STATUS, Result.TEXT, Result.IMAGES, Result.AUTHENTICITY,
                        //Result.DOCUMENT_TYPE, Result.RFID_TEXT, Result.RFID_GRAPHICS,
                        //// legacy results
                        //Result.MRZ_TEXT, Result.VISUAL_TEXT,
                        //Result.VISUAL_GRAPHICS,
                        //Result.LEXICAL_ANALYSIS, Result.IMAGE_QUALITY
                    });
                requestParams.DoublePageSpread = true;

                List<ProcessRequestImage> _listProcessRequestImage = new List<ProcessRequestImage>();
                _listProcessRequestImage.Add(new ProcessRequestImage(
                                                    new ImageData(Convert.FromBase64String(front.Base64ImageString)),
                                                    Light.WHITE, 0));

                var request = new RecognitionRequest(requestParams, _listProcessRequestImage);

                var response = _API_DOCUMENTREADER.Process(request);

                if (response != null)
                {
                    LOG.Debug("ApiFunctions.GetImageDataToFixv6_2 - Processing images => Q In = " + 
                                (documentIn._DocImages != null ? documentIn._DocImages.Count.ToString() : "Null") + "...");
                    int isGetOk = GetImageDataFix(response, documentIn, out documentOut);
                                       
                    if (isGetOk != 0)
                    {
                        LOG.Warn("ApiFunctions.GetImageDataToFixv6_2 - Error recuperando datos desde API [err=" + isGetOk + "]");
                    } else
                    {
                        LOG.Debug("ApiFunctions.GetImageDataToFixv6_2 - Processed images! Q out = " +
                                    (documentOut._DocImages != null ? documentOut._DocImages.Count.ToString() : "Null"));
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ApiFunctions.GetImageDataToFixv6_2 Excp Error: " + ex.Message);
            }
            LOG.Debug("ApiFunctions.GetImageDataToFixv6_2 OUT! - ret = " + ret.ToString());
            return ret;
        }


        private int GetImageData(RecognitionResponse response, Document documentIn, out Document documentOut)
        {
            int ret = 0;
            documentOut = null;
            try
            {

                if (documentIn == null)
                {
                    documentOut = new Document();
                    documentOut._DocInfo = new DOCInfoData();
                    documentOut._DocImages = new List<DOCInfoImage>();
                }
                else
                {
                    documentOut = documentIn;
                    if (documentOut._DocImages == null)
                    {
                        documentOut._DocImages = new List<DOCInfoImage>();
                    }
                }

                Images _listImages = response.Images();
                DOCInfoImage newImage;
                foreach (ImagesField item in _listImages.FieldList)
                {


                    //var documentImage = response.Images().GetField(GraphicFieldType.DOCUMENT_FRONT).GetValue();
                    ////var documentImage1 = response.Images().GetField(GraphicFieldType.).GetValue();
                    //var documentImage2 = response.Images().GetField(GraphicFieldType.SIGNATURE).GetValue();

                    newImage = new DOCInfoImage();
                    if (item.FieldType == 207) //Es DOCUMENT_FRONT => Veo si vienen dos values, la PageIndex = 0 es frente y la 1 es Back
                    {
                        if (item.ValueList.Count > 1) {
                            foreach (ImagesFieldValue itemInt in item.ValueList)
                            {
                                //Es front porque es la primera hoja presentada y el retorno es en pos 1
                                //Pero en el bug de la 6.2, cambiaba el orden y ponia el back al comienzo. Asi que pregunto 
                                //si esta habilitado el fix sino uso los valores como antes
                                if ((PROCESS_TYPE == 1 && itemInt.PageIndex == 1) ||
                                    (PROCESS_TYPE == 0 && itemInt.PageIndex == 0)) 
                                {
                                    newImage = new DOCInfoImage();
                                    newImage.code = "199"; // Item.GetElementsByTagName("FieldType").Item(0).InnerText;
                                    newImage.name = "Document Front"; //Item.GetElementsByTagName("FieldName").Item(0).InnerText;
                                    newImage.b64image = Convert.ToBase64String(itemInt.Value); // Item.GetElementsByTagName("File_Image").Item(0).InnerText;
                                } else //Es back
                                {
                                    newImage = new DOCInfoImage();
                                    newImage.code = "200"; // Item.GetElementsByTagName("FieldType").Item(0).InnerText;
                                    newImage.name = "Document Back"; //Item.GetElementsByTagName("FieldName").Item(0).InnerText;
                                    newImage.b64image = Convert.ToBase64String(itemInt.Value); // Item.GetElementsByTagName("File_Image").Item(0).InnerText;
                                }
                                documentOut._DocImages.Add(newImage);
                            }
                        }
                        else
                        {
                            newImage.code = item.FieldType.ToString(); // Item.GetElementsByTagName("FieldType").Item(0).InnerText;
                            newImage.name = item.FieldName; //Item.GetElementsByTagName("FieldName").Item(0).InnerText;
                            newImage.b64image = Convert.ToBase64String(item.ValueList[0].Value); // Item.GetElementsByTagName("File_Image").Item(0).InnerText;
                            documentOut._DocImages.Add(newImage);
                        }
                    } else
                    {
                        newImage.code = item.FieldType.ToString(); // Item.GetElementsByTagName("FieldType").Item(0).InnerText;
                        newImage.name = item.FieldName; //Item.GetElementsByTagName("FieldName").Item(0).InnerText;
                        newImage.b64image = Convert.ToBase64String(item.ValueList[0].Value); // Item.GetElementsByTagName("File_Image").Item(0).InnerText;
                        documentOut._DocImages.Add(newImage);
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ApiFunctions.GetImageData Excp Error: " + ex.Message);
            }
            return ret;
        }

        
        private int GetImageDataFix(RecognitionResponse response, Document documentIn, out Document documentOut)
        {
            int ret = 0;
            documentOut = documentIn;
            LOG.Debug("ApiFunctions.GetImageDataFix IN - Q Images IN => " + 
                        (documentOut != null && documentOut._DocImages != null ? 
                            documentOut._DocImages.Count.ToString() : "Null"));
            try
            {
                if (documentIn == null)
                {
                    return -2;
                }
                Images _listImages = response.Images();
                DOCInfoImage newImage;
                if (documentOut != null && documentOut._DocImages == null) 
                    documentOut._DocImages = new List<DOCInfoImage>();
                foreach (ImagesField item in _listImages.FieldList)
                {
                    newImage = new DOCInfoImage();
                    if ((item.FieldType == 204 || item.FieldType == 210) && !ExistInList(item.FieldType, documentOut._DocImages)) //Es Signature
                    {
                        LOG.Debug("ApiFunctions.GetImageDataFix - Agregando => " + item.FieldName + "...");
                        newImage.code = item.FieldType.ToString(); 
                        newImage.name = item.FieldName; 
                        newImage.b64image = Convert.ToBase64String(item.ValueList[0].Value); 
                        documentOut._DocImages.Add(newImage);
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ApiFunctions.GetImageDataFix Excp Error: " + ex.Message);
            }
            LOG.Debug("ApiFunctions.GetImageDataFix OUT - Q Images IN => " +
                        (documentOut != null && documentOut._DocImages != null ?
                            documentOut._DocImages.Count.ToString() : "Null"));
            LOG.Debug("ApiFunctions.GetImageDataFix OUT! - ret = " + ret.ToString());
            return ret;
        }

        private bool ExistInList(int fieldType, List<DOCInfoImage> docImages)
        {
            bool ret = false;
            try
            {
                LOG.Debug("ApiFunctions.GetImageDataFix IN...");
                if (docImages != null) {
                    foreach (var image in docImages)
                    {
                        if (Convert.ToInt32(image.code) == fieldType)
                        {
                            ret = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("ApiFunctions.ExistInList Excp Error: " + ex.Message);
                ret = false;
            }
            LOG.Debug("ApiFunctions.GetImageDataFix OUT! ret = " + ret.ToString());
            return ret;
        }

        private int GetTextData(RecognitionResponse response, Document documentIn, out Document documentOut)
        {
            int ret = 0;
            documentOut = documentIn;
            try
            {
                if (documentIn == null)
                {
                    documentOut = new Document();
                    documentOut._DocInfo = new DOCInfoData();
                }
                else
                {
                    documentOut = documentIn;
                    if (documentOut._DocInfo == null)
                    {
                        documentOut._DocInfo = new DOCInfoData();
                    }
                }

                var _field = response.Text().GetField(TextFieldType.DOCUMENT_CLASS_CODE);
                if (_field != null)
                    documentOut._DocInfo.ft_Document_Class_Code = (_field.GetValue(Source.MRZ) != null) ?
                                                               _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);


                _field = response.Text().GetField(TextFieldType.ISSUING_STATE_CODE);
                if (_field != null)
                    documentOut._DocInfo.ft_Issuing_State_Code = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                               _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.DOCUMENT_NUMBER);
                if (_field != null)
                    documentOut._DocInfo.ft_Document_Number = (_field != null && _field != null && _field.GetValue(Source.MRZ) != null) ?
                                                               _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.DATE_OF_EXPIRY);
                if (_field != null)
                    documentOut._DocInfo.ft_Date_of_Expiry = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                               _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.DATE_OF_ISSUE);
                if (_field != null)
                    documentOut._DocInfo.ft_Date_of_Issue = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                               _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.DATE_OF_BIRTH);
                if (_field != null)
                    documentOut._DocInfo.ft_Date_of_Birth = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                               _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.PLACE_OF_BIRTH);
                if (_field != null)
                    documentOut._DocInfo.ft_Place_of_Birth = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                               _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.PERSONAL_NUMBER);
                if (_field != null)
                    documentOut._DocInfo.ft_Personal_Number = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                               _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);
                //Added xq a veces en el serial es el personalnumber como en cedula argentina
                if (string.IsNullOrEmpty(documentOut._DocInfo.ft_Personal_Number) && 
                    !string.IsNullOrEmpty(documentOut._DocInfo.ft_Document_Number))
                {
                    documentOut._DocInfo.ft_Personal_Number = documentOut._DocInfo.ft_Document_Number;
                }

                //Chequeo que rut reconocido en cada lado es el mismo sino sale 
                if (_field != null &&  !EqualNumber(_field.GetValue(Source.MRZ), _field.GetValue(Source.VISUAL)))
                {
                    documentOut = null;
                    return -9000;
                }
                //Chequeo que rut sea valido formado si es chile => Sino nada para tomar datos de pasaportes y otras cedulas
                if ((!string.IsNullOrEmpty(documentOut._DocInfo.ft_Document_Class_Code) &&
                      (documentOut._DocInfo.ft_Document_Class_Code.Equals("IE") ||
                       documentOut._DocInfo.ft_Document_Class_Code.Equals("ID"))) &&
                    ((!string.IsNullOrEmpty(documentOut._DocInfo.ft_Issuing_State_Code) &&
                      documentOut._DocInfo.ft_Issuing_State_Code.Equals("CHL"))
                   ))
                {
                    RUN run = RUN.ConvertirEx(documentOut._DocInfo.ft_Personal_Number);
                    if (run == null)
                    {
                        documentOut = null;  //Si no reconoce ok => Devuelve nulo
                        return -9001;
                    }
                    else documentOut._DocInfo.ft_Personal_Number = run.ToString(true);
                }

                _field = response.Text().GetField(TextFieldType.SURNAME);
                if (_field != null)
                    documentOut._DocInfo.ft_Surname = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                   _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.GIVEN_NAME);
                if (_field != null)
                    documentOut._DocInfo.ft_Given_Names = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                   _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.NATIONALITY);
                if (_field != null)
                    documentOut._DocInfo.ft_Nationality = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                   _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.SEX);
                if (_field != null)
                    documentOut._DocInfo.ft_Sex = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                   _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.SURNAME_AND_GIVEN_NAMES);
                if (_field != null)
                    documentOut._DocInfo.ft_Surname_And_Given_Names = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                   _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.NATIONALITY_CODE);
                if (_field != null)
                    documentOut._DocInfo.ft_Nationality_Code = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                   _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.ISSUING_STATE_NAME);
                if (_field != null)
                    documentOut._DocInfo.ft_Issuing_State_Name = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                   _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.PLACE_OF_ISSUE);
                if (_field != null)
                    documentOut._DocInfo.ft_Place_of_Issue = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                   _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.VISA_TYPE);
                if (_field != null)
                    documentOut._DocInfo.ft_Visa = (_field!=null && _field.GetValue(Source.MRZ) != null) ?
                                                   _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.AGE);
                if (_field != null)
                    documentOut._DocInfo.ft_Age = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                   _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);

                _field = response.Text().GetField(TextFieldType.PROFESSION);
                if (_field != null)
                    documentOut._DocInfo.ft_Profesion = (_field != null && _field.GetValue(Source.MRZ) != null) ?
                                                   _field.GetValue(Source.MRZ) : _field.GetValue(Source.VISUAL);
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("ApiFunctions.GetTextData Excp Error: " + ex.Message);
            }
            return ret;
        }

        private bool EqualNumber(string runMRZ, string runVisual)
        {
            try
            {
                if (string.IsNullOrEmpty(runMRZ) || string.IsNullOrEmpty(runVisual)) return true;

                ///Comento 07-05-2021 xq si no es chileno se cae por formateo de rut. El chequeo de rut
                ///Se hace luego consultando si es o no de chile primero
                //if (!string.IsNullOrEmpty(runMRZ) && !string.IsNullOrEmpty(runVisual))
                //{
                //    RUN run1 = RUN.ConvertirEx(runMRZ);
                //    RUN run2 = RUN.ConvertirEx(runVisual);
                //    return (run1.ToString(true).Equals(run2.ToString(true)));
                //}
            }
            catch (Exception ex)
            {
                LOG.Error("RegulaHandler.NotEqualNumber Error: " + ex.Message);
                return false;
            }
            return true;
        }
    }
}
