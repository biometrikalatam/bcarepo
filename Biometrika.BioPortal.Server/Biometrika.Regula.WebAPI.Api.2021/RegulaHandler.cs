﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using log4net;
using Newtonsoft.Json;
using Regula.DocumentReader.WebClient.Api;

namespace Biometrika.Regula.WebAPI.Api._2021
{
    public class RegulaHandler
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(RegulaHandler));

        private static string _API_BASE_PATH = "https://api.regulaforensics.com/"; //"http://localhost:8089";
        private static string _TEST_LICENSE = "LICENSE"; //"TEST_LICENSE";
        private static string _LICENSE_FILE_NAME = "regula.license"; //@"D:\Biometrika\BioPartners\Regula\SW\2021\OL79677\regula.license";
        private static ApiFunctions _API_DOCUMENTREADER = null;
        internal static bool _IsInitialized = false;
        internal static byte[] _LicenseFromFile;

        internal static int _TYPE = 0;

        //private static string _Token;
        //public static bool _IsLoggedIn;

        //internal string _URLAPI = "http://localhost:8089/WebApi/";
        //internal string _USER = "testuser";
        //internal string _PASSWORD = "Regul@SdkTest";


        public bool IsInitialized()
        {
            return _IsInitialized;
        }

        /// <summary>
        /// Inicializa licencia y crea objeto de API.
        /// </summary>
        /// <returns></returns>
        public int Init()
        {
            try
            {
                if (!_IsInitialized)
                {
                    _API_DOCUMENTREADER = new ApiFunctions();
                    _IsInitialized = _API_DOCUMENTREADER.Initialize(_API_BASE_PATH, _LICENSE_FILE_NAME, _TYPE);

                }
            }
            catch (Exception ex)
            {
                _IsInitialized = false;
                LOG.Error("RegulaHandler.Init() Error: " + ex.Message);
            }
            return 0;
        }

        /// <summary>
        /// Setes parametros de conexión y llama a Init()
        /// </summary>
        /// <param name="url"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public int Init(string url, string pathlicense, int type = 0)
        {
            try
            {
                LOG.Debug("RegulaHandler.Init - IN...");
                _API_BASE_PATH = url;
                _LICENSE_FILE_NAME = pathlicense;
                _TYPE = type;
                LOG.Debug("RegulaHandler.Init - _API_BASE_PATH = " + url + " - _LICENSE_FILE_NAME = " + _LICENSE_FILE_NAME + "..."); 
            }
            catch (Exception ex)
            {
                LOG.Error("RegulaHandler.Init(string url, string user, string password) Error: " + ex.Message);
                return -1;
            }
            LOG.Debug("RegulaHandler.Init OUT!");
            return Init();
        }

        
        /// <summary>
        /// Dado el front y back del documento, intento reconocer lo mejor posible para llenar los datos en Document
        /// </summary>
        /// <param name="front"></param>
        /// <param name="back"></param>
        /// <param name="document"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        public int RecognizeDocument(string front, string back, out Document document, out string msgerr)
        {
            int ret = 0;
            document = new Document();
            msgerr = null;
            List<Picture> listPict;
            try
            {
                LOG.Debug("RegulaHandler.RecognizeDocument IN...");
                //0.-Si vienen vacios front y back => ret error
                if (string.IsNullOrEmpty(front) && string.IsNullOrEmpty(back))
                {
                    msgerr = "Parametros de entrada en null";
                    return -6; // Errors.IERR_BAD_PARAMETER
                } else {
                    listPict = new List<Picture>();
                }

                //1.- Si viene fron y dorso agrego a lista
                int idx = 0;
                if (!string.IsNullOrEmpty(front)) {
                    Picture picFront = new Picture();
                    picFront.Base64ImageString = front;
                    picFront.Format = ".jpg";
                    picFront.LightIndex = 6;
                    picFront.PageIndex = idx;
                    idx++;
                    listPict.Add(picFront);
                }
                if (!string.IsNullOrEmpty(back))
                {
                    Picture picBack = new Picture();
                    picBack.Base64ImageString = back;
                    picBack.Format = ".jpg";
                    picBack.LightIndex = 6;
                    picBack.PageIndex = idx;
                    idx++;
                    listPict.Add(picBack);
                }

                //2.- LLamo a Process
                ret = _API_DOCUMENTREADER.Process(listPict, out document);

                if (ret < 0)
                {
                    document = null;
                    LOG.Error("RegulaHandler.RecognizeDocument - ret = " + ret.ToString());
                } 
            }
            catch (Exception ex)
            {
                ret = -1;
                document = null;
                LOG.Error("RegulaHandler.RecognizeDocument Excp Error: " + ex.Message);
            }

            LOG.Debug("RegulaHandler.RecognizeDocument OUT!");
            return ret;
        }

    }
}
