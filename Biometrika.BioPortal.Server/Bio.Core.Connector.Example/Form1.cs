﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Bio.Core.Api.Matcher.Interface;
using Bio.Core.Matcher;
using Bio.Core.Utils;
using BioPortal.Server.Api;

namespace Bio.Core.Connector.Example
{
    public partial class Form1 : Form
    {
        internal static ConnectorManager CONNECTOR_MANAGER;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //3.- Genero e inicializo ConnectorManager
            CONNECTOR_MANAGER = SerializeHelper.DeserializeFromFile<ConnectorManager>("Connectors.cfg");
            if (CONNECTOR_MANAGER == null || CONNECTOR_MANAGER.ConnectorsConfigured.Count == 0)
            {
                //LOG.Info("   BioPortal Server Reading Connectors - No Connectors Configured!");
            }
            else
            {
                //LOG.Info("   BioPortal Server Reading Connectors = OK - Connectors Configured = " +
                CONNECTOR_MANAGER.ConnectorsConfigured.Count.ToString();

                //Si levanto connectors, inicializo creando las instancias
                CONNECTOR_MANAGER.Initialization();

                if (CONNECTOR_MANAGER.QuantityConnectorsAvailables() == 0)
                {
                    //LOG.Info("   BioPortal Server Initializing Connectors = WARNING - No Connectors Availables!");
                }
                else
                {
                    //LOG.Info("   BioPortal Server Initializing Connectors = OK  - Connectors Availables = " +
                    //               CONNECTOR_MANAGER.QuantityConnectorsAvailables().ToString());
                    //LOG.Info("   Connectors Availables = " + CONNECTOR_MANAGER.ConnecotorsAvailablesToString());
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConnectorInstance connectorintance =
                            CONNECTOR_MANAGER.GetConnectorInstance("<Nombre Conector desde Connectors.cfg>");

            //LOG.Debug("ServiceManager.Verify -- GetConnectorInstance = " + connectorintance != null ? "OK" : "NO OK");

            //    1.1.- Si no existe retorno con error.
            if (connectorintance == null)
            {
                //msg = "No existe Connector configurado para este ConnectorId [" +
                //                                oparamin.Verifybyconnectorid + "]";
                //LOG.Warn(msg);
            }

            IConnector connector = connectorintance.Connector;
            string strxmlout;
            XmlParamIn oparamin = new XmlParamIn();                  
                //LLenar los parametros necesarios, como valueid = rut, Sample,con la huella, etc. Ver Manual
                //de BioPortal para detalles de que significa cada item.
            string strxmlin = BioPortal.Server.Api.XmlUtils.SerializeObject(oparamin);
            
            //LOG.Debug("ServiceManager.Verify -- connector.Verify...");
            int iErr = connector.Verify(strxmlin, out strxmlout);
            //LOG.Debug("ServiceManager.Verify -- connector.Verify = " + iErr.ToString());
        }
    }
}
