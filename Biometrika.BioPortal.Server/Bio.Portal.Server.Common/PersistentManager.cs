﻿using System;
using System.Collections.Generic;
using System.Text;
using NHibernate;
using System.Web;
using NHibernate.Cfg;
using log4net;

namespace Bio.Portal.Server.Common
{
    public static class PersistentManager
    {
        private const string CurrentSessionKey = "nhibernate.current_session";
        private static readonly ISessionFactory sessionFactory;
        public static String PathConf;
        private static readonly ILog log = LogManager.GetLogger(typeof(PersistentManager));

        static PersistentManager()
        {
            try
            {
                Configuration cfg = new Configuration();
                //Este parámetro lo toma desde el archivo de configuración que este invocando esto.
                cfg.Configure(System.Configuration.ConfigurationManager.AppSettings.Get("PathHibernate").ToString());                
                sessionFactory = cfg.BuildSessionFactory();

            }
            catch (Exception exe)
            {

                log.Error("Se produjo un error al inicializar la capa de persistencia:" + exe.Message, exe);
                
            }
        }

        public static ISession session()
        {
            HttpContext context = HttpContext.Current;
            ISession currentSession = context.Items[CurrentSessionKey] as ISession;

            if (currentSession == null || !currentSession.IsOpen)
            {
                currentSession =  sessionFactory.OpenSession();
                context.Items[CurrentSessionKey] = currentSession;
            }

            return currentSession;
        }

        public static void CloseSession()
        {
            HttpContext context = HttpContext.Current;
            ISession currentSession = context.Items[CurrentSessionKey] as ISession;

            if (currentSession == null)
            {
                // No current session
                return;
            }
            currentSession.Close();
            context.Items.Remove(CurrentSessionKey);
        }

        public static void CloseSessionFactory()
        {
            if (sessionFactory != null)
            {
                sessionFactory.Close();
            }
        }
    }

}
