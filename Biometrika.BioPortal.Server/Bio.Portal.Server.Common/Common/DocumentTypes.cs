﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Bio.Portal.Server.Common.Common
{
    /// <summary>
    /// Clase que lee los xml asociados a tipos de documento.
    /// </summary>
    [Serializable]
    public class DocumentTypes
    {
        
        private ArrayList listDocuments;

        public DocumentTypes() {
            listDocuments=new ArrayList();
        }
        public int AddItem(DocumentType item)
        {            
            return listDocuments.Add(item);

        }
        public DocumentType[] documents
        {
            get
            {
                DocumentType[] documents = new DocumentType[listDocuments.Count];
                listDocuments.CopyTo(documents);
                return documents;
            }
            set
            {
                if (value == null) return;
                DocumentType[] documents = (DocumentType[])value;
                listDocuments.Clear();
                foreach (DocumentType item in documents)
                {
                    listDocuments.Add(item);
                    
                }
            }
        }
        public DocumentType FindDocument(String name)
        {
            DocumentType doc = null;
            foreach (DocumentType item in documents)
            {
                if (item.Description == name)
                    doc = item;
                
            }
            return doc;
        }
    }
}
