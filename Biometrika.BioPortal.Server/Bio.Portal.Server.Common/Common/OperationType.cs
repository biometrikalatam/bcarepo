﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bio.Portal.Server.Common.Common
{
    public class OperationType
    {
        public string id;
        public string description;

        public string ID
        {
            get { return id; }
        }

        public string Description
        {
            get { return description; }
        }

        public OperationType() {}

        public OperationType(string idx, string descriptionx)
        {
            id = idx;
            description = descriptionx;
        }
    }
}
