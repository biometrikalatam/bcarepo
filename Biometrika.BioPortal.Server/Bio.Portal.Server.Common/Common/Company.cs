﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Bio.Portal.Server.Common.Common
{
    
    public class Company
    {
       
        public int id;
  
        public string rut;
   
        public int status;
 
        public DateTime? fecha;

        public Company() {}

        public Company(int idx,string rutx, int statusx, DateTime? fechax ) {

            
            id = idx;
            rut = rutx;
            status = statusx;           
            fecha = fechax;
            
        }


    }
}
