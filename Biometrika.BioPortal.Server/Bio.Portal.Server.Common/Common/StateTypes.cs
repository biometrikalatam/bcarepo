﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Bio.Portal.Server.Common.Common
{
    [Serializable]
    public class StateTypes
    {

        private ArrayList liststates;

        public StateTypes() {
            liststates = new ArrayList();
        }
        public int AddItem(StateType item)
        {            
            return liststates.Add(item);

        }
        public StateType[] states
        {
            get
            {
                StateType[] states = new StateType[liststates.Count];
                liststates.CopyTo(states);
                return states;
            }
            set
            {
                if (value == null) return;
                StateType[] states = (StateType[])value;
                liststates.Clear();
                foreach (StateType item in states)
                {
                    liststates.Add(item);
                    
                }
            }
        }

    }
}
