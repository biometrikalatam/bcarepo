﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bio.Portal.Server.Common.Common
{
    public class StateType
    {
        public string id;
        public string description;

        public string ID
        {
            get { return id; }
        }

        public string Description
        {
            get { return description; }
        }

        public StateType() {}

        public StateType(string idx, string descriptionx)
        {
            id = idx;
            description = descriptionx;
        }
    }
}
