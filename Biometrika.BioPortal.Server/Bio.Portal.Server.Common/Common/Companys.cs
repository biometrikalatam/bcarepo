﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml.Serialization;


namespace Bio.Portal.Server.Common.Common
{
    /// <summary>
    /// Companys permite administrar un listado de compañías.
    /// </summary>
    [Serializable]
    public class Companys
    {
        private ArrayList listCompanys;
        Hashtable HtCompany;


        public Companys() {
            listCompanys = new ArrayList();
            HtCompany = new Hashtable();
        }
        
        public Company[] companys
        {
            get
            {
                Company[] companys = new Company[listCompanys.Count];
                listCompanys.CopyTo(companys);                
                return companys;
            }
            set
            {
                if (value == null) return;
                Company[] companys = (Company[])value;
                listCompanys.Clear();
                foreach (Company item in companys)
                {
                    listCompanys.Add(item);
                    HtCompany.Add(item.id, item);
                }
            }
        }

        public int AddItem(Company item)
        {
            HtCompany.Add(item.id, item);
            return listCompanys.Add(item);
            
        }
        public bool ExistCompanyId(int id)
        {
            if (HtCompany.ContainsKey(id))              
                return true;
            else
                return false;
        }
        public Company getCompany(int id)
        {
            return (Company)HtCompany[id];
        }
        public bool ReplaceCompany(int id, Company company)
        {
            listCompanys.Remove(getCompany(id));
            listCompanys.Add(company);
            HtCompany.Remove(getCompany(id));
            HtCompany.Add(id, company);
            return true;
        }

        public string CompanysToString()
        {
            if (HtCompany == null || HtCompany.Count == 0)
            {
                return "Lista de compañias vacia.";
            }

            string sret = "";
            foreach (DictionaryEntry de in HtCompany)
            {
                sret += de.Key.ToString() + "-" + ((Company) de.Value).rut + "-" +
                        ((Company) de.Value).status.ToString() + "|";
            }
            return sret;
        }

    }
}
