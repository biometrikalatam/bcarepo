﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Bio.Portal.Server.Common.Common
{
    [Serializable]
    public class OperationTypes
    {
        private ArrayList listOperations;

        public OperationTypes() {
            listOperations=new ArrayList();
        }
        public int AddItem(OperationType item)
        {            
            return listOperations.Add(item);

        }
        public OperationType[] operations
        {
            get
            {
                OperationType[] operations = new OperationType[listOperations.Count];
                listOperations.CopyTo(operations);
                return operations;
            }
            set
            {
                if (value == null) return;
                OperationType[] operations = (OperationType[])value;
                listOperations.Clear();
                foreach (OperationType item in operations)
                {
                    listOperations.Add(item);
                    
                }
            }
        }
    
    }
}
