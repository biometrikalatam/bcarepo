﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web;

namespace Bio.Portal.Server.Common.Common
{
    /// <summary>
    /// Clase que administra las variables globales de las páginas.
    /// </summary>
    public class Pages:Page
    {
        #region variables globales.
        public int CompanyId
        {
            get 
            {
                return Convert.ToInt32(Session["companyId"]);
            }
            set
            {
                Session["companyId"] = value;
            }
        }
        public int UserId
        {
            get
            {
                return Convert.ToInt32(Session["UserId"]);
            }
            set
            {
                Session["UserId"] = value;
            }
        
        }
        public String Username
        {
            get
            {
                return Session["UserName"].ToString();
            }
            set
            {
                Session["UserName"] = value;
            }
        }
        public String Domain
        {
            get
            {
                return Session["Domain"].ToString();
            }
            set
            {
                Session["Domain"] = value;
            }
        }
        public String UserNameFull
        {
            get
            {
                return Session["UserNameFull"].ToString();
            }
            set
            {
                Session["UserNameFull"] = value;
            }
        }
        #endregion variables globlales.

        #region metodos globales
    //GGS desde 26-07-2010
        public static int FormatSelectedIndex(int index)
        {
            return index;
        }
        public static int FormatSelectedIndex(bool isapproved)
        {
            if (isapproved)
                return 0;
            else
                return 1;
        }
        public static string FormatIsSelected(bool isapproved, bool flag)
        {
            if (isapproved == flag)
                return "Selected";
            else
                return "";
        }
        public static string FormatIsApproved(bool isapproved)
        {
            if (isapproved)
                return "Activo";
            else
                return "Inactivo";
        }
   //GGS desde 26-07-2010
        public static string FormatStatus(int status)
        {
            if (status == 0)
                return "activa";
            else
                return "inactiva";
        }
        public static string ImagenScript(int status)
        {
            return "return ModificarOrigen(" + status + ")";
           
        }
        public static string ImagenEditarCompany(int status)
        {
            return "return ModificarCompany(" + status + ")";

        }
        public static string ImagenCambiarPassword(int id,String user)
        {
            return "return CambiarPassword(" + id + ",'" + user + "')";

        }
        public static string ImagenCambiarRoles(int id, String user)
        {
            return "return CambiarRoles(" + id + ",'" + user + "')";

        }
        
        public static string FormatoFoto(String foto)
        {
            if (foto != null)
            {
                if (foto.Length != 0)
                    return "Si";
                else
                    return "No";
            }
            else
                return "No";
        }

        public static string FormatoFotoIcoImage(String foto)
        {
            if (foto != null)
            {
                return foto.Length != 0 ? "~/Images/ico_select_ok.gif" : "~/Images/ico_not_ok.gif";
            }
            return "~/Images/ico_not_ok.gif";
        }


        public static string ImagenEliminarCompany(int status)
        {
            return "return EliminarCompany(" + status + ")";

        }
        public static string ImagenUsuario(int status)
        {
            return "return ModificarUsuarios(" + status + ")";

        }
        
        public string PopUpInfo(int Id,string tipo, string valor)
        {
            String URL = Request.Url.ToString().Substring(0,Request.Url.ToString().LastIndexOf('/')) + "/VerInfo.aspx?id=" + Id + "&tipo=" + tipo.Trim() + "&valor=" + valor.Trim();
            return "return PopUpInfo('" + URL + "')";
        }
        public string AddBirs(int Id,String typeid, String valueid)
        {
            return "return AddBirs(" + Id  +",'" + typeid + "','" + valueid + "')";
        }
        public string ModifyIdentity(int Id)
        {
            return "return ModificarIdentidad(" + Id + ")";
        }
        public string DeleteIdentity(int Id)
        {
            return "return DeleteBirs(" + Id + ")";
        }
        public string FormatMinutiaeType(int minutiae)
        {
            return Bio.Core.Matcher.Constant.MinutiaeType.GetDescription(minutiae);   
        }
        public string FormatAutheticationFactor(int factor)
        {
            return Bio.Core.Matcher.Constant.AuthenticationFactor.GetName(factor);
        }
        public string FormatType(int typem)
        {
            return Bio.Core.Matcher.Constant.BirType.GetName(typem);
        }
        public string FormatBodyPart(int bodypart)
        {
            return Bio.Core.Matcher.Constant.BodyPart.GetName(bodypart);
        }
        public static bool EsNumerico(string value)
        {
            int valor = 0;
            try
            {
                valor = Convert.ToInt32(value);
                return true;
            }
            catch (Exception exe)
            {                
                return false;
            }
        }
        
       
        #endregion metodos globales
    }
}
