﻿using System;
using System.Collections.Generic;
using System.Text;
using log4net;
using System.Data;
using NHibernate;
using System.Collections;
using NHibernate.Criterion;
using System.Globalization;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Common.Entities
{
    
    public class AdminBpLog
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpLog));
        public static DataView RetrieveLog(string level, String fechad, String fechah,out string msgErr)
        {
            msgErr = "S/C";
            IList l = null;
            DataView dw = null;
            DateTime dtD = new DateTime();
            DateTime dtH = new DateTime();
            CultureInfo ci = new CultureInfo("es-CL");
            ci.DateTimeFormat.ShortDatePattern = "yyyy/MM/dd";
            ci.DateTimeFormat.ShortTimePattern = "HH:mm:ss";

            //ISession sess = null;
            try
            {
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                    //sess = PersistentManager.session();
                    using (ISession sess = PersistentManager.session())
                    {
                        //Aplico filtros para BpLog
                        ICriteria crit = sess.CreateCriteria(typeof(BpLog));
                        if (!(level.Equals("Todos")))
                        {
                            crit.Add(Expression.Eq("Levell", level));
                        }
                        //Si ambas fechas no son nulas
                        if ((!(fechad.Equals(""))) && (!(fechah.Equals(""))))
                        {

                            dtD = DateTime.Parse(fechad, ci);
                            dtH = DateTime.Parse(fechah, ci);
                            crit.Add(Expression.Between("Datel", dtD, dtH));
                        }
                        else if (!(fechad.Equals("")))
                        {
                            dtD = DateTime.Parse(fechad, ci);
                            crit.Add(Expression.Ge("Datel", dtD));
                        }
                        else if (!(fechah.Equals("")))
                        {
                            dtH = DateTime.Parse(fechah, ci);
                            crit.Add(Expression.Le("Datel", dtH));
                        }
                        l = crit.AddOrder(new Order("Datel", false)).List();
                    }
                //Gereramos el DataView
                DataRow dr;
                DataRow drConx;
                DataTable dt = new DataTable();
                //Agrego columnas
                dt.Columns.Add(new DataColumn("#")); 
                dt.Columns.Add(new DataColumn("Fecha"));
                //dt.Columns.Add(new DataColumn("Thread"));
                dt.Columns.Add(new DataColumn("Nivel"));
                dt.Columns.Add(new DataColumn("Origen"));
                dt.Columns.Add(new DataColumn("Mensaje"));
                dt.Columns.Add(new DataColumn("Excepcion"));
                foreach (BpLog lg in l)
                {
                    dr = dt.NewRow();
                    dr["#"] = lg.Id;
                    dr["Fecha"] = lg.Datel;
                    //dr["Thread"]=lg.Threadl;
                    dr["Nivel"] = lg.Levell;
                    dr["Origen"] = lg.Loggerl;
                    dr["Mensaje"] = lg.Messagel;
                    dr["Excepcion"] = lg.Exceptionl;
                    dt.Rows.Add(dr);
                }
                dw = new DataView(dt);
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                dw = null;
                LOG.Error("AdminBpLog.Create", ex);
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return dw;
        }

    }
}
