﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Constant;
using Bio.Core.Matcher;
using Bio.Core.Matcher.Interface;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using System.Collections;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Common.Entities
{
    public class AdminBpIdentityDynamicdata
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpIdentityDynamicdata));

#region static Region
        //public static int DeleteBirs(int companyid, String valueid, String typeid, out String msgErr, out Exception objException)
        //{
        //    int res = 0;
        //    msgErr = "";
        //    objException = null;
        //    //ISession sess = HSessionFactory._sessionFactory.OpenSession();
        //    ISession sess = PersistentManager.session();

        //    try
        //    {
        //        IQuery query = sess.GetNamedQuery("DELETEBIRS");
        //        query.SetString("valueid", valueid);
        //        query.SetString("typeid", typeid);
        //        query.SetInt32("companyidenroll", companyid);
        //        query.ExecuteUpdate();
        //        LOG.Info("Se eliminan las huellas del identity");

        //    }
        //    catch (Exception exe)
        //    {
        //        LOG.Info("Se produjo un error al eliminar las huellas");
        //        msgErr = exe.Message;
        //        objException = exe;
        //        res = -1;
        //    }
        //    return res;
        //}

        public static BpIdentityDynamicdata Retrieve(int id, out string msgErr)
            {
                msgErr = "S/C";
                BpIdentityDynamicdata bir;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                try
                {
                    //bir = (BpBir)sess.Load(typeof(BpBir), id);
                    using (ISession sess = PersistentManager.session())
                    {
                        bir = sess.CreateCriteria(typeof(BpIdentityDynamicdata))
                                                      .Add(Expression.Eq("Id", id))
                                                      .UniqueResult<BpIdentityDynamicdata>();
                    }
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpIdentityDynamicdata.Retrieve", ex);
                    bir = null;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
                return bir;

            }

            public static bool Delete(int id, out string msgErr)
            {
                msgErr = "S/C";
                BpIdentityDynamicdata bir = null;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                try
                {
                    bir = AdminBpIdentityDynamicdata.Retrieve(id, out msgErr);
                    if (bir != null)
                    {
                        using (ISession sess = PersistentManager.session())
                        {
                            sess.Delete(bir);
                            sess.Flush();
                        }
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpIdentityDynamicdata.Delete", ex);
                    return false;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
            }

            public static int RetrieveWithFilter(int id, int identid, string key,
                                                 out List<BpIdentityDynamicdata> listddata)
            {
                int ret;
                List<BpIdentityDynamicdata> ddata = new List<BpIdentityDynamicdata>();
                listddata = null;
                string msg;
                BpIdentity identity;
                IList listIdentities = null;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                try
                {
                    LOG.Debug("AdminBpIdentityDynamicdata.RetrieveWithFilter in...");
                    LOG.Debug("AdminBpIdentityDynamicdatar.RetrieveWithFilter identid = " + identid.ToString() +
                              " - key = " + key);
                    //Busco que exista la identidad, si viene typeid/valueid != null (es Verify) sino retorno con error
                    identity = AdminBpIdentity.Retrieve(identid, out msg);

                    //Si es Verify y no encotnro identidad retorno error, sino si es Identify sigo
                    if (identity == null)
                    {
                        LOG.Debug("No se encontro la identidad con valores identid=" + identid);
                        listddata = null;
                        return Errors.IERR_IDENTITY_NOT_FOUND;
                    }

                    LOG.Debug("AdminBpIdentityDynamicdata.RetrieveWithFilter id = " + id.ToString());
                        //Si viende id de BIR busco por id, porque es unico y mas rapido
                    if (id > 0)
                    {
                        ddata.Add(AdminBpIdentityDynamicdata.Retrieve(id, out msg));
                    } else  //Genero filtro
                    {
                        LOG.Debug("AdminBpIdentityDynamicdata.RetrieveWithFilter crea criteria...");
                        //Creo criteria
                        using (ISession sess = PersistentManager.session())
                        {
                            ICriteria crit = sess.CreateCriteria(typeof(BpIdentityDynamicdata));

                            //Filtro por Identity primero siemrpe que sea verificacion
                            if (identity != null)
                            {
                                crit.Add(Expression.Eq("BpIdentity", identity));
                            }

                            //Si es >0 => no es Any/All/None
                            if (!String.IsNullOrEmpty(key))
                            {
                                crit.Add(Expression.Eq("DDKey", key));
                            }


                            LOG.Debug("AdminBpIdentityDynamicdata.RetrieveWithFilter Criteria = " + crit.ToString());
                            //Recupero los elementos con filtro
                            ddata = (List<BpIdentityDynamicdata>)crit.AddOrder(new Order("Id", true)).
                                                              List<BpIdentityDynamicdata>();
                        }
                        listddata = ddata;
                        LOG.Debug("AdminBpIdentityDynamicdata.RetrieveWithFilter listddata.count = ");
                        LOG.Debug(listddata == null ? "null" : listddata.Count.ToString());
                    }
                    ret = Errors.IERR_OK;
                }
                catch (Exception ex)
                {
                    LOG.Error("AdminBpIdentityDynamicdata.RetrieveWithFilter", ex);
                    ddata = null;
                    ret = Errors.IERR_UNKNOWN;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
                return ret;
            }

            public static int SerializeListDDToXML(List<BpIdentityDynamicdata> listddata, out string xmllistDD)
            {
                int ret;
                xmllistDD = null; 
                try
                {
                    if (listddata == null || listddata.Count == 0) return Errors.IERR_OK;

                    Bio.Core.Api.DynamicData arrDD = new Core.Api.DynamicData();
                    
                    foreach (BpIdentityDynamicdata ddata in listddata)
                    {
                        arrDD.AddValue(ddata.DDKey, ddata.DDValue);
                    }

                    xmllistDD = Bio.Core.Utils.SerializeHelper.SerializeToXml(arrDD);
                    ret = Errors.IERR_OK;
                }
                catch (Exception ex)
                {
                    LOG.Error("AdminBpIdentityDynamicdata.SerializeListDDToXML", ex);
                    xmllistDD = null;
                    ret = Errors.IERR_UNKNOWN;
                }
                return ret;
            }

        #endregion static Region

    }
}
