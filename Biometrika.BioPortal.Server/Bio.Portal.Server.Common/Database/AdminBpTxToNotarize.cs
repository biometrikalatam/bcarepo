﻿using System;
using log4net;
using NHibernate;
using System.Collections;
using NHibernate.Criterion;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Common.Entities
{
    [Serializable]
    public class AdminBpTxToNotarize
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpTxToNotarize));

        #region staticMethods

        public static BpTxToNotarize Create(string trackid, out string msgErr)
        {
            BpTxToNotarize transaccion;

            msgErr = "S/C";
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();

            try
            {
                transaccion = new BpTxToNotarize(trackid);
                using (ISession sess = PersistentManager.session())
                {
                    sess.Save(transaccion);
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                transaccion = null;
                LOG.Error("AdminBpTxToNotarize.Create", ex);
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return transaccion;

        }



        public static BpTxToNotarize Retrieve(int id, out string msgErr)
        {
            msgErr = "S/C";
            BpTxToNotarize transaccion;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();

            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    transaccion = (BpTxToNotarize)sess.Load(typeof(BpTxToNotarize), id);
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                transaccion = null;
                LOG.Error("AdminBpTxToNotarize.Retrieve", ex);
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return transaccion;

        }

        public static IList RetrieveTxByTrackid(string trackid, out string msgErr)
        {
            msgErr = "S/C";
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();

            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    return sess.CreateCriteria(typeof(BpTxToNotarize)).
                        Add(Expression.Eq("Trackid", trackid.Trim()))
                        .List();
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("AdminBpTxToNotarize.RetrieveTxByTrackid", ex);
                return null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
        }



        public static IList ListAll
        {

            get
            {
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                IList list;
                using (ISession sess = PersistentManager.session())
                {
                    list = sess.CreateCriteria(typeof(BpTxToNotarize)).List();
                }
                return list;
            }
        }


        public static bool Write(BpTxToNotarize bptx, out BpTxToNotarize bptxsaved, out string msgErr)
        {
            msgErr = "S/C";
            bptxsaved = null;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            //ITransaction tx = sess.BeginTransaction();
            try
            {
                //sess.SaveOrUpdate(bptx);
                //bptxsaved = (BpTx)
                using (ISession sess = PersistentManager.session())
                {
                    sess.SaveOrUpdate(bptx);
                    bptxsaved = bptx;
                }
                //  tx.Commit();
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                //if (tx != null) tx.Rollback();
                LOG.Error("AdminBpTxToNotarize.Write", ex);
                return false;
            }
            //finally
            //{
            //    if (sess != null) sess.Close();
            //}
            return true;
        }

        public static bool Update(BpTxToNotarize bptx, out string msgErr)
        {
            msgErr = "S/C";
            bool bRet = false;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            //ITransaction tx = sess.BeginTransaction();
            try
            {
                //sess.SaveOrUpdate(bptx);
                using (ISession sess = PersistentManager.session())
                {
                    sess.Update(bptx);
                    sess.Flush();
                    bRet = true;
                    //  tx.Commit();
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                //if (tx != null) tx.Rollback();
                LOG.Error("AdminBpTxToNotarize.Update", ex);
                bRet = false;
            }
            //finally
            //{
            //    if (sess != null) sess.Close();
            //}
            return bRet;
        }
        #endregion staticMethods

    }
}
