﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Constant;
using Bio.Core.Matcher;
using Bio.Core.Matcher.Interface;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using System.Collections;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Common.Entities
{
   public class AdminBpBir
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpBir));

        #region static Region
        public static int DeleteBirs(int companyid, String valueid, String typeid, out String msgErr, out Exception objException)
        {
            int res = 0;
            msgErr = "";
            objException = null;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();

            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    IQuery query = sess.GetNamedQuery("DELETEBIRS");
                    query.SetString("valueid", valueid);
                    query.SetString("typeid", typeid);
                    query.SetInt32("companyidenroll", companyid);
                    query.ExecuteUpdate();
                    LOG.Info("Se eliminan las huellas del employee");
                }
            }
            catch (Exception exe)
            {
                LOG.Info("Se produjo un error al eliminar las huellas");
                msgErr = exe.Message;
                objException = exe;
                res = -1;
            }
            return res;
        }

            public static BpBir Retrieve(int id, out string msgErr)
            {
                msgErr = "S/C";
                BpBir bir;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        //bir = (BpBir)sess.Load(typeof(BpBir), id);
                        bir = sess.CreateCriteria(typeof(BpBir))
                                                      .Add(Expression.Eq("Id", id))
                                                      .UniqueResult<BpBir>();
                    }
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpBir.Retrieve", ex);
                    bir = null;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
                return bir;

            }

            public static bool Delete(int id, out string msgErr)
            {
                msgErr = "S/C";
                BpBir bir = null;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                try
                {
                    bir = AdminBpBir.Retrieve(id, out msgErr);
                    if (bir != null)
                    {
                        using (ISession sess = PersistentManager.session())
                        {
                            sess.Delete(bir);
                            sess.Flush();
                        }
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpBir.Delete", ex);
                    return false;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
            }

            public static int RetrieveWithFilter(int id, int companyidenroll,
                                                 string typeid, string valueid,
                                                 int authenticationfactor, int minutiaetype, int bodypart,
                                                 bool checkvf, out List<BpBir> listBirs)
            {
                int ret;
                List<BpBir> birs = new List<BpBir>();
                listBirs = null;
                string msg;
                BpIdentity identity;
                IList listIdentities = null;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                try
                {
                    LOG.Debug("AdminBpBir.RetrieveWithFilter in...");
                    LOG.Debug("AdminBpBir.RetrieveWithFilter companyidenroll = " + companyidenroll.ToString() +
                              " - typeid = " + typeid + " - valueid = " + valueid);
                    //Busco que exista la identidad, si viene typeid/valueid != null (es Verify) sino retorno con error
                    if (typeid != null && valueid != null)
                    {
                        listIdentities =
                            AdminBpIdentity.GetIdentitiesByFiltro(companyidenroll, typeid.Trim(), valueid.Trim(), null, null);
                    }

                    identity = listIdentities != null && listIdentities.Count > 0 ? (BpIdentity)listIdentities[0] : null;

                    //Si es Verify y no encotnro identidad retorno error, sino si es Identify sigo
                    if (identity == null && (typeid != null && valueid != null))
                    {
                        LOG.Debug("No se encontro la identidad con valores typeid=" + typeid + "/valueid=" + valueid);
                        listBirs = null;
                        return Errors.IERR_IDENTITY_NOT_FOUND;
                    }

                    LOG.Debug("AdminBpBir.RetrieveWithFilter id = " + id.ToString());
                        //Si viende id de BIR busco por id, porque es unico y mas rapido
                    if (id > 0)
                    {
                        birs.Add(AdminBpBir.Retrieve(id, out msg));
                    } else  //Genero filtro
                    {
                        LOG.Debug("AdminBpBir.RetrieveWithFilter crea criteria...");
                        using (ISession sess = PersistentManager.session())
                        {

                            //Creo criteria
                            ICriteria crit = sess.CreateCriteria(typeof(BpBir));

                            //Filtro por Identity primero siemrpe que sea verificacion
                            if (identity != null)
                            {
                                crit.Add(Expression.Eq("BpIdentity", identity));
                            }

                            //Si es >0 => no es Any/All/None
                            if (authenticationfactor > 0)
                            {
                                crit.Add(Expression.Eq("Authenticationfactor", authenticationfactor));
                            }

                            if (minutiaetype > 0)
                            {
                                crit.Add(Expression.Eq("Minutiaetype", minutiaetype));
                            }

                            if (bodypart > 0)
                            {
                                crit.Add(Expression.Eq("Bodypart", bodypart));
                            }

                            if (companyidenroll > 0)
                            {
                                crit.Add(Expression.Eq("Companyidenroll", companyidenroll));
                            }

                            LOG.Debug("AdminBpBir.RetrieveWithFilter Criteria = " + crit.ToString());
                            //Recupero los elementos con filtro
                            birs = (List<BpBir>)crit.AddOrder(new Order("Authenticationfactor", true)).
                                                              AddOrder(new Order("Type", true)).
                                                              AddOrder(new Order("Bodypart", true)).
                                                              AddOrder(new Order("Minutiaetype", true)).
                                                              List<BpBir>();
                        }
                        //Added 28-03-2018 
                        //Se descartan las birs de las identities que tienen VerificationSource == null 
                        //para dejarlas como enroladas temporales a esas.
                        LOG.Debug("AdminBpBir.RetrieveWithFilter Antes de depurar => listBirs.count = ");
                        LOG.Debug(birs == null ? "null" : birs.Count.ToString());

                        LOG.Debug("AdminBpBir.RetrieveWithFilter Calling DepureBirsFromIdentityVerifiedSource [CheckVF = " + checkvf.ToString() + "]...");

                        //Added 29-05-2019 - Para poder hacer compatible version anterior x Pronto!
                        //                   Si checkvf que viene en Web.Config de BioPortal.Service es true => Depura lista 
                        //                   sino toma tal cual hizo GetBIRs        
                        if (checkvf) listBirs = DepureBirsFromIdentityVerifiedSource(birs, authenticationfactor);
                        else listBirs = birs;

                        LOG.Debug("AdminBpBir.RetrieveWithFilter Depurado listBirs.count = ");
                        LOG.Debug(listBirs == null ? "null" : listBirs.Count.ToString());
                    }
                    ret = Errors.IERR_OK;

                }
                catch (Exception ex)
                {
                    LOG.Error("AdminBpBir.RetrieveWithFilter", ex);
                    birs = null;
                    ret = Errors.IERR_UNKNOWN;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
                return ret;
            }

        /// <summary>
        /// Recorre lista de birs, y si BpIdentity es != nulo y Verificationsource != null =>
        /// lo agrega a lista de retorno, sino no. Es para depurar y luego hacer la verificación 
        /// o identificación SOLO con las identities verificadasde  alguna forma.
        /// </summary>
        /// <param name="birs"></param>
        /// <returns></returns>
        private static List<BpBir> DepureBirsFromIdentityVerifiedSource(List<BpBir> birs)
        {
            List<BpBir> listRet = new List<BpBir>();
            try
            {
                foreach (BpBir item in birs)
                {
                    
                    if (item.BpIdentity != null && !String.IsNullOrEmpty(item.BpIdentity.Verificationsource))
                    {
                        listRet.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpBir.RetrieveWithFilter", ex);
                birs = null;
                listRet = null;
            }
            return listRet;
        }

        /// <summary>
        /// Added 08-2018 para evaluar verification source de acuerdo a si verifico manual o automatico
        /// Recorre lista de birs, y si BpIdentity es != nulo y Verificationsource != null =>
        /// lo agrega a lista de retorno, sino no. Es para depurar y luego hacer la verificación 
        /// o identificación SOLO con las identities verificadasde  alguna forma.
        /// </summary>
        /// <param name="birs"></param>
        /// <param name="authenticationfactor"></param>
        /// <returns></returns>
        private static List<BpBir> DepureBirsFromIdentityVerifiedSource(List<BpBir> birs, int authenticationfactor)
        {
            List<BpBir> listRet = new List<BpBir>();
            try
            {
                LOG.Debug("AdminBpBir.DepureBirsFromIdentityVerifiedSource IN...");
                Hashtable htVS;
                //bool isverificated = VerificationSource.IsIdentityValidated(birs[0].BpIdentity.Verificationsource, birs[0].Authenticationfactor); // AdminBpIdentity.IsIdentityValidated(birs[0].BpIdentity, out htVS);


                bool isverificated = false;
                if (birs != null && birs.Count > 0 && birs[0].BpIdentity != null && birs[0].BpIdentity.Verificationsource != null)
                {
                    LOG.Debug("AdminBpBir.DepureBirsFromIdentityVerifiedSource - Calling a VerificationSource.IsIdentityValidated...");
                    isverificated = VerificationSource.IsIdentityValidated(birs[0].BpIdentity.Verificationsource, authenticationfactor);
                }
                LOG.Debug("AdminBpBir.DepureBirsFromIdentityVerifiedSource isverificated = " + isverificated.ToString());
                if (!isverificated)  //No está ok ningun AF => directo depuro todo
                {
                    LOG.Debug("AdminBpBir.DepureBirsFromIdentityVerifiedSource isverificated = false");
                    listRet = null;
                }
                else //Si hay al menos uno verificado, pregunto por cada bir
                {
                    LOG.Debug("AdminBpBir.DepureBirsFromIdentityVerifiedSource isverificated = true => Recorro lista y chequeo...");
                    foreach (BpBir item in birs)
                    {
                        LOG.Debug("AdminBpBir.DepureBirsFromIdentityVerifiedSource Check Id=" + item.Id + "-AF=" + item.Authenticationfactor);
                        listRet.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpBir.DepureBirsFromIdentityVerifiedSource", ex);
                birs = null;
                listRet = null;
            }
            LOG.Debug("AdminBpBir.DepureBirsFromIdentityVerifiedSource OUT!");
            return listRet;
        }

        public static int SerializeListBirsToITemplate(List<BpBir> listBirs, out string xmllistBirs)
        {
            int ret;
            xmllistBirs = null;
            List<Template> Templates = new List<Template>();
            try
            {
                Template temp; 
                foreach (BpBir bir in listBirs)
                {
                    temp = new Template();
                    temp.Id = bir.Id;
                    temp.AuthenticationFactor = bir.Authenticationfactor;
                    temp.MinutiaeType = bir.Minutiaetype;
                    temp.BodyPart = bir.Bodypart;
                    //string[] aux = bir.Data.Split('|');
                    //string sdata = "";
                    //if (aux.Length == 1) sdata = aux[0];
                    //if (aux.Length >= 2) sdata = aux[1];
                    temp.SetData = bir.Data;
                    temp.Companyidenroll = bir.Companyidenroll;
                    temp.Useridenroll = bir.Useridenroll;
                    //if (bir.BpIdentity.Equals(null))
                    //{
                    //    bir.BpIdentity 
                    //}
                    temp.Verificationsource = bir.BpIdentity.Verificationsource;
                    Templates.Add(temp);
                }

                xmllistBirs = Bio.Core.Utils.SerializeHelper.SerializeToXml(Templates);
                temp = null;

                ret = Errors.IERR_OK;
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpBir.RetrieveWithFilter", ex);
                xmllistBirs = null;
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }

        public static int ConvertTemplateToBir(BpIdentity oIdentity, ITemplate template, 
                                                int companyidenroll, int useridenroll, 
                                                out BpBir birgenerated)
        {
            int ret = Errors.IERR_OK;
            birgenerated = null;
                
            try
            {
                LOG.Error("AdminBpBir.ConvertTemplateToBir IN...");
                birgenerated = new BpBir(oIdentity, template.Type, template.AuthenticationFactor, 
                                            template.MinutiaeType, template.BodyPart, template.GetData, 
                                            template.AdditionalData, DateTime.Now, DateTime.Now,null,
                                            companyidenroll, useridenroll);
                ret = Errors.IERR_OK;
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpBir.ConvertTemplateToBir", ex);
                birgenerated = null;
                ret = Errors.IERR_UNKNOWN;
            }
            LOG.Error("AdminBpBir.ConvertTemplateToBir OUT!");
            return ret;
        }

        /// <summary>
        /// Dados los parametros de AF/MT, retorna el ultimo BIR que cumple con ese filtro enrolado, de acuerdoa  CreationDate
        /// </summary>
        /// <param name="companyid"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="authenticationfactor"></param>
        /// <param name="minutiaetype"></param>
        /// <returns></returns>
        public static BpBir RetrieveLastBir(int companyid, string typeid, string valueid, 
            int authenticationfactor, int minutiaetype, int bodypart)
        {
            BpBir lastbir = null;
            List<BpBir> birs = new List<BpBir>();
            string msg;
            BpIdentity identity;
            IList listIdentities = null;
            try
            {
                LOG.Debug("AdminBpBir.RetrieveLastBir IN - companyidenroll = " + companyid.ToString() +
                              " - typeid = " + typeid + " - valueid = " + valueid);
                //Busco que exista la identidad, si viene typeid/valueid != null (es Verify) sino retorno con error
                if (typeid != null && valueid != null)
                {
                    listIdentities =
                        AdminBpIdentity.GetIdentitiesByFiltro(companyid, typeid.Trim(), valueid.Trim(), null, null);
                }

                identity = listIdentities != null && listIdentities.Count > 0 ? (BpIdentity)listIdentities[0] : null;

                //Si es Verify y no encotnro identidad retorno error, sino si es Identify sigo
                if (identity == null && (typeid != null && valueid != null))
                {
                    LOG.Debug("AdminBpBir.RetrieveLastBir No se encontro la identidad con valores typeid=" + typeid + "/valueid=" + valueid + 
                                ". Devuelve NULL");
                    lastbir = null;
                    return lastbir;
                }


                LOG.Debug("AdminBpBir.RetrieveLastBir crea criteria...");
                using (ISession sess = PersistentManager.session())
                {

                    //Creo criteria
                    ICriteria crit = sess.CreateCriteria(typeof(BpBir));

                    //Filtro por Identity primero siemrpe que sea verificacion
                    if (identity != null)
                    {
                        crit.Add(Expression.Eq("BpIdentity", identity));
                    }

                    //Si es >0 => no es Any/All/None
                    if (authenticationfactor > 0)
                    {
                        crit.Add(Expression.Eq("Authenticationfactor", authenticationfactor));
                    }

                    if (minutiaetype > 0)
                    {
                        crit.Add(Expression.Eq("Minutiaetype", minutiaetype));
                    }

                    if (bodypart > 0)
                    {
                        crit.Add(Expression.Eq("Bodypart", bodypart));
                    }

                    if (companyid > 0)
                    {
                        crit.Add(Expression.Eq("Companyidenroll", companyid));
                    }

                    LOG.Debug("AdminBpBir.RetrieveLastBir Criteria = " + crit.ToString());
                    //Recupero los elementos con filtro
                    birs = ((List<BpBir>)crit.AddOrder(new Order("Creation", false)).
                                                      List<BpBir>());

                    if (birs==null || birs.Count == 0)
                    {
                        lastbir = null;
                        LOG.Debug("AdminBpBir.RetrieveLastBir Lista de BIRs vacia => return NULL!");
                    } else
                    {
                        lastbir = birs[0];
                        LOG.Debug("AdminBpBir.RetrieveLastBir - Last BIR => Id = " + lastbir.Id +
                                    " - Creation = " + lastbir.Creation.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpBir.RetrieveLastBir", ex);
                lastbir = null;
            }
            return lastbir;
        }

        #endregion static Region

    }
}
