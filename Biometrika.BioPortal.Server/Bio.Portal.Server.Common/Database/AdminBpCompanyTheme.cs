﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bio.Core.Constant;
using Bio.Portal.Server.Common.Entities.Database;
using log4net;
using NHibernate;
using NHibernate.Criterion;

namespace Bio.Portal.Server.Common.Entities
{
    public class AdminBpCompanyTheme
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpCompanyTheme));

        public static int Retrieve(int companyid, string name, out BpCompanyTheme bpCompanyTheme) 
        {
            bpCompanyTheme = null;
            int ret = Errors.IERR_OK;
            try
            {
                IList l;
                using (ISession sess = PersistentManager.session())
                {
                    l = sess.CreateCriteria(typeof(BpCompanyTheme)).
                        Add(Restrictions.Eq("CompanyId", companyid)).
                        Add(Expression.Eq("Name", name))
                        .List();
                }
                if (l == null || l.Count == 0)
                    ret = Errors.IERR_IDENTITY_NOT_FOUND;
                else 
                {
                    IEnumerator en = l.GetEnumerator();
                    en.MoveNext();
                    bpCompanyTheme = (BpCompanyTheme)en.Current;
                    ret = Errors.IERR_OK;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpCompanyTheme.Retrieve (CompanyId, Name)", ex);
                bpCompanyTheme = null;
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }
    }
}
