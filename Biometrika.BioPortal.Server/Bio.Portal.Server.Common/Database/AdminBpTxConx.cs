﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Matcher.Constant;
using log4net;
using NHibernate;
using System.Collections;
using System.Data;
using System.Globalization;
using NHibernate.Criterion;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Common.Entities
{
    [Serializable]
    public class AdminBpTxConx
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpTxConx));

#region staticMethods
        
        //public static BpTx Create(string trackid, int operationcode, string typeid, string valueid, int result, double score,
        //                    double threshold, DateTime timestampstart, DateTime timestampend, int authenticatorfactor, 
        //                    int minutiaetype, int bodypart, int actiontype, BpOrigin bporigin, DateTime? timestampclient, 
        //                    string clientid, string ipenduser, string enduser, string dynamicdata, int companyidtx, 
        //                    int useridtx, int operationsource, string abs, out string msgErr)
        //    {
        //        BpTx transaccion;

        //        msgErr = "S/C";
        //        //ISession sess = HSessionFactory._sessionFactory.OpenSession();
        //        //ISession sess = PersistentManager.session();

        //        try
        //        {
        //            transaccion = new BpTx(trackid, operationcode, typeid, valueid, result, score, 
        //                    threshold, timestampstart, timestampend, authenticatorfactor, 
        //                    minutiaetype, bodypart, actiontype, bporigin, timestampclient,
        //                    clientid, ipenduser, enduser, dynamicdata, companyidtx, useridtx, operationsource, abs);
        //            using (ISession sess = PersistentManager.session())
        //            {
        //                sess.Save(transaccion);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            msgErr = ex.Message;
        //            transaccion = null;
        //            LOG.Error("AdminBpTx.Create", ex);
        //        }
        //        //finally
        //        //{
        //        //    if (sess != null && sess.IsOpen) sess.Close();
        //        //}
        //        return transaccion;

        //    }

            //public static BpTx Create(string trackid, string typeid, string valueid, int result, double score,
            //                double threshold, DateTime timestampstart, DateTime timestampend, int authenticatorfactor, 
            //                int minutiaetype, int bodypart, int actiontype, BpOrigin bporigin, DateTime? timestampclient, 
            //                string clientid, string ipenduser, string enduser, string dynamicdata, int companyidtx, 
            //                int useridtx, BpTxConx conx, out string msgErr)
            //{
            //    msgErr = "S/C";
            //    BpTx transaccion;
            //    ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //    ITransaction tx = sess.BeginTransaction();
            //    try
            //    {
            //        transaccion = new BpTx(trackid, typeid, valueid, result, score,
            //                threshold, timestampstart, timestampend, authenticatorfactor,
            //                minutiaetype, bodypart, actiontype, bporigin, timestampclient,
            //                clientid, ipenduser, enduser, dynamicdata, companyidtx, useridtx);
            //        sess.Save(transaccion);

            //        if (conx != null)
            //        {
            //            conx.BpTx = transaccion;
            //            AdminBpTx.AddTxConx(transaccion, conx, out msgErr);
            //        }
            //        tx.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        msgErr = ex.Message;
            //        tx.Rollback();
            //        transaccion = null;
            //        LOG.Error("AdminBpTx.Create (w/Conx) ", ex);
            //    }
            //    finally
            //    {
            //        if (sess != null && sess.IsOpen) sess.Close();
            //        if (tx != null) tx = null;
            //    }
            //    return transaccion;

            //}


            //public static BpTx Create(string trackid, int operationcode, string typeid, string valueid, int result, double score,
            //                double threshold, DateTime timestampstart, DateTime timestampend, int authenticationfactor,
            //                int minutiaetype, int bodypart, int actiontype, BpOrigin bporigin, DateTime? timestampclient,
            //                string clientid, string ipenduser, string enduser, string dynamicdata, int companyidtx,
            //                int useridtx, int operationsource, string abs, IList conxList, out string msgErr)
            //{
            //    msgErr = "S/C";
            //    BpTx transaccion;
            //    BpTx transaccionsaved = null;

            //    try
            //    {
            //        transaccion = new BpTx(trackid, operationcode, typeid, valueid, result, score,
            //                threshold, timestampstart, timestampend, authenticationfactor,
            //                minutiaetype, bodypart, actiontype, bporigin, timestampclient,
            //                clientid, ipenduser, enduser, dynamicdata, companyidtx, useridtx, operationsource, abs);

            //        if (conxList != null)
            //        {
            //            foreach (BpTxConx conxAux in conxList)
            //            {
            //                //conxAux.BpTx = transaccion;
            //                AdminBpTx.AddTxConx(transaccion, conxAux, out msgErr);
            //            }
            //        }
            //        if (!AdminBpTx.Write(transaccion, out transaccionsaved, out msgErr))
            //        {
            //            transaccionsaved = null;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        msgErr = ex.Message;
            //        transaccionsaved = null;
            //        LOG.Error("AdminBpTx.Create (w/List Conx)", ex);
            //    }

            //    return transaccionsaved;

            //}

            //public static int CreateTx(int actiontype, int operationcode,  
            //                           int company, string typeid, string valueid, string trackid, int consultationtype,
            //                           string customertrackid, string callbackurl, string redirecturl, int signerinclude, int checklock,
            //                           int onboardingmandatory, string theme, float threshold, string new_trackid3ro,
            //                           int authenticationfactor, int minutiaetype, int bodypart, string url3ro,
            //                           string titlebpweb, string messagebpweb, out string msgErr)
            //{
            //    msgErr = "S/C";
            //    BpTx transaccion;
            //    BpTx transaccionsaved = null;
            //    int iRet = 0;
            //    try
            //    {
            //        BpOrigin bporigin = AdminBpOrigin.Retrieve(45, out msgErr);  //new BpOrigin(45, "BioPortal Web");
            //        if (bporigin == null) bporigin = AdminBpOrigin.Retrieve(1, out msgErr);
            //        int operationsource = 0;
            //        switch (consultationtype)
            //        {
            //            case 1:
            //                operationsource = 1;
            //                break;
            //            case 2:
            //                operationsource = 3;
            //                break;
            //            case 3:
            //                operationsource = 3;
            //                break;
            //            default:
            //                operationsource = 3;
            //                break;
            //        }

            //        transaccion = new BpTx(trackid, operationcode, typeid, valueid, 0, 0,
            //                threshold, DateTime.Now, DateTime.Now, authenticationfactor,
            //                minutiaetype, bodypart, actiontype, bporigin, null,
            //                null, null, null, null, company, 0, operationsource, null);
            //        transaccion.CheckLock = checklock;
            //        transaccion.TitleBpWeb = titlebpweb;
            //        transaccion.MessageBpWeb = messagebpweb;
              
            //        BpTxConx conxAux = new BpTxConx();
            //        conxAux.Connectorid = consultationtype.ToString();
            //        conxAux.Consultationtype = consultationtype.ToString();
            //        conxAux.CallbackUrl = callbackurl;
            //        conxAux.CustomerTrackId = customertrackid;
            //        conxAux.OnboardingMandatory = onboardingmandatory;
            //        conxAux.RedirectUrl = redirecturl;
            //        conxAux.SignerInclude = signerinclude;
            //        //conxAux.CheckLock = checklock;
            //        conxAux.Theme = theme;
            //        conxAux.Threshold = threshold;
            //        conxAux.Timestamp = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            //        conxAux.Trackid = trackid;
            //        conxAux.Trackid3ro = new_trackid3ro;
            //        conxAux.Status = 0;
            //        conxAux.Result = 0;
            //        conxAux.Score = 0;
            //        //conxAux.TypeVerify = consultationtype;
            //        conxAux.Url3ro = url3ro;
                    
            //        //conxAux.BpTx = transaccion;
            //        transaccionsaved = AdminBpTx.AddTxConx(transaccion, conxAux, out msgErr);
            //        if (!AdminBpTx.Write(transaccion, out transaccionsaved, out msgErr))
            //        {
            //            transaccionsaved = null;
            //        }
            //        iRet = 0;
            //    }
            //    catch (Exception ex)
            //    {
            //        iRet = -1;
            //        msgErr = ex.Message;
            //        transaccionsaved = null;
            //        LOG.Error("AdminBpTx.Create (w/List Conx)", ex);
            //    }

            //    return iRet;
            //}

            public static BpTxConx Retrieve(int id, out string msgErr)
            {
                msgErr = "S/C";
                BpTxConx transaccion;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();

                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        transaccion = (BpTxConx)sess.Load(typeof(BpTxConx), id);
                    }
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    transaccion = null;
                    LOG.Error("AdminBpTxConx.Retrieve", ex);
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
                return transaccion;

            }

            public static IList RetrieveTxByTrackid(string trackid, out string msgErr)
            {
                msgErr = "S/C";
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();

                try
                {
                    IList list;
                    using (ISession sess = PersistentManager.session())
                    {
                        list = sess.CreateCriteria(typeof(BpTxConx)).
                            Add(Expression.Eq("AdminBpTxConx", trackid.Trim()))
                            .List();
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpTxConx.RetrieveTxByTrackid", ex);
                    return null;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
            }

            

            public static IList ListAll
            {
           
                get
                {
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                    IList list;
                    using (ISession sess = PersistentManager.session())
                    {
                        list = sess.CreateCriteria(typeof(BpTxConx)).List();
                    }
                    return list;
                }
            }

            

            public static bool Write(BpTxConx bptx, out BpTxConx bptxsaved, out string msgErr)
            {
                msgErr = "S/C";
                bptxsaved = null;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                //ITransaction tx = sess.BeginTransaction();
                try
                {
                    //sess.SaveOrUpdate(bptx);
                    //bptxsaved = (BpTx)
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.SaveOrUpdate(bptx);
                        bptxsaved = bptx;
                    }
                    //  tx.Commit();
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    //if (tx != null) tx.Rollback();
                    LOG.Error("AdminBpTxConx.Write", ex);
                    return false;
                }
                //finally
                //{
                //    if (sess != null) sess.Close();
                //}
                return true;
            }

            public static bool Update(BpTxConx bptx, out string msgErr)
            {
                msgErr = "S/C";
                bool bRet = false;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                //ITransaction tx = sess.BeginTransaction();
                try
                {
                    //sess.SaveOrUpdate(bptx);
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.Update(bptx);
                        sess.Flush();
                        bRet = true;
                        //  tx.Commit();
                    }
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    //if (tx != null) tx.Rollback();
                    LOG.Error("AdminBpTxConx.Update", ex);
                    bRet = false;
                }
                //finally
                //{
                //    if (sess != null) sess.Close();
                //}
                return bRet;
            }

        


            public static string RetrieveTrackId3ro(string idtrack, out string msgErr)
            {
                msgErr = "S/C";
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                IList list;
                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        list = sess.CreateCriteria(typeof(BpTxConx)).
                            Add(Expression.Eq("Trackid", idtrack)).List();
                    }
                    if (list != null && list.Count > 0) return ((BpTxConx)list[0]).Trackid3ro;
                    else return null;
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpTxConx.RetrieveTrackId3ro (by idtrack)", ex);
                    return null;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
            }

            public static string RetrieveTrackIdFromTrackid3ro(string idtrack3ro, out string msgErr)
            {
                msgErr = "S/C";
                IList list;
                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        list = sess.CreateCriteria(typeof(BpTxConx)).
                            Add(Expression.Eq("Trackid3ro", idtrack3ro)).List();
                    }
                    if (list != null && list.Count > 0) return ((BpTxConx)list[0]).Trackid;
                    else return null;
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpTxConx.RetrieveTrackIdFromTrackid3ro (by idtrack3ro)", ex);
                    return null;
                }
            }


        #endregion staticMethods

    }


}
