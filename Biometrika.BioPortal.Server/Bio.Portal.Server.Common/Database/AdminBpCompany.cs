﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Constant;
using NHibernate.Criterion;
using NHibernate;
using System.Collections;
using log4net;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Common.Entities
{
    public class AdminBpCompany
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpIdentity));
        #region Static Methods
        /// <summary>
        /// Método que lista todas aquellas companías activas.
        /// </summary>
        /// <returns>retorna una lista de objetos BpCompany</returns>
        public static IList<BpCompany> GetCompanyActive(out string msgErr)
        {
            msgErr = "";
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            IList<BpCompany> companys = null;
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    ICriteria crit = sess.CreateCriteria(typeof(BpCompany)).
                                    Add(Expression.IsNull("Endate"));

                    companys = crit.List<BpCompany>();
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("AdminBpIdentity.Retrieve (typeid,valueid)", ex);
                companys = null;
            }
            /*finally
            {
                if (sess != null && sess.IsOpen) sess.Close();
            }*/
            return companys;
        }
        /// <summary>
        /// Método que trae todas las compañía según filtro
        /// </summary>
        /// <param name="estado"></param>
        /// <param name="msgErr"></param>
        /// <returns></returns>
        public static IList<BpCompany> GetCompanys(int estado, out string msgErr)
        {
            msgErr = "";
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            IList<BpCompany> companys = null;
            ICriteria crit = null;
            try
            {
                using (ISession session = PersistentManager.session())
                {
                    if (estado == 0)
                        crit = session.CreateCriteria(typeof(BpCompany));
                    else if (estado == 1)
                        crit = session.CreateCriteria(typeof(BpCompany)).
                                        Add(Expression.IsNull("Endate"));
                    else
                        crit = session.CreateCriteria(typeof(BpCompany)).
                                        Add(Expression.IsNotNull("Endate"));

                    companys = crit.List<BpCompany>();
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("AdminBpCompany.Retrieve (typeid,valueid)", ex);
                companys = null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return companys;
        }

        public static BpCompany RetrieveAuthCredential(string accessname, out string msgErr)
        {
            msgErr = "S/C";
            BpCompany company;
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    company = PersistentManager.session().CreateCriteria(typeof(BpCompany))
                                                  .Add(Expression.Eq("AccessName", accessname))
                                                  .UniqueResult<BpCompany>();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpCompany.RetrieveAuthCredential (accessname)", ex);
                msgErr = ex.Message;
                company = null;
            }
            return company;
        }

        /// <summary>
        /// Trae las compañías según criterios asociados
        /// </summary>
        /// <param name="rut">rut de la compañia sin puntos ni dígito verificador</param>
        /// <param name="razonsocial">razón social de la compañía</param>
        /// <param name="estado">estado, dependiendo si se ven todas las compañías o no.</param>
        /// <param name="msgErr"></param>
        /// <returns></returns>
        public static IList<BpCompany> GetCompanys(string rut, string razonsocial, int estado, out string msgErr)
        {
            msgErr = "";
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            IList<BpCompany> companys = null;
            ICriteria crit = null;
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    if ((estado == 0) || (estado == -1))
                        crit = sess.CreateCriteria(typeof(BpCompany));
                    else if (estado == 1)
                        crit = sess.CreateCriteria(typeof(BpCompany)).
                                        Add(Expression.IsNull("Endate"));
                    else if (estado == 2)
                        crit = sess.CreateCriteria(typeof(BpCompany)).
                                        Add(Expression.IsNotNull("Endate"));
                    if (rut != String.Empty)
                        crit.Add(Expression.Eq("Rut", rut));
                    if (razonsocial != String.Empty)
                        crit.Add(Expression.Eq("Name", razonsocial));
                    companys = crit.List<BpCompany>();
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("AdminBpIdentity.Retrieve (typeid,valueid)", ex);
                companys = null;
            }
            /*finally
            {
                if (sess != null && sess.IsOpen) sess.Close();
            }*/
            return companys;
        }

        public static IList<BpCompany> GetCompanyActive(string domain, out string msgErr)
        {
            msgErr = "";
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session(); 
            IList<BpCompany> companys = null;
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    ICriteria crit = sess.CreateCriteria(typeof(BpCompany)).
                                    Add(Expression.IsNull("Endate"));

                    //Esto quiere decir que es necesario traer sólo esa compañía activa al listado.
                    //Esta condición se utiliza para los objectdatasource de los usuarios.
                    if (domain != String.Empty)
                    {
                        crit.Add(Expression.Eq("Domain", domain));
                    }

                    companys = crit.List<BpCompany>();
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("AdminBpIdentity.Retrieve (typeid,valueid)", ex);
                companys = null;
            }
            /*finally
            {
                if (sess != null && sess.IsOpen) sess.Close();
            }*/
            return companys;
        }
        /// <summary>
        /// Método que retorna una companía
        /// </summary>
        /// <param name="id">Identificador interno de la compañía</param>
        /// <param name="msgErr">mensaje de error en caso de que exista</param>
        /// <returns></returns>
        /// 
        public static BpCompany GetCompanyById(int id, out string msgErr)
        {

            return AdminBpCompany.Retrieve(id, out msgErr);

        }
        /// <summary>
        /// Método que permite retornar una compañía por dominio.
        /// </summary>
        /// <param name="domain">dominio de la compañía, sin espacios</param>
        /// <param name="msgErr">mensaje de error en caso de que exista</param>
        /// <returns></returns>
        public static BpCompany GetCompanyByDomain(string domain, out string msgErr)
        {

            return AdminBpCompany.Retrieve(domain, out msgErr);

        }
        /// <summary>
        /// Método que permite agregar una nueva compañía.
        /// Como condición para su creación ni el rut y/o el dominio pueden existir asociados a otra empresa.
        /// </summary>
        /// <param name="rut">rut de la compañía obligatorio</param>
        /// <param name="address">dirección de la compañía obligatorio</param>
        /// <param name="name">nombre de la compañía obligatorio</param>
        /// <param name="phone">telefono de la compañía obligatorio</param>
        /// <param name="domain">dominio de la compañía obligatorio</param>
        /// <param name="phone2">teléfono adicional de la compañía</param>
        /// <param name="fax">fax de la compañía</param>
        /// <param name="contactname">Nombre de contacto de la compañía</param>
        /// <param name="status">Estado, 0 Activa , 1 Inactiva</param>
        /// <param name="msgErr">Devuelve mensaje de error en caso de existir</param>
        /// <returns></returns>
        public static int Create(string rut, string address, string name, string phone, string domain,
                                 string phone2, string fax, string contactname, int status, out string msgErr)
        {
            msgErr = "S/C";
            int result = 0;
            BpCompany company;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                company = Retrieve(rut, domain, out msgErr);
                if (company == null)
                {
                    company = new BpCompany();
                    company.Rut = rut;
                    company.Address = address;
                    company.Name = name;
                    company.Phone = phone;
                    company.Domain = domain;
                    company.Phone2 = phone2;
                    company.Fax = fax;
                    company.Contactname = contactname;
                    company.Status = status;
                    company.Startdate = DateTime.Now;
                    company.Laststatusdate = null;
                    company.Lastmodifydate = null;
                    company.Endate = null;
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.Save(company);
                        sess.Flush();
                    }

                }
                else
                {
                    result = -1;
                }

            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.Create (BpIdentity)", ex);
                msgErr = ex.Message;
                result = -1;
                return result;
            }
            /*finally
            {
                if (sess != null && sess.IsOpen) sess.Close();
            }*/
            return result;
        }
        /// <summary>
        /// Método que permite actualizar una compañia existente.
        /// Debe considerarse que no puede existir el dominio antes de modificar.
        /// </summary>
        /// <param name="id">identificador interno</param>
        /// <param name="rut">rut de la compañía obligatorio</param>
        /// <param name="address">dirección de la compañía obligatorio</param>
        /// <param name="name">nombre de la compañía obligatorio</param>
        /// <param name="phone">telefono de la compañía obligatorio</param>
        /// <param name="domain">dominio de la compañía obligatorio</param>
        /// <param name="phone2">teléfono adicional de la compañía</param>
        /// <param name="fax">fax de la compañía</param>
        /// <param name="contactname">Nombre de contacto de la compañía</param>
        /// <param name="status">Estado, 0 Activa , 1 Inactiva</param>
        /// <param name="msgErr">Devuelve mensaje de error en caso de existir</param>
        /// <returns></returns>

        public static int Modify(int id, string rut, string address, string name, string phone, string domain,
                                 string phone2, string fax, string contactname, int status, out string msgErr)
        {
            msgErr = "S/C";
            int result = 0;
            BpCompany company;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                company = Retrieve(id, domain, out msgErr);
                if (company != null)
                {

                    company.Rut = rut;
                    company.Address = address;
                    company.Name = name;
                    company.Phone = phone;
                    company.Domain = domain;
                    company.Phone2 = phone2;
                    company.Fax = fax;
                    company.Contactname = contactname;
                    company.Lastmodifydate = DateTime.Now;

                    if (status == 1)
                    {
                        company.Status = status;
                        company.Laststatusdate = DateTime.Now;
                        company.Endate = DateTime.Now;
                    }
                    else
                    {
                        company.Status = status;
                        company.Laststatusdate = DateTime.Now;
                        company.Endate = null;
                    }
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.Update(company);
                        sess.Flush();
                    }
                }
                else
                {
                    result = -1;
                }

            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpCompany.Modify (BpCompany)", ex);
                msgErr = ex.Message;
                result = -1;
                return result;
            }
            /*finally
            {
                if (sess != null && sess.IsOpen) sess.Close();
            }*/
            return result;
        }

        public static int Delete(int id, out string msgErr)
        {
            msgErr = "S/C";
            int result = 0;
            BpCompany company;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                company = Retrieve(id, out msgErr);
                if (company != null)
                {

                    company.Status = 1;
                    company.Endate = DateTime.Now;
                    company.Laststatusdate = DateTime.Now;
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.Update(company);
                        sess.Flush();
                    }
                }
                else
                {
                    result = -1;
                }

            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpCompany.Delete (id)", ex);
                msgErr = ex.Message;
                result = -1;
                return result;
            }
            /*finally
            {
                if (sess != null && sess.IsOpen) sess.Close();
            }*/
            return result;
        }

        public static BpCompany Retrieve(int id, out string msgErr)
        {
            msgErr = "S/C";
            BpCompany company;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    company = sess.CreateCriteria(typeof(BpCompany))
                                                  .Add(Expression.Eq("Id", id))
                                                  .UniqueResult<BpCompany>();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpCompany.Retrieve (id)", ex);
                msgErr = ex.Message;
                company = null;
            }
            /*finally
            {
                if (sess != null && sess.IsOpen) sess.Close();
            }*/
            return company;

        }
        public static BpCompany Retrieve(string domain, out string msgErr)
        {
            msgErr = "S/C";
            BpCompany company;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    company = PersistentManager.session().CreateCriteria(typeof(BpCompany))
                                                  .Add(Expression.Eq("Domain", domain))
                                                  .UniqueResult<BpCompany>();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpCompany.Retrieve (domain)", ex);
                msgErr = ex.Message;
                company = null;
            }
            /*finally
            {
                if (sess != null && sess.IsOpen) sess.Close();
            }*/
            return company;

        }
        public static BpCompany Retrieve(string rut, string domain, out string msgErr)
        {
            msgErr = "S/C";
            BpCompany company;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    company = sess.CreateCriteria(typeof(BpCompany))
                                                  .Add(Expression.Or(Expression.Eq("Domain", domain), Expression.Eq("rut", rut)))
                                                  .UniqueResult<BpCompany>();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpCompany.Retrieve (rut,domain)", ex);
                msgErr = ex.Message;
                company = null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return company;
        }
        public static BpCompany Retrieve(int id, string domain, out string msgErr)
        {
            msgErr = "S/C";
            BpCompany company;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    company = sess.CreateCriteria(typeof(BpCompany))
                                                  .Add(Expression.Or(Expression.Eq("Domain", domain), Expression.Eq("Id", id)))
                                                  .UniqueResult<BpCompany>();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpCompany.Retrieve (id,domain)", ex);
                msgErr = ex.Message;
                company = null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return company;
        }



        public static int GetCompanyIdByIdLegal(string companyLegalId, out string msgErr)
        {
            int ret = -1;
            msgErr = "S/C";
            BpCompany company;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    company = sess.CreateCriteria(typeof(BpCompany))
                                                  .Add(Expression.Eq("Rut", companyLegalId))
                                                  .UniqueResult<BpCompany>();
                }
                if (company == null) ret = Errors.IERR_COMPANY_NOT_EXIST;
                else ret = company.Id;
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpCompany.GetCompanyIdByIdLegal (companyLegalId,msgErr)", ex);
                msgErr = ex.Message;
                company = null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return ret;

        }

        #endregion Static Methods
    }
}
