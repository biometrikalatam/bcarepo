﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Api;
using log4net;
using NHibernate;
using System.IO;
using System.Xml;
using System.Drawing;
using Bio.Core.Imaging;
using NHibernate.Criterion;
using System.Collections;
using System.Data;
using BioPortal.Server.Api;
using Bio.Core.Constant;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Common.Entities
{
    public class AdminBpIdentity
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpIdentity));


        #region Static Methods
        /// <summary>
        /// Agrega un BIR a una identidad
        /// </summary>
        /// <param name="bpi">Identidad</param>
        /// <param name="bir">BIR</param>
        /// <param name="msgErr">Descripcion de error si existe</param>
        public static void AddBIR(BpIdentity bpi, BpBir bir, out string msgErr)
        {
            msgErr = "S/C";
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                if (bir.BpIdentity == null)
                {
                    bir.BpIdentity = bpi;
                }
                bpi.BpBir.Add(bir);
                using (ISession sess = PersistentManager.session())
                {
                    sess.SaveOrUpdate(bir);
                    sess.Flush();
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("AdminBpIdentity.AddBIR", ex);
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
        }


        /// <summary>
        /// Setea un valor dinamico para extender el modelo de BD
        /// </summary>
        /// <param name="key">Clave</param>
        /// <param name="value">Valor</param>
        /// <param name="msgErr">Si existe error</param>
        public static void SetDynamicData(BpIdentity bpi, string key, string value, out string msgErr)
        {
            msgErr = "S/C";
            StringReader sr = null;
            XmlTextReader rdr;
            XmlDocument dom;

            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                //Si esta vacio creo el XML
                if (bpi.Dynamicdata == null)
                {
                    sr = new StringReader("<dynamicdata></dynamicdata>");
                }
                else
                {
                    sr = new StringReader(bpi.Dynamicdata);
                }
                rdr = new XmlTextReader(sr);

                dom = new XmlDocument();
                dom.Load(rdr);

                XmlNode root = dom.DocumentElement;
                XmlNode nodo = root.SelectSingleNode("descendant::data[key='" + key.Trim() + "']");

                if (nodo == null)
                {
                    //AddAttribute(key, value, out msgErr);
                    //Crea nodo
                    XmlNode newNodeAtributo = dom.CreateNode(XmlNodeType.Element, "data", null);

                    XmlNode newNodeKey = dom.CreateNode(XmlNodeType.Element, "key", null);
                    newNodeKey.InnerText = key.Trim();
                    newNodeAtributo.AppendChild(newNodeKey);

                    XmlNode newNodeValue = dom.CreateNode(XmlNodeType.Element, "value", null);
                    newNodeValue.InnerText = value.Trim();
                    newNodeAtributo.AppendChild(newNodeValue);

                    root.AppendChild(newNodeAtributo);
                }
                else
                {
                    //Update Nodo
                    nodo.LastChild.InnerText = value.Trim();
                    bpi.Dynamicdata = dom.InnerXml;
                }
                bpi.Dynamicdata = dom.InnerXml;
                using (ISession sess = PersistentManager.session())
                {
                    sess.SaveOrUpdate(bpi);
                    sess.Flush();
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("AdminBpIdentity.SetDynamicData", ex);
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}

        }


        /// <summary>
        /// Obtiene un valor dinamico desde el XML de Dynamicdata
        /// </summary>
        /// <param name="bpi">Objeto BpIdentity</param>
        /// <param name="key">Clave</param>
        /// <param name="msgErr">descripcion de Error si existe</param>
        /// <returns>Valor del dato dinamico</returns>
        public static string GetDynamicData(BpIdentity bpi, string key, out string msgErr)
        {
            msgErr = "S/C";
            string retValue = null;
            StringReader sr = null;
            XmlTextReader rdr;
            XmlDocument dom;

            try
            {
                if (bpi.Dynamicdata != null)
                {
                    sr = new StringReader(bpi.Dynamicdata);
                    rdr = new XmlTextReader(sr);

                    dom = new XmlDocument();
                    dom.Load(rdr);

                    XmlNode root = dom.DocumentElement;
                    XmlNode nodo = root.SelectSingleNode("descendant::data[key='" + key.Trim() + "']");

                    retValue = nodo.LastChild.InnerText;
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("AdminBpIdentity.GetDynamicData", ex);
            }
            return retValue;
        }

        /// <summary>
        /// Dada una identidad, si tiene foto devuelve un objeto Image 
        /// con esa foto.
        /// </summary>
        /// <param name="bpi">Identidad</param>
        /// <returns>Imagen</returns>
        public static Image GetImageFoto(BpIdentity bpi)
        {
            if (bpi.Photography == null)
                return null;
            try
            {
                byte[] raw = Convert.FromBase64String(bpi.Photography);
                return ImageProcessor.ToImage(raw);
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.GetImageFoto", ex);
                return null;
            }
        }

        public static IList<BpBir> GetBIRs(BpIdentity bpi)
        {
            return bpi != null ? bpi.BpBir : null;
        }

        public static IList<BpBir> GetBIRs(BpIdentity bpi, int authenticationfactor, int minutiaetype)
        {
            return bpi != null ? bpi.BpBir : null;
        }

        public static BpIdentity Create(string nick, bool checkExists, out string msgErr)
        {
            msgErr = "S/C";
            BpIdentity identity;
            object obj;

            if (checkExists)
            {
                obj = AdminBpIdentity.Retrieve(nick, 1, out msgErr);

                if (obj != null)
                {
                    if (obj.GetType().Equals(typeof(BpIdentity)))
                    {
                        return (BpIdentity)obj;
                    }
                    else //Sino es lista => busco entre la lista
                    {
                        IList list = (IList)obj;
                        foreach (BpIdentity bpi in list)
                        {
                            if (bpi.Nick.Trim().Equals(nick.Trim()))
                            {
                                return bpi;
                            } //Si sale de aca, es que no existe => se crea
                        }
                    }
                }
            }
            identity = new BpIdentity(nick);
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    sess.Save(identity);
                    sess.Flush();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.Create (nick)", ex);
                msgErr = ex.Message;
                identity = null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return identity;
        }

        public static int Create(BpIdentity bpi, bool checkExists,
                                 out BpIdentity identitycreated, out string msgErr)
        {
            msgErr = "S/C";
            identitycreated = null;
            int iErr = Errors.IERR_OK;

            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                if (checkExists)
                {
                    if (bpi.Id > 0)
                    {
                        iErr = AdminBpIdentity.Retrieve(bpi.Id, out identitycreated, out msgErr);
                    }
                    else
                    {
                        iErr = AdminBpIdentity.Retrieve(bpi.Companyidenroll.Id, bpi.Typeid, bpi.Valueid,
                                                        out identitycreated, out msgErr);
                    }
                }
                using (ISession sess = PersistentManager.session())
                {
                    sess.Save(bpi);
                    sess.Flush();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.Create (BpIdentity)", ex);
                msgErr = ex.Message;
                iErr = Errors.IERR_UNKNOWN;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return iErr;
        }

        public static int Create(int companyidenroll, string typeid, string valueid,
                                 bool checkExists, out BpIdentity identity, out string msgErr)
        {
            msgErr = "S/C";
            identity = null;
            int iErr = Errors.IERR_OK;
            BpCompany company = AdminBpCompany.GetCompanyById(companyidenroll, out msgErr);
            if (checkExists)
            {

                /*iErr = AdminBpIdentity.Retrieve(companyidenroll, typeid, valueid,
                                                out identity, out msgErr);*/
                iErr = AdminBpIdentity.Retrieve(company.Id, typeid, valueid,
                                                out identity, out msgErr);
            }
            else
            {
                //identity = new BpIdentity(companyidenroll, typeid.Trim(), valueid.Trim());
                identity = new BpIdentity(company, typeid.Trim(), valueid.Trim());
            }
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    sess.Save(identity);
                    sess.Flush();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.Create (typeid, valueid)", ex);
                msgErr = ex.Message;
                iErr = Errors.IERR_UNKNOWN;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return iErr;
        }


        public static BpIdentity Update(BpIdentity bpi, out string msgErr)
        {
            msgErr = "S/C";
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    sess.Update(bpi);
                    sess.Flush();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.Update (BpIdentity)", ex);
                msgErr = ex.Message;
                bpi = null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return bpi;
        }

        public static bool Delete(BpIdentity bpi, out string msgErr)
        {
            msgErr = "S/C";
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    sess.Delete(bpi);
                    sess.Flush();
                }
                return true;
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.Delete (BpIdentity)", ex);
                msgErr = ex.Message;
                return false;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
        }

        public static bool Delete(int id, out string msgErr)
        {
            msgErr = "S/C";
            BpIdentity bpi;

            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    bpi = sess.CreateCriteria(typeof(BpIdentity)).
                        Add(Restrictions.Eq("Id", id)).UniqueResult<BpIdentity>();
                }
                if (bpi != null)
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.Delete(bpi);
                        sess.Flush();
                    }
                    return true;
                }
                else
                {
                    throw new Exception("No se encontro Identity con id = " + id.ToString());
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.Delete (id)", ex);
                msgErr = ex.Message;
                return false;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
        }

        public static int Delete(int companyidenroll, string typeid, string valueid, out string msgErr)
        {
            int iErr = Errors.IERR_OK;
            msgErr = "S/C";
            BpIdentity ident = null;
            BpCompany company = null;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                company = AdminBpCompany.GetCompanyById(companyidenroll, out msgErr);

                //iErr = AdminBpIdentity.Retrieve(company.Id, typeid, valueid, out ident, out msgErr);
                using (ISession sess = PersistentManager.session())
                {
                    ident = sess.CreateCriteria(typeof(BpIdentity)).
                                Add(Restrictions.Eq("Companyidenroll", company)).
                                Add(Restrictions.Eq("Typeid", typeid)).
                                Add(Restrictions.Eq("Valueid", valueid)).
                                UniqueResult<BpIdentity>();
                }
                if (ident != null)
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.Delete(ident);
                        sess.Flush();
                    }
                }
                else
                {
                    iErr = Errors.IERR_IDENTITY_NOT_FOUND;
                }

            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.Delete (companyidenroll, typeid, valueid)", ex);
                msgErr = ex.Message;
                iErr = Errors.IERR_UNKNOWN;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return iErr;
        }

        public static BpIdentity Retrieve(int id, out string msgErr)
        {
            msgErr = "S/C";
            BpIdentity bpidentity = null;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    bpidentity = sess.CreateCriteria(typeof(BpIdentity))
                                                  .Add(Expression.Eq("Id", id))
                                                  .UniqueResult<BpIdentity>();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.Retrieve (id)", ex);
                msgErr = ex.Message;
                bpidentity = null;
            }
            return bpidentity;
        }

        public static int Retrieve(int id, out BpIdentity identity, out string msgErr)
        {
            msgErr = "S/C";
            identity = null;
            int iErr;

            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    identity = (BpIdentity)sess.Load(typeof(BpIdentity), id);
                }
                iErr = (identity == null) ? Errors.IERR_IDENTITY_NOT_FOUND : Errors.IERR_OK;
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.Retrieve (id)", ex);
                msgErr = ex.Message;
                iErr = Errors.IERR_UNKNOWN;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return iErr;

        }

        /// <summary>
        /// Retorna una idendad o todas dependiendo del flag.
        /// </summary>
        /// <param name="nick">Nick a buscar</param>
        /// <param name="flag">1-First | 2-Last | default-All</param>
        /// <param name="msgErr">Descripcion del error si existe</param>
        /// <returns>Retorna un objeto BpIdentity o una lista segun flag</returns>
        public static object Retrieve(string nick, int flag, out string msgErr)
        {
            msgErr = "S/C";
            object identity = null;
            IList l;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    ICriteria crit = sess.CreateCriteria(typeof(BpIdentity)).
                                    Add(Expression.Eq("Nick", nick));
                    switch (flag)
                    {
                        case 1: //First
                            crit.AddOrder(new Order("Creation", true));
                            break;
                        case 2: //Last
                            crit.AddOrder(new Order("Creation", false));
                            break;
                        default: //All
                            break;
                    }
                    l = crit.List();
                }
                if (l == null || l.Count == 0)
                    identity = null;
                else
                {
                    if (flag != 1 && flag != 2)
                    {
                        identity = l;
                    }
                    else
                    {
                        IEnumerator en = l.GetEnumerator();
                        en.MoveNext();
                        identity = (BpIdentity)en.Current;
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.Retrieve (nick,flag)", ex);
                msgErr = ex.Message;
                identity = null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return identity;
        }

        public static int Retrieve(int companyidenroll, string typeid, string valueid,
                                   out BpIdentity identity, out string msgErr, bool withBirs = false)
        {
            msgErr = "S/C";
            identity = null;
            int ret = Errors.IERR_OK;
            BpCompany company = null;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                company = AdminBpCompany.GetCompanyById(companyidenroll, out msgErr);
                IList l;
                using (ISession sess = PersistentManager.session())
                {
                    l = sess.CreateCriteria(typeof(BpIdentity)).
                        Add(Restrictions.Eq("Companyidenroll", company)).
                        Add(Expression.Eq("Typeid", typeid)).
                        Add(Expression.Eq("Valueid", valueid))
                        .List();
                    if (withBirs)
                    {
                        foreach (BpIdentity item in l)
                        {
                            NHibernateUtil.Initialize(item.BpBir);  //Si es con birs, se agregan
                            NHibernateUtil.Initialize(item.BpIdentity3ro); //Cargo los id de 3ros //Added 07/2020
                        }
                        foreach (BpIdentity item in l)
                        {
                            NHibernateUtil.Initialize(item.BpIdentityDynamicdata);  //Si es con additionaldata, se agregan
                        }
                    }
                }
                if (l == null || l.Count == 0)
                    ret = Errors.IERR_IDENTITY_NOT_FOUND;
                else
                {
                    IEnumerator en = l.GetEnumerator();
                    en.MoveNext();
                    identity = (BpIdentity)en.Current;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.Retrieve (typeid,valueid)", ex);
                msgErr = ex.Message;
                identity = null;
                ret = Errors.IERR_UNKNOWN;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return ret;
        }

        public static IList ListAll
        {
            get
            {
                IList l = null;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        l = sess.CreateCriteria(typeof(BpIdentity)).List();
                    }
                }
                catch (Exception ex)
                {
                    LOG.Error("AdminBpIdentity.ListAll", ex);
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
                return l;
            }
        }

        public static DataView GetAllIdentitiesDataView(DictionaryEntry[] columns)
        {
            IList l = null;
            DataView dw = null;

            try
            {
                l = AdminBpIdentity.ListAll;

                dw = HPersistent.GetDataView(typeof(BpIdentity), l, columns);
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.ListAll", ex);
            }
            return dw;
        }

        public static IList GetIdentitiesByFiltro(int companyidenroll,
                                                  string typeid, string valueid,
                                                  string name, string lastname)
        {
            IList l = null;
            string msg;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            BpCompany oCompany = null;
            try
            {
                if (companyidenroll > 0)
                {
                    oCompany = AdminBpCompany.GetCompanyById(companyidenroll, out msg);
                }
                using (ISession sess = PersistentManager.session())
                {
                    ICriteria crit = sess.CreateCriteria(typeof(BpIdentity));

                    if (companyidenroll > 0)
                    {
                        crit.Add(Restrictions.Eq("Companyidenroll", oCompany));
                    }

                    if (typeid != null && typeid.Trim().Length > 0)
                    {
                        crit.Add(Expression.Eq("Typeid", typeid));
                    }

                    if (valueid != null && valueid.Trim().Length > 0)
                    {
                        crit.Add(Expression.Eq("Valueid", valueid));
                    }

                    if (name != null && name.Trim().Length > 0)
                    {
                        crit.Add(Expression.InsensitiveLike("Name", name));
                    }

                    if (lastname != null && lastname.Trim().Length > 0)
                    {
                        crit.Add(Expression.Or(Expression.InsensitiveLike("Patherlastname", lastname),
                                               Expression.InsensitiveLike("Motherlastname", lastname)));

                    }

                    l = crit.List();
                }
                //Elimino los Birs para que no salgan de la plataforma
                foreach (BpIdentity identity in l)
                {
                    identity.BpBir = null;
                }

            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.ListAll", ex);
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return l;
        }

        public static bool Write(BpIdentity data, out string msgErr)
        {
            msgErr = "S/C";
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            //ITransaction tx = null;

            try
            {
                LOG.Debug("AdminBpIdentity.Write IN...");
                using (ISession sess = PersistentManager.session())
                {
                    
                    LOG.Debug("AdminBpIdentity.Write Ingresando a sess.SaveOrUpdate(data)...");
                    //tx = sess.BeginTransaction();
                    sess.SaveOrUpdate(data);
                    sess.Flush();
                    //tx.Commit();
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                //if (tx != null) tx.Rollback();
                LOG.Error("AdminBpIdentity.Write Error", ex);
                return false;
            }
            //finally
            //{
            //    if (sess != null) sess.Close();
            //    tx = null;
            //}
            LOG.Debug("AdminBpIdentity.Write Saliendo OK!");
            return true;
        }

        public static int FillIdentity(int companyid, PersonalData pdata, out BpIdentity identitycreated, out string msg)
        {
            msg = "S/C";
            identitycreated = null;
            int ret = Errors.IERR_OK;

            try
            {
                //string ddata = XmlUtils.SerializeObject(pdata.Dynamicdata);
                //Se tendrá que armar de nuevo
                identitycreated = new BpIdentity(AdminBpCompany.GetCompanyById(companyid, out msg), pdata.Typeid,
                                                 pdata.Valueid);
                identitycreated.Nick = pdata.Nick;
                identitycreated.Name = pdata.Name;
                identitycreated.Patherlastname = pdata.Patherlastname;
                identitycreated.Motherlastname = pdata.Motherlastname;
                identitycreated.Sex = pdata.Sex;
                identitycreated.Documentseriesnumber = pdata.Documentseriesnumber;
                identitycreated.Documentexpirationdate = pdata.Documentexpirationdate.Year < 1960
                                                    ? DateTime.Parse("01/01/1900")
                                                    : pdata.Documentexpirationdate;
                identitycreated.Visatype = pdata.Visatype;
                identitycreated.Birthdate = pdata.Birthdate.Year < 1960
                                                    ? DateTime.Parse("01/01/1900")
                                                    : pdata.Birthdate;
                identitycreated.Birthplace = pdata.Birthplace;
                identitycreated.Nationality = pdata.Nationality;
                identitycreated.Photography = pdata.Photography;
                identitycreated.DocImageFront = pdata.DocImageFront;
                identitycreated.DocImageBack = pdata.DocImageBack;
                identitycreated.Selfie = pdata.Selfie;
                identitycreated.Signatureimage = pdata.Signatureimage;
                identitycreated.Profession = pdata.Profession;

                //ADDED 11-2014 - Separado a BD
                //identitycreated.Dynamicdata = ddata;
                BpIdentityDynamicdata bpiDD;
                if (pdata.Dynamicdata != null && pdata.Dynamicdata.Length > 0)
                {
                    identitycreated.BpIdentityDynamicdata = new List<BpIdentityDynamicdata>();
                    foreach (DynamicDataItem item in pdata.Dynamicdata)
                    {
                        bpiDD = new BpIdentityDynamicdata(identitycreated, item.key, item.value);
                        identitycreated.BpIdentityDynamicdata.Add(bpiDD);
                    }
                }

                identitycreated.Enrollinfo = pdata.Enrollinfo;
                identitycreated.Creation = DateTime.Now;
                identitycreated.Useridenroll = pdata.Useridenroll;

                //identitycreated.Verificationsource = pdata.Verificationsource;


                /*pdata.Nick, pdata.Typeid, pdata.Valueid,
                    pdata.Name, pdata.Patherlastname, pdata.Motherlastname, pdata.Sex,
                    pdata.Documentseriesnumber, pdata.Documentexpirationdate, pdata.Visatype,
                    pdata.Birthdate, pdata.Birthplace, pdata.Nationality, pdata.Photography, 
                    pdata.Signatureimage, pdata.Profession, ddata, pdata.Enrollinfo,
                    pdata.Creation, pdata.Verificationsource, 
                    pdata.Companyidenroll == 0 ? companyid : pdata.Companyidenroll,
                    pdata.Useridenroll); // == 0 ? pdata.Useridenroll : pdata.Useridenroll);*/
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.FillIdentity", ex);
                msg = ex.Message;
                identitycreated = null;
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }


        public static int MergeIdentity(PersonalData pdata, BpIdentity oDBBpIdentity,
                                        out BpIdentity oNewBpIdentity, out string msg)
        {
            msg = "S/C";
            int ret = Errors.IERR_OK;
            oNewBpIdentity = oDBBpIdentity;

            try
            {
                LOG.Debug("AdminBpIdentity.MergeIdentity IN...");
                if (oDBBpIdentity == null)
                {
                    LOG.Debug("AdminBpIdentity.MergeIdentity oDBBpIdentity == null => Sale con IERR_IDENTITY_NOT_FOUND");
                    return Errors.IERR_IDENTITY_NOT_FOUND;
                }
                oNewBpIdentity.Nick = pdata.Nick ?? oDBBpIdentity.Nick;

                oNewBpIdentity.Verificationsource = (!string.IsNullOrEmpty(oDBBpIdentity.Verificationsource))? oDBBpIdentity.Verificationsource:pdata.Verificationsource;
                LOG.Debug("AdminBpIdentity.MergeIdentity Verificationsource = " +
                    (string.IsNullOrEmpty(oNewBpIdentity.Verificationsource)?"NULL": oNewBpIdentity.Verificationsource));
                oNewBpIdentity.Name = pdata.Name ?? oNewBpIdentity.Name;
                oNewBpIdentity.Patherlastname = pdata.Patherlastname ?? oNewBpIdentity.Patherlastname;
                oNewBpIdentity.Motherlastname = pdata.Motherlastname ?? oNewBpIdentity.Motherlastname;
                oNewBpIdentity.Sex = pdata.Sex ?? oNewBpIdentity.Sex;
                oNewBpIdentity.Documentseriesnumber = pdata.Documentseriesnumber ?? oNewBpIdentity.Documentseriesnumber;
                oNewBpIdentity.Documentexpirationdate = pdata.Documentexpirationdate.Year < 1960
                                                        ? oNewBpIdentity.Documentexpirationdate
                                                        : pdata.Documentexpirationdate;
                oNewBpIdentity.Visatype = pdata.Visatype ?? oNewBpIdentity.Visatype;
                oNewBpIdentity.Birthdate = pdata.Birthdate.Year < 1850
                                           ? oNewBpIdentity.Birthdate
                                           : pdata.Birthdate;
                oNewBpIdentity.Birthplace = pdata.Birthplace ?? oNewBpIdentity.Birthplace;
                oNewBpIdentity.Nationality = pdata.Nationality ?? oNewBpIdentity.Nationality;
                oNewBpIdentity.Photography = String.IsNullOrEmpty(pdata.Photography) ?
                    oNewBpIdentity.Photography : pdata.Photography;
                oNewBpIdentity.Signatureimage = String.IsNullOrEmpty(pdata.Signatureimage) ?
                    oNewBpIdentity.Signatureimage : pdata.Signatureimage;
                oNewBpIdentity.Selfie = String.IsNullOrEmpty(pdata.Selfie) ?
                    oNewBpIdentity.Selfie : pdata.Selfie;
                oNewBpIdentity.DocImageFront = String.IsNullOrEmpty(pdata.DocImageFront) ?
                    oNewBpIdentity.DocImageFront : pdata.DocImageFront;
                oNewBpIdentity.DocImageBack = String.IsNullOrEmpty(pdata.DocImageBack) ?
                                    oNewBpIdentity.DocImageBack : pdata.DocImageBack;
                oNewBpIdentity.Profession = pdata.Profession ?? oNewBpIdentity.Profession;
                //Si hay DinamicData = debo mergear con la nueva
                //string ddataaux = oNewBpIdentity.Dynamicdata;
                //if (!String.IsNullOrEmpty(ddataaux) && ddataaux.IndexOf("<ArrayOfDynamicDataItem") > 0)
                //    ddataaux = ddataaux.Substring(ddataaux.IndexOf("<ArrayOfDynamicDataItem"));
                //List<DynamicDataItem> dd = XmlUtils.DeserializeObject<List<DynamicDataItem>>(ddataaux);
                //if (dd == null && (pdata.Dynamicdata != null && pdata.Dynamicdata.Length > 0))
                //{
                //    dd = new List<DynamicDataItem>();
                //}
                //if (pdata.Dynamicdata != null)
                //{
                //    foreach (DynamicDataItem ddi in pdata.Dynamicdata)
                //    {
                //        dd.Add(ddi);
                //    }
                //}
                //string ddata = XmlUtils.SerializeObject(dd);
                //ddata = ddata.Replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
                //oNewBpIdentity.Dynamicdata = ddata;

                //ADDED 11-2014 - Separado a BD
                BpIdentityDynamicdata bpiDD;
                LOG.Debug("AdminBpIdentity.MergeIdentity Ingresa a procesar Dynamicdata...");
                LOG.Debug("AdminBpIdentity.MergeIdentity (pdata.Dynamicdata != null)=>" + (pdata.Dynamicdata != null).ToString());
                LOG.Debug("AdminBpIdentity.MergeIdentity pdata.Dynamicdata.Length => " +
                                ((pdata.Dynamicdata!=null) ? pdata.Dynamicdata.Length.ToString() : "null"));
                try
                {
                    if (pdata.Dynamicdata != null && pdata.Dynamicdata.Length > 0)
                    {
                        if (oDBBpIdentity.BpIdentityDynamicdata == null)
                        {
                            oNewBpIdentity.BpIdentityDynamicdata = new List<BpIdentityDynamicdata>();
                        }
                        else
                        {
                            oNewBpIdentity.BpIdentityDynamicdata = oDBBpIdentity.BpIdentityDynamicdata;
                        }
                        foreach (DynamicDataItem item in pdata.Dynamicdata)
                        {
                            if (item != null)
                            {
                                if (item.key == null || item.value == null)
                                {
                                    LOG.Debug("AdminBpIdentity.MergeIdentity NO Agrega key = " +
                                        (!string.IsNullOrEmpty(item.key) ? item.key : "key nulo") + " - value = " +
                                        (!string.IsNullOrEmpty(item.value) ? item.value : "value nulo"));
                                }
                                else
                                {
                                    if (!ExisteKey(oNewBpIdentity.BpIdentityDynamicdata, item.key, item.value))
                                    {
                                        LOG.Debug("AdminBpIdentity.MergeIdentity Agrega key = " +
                                            (!string.IsNullOrEmpty(item.key) ? item.key : "key nulo") + " - value = " +
                                            (!string.IsNullOrEmpty(item.value) ? item.value : "value nulo"));
                                        bpiDD = new BpIdentityDynamicdata(oNewBpIdentity, item.key, item.value);
                                        oNewBpIdentity.BpIdentityDynamicdata.Add(bpiDD);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LOG.Error("AdminBpIdentity.MergeIdentity - Error procesando los Dynamic Data [" + ex.Message + "]");
                }
                
                oNewBpIdentity.Enrollinfo = pdata.Enrollinfo;
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.MergeIdentity", ex);
                msg = ex.Message;
                oNewBpIdentity = null;
                ret = Errors.IERR_UNKNOWN;
            }
            LOG.Debug("AdminBpIdentity.MergeIdentity OUT!");
            return ret;
        }

        private static bool ExisteKey(IList<BpIdentityDynamicdata> list, string key, string value)
        {
            bool ret = false;
            try
            {
                foreach (BpIdentityDynamicdata item in list)
                {
                    if (item.DDKey.Trim().Equals(key.Trim()))
                    {
                        item.DDValue = value;
                        item.LastUpdate = DateTime.Now;
                        ret = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity.ExisteKey", ex);
                ret = false;
            }
            return ret;
        }

        //Added 082018 - Para enroll temporales
        //Valores ejemplos
        //   Verificationsource = 2,1,A,SRCeI|4,0,A        => Finger verificado automatico en Registro Civil y facial no verificado
        //                        2,1,A,CI|4,1,M,Usuario1   => Finger verificado automatico contra cedula de identidad y facial manual por Usuario1
        //internal static bool IsIdentityValidated(BpIdentity identity, out Hashtable htVerified)
        //{
        //    bool ret = false;
        //    htVerified = new Hashtable();
        //    VerificationSource objVS;
        //    try
        //    {
        //        if (identity != null && identity.Verificationsource != null)
        //        {
        //            string[] allVS = identity.Verificationsource.Split('|');

        //            if (allVS == null || allVS.Length == 0)
        //            {
        //                htVerified = null;
        //            }
        //            else
        //            {
        //                foreach (var item in allVS)
        //                {
        //                    string[] itemVS = item.Split(',');
        //                    if (itemVS != null && itemVS.Length >= 3)
        //                    {
        //                        objVS = new VerificationSource();
        //                        objVS.AuthenticationFactor = Convert.ToInt32(itemVS[0]);
        //                        objVS.StatusVerified = Convert.ToInt32(itemVS[1]);
        //                        objVS.TypeVerified = itemVS[2];
        //                        if (itemVS.Length > 3)
        //                        {
        //                            objVS.SourceVerified = itemVS[3];
        //                        }
        //                        htVerified.Add(objVS.AuthenticationFactor, objVS);
        //                        if (objVS.StatusVerified == 1)
        //                        {
        //                            ret = true;   //Si al menos uno de todos los AF es 1 => devuelvo true que esta verificado, pero devuelvo 
        //                                          //igual la lista de cada AF con su estado.
        //                        }
        //                    }
        //                }
        //            }
        //        } else
        //        {
        //            htVerified = null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("AdminBpIdentity.IsIdentityValidated", ex);
        //        ret = false;
        //        htVerified = null;
        //    }
        //    return ret;
        //}

        //internal static bool IsVerified(Hashtable htVS, int authenticationfactor)
        //{
        //    bool ret = false;
        //    try
        //    {
        //        if (htVS.ContainsKey(authenticationfactor)) {
        //            VerificationSource vs = (VerificationSource)htVS[authenticationfactor];
        //            ret = (vs.StatusVerified == 1);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("AdminBpIdentity.IsVerified", ex);
        //        ret = false;
        //    }
        //    return ret;
        //}


        #endregion Static Methods
    }

    //public class VerificationSource
    //{
    //    public int AuthenticationFactor;
    //    public int StatusVerified;  //0- No verificado | 1-Verificado
    //    public string TypeVerified; //A-Automatico | M-Manual
    //    public string SourceVerified; //Verificador => CI - Cedula de identidad | SRCeI2018 - Registro Civul, etc. 
    //}
}
