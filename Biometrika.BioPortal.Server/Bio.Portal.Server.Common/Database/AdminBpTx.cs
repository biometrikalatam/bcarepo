﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Matcher.Constant;
using log4net;
using NHibernate;
using System.Collections;
using System.Data;
using System.Globalization;
using NHibernate.Criterion;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Common.Entities
{
    [Serializable]
    public class AdminBpTx
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpTx));

#region staticMethods
        
        public static BpTx Create(string trackid, int operationcode, string typeid, string valueid, int result, double score,
                            double threshold, DateTime timestampstart, DateTime timestampend, int authenticatorfactor, 
                            int minutiaetype, int bodypart, int actiontype, BpOrigin bporigin, DateTime? timestampclient, 
                            string clientid, string ipenduser, string enduser, string dynamicdata, int companyidtx, 
                            int useridtx, int operationsource, string abs, out string msgErr)
            {
                BpTx transaccion;

                msgErr = "S/C";
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();

                try
                {
                    transaccion = new BpTx(trackid, operationcode, typeid, valueid, result, score, 
                            threshold, timestampstart, timestampend, authenticatorfactor, 
                            minutiaetype, bodypart, actiontype, bporigin, timestampclient,
                            clientid, ipenduser, enduser, dynamicdata, companyidtx, useridtx, operationsource, abs);
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.Save(transaccion);
                    }
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    transaccion = null;
                    LOG.Error("AdminBpTx.Create", ex);
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
                return transaccion;

            }

            //public static BpTx Create(string trackid, string typeid, string valueid, int result, double score,
            //                double threshold, DateTime timestampstart, DateTime timestampend, int authenticatorfactor, 
            //                int minutiaetype, int bodypart, int actiontype, BpOrigin bporigin, DateTime? timestampclient, 
            //                string clientid, string ipenduser, string enduser, string dynamicdata, int companyidtx, 
            //                int useridtx, BpTxConx conx, out string msgErr)
            //{
            //    msgErr = "S/C";
            //    BpTx transaccion;
            //    ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //    ITransaction tx = sess.BeginTransaction();
            //    try
            //    {
            //        transaccion = new BpTx(trackid, typeid, valueid, result, score,
            //                threshold, timestampstart, timestampend, authenticatorfactor,
            //                minutiaetype, bodypart, actiontype, bporigin, timestampclient,
            //                clientid, ipenduser, enduser, dynamicdata, companyidtx, useridtx);
            //        sess.Save(transaccion);

            //        if (conx != null)
            //        {
            //            conx.BpTx = transaccion;
            //            AdminBpTx.AddTxConx(transaccion, conx, out msgErr);
            //        }
            //        tx.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        msgErr = ex.Message;
            //        tx.Rollback();
            //        transaccion = null;
            //        LOG.Error("AdminBpTx.Create (w/Conx) ", ex);
            //    }
            //    finally
            //    {
            //        if (sess != null && sess.IsOpen) sess.Close();
            //        if (tx != null) tx = null;
            //    }
            //    return transaccion;

            //}


            public static BpTx Create(string trackid, int operationcode, string typeid, string valueid, int result, double score,
                            double threshold, DateTime timestampstart, DateTime timestampend, int authenticationfactor,
                            int minutiaetype, int bodypart, int actiontype, BpOrigin bporigin, DateTime? timestampclient,
                            string clientid, string ipenduser, string enduser, string dynamicdata, int companyidtx,
                            int useridtx, int operationsource, string abs, IList conxList, out string msgErr)
            {
                msgErr = "S/C";
                BpTx transaccion;
                BpTx transaccionsaved = null;

                try
                {
                    transaccion = new BpTx(trackid, operationcode, typeid, valueid, result, score,
                            threshold, timestampstart, timestampend, authenticationfactor,
                            minutiaetype, bodypart, actiontype, bporigin, timestampclient,
                            clientid, ipenduser, enduser, dynamicdata, companyidtx, useridtx, operationsource, abs);

                    if (conxList != null)
                    {
                        foreach (BpTxConx conxAux in conxList)
                        {
                            //conxAux.BpTx = transaccion;
                            AdminBpTx.AddTxConx(transaccion, conxAux, out msgErr);
                        }
                    }
                    if (!AdminBpTx.Write(transaccion, out transaccionsaved, out msgErr))
                    {
                        transaccionsaved = null;
                    }
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    transaccionsaved = null;
                    LOG.Error("AdminBpTx.Create (w/List Conx)", ex);
                }

                return transaccionsaved;

            }

            public static int CreateTx(int actiontype, int operationcode,  
                                       int company, string typeid, string valueid, string taxidcompany, string trackid, int consultationtype,
                                       string customertrackid, string callbackurl, string redirecturl, string successurl, int signerinclude, 
                                       int checklock, int onboardingmandatory, string theme, float threshold, 
                                       string new_trackid3ro, int authenticationfactor, int minutiaetype, int bodypart, 
                                       string url3ro, string session, string titlebpweb, string messagebpweb,
                                       int autorizationinclude, string autorizationmessage, string jsonWorkflow,
                                       int videoinclude, string videomessage, int obtype,
                                       int carregisterinclude, int writinginclude, string forminclude,
                                       out string msgErr)
            {
                msgErr = "S/C";
                BpTx transaccion;
                BpTx transaccionsaved = null;
                int iRet = 0;
                try
                {
                    BpOrigin bporigin = AdminBpOrigin.Retrieve(45, out msgErr);  //new BpOrigin(45, "BioPortal Web");
                    if (bporigin == null) bporigin = AdminBpOrigin.Retrieve(1, out msgErr);
                    int operationsource = 0;
                    switch (consultationtype)
                    {
                        case 1:
                            operationsource = 1;
                            break;
                        case 2:
                            operationsource = 2;
                            break;
                        case 3:
                            operationsource = 3;
                            break;
                        default:
                            operationsource = 3;
                            break;
                    }

                    transaccion = new BpTx(trackid, operationcode, typeid, valueid, 0, 0,
                            threshold, DateTime.Now, DateTime.Now, authenticationfactor,
                            minutiaetype, bodypart, actiontype, bporigin, null,
                            null, null, null, null, company, 0, operationsource, null);
                    transaccion.CheckLock = checklock;
                    transaccion.TitleBpWeb = titlebpweb;
                    transaccion.MessageBpWeb = messagebpweb;
                    transaccion.TaxIdCompany = taxidcompany;

                    BpTxConx conxAux = new BpTxConx();
                    conxAux.Connectorid = consultationtype.ToString();
                    conxAux.Consultationtype = consultationtype.ToString();
                    conxAux.CallbackUrl = callbackurl;
                    conxAux.RedirectUrl = redirecturl;
                    conxAux.SuccessUrl = successurl;
                    conxAux.CustomerTrackId = customertrackid;
                    conxAux.OnboardingMandatory = onboardingmandatory;
                    conxAux.OnboardingType = obtype;
                
                    conxAux.SignerInclude = signerinclude;
                    //conxAux.CheckLock = checklock;
                    conxAux.Theme = theme;
                    conxAux.Threshold = threshold;
                    conxAux.Timestamp = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    conxAux.Trackid = trackid;
                    conxAux.Trackid3ro = new_trackid3ro;
                    conxAux.Status = 0;
                    conxAux.Result = 0;
                    conxAux.Score = 0;
                    conxAux.AutorizationInclude = autorizationinclude;
                    conxAux.AutorizationMessage = autorizationmessage;
                    conxAux.Workflow = jsonWorkflow;
                    conxAux.VideoInclude = videoinclude;
                    conxAux.VideoMessage = videomessage;
                    //conxAux.TypeVerify = consultationtype;
                    //conxAux.Url3ro = url3ro;
                    //Por facetec
                    conxAux.Session = session;

                    conxAux.CarRegisterImageInclude = carregisterinclude;
                    conxAux.WritingImageInclude = writinginclude;
                    conxAux.FormInclude = forminclude;
                    //Si es Facetec y es Auth => Tomo el FaceMap de la persona desde bp_Identity3ro
                    if (operationsource == 2 && onboardingmandatory == 0)
                    {
                        conxAux.Sample3roTarget = url3ro;
                        conxAux.EnrollmentIdentifier = company.ToString() + "_" + typeid + "_" + valueid;
                        //string strFaceMap = null;
                        //string msgerr = null;
                        //int retG = AdminBpIdentity3ro.GetFaceMap(company, typeid, valueid, consultationtype, out strFaceMap, out msgerr);
                        //if (retG < 0)
                        //{
                        //    LOG.Warn("AdminBpTx.CreateTx - Error recuperando FaceMap desde bpIdentity3ro o no existe [" +
                        //         (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]");
                        //} else {
                        //    conxAux.Sample3roTarget = strFaceMap;
                        //}
                    } else
                    {
                        conxAux.Url3ro = url3ro;
                    }

                    //conxAux.BpTx = transaccion;
                    transaccionsaved = AdminBpTx.AddTxConx(transaccion, conxAux, out msgErr);
                    if (!AdminBpTx.Write(transaccion, out transaccionsaved, out msgErr))
                    {
                        transaccionsaved = null;
                    }
                    iRet = 0;
                }
                catch (Exception ex)
                {
                    iRet = -1;
                    msgErr = ex.Message;
                    transaccionsaved = null;
                    LOG.Error("AdminBpTx.Create (w/List Conx)", ex);
                }

                return iRet;
            }

        public static int CreateTx(int actiontype, int operationcode,
                                   int company, string typeid, string valueid, string taxidcompany, string trackid, float threshold,
                                   int authenticationfactor, int minutiaetype, int bodypart, string clientid, 
                                   out string msgErr)
        {
            msgErr = "S/C";
            BpTx transaccion;
            BpTx transaccionsaved = null;
            int iRet = 0;
            try
            {
                BpOrigin bporigin = AdminBpOrigin.Retrieve(61, out msgErr);  //new BpOrigin(45, "POSController");
                if (bporigin == null) bporigin = AdminBpOrigin.Retrieve(1, out msgErr);
                
                transaccion = new BpTx(trackid, operationcode, typeid, valueid, 0, 0,
                        threshold, DateTime.Now, DateTime.Now, authenticationfactor,
                        minutiaetype, bodypart, actiontype, bporigin, null,
                        null, null, null, null, company, 0, 0, null);
                transaccion.Threshold = threshold;
                transaccion.Clientid = clientid;

                //conxAux.BpTx = transaccion;
                transaccionsaved = AdminBpTx.AddTxConx(transaccion, null, out msgErr);
                if (!AdminBpTx.Write(transaccion, out transaccionsaved, out msgErr))
                {
                    transaccionsaved = null;
                }
                iRet = 0;
            }
            catch (Exception ex)
            {
                iRet = -1;
                msgErr = ex.Message;
                transaccionsaved = null;
                LOG.Error("AdminBpTx.Create (for POSController)", ex);
            }

            return iRet;
        }



        public static BpTx Retrieve(int id, out string msgErr)
            {
                msgErr = "S/C";
                BpTx transaccion;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();

                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        transaccion = (BpTx)sess.Load(typeof(BpTx), id);
                    }
                }
                catch (Exception ex)
                {   
                    msgErr = ex.Message;
                    transaccion = null;
                    LOG.Error("AdminBpTx.Retrieve", ex);
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
                return transaccion;

            }

            public static IList RetrieveTxByTrackid(string trackid, out string msgErr)
            {
                msgErr = "S/C";
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();

                try
                {
                    IList list;
                    using (ISession sess = PersistentManager.session())
                    {
                        list = sess.CreateCriteria(typeof(BpTx)).
                            Add(Expression.Eq("Trackid", trackid.Trim()))
                            .List();
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpTx.RetrieveTxByTrackid", ex);
                    return null;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
            }

            public static BpTx RetrieveTxByTrackid(string trackid, out string msgErr, bool withChild = false)
            {
                msgErr = "S/C";
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                BpTx tx = null;
                IList list = null;
                try
                {
            
                    using (ISession sess = PersistentManager.session())
                    {
                        list = sess.CreateCriteria(typeof(BpTx)).
                            Add(Expression.Eq("Trackid", trackid.Trim()))
                            .List();
                        if (list != null && list.Count > 0)
                        {
                            tx = (BpTx)list[0];
                            if (withChild)
                            {
                                NHibernateUtil.Initialize(tx.BpTxConx);
                                NHibernateUtil.Initialize(tx.BpOrigin);
                                NHibernateUtil.Initialize(tx.BpVerified);
                            }
                        }
                    }
                    
                    return tx;
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpTx.RetrieveTxByTrackid", ex);
                    return null;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
            }

            public static IList Retrieve(string typeid, string valueid,
                                         out string msgErr)
            {
                msgErr = "S/C";
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                IList list;
                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        list = sess.CreateCriteria(typeof(BpTx)).
                            Add(Expression.Eq("Typeid", typeid)).
                            Add(Expression.Eq("Valueid", valueid))
                            .List();
                    }
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpTx.Retrieve (by typeid,valueid)", ex);
                    return null;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
                return list;
            }

            public static IList ListAll
            {
           
                get
                {
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                    IList list;
                    using (ISession sess = PersistentManager.session())
                    {
                        list = sess.CreateCriteria(typeof(BpTx)).List();
                    }
                    return list;
                }
            }

            /// <summary>
            /// Formatea la salida de una consulta en un DataView para alimentar una Grilla
            /// </summary>
            /// <param name="columns">Arreglo con los nombres de las columnas</param>
            /// <param name="msgErr">Descripcion de error si existe</param>
            /// <returns></returns>
            public static DataView GetAllTxDataView(DictionaryEntry[] columns, out string msgErr)
            {
                msgErr = "S/C";
                IList l = null;
                DataView dw = null;

                try
                {
                    l = AdminBpTx.ListAll;

                    dw = HPersistent.GetDataView(typeof(BpTx), l, columns);
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpTx.GetAllTxDataView", ex);
                }
                return dw;
            }

 
            public static DataView GetAllTxWithFilterToDataView(int id, string trackid, int origin, 
                                    string typeid, string valueid, int actiontype,
                                    string idconx, int statusconx, int resultconx, 
                                    string timestamprcdesde, string timestamprchasta,
                                    int companyid, string connectorid,
                                    out string msgErr)
            {
                msgErr = "S/C";
                IList l = null;
                DataView dw = null;
                BpOrigin ori = null;

                //ISession sess = null;
            try
                {
                    LOG.Debug("GetAllTxWithFilterToDataView - In...");
                    //sess = HSessionFactory._sessionFactory.OpenSession();
                    //sess = PersistentManager.session();

                    if (origin > 0)
                    {
                        string msg;
                        ori = AdminBpOrigin.Retrieve(origin, out msg);
                        if (ori == null)
                        {
                            LOG.Warn("Error obteniendo Origen con id = " + origin.ToString() + "[" + msg + "]");
                        }
                    }

                    using (ISession sess = PersistentManager.session())
                    {
                        //Aplico filtros para BpTx
                        ICriteria crit = sess.CreateCriteria(typeof(BpTx));

                        LOG.Debug("GetAllTxWithFilterToDataView - Id = " + id.ToString());
                        if (id > 0)
                        {
                            crit.Add(Expression.Eq("Id", id));
                            LOG.Debug("GetAllTxWithFilterToDataView - Id crit.Added");
                        }

                        LOG.Debug("GetAllTxWithFilterToDataView - trackid = " + trackid);
                        if (trackid != null && trackid.Trim().Length > 0)
                        {
                            crit.Add(Expression.Eq("Trackid", trackid.Trim()));
                            LOG.Debug("GetAllTxWithFilterToDataView - trackid crit.Added");
                        }

                        LOG.Debug("GetAllTxWithFilterToDataView - origin = " + origin.ToString());
                        if (origin > 0)
                        {
                            if (ori != null)
                            {
                                crit.Add(Expression.Eq("BpOrigin", ori));
                                LOG.Debug("GetAllTxWithFilterToDataView - BpOrigin crit.Added");
                            }
                            //else
                            //{
                            //    LOG.Warn("Error obteniendo Origen con id = " + origin.ToString() + "[" + msg + "]");
                            //}
                        }

                        LOG.Debug("GetAllTxWithFilterToDataView - typeid = " + typeid);
                        if (typeid != null && typeid.Trim().Length > 0)
                        {
                            crit.Add(Expression.Eq("Typeid", typeid));
                            LOG.Debug("GetAllTxWithFilterToDataView - Typeid crit.Added");
                        }

                        LOG.Debug("GetAllTxWithFilterToDataView - valueid = " + valueid);
                        if (valueid != null && valueid.Trim().Length > 0)
                        {
                            crit.Add(Expression.Eq("Valueid", valueid));
                            LOG.Debug("GetAllTxWithFilterToDataView - valueid crit.Added");
                        }

                        LOG.Debug("GetAllTxWithFilterToDataView - actiontype = " + actiontype.ToString());
                        if (actiontype > 0)
                        {
                            crit.Add(Expression.Eq("Actiontype", actiontype));
                            LOG.Debug("GetAllTxWithFilterToDataView - actiontype crit.Added");
                        }

                        LOG.Debug("GetAllTxWithFilterToDataView - companyid = " + companyid.ToString());
                        if (companyid > 0)
                        {
                            crit.Add(Expression.Eq("Companyidtx", companyid));
                            LOG.Debug("GetAllTxWithFilterToDataView - Companyidtx crit.Added");
                            //string msg;
                            //BpCompany comp = AdminBpCompany.Retrieve(companyid, out msg);
                            //if (comp != null)
                            //{
                            //    crit.Add(Expression.Eq("BpCompany", comp));
                            //}
                            //else
                            //{
                            //    LOG.Warn("Error obteniendo Company con id = " + companyid.ToString() + "[" + msg + "]");
                            //}
                        }

                        LOG.Debug("GetAllTxWithFilterToDataView - timestamprcdesde = " + timestamprcdesde);
                        if (!String.IsNullOrEmpty(timestamprcdesde))
                        {
                            DateTime dtDesde;
                            try
                            {
                                CultureInfo ci = new CultureInfo("es-CL");
                                ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
                                ci.DateTimeFormat.ShortTimePattern = "HH:mm:ss";
                                dtDesde = DateTime.Parse(timestamprcdesde, ci);
                            }
                            catch (Exception)
                            {
                                dtDesde = new DateTime(1900, 1, 1);
                            }
                            crit.Add(Expression.Ge("Timestampstart", dtDesde));
                            LOG.Debug("GetAllTxWithFilterToDataView - Timestampstart crit.Added <= [" +
                                dtDesde.ToString("dd/MM/yyyy"));
                        }

                        LOG.Debug("GetAllTxWithFilterToDataView - timestamprchasta = " + timestamprchasta);
                        if (!String.IsNullOrEmpty(timestamprchasta))
                        {
                            DateTime dtHasta;
                            try
                            {
                                CultureInfo ci = new CultureInfo("es-CL");
                                ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
                                ci.DateTimeFormat.ShortTimePattern = "HH:mm:ss";
                                dtHasta = DateTime.Parse(timestamprchasta, ci);
                            }
                            catch (Exception)
                            {
                                dtHasta = new DateTime(1900, 1, 1);
                            }
                            crit.Add(Expression.Le("Timestampstart", dtHasta));
                            LOG.Debug("GetAllTxWithFilterToDataView - Timestampstart crit.Added >= [" +
                                dtHasta.ToString("dd/MM/yyyy"));
                        }

                        l = crit.AddOrder(Order.Desc("Id")).List();
                        LOG.Debug("GetAllTxWithFilterToDataView - l.count = " + l.Count.ToString());
                    }

                        //Creo DataView para responder
                        DataRow dr;
                        DataRow drConx;
                        DataTable dt = new DataTable();

                        //Agrego columnas
                        dt.Columns.Add(new DataColumn("#"));
                        dt.Columns.Add(new DataColumn("TrackId BP"));
                        dt.Columns.Add(new DataColumn("Identidad"));
                        dt.Columns.Add(new DataColumn("Cod"));
                        //dt.Columns.Add(new DataColumn("Tipo Id"));
                        //dt.Columns.Add(new DataColumn("Valor Id"));

                        dt.Columns.Add(new DataColumn("Start"));
                        dt.Columns.Add(new DataColumn("End"));

                        dt.Columns.Add(new DataColumn("Resultado"));
                        dt.Columns.Add(new DataColumn("Origen"));
                        dt.Columns.Add(new DataColumn("Usuario"));
                        dt.Columns.Add(new DataColumn("IP"));

                        dt.Columns.Add(new DataColumn("Algoritmo"));
                        dt.Columns.Add(new DataColumn("Accion"));

                        dt.Columns.Add(new DataColumn("Conx.Id"));
                        dt.Columns.Add(new DataColumn("Fecha"));
                        dt.Columns.Add(new DataColumn("TrackId"));
                        dt.Columns.Add(new DataColumn("Estado"));
                        dt.Columns.Add(new DataColumn("Result"));
                        dt.Columns.Add(new DataColumn("Score"));
                        dt.Columns.Add(new DataColumn("Compañia"));

                        //Luego, filtro las listas de RC de cada BpTx
                        LOG.Debug("GetAllTxWithFilterToDataView - foreach tx in...");
                        foreach (BpTx tx in l)
                        {

                            if (CumpleFiltro(tx, idconx, statusconx, resultconx,
                                             timestamprcdesde, timestamprchasta, 
                                             connectorid))
                            {
                                dr = dt.NewRow();

                                dr["#"] = tx.Id;
                                dr["TrackId BP"] = tx.Trackid;
                                dr["Identidad"] = tx.Typeid + " " + tx.Valueid;
                                dr["Cod"] = tx.Operationcode;
                                //dr["Tipo Id"] = tx.Typeid;
                                //dr["Valor Id"] = tx.Valueid;
                            
                                dr["Start"] = tx.Timestampstart.ToString("dd/MM/yyyy hh:mm:ss.fff");
                                dr["End"] = tx.Timestampend.ToString("dd/MM/yyyy hh:mm:ss.fff");

                                switch (tx.Result)
                                {
                                    case 1: 
                                        dr["Resultado"] = "Positivo";
                                        break;
                                    case 2:
                                        dr["Resultado"] = "Negativo";
                                        break;
                                    default:
                                        dr["Resultado"] = "";
                                        break;
                                }
                                dr["Origen"] = tx.BpOrigin.Description;
                                dr["Usuario"] = tx.Enduser;
                                dr["IP"] = tx.Ipenduser;

                                dr["Algoritmo"] = MinutiaeType.GetDescription(tx.Minutiaetype);   
                                dr["Accion"] = Bio.Core.Matcher.Constant.Action.GetName(tx.Actiontype);

                                //Cargo el score por si es una validacion local. Si
                                //es de validacion contra fuente externa se sobreescribe mas abajo.
                                dr["Score"] = tx.Score;
                                dr["Compañia"] = (AdminBpCompany.GetCompanyById(tx.Companyidtx, out msgErr)).Name;
                            
                                IList<BpTxConx> lconx = tx.BpTxConx;
                           
                                bool isFirst = true;
                                foreach (BpTxConx txconx in lconx)
                                {
                                    if (isFirst)
                                    {
                                        dr["Accion"] = Bio.Core.Matcher.Constant.Action.GetName(tx.Actiontype); //txconx.Consultationtype;
                                        dr["Conx.Id"] = txconx.Connectorid;
                                        dr["Fecha"] = txconx.Timestamp;
                                        dr["TrackId"] = txconx.Trackid;
                                        dr["Estado"] = txconx.Status;
                                        switch (txconx.Result)
                                        {
                                            case 1:
                                                dr["Result"] = "Positivo";
                                                break;
                                            case 2:
                                                dr["Result"] = "Negativo";
                                                break;
                                            default:
                                                dr["Result"] = "";
                                                break;
                                        }
                                        dr["Score"] = txconx.Score;
                                        //Agrego fila completa para que este primera
                                        dt.Rows.Add(dr);
                                        //Para cortar el ciclo
                                        isFirst = false;
                                    }
                                    else
                                    {

                                        //Esta solo tiene datos de tx del RC
                                        //dado que la primera parte queda vacia
                                        //porque es igual a la primera
                                        drConx = dt.NewRow();
                                        drConx["#"] = "";
                                        drConx["TrackId BP"] = "";
                                        drConx["Identidad"] = "";
                                        drConx["Cod"] = "";
                                        //drConx["Tipo Id"] = "";
                                        //drConx["Valor Id"] = "";
                                        drConx["Start"] = "";
                                        drConx["End"] = "";
                                        drConx["Resultado"] = "";
                                        drConx["Origen"] = "";
                                        drConx["Usuario"] = "";
                                        drConx["IP"] = "";
                                        drConx["Algoritmo"] = "";

                                        drConx["Accion"] = Bio.Core.Matcher.Constant.Action.GetName(tx.Actiontype); //txconx.Consultationtype;
                                        drConx["Conx.Id"] = txconx.Connectorid;
                                        drConx["Fecha"] = txconx.Timestamp;
                                        drConx["TrackId"] = txconx.Trackid;
                                        drConx["Estado"] = txconx.Status;
                                        drConx["Result"] = txconx.Result;
                                        drConx["Score"] = txconx.Score;
                                        drConx["Compañia"] = "";
                                        //Agrego fila con solo datos del RC	
                                        dt.Rows.Add(drConx);
                                    }
                                }
                                if (isFirst)
                                {
                                    //Agrego fila completa en caso que no tenga trx de fuente externa
                                    //porque si es asi, no entro en el for anterior y no inserto la fila completa
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                        LOG.Debug("GetAllTxWithFilterToDataView - foreach tx out!");
                        //Creo el DataView
                    
                        dw = new DataView(dt);
                        LOG.Debug("GetAllTxWithFilterToDataView - dw = new DataView(dt)");
                }
                catch (Exception ex)
                {
                    dw = null;
                    msgErr = ex.Message;
                    LOG.Error("AdminBpTx.GetAllTxWithFilterToDataView", ex);
                }
                /*finally
                {
                    if (sess != null) sess.Close();
                }*/
                return dw;
            }

            private static bool CumpleFiltro(BpTx tx,
                                    string idconx, int statusconx, int resultconx, 
                                    string timestamprcdesde, string timestamprchasta,
                                    string connectorid)
            {
                bool bRes = true;

                try
                {
                    IList<BpTxConx> lconx = tx.BpTxConx;
                     //(timestamprcdesde == null || timestamprcdesde.Trim().Length == 0) &&
                     //   (timestamprchasta == null || timestamprchasta.Trim().Length == 0) &&
                    if ((idconx == null || idconx.Trim().Length == 0) &&
                        (statusconx == -1) && (resultconx == 0) &&
                        (connectorid == null || connectorid.Trim().Length == 0))
                    {
                        return bRes;
                    }

                    if (lconx == null || lconx.Count == 0) return false;

                    foreach (BpTxConx txconx in lconx)
                    {
                        if (idconx != null && idconx.Trim().Length > 0)
                        {
                            bRes = txconx.Trackid.Trim().Equals(idconx.Trim());
                        }
                        if (bRes && statusconx != -1)
                        {
                            bRes = bRes && txconx.Status == statusconx;
                        }
                        if (bRes && resultconx > 0)
                        {
                            bRes = bRes && txconx.Result == resultconx;
                        }
                        if (bRes && (connectorid != null && connectorid.Trim().Length > 0))
                        {
                            bRes = bRes && txconx.Connectorid.Trim().Equals(connectorid);
                        }
                        //if (bRes && ((timestamprcdesde != null && timestamprcdesde.Trim().Length > 0) 
                        //         || (timestamprchasta != null && timestamprchasta.Trim().Length > 0)))
                        //{
                        //    bRes = bRes &&
                        //            CumpleFiltroFecha(txconx.Timestamp,
                        //            timestamprcdesde, timestamprchasta);
                        //    //CumpleFiltroFecha(tx.Timestampstart,
                        //    //                      timestamprcdesde, timestamprchasta);
                        //}
                    }
                }
                catch (Exception ex)
                {
                    LOG.Error("BpTx.CumpleFiltro Error", ex);
                    bRes = false;
                }
                return bRes;
            }

            private static bool CumpleFiltroFecha(string timestamp,
                                                  string desde, string hasta)
            {
                DateTime dtD = new DateTime();
                DateTime dtH = new DateTime();
                DateTime dtTS = new DateTime();

                CultureInfo ci = new CultureInfo("es-CL");
                ci.DateTimeFormat.ShortDatePattern = "yyyy/MM/dd";
                ci.DateTimeFormat.ShortTimePattern = "HH:mm:ss";

                try
                {
                    //Si no son nulas las dos, parseo por cada una
                    if (desde.Trim().Length > 0)
                    {
                        try
                        {
                            dtD = DateTime.Parse(desde, ci);
                        }
                        catch (Exception de)
                        {
                            LOG.Warn("AdminBpTx.CumpleFiltroFecha error", de);
                            return false;
                        }
                    }
                    //Si no son nulas las dos, parseo por cada una
                    if (hasta.Trim().Length > 0)
                    {
                        try
                        {
                            dtH = DateTime.Parse(hasta, ci);
                        }
                        catch (Exception de)
                        {
                            LOG.Warn("AdminBpTx.CumpleFiltroFecha error", de);
                            return false;
                        }
                    }
                    //Si no son nulas las dos, parseo por cada una
                    if (timestamp.Trim().Length > 0)
                    {
                        try
                        {
                            dtTS = DateTime.Parse(timestamp, ci);
                        }
                        catch (Exception ts)
                        {
                            LOG.Warn("AdminBpTx.CumpleFiltroFecha error", ts);
                            return false;
                        }
                    }

                    //Si parsearon bien las dos, veo que cumpla el filtro
                    if (dtD <= dtTS && dtTS <= dtH)
                    {
                        return true;
                    }
                    return false;
                }
                catch (Exception ex) {
                    LOG.Error("BpTx.CumpleFiltroFecha", ex);
                    return false;
                }
            }


            public static BpTx AddTxConx(BpTx tx, BpTxConx conx, out string msgErr)
            {
                msgErr = "S/C";
                BpTx oRes = null;
                try
                {
                    if (conx != null)
                    {
                        conx.BpTx = tx;
                        if (tx.BpTxConx == null) tx.BpTxConx = new List<BpTxConx>();
                        tx.BpTxConx.Add(conx);
                    }
                    oRes = tx;
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("BpTx.AddTxConx", ex);
                }
                return oRes;
            }

            public static bool Write(BpTx bptx, out BpTx bptxsaved, out string msgErr)
            {
                msgErr = "S/C";
                bptxsaved = null;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                //ITransaction tx = sess.BeginTransaction();
                try
                {
                    //sess.SaveOrUpdate(bptx);
                    //bptxsaved = (BpTx)
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.SaveOrUpdate(bptx);
                        bptxsaved = bptx;
                    }
                    //  tx.Commit();
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    //if (tx != null) tx.Rollback();
                    LOG.Error("BpTx.Write", ex);
                    return false;
                }
                //finally
                //{
                //    if (sess != null) sess.Close();
                //}
                return true;
            }

            public static bool Update(BpTx bptx, out string msgErr)
            {
                msgErr = "S/C";
                bool bRet = false;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                //ITransaction tx = sess.BeginTransaction();
                try
                {
                    //sess.SaveOrUpdate(bptx);
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.Update(bptx);
                        sess.Flush();
                        bRet = true;
                        //  tx.Commit();
                    }
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    //if (tx != null) tx.Rollback();
                    LOG.Error("BpTx.Update", ex);
                    bRet = false;
                }
                //finally
                //{
                //    if (sess != null) sess.Close();
                //}
                return bRet;
            }


            public static int GetQCurrentClient()
            {
                String msgErr;
                int res = 0;
                String strsql = "";
                string msgerr;

                //ISession sess = PersistentManager.session(); HSessionFactory._sessionFactory.OpenSession();
                try
                {
                //strsql = "SELECT COUNT(*) AS QCantidad " +
                //         "FROM (SELECT DISTINCT ipenduser " +
                //         "      FROM bp_tx) AS derivedtbl_1";

                //Added 08-2016 - Controla sobre bp_client
                //            Si está habilitada => Control cantidad 
                    using (ISession session = PersistentManager.session())
                    {
                        strsql = "SELECT COUNT(*) AS QCantidad " +
                                 "FROM (SELECT DISTINCT companyid, clientid " +
                                 "      FROM bp_client " +
                                 "      WHERE status = 1) AS derivedtbl_1";

                        IList<Cantidad> result = session.CreateSQLQuery(strsql)
                                                    .AddScalar("QCantidad", NHibernateUtil.Int32)
                                                .SetResultTransformer(new NHibernate.Transform.AliasToBeanResultTransformer(typeof(Cantidad)))
                                                .List<Cantidad>();
                        res = (result == null || result.Count == 0) ? 0 : result[0].QCantidad;
                    }
                }
                catch (Exception ex)
                {
                    LOG.Error("AdminBpTx.GetQCurrentClient", ex);
                    msgErr = ex.Message;
                    res = -1;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
                return res;
            }

            public static BpTx Retrieve(string idtrack, out string msgErr)
            {
                msgErr = "S/C";
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                IList list;
                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        list = sess.CreateCriteria(typeof(BpTx)).
                            Add(Expression.Eq("Trackid", idtrack)).List();
                    }
                    if (list != null && list.Count > 0) return (BpTx)list[0];
                    else return null;
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpTx.Retrieve (by typeid,valueid)", ex);
                    return null;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
            }

            //Added from 29-04-2016
            //Agregado para buscar Tx desde Proxy con ventana de tiempo hacia atrás y accion verificacion o identificación
            public static IList Retrieve(int companyid, string typeid, string valueid, int windows, 
                                 out string msgErr)
            {
                msgErr = "S/C";
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                IList list;
                try
                {
                    //Retorno los que son de ese CompanyId,TypeId,ValueId, pero que sean de verificacion o identificacion o Verify and Get,
                    // de menos de (Ahora - ventana de tiempo) y con result Positivo
                    using (ISession sess = PersistentManager.session())
                    {
                        list = sess.CreateCriteria(typeof(BpTx)).
                            Add(Expression.Eq("Companyidtx", companyid)).
                            Add(Expression.Eq("Typeid", typeid)).
                            Add(Expression.Eq("Valueid", valueid)).
                            Add(Expression.Eq("Operationcode", 0)).
                            Add(Expression.Eq("Result", 1)).
                            Add(Expression.IsNull("Consumed")).
                            Add(Expression.Or(Expression.Eq("Actiontype", 1), Expression.Or(Expression.Eq("Actiontype", 2), Expression.Eq("Actiontype", 9)))).
                            Add(Expression.Ge("Timestampend", (DateTime.Now.AddMilliseconds(windows * -1)))).
                            AddOrder(Order.Desc("Timestampend"))
                            .List();
                        //Add(Expression.IsNull("Consumed")).
                    }
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpTx.Retrieve (by typeid,valueid,windows)", ex);
                    return null;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
                return list;
            }

            //Added 07/2020
            /// <summary>
            /// Genera un id unico de TrackId, corroborando en BD que no exista uno repetido.
            /// </summary>
            /// <returns></returns>
            public static string GetTrackIdUnique()
            {
                BpTx nvUP;
                string retGuid = null;
                try
                {
                    bool isOK = false;
                    LOG.Debug("AdminBpTx.GetTrackIdUnique IN...");
                    while (!isOK)
                    {
                        retGuid = Guid.NewGuid().ToString("N");
                        LOG.Debug("AdminBpTx.GetTrackIdUnique - Chequeando TrackId = " + retGuid);
                        using (ISession session = PersistentManager.session())
                        {
                            nvUP = PersistentManager.session().CreateCriteria(typeof(BpTx))
                                             .Add(NHibernate.Criterion.Expression.Eq("Trackid", retGuid))
                                             .UniqueResult<BpTx>();
                        }
                        if (nvUP != null)
                        {
                            LOG.Debug("AdminBpTx.GetTrackIdUnique - Existe GUID => Se genera una nueva...");
                            nvUP = null;
                        }
                        else
                        {
                            LOG.Debug("AdminBpTx.GetTrackIdUnique GUID OK!");
                            isOK = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    retGuid = Guid.NewGuid().ToString("N"); ;
                    LOG.Error("AdminBpTx.GetTrackIdUnique Error", ex);
                }
                LOG.Debug("AdminBpTx.GetTrackIdUnique OUT!");
                return retGuid;
            }

            /// <summary>
            /// Recupera la Tx con sus hijos si withChild = true
            /// </summary>
            /// <param name="trackid"></param>
            /// <param name="msgErr"></param>
            /// <param name="withChild"></param>
            /// <returns></returns>
            public static int RetrieveTxByTrackid(string trackid, out BpTx txout, out string msgErr, bool withChild = false)
            {
                msgErr = "S/C";
                txout = null;
                int ret = 0;

                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        IList ltx = sess.CreateCriteria(typeof(BpTx)).
                            Add(Expression.Eq("Trackid", trackid.Trim()))
                            .List();

                        if (ltx != null && ltx.Count > 0)
                        {
                            txout = (BpTx)ltx[0];

                            if (withChild && txout != null) //Si es true => Cargo hijos
                            {
                                NHibernateUtil.Initialize(txout.BpTxConx);
                            }
                        }
                        else if (txout == null)
                        {
                            ret = -601; //IERR_TX_NOT_EXIST
                        }
                    }
                    return ret;
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpTx.RetrieveTxByTrackid", ex);
                    txout = null;
                    return ret = -1;
                }
            }

#endregion staticMethods

    }

    public class Cantidad
    {
        private int qcantidad;

        public int QCantidad
        {
            get
            {
                return qcantidad;
            }
            set
            {
                qcantidad = value;
            }
        }

    }
}
