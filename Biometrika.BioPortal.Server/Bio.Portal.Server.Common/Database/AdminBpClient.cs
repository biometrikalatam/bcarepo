﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Matcher.Constant;
using log4net;
using NHibernate;
using System.Collections;
using System.Data;
using System.Globalization;
using NHibernate.Criterion;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Common.Entities
{
    [Serializable]
    public class AdminBpClient
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpClient));


#region staticMethods
        
        public static BpClient Create(int companyid, string clientid, int status, out string msgErr)
        {
            BpClient client;

            msgErr = "S/C";
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();

            try
            {
                client = new BpClient(companyid, clientid, status);
                using (ISession sess = PersistentManager.session())
                {
                    sess.Save(client);
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                client = null;
                LOG.Error("AdminBpClient.Create", ex);
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return client;

        }



        public static BpClient Retrieve(int id, out string msgErr)
        {
            msgErr = "S/C";
            BpClient transaccion;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();

            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    transaccion = (BpClient)sess.Load(typeof(BpClient), id);
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                transaccion = null;
                LOG.Error("BpClient.Retrieve", ex);
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return transaccion;

        }

        public static IList RetrieveClientByCompany(int companyid, out string msgErr)
        {
            msgErr = "S/C";
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            IList list;
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    list = sess.CreateCriteria(typeof(BpClient)).
                    Add(Expression.Eq("Companyid", companyid))
                    .List();
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("BpClient.RetrieveClientByCompany", ex);
                return null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return list;
        }


        public static BpClient Retrieve(int companyid, string clientid, out string msgErr)
        {
            msgErr = "S/C";
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();

            try
            {
                IList l;
                using (ISession sess = PersistentManager.session())
                {
                    l = sess.CreateCriteria(typeof(BpClient)).
                    Add(Expression.Eq("Companyid", companyid)).
                    Add(Expression.Eq("Clientid", clientid))
                    .List();
                }
                if (l != null && l.Count > 0) return (BpClient)l[0];
                else return null;
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("BpClient.Retrieve (by companyid,clientid)", ex);
                return null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
        }

        public static IList ListAll
        {
            get
            {
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                IList l;
                using (ISession sess = PersistentManager.session())
                {
                    l = sess.CreateCriteria(typeof(BpClient)).List();
                }
                return l;
            }
        }

        public static IList IListAll
        {
            get
            {
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                IList l;
                using (ISession sess = PersistentManager.session())
                {
                    l = sess.CreateCriteria(typeof(BpClient)).List();
                }
                return l;
            }
        }

        //public static List<BpClient> ListAll
        //{
        //    get
        //    {
        //        //ISession sess = HSessionFactory._sessionFactory.OpenSession();
        //        ISession sess = PersistentManager.session();
        //        return (List<BpClient>)sess.CreateCriteria(typeof(BpClient)).List();
        //    }
        //}

        public static bool RetrieveAll(out BpClient[] result,
                                       out string msgErr)
        {
            msgErr = "S/C";
            IList lClient;
            result = null;
            bool bRes = false;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    lClient = sess.CreateCriteria(typeof(BpClient)).List();
                }
                if (lClient != null)
                {
                    result = new BpClient[lClient.Count];
                    int i = 0;
                    foreach (BpClient obj in lClient)
                    {
                        result[i++] = obj;
                    }
                    bRes = true;
                }
            }
            catch (Exception ex)
            {
                bRes = false;
                lClient = null;
                msgErr = ex.Message;
                LOG.Error("AdminBpClient.RetrieveAll", ex);
            }
            /*finally
            {
                if (sess != null && sess.IsOpen) sess.Close();
            }*/
            return bRes;
        }

        private static bool CumpleFiltro(BpTx tx,
                                string idconx, int statusconx, int resultconx, 
                                string timestamprcdesde, string timestamprchasta,
                                string connectorid)
        {
            bool bRes = true;

            try
            {
                IList<BpTxConx> lconx = tx.BpTxConx;
                    //(timestamprcdesde == null || timestamprcdesde.Trim().Length == 0) &&
                    //   (timestamprchasta == null || timestamprchasta.Trim().Length == 0) &&
                if ((idconx == null || idconx.Trim().Length == 0) &&
                    (statusconx == -1) && (resultconx == 0) &&
                    (connectorid == null || connectorid.Trim().Length == 0))
                {
                    return bRes;
                }

                if (lconx == null || lconx.Count == 0) return false;

                foreach (BpTxConx txconx in lconx)
                {
                    if (idconx != null && idconx.Trim().Length > 0)
                    {
                        bRes = txconx.Trackid.Trim().Equals(idconx.Trim());
                    }
                    if (bRes && statusconx != -1)
                    {
                        bRes = bRes && txconx.Status == statusconx;
                    }
                    if (bRes && resultconx > 0)
                    {
                        bRes = bRes && txconx.Result == resultconx;
                    }
                    if (bRes && (connectorid != null && connectorid.Trim().Length > 0))
                    {
                        bRes = bRes && txconx.Connectorid.Trim().Equals(connectorid);
                    }
                    //if (bRes && ((timestamprcdesde != null && timestamprcdesde.Trim().Length > 0) 
                    //         || (timestamprchasta != null && timestamprchasta.Trim().Length > 0)))
                    //{
                    //    bRes = bRes &&
                    //            CumpleFiltroFecha(txconx.Timestamp,
                    //            timestamprcdesde, timestamprchasta);
                    //    //CumpleFiltroFecha(tx.Timestampstart,
                    //    //                      timestamprcdesde, timestamprchasta);
                    //}
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BpTx.CumpleFiltro Error", ex);
                bRes = false;
            }
            return bRes;
        }

        private static bool CumpleFiltroFecha(string timestamp,
                                                string desde, string hasta)
        {
            DateTime dtD = new DateTime();
            DateTime dtH = new DateTime();
            DateTime dtTS = new DateTime();

            CultureInfo ci = new CultureInfo("es-CL");
            ci.DateTimeFormat.ShortDatePattern = "yyyy/MM/dd";
            ci.DateTimeFormat.ShortTimePattern = "HH:mm:ss";

            try
            {
                //Si no son nulas las dos, parseo por cada una
                if (desde.Trim().Length > 0)
                {
                    try
                    {
                        dtD = DateTime.Parse(desde, ci);
                    }
                    catch (Exception de)
                    {
                        LOG.Warn("AdminBpTx.CumpleFiltroFecha error", de);
                        return false;
                    }
                }
                //Si no son nulas las dos, parseo por cada una
                if (hasta.Trim().Length > 0)
                {
                    try
                    {
                        dtH = DateTime.Parse(hasta, ci);
                    }
                    catch (Exception de)
                    {
                        LOG.Warn("AdminBpTx.CumpleFiltroFecha error", de);
                        return false;
                    }
                }
                //Si no son nulas las dos, parseo por cada una
                if (timestamp.Trim().Length > 0)
                {
                    try
                    {
                        dtTS = DateTime.Parse(timestamp, ci);
                    }
                    catch (Exception ts)
                    {
                        LOG.Warn("AdminBpTx.CumpleFiltroFecha error", ts);
                        return false;
                    }
                }

                //Si parsearon bien las dos, veo que cumpla el filtro
                if (dtD <= dtTS && dtTS <= dtH)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex) {
                LOG.Error("BpTx.CumpleFiltroFecha", ex);
                return false;
            }
        }


        public static bool Write(BpClient bptx, out BpClient bptxsaved, out string msgErr)
        {
            msgErr = "S/C";
            bptxsaved = null;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            //ITransaction tx = sess.BeginTransaction();
            try
            {
                //sess.SaveOrUpdate(bptx);
                //bptxsaved = 
                IList l;
                using (ISession sess = PersistentManager.session())
                {
                    sess.SaveOrUpdate(bptx);
                }
                //  tx.Commit();
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                //if (tx != null) tx.Rollback();
                LOG.Error("BpClient.Write", ex);
                return false;
            }
            //finally
            //{
            //    if (sess != null) sess.Close();
            //}
            return true;
        }
        
        public static int GetQCurrentClient()
        {
            String msgErr;
            int res = 0;
            String strsql = "";
            string msgerr;

            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            try
            {
                strsql = "SELECT COUNT(*) AS QCantidad " +
                            "FROM FROM bp_client";

                IList<Cantidad> result;

                using (ISession sess = PersistentManager.session())
                {
                    result = sess.CreateSQLQuery(strsql)
                                            .AddScalar("QCantidad", NHibernateUtil.Int32)
                                        .SetResultTransformer(new NHibernate.Transform.AliasToBeanResultTransformer(typeof(Cantidad)))
                                        .List<Cantidad>();
                }
                res = (result == null || result.Count == 0) ? 0 : result[0].QCantidad;

            }
            catch (Exception ex)
            {
                LOG.Error("BpClient.GetQCurrentClient", ex);
                msgErr = ex.Message;
                res = -1;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return res;
        }

        public static int GetQCurrentClient(int companyid)
        {
            String msgErr;
            int res = 0;
            String strsql = "";
            string msgerr;

            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            try
            {
                strsql = "SELECT COUNT(*) AS QCantidad " +
                           "FROM FROM bp_client " +
                           " WHERE companyid = " + companyid.ToString();

                IList<Cantidad> result;

                using (ISession sess = PersistentManager.session())
                {
                    result = sess.CreateSQLQuery(strsql)
                                            .AddScalar("QCantidad", NHibernateUtil.Int32)
                                        .SetResultTransformer(new NHibernate.Transform.AliasToBeanResultTransformer(typeof(Cantidad)))
                                        .List<Cantidad>();
                }
                res = (result == null || result.Count == 0) ? 0 : result[0].QCantidad;

            }
            catch (Exception ex)
            {
                LOG.Error("BpClient.GetQCurrentClient", ex);
                msgErr = ex.Message;
                res = -1;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return res;
        }
            

#endregion staticMethods

    
    }

    //public class Cantidad
    //{
    //    private int qcantidad;

    //    public int QCantidad
    //    {
    //        get
    //        {
    //            return qcantidad;
    //        }
    //        set
    //        {
    //            qcantidad = value;
    //        }
    //    }

    //}
}
