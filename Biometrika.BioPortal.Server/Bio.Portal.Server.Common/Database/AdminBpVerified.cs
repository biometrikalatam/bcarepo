﻿using System;
using System.Collections.Generic;
using System.Text;
using log4net;
using NHibernate;
using System.Collections;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Common.Entities
{
    public class AdminBpVerified
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpVerified));

        #region Static Method

            public static BpVerified Retrieve(int id, out string msgErr)
            {
                msgErr = "S/C";
                BpVerified data;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();

                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        data = (BpVerified)sess.Load(typeof(BpVerified), id);
                    }
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    LOG.Error("AdminBpVerified.Retrieve", ex);
                    data = null;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
                return data;
            }


            public static IList ListAll
            {
                get
                {
                    //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                    //ISession sess = PersistentManager.session();
                    IList lres = null;
                    try
                    {
                        using (ISession sess = PersistentManager.session())
                        {
                            lres = sess.CreateCriteria(typeof(BpVerified)).List();
                        }
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("AdminBpVerified.ListAll", ex);
                        lres = null;
                    }
                    //finally
                    //{
                    //    if (sess != null && sess.IsOpen) sess.Close();
                    //}
                    return lres;
                }
            }


            public static BpVerified Create(int datatype, string data, string additionaldata, DateTime timestamp, 
                                            BpTx bptx, int? insertoption, int? matchingtype, out string msgErr)
            {
                msgErr = "S/C";
                BpVerified oData;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                //ITransaction tx = sess.BeginTransaction();
                try
                {
                    oData = new BpVerified(datatype, data, additionaldata, timestamp, bptx,  
                                           insertoption, matchingtype);
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.Save(oData);
                    }
                    //  tx.Commit();
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    //tx.Rollback();
                    LOG.Error("AdminBpVerified.Create", ex);
                    oData = null;
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //    if (tx != null) tx = null;
                //}
                return oData;

            }

            public static bool Write(BpVerified data, out string msgErr)
            {
                msgErr = "S/C";
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                //ITransaction tx = sess.BeginTransaction();
                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.SaveOrUpdate(data);
                    }
                  //  tx.Commit();
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    //if (tx != null) tx.Rollback();
                    LOG.Error("AdminBpVerified.Write", ex);
                    return false;
                }
                //finally
                //{
                //    if (sess != null) sess.Close();
                //    if (tx != null) tx = null;
                //}
                return true;
            }


        #endregion Static Method


    }
}
