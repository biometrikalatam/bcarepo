﻿using System;
using System.Collections;
using System.Data;
using NHibernate;
using NHibernate.Criterion;
using log4net;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Common.Entities
{

    /// <summary>
    /// Manejo de origenes para BioPortal Server
    /// </summary>
    [Serializable]
    public class AdminBpOrigin
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpOrigin));

        #region staticMethods

            public static BpOrigin Retrieve(int id, out string msgErr)
            {
                msgErr = "S/C";
                BpOrigin origen;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        //origen = (BpOrigin)sess.Load(typeof(BpOrigin), id);
                        origen = sess.CreateCriteria(typeof(BpOrigin)).
                            Add(Restrictions.Eq("Id", id)).UniqueResult<BpOrigin>();
                    }
                }
                catch (Exception ex)
                {
                    origen = null;
                    msgErr = ex.Message;
                    LOG.Error("AdminBpOrigin.Retrieve", ex);
                }
                /*finally
                {
                    if (sess != null && sess.IsOpen) sess.Close();
                }*/
                return origen;

            }

            public static bool RetrieveAll(out BpOrigin[] result,
                                           out string msgErr)
            {
                msgErr = "S/C";
                IList lOrigen;
                result = null;
                bool bRes = false;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        lOrigen = sess.CreateCriteria(typeof(BpOrigin)).List();
                    }
                    if (lOrigen != null)
                    {
                        result = new BpOrigin[lOrigen.Count];
                        int i = 0;
                        foreach (BpOrigin obj in lOrigen)
                        {
                            result[i++] = obj;
                        }
                        bRes = true;
                    }
                }
                catch (Exception ex)
                {
                    bRes = false;
                    lOrigen = null;
                    msgErr = ex.Message;
                    LOG.Error("AdminBpOrigin.RetrieveAll", ex);
                }
                /*finally
                {
                    if (sess != null && sess.IsOpen) sess.Close();
                }*/
                return bRes;

            }

            public static IList ListAll
            {
                get
                {
                    IList l = null;
                    //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                    //ISession sess = PersistentManager.session();
                    try
                    {
                        using (ISession sess = PersistentManager.session())
                        {
                            l = sess.CreateCriteria(typeof(BpOrigin)).List();
                        }
                    }
                    catch (Exception ex)
                    {
                        string msgErr = ex.Message;
                        LOG.Error("AdminBpOrigin.ListAll", ex);
                    }
                    //finally
                    //{
                    //    if (sess != null && sess.IsOpen) sess.Close();
                    //}
                    return l;
                }
            }

            public static DataView GetAllOrigenDataView(DictionaryEntry[] columns)
            {
                IList l = null;
                DataView dw = null;

                try
                {

                    l = AdminBpOrigin.ListAll;

                    dw = HPersistent.GetDataView(typeof(BpOrigin), l, columns);
                }
                catch (Exception ex)
                {
                    LOG.Error("AdminBpOrigin.GetAllOrigenDataView", ex);
                }
                return dw;
            }

            public static BpOrigin Create(int id, string descripcion, out string msgErr)
            {
                BpOrigin origen = null;

                msgErr = "S/C";
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                try
                {
                    origen = new BpOrigin(id, descripcion);
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.Save(origen);
                        sess.Flush();
                    }
                }
                catch (Exception ex)
                {
                    msgErr = ex.Message;
                    origen = null;
                    LOG.Error("AdminBpOrigin.Create", ex);
                }
                /*finally
                {
                    if (sess != null && sess.IsOpen) sess.Close();
                }*/
                return origen;

            }

            public static bool Delete(int id, out string msgErr)
            {
                BpOrigin origen;
                msgErr = "S/C";
                bool bRes = false;

                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                try
                {
                    origen = AdminBpOrigin.Retrieve(id, out msgErr);
                    if (origen != null)
                    {
                        using (ISession sess = PersistentManager.session())
                        {
                            sess.Delete(origen);
                            sess.Flush();
                        }
                        bRes = true;
                    }

                }
                catch (Exception ex)
                {
                    bRes = false;
                    msgErr = ex.Message;
                    origen = null;
                    LOG.Error("AdminBpOrigin.Delete", ex);
                }
                /*finally
                {
                    if (sess != null && sess.IsOpen) sess.Close();
                }*/
                return bRes;
            }

            public static bool Modify(int id, string descripcion, out string msgErr)
            {
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                BpOrigin origen;
                bool bRes = false;
                try
                {
                    origen = Retrieve(id, descripcion, out msgErr);
                    if (origen != null)
                    {
                        origen = Create(id, descripcion, out msgErr);
                        if (origen != null)
                            bRes = true;
                        else
                            bRes = false;
                    }
                }
                catch (Exception ex)
                {
                    bRes = false;
                    msgErr = ex.Message;
                    origen = null;
                    LOG.Error("AdminBpOrigin.Delete", ex);
                }
                //finally
                //{
                //    if (sess != null && sess.IsOpen) sess.Close();
                //}
                return bRes;
            }

            public static BpOrigin Retrieve(int id, string descripcion, out string msgErr)
            {
                msgErr = "S/C";
                BpOrigin origen;
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                try
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        origen = sess.CreateCriteria(typeof(BpOrigin))
                                                          .Add(Expression.Or(Expression.Eq("Description", descripcion), Expression.Eq("Id", id)))
                                                          .UniqueResult<BpOrigin>();
                    }
                }
                catch (Exception ex)
                {
                    LOG.Error("AdminBpOrigin.Retrieve (id,description)", ex);
                    msgErr = ex.Message;
                    origen = null;
                }
                /*finally
                {
                    if (sess != null && sess.IsOpen) sess.Close();
                }*/
                return origen;
            }

            public static bool Modify(int id, string descripcion, int idori, out string msgErr)
            {
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //ISession sess = PersistentManager.session();
                BpOrigin origen;
                bool bRes = false;
                try
                {
                    origen = Retrieve(id, descripcion, out msgErr);
                    if (origen != null)
                    {
                        if (id == idori)
                        {
                            origen.Description = descripcion;
                            using (ISession sess = PersistentManager.session())
                            {
                                sess.Update(origen);
                                sess.Flush();
                            }
                            bRes = true;
                        }
                        else
                            bRes = false;
                    }
                }
                catch (Exception ex)
                {
                    bRes = false;
                    msgErr = ex.Message;
                    origen = null;
                    LOG.Error("AdminBpOrigin.Delete", ex);
                }
                /*finally
                {
                    if (sess != null && sess.IsOpen) sess.Close();
                }*/
                return bRes;
            }

        #endregion staticMethods

    }
}