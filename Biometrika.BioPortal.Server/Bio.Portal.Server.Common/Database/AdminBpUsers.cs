﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Portal.Server.Common;
using Bio.Portal.Server.Common.BioMembership;
using NHibernate;
using NHibernate.Criterion;
using log4net;
using System.Web.Security;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Common.Entities
{
    public class AdminBpUsers
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpUsers));
        /// <summary>
        /// Agrega un usuario a la tabla bp_users.
        /// </summary>
        /// <param name="user">Tipo User que soporta la clase mapeada de Membership</param>        
        public static void AddUser(User user)
        {

            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    sess.Save(user);
                    sess.Flush();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpUsers.AddUser", ex);
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
        }

        public static int ModifyUser(int id, string mail, bool IsApproved, out string msgErr)
        {
            msgErr = "S/C";
            int result = 0;
            User user;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                //Verificamos que el mail no se encuentre asociado a otro usuario
                user = GetUserByMail(mail, out msgErr);
                if (user != null)
                {

                    //Se trata del mismo usuario
                    if (user.Id == id)
                    {
                        user.Email = mail;
                        user.IsApproved = IsApproved;
                        using (ISession sess = PersistentManager.session())
                        {
                            sess.Update(user);
                            sess.Flush();
                        }
                    }
                    else
                        return -1;

                }
                else
                {
                    user = GetUserById(id, out msgErr);
                    user.Email = mail;
                    user.IsApproved = IsApproved;
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.Update(user);
                        sess.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpUsers.Modify (BpUsers)", ex);
                msgErr = ex.Message;
                result = -1;
                return result;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return result;
        }

        public static int ModifyUser(int id, string mail, string Name, string Surname, 
                                     string Phone, string PostalCode, bool IsApproved, 
                                     out string msgErr)
        {
            msgErr = "S/C";
            int result = 0;
            User user;
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                //Verificamos que el mail no se encuentre asociado a otro usuario
                user = GetUserByMail(mail, out msgErr);
                if (user != null)
                {

                    //Se trata del mismo usuario
                    if (user.Id == id)
                    {
                        user.Email = mail;
                        user.Name = Name;
                        user.Surname = Surname;
                        user.Phone = Phone;
                        user.PostalCode = PostalCode;
                        user.IsApproved = IsApproved;
                        using (ISession sess = PersistentManager.session())
                        {
                            sess.Update(user);
                            sess.Flush();
                        }
                    }
                    else
                        return -1;

                }
                else
                {
                    user = GetUserById(id, out msgErr);
                    user.Email = mail;
                    user.Name = Name;
                    user.Surname = Surname;
                    user.Phone = Phone;
                    user.PostalCode = PostalCode;
                    user.IsApproved = IsApproved;
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.Update(user);
                        sess.Flush();
                    }
                }

            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpUsers.Modify (BpUsers)", ex);
                msgErr = ex.Message;
                result = -1;
                return result;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return result;
        }

        /// <summary>
        /// Método que permite recuperar un usuario por su identificador interno
        /// </summary>
        /// <param name="userid">indetificador interno</param>
        /// <param name="msgErr"></param>
        /// <returns></returns>
        public static User GetUserById(int userid, out string msgErr)
        {
            msgErr = "";
            User user = null;
            //ISession sess = null;
            try
            {

                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //sess = PersistentManager.session();
                using (ISession sess = PersistentManager.session())
                {
                    ICriteria crit = sess.CreateCriteria(typeof(User))
                                 .Add(Expression.Eq("Id", userid));

                    user = crit.UniqueResult<User>();
                }

            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("AdminBpUsers.Retrieve (typeid,valueid)", ex);
                user = null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return user;
        }
        /// <summary>
        /// Método que permite recuperar un usuario por su mail
        /// </summary>
        /// <param name="mail">correo electrónico del usuario</param>
        /// <param name="msgErr"></param>
        /// <returns></returns>
        public static User GetUserByMail(string mail, out string msgErr)
        {
            msgErr = "";
            User user = null;
            //ISession sess = null;
            try
            {


                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //sess = PersistentManager.session();
                using (ISession sess = PersistentManager.session())
                {
                    ICriteria crit = sess.CreateCriteria(typeof(User))
                                 .Add(Expression.Eq("Email", mail));

                    user = crit.UniqueResult<User>();
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("AdminBpUsers.GetUserByMail (typeid,valueid)", ex);
                user = null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return user;
        }
        /// <summary>
        /// método que permite obtener todos los usuarios de una compañía en particular.
        /// </summary>
        /// <param name="companyId">identificador de la compañía</param>
        /// <param name="msgErr"></param>
        /// <returns></returns>
        public static IList<User> GetAllUser(int companyId, out string msgErr)
        {
            msgErr = "";
            IList<User> users = null;
            //ISession sess = null;
            try
            {

                BpCompany company = AdminBpCompany.GetCompanyById(companyId, out msgErr);
                //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                //sess = PersistentManager.session();
                using (ISession sess = PersistentManager.session())
                {
                    ICriteria crit = sess.CreateCriteria(typeof(User))
                                 .Add(Expression.Eq("CompanyId", company));
                    users = crit.List<User>();
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("AdminBpIdentity.Retrieve (typeid,valueid)", ex);
                users = null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return users;
        }
        /// <summary>
        /// Método que permite realizar la búsqueda en el listado de usuarios.
        /// </summary>
        /// <param name="companyId">id de la compañía, si viene 0 es que se busca en todas</param>
        /// <param name="username">nombre de usuario</param>
        /// <param name="msgErr"></param>
        /// <returns></returns>
        public static IList<User> GetAllUser(int companyId, string username, out string msgErr)
        {
            msgErr = "";
            IList<User> users = null;
            //ISession sess = null;
            BpCompany company = null;
            try
            {
                if (companyId > 0)
                {
                    company = AdminBpCompany.GetCompanyById(companyId, out msgErr);
                }
                    //ISession sess = HSessionFactory._sessionFactory.OpenSession();
                    //sess = PersistentManager.session();
                using (ISession sess = PersistentManager.session())
                {
                    ICriteria crit = sess.CreateCriteria(typeof(User));
                    if (companyId > 0)
                    {
                        crit.Add(Expression.Eq("CompanyId", company));
                    }
                    if (username != String.Empty)
                        crit.Add(Expression.Eq("UserName", username));

                    users = crit.List<User>();
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("AdminBpIdentity.Retrieve (typeid,valueid)", ex);
                users = null;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return users;
        }

        public static int DeleteUser(int id, CustomRoleProvider roles, out string msgErr)
        {
            msgErr = "S/C";
            int result = 0;
            User user;
            string[] users = new string[1];
            //ISession sess = HSessionFactory._sessionFactory.OpenSession();
            //ISession sess = PersistentManager.session();
            try
            {
                user = GetUserById(id, out msgErr);
                if (user != null)
                {
                    string[] rols = roles.GetAllRoles();
                    users[0] = user.UserName;
                    roles.RemoveUsersFromRoles(users, rols);
                    using (ISession sess = PersistentManager.session())
                    {
                        sess.Delete(user);
                        sess.Flush();
                    }
                }
                else
                {
                    result = -1;
                }

            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpCompany.Delete (id)", ex);
                msgErr = ex.Message;
                result = -1;
                return result;
            }
            //finally
            //{
            //    if (sess != null && sess.IsOpen) sess.Close();
            //}
            return result;
        }

    }
}
