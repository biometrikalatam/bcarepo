﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bio.Core.Constant;
using Bio.Portal.Server.Common.Entities.Database;
using log4net;
using NHibernate;
using NHibernate.Criterion;

namespace Bio.Portal.Server.Common.Entities
{
    public class AdminBpIdentity3ro
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdminBpIdentity3ro));

        public static int Retrieve(int identid, int type, out BpIdentity3ro bpIdentity3Ro) 
        {
            bpIdentity3Ro = null;
            int ret = Errors.IERR_OK;
            string msg;
            try
            {
                IList l;
                LOG.Debug("AdminBpIdentity3ro.Retrieve IN - Recupera Identity con id = " + identid + "..."); 
                BpIdentity ident = AdminBpIdentity.Retrieve(identid, out msg);
                LOG.Debug("AdminBpIdentity3ro.Retrieve - ident!=null => " + (ident != null).ToString());
                using (ISession sess = PersistentManager.session())
                {
                    LOG.Debug("AdminBpIdentity3ro.Retrieve - Recupera con ident + Type3roService = " + type);
                    l = sess.CreateCriteria(typeof(BpIdentity3ro)).
                        Add(Restrictions.Eq("BpIdentity", ident)).
                        Add(Expression.Eq("Type3roService", type))
                        .List();
                }
                if (l == null || l.Count == 0)
                {
                    LOG.Debug("AdminBpIdentity3ro.Retrieve - Retorna no encontrado...");
                    ret = Errors.IERR_IDENTITY_NOT_FOUND;
                }
                else
                {
                    LOG.Debug("AdminBpIdentity3ro.Retrieve - Retorna lo que encontro...");
                    IEnumerator en = l.GetEnumerator();
                    en.MoveNext();
                    bpIdentity3Ro = (BpIdentity3ro)en.Current;
                    LOG.Debug("AdminBpIdentity3ro.Retrieve - bpIdentity3Ro!=null => " +
                                                    (bpIdentity3Ro != null).ToString());
                    ret = Errors.IERR_OK;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity3ro.Retrieve (identid, type)", ex);
                bpIdentity3Ro = null;
                ret = Errors.IERR_UNKNOWN;
            }
            LOG.Debug("AdminBpIdentity3ro.Retrieve OUT!");
            return ret;
        }

        public static int GetFaceMap(int company, string typeid, string valueid, int type, out string strFaceMap, out string msgerr)
        {
            int ret = Errors.IERR_OK;
            strFaceMap = null;
            msgerr = null;
            try
            {
                IList l;
                BpIdentity ident; 
                ret = AdminBpIdentity.Retrieve(company, typeid, valueid, out ident, out msgerr, false);
                using (ISession sess = PersistentManager.session())
                {
                    l = sess.CreateCriteria(typeof(BpIdentity3ro)).
                        Add(Restrictions.Eq("BpIdentity", ident)).
                        Add(Expression.Eq("Type3roService", type))
                        .List();
                }
                if (l == null || l.Count == 0)
                    ret = Errors.IERR_IDENTITY_NOT_FOUND;
                else
                {
                    IEnumerator en = l.GetEnumerator();
                    en.MoveNext();
                    strFaceMap = ((BpIdentity3ro)en.Current).TrackId3ro; //Es facemap de FaceTec
                    ret = Errors.IERR_OK;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity3ro.GetFaceMap (identid, type)", ex);
                msgerr = "AdminBpIdentity3ro.GetFaceMap => " + ex.Message;
                strFaceMap = null;
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }

        public static int Update(BpIdentity3ro identity3ro, out string msgErr)
        {
            msgErr = "S/C";
            int bRet = 0;
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    sess.Update(identity3ro);
                    sess.Flush();
                    bRet = 0;
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("AdminBpIdentity3ro.Update", ex);
                bRet = -1;
            }
            return bRet;
        }

        public static int InsertOrUpdate(BpIdentity ident, int type, string trackidORsample3ro, out string msgerr)
        {
            int ret = Errors.IERR_OK;
            msgerr = null;
            try
            {
                IList l;
                //BpIdentity ident = AdminBpIdentity.Retrieve(identid, out msgerr);
                if (ident != null)
                {
                    using (ISession sess = PersistentManager.session())
                    {
                        l = sess.CreateCriteria(typeof(BpIdentity3ro)).
                            Add(Restrictions.Eq("BpIdentity", ident)).
                            Add(Expression.Eq("Type3roService", type))
                            .List();
                    }
                    if (l == null || l.Count == 0)
                    {
                        //No existe => Agrego
                        BpIdentity3ro bpidentity3ro = new BpIdentity3ro(ident, type, trackidORsample3ro, DateTime.Now, DateTime.Now);
                        ret = AdminBpIdentity3ro.Write(bpidentity3ro, out msgerr);
                        //ret = Errors.IERR_IDENTITY_NOT_FOUND;
                    }
                    else
                    {
                        //Existe => No hago nada
                        //IEnumerator en = l.GetEnumerator();
                        //en.MoveNext();
                        //BpIdentity3ro bpidentity3ro = ((BpIdentity3ro)en.Current);
                    }
                    ret = Errors.IERR_OK;
                } else
                {
                    ret = Errors.IERR_IDENTITY_NOT_FOUND;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity3ro.InsertOrUpdate (identid, type)", ex);
                msgerr = "AdminBpIdentity3ro.InsertOrUpdate => " + ex.Message;
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }

        private static int Write(BpIdentity3ro bpidentity3ro, out string msgErr)
        {
            msgErr = "S/C";
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    sess.SaveOrUpdate(bpidentity3ro);
                }
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("BpTx.Write", ex);
                return -1;
            }
            return 0;
        }

        public static int Retrieve(BpIdentity identid, out IList listBpIdentity3ro)
        {
            listBpIdentity3ro = null;
            int ret = Errors.IERR_OK;
            string msg;
            try
            {
                LOG.Debug("AdminBpIdentity3ro.Retrieve IN - Recupera Identity con id = " + 
                    (identid==null?"Null": identid.Id.ToString()) + "...");

                if (identid == null) return Errors.IERR_IDENTITY_NOT_FOUND;

                LOG.Debug("AdminBpIdentity3ro.Retrieve - Recupero lista...");
                using (ISession sess = PersistentManager.session())
                {
                    listBpIdentity3ro = sess.CreateCriteria(typeof(BpIdentity3ro)).
                                        Add(Restrictions.Eq("BpIdentity", identid))
                                        .List();
                }
                if (listBpIdentity3ro == null || listBpIdentity3ro.Count == 0)
                {
                    LOG.Debug("AdminBpIdentity3ro.Retrieve - Retorna no encontrado...");
                    ret = Errors.IERR_BIR_NOT_FOUND;
                }
                else
                {
                    ret = Errors.IERR_OK;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdminBpIdentity3ro.Retrieve (identid, type)", ex);
                listBpIdentity3ro = null;
                ret = Errors.IERR_UNKNOWN;
            }
            LOG.Debug("AdminBpIdentity3ro.Retrieve OUT!");
            return ret;
        }
    }
}
