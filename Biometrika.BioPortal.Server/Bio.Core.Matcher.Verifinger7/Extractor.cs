﻿using System;
using System.Drawing;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using Bio.Core.Wsq.Decoder;
using log4net;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Images;
//using Neurotec.Images;

namespace Bio.Core.Matcher.Verifinger7
{
    public class Extractor : IExtractor
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Extractor));

        NFRecord nfeRecord;
        NBiometricClient _biometricClient;
        NSubject _subject = new NSubject();
        Bitmap imgBMP;
        NImage imageRAWtoBMP;
        Neurotec.IO.NBuffer nBuf;

#region Implementation of IExtractor

        private int _authenticationFactor;

        private int _minutiaeType;

        private double _threshold;

        private string _parameters;

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        /// <summary>
        /// Tecnologia a utilizar para extraccion
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo d eminucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de extracción considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Extrae desde el template ingresado en el parametro inicial, de acuerdo a 
        /// los adtos de tecnologia y minucias seteados, el template resultante. 
        /// Estos parámetros están serializados en xml, por lo que primero se deserializa.
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="xmloutput">template generado serializado en xml</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out string xmloutput)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero entrega un ITemplate
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out ITemplate templateout)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero ingresa un ITemplate y entrega un ITemplate
        /// </summary>
        /// <param name="templatebase">Template base para extraccion</param>
        /// <param name="destination">Determina si es template para 1-Verify | 2-Enroll </param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(ITemplate templatebase, int destination, out ITemplate templateout)
        {
            int res = Errors.IERR_OK;
            templateout = null;

            try
            {

                if (!VfeUtils.IsLicenseConfigured)
                {
                    if (!VfeUtils.ConfigureLicense(_parameters)) return Errors.IERR_LICENSE;
                }

                if (templatebase == null) return Errors.IERR_NULL_TEMPLATE;
                if (templatebase.Data == null) return Errors.IERR_NULL_TEMPLATE;

                if (templatebase.AuthenticationFactor !=
                    Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT ||
                    (templatebase.MinutiaeType !=
                    Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER && 
                    templatebase.MinutiaeType !=
                    Constant.MinutiaeType.MINUTIAETYPE_WSQ &&
                    templatebase.MinutiaeType !=
                    Constant.MinutiaeType.MINUTIAETYPE_RAW))
                {
                    return Errors.IERR_INVALID_TEMPLATE;
                }

                //Si lo anterior esta ok, y la minucia es Verifinger solo asigno porque 
                if (templatebase.MinutiaeType == Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER)
                {
                    LOG.Error("Verifinger7.Extractor.Extract - In Minutiae VF...");
                    templateout = templatebase;
                }
                else //es Constant.MinutiaeType.MINUTIAETYPE_WSQ
                {
                    //NFRecord nfeRecord;
                    //NBiometricClient _biometricClient;
                    //NSubject _subject;
                    byte[] raw;
                    bool rawOK = false;
                    if (templatebase.MinutiaeType == Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                    {
                        LOG.Debug("Verifinger7.Extractor.Extract - In WSQ...");
                        //1.- Descomprimo WSQ
                        WsqDecoder decoder = new WsqDecoder();
                        short w, h;
                        if (!decoder.DecodeMemory(templatebase.Data, out w, out h, out raw))
                        {
                            res = Errors.IERR_WSQ_DECOMPRESSING;
                            LOG.Error("Verifinger7.Extractor.Extract Error [decoder.DecodeMemory]");
                        } else
                        {
                            rawOK = true;
                        }
                    } else
                    {
                        LOG.Debug("Verifinger7.Extractor.Extract - In RAW...");
                        raw = templatebase.Data;
                        rawOK = true;
                    }
                    
                    if (rawOK)
                    {
                        LOG.Debug("Verifinger7.Extractor.Extract - Crea NBiometricClient/Subject/NFinger...");
                        using (var biometricClient = new NBiometricClient())
                        using (var subject = new NSubject())
                        using (var finger = new NFinger())
                        {
                            LOG.Debug("Verifinger7.Extractor.Extract - Lee RAW desde NBuffer...");
                            Neurotec.IO.NBuffer buffer = new Neurotec.IO.NBuffer(raw); //File.ReadAllBytes("BMP_Biometrika.raw"));

                            LOG.Debug("Verifinger7.Extractor.Extract - Formatea 512x512...");
                            NImage image = NImage.FromData(NPixelFormat.Grayscale8U, 512, 512, 0, 512, buffer);
                            if (image.HorzResolution < 250)
                            {
                                image.HorzResolution = 500;
                                image.ResolutionIsAspectRatio = false;
                            }
                            if (image.VertResolution < 250)
                            {
                                image.VertResolution = 500;
                                image.ResolutionIsAspectRatio = false;
                            }
                            LOG.Debug("Verifinger7.Extractor.Extract - Set image en NFinger...");
                            finger.Image = image;

                            //Read finger image from file and add it NSubject
                             LOG.Debug("Verifinger7.Extractor.Extract - Add NFinger en Subject...");
                            subject.Fingers.Add(finger);

                            //Set finger template size (recommended, for enroll to database, is large) (optional)
                            LOG.Debug("Verifinger7.Extractor.Extract - Set NTemplateSize.Large..."); 
                            biometricClient.FingersTemplateSize = NTemplateSize.Large;

                            //Create template from added finger image
                            LOG.Debug("Verifinger7.Extractor.Extract - Set BiometricTemplateFormat.Proprietary...");
                            biometricClient.BiometricTemplateFormat = BiometricTemplateFormat.Proprietary;

                            LOG.Debug("Verifinger7.Extractor.Extract - Create Tempalte con biometricClient.CreateTemplate(subject)...");
                            var status = biometricClient.CreateTemplate(subject);
                            LOG.Debug("Verifinger7.Extractor.Extract - Status => biometricClient.CreateTemplate(subject) = " + status.ToString() + "...");
                            
                            if (status == NBiometricStatus.Ok)
                            {
                                //nBuf = nfeRecord.Save();
                                //_subject.SetTemplateBuffer(nBuf);
                                byte[] data = subject.GetTemplateBuffer().ToArray();
                                templateout = new Template
                                {
                                    Data = data
                                };
                                templateout.AdditionalData = "SizeData=" + templateout.Data.Length.ToString() +
                                                        "|Quality=" + subject.Fingers[0].Objects[0].NfiqQuality;
                                templateout.AuthenticationFactor = templatebase.AuthenticationFactor;
                                templateout.BodyPart = templatebase.BodyPart;
                                templateout.MinutiaeType = Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER;
                                templateout.Type = Constant.BirType.PROCESSED_DATA;
                                LOG.Debug("Verifinger7.Extractor.Extract - templateout.Data = " + Convert.ToBase64String(data));
                            }
                            else
                            {
                                res = Errors.IERR_EXTRACTING;
                                templateout = null;
                                LOG.Error("Verifinger7.Extractor.Extract Error [extractionStatus=" + status.ToString() + "]");
                            }
                        }                       
                    } 
                }
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("Bio.Core.Matcher.Verifinger7.Extract(T, T) Error", ex);
            }
            return res;
        }

#endregion Implementation of IExtractor

        public void Dispose()
        {
            if (VfeUtils.IsLicenseConfigured)
            {
                VfeUtils.ReleaseLicenses();
            }
        }
    }
}
