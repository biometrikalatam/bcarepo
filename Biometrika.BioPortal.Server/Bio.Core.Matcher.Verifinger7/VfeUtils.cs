﻿
   using System;
using System.Globalization;
using System.Reflection;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.IO;
#if !N_PRODUCT_HAS_NO_LICENSES
   using log4net;
   using Neurotec.Licensing;
#endif

namespace Bio.Core.Matcher.Verifinger7
{
	public static class VfeUtils
	{
        private static readonly ILog LOG = LogManager.GetLogger(typeof(VfeUtils));

        public static bool IsLicenseConfigured;

        private static Dictionary<string, string> _licenseCfg;

		//private const string LicensesConfiguration = "NLicenses.cfg";
        private const string LicensesConfiguration = "Biometrics.FingerExtraction,Biometrics.FingerMatching"; 
        
        /// <summary>
        /// Inicializa la licencia la primera vez que se usa una clase de este Namespace.
        /// </summary>
        public static bool ConfigureLicense(string parameters)
        {
            IList<string> licensesMain = new List<string>(new string[] { "FingersExtractor", "FingersMatcher" });

            try
            {

                if (parameters == null)
                {
                    LOG.Error("Bio.Core.Matcher.Verifinger7.VfeUtils.ConfigureLicense Error - Parameters Matcher = NULL");
                    return false;
                }

                LOG.Debug("Bio.Core.Matcher.Verifinger7.VfeUtils.ConfigureLicense - ExtractPath(parameters) de parameters=" + parameters);
                string server = "/local";
                int port = 5000;
                string componentlicensecfg = ExtractParams(parameters, out server, out port);

                if (componentlicensecfg == null)
                {
                    LOG.Error("Bio.Core.Matcher.Verifinger7.VfeUtils.ConfigureLicense Error - Component License Cfg = NULL");
                    return false;
                }

                LOG.Debug("Bio.Core.Matcher.Verifinger7.VfeUtils.ConfigureLicense - Entrando a VfeUtils.ObtainLicenses");
                VfeUtils.ObtainLicenses(componentlicensecfg, server, port);               
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Verifinger7.VfeUtils.ConfigureLicense Error", ex);
            }
            return IsLicenseConfigured;
            //finally
            //{
            //    Utils.ReleaseLicenses(licensesMain);
            //    Utils.ReleaseLicenses(licensesBss);
            //}
        }

        //Extrae el pathj de NLicense.cfg de los parametros
        // Formato: PathLicenseCfg=C:\\NLicense.cfg
	    private static string ExtractPath(string parameters)
	    {
	        string path = null;
	        try
	        {
	            string[] arr = parameters.Split(new string[] {"|"}, StringSplitOptions.RemoveEmptyEntries);
                if (arr.Length == 0) return null;

                for (int i = 0; i < arr.Length; i++)
	            {
                    string[] item = parameters.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                    if (item[0].Equals("ComponentLicenseCfg") || item[0].Equals("ConfigLicense"))
                    {
                        path = item[1];
                        break;
                    }
	            }
	            
	        } catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Verifinger7.VfeUtils.ExtractPath Error", ex);
            }
	        return path;
	    }


        //Extrae el pathj de NLicense.cfg de los parametros
        // Formato: Server=/local|Port=5000|ComponentLicenseCfg=Biometrics.FingerExtraction,Biometrics.FingerMatching
        private static string ExtractParams(string parameters, out string server, out int port)
        {
            server = "/local";
            port = 5000;
            string path = null;
            try
            {
                string[] arr = parameters.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                LOG.Debug("ExtractParams arr.length=" + arr.Length.ToString());
                if (arr.Length == 0) return null;
                 
                for (int i = 0; i < arr.Length; i++)
                {
                    LOG.Debug("ExtractParams procesando arr[" + i.ToString() + "]=" + arr[i]);
                    string[] item = arr[i].Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                    LOG.Debug("ExtractParams item.length=" + item.Length.ToString());
                    if (item[0].Equals("ComponentLicenseCfg") || item[0].Equals("ConfigLicense"))
                    {
                        path = item[1];
                        LOG.Debug("ExtractParams path=" + path);
                    }
                    if (item[0].Equals("Server") || item[0].Equals("server"))
                    {
                        server = item[1];
                        LOG.Debug("ExtractParams Server=" + server);
                    }
                    if (item[0].Equals("Port") || item[0].Equals("port"))
                    {
                        try
                        {
                            port = Convert.ToInt32(item[1].Trim());
                        } catch(Exception ex)
                        {
                            LOG.Debug("ExtractParams Parse Port Exr=" + ex.Message);
                            port = 5000;
                        }
                        LOG.Debug("ExtractParams Port=" + port.ToString());
                    }

                }

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Verifinger7.VfeUtils.ExtractParams Error", ex);
            }
            return path;
        }


	   // Load configuration file 
		public static Dictionary<string, string> LoadConfigurations(string filename)
		{
			Dictionary<string, string> config = new Dictionary<string, string>();

			string[] lines = File.ReadAllLines(filename);

			foreach (string line in lines)
			{
				if (line.TrimStart().StartsWith("#") || line.Trim() == string.Empty)
				{
					//ignore comment or empty line
					continue;
				}

				string[] values = line.Split('=');
				string value = (values.Length > 1) ? values[1] : "";
				if (values.Length > 0)
				{
					// values[0] is the key
					config.Add(values[0].Trim(), value.Trim());
				}
			}

			return config;
		}

		// Load licenses configuration file with names of licenses to obtain
		private static void LoadLicenseConfiguration(string pathlicensecfg)
		{
			//string path = Path.Combine(GetAssemblyPath(), LicensesConfiguration);

            _licenseCfg = LoadConfigurations(pathlicensecfg);
		}

	#if !N_PRODUCT_HAS_NO_LICENSES

		public static void ObtainLicenses(string componenticensecfg)
		{
			//ObtainLicenses(pathlicensecfg, new string[] { license });

            try
            {
                if (String.IsNullOrEmpty(componenticensecfg)) return;
                LOG.Debug(
                    "Bio.Core.Matcher.Verifinger7.VfeUtils.ObtainLicenses - Entrando a NLicense.ObtainComponents('/local', 5000, componenticensecfg) con " +
                    " componenticensecfg=" + componenticensecfg);
                if (!NLicense.ObtainComponents("/local", 5000, componenticensecfg))
                {
                    LOG.Fatal(string.Format("No puede obtener licencias válidas de los componentes: {0}", componenticensecfg));
                    IsLicenseConfigured = false;
                } else
                {
                    LOG.Debug("Bio.Core.Matcher.Verifinger7.VfeUtils.ObtainLicenses - Licencia OK");
                    IsLicenseConfigured = true;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Verifinger7.VfeUtils.ObtainLicenses", ex);
            }

            //finally
            //{
            //    NLicense.ReleaseComponents(componenticensecfg);
            //}
		}

        public static void ObtainLicenses(string componenticensecfg, string server, int port)
        {
            //ObtainLicenses(pathlicensecfg, new string[] { license });

            try
            {
                if (String.IsNullOrEmpty(componenticensecfg)) return;
                LOG.Debug(
                    "Bio.Core.Matcher.Verifinger7.VfeUtils.ObtainLicenses - Entrando a NLicense.ObtainComponents(server, port, componenticensecfg) con " +
                    server + "," + port.ToString() + "," + componenticensecfg);
                if (!NLicense.ObtainComponents(server, port, componenticensecfg))
                {
                    LOG.Fatal(string.Format("No puede obtener licencias válidas de los componentes: {0}", componenticensecfg));
                    IsLicenseConfigured = false;
                }
                else
                {
                    LOG.Debug("Bio.Core.Matcher.Verifinger7.VfeUtils.ObtainLicenses - Licencia OK");
                    IsLicenseConfigured = true;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Verifinger7.VfeUtils.ObtainLicenses", ex);
            }

            //finally
            //{
            //    NLicense.ReleaseComponents(componenticensecfg);
            //}
        }


        public static void ReleaseLicenses()
        {
            if (IsLicenseConfigured)
            {
                IList<string> licensesMain = new List<string>(new string[] {"FingersExtractor", "FingersMatcher"});
                ReleaseLicenses(LicensesConfiguration);
            }
        }

        public static void ReleaseLicenses(string componenticensecfg)
		{
		    try
		    {
                NLicense.ReleaseComponents(componenticensecfg);
		    }
		    catch (Exception ex)
		    {
                LOG.Error("Bio.Core.Matcher.Verifinger7.VfeUtils.ReleaseLicenses", ex);
		    }
            //for (int i = 0; i < licenses.Count; i++)
            //{
            //    if (licenses[i] == string.Empty)
            //        continue;

            //    try
            //    {
            //        NLicense.Release(licenses[i]);
            //    }
            //    catch (Exception ex)
            //    {
            //        LOG.Error("VfeUtils.ReleaseLicenses Error", ex);
            //    }
            //}
		    IsLicenseConfigured = false;
		}

	#endif // !N_PRODUCT_HAS_NO_LICENSES

		public static string GetAssemblyName()
		{
			return Assembly.GetExecutingAssembly().GetName().Name;
		}

		public static string GetAssemblyPath()
		{
			return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
		}

		public static int QualityToPercent(byte value)
		{
			return (2 * value * 100 + 255) / (2 * 255);
		}

		public static byte QualityFromPercent(int value)
		{
			return (byte)((2 * value * 255 + 100) / (2 * 100));
		}

		public static string MatchingThresholdToString(int value)
		{
			double p = -value / 12.0;
			return string.Format(string.Format("{{0:P{0}}}", Math.Max(0, (int)Math.Ceiling(-p) - 2)), Math.Pow(10, p));
		}

		public static int MatchingThresholdFromString(string value)
		{
			double p = Math.Log10(Math.Max(double.Epsilon, Math.Min(1,
				double.Parse(value.Replace(CultureInfo.CurrentCulture.NumberFormat.PercentSymbol, "")) / 100)));
			return Math.Max(0, (int)Math.Round(-12 * p));
		}

		public static int MaximalRotationToDegrees(byte value)
		{
			return (2 * value * 360 + 256) / (2 * 256);
		}

		public static byte MaximalRotationFromDegrees(int value)
		{
			return (byte)((2 * value * 256 + 360) / (2 * 360));
		}



	    //Descarte
        public static void ObtainLicenses(string pathlicensecfg, IList<string> licenses)
        {
            int i;

            if (_licenseCfg == null)
            {
                LoadLicenseConfiguration(pathlicensecfg);
            }

            for (i = 0; i < licenses.Count; i++)
            {
                if (_licenseCfg.ContainsKey(licenses[i]))
                    licenses[i] = _licenseCfg[licenses[i]];
                else
                    licenses[i] = string.Empty;
            }

            // Remove duplicates
            for (i = 0; i < licenses.Count - 1; i++)
            {
                if (licenses[i] == string.Empty)
                {
                    continue;
                }

                int j;
                for (j = i + 1; j < licenses.Count; j++)
                {
                    if (licenses[i] == licenses[j])
                    {
                        licenses[j] = string.Empty;
                    }
                }
            }

            string licenseServer = _licenseCfg["Address"];
            string licensePort = _licenseCfg["Port"];

            for (i = 0; i < licenses.Count; i++)
            {
                if (licenses[i] == string.Empty)
                    continue;

                try
                {
                    bool available = NLicense.Obtain(licenseServer, licensePort, licenses[i]);

                    if (!available)
                    {
                        LOG.Error("Bio.Core.Matcher.Verifinger7.VfeUtils.ObtainLicenses Error Verificando Validez de " +
                                    licenses[i] + " [" + "Licencia Inválida para : Server=" + licenseServer +
                                    " - Port=" + licensePort + "]");
                        //throw new Exception(string.Format("license for {0} was not obtained", licenses[i]));
                    }
                    else
                    {
                        if (i == 0) IsLicenseConfigured = true;
                        else IsLicenseConfigured = IsLicenseConfigured & true;
                    }
                }
                catch (Exception ex)
                {
                    LOG.Error("Bio.Core.Matcher.Verifinger7.VfeUtils.ObtainLicenses Error, Please check if Neurotec Activation Service is running.", ex);
                    //throw new Exception(string.Format("Error while obtaining license. Please check if Neurotec Activation Service is running. Details: {0}", ex));
                }
            }
        }

        public static string GetUserLocalDataDir(string productName)
        {
            string localDataDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            localDataDir = Path.Combine(localDataDir, "Neurotechnology");
            if (!Directory.Exists(localDataDir))
            {
                Directory.CreateDirectory(localDataDir);
            }
            localDataDir = Path.Combine(localDataDir, productName);
            if (!Directory.Exists(localDataDir))
            {
                Directory.CreateDirectory(localDataDir);
            }

            return localDataDir;
        }
	}
}
