﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using Bio.Core.Serialize;
using log4net;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;


namespace Bio.Core.Matcher.Verifinger7
{
    public class Matcher : IMatcher
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Matcher));

        NBiometricClient _matcher = new NBiometricClient();
        Bio.Core.Matcher.Template templateVerified = null;
        List<Bio.Core.Matcher.Template> listTemplatesBase;
        NBiometricTask enrollTask;
        NBiometricStatus status;
        NSubject currentSubject1 = new NSubject();
        NSubject currentSubject2 = new NSubject();

#region Implementation of IMatcher

        private int _authenticationFactor;
        private int _minutiaeType;
        private double _threshold;
        private int _matchingType;

        private string _parameters;

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }


        /// <summary>
        /// Tecnologia a utilizar para matching
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de verificación considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Dos posibles valores:
        ///     0 - FIRST   -> El primer matching positivo
        ///     1 - BEST    -> El mejor matching positivo
        /// </summary>
        public int MatchingType
        {
            get { return _matchingType; }
            set { _matchingType = (value == 1 || value == 2) ? value : 1; }
        }

        //int IMatcher.AuthenticationFactor { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //int IMatcher.MinutiaeType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //double IMatcher.Threshold { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //int IMatcher.MatchingType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //string IMatcher.Parameters { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// y nuevo, parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;

            try
            {
                if (!VfeUtils.IsLicenseConfigured)
                {
                    if (!VfeUtils.ConfigureLicense(_parameters))
                    {
                        msg = "Bio.Core.Matcher.Verifinger7.Matcher Error Chequeando Licencia VF6";
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_LICENSE;
                    }
                }

                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                //1. Importo template de registro, creo cada uno y los importo
                try
                {
                    byCurrentTemplate = Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.Verifinger7.Matcher Error importando CurrentTemplate " +
                         "[ex=" + ex.Message + "]";
                    LOG.Fatal(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Genero objeto para matching
                NBiometricClient _matcher = new NBiometricClient();
                // Set paramater to return matching details
                _matcher.MatchingWithDetails = true;
                _matcher.MatchingThreshold = Convert.ToInt32(_threshold);
                LOG.Debug("Verifinger7.Verify - _matcher.MatchingThreshold = " + _matcher.MatchingThreshold);
                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                                (string)paramin.GetValue("listBaseTemplates"));
                LOG.Debug("Verifinger7.Verify - listTemplatesBase != null => " + (listTemplatesBase != null));
                if (listTemplatesBase != null) LOG.Debug("Verifinger7.Verify - listTemplatesBase.count => " + listTemplatesBase.Count);

                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                //Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                // Read probe template
                using (NSubject currentSubject = CreateSubject(byCurrentTemplate, "CurrentSubject"))
                {
                    // Read gallery templates and enroll them
                    NBiometricTask enrollTask = _matcher.CreateTask(NBiometricOperations.Enroll, null);
                    for (int i = 0; i < listTemplatesBase.Count; ++i)
                    {
                        enrollTask.Subjects.Add(CreateSubject(listTemplatesBase[i].Data, string.Format("{0}", i)));
                    }
                    _matcher.PerformTask(enrollTask);
                    NBiometricStatus status = enrollTask.Status;
                    if (status != NBiometricStatus.Ok)
                    {
                        msg = "Verifinger7.Verify - Enrollment fallo (enrollTask). Status: " + status;
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_IDENTIFY;
                    }

                    // Identify probe subject
                    status = _matcher.Identify(currentSubject);
                    if (status == NBiometricStatus.Ok)
                    {
                        foreach (var matchingResult in currentSubject.MatchingResults)
                        {
                            //Console.WriteLine("Matched with ID: '{0}' with score {1}", matchingResult.Id, matchingResult.Score);
                            if (matchingResult.MatchingDetails != null)
                            {
                                //Console.WriteLine(MatchingDetailsToString(matchingResult.MatchingDetails));
                                score = matchingResult.Score;
                                if (score >= _threshold)
                                {
                                    verifyOk = true;
                                    //Si es Matching type first => Retorno
                                    if (this._matchingType == 1)
                                    {
                                        bestscore = score;
                                        templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                        break;
                                    }
                                    else
                                    {
                                        if (score > bestscore)
                                        {
                                            templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                            bestscore = score;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        msg = "Verifinger7.Verify - _matcher.Identify Status: " + status;
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        //return Errors.IERR_IDENTIFY;
                    }
                }

                //3.- Si verifico bien armo salida
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                if (verifyOk)
                {
                    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                }
                else
                {
                    paramout.SetValue("message", "No hubo matching positivo");
                }
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Verifinger7.Matcher.Verify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_VERIFY;
            }

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// y lista contra que verificar, parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;

            try
            {
                ////0.-Deserializo xmlimput
                //paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                ////1. Importo template de registro, creo cada uno y los importo
                //try
                //{
                //    byCurrentTemplate = Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                //}
                //catch (Exception ex)
                //{
                //    msg = "Bio.Core.Matcher.Verifinger7.Matcher Error importando CurrentTemplate " +
                //         "[ex=" + ex.Message + "]";
                //    LOG.Fatal(msg);
                //    paramout.SetValue("message", msg);
                //    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                //    return Errors.IERR_BAD_PARAMETER;
                //}

                ////2.- Genero objeto para matching
                //NFMatcher _matcher = new NFMatcher();
                //_matcher.MatchingThreshold = Convert.ToInt32(_threshold);
                //LOG.Debug("Verifinger7.Identify - _matcher.MatchingThreshold = " + _matcher.MatchingThreshold);

                ////Deserializo la lista de templates pasados contra que verificar
                //List<Bio.Core.Matcher.Template> listTemplatesBase =
                //    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                //                (string)paramin.GetValue("listBaseTemplates"));
                //LOG.Debug("Verifinger7.Identify - listTemplatesBase != null => " + (listTemplatesBase != null));
                //if (listTemplatesBase != null) LOG.Debug("Verifinger7.Identify - listTemplatesBase.count => " + listTemplatesBase.Count);

                ////Contendra el template contra el que se verifico positivametne si asi fue,
                ////para datos de bodypart, companyid, etc.
                //Bio.Core.Matcher.Template templateVerified = null;

                ////Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                //_matcher.IdentifyStart(byCurrentTemplate);
                //for (int i = 0; i < listTemplatesBase.Count; ++i)
                //{
                //    score = _matcher.IdentifyNext(listTemplatesBase[i].Data);
                //    if (score >= _threshold)
                //    {
                //        verifyOk = true;
                //        //Si es Matching type first => Retorno
                //        if (this._matchingType == 1)
                //        {
                //            bestscore = score;
                //            templateVerified = listTemplatesBase[i];
                //            break;
                //        }
                //        else
                //        {
                //            if (bestscore > score)
                //            {
                //                templateVerified = listTemplatesBase[i];
                //                bestscore = score;
                //            }
                //        }
                //    }
                //}
                //_matcher.IdentifyEnd();

                ////3.- Si verifico bien armo salida
                //paramout.AddValue("score", bestscore);
                //paramout.AddValue("result", verifyOk ? 1 : 2);
                //paramout.AddValue("threshold", threshold);
                //if (verifyOk)
                //{
                //    paramout.AddValue("id", templateVerified == null ? 0 : templateVerified.Id);
                //    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                //    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                //    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                //    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                //}
                //else
                //{
                //    paramout.SetValue("message", "No hubo matching positivo");
                //}
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                if (!VfeUtils.IsLicenseConfigured)
                {
                    if (!VfeUtils.ConfigureLicense(_parameters))
                    {
                        msg = "Bio.Core.Matcher.Verifinger7.Matcher Error Chequeando Licencia";
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_LICENSE;
                    }
                }

                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                //1. Importo template de registro, creo cada uno y los importo
                try
                {
                    byCurrentTemplate = Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.Verifinger7.Matcher Error importando CurrentTemplate " +
                         "[ex=" + ex.Message + "]";
                    LOG.Fatal(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Genero objeto para matching
                NBiometricClient _matcher = new NBiometricClient();
                // Set paramater to return matching details
                _matcher.MatchingWithDetails = true;
                _matcher.MatchingThreshold = Convert.ToInt32(_threshold);
                LOG.Debug("Verifinger7.Identify - _matcher.MatchingThreshold = " + _matcher.MatchingThreshold);
                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                                (string)paramin.GetValue("listBaseTemplates"));
                LOG.Debug("Verifinger7.Identify - listTemplatesBase != null => " + (listTemplatesBase != null));
                if (listTemplatesBase != null) LOG.Debug("Verifinger7.Identify - listTemplatesBase.count => " + listTemplatesBase.Count);

                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                //Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                // Read probe template
                using (NSubject currentSubject = CreateSubject(byCurrentTemplate, "CurrentSubject"))
                {
                    // Read gallery templates and enroll them
                    NBiometricTask enrollTask = _matcher.CreateTask(NBiometricOperations.Enroll, null);
                    for (int i = 1; i < listTemplatesBase.Count; ++i)
                    {
                        enrollTask.Subjects.Add(CreateSubject(listTemplatesBase[i].Data, string.Format("{0}", i)));
                    }
                    _matcher.PerformTask(enrollTask);
                    NBiometricStatus status = enrollTask.Status;
                    if (status != NBiometricStatus.Ok)
                    {
                        msg = "Verifinger7.Identify - Enrollment fallo (enrollTask). Status: " + status;
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_IDENTIFY;
                    }

                    // Identify probe subject
                    status = _matcher.Identify(currentSubject);
                    if (status == NBiometricStatus.Ok)
                    {
                        foreach (var matchingResult in currentSubject.MatchingResults)
                        {
                            //Console.WriteLine("Matched with ID: '{0}' with score {1}", matchingResult.Id, matchingResult.Score);
                            if (matchingResult.MatchingDetails != null)
                            {
                                //Console.WriteLine(MatchingDetailsToString(matchingResult.MatchingDetails));
                                score = matchingResult.Score;
                                if (score >= _threshold)
                                {
                                    verifyOk = true;
                                    //Si es Matching type first => Retorno
                                    if (this._matchingType == 1)
                                    {
                                        bestscore = score;
                                        templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                        break;
                                    }
                                    else
                                    {
                                        if (score > bestscore)
                                        {
                                            templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                            bestscore = score;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        msg = "Verifinger7.Identify - _matcher.Identify Status: " + status;
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_IDENTIFY;
                    }
                }
                if (_matcher != null) _matcher.Dispose();

                //3.- Si verifico bien armo salida
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                if (verifyOk)
                {
                    paramout.AddValue("id", templateVerified == null ? 0 : templateVerified.Id);
                    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                }
                else
                {
                    paramout.SetValue("message", "No hubo matching positivo");
                }
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Verifinger7.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// y nuevo, parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(ITemplate template1, ITemplate template2, out float score)
        {
            score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            string msg;
            //byte[] byCurrentTemplate1;
            //byte[] byCurrentTemplate2;
            bool verifyOk = false;

            try
            {
                if (!VfeUtils.IsLicenseConfigured)
                {
                    if (!VfeUtils.ConfigureLicense(_parameters))
                    {
                        msg = "Bio.Core.Matcher.Verifinger7.Matcher Error Chequeando Licencia VF7";
                        LOG.Fatal(msg);
                        return Errors.IERR_LICENSE;
                    }
                }

                //0.-Deserializo xmlimput
                //paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                //1. Importo template de registro, creo cada uno y los importo
                try
                {
                    //byCurrentTemplate1 = template1.Data; //Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                    //LOG.Debug("Verifinger7.Verify - byCurrentTemplate1 Seteado = " + Convert.ToBase64String(byCurrentTemplate1));
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.Verifinger7.Matcher Error importando CurrentTemplate " +
                         "[ex=" + ex.Message + "]";
                    LOG.Fatal(msg);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Genero objeto para matching
                //NBiometricClient _matcher = new NBiometricClient();
                // Set paramater to return matching details
                _matcher.MatchingWithDetails = true;
                _matcher.MatchingThreshold = Convert.ToInt32(_threshold);
                LOG.Debug("Verifinger7.Verify - _matcher.MatchingThreshold = " + _matcher.MatchingThreshold);
                //Deserializo la lista de templates pasados contra que verificar
                //List<Bio.Core.Matcher.Template> listTemplatesBase =
                //    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                //                (string)paramin.GetValue("listBaseTemplates"));
                //LOG.Debug("Verifinger7.Verify - listTemplatesBase != null => " + (listTemplatesBase != null));
                //if (listTemplatesBase != null) LOG.Debug("Verifinger7.Verify - listTemplatesBase.count => " + listTemplatesBase.Count);

                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                //Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                // Read probe template
                //using (NSubject currentSubject1 = CreateSubject(byCurrentTemplate1, "CurrentSubject1"))
                //{

                currentSubject1.SetTemplateBuffer(new Neurotec.IO.NBuffer(template1.Data));
                currentSubject1.Id = "t1";  

                    //byCurrentTemplate2 = template2.Data;
                    //NSubject currentSubject2 = CreateSubject(byCurrentTemplate2, "CurrentSubject2");

                currentSubject2.SetTemplateBuffer(new Neurotec.IO.NBuffer(template2.Data));
                    currentSubject2.Id = "t2";
                    LOG.Debug("Verifinger7.Verify - byCurrentTemplate2 Seteado = " + Convert.ToBase64String(template2.Data));

                    LOG.Debug("Verifinger7.Verify - Entro a _matcher.Verify(currentSubject1, currentSubject2)...");
                    status = _matcher.Verify(currentSubject1, currentSubject2);
                    LOG.Debug("Verifinger7.Verify - Salgo de _matcher.Verify(currentSubject1, currentSubject2). Status = " + status.ToString());
                    
                    // Read gallery templates and enroll them
                    //NBiometricTask enrollTask = _matcher.CreateTask(NBiometricOperations.Enroll, null);
                    //for (int i = 0; i < listTemplatesBase.Count; ++i)
                    //{
                    //    enrollTask.Subjects.Add(CreateSubject(listTemplatesBase[i].Data, string.Format("{0}", i)));
                    //}
                    //_matcher.PerformTask(enrollTask);
                    //NBiometricStatus status = enrollTask.Status;
                    //if (status != NBiometricStatus.Ok)
                    //{
                    //    msg = "Verifinger7.Verify - Enrollment fallo (enrollTask). Status: " + status;
                    //    LOG.Fatal(msg);
                    //    paramout.SetValue("message", msg);
                    //    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    //    return Errors.IERR_IDENTIFY;
                    //}

                    //// Identify probe subject
                    //status = _matcher.Identify(currentSubject);
                    if (status == NBiometricStatus.Ok)
                    {
                        foreach (var matchingResult in currentSubject1.MatchingResults)
                        {
                            //Console.WriteLine("Matched with ID: '{0}' with score {1}", matchingResult.Id, matchingResult.Score);
                            if (matchingResult.MatchingDetails != null)
                            {
                                //Console.WriteLine(MatchingDetailsToString(matchingResult.MatchingDetails));
                                score = matchingResult.Score;
                                LOG.Debug("Verifinger7.Verify - score = " + score.ToString());
                                if (score >= _threshold)
                                {
                                    verifyOk = true;
                                    //Si es Matching type first => Retorno
                                    if (this._matchingType == 1)
                                    {
                                        bestscore = score;
                                        //templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                        break;
                                    }
                                    else
                                    {
                                        if (score > bestscore)
                                        {
                                            //templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                            bestscore = score;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        msg = "Verifinger7.Verify - _matcher.Identify Status: " + status;
                        LOG.Debug(msg);
                        //return Errors.IERR_IDENTIFY;
                    }
                //}

                    status = NBiometricStatus.None;

                //3.- Si verifico bien armo salida
                //paramout.AddValue("score", bestscore);
                //paramout.AddValue("result", verifyOk ? 1 : 2);
                //paramout.AddValue("threshold", threshold);
                //if (verifyOk)
                //{
                //    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                //    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                //    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                //    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                //}
                //else
                //{
                //    paramout.SetValue("message", "No hubo matching positivo");
                //}
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Verifinger7.Matcher.Verify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                return Errors.IERR_VERIFY;
            }

            return Errors.IERR_OK;
        }

        public int Identify(ITemplate templateCurrent, List<Bio.Core.Matcher.Template> listTemplatePeople, 
                            out float score, out int idBir)
        {
            idBir = 0;
            score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;

            try
            {

                if (!VfeUtils.IsLicenseConfigured)
                {
                    if (!VfeUtils.ConfigureLicense(_parameters))
                    {
                        msg = "Bio.Core.Matcher.Verifinger7.Matcher Error Chequeando Licencia VF7";
                        LOG.Fatal(msg);
                        return Errors.IERR_LICENSE;
                    }
                }

                byCurrentTemplate = templateCurrent.Data;

                //2.- Genero objeto para matching
                //NBiometricClient _matcher = new NBiometricClient();
                // Set paramater to return matching details
                _matcher.MatchingWithDetails = true;
                _matcher.MatchingThreshold = Convert.ToInt32(_threshold);
                LOG.Debug("Verifinger7.Identify - _matcher.MatchingThreshold = " + _matcher.MatchingThreshold);
                //Deserializo la lista de templates pasados contra que verificar
                listTemplatesBase = listTemplatePeople;
                LOG.Debug("Verifinger7.Identify - listTemplatesBase != null => " + (listTemplatesBase != null));
                if (listTemplatesBase != null) LOG.Debug("Verifinger7.Identify - listTemplatesBase.count => " + listTemplatesBase.Count);

                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                templateVerified = null;

                //Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                // Read probe template
                using (NSubject currentSubject = CreateSubject(byCurrentTemplate, "CurrentSubject"))
                {
                    // Read gallery templates and enroll them
                    enrollTask = _matcher.CreateTask(NBiometricOperations.Enroll, null);
                    for (int i = 0; i < listTemplatesBase.Count; ++i)
                    {
                        enrollTask.Subjects.Add(CreateSubject(listTemplatesBase[i].Data, string.Format("{0}", i)));
                    }
                    _matcher.PerformTask(enrollTask);
                    status = enrollTask.Status;
                    if (status != NBiometricStatus.Ok)
                    {
                        msg = "Verifinger7.Identify - Enrollment fallo (enrollTask). Status: " + status;
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_IDENTIFY;
                    }

                    // Identify probe subject
                    status = _matcher.Identify(currentSubject);
                    enrollTask = null;
              
                    if (status == NBiometricStatus.Ok)
                    {
                        foreach (var matchingResult in currentSubject.MatchingResults)
                        {
                            //Console.WriteLine("Matched with ID: '{0}' with score {1}", matchingResult.Id, matchingResult.Score);
                            if (matchingResult.MatchingDetails != null)
                            {
                                //Console.WriteLine(MatchingDetailsToString(matchingResult.MatchingDetails));
                                score = matchingResult.Score;
                                if (score >= _threshold)
                                {
                                    verifyOk = true;
                                    //Si es Matching type first => Retorno
                                    if (this._matchingType == 1)
                                    {
                                        bestscore = score;
                                        templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                        break;
                                    }
                                    else
                                    {
                                        if (score > bestscore)
                                        {
                                            templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                            bestscore = score;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        msg = "Verifinger7.Identify - _matcher.Identify Status: " + status;
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_IDENTIFY;
                    }
                }

                //3.- Si verifico bien armo salida
                score = (float)bestscore;
                idBir = templateVerified == null ? 0 : templateVerified.Id;

                //paramout.AddValue("score", bestscore);
                //paramout.AddValue("result", verifyOk ? 1 : 2);
                //paramout.AddValue("threshold", threshold);
                //if (verifyOk)
                //{
                //    paramout.AddValue("id", templateVerified == null ? 0 : templateVerified.Id);
                //    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                //    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                //    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                //    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                //}
                //else
                //{
                //    paramout.SetValue("message", "No hubo matching positivo");
                //}
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Verifinger7.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

         private static NSubject CreateSubject(byte[] template, string subjectId)
        {
            var subject = new NSubject();
            subject.SetTemplateBuffer(new Neurotec.IO.NBuffer(template));
            subject.Id = subjectId;

            return subject;
        }

#endregion

        public void Dispose()
        {
            if (VfeUtils.IsLicenseConfigured)
            {
                VfeUtils.ReleaseLicenses();
            }
        }

      

       
        int IMatcher.Identify(ITemplate templateCurrent, List<ITemplate> listTemplatePeople, out float score, out string idBir)
        {
            throw new NotImplementedException();
        }

        int IMatcher.Identify(ITemplate templateCurrent, out float score, out int idBir)
        {
            throw new NotImplementedException();
        }

        int IMatcher.Inicialize(string xmlparam)
        {
            throw new NotImplementedException();
        }

        int IMatcher.Enroll(List<ITemplate> listTemplatePeople)
        {
            throw new NotImplementedException();
        }

        void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
