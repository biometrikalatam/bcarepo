﻿
using System;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using Bio.Core.Wsq.Decoder;
using DPUruNet;
using log4net;

namespace Bio.Core.Matcher.ANSI_378_2004
{
    public class Extractor : IExtractor
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Extractor));

#region Implementation of IExtractor

        private int _authenticationFactor;

        private int _minutiaeType;

        private double _threshold;

        private string _parameters;

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        /// <summary>
        /// Tecnologia a utilizar para extraccion
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo d eminucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de extracción considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Extrae desde el template ingresado en el parametro inicial, de acuerdo a 
        /// los adtos de tecnologia y minucias seteados, el template resultante. 
        /// Estos parámetros están serializados en xml, por lo que primero se deserializa.
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="xmloutput">template generado serializado en xml</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out string xmloutput)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero entrega un ITemplate
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out ITemplate templateout)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero ingresa un ITemplate y entrega un ITemplate
        /// </summary>
        /// <param name="templatebase">Template base para extraccion</param>
        /// <param name="destination">Determina si es template para 1-Verify | 2-Enroll </param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(ITemplate templatebase, int destination, out ITemplate templateout)
        {
            int res = Errors.IERR_OK;
            templateout = null;

            try
            {
                LOG.Debug("ANSI_378_2004.Extractor.Extract IN...");
                if (!ANSIUtils.IsInitialized)
                {
                    if (!ANSIUtils.Initialize(_parameters)) return Errors.IERR_BAD_PARAMETER;
                }
                LOG.Debug("ANSI_378_2004.Extractor.Extract Check Initialized OK"); 
                if (templatebase == null) return Errors.IERR_NULL_TEMPLATE;
                if (templatebase.Data == null) return Errors.IERR_NULL_TEMPLATE;

                if (templatebase.AuthenticationFactor != 
                    Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT ||
                    (templatebase.MinutiaeType != Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004 &&
                     templatebase.MinutiaeType != Constant.MinutiaeType.MINUTIAETYPE_WSQ &&
                     templatebase.MinutiaeType != Constant.MinutiaeType.MINUTIAETYPE_RAW))
                {
                    return Errors.IERR_INVALID_TEMPLATE;
                }
                LOG.Debug("ANSI_378_2004.Extractor.Extract Check MinutiaeType OK");     
 
                //Depende de la minucia que venga, hago:
                //  1.- Si es ANSI = Ya viene generado, solo asigno
                //  2.- Si es RAW => extraigo minucias ANSI (Con DP preferente x ser gratuito) o VF si prueva no anda con DP)
                //  3.- Si es WSQ => descomrpimo y trato como RAW
                templateout = templatebase;
                switch (templatebase.MinutiaeType)
                {
                    case 13: //Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004:
                        if (ANSIUtils._GeneraconANSI)
                        {
                            templateout.Data = ((ANSI_378_2004.Template) templatebase).Data; // DataANSI378;
                            LOG.Debug("ANSI_378_2004.Extractor.Extract    Template ANSI Asignado => " + Convert.ToBase64String(templateout.Data));
                        }
                        else
                        {
                            LOG.Debug("ANSI_378_2004.Extractor.Extract    Template NO Habilitado para trabajar con este Matcher (_GeneraconANSI=false)");
                            return Errors.IERR_BAD_PARAMETER;
                        } 
                        break;
                    case 21: //Constant.MinutiaeType.MINUTIAETYPE_WSQ:
                    case 22: //Constant.MinutiaeType.MINUTIAETYPE_RAW:
                        bool rawOK = false;
                        byte[] raw = null;
                        short w, h;
                        if (ANSIUtils._GeneraconWSQ || ANSIUtils._GeneraconRAW)
                        {

                            if (templatebase.MinutiaeType == 21) //Constant.MinutiaeType.MINUTIAETYPE_WSQ
                            {
                                LOG.Debug("ANSI_378_2004.Extractor.Extract - In WSQ...");
                                //1.- Descomprimo WSQ
                                WsqDecoder decoder = new WsqDecoder();
                                byte[] wsq = templatebase.Data;
                                if (!decoder.DecodeMemory(wsq, out w, out h, out raw))
                                {
                                    res = Errors.IERR_WSQ_DECOMPRESSING; 
                                    LOG.Error("ANSI_378_2004.Extractor.Extract Error [decoder.DecodeMemory]");
                                }
                                else
                                {
                                    rawOK = true;
                                }
                                LOG.Debug("ANSI_378_2004.Extractor.Extract - OUT WSQ!");
                            } else
                            {
                                raw = templatebase.Data;
                                rawOK = true;
                                LOG.Debug("ANSI_378_2004.Extractor.Extract - RAW Recibido...");
                            }
                            if (rawOK)
                            {
                                LOG.Debug("ANSI_378_2004.Extractor.Extract FeatureExtraction.CreateFmdFromRaw IN...");
                                LOG.Debug("ANSI_378_2004.Extractor.Extract    RAW => " + Convert.ToBase64String(raw));
                                DataResult<Fmd> resultConversion1 = FeatureExtraction.CreateFmdFromRaw(raw, 1, 0, 512, 512, 500, Constants.Formats.Fmd.ANSI);
                                LOG.Debug("ANSI_378_2004.Extractor.Extract FeatureExtraction.CreateFmdFromRaw OUT!");
                                if (resultConversion1.ResultCode == Constants.ResultCode.DP_SUCCESS)
                                {
                                    LOG.Debug("ANSI_378_2004.Extractor.Extract FeatureExtraction.CreateFmdFromRaw OK");
                                    templateout.Data = resultConversion1.Data.Bytes;
                                    templateout.MinutiaeType = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004;
                                    LOG.Debug("ANSI_378_2004.Extractor.Extract    Template ANSI Generado => " + Convert.ToBase64String(templateout.Data));
                                }
                                else
                                {
                                    LOG.Debug("ANSI_378_2004.Extractor.Extract Error Extracting [ResultCode=" + resultConversion1.ResultCode.ToString() + "]");
                                    return Errors.IERR_EXTRACTING;
                                }
                            }

                        } else
                        {
                            LOG.Debug("ANSI_378_2004.Extractor.Extract    Template NO Habilitado para trabajar con este Matcher (_GeneraconRAW=false && _GeneraconWSQ=false)");
                            return Errors.IERR_BAD_PARAMETER;
                        } 
                        break;
                    //case 22: //Constant.MinutiaeType.MINUTIAETYPE_RAW:
                    //    if (ANSIUtils._GeneraconRAW)
                    //    {
                    //        //TODO 
                    //    }
                    //    else
                    //    {
                    //        return Errors.IERR_BAD_PARAMETER;
                    //    }   
                    //    break;
                    default:
                        LOG.Debug("ANSI_378_2004.Extractor.Extract Invalid Template Out!");
                        return Errors.IERR_INVALID_TEMPLATE;
                }              
                if (templateout.Data != null)
                    templateout.AdditionalData = "SizeData=" + templateout.Data.Length.ToString();
            }
            catch (Exception ex)
            {
                res = Errors.IERR_EXTRACTING;
                LOG.Error("Bio.Core.Matcher.ANSI_378_2004.Extract(T, T) Error", ex);
            }
            return res;
        }

#endregion Implementation of IExtractor

        public void Dispose() 
        {
            
        }
    }
}
