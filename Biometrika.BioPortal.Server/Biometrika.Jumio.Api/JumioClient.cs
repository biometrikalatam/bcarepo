﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using RestSharp;

namespace Biometrika.Jumio.Api
{
    public class JumioClient
    {
        private string _baseURLOnBoarding = "https://netverify.com/api/v4/";
        private string _baseURLAuthentication = "https://netverify.com/api/authentication/v1/web/";
        private string _baseURLGet = "https://netverify.com/api/netverify/v2/scans/";
        private string _baseURLGetAuth = "https://netverify.com/api/netverify/v2/authentications/";
        private string _token;
        private string _secret;
        private int _timeoutWS = 60000;

        private HttpClient client;

        private static readonly ILog LOG = LogManager.GetLogger(typeof(JumioClient));

        public JumioClient(string baseURLOnBoarding, string baseURLAuthentication, string baseURLGet, string baseURLGetAuth,
                            int timeout, string token, string secret)
        {
            _baseURLOnBoarding = baseURLOnBoarding;
            _baseURLAuthentication = baseURLAuthentication;
            _baseURLGet = baseURLGet;
            _baseURLGetAuth = baseURLGetAuth;
            _token = token;
            _secret = secret;
            _timeoutWS = timeout;
        }

#region Sync Zone

        public InitiateResponse initiateEnrollSync(string customerInternalReference, string userReference, string callbackUrl)
        {
            try
            {
                LOG.Error("JumioClient.initiateEnrollSync IN...");
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                LOG.Error("JumioClient.initiateEnrollSync Url = " + _baseURLOnBoarding + "initiate");
                var client = new RestClient(_baseURLOnBoarding + "initiate");
                client.Timeout = _timeoutWS;
                var request = new RestRequest(Method.POST);
                client.UserAgent = "biometrika";
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                string sendToken = "Basic " +
                    Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", _token, _secret)));
                request.AddHeader("Authorization", sendToken);
                string jsonParams = JsonConvert.SerializeObject(new
                {
                    customerInternalReference,
                    userReference,
                    callbackUrl
                });
                request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                LOG.Debug("JumioClient.initiateEnrollSync response = " + ((response == null) ? "NULL" : response.StatusCode.ToString()));
                LOG.Debug("JumioClient.initiateEnrollSync response.contenct = " + ((response == null || response.Content == null) ? "NULL" : response.Content.ToString()));
                return JsonConvert.DeserializeObject<InitiateResponse>(response.Content);
            }
            catch (Exception ex)
            {
                LOG.Error("JumioClient.initiateEnrollSync Error: " + ex.Message);
            }
            return null; 
        }

        public object scansStatusAuthSync(string trackid3ro, object p)
        {
            throw new NotImplementedException();
        }

        public InitiateResponse initiateAuthSync(string enrollmentTransactionReference, string userReference, string callbackUrl)
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                LOG.Error("JumioClient.initiateAuthSync Url = " + _baseURLAuthentication + "initiate");
                var client = new RestClient(_baseURLAuthentication + "initiate");
                client.Timeout = _timeoutWS;
                var request = new RestRequest(Method.POST);
                client.UserAgent = "biometrika";
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "*/*");  //"application/json");
                string sendToken = "Basic " +
                    Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", _token, _secret)));
                request.AddHeader("Authorization", sendToken);
                //string successUrl = "https://www.yourcompany.com/success";
                //string errorUrl = "https://www.yourcompany.com/error";
                //string locale = "es";
                string jsonParams = JsonConvert.SerializeObject(new
                {
                    enrollmentTransactionReference,
                    callbackUrl,
                    userReference //,
                                  //successUrl,
                                  //errorUrl,
                                  //locale
                });
                request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                LOG.Debug("JumioClient.initiateAuthSync response = " + ((response == null) ? "NULL" : response.StatusCode.ToString()));
                LOG.Debug("JumioClient.initiateAuthSync response.contenct = " + ((response == null || response.Content == null) ? "NULL" : response.Content.ToString()));
                return JsonConvert.DeserializeObject<InitiateResponse>(response.Content);
            }
            catch (Exception ex)
            {
                LOG.Error("JumioClient.initiateAuthSync Error: " + ex.Message);
            }
            return null;
        }

        

        public StatusResponse scansStatusSync(string scanReference, string option)
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                LOG.Error("JumioClient.scansStatusSync Url = " + _baseURLGet + scanReference);
                var client = new RestClient(_baseURLGet + scanReference);
                client.Timeout = _timeoutWS;
                var request = new RestRequest(Method.GET);
                client.UserAgent = "biometrika";
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                string sendToken = "Basic " +
                    Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", _token, _secret)));
                request.AddHeader("Authorization", sendToken);
                IRestResponse response = client.Execute(request);
                LOG.Debug("JumioClient.scansStatusSync response = " + ((response == null) ? "NULL" : response.StatusCode.ToString()));
                return JsonConvert.DeserializeObject<StatusResponse>(response.Content);
            }
            catch (Exception ex)
            {
                LOG.Error("JumioClient.scansStatusSync Error: " + ex.Message);
            }
            return null;
        }

        public DataResponse scansDataSync(string scanReference, string option)
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                LOG.Error("JumioClient.scansDataSync Url = " + _baseURLGet + scanReference + "/data");
                var client = new RestClient(_baseURLGet + scanReference + "/data");
                client.Timeout = _timeoutWS;
                var request = new RestRequest(Method.GET);
                client.UserAgent = "biometrika";
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                string sendToken = "Basic " +
                    Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", _token, _secret)));
                request.AddHeader("Authorization", sendToken);
                IRestResponse response = client.Execute(request);
                LOG.Debug("JumioClient.scansDataSync response = " + ((response == null) ? "NULL" : response.StatusCode.ToString()));

                //string responseReformated = JumioHelper.ProcessResponse(response.Content);
                return JsonConvert.DeserializeObject<DataResponse>(response.Content);
                //JumioHelper.ProcessResponse(response.Content);  //
            }
            catch (Exception ex)
            {
                LOG.Error("JumioClient.scansDataSync Error: " + ex.Message);
            }
            return null;
        }


        public ImagesResponse scansImageSync(string scanReference, string option)
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                LOG.Error("JumioClient.scansImageSync Url = " + _baseURLGet + scanReference + "/images");
                var client = new RestClient(_baseURLGet + scanReference + "/images");
                client.Timeout = _timeoutWS;
                var request = new RestRequest(Method.GET);
                client.UserAgent = "biometrika";
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                string sendToken = "Basic " +
                    Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", _token, _secret)));
                request.AddHeader("Authorization", sendToken);
                IRestResponse response = client.Execute(request);
                LOG.Debug("JumioClient.scansImageSync response = " + ((response == null) ? "NULL" : response.StatusCode.ToString()));
                return JsonConvert.DeserializeObject<ImagesResponse>(response.Content);
            }
            catch (Exception ex)
            {
                LOG.Error("JumioClient.scansImageSync Error: " + ex.Message);
            }
            return null;
        }


        public byte[] downloadSync(string scanReference, string option)
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                LOG.Error("JumioClient.downloadSync Url = " + _baseURLGet + scanReference + "/images" + "/" + option);
                var client = new RestClient(_baseURLGet + scanReference + "/images" + "/" + option);
                client.Timeout = _timeoutWS;
                var request = new RestRequest(Method.GET);
                client.UserAgent = "biometrika";
                //request.AddHeader("Content-Type", "*/*");
                request.AddHeader("Accept", "*/*");
                string sendToken = "Basic " +
                    Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", _token, _secret)));
                request.AddHeader("Authorization", sendToken);
                request.AddParameter("text/plain", "", ParameterType.RequestBody);
                //byte[] response = client.DownloadData(request);
                IRestResponse response = client.Execute(request);
                LOG.Debug("JumioClient.downloadSync response = " + ((response == null) ? "NULL" : response.StatusCode.ToString()));
                //System.IO.File.WriteAllBytes(@"c:\tmp\im.jpg", response.RawBytes); // UTF8Encoding.UTF8.GetBytes(response.Content));

                return response.RawBytes; // JsonConvert.DeserializeObject<ImagesResponse>(response.Content);
            }
            catch (Exception ex)
            {
                LOG.Error("JumioClient.downloadSync Error: " + ex.Message);
            }
            return null;
        }

        public byte[] downloadFacemapSync(string scanReference, string option)
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                LOG.Error("JumioClient.downloadSync Url = " + _baseURLGet + scanReference + "/data" + "/" + option);
                var client = new RestClient(_baseURLGet + scanReference + "/data" + "/" + option);
                client.Timeout = _timeoutWS;
                var request = new RestRequest(Method.GET);
                client.UserAgent = "biometrika";
                //request.AddHeader("Content-Type", "*/*");
                request.AddHeader("Accept", "*/*");
                string sendToken = "Basic " +
                    Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", _token, _secret)));
                request.AddHeader("Authorization", sendToken);
                request.AddParameter("text/plain", "", ParameterType.RequestBody);
                //byte[] response = client.DownloadData(request);
                IRestResponse response = client.Execute(request);
                LOG.Debug("JumioClient.downloadSync response = " + ((response == null) ? "NULL" : response.StatusCode.ToString()));
                //System.IO.File.WriteAllBytes(@"c:\tmp\im.jpg", response.RawBytes); // UTF8Encoding.UTF8.GetBytes(response.Content));

                return response.RawBytes; // JsonConvert.DeserializeObject<ImagesResponse>(response.Content);
            }
            catch (Exception ex)
            {
                LOG.Error("JumioClient.downloadSync Error: " + ex.Message);
            }
            return null;
        }

        public byte[] downloadLivenessImageSync(string scanreference, string options)
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                LOG.Error("JumioClient.downloadLivenessImageSync Url = " + _baseURLGet + scanreference + "/images/liveness/" + options);
                var client = new RestClient(_baseURLGet + scanreference + "/images/liveness/" + options);
                client.Timeout = _timeoutWS;
                var request = new RestRequest(Method.GET);
                client.UserAgent = "biometrika";
                //request.AddHeader("Content-Type", "*/*");
                request.AddHeader("Accept", "*/*");
                string sendToken = "Basic " +
                    Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", _token, _secret)));
                request.AddHeader("Authorization", sendToken);
                request.AddParameter("text/plain", "", ParameterType.RequestBody);
                //byte[] response = client.DownloadData(request);
                IRestResponse response = client.Execute(request);
                LOG.Debug("JumioClient.downloadLivenessImageSync response = " + ((response == null) ? "NULL" : response.StatusCode.ToString()));
                //System.IO.File.WriteAllBytes(@"c:\tmp\im.jpg", response.RawBytes); // UTF8Encoding.UTF8.GetBytes(response.Content));
                return response.RawBytes;
            }
            catch (Exception ex)
            {
                LOG.Error("JumioClient.downloadLivenessImageSync Error: " + ex.Message);
            }
            return null;
        }

        public byte[] downloadAuthSync(string scanReference, string option)
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                LOG.Error("JumioClient.downloadAuthSync Url = " + _baseURLGetAuth + scanReference + "/images" + "/" + option);
                var client = new RestClient(_baseURLGetAuth + scanReference + "/images" + "/" + option);
                client.Timeout = _timeoutWS;
                var request = new RestRequest(Method.GET);
                client.UserAgent = "biometrika";
                //request.AddHeader("Content-Type", "*/*");
                request.AddHeader("Accept", "*/*");
                string sendToken = "Basic " +
                    Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", _token, _secret)));
                request.AddHeader("Authorization", sendToken);
                request.AddParameter("text/plain", "", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                LOG.Debug("JumioClient.downloadAuthSync response = " + ((response == null) ? "NULL" : response.StatusCode.ToString()));
                //System.IO.File.WriteAllBytes(@"c:\tmp\im.jpg", response.RawBytes); // UTF8Encoding.UTF8.GetBytes(response.Content));

                return response.RawBytes; // JsonConvert.DeserializeObject<ImagesResponse>(response.Content);
            }
            catch (Exception ex)
            {
                LOG.Error("JumioClient.downloadAuthSync Error: " + ex.Message);
            }
            return null;
        }


        public DataAuthResponse scansDataAuthSync(string scanReferenceAuth, string option)
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                LOG.Error("JumioClient.scansDataAuthSync Url = " + _baseURLGetAuth + scanReferenceAuth);
                var client = new RestClient(_baseURLGetAuth + scanReferenceAuth);
                client.Timeout = _timeoutWS;
                var request = new RestRequest(Method.GET);
                client.UserAgent = "biometrika";
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                string sendToken = "Basic " +
                    Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", _token, _secret)));
                request.AddHeader("Authorization", sendToken);
                IRestResponse response = client.Execute(request);
                LOG.Debug("JumioClient.scansDataAuthSync response = " + ((response == null) ? "NULL" : response.StatusCode.ToString()));
                return JsonConvert.DeserializeObject<DataAuthResponse>(response.Content);
            }
            catch (Exception ex)
            {
                LOG.Error("JumioClient.downloadAuthSync Error: " + ex.Message);
            }
            return null;
        }
#endregion Sync Zone

        #region Async

        public async Task<byte[]> download(string scanReference, string option)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"netverify/v2/scans/{scanReference}/images/{option}");
            var response = await client.SendAsync(request);
            var responseBytes = await response.Content.ReadAsByteArrayAsync();
            //_logger.LogInformation("download Response {}", responseBytes.Length);
            LOG.Debug("download Response => " + responseBytes.Length);
            Console.WriteLine(responseBytes.Length);

            response.EnsureSuccessStatusCode();
            return responseBytes;
        }

        public async Task<InitiateResponse> initiateEnroll(string customerInternalReference, string userReference)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "v4/initiate");

            request.Content = new StringContent(JsonConvert.SerializeObject(new
            {
                customerInternalReference,
                userReference,
            }),
            Encoding.UTF8, "application/json");

            var response = await client.SendAsync(request);
            var responseString = await response.Content.ReadAsStringAsync();
            LOG.Debug("initiate Response => " + responseString);
            Console.WriteLine(responseString);

            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<InitiateResponse>(responseString);
        }

        public async Task<InitiateResponse> initiateAuth(string enrollmentTransactionReference, string callbackUrl)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "authentication/v1/web/initiate");

            request.Content = new StringContent(JsonConvert.SerializeObject(new
            {
                enrollmentTransactionReference,
                callbackUrl,
            }),
            Encoding.UTF8, "application/json");

            var response = await client.SendAsync(request);
            var responseString = await response.Content.ReadAsStringAsync();
            LOG.Debug("initiate Response => " + responseString);
            Console.WriteLine(responseString);

            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<InitiateResponse>(responseString);
        }

        public async Task<string> scans(string scanReference, string option)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"netverify/v2/scans/{scanReference}/{option}");
            var response = await client.SendAsync(request);
            var responseString = await response.Content.ReadAsStringAsync();
            LOG.Debug("initiate Response => " + responseString);
            Console.WriteLine(responseString);

            response.EnsureSuccessStatusCode();
            return responseString;
        }

        #endregion Async
    }

    public class InitiateResponse
    {
        public DateTime Timestamp { get; set; }
        public string TransactionReference { get; set; }
        public string RedirectUrl { get; set; }

        public override string ToString()
        {
            return base.ToString() +
                ", Timestamp=" + Timestamp +
                ", TransactionReference=" + TransactionReference +
                ", RedirectUrl=" + RedirectUrl;
        }
    }

    public class StatusResponse
    {
        public DateTime Timestamp { get; set; }
        public string ScanReference { get; set; }
        public string Status { get; set; }

    }

    public class DataResponse
    {
        public DateTime timestamp { get; set; }
        public string scanReference { get; set; }
        public Document document { get; set; }
        public Transaction transaction { get; set; }
        public Verification verification { get; set; }
        public Facemap facemap { get; set; }

    }
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Document
    {
        public string type { get; set; }
        public string dob { get; set; }
        public string expiry { get; set; }
        public string firstName { get; set; }
        public string gender { get; set; }
        public string idSubtype { get; set; }
        public string issuingCountry { get; set; }
        public string issuingDate { get; set; }
        public string lastName { get; set; }
        public string nacionality { get; set; }
        public string number { get; set; }
        public string status { get; set; }
        public string optionalData1 { get; set; }
        public string optionalData2 { get; set; }


    }

    public class Transaction
    {
        public string clientIp { get; set; }
        public string customerId { get; set; }
        public DateTime date { get; set; }
        public string merchantReportingCriteria { get; set; }
        public string merchantScanReference { get; set; }
        public string source { get; set; }
        public string status { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class IdentityVerification
    {
        public string similarity { get; set; }
        public string similarityDecision { get; set; }
        public double similarityScore { get; set; }
        public string validity { get; set; }
        public string reason { get; set; }

    }

    public class Verification
    {
        public IdentityVerification identityVerification { get; set; }
        public string mrzCheck { get; set; }
        public RejectReason rejectReason { get; set; }
    }

    public class RejectReasonDetail
    {
        public string detailsCode { get; set; }
        public string detailsDescription { get; set; }

    }

    public class RejectReason
    {
        public string rejectReasonCode { get; set; }
        public string rejectReasonDescription { get; set; }
        //public RejectReasonDetail[] rejectReasonDetails { get; set; }
        [JsonConverter(typeof(RejectReasonDetailsConverter))]
        public List<RejectReasonDetail> rejectReasonDetails { get; set; }

    }

    public class Facemap
    {
        public string classifier { get; set; }
        public string href { get; set; }

    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Image
    {
        public string classifier { get; set; }
        public string href { get; set; }

    }

    public class ImagesResponse
    {
        public DateTime timestamp { get; set; }
        public List<Image> images { get; set; }
        public List<string> livenessImages { get; set; }
        public string scanReference { get; set; }

    }

    public class DataAuthResponse
    {
        public string transactionReference { get; set; }
        public string enrollmentTransactionReference { get; set; }
        public string transactionResult { get; set; }
        public string scanSource { get; set; }
        public DateTime transactionDate { get; set; }
        public List<Image> images { get; set; }
        public List<string> livenessImages { get; set; }
    }
}
