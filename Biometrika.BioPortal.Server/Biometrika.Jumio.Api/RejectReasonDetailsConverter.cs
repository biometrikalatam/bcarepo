﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using System.Text.Json;
//using System.Text.Json.Serialization;


namespace Biometrika.Jumio.Api
{
    internal class RejectReasonDetailsConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        //[return: NullableAttribute(2)]
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = null;
            try
            {
                token = JToken.Load(reader);
                if (token.Type == JTokenType.Object)
                {
                    return new List<RejectReasonDetail> { token.ToObject<RejectReasonDetail>(serializer) };
                } else if (token.Type == JTokenType.Array)
                {
                    return token.ToObject<List<RejectReasonDetail>>(serializer);
                }
            } catch (Exception ex)
            {
                string s = ex.Message;
            }
            throw new JsonSerializationException("Unexpected JSON format encountered in RejectReasonDetailsConverter: " + 
                                                 (token!=null?token.ToString():"null"));
        }
    

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    internal class JumioHelper
    {
        internal static DataResponse ProcessResponse(string content)
        {
            DataResponse drRet = null; 
            bool _deserialedOK = false;
            string ret = content;
            try
            {
                
                //Convierto en el tradicional, si anda devuelvo 
                try
                {

                    _deserialedOK = true;
                }
                catch (Exception ex)
                {

                    //LOG.Error(" Error: " + ex.Message);
                }

                if (!_deserialedOK)
                {
                    try
                    {

                        _deserialedOK = true;
                    }
                    catch (Exception ex)
                    {

                        //LOG.Error(" Error: " + ex.Message);
                    }
                }

            }
            catch (Exception ex)
            {
                ret = content;
                //LOG.Error(" Error: " + ex.Message);
            }
            return drRet;
        }
    }
}