﻿using Bio.Core.Api.Constant;
using Biometrika.Tech5.Api.Models;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Tech5.Api
{
    public  class ABISTech5Client
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(ABISTech5Client));

        internal string _API_BASE_PATH = "http://gn-testapi.tech5.tech:8080/T5CloudService/1.0";
        internal string _TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJxTnhuY1Ywc1Z5N2owdGlKdjRvaURkLU5BWXVFb01BSW0zdERXdkJEd2tjIn0.eyJleHAiOjE2NTI5OTU3NjYsImlhdCI6MTY1Mjk5NTQ2NiwianRpIjoiYTY1YTYxMTMtZGExNC00OWEwLWI5NGYtZmY1ZDJlMzNhNzAwIiwiaXNzIjoiaHR0cHM6Ly9mb3hpZDVzc28uZWluemVsbmV0LmNvbS9hdXRoL3JlYWxtcy9zdmVkNSIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiI3ZTZkMGVjMC01YmNkLTQ2YmUtODFhYy03MGM5ZTk3YjZhOTAiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJ3ZWJfYXBwIiwic2Vzc2lvbl9zdGF0ZSI6IjU3NGNkZTQ1LTBhM2ItNDMxOS1iNGVkLWQ0MzdiM2QxZGQ2MCIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cHM6Ly9mb3hpZDUuZWluemVsbmV0LmNvbSJdLCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwic2lkIjoiNTc0Y2RlNDUtMGEzYi00MzE5LWI0ZWQtZDQzN2IzZDFkZDYwIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsIlJPTEVfSU5URUdSQVRJT04iLCJ1bWFfYXV0aG9yaXphdGlvbiIsImRlZmF1bHQtcm9sZXMtc3ZlZDUiXSwibmFtZSI6IkludCBCaW9tZXRyaWthIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiaW5mb0BiaW9tZXRyaWthLmNsIiwiZ2l2ZW5fbmFtZSI6IkludCIsImZhbWlseV9uYW1lIjoiQmlvbWV0cmlrYSIsImVtYWlsIjoiaW5mb0BiaW9tZXRyaWthLmNsIn0.d1impCno7VkFF8PmvHGoAFKzdsiZNdrwiHPldoh1yCzq0fGMjoqa8I4ckwhwcEkUyfYEWRr7r727fnhNsaDdDBFnG4Y1Jasuf0PZOXajdjDJUDCRNj9lAvT-9rjBW5_5F5JJu1nxLuUlVkZSJnbHD5Wo9iLNC2PqYK-pucTvA42ERk8bgox9ykKgfLU8PC_4B8OuA0IiOFi-DiHBGLJ4kkwgG14VE_JXpaIWee6LCc2Eq6Q9ZAmHWoAFWrt0A_Go3tlGziDvklZyvbGbY3e1hFcEeQ93aG8vHjQwyq81FNxISl-NEzqkpTDpLg6BiQaPrLaADEqeAJQmnYIfFrBUJw"; //JWT Token para acceso;
        internal int _TIMEOUT = 30000;

        internal string _API_VERSION = "1.0";
        internal string _API_URL = "";
        internal string _USERNAME = "info@biometrika.cl";
        internal string _USERSECRET = "B1ometrik@";
        internal string _CLIENTID = "web_app";
        internal string _CLIENTSECRET = "42dbc72e-bf12-4773-bea8-bb31692625eb";

        private RestClient _CLIENT;

        internal bool _IsInitialized = false;

        public ABISTech5Client() { }

        public bool IsInitialized { get { return _IsInitialized; } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlbaseservice"></param>
        /// <param name="timeout"></param>
        /// </param>
        /// <returns></returns>
        public int Initialize(string urlbaseservice, string username, string usersecret,
                              string clientid, string clientsecret, out string msgFeedback, int timeout = 30000)
        {
            int ret = 0;
            msgFeedback = null;
            try
            {
                _IsInitialized = false;
                LOG.Debug("ABISTech5Client.Initialize IN...");
                if (!string.IsNullOrEmpty(urlbaseservice) &&
                    !string.IsNullOrEmpty(username) &&
                    !string.IsNullOrEmpty(usersecret) &&
                    !string.IsNullOrEmpty(clientid) &&
                    !string.IsNullOrEmpty(clientsecret))
                {
                    _API_BASE_PATH = urlbaseservice;
                    _TIMEOUT = timeout;
                    _USERNAME = username;
                    _USERSECRET = usersecret;
                    _CLIENTID = clientid;
                    _CLIENTSECRET = clientsecret;
                    LOG.Debug("ABISTech5Client.Initialize - URL Base Service = " + _API_BASE_PATH);
                    LOG.Debug("ABISTech5Client.Initialize - Timeout = " + _TIMEOUT.ToString());
                    LOG.Debug("ABISTech5Client.Initialize - username = " + _USERNAME.ToString());
                    LOG.Debug("ABISTech5Client.Initialize - clienid = " + _CLIENTID.ToString());

                    //Si es sitio con SSL => HAbilito
                    if (_API_BASE_PATH.StartsWith("https"))
                    {
                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    }

                    _CLIENT = new RestClient(_API_BASE_PATH);
                    _CLIENT.Timeout = timeout;

                    string msg;
                    ret = Status(out msgFeedback);
                    LOG.Debug("ABISTech5Client.Initialize - retorno Status = " + ret.ToString() + " [msg=" +
                                (string.IsNullOrEmpty(msgFeedback) ? "" : msgFeedback) + "]");
                    _IsInitialized = (ret == 0);
                }
                else
                {
                    msgFeedback = "ABISTech5Client.Initialize - Alguno de los parametros es nulo...";
                    LOG.Debug(msgFeedback);
                    ret = -6;
                }
            }
            catch (Exception ex)
            {
                _IsInitialized = false;
                ret = -1;
                msgFeedback = "ABISTech5Client.Initialize Excp Error: " + ex.Message;
                LOG.Error(msgFeedback);
                LOG.Error("StackTrace => " + ex.StackTrace);
                LOG.Error("InnerException => " + ex.InnerException);
            }
            LOG.Debug("ABISTech5Client.Initialize - OUT! _IsInitialized = " + _IsInitialized.ToString());
            return ret;
        }


        public int Status(out string msgFeedback)
        {
            int ret = 0;
            msgFeedback = null;
            LOG.Debug("ABISTech5Client.Status IN...");
            try
            {
                var request = new RestRequest(Method.GET);
                _CLIENT.BaseUrl = new Uri(_API_BASE_PATH + "/getStatus");
                LOG.Debug("ABISTech5Client.Status - _CLIENT.BaseUrl = " + _CLIENT.BaseUrl);
                //request.AddHeader("X-Device-License-Key", deviceLicenseKey);
                //request.AddHeader("Content-Type", "application/json");
                //request.AddHeader("Accept", "application/json");

                IRestResponse response = _CLIENT.Execute(request);
                LOG.Debug("FacetecClient.sessionTokenSync response = " + ((response == null) ? "NULL" :
                                   (response.StatusCode != null ? response.StatusCode.ToString() : "response.StatusCode Null")));
                LOG.Debug("FacetecClient.sessionTokenSync response.contenct = " + ((response == null || response.Content == null) ?
                                                                                    "NULL" : response.Content.ToString()));
                //_ZOOMRESPONSE = JsonConvert.DeserializeObject<ZoomResponse<SessionResponse>>(response.Content);

                string responseProcessed = null;
                if (response != null && response.Content != null)
                {
                    responseProcessed = response.Content.Replace("<pre>", "");
                    responseProcessed = responseProcessed.Replace("</pre>", "");
                    responseProcessed = responseProcessed.Replace("<b>", "");
                    responseProcessed = responseProcessed.Replace("</b>", "");
                    responseProcessed = responseProcessed.Replace("<font color='#1279c4'>", "");
                    responseProcessed = responseProcessed.Replace(" <font color='#b02a02'>", "");
                    responseProcessed = responseProcessed.Replace("<font color='#550769'>", "");
                    responseProcessed = responseProcessed.Replace("</font>", "");

                    responseProcessed = responseProcessed.Replace("Current DB Size", "CurrentDBSize");
                    responseProcessed = responseProcessed.Replace("DB License", "DBLicense");
                }

                T5StatusResponse t5Response = JsonConvert.DeserializeObject<T5StatusResponse>(responseProcessed);
                LOG.Debug("ABISTech5Client.Status - T5StatusResponse != null => " + (t5Response != null).ToString());
                LOG.Debug("ABISTech5Client.Status OUT!");
                if (t5Response == null)
                {
                    ret = -2;
                    msgFeedback = "ABISTech5Client.Status - Deserializacion Repsuesta Nula!";
                    LOG.Debug(msgFeedback);
                } else
                {
                    LOG.Debug("ABISTech5Client.Status => " + responseProcessed);
                    msgFeedback = t5Response.LicenseStatus.Name + " - " + t5Response.LicenseStatus.Version 
                                    + "[Q Finger=" +
                                       (string.IsNullOrEmpty(t5Response.LicenseStatus.Slaves.Finger[0].CurrentDBSize) ?
                                                             t5Response.LicenseStatus.Slaves.Finger[1].CurrentDBSize :
                                                             t5Response.LicenseStatus.Slaves.Finger[0].CurrentDBSize ) 
                                    + "]" 
                                    + "[Q Face=" +
                                       (string.IsNullOrEmpty(t5Response.LicenseStatus.Slaves.Face[0].CurrentDBSize) ?
                                                             t5Response.LicenseStatus.Slaves.Face[1].CurrentDBSize :
                                                             t5Response.LicenseStatus.Slaves.Face[0].CurrentDBSize)
                                    + "]"
                                    + "[Q Iris=" +
                                       (string.IsNullOrEmpty(t5Response.LicenseStatus.Slaves.Iris[0].CurrentDBSize) ?
                                                             t5Response.LicenseStatus.Slaves.Iris[1].CurrentDBSize :
                                                             t5Response.LicenseStatus.Slaves.Iris[0].CurrentDBSize)
                                    + "]"
                                    + " => [OK]";
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msgFeedback = "ABISTech5Client.Status Excp [" + ex.Message + "]";
                LOG.Debug(msgFeedback);
            }

            LOG.Debug("ABISTech5Client.Status OUT! ret = " + ret.ToString());
            return ret;
        }

        public int Extract(T5ExtractParamIn paramin, out T5ExtractParamOut paramout, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            paramout = null;
            LOG.Debug("ABISTech5Client.Extract IN...");
            try
            {
                //Check ParamIn
                if (paramin == null || 
                    ((paramin.face_data == null || paramin.face_data.Count == 0) &&
                     (paramin.iris_data == null || paramin.iris_data.Count == 0) &&
                     (paramin.finger_data == null || paramin.finger_data.live_scan_plain == null || paramin.finger_data.live_scan_plain.Count == 0)))
                {
                    msgerr = "ABISTech5Client.Extract - Parametros no puede ser nulo...";
                    LOG.Debug(msgerr);
                    ret = Errors.IERR_BAD_PARAMETER;
                } else
                {
                    paramin.request_type = "CreateTemplates";
                    _CLIENT.BaseUrl = new Uri(_API_BASE_PATH + "/processRequest");
                    LOG.Debug("ABISTech5Client.Extract - Inicio extract en servicio => " + _CLIENT.BaseUrl);
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    var body = JsonConvert.SerializeObject(paramin);
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    IRestResponse response = _CLIENT.Execute(request);

                    if (response == null)
                    {
                        msgerr = "ABISTech5Client.Extract - Respuesta de servicio remoto nula!";
                        LOG.Debug(msgerr);
                        ret = Errors.IERR_CONNECTOR_NOT_AVAILABLE;
                    }
                    else if (response.StatusCode != HttpStatusCode.OK)
                    {
                        msgerr = "ABISTech5Client.Extract - Status NOOK [" + response.StatusCode.ToString() + "]";
                        LOG.Debug(msgerr);
                        ret = Errors.IERR_CONNECTOR_REMOTE_ERROR;
                    }
                    else
                    {
                        LOG.Debug("ABISTech5Client.Extract - Inicio deserializacion respuesta...");
                        paramout = JsonConvert.DeserializeObject<T5ExtractParamOut>(response.Content);
                        if (paramout == null)
                        {
                            msgerr = "ABISTech5Client.Extract - Desrialización de respuesta de con error!";
                            LOG.Debug(msgerr);
                            ret = Errors.IERR_DESERIALIZING_DATA;
                        }
                        else
                        {
                            if ((paramout.response != null && paramout.response.Equals("false")) ||
                                !string.IsNullOrEmpty((string)paramout.error) ||
                                !string.IsNullOrEmpty((string)paramout.errorCode))
                            {
                                msgerr = "ABISTech5Client.Extract - Error procesando tid = " +
                                        (string.IsNullOrEmpty(paramout.tid) ? "Null" : paramout.tid) +
                                        " - [msgerr=" + paramout.errorCode.ToString() + " - " +
                                        (!string.IsNullOrEmpty((string)paramout.error) ? (string)paramout.error : "") + "]";
                                LOG.Debug(msgerr);
                                ret = Errors.IERR_EXTRACTING;
                            } else
                            {
                                LOG.Debug("ABISTech5Client.Extract - Deserialized OK! tid = " +
                                            (string.IsNullOrEmpty(paramout.tid) ? "Null" : paramout.tid));
                                ret = Errors.IERR_OK;
                            }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msgerr = "ABISTech5Client.Extract Excp [" + ex.Message + "]";
                LOG.Debug(msgerr);
            }

            LOG.Debug("ABISTech5Client.Extract OUT! ret = " + ret.ToString());
            return ret;
        }

        public int Verify(T5VerifyParamIn paramin, out T5VerifyParamOut paramout, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            paramout = null;
            LOG.Debug("ABISTech5Client.Verify IN...");
            try
            {
                //Check ParamIn
                if (paramin == null ||
                    ((paramin.face_data == null || paramin.face_data.Count != 2) &&
                     (paramin.iris_data == null || paramin.iris_data.Count == 2) &&
                     (paramin.finger_data == null || paramin.finger_data.live_scan_plain == null || paramin.finger_data.live_scan_plain.Count != 2)))
                {
                    msgerr = "ABISTech5Client.Verify - Parametros no puede ser nulo...";
                    LOG.Debug(msgerr);
                    ret = Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    paramin.request_type = "Verification";
                    _CLIENT.BaseUrl = new Uri(_API_BASE_PATH + "/processRequest");
                    LOG.Debug("ABISTech5Client.Verify - Inicio Verify en servicio => " + _CLIENT.BaseUrl);
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    var body = JsonConvert.SerializeObject(paramin);
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    IRestResponse response = _CLIENT.Execute(request);

                    if (response == null)
                    {
                        msgerr = "ABISTech5Client.Verify - Respuesta de servicio remoto nula!";
                        LOG.Debug(msgerr);
                        ret = Errors.IERR_CONNECTOR_NOT_AVAILABLE;
                    }
                    else if (response.StatusCode != HttpStatusCode.OK)
                    {
                        msgerr = "ABISTech5Client.Verify - Status NOOK [" + response.StatusCode.ToString() + "]";
                        LOG.Debug(msgerr);
                        ret = Errors.IERR_CONNECTOR_REMOTE_ERROR;
                    }
                    else
                    {
                        LOG.Debug("ABISTech5Client.Verify - Inicio deserializacion respuesta...");
                        //var obj = JsonConvert.DeserializeObject(response.Content);
                        T5VerifyParamOutInt t5Response = JsonConvert.DeserializeObject<T5VerifyParamOutInt>(response.Content);
                        

                        if (t5Response == null)
                        {
                            msgerr = "ABISTech5Client.Verify - Desrialización de respuesta de con error!";
                            LOG.Debug(msgerr);
                            ret = Errors.IERR_DESERIALIZING_DATA;
                        }
                        else
                        {
                            if ((string.IsNullOrEmpty(t5Response.response)) ||
                                !string.IsNullOrEmpty((string)t5Response.error))
                            {
                                msgerr = "ABISTech5Client.Verify - Error procesando tid = " +
                                        (string.IsNullOrEmpty(t5Response.tid) ? "Null" : t5Response.tid) +
                                        " - [msgerr=" + t5Response.errorCode.ToString() + " - " +
                                        (!string.IsNullOrEmpty((string)t5Response.error) ? (string)t5Response.error : "") + "]";
                                LOG.Debug(msgerr);
                                ret = Errors.IERR_EXTRACTING;
                            }
                            else
                            {
                                paramout = new T5VerifyParamOut(t5Response.tid, t5Response.encounter_id, t5Response.error,
                                            t5Response.finger_data, t5Response.iris_data, t5Response.face_data, t5Response.request_type,
                                            t5Response.maxResults, t5Response.errorCode, t5Response.faceThreshold, t5Response.fingerThreshold,
                                            t5Response.irisThreshold);

                                //Deserializo a mano el resultado por ahora. Despues ver si se puede automatizar.
                                paramout.response = ParseResponseVerify(t5Response.tid, t5Response.response);
                                LOG.Debug("ABISTech5Client.Verify - Deserialized OK! tid = " +
                                            (string.IsNullOrEmpty(paramout.tid) ? "Null" : paramout.tid));
                                ret = Errors.IERR_OK;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msgerr = "ABISTech5Client.Verify Excp [" + ex.Message + "]";
                LOG.Debug(msgerr);
            }

            LOG.Debug("ABISTech5Client.Verify OUT! ret = " + ret.ToString());
            return ret;
        }

        private List<T5VerifyResponse> ParseResponseVerify(string tid, string response)
        {
            List<T5VerifyResponse> ret = new List<T5VerifyResponse>(); 
            try
            {
                //"response": "{\"finger\":{\"FINGER_T5\":{\"0daa3a8b-caf9-4862-935b-97b8b668ab29\":20.0}}}",
                //"response": "{\"iris\":{\"IRIS_T5\":{\"314348be-3a33-4d19-a254-7ed53d5b6f16\":20.0}}}",
                //"response": "{\"face\":{\"FACE_T5\":{\"314348be-3a33-4d19-a254-7ed53d5b6f16\":20.0}}}",
                //"{\"finger\":{\"FINGER_T5\":{\"314348be-3a33-4d19-a254-7ed53d5b6f16\":20.0}},\"face\":{\"FACE_T5\":{\"314348be-3a33-4d19-a254-7ed53d5b6f16\":11.2865715}}}";
                string[] sArr = response.Split(',');

                foreach (string item in sArr)
                {
                    T5VerifyResponse respAux = new T5VerifyResponse();
                    string[] sarr2 = item.Split(':');
                    string score = sarr2[sarr2.Length - 1].Replace("}", "");
                    respAux.tid = tid;
                    respAux.score = double.Parse(score, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture); 
                    if (item.Contains("FINGER_T5"))
                    {
                        respAux.type = "FINGER_T5";
                    }
                    else if (item.Contains("IRIS_T5")) {
                        respAux.type = "IRIS_T5";
                    } else
                    {
                        respAux.type = "FACE_T5";
                    }
                    ret.Add(respAux);
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Debug("ABISTech5Client.ParseResponse Excp - " + ex.Message);
            }
            return ret;
        }

        
        public int Enroll(T5ExtractParamIn paramin, out T5ExtractParamOut paramout, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            paramout = null;
            LOG.Debug("ABISTech5Client.Enroll IN...");
            try
            {
                //Check ParamIn
                if (paramin == null ||
                    ((paramin.face_data == null || paramin.face_data.Count == 0) &&
                     (paramin.iris_data == null || paramin.iris_data.Count == 0) &&
                     (paramin.finger_data == null || paramin.finger_data.live_scan_plain == null || paramin.finger_data.live_scan_plain.Count == 0)))
                {
                    msgerr = "ABISTech5Client.Enroll - Parametros no puede ser nulo...";
                    LOG.Debug(msgerr);
                    ret = Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    paramin.request_type = "Enroll";
                    _CLIENT.BaseUrl = new Uri(_API_BASE_PATH + "/processRequest");
                    LOG.Debug("ABISTech5Client.Extract - Inicio extract en servicio => " + _CLIENT.BaseUrl);
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    var body = JsonConvert.SerializeObject(paramin);
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    IRestResponse response = _CLIENT.Execute(request);

                    if (response == null)
                    {
                        msgerr = "ABISTech5Client.Enroll - Respuesta de servicio remoto nula!";
                        LOG.Debug(msgerr);
                        ret = Errors.IERR_CONNECTOR_NOT_AVAILABLE;
                    }
                    else if (response.StatusCode != HttpStatusCode.OK)
                    {
                        msgerr = "ABISTech5Client.Enroll - Status NOOK [" + response.StatusCode.ToString() + "]";
                        LOG.Debug(msgerr);
                        ret = Errors.IERR_CONNECTOR_REMOTE_ERROR;
                    }
                    else
                    {
                        LOG.Debug("ABISTech5Client.Enroll - Inicio deserializacion respuesta...");
                        paramout = JsonConvert.DeserializeObject<T5ExtractParamOut>(response.Content);
                        if (paramout == null)
                        {
                            msgerr = "ABISTech5Client.Enroll - Desrialización de respuesta de con error!";
                            LOG.Debug(msgerr);
                            ret = Errors.IERR_DESERIALIZING_DATA;
                        }
                        else
                        {
                            if ((paramout.response != null && paramout.response.Equals("false")) ||
                                !string.IsNullOrEmpty((string)paramout.error) ||
                                !string.IsNullOrEmpty((string)paramout.errorCode))
                            {
                                msgerr = "ABISTech5Client.Enroll - Error procesando tid = " +
                                        (string.IsNullOrEmpty(paramout.tid) ? "Null" : paramout.tid) +
                                        " - [msgerr=" + paramout.errorCode.ToString() + " - " +
                                        (!string.IsNullOrEmpty((string)paramout.error) ? (string)paramout.error : "") + "]";
                                LOG.Debug(msgerr);
                                ret = Errors.IERR_EXTRACTING;
                            }
                            else
                            {
                                LOG.Debug("ABISTech5Client.Enroll - Deserialized OK! tid = " +
                                            (string.IsNullOrEmpty(paramout.tid) ? "Null" : paramout.tid));
                                ret = Errors.IERR_OK;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msgerr = "ABISTech5Client.Enroll Excp [" + ex.Message + "]";
                LOG.Debug(msgerr);
            }

            LOG.Debug("ABISTech5Client.Enroll OUT! ret = " + ret.ToString());
            return ret;
        }

        public int Identify(T5IdentifyParamIn paramin, out T5IdentifyParamOut paramout, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            paramout = null;
            LOG.Debug("ABISTech5Client.Identify IN...");
            try
            {
                //Check ParamIn
                if (paramin == null || (paramin.face_data == null && paramin.iris_data == null && 
                     (paramin.finger_data == null || paramin.finger_data.live_scan_plain == null)))
                {
                    msgerr = "ABISTech5Client.Identify - Parametros no puede ser nulo...";
                    LOG.Debug(msgerr);
                    ret = Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    paramin.request_type = "Identification";
                    paramin.maxResults = 5;
                    paramin.faceThreshold = 6.0;
                    paramin.fingerThreshold = 0.85;
                    paramin.irisThreshold = 0.52;
                    _CLIENT.BaseUrl = new Uri(_API_BASE_PATH + "/processRequest");
                    LOG.Debug("ABISTech5Client.Verify - Inicio Verify en servicio => " + _CLIENT.BaseUrl);
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    var body = JsonConvert.SerializeObject(paramin);
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    IRestResponse response = _CLIENT.Execute(request);

                    if (response == null)
                    {
                        msgerr = "ABISTech5Client.Identify - Respuesta de servicio remoto nula!";
                        LOG.Debug(msgerr);
                        ret = Errors.IERR_CONNECTOR_NOT_AVAILABLE;
                    }
                    else if (response.StatusCode != HttpStatusCode.OK)
                    {
                        msgerr = "ABISTech5Client.Identify - Status NOOK [" + response.StatusCode.ToString() + "]";
                        LOG.Debug(msgerr);
                        ret = Errors.IERR_CONNECTOR_REMOTE_ERROR;
                    }
                    else
                    {
                        LOG.Debug("ABISTech5Client.Identify - Inicio deserializacion respuesta...");
                        T5VerifyParamOutInt t5Response = JsonConvert.DeserializeObject<T5VerifyParamOutInt>(response.Content);

                        if (t5Response == null)
                        {
                            msgerr = "ABISTech5Client.Identify - Desrialización de respuesta de con error!";
                            LOG.Debug(msgerr);
                            ret = Errors.IERR_DESERIALIZING_DATA;
                        }
                        else
                        {
                            LOG.Debug("ABISTech5Client.Identify - Inicio deserializacion de response de candidatos...");
                            if ((string.IsNullOrEmpty(t5Response.response)) ||
                                !string.IsNullOrEmpty((string)t5Response.error))
                            {
                                msgerr = "ABISTech5Client.Identify - Error procesando tid = " +
                                        (string.IsNullOrEmpty(t5Response.tid) ? "Null" : t5Response.tid) +
                                        " - [msgerr=" + t5Response.errorCode.ToString() + " - " +
                                        (!string.IsNullOrEmpty((string)t5Response.error) ? (string)t5Response.error : "") + "]";
                                LOG.Debug(msgerr);
                                ret = Errors.IERR_EXTRACTING;
                            }
                            else
                            {
                                LOG.Debug("ABISTech5Client.Identify - Creo paramout...");
                                paramout = new T5IdentifyParamOut(t5Response.tid, t5Response.encounter_id, t5Response.error,
                                            t5Response.finger_data, t5Response.iris_data, t5Response.face_data, t5Response.request_type,
                                            t5Response.maxResults, t5Response.errorCode, t5Response.faceThreshold, t5Response.fingerThreshold,
                                            t5Response.irisThreshold);

                                LOG.Debug("ABISTech5Client.Identify - Deserializo candidates in...");
                                paramout.Candidates = ParseCandidatesIdentify(t5Response.tid, t5Response.response);
                                LOG.Debug("ABISTech5Client.Identify - Deserializo scores in...");
                                paramout.CandidatesFused = ParseCandidatesFusedIdentify(t5Response.tid, t5Response.response);
                                LOG.Debug("ABISTech5Client.Identify - Deserialized OK! tid = " +
                                            (string.IsNullOrEmpty(paramout.tid) ? "Null" : paramout.tid));
                                LOG.Debug("ABISTech5Client.Identify - Candidates Fused => " + 
                                            (paramout.CandidatesFused==null?"0": paramout.CandidatesFused.Count.ToString()));
                                ret = Errors.IERR_OK;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msgerr = "ABISTech5Client.Identify Excp [" + ex.Message + "]";
                LOG.Debug(msgerr);
            }

            LOG.Debug("ABISTech5Client.Identify OUT! ret = " + ret.ToString());
            return ret;
        }

        private List<T5IdentifyResponse> ParseCandidatesIdentify(string tid, string response)
        {
            List<T5IdentifyResponse> ret = new List<T5IdentifyResponse>();
            try
            {
                LOG.Debug("ABISTech5Client.ParseCandidatesIdentify IN...");
                //string s1 = "{\"finger\":{\"FINGER_T5\":{\"10000000000080089\":20.0,\"1000000000000053\":3.1874659061431885}},\"iris\":{\"IRIS_T5\":{\"100000089\":20.0}},\"face\":{\"FACE_T5\":{\"10000000000080089\":19.865537643432617}},\"scores\":{\"fused_scores\":{\"1000e038\":20.0,\"100000089\":19.959661293029786,\"100000000001\":19.959661293029786,\"10000000000080089\":19.946215057373045,\"1000000000008j0089\":19.946215057373045,\"10000089\":19.946215057373045,\"1000000000000003003\":0.8389443159103394,\"10000000000000140\":0.5996870398521423}}}";
                //Saco los escapes
               response = response.Replace("\\", "");
                //Parseo interno para cnseguir los candidatos por separado
                Models.T5ResponseIdentifyInternal obj = JsonConvert.DeserializeObject<Tech5.Api.Models.T5ResponseIdentifyInternal>(response);
                int q = 0;
                if (obj != null)
                {
                    LOG.Debug("ABISTech5Client.ParseCandidatesIdentify - Checking Finger...");
                    //Agrego si hay finger candidates
                    if (obj.finger != null && obj.finger.FINGER_T5 != null)
                    {
                        LOG.Debug("ABISTech5Client.ParseCandidatesIdentify - Adding finger candidates..."); 
                        JObject jsonObj = JObject.Parse(obj.finger.FINGER_T5.ToString());
                        Dictionary<string, string> dictObj = jsonObj.ToObject<Dictionary<string, string>>();

                        foreach (string key in dictObj.Keys)
                        {
                            Models.T5IdentifyResponse cand = new Models.T5IdentifyResponse();
                            cand.tid = "tid";
                            cand.encounter_id = key;
                            cand.type = "Finger";
                            cand.score = double.Parse(dictObj[key], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
                            ret.Add(cand);
                            q++;
                        }
                        LOG.Debug("ABISTech5Client.ParseCandidatesIdentify - Finger candidates Added = " + q.ToString());
                    }

                    LOG.Debug("ABISTech5Client.ParseCandidatesIdentify - Checking Face...");
                    //Agrego si hay face candidates
                    if (obj.face != null && obj.face.FACE_T5 != null)
                    {
                        LOG.Debug("ABISTech5Client.ParseCandidatesIdentify - Adding face candidates...");
                        JObject jsonObj = JObject.Parse(obj.face.FACE_T5.ToString());
                        Dictionary<string, string> dictObj = jsonObj.ToObject<Dictionary<string, string>>();
                        q = 0;
                        foreach (string key in dictObj.Keys)
                        {
                            Models.T5IdentifyResponse cand = new Models.T5IdentifyResponse();
                            cand.tid = "tid";
                            cand.encounter_id = key;
                            cand.type = "Face";
                            cand.score = double.Parse(dictObj[key], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
                            ret.Add(cand);
                            q++;
                        }
                        LOG.Debug("ABISTech5Client.ParseCandidatesIdentify - Face candidates Added = " + q.ToString());
                    }

                    LOG.Debug("ABISTech5Client.ParseCandidatesIdentify - Checking Iris...");
                    //Agrego si hay face candidates
                    if (obj.iris != null && obj.iris.IRIS_T5 != null)
                    {
                        LOG.Debug("ABISTech5Client.ParseCandidatesIdentify - Adding iris candidates...");
                        JObject jsonObj = JObject.Parse(obj.iris.IRIS_T5.ToString());
                        Dictionary<string, string> dictObj = jsonObj.ToObject<Dictionary<string, string>>();
                        q = 0;
                        foreach (string key in dictObj.Keys)
                        {
                            Models.T5IdentifyResponse cand = new Models.T5IdentifyResponse();
                            cand.tid = "tid";
                            cand.encounter_id = key;
                            cand.type = "Iris";
                            cand.score = double.Parse(dictObj[key], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
                            ret.Add(cand);
                            q++;
                        }
                        LOG.Debug("ABISTech5Client.ParseCandidatesIdentify - Iris candidates Added = " + q.ToString());
                    }
                } else
                {
                    LOG.Debug("ABISTech5Client.ParseCandidatesIdentify - Lista candidatos vacia...");
                    ret = null;
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Debug("ABISTech5Client.ParseCandidatesIdentify Excp - " + ex.Message);
            }
            LOG.Debug("ABISTech5Client.ParseCandidatesIdentify OUT! - Total candidates Added = " + 
                        (ret == null ? "0" : ret.Count.ToString()));
            return ret;
        }


        private List<T5IdentifyResponse> ParseCandidatesFusedIdentify(string tid, string response)
        {
            List<T5IdentifyResponse> ret = new List<T5IdentifyResponse>();
            try
            {
                LOG.Debug("ABISTech5Client.ParseCandidatesFusedIdentify IN...");
                //string s1 = "{\"finger\":{\"FINGER_T5\":{\"10000000000080089\":20.0,\"1000000000000053\":3.1874659061431885}},\"iris\":{\"IRIS_T5\":{\"100000089\":20.0}},\"face\":{\"FACE_T5\":{\"10000000000080089\":19.865537643432617}},\"scores\":{\"fused_scores\":{\"1000e038\":20.0,\"100000089\":19.959661293029786,\"100000000001\":19.959661293029786,\"10000000000080089\":19.946215057373045,\"1000000000008j0089\":19.946215057373045,\"10000089\":19.946215057373045,\"1000000000000003003\":0.8389443159103394,\"10000000000000140\":0.5996870398521423}}}";
                //Saco los escapes
                response = response.Replace("\\", "");
                //Parseo interno para cnseguir los candidatos por separado
                Models.T5ResponseIdentifyInternal obj = JsonConvert.DeserializeObject<Tech5.Api.Models.T5ResponseIdentifyInternal>(response);
                int q = 0;
                if (obj != null)
                {
                    LOG.Debug("ABISTech5Client.ParseCandidatesFusedIdentify - Checking scored fused...");
                    //Agrego si hay finger candidates
                    if (obj.scores != null && obj.scores.fused_scores != null)
                    {
                        LOG.Debug("ABISTech5Client.ParseCandidatesIdentify - Adding scores fused candidates...");
                        JObject jsonObj = JObject.Parse(obj.scores.fused_scores.ToString());
                        Dictionary<string, string> dictObj = jsonObj.ToObject<Dictionary<string, string>>();

                        foreach (string key in dictObj.Keys)
                        {
                            Models.T5IdentifyResponse cand = new Models.T5IdentifyResponse();
                            cand.tid = "tid";
                            cand.encounter_id = key;
                            cand.type = "Fused";
                            cand.score = double.Parse(dictObj[key], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
                            ret.Add(cand);
                            q++;
                        }
                        LOG.Debug("ABISTech5Client.ParseCandidatesIdentify - Fused Scored candidates Added = " + q.ToString());
                    }
                }
                else
                {
                    LOG.Debug("ABISTech5Client.ParseCandidatesIdentify - Lista Sored Fused candidatos vacia...");
                    ret = null;
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Debug("ABISTech5Client.ParseCandidatesIdentify Excp - " + ex.Message);
            }
            LOG.Debug("ABISTech5Client.ParseCandidatesIdentify OUT! - Total Fused Scored candidates Added = " +
                        (ret == null ? "0" : ret.Count.ToString()));
            return ret;
        }

        public int Delete(T5DeleteParamIn paramin, out string msgerr)
        {
            
            int ret = 0;
            msgerr = null;
            LOG.Debug("ABISTech5Client.Delete IN...");
            try
            {
                //Check ParamIn
                if (paramin == null || string.IsNullOrEmpty(paramin.encounter_id))
                {
                    msgerr = "ABISTech5Client.Delete - Parametros no puede ser nulo...";
                    LOG.Debug(msgerr);
                    ret = Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    paramin.request_type = "Delete";
                    _CLIENT.BaseUrl = new Uri(_API_BASE_PATH + "/processRequest");
                    LOG.Debug("ABISTech5Client.Extract - Inicio Delete en servicio => " + _CLIENT.BaseUrl);
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    var body = JsonConvert.SerializeObject(paramin);
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    IRestResponse response = _CLIENT.Execute(request);

                    if (response == null)
                    {
                        msgerr = "ABISTech5Client.Delete - Respuesta de servicio remoto nula!";
                        LOG.Debug(msgerr);
                        ret = Errors.IERR_CONNECTOR_NOT_AVAILABLE;
                    }
                    else if (response.StatusCode != HttpStatusCode.OK)
                    {
                        msgerr = "ABISTech5Client.Delete - Status NOOK [" + response.StatusCode.ToString() + "]";
                        LOG.Debug(msgerr);
                        ret = Errors.IERR_CONNECTOR_REMOTE_ERROR;
                    }
                    else
                    {
                        LOG.Debug("ABISTech5Client.Delete - Inicio deserializacion respuesta...");
                        var paramout = JsonConvert.DeserializeObject<T5DeleteParamOut>(response.Content);
                        if (paramout == null)
                        {
                            msgerr = "ABISTech5Client.Delete - Desrialización de respuesta de con error!";
                            LOG.Debug(msgerr);
                            ret = Errors.IERR_DESERIALIZING_DATA;
                        }
                        else
                        {
                            if ((paramout.response != null && paramout.response.Equals("false")) ||
                                !string.IsNullOrEmpty((string)paramout.error))
                            {
                                msgerr = "ABISTech5Client.Delete - Error procesando tid = " +
                                        (string.IsNullOrEmpty(paramout.tid) ? "Null" : paramout.tid) +
                                        " - [msgerr=" + paramout.errorCode.ToString() + " - " +
                                        (!string.IsNullOrEmpty((string)paramout.error) ? (string)paramout.error : "") + "]";
                                LOG.Debug(msgerr);
                                ret = Errors.IERR_DELETE_ORIGIN;
                            }
                            else
                            {
                                LOG.Debug("ABISTech5Client.Delete - Deserialized OK! tid = " +
                                            (string.IsNullOrEmpty(paramout.tid) ? "Null" : paramout.tid));
                                msgerr = paramout.response;
                                ret = Errors.IERR_OK;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msgerr = "ABISTech5Client.Enroll Excp [" + ex.Message + "]";
                LOG.Debug(msgerr);
            }

            LOG.Debug("ABISTech5Client.Enroll OUT! ret = " + ret.ToString());
            return ret;
        }

        //private List<T5IdentifyResponse> ParseResponseIdentify(string tid, string response)
        //{
        //    List<T5IdentifyResponse> ret = new List<T5IdentifyResponse>();
        //    try
        //    {
        //        //string s1 = "{\"finger\":{},\"face\":{\"FACE_T5\":{\"21284415-2\":9.238061904907227}},\"scores\":{\"fused_scores\":{\"21284415-2\":9.238061904907227}}}";
        //        string s2 = response.Substring(response.IndexOf("fused_scores") + 14);
        //        string s3 = s2.Replace("}}", "");
        //        string s4 = s3.Replace("\\", "");
        //        string[] sArr = s4.Split(',');

        //        foreach (string item in sArr)
        //        {
        //            T5IdentifyResponse respAux = new T5IdentifyResponse();
        //            string[] sarr2 = item.Split(':');
        //            respAux.encounter_id = sarr2[0];
        //            string score = sarr2[sarr2.Length - 1].Replace("}", "");
        //            respAux.tid = tid;
        //            respAux.score = double.Parse(score, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
        //            ret.Add(respAux);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = null;
        //        LOG.Debug("ABISTech5Client.ParseResponse Excp - " + ex.Message);
        //    }
        //    return ret;
        //}


    }
}
