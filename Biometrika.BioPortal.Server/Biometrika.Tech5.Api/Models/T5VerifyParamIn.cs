﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Tech5.Api.Models
{
    public class T5VerifyParamIn
    {
        public string tid { get; set; }
        public object encounter_id { get; set; }
        public object error { get; set; }
        public object response { get; set; }
        public FingerData finger_data { get; set; }
        public List<IrisDatum> iris_data { get; set; }
        public List<FaceDatum> face_data { get; set; }
        public string request_type { get; set; }
        public double threshold { get; set; }
        public int maxResults { get; set; }
        public int errorCode { get; set; }
        public double fingerThreshold { get; set; }
        public double irisThreshold { get; set; }
    }

    public class T5IdentifyParamIn
    {
        public string tid { get; set; }
        public string request_type { get; set; }
        public double faceThreshold { get; set; }
        public double fingerThreshold { get; set; }
        public double irisThreshold { get; set; }
        public int maxResults { get; set; }
        public FingerData finger_data { get; set; }
        public List<IrisDatum> iris_data { get; set; }
        public List<FaceDatum> face_data { get; set; }
    }

}
