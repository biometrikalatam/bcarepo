﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Tech5.Api.Models
{
    public class T5DeleteParamIn
    {
        public string tid { get; set; }
        public string encounter_id { get; set; }
        public string request_type { get; set; }
   }

}
