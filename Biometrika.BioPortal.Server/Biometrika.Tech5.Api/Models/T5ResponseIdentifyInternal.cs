﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Tech5.Api.Models
{ 
    public class T5ResponseIdentifyInternal
    {
        public FingerCandidatesInternal finger { get; set; }
        public IrisCandidatesInternal iris { get; set; }
        public FaceCandidatesInternal face { get; set; }
        public Scores scores { get; set; }
    }


    public class FaceCandidatesInternal
    {
        public object FACE_T5 { get; set; }
    }

    public class IrisCandidatesInternal
    {
        public object IRIS_T5 { get; set; }
    }

    public class FingerCandidatesInternal
    {
        public object FINGER_T5 { get; set; }
    }

    public class Scores
    {
        public object fused_scores { get; set; }
    }

    //public class FINGERT5
    //{
    //    public double _10000000000080089 { get; set; }
    //}


    //public class FACET5
    //{
    //    public double _10000000000080089 { get; set; }
    //}

    //public class FusedScores
    //{
    //    public double _1000e038 { get; set; }
    //    public double _100000089 { get; set; }
    //    public double _100000000001 { get; set; }
    //    public double _10000000000080089 { get; set; }
    //    public double _1000000000008j0089 { get; set; }
    //    public double _10000089 { get; set; }
    //    public double _1000000000000003003 { get; set; }
    //    public double _10000000000000140 { get; set; }
    //}



    //public class IRIST5
    //{
    //    public double _100000089 { get; set; }
    //}




}
