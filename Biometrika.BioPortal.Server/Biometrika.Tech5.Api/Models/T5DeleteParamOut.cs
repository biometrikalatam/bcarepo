﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Tech5.Api.Models
{
    public class T5DeleteParamOut
    {
        public string tid { get; set; }
        public string encounter_id { get; set; }
        public string error { get; set; }
        public string response { get; set; }
        public IList<object> finger_data { get; set; }
        public IList<object> iris_data { get; set; }
        public IList<object> face_data { get; set; }
        public string request_type { get; set; }
        public int maxResults { get; set; }
        public object errorCode { get; set; }
        public object faceThreshold { get; set; }
        public object fingerThreshold { get; set; }
        public object irisThreshold { get; set; }

    }
}
