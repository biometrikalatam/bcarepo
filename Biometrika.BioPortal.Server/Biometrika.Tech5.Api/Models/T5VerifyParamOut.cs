﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Tech5.Api.Models
{
    public class T5VerifyParamOut
    {
        
        public T5VerifyParamOut(string _tid, object _encounter_id, object _error, object _finger_data, object _iris_data,
                                object _face_data, string _request_type, int _maxResults, object _errorCode, double _faceThreshold,
                                double _fingerThreshold, double _irisThreshold)
        {
            tid = _tid;
            encounter_id = _encounter_id;
            error = _error; 
            finger_data = _finger_data; 
            iris_data = _iris_data; 
            face_data = _face_data; 
            request_type = _request_type;   
            maxResults = _maxResults;   
            errorCode = _errorCode;
            faceThreshold = _faceThreshold;
            fingerThreshold = _fingerThreshold;
            irisThreshold = _irisThreshold; 
        }

        public string tid { get; set; }
        public object encounter_id { get; set; }
        public object error { get; set; }
        public List<T5VerifyResponse> response { get; set; }
        public object finger_data { get; set; }
        public object iris_data { get; set; }
        public object face_data { get; set; }
        public string request_type { get; set; }
        public int maxResults { get; set; }
        public object errorCode { get; set; }
        public double faceThreshold { get; set; }
        public double fingerThreshold { get; set; }
        public double irisThreshold { get; set; }
    }

    public class T5VerifyResponse {
        public string type { get; set; }
        public string tid { get; set; }
        public double score { get; set; }

    }

    
    internal class T5VerifyParamOutInt
    {

        //public T5VerifyParamOutInt(string _tid, object _encounter_id, object _error, object _finger_data, object _iris_data,
        //                        object _face_data, string _request_type, int _maxResults, object _errorCode, double _faceThreshold,
        //                        double _fingerThreshold, double _irisThreshold)
        //{
        //    tid = _tid;
        //    encounter_id = _encounter_id;
        //    error = _error;
        //    finger_data = _finger_data;
        //    iris_data = _iris_data;
        //    face_data = _face_data;
        //    request_type = _request_type;
        //    maxResults = _maxResults;
        //    errorCode = _errorCode;
        //    faceThreshold = _faceThreshold;
        //    fingerThreshold = _fingerThreshold;
        //    irisThreshold = _irisThreshold;
        //}

        public string tid { get; set; }
        public object encounter_id { get; set; }
        public object error { get; set; }
        public string response { get; set; }
        public object finger_data { get; set; }
        public object iris_data { get; set; }
        public object face_data { get; set; }
        public string request_type { get; set; }
        public int maxResults { get; set; }
        public object errorCode { get; set; }
        public double faceThreshold { get; set; }
        public double fingerThreshold { get; set; }
        public double irisThreshold { get; set; }
    }
}
