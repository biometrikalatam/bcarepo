﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Tech5.Api.Models
{
    public class T5ExtractParamIn
    {
        public string tid { get; set; }
        public object encounter_id { get; set; }
        public string request_type { get; set; }
        public FingerData finger_data { get; set; }
        public List<IrisDatum> iris_data { get; set; }
        public List<FaceDatum> face_data { get; set; }
    }

    public class FaceDatum
    {
        public string pos { get; set; }
        public string data { get; set; }
    }

    public class FingerData
    {
        public List<LiveScanPlain> live_scan_plain { get; set; }
        public object live_scan_rolled { get; set; }
    }

    public class IrisDatum
    {
        public string pos { get; set; }
        public string data { get; set; }
    }

    public class LiveScanPlain
    {
        public string pos { get; set; }
        public string data { get; set; }
    }

   


}
