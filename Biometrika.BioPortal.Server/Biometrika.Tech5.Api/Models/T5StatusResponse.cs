﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Tech5.Api.Models
{
    public class T5StatusResponse
    {
        public LicenseStatus LicenseStatus { get; set; }
    }


    public class Face
    {
        public string Mode { get; set; }
        public string Hostname { get; set; }
        public string CurrentDBSize { get; set; }
        public string Modality { get; set; }
        public string Status { get; set; }

    }
    public class Finger
    {
        public string Mode { get; set; }
        public string Hostname { get; set; }
        public string CurrentDBSize { get; set; }
        public string Modality { get; set; }
        public string Status { get; set; }

    }
    public class Iris
    {
        public string Mode { get; set; }
        public string Hostname { get; set; }
        public string Modality { get; set; }
        public string Status { get; set; }
        public string CurrentDBSize { get; set; }
        public string DBLicense { get; set; }
        public string Set { get; set; }
        public string version { get; set; }

    }
    public class Slaves
    {
        public IList<Face> Face { get; set; }
        public IList<Finger> Finger { get; set; }
        public IList<Iris> Iris { get; set; }

    }
        public class Master
    {
        public string Mode { get; set; }
        public string Hostname { get; set; }
        public string Status { get; set; }

    }
    public class LicenseStatus
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public string ReleaseDate { get; set; }
        public string ReleaseNotes { get; set; }
        public int TotalNodes { get; set; }
        public IList<Master> Master { get; set; }
        public Slaves Slaves { get; set; }

    }

}
