﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Tech5.Api.Models
{
    public class T5ExtractParamOut
    {
        public string tid { get; set; }
        public object encounter_id { get; set; }
        public object error { get; set; }
        public string response { get; set; }
        public FingerDataOut finger_data { get; set; }
        public List<IrisDatumOut> iris_data { get; set; }
        public List<FaceDatumOut> face_data { get; set; }
        public string request_type { get; set; }
        public int maxResults { get; set; }
        public object errorCode { get; set; }
        public double faceThreshold { get; set; }
        public double fingerThreshold { get; set; }
        public double irisThreshold { get; set; }
    }
    public class FaceDatumOut
    {
        public string pos { get; set; }
        public object data { get; set; }
        public string template { get; set; }
        public double quality { get; set; }
        public string age { get; set; }
        public string background { get; set; }
        public string blur { get; set; }
        public string closedEyes { get; set; }
        public string confidence { get; set; }
        public string faceColor { get; set; }
        public string gender { get; set; }
        public string glasses { get; set; }
        public string hotspots { get; set; }
        public string ageGenderSet { get; set; }
        public object qualitySet { get; set; }
        public string mask { get; set; }
        public string occlusion { get; set; }
        public string overExposure { get; set; }
        public string redLeftEye { get; set; }
        public string redRightEye { get; set; }
        public string rotation { get; set; }
        public string smile { get; set; }
    }

    public class FingerDataOut
    {
        public List<LiveScanPlainOut> live_scan_plain { get; set; }
        public List<object> live_scan_rolled { get; set; }
    }

    public class IrisDatumOut
    {
        public string pos { get; set; }
        public object data { get; set; }
        public string template { get; set; }
        public double quality { get; set; }
        public double blur { get; set; }
        public double occlusion { get; set; }
    }

    public class LiveScanPlainOut
    {
        public string pos { get; set; }
        public object data { get; set; }
        public string template { get; set; }
        public double quality { get; set; }
    }

    

}
