﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Neurotec.Licensing;

namespace VFLicenseTest
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                if (args.Length == 0)
                {
                    Console.WriteLine("Debe ingresar los parametros IP Port LicenciasAChequear...");
                    return;
                }

                //if (!NLicense.ObtainComponents("/local", 5000, componenticensecfg))
                Console.WriteLine("Parametros a usar...");
                Console.WriteLine("IP = " + args[0]);
                int p = Convert.ToInt32(args[1]);
                Console.WriteLine("Port = " + p.ToString());
                Console.WriteLine("Licencias a Check = " + args[2]);

                if (!Neurotec.Licensing.NLicense.ObtainComponents(args[0], Convert.ToInt32(args[1]), args[2]))
                {
                    Console.WriteLine("No pudo obtener licencia válida!");
                }
                else
                {
                    Console.WriteLine("Licencia Obtenida OK!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error - Msg=" + ex.Message + " - Stack = " + ex.StackTrace);
            }

        }
    }
}
