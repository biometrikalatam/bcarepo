﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Biometrika.NamkuFaceAPI.MultipartFormDataSample.Client
{
    public class ImageSet
    {
        public string Name { get; set; }

        [JsonIgnore]
        public List<Image> Images { get; set; }
    }

    public class Image
    {
        public string FileName { get; set; }

        public string MimeType { get; set; }

        public byte[] ImageData { get; set; }
    }
}
