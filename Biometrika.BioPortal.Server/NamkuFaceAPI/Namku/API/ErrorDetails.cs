﻿using Newtonsoft.Json;

namespace Biometrika.NamkuFaceAPI
{
    public class ErrorDetails
    {
        [JsonProperty("domain")]
        public string Domain { get; set; }
        [JsonProperty("reason")]
        public string Reason { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}