﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Biometrika.NamkuFaceAPI.Face
{
    public class CompareRequest
    {
        [JsonProperty("source")]
        public CompareRequestFaceObject Source { get; set; }
        [JsonProperty("target")]
        public List<CompareRequestFaceObject> Target { get; set; }
    }
}