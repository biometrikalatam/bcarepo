﻿using Biometrika.NamkuFaceAPI;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Biometrika.NamkuFaceAPI.Face
{
    public class CompareResponse
    {
        [JsonProperty("api_version")]
        public string ApiVersion { get; set; }
        [JsonProperty("error")]
        public Error Error { get; set; }
        [JsonProperty("data")]
        public CompareResponseData Data { get; set; }
    }
}