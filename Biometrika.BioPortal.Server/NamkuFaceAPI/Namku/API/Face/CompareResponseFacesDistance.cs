﻿using Newtonsoft.Json;

namespace Biometrika.NamkuFaceAPI.Face
{
    public class CompareResponseFacesDistance
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("distance")]
        public float Distance { get; set; }
    }
}