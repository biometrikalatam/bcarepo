﻿using Biometrika.NamkuFaceAPI;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Biometrika.NamkuFaceAPI.Face
{
    public class ExtractResponseData
    {
        [JsonProperty("bounding_box")]
        public BoundingBox BoundingBox { get; set; }
        [JsonProperty("embeddings")]
        public List<float> Embeddings { get; set; }
    }
}