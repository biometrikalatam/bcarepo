﻿using Biometrika.NamkuFaceAPI.Auth;
using Biometrika.NamkuFaceAPI.Face;
using Biometrika.NamkuFaceAPI.MultipartFormDataSample.Client;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.NamkuFaceAPI
{
    public class NamkuFaceApi 
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(NamkuFaceApi));

        private const string DEFAULT_API_BASE_URI = "http://facial.biometrika.cl/";
        private const string DEFAULT_URI_EXTRACT = "v2/recognition/extract";
        private const string DEFAULT_URI_COMPARE = "v2/recognition/compare";
        private const string DEFAULT_URI_CEDULA = "v2/dni";
        private const string DEFAULT_URI_AUTH = "v2/auth/token";
        private const string DEFAULT_USER = "facial@biometrika.cl";
        private const string DEFAULT_PASSWORD = "$F4n=hh!WxgHakf-";
        private const bool DEFAULT_LOGIN_ENABLED = true;

        public string API_BASE_URI = "http://facial.biometrika.cl/";
        public string URI_EXTRACT = "v2/recognition/extract";
        public string URI_COMPARE = "v2/recognition/compare";
        public string URI_CEDULA = "v2/dni";
        public string URI_AUTH = "v2/auth/token";
        public string USER = "facial@biometrika.cl";
        public string PASSWORD = "$F4n=hh!WxgHakf-";
        public bool LOGIN_ENABLED = true;

        public int TIMEOUT = 30000;
        private HttpClient webClient = null;
        private string accessToken = null;


        public NamkuFaceApi(string api_base_uri, string uri_extract, string uri_compare, string uri_cedula, string uri_auth, string user, string password, int timeout, bool loginenabled)
        {
            this.webClient = new HttpClient();

            API_BASE_URI = string.IsNullOrEmpty(api_base_uri)? DEFAULT_API_BASE_URI : api_base_uri;
            URI_EXTRACT = string.IsNullOrEmpty(uri_extract) ? DEFAULT_URI_EXTRACT : uri_extract;
            URI_COMPARE = string.IsNullOrEmpty(uri_compare) ? DEFAULT_URI_COMPARE : uri_compare;
            URI_CEDULA = string.IsNullOrEmpty(uri_cedula) ? DEFAULT_URI_CEDULA : uri_cedula;
            URI_AUTH = string.IsNullOrEmpty(uri_auth) ? DEFAULT_URI_AUTH : uri_auth;
            USER = string.IsNullOrEmpty(user) ? DEFAULT_USER : user;
            PASSWORD = string.IsNullOrEmpty(password) ? DEFAULT_PASSWORD : password;
            TIMEOUT = (timeout <= 2000) ? 30000 : timeout;
            LOGIN_ENABLED = loginenabled;

            this.webClient.BaseAddress = new Uri(API_BASE_URI);
            this.webClient.Timeout = new TimeSpan(0,0,0,TIMEOUT);
            this.webClient.DefaultRequestHeaders.Accept.Clear();

            LOG.Debug("NamkuFaceApi => API_BASE_URI=" + API_BASE_URI + "-URI_EXTRACT=" + URI_EXTRACT + "-URI_COMPARE=" + URI_COMPARE + "-URI_CEDULA=" + URI_CEDULA + "-USER=" + USER + "-TIMEOUT=" + TIMEOUT);

        }

        public NamkuFaceApi()
        {
            this.webClient = new HttpClient();

            API_BASE_URI =DEFAULT_API_BASE_URI;
            URI_EXTRACT = DEFAULT_URI_EXTRACT;
            URI_COMPARE = DEFAULT_URI_COMPARE;
            URI_CEDULA = DEFAULT_URI_CEDULA;
            URI_AUTH = DEFAULT_URI_AUTH;
            USER = DEFAULT_USER;
            PASSWORD = DEFAULT_PASSWORD;
            LOGIN_ENABLED = DEFAULT_LOGIN_ENABLED;

            this.webClient.BaseAddress = new Uri(API_BASE_URI);
            this.webClient.Timeout = new TimeSpan(0, 0, 0, TIMEOUT);
            this.webClient.DefaultRequestHeaders.Accept.Clear();
        }

#region Sin THREADS

        public CompareResponse Compare(MemoryStream source = null, MemoryStream target = null, CompareRequest json = null)
        {
            if (this.LogIn())
            {
                string uri = "v2/recognition/compare";
                string response = this.PutFilesAsync(uri, source, target, json);
                //Console.WriteLine(response);
                return JsonConvert.DeserializeObject<CompareResponse>(response);
            }
            return null;
        }

        private bool LogIn()
        {
            try
            {
                LOG.Debug("NamkuFaceApi.LogIn IN...");

                if (!LOGIN_ENABLED) return true;

                if (this.accessToken != null)
                {
                    LOG.Debug("NamkuFaceApi.LogIn accessToken = " + accessToken.Substring(0, 10) + "...");
                    //string auxtokenString = this.accessToken.PadRight(this.accessToken.Length + (4 - this.accessToken.Length % 4) % 4, '=');
                    var token = new JwtSecurityTokenHandler().ReadJwtToken(this.accessToken); // auxtokenString); // this.accessToken);
                    if (token != null)
                    {
                        DateTime now = DateTime.Now;
                        LOG.Debug("NamkuFaceApi.LogIn token.ValidTo = " + token.ValidTo + " => Current = " + now);
                        if (token.ValidTo > now) return true;
                        else LOG.Debug("NamkuFaceApi.LogIn token Vencido!");
                    } else
                    {
                        LOG.Debug("NamkuFaceApi.LogIn token = null");
                    } 

                } else
                {
                    LOG.Debug("NamkuFaceApi.LogIn this.accessToken = null");
                }

                LOG.Debug("NamkuFaceApi.LogIn Ingresa generacion o regeneracion de token...");
                byte[] authBase64 = Encoding.ASCII.GetBytes(USER + ":" + PASSWORD);
                this.webClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(authBase64));
                LOG.Debug("NamkuFaceApi.LogIn Pide token con user = " + USER); 
                HttpResponseMessage response = this.webClient.PostAsync("v2/auth/token", null).Result;
                LOG.Debug("NamkuFaceApi.LogIn response != null => " + (response != null).ToString()); 
                HttpContent content = response.Content;
                LOG.Debug("NamkuFaceApi.LogIn content != null => " + (content != null).ToString());
                string result = content.ReadAsStringAsync().Result;
                LOG.Debug("NamkuFaceApi.LogIn result != null => " + (result != null).ToString());
                if (result != null && (int)response.StatusCode == 200)
                {
                    LOG.Debug("NamkuFaceApi.LogIn Token recibido ok!");
                    var authData = JsonConvert.DeserializeObject<AuthResponse>(result);
                    LOG.Debug("NamkuFaceApi.LogIn authData != null => " + (authData != null).ToString());
                    this.accessToken = authData.Data.AccessToken;
                    LOG.Debug("NamkuFaceApi.LogIn accessToken New = " + accessToken.Substring(0, 10) + "... - ValidTo = " + 
                                (new JwtSecurityTokenHandler().ReadJwtToken(this.accessToken)).ValidTo.ToString());
                    return true;
                }
                else
                {
                    LOG.Debug("NamkuFaceApi.LogIn authData = null => NO Consiguio Token Valido!");
                    //Console.WriteLine(result);
                    return false;
                }
            }
            catch (Exception ex)
            {

                return false;
            }

        }


        private  string PutFilesAsync(string endpoint, MemoryStream source = null, MemoryStream target = null, CompareRequest json = null)
        {
            using (var content = new MultipartFormDataContent())
            {
                if (source != null)
                {
                    content.Add(
                        new StreamContent(source),
                        "source",
                        "source"
                    );
                }
                if (target != null)
                {
                    content.Add(
                        new StreamContent(target),
                        "target",
                        "target"
                    );
                }
                if (json != null)
                {
                    var jsonString = JsonConvert.SerializeObject(
                           json,
                           Formatting.Indented,
                           new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }
                       );
                    byte[] byteArray = Encoding.ASCII.GetBytes(jsonString);
                    MemoryStream stream = new MemoryStream(byteArray);
                    content.Add(
                        new StreamContent(stream),
                        "json",
                        "json"
                    );
                }

                this.webClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(
                    "Bearer",
                    this.accessToken
                );
                using (var message = this.webClient.PostAsync(endpoint, content).Result)
                {
                    var response = message.Content.ReadAsStringAsync().Result;
                    return response;
                }
            }
        }

        public ExtractResponse Extract(MemoryStream img, bool onlyBiggestFace = false, bool onlyDetectFaces = false)
        {
            if (this.LogIn())
            {
                string extractionType = onlyDetectFaces ? "face" : "all";
                string biggestFace = onlyBiggestFace ? "&biggest=1" : "";
                string uri = $"v2/recognition/extract?type={extractionType}{biggestFace}";

                string response = this.PutFilesAsync(uri, source: img);
                Console.WriteLine(response);

                return JsonConvert.DeserializeObject<ExtractResponse>(response);
            }
            return null;
        }

        public CedulaResponse ParseDocument(MemoryStream img)
        {
            LOG.Debug("NamkuFaceApi.ParseDocument IN..."); 
            if (this.LogIn())
            {
                //string uri = $"v2/dni";

                LOG.Debug("NamkuFaceApi.ParseDocument Set Paramerters...");
                //Consumo servicio de namku
                ImageSet imageSetNew = new ImageSet();
                MultipartFormDataSample.Client.Image parameter1 = new MultipartFormDataSample.Client.Image();
                parameter1.FileName = @"image1.jpg";
                parameter1.ImageData = img.ToArray();
                parameter1.MimeType = "image/jpeg";
                imageSetNew.Images = new List<MultipartFormDataSample.Client.Image>();
                imageSetNew.Images.Add(parameter1);
                var multipartContent = new MultipartFormDataContent();
                var imageSetJson = JsonConvert.SerializeObject(imageSetNew,
                    new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    });

                multipartContent.Add(new StringContent(imageSetJson, Encoding.UTF8, "application/json"), "imageset");

                foreach (var image in imageSetNew.Images)
                {
                    var imageContent = new ByteArrayContent(image.ImageData);
                    imageContent.Headers.ContentType = new MediaTypeHeaderValue(image.MimeType);
                    multipartContent.Add(imageContent, "data", image.FileName);
                }

                //var ou = null;
                DateTime start = DateTime.Now;
                using (var response = new HttpClient())
                {
                    response.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.accessToken);
                    LOG.Debug("NamkuFaceApi.ParseDocument Call Endpoint = " + API_BASE_URI + URI_CEDULA + "...");
                    var result = response.PostAsync(API_BASE_URI + URI_CEDULA, multipartContent).Result;
                    DateTime end = DateTime.Now;

                    var responseContent = result.Content.ReadAsStringAsync().Result;
                    LOG.Debug("NamkuFaceApi.ParseDocument responseContent = " + responseContent);
                    var ou = JsonConvert.DeserializeObject<CedulaResponse>(responseContent);

                    return ou;
                }
            }
            return null;
        }

#endregion Sin THREADS

#region Con THREADS

        public async Task<CompareResponse> CompareTH(System.Drawing.Image source = null, System.Drawing.Image target = null, CompareRequest json = null)
        {
            if (await this.LogInTH())
            {
                string uri = "v2/recognition/compare";
                string response = await this.PutFilesAsyncTH(uri, source, target, json);
                //Console.WriteLine(response);
                return await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<CompareResponse>(response));
            }
            return null;
        }

        public async Task<CompareResponse> CompareTH(MemoryStream source = null, MemoryStream target = null, CompareRequest json = null)
        {
            if (await this.LogInTH())
            {
                string uri = "v2/recognition/compare";
                string response = await this.PutFilesAsyncTH(uri, source, target, json);
                //Console.WriteLine(response);
                return await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<CompareResponse>(response));
            }
            return null;
        }

        public async Task<ExtractResponse> ExtractTH(System.Drawing.Image img, bool onlyBiggestFace = false, bool onlyDetectFaces = false)
        {
            if(await this.LogInTH())
            {
                string extractionType = onlyDetectFaces ? "face" : "all";
                string biggestFace = onlyBiggestFace ? "&biggest=1" : "";
                string uri = $"v2/recognition/extract?type={extractionType}{biggestFace}";

                string response = await this.PutFilesAsyncTH(uri, source: img);
                Console.WriteLine(response);

                return await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<ExtractResponse>(response));
            }
            return null;
        }

        private async Task<string> PutFilesAsyncTH(string endpoint, System.Drawing.Image source = null, System.Drawing.Image target = null, CompareRequest json = null)
        {
            string response = await PutFilesAsyncTH(endpoint, this.ImageToMemoryStream(source), this.ImageToMemoryStream(target), json);
            return response;
        }

        private async Task<string> PutFilesAsyncTH(string endpoint, MemoryStream source = null, MemoryStream target = null, CompareRequest json = null)
        {
            using (var content = new MultipartFormDataContent())
            {
                if (source != null)
                {
                    content.Add(
                        new StreamContent(source),
                        "source",
                        "source"
                    );
                }
                if (target != null)
                {
                    content.Add(
                        new StreamContent(target),
                        "target",
                        "target"
                    );
                }
                if (json != null)
                {
                    var jsonString = await Task.Factory.StartNew(
                       () => JsonConvert.SerializeObject(
                           json,
                           Formatting.Indented,
                           new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }
                       )
                    );
                    byte[] byteArray = Encoding.ASCII.GetBytes(jsonString);
                    MemoryStream stream = new MemoryStream(byteArray);
                    content.Add(
                        new StreamContent(stream),
                        "json",
                        "json"
                    );
                }

                this.webClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(
                    "Bearer",
                    this.accessToken
                );
                using (var message = await this.webClient.PostAsync(endpoint, content))
                {
                    var response = await message.Content.ReadAsStringAsync();
                    return response;
                }
            }
        }

        private MemoryStream ImageToMemoryStream(System.Drawing.Image img)
        {
            ImageConverter converter = new ImageConverter();
            byte[] imageStream = (byte[])converter.ConvertTo(img, typeof(byte[]));
            return new MemoryStream(imageStream);
        }

        private async Task<bool> LogInTH()
        {
            if(this.accessToken != null)
            {
                var token = new JwtSecurityTokenHandler().ReadJwtToken(this.accessToken);
                if (token.ValidTo > DateTime.Now)
                    return true;
            }
            byte[] authBase64 = Encoding.ASCII.GetBytes(USER + ":" + PASSWORD);
            this.webClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(authBase64));
            HttpResponseMessage response = await this.webClient.PostAsync("v2/auth/token", null);
            HttpContent content = response.Content;
            string result = await content.ReadAsStringAsync();
            if (result != null && (int)response.StatusCode == 200)
            {
                var authData = await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<AuthResponse>(result));
                this.accessToken = authData.Data.AccessToken;
                return true;
            }
            else
            {
                //Console.WriteLine(result);
                return false;
            }
        }
    }
    
#endregion Con THREADS
}
