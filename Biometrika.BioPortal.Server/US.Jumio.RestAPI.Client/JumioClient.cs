﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace US.Jumio.RestAPI.Client
{
    public enum HttpMethod
    {
        GET,
        POST
    }

    public class JumioClient
    {
        #region Private Members

        private string ApiKey;
        private string SecretKey;
        private string ApiUrl = "https://netverify.com/api/v4";

        #endregion



        public JObject Send(string service, IDictionary<string, string> parameters, HttpMethod method = HttpMethod.GET)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            //CheckInit();
            RestClient client = new RestClient(ApiUrl);
            RestRequest request = GetRequest(service, parameters, method);
            IRestResponse response = client.Execute(request);

            var cod = response.StatusCode;

            JObject content = JsonConvert.DeserializeObject<JObject>(response.Content);

            if (HasError(content))
            {
                string code = GetValue(content, "code");
                string message = GetValue(content, "message");
                throw new FlowException(message, code);
            }

            return content;
        }

        private RestRequest GetRequest(string service, IDictionary<string, string> parameters, HttpMethod method)
        {
            RestRequest request = new RestRequest(service, (method == HttpMethod.GET) ? Method.GET : Method.POST);
            IDictionary<string, string> allparameters = (parameters != null) ? parameters : new Dictionary<string, string>();
            allparameters.Add("apiKey", ApiKey);

            var keyList = allparameters.Keys.ToList();
            keyList.Sort();

            foreach (var key in keyList)
            {
                request.AddParameter(key, allparameters[key]);
            }

            request.AddParameter("s", SignParameters(request.Parameters));
            return request;
        }
    }
}
