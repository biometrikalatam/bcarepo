﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
//using System.ServiceModel;
//using System.ServiceModel.Channels;
//using System.ServiceModel.Description;
//using System.ServiceModel.Security;
//using System.ServiceModel.Security.Tokens;
using System.Text;
using Bio.Core.Api;
using Bio.Core.Api.Matcher.Interface;
using Bio.Core.Constant;
using Bio.Core.Wsq.Encoder;
using BioPortal.Server.Api;
using log4net;
using System.Data;
using System.Text;
using System.IO;
using System.Threading.Tasks;

using System.Runtime.InteropServices;

namespace Bio.Core.Matcher.Connector.RENIEC
{
    public class Connector : IConnector
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Connector));

#region Private properties propietario
        //RUN _currenRUN;
        bool _getInfo;
        bool _getFoto;
        bool _getFirma;

        private string _urlWSWSQ = "https://wsbiometrico.reniec.gob.pe/svbiometrica-v.3/BiometricVerificationImageService";
        private string _urlWSMin = "https://wsbiometrico.reniec.gob.pe/svbiometrica-v.3/BiometricVerificationMinutiaService";
        private int _timeout = 30000;
        private string _rucEmpresaRENIEC = "20600348893";
        string _nuDni = "09955646";
        string _coduserReniec = "09607650";
        string _WSQuserReniec = "/6D/pAA6CQcACTLTJc0ACuDzGZoBCkHv8ZoBC44nZM0AC+F5ozMACS7/VgABCvkz0zMBC/KHIZoACiZ32jP/pQGFAgAsA5aGA7SgA5aGA7SgA5aGA7SgA5aGA7SgA6V/A8aYA7JdA9YJA7ZnA9riA7EWA9SBA6RiA8VCA6qZA8y3A6IVA8J/A8CnA+cvA6+ZA9K3A6xsA87oA6Z0A8e+A6a4A8gRA6jOA8qRA68fA9IlA6xQA87GA6l7A8thA73ZA+PSA7BjA9OqA8V0A+zxA711A+NaA76VA+SzA8cHA+7VA8FKA+fyA8blA+6tA9XlAhmrA8goA/AvA8V1A+zzA8hiA/B1A9O9A/4WA73BA+O1A8LlA+nfA8NrA+qBA7oqA99lA9W1AhmlA90/AhqNA8yAA/VmA8ljA/GqA97CAhq7A837A/cuA+DDAhr5A9ZAAhm2A+5OAhyZA+1FAhx5A9jUAhoFA8D6A+eTA95zAhqyA9PrA/5OA9u/AhpfA652A9FaA/oBAh4AA7wfA+G/Ah0HAiLVA9ZQAhm4A9A2A/nbAiWSAi0VAifQAi/GAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/6IAEQD/AgACAAJSGQRAI+0nv/+mAHgAAAECAgUECQoLCBkTBQAAALMBtbK2ArCxt7ivubrLAw8QERKtrrvKBA4TFBUWF6y8vwUHCRhmaqqrvcDBBhkaHWmpvskICh8hUG5yc3R1eH1/gYOEhouRmKGjqMLMDBweICk4a2xxdnd5foKSmZ2gw6KkpafE/6MAAwD4+fPP/r+Huz+H89emuuuvx8/P9/5/2ft/5+z469Ndddfj8v8A7+v9H+3+n+9fl06a666+fx/f+T/X83sTPzfu+X29NdddfPy/X/j+H2JXsT/Ylfjz+3prrrr8uX5/xexN9if7Er2Jn+H8f6dNdddflb9X5f8AL2JnsUPYofl/9f0+3XXXX5P936vx/h9iZ+L8n/f9Pt1111+Pw/j+/wDT+L/P8v6zXprrrr5+Z/D9v7P0/o/48vt6a666+fxzr7v4/wAP/lvl06a666+fn9mFYePl06a666+fn5/Dn8NPh5/LXprrrr06dOnTp06dOmuuuuuvTp06dOnTp06a666666666666666666666pxdTkOfTF/HT5e7Mn5jSeWbnlL+Yzrcytf7PSXqpTzt6Z+tXpoe74HofKsynbHxj0PKudarFfjHoItgyyWMz5gpZ/H1NtmWeb9Q8nEz/UUn+851ymfUTF2+b9XFpl8n6RqbWtU9JW7nSx6sKG5yzPQ42NFcz0kZLPyWT9N7ucaXf3HEwuP3RlaV9Lbo059LbbbbfqAD+52Jv7o9VvF3iD7t21xd/uRGE6H3HKd1f1RAEOY+ZrlW9qrOPSc7Rky9/SOLEc7+q7AvP3SYwn1v8DJqPqOy/lpkQHUcLulcsNPYbi/O8VLTcm4jaYGSVWLuXQ2nIM4ygEZ8mjc8Tmwi7m97BvilSw1nLtiNpkr4TpLzRXONvJZ7BN2hPL3x1ty01KFlyVo/k9teWmGEu8crFXc67aRChzFpCdP5XOvxvW2ICaNza+Drc3lu1oIHnp7ntIdZTCag3XB1ubXpMKpmpcG1kMhpxZrlcN1msqMcbNMRwgV7LyWE8THZLayZaZaNzo6O7Nko7QQ6ModoBSOo+8fV5j5VVuWfrxLs6Wz724BGzk+yXGatCi5g4OyiK5CbvkcBQ8TM7IzRvkU3bdKznkcJTHaXKmnkbmUll8bC+8YWEwUoKm2MvICgiBRe+8EwKOZVpOsi0he1xtOMzaXpFG2TKtxzNw1FJdeVlWJ3SRZxQtawo3tyOSWK6iu8CJIJLy1bsl3JmHLlzwlugWdrNvuaJQiM+4TahNW/AAAABAAH1eg7XsesLPOlY8Hal6REX7IqrIRF3eOy4oIbhp27AJgGxXxHBzGCjTFORualjoMdL133YiYESWJ2tQYcRR4UldztxhxajvnMqWbYcxlSBSikW2wITEK5SMt0XlFotsiXEV33G6QpAT3QolWpES2mcCUJk4Fm898CiEEoRnO8LDUjRDZ32BMYjtKBZlDvE2fQ0dQn6z2gI62I+huEUvPrtNIYjwuRSIodsYaaGNxwhnVeIdG97d0DEUFPDFaBRqKON0SXToKIZjcQ0UKO0KNNNszxo1CZFy9XvmHSVMOVgg3l3Kynq+F772i5hwrilRtaYXJRS9quNwnQFmmmsQbSjJik4pWY4CmjWZdvHhcvzIVzg0Jhiw1NjebLQgxfYcBMi7HR972RRh4NCPaAjIomJf0CbV3D8LgjSt333l3FIKe616CC9DeYam85OFBl2ReJoIRiI4RjYwHIq33ClQKJFgWm+XZCbbi7c8Z22iXQiFHjm37ntHkWlw7Yi2d6va250U5nl7/cs9LPaIlZfXzWL15CZts628kOFBaHfc23khw7Zcssr33NymBMDxCNzTkbd06x434TCL5l8UIjgDvVxaZydewTbhhR+AphQm+90H1P1uj9oCNO8QAeuS6iQ74TIZCKnYA9kS0TwdnJFLiIt2wHzOj7WoadG2nulBshtQS52vLlVNRCFAb54xorOktYB23WV+UXzifKym2mNpMv3czNmdk6x5Tv5RhzDpDUQ+txiL1MReKTa2k9YpIw04QoGbyk4IjOwUye4c3cS1E0duArSOyIzUcCIYiliUdjgUDiyY+AdUSAo+d2bT+h/wBpR+0BGHF5JH67SzCEd7hzZSI8HejEA+FmMexl2dkuZpKEzsvF4vAxOhG8smCijBw9trIo1dEubxteV1EDGVIVVG60mNjmq8cMe2YyvNlE10xbSJNr0+uPOMWceL051m22z4/ZzmHJiaqHG12x9n1uwLjHO2We/K9yIUuufLGituEbLqJXv4wzeDydnS+U6W4Q2rQrGIh9o25jAg8GK8kh3ggo03/XP0jPaAjN5mzE/B5ca5uSTv5ac8WsI7pvePfx0dhnY3Npu+WyRveKL2dJAiTe7JzDcSItHgUFIbwajYX2Nz2DdG+pxO4cSKDY2J7WCcXJ2McPc4TOo2EPcDsj0RgneNl7SERDieEzKZdtsw97oJyO0IjgKpRjmC1n3NiOpvgUE0IEHB+pp/STPrf3xHAAAAAAA+rzLFk8p9dcrNl7N92kTQdb+HHOkImO6XzghS0Q54W93jKGopnFd5p585TdG7Td78vHjxshQoRBur4xz0vFL0czfaVjDtCi5CLZM6yE+WWnN8eUMpEbcSrJzpKkd3B1u8zKgY/AiIa0u25ZD2jtLT5QSm1d7h2ixpjGwI73XIE7q28CHaZUCxWN4pauA7uvdDTGNw+9yNtsDwZsf3z2gJvjGlqDfhy4+5XtJ3h9mnOeOeV32z434r3+/wAq6cp7p4/+ctNL1Uw+DUXt/wDtPHSsi5HCYVcfz8pVcRfhFlExfnF4FBuFKZeKsV0RulppmPHGyU943RutGolvaZN0tf620WU573CJcxQoRtguNQjY7je6Nj98IgMTNntbUTWaxaKlJ3NlJiaDAjhNse/MV8OcZve0OWAFnbtOWDFqCJ7WJ4aYT2NkDn0W7mgbGJ97YpE/A+Y+r4IiBcp+eZyu1y8G8i9VHlY7c5i8wufG3l2kac6Y8b3/APa5vhp5HLAuXOeXK0cCuPhyl258fHHPN7mTX+X188rxD93vsbod7+StNeTtaye6aELlY91qZuptHd4Kmc0vdzuHLRshCiCNrCBPYCaHtgQN6TSQA7IqRWGiJmd5HvfHPmotMK0bhXnlpGRdQmPeSQvGZqOOXYPxxxv9mLo5D3ghx5YnTGGjg4TEOLNne6GaHD8D5p+l+0BGZQ4seuW6EQd0A0Jpvtd1CZBMj7HETKhOkWOE4IextPHc31SNWy3xx5UGQiIUvdkv55sQojOztG2F/O315Zysc55aZZ32x5KLf+MVZvaT3t9bkr5Y5SVbbucX13reqYmx7Cu0iBkLS1BSRtMQ72uojO0S723NVIhX0jK5Y0NoJtSZF5FaeDYJqE88nwaaES6B2BRihkpngdQfO0z2gK2QA/oNjlR33oOJTh9oTZROy77wGbJT7JtMegY+DI6mShwbiUCbumhqNwrbGmi9BnWYTsomkmdphPrNOL5wWQrcWYDaeVSMZUvFq28cV63xXPjF7GJGVuza/G1nAZc01GmN168ecwopcvXmja75bCXGBmLX33hym1by05Ce4LFJIdbxZ8GxCKMb7iTZKanvbo2B87ox/wBh95+0BGCg1B65TTo/ALukJvtc+glR3V5Q00GFbLhM6ZoELPNk8Iyth0hxa2cb5c/C8CZbxt4w9xMcspQq+JPnmb7RfH2IcSXjlfeyLVHA2KZ3y4d4iRQHB7CL0Gnfg1KlxBSaG8TUJvDGxwbRwora1bYamB772rYxjFtj3svdCMstIkDg5lqMHiNR2jVk7xlUng31FBM7mFLJ/SJ+0BO7Ds/ohqE2+8CBuZfcNQmnBPaXz6ndNM4VUKYadImz3kNNp0dI3szYygyk23FYmETR4IHvtnF4lMnlp44l7SePO17Bac7aZS9sPytMWL2cS8G+rTEQ0yB75s5Y84o1ZG90fUNNQ9pZZVpdkiDEb63iswnB3VxZOGBMOHvEOFFL8fKrO1sY00J9j2CG033ug+p+DNj9oCZf/6YAgQEAAgECAgIIDAkMGQ8HDQAAs7UBArIDtgQFBgcQERNpsbcICRIUFRYXGBlqsLgKCwwODxobHLkNHSBDRUdJTE1OUq8eH0FCREZISktPUVNUVVlbXF1fYGJjZLq8ISIlP0BQVldaXmGsra67JCkqLDtYqycoLTAyMzY6PD2oqb3/owADAei2h9tuc57wcxse0SHk5betUr4JG3ghmDFq76iXbTCud9hcWueW7fYFWhmvOZ+Gd4Z8oEG2Z+G9oIQI+dPsV5QmZbm+fPnvJkJy8650R38s5/p5f6nPP+v/APeXlefYq35zmz/b/wDP/H889SuC+eXy8i3dZzx8Jnzvz/8A247ktaPNhL7MpovKPUBGHKEuDwaYEfAYGHuaI34gJwEPcUhnvMDY9zQkO1sphg6yrgAidjCGM+UewYxLU3mRpI3yp6udtwSDDmbuQ7fLmc/J9gka5lZu3zNyxFhlzZz3lMvNz4XMtm5mS8CJyvrzocw8y3e5wXPhXPk8LiZl5iw7DDbZnPuYQ4GzOHwTQ9ow8QE7Bv3Eyp3Jplo61KDB1pBMMyPYxvDDlvaLYjLZzd4OdRRd7BptIwdyugQS3O58hxcJmeVm5f8AeyXOV+fw/wCL85n2Dn/Xkf7l5+3PPI59lnl5ZWeeYPIt9j9v+3wY28/Ml3e5vkiQhz/pn/c3tXbmM5hyeDLwikvgkuy1g8GMyuMx7DDT7hKO8ow+ICcWQ+Re4wVzE6zMNEOzMY81CnhbCGy3e0YIRI9TB2vC5lWssKd/JjgZdxN44SDc5WPsBLW6FvLncIZUyRnKJ7BvOZcGHwXcUUEM15eceojRG5z67jMtNGetgMJcYO9xmCTMPBjQ+Bg9x4gK8wDwW4h7mlj25zLPcGq9pbHZngq4KU4ZhAMK9bM0tZje9DPnsY5TexdDzlibzOgLPtXqCFjzq+VO4cecy8uRn4Z3pOVunPAbyc2hzz5+fPO8ebXmQJ8DtSF2znz4NNZj7TDFjwaQ0e48QFedG/8A7Ro9udXrDR8CMTQDqdBjE7DoVjvyRoaaXgLGKFXvRdgCPsYaNIA70LHy58n7YGb6h+F/0YQc8jcNKZH/AI+GHffTztd5hBeZEetFwx5dt6Dlj/7GifGeICxWd5oJ3kbuHeEACMe8uJLOwywpjnqYGXRZfUXfoI7iciOmZZwZejGDzfYomzKUdWYES+aJvXLP98Z5xLt9ZGxWkZyu9wEzoWzyewp1L6mXq6c3qSMYXcTrCZpjH4zwD5A+R8QE8av3NPxmafANp1iMzq9YrMlEOBFjoQOocOxh2NECHY3BwPAE23RDrTN5f58qz1Nt8zmzLY8vYo8vhm3PlAhwRhC+Xnkj1c0aBXPULBITLnl3XCkj2CsIHtUgviAzTT35clOe8iy5k7FxlhR1oMXHN4Zq4RpOt6SXwQp0Qt3ZbNGhXeM5iaZuncQ5uABcu7lz58icnkN5zbuLzLIkazyT2c4QZkg0G5yXzi0lc3eww2RzMm8YMWEynUUxi1yye98Eq0j7Qh4gL0PuaKPBpPe/GOGOL6iZjsTg3hhSL2PoNxFDBTwUp0WWJuyEUhmzPU3L5jDFp1MG5Y6ENyRSnAx4NGQQYbhjo4KOohRGAG9aVUSD1NEIOE9wnxvvfEBXSm3wcCHuaDtIREXsPLDGJDhygC6nBphATK9RdEWmI7m4xoxzJndewpcsdxOXKcgXN5527yMypQ8r3uGcsphp3W3hu6SG5g55KJHBuZnRNDqya3kso6lE0bTwR5APWw/qKI+IC8vuY6HglLT1uHBDssiYIHB0dXOeDDaw6mLh78gbDCexhF0OZkd+SEUucrjD2GOfJyZ88vMXfdzkUBMp1A65Y+WXc1yji7ol7y8xjXK6eHKOhcsey6sjLj2qQbg9yavgAfGeICuEYe1oiw9y0fGQ60p2HAuMIaZh2IYe1NCL2OCEaGncoRdMpHe84QQ82+GW0vKciuablhfK6YZs3MPL4UDeHO8qzS1pNwbcnJV7Gis5OtwXQ0d5L+Mh8j4gMwYcD3HQ+F0vYxiLocEgup1hRsYcCjY8HBsIvUwgakTeaKNPZmKLGiLuSrnNvJ5mV9hT5NpGm4bhiOG0y8HNDRLTtdQ+N97GjwaPEBnT3OaV71pCPaxQYQ6wMBodbF2W9bB2OTqzTTSFjuWFLAwu8JljCZYX7CMyww3dPsB5Z55zCZicDnPh/QMlKm65nNmYpAb3l2wzd/C5zM73z8jVy2HUYcleWad60UFZ5HYRpo8DaeBD+s8QFdfe+1jCMTvY0PcYIQe52mTgxdo9RtC6L4MIUx62jUCMPY3GKOqbnMWXTOXknUPIzyDmJmJvfLzGZtb527+flzGJnk87L35EhGm4cFpuknMXgMEyN9yNEcx7z5DxAXvnlSHfc58vJOZ3c7ufAyL2eY+efLy5y+snJnlfPkt9hL5vw+HlzLmTq5xP55/py8ws7E8/9fP+dvYacvPmYDqyMQ5S+0hTBwrvayQsIUX12wXtQyFEKYbirn++aAj3XGXyMkOGbyYUTsGZYZTt8/LMKHnwJ5ZiXh7CJzuWQ7SGbzSdx4gJ8BAt7mmkHt5Di8tnW5TJefLL18vLlm8f0/2vz58F87zbOf8A45N8uoJ8GX8OR/P4Zeozz5/bZd/8c/POd5mE/nyPLz88y+Fkef8APleWEL3OYUxazF3EI5QzVj2sc2NLuYVmJRbDgWZC9DL1E/2KMC7llzkeeXAm9mfO/Py8gwHU+ds+3Obs5HU4zY5vkHWpknK2/AWD7iZMHiAm+eeE7y4zm+3zzmEvuU8pbfc1kTD2N5gQgd5RDuvBGOj2pT2kNCmIb16V6zDovYiUQ7imMcC3uKWEOVmDheCZfcUZIvUwoRCufVbh1DgzIlj4CU0x8FPuHiAnYER7lKyK9gWPOW5s4LORHU4AoKLHs5W4SuR15Roxl3thDlkYsDcYTNrq7nMulCEep0WxhTfB0Iwv7jT1rcWmEeBGBseBB8DRguA6kIR2G9HCxgdwYPc0e88QFetYHeuYRu+4IZgQeDWShI9fK9C2W+9AHgZNC/K8vUzPPOLT7bvqJdMsDyy71cqpgTqLw5w0vBMDCje8pkGhpl7mBRQ0nBGc+e13hloQ0vctixcmHhnDCJHuzDlcvn3pd3yFOoMMBfEBemGYeDej35KaC+xtIy8nZyyRGjJwHRwicGO0lvURKKtU33bGIW+QbyhNOY3nhfIvlZ5XDlld1xy84Qp5ruZdW3qB7AI4stF4EYjGCDvWimmDvSEYUA9QUwjbbHe0OgFPWkGMfcQh7ij3j4gJ6p4OCPO+0c0xzDrUGgp7csNj2GYQhfN4E5aPt5zJqBuI0DgIvAigrE3roAkYhuGNI+BRHa7nUEogbmiNNDE4JkgeBCMQo4NZoih2LgoE9pn2n9bT4gK06Hg6nexhmCdiQWNPZnob68+Y6EyHU5fRfVYZKcZibszNkKu6U3kPtwhbRuAzBmXS3fnMIZhCNHsSKsYuj7FKQtwu9hM0ikG+shm5y7TDWbHJHemxgHaQSGYw7kGk7z7p4gK/m6e9ft52ntv7cEewpszg7CrLdXwzgj4ASzscNuhwNXV3uxzGHexzgQ6hjDNCw7Up7HVKcEeDDQaHgpTT3NMVp7hRjF6xiUdzQjQnvPkfkfEBNyD7knPPLyHtWGeV3yewpjMpy7Cc4JTA4E5sWOZZwKIMWBwMBseALaancQh2EIaOHgMF2MODDDpe9h0tPUUpo4OsOUO5YX9sTDwIznmJfgFLhfAF8Ai/1ne+ICgGfjIP/Z2CdxsfC4aHgKRTgfcDV73ofkHB2Ev7rtfum40PuMyRh7RHwUwHaaDHvHRwd5TH43xAVwie1wQO8oivcRpDuIuFHtKHQfcBDvInvQh711faYet0dE7mGx6z+sdhwayPtKND2rF0epgBqdwwfe0x9wQ+4eICdqEX3jPNOwXSzJ2Gc6JfYznSxZy7WDTG/ahGjeEI6NN7yLB7wglI4HeOrGngxwwdTqNDvAwKR7miPx+XNoYdTLjnME73LaMO0l4Y095q9pqx7zxAWJDwYU5t8CiJ2Dgoe3lCBhbOBTrmWdS89CDfl1DOfnsZ5+e8Y/DNMITO9W3n5E5DFN4kzLIQYbkoacAPWQmQ0d60DmDS9mTnljCJvJkh8OYhwK5x8v8Aj+fJOwLaPNIPAYwRFe8jb7mMPa+ICxL7jB7yAdq6uTvKFvPc6OT252vcYT3Lh7ShKae0qyHuR2Hcn/Yo8Gl/qPcl3h77XmQh3l25fcmh7xaPEBW3/wCRwUdr0X2rhps63YzkX2ptXsMmA7WKJnyHvvkmfczPvC9SneMRj3OjRtdxhnP3pBoHsCciODwzbnvtJeeZT2kXJzD/AOg8QJxMHrOL44OLO7Di9OB2uw4twm9f4Mdrq7TQf3sCHxj+s0fuv+Z0PpIx2v8A3cGCPqTU6T9btaIeg9J+kdp2p0H6Gn3sdj+lwaHxtP6GgfY6u4z+Ygj8bT0v5k/5iR+sMHyI9B9TT0mp0PxDrf5RKdWnR1fWx6D6WjBg1NTrKKf7zVDrOlIj+ZMDq97oxgfYQph7UwURaH8qMHBqcEdRjT9ThEhtepKSFGH6GrpPuroQY4fxN1kREg4fAR1R+hIOjHB4DRoU/QhgRMDhOh9RgjEfpGjJRsdTU6Ck0GZMJ+FwiRER+I0dCCjQ4H8i2kze8oodUmSyhtixPmKvMEiIkEiasdUg4yXDOEzFPvkUYZHN+pg6NImTBGWTJMlw+cq8hkUY2mo7CnCDkmZYpHR++ohmXmCJm6NSnDQlMLmazLGCUPyiDDMvMSBkMiOxjhKyy5mDCKP4rIRwUJkM4NpRTDRhEzo0fNakYXYLaGabTJ0GGi5luI0UQ+UiKURzBhmm4ZhqNCxpAgNLdDH5RbMOYMMwzbM+m4EAmXDGEyR/CwGMzZhDJRg2W5NoRwoLhD+5ojM0Gabja2Edi3Mw2OpV2uEP7SKjLjkoheZZHNDRcYFDsMGWXMhH7+QgLC2Mc3blLhE0KcFOAvC05/A0zLCDqRLI0zOp6mMC7oj98IrLMxhAGEXUIrH0kunYR/uJlAmc6ZWCUaOAI6HSuaaI/Ky6aGmEcx1NuYGHpALNj94isNVaMO1hmsuDY4aXLgPvOwFhS0QSikyTMDodCESBGH3yCMI+gdRpCGHYtLApD7xTHYDqEcMBq5ldxgjT98AwrsAYQpgigdBtXQ/Cu1dD0IzNmXV0KaML8zQRYwIw6Wm81zgvpNQrL+AcwoWXcUcGhBGCgu01Vl/OGpYZg7Mw0IxBo1SjACn4swuOZmGwpjBKzgmacBCl0fwtFLBYmBHVKSJM3MlHSUPzGmUHCIjoA0JmGSZA9Cwh+ApwaJE6SkSMUzgjDYfQ4YEcJ6kloxDMswehj9Iwp2NNLsYZnIjSwNh9RqRDYajG0jcctBFX8xgdhovoGiIEVoj9ZQ4EjCnBGkRGBlIsdD87DVhDRoohkYZlrAIwjD8j6ja7DajmyEYZUP0G00KTQiMMwS4kcJ/i9TgjbDBA/Oex6EgwyYJk/a0dJRCDRG8xMJT/ACNjQ0zJCMaOLaUxgkHafuTQojQ6sP3nA0F4tb73xAe1Ot8RW/8A/6MAAwH/AJHGEXi4hVuXoSXHi7mpHi0mpsApdH+ThjVnqXizrTRtYOxP4PqNw/ydWHpGn+Au9g7Q/e6OrRHoXBH9jGFB6H0mwh+oo9DHYhqH7zUoKYx9j+xix9q4P3Opo0dYfYuhizYR6CPpQ/M6vqY+hDBorR+Y9TTF2vpD9K4KPc7H8409BhhEj2n2EY6kXpY9YMPpcBh1IaXtfSq00fjV2GDDsIR9Jh/xOg0MHeRf0NNOA7HVhg+xpoj7ww/SYWG0g0dYfpYvqNhHcatEYB87S4fkdjq/kc+x7CjYn1NC6gfEegpgR2h+MH1L0G1wauy7/E4Dot2Gp0LRo6v1B0EunYrheg9AQp+UMEL6Hax1afUGwj+ApdGA0vhei0R0fwsdMuAYaODamFDpPoI0aNGjDacM7FrL8xtCOAdHY7DSzBsuMfmfW7hIbLw0UX9KRSX0HS7XF3q4L0si/wBxHaYNV+IgaOYqOCicoH4jBsdD4nCtFmdOVK/iTA0dDhh6SHOK5pjemVh8oaBSQImAB1NSFN3lzjkLHJF+VpDOA0zDY6MIsMKuLln0EdWOHDCLDUI5bt5i0R0uzB816NXgMNAJRFIkecGikC2DF+YtcFZYQphTMzLTOVhOcJyM5mXnnm0GGHy52AuYsurumcwhOV3bzq7uczldhM0uGH9rC8MuIQuMcyw5OSZhGxzFLnnzWK86HR/A0RjWVIuYy+Quc6ZzLLEuZzzgQgEzF+U0DBmnNZmchMphw3cW7F5y8l0wy0f2uw0y4vkEfQwICqHIFgEY0v3iG0gOUOUZnBRo1l5uZl5XcGLT87SauYCwC9TDCEuChR5FxiRPxK02QijpZhg06kORbm4crmZnJej+Aw0Qjhh0FMzGZpsjLAmXOH5gdCiZJl0aKMwgRdHOYHIAh5wRj85GJZFKA0NQwkYmEmWy451fwlBHU4MVxzzccsu70fxqwJlhT8RThl05nJisYVn8LgTVKKBCnDFArOCzOUmfqKJd7HRp1TRXVp55LE+pwMNSOH0C4I0ql2Sz6nBsdFgYKWXRFxk5WFkuP0ursNhgwg0xojFgaP5TU1Y6FODYLAhM6B856XoMOHY4Ujo/W7TQp9BqmjkjdZjD+8oOhihoR2kQMH8HYUaLTFaSED9Ro4XQ2uEsp0dE/UahobFYf5PRfQuhq6OpoYD6X4mOw9QfxNrDoYPsD9xh7z/N9BteLubCmn0nFweLObn2PFxKU49Dh/qOPUcY98RTwOLs8fl4vxwDi4vGAPQeICzHGWeLO+44xZ+p4wT/AFPGCP4nFrOMEel4vhH3HiAsDxdHjFP3HjolH/ux45ZxiX/o8XV2vFyOPScDodCHzHsOL+eDxfD1nF4NXR/If+b8R94j8Z6D0H1n/JPYnF+fnNE3HxFODU4vJGji5uww7H8ptdrq6uHYbD9Lo+q4mpgTY/yQq4jHR1Y+k+s9bGKOgMBhoO1/tfWUdTTGMFNolH1psNDBDQ1IR/U7UzhI4OgCWFEHB+12vpIbT8zo7GGHvSMbf1vqOkaY3Vv+Lq+g1dUw0mjDB9D0HxJ7DUX8h6z0nQelpifkOg1PjdUf2nYmGk4sp6yjV1PsfU+sw0MI/nOg6DpdTaPFlO0wP2HB9ZT0vFpeg6X9R0nQ0cYd4vRocXw4wz+x7DxAQM49x4jP4+x4tZxjDi8HF/eL8cYw8QEHPEChni/HF9fEBgX9Lh41D9gf2G1eLs/5P9ho/W/+gYf1nvcH0mr4gNMbTi0naw+s/sMGF+g2PaepgdJH6HuY7wOLu0sIaGH6H3uoRpo0YR/Ew1PWbl1I4fndrD0O4jGnUw/Ox0Oko6nRhRsND8bsYwO5WGwCh+8UauA9AegxmDQYaz9bRCnVjqeghdZhhcOp98h0PcoU4IRcEcP4DAGGOrsd2aVlmDV+YNGFOxpNopqBRh0MEfvOiujqRhcXU0NjTAwuH5lwBFphGEY0mmWnpDZf0kFAKcEYGHQ+J2jDC/fKcG0wYaA6DaNLMujD+4vUGBA2MIYcFGhsIQo0YffZdxwq0bg0aehKWP43QhGnabFh6XBHKF/O6Ax1MEdWNLl6SOt4fmNXY+wiwD1JhVfnFjq4YdLTRMx6BpKIYPlYQNTQp2GuclG02Kq/MDWRhHV9awGEdHDQFEfmUEUIsN7Clw4djR8yDShneYWsoYKYwpcPzOEHQ9xqQwH1CwgJHoOgi9Lq6P0pF2mCnVwxpVoXaw+thudGmmlmZf6DGU0Nzq0qC0/6HS7GDhYh0n43R9gYDQjAAj0n1Edz0mAZnDT+V6nqdA/QUel3tCaP6zR6DBHQ0CZdQ/wehw4dg9CnFoek2LGGGP8AAPUO0OLSaHWcWc7Xi4h7F4tp4gIEce56DR8S0FeMO8XJjxgHi+HGJSHFuOLocZA9acWd3vFzPW8W5Ol0Tj4vsYP5D/q/oe5OMkYP7z/q08fU0MHFyPEBgHi8nGIPEe5w4/rHoek4sxq0GpxfTi6Gxo9DoP8Ao9LCOrh1Ti3sOLgcGGBj/mYO40f4mj6DBQ8W86XV/i+xfSRP8D42Gph/xeh9Jh1YOr+l2ujTuacEf3HScCji1LqRwYP2m00Now6E4uJq6JxdWGjRofvHg6mjxcGGBw06H5nY6PqdBhBjgf1JsYepSjAn63pegKdB0IJxaHQCiI6uGP8AkOjqYAjTHpY/xaOkeLU4dFYYInSUYPpOpelwPpP3ui6MNB4tpR0Gg/E/lNXR2jgjDi9mBNXi0uqUHpfiND7E6BgA7Trfpd5Di+kIcXsDvf5EOLw6EXi5nQcYADi+IHxH+Rh/8jDofpdX2n6z+p/c/wDR/ecWl/8Ag/CcZU7EhRD9gLT7SH6iBEo+Q/wCO14uTA9p6n7GNET0lFGH+SEfUQNz/ebR4tjuY0Jo4Y8Wl1YlG1jTxbE6BNHD+91KdXDocW0pBhHB0PFpIlMHV4uBSDDoIaPrafoMPQUUwjFIQ1OLM6EWCRiQ0Nj/AKOxppo2tP8AAjhg6ECnDxak0GGrHoehP0mjCECjVNHR6T8zsNBTBApNg+k/YHSUnSnFnPUbD+IUethtE/g7gwPFuPiOLiam02vGEeLWavH+YcXEOMS+I7JH5zj+H/N4xBx/D9jxpTxAXljxe2JT+d6TgOjRxdimmEcFPF1eLu0YMOGODi5FLTqfW7HDvY9DofnNHB63DqaMCBB/IRNrT7DQ6H9DtKN562n8qnqcOj8RHoafyMDYet9jCg+w9D8Zg+s94eg6T6in0vyMP1tHuPscPpPebD+8/wDU/O7CPGcOLSd5+54wro7X/u9B6WjD+x+4U9TghxZXjAsOLyUvF9Hi8HpcOhxaxwYfQf4PQ6jhjTh4trSU4YaMaaf1kz2vS/tYYcPpY0Oi/uNjDpGGjS/5lPsMJgwwwcW0p2sI8Wd2upgjD97HoTpadp6D9Lh6nAet4tbgoaUjh4uQNMMMf3PSOrhwfoeB8Q06v7U7zYcXA1GOpteLOx0KYcWkwfE8Xp8ZhU//oQ==";
        byte[] _byWSQuserReniec;
        string _ANSIuserReniec = "Rk1SACAyMAABKAA1AA0AAAIAAgAAxQDFAQAAACgsgLgA0HUAgKYAtogAgN0AwYsAgM0ApZMAgPIA7oEAQGcA7w4AgJoAiWIAQKUAf6sAQQUA/IkAgOcAhKAAgQkAmkQAQQ0BDzAAgREBF3gAQEYBKAoAgFcBTAUAgKAA0ykAgI0Av4AAgNQBAHEAgO0Az4wAgG8A5SUAQP0AxZQAgGkBBwwAgNIBKRYAgJ0BNwsAQJ4AdV4AgHEAgggAgEMA13gAgQ8BJWkAQOQAWk8AQKUAPKwAQMkAy4IAgKYApmEAgIEA9mYAgHAAzYAAgOYAq5QAgO4ApZoAgQUA6pIAQFsAvnoAQFUA+hoAgFYAoHIAQPwBJBEAQL8BQgsAQGMBPGAAQHEBVwYAAAA=";
        string _ipCliente = "190.40.131.119";
        string _macCliente = "9C-5C-8E-3D-07-67";
        int _TypeWSReniec = 0;  // 0 - Ambos | 1 - Solo WSQ | 2 - Minucia

        int _TypeWSMinucias = 2;    //2 - WS2 o 3 - WS3
        int _TypeWSWsq = 2;         //2 - WS2 o 3 - WS3

        byte[] _wsqRight = null;
        byte[] _wsqLeft = null;
        
        //private int _threshold = 3500;
        
        //0 - Todo 
        //1 - Solo Info
        //2 - Solo Foto
        //3 - Solo Firma
        //4 - Info + Foto
        //5 - Info + Firma
        //6 - Foto + Firma
        private int _typeget = 0;
        private int _wFoto = 140;
        private int _hFoto = 140;
        private int _wFirma = 200;
        private int _hFirma = 85;


        private bool _isConfigured; //Solo para control en initialization

        private string _trackId;
        
#endregion Private properties propietario

#region Public properties propietario


        public string CodUserRENIEC
        {
            get { return _coduserReniec; }
            set { _coduserReniec = value; }
        }

        public string UrlWsWSQ
        {
            get { return _urlWSWSQ; }
            set { _urlWSWSQ = value; }
        }

        public string UrlWsMin
        {
            get { return _urlWSMin; }
            set { _urlWSMin = value; }
        }

        public int TypeWSWsq
        {
            get { return _TypeWSWsq; }
            set { _TypeWSWsq = value; }
        }

        public int TypeWSMinucias
        {
            get { return _TypeWSMinucias; }
            set { _TypeWSMinucias = value; }
        }

        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        public string RUCEmpresaRENIEC
        {
            get { return _rucEmpresaRENIEC; }
            set { _rucEmpresaRENIEC = value; }
        }
        
        public string IpCliente
        {
            get { return _ipCliente; }
            set { _ipCliente = value; }
        }

        public string MACCliente
        {
            get { return _macCliente; }
            set { _macCliente = value; }
        }
        

        //VER SI SE SACA
        public int WFoto
        {
            get { return _wFoto; }
            set { _wFoto = value; }
        }
        public int HFoto
        {
            get { return _hFoto; }
            set { _hFoto = value; }
        }
        public int WFirma
        {
            get { return _wFirma; }
            set { _wFirma = value; }
        }
        public int HFirma
        {
            get { return _hFirma; }
            set { _hFirma = value; }
        }
        
       
#endregion Public properties propietario

#region Private Method

        /// <summary>
        /// Inicializa las variables del objeto desde config, para no perder tiempo en buquedas luego
        /// </summary>
        private void Initialization()
        {
            try
            {
                LOG.Debug("");
                if (_config == null || _config.DynamicDataItems == null)
                {
                    _isConfigured = false;
                    return;
                }

                foreach (DynamicDataItem dd in _config.DynamicDataItems)
                {
                    if (dd.key.Trim().Equals("UrlWSWSQ"))
                    {
                        _urlWSWSQ = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("UrlWSMin"))
                    {
                        _urlWSMin = dd.value.Trim();
                    }

                    if (dd.key.Trim().Equals("TypeWSWsq"))
                    {
                        _TypeWSWsq = Convert.ToInt32(dd.value.Trim());
                    }

                    if (dd.key.Trim().Equals("TypeWSMinucias"))
                    {
                        _TypeWSMinucias = Convert.ToInt32(dd.value.Trim());
                    }

                    if (dd.key.Trim().Equals("Timeout"))
                    {
                        try
                        {
                            _timeout = Convert.ToInt32(dd.value.Trim());
                        }
                        catch (Exception ex)
                        {
                            _timeout = 30000;
                        }
                    }
                    if (dd.key.Trim().Equals("RUCEmpresaRENIEC"))
                    {
                        _rucEmpresaRENIEC = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("CodUserRENIEC"))
                    {
                        _coduserReniec = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("WSQUserRENIEC"))
                    {
                        _WSQuserReniec = dd.value.Trim();
                    }
                    _byWSQuserReniec = Convert.FromBase64String(_WSQuserReniec);
                    if (dd.key.Trim().Equals("ANSIUserRENIEC"))
                    {
                        _ANSIuserReniec = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("TypeWSRENIEC"))
                    {
                        _TypeWSReniec = Convert.ToInt32(dd.value.Trim());
                    }
                   
          //VER SI SE SACA
                    if (dd.key.Trim().Equals("WFoto"))
                    {
                        _wFoto = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("HFoto"))
                    {
                        _hFoto = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("WFirma"))
                    {
                        _wFirma = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("HFirma"))
                    {
                        _hFirma = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("TypeGet"))
                    {
                        _typeget = Convert.ToInt32(dd.value.Trim());
                        if (_typeget < 0 || _typeget > 6) _typeget = 0;
                        if (_typeget == 0 || _typeget == 1 || _typeget == 4 || _typeget == 5)
                        {
                            _getInfo = true;
                        }
                        if (_typeget == 0 || _typeget == 2 || _typeget == 4 || _typeget == 6)
                        {
                            _getFoto = true;
                        }
                        if (_typeget == 0 || _typeget == 3 || _typeget == 5 || _typeget == 6)
                        {
                            _getFirma = true;
                        }
                    }
                }
                _ipCliente = Utiles.GetIP();
                _macCliente = Utiles.GetMACAddress();
                if (string.IsNullOrEmpty(_macCliente) || _macCliente.Length != 12)
                {
                    _macCliente = "9C-5C-8E-3D-07-67";
                }
                _isConfigured = true;
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Connector.RENIEC Initialization Error", ex);
                _isConfigured = false;
            }
        }

    
        private byte[] ExtraeSampleFromParamIn(XmlParamIn oXmlIn, int type, out int sampleextracted)
        {
            byte[] sampleEx = null;
            sampleextracted = 0;
            
            try
            {
                LOG.Debug("ExtraeSampleFromParamIn In...");
                foreach (var sample in oXmlIn.SampleCollection)
                {
                    if (sample.Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004 &&
                        (type == 0 || type == 2))
                    {
                        sampleEx = Convert.FromBase64String(sample.Data);
                        sampleextracted = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004;
                        LOG.Debug("ExtraeSampleFromParamIn ANSI Extracted = " + sample.Data);
                        break;
                    } else if (sample.Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ &&
                               (type == 0 || type == 1))
                    {
                        sampleEx = Convert.FromBase64String(sample.Data);
                        sampleextracted = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ;
                        LOG.Debug("ExtraeSampleFromParamIn WSQ Extracted = " + sample.Data);
                        break;
                    } else if (sample.Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW &&
                               (type == 0 || type == 1)  )
                    {
                        LOG.Debug("ExtraeSampleFromParamIn RAW Extracted = " + sample.Data);
                        byte[] raw = Convert.FromBase64String(sample.Data);
                        WsqEncoder encoder = new WsqEncoder();
                        encoder.EncodeMemory(raw, 512, 512, out sampleEx);
                        sampleextracted = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ;
                        LOG.Debug("ExtraeSampleFromParamIn   => WSQ Generated from RAW = " + Convert.ToBase64String(sampleEx));
                        break;
                    }  
                }
                LOG.Debug("ExtraeSampleFromParamIn Out!");
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Connector.RENIEC ExtraeSampleFromParamIn Error", ex);
            }
            return sampleEx;
        }

#endregion Private Method

#region Implementation of IConnector

        private string _connectorId;

        private DynamicData _config;

        /// <summary>
        /// Id identificador del conector
        /// </summary>
        public string ConnectorId
        {
            get { return _connectorId; }
            set { _connectorId = value; }
        }

        /// <summary>
        /// Pares de key/value de configuracion para el conector
        /// </summary>
        public DynamicData Config
        {
            get { return _config; }
            set
            {
                 _config = value;
                 if (!_isConfigured) Initialization();
            }
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */
            int _ret = Errors.IERR_OK;
            string msg;
            string xmloutbp;
            int iretremoto;
            int _currentThreshold;
            int _SampleExtracted;

            DynamicData oXmlout = new DynamicData();
            oXmlout.AddValue("message", "");
            oXmlout.AddValue("trackid", "");
            oXmlout.AddValue("status", "");
            oXmlout.AddValue("result", "");
            oXmlout.AddValue("score", "");
            oXmlout.AddValue("threshold", "");
            oXmlout.AddValue("timestamp", "");
            oXmlout.AddValue("personaldata", "");
            //Added 02-2015 para agregar todas las BpTxConx cuando son varias
            /*
                 Consultationtype > string > nombre metodo
                 Connectorid > string
                 Trackid > string > Trackid externa
                 Status > int > Retorno de servicio 
                 Result > int > 1-Verify Positivo | 2-Verify Negativo (Si es verify si es get 0)
                 Score > double > Score obtenido (Si es verify si es get 0)
                 Threshold > double > Umbral utilizado (Si es verify si es get 0)
                 Timestamp > string > fecha y hora tx externa
                Devuelve un Dynamicdata de la forma:
                    <DynamicData>
                        <DynamicDataItem>
                            <key>tx1</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                      .........  
                        <DynamicDataItem>
                            <key>txN</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                   </DynamicData>
             */
            DynamicData oExternalTxs = new DynamicData();
            oXmlout.AddValue("externaltxs", "");  //Tipo DynamicData

            //oXmlout.SetValue("message", "");
            //oXmlout.SetValue("trackid", "283734647");
            //oXmlout.SetValue("status", "0");
            //oXmlout.SetValue("result", "1");
            //oXmlout.SetValue("score", "1000");
            //oXmlout.SetValue("threshold", _threshold.ToString());
            //oXmlout.SetValue("timestamp", FormatFechaHoraSRCeI("23112013100000"));
            //xmloutput = DynamicData.SerializeToXml(oXmlout);
            //return Errors.IERR_OK;

            try
            {

                LOG.Debug("Bio.Core.Matcher.Connector.RENIEC - Ingresando...");
                if (!this._isConfigured)
                {
                    msg = "Bio.Core.Matcher.Connector.RENIEC Error = Connector No Configurado!";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
                }
                else
                {
                    LOG.Debug("Bio.Core.Matcher.Connector.RENIEC - Is Configured!");
                }


                //1.-Deserializo parametros de entrada
                xmlinput = xmlinput.Replace("&#x0;", "");
                LOG.Debug("Bio.Core.Matcher.Connector.RENIEC - xmlinput = " + xmlinput);
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlinput);
                if (oXmlIn == null)
                {
                    msg = "Bio.Core.Matcher.Connector.RENIEC Error deserealizando xmlinput";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //Tomo el WSQ si es que viene
                LOG.Debug("Bio.Core.Matcher.Connector.RENIEC - Extrayendo Sample para verificar from xmlinput...");
                byte[] sample = ExtraeSampleFromParamIn(oXmlIn, _TypeWSReniec, out _SampleExtracted);  //Extraer SAMPLE!!
//                byte[] wsq = ExtraeWSQFromParamIn(oXmlIn);

                if (sample != null)
                {
                    LOG.Debug("Bio.Core.Matcher.Connector.RENIEC - (sample != null!");

                    string[] result;
                    byte[] sampleRight = sample;
                    byte[] sampleLeft = sample;
                    string nuRuc = _rucEmpresaRENIEC;
                    string nuDni = oXmlIn.PersonalData.Valueid; // "09955646";
                    string coduser = _coduserReniec;
                    string ipCliente = _ipCliente;
                    string macCliente = _macCliente;
                    LOG.Debug("Bio.Core.Matcher.Connector.RENIEC Call BiometricVerificationService.verificarIdentidad con valores:");
                    LOG.Debug("         => WSQ=" + Convert.ToBase64String(sample));
                    LOG.Debug("         => nuRuc=" + _rucEmpresaRENIEC);
                    LOG.Debug("         => nuDni=" + oXmlIn.PersonalData.Valueid);
                    LOG.Debug("         => coduser=" + _coduserReniec);
                    LOG.Debug("         => WSQcoduser=" + _WSQuserReniec);
                    LOG.Debug("         => ANSIcoduser=" + _ANSIuserReniec);
                    LOG.Debug("         => ipCliente=" + _ipCliente);
                    LOG.Debug("         => macCliente=" + _macCliente);

                    if ((_TypeWSReniec == 0 || _TypeWSReniec == 2) && _SampleExtracted == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
                    {
                        if (_TypeWSMinucias == 3)
                        {
                            using (WS3RENIECMin.BiometricVerificationMinutiaService ws = new WS3RENIECMin.BiometricVerificationMinutiaService())
                            {
                                ws.Url = _urlWSMin;
                                LOG.Debug("         => URL WS3 Minucias =" + ws.Url);
                                ws.Timeout = _timeout;
                                LOG.Debug("         => URL WS3 Minucias Timeout=" + ws.Timeout);
                                result = ws.verificarIdentidad(_ANSIuserReniec, Convert.ToBase64String(sampleRight), Convert.ToBase64String(sampleLeft),
                                                               nuRuc, coduser, nuDni, 1, true, ipCliente, macCliente);
                            }
                        }
                        else //Es 2
                        {
                            using (WS2RENIECMin.VerificacionBiometricaService ws = new WS2RENIECMin.VerificacionBiometricaService())
                            {
                                ws.Url = _urlWSMin;
                                LOG.Debug("         => URL WS2 Minucias =" + ws.Url);
                                ws.Timeout = _timeout;
                                LOG.Debug("         => URL WS2 Minucias Timeout=" + ws.Timeout);
                                result = ws.verificarIdentidadAP(Convert.ToBase64String(sampleRight), Convert.ToBase64String(sampleLeft),
                                                               nuRuc, coduser, nuDni, 0, true, ipCliente, macCliente);
                            }
                        }
                    }
                    else
                    {
                        if (_TypeWSWsq == 3)
                        {
                            using (WS3RENIECWsq.BiometricVerificationImageService ws = new WS3RENIECWsq.BiometricVerificationImageService())
                            {
                                ws.Url = _urlWSWSQ;
                                LOG.Debug("         => URL WS3 WSQ=" + ws.Url);
                                ws.Timeout = _timeout;
                                LOG.Debug("         => URL WS3 WSQ Timeout=" + ws.Timeout);
                                result = ws.verificarIdentidad(_byWSQuserReniec, sampleRight, sampleLeft, nuRuc, nuDni, coduser, ipCliente, macCliente);
                            }
                        }
                        else  //Es WS2
                        {
                            using (WS2RENIECWsq.BiometricVerificationService ws = new WS2RENIECWsq.BiometricVerificationService())
                            {
                                ws.Url = _urlWSWSQ;
                                LOG.Debug("         => URL WS2 WSQ=" + ws.Url);
                                ws.Timeout = _timeout;
                                LOG.Debug("         => URL WS2 WSQ Timeout=" + ws.Timeout);
                                result = ws.verificarIdentidad(sampleRight, sampleLeft, nuRuc, nuDni, coduser, ipCliente, macCliente);
                            }
                        }
                    }
                    LOG.Debug("Bio.Core.Matcher.Connector.RENIEC result[] != null =>" + (result!=null));
                    LOG.Debug("Bio.Core.Matcher.Connector.RENIEC result Length => " + (result != null?result.Length.ToString():"NULL"));

                    if (result != null) // && response..EncabezadoRespuesta != null)
                    {
                        /*
                            Si Corresponde(70006)
                            No Corresponde(70007)
                          Ej:   resp[0] : 70006
                                resp[1]: Marco Antonio
                                resp[2]: Nina
                                resp[3]: Aguilar
                                resp[4]: 07/04/1954
                                resp[5]:31/10/2014
                                resp[6]: 01-01-3000
                                resp[7]: bytes foto
                         * 
                         * o Error:
                                resp[0]: 3
                                resp[1]: Entidad no se encuentra registrada y/o no tiene convenios con RENIEC
                        */

                        _trackId = "RENIECWS_" + Guid.NewGuid().ToString("N");
                        string _timestamp = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff");
                        PersonalData pdataOut = new PersonalData();
                        pdataOut.Typeid = "DNI";
                        pdataOut.Valueid = oXmlIn.PersonalData.Valueid;
                        if (result.Length == 2 && !result[0].Equals("70006") && !result[0].Equals("70007"))  //Hubo error
                        {
                            LOG.Debug("Bio.Core.Matcher.Connector.RENIEC result[0] = " + result[0] + " - " + result[1]);
                            oXmlout.SetValue("message", "Error indicado por RENIEC = " + result[0] + "-" + result[1]);
                            oXmlout.SetValue("trackid", _trackId);
                            oXmlout.SetValue("status", result[0]);
                            oXmlout.SetValue("result", "2");
                            oXmlout.SetValue("score", "0");
                            oXmlout.SetValue("threshold", "0");
                            oXmlout.SetValue("timestamp", _timestamp);

                            
                        }
                        else  //Verificacion Positiva o negativa
                        {
                            LOG.Debug("Bio.Core.Matcher.Connector.RENIEC result[0] = " + result[0].Length + "...");
                            for (int i = 0; i < result.Length; i++)
                            {
                                LOG.Debug("Bio.Core.Matcher.Connector.RENIEC      result[" + i.ToString() + "] = " + result[i]);
                            }
                            oXmlout.SetValue("message", "");
                            oXmlout.SetValue("trackid", _trackId);
                            oXmlout.SetValue("status", result[0]);
                            oXmlout.SetValue("result", result[0].Equals("70006") ? "1" : "2");
                            oXmlout.SetValue("score", "0");
                            oXmlout.SetValue("threshold", "0");
                            oXmlout.SetValue("timestamp", _timestamp);

                            /*
                             *      resp[0] : 70006
                                    resp[1]: Marco Antonio
                                    resp[2]: Nina
                                    resp[3]: Aguilar
                                    resp[4]: 07/04/1954
                                    resp[5]:31/10/2014
                                    resp[6]: 01-01-3000
                                    resp[7]: bytes foto
                             */
                            if (result.Length > 2) //Entonces hay datos de la persona y quizá foto
                            {
                                pdataOut.Name = result[2];
                                pdataOut.Patherlastname = result[3];
                                pdataOut.Motherlastname = result[4];
                                //pdataOut.Birthdate = DateTime.Parse(result[4]);
                                pdataOut.Documentexpirationdate = Utiles.ParseFecha(result[5]);
                                pdataOut.Photography = null; // result[6] != null ? result[6] : null;
                                pdataOut.Nick = result[6] != null ? result[6] : null; //Es estado de DNI
                            }
                        }

                        //Completo Personaldata => Si error solo DNI sino datos completos devueltos
                        oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(pdataOut));

                        int res = 0; // (Score >= _currentThreshold) ? 1 : 2;
                        oExternalTxs.AddValue("txVerify", GetDDFormExternalTx("verificarIdentidad", result, _trackId, _timestamp));
                        oXmlout.SetValue("externaltxs", DynamicData.SerializeToXml(oExternalTxs));
                    }
                    else
                    {
                        msg = "Bio.Core.Matcher.Connector.RENIEC.Connector - Respuesta Erronea del RENIEC - " +
                            "Result=" + (result==null?"NULL":"Not NULL");
                        LOG.Fatal(msg);
                        oXmlout.SetValue("message", msg);
                        xmloutput = DynamicData.SerializeToXml(oXmlout);
                        return Errors.IERR_CONNECTOR_MALFORMED_ANSWER;
                    }
                }
                else
                {
                    msg = "Bio.Core.Matcher.Connector.RENIEC.Connector Falta huella para enviar";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
           
                xmloutput = DynamicData.SerializeToXml(oXmlout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Connector.RENIEC.Verify Error [" + ex.Message + "]";
                LOG.Error("Bio.Core.Matcher.Connector.RENIEC.Verify Error", ex);
                oXmlout.SetValue("message", msg);
                xmloutput = DynamicData.SerializeToXml(oXmlout);
                return Errors.IERR_UNKNOWN;
            }

            return _ret;
        }

        private string ConvertToNumber(string status)
        {
            string ret = "0";
            try
            {
                ret = status.Substring(4);
            }
            catch (Exception ex)
            {

                LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.ConvertToNumber Error", ex);
            }
            return ret;
        }

        private string ReadFromHDD(string path)
        {
            string sret = null;
            try
            {
                System.IO.StreamReader sr = new StreamReader(path);
                sret = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.ReadFromHDD Error", ex);
                sret = null;
            }
            return sret;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */

            string msg;
            xmloutput = null;
            int iretremoto;

            //DynamicData oXmlout = new DynamicData();
            //oXmlout.AddValue("message", "");
            //oXmlout.AddValue("trackid", "");
            //oXmlout.AddValue("status", "");
            //oXmlout.AddValue("result", "");
            //oXmlout.AddValue("score", "");
            //oXmlout.AddValue("timestamp", "");
            //oXmlout.AddValue("personaldata", "");

            //try
            //{
            //    if (!this._isConfigured)
            //    {
            //        msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error = Connector No Configurado!";
            //        LOG.Fatal(msg);
            //        oXmlout.SetValue("message", msg);
            //        xmloutput = DynamicData.SerializeToXml(oXmlout);
            //        return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
            //    }

            //    using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
            //    {
            //        ws.Timeout = this._timeout;
            //        ws.Url = this._urlWS;

            //        iretremoto = ws.Identify(xmlinput, out xmloutbp);
            //        if (iretremoto == Errors.IERR_OK)
            //        {
            //            XmlParamOut oXmlParamOut = Utils.XmlUtils.DeserializeObject<XmlParamOut>(xmloutbp);
            //            oXmlout.SetValue("message", "");
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            oXmlout.SetValue("trackid", oXmlParamOut.Trackid);
            //            oXmlout.SetValue("result", oXmlParamOut.Result.ToString());
            //            oXmlout.SetValue("score", oXmlParamOut.Score.ToString());
            //            oXmlout.SetValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
            //            oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(oXmlParamOut.PersonalData));
            //        }
            //        else
            //        {
            //            msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error en WS Remoto [" + iretremoto.ToString() + "]";
            //            LOG.Fatal(msg);
            //            oXmlout.SetValue("message", msg);
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            xmloutput = DynamicData.SerializeToXml(oXmlout);
            //            return Errors.IERR_CONNECTOR_REMOTE_ERROR;
            //        }
            //    }

            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //}
            //catch (Exception ex)
            //{
            //    msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error [" + ex.Message + "]";
            //    LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.Identity Error", ex);
            //    oXmlout.SetValue("message", msg);
            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //    return Errors.IERR_UNKNOWN;
            //}

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de Recuperación. Ingresa información recuperar, 
        /// y se realiza la operacion.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Get(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */

            string msg;
            string xmloutbp;
            int iretremoto;

            DynamicData oXmlout = new DynamicData();
            oXmlout.AddValue("message", "");
            oXmlout.AddValue("trackid", "");
            oXmlout.AddValue("status", "");
            oXmlout.AddValue("result", "0");
            oXmlout.AddValue("score", "0");
            oXmlout.AddValue("threshold", "0");
            oXmlout.AddValue("timestamp", "");
            oXmlout.AddValue("personaldata", "");
                //Added 02-2015 para agregar todas las BpTxConx cuando son varias
            /*
                 Consultationtype > string > nombre metodo
                 Connectorid > string
                 Trackid > string > Trackid externa
                 Status > int > Retorno de servicio 
                 Result > int > 1-Verify Positivo | 2-Verify Negativo (Si es verify si es get 0)
                 Score > double > Score obtenido (Si es verify si es get 0)
                 Threshold > double > Umbral utilizado (Si es verify si es get 0)
                 Timestamp > string > fecha y hora tx externa
                Devuelve un Dynamicdata de la forma:
                    <DynamicData>
                        <DynamicDataItem>
                            <key>tx1</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                      .........  
                        <DynamicDataItem>
                            <key>txN</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                   </DynamicData>
             */
            DynamicData oExternalTxs = new DynamicData();
            oXmlout.AddValue("externaltxs", "");  //Tipo DynamicData

            msg = "Bio.Core.Matcher.SRCeI.GetInfo2015 Opcion NO implementada en el Conector!";
            LOG.Fatal(msg);
            oXmlout.SetValue("message", msg);
            xmloutput = DynamicData.SerializeToXml(oXmlout);
            return Errors.IERR_CONNECTOR_NOT_AVAILABLE;
        }

        private string GetDDFormExternalTx(string CT, string[] result, string trackid, string timestamp)
        {
            string _ret = "";
            try
            {
                if (result == null) return "";

                DynamicData ddTx = new DynamicData();
                ddTx.AddValue("Consultationtype", CT);
                ddTx.AddValue("Connectorid", "RENIEC");
                ddTx.AddValue("Trackid", trackid);
                ddTx.AddValue("Status", result[0]);
                ddTx.AddValue("Result", result[0].Equals("70006") ? "1" : "2");
                ddTx.AddValue("Score", "0");
                ddTx.AddValue("Threshold", "0");
                ddTx.AddValue("Timestamp", timestamp);
                _ret = DynamicData.SerializeToXml(ddTx);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Connector.RENIEC.GetDDFormExternalTx Error", ex);
                _ret = "";
            }
            return _ret;
        }

        /// <summary>
        /// Dado un arreglo de byte[] copn una imagen que viene del SRCeI, 
        /// Lo transforma a 
        /// </summary>
        /// <param name="p"></param>
        /// <param name="nullable"></param>
        /// <returns></returns>
        //private string GeneraImagenB64FromBytes(int imageType, byte[] byImage, FormatoType? formato)
        //{
        //    string _strret = null;
        //    try
        //    {
        //        //FIBITMAP dib = new FIBITMAP();
        //        //FreeImage.UnloadEx(ref dib);
        //        //IntPtr imgPtr = Marshal.AllocHGlobal(byImage.Length);
        //        //Marshal.Copy(byImage, 0, imgPtr, byImage.Length);
        //        //FIMEMORY fiMStream = FreeImage.OpenMemory(imgPtr, (uint)byImage.Length);
        //        //// get the file type
        //        //FREE_IMAGE_FORMAT fiFormat = FreeImage.GetFileTypeFromMemory(fiMStream, 0);
        //        //// load an image from the memory stream
        //        //// mFileHandle is a static uint property of the class
        //        //dib = FreeImage.LoadFromMemory(fiFormat, fiMStream, 0);
        //        ////Bitmap bFoto = FreeImage.GetBitmap(dib);
        //        //FIBITMAP dib2;
        //        //if (imageType == 1) //es Foto
        //        //{
        //        //    if (_wFoto > 0) dib2 = FreeImage.Rescale(dib, _wFoto, _hFoto, FREE_IMAGE_FILTER.FILTER_BICUBIC);
        //        //    else dib2 = dib;
        //        //}
        //        //else
        //        //{ //Es Firma
        //        //    if (_wFirma > 0) dib2 = FreeImage.Rescale(dib, _wFirma, _hFirma, FREE_IMAGE_FILTER.FILTER_BICUBIC);
        //        //    else dib2 = dib;
        //        //}
        //        //byte[] buff = new byte[FreeImage.GetWidth(dib2) * FreeImage.GetHeight(dib2)];
        //        //MemoryStream ms = new MemoryStream(buff);
        //        //FreeImage.SaveToStream(dib2, ms, FREE_IMAGE_FORMAT.FIF_JPEG);
        //        //_strret = Convert.ToBase64String(buff);
        //        //ms.Close();
        //        //Marshal.FreeHGlobal(imgPtr);

        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error", ex);
        //        _strret = null;
        //    }
        //    return _strret;
        //}

#endregion Implementation of IConnector

        private static string FormatFechaHoraSRCeI(string fechaoperacion)
        {
            string strRet = "";
            try
            {
                strRet = fechaoperacion.Substring(6, 2) + "/" +
                         fechaoperacion.Substring(4, 2) + "/" +
                         fechaoperacion.Substring(0, 4) + " " +
                         fechaoperacion.Substring(8, 2) + ":" +
                         fechaoperacion.Substring(10, 2) + ":" +
                         fechaoperacion.Substring(12, 2);
            }
            catch
            {
                strRet = "";
            }
            return strRet;
        }

        public void Dispose()
        {
            
        }
    }


    //Codigos de error que pueden llegar
    /*
     SOAP Response
                    (Código) Descripción Tipo Ancho Ejemplo
                    1 100 Objeto enviado para la consulta 
                    biométrica es nulo. String
                    resp[0]:100
                    resp[1]: Se ha
                    encontrado datos
                    nulos en el objeto
                    enviado para la
                    consulta
                    biométrica
                    2 101
                    Se ha encontrado datos nulos en el
                    objeto enviado para la consulta
                    biométrica.
                    String
                    resp[0]:101
                    resp[1]: Se ha
                    encontrado datos
                    no válidos en el
                    objeto enviado
                    para la consulta
                    biométrica.
                    3 102
                    Se ha encontrado datos no válidos
                    en el objeto enviado para la consulta
                    biométrica.
                    String
                    4 109
                    Se generó una excepción en el
                    proceso de comparación biométrica
                    de las plantillas biométricas.
                    String
                    5 110
                    Error en las imágenes dactilares
                    WSQ enviados para la consulta
                    biométrica.
                    String
                    6 111
                    No se logró obtener las plantillas
                    biométricas del ciudadano desde las
                    imágenes WSQ
                    String
                    7 112
                    Las imágenes dactilares WSQ
                    enviados para la consulta biométrica
                    son de baja calidad.
                    String
                    8 120 Error en la imagen dactilar WSQ
                    enviados para la consulta biométrica. String
                    9 121
                    No se logró obtener la plantilla
                    biométrica del ciudadano desde la
                    imagen WSQ.
                    String
                    1
                    0
                    122
                    La imagen dactilar WSQ enviado
                    para la consulta biométrica es de
                    baja calidad.
                    String
                    1
                    1
                    200
                    No se cargó la librería DLL para el
                    servicio de consulta biométrica en el
                    MOO.
                    String
                    1
                    2
                    202 No se encontró registros del número
                    de DNI en el AFIS. String
                    1
                    3
                    203 No se encontró plantillas biométricas
                    del número de DNI en el AFIS. String
                    1
                    4
                    206 Formato de plantilla biométrica
                    declarada en el objeto no es válido. String
                    1
                    5
                    207
                    Se ha encontrado más de un registro
                    de plantillas biométricas activas para
                    el número de DNI en el AFIS.
                    String
                    1
                    6
                    208 Identificador en el AFIS para el
                    número de DNI es nulo o vacío.
                    String
                    1
                    7
                    209
                    El registro correspondiente al
                    número de DNI en el AFIS no
                    contiene las plantillas biométricas, se
                    encontró valores nulos.
                    String
                    1
                    8
                    210
                    El registro correspondiente al
                    número de DNI en el AFIS no
                    contiene una plantilla biométrica, se
                    encontró una de las plantillas con
                    valor nulo.
                    String
                    1
                    9
                    212
                    No se ha obtenido ninguna imagen
                    dactilar WSQ desde el cliente para la
                    consulta biométrica.
                    String
                    2
                    0
                    213
                    Se generó una excepción a nivel de
                    la comunicación con el componente
                    MOO.
                    Strin
                         * 
 
                     SOAP Response
                    (Código) Descripción Tipo Ancho Ejemplo
                    1 1 El usuario está autorizado String Variable
                    resp[0]:1
                    resp[1]:”El usuario
                    está autorizado”
                    2 2 El usuario no existe String Variable
                    resp[0]:2
                    resp[1]:”El usuario
                    no existe”
                    3 3 Entidad no se encuentra registrada
                    y/o no tiene convenios con RENIEC String Variable
                    resp[0]:3
                    resp[1]:”Entidad
                    no se encuentra
                    registrada y/o no
                    tiene convenios
                    con RENIEC”
                    4 4 Entidad no cuenta con este servicio String Variable
                    resp[0]:4
                    resp[1]:”Entidad
                    no cuenta con
                    este servicio”
                    5 5 El convenio de la entidad con
                    RENIEC se encuentra deshabilitado String Variable
                    resp[0]:5
                    resp[1]:” El
                    convenio de la
                    entidad con
                    RENIEC se
                    encuentra
                    deshabilitado”
                    6 6
                    El servicio se encuentra
                    deshabilitado para esta entidad String Variable
                    resp[0]:6
                    resp[1]:”El servicio
                    se encuentra
                    deshabilitado para
                    esta entidad”
                    7 7 El servicio no está vigente para esta
                    entidad String Variable
                    resp[0]:7
                    resp[1]:”El servicio
                    no está vigente
                    para esta entidad”
                    8 8
                    El usuario se encuentra
                    deshabilitado para este servicio de la
                    entidad
                    String Variable
                    resp[0]:8
                    resp[1]:”El usuario
                    se encuentra
                    deshabilitado para
                    este servicio de la
                    entidad”
                    9 9 El usuario se encuentra cancelado
                    en el convenio String Variable
                    resp[0]:9
                    resp[1]:”El usuario
                    se encuentra
                    cancelado en el
                    convenio”
                    1
                    0
                    10 El usuario está eliminado String Variable
                    resp[0]:10
                    resp[1]:”El usuario
                    está eliminado”
                    1
                    1
                    11 El usuario no se encuentra
                    registrado en el convenio
                    String Variable
                    resp[0]:11
                    resp[1]:”El usuario
                    no se encuentra
                    registrado en el
                    convenio”
                    1
                    2
                    12 RUC de la entidad se encuentra
                    inactivo por deuda String Variable
                    resp[0]:12
                    resp[1]:”RUC de la
                    entidad se
                    encuentra inactivo
                    por deuda”
                    1
                    3
                    13 RUC de la entidad está cancelado String Variable
                    resp[0]:13
                    resp[1]:”RUC de la
                    entidad está
                    cancelada”
                    1
                    4
                    14 Usuario cancelado en RENIEC String Variable
                    resp[0]:14
                    resp[1]:”Usuario
                    cancelado en
                    RENIEC”
     */
}
