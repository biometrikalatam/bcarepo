using System;
using System.Net;
using System.Net.NetworkInformation;
using log4net;

namespace Bio.Core.Matcher.Connector.RENIEC
{
	/// <summary>
	/// Summary description for Utiles.
	/// </summary>
    public class Utiles
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(Utiles));

        internal static string GetIP()
        {
            String sIP = "";
            bool isFirst = true;
            try
            {
                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
                foreach (IPAddress ipAddress in localIPs)
                {
                    if (ipAddress.ToString().Length <= 15)
                    {
                        sIP = ipAddress.ToString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Connector.RENIEC.GetIP Error - " + ex.Message);
            }
            return sIP;
        }


        public static string GetMACAddress()
        {
            String sMacAddress = "";
            bool isFirst = true;
            try
            {
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                int index = 0;
                for (int i = 0; i < nics.Length; i++)
                {
                    if (nics[i].NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                    {
                        index = i;
                        break;
                    }
                }
                sMacAddress = FormatMAC(nics[index].GetPhysicalAddress().ToString());

            }
            catch (Exception ex)
            {
                //LOG.Error("Utils.GetMACAddress Error - " + ex.Message);
            }
            return sMacAddress;
        }

        private static string FormatMAC(string smac)
        {
            String sMacAddress = "";
            try
            {
                bool first = true;
                if (!String.IsNullOrEmpty(smac))
                {
                    int index = 0;
                    while (index < smac.Length)
                    {
                        if (first)
                        {
                            sMacAddress = smac.Substring(index, 2);
                            first = false;
                        }
                        else
                        {
                            sMacAddress = sMacAddress + "-" + smac.Substring(index, 2); 
                        }
                        index += 2;
                    }
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("Utils.FormatMAC Error - " + ex.Message);
                sMacAddress = "";
            }
            return sMacAddress;
        }

        public static DateTime ParseFecha(string fechain)
        {

            if (String.IsNullOrEmpty(fechain)) {
                return (new DateTime(2000,1,1));
            }

            DateTime ret = new DateTime(2000,1,1);
            bool _sigue = true;
            try
            {
                try
                {
                    ret = DateTime.ParseExact(fechain, "dd-MM-yyyy", null);
                    _sigue = false;
                }
                catch (Exception )
                {
                    _sigue = true;
                }

                if (_sigue)
                {
                    try
                    {
                        ret = DateTime.ParseExact(fechain, "dd/MM/yyyy", null);
                        _sigue = false;
                    }
                    catch (Exception)
                    {
                        _sigue = true;
                    }
                }

                if (_sigue)
                {
                    try
                    {
                        ret = DateTime.ParseExact(fechain, "MM/dd/yyyy", null);
                        _sigue = false;
                    }
                    catch (Exception)
                    {
                        _sigue = true;
                    }
                }

                if (_sigue)
                {
                    try
                    {
                        ret = DateTime.ParseExact(fechain, "MM-dd-yyyy", null);
                        _sigue = false;
                    }
                    catch (Exception)
                    {
                        _sigue = true;
                    }
                }

            }
            catch (Exception ex)
            {
                ret = new DateTime(2000, 1, 1);
            }
            return ret;

        }
    }
}
