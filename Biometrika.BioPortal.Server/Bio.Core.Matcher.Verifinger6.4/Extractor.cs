﻿using System;
using System.Drawing;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using Bio.Core.Wsq.Decoder;
using log4net;
using Neurotec.Biometrics;
using Neurotec.Images;
//using Neurotec.Images;

namespace Bio.Core.Matcher.Verifinger6_4
{
    public class Extractor : IExtractor
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Extractor));

        


#region Implementation of IExtractor

        private int _authenticationFactor;

        private int _minutiaeType;

        private double _threshold;

        private string _parameters;

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        /// <summary>
        /// Tecnologia a utilizar para extraccion
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo d eminucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de extracción considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Extrae desde el template ingresado en el parametro inicial, de acuerdo a 
        /// los adtos de tecnologia y minucias seteados, el template resultante. 
        /// Estos parámetros están serializados en xml, por lo que primero se deserializa.
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="xmloutput">template generado serializado en xml</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out string xmloutput)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero entrega un ITemplate
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out ITemplate templateout)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero ingresa un ITemplate y entrega un ITemplate
        /// </summary>
        /// <param name="templatebase">Template base para extraccion</param>
        /// <param name="destination">Determina si es template para 1-Verify | 2-Enroll </param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(ITemplate templatebase, int destination, out ITemplate templateout)
        {
            int res = Errors.IERR_OK;
            templateout = null;

            try
            {

                if (!VfeUtils.IsLicenseConfigured)
                {
                    if (!VfeUtils.ConfigureLicense(_parameters)) return Errors.IERR_LICENSE;
                }

                if (templatebase == null) return Errors.IERR_NULL_TEMPLATE;
                if (templatebase.Data == null) return Errors.IERR_NULL_TEMPLATE;

                if (templatebase.AuthenticationFactor !=
                    Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT ||
                    (templatebase.MinutiaeType !=
                    Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER && 
                    templatebase.MinutiaeType !=
                    Constant.MinutiaeType.MINUTIAETYPE_WSQ &&
                    templatebase.MinutiaeType !=
                    Constant.MinutiaeType.MINUTIAETYPE_RAW))
                {
                    return Errors.IERR_INVALID_TEMPLATE;
                }

                //Si lo anterior esta ok, y la minucia es Verifinger solo asigno porque 
                if (templatebase.MinutiaeType == Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER)
                {
                    LOG.Error("Verifinger6_4.Extractor.Extract - In Minutiae VF...");
                    templateout = templatebase;
                }
                else //es Constant.MinutiaeType.MINUTIAETYPE_WSQ
                {
                    byte[] raw;
                    bool rawOK = false;
                    if (templatebase.MinutiaeType == Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                    {
                        LOG.Error("Verifinger6_4.Extractor.Extract - In WSQ...");
                        //1.- Descomprimo WSQ
                        WsqDecoder decoder = new WsqDecoder();
                        short w, h;
                        if (!decoder.DecodeMemory(templatebase.Data, out w, out h, out raw))
                        {
                            res = Errors.IERR_WSQ_DECOMPRESSING;
                            LOG.Error("Verifinger6_4.Extractor.Extract Error [decoder.DecodeMemory]");
                        } else
                        {
                            rawOK = true;
                        }
                    } else
                    {
                        LOG.Error("Verifinger6_4.Extractor.Extract - In RAW...");
                        raw = templatebase.Data;
                        rawOK = true;
                    }
                    
                    if (rawOK)
                    {
                        LOG.Error("Verifinger6_4.Extractor.Extract - Crea NFExtractor...");
                        //2.- Extraigo  
                        NFExtractor _extractor = new NFExtractor();
                                                     //{
                                                     //    QualityThreshold = (byte) ((2*_threshold*255 + 100)/(2*100))
                                                     //};

                        LOG.Error("Verifinger6_4.Extractor.Extract - Crea Bitmap...");
                        //Creo Bitmap desde RAW
                        Bitmap imgBMP = Imaging.ImageProcessor.RawToBitmap(raw,512,512);

                        //imgBMP.Save(@"D:\temp\a\wsq.bmp");

                        LOG.Error("Verifinger6_4.Extractor.Extract - Crea NImage...");
                            //Creo NImgae (VF) desde el BMP
                        NImage imageRAWtoBMP = NImage.FromBitmap(imgBMP);
                        LOG.Error("Verifinger6_4.Extractor.Extract - Crea NGrayscaleImage...");
                        NImage NGImage = (NImage)NImage.FromImage(NPixelFormat.Grayscale8U,
                                                                                     0, 500, 500, imageRAWtoBMP);
                        
                        NfeExtractionStatus extractionStatus = new NfeExtractionStatus();
                        LOG.Error("Verifinger6_4.Extractor.Extract - Crea nfeRecord (Extract Func)...");
                        NFRecord nfeRecord = _extractor.Extract(NGImage, NFPosition.Unknown,
                                                                NFImpressionType.NonliveScanPlain,
                                                                out extractionStatus);
                        
                        
                        //System.IO.StreamWriter sr = new StreamWriter(@"D:\temp\a\vf_1.txt");
                        //sr.Write(Convert.ToBase64String(templateout.Data));
                        //sr.Close();

                        //FileStream fs = new FileStream(@"D:\temp\a\Token\vf1.bin", FileMode.Create);
                        //fs.Write(templateout.Data, 0, templateout.Data.Length);
                        //fs.Close();
                        //if (templateout.Data == null)
                        if (extractionStatus != NfeExtractionStatus.TemplateCreated)
                        {
                            res = Errors.IERR_EXTRACTING;
                            templateout = null;
                            LOG.Error("Verifinger6_4.Extractor.Extract Error [extractionStatus=" + extractionStatus.ToString() + "]");
                        } else
                        {
                            templateout = new Template
                                              {
                                                  Data = nfeRecord.Save()
                                               };
                            templateout.AdditionalData = "SizeData=" + templateout.Data.Length.ToString() +
                                                    "|Quality=" + nfeRecord.Quality +
                                                    "|G=" + nfeRecord.G;
                            templateout.AuthenticationFactor = templatebase.AuthenticationFactor;
                            templateout.BodyPart = templatebase.BodyPart;
                            templateout.MinutiaeType = Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER;
                            templateout.Type = Constant.BirType.PROCESSED_DATA;
                            imageRAWtoBMP.Dispose();
                            NGImage.Dispose();
                        }
                    } 
                }
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("Bio.Core.Matcher.Verifinger6_4.Extract(T, T) Error", ex);
            }
            return res;
        }

#endregion Implementation of IExtractor

        public void Dispose()
        {
            if (VfeUtils.IsLicenseConfigured)
            {
                VfeUtils.ReleaseLicenses();
            }
        }
    }
}
