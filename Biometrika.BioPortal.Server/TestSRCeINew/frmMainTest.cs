﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Bio.Core.Wsq.Encoder;
using DPUruNet;

namespace TestSRCeINew
{
    public partial class Form1 : Form
    {

        private ReaderCollection _readers;
        private Reader _reader;
        private Bitmap _rawCaptured;
        private List<Fmd> _preEnrollmentFmd;
        private DataResult<Fmd> _enrollmentFmd;
        private bool _mustStop = false;
        private bool _sensorConnected;
        private bool _isOpened = false;
        private bool _isEventAdded = false;
        private int _device = 1;
        private string _model = "4500";
        private int _timeout = 3000;
        private int _tokencontent = 0;       //[0-Ambas | 1-WSQ Solo | 2-AFIS Solo]
        private string _wsq;               //WSQ
        private string _template;               //Template
        private string _templateISO;               //Template
        private string _templateANSI;               //Template
        private string _templateANSI2;               //Template

        private string _raw512;               //WSQ
        private string _raw512_2;               //WSQ

        private string _innovatricsTemplateISO;               //Template
        private string _innovatricsTemplateANSI;               //Template
        
        private string _token;               //Data a transferir encriptada
        private string _jpgHuella;

        public Form1()
        {
            InitializeComponent();
        }

        public static string GetMACAddress()
        {
            String sMacAddress = "";
            bool isFirst = true;
            try
            {
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                int index = 0;
                for (int i = 0; i < nics.Length; i++)
                {
                    if (nics[i].NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                    {
                        index = i;
                        break;
                    }
                }
                sMacAddress = FormatMAC(nics[index].GetPhysicalAddress().ToString());

            }
            catch (Exception ex)
            {
                //LOG.Error("Utils.GetMACAddress Error - " + ex.Message);
            }
            return sMacAddress;
        }

        private static string FormatMAC(string smac)
        {
            String sMacAddress = "";
            try
            {
                bool first = true;
                if (!String.IsNullOrEmpty(smac))
                {
                    int index = 0;
                    while (index < smac.Length)
                    {
                        if (first)
                        {
                            sMacAddress = smac.Substring(index, 2);
                            first = false;
                        }
                        else
                        {
                            sMacAddress = sMacAddress + "-" + smac.Substring(index, 2);
                        }
                        index += 2;
                    }
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("Utils.FormatMAC Error - " + ex.Message);
                sMacAddress = "";
            }
            return sMacAddress;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string s = GetMACAddress();
            CheckSensorConnected();
            try 
	        {	        
                Innovatrics.AnsiIso.IEngine.Init();
		
	        }
	        catch (Exception exep)
	        {
                System.Console.Write(exep.Message);
            }
        }


 #region DP

        private void CheckSensorConnected()
        {
            try
            {
                _readers = null;
                _readers = ReaderCollection.GetReaders();
                Reader _currentReader = null;
                if (_readers.Count == 0)
                {
                    _sensorConnected = false;
                }
                else
                {
                    bool isNewReaderSelected = false;
                    for (int i = 0; i < _readers.Count; i++)
                    {
                        if (_readers[i].Description.Id.ProductName.IndexOf("U.are.U") > -1)
                        {
                            _currentReader = _readers[i];
                            if (_reader == null || !(_currentReader.Equals(_reader)))
                            {
                                isNewReaderSelected = true;
                                this.textBox1.Text = _currentReader.Description.SerialNumber;
                                _reader = _currentReader;
                            }
                        }
                    }
                    if (_currentReader == null)
                    {
                        _sensorConnected = false;
                    }
                    else
                    {
                        _sensorConnected = true;
                        //if (_isFirstCheckSensor || isNewReaderSelected)
                        //{
                        //    ClearBio();
                        //    _isFirstCheckSensor = false;
                        //    _reader = _currentReader;
                        //    _isOpened = false;
                        //    _isEventAdded = false;
                        //    _serialsensor = _reader.Description.SerialNumber;

                        //    _sensorConnected = true;
                        //    picLectorStatus.Image = Properties.Resources.dp_oK;
                        //    picWizardStatus.Image = Properties.Resources.muestra_desconoc;
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                //WriteLog("ERROR - CheckSensorConnected Error - " + ex.Message);
            }
            //timerCheckReaderStatus.Enabled = true;
        }

        private void StartGetSample()
        {
            try
            {
                Constants.ResultCode readerResult = Constants.ResultCode.DP_DEVICE_FAILURE;
                _preEnrollmentFmd = new List<Fmd>();

                if (!_isOpened)
                {
                    //readerResult = _reader.Open(Constants.CapturePriority.DP_PRIORITY_COOPERATIVE);
                    readerResult = _reader.Open(Constants.CapturePriority.DP_PRIORITY_EXCLUSIVE);
                    if (readerResult == Constants.ResultCode.DP_SUCCESS) _isOpened = true;
                }
                if (_isOpened)
                {
                    if (!_isEventAdded)
                    {
                        _reader.On_Captured += new Reader.CaptureCallback(reader_On_Captured);

                        _isEventAdded = true;
                    }
                    BeginCapture();
                }
                else
                {
                    WriteLog("WARN - StartGetSample Sensor NO Open [" + readerResult + "]");
                    //this.picWizardStatus.Image = Resources.muestra_sensorNoOk;
                }
            }
            catch (Exception ex)
            {
                WriteLog("ERROR - StartGetSample Error - " + ex.Message);
            }

        }

        private void BeginCapture()
        {
            try
            {
                Constants.ResultCode captureResult = _reader.CaptureAsync(Constants.Formats.Fid.ANSI,
                                                                          Constants.CaptureProcessing.
                                                                              DP_IMG_PROC_DEFAULT,
                                                                          _reader.Capabilities.Resolutions[0]);
                if (captureResult != Constants.ResultCode.DP_SUCCESS)
                {
                    WriteLog("WARN - CaptureAsync Error [" + captureResult + "]");
                }
                else
                {
                    //picWizardStatus.Image = Properties.Resources.muestra_enProceso;
                    //timerTimeouCapture.Interval = _timeout;
                    //timerTimeouCapture.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                WriteLog("ERROR - BeginCapture Error - " + ex.Message);
            }
        }

        void reader_On_Captured(CaptureResult capResult)
        {
            try
            {
                //WriteLog("DEBUG - reader_On_Captured IN...");
                if (capResult.Quality == Constants.CaptureQuality.DP_QUALITY_GOOD)
                {
                    //timerTimeouCapture.Enabled = false;

                    //richTextBox1.Text = capResult.Data.
                    

                    // Get view bytes to create bitmap.
                    foreach (Fid.Fiv fiv in capResult.Data.Views)
                    {
                        picSample.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(fiv.RawImage, fiv.Width, fiv.Height);
                        _jpgHuella = fiv.Width + "~" + fiv.Height + "~" + Convert.ToBase64String(fiv.RawImage);
                        //WriteLog("DEBUG - Image Mostrada!");
                        break;
                    }

                    byte[] raw512x512 = null;
                    foreach (Fid.Fiv fiv in capResult.Data.Views)
                    {
                        //Ask user to press finger to get fingerprint
                        raw512x512 = Bio.Core.Imaging.ImageProcessor.FillRaw(fiv.RawImage, fiv.Width, fiv.Height,
                                                                                    512, 512);

                        if (String.IsNullOrEmpty(_raw512))
                        {
                            _raw512 = Convert.ToBase64String(raw512x512);
                        }
                        else
                        {
                            _raw512_2 = Convert.ToBase64String(raw512x512);
                        }

                        Bio.Core.Wsq.Encoder.WsqEncoder encoder = new WsqEncoder();
                        byte[] wsqCaptured;
                        //if (encoder.EncodeMemory(fiv.RawImage, (short)fiv.Width, (short)fiv.Height, out wsqCaptured))
                        if (encoder.EncodeMemory(raw512x512, 512, 512, out wsqCaptured))
                        {
                            _wsq = Convert.ToBase64String(wsqCaptured);
                            //WriteLog("DEBUG - WSQ Generado [" + _wsq + "]");
                        }

                        byte[] InnovatricsTemplateANSI = new byte[Innovatrics.AnsiIso.IEngine.MAX_ANSI_TEMPLATE_SIZE];
                        Innovatrics.AnsiIso.Ansi.CreateTemplate(512, 512, raw512x512, InnovatricsTemplateANSI);
                        _innovatricsTemplateANSI = Convert.ToBase64String(InnovatricsTemplateANSI);

                        byte[] InnovatricsTemplateISO = new byte[Innovatrics.AnsiIso.IEngine.MAX_ISO_TEMPLATE_SIZE];
                        Innovatrics.AnsiIso.Iso.CreateTemplate(512, 512, raw512x512, InnovatricsTemplateISO);
                        _innovatricsTemplateISO = Convert.ToBase64String(InnovatricsTemplateISO);

                        

                        break;
                    }
                    DataResult<Fmd> resultConversion =
                            FeatureExtraction.CreateFmdFromFid(capResult.Data,
                                                                Constants.Formats.Fmd.DP_PRE_REGISTRATION);

                    if (resultConversion.ResultCode != Constants.ResultCode.DP_SUCCESS)
                    {
                        //WriteLog("ERROR - CreateFmdFromFid Error [" + resultConversion.ResultCode + "]");
                    }

                    for (int i = 0; i < 4; i++)
                    {
                        _preEnrollmentFmd.Add(resultConversion.Data);
                    }

                    //count++;
                    DataResult<Fmd> fmdANSI = FeatureExtraction.CreateFmdFromFid(capResult.Data, Constants.Formats.Fmd.ANSI);
                    DataResult<Fmd> fmdISO = FeatureExtraction.CreateFmdFromFid(capResult.Data, Constants.Formats.Fmd.ISO);
                    //foreach (Fid.Fiv fiv in capResult.Data.Views)
                    //{
                    //    DataResult<Fmd> fmdISOTest = FeatureExtraction.CreateFmdFromRaw(
                    //        fiv.RawImage, 1, 1, fiv.Width, fiv.Height, 500, Constants.Formats.Fmd.ISO);

                    //    break;
                    //}
                    //DataResult<Fmd> fmdISOTest1 = FeatureExtraction.CreateFmdFromRaw(
                    //    raw512x512, 1, 1, 512, 512, 500, Constants.Formats.Fmd.ISO);

                    DataResult<Fmd> resultEnrollmentV = null;
                    DataResult<Fmd> resultEnrollmentE = null;
                    resultEnrollmentV = FeatureExtraction.CreateFmdFromFid(capResult.Data, Constants.Formats.Fmd.DP_VERIFICATION);
                    //DPUruNet.Enrollment.CreateEnrollmentFmd(Constants.Formats.Fmd.DP_REGISTRATION, _preEnrollmentFmd);

                    resultEnrollmentE = DPUruNet.Enrollment.CreateEnrollmentFmd(
                                            Constants.Formats.Fmd.DP_REGISTRATION, _preEnrollmentFmd);

                    if (resultEnrollmentV.ResultCode == Constants.ResultCode.DP_SUCCESS &&
                        resultEnrollmentE.ResultCode == Constants.ResultCode.DP_SUCCESS &&
                        fmdANSI.ResultCode == Constants.ResultCode.DP_SUCCESS &&
                        fmdISO.ResultCode == Constants.ResultCode.DP_SUCCESS)
                    {
                        _template = Convert.ToBase64String(resultEnrollmentV.Data.Bytes) +
                                    "-septmpl-" +
                                    Convert.ToBase64String(resultEnrollmentE.Data.Bytes) +
                                    "-septmpl-" +
                                    Convert.ToBase64String(fmdANSI.Data.Bytes) +
                                    "-septmpl-" +
                                    Convert.ToBase64String(fmdISO.Data.Bytes);
                        if (String.IsNullOrEmpty(_templateANSI))
                        {
                            _templateANSI = Convert.ToBase64String(fmdANSI.Data.Bytes);
                        }
                        else
                        {
                            _templateANSI2 = Convert.ToBase64String(fmdANSI.Data.Bytes);
                        }
                        _templateISO = Convert.ToBase64String(fmdISO.Data.Bytes);
                        //WriteLog("DEBUG - TEMPLATES Generados [" + _template + "]");
                        _preEnrollmentFmd.Clear();
                    }
                    else if (resultEnrollmentV.ResultCode == Constants.ResultCode.DP_ENROLLMENT_INVALID_SET ||
                                resultEnrollmentE.ResultCode == Constants.ResultCode.DP_ENROLLMENT_INVALID_SET)
                    {
                        _preEnrollmentFmd.Clear();
                    }

                    //}

                    short errBpToken = 0;
                    //_tstamp = DateTime.Now.ToString("ddMMyyyyHHmmss");

                    //_token = BPTOKEN.GeneraToken(_typeid, _valueid, _serialsensor, (short)_finger, _wsq, _template, _tstamp, _ip,
                    //                    (short)_tokencontent, (short)_minutiaetype, (short)_device, (short)_authenticationFactor,
                    //                    (short)_operationtype, ref errBpToken);
                    //if (errBpToken == 0)
                    //{
                    //    picWizardStatus.Image = Properties.Resources.muestra_ok;
                    //    this.labFeedbackBotton.Text = "Token del dedo " +
                    //                                    UTILS.GetNombreDedo(_finger) + " generado con éxito!";
                    //    WriteLog("DEBUG - TOKEN Generado [" + _token + "]");
                    //}
                    //else
                    //{
                    //    _template = null;
                    //    _wsq = null;
                    //    _token = null;
                    //    picWizardStatus.Image = Properties.Resources.muestra_sensorNoOk;
                    //    this.labFeedbackBotton.Text = "Error en la generación del Token. Reintente...";
                    //    WriteLog("ERROR - Generando TOKEN [ErrNum=" + errBpToken.ToString() + "]");
                    //}
                }
                else
                {
                    //WriteLog("DEBUG - reader_On_Captured Mala Calidad Imagen!");
                }
            }
            catch (Exception ex)
            {
                //WriteLog("ERROR - BeginCapture Error - " + ex.Message);
                MessageBox.Show("ERROR - BeginCapture Error - " + ex.Message);
            }
            
            _mustStop = true;

        }

        public void StopCapture()
        {
            try
            {
                //_reader.Reset();
                _reader.CancelCapture();
            }
            catch (Exception ex)
            {
                WriteLog("ERROR - StopCapture Error - " + ex.Message);
            }
        }

        public void CancelCaptureAndCloseReader()
        {
            try
            {
                if (_reader != null)
                {
                    _reader.CancelCapture();
                    _reader.Dispose();
                }
            }
            catch (Exception ex)
            {
                WriteLog("ERROR - CancelCaptureAndCloseReader Error - " + ex.Message);
            }
        }

        public void CheckReaderStatus()
        {
            try
            {
                if (_reader.Status.Status == Constants.ReaderStatuses.DP_STATUS_BUSY)
                {
                    Thread.Sleep(50);
                }
                else if ((_reader.Status.Status == Constants.ReaderStatuses.DP_STATUS_NEED_CALIBRATION))
                {
                    _reader.Calibrate();
                }
                else if ((_reader.Status.Status != Constants.ReaderStatuses.DP_STATUS_READY))
                {
                    //MessageBox.Show("Reader Status - " + _reader.Status.Status);
                }
            }
            catch (Exception ex)
            {
                WriteLog("ERROR - CheckReaderStatus Error - " + ex.Message);
            }
        }

        private void WriteLog(string msg)
        {
            try
            {
                this.richTextBox1.Text = this.richTextBox1.Text  + Environment.NewLine + msg;
            }
            catch (Exception ex)
            {
                this.richTextBox1.Text = ex.StackTrace;
            }
        }

#endregion DP

        private void button1_Click(object sender, EventArgs e)
        {
            StartGetSample();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopCapture();
            CancelCaptureAndCloseReader();
            try
            {
                Innovatrics.AnsiIso.IEngine.Terminate();

            }
            catch (Exception exep)
            {
                System.Console.Write(exep.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + Environment.NewLine +
                "WSQ=" + _wsq.Substring(0, 50) + "..." + Environment.NewLine +
                "ANSI=" + _templateANSI.Substring(0, 50) + "..." + Environment.NewLine +
                "ISO=" + _templateISO.Substring(0, 50) + "..." + Environment.NewLine +
                "INN ANSI=" + _innovatricsTemplateANSI.Substring(0, 50) + "..." + Environment.NewLine +
                "INN ISO=" + _innovatricsTemplateISO.Substring(0, 50) + "..." + Environment.NewLine +
                "ANSI LEN=" + _templateANSI.Length + "..." + Environment.NewLine +
                "INN ANSI LEN=" + _innovatricsTemplateANSI.Length + "..." + Environment.NewLine +
                "ISO LEN=" + _templateISO.Length + "..." + Environment.NewLine +
                "INN ISO LEN=" + _innovatricsTemplateISO.Length; 
                

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Fmd firstFinger;
            Fmd secondFinger;
            try
            {
                
                //DataResult<Fmd> resultConversion = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.ANSI);
                //firstFinger = resultConversion.Data;
                //secondFinger = resultConversion.Data;
                //CompareResult compareResult = Comparison.Compare(firstFinger, 0, secondFinger, 0);

                byte[] finger1 = Convert.FromBase64String(_innovatricsTemplateANSI);
                byte[] finger2 = Convert.FromBase64String(_templateANSI);
                int score = 0;
                Innovatrics.AnsiIso.Ansi.VerifyMatch(finger1, finger2, 40, out score);
                richTextBox1.Text = "Resultado= " + score;

                try
                {
                    //currentTemplate.Deserialize(byCurrentTemplate);
                    //currentTemplate = Fmd.DeserializeXml((string) paramin.GetValue("currentTemplate"));
                    Fmd temp1 = Fmd.DeserializeXml(GeneraXML(_innovatricsTemplateANSI, 3));
                    Fmd temp2 = Fmd.DeserializeXml(GeneraXML(_templateANSI, 3));
                    CompareResult compareResult = Comparison.Compare(temp1, 0, temp2, 0);
                    richTextBox1.Text = "Resultado= " + compareResult.ResultCode + " - Score=" + compareResult.Score;
                }
                catch (Exception ex)
                {
                    richTextBox1.Text = ex.Message;
                }


            }
            catch (Exception ex)
            {
                richTextBox1.Text = ex.Message;
            }
        }

        internal static string GeneraXML(string b64Template, int mt)
        {
            string xml = null;
            switch (mt)
            {
                case 1: //DP Enroll
                    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                          "<Fid>" +
                          "<Bytes>" + b64Template + "</Bytes>" +
                          "<Format>1</Format>" +
                          "<Version>1.0.0</Version>" +
                          "</Fid>";
                    break;
                case 2: //DP Verify
                    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                          "<Fid>" +
                          "<Bytes>" + b64Template + "</Bytes>" +
                          "<Format>2</Format>" +
                          "<Version>1.0.0</Version>" +
                          "</Fid>";
                    break;
                case 3: //ANSI
                    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                          "<Fid>" +
                          "<Bytes>" + b64Template + "</Bytes>" +
                          "<Format>1769473</Format>" +
                          "<Version>1.0.0</Version>" +
                          "</Fid>";
                    break;
                case 4: //ISO
                    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                          "<Fid>" +
                          "<Bytes>" + b64Template + "</Bytes>" +
                          "<Format>16842753</Format>" +
                          "<Version>1.0.0</Version>" +
                          "</Fid>";
                    break;

                default:
                    break;
            }
            return xml;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                //Fiv img1 = new Fid();
                
            }
            catch (Exception ex)
            {
               richTextBox1.Text = ex.Message;
            }
        }

        byte[] inFromRaw;
        byte[] inFromBmp;
        private void button6_Click(object sender, EventArgs e)
        {
            byte[] by512x512 = new byte[512*512];
            using (System.IO.Stream file = System.IO.File.OpenRead(@"c:\tmp\A\Sec\IndiceDerecho\dedo512x512.raw"))
            {
                file.Read(by512x512, 0, by512x512.Length);
            }
            inFromRaw = new byte[Innovatrics.AnsiIso.IEngine.MAX_ISO_TEMPLATE_SIZE];
            Innovatrics.AnsiIso.Iso.CreateTemplate(512, 512, by512x512, inFromRaw);
            _innovatricsTemplateISO = Convert.ToBase64String(inFromRaw);
            this.richTextBox1.Text = _innovatricsTemplateISO;
            this.picSample.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(by512x512, 512, 512);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Bitmap bmp = new Bitmap(Image.FromFile(@"c:\tmp\A\Sec\IndiceDerecho\dedo.bmp"));
            byte[] raw = Bio.Core.Imaging.ImageProcessor.BitmapToRaw(bmp);
            inFromBmp = new byte[Innovatrics.AnsiIso.IEngine.MAX_ISO_TEMPLATE_SIZE];
            Innovatrics.AnsiIso.Iso.CreateTemplate(258, 336, raw, inFromBmp);
            _innovatricsTemplateISO = Convert.ToBase64String(inFromBmp);
            this.richTextBox1.Text = _innovatricsTemplateISO;
            this.picSample.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw, 258, 336);

            int score = 0;
            Innovatrics.AnsiIso.Ansi.VerifyMatch(inFromRaw, inFromBmp, 180, out score);
            richTextBox1.Text = richTextBox1.Text + Environment.NewLine + "Resultado= " + score;

        }

        private void button8_Click(object sender, EventArgs e)
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(@"C:\tmp\GrabaChino\ImagenmpRecortada.txt");
            string s = sr.ReadToEnd();
            sr.Close();

            byte[] by = Convert.FromBase64String(s);
            byte[] by512 = Bio.Core.Imaging.ImageProcessor.FillRaw(by, 256, 360, 512, 512);

            pictureBox1.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(by512, 512, 512);
        }
    }
}
