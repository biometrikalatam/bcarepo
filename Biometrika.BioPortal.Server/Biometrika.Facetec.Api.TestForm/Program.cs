﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Facetec.Api.TestForm
{
    static class Program
    {

        internal static FacetecClient FACETEC_CLIENT;
        internal static FacetecClient9_4 FACETEC_CLIENT_9_4;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
