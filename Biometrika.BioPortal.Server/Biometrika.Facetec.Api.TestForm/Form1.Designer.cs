﻿namespace Biometrika.Facetec.Api.TestForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkIsAuth = new System.Windows.Forms.CheckBox();
            this.txtClasiffier = new System.Windows.Forms.TextBox();
            this.chkIsAuthentication = new System.Windows.Forms.PictureBox();
            this.rbGet2 = new System.Windows.Forms.RadioButton();
            this.rbGet1 = new System.Windows.Forms.RadioButton();
            this.rbGet = new System.Windows.Forms.RadioButton();
            this.button7 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTrackId3roGet = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.rtbResultImage = new System.Windows.Forms.RichTextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtUserReference = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTrackId3roAuth = new System.Windows.Forms.TextBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTrackId3roForAuth = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTrackId3ro = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnCompare3d3d = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsAuthentication)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkIsAuth);
            this.groupBox3.Controls.Add(this.txtClasiffier);
            this.groupBox3.Controls.Add(this.chkIsAuthentication);
            this.groupBox3.Controls.Add(this.rbGet2);
            this.groupBox3.Controls.Add(this.rbGet1);
            this.groupBox3.Controls.Add(this.rbGet);
            this.groupBox3.Controls.Add(this.button7);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtTrackId3roGet);
            this.groupBox3.Controls.Add(this.button8);
            this.groupBox3.Controls.Add(this.rtbResultImage);
            this.groupBox3.Controls.Add(this.button9);
            this.groupBox3.Location = new System.Drawing.Point(12, 520);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1322, 220);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Get";
            // 
            // chkIsAuth
            // 
            this.chkIsAuth.AutoSize = true;
            this.chkIsAuth.Location = new System.Drawing.Point(832, 23);
            this.chkIsAuth.Name = "chkIsAuth";
            this.chkIsAuth.Size = new System.Drawing.Size(104, 17);
            this.chkIsAuth.TabIndex = 14;
            this.chkIsAuth.Text = "is Authentication";
            this.chkIsAuth.UseVisualStyleBackColor = true;
            // 
            // txtClasiffier
            // 
            this.txtClasiffier.Location = new System.Drawing.Point(451, 48);
            this.txtClasiffier.Name = "txtClasiffier";
            this.txtClasiffier.Size = new System.Drawing.Size(80, 20);
            this.txtClasiffier.TabIndex = 13;
            this.txtClasiffier.Text = "front";
            // 
            // chkIsAuthentication
            // 
            this.chkIsAuthentication.Location = new System.Drawing.Point(1196, 70);
            this.chkIsAuthentication.Name = "chkIsAuthentication";
            this.chkIsAuthentication.Size = new System.Drawing.Size(113, 101);
            this.chkIsAuthentication.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.chkIsAuthentication.TabIndex = 12;
            this.chkIsAuthentication.TabStop = false;
            // 
            // rbGet2
            // 
            this.rbGet2.AutoSize = true;
            this.rbGet2.Location = new System.Drawing.Point(391, 48);
            this.rbGet2.Name = "rbGet2";
            this.rbGet2.Size = new System.Drawing.Size(54, 17);
            this.rbGet2.TabIndex = 11;
            this.rbGet2.Text = "Image";
            this.rbGet2.UseVisualStyleBackColor = true;
            // 
            // rbGet1
            // 
            this.rbGet1.AutoSize = true;
            this.rbGet1.Location = new System.Drawing.Point(337, 48);
            this.rbGet1.Name = "rbGet1";
            this.rbGet1.Size = new System.Drawing.Size(48, 17);
            this.rbGet1.TabIndex = 10;
            this.rbGet1.Text = "Data";
            this.rbGet1.UseVisualStyleBackColor = true;
            // 
            // rbGet
            // 
            this.rbGet.AutoSize = true;
            this.rbGet.Checked = true;
            this.rbGet.Location = new System.Drawing.Point(276, 48);
            this.rbGet.Name = "rbGet";
            this.rbGet.Size = new System.Drawing.Size(55, 17);
            this.rbGet.TabIndex = 9;
            this.rbGet.TabStop = true;
            this.rbGet.Text = "Status";
            this.rbGet.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(1224, 177);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(68, 23);
            this.button7.TabIndex = 8;
            this.button7.Text = "Abrir";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Datos...";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(195, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Transaction Id";
            // 
            // txtTrackId3roGet
            // 
            this.txtTrackId3roGet.Location = new System.Drawing.Point(276, 21);
            this.txtTrackId3roGet.Name = "txtTrackId3roGet";
            this.txtTrackId3roGet.Size = new System.Drawing.Size(550, 20);
            this.txtTrackId3roGet.TabIndex = 5;
            this.txtTrackId3roGet.Text = "cc709a20-02c3-4f7b-ac41-680c5c73b98e";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(1224, 0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(68, 23);
            this.button8.TabIndex = 4;
            this.button8.Text = "Clear";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // rtbResultImage
            // 
            this.rtbResultImage.Location = new System.Drawing.Point(15, 70);
            this.rtbResultImage.Name = "rtbResultImage";
            this.rtbResultImage.Size = new System.Drawing.Size(1161, 101);
            this.rtbResultImage.TabIndex = 3;
            this.rtbResultImage.Text = "";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(15, 19);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(68, 23);
            this.button9.TabIndex = 2;
            this.button9.Text = "Get";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtUserReference);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtTrackId3roAuth);
            this.groupBox2.Controls.Add(this.button10);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtTrackId3roForAuth);
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.richTextBox2);
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Location = new System.Drawing.Point(12, 263);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1322, 220);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Authentication";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(170, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(128, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Transaction User fro Auth";
            // 
            // txtUserReference
            // 
            this.txtUserReference.Location = new System.Drawing.Point(304, 47);
            this.txtUserReference.Name = "txtUserReference";
            this.txtUserReference.Size = new System.Drawing.Size(449, 20);
            this.txtUserReference.TabIndex = 12;
            this.txtUserReference.Text = "123456789";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(653, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Transaction Id Auth";
            // 
            // txtTrackId3roAuth
            // 
            this.txtTrackId3roAuth.Location = new System.Drawing.Point(759, 44);
            this.txtTrackId3roAuth.Name = "txtTrackId3roAuth";
            this.txtTrackId3roAuth.Size = new System.Drawing.Size(550, 20);
            this.txtTrackId3roAuth.TabIndex = 10;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(832, 18);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(99, 23);
            this.button10.TabIndex = 9;
            this.button10.Text = "Get from Up";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1224, 177);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(68, 23);
            this.button4.TabIndex = 8;
            this.button4.Text = "Abrir";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "URL para Onboarding...";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(166, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Transaction Id Enroll";
            // 
            // txtTrackId3roForAuth
            // 
            this.txtTrackId3roForAuth.Location = new System.Drawing.Point(276, 21);
            this.txtTrackId3roForAuth.Name = "txtTrackId3roForAuth";
            this.txtTrackId3roForAuth.Size = new System.Drawing.Size(550, 20);
            this.txtTrackId3roForAuth.TabIndex = 5;
            this.txtTrackId3roForAuth.Text = "b343834b-bdec-4029-966a-f2df57b952f2 ";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(1224, 0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(68, 23);
            this.button5.TabIndex = 4;
            this.button5.Text = "Clear";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(15, 70);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(1294, 101);
            this.richTextBox2.TabIndex = 3;
            this.richTextBox2.Text = "";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(15, 19);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(68, 23);
            this.button6.TabIndex = 2;
            this.button6.Text = "Init";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button11);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtTrackId3ro);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.richTextBox1);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(12, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1322, 220);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "OnBoarding";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1224, 177);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(68, 23);
            this.button3.TabIndex = 8;
            this.button3.Text = "Abrir";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "URL para Onboarding...";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(195, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Transaction Id";
            // 
            // txtTrackId3ro
            // 
            this.txtTrackId3ro.Location = new System.Drawing.Point(276, 21);
            this.txtTrackId3ro.Name = "txtTrackId3ro";
            this.txtTrackId3ro.Size = new System.Drawing.Size(550, 20);
            this.txtTrackId3ro.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1224, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(68, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(15, 70);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(1294, 101);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(68, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Init";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnCompare3d3d
            // 
            this.btnCompare3d3d.Location = new System.Drawing.Point(527, 238);
            this.btnCompare3d3d.Name = "btnCompare3d3d";
            this.btnCompare3d3d.Size = new System.Drawing.Size(132, 36);
            this.btnCompare3d3d.TabIndex = 14;
            this.btnCompare3d3d.Text = "Compare 3d-3d";
            this.btnCompare3d3d.UseVisualStyleBackColor = true;
            this.btnCompare3d3d.Click += new System.EventHandler(this.btnCompare3d3d_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(89, 19);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(68, 23);
            this.button11.TabIndex = 9;
            this.button11.Text = "Init";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1363, 787);
            this.Controls.Add(this.btnCompare3d3d);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Biometrika.Facetec.Api.TestForm...";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsAuthentication)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chkIsAuth;
        private System.Windows.Forms.TextBox txtClasiffier;
        private System.Windows.Forms.PictureBox chkIsAuthentication;
        private System.Windows.Forms.RadioButton rbGet2;
        private System.Windows.Forms.RadioButton rbGet1;
        private System.Windows.Forms.RadioButton rbGet;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTrackId3roGet;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.RichTextBox rtbResultImage;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtUserReference;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTrackId3roAuth;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTrackId3roForAuth;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTrackId3ro;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnCompare3d3d;
        private System.Windows.Forms.Button button11;
    }
}

