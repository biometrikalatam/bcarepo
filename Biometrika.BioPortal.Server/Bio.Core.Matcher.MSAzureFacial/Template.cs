﻿using Bio.Core.Matcher.Interface;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bio.Core.Matcher.MSAzureFacial
{
    public class Template : ITemplate
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Template));

        private int _authenticationFactor;

        private int _minutiaeType;

        private int _bodyPart;

        private int _type = Constant.BirType.PROCESSED_DATA;

        private byte[] _data;

        private string _additionalData;
        public int AuthenticationFactor { get => _authenticationFactor; set => _authenticationFactor = value; }
        public int MinutiaeType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int Type { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int BodyPart { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public byte[] Data { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string AdditionalData { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public string GetData => throw new NotImplementedException();

        public string SetData { set => throw new NotImplementedException(); }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Template() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion




    }
}
