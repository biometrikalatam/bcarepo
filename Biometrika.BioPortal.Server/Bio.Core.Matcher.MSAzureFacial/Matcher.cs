﻿using Bio.Core.Matcher.Interface;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bio.Core.Matcher.MSAzureFacial
{
    public class Matcher : IMatcher
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Matcher));

        int IMatcher.AuthenticationFactor { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        int IMatcher.MinutiaeType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        double IMatcher.Threshold { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        int IMatcher.MatchingType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        string IMatcher.Parameters { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        int IMatcher.Identify(string xmlinput, out string xmloutput)
        {
            throw new NotImplementedException();
        }

        int IMatcher.Identify(ITemplate templateCurrent, List<Core.Matcher.Template> listTemplatePeople, out float score, out int idBir)
        {
            throw new NotImplementedException();
        }

        int IMatcher.Verify(string xmlinput, out string xmloutput)
        {
            throw new NotImplementedException();
        }

        int IMatcher.Verify(ITemplate templateCurrent1, ITemplate templateCurrent2, out float score)
        {
            throw new NotImplementedException();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Matcher() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion


    }
}
