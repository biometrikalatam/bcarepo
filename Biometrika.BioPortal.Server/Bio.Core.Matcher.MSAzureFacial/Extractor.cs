﻿using Bio.Core.Matcher.Interface;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bio.Core.Matcher.MSAzureFacial
{
    public class Extractor : IExtractor
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Extractor));

        int IExtractor.AuthenticationFactor { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        int IExtractor.MinutiaeType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        double IExtractor.Threshold { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        string IExtractor.Parameters { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        int IExtractor.Extract(string xmlinput, out string xmloutput)
        {
            throw new NotImplementedException();
        }

        int IExtractor.Extract(string xmlinput, out ITemplate templateout)
        {
            throw new NotImplementedException();
        }

        int IExtractor.Extract(ITemplate templatebase, int destination, out ITemplate templateout)
        {
            throw new NotImplementedException();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Extractor() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}
