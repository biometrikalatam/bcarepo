﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.FoxId.Api
{
    
    public class ResponseToken
    {
        public IList<string> roles { get; set; }
        public string token { get; set; }

    }
}
