﻿using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.FoxId.Api
{
    public class ApiFunctions
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ApiFunctions));

        internal string _API_BASE_PATH = "https://foxid5.einzelnet.com";
        internal string _TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJxTnhuY1Ywc1Z5N2owdGlKdjRvaURkLU5BWXVFb01BSW0zdERXdkJEd2tjIn0.eyJleHAiOjE2NTI5OTU3NjYsImlhdCI6MTY1Mjk5NTQ2NiwianRpIjoiYTY1YTYxMTMtZGExNC00OWEwLWI5NGYtZmY1ZDJlMzNhNzAwIiwiaXNzIjoiaHR0cHM6Ly9mb3hpZDVzc28uZWluemVsbmV0LmNvbS9hdXRoL3JlYWxtcy9zdmVkNSIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiI3ZTZkMGVjMC01YmNkLTQ2YmUtODFhYy03MGM5ZTk3YjZhOTAiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJ3ZWJfYXBwIiwic2Vzc2lvbl9zdGF0ZSI6IjU3NGNkZTQ1LTBhM2ItNDMxOS1iNGVkLWQ0MzdiM2QxZGQ2MCIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cHM6Ly9mb3hpZDUuZWluemVsbmV0LmNvbSJdLCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwic2lkIjoiNTc0Y2RlNDUtMGEzYi00MzE5LWI0ZWQtZDQzN2IzZDFkZDYwIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsIlJPTEVfSU5URUdSQVRJT04iLCJ1bWFfYXV0aG9yaXphdGlvbiIsImRlZmF1bHQtcm9sZXMtc3ZlZDUiXSwibmFtZSI6IkludCBCaW9tZXRyaWthIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiaW5mb0BiaW9tZXRyaWthLmNsIiwiZ2l2ZW5fbmFtZSI6IkludCIsImZhbWlseV9uYW1lIjoiQmlvbWV0cmlrYSIsImVtYWlsIjoiaW5mb0BiaW9tZXRyaWthLmNsIn0.d1impCno7VkFF8PmvHGoAFKzdsiZNdrwiHPldoh1yCzq0fGMjoqa8I4ckwhwcEkUyfYEWRr7r727fnhNsaDdDBFnG4Y1Jasuf0PZOXajdjDJUDCRNj9lAvT-9rjBW5_5F5JJu1nxLuUlVkZSJnbHD5Wo9iLNC2PqYK-pucTvA42ERk8bgox9ykKgfLU8PC_4B8OuA0IiOFi-DiHBGLJ4kkwgG14VE_JXpaIWee6LCc2Eq6Q9ZAmHWoAFWrt0A_Go3tlGziDvklZyvbGbY3e1hFcEeQ93aG8vHjQwyq81FNxISl-NEzqkpTDpLg6BiQaPrLaADEqeAJQmnYIfFrBUJw"; //JWT Token para acceso;
        internal int _TIMEOUT = 30000;

        internal string _API_VERSION = "1.0";
        internal string _API_URL = "";
        internal string _USERNAME = "info@biometrika.cl";
        internal string _USERSECRET = "B1ometrik@";
        internal string _CLIENTID = "web_app";
        internal string _CLIENTSECRET = "42dbc72e-bf12-4773-bea8-bb31692625eb";

         internal bool _IsInitialized = false;

        public ApiFunctions() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlbaseservice"></param>
        /// <param name="timeout"></param>
        /// </param>
        /// <returns></returns>
        public int Initialize(string urlbaseservice, string username, string usersecret, 
                              string clientid, string clientsecret, int timeout = 30000)
        {
            int ret = 0;
            try
            {
                _IsInitialized = false;
                LOG.Debug("ApiFunctions.Initialize IN...");
                if (!string.IsNullOrEmpty(urlbaseservice) &&
                    !string.IsNullOrEmpty(username) &&
                    !string.IsNullOrEmpty(usersecret) &&
                    !string.IsNullOrEmpty(clientid) &&
                    !string.IsNullOrEmpty(clientsecret)) 
                {
                    _API_BASE_PATH = urlbaseservice;
                    _TIMEOUT = timeout;
                    _USERNAME = username;
                    _USERSECRET = usersecret;
                    _CLIENTID = clientid;
                    _CLIENTSECRET = clientsecret;
                    LOG.Debug("ApiFunctions.Initialize - URL Base Service = " + _API_BASE_PATH);
                    LOG.Debug("ApiFunctions.Initialize - Timeout = " + _TIMEOUT.ToString());
                    LOG.Debug("ApiFunctions.Initialize - username = " + _USERNAME.ToString());
                    LOG.Debug("ApiFunctions.Initialize - clienid = " + _CLIENTID.ToString());

                    string msg;
                    ret = RenewToken(out msg);
                    LOG.Debug("ApiFunctions.Initialize - retorno RenewToken = " + ret.ToString() + " [msg=" +
                                (string.IsNullOrEmpty(msg) ? "" : msg) + "]");
                    _IsInitialized = (ret == 0);
                } else
                {
                    LOG.Debug("ApiFunctions.Initialize - Alguno de los parametros es nulo...");
                    ret = -6;
                }
            }
            catch (Exception ex)
            {
                _IsInitialized = false;
                ret = -1;
                LOG.Error("ApiFunctions.Initialize Excp Error: " + ex.Message);
                LOG.Error("StackTrace => " + ex.StackTrace);
                LOG.Error("InnerException => " + ex.InnerException);
            }
            LOG.Debug("ApiFunctions.Initialize - OUT! _IsInitialized = " + _IsInitialized.ToString());
            return ret;
        }

        public int Process(List<Picture> pictureList, out Report report, out string msg) //, int capabilities, int authenticity)
        {
            int ret = 0;
            report = null;
            msg = null;
            try
            {
                LOG.Debug("ApiFunctions.Process IN...");

                if (!_IsInitialized)
                {
                    ret = -2;
                    msg = "API no inicializada!";
                    LOG.Error("ApiFunctions.Process - API no inicializada!");
                    return ret;
                }

                if (pictureList == null || pictureList.Count == 0)
                {
                    ret = -6;
                    msg = "Parametros erroneos. Lista vacia";
                    LOG.Error("ApiFunctions.Process - Parametros erroneos");
                    return ret;
                }

                var client = new RestClient(_API_BASE_PATH + "/api/public/mobile/createAndIdentify");
                client.Timeout = _TIMEOUT;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "Bearer " + _TOKEN);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Cookie", "XSRF-TOKEN=9a7cbdc8-f9cb-4a16-b034-235957d0a624");
                APIParams parametros = new APIParams(GetImage(pictureList, Picture.SIDE_FRONT),
                                                     GetImage(pictureList, Picture.SIDE_BACK),
                                                     GetImage(pictureList, Picture.SELFIE)); 
                var body = JsonConvert.SerializeObject(parametros);
                request.AddParameter("application/json", body,  ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response != null)
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        ret = RenewToken(out msg);
                        if (ret != 0) msg = "No pudo renovar token [" + msg + "]";
                        LOG.Error("ApiFunctions.Process - Error renovando token [" + msg + "]");
                        return ret;
                    }


                    string jsonString = response.Content;
                    report = JsonConvert.DeserializeObject<Report>(jsonString);
                } else
                {
                    report = null;
                    msg = "Response de API == null!";
                    LOG.Error("ApiFunctions.Process - Response de API == null!");
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "Excp Error: " + ex.Message;
                LOG.Error("ApiFunctions.Process Excp Error: " + ex.Message);
            }
            LOG.Debug("ApiFunctions.Process OUT! ret = " + ret.ToString());
            return ret;
        }

        public int RenewToken(out string msg) 
        {
            int ret = 0;
            msg = null;
            try
            {
                LOG.Debug("ApiFunctions.RenewToken IN...");

                //if (!_IsInitialized)
                //{
                //    ret = -2;
                //    msg = "API no inicializada!";
                //    LOG.Error("ApiFunctions.RenewToken - API no inicializada!");
                //    return ret;
                //}

                var client = new RestClient(_API_BASE_PATH + "/api/public/security/token/create");
                client.Timeout = _TIMEOUT;

             
                var request = new RestRequest(Method.POST);
                request.AddHeader("username", _USERNAME);
                request.AddHeader("userSecret", _USERSECRET);
                request.AddHeader("clientId", _CLIENTID);
                request.AddHeader("clientSecret", _CLIENTSECRET);
                //request.AddHeader("Cookie", "XSRF-TOKEN=7bd0d2d7-9a4b-4f18-82aa-9057966edb0b");
                IRestResponse response = client.Execute(request);

                if (response != null)
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        ResponseToken rt = JsonConvert.DeserializeObject<ResponseToken>(response.Content);
                        if (rt != null)
                        {
                            _TOKEN = rt.token;
                            LOG.Debug("ApiFunctions.RenewToken - Token renovado = " + (!string.IsNullOrEmpty(_TOKEN)).ToString());
                        }
                    }
                }
                else
                {
                    msg = "Response de API para renovar token == null!";
                    LOG.Error("ApiFunctions.Process - Response de API para renovar token == null!");
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "Excp Error: " + ex.Message;
                LOG.Error("ApiFunctions.Process Excp Error: " + ex.Message);
            }
            LOG.Debug("ApiFunctions.Process OUT! ret = " + ret.ToString());
            return ret;
        }

        private string GetImage(List<Picture> pictureList, int flag)
        {
            string ret = null;
            try
            {
                LOG.Debug("ApiFunctions.GetImage IN...");
                if (pictureList == null)
                {
                    LOG.Debug("ApiFunctions.GetImage - Retorno null porque pictureList == null...");
                    return null;
                }

                foreach (Picture item in pictureList)
                {
                    if (item.Side == flag)
                    {
                        ret = item.Base64ImageString;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("ApiFunctions.GetImage Excp Error: " + ex.Message);
            }
            LOG.Debug("ApiFunctions.GetImage OUT!");
            return ret;
        }

        private bool EqualNumber(string runMRZ, string runVisual)
        {
            try
            {
                if (string.IsNullOrEmpty(runMRZ) || string.IsNullOrEmpty(runVisual)) return true;

                ///Comento 07-05-2021 xq si no es chileno se cae por formateo de rut. El chequeo de rut
                ///Se hace luego consultando si es o no de chile primero
                //if (!string.IsNullOrEmpty(runMRZ) && !string.IsNullOrEmpty(runVisual))
                //{
                //    RUN run1 = RUN.ConvertirEx(runMRZ);
                //    RUN run2 = RUN.ConvertirEx(runVisual);
                //    return (run1.ToString(true).Equals(run2.ToString(true)));
                //}
            }
            catch (Exception ex)
            {
                LOG.Error("RegulaHandler.NotEqualNumber Error: " + ex.Message);
                return false;
            }
            return true;
        }
    }
}
