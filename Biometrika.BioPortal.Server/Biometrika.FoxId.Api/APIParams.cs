﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.FoxId.Api
{
    internal class APIParams
    {
        public APIParams() { }
        public APIParams(string _front_document, string _back_document, string _selfie)
        {
            front_document = _front_document;
            back_document = _back_document;
            selfie = _selfie;
        }

        public string front_document;
        public string back_document;
        public string selfie;
    }
}
