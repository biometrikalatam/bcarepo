﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Biometrika.FoxId.Api
{
    public class Picture
    {

        public static int SIDE_FRONT = 1;
        public static int SIDE_BACK = 2;
        public static int SELFIE = 3;

        public string Base64ImageString { get; set; }
        public string Format { get; set; } = ".jpg";
        public int Side { get; set; } = 1;  //1-Fron | 2-Back

        public static string ConvertImageToBase64String(Image img)
        {
            string base64String;
            try
            {
                using (var ms = new MemoryStream())
                {
                    img.Save(ms, ImageFormat.Jpeg);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    base64String = Convert.ToBase64String(imageBytes);
                }
            }
            catch (Exception ex)
            {
                base64String = null;
                //LOG.Error(" Error: " + ex.Message);
            }
           
            return base64String;
        }
    }
}
