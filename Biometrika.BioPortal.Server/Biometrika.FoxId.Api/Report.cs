﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.FoxId.Api
{
    public class Report
    {
        public string status { get; set; }
        public Data data { get; set; }
        public IList<string> messages { get; set; }

    }

    public class OriginalImages
    {
        public string front { get; set; }
        public string back { get; set; }
        public string selfie { get; set; }

    }
    public class MiniImages
    {
        public string front { get; set; }
        public string back { get; set; }
        public string selfie { get; set; }

    }
    public class MrzData
    {
        public string address { get; set; }
        public string card_number { get; set; }
        public string date_of_birth { get; set; }
        public string date_of_expiry { get; set; }
        public bool expired { get; set; }
        public string date_of_issue { get; set; }
        public string document_type { get; set; }
        public string first_name { get; set; }
        public string gender { get; set; }
        public string issuing_state_code { get; set; }
        public string mrz { get; set; }
        public string nationality { get; set; }
        public string optional_data { get; set; }
        public IList<string> parents { get; set; }
        public string personal_number { get; set; }
        public string place_of_birth { get; set; }
        public string surname_and_name { get; set; }
        public string surnames { get; set; }

    }
    public class VizData
    {
        public string address { get; set; }
        public string card_number { get; set; }
        public string date_of_birth { get; set; }
        public string date_of_expiry { get; set; }
        public bool expired { get; set; }
        public string date_of_issue { get; set; }
        public string document_type { get; set; }
        public string first_name { get; set; }
        public string gender { get; set; }
        public string issuing_state_code { get; set; }
        public string mrz { get; set; }
        public string nationality { get; set; }
        public string optional_data { get; set; }
        public string parents { get; set; }
        public string personal_number { get; set; }
        public string place_of_birth { get; set; }
        public string surname_and_name { get; set; }
        public string surnames { get; set; }

    }
    public class BarcodeData
    {
        public string address { get; set; }
        public string card_number { get; set; }
        public string date_of_birth { get; set; }
        public string date_of_expiry { get; set; }
        public bool expired { get; set; }
        public string date_of_issue { get; set; }
        public string document_type { get; set; }
        public string first_name { get; set; }
        public string gender { get; set; }
        public string issuing_state_code { get; set; }
        public string mrz { get; set; }
        public string nationality { get; set; }
        public string optional_data { get; set; }
        public string parents { get; set; }
        public string personal_number { get; set; }
        public string place_of_birth { get; set; }
        public string surname_and_name { get; set; }
        public string surnames { get; set; }

    }
    public class ValueData
    {
        public string address { get; set; }
        public string card_number { get; set; }
        public string date_of_birth { get; set; }
        public string date_of_expiry { get; set; }
        public bool expired { get; set; }
        public string date_of_issue { get; set; }
        public string document_type { get; set; }
        public string first_name { get; set; }
        public string gender { get; set; }
        public string issuing_state_code { get; set; }
        public string mrz { get; set; }
        public string nationality { get; set; }
        public string optional_data { get; set; }
        public string parents { get; set; }
        public string personal_number { get; set; }
        public string place_of_birth { get; set; }
        public string surname_and_name { get; set; }
        public string surnames { get; set; }

    }
    public class AddressData
    {
        public string address { get; set; }
        public string city { get; set; }
        public string postalCode { get; set; }
        public string province { get; set; }
        public string country { get; set; }

    }
    public class PersonalData
    {
        public AddressData addressData { get; set; }
        public int age { get; set; }
        public string birthDate { get; set; }
        public string birthPlace { get; set; }
        public string country { get; set; }
        public string givenName { get; set; }
        public string lastName { get; set; }
        public string nationality { get; set; }
        public string optionalData { get; set; }
        public string personalNumber { get; set; }
        public string sex { get; set; }

    }
    public class DocumentData
    {
        public string country { get; set; }
        public string country_iso { get; set; }
        public string country_risk { get; set; }
        public string doc_subtype { get; set; }
        public string doc_type { get; set; }

    }
    public class Results
    {
        public bool automaticVerificationsResult { get; set; }
        public string automaticVerificationsResultValue { get; set; }
        public string automaticVerificationsDescriptionResult { get; set; }
        public string automaticVerificationsDescriptionResultCode { get; set; }
        public string expertVerificationsResult { get; set; }
        public string expertVerificationsDescriptionResult { get; set; }
        public string expertVerificationsDescriptionResultCode { get; set; }
        public string expertComment { get; set; }
        public bool finalVerificationsResult { get; set; }
        public string finalVerificationsResultValue { get; set; }
        public string finalVerificationsDescriptionResult { get; set; }
        public string finalVerificationsDescriptionResultCode { get; set; }
        public bool onboardingVerificationResult { get; set; }
        public string onboardingVerificationResultValue { get; set; }
        public List<string> notifications { get; set; }

    }
    public class Timeline
    {
        public string operationRequestDate { get; set; }
        public string engineRequestDate { get; set; }
        public string engineResponseDate { get; set; }
        public string operationResponseDate { get; set; }
        public string operationEllapsedTime { get; set; }
        public string expertRequestDate { get; set; }
        public string expertEstimatedResponseDate { get; set; }
        public string expertResponseDate { get; set; }

    }
    public class Data
    {
        public string operationId { get; set; }
        public string operationState { get; set; }
        public string operationStateDescription { get; set; }
        public OriginalImages originalImages { get; set; }
        public MiniImages miniImages { get; set; }
        public MrzData mrzData { get; set; }
        public VizData vizData { get; set; }
        public BarcodeData barcodeData { get; set; }
        public ValueData valueData { get; set; }
        public PersonalData personalData { get; set; }
        public DocumentData documentData { get; set; }
        public Results results { get; set; }
        public Timeline timeline { get; set; }

    }
    
}
