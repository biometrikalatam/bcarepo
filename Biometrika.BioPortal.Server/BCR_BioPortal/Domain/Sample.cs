﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCR_BioPortal.Domain
{
    /// <summary>
    /// Codigo importado del Framework de TFS, sin version
    /// </summary>
    public class Sample
    {
        /// <summary>
        /// Contiene el sample en Base64
        /// </summary>
        public virtual string Data
        {
            get;
            set;
        }

        /// <summary>
        /// Tipo de data contenido en Data
        /// </summary>
        public virtual int Minutiaetype
        {
            get;
            set;
        }

        /// <summary>
        /// Reservado apra datos adicionales como w/h en WSQ o
        /// coeficiente en NEC
        /// </summary>
        public virtual string Additionaldata
        {
            get;
            set;
        }

        /// <summary>
        /// Id de device usado para obtener la muestra
        /// </summary>
        public int Device
        {
            get;
            set;
        }
    }
}