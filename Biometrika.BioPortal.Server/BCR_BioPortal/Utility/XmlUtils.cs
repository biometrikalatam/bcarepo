﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace BCR_BioPortal.Utility
{
    /// <summary>
    /// Codigo importado del Framework de TFS, sin version
    /// </summary>
    public class XmlUtils
    {
        //private static readonly ILog LOG = LogManager.GetLogger(typeof(XmlUtils));

        /// <summary>
        /// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.
        /// </summary>
        /// <param name="characters">Unicode Byte Array to be converted to String</param>
        /// <returns>String converted from Unicode Byte Array</returns>
        private static string UTF8ByteArrayToString(byte[] characters)
        {
            string constructedString = null;
            try
            {
                UTF8Encoding encoding = new UTF8Encoding();
                constructedString = encoding.GetString(characters);
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Utils.XmlUtils.UTF8ByteArrayToString", ex);
            }
            return constructedString;
        }

        /// <summary>
        /// Converts the String to UTF8 Byte array and is used in De serialization
        /// </summary>
        /// <param name="pXmlString"></param>
        /// <returns></returns>
        private static Byte[] StringToUTF8ByteArray(string pXmlString)
        {
            byte[] byteArray = null;
            try
            {
                UTF8Encoding encoding = new UTF8Encoding();
                byteArray = encoding.GetBytes(pXmlString);
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Utils.XmlUtils.StringToUTF8ByteArray", ex);
            }
            return byteArray;
        }

        /// <summary>
        /// Serialize an object into an XML string
        /// </summary>
        /// <typeparam name="T">Tipo del objeto a serializar</typeparam>
        /// <param name="obj">Objeto a serializar</param>
        /// <returns>XML conteninedo el objeto serializado</returns>
        public static string SerializeObject<T>(T obj)
        {
            string xmlString = null;
            try
            {
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(obj.GetType()); //typeof(T));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xs.Serialize(xmlTextWriter, obj);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                xmlString = UTF8ByteArrayToString(memoryStream.ToArray()); return xmlString;
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Utils.XmlUtils.SerializeObject<T>", ex);
            }
            return xmlString;
        }

        /// <summary>
        /// Reconstruct an object from an XML string
        /// </summary>
        /// <param name="xml">xml a deserializar</param>
        /// <returns>objeto deserializado</returns>
        public static T DeserializeObject<T>(string xml)
        {
            object oRet = null;
            XmlSerializer xs = null;
            try
            {
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");
                xs = new XmlSerializer(typeof(T), "");
                MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(xml));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                oRet = (T)xs.Deserialize(memoryStream);
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Utils.XmlUtils.DeserializeObject<T>", ex);
            }
            return (T)oRet;
        }

        /// <summary>
        /// Dado un objeto, lo serializa a xml
        /// </summary>
        /// <param name="obj">Objeto para serializar</param>
        /// <returns>XML de la serialización</returns>
        public static string SerializeAnObject(object obj)
        {
            string sRet = null;
            MemoryStream stream = new MemoryStream();
            try
            {
                XmlDocument doc = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");
                serializer.Serialize(stream, obj, ns);
                stream.Position = 0; doc.Load(stream);
                sRet = doc.InnerXml;
            }
            catch (Exception ex)
            {
                //LOG.Error("Bio.Core.Utils.XmlUtils.SerializeAnObject", ex);
            }
            finally
            {
                stream.Close();
                stream.Dispose();
            }
            return sRet;
        }
    }
}
