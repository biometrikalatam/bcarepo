﻿using System;
using System.Windows.Forms;
using Bio.Core.Api;
using Bio.Core.Imaging;
using Bio.Portal.Server.Sync.GUITest.BioPortal.Server.Sync.WS;
using BioPortal.Server.Api;
using XmlUtils = Bio.Core.Utils.XmlUtils;
using BpIdentitySync = BioPortal.Server.Sync.Api.BpIdentity;
using BpBirSync = BioPortal.Server.Sync.Api.BpBir;

namespace Bio.Portal.Server.Sync.GUITest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
            pictureBox1.Image = null;
            pictureBox2.Image = null;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "Get Sync : " + textBox1.Text + "..." + Environment.NewLine;
            int expected = 0;
            int actual;
            string xmlparamout = string.Empty;
            using (BioPortal.Server.Sync.WS.BioPortalServerSync target = new BioPortalServerSync())
            {
                target.Timeout = 999999999;
                XmlParamIn pin = new XmlParamIn();
                pin.Actionid = Convert.ToInt32(textBox1.Text);
                pin.Clientid = "GUITestClientG";
                pin.Companyid = Convert.ToInt32(textBox2.Text);
                pin.Enduser = "GUITestClientG";
                pin.Ipenduser = "127.0.0.1";
                pin.Authenticationfactor = Convert.ToInt32(textBox4.Text);
                pin.Minutiaetype = Convert.ToInt32(textBox3.Text);
                pin.Bodypart = Convert.ToInt32(textBox5.Text); //Bio.Core.Matcher.Constant.BodyPart.BODYPART_ALL;
                pin.Matchingtype = 1;
                pin.Origin = 1;
                pin.SampleCollection = null;
                pin.Userid = 0;
                pin.PersonalData = new PersonalData();
                pin.PersonalData.Typeid = txtID.Text.Trim();
                pin.PersonalData.Valueid = txtValueID.Text.Trim();
                pin.InsertOption = 0;

                string xmlparamin = XmlUtils.SerializeAnObject(pin);
                actual = target.Sync("xxxx", xmlparamin, out xmlparamout);
            }
            richTextBox1.Text = "Codigo Retorno=" + actual.ToString() + Environment.NewLine + xmlparamout;

            if (actual == 0)
            {
                if (xmlparamout != null)
                {
                    XmlParamOut paramout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);

                    string xmlidentout = (string)paramout.Additionaldata.GetValue("Identity");

                    BpIdentitySync identout =
                        Bio.Core.Utils.SerializeHelper.DeserializeFromXml<BpIdentitySync>(xmlidentout);

                    richTextBox1.Text = richTextBox1.Text + Environment.NewLine + "Birs....";
                    if (identout.BpBir != null)
                    {
                        richTextBox1.Text = "Cantidad de BIRs = " + identout.BpBir.Count.ToString() + "...";
                        foreach (BpBirSync bir in identout.BpBir)
                        {
                            richTextBox1.Text = richTextBox1.Text + Environment.NewLine +
                                                "--------------------------------------------" +
                                                Environment.NewLine +
                                                "Tecnologia= " + bir.Authenticationfactor.ToString() + "[ " + Bio.Core.Matcher.Constant.AuthenticationFactor.GetName(bir.Authenticationfactor) + "] - " +
                                                "Minucia= " + bir.Minutiaetype.ToString() + "[" + Core.Matcher.Constant.MinutiaeType.GetDescription(bir.Minutiaetype) + "] -" +
                                                "Dedo= " + bir.Bodypart.ToString() + "[" + Core.Matcher.Constant.BodyPart.GetName(bir.Bodypart) + "] -" +
                                                "Data Bio=" + bir.Data.Substring(0,25) + "... -" +
                                                "Fecha Enroll=" + bir.Creation.ToString("dd/MM/yyyy HH:mm:ss");

                        }
                    }
                    if (identout != null)
                    {
                        if (!String.IsNullOrEmpty(identout.Photography))
                        {
                            Bio.Core.Imaging.Imagen img =
                                new Imagen(Convert.FromBase64String(identout.Photography));
                            pictureBox1.Image = img.Image;
                        }
                        else
                        {
                            pictureBox1.Image = null;
                            richTextBox1.Text = richTextBox1.Text + Environment.NewLine +
                                                "No existe foto para esta identidad!";
                        }

                        if (!String.IsNullOrEmpty(identout.Signatureimage))
                        {
                            Bio.Core.Imaging.Imagen img =
                                new Imagen(Convert.FromBase64String(identout.Signatureimage));
                            pictureBox2.Image = img.Image;
                        }
                        else
                        {
                            pictureBox2.Image = null;
                            richTextBox1.Text = richTextBox1.Text + Environment.NewLine +
                                                "No existe firma para esta identidad!";
                        }
                    }
                }
            }
        }


    }
}
