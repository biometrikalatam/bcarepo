﻿using Bio.Core.Api.Constant;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BioPortal.Server.Api.Json.API
{
    internal class APIWSWEB
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        protected IRestClient Client;

        internal APIWSWEB(string host)
        {
            Client = new RestClient(host);
        }

        internal XmlParamOut Enroll(XmlParamIn xmlParamIn, int customTimeOut = 0)
        {
            var request = new RestRequest("api/WSWEB/Enroll/{customTimeOut?}", Method.POST);
            if (customTimeOut > 0)
            {
                request.Timeout = customTimeOut;
                request.AddUrlSegment("customTimeOut", customTimeOut.ToString());
            }

            request.AddJsonBody(xmlParamIn);

            try
            {
                IRestResponse response = Client.Execute(request);
                var content = response.Content;

                return JsonConvert.DeserializeObject<XmlParamOut>(content);
            }
            catch (Exception e)
            {
                Log.Error("Error APIWSWEB.Enroll", e);
                return new XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_DESERIALIZE_XMLPARAMOUT)
                };
            }
        }

        internal XmlParamOut Identify(XmlParamIn xmlParamIn, int customTimeOut = 0)
        {
            var request = new RestRequest("api/WSWEB/Identify/{customTimeOut?}", Method.POST);
            if (customTimeOut > 0)
            {
                request.Timeout = customTimeOut;
                request.AddUrlSegment("customTimeOut", customTimeOut.ToString());
            }

            request.AddJsonBody(xmlParamIn);

            try
            {
                IRestResponse response = Client.Execute(request);
                var content = response.Content;

                return JsonConvert.DeserializeObject<XmlParamOut>(content);
            }
            catch (Exception e)
            {
                Log.Error("Error APIWSWEB.Identify", e);
                return new XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_DESERIALIZE_XMLPARAMOUT)
                };
            }
        }

        internal XmlParamOut Verify(XmlParamIn xmlParamIn, int customTimeOut = 0)
        {
            var request = new RestRequest("api/WSWEB/Verify/{customTimeOut?}", Method.POST);
            if (customTimeOut > 0)
            {
                request.Timeout = customTimeOut;
                request.AddUrlSegment("customTimeOut", customTimeOut.ToString());
            }

            request.AddJsonBody(xmlParamIn);

            try
            {
                IRestResponse response = Client.Execute(request);
                var content = response.Content;

                return JsonConvert.DeserializeObject<XmlParamOut>(content);
            }
            catch (Exception e)
            {
                Log.Error("Error APIWSWEB.Verify", e);
                return new XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_DESERIALIZE_XMLPARAMOUT)
                };
            }
        }
    }
}
