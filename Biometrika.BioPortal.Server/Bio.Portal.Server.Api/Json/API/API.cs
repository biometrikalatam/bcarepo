﻿using log4net;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BioPortal.Server.Api.Json.API
{
    public class API
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        internal APIWS APIWS;
        internal APIWSWEB APIWSWEB;
        internal APIPP APIPP;

        /// <summary>
        /// Constructor de la clase API para la comunicación con la API Rest BioPortalServerServices.WebApi
        /// </summary>
        /// <param name="hostAPI">URL de comunicación con API</param>
        public API(string hostAPI)
        {
            if (!string.IsNullOrEmpty(hostAPI))
            {
                APIWS = new APIWS(hostAPI);
                APIWSWEB = new APIWSWEB(hostAPI);
                APIPP = new APIPP(hostAPI);
            }
        }

        #region APIWS

        /// <summary>
        /// Método de consumo de API Bioportal, Enroll sin token
        /// </summary>
        /// <param name="xmlParamIn">XmlParamIn de entrada</param>
        /// <param name="customTimeOut">TimeOut propio del método</param>
        /// <returns></returns>
        public XmlParamOut WSEnroll(XmlParamIn xmlParamIn, int customTimeOut = 0)
        {
            if (APIWS != null)
                return APIWS.Enroll(xmlParamIn, customTimeOut);
            return new XmlParamOut
            {
                Message = "Debe inicializar API con hostAPI"
            };
        }

        public XmlParamOut WSIdentify(XmlParamIn xmlParamIn, int customTimeOut = 0)
        {
            if (APIWS != null)
                return APIWS.Identify(xmlParamIn, customTimeOut);
            return new XmlParamOut
            {
                Message = "Debe inicializar API con hostAPI"
            };
        }

        public XmlParamOut WSVerify(XmlParamIn xmlParamIn, int customTimeOut = 0)
        {
            if (APIWS != null)
                return APIWS.Verify(xmlParamIn, customTimeOut);
            return new XmlParamOut
            {
                Message = "Debe inicializar API con hostAPI"
            };
        }

        public XmlParamOut WSGet(XmlParamIn xmlParamIn, int customTimeOut = 0)
        {
            if (APIWS != null)
                return APIWS.Get(xmlParamIn, customTimeOut);
            return new XmlParamOut
            {
                Message = "Debe inicializar API con hostAPI"
            };
        }

        #endregion

        #region APIWSWEB

        public XmlParamOut WSWEBEnroll(XmlParamIn xmlParamIn, int customTimeOut = 0)
        {
            if (APIWSWEB != null)
                return APIWSWEB.Enroll(xmlParamIn, customTimeOut);
            return new XmlParamOut
            {
                Message = "Debe inicializar API con hostAPI"
            };
        }

        public XmlParamOut WSWEBIdentify(XmlParamIn xmlParamIn, int customTimeOut = 0)
        {
            if (APIWSWEB != null)
                return APIWSWEB.Identify(xmlParamIn, customTimeOut);
            return new XmlParamOut
            {
                Message = "Debe inicializar API con hostAPI"
            };
        }

        public XmlParamOut WSWEBVerify(XmlParamIn xmlParamIn, int customTimeOut = 0)
        {
            if (APIWSWEB != null)
                return APIWSWEB.Verify(xmlParamIn, customTimeOut);
            return new XmlParamOut
            {
                Message = "Debe inicializar API con hostAPI"
            };
        }

        #endregion

        #region APIPP

        /// <summary>
        /// Método de consumo de API Bioportal, Enroll sin token
        /// </summary>
        /// <param name="xmlParamIn">XmlParamIn de entrada</param>
        /// <param name="customTimeOut">TimeOut propio del método</param>
        /// <returns></returns>
        public XmlParamOut Process(XmlParamIn xmlParamIn, int customTimeOut = 0)
        {
            if (APIPP != null)
                return APIPP.Process(xmlParamIn, customTimeOut);
            return new XmlParamOut
            {
                Message = "Debe inicializar API con hostAPI"
            };
        }

        #endregion

    }
}
