﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BioPortal.Server.Api.Json
{
    /// <summary>
    /// Codigo importado del Framework de TFS, sin version
    /// </summary>
    public class DynamicDataItem
    {
        /// <summary>
        ///  Ctor for serialization 
        /// </summary>
        public DynamicDataItem()
        {

        }

        public DynamicDataItem(string pkey, string pvalue)
        {
            Key = pkey;
            Value = pvalue;
        }

        ///<summary>
        ///</summary>
        public string Key
        {
            get;
            set;
        }

        ///<summary>
        ///</summary>
        public string Value
        {
            get;
            set;
        }
    }
}
