﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BioPortal.Server.Api.Json
{
    /// <summary>
    /// Codigo importado del Framework de TFS, sin version
    /// </summary>
    public class XmlParamIn : ICloneable
    {

        #region Private

        /// <summary>
        /// Posibles Valores =
        ///      1-Verify
        ///      2-Identify
        ///      3-Enroll
        ///         4-Get
        ///         5-Modify
        ///         6-Delete
        ///      7-GetBirs
        /// </summary>
        private int _actionid;

        /// <summary>
        ///Datos de una persona: nombre, apellido, foto, etc.
        ///Si no es para Enroll o Modify, solo viene con typeid y valueid.
        /// </summary>
        private PersonalData _personalData;

        /// <summary>
        ///Informacion sobre tipo de tecnologia a utilizar para verificacion o identificacion.
        ///Si no esta definido, no prosigue.
        /// </summary>
        private int _authenticationfactor;
        /// <summary>
        ///Parte de cuerpo a verificar. Si es 0 o menor => Chequea contra todas las partes
        /// </summary>
        private int _bodypart;
        /// <summary>
        ///En caso que venga sample sin procesar (ej WSQ) esto indica que usar para chequear
        ///Si viene 0 o menor 0 => Usa las definidas por default en Config.
        /// </summary>
        private int _minutiaetype;

        /// <summary>
        /// Indica que tipo de matching se hace en Identify o en Verify (cuando hay muchas muestras
        /// por parte de cuerpo o es con _bodypart = 0 o menor.
        /// Posibles valores:
        ///         0-Default   => Configurado en config
        ///         1-First     => Primer match positivo
        ///         2-Best      => Mejor match
        /// </summary>
        private int _matchingtype;

        /// <summary>
        /// Sample enviada para verificar, identificar o enrolar (en base64)
        /// Se envia un XML conteniendo uno o mas samples. El formato del XML será:
        ///     <samplecollection>
        ///         <sample>
        ///             <data>En Base 64</data>
        ///             <minutiaetype>
        ///                     Tipo de sample, desde WSQ, RAW, templates u otro, de acuerdo a MinutiaeType Constant
        ///                     Si es 0 o menor, o no existe en los definidos, retorna con error.
        ///             </minutiaetype>
        ///         </sample>
        ///     </samplecollection>
        /// </summary>
        private List<Sample> _samplecollection;
        ///// <summary>
        /////Tipo de sample, desde WSQ, RAW, templates u otro, de acuerdo a MinutiaeType Constant
        /////Si es 0 o menor, o no existe en los definidos, retorna con error.
        ///// </summary>
        //private int _sampleminutiaetype;

        /// <summary>
        /// Es usado para enviar datos adicionales de la muestra, por ejemplo si es WSQ, 
        /// se envia W=256|H=256, o si es template NEC, se envia COEF=90. Esto debe ser 
        /// entendico por el Matcher correspondiente, o quien trate el parametro IN.
        /// </summary>
        private string _additionaldata;

        /// <summary>
        /// Si es verificación umbral de verificacion, si es enroll, de extraccion.
        /// Si es 0 => usa default de config
        /// </summary>
        private double _threshold;

        /// <summary>
        /// Id de Compañia para filtrar si está deshabilitada. 
        /// Si no viene informada o no existe, se rechaza transaccion
        /// </summary>
        private int _companyid;
        /// <summary>
        /// Usuario de la compañia companyid, para referencia.
        /// </summary>
        private int _userid;

        /// <summary>
        /// Id de Origen de la transacción. Si el origen no existe se rechaza la transacción.
        /// </summary>
        private int _origin;

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist 
        /// </summary>
        private string _clientid;
        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        private string _ipenduser;
        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        private string _enduser;

        /// <summary>
        /// Determina si se graba en BD una copia de cada muestra verificada
        /// Valores Posibles:
        ///     0-Default del Config
        ///     1-Save Data
        ///     2-No Save Data
        /// </summary>
        private int _saveVerified;

        /// <summary>
        /// En caso de enrolamiento, indica a través de que conector a fuente de datos
        /// externa puede verificar. Si es nulo => no verifica.
        /// </summary>
        private string _verifybyconnectorid;

        /// <summary>
        /// Determina en que orden se hacen las operaciones:
        ///     OPERATIONORDER_LOCALONLY = 1;
        ///     OPERATIONORDER_LOCALFIRST = 2;
        ///     OPERATIONORDER_REMOTEONLY = 3;
        /// </summary>
        private int _operationOrder;

        /// <summary>
        /// Determina en que orden se hace 
        /// Valores Posibles:
        ///     0-Default del Config
        ///     1-Enroll
        ///     2-No Enroll
        /// </summary>
        private int _insertOption;

        #endregion Private

        #region Public

        /// <summary>
        /// Posibles Valores =
        ///      1-Verify
        ///      2-Identify
        ///      3-Enroll
        ///      4-Get
        ///      5-Modify
        ///      6-Delete
        ///      7-DeleteBir
        /// </summary>
        public int Actionid
        {
            get { return _actionid; }
            set { _actionid = value; }
        }

        /// <summary>
        ///Datos de una persona: nombre, apellido, foto, etc.
        ///Si no es para Enroll o Modify, solo viene con typeid y valueid.
        /// </summary>
        public PersonalData PersonalData
        {
            get { return _personalData; }
            set { _personalData = value; }
        }

        /// <summary>
        ///Informacion sobre tipo de tecnologia a utilizar para verificacion o identificacion.
        ///Si no esta definido, no prosigue.
        /// </summary>
        public int Authenticationfactor
        {
            get { return _authenticationfactor; }
            set { _authenticationfactor = value; }
        }

        /// <summary>
        ///Pparte de cuerpo a verificar. Si es 0 => Chequea contra todas las partes
        /// </summary>
        public int Bodypart
        {
            get { return _bodypart; }
            set { _bodypart = value; }
        }

        /// <summary>
        ///En caso que venga sample sin procesar (ej WSQ) esto indica que usar para chequear
        ///Si viene 0 o menor 0 => Usa las definidas por default en Config.
        /// </summary>
        public int Minutiaetype
        {
            get { return _minutiaetype; }
            set { _minutiaetype = value; }
        }

        /// <summary>
        /// Indica que tipo de matching se hace en Identify o en Verify (cuando hay muchas muestras
        /// por parte de cuerpo o es con _bodypart = 0 o menor.
        /// Posibles valores:
        ///         0-Default   => Configurado en config
        ///         1-First     => Primer match positivo
        ///         2-Best      => Mejor match
        /// </summary>
        public int Matchingtype
        {
            get { return _matchingtype; }
            set { _matchingtype = value; }
        }

        /// <summary>
        /// Sample enviada para verificar, identificar o enrolar (en base64)
        /// Se envia un XML conteniendo uno o mas samples. El formato del XML será:
        ///     <sample>
        ///         <sampleinstance>
        ///             <data>En Base 64</data>
        ///             <minutiaetype>
        ///                     Tipo de sample, desde WSQ, RAW, templates u otro, de acuerdo a MinutiaeType Constant
        ///                     Si es 0 o menor, o no existe en los definidos, retorna con error.
        ///             </minutiaetype>
        ///         </sampleinstance>
        ///     </sample>
        /// </summary>
        public List<Sample> SampleCollection
        {
            get { return _samplecollection; }
            set { _samplecollection = value; }
        }

        ///// <summary>
        /////Tipo de sample, desde WSQ, RAW, templates u otro, de acuerdo a MinutiaeType Constant
        /////Si es 0 o menor, o no existe en los definidos, retorna con error.
        ///// </summary>
        //public int Sampleminutiaetype
        //{
        //    get { return _sampleminutiaetype; }
        //    set { _sampleminutiaetype = value; }
        //}

        /// <summary>
        /// Es usado para enviar datos adicionales de la muestra, por ejemplo si es WSQ, 
        /// se envia W=256|H=256, o si es template NEC, se envia COEF=90. Esto debe ser 
        /// entendico por el Matcher correspondiente, o quien trate el parametro IN.
        /// </summary>
        public string Additionaldata
        {
            get { return _additionaldata; }
            set { _additionaldata = value; }
        }

        /// <summary>
        /// Si es verificación umbral de verificacion, si es enroll, de extraccion.
        /// Si es 0 => usa default de config
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Id de Compañia para filtrar si está deshabilitada. 
        /// Si no viene informada o no existe, se rechaza transaccion
        /// </summary>
        public int Companyid
        {
            get { return _companyid; }
            set { _companyid = value; }
        }

        /// <summary>
        /// Usuario de la compañia companyid, para referencia.
        /// </summary>
        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }
        }

        /// <summary>
        /// Id de Origen de la transacción. Si el origen no existe se rechaza la transacción.
        /// </summary>
        public int Origin
        {
            get { return _origin; }
            set { _origin = value; }
        }

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist 
        /// </summary>
        public string Clientid
        {
            get { return _clientid; }
            set { _clientid = value; }
        }

        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        public string Ipenduser
        {
            get { return _ipenduser; }
            set { _ipenduser = value; }
        }

        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        public string Enduser
        {
            get { return _enduser; }
            set { _enduser = value; }
        }

        /// <summary>
        /// En caso de enrolamiento, indica a través de que conector a fuente de datos
        /// externa puede verificar.
        /// </summary>
        public string Verifybyconnectorid
        {
            get { return _verifybyconnectorid; }
            set { _verifybyconnectorid = value; }
        }

        /// <summary>
        /// Determina en que orden se hacen las operaciones:
        ///     1 - Solo Local
        ///     2 - Primero Local
        ///     3 - Solo Remoto/s
        /// </summary>
        public int OperationOrder
        {
            get { return _operationOrder; }
            set { _operationOrder = value; }
        }

        /// <summary>
        /// Determina si se graba en BD una copia de cada muestra verificada
        /// Valores Posibles:
        ///     0-Default del Config
        ///     1-Save Data
        ///     2-No Save Data
        /// </summary>
        public int SaveVerified
        {
            get { return _saveVerified; }
            set { _saveVerified = value; }
        }

        /// <summary>
        /// Determina si se hace enroll ante un matching positivo de una fuente externa
        /// Valores Posibles:
        ///     0-Default del Config
        ///     1-Enroll
        ///     2-No Enroll
        /// </summary>
        public int InsertOption
        {
            get { return _insertOption; }
            set { _insertOption = value; }
        }

        #endregion Public

        #region Implementation of ICloneable

        /// <summary>
        /// Crea un nuevo objeto copiado de la instancia actual.
        /// </summary>
        /// <returns>
        /// Nuevo objeto que es una copia de esta instancia.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public object Clone()
        {
            return MemberwiseClone();
        }


        #endregion
    }
}
