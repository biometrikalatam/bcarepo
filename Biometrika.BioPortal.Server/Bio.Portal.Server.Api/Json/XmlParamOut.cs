﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BioPortal.Server.Api.Json
{
    /// <summary>
    /// Codigo importado del Framework de TFS, sin version
    /// </summary>
    public class XmlParamOut : ICloneable
    {
        //private static readonly ILog LOG = LogManager.GetLogger(typeof(XmlParamOut));

        #region Private

        /// <summary>
        /// Mensaje de error si el retorno es =! 0
        /// </summary>
        private string _message;

        /// <summary>
        /// Track ID de la transacción, para enlazarlo con la transacción
        /// de negocios.
        /// </summary>
        private string _trackid;

        /// <summary>
        /// Timestamp d ela transacción realizada
        /// </summary>
        private DateTime _timestampend;



        /// <summary>
        /// Accion realizada
        /// Posibles Valores =
        ///      1-Verify
        ///      2-Identify
        ///      3-Enroll
        ///      4-Get
        ///      5-Modify
        ///      6-Delete
        ///      7-DeleteBir
        /// </summary>
        private int _actionid;

        /// <summary>
        ///Datos de una persona: nombre, apellido, foto, etc.
        ///Si no es por identify. Sino solo typeid y valueid.
        /// </summary>
        private PersonalData _personalData;

        /// <summary>
        ///Informacion sobre tipo de tecnologia utilizado para verificacion o identificacion.
        /// </summary>
        private int _authenticationfactor;
        /// <summary>
        ///Parte de cuerpo verificada. 
        /// </summary>
        private int _bodypart;
        /// <summary>
        ///Minucia utilizada en el matching
        /// </summary>
        private int _minutiaetype;

        /// <summary>
        /// Indica que tipo de matching se hizo en Identify o en Verify (cuando hay muchas muestras
        /// por parte de cuerpo o es con _bodypart = 0 o menor.
        /// Posibles valores:
        ///         1-First     => Primer match positivo
        ///         2-Best      => Mejor match
        /// </summary>
        private int _matchingtype;

        /// <summary>
        /// Es usado para enviar datos adicionales de la muestra, por ejemplo si es WSQ, 
        /// se envia W=256|H=256, o si es template NEC, se envia COEF=90. Esto debe ser 
        /// entendico por el Matcher correspondiente, o quien trate el parametro IN.
        /// </summary>
        private DynamicData _additionaldata;


        /// <summary>
        /// Es el umbral utilizado para el matcher si corresponde.
        /// </summary>
        private double _threshold;

        /// <summary>
        /// Es el resultado del matching obtenido, si hubo matcher. Sino 0.
        /// En caso de resultado no numericos, solo 1-Positivo o 0-Negativo
        /// </summary>
        private double _score;

        /// <summary>
        /// Resultado de la verifiacion de acuerdo al Score / Threshold [1-Positivo | 2-Negativo]
        /// </summary>
        private double _result;

        /// <summary>
        /// Id de Compañia para filtrar si está deshabilitada. 
        /// Si no viene informada o no existe, se rechaza transaccion
        /// </summary>
        private int _companyid;
        /// <summary>
        /// Usuario de la compañia companyid, para referencia.
        /// </summary>
        private int _userid;

        /// <summary>
        /// Id de Origen de la transacción. Si el origen no existe se rechaza la transacción.
        /// </summary>
        private int _origin;

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist 
        /// </summary>
        private string _clientid;
        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        private string _ipenduser;
        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        private string _enduser;

        /// <summary>
        /// En caso de matching en fuente externa de datos, el conector retorna los valores 
        /// de la forma key/value.
        /// </summary>
        private DynamicData _resultconnector;

        /// <summary>
        /// Devuelve si fue verificado de alguna forma en el enrolamiento
        /// </summary>
        private string _verificationsource;
        /// <summary>
        /// Indica id de compañia enroladora
        /// </summary>
        private int _companyidenroll;
        /// <summary>
        /// Indica id usuario enrolador
        /// </summary>
        private int _useridenroll;

        #endregion Private

        #region Public

        /// <summary>
        /// Mensaje de error si el retorno es =! 0
        /// </summary>
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        /// <summary>
        /// Track ID de la transacción, para enlazarlo con la transacción
        /// de negocios.
        /// </summary>
        public string Trackid
        {
            get { return _trackid; }
            set { _trackid = value; }
        }

        /// <summary>
        /// Timestamp d ela transacción realizada
        /// </summary>
        public DateTime Timestampend
        {
            get { return _timestampend; }
            set { _timestampend = value; }
        }

        /// <summary>
        /// Accion realizada
        /// Posibles Valores =
        ///      1-Verify
        ///      2-Identify
        ///      3-Enroll
        ///      4-Get
        ///      5-Modify
        ///      6-Delete
        ///      7-DeleteBir
        /// </summary>
        public int Actionid
        {
            get { return _actionid; }
            set { _actionid = value; }
        }

        /// <summary>
        ///Datos de una persona: nombre, apellido, foto, etc.
        ///Si no es por identify. Sino solo typeid y valueid.
        /// </summary>
        public PersonalData PersonalData
        {
            get { return _personalData; }
            set { _personalData = value; }
        }

        /// <summary>
        ///Informacion sobre tipo de tecnologia utilizado para verificacion o identificacion.
        /// </summary>
        public int Authenticationfactor
        {
            get { return _authenticationfactor; }
            set { _authenticationfactor = value; }
        }

        /// <summary>
        ///Parte de cuerpo verificada. 
        /// </summary>
        public int Bodypart
        {
            get { return _bodypart; }
            set { _bodypart = value; }
        }

        /// <summary>
        ///Minucia utilizada en el matching
        /// </summary>
        public int Minutiaetype
        {
            get { return _minutiaetype; }
            set { _minutiaetype = value; }
        }

        /// <summary>
        /// Indica que tipo de matching se hizo en Identify o en Verify (cuando hay muchas muestras
        /// por parte de cuerpo o es con _bodypart = 0 o menor.
        /// Posibles valores:
        ///         1-First     => Primer match positivo
        ///         2-Best      => Mejor match
        /// </summary>
        public int Matchingtype
        {
            get { return _matchingtype; }
            set { _matchingtype = value; }
        }

        /// <summary>
        /// Es usado para enviar datos adicionales de la muestra, por ejemplo si es WSQ, 
        /// se envia W=256|H=256, o si es template NEC, se envia COEF=90. Esto debe ser 
        /// entendico por el Matcher correspondiente, o quien trate el parametro IN.
        /// </summary>
        public DynamicData Additionaldata
        {
            get { return _additionaldata; }
            set { _additionaldata = value; }
        }

        /// <summary>
        /// Es el umbral utilizado para el matcher si corresponde.
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Es el resultado del matching obtenido, si hubo matcher. Sino 0.
        /// En caso de resultado no numericos, solo 1-Positivo o 0-Negativo
        /// </summary>
        public double Score
        {
            get { return _score; }
            set { _score = value; }
        }

        /// <summary>
        /// Resultado de la verifiacion de acuerdo al Score / Threshold [1-Positivo | 2-Negativo]
        /// </summary>
        public double Result
        {
            get { return _result; }
            set { _result = value; }
        }

        /// <summary>
        /// Id de Compañia para filtrar si está deshabilitada. 
        /// Si no viene informada o no existe, se rechaza transaccion
        /// </summary>
        public int Companyid
        {
            get { return _companyid; }
            set { _companyid = value; }
        }

        /// <summary>
        /// Usuario de la compañia companyid, para referencia.
        /// </summary>
        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }
        }

        /// <summary>
        /// Id de Origen de la transacción. Si el origen no existe se rechaza la transacción.
        /// </summary>
        public int Origin
        {
            get { return _origin; }
            set { _origin = value; }
        }

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist 
        /// </summary>
        public string Clientid
        {
            get { return _clientid; }
            set { _clientid = value; }
        }

        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        public string Ipenduser
        {
            get { return _ipenduser; }
            set { _ipenduser = value; }
        }

        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        public string Enduser
        {
            get { return _enduser; }
            set { _enduser = value; }
        }

        /// <summary>
        /// En caso de matching en fuente externa de datos, el conector retorna los valores 
        /// de la forma key/value.
        /// </summary>
        public DynamicData Resultconnector
        {
            get { return _resultconnector; }
            set { _resultconnector = value; }
        }

        /// <summary>
        /// Devuelve si fue verificado de alguna forma en el enrolamiento
        /// </summary>
        public string Verificationsource
        {
            get { return _verificationsource; }
            set { _verificationsource = value; }
        }

        /// <summary>
        /// Indica id de compañia enroladora
        /// </summary>
        public int Companyidenroll
        {
            get { return _companyidenroll; }
            set { _companyidenroll = value; }
        }

        /// <summary>
        /// Indica id usuario enrolador
        /// </summary>
        public int Useridenroll
        {
            get { return _useridenroll; }
            set { _useridenroll = value; }
        }

        public int ExecutionResult { get; set; }

        #endregion Public

        #region Static Public

        static public string GetXML(int action, string trackid, int authenticationfactor, int minutiaetype, int bodypart,
                             int result, double score, double threshold, int matchingtype, string clientid,
                             string ipenduser, string enduser, int companyid, int userid, int companyidenroll,
                             int useridenroll, string typeid, string valueid, DateTime end, int originid,
                             DynamicData resultconnector, string verificationsource)
        {
            XmlParamOut paramout = new XmlParamOut();
            string xmlret = null;
            try
            {
                paramout.Actionid = action;
                paramout.Trackid = trackid;
                paramout.Authenticationfactor = authenticationfactor;
                paramout.Minutiaetype = minutiaetype;
                paramout.Bodypart = bodypart;
                paramout.Result = result;
                paramout.Score = score;
                paramout.Threshold = threshold;
                paramout.Matchingtype = matchingtype;
                paramout.Clientid = clientid;
                paramout.Ipenduser = ipenduser;
                paramout.Enduser = enduser;
                paramout.Companyid = companyid;
                paramout.Userid = userid;
                paramout.Companyidenroll = companyidenroll;
                paramout.Useridenroll = useridenroll;
                paramout.PersonalData = new PersonalData();
                paramout.PersonalData.Typeid = typeid;
                paramout.PersonalData.Valueid = valueid;
                paramout.Timestampend = end;
                paramout.Origin = originid;
                paramout.Resultconnector = resultconnector;
                paramout.Verificationsource = verificationsource;
                //Serializo
                xmlret = XmlUtils.SerializeObject<XmlParamOut>(paramout);
            }
            catch (Exception ex)
            {
                //LOG.Error("XmlParamOut.GetXML Exception", ex);
            }
            return xmlret;
        }

        static public string GetXML(int action, string trackid, int authenticationfactor, int minutiaetype, int bodypart,
                     int result, double score, double threshold, int matchingtype, string clientid,
                     string ipenduser, string enduser, int companyid, int userid, int companyidenroll,
                     int useridenroll, PersonalData pd, DateTime end, int originid,
                     DynamicData resultconnector, string verificationsource, string abs)
        {
            XmlParamOut paramout = new XmlParamOut();
            string xmlret = null;
            try
            {
                paramout.Actionid = action;
                paramout.Trackid = trackid;
                paramout.Authenticationfactor = authenticationfactor;
                paramout.Minutiaetype = minutiaetype;
                paramout.Bodypart = bodypart;
                paramout.Result = result;
                paramout.Score = score;
                paramout.Threshold = threshold;
                paramout.Matchingtype = matchingtype;
                paramout.Clientid = clientid;
                paramout.Ipenduser = ipenduser;
                paramout.Enduser = enduser;
                paramout.Companyid = companyid;
                paramout.Userid = userid;
                paramout.Companyidenroll = companyidenroll;
                paramout.Useridenroll = useridenroll;
                paramout.PersonalData = pd;
                paramout.Timestampend = end;
                paramout.Origin = originid;
                paramout.Resultconnector = resultconnector;
                paramout.Verificationsource = verificationsource;
                if (!String.IsNullOrEmpty(abs))
                {
                    DynamicData adata = new DynamicData();
                    adata.AddValue("BioSignature", abs);
                    paramout.Additionaldata = adata;
                }
                //Serializo
                xmlret = XmlUtils.SerializeObject<XmlParamOut>(paramout);
            }
            catch (Exception ex)
            {
                //LOG.Error("XmlParamOut.GetXML Exception", ex);
            }
            return xmlret;
        }

        static public string GetXML(int action, string trackid, int authenticationfactor, int minutiaetype, int bodypart,
             int result, double score, double threshold, int matchingtype, string clientid,
             string ipenduser, string enduser, int companyid, int userid, int companyidenroll,
             int useridenroll, PersonalData pd, DateTime end, int originid,
             DynamicData resultconnector, string verificationsource, DynamicData infobirs)
        {
            XmlParamOut paramout = new XmlParamOut();
            string xmlret = null;
            try
            {
                paramout.Actionid = action;
                paramout.Trackid = trackid;
                paramout.Authenticationfactor = authenticationfactor;
                paramout.Minutiaetype = minutiaetype;
                paramout.Bodypart = bodypart;
                paramout.Result = result;
                paramout.Score = score;
                paramout.Threshold = threshold;
                paramout.Matchingtype = matchingtype;
                paramout.Clientid = clientid;
                paramout.Ipenduser = ipenduser;
                paramout.Enduser = enduser;
                paramout.Companyid = companyid;
                paramout.Userid = userid;
                paramout.Companyidenroll = companyidenroll;
                paramout.Useridenroll = useridenroll;
                paramout.PersonalData = pd;
                paramout.Timestampend = end;
                paramout.Origin = originid;
                paramout.Resultconnector = resultconnector;
                paramout.Verificationsource = verificationsource;
                paramout.Additionaldata = infobirs;
                //Serializo
                xmlret = XmlUtils.SerializeObject<XmlParamOut>(paramout);
            }
            catch (Exception ex)
            {
                //LOG.Error("XmlParamOut.GetXML Exception", ex);
            }
            return xmlret;
        }

        static public string GetXML(int action, string trackid, string clientid, string ipenduser,
            string enduser, int companyid, int userid, DateTime end, int originid, DynamicData additionaldata)
        {
            XmlParamOut paramout = new XmlParamOut();
            string xmlret = null;
            try
            {
                paramout.Actionid = action;
                paramout.Trackid = trackid;
                paramout.Clientid = clientid;
                paramout.Ipenduser = ipenduser;
                paramout.Enduser = enduser;
                paramout.Companyid = companyid;
                paramout.Userid = userid;
                paramout.Timestampend = end;
                paramout.Origin = originid;
                paramout.Additionaldata = additionaldata;
                //Serializo
                xmlret = XmlUtils.SerializeObject<XmlParamOut>(paramout);
            }
            catch (Exception ex)
            {
                //LOG.Error("XmlParamOut.GetXML Exception", ex);
            }
            return xmlret;
        }
        #endregion Static Public

        #region Implementation of ICloneable

        /// <summary>
        /// Crea un nuevo objeto copiado de la instancia actual.
        /// </summary>
        /// <returns>
        /// Nuevo objeto que es una copia de esta instancia.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public object Clone()
        {
            return MemberwiseClone();
        }

        #endregion
    }
}
