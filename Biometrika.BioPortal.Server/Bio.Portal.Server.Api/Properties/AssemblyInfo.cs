﻿using log4net.Config;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Bio.Portal.Server.Api")]
[assembly: AssemblyDescription("Api Helper de conexión a los web services de BioPortal Server")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Biometrika")]
[assembly: AssemblyProduct("Bio.Portal.Server.Api")]
[assembly: AssemblyCopyright("Copyright ©  Biometrika 2010")]
[assembly: AssemblyTrademark("Biometrika")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("5c73d96a-9423-4cdc-ab7e-bb5a15f06821")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("5.1.1.0")]
[assembly: AssemblyFileVersion("5.1.1.0")]
//
//
// Assembly para log4net
[assembly: XmlConfigurator(Watch = true)]
