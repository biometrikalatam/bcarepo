﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioPortal.Server.Api.Model
{
    public class Model
    {
    }

    //Added 08-2021 - Para unificar la respuesta de APIs
    public class BPResponse
    {
        public int Code { get; set; }       //Codigo de error: 0 - Sin Error | <0 - Codigo de error
        public string Message { get; set; } //Descripcion de error si hubo
        public object Data { get; set; }    //Respuesta Json segun operacion

    }

    public class POSTxCreateIn
    {
        public int type { get; set; }
        public int authenticationfactor { get; set; }
        public int minutiaetype { get; set; }
        public string clientid { get; set; }
        //public string qr { get; set; }
        //public string sample { get; set; }
        //public int minutiaetype { get; set; }
    }

    public class POSTxCreateOutData
    {
        public string trackid { get; set; }
        //public string sample { get; set; }

    }

    public class POSVerifyIn
    {
        public string clientid { get; set; }
        public string trackid { get; set; }
        public int resultmoc { get; set; }
        public string barcode { get; set; }
        public string sample { get; set; }
        public int bodypart { get; set; }
        public int minutiaetype { get; set; }
        public double threshold { get; set; }
        public PersonalData perdonaldata { get; set; }
    }

    public class PersonalData
    {
        public string valueid { get; set; }
        public string serialnumber { get; set; }
        public string name { get; set; }
        public string phaterlastname { get; set; }
        public string motherlastname { get; set; }
        public string expirationdate { get; set; }
        public string birthdate { get; set; }
        public string sex { get; set; }
        public string nationality { get; set; }
        public string photo { get; set; }
        public string signature { get; set; }

    }

    public class POSVerifyOutData
    {
        public int result { get; set; }
        public double score { get; set; }
        public PersonalData perdonaldata { get; set; }
    }

    public class PDF417
    {
        public string valueid { get; set; }
        public string lastname { get; set; }
        public string nationality { get; set; }
        public string expirationdate { get; set; }
        public string serialnumber { get; set; }
        public string finger { get; set; }
    }

    //Facial - 09-08-2022

    public class POSVerifyFacialIn
    {
        public string valueid { get; set; }
        public string clientid { get; set; }
        public string frontcardid { get; set; }
        public string backcardid { get; set; }
        public string selfie { get; set; }
        public double threshold { get; set; }
    }

    public class POSVerifyFacialOutData
    {
        public string trackid { get; set; }
        public int result { get; set; }
        public double score { get; set; }
        public double threshold { get; set; }
        public Bio.Core.Api.PersonalData personaldata { get; set; }
    }
}
