﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bio.Portal.Server.Api.Constant
{
    public class BodyPart
    {

        static public int BODYPART_ANY = -2;
        static public string SBODYPART_ANY = "Cualquiera";

        static public int BODYPART_ALL = -1;
        static public string SBODYPART_ALL = "Todos";

        static public int BODYPART_NONE = 0;
        static public string SBODYPART_NONE = "Ninguno";

        //Dedos
        static public int BODYPART_DEDOPULGARDERECHO = 1;
        static public string SBODYPART_DEDOPULGARDERECHO = "Dedo Pulgar Derecho";
        static public int BODYPART_DEDOINDICEDERECHO = 2;
        static public string SBODYPART_DEDOINDICEDERECHO = "Dedo Indice Derecho";
        static public int BODYPART_DEDOMAYORDERECHO = 3;
        static public string SBODYPART_DEDOMAYORDERECHO = "Dedo Mayor Derecho";
        static public int BODYPART_DEDOANULARDERECHO = 4;
        static public string SBODYPART_DEDOANULARDERECHO = "Dedo Anular Derecho";
        static public int BODYPART_DEDOMENIQUEDERECHO = 5;
        static public string SBODYPART_DEDOMENIQUEDERECHO = "Dedo Menique Derecho";
        static public int BODYPART_DEDOPULGARIZQUIERDO = 6;
        static public string SBODYPART_DEDOPULGARIZQUIERDO = "Dedo Pulgar Izquierdo";
        static public int BODYPART_DEDOINDICEIZQUIERDO = 7;
        static public string SBODYPART_DEDOINDICEIZQUIERDO = "Dedo Indice Izquierdo";
        static public int BODYPART_DEDOMAYORIZQUIERDO = 8;
        static public string SBODYPART_DEDOMAYORIZQUIERDO = "Dedo Mayor Izquierdo";
        static public int BODYPART_DEDOANULARIZQUIERDO = 9;
        static public string SBODYPART_DEDOANULARIZQUIERDO = "Dedo Anular Izquierdo";
        static public int BODYPART_DEDOMENIQUEIZQUIERDO = 10;
        static public string SBODYPART_DEDOMENIQUEIZQUIERDO = "Dedo Menique Izquierdo";
        //Manos
        static public int BODYPART_MANODERECHA = 11;
        static public string SBODYPART_MANODERECHA = "Mano Izquierda";
        static public int BODYPART_MANOIZQUIERDA = 12;
        static public string SBODYPART_MANOIZQUIERDA = "Mano Derecha";
        //Ojos
        static public int BODYPART_OJODERECHO = 13;
        static public string SBODYPART_OJODERECHO = "Ojo Derecho";
        static public int BODYPART_OJOIZQUIERDO = 14;
        static public string SBODYPART_OJOIZQUIERDO = "Ojo Izquierdo";
        //Voz
        static public int BODYPART_VOZ = 15;
        static public string SBODYPART_VOZ = "Voz";
        //Face
        static public int BODYPART_FACE = 16;
        static public string SBODYPART_FACE = "Cara";
        

        public static string GetName(int bp)
        {
            string ret = null;
            switch (bp)
            {
                case -2:
                    ret = "Cualquiera";
                    break;
                case -1:
                    ret = "Todos";
                    break;
                case 0:
                    ret = "Ninguno";
                    break;
                case 1:
                    ret = "Dedo Pulgar Derecho";
                    break;
                case 2:
                    ret = "Dedo Indice Derecho";
                    break;
                case 3:
                    ret = "Dedo Mayor Derecho";
                    break;
                case 4:
                    ret = "Dedo Anular Derecho";
                    break;
                case 5:
                    ret = "Dedo Menique Derecho";
                    break;
                case 6:
                    ret = "Dedo Pulgar Izquierdo";
                    break;
                case 7:
                    ret = "Dedo Indice Izquierdo";
                    break;
                case 8:
                    ret = "Dedo Mayor Izquierdo";
                    break;
                case 9:
                    ret = "Dedo Anular Izquierdo";
                    break;
                case 10:
                    ret = "Dedo Menique Izquierdo";
                    break;
                case 11:
                    ret = "Mano Derecha";
                    break;
                case 12:
                    ret = "Mano Izquierda";
                    break;
                case 13:
                    ret = "Ojo Derecho";
                    break;
                case 14:
                    ret = "Ojo Izquierdo";
                    break;
                case 15:
                    ret = "Voz";
                    break;
                case 16:
                    ret = "Cara";
                    break;
                default:
                    ret = "Bodypart no documentado";
                    break;

            }
            return ret;
        }
    }
}
