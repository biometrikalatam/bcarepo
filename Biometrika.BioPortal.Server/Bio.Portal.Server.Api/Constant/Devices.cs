﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bio.Portal.Server.Api.Constant
{
    public class Devices
    {
        static public int DEVICE_ANY = -2;
        static public string SDEVICE_ANY = "Cualquiera";

        static public int DEVICE_ALL = -1;
        static public string SDEVICE_ALL = "Todos";

        static public int DEVICE_NONE = 0;
        static public string SDEVICE_NONE = "Ninguno";

        static public int DEVICE_DIGITALPERSONA = 1;
        static public string SDEVICE_DIGITALPERSONA = "Digital Persona";

        static public int DEVICE_SECUGEN = 2;
        static public string SDEVICE_SECUGEN = "Secugen";

        static public int DEVICE_CROSSMATCH = 3;
        static public string SDEVICE_CROSSMATCH = "Crossmatch";

        static public int DEVICE_TESTECH = 4;
        static public string SDEVICE_TESTECH = "Testech";

        static public int DEVICE_SAGEM = 5;
        static public string SDEVICE_SAGEM = "Sagem";

        static public int DEVICE_IDENTIX = 6;
        static public string SDEVICE_IDENTIX = "Identix";

        static public int DEVICE_UPEK = 7;
        static public string SDEVICE_UPEK = "Upek";

        static public int DEVICE_COGENT = 8;
        static public string SDEVICE_COGENT = "Cogent";


        static public int DEVICE_HANDKEY = 30;
        static public string SDEVICE_HANDKEY = "Handkey";

        static public int DEVICE_FACIAL = 40;
        static public string SDEVICE_FACIAL = "Facial";

        static public int DEVICE_IRIS = 50;
        static public string SDEVICE_IRIS = "Iris";

        static public int DEVICE_RETINA = 60;
        static public string SDEVICE_RETINA = "Retina";

        static string GetDescription(int DEVICE)
        {
            switch (DEVICE)
            {
                case -2:
                    return Devices.SDEVICE_ANY;

                case -1:
                    return Devices.SDEVICE_ALL;

                case 0:
                    return Devices.SDEVICE_NONE;

                case 1:
                    return Devices.SDEVICE_DIGITALPERSONA;

                case 2:
                    return Devices.SDEVICE_SECUGEN;

                case 3:
                    return Devices.SDEVICE_CROSSMATCH;

                case 4:
                    return Devices.SDEVICE_TESTECH;

                case 5:
                    return Devices.SDEVICE_SAGEM;

                case 6:
                    return Devices.SDEVICE_IDENTIX;

                case 7:
                    return Devices.SDEVICE_UPEK;

                case 8:
                    return Devices.SDEVICE_COGENT;

                case 30:
                    return Devices.SDEVICE_HANDKEY;

                case 40:
                    return Devices.SDEVICE_FACIAL;

                case 50:
                    return Devices.SDEVICE_IRIS;

                case 60:
                    return Devices.SDEVICE_RETINA;

                default:
                    return "Device no documentado";
            }
        }
    }
}
