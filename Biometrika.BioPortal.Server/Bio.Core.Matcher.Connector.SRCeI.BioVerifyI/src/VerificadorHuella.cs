using System;
//using BCI.SRCeI;
//using BCI.Bio.Portal;
//using BCI.SRCeI;
//using Bio.Core.Wsq.Encoder;
//using Bio.Imaging;
//using Bio.Wsq.Encoder;
using log4net;
//using BCI.SRCeI;

namespace Bio.Core.Matcher.Connector.SRCeI.BioVerifyI
{
	/// <summary>
	/// Summary description for VerificacionHuella.
	/// </summary>
	public class VerificadorHuella
    {
        /// <summary>
        /// Generaci�n de variable est�tica para escritura de Log, de acuerdo
        /// a la documentaci�n de log4net.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(typeof(VerificadorHuella));
	    
// #region Verficacion Remota via Directa SRCeI

//        private static Encabezado CrearEncabezado()
//        {
//            Encabezado enc = new Encabezado();
//            enc.Empresa = int.Parse(BCI.Properties.Settings.Default.IdEmpresaSRCeI);
//            enc.Usuario = BCI.Properties.Settings.Default.UserSRCeI;
//            enc.Clave = BCI.Properties.Settings.Default.ClaveSRCeI;
//                //El id tx esta generado desde la fecha, como:
//                // 0808110940 = 2008 08 11 09 40 = a�o mes dia hora(24H format) minutos
//                //con esto aseguro no repetir y me da coherencia para seguimientos contra 
//                //SRCeI porque desde fecha de transaccion puedo sacar el Id Tx y con eso
//                //consultar en SRCeI.
//            enc.IdTransaccion = int.Parse(DateTime.Now.ToString("yyMMddHHmm"));

//            enc.IpEstacionUsuariofinal = " 172.16.1.32"; // BCI.MainForm.sIP;
//            enc.UsuarioFinal = "Test"; // BCI.MainForm.sEstacion;
//            return enc;
//        }

//        private static AFISImpresionDactilarParametro CrearParametros(RUN run, int dedo,
//                                                                      byte[] wsq)
//        {
//            AFISImpresionDactilarParametro param = new AFISImpresionDactilarParametro();
//            param.RUN = run.Mantisa;
//            param.NumeroDedo = dedo;
//            param.ImagenImpresionDactilar = new Datos();
//            param.ImagenImpresionDactilar.Formato = "WSQ";
//            param.ImagenImpresionDactilar.Imagen = wsq;
//            return param;
//        }

//        public static VerifyResult VerificacionWsqToSRCeI(RUN run, int dedo, 
//                                                             byte[] raw, int width, int height)
//        {
//            VerifyResult rta = new BCI.Bio.Portal.BISVerifyResult();

            
//            //Esto lo agrego para cuando no tengo conexion para poder debuguear. Luego comentarlo.
//            //rta.Cod_rta = "COD_OK";
//            //rta.Mensaje = "S/C";
//            //rta.CodigoRta = 0;

//            //rta.Fecha = FormatFechaHoraSRCeI("20090928100102");
//            //rta.BodyPartValidado = 1;
//            //rta.Idtrackbis = "N/A";
//            //rta.Idtrackrc = "1234567890";

//            //rta.IPValidada = BCI.MainForm.sIP;
//            //rta.NroIdValidado = "21284415-2";
//            //rta.TipoIdValidado = "RUT";
//            //rta.SerialIdValidado = "N/A";

//            //rta.Resultado = 1;
//            //rta.Score = 9999;
//            //return rta;
            
//            try
//            {
//                //byte[] rawNorm = RawImageManipulator.Adjust(raw, width, height, 512, 512);
//                WsqEncoder encoder = new WsqEncoder();
//                byte[] wsq;
//                encoder.EncodeMemory(raw, 512, 512, out wsq);
//                //string swsq = Convert.ToBase64String(wsq);

//                Encabezado enc = CrearEncabezado();
//                AFISImpresionDactilarParametro param = CrearParametros(run, dedo, wsq);
                
//                using (DS64 ws_SRCeI = new DS64())
//                {
//                    ws_SRCeI.Timeout = BCI.MainForm.timeoutWSBIS;
//                    estructuraafis respVerify;
//                    EncabezadoRespuesta respEncabezado =
//                        ws_SRCeI.AFISImpresionDactilarDS64(enc, param, out respVerify);
                    
//                        //0: Ok
//                        //1: Usuario no v�lido (case sensitive)
//                        //2: XML no v�lido
//                        //3: No existen registros o no hay imagen digitalizada
//                        //4:Calidad no adecuada para efectuar AFIS
//                    if (respEncabezado != null && respEncabezado.Estado == 0)
//                    {
//                        rta.Cod_rta = "COD_OK";
//                        rta.Mensaje = "S/C";
//                        rta.CodigoRta = respEncabezado.Estado.Value;

//                        rta.Fecha = FormatFechaHoraSRCeI(respEncabezado.FechaHoraOperacion);
//                        rta.BodyPartValidado = dedo;
//                        rta.Idtrackbis = "N/A";
//                        rta.Idtrackrc = respEncabezado.IdOperacion.ToString();

//                        rta.IPValidada = BCI.MainForm.sIP;
//                        rta.NroIdValidado = run.Mantisa.ToString() + "-" + run.Dv.ToString();
//                        rta.TipoIdValidado = "RUT";
//                        rta.SerialIdValidado = "N/A";
                        
//                        rta.Resultado = respVerify.decision;
//                        rta.Score = respVerify.puntajeafis;
//                    } else  //Error en SRCeI o directametne null 
//                    {
//                        if (respEncabezado == null)
//                        {
//                            rta.Cod_rta = "COD_ERR_PROBLEMAS_EN_CONX_SERVICIO_RC";
//                            rta.Mensaje = "Respuesta Nula";
//                        } else
//                        {
//                            rta = GeneraRtaFromEnc(respEncabezado);
//                            rta.CodigoRta = respEncabezado.Estado.Value;
//                            rta.Fecha = FormatFechaHoraSRCeI(respEncabezado.FechaHoraOperacion);
//                            rta.BodyPartValidado = dedo;
//                            rta.Idtrackbis = null;
//                            rta.Idtrackrc = respEncabezado.IdOperacion.ToString();

//                            rta.IPValidada = BCI.MainForm.sIP;
//                            rta.NroIdValidado = run.Mantisa.ToString() + "-" + run.Dv.ToString();
//                            rta.TipoIdValidado = "RUT";
//                            rta.SerialIdValidado = null;
//                            if (respVerify != null)
//                            {
//                                rta.Resultado = respVerify.decision;
//                                rta.Score = respVerify.puntajeafis;
//                            }
//                        }
//                    }
//                }
               
//            }
//            catch (Exception ex)
//            {
//                log.Error("VerificadorHuella.VerificacionWsqToSRCeI", ex);
//                rta.Cod_rta = "COD_ERR_DESCONOCIDO";
//                rta.Mensaje = ex.Message;
//            }
//            return rta;
//        }

//        private static string FormatFechaHoraSRCeI(string fechaoperacion)
//        {
//            string strRet = "";
//            try
//            {
//                strRet = fechaoperacion.Substring(6, 2) + "/" +
//                         fechaoperacion.Substring(4, 2) + "/" +
//                         fechaoperacion.Substring(0, 4) + " " +
//                         fechaoperacion.Substring(8, 2) + ":" +
//                         fechaoperacion.Substring(10, 2) + ":" +
//                         fechaoperacion.Substring(12, 2);
//            } catch
//            {
//                strRet = "";
//            }
//            return strRet;
//        }

//        private static VerifyResult GeneraRtaFromEnc(EncabezadoRespuesta encabezado)
//        {
//            BISVerifyResult rta = new BISVerifyResult();
//            switch(encabezado.Estado.Value)
//            {
//                case 1:
//                    rta.CodigoRta = 1;
//                    rta.Cod_rta = "COD_ERR_USUARIO_NO_VALIDO_EN_RC";
//                    rta.Mensaje = "Usuario no valido en SRCeI";
//                    break;
//                case 2:
//                    rta.CodigoRta = 2;
//                    rta.Cod_rta = "COD_ERR_USUARIO_NO_VALIDO_EN_RC";
//                    rta.Mensaje = "Usuario no valido en SRCeI";
//                    break;
//                case 3:
//                    rta.CodigoRta = 3;
//                    rta.Cod_rta = "COD_ERR_NO_EXISTEN_DATOS_EN_RC";
//                    rta.Mensaje = "No existen datos para validar en SRCeI";
//                    break;
//                case 4:
//                    rta.CodigoRta = 4;
//                    rta.Cod_rta = "COD_ERR_IMAGEN_MALA_CALIDAD_EN_RC";
//                    rta.Mensaje = "Imagen de baja calidad para validar en SRCeI";
//                    break;
//                default:
//                    rta.CodigoRta = 5;
//                    rta.Cod_rta = "COD_ERR_DESCONOCIDO";
//                    rta.Mensaje = "Codigo inexistente";
//                    break;
//            }
//            return rta;
//        }
   }
//#endregion Verficacion Remota via Directa SRCeI

}
