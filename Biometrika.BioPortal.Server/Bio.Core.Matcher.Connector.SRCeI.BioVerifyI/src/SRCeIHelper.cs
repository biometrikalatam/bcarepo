﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Matcher.Connector.SRCeI.BioVerifyI.SRCeI;
using log4net;

namespace Bio.Core.Matcher.Connector.SRCeI.BioVerifyI
{

    internal class SRCeIHelper
    {
        /// <summary>
        /// Generación de variable estática para escritura de Log, de acuerdo
        /// a la documentación de log4net.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(typeof(SRCeIHelper));


        internal static Encabezado CrearEncabezado(int idEmpresa, string user, string clave, string ip)
        {
            Encabezado enc = new Encabezado();
            enc.Empresa = idEmpresa;
            enc.Usuario = user;
            enc.Clave = clave;
            //El id tx esta generado desde la fecha, como:
            // 0808110940 = 2008 08 11 09 40 = año mes dia hora(24H format) minutos
            //con esto aseguro no repetir y me da coherencia para seguimientos contra 
            //SRCeI porque desde fecha de transaccion puedo sacar el Id Tx y con eso
            //consultar en SRCeI.
            enc.IdTransaccion = int.Parse(DateTime.Now.ToString("yyMMddHHmm"));

            enc.IpEstacionUsuariofinal = ip; // " 172.16.1.32"; // BCI.MainForm.sIP;
            enc.UsuarioFinal = user; // "Test"; // BCI.MainForm.sEstacion;
            return enc;
        }

        internal static AFISImpresionDactilarParametro CrearParametros(RUN run, int dedo, byte[] wsq)
        {
            AFISImpresionDactilarParametro param = new AFISImpresionDactilarParametro();
            param.RUN = run.Mantisa;
            param.NumeroDedo = dedo;
            param.ImagenImpresionDactilar = new Datos();
            param.ImagenImpresionDactilar.Formato = "WSQ";
            param.ImagenImpresionDactilar.Imagen = wsq;
            return param;
        }

    }
}
