﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Api;
using Bio.Core.Api.Matcher.Interface;
using Bio.Core.Constant;
using Bio.Core.Matcher.Connector.SRCeI.BioVerifyI.SRCeI;
using Bio.Core.Wsq.Encoder;
using BioPortal.Server.Api;
using log4net;

namespace Bio.Core.Matcher.Connector.SRCeI.BioVerifyI
{
    public class Connector : IConnector
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Connector));

#region Private properties propietario
        
        private string _urlWS;
        private int _timeout = 30000;
        private string _idEmpresaSRCeI;
        private string _userSRCeI;
        private string _claveSRCeI;
        private string _ip;
        private int _threshold = 750;
        private bool _isConfigured; //Solo para control en initialization

#endregion Private properties propietario

#region Public properties propietario

        public string UrlWs
        {
            get { return _urlWS; }
            set { _urlWS = value; }
        }

        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        public string IdEmpresaSRCeI
        {
            get { return _idEmpresaSRCeI; }
            set { _idEmpresaSRCeI = value; }
        }

        public string UserSRCeI
        {
            get { return _userSRCeI; }
            set { _userSRCeI = value; }
        }

        public string ClaveSRCeI
        {
            get { return _claveSRCeI; }
            set { _claveSRCeI = value; }
        }

        public string Ip
        {
            get { return _ip; }
            set { _ip = value; }
        }

        public int Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        #endregion Public properties propietario

#region Private Method

        /// <summary>
        /// Inicializa las variables del objeto desde config, para no perder tiempo en buquedas luego
        /// </summary>
        private void Initialization()
        {
            try
            {
                if (_config == null || _config.DynamicDataItems == null)
                {
                    _isConfigured = false;
                    return;
                }

                foreach (DynamicDataItem dd in _config.DynamicDataItems)
                {
                    if (dd.key.Trim().Equals("UrlWS"))
                    {
                        _urlWS = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("Timeout"))
                    {
                        try
                        {
                            _timeout = Convert.ToInt32(dd.value.Trim());
                        }
                        catch (Exception ex)
                        {
                            _timeout = 30000;
                        }
                    }
                    if (dd.key.Trim().Equals("IdEmpresaSRCeI"))
                    {
                        _idEmpresaSRCeI = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("UserSRCeI"))
                    {
                        _userSRCeI = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("ClaveSRCeI"))
                    {
                        _claveSRCeI = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("Threshold"))
                    {
                        _threshold = Convert.ToInt32(dd.value.Trim());
                    }
                }
                _ip = BioVerifyI.Utiles.GetIP();
                _isConfigured = true;
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Connector.SRCeI.BioVerifyI Initialization Error", ex);
                _isConfigured = false;
            }
        }

#endregion Private Method

#region Implementation of IConnector

        private string _connectorId;

        private DynamicData _config;

        /// <summary>
        /// Id identificador del conector
        /// </summary>
        public string ConnectorId
        {
            get { return _connectorId; }
            set { _connectorId = value; }
        }

        /// <summary>
        /// Pares de key/value de configuracion para el conector
        /// </summary>
        public DynamicData Config
        {
            get { return _config; }
            set
            {
                 _config = value;
                 if (!_isConfigured) Initialization();
            }
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */
            
            string msg;
            string xmloutbp;
            int iretremoto;

            DynamicData oXmlout = new DynamicData();
            oXmlout.AddValue("message", "");
            oXmlout.AddValue("trackid", "");
            oXmlout.AddValue("status", "");
            oXmlout.AddValue("result", "");
            oXmlout.AddValue("score", "");
            oXmlout.AddValue("threshold", "");
            oXmlout.AddValue("timestamp", "");


            //oXmlout.SetValue("message", "");
            //oXmlout.SetValue("trackid", "283734647");
            //oXmlout.SetValue("status", "0");
            //oXmlout.SetValue("result", "1");
            //oXmlout.SetValue("score", "1000");
            //oXmlout.SetValue("threshold", _threshold.ToString());
            //oXmlout.SetValue("timestamp", FormatFechaHoraSRCeI("23112013100000"));
            //xmloutput = DynamicData.SerializeToXml(oXmlout);
            //return Errors.IERR_OK;

            try
            {
                if (!this._isConfigured)
                {
                    msg = "Bio.Core.Matcher.SRCeI.BioVerifyI Error = Connector No Configurado!";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
                }


                //1.-Deserializo parametros de entrada
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlinput);
                if (oXmlIn == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.BioVerifyI Error deserealizando xmlinput";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //Tomo el WSQ si es que viene
                byte[] wsq = ExtraeWSQFromParamIn(oXmlIn);

                if (wsq != null)
                {
                    Encabezado enc = SRCeIHelper.CrearEncabezado(Convert.ToInt32(_idEmpresaSRCeI), _userSRCeI,
                                                                    _claveSRCeI, _ip);
                    AFISImpresionDactilarParametro param =
                        SRCeIHelper.CrearParametros(RUN.Convertir(oXmlIn.PersonalData.Valueid),
                                                                        oXmlIn.Bodypart, wsq);

                    using (DS64 ws_SRCeI = new DS64())
                    {
                        ws_SRCeI.Timeout = _timeout;
                        estructuraafis respVerify;
                        EncabezadoRespuesta respEncabezado =
                            ws_SRCeI.AFISImpresionDactilarDS64(enc, param, out respVerify);

                        //0: Ok
                        //1: Usuario no válido (case sensitive)
                        //2: XML no válido
                        //3: No existen registros o no hay imagen digitalizada
                        //4:Calidad no adecuada para efectuar AFIS
                        if (respEncabezado != null && respEncabezado.Estado == 0)
                        {
                            oXmlout.SetValue("message", "");
                            oXmlout.AddValue("trackid", respEncabezado.IdOperacion.ToString());
                            oXmlout.AddValue("status", respEncabezado.Estado.Value.ToString());
                            oXmlout.AddValue("result", respVerify.puntajeafis >= _threshold ? "1" : "2");
                            oXmlout.AddValue("score", respVerify.puntajeafis.ToString());
                            oXmlout.AddValue("threshold", _threshold.ToString());
                            oXmlout.AddValue("timestamp", FormatFechaHoraSRCeI(respEncabezado.FechaHoraOperacion));
                            //rta.Cod_rta = "COD_OK";
                            //rta.Mensaje = "S/C";
                            //rta.CodigoRta = respEncabezado.Estado.Value;

                            //rta.Fecha = FormatFechaHoraSRCeI(respEncabezado.FechaHoraOperacion);
                            //rta.BodyPartValidado = dedo;
                            //rta.Idtrackbis = "N/A";
                            //rta.Idtrackrc = respEncabezado.IdOperacion.ToString();

                            //rta.IPValidada = BCI.MainForm.sIP;
                            //rta.NroIdValidado = run.Mantisa.ToString() + "-" + run.Dv.ToString();
                            //rta.TipoIdValidado = "RUT";
                            //rta.SerialIdValidado = "N/A";

                            //rta.Resultado = respVerify.decision;
                            //rta.Score = respVerify.puntajeafis;
                        }
                        else //Error en SRCeI o directametne null 
                        {
                            if (respEncabezado == null)
                            {
                                msg = "Bio.Core.Matcher.SRCeI.BioVerifyI COD_ERR_PROBLEMAS_EN_CONX_SERVICIO_RC [Rta = null]";
                                LOG.Fatal(msg);
                                oXmlout.SetValue("message", msg);
                            }
                            else
                            {
                                oXmlout.SetValue("message", "");
                                oXmlout.AddValue("trackid", respEncabezado.IdOperacion.ToString());
                                oXmlout.AddValue("status", respEncabezado.Estado.Value.ToString());
                                oXmlout.AddValue("result", "2");
                                oXmlout.AddValue("score", "0");
                                oXmlout.AddValue("threshold", _threshold.ToString());
                                oXmlout.AddValue("timestamp", FormatFechaHoraSRCeI(respEncabezado.FechaHoraOperacion));

                                //rta = GeneraRtaFromEnc(respEncabezado);
                                //rta.CodigoRta = respEncabezado.Estado.Value;
                                //rta.Fecha = FormatFechaHoraSRCeI(respEncabezado.FechaHoraOperacion);
                                //rta.BodyPartValidado = dedo;
                                //rta.Idtrackbis = null;
                                //rta.Idtrackrc = respEncabezado.IdOperacion.ToString();

                                //rta.IPValidada = BCI.MainForm.sIP;
                                //rta.NroIdValidado = run.Mantisa.ToString() + "-" + run.Dv.ToString();
                                //rta.TipoIdValidado = "RUT";
                                //rta.SerialIdValidado = null;
                                //if (respVerify != null)
                                //{
                                //    rta.Resultado = respVerify.decision;
                                //    rta.Score = respVerify.puntajeafis;
                                //}
                            }
                        }
                    }
                }
                else
                {
                    msg = "Bio.Core.Matcher.SRCeI.BioVerifyI Falta huella para enviar";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
                //{
                //    ws.Timeout = this._timeout;
                //    ws.Url = this._urlWS;

                //    iretremoto = ws.Verify(xmlinput, out xmloutbp);
                //    if (iretremoto == Errors.IERR_OK)
                //    {
                //        XmlParamOut oXmlParamOut = Utils.XmlUtils.DeserializeObject<XmlParamOut>(xmloutbp);
                //        oXmlout.SetValue("message", "");
                //        oXmlout.SetValue("status", iretremoto.ToString());
                //        oXmlout.SetValue("trackid", oXmlParamOut.Trackid);
                //        oXmlout.SetValue("result", oXmlParamOut.Result.ToString());
                //        oXmlout.SetValue("score", oXmlParamOut.Score.ToString());
                //        oXmlout.SetValue("threshold", oXmlParamOut.Threshold.ToString());
                //        oXmlout.SetValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
                //        oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(oXmlParamOut.PersonalData));
                //    } else
                //    {
                //        msg = "Bio.Core.Matcher.ConnectorBioPortal4 Error en WS Remoto [" + iretremoto.ToString() + "]";
                //        LOG.Fatal(msg);
                //        oXmlout.SetValue("message", msg);
                //        oXmlout.SetValue("status", iretremoto.ToString());
                //        xmloutput = DynamicData.SerializeToXml(oXmlout);
                //        return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                //    }
                //}

                xmloutput = DynamicData.SerializeToXml(oXmlout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.SRCeI.BioVerifyI.Verify Error [" + ex.Message + "]";
                LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.Verify Error", ex);
                oXmlout.SetValue("message", msg);
                xmloutput = DynamicData.SerializeToXml(oXmlout);
                return Errors.IERR_UNKNOWN;
            }

            return Errors.IERR_OK;
        }

        private byte[] ExtraeWSQFromParamIn(XmlParamIn oXmlIn)
        {
            byte[] wsqr = null;
            try
            {
                foreach (var sample in oXmlIn.SampleCollection)
                {
                    if (sample.Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                    {
                        wsqr = Convert.FromBase64String(sample.Data);
                        break;
                    } else if (sample.Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW)
                    {
                        byte[] raw = Convert.FromBase64String(sample.Data);
                        WsqEncoder encoder = new WsqEncoder();
                        encoder.EncodeMemory(raw, 512, 512, out wsqr);
                        break;
                    }  
                }
                 
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.BioVerifyI ExtraeWSQFromParamIn Error", ex);
            }
            return wsqr;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */

            string msg;
            xmloutput = null;
            int iretremoto;

            //DynamicData oXmlout = new DynamicData();
            //oXmlout.AddValue("message", "");
            //oXmlout.AddValue("trackid", "");
            //oXmlout.AddValue("status", "");
            //oXmlout.AddValue("result", "");
            //oXmlout.AddValue("score", "");
            //oXmlout.AddValue("timestamp", "");
            //oXmlout.AddValue("personaldata", "");

            //try
            //{
            //    if (!this._isConfigured)
            //    {
            //        msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error = Connector No Configurado!";
            //        LOG.Fatal(msg);
            //        oXmlout.SetValue("message", msg);
            //        xmloutput = DynamicData.SerializeToXml(oXmlout);
            //        return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
            //    }

            //    using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
            //    {
            //        ws.Timeout = this._timeout;
            //        ws.Url = this._urlWS;

            //        iretremoto = ws.Identify(xmlinput, out xmloutbp);
            //        if (iretremoto == Errors.IERR_OK)
            //        {
            //            XmlParamOut oXmlParamOut = Utils.XmlUtils.DeserializeObject<XmlParamOut>(xmloutbp);
            //            oXmlout.SetValue("message", "");
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            oXmlout.SetValue("trackid", oXmlParamOut.Trackid);
            //            oXmlout.SetValue("result", oXmlParamOut.Result.ToString());
            //            oXmlout.SetValue("score", oXmlParamOut.Score.ToString());
            //            oXmlout.SetValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
            //            oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(oXmlParamOut.PersonalData));
            //        }
            //        else
            //        {
            //            msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error en WS Remoto [" + iretremoto.ToString() + "]";
            //            LOG.Fatal(msg);
            //            oXmlout.SetValue("message", msg);
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            xmloutput = DynamicData.SerializeToXml(oXmlout);
            //            return Errors.IERR_CONNECTOR_REMOTE_ERROR;
            //        }
            //    }

            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //}
            //catch (Exception ex)
            //{
            //    msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error [" + ex.Message + "]";
            //    LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.Identity Error", ex);
            //    oXmlout.SetValue("message", msg);
            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //    return Errors.IERR_UNKNOWN;
            //}

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de Recuperación. Ingresa información recuperar, 
        /// y se realiza la operacion.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Get(string xmlinput, out string xmloutput)
        {
            /*
                Trackid" column="trackid" type="string" />
                Status" column="status" type="int" />
                Result" column="result" type="int" />
                Score" column="score" type="double" />
                Timestamp" column="timestamp" type="string" />
              
                Message = String 
             */
            xmloutput = null;

            //string msg;
            //string xmloutbp;
            //int iretremoto;

            //DynamicData oXmlout = new DynamicData();
            //oXmlout.AddValue("message", "");
            //oXmlout.AddValue("trackid", "");
            //oXmlout.AddValue("status", "0");
            //oXmlout.AddValue("result", "0");
            //oXmlout.AddValue("score", "0");
            //oXmlout.AddValue("timestamp", "");
            //oXmlout.AddValue("personaldata", "");

            //try
            //{
            //    if (!this._isConfigured)
            //    {
            //        msg = "Bio.Core.Matcher.ConnectorBioPortal4.Get Error = Connector No Configurado!";
            //        LOG.Fatal(msg);
            //        oXmlout.SetValue("message", msg);
            //        xmloutput = DynamicData.SerializeToXml(oXmlout);
            //        return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
            //    }

            //    using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
            //    {
            //        ws.Timeout = this._timeout;
            //        ws.Url = this._urlWS;

            //        iretremoto = ws.Get(xmlinput, out xmloutbp);
            //        if (iretremoto == Errors.IERR_OK)
            //        {
            //            XmlParamOut oXmlParamOut = Utils.XmlUtils.DeserializeObject<XmlParamOut>(xmloutbp);
            //            oXmlout.SetValue("message", "");
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            oXmlout.SetValue("trackid", oXmlParamOut.Trackid);
            //            //oXmlout.SetValue("result", oXmlParamOut.Result.ToString());
            //            //oXmlout.SetValue("score", oXmlParamOut.Score.ToString());
            //            oXmlout.SetValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
            //            oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(oXmlParamOut.PersonalData));
            //        }
            //        else
            //        {
            //            msg = "Bio.Core.Matcher.ConnectorBioPortal4.Get Error en WS Remoto [" + iretremoto.ToString() + "]";
            //            LOG.Fatal(msg);
            //            oXmlout.SetValue("message", msg);
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            xmloutput = DynamicData.SerializeToXml(oXmlout);
            //            return Errors.IERR_CONNECTOR_REMOTE_ERROR;
            //        }
            //    }

            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //}
            //catch (Exception ex)
            //{
            //    msg = "Bio.Core.Matcher.ConnectorBioPortal4.Get Error [" + ex.Message + "]";
            //    LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.Get Error", ex);
            //    oXmlout.SetValue("message", msg);
            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //    return Errors.IERR_UNKNOWN;
            //}

            return Errors.IERR_OK;
        }

#endregion Implementation of IConnector

        private static string FormatFechaHoraSRCeI(string fechaoperacion)
        {
            string strRet = "";
            try
            {
                strRet = fechaoperacion.Substring(6, 2) + "/" +
                         fechaoperacion.Substring(4, 2) + "/" +
                         fechaoperacion.Substring(0, 4) + " " +
                         fechaoperacion.Substring(8, 2) + ":" +
                         fechaoperacion.Substring(10, 2) + ":" +
                         fechaoperacion.Substring(12, 2);
            }
            catch
            {
                strRet = "";
            }
            return strRet;
        }

        public void Dispose()
        {
            
        }
    }
}
