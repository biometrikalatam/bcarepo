using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace Bio.Core.WebControls.Export
{
	/// <summary>
	/// Descripción breve de Export.
	/// </summary>
	[DefaultProperty("Export"),
		ToolboxData("<{0}:Export width=100% runat=server></{0}:Export>")]
    [ToolboxBitmap(typeof(Export), "Icono_small.gif.gif")]
    public class Export : 
		WebControl, 
		INamingContainer			//Para repetir control en misma pagina
	{
		//Data visible, imagenes
//		protected System.Web.UI.WebControls.ImageButton imgExcel = new ImageButton();
//		public event System.Web.UI.ImageClickEventHandler Click;
//		protected System.Web.UI.WebControls.ImageButton imgWord = new ImageButton(); 
//		protected System.Web.UI.WebControls.ImageButton imgText = new ImageButton();
		
		//Data para pasar parametros
		private DataGrid dgToExport;
		private HttpResponse response;
		private string pathImageExcel = "../images/Icon_Excel.gif";
		private string pathImageWord = "../images/Icon_Word.gif";
		private string pathImageText = "../images/Icon_Text.gif";
		private bool excelVisible = true;
		private bool wordVisible = true;
		private bool textVisible = true;

		private int cantVisible = 3; //Determina cuantos estan visibles para saber
									 // posicion del LabMsg.

		[Bindable(true), Category("Category"),DefaultValue("null")]
		public DataGrid DGToExport
		{
			get { return dgToExport; }
			set { dgToExport = value; }
		}

		[Bindable(false), Category("Category"),DefaultValue("null")]
		public HttpResponse Response {
			get { return response; }
			set { response = value; }
		}

		[Editor(typeof(ImageUrlEditor), typeof(UITypeEditor)),
			Category("Category"), Description("Path imagen de exportacion de Excel")]
		public string PathImageExcel {
			get { return pathImageExcel; }
			set { pathImageExcel = value; }
		}

		[Editor(typeof(ImageUrlEditor), typeof(UITypeEditor)),
			Category("Category"), Description("Path imagen de exportacion de Word")]
		public string PathImageWord {
			get { return pathImageWord; }
			set { pathImageWord = value; }
		}

		[Editor(typeof(ImageUrlEditor), typeof(UITypeEditor)),
			Category("Category"), Description("Path imagen de exportacion de Texto")]
		public string PathImageText {
			get { return pathImageText; }
			set { pathImageText = value; }
		}

		[Editor(typeof(Boolean), typeof(ValueType)),
			Category("Category"), Description("Visible la exportacion a Excel"),DefaultValue(true)]
		public bool ExcelVisible {
			get { return excelVisible; }
			set { excelVisible = value; }
		}

		[Editor(typeof(Boolean), typeof(ValueType)),
			Category("Category"), Description("Visible la exportacion a Word"),DefaultValue(true)]
		public bool WordVisible {
			get { return wordVisible; }
			set { wordVisible = value; }
		}

		[Editor(typeof(Boolean), typeof(ValueType)),
			Category("Category"), Description("Visible la exportacion a Texto"),DefaultValue(true)]
		public bool TextVisible {
			get { return textVisible; }
			set { textVisible = value;}
		}

		override protected void OnInit(EventArgs e) {
			this.Response = this.Page.Response;
//			InitializeComponent();
			base.OnInit(e);
		}

//		private void InitializeComponent() {
////			this.imgExcel.Click += new System.Web.UI.ImageClickEventHandler(this.ExportToExcel);
//		}

		/// <summary>
		/// Procesar este control en el parámetro de salida especificado.
		/// </summary>
		/// <param name="output"> Programa de escritura HTML para escribir </param>
		protected override void Render(HtmlTextWriter output)
		{
			base.Render(output);
//			output.Write("<script language=\"C#\" runat=\"server\"><br>" +
//						 "private void Export_Click(Object sender, EventArgs e) {<br>" +
//						 "	Export1.DGLog = this.DGLog;<br>" +
//						 "	Export1.Response = this.Response;<br>" +
//						 "	Export1.ExcelVisible = true;<br>" +
//						 "	Export1.WordVisible = true;<br>" +
//						 "	Export1.TextVisible = true;<br>" +
//						 "}<br>" +
//						 "</script><br>");
		}

		protected override void CreateChildControls()
		{
//			this.Controls.Add(new LiteralControl("<b><font face=\"Arial\" size=\"2\">Exportar a:</font></b>"));
//			this.Controls.Add(new LiteralControl("&nbsp;"));

			cantVisible = 0;

			ImageButton imgExcel = new ImageButton();
			ImageButton imgWord = new ImageButton(); 
			ImageButton imgText = new ImageButton();
			if (excelVisible) {
				cantVisible++;
				imgExcel.Width = Unit.Pixel(21);
				imgExcel.Height = Unit.Pixel(20);
				imgExcel.ImageUrl = this.PathImageExcel.Trim();
				imgExcel.ToolTip = "Exporta a Excel...";
				this.Controls.Add(imgExcel);
				imgExcel.Click += new ImageClickEventHandler(this.ExportToExcel);
			}

			if (wordVisible) {
				cantVisible++;
				imgWord.Width = Unit.Pixel(21);
				imgWord.Height = Unit.Pixel(20);
				imgWord.ImageUrl = this.PathImageWord.Trim();
				imgWord.ToolTip = "Exporta a Word...";
				this.Controls.Add(imgWord);
				imgWord.Click += new ImageClickEventHandler(this.ExportToWord);
			}

			if (textVisible) {
				cantVisible++;
				imgText.Width = Unit.Pixel(21);
				imgText.Height = Unit.Pixel(20);
				imgText.ImageUrl = this.PathImageText.Trim();
				imgText.ToolTip = "Exporta a Texto...";
				this.Controls.Add(imgText);
				imgText.Click += new ImageClickEventHandler(this.ExportToText);
			}

			//Label para mensajes de error
			this.Controls.Add(new LiteralControl("&nbsp;"));
			Label labMsg = new Label();
			labMsg.ForeColor = Color.Black;
			labMsg.Text = ""; //"Seleccione un tipo de exportación...";
			labMsg.Font.Name = "Arial";
			labMsg.Font.Size = FontUnit.XSmall;
			this.Controls.Add(labMsg);

//			this.Attributes.Add("OnClick","Export_Click"); 
//			this.Attributes.Add("Width","100%"); 
//			this.Width = Unit.Percentage(100);
		}

		public void ExportToExcel(object sender, ImageClickEventArgs e)
		{
			try {
				((Label)this.Controls[cantVisible+1]).ForeColor = Color.Black;
				((Label)this.Controls[cantVisible+1]).Text = "Exportando a Excel...";
				Response.Clear();
				Response.AddHeader("content-disposition", "attachment;filename=FileName.xls");
				Response.Charset = "";
				Response.Cache.SetCacheability(HttpCacheability.NoCache);
				Response.ContentType = "application/vnd.xls";
				StringWriter stringWrite = new StringWriter();
				HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
				this.DGToExport.RenderControl(htmlWrite);
				Response.Write(stringWrite.ToString());
				Response.End();
			} catch (Exception ex)
			{
				((Label)this.Controls[cantVisible+1]).ForeColor = Color.Red;
                string msgCut = null;
                if (ex.Message != null)
                {
                    msgCut = ex.Message.Length>50 ? ex.Message.Substring(0, 50) + "..." : ex.Message;
                }
				((Label)this.Controls[cantVisible+1]).Text = msgCut;
			}

		}

		public void ExportToWord(object sender, ImageClickEventArgs e) {
			try {
				((Label)this.Controls[cantVisible+1]).ForeColor = Color.Black;
				((Label)this.Controls[cantVisible+1]).Text = "Exportando a Word...";
				Response.Clear();
				Response.AddHeader("content-disposition", "attachment;filename=FileName.doc");
				Response.Charset = "";
				Response.Cache.SetCacheability(HttpCacheability.NoCache);
				Response.ContentType = "application/vnd.word";
				StringWriter stringWrite = new StringWriter();
				HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
				this.DGToExport.RenderControl(htmlWrite);
				Response.Write(stringWrite.ToString());
				Response.End();
			} catch (Exception ex) {
				((Label)this.Controls[cantVisible+1]).ForeColor = Color.Red;
                string msgCut = null;
                if (ex.Message != null)
                {
                    msgCut = ex.Message.Length > 50 ? ex.Message.Substring(0, 50) + "..." : ex.Message;
                }

				((Label)this.Controls[cantVisible+1]).Text = msgCut;
			}

		}

		public void ExportToText(object sender, ImageClickEventArgs e) {
			try {

				((Label)this.Controls[cantVisible+1]).ForeColor = Color.Black;
				((Label)this.Controls[cantVisible+1]).Text = "Exportando a Texto...";
				StringBuilder str = new StringBuilder(); 

				for(int i=0;i<this.DGToExport.Items.Count;i++) {
					int cantCells = this.DGToExport.Items[i].Cells.Count;
					for(int j=0;j<=cantCells - 1; j++) {
						if (!this.DGToExport.Items[i].Cells[j].Text.Trim().Equals("&nbsp;")) {
							str.Append(this.DGToExport.Items[i].Cells[j].Text.Trim());
						}
							//Si no es la ultima celda, agrego coma
						if (j<cantCells-1) str.Append(","); 
					}
					str.Append("\n");
				}
				Response.Clear();
				Response.AddHeader("content-disposition", "attachment;filename=FileName.txt");
				Response.Charset = "";
				Response.Cache.SetCacheability(HttpCacheability.NoCache);
				Response.ContentType = "application/vnd.text";
				Response.Write(str.ToString());
				Response.End();
			} catch (Exception ex) {
				((Label)this.Controls[cantVisible+1]).ForeColor = Color.Red;
                string msgCut = null;
                if (ex.Message != null)
                {
                    msgCut = ex.Message.Length > 50 ? ex.Message.Substring(0, 50) + "..." : ex.Message;
                }

				((Label)this.Controls[cantVisible+1]).Text = msgCut;
			}

		}

	}
}
