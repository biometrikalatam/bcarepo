﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bio.Core.Api;
using Bio.Core.Constant;
using Bio.Core.Matcher.Token;
using BioCryptoAx;
using log4net;
using System.Web.Security;

namespace Bio.Core.TokenAV040101
{
    public class TokenAV040101 : Bio.Core.Matcher.Token.IToken
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(TokenAV040101));


        #region Private Properties Implementation of IToken

        private int _operationType;

        private int _authenticationFactor;

        private int _minutiaeType;

        private int _datatypetoken;

        private int _bodyPart;

        private List<Sample> _samples;

        private string _additionalData;

        private string _getData;

        private string _setData;

        private string _typeid;

        private string _valueid;

        private string _clientid;

        private string _ipenduser;

        private string _enduser;

        #endregion Private Properties Implementation of IToken

        #region Public Properties Implementation of IToken

        /// <summary>
        /// 1-Verify | 2-Enroll
        /// </summary>
        public int OperationType
        {
            get { return _operationType; }
            set { _operationType = value; }
        }

        /// <summary>
        /// Tecnologia a la que corresponde el template
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia que contiene Data, incluidos WSQ, RAW, etc.
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        ///// <summary>
        ///// Tipo de informacion (data) que contendra el template (desde BIR_TYPE)
        ///// </summary>
        //public int Type
        //{
        //    get { return _type; }
        //    set { _type = value; }
        //}

        /// <summary>
        /// Parte del cuerpo si es tecnologia biometrica
        /// </summary>
        public int BodyPart
        {
            get { return _bodyPart; }
            set { _bodyPart = value; }
        }

        /// <summary>
        /// [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
        /// </summary>
        public int DataTypeToken
        {
            get { return _datatypetoken; }
            set { _datatypetoken = value; }
        }

        /// <summary>
        /// Data propia del template o sample
        /// </summary>
        public List<Sample> Samples
        {
            get { return _samples; }
            set { _samples = value; }
        }

        /// <summary>
        /// Datos adicionales segun sea la data. Por ejemplo si es WSQ, 
        /// se puede colocar las dimensiones de la forma w=512|h=512, o si
        /// es template NEC, el coeficiente coef=xxx. Si hay mas de uno separado 
        /// por pipe "|"
        /// </summary>
        public string AdditionalData
        {
            get { return _additionalData; }
            set { _additionalData = value; }
        }

        /// <summary>
        /// Obtiene la Data en base 64
        /// </summary>
        public string GetData
        {
            get { return _getData; }
        }

        /// <summary>
        /// Setea la data desde base 64
        /// </summary>
        public string SetData
        {
            set { _setData = value; }
        }

        /// Tipo ID de la persona (RUT, DNI, etc) 
        /// </summary>
        public string Typeid
        {
            get { return _typeid; }
            set { _typeid = value; }
        }

        /// Valor ID de la persona (RUT, DNI, etc) 
        /// </summary>
        public string Valueid
        {
            get { return _valueid; }
            set { _valueid = value; }
        }

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist 
        /// </summary>
        public string Clientid
        {
            get { return _clientid; }
            set { _clientid = value; }
        }

        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        public string Ipenduser
        {
            get { return _ipenduser; }
            set { _ipenduser = value; }
        }

        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        public string Enduser
        {
            get { return _enduser; }
            set { _enduser = value; }
        }

        #endregion Public Properties Implementation of IToken

        #region Public Method

        /// <summary>
        /// Constructor vacio
        /// </summary>
        public TokenAV040101() { }

        /// <summary>
        /// Constructor que recibe token y extrae
        /// </summary>
        /// <param name="token"></param>
        public TokenAV040101(string token)
        {
            Extract(token);
        }


        /// <summary>
        /// Extrae el contenido del token y llena las propiedades del objeto
        /// </summary>
        /// <param name="token">Token a extraer</param>
        /// <returns></returns>
        public int Extract(string token)
        {
            int ret = Errors.IERR_OK;
            try
            {
                //Si ES NULO O NO EMPIEZA CON av0305 retorno error
                if (token == null ||
                    ((!token.Trim().Substring(0, 8).Equals("AV040101")) && (!token.Trim().Substring(0, 8).Equals("AV030901"))))
                    return Errors.IERR_BAD_PARAMETER;

                //Saco encabezado
                string auxtoken = token.Trim().Substring(8);
                //Desencripto la data que viene en Hexa
                BKclassCryptoClass oCrypt = new BKclassCryptoClass();
                string dataDecrypted = null;
                try
                {
                    dataDecrypted = oCrypt.Decrypt(auxtoken, "BioPortalClientAx14042010", true,
                                                          frezCryptoEncryptionType.frezBlockEncryption);
                    if (dataDecrypted == null) return Errors.IERR_INVALID_TOKEN;
                }
                catch (Exception exdec)
                {
                    LOG.Error("TokenAV040101.Extract - Desencriptando - ", exdec);
                    return Errors.IERR_INVALID_TOKEN;
                }

                //Separo segun patron de separacion
                dataDecrypted = dataDecrypted.Replace("&#x0;", "");
                string[] aData = dataDecrypted.Split('|');

                //Lleno propiedades desde token
                /*
                   Pos > 0 - Tipo_Doc
                   Pos > 1 - Nro_Doc
                   Pos > 2 - Si es DP Serial Number
                   Pos > 3 - [1-10] Dedo  = BodyPart
                   Pos > 4 - WSQ en B64 si sContenido = 0,1
                   Pos > 5 - Template en B64 si sContenido = 0,2 (NEC o DP)
                   Pos > 6 - Timestamp Cliente [ddmmyyyyhhmmss]
                   Pos > 7 - IP cliente
                   Pos > 8 - [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
                   Pos > 9 - [1-NEC | 2-DP] Formato del Template
                   Pos > 10 - Coeficiente = AdditionalData
                   Pos > 11 - [UNKNOW-0|DP-1|SEC-2|BIOI-3|CROSSMATCH-4] Dispositivo
                   Pos > 12 - AutenticationFactor
                   Pos > 13 - OperationType
                 */
                _operationType = Convert.ToInt32(aData[13]);
                _authenticationFactor = Convert.ToInt32(aData[12]);
                _minutiaeType = Convert.ToInt32(aData[9]);
                _bodyPart = Convert.ToInt32(aData[3]);
                _samples = new List<Sample>();
                _datatypetoken = Convert.ToInt32(aData[8]);

                //Si viene Tewmplate, lo pongo primero para qyue verifique por el 
                string auxdata;
                if (_datatypetoken == 0 || _datatypetoken == 2)
                {
                    //Si es Password, aplico hash
                    if (_authenticationFactor == Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
                    {
                        auxdata = aData[5]; // Encoding.ASCII.GetString(aData[5]);
                        auxdata = FormsAuthentication.HashPasswordForStoringInConfigFile(auxdata, "SHA1");
                    }
                    else
                    {    //Sino paso tal cual viene
                        auxdata = aData[5];
                    }

                    //Added 01-11-2013 para agregar soprote a M;inucias ANSI y ISO que vienene en nuevo Token de BPClient.NET
                    //Con SDK de DP v2.2.0
                    string[] sep = new string[1] { "-septmpl-" };
                    string[] sa = auxdata.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    string _templDP = sa[0] + "-septmpl-" + sa[1];
                    //Add DP enroll y verify template concatenados
                    Sample sTmpl = new Sample
                    {
                        Data = _templDP,
                        Additionaldata = aData[10],
                        Device = Convert.ToInt32(aData[11]),
                        Minutiaetype = _minutiaeType //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW //Convert.ToInt32(aData[9])
                    };
                    //_samples.Add(sTmpl);
                    string _datatANSI378;
                    string _datatISO19794_2;
                    Sample sTmpl1 = null;
                    Sample sTmpl2 = null;
                    if (sa.Length > 2)
                    {
                        _datatANSI378 = sa[2];
                        //Add ANSI 378
                        sTmpl1 = new Sample
                        {
                            Data = _datatANSI378,
                            Additionaldata = aData[10],
                            Device = Convert.ToInt32(aData[11]),
                            Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                        };
                        //_samples.Add(sTmpl1);
                    }
                    if (sa.Length > 3)
                    {
                        _datatISO19794_2 = sa[3];
                        //Add ISO 19794-2
                        sTmpl2 = new Sample
                        {
                            Data = _datatISO19794_2,
                            Additionaldata = aData[10],
                            Device = Convert.ToInt32(aData[11]),
                            Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                        };
                        //_samples.Add(sTmpl2);
                    }

                    //Added 05-2014 para armar el orden d elos templates, porque si es ANSI o ISO deben ir primero
                    //              por como se verifica en Service.Manager.Verify, que si viene un template generado
                    //              => Verifica con el Minutiaetype indicado pero con el sample[0] (el primero d ela lista)
                    if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
                    {
                        _samples.Add(sTmpl1);  //Primero ANSI
                        _samples.Add(sTmpl);   //Segundo OTW
                        _samples.Add(sTmpl2);  //Tercero ISO
                    }
                    else if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
                    {
                        _samples.Add(sTmpl2);  //Primero ISO
                        _samples.Add(sTmpl);   //Segundo OTW
                        _samples.Add(sTmpl1);  //Tercero ANSI
                    }
                    else //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW)
                    {
                        _samples.Add(sTmpl);  //Primero OTW o Secugen o el que sea
                        if (sTmpl1 != null) _samples.Add(sTmpl1); //Segundo ANSI    Agrego chequeo de NULL, para hacerlo compatible con anteriores versiones de 
                        if (sTmpl2 != null) _samples.Add(sTmpl2); //Tercero ISO     TokenAV040101
                    }

                }
                //Si viene WSQ, lo agrego para verificacion si no viene Template
                if (_datatypetoken == 0 || _datatypetoken == 1)  //Viene WSQ
                {
                    Sample sWsq = new Sample
                    {
                        Data = aData[4],
                        Additionaldata = aData[10],
                        Device = Convert.ToInt32(aData[11]),
                        Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ
                    };
                    _samples.Add(sWsq);
                }
                _typeid = aData[0];
                _valueid = aData[1];
                _clientid = aData[2];
                _ipenduser = aData[7];
                _enduser = aData[7];
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("TokenAV040101.Extarct", ex);
            }
            return ret;
        }

        /// <summary>
        /// Extrae el contenido del token y llena las propiedades del objeto y ademas de llenar las prop del bjeto, 
        /// setea las variable que corresponden a un objeto XmlParamIn
        /// </summary>
        /// <param name="token">token en base 64</param>
        /// <param name="newparamin">Objeto XmlParamIn con datos extraidos desde token</param>
        /// <returns></returns>
        public int ExtractToParameters(string token, out string newparamin)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {

        }

#endregion Public Method

    }
}
