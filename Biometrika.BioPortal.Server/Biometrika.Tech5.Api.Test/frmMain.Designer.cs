﻿namespace Biometrika.Tech5.Api.Test
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabDirecto = new System.Windows.Forms.TabPage();
            this.btnCreateFolder = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.labResult = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPersonId = new System.Windows.Forms.TextBox();
            this.btnVerify = new System.Windows.Forms.Button();
            this.btnDirIdentify = new System.Windows.Forms.Button();
            this.btnEnroll = new System.Windows.Forms.Button();
            this.btnDirExtract = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.rtbResultDirecto = new System.Windows.Forms.RichTextBox();
            this.button10 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUrlT5 = new System.Windows.Forms.TextBox();
            this.tabViaBP = new System.Windows.Forms.TabPage();
            this.chkSaveHDD = new System.Windows.Forms.CheckBox();
            this.labDedo1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.button15 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.button16 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnDedo10 = new System.Windows.Forms.Button();
            this.ckkIsB642 = new System.Windows.Forms.CheckBox();
            this.btnLoad2 = new System.Windows.Forms.Button();
            this.ckkIsB641 = new System.Windows.Forms.CheckBox();
            this.btnLoad1 = new System.Windows.Forms.Button();
            this.txtCamara = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnCapture2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnCapture1 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picDedo5 = new System.Windows.Forms.PictureBox();
            this.picDedo4 = new System.Windows.Forms.PictureBox();
            this.picDedo3 = new System.Windows.Forms.PictureBox();
            this.picDedo2 = new System.Windows.Forms.PictureBox();
            this.picDedo1 = new System.Windows.Forms.PictureBox();
            this.picDedo6 = new System.Windows.Forms.PictureBox();
            this.picDedo7 = new System.Windows.Forms.PictureBox();
            this.picDedo8 = new System.Windows.Forms.PictureBox();
            this.picDedo9 = new System.Windows.Forms.PictureBox();
            this.picDedo10 = new System.Windows.Forms.PictureBox();
            this.picImage2 = new System.Windows.Forms.PictureBox();
            this.picImage1 = new System.Windows.Forms.PictureBox();
            this.imageCam = new System.Windows.Forms.PictureBox();
            this.picResult = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabDirecto.SuspendLayout();
            this.labDedo1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResult)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabDirecto);
            this.tabControl1.Controls.Add(this.tabViaBP);
            this.tabControl1.Location = new System.Drawing.Point(29, 641);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1480, 340);
            this.tabControl1.TabIndex = 0;
            // 
            // tabDirecto
            // 
            this.tabDirecto.BackColor = System.Drawing.Color.LightGray;
            this.tabDirecto.Controls.Add(this.btnCreateFolder);
            this.tabDirecto.Controls.Add(this.btnDelete);
            this.tabDirecto.Controls.Add(this.labResult);
            this.tabDirecto.Controls.Add(this.picResult);
            this.tabDirecto.Controls.Add(this.label14);
            this.tabDirecto.Controls.Add(this.txtPersonId);
            this.tabDirecto.Controls.Add(this.btnVerify);
            this.tabDirecto.Controls.Add(this.btnDirIdentify);
            this.tabDirecto.Controls.Add(this.btnEnroll);
            this.tabDirecto.Controls.Add(this.btnDirExtract);
            this.tabDirecto.Controls.Add(this.button5);
            this.tabDirecto.Controls.Add(this.rtbResultDirecto);
            this.tabDirecto.Controls.Add(this.button10);
            this.tabDirecto.Controls.Add(this.label2);
            this.tabDirecto.Controls.Add(this.txtUrlT5);
            this.tabDirecto.Location = new System.Drawing.Point(4, 22);
            this.tabDirecto.Name = "tabDirecto";
            this.tabDirecto.Padding = new System.Windows.Forms.Padding(3);
            this.tabDirecto.Size = new System.Drawing.Size(1472, 314);
            this.tabDirecto.TabIndex = 0;
            this.tabDirecto.Text = "Directo";
            // 
            // btnCreateFolder
            // 
            this.btnCreateFolder.BackColor = System.Drawing.Color.DarkGray;
            this.btnCreateFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateFolder.Location = new System.Drawing.Point(179, 109);
            this.btnCreateFolder.Name = "btnCreateFolder";
            this.btnCreateFolder.Size = new System.Drawing.Size(133, 54);
            this.btnCreateFolder.TabIndex = 88;
            this.btnCreateFolder.Text = "Create Folder Enroll Bulk";
            this.btnCreateFolder.UseVisualStyleBackColor = false;
            this.btnCreateFolder.Click += new System.EventHandler(this.btnCreateFolder_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Red;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(42, 190);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(131, 37);
            this.btnDelete.TabIndex = 125;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // labResult
            // 
            this.labResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResult.Location = new System.Drawing.Point(431, 112);
            this.labResult.Name = "labResult";
            this.labResult.Size = new System.Drawing.Size(170, 184);
            this.labResult.TabIndex = 124;
            this.labResult.Text = "Result Feedback";
            this.labResult.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(47, 112);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 89;
            this.label14.Text = "Id de Persona";
            // 
            // txtPersonId
            // 
            this.txtPersonId.Location = new System.Drawing.Point(50, 128);
            this.txtPersonId.Name = "txtPersonId";
            this.txtPersonId.Size = new System.Drawing.Size(123, 20);
            this.txtPersonId.TabIndex = 88;
            this.txtPersonId.Text = "21284415-2";
            // 
            // btnVerify
            // 
            this.btnVerify.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnVerify.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVerify.Location = new System.Drawing.Point(42, 228);
            this.btnVerify.Name = "btnVerify";
            this.btnVerify.Size = new System.Drawing.Size(131, 37);
            this.btnVerify.TabIndex = 87;
            this.btnVerify.Text = "Verify";
            this.btnVerify.UseVisualStyleBackColor = false;
            this.btnVerify.Click += new System.EventHandler(this.btnVerify_Click);
            // 
            // btnDirIdentify
            // 
            this.btnDirIdentify.BackColor = System.Drawing.Color.IndianRed;
            this.btnDirIdentify.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDirIdentify.Location = new System.Drawing.Point(42, 271);
            this.btnDirIdentify.Name = "btnDirIdentify";
            this.btnDirIdentify.Size = new System.Drawing.Size(131, 37);
            this.btnDirIdentify.TabIndex = 86;
            this.btnDirIdentify.Text = "Identify";
            this.btnDirIdentify.UseVisualStyleBackColor = false;
            this.btnDirIdentify.Click += new System.EventHandler(this.btnDirIdentify_Click);
            // 
            // btnEnroll
            // 
            this.btnEnroll.BackColor = System.Drawing.Color.Orange;
            this.btnEnroll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnroll.Location = new System.Drawing.Point(42, 150);
            this.btnEnroll.Name = "btnEnroll";
            this.btnEnroll.Size = new System.Drawing.Size(131, 37);
            this.btnEnroll.TabIndex = 85;
            this.btnEnroll.Text = "Enroll";
            this.btnEnroll.UseVisualStyleBackColor = false;
            this.btnEnroll.Click += new System.EventHandler(this.btnEnroll_Click);
            // 
            // btnDirExtract
            // 
            this.btnDirExtract.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnDirExtract.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDirExtract.Location = new System.Drawing.Point(42, 71);
            this.btnDirExtract.Name = "btnDirExtract";
            this.btnDirExtract.Size = new System.Drawing.Size(131, 37);
            this.btnDirExtract.TabIndex = 84;
            this.btnDirExtract.Text = "Extract";
            this.btnDirExtract.UseVisualStyleBackColor = false;
            this.btnDirExtract.Click += new System.EventHandler(this.btnDirExtract_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(1375, 11);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 83;
            this.button5.Text = "Borrar";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // rtbResultDirecto
            // 
            this.rtbResultDirecto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.rtbResultDirecto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbResultDirecto.Location = new System.Drawing.Point(622, 40);
            this.rtbResultDirecto.Name = "rtbResultDirecto";
            this.rtbResultDirecto.ReadOnly = true;
            this.rtbResultDirecto.Size = new System.Drawing.Size(828, 256);
            this.rtbResultDirecto.TabIndex = 82;
            this.rtbResultDirecto.Text = "btnCreateFolder";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(347, 37);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(108, 23);
            this.button10.TabIndex = 81;
            this.button10.Text = "Set T5 Endpoint";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 13);
            this.label2.TabIndex = 66;
            this.label2.Text = "URL Base Servicio Tech 5";
            // 
            // txtUrlT5
            // 
            this.txtUrlT5.Location = new System.Drawing.Point(31, 37);
            this.txtUrlT5.Name = "txtUrlT5";
            this.txtUrlT5.Size = new System.Drawing.Size(305, 20);
            this.txtUrlT5.TabIndex = 64;
            this.txtUrlT5.Text = "http://gn-testapi.tech5.tech:8080/T5CloudService/1.0";
            // 
            // tabViaBP
            // 
            this.tabViaBP.Location = new System.Drawing.Point(4, 22);
            this.tabViaBP.Name = "tabViaBP";
            this.tabViaBP.Padding = new System.Windows.Forms.Padding(3);
            this.tabViaBP.Size = new System.Drawing.Size(1472, 314);
            this.tabViaBP.TabIndex = 1;
            this.tabViaBP.Text = "Via BioPortal Server";
            this.tabViaBP.UseVisualStyleBackColor = true;
            // 
            // chkSaveHDD
            // 
            this.chkSaveHDD.AutoSize = true;
            this.chkSaveHDD.Location = new System.Drawing.Point(491, 158);
            this.chkSaveHDD.Name = "chkSaveHDD";
            this.chkSaveHDD.Size = new System.Drawing.Size(122, 17);
            this.chkSaveHDD.TabIndex = 63;
            this.chkSaveHDD.Text = "Save in HDD Result";
            this.chkSaveHDD.UseVisualStyleBackColor = true;
            this.chkSaveHDD.Visible = false;
            // 
            // labDedo1
            // 
            this.labDedo1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.labDedo1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.labDedo1.Controls.Add(this.groupBox2);
            this.labDedo1.Controls.Add(this.ckkIsB642);
            this.labDedo1.Controls.Add(this.btnLoad2);
            this.labDedo1.Controls.Add(this.ckkIsB641);
            this.labDedo1.Controls.Add(this.btnLoad1);
            this.labDedo1.Controls.Add(this.txtCamara);
            this.labDedo1.Controls.Add(this.label1);
            this.labDedo1.Controls.Add(this.button4);
            this.labDedo1.Controls.Add(this.button2);
            this.labDedo1.Controls.Add(this.picImage2);
            this.labDedo1.Controls.Add(this.btnCapture2);
            this.labDedo1.Controls.Add(this.button3);
            this.labDedo1.Controls.Add(this.picImage1);
            this.labDedo1.Controls.Add(this.btnCapture1);
            this.labDedo1.Controls.Add(this.button1);
            this.labDedo1.Controls.Add(this.imageCam);
            this.labDedo1.Controls.Add(this.chkSaveHDD);
            this.labDedo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDedo1.Location = new System.Drawing.Point(33, 101);
            this.labDedo1.Name = "labDedo1";
            this.labDedo1.Size = new System.Drawing.Size(1480, 534);
            this.labDedo1.TabIndex = 1;
            this.labDedo1.TabStop = false;
            this.labDedo1.Text = "Zona de Capturas...";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.picDedo5);
            this.groupBox2.Controls.Add(this.button12);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.picDedo4);
            this.groupBox2.Controls.Add(this.button13);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.picDedo3);
            this.groupBox2.Controls.Add(this.button14);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.picDedo2);
            this.groupBox2.Controls.Add(this.button15);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.picDedo1);
            this.groupBox2.Controls.Add(this.button16);
            this.groupBox2.Controls.Add(this.button11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.picDedo6);
            this.groupBox2.Controls.Add(this.button9);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.picDedo7);
            this.groupBox2.Controls.Add(this.button8);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.picDedo8);
            this.groupBox2.Controls.Add(this.button7);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.picDedo9);
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.picDedo10);
            this.groupBox2.Controls.Add(this.btnDedo10);
            this.groupBox2.Location = new System.Drawing.Point(28, 290);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1424, 231);
            this.groupBox2.TabIndex = 84;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Captura Huellas...";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(497, 15);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(51, 13);
            this.label13.TabIndex = 85;
            this.label13.Text = "Sensores";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(496, 33);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(285, 21);
            this.comboBox1.TabIndex = 122;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(781, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 121;
            this.label3.Text = "Pulgar Der (1)";
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(1200, 80);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 119;
            this.button12.Text = "Capturar";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(885, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 118;
            this.label4.Text = "Indice Der (2)";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(1096, 62);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 116;
            this.button13.Text = "Capturar";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(998, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 115;
            this.label5.Text = "Mayor Der (3)";
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(992, 47);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 113;
            this.button14.Text = "Capturar";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1102, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 112;
            this.label6.Text = "Anular Der (4)";
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(888, 62);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 23);
            this.button15.TabIndex = 110;
            this.button15.Text = "Capturar";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1197, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 109;
            this.label7.Text = "Meñique Der (5)";
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(784, 76);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 107;
            this.button16.Text = "Capturar";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(1333, 15);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 106;
            this.button11.Text = "Borrar";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(460, 64);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 105;
            this.label12.Text = "Pulgar Izq (6)";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(458, 80);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 103;
            this.button9.Text = "Capturar";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(359, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 102;
            this.label11.Text = "Indice Izq (7)";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(354, 62);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 100;
            this.button8.Text = "Capturar";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(256, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 13);
            this.label10.TabIndex = 99;
            this.label10.Text = "Mayor Izq (8)";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(250, 47);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 97;
            this.button7.Text = "Capturar";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(152, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 13);
            this.label9.TabIndex = 96;
            this.label9.Text = "Anular Izq (9)";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(146, 62);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 94;
            this.button6.Text = "Capturar";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 13);
            this.label8.TabIndex = 93;
            this.label8.Text = "Meñique Izq (10)";
            // 
            // btnDedo10
            // 
            this.btnDedo10.Location = new System.Drawing.Point(42, 76);
            this.btnDedo10.Name = "btnDedo10";
            this.btnDedo10.Size = new System.Drawing.Size(75, 23);
            this.btnDedo10.TabIndex = 84;
            this.btnDedo10.Text = "Capturar";
            this.btnDedo10.UseVisualStyleBackColor = true;
            this.btnDedo10.Click += new System.EventHandler(this.btnDedo10_Click);
            // 
            // ckkIsB642
            // 
            this.ckkIsB642.AutoSize = true;
            this.ckkIsB642.Location = new System.Drawing.Point(1186, 26);
            this.ckkIsB642.Name = "ckkIsB642";
            this.ckkIsB642.Size = new System.Drawing.Size(77, 17);
            this.ckkIsB642.TabIndex = 80;
            this.ckkIsB642.Text = "Es Base64";
            this.ckkIsB642.UseVisualStyleBackColor = true;
            this.ckkIsB642.Visible = false;
            // 
            // btnLoad2
            // 
            this.btnLoad2.Location = new System.Drawing.Point(1273, 21);
            this.btnLoad2.Name = "btnLoad2";
            this.btnLoad2.Size = new System.Drawing.Size(75, 23);
            this.btnLoad2.TabIndex = 79;
            this.btnLoad2.Text = "Load Image";
            this.btnLoad2.UseVisualStyleBackColor = true;
            this.btnLoad2.Visible = false;
            // 
            // ckkIsB641
            // 
            this.ckkIsB641.AutoSize = true;
            this.ckkIsB641.Location = new System.Drawing.Point(780, 27);
            this.ckkIsB641.Name = "ckkIsB641";
            this.ckkIsB641.Size = new System.Drawing.Size(77, 17);
            this.ckkIsB641.TabIndex = 78;
            this.ckkIsB641.Text = "Es Base64";
            this.ckkIsB641.UseVisualStyleBackColor = true;
            this.ckkIsB641.Visible = false;
            // 
            // btnLoad1
            // 
            this.btnLoad1.Location = new System.Drawing.Point(867, 22);
            this.btnLoad1.Name = "btnLoad1";
            this.btnLoad1.Size = new System.Drawing.Size(75, 23);
            this.btnLoad1.TabIndex = 77;
            this.btnLoad1.Text = "Load Image";
            this.btnLoad1.UseVisualStyleBackColor = true;
            this.btnLoad1.Visible = false;
            this.btnLoad1.Click += new System.EventHandler(this.btnLoad1_Click);
            // 
            // txtCamara
            // 
            this.txtCamara.Location = new System.Drawing.Point(128, 29);
            this.txtCamara.Name = "txtCamara";
            this.txtCamara.Size = new System.Drawing.Size(100, 20);
            this.txtCamara.TabIndex = 76;
            this.txtCamara.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 75;
            this.label1.Text = "Camara: ";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1385, 264);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 74;
            this.button4.Text = "Borrar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(974, 264);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 73;
            this.button2.Text = "Borrar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnCapture2
            // 
            this.btnCapture2.Location = new System.Drawing.Point(1102, 22);
            this.btnCapture2.Name = "btnCapture2";
            this.btnCapture2.Size = new System.Drawing.Size(75, 23);
            this.btnCapture2.TabIndex = 71;
            this.btnCapture2.Text = "Capture";
            this.btnCapture2.UseVisualStyleBackColor = true;
            this.btnCapture2.Click += new System.EventHandler(this.btnCapture2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(412, 84);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 70;
            this.button3.Text = "Stop";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnCapture1
            // 
            this.btnCapture1.Location = new System.Drawing.Point(691, 26);
            this.btnCapture1.Name = "btnCapture1";
            this.btnCapture1.Size = new System.Drawing.Size(75, 23);
            this.btnCapture1.TabIndex = 68;
            this.btnCapture1.Text = "Capture";
            this.btnCapture1.UseVisualStyleBackColor = true;
            this.btnCapture1.Click += new System.EventHandler(this.btnCapture1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(412, 55);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 67;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Image = global::Biometrika.Tech5.Api.Test.Properties.Resources.LogoTech5;
            this.pictureBox2.Location = new System.Drawing.Point(1179, 21);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(314, 63);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = global::Biometrika.Tech5.Api.Test.Properties.Resources.Biometrika_Bajada_Big;
            this.pictureBox1.Location = new System.Drawing.Point(68, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(420, 83);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // picDedo5
            // 
            this.picDedo5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picDedo5.Location = new System.Drawing.Point(1190, 105);
            this.picDedo5.Name = "picDedo5";
            this.picDedo5.Size = new System.Drawing.Size(98, 112);
            this.picDedo5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDedo5.TabIndex = 120;
            this.picDedo5.TabStop = false;
            // 
            // picDedo4
            // 
            this.picDedo4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picDedo4.Location = new System.Drawing.Point(1086, 91);
            this.picDedo4.Name = "picDedo4";
            this.picDedo4.Size = new System.Drawing.Size(98, 112);
            this.picDedo4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDedo4.TabIndex = 117;
            this.picDedo4.TabStop = false;
            // 
            // picDedo3
            // 
            this.picDedo3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picDedo3.Location = new System.Drawing.Point(982, 76);
            this.picDedo3.Name = "picDedo3";
            this.picDedo3.Size = new System.Drawing.Size(98, 112);
            this.picDedo3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDedo3.TabIndex = 114;
            this.picDedo3.TabStop = false;
            // 
            // picDedo2
            // 
            this.picDedo2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picDedo2.Location = new System.Drawing.Point(878, 91);
            this.picDedo2.Name = "picDedo2";
            this.picDedo2.Size = new System.Drawing.Size(98, 112);
            this.picDedo2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDedo2.TabIndex = 111;
            this.picDedo2.TabStop = false;
            // 
            // picDedo1
            // 
            this.picDedo1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picDedo1.Location = new System.Drawing.Point(774, 105);
            this.picDedo1.Name = "picDedo1";
            this.picDedo1.Size = new System.Drawing.Size(98, 112);
            this.picDedo1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDedo1.TabIndex = 108;
            this.picDedo1.TabStop = false;
            // 
            // picDedo6
            // 
            this.picDedo6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picDedo6.Location = new System.Drawing.Point(448, 105);
            this.picDedo6.Name = "picDedo6";
            this.picDedo6.Size = new System.Drawing.Size(98, 112);
            this.picDedo6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDedo6.TabIndex = 104;
            this.picDedo6.TabStop = false;
            // 
            // picDedo7
            // 
            this.picDedo7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picDedo7.Location = new System.Drawing.Point(344, 91);
            this.picDedo7.Name = "picDedo7";
            this.picDedo7.Size = new System.Drawing.Size(98, 112);
            this.picDedo7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDedo7.TabIndex = 101;
            this.picDedo7.TabStop = false;
            // 
            // picDedo8
            // 
            this.picDedo8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picDedo8.Location = new System.Drawing.Point(240, 76);
            this.picDedo8.Name = "picDedo8";
            this.picDedo8.Size = new System.Drawing.Size(98, 112);
            this.picDedo8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDedo8.TabIndex = 98;
            this.picDedo8.TabStop = false;
            // 
            // picDedo9
            // 
            this.picDedo9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picDedo9.Location = new System.Drawing.Point(136, 91);
            this.picDedo9.Name = "picDedo9";
            this.picDedo9.Size = new System.Drawing.Size(98, 112);
            this.picDedo9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDedo9.TabIndex = 95;
            this.picDedo9.TabStop = false;
            // 
            // picDedo10
            // 
            this.picDedo10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picDedo10.Location = new System.Drawing.Point(32, 105);
            this.picDedo10.Name = "picDedo10";
            this.picDedo10.Size = new System.Drawing.Size(98, 112);
            this.picDedo10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDedo10.TabIndex = 85;
            this.picDedo10.TabStop = false;
            // 
            // picImage2
            // 
            this.picImage2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picImage2.Location = new System.Drawing.Point(1102, 55);
            this.picImage2.Name = "picImage2";
            this.picImage2.Size = new System.Drawing.Size(358, 203);
            this.picImage2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picImage2.TabIndex = 72;
            this.picImage2.TabStop = false;
            // 
            // picImage1
            // 
            this.picImage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picImage1.Location = new System.Drawing.Point(691, 55);
            this.picImage1.Name = "picImage1";
            this.picImage1.Size = new System.Drawing.Size(358, 203);
            this.picImage1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picImage1.TabIndex = 69;
            this.picImage1.TabStop = false;
            // 
            // imageCam
            // 
            this.imageCam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageCam.Location = new System.Drawing.Point(66, 55);
            this.imageCam.Name = "imageCam";
            this.imageCam.Size = new System.Drawing.Size(340, 203);
            this.imageCam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageCam.TabIndex = 66;
            this.imageCam.TabStop = false;
            // 
            // picResult
            // 
            this.picResult.Image = global::Biometrika.Tech5.Api.Test.Properties.Resources.negative;
            this.picResult.Location = new System.Drawing.Point(360, 112);
            this.picResult.Name = "picResult";
            this.picResult.Size = new System.Drawing.Size(65, 66);
            this.picResult.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResult.TabIndex = 123;
            this.picResult.TabStop = false;
            this.picResult.Visible = false;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(543, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(558, 75);
            this.label15.TabIndex = 76;
            this.label15.Text = "Demostración básica de uso e integración de ABIS Tech5...";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1534, 994);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labDedo1);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Biometrika Tech5 ABIS Demo v1...";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabDirecto.ResumeLayout(false);
            this.tabDirecto.PerformLayout();
            this.labDedo1.ResumeLayout(false);
            this.labDedo1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picResult)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabDirecto;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.RichTextBox rtbResultDirecto;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUrlT5;
        private System.Windows.Forms.TabPage tabViaBP;
        private System.Windows.Forms.CheckBox chkSaveHDD;
        private System.Windows.Forms.GroupBox labDedo1;
        private System.Windows.Forms.CheckBox ckkIsB642;
        private System.Windows.Forms.Button btnLoad2;
        private System.Windows.Forms.CheckBox ckkIsB641;
        private System.Windows.Forms.Button btnLoad1;
        private System.Windows.Forms.TextBox txtCamara;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox picImage2;
        private System.Windows.Forms.Button btnCapture2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox picImage1;
        private System.Windows.Forms.Button btnCapture1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox imageCam;
        private System.Windows.Forms.Button btnEnroll;
        private System.Windows.Forms.Button btnDirExtract;
        private System.Windows.Forms.Button btnVerify;
        private System.Windows.Forms.Button btnDirIdentify;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox picDedo10;
        private System.Windows.Forms.Button btnDedo10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox picDedo5;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox picDedo4;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox picDedo3;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox picDedo2;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox picDedo1;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox picDedo6;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox picDedo7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox picDedo8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox picDedo9;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label labResult;
        private System.Windows.Forms.PictureBox picResult;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPersonId;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnCreateFolder;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label15;
    }
}

