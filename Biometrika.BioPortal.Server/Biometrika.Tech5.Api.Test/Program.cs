﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Tech5.Api.Test
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //string s1 = "{\"finger\":{\"FINGER_T5\":{\"10000000000080089\":20.0,\"1000000000000053\":3.1874659061431885}},\"iris\":{\"IRIS_T5\":{\"100000089\":20.0}},\"face\":{\"FACE_T5\":{\"10000000000080089\":19.865537643432617}},\"scores\":{\"fused_scores\":{\"1000e038\":20.0,\"100000089\":19.959661293029786,\"100000000001\":19.959661293029786,\"10000000000080089\":19.946215057373045,\"1000000000008j0089\":19.946215057373045,\"10000089\":19.946215057373045,\"1000000000000003003\":0.8389443159103394,\"10000000000000140\":0.5996870398521423}}}";
            //s1 = s1.Replace("\\", "");
            //Models.T5ResponseIdentifyInternal obj = JsonConvert.DeserializeObject<Tech5.Api.Models.T5ResponseIdentifyInternal>(s1);
            //JObject jsonObj = JObject.Parse(obj.finger.FINGER_T5.ToString());
            //Dictionary<string, string> dictObj = jsonObj.ToObject<Dictionary<string, string>>();

            //Models.T5IdentifyParamOut pout = new Models.T5IdentifyParamOut(null, null, null, null, null, null, null, 0, null, 0, 0, 0);
            //pout.Candidates = new List<Models.T5IdentifyResponse>();
            //foreach (string key in dictObj.Keys)
            //{
            //    Models.T5IdentifyResponse cand = new Models.T5IdentifyResponse();
            //    cand.tid = "tid";
            //    cand.encounter_id = key;
            //    cand.type = "Finger";
            //    cand.score = double.Parse(dictObj[key], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
            //    pout.Candidates.Add(cand);
            //}

            //string s1 = "{\"finger\":{\"FINGER_T5\":{\"10000000000080089\":20.0}},\"iris\":{\"IRIS_T5\":{\"100000089\":20.0,}},\"face\":{\"FACE_T5\":{\"10000000000080089\":19.865537643432617,}},\"scores\":{\"fused_scores\":{\"1000e038\":20.0,\"100000089\":19.959661293029786,\"100000000001\":19.959661293029786,\"10000000000080089\":19.946215057373045,\"1000000000008j0089\":19.946215057373045,\"10000089\":19.946215057373045,\"1000000000000003003\":0.8389443159103394,\"10000000000000140\":0.5996870398521423}}}";
            //string s1 = "{\"finger\":{},\"face\":{\"FACE_T5\":{\"21284415-2\":9.238061904907227}},\"scores\":{\"fused_scores\":{\"21284415-2\":9.238061904907227}}}";
            //string s2 = s1.Substring(s1.IndexOf("fused_scores")+14);
            //string s3 = s2.Replace("}}", "");
            //string s4 = s3.Replace("\\", ""); 
            //string[] sarr = s4.Split(',');
            //string s = "{\"finger\":{\"FINGER_T5\":{\"21284415-2\":20.0,\"1000000000000053\":3.1874659061431885}},\"face\":{\"FACE_T5\":{\"21284415-2\":9.238061904907227}},\"scores\":{\"fused_scores\":{\"21284415-2\":15.69522476196289,\"1000000000000053\":3.1874659061431885}}}";



            //string s = "{\"finger\":{\"FINGER_T5\":{\"314348be-3a33-4d19-a254-7ed53d5b6f16\":20.0}},\"face\":{\"FACE_T5\":{\"314348be-3a33-4d19-a254-7ed53d5b6f16\":11.2865715}}}";
            //string[] sarr = s.Split(',');
            //string[] sarr2 = sarr[0].Split(':');
            //string score = sarr2[sarr2.Length - 1].Replace("}", "");
            //score = "20.5";
            ////score = score.Replace(".", ",");
            //double score2 = double.Parse(score, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture) ;
            //double score3 = 20.5;
            int i = 0;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
