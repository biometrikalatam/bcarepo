﻿using Biometrika.BioApi20;
using Biometrika.BioApi20.Interfaces;
using Biometrika.Tech5.Api.Models;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Tech5.Api.Test
{

    public partial class frmMain : Form
    {
        ABISTech5Client _T5_CLIENT = new ABISTech5Client();

        //Para capture de Face
        private Capture _CAPTURECAM;
        string _STR_IMAGE1;
        string _STR_IMAGE2;

        //Para capture de Finger
        string[] _STR_FINGER = new string[10];
        //string _STR_FINGER2;
        //string _STR_FINGER3;
        //string _STR_FINGER4;
        //string _STR_FINGER5;
        //string _STR_FINGER6;
        //string _STR_FINGER7;
        //string _STR_FINGER8;
        //string _STR_FINGER9;
        //string _STR_FINGER10;

        int _CURRENT_FINGER_CAPTURING = 1;

        Biometrika.BioApi20.BSP.BSPBiometrika BSP;
        List<Sample> _SamplesCaptured;
        byte[] _Sample_Standard;

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            InitBSP();
        }

        private void InitBSP()
        {
            BSP = new Biometrika.BioApi20.BSP.BSPBiometrika();

            BSP.BSPAttach("2.0", true);

            GetInfoFromBSP();

            BSP.OnCapturedEvent += OnCaptureEvent;
            BSP.OnTimeoutEvent += OnTimeoutEvent;
            AddLog(0, 0, "BPS Inicializado...");
        }

        public void GetInfoFromBSP()
        {
            List<BFPListElement> listBFP = BSP.QueryBFPs();

            foreach (BFPListElement item in listBFP)
            {
                comboBox1.Items.Add(item.SerialSensor + "    | " + GetTec(item.AuthenticationFactor) + " | " + item.Brand);
            }
            if (listBFP!= null && listBFP.Count > 0)
            {
                comboBox1.SelectedIndex = 0;
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            rtbResultDirecto.Text = "";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            string msg;
            int ret = _T5_CLIENT.Initialize(txtUrlT5.Text, "info@biometrika.cl", "B1ometrik@",
                                           "web_app", "42dbc72e-bf12-4773-bea8-bb31692625eb", out msg, 60000);
            rtbResultDirecto.Text = "Set service Tech5 en: " + txtUrlT5.Text + " => ret = " + ret.ToString() + " => [msgFeedback=" +
                                (string.IsNullOrEmpty(msg) ? "" : msg) + "]";
        }

        #region Face Sample 
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                _CAPTURECAM = new Capture(Convert.ToInt32(txtCamara.Text.Trim())); //(HelperCamera.SelectCamera(_DRCONFIG.CAMCameraDocSelected));
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 1280);
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 720);
                Application.Idle += ShowCamStream;
            }
            catch (Exception ex)
            {
                //LOG.Error("DRUI.labCamStart1_Click Error: " + ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            StopCamStream();
        }

        private void StopCamStream()
        {
            //LOG.Info("DRUI.StopCamStream IN...");
            try
            {
                Application.Idle -= ShowCamStream;
                _CAPTURECAM.Stop();
                _CAPTURECAM.Dispose();
            }
            catch (Exception ex)
            {
                //LOG.Error("DRUI.StopCamStream Error = " + ex.Message, ex);
            }
            //LOG.Info("DRUI.StopCamStream OUT!");
        }

        private void ShowCamStream(object sender, EventArgs e)
        {
            try
            {
                //paperDocument.DrawRectangle(pencil, 10, 10, imageCam.Im.Width - 10, imageCam.Height - 10);
                //paperDocument.DrawRectangle(pencil, 10, 10, 430, 250);
                Mat mat = new Mat();
                _CAPTURECAM.Retrieve(mat);
                Image<Bgr, Byte> img = mat.ToImage<Bgr, Byte>();
                //Image<Bgr, Byte> frame = img.Resize(1024, 768, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR).Copy();
                imageCam.Image = img.ToBitmap();
            }
            catch (Exception ex)
            {
                //LOG.Error("DRUI.ShowCamStream Error = " + ex.Message, ex);
            }
        }

        private void btnCapture1_Click(object sender, EventArgs e)
        {
            picImage1.Image = imageCam.Image;
            _STR_IMAGE1 = GetB64FromPictureBox(picImage1);
        }

        private string GetB64FromPictureBox(PictureBox pic)
        {
            return ImageToBase64(pic.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        public string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            string base64String = null;
            //LOG.Debug("DRUI.ImageToBase64 IN...");
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    image.Save(ms, format);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    base64String = Convert.ToBase64String(imageBytes);
                    //LOG.Debug("CIUIAdapter.ImageToBase64 base64String = " + base64String);
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("CIUIAdapter.ImageToBase64 Error = " + ex.Message, ex);
                base64String = null;
            }
            //LOG.Debug("CIUIAdapter.ImageToBase64 OUT!");
            return base64String;
        }

        private void btnCapture2_Click(object sender, EventArgs e)
        {
            picImage2.Image = imageCam.Image;
            _STR_IMAGE2 = GetB64FromPictureBox(picImage2);
        }

        #endregion Face Sample 

        #region Operation

        private void btnDirExtract_Click(object sender, EventArgs e)
        {
            try
            {
                FillMessageFeedback(false, 0, null);
                if (_T5_CLIENT == null || !_T5_CLIENT.IsInitialized)
                {
                    button10_Click(null, null);
                }

                if (string.IsNullOrEmpty(_STR_IMAGE1) && !ExistFingerCaptured(_STR_FINGER))
                {
                    MessageBox.Show("Debe tomar una muestra valida...", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                } else
                {
                    Tech5.Api.Models.T5ExtractParamIn pin = new Models.T5ExtractParamIn();

                    //Agrego Fotos si hay
                    pin.tid = Guid.NewGuid().ToString("N");

                    if (!string.IsNullOrEmpty(_STR_IMAGE1))
                    {
                        pin.face_data = new List<Models.FaceDatum>();
                        Models.FaceDatum faceDatum = new Models.FaceDatum();
                        faceDatum.pos = "F";
                        faceDatum.data = _STR_IMAGE1;
                        pin.face_data.Add(faceDatum);
                    }
                     
                    if (ExistFingerCaptured(_STR_FINGER))
                    {
                        pin.finger_data = new FingerData();
                        pin.finger_data.live_scan_plain = new List<LiveScanPlain>();
                        for (int i = 0; i < _STR_FINGER.Length; i++)
                        {
                            LiveScanPlain finger = new LiveScanPlain();
                            if (!string.IsNullOrEmpty(_STR_FINGER[i]))
                            {
                                finger.pos = i.ToString();
                                finger.data = _STR_FINGER[i];
                                pin.finger_data.live_scan_plain.Add(finger);
                            }
                        }
                    }

                    Tech5.Api.Models.T5ExtractParamOut pout = null;
                    string msgerr;
                    int ret = _T5_CLIENT.Extract(pin, out pout, out msgerr);
                    if (ret == 0)
                    {
                        AddLog(1, 0, "Procesado Extract OK! tid = " + pout.tid);
                        foreach (FaceDatumOut item in pout.face_data)
                        {
                            AddLog(1, 0, "   => Pos = " + item.pos + " - Quality = " + item.quality + 
                                         " - Template = " + item.template.Substring(0, 15) + "...");
                        }

                        if (pout.finger_data != null && pout.finger_data.live_scan_plain != null &&
                            pout.finger_data.live_scan_plain.Count > 0) {
                            foreach (LiveScanPlainOut item in pout.finger_data.live_scan_plain)
                            {
                                AddLog(1, 0, "   => Pos = " + (item.pos+1).ToString() + " - Quality = " + item.quality +
                                             " - Template = " + item.template.Substring(0, 15) + "...");
                            }
                        }
                        FillMessageFeedback(true, 1, "OK Extract!");
                    } else
                    {
                        AddLog(1, 0, "Procesado Extract NOOK! tid = " + pout.tid + " [code = " + ret + " - " + msgerr + "]");
                        FillMessageFeedback(true, 0, "Error en proceso de Extract!");
                    }
                }
            }
            catch (Exception ex)
            {
                AddLog(1, 0, "Excp Extract => " + ex.Message);
                FillMessageFeedback(true, 0, "Excp en proceso de Extract!");
            }
        }

        private bool ExistFingerCaptured(string[] _str_finger)
        {
            bool ret = false;
            foreach (string item in _str_finger)
            {
                ret = ret || !string.IsNullOrEmpty(item);
                if (ret) break;
            }
            return ret;
        }

        #endregion Operation

        private void AddLog(int type, int destination, string msg)
        {
            if (destination == 0) //Directo
            {
                if (type == 0) rtbResultDirecto.Text = msg;
                else rtbResultDirecto.Text += Environment.NewLine + msg;
            } else //Via BP
            {
                //if (type == 0) rtbResultBP.Text = msg;
                //else rtbResultBP.Text += Environment.NewLine + msg;
            }
        }

        private void btnLoad1_Click(object sender, EventArgs e)
        {

        }

        #region Finger Capture

        private void btnCapturar_Click(object sender, EventArgs e)
        {
            Error err;

            if (BSP == null) InitBSP();

            _SamplesCaptured = null;
            string serialSelected = SetSerialSelected();

            if (!string.IsNullOrEmpty(serialSelected))
            {
                AddLog(1, 0, "Capturando dedo " + _CURRENT_FINGER_CAPTURING + " desde Sensor = " + serialSelected + "...");
                BSP.Capture(2, _CURRENT_FINGER_CAPTURING, serialSelected, 5000, 0, 50, out err);
            }
            else
            {
                AddLog(1, 0, "Seleccione sensor para la captura primero...");
            }
        }

        private string SetSerialSelected()
        {
            try
            {
                string s = (string)comboBox1.SelectedItem;
                if (string.IsNullOrEmpty(s)) return null;
                else
                {
                    return s.Split('|')[0].Trim();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private string GetTec(int authenticationFactor)
        {
            switch (authenticationFactor)
            {
                case 2:
                    return "Fingerprint";
                case 4:
                    return "Facial";
                default:
                    return "AF=" + authenticationFactor.ToString();
            }
        }

        private static string GetLast(string source, int tailLength)
        {
            if (tailLength >= source.Length)
                return source;
            return source.Substring(source.Length - tailLength);
        }

        private void OnTimeoutEvent(int errCode, string errMessage, ISensor sensor)
        {
            AddLog(1, 0, "Error capturan => " + errCode + " - " + errMessage);
        }

        private void OnCaptureEvent(int errCode, string errMessage, List<Sample> samplesCaptured)
        {
            try
            {
                _SamplesCaptured = samplesCaptured;

                Sample s;
                try
                {
                    if (samplesCaptured != null && samplesCaptured.Count > 0)
                    {
                        s = samplesCaptured[samplesCaptured.Count - 2];
                        if (s.MinutiaeType == 41)
                            ShowFinger((Bitmap)s.Data, _CURRENT_FINGER_CAPTURING);
                    }
                }
                catch (Exception ex)
                {
                    AddLog(1, 0, "Error obteniendo imagen!");
                }

                AsignWSQ(samplesCaptured, _CURRENT_FINGER_CAPTURING);
            }
            catch (Exception ex)
            {
                AddLog(1, 0, "OnCaptureEvent Excp - " + ex.Message);
            }
            
        }

        private void AsignWSQ(List<Sample> samplesCaptured, int _finger)
        {
            try
            {
                foreach (Sample sample in samplesCaptured)
                {
                    if (sample.MinutiaeType == 21) {
                        switch (_finger)
                        {
                            case 1:
                                _STR_FINGER[0] = Convert.ToBase64String((byte[])sample.Data);
                            break;
                            case 2:
                                _STR_FINGER[1] = Convert.ToBase64String((byte[])sample.Data);
                                break;
                            case 3:
                                _STR_FINGER[2] = Convert.ToBase64String((byte[])sample.Data);
                                break;
                            case 4:
                                _STR_FINGER[3] = Convert.ToBase64String((byte[])sample.Data);
                                break;
                            case 5:
                                _STR_FINGER[4] = Convert.ToBase64String((byte[])sample.Data);
                                break;
                            case 6:
                                _STR_FINGER[5] = Convert.ToBase64String((byte[])sample.Data);
                                break;
                            case 7:
                                _STR_FINGER[6] = Convert.ToBase64String((byte[])sample.Data);
                                break;
                            case 8:
                                _STR_FINGER[7] = Convert.ToBase64String((byte[])sample.Data);
                                break;
                            case 9:
                                _STR_FINGER[8] = Convert.ToBase64String((byte[])sample.Data);
                                break;
                            default: //10
                                _STR_FINGER[9] = Convert.ToBase64String((byte[])sample.Data);
                                break;
                        }
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                AddLog(1, 0, "AsignWSQ Excp - " + ex.Message);
            }
        }

        private void ShowFinger(Bitmap data, int _finger)
        {
            try
            {
                switch (_finger)
                {
                    case 1:
                        picDedo1.Image = data;
                        break;
                    case 2:
                        picDedo2.Image = data;
                        break;
                    case 3:
                        picDedo3.Image = data;
                        break;
                    case 4:
                        picDedo4.Image = data;
                        break;
                    case 5:
                        picDedo5.Image = data;
                        break;
                    case 6:
                        picDedo6.Image = data;
                        break;
                    case 7:
                        picDedo7.Image = data;
                        break;
                    case 8:
                        picDedo8.Image = data;
                        break;
                    case 9:
                        picDedo9.Image = data;
                        break;
                    default: //10
                        picDedo10.Image = data;
                        break;

                }
            }
            catch (Exception ex)
            {
                AddLog(1, 0, "ShowFinger Excp - " + ex.Message);
            }
        }

        #endregion Finger Capture

        private void button16_Click(object sender, EventArgs e)
        {
            _CURRENT_FINGER_CAPTURING = 1;
            btnCapturar_Click(null, null);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            _CURRENT_FINGER_CAPTURING = 2;
            btnCapturar_Click(null, null);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            _CURRENT_FINGER_CAPTURING = 3;
            btnCapturar_Click(null, null);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            _CURRENT_FINGER_CAPTURING = 4;
            btnCapturar_Click(null, null);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            _CURRENT_FINGER_CAPTURING = 5;
            btnCapturar_Click(null, null);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            _CURRENT_FINGER_CAPTURING = 6;
            btnCapturar_Click(null, null);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            _CURRENT_FINGER_CAPTURING = 7;
            btnCapturar_Click(null, null);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            _CURRENT_FINGER_CAPTURING = 8;
            btnCapturar_Click(null, null);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            _CURRENT_FINGER_CAPTURING = 9;
            btnCapturar_Click(null, null);
        }

        private void btnDedo10_Click(object sender, EventArgs e)
        {
            _CURRENT_FINGER_CAPTURING = 10;
            btnCapturar_Click(null, null);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < _STR_FINGER.Length; i++)
            {
                _STR_FINGER[i] = null;
            }
            picDedo1.Image = null;
            picDedo2.Image = null;
            picDedo3.Image = null;
            picDedo4.Image = null;
            picDedo5.Image = null;
            picDedo6.Image = null;
            picDedo7.Image = null;
            picDedo8.Image = null;
            picDedo9.Image = null;
            picDedo10.Image = null;
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BSP != null)
            {
                BSP.OnCapturedEvent -= OnCaptureEvent;
                BSP.OnTimeoutEvent -= OnTimeoutEvent;
            }
        }

        private void btnVerify_Click(object sender, EventArgs e)
        {
            try
            {
                FillMessageFeedback(false, 0, null);
                if (_T5_CLIENT == null || !_T5_CLIENT.IsInitialized)
                {
                    button10_Click(null, null);
                }
                bool hay2Faces = !string.IsNullOrEmpty(_STR_IMAGE1) && !string.IsNullOrEmpty(_STR_IMAGE2);
                bool hay2Fingers = !string.IsNullOrEmpty(_STR_FINGER[0]) && !string.IsNullOrEmpty(_STR_FINGER[1]);

                if (!hay2Faces && !hay2Fingers)
                {
                    MessageBox.Show("Debe tomar dos fotos o dos huellas...", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    Tech5.Api.Models.T5VerifyParamIn pin = new Models.T5VerifyParamIn();

                    //Agrego Fotos si hay
                    pin.tid = Guid.NewGuid().ToString("N");

                    if (hay2Faces)
                    {
                        pin.face_data = new List<Models.FaceDatum>();
                        Models.FaceDatum faceDatum = new Models.FaceDatum();
                        faceDatum.pos = "A";
                        faceDatum.data = _STR_IMAGE1;
                        pin.face_data.Add(faceDatum);

                        faceDatum = new Models.FaceDatum();
                        faceDatum.pos = "F";
                        faceDatum.data = _STR_IMAGE2;
                        pin.face_data.Add(faceDatum);

                    }

                    if (hay2Fingers)
                    {
                        pin.finger_data = new FingerData();
                        pin.finger_data.live_scan_plain = new List<LiveScanPlain>();
                        for (int i = 0; i < 2; i++)
                        {
                            LiveScanPlain finger = new LiveScanPlain();
                            if (!string.IsNullOrEmpty(_STR_FINGER[i]))
                            {
                                finger.pos = (i+1).ToString();
                                finger.data = _STR_FINGER[i];
                                pin.finger_data.live_scan_plain.Add(finger);
                            }
                        }
                    }

                    Tech5.Api.Models.T5VerifyParamOut pout = null;
                    string msgerr;
                    int ret = _T5_CLIENT.Verify(pin, out pout, out msgerr);
                    if (ret == 0)
                    {
                        AddLog(1, 0, "Procesado Extract OK! tid = " + pout.tid);
                        if (pout != null && pout.response != null && pout.response.Count > 0)
                        {
                            foreach (T5VerifyResponse item in pout.response)
                            {
                                AddLog(1, 0, "   => Type = " + item.type + " - Quality = " + item.score.ToString() + "/20 ...");
                            }
                            FillMessageFeedback(true, 1, "POSITIVO Verify!");
                        } else
                        {
                            AddLog(1, 0, "Verify Response vacio!");
                            FillMessageFeedback(true, 2, "NEGATIVO Verify!");
                        }
                    }
                    else
                    {
                        AddLog(1, 0, "Procesado Verify NOOK! [code = " + ret + " - " + msgerr + "]");
                        FillMessageFeedback(true, 0, "Error en el proceso de Verify!");
                    }
                }
            }
            catch (Exception ex)
            {
                FillMessageFeedback(true, 0, "Excp en el proceso de Verify!");
                AddLog(1, 0, "Excp Extract => " + ex.Message);
            }
        }

        private void btnEnroll_Click(object sender, EventArgs e)
        {
            try
            {
                FillMessageFeedback(false, 0, null);
                if (string.IsNullOrEmpty(txtPersonId.Text))
                {
                    MessageBox.Show("Ingrese un identificador de la identidad para enrolar...", "Atención...",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPersonId.Focus();
                }
                else
                {

                    if (_T5_CLIENT == null || !_T5_CLIENT.IsInitialized)
                    {
                        button10_Click(null, null);
                    }

                    if (string.IsNullOrEmpty(_STR_IMAGE1) && !ExistFingerCaptured(_STR_FINGER))
                    {
                        MessageBox.Show("Debe tomar una muestra valida...", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        Tech5.Api.Models.T5ExtractParamIn pin = new Models.T5ExtractParamIn();

                        //Agrego Fotos si hay
                        pin.tid = Guid.NewGuid().ToString("N");
                        pin.encounter_id = (string.IsNullOrEmpty(txtPersonId.Text) ? pin.tid : txtPersonId.Text);
                        if (!string.IsNullOrEmpty(_STR_IMAGE1))
                        {
                            pin.face_data = new List<Models.FaceDatum>();
                            Models.FaceDatum faceDatum = new Models.FaceDatum();
                            faceDatum.pos = "F";
                            faceDatum.data = _STR_IMAGE1;
                            pin.face_data.Add(faceDatum);
                        }

                        if (ExistFingerCaptured(_STR_FINGER))
                        {
                            pin.finger_data = new FingerData();
                            pin.finger_data.live_scan_plain = new List<LiveScanPlain>();
                            for (int i = 0; i < _STR_FINGER.Length; i++)
                            {
                                LiveScanPlain finger = new LiveScanPlain();
                                if (!string.IsNullOrEmpty(_STR_FINGER[i]))
                                {
                                    finger.pos = (i+1).ToString();
                                    finger.data = _STR_FINGER[i];
                                    pin.finger_data.live_scan_plain.Add(finger);
                                }
                            }
                        }

                        Tech5.Api.Models.T5ExtractParamOut pout = null;
                        string msgerr;
                        int ret = _T5_CLIENT.Enroll(pin, out pout, out msgerr);
                        if (ret == 0)
                        {
                            AddLog(1, 0, "Procesado Extract OK! tid = " + pout.tid);
                            foreach (FaceDatumOut item in pout.face_data)
                            {
                                AddLog(1, 0, "   => Pos = " + item.pos + " - Quality = " + item.quality +
                                             " - Template = " + item.template.Substring(0, 15) + "...");
                            }

                            if (pout.finger_data != null && pout.finger_data.live_scan_plain != null &&
                                pout.finger_data.live_scan_plain.Count > 0)
                            {
                                foreach (LiveScanPlainOut item in pout.finger_data.live_scan_plain)
                                {
                                    AddLog(1, 0, "   => Pos = " + (item.pos + 1).ToString() + " - Quality = " + item.quality +
                                                 " - Template = " + item.template.Substring(0, 15) + "...");
                                }
                            }
                            FillMessageFeedback(true, 1, "Persona con Id = " + pin.encounter_id + " enrolada correctamente!");
                        }
                        else
                        {
                            AddLog(1, 0, "Procesado Extract NOOK! tid = " + pout.tid + " [code = " + ret + " - " + msgerr + "]");
                            FillMessageFeedback(true, 0, "Persona con Id = " + pin.encounter_id + " NO enrolada!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AddLog(1, 0, "Excp Extract => " + ex.Message);
                FillMessageFeedback(true, 0, "Error en el proceso de Extract!");
            }
        }

        private void FillMessageFeedback(bool show, int flag, string msg)
        {
            //flag: 0-Error | 1-OK | 2-Negative
            try
            {
                switch (flag)
                {
                    case 1:
                        this.picResult.Image = Properties.Resources.positive;
                        break;
                    case 2:
                        this.picResult.Image = Properties.Resources.negative;
                        break;
                    default:
                        this.picResult.Image = Properties.Resources.alert;
                        break;
                }
                this.labResult.Text = msg;

                this.labResult.Visible = show;
                this.picResult.Visible = show;
                this.Refresh();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnDirIdentify_Click(object sender, EventArgs e)
        {
            try
            {
                FillMessageFeedback(false, 0, null);
                if (_T5_CLIENT == null || !_T5_CLIENT.IsInitialized)
                {
                    button10_Click(null, null);
                }
                //bool hay2Faces = !string.IsNullOrEmpty(_STR_IMAGE1) && !string.IsNullOrEmpty(_STR_IMAGE2);
                //bool hay2Fingers = !string.IsNullOrEmpty(_STR_FINGER[0]) && !string.IsNullOrEmpty(_STR_FINGER[1]);

                if (string.IsNullOrEmpty(_STR_IMAGE1) && !ExistFingerCaptured(_STR_FINGER))
                {
                    MessageBox.Show("Debe tomar dos fotos o dos huellas...", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    Tech5.Api.Models.T5IdentifyParamIn pin = new Models.T5IdentifyParamIn();

                    //Agrego Fotos si hay
                    pin.tid = Guid.NewGuid().ToString("N");

                    if (!string.IsNullOrEmpty(_STR_IMAGE1))
                    {
                        pin.face_data = new List<Models.FaceDatum>();
                        Models.FaceDatum faceDatum = new Models.FaceDatum();
                        faceDatum.pos = "A";
                        faceDatum.data = _STR_IMAGE1;
                        pin.face_data.Add(faceDatum);

                        //faceDatum = new Models.FaceDatum();
                        //faceDatum.pos = "F";
                        //faceDatum.data = _STR_IMAGE2;
                        //pin.face_data.Add(faceDatum);

                    }

                    if (ExistFingerCaptured(_STR_FINGER))
                    {
                        pin.finger_data = new FingerData();
                        pin.finger_data.live_scan_plain = new List<LiveScanPlain>();
                        for (int i = 0; i < _STR_FINGER.Length; i++)
                        {
                            LiveScanPlain finger = new LiveScanPlain();
                            if (!string.IsNullOrEmpty(_STR_FINGER[i]))
                            {
                                finger.pos = (i+1).ToString();
                                finger.data = _STR_FINGER[i];
                                pin.finger_data.live_scan_plain.Add(finger);
                            }
                        }
                    }

                    Tech5.Api.Models.T5IdentifyParamOut pout = null;
                    string msgerr;
                    DateTime dStart = DateTime.Now;
                    int ret = _T5_CLIENT.Identify(pin, out pout, out msgerr);
                    DateTime dEnd = DateTime.Now;
                    AddLog(1, 0, "Procesado Identify milisegundos => " + (dEnd-dStart).Milliseconds.ToString());
                    if (ret == 0)
                    {
                        AddLog(1, 0, "Procesado Identify OK! tid = " + pout.tid);
                        if (pout != null && pout.Candidates != null && pout.Candidates.Count > 0)
                        {
                            AddLog(1, 0, "  Cadidatos");
                            foreach (T5IdentifyResponse item in pout.Candidates)
                            {
                                AddLog(1, 0, "   => Person ID = " + item.encounter_id + 
                                             " - type = " + item.type +  
                                             " - Score = " + item.score.ToString() + "/20 ...");
                            }
                        }
                        else
                        {
                            AddLog(1, 0, "Identify Candidates vacio!");
                        }

                        if (pout != null && pout.CandidatesFused != null && pout.CandidatesFused.Count > 0)
                        {
                            AddLog(1, 0, "  Cadidatos Fusionados");
                            foreach (T5IdentifyResponse item in pout.Candidates)
                            {
                                AddLog(1, 0, "   => Person ID = " + item.encounter_id +
                                             " - Score = " + item.score.ToString() + "/20 ...");
                            }
                            FillMessageFeedback(true, 1, "Identificado!");
                        }
                        else
                        {
                            AddLog(1, 0, "Identify CandidatesFused vacio!");
                            FillMessageFeedback(true, 2, "No Identificado!");
                        }
                    }
                    else
                    {
                        AddLog(1, 0, "Procesado Identify NOOK! [code = " + ret + " - " + msgerr + "]");
                        FillMessageFeedback(true, 0, "Error en el proceso de Identify!");
                    }
                }
            }
            catch (Exception ex)
            {
                AddLog(1, 0, "Excp Identify => " + ex.Message);
                FillMessageFeedback(true, 0, "Excp en el proceso de Identify!");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            picImage2.Image = null;
            _STR_IMAGE2 = null;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            picImage1.Image = null;
            _STR_IMAGE1 = null;
        }

        private void btnCreateFolder_Click(object sender, EventArgs e)
        {

            try
            {
                FillMessageFeedback(false, 0, null);
                if (string.IsNullOrEmpty(_STR_IMAGE1) || !CompleteFingerCaptured(_STR_FINGER) ||
                    string.IsNullOrEmpty(txtPersonId.Text))
                {
                    MessageBox.Show("Debe ingresar un identificador de persona, tomar una foto y las 10 huellas...",
                                    "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    
                }
                else
                {
                    bool _GrabaFolder = true;
                    if (System.IO.Directory.Exists(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim()))
                    {
                        DialogResult ret = MessageBox.Show("Ya existe ese identificador!. Reemplaza lo existente?",
                                                          "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        _GrabaFolder = (ret == DialogResult.Yes);
                        System.IO.Directory.Delete(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim(), true);
                    }

                    if (_GrabaFolder)
                    {
                        System.IO.Directory.CreateDirectory(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim());

                        //Foto
                        System.IO.File.WriteAllBytes(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim() + "\\"
                                                     + txtPersonId.Text.Trim() + "_Face.jpg",
                                                     Convert.FromBase64String(_STR_IMAGE1));
                        //Huellas
                        System.IO.File.WriteAllBytes(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim() + "\\"
                                                     + txtPersonId.Text.Trim() + "_LT.wsq",
                                                     Convert.FromBase64String(_STR_FINGER[0]));
                        System.IO.File.WriteAllBytes(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim() + "\\"
                                                     + txtPersonId.Text.Trim() + "_LI.wsq",
                                                     Convert.FromBase64String(_STR_FINGER[1]));
                        System.IO.File.WriteAllBytes(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim() + "\\"
                                                     + txtPersonId.Text.Trim() + "_LM.wsq",
                                                     Convert.FromBase64String(_STR_FINGER[2]));
                        System.IO.File.WriteAllBytes(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim() + "\\"
                                                     + txtPersonId.Text.Trim() + "_LR.wsq",
                                                     Convert.FromBase64String(_STR_FINGER[3]));
                        System.IO.File.WriteAllBytes(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim() + "\\"
                                                     + txtPersonId.Text.Trim() + "_LL.wsq",
                                                     Convert.FromBase64String(_STR_FINGER[4]));

                        System.IO.File.WriteAllBytes(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim() + "\\"
                                                     + txtPersonId.Text.Trim() + "_RT.wsq",
                                                     Convert.FromBase64String(_STR_FINGER[5]));
                        System.IO.File.WriteAllBytes(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim() + "\\"
                                                     + txtPersonId.Text.Trim() + "_RI.wsq",
                                                     Convert.FromBase64String(_STR_FINGER[6]));
                        System.IO.File.WriteAllBytes(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim() + "\\"
                                                     + txtPersonId.Text.Trim() + "_RM.wsq",
                                                     Convert.FromBase64String(_STR_FINGER[7]));
                        System.IO.File.WriteAllBytes(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim() + "\\"
                                                     + txtPersonId.Text.Trim() + "_RR.wsq",
                                                     Convert.FromBase64String(_STR_FINGER[8]));
                        System.IO.File.WriteAllBytes(Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim() + "\\"
                                                     + txtPersonId.Text.Trim() + "_RL.wsq",
                                                     Convert.FromBase64String(_STR_FINGER[9]));

                        AddLog(1, 0, "Procesado Create Folder! => " + Application.StartupPath + "\\FolderToEnroll\\" + txtPersonId.Text.Trim());
                    }


                }
            }
            catch (Exception ex)
            {
                AddLog(1, 0, "Excp CreateFolder => " + ex.Message);
            }
        }

        private bool CompleteFingerCaptured(string[] _str_finger)
        {
            bool ret = true;
            foreach (string item in _str_finger)
            {
                ret = ret || !string.IsNullOrEmpty(item);
                if (!ret) break;
            }
            return ret; 
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                FillMessageFeedback(false, 0, null);
                if (string.IsNullOrEmpty(txtPersonId.Text))
                {
                    MessageBox.Show("Ingrese un identificador de la identidad para eliminar...", "Atención...",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPersonId.Focus();
                }
                else
                {

                    if (_T5_CLIENT == null || !_T5_CLIENT.IsInitialized)
                    {
                        button10_Click(null, null);
                    }

                    //if (string.IsNullOrEmpty(_STR_IMAGE1) && !ExistFingerCaptured(_STR_FINGER))
                    //{
                    //    MessageBox.Show("Debe tomar una muestra valida...", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //}
                    //else
                    //{
                        Tech5.Api.Models.T5DeleteParamIn pin = new Models.T5DeleteParamIn();

                        //Agrego Fotos si hay
                        pin.tid = Guid.NewGuid().ToString("N");
                        pin.encounter_id = (string.IsNullOrEmpty(txtPersonId.Text) ? pin.tid : txtPersonId.Text);
                        
                        Tech5.Api.Models.T5ExtractParamOut pout = null;
                        string msgerr;
                        int ret = _T5_CLIENT.Delete(pin, out msgerr);
                        if (ret == 0)
                        {
                            AddLog(1, 0, "Procesado Delete OK! tid = " + pin.tid + " - Id Persona = " + txtPersonId.Text + 
                                    " - Resultado = " + msgerr);
                            FillMessageFeedback(true, 1, "Id Persona = " + txtPersonId.Text + " Eliminada!");
                        }
                        else
                        {
                            AddLog(1, 0, "Procesado Extract Delete! tid = " + pin.tid + " [code = " + ret + " - " + msgerr + "]");
                            FillMessageFeedback(true, 0, "Error en proceso de Delete!");
                        }
                    //}
                }
            }
            catch (Exception ex)
            {
                AddLog(1, 0, "Excp Extract => " + ex.Message);
                FillMessageFeedback(true, 0, "Excp en proceso de Delete!");
            }
        }
    }
}
