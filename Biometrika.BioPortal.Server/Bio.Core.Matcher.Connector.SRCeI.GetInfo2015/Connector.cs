﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.ServiceModel.Security.Tokens;
using System.Text;
using Bio.Core.Api;
using Bio.Core.Api.Constant;
using Bio.Core.Api.Matcher.Interface;
using Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.cl.registrocivil;
using Bio.Core.Wsq.Encoder;
using BioPortal.Server.Api;
using FreeImageAPI;
using log4net;

namespace Bio.Core.Matcher.Connector.SRCeI.GetInfo2015
{
    public class Connector : IConnector
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Connector));

#region Private properties propietario

        RUN _currenRUN;
        bool _getInfo;
        bool _getFoto;
        bool _getFirma;

        private string _urlWS = "https://terceros.sidiv.registrocivil.cl:8443/InteroperabilityPlateform/TercerosDS64ProxyService";
        private int _timeout = 30000;
        private int _idEmpresaSRCeI = 61003000;
        private string _userSRCeI = "usr_medleg";
        private string _claveSRCeI = "H%E1K!9&rC2U";
        private string _ip;
            //0 - Todo 
            //1 - Solo Info
            //2 - Solo Foto
            //3 - Solo Firma
            //4 - Info + Foto
            //5 - Info + Firma
            //6 - Foto + Firma
        private int _typeget = 0;
        private int _wFoto = 140;
        private int _hFoto = 140;
        private int _wFirma = 200;
        private int _hFirma = 85;

        private bool _isConfigured; //Solo para control en initialization
        private string _pathcertifySRCeI;
        private byte[] _certifySRCeI;
        private string _pathpvkey;
        private string _pvkey;
        private string _pathcertifcatekey;
        private string _certifcatekey;

#endregion Private properties propietario

#region Public properties propietario
        public int WFoto
        {
            get { return _wFoto; }
            set { _wFoto = value; }
        }
        public int HFoto
        {
            get { return _hFoto; }
            set { _hFoto = value; }
        }
        public int WFirma
        {
            get { return _wFirma; }
            set { _wFirma = value; }
        }
        public int HFirma
        {
            get { return _hFirma; }
            set { _hFirma = value; }
        }
        

        public string PathcertifySRCeI
        {
            get { return _pathcertifySRCeI; }
            set
            {
                _pathcertifySRCeI = value;
                byte[] _certifySRCeI = File.ReadAllBytes(this._pathcertifySRCeI);
            }
        }

        public string PathCertifcatekey
        {
            get { return _pathcertifcatekey; }
            set
            {
                _pathcertifcatekey = value;
                _certifcatekey = ReadFromHDD(this._pathcertifcatekey);
            }
        }

        public string PathPvkey
        {
            get { return _pathpvkey; }
            set
            {
                _pathpvkey = value;
                _pvkey = ReadFromHDD(this._pathpvkey);
            }
        }

        public string UrlWs
        {
            get { return _urlWS; }
            set { _urlWS = value; }
        }

        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        public int IdEmpresaSRCeI
        {
            get { return _idEmpresaSRCeI; }
            set { _idEmpresaSRCeI = value; }
        }

        public string UserSRCeI
        {
            get { return _userSRCeI; }
            set { _userSRCeI = value; }
        }

        public string ClaveSRCeI
        {
            get { return _claveSRCeI; }
            set { _claveSRCeI = value; }
        }

        public string Ip
        {
            get { return _ip; }
            set { _ip = value; }
        }

        public int TypeGet
        {
            get { return _typeget; }
            set { _typeget = value; }
        }

        #endregion Public properties propietario

#region Private Method

        /// <summary>
        /// Inicializa las variables del objeto desde config, para no perder tiempo en buquedas luego
        /// </summary>
        private void Initialization()
        {
            try
            {
                if (_config == null || _config.DynamicDataItems == null)
                {
                    _isConfigured = false;
                    return;
                }

                foreach (DynamicDataItem dd in _config.DynamicDataItems)
                {
                    if (dd.key.Trim().Equals("UrlWS"))
                    {
                        _urlWS = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("Timeout"))
                    {
                        try
                        {
                            _timeout = Convert.ToInt32(dd.value.Trim());
                        }
                        catch (Exception ex)
                        {
                            _timeout = 30000;
                        }
                    }


                    if (dd.key.Trim().Equals("IdEmpresaSRCeI"))
                    {
                        _idEmpresaSRCeI = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("UserSRCeI"))
                    {
                        _userSRCeI = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("ClaveSRCeI"))
                    {
                        _claveSRCeI = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("WFoto"))
                    {
                        _wFoto = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("HFoto"))
                    {
                        _hFoto = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("WFirma"))
                    {
                        _wFirma = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("HFirma"))
                    {
                        _hFirma = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("TypeGet"))
                    {
                        _typeget = Convert.ToInt32(dd.value.Trim());
                        if (_typeget < 0 || _typeget > 6) _typeget = 0;
                        if (_typeget == 0 || _typeget == 1 || _typeget == 4 || _typeget == 5)
                        {
                            _getInfo = true;
                        }
                        if (_typeget == 0 || _typeget == 2 || _typeget == 4 || _typeget == 6)
                        {
                            _getFoto = true;
                        }
                        if (_typeget == 0 || _typeget == 3 || _typeget == 5 || _typeget == 6)
                        {
                            _getFirma = true;
                        }
                    }
                    if (dd.key.Trim().Equals("PathCertifySRCeI"))
                    {
                        _pathcertifySRCeI = dd.value.Trim();
                        _certifySRCeI = File.ReadAllBytes(this._pathcertifySRCeI);
                    }
                    if (dd.key.Trim().Equals("PathPvKey"))
                    {
                        _pathpvkey = dd.value.Trim();
                        _pvkey = ReadFromHDD(this._pathpvkey);
                    }
                    if (dd.key.Trim().Equals("PathCertificateKey"))
                    {
                        _pathcertifcatekey = dd.value.Trim();
                        _certifcatekey = ReadFromHDD(this._pathcertifcatekey);
                    }
                }
                _ip = GetInfo2015.Utiles.GetIP();
                _isConfigured = true;
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015 Initialization Error", ex);
                _isConfigured = false;
            }
        }

        private CustomBinding GetCustomHttpBinding()
        {
            CustomBinding binding = new CustomBinding();
            // Open and Close = 20s 
            binding.OpenTimeout = new TimeSpan(0, 0, this._timeout);
            binding.CloseTimeout = new TimeSpan(0, 0, this._timeout);
            // Send and Receive = 300s
            binding.SendTimeout = new TimeSpan(0, 0, this._timeout + 10);
            binding.ReceiveTimeout = new TimeSpan(0, 0, this._timeout + 10);
            // ++ Setting security binding ++
            var userNameToken = new UserNameSecurityTokenParameters();
            userNameToken.InclusionMode = SecurityTokenInclusionMode.AlwaysToRecipient;

            SecurityBindingElement securityElement;
            securityElement = new AsymmetricSecurityBindingElement();
            (securityElement as AsymmetricSecurityBindingElement).RecipientTokenParameters = new X509SecurityTokenParameters(X509KeyIdentifierClauseType.IssuerSerial, SecurityTokenInclusionMode.Never);
            (securityElement as AsymmetricSecurityBindingElement).InitiatorTokenParameters = new X509SecurityTokenParameters(X509KeyIdentifierClauseType.IssuerSerial, SecurityTokenInclusionMode.AlwaysToRecipient);
            securityElement.DefaultAlgorithmSuite = SecurityAlgorithmSuite.Basic256;
            securityElement.SecurityHeaderLayout = SecurityHeaderLayout.Strict;

            //securityElement.MessageProtectionOrder = MessageProtectionOrder.SignBeforeEncrypt;
            //otra forma: securityElement = new TransportSecurityBindingElement();

            securityElement.AllowInsecureTransport = false;
            securityElement.EnableUnsecuredResponse = true;
            securityElement.IncludeTimestamp = false;
            securityElement.LocalClientSettings.DetectReplays = false;
            securityElement.SetKeyDerivation(true);
            securityElement.EndpointSupportingTokenParameters.Signed.Add(userNameToken);
            securityElement.MessageSecurityVersion = MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10; //.WSSecurity11WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10;
            binding.Elements.Add(securityElement);

            // ++ Setting message encoding binding ++
            var encodingElement = new TextMessageEncodingBindingElement();
            encodingElement.MessageVersion = MessageVersion.Soap11;
            encodingElement.WriteEncoding = Encoding.UTF8;
            //encodingElement.MaxReadPoolSize = 50000000;
            //encodingElement.MaxWritePoolSize = 50000000;
            encodingElement.ReaderQuotas.MaxArrayLength = 50000000;
            encodingElement.ReaderQuotas.MaxStringContentLength = 50000000;

            binding.Elements.Add(encodingElement);

            // ++ Setting https transport binding ++
            var httpsElement = new HttpsTransportBindingElement();
            // Messagge buffer size
            httpsElement.MaxBufferSize = 50000000;
            httpsElement.MaxReceivedMessageSize = 50000000;
            httpsElement.MaxBufferPoolSize = 50000000;

            // Others
            httpsElement.UseDefaultWebProxy = true;
            binding.Elements.Add(httpsElement);

            return binding;
        }

        // callback used to validate the certificate in an SSL conversation
        private static bool ValidateRemoteCertificate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors policyErrors)
        {
            bool result = false;
            string lower = cert.Subject.ToLower();
            if (lower.Contains("terceros.sidiv.registrocivil.cl") || lower.Contains("servers ca") || lower.Contains("technical root ca"))
            {
                result = true;
            }

            return result;
        }

        private byte[] ExtraeWSQFromParamIn(XmlParamIn oXmlIn)
        {
            byte[] wsqr = null;
            try
            {
                foreach (var sample in oXmlIn.SampleCollection)
                {
                    if (sample.Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                    {
                        wsqr = Convert.FromBase64String(sample.Data);
                        break;
                    }
                    else if (sample.Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW)
                    {
                        byte[] raw = Convert.FromBase64String(sample.Data);
                        WsqEncoder encoder = new WsqEncoder();
                        encoder.EncodeMemory(raw, 512, 512, out wsqr);
                        break;
                    }
                }

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.GetInfo2015 ExtraeWSQFromParamIn Error", ex);
            }
            return wsqr;
        }

#endregion Private Method

#region Implementation of IConnector

        private string _connectorId;

        private DynamicData _config;

        /// <summary>
        /// Id identificador del conector
        /// </summary>
        public string ConnectorId
        {
            get { return _connectorId; }
            set { _connectorId = value; }
        }

        /// <summary>
        /// Pares de key/value de configuracion para el conector
        /// </summary>
        public DynamicData Config
        {
            get { return _config; }
            set
            {
                _config = value;
                if (!_isConfigured) Initialization();
            }
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            xmloutput = null;
            return 0;
        }

        private string ConvertToNumber(string status)
        {
            string ret = "0";
            try
            {
                ret = status.Substring(4);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.ConvertToNumber Error", ex);
            }
            return ret;
        }

        private string ReadFromHDD(string path)
        {
            string sret = null;
            try
            {
                System.IO.StreamReader sr = new StreamReader(path);
                sret = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.ReadFromHDD Error", ex);
                sret = null;
            }
            return sret;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            
            string msg;
            xmloutput = null;
            int iretremoto;

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de Recuperación. Ingresa información recuperar, 
        /// y se realiza la operacion.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Get(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */

            string msg;
            string xmloutbp;
            int iretremoto;

            DynamicData oXmlout = new DynamicData();
            oXmlout.AddValue("message", "");
            oXmlout.AddValue("trackid", "");
            oXmlout.AddValue("status", "");
            oXmlout.AddValue("result", "0");
            oXmlout.AddValue("score", "0");
            oXmlout.AddValue("threshold", "0");
            oXmlout.AddValue("timestamp", "");
            oXmlout.AddValue("personaldata", "");


            //oXmlout.SetValue("message", "");
            //oXmlout.SetValue("trackid", "283734647");
            //oXmlout.SetValue("status", "0");
            //oXmlout.SetValue("result", "1");
            //oXmlout.SetValue("score", "1000");
            //oXmlout.SetValue("threshold", _threshold.ToString());
            //oXmlout.SetValue("timestamp", FormatFechaHoraSRCeI("23112013100000"));
            //xmloutput = DynamicData.SerializeToXml(oXmlout);
            //return Errors.IERR_OK;

            try
            {
                if (!this._isConfigured)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015 Error = Connector No Configurado!";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
                }


                //1.-Deserializo parametros de entrada
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlinput);
                if (oXmlIn == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015 Error deserealizando xmlinput";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                if (oXmlIn.PersonalData == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015 Error PersonalData Nulo en xmlinput";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    try
                    {
                        _currenRUN = RUN.ConvertirEx(oXmlIn.PersonalData.Valueid.Trim());
                        if (_currenRUN == null)
                        {
                            msg = "Bio.Core.Matcher.SRCeI.GetInfo2015 Error Parseando RUT PersonalData.ValueId (_currenRUN == null) en xmlinput";
                            LOG.Fatal(msg);
                            oXmlout.SetValue("message", msg);
                            xmloutput = DynamicData.SerializeToXml(oXmlout);
                            return Errors.IERR_BAD_PARAMETER;
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Bio.Core.Matcher.SRCeI.GetInfo2015 Error Parseando RUT PersonalData.ValueId en xmlinput";
                        LOG.Fatal(msg);
                        oXmlout.SetValue("message", msg);
                        xmloutput = DynamicData.SerializeToXml(oXmlout);
                        return Errors.IERR_UNKNOWN;
                    }
                }


                //Trust all certificates
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((senderA, certificate, chain, sslPolicyErrors) => true);

                // trust sender
                System.Net.ServicePointManager.ServerCertificateValidationCallback
                                = ((senderA, cert, chain, errors) =>
                                {
                                    string lower = cert.Subject.ToLower();
                                    return (lower.Contains("terceros.sidiv.registrocivil.cl") || lower.Contains("servers ca") || lower.Contains("technical root ca"));
                                });

                // validate cert by calling a function
                System.Net.ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateRemoteCertificate);
                DS64PortTypeClient client = new DS64PortTypeClient();
                client.Endpoint.ListenUri = new Uri(this._urlWS);
                client.ChannelFactory.Endpoint.Behaviors.Add(new RegistroCivilEndpointBehavior());
                //var vs = client.Endpoint.Behaviors.Where((i) => i.GetType().Namespace.Contains("VisualStudio"));
                //if (vs != null)
                //    client.Endpoint.Behaviors.Remove((System.ServiceModel.Description.IEndpointBehavior)vs.Single());

                client.ClientCredentials.UserName.UserName = this._userSRCeI;
                client.ClientCredentials.UserName.Password = this._claveSRCeI;

                string certificateKey = this._certifcatekey;
                string privateKey = this._pvkey;

                if (String.IsNullOrEmpty(certificateKey) == null || String.IsNullOrEmpty(privateKey))
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015 Error Certificado o Key Nulos";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Certificado y key levantados");
                Certificate virtualCertificate = new Certificate(certificateKey, privateKey, "");
                X509Certificate2 localCertificate = virtualCertificate.GetCertificateFromPEMstring(false);
                client.ChannelFactory.Credentials.ClientCertificate.Certificate = localCertificate;

                X509Certificate2 remoteCertificate = new X509Certificate2(this._certifySRCeI);
                client.ChannelFactory.Credentials.ServiceCertificate.DefaultCertificate = remoteCertificate;
                client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector client.ClientCredentials Configurado");

                Encabezado objEncabezado = SRCeIHelper.CrearEncabezado(this._idEmpresaSRCeI, this._userSRCeI, Utiles.GetIP());
                DatosDocumento objDocRet = new DatosDocumento();
                DocumentoParametro objDocParam = new DocumentoParametro();
                objDocParam.RUN = _currenRUN.Mantisa;
                objDocParam.Tipo = TipoDocumentoType.C;
                Datos dataFoto = null;
                Datos dataFirma = null;
                RunParametro runparam = new RunParametro();
                runparam.RUN = _currenRUN.Mantisa;
                EncabezadoRespuesta responseDatos = null;
                EncabezadoRespuesta responseFoto = null;
                EncabezadoRespuesta responseFirma = null;

                if (objEncabezado == null || objDocParam == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015.Connector - Error creando Encabezado y/o parametros";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Encabezado y Parametros creados");
                }

                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Ingresando a consultas..._typeget = " + _typeget);
                    //0 - Todo 
                    //1 - Solo Info
                    //2 - Solo Foto
                    //3 - Solo Firma
                    //4 - Info + Foto
                    //5 - Info + Firma
                    //6 - Foto + Firma
                if (_getInfo) { //Tomo info
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Ingresando a consulta Datos...");
                    responseDatos = client.ConsultaDocumentoDS64(objEncabezado, objDocParam, out objDocRet);
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Salio consulta Datos!");
                }

                if (_getFoto)
                { //Tomo Foto
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Ingresando a consulta Foto...");
                    responseFoto = client.ConsultaFotografiaDS64(objEncabezado, runparam, out dataFoto);
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Salio consulta Foto!");
                }

                if (_getFirma)
                { //Tomo Firma
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Ingresando a consulta Firma...");
                    responseFirma = client.ConsultaFirmaDS64(objEncabezado, runparam, out dataFirma);
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Salio consulta Firma!");
                }

                if (responseDatos == null && responseFoto == null && responseFirma == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015.Connector - Respuesta Erronea del SRCeI - " +
                      "ResponseDatos=" + (responseDatos == null ? "NULL" : "Not NULL") + " | " +
                      "ResponseFoto=" + (responseFoto == null ? "NULL" : "Not NULL") + " | " +
                      "ResponseFirma=" + (responseFirma == null ? "NULL" : "Not NULL");
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_MALFORMED_ANSWER;
                }

                if (_getInfo && responseDatos != null && responseDatos.Estado != EstadoType.Item000)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015.Connector - Respuesta Erronea del SRCeI - " +
                      "ResponseDatos.Estado=" + responseDatos.Estado.ToString();
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    oXmlout.SetValue("status", ConvertToNumber(responseDatos.Estado.ToString()));
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                }

                if (_getFoto && responseFoto != null && responseFoto.Estado != EstadoType.Item000)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015.Connector - Respuesta Erronea del SRCeI - " +
                      "ResponseFoto.Estado=" + responseFoto.Estado.ToString();
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    oXmlout.SetValue("status", ConvertToNumber(responseFoto.Estado.ToString()));
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                }

                if (_getFirma && responseFirma != null && responseFirma.Estado != EstadoType.Item000)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015.Connector - Respuesta Erronea del SRCeI - " +
                      "ResponseFirma.Estado=" + responseFirma.Estado.ToString();
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    oXmlout.SetValue("status", ConvertToNumber(responseFirma.Estado.ToString()));
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                }


                //else
                //{
                    /*
                        201 (Error técnico)
                        206 (Solicitud de formato invalido)    
                        207 (Error Seguridad)
                        301 (Datos inexistentes)
                    */
                    string _satusConcat = "0"; //Concatena estados de todas las consultas
                    string _msgerrConcat = ""; //Concatena msg errores de todas las consultas
                    PersonalData pdataOut = new PersonalData();
                    string _fechaOp = "";
                    pdataOut.Typeid = "RUT"; 
                    pdataOut.Valueid = _currenRUN.Mantisa + "-" + _currenRUN.Dv;
                    pdataOut.Verificationsource = "SRCeI";
                    if (_typeget == 0 || _typeget == 1 || _typeget == 4 || _typeget == 5) { //Proceso info
                         if (responseDatos != null) {
                             if (responseDatos.Estado == EstadoType.Item000) {
                                 //_satusConcat = "0"; //"EstadoDatos:" + responseDatos.Estado;
                                 _fechaOp = responseDatos.FechaHoraOperacion;
                                 if (objDocRet!= null) {
                                     pdataOut.Name = objDocRet.Nombres;
                                     pdataOut.Patherlastname = objDocRet.PrimerApellido;
                                     pdataOut.Motherlastname = objDocRet.SegundoApellido;
                                     pdataOut.Sex = objDocRet.Sexo.ToString();
                                     pdataOut.Documentseriesnumber = objDocRet.Serie;
                                     pdataOut.Documentexpirationdate =  DateTime.Parse(objDocRet.FechaVencimiento);
                                     pdataOut.Visatype = objDocRet.TipoVisa;
                                     pdataOut.Birthdate = DateTime.Parse(objDocRet.FechaNacimiento);
                                     pdataOut.Birthplace = objDocRet.LugarNacimiento;
                                     pdataOut.Nationality = objDocRet.PaisNacionalidad;
                                     pdataOut.Profession = objDocRet.Profesion;
                                 } else {
                                      _msgerrConcat = "Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error [objDocRet=NULL]";
                                      LOG.Error(_msgerrConcat);
                                 }
                             } else {
                                 _satusConcat = ConvertToNumber(responseDatos.Estado.ToString());
                             }
                         } else {
                             _satusConcat = "-1"; 
                         }
                    }

                    if (_getFoto) { //Proceso Foto
                         if (responseFoto != null) {
                             if (responseFoto.Estado == EstadoType.Item000)
                             {
                                  //_satusConcat = (String.IsNullOrEmpty(_satusConcat)) ? 
                                  //              "EstadoFoto:" + responseFoto.Estado : 
                                  //              _satusConcat+"|" + "EstadoFoto:" + responseFoto.Estado;
                                  _fechaOp = responseDatos.FechaHoraOperacion;
                                 if (dataFoto!= null) {
                                        pdataOut.Photography = GeneraImagenB64FromBytes(1, dataFoto.Imagen, dataFoto.Formato);
                                 } else {
                                      _msgerrConcat = (String.IsNullOrEmpty(_msgerrConcat)) ? 
                                          "Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error [dataFoto=NULL]" :
                                          _msgerrConcat + "|Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error [dataFoto=NULL]";
                                      LOG.Error(_msgerrConcat);
                                 }
                             } else {
                                  //_satusConcat = (String.IsNullOrEmpty(_satusConcat)) ? 
                                  //              "EstadoFoto:" + responseFoto.Estado : 
                                  //              _satusConcat+"|" + "EstadoFoto:" + responseFoto.Estado; 
                                 _satusConcat = ConvertToNumber(responseFoto.Estado.ToString());
                             }
                         } else {
                             //_satusConcat = (String.IsNullOrEmpty(_satusConcat)) ? 
                             //    "EstadoFoto:Null" : _satusConcat+"|EstadoFoto:Null"; 
                             _satusConcat = ConvertToNumber(responseFoto.Estado.ToString());
                         }
                    }

                    if (_getFirma)
                    { //Proceso Firma
                         if (responseFirma != null) {
                             if (responseFirma.Estado == EstadoType.Item000)
                             {
                                  //_satusConcat = (String.IsNullOrEmpty(_satusConcat)) ? 
                                  //              "EstadoFirma:" + responseFirma.Estado : 
                                  //              _satusConcat+"|" + "EstadoFirma:" + responseFirma.Estado;
                                  _fechaOp = responseDatos.FechaHoraOperacion;
                                 if (dataFoto!= null) {
                                        pdataOut.Signatureimage = GeneraImagenB64FromBytes(2, dataFirma.Imagen, dataFirma.Formato);
                                 } else {
                                      _msgerrConcat = (String.IsNullOrEmpty(_msgerrConcat)) ? 
                                          "Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error [dataFoto=NULL]" :
                                          _msgerrConcat + "|Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error [dataFirma=NULL]";
                                      LOG.Error(_msgerrConcat);
                                 }
                             } else {
                                  //_satusConcat = (String.IsNullOrEmpty(_satusConcat)) ? 
                                  //              "EstadoFirma:" + responseFirma.Estado : 
                                  //              _satusConcat+"|" + "EstadoFirma:" + responseFirma.Estado;
                                 _satusConcat = ConvertToNumber(responseFirma.Estado.ToString());
                             }
                         } else {
                             //_satusConcat = (String.IsNullOrEmpty(_satusConcat)) ? 
                             //    "EstadoFirma:Null" : _satusConcat+"|EstadoFirma:Null"; 
                             _satusConcat = ConvertToNumber(responseFirma.Estado.ToString());
                         }
                    }
                    
                    oXmlout.SetValue("message", _msgerrConcat);
                    oXmlout.SetValue("status", _satusConcat);
                    oXmlout.SetValue("trackid", objEncabezado.IdTransaccion.ToString());
                    oXmlout.SetValue("timestamp", (!String.IsNullOrEmpty(_fechaOp) ? _fechaOp : DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(pdataOut));
                //}
                xmloutput = DynamicData.SerializeToXml(oXmlout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error [" + ex.Message + "]";
                LOG.Error("Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error", ex);
                oXmlout.SetValue("message", msg);
                xmloutput = DynamicData.SerializeToXml(oXmlout);
                return Errors.IERR_UNKNOWN;
            }

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Dado un arreglo de byte[] copn una imagen que viene del SRCeI, 
        /// Lo transforma a 
        /// </summary>
        /// <param name="p"></param>
        /// <param name="nullable"></param>
        /// <returns></returns>
        private string GeneraImagenB64FromBytes(int imageType, byte[] byImage, FormatoType? formato)
        {
            string _strret = null;
            try
            {
                FIBITMAP dib = new FIBITMAP();
                FreeImage.UnloadEx(ref dib);
                IntPtr imgPtr = Marshal.AllocHGlobal(byImage.Length);
                Marshal.Copy(byImage, 0, imgPtr, byImage.Length);
                FIMEMORY fiMStream = FreeImage.OpenMemory(imgPtr, (uint)byImage.Length);
                // get the file type
                FREE_IMAGE_FORMAT fiFormat = FreeImage.GetFileTypeFromMemory(fiMStream, 0);
                // load an image from the memory stream
                // mFileHandle is a static uint property of the class
                dib = FreeImage.LoadFromMemory(fiFormat, fiMStream, 0);
                //Bitmap bFoto = FreeImage.GetBitmap(dib);
                FIBITMAP dib2;
                if (imageType == 1) //es Foto
                {
                    if (_wFoto > 0) dib2 = FreeImage.Rescale(dib, _wFoto, _hFoto, FREE_IMAGE_FILTER.FILTER_BICUBIC);
                    else dib2 = dib;
                }
                else
                { //Es Firma
                    if (_wFirma > 0) dib2 = FreeImage.Rescale(dib, _wFirma, _hFirma, FREE_IMAGE_FILTER.FILTER_BICUBIC);
                    else dib2 = dib;
                }
                byte[] buff = new byte[FreeImage.GetWidth(dib2) * FreeImage.GetHeight(dib2)];
                MemoryStream ms = new MemoryStream(buff);
                FreeImage.SaveToStream(dib2, ms, FREE_IMAGE_FORMAT.FIF_JPEG);
                _strret = Convert.ToBase64String(buff);
                ms.Close();
                Marshal.FreeHGlobal(imgPtr);
                
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error", ex);
                _strret = null;
            }
            return _strret;
        }

        private string GeneraImagenB64FromBytes1(int imageType, byte[] byImage, FormatoType? formato)
        {
            string _strret = null;
            try
            {
                FIBITMAP dib3 = new FIBITMAP();
                FreeImage.UnloadEx(ref dib3);
                IntPtr imgPtr = Marshal.AllocHGlobal(byImage.Length);
                Marshal.Copy(byImage, 0, imgPtr, byImage.Length);
                FIMEMORY fiMStream = FreeImage.OpenMemory(imgPtr, (uint)byImage.Length);
                // get the file type
                FREE_IMAGE_FORMAT fiFormat = FreeImage.GetFileTypeFromMemory(fiMStream, 0);
                // load an image from the memory stream
                // mFileHandle is a static uint property of the class
                dib3 = FreeImage.LoadFromMemory(fiFormat, fiMStream, 0);
                //Bitmap bFoto = FreeImage.GetBitmap(dib);
                FIBITMAP dib4;
                if (imageType == 1) //es Foto
                {
                    if (_wFoto > 0) dib4 = FreeImage.Rescale(dib3, _wFoto, _hFoto, FREE_IMAGE_FILTER.FILTER_BICUBIC);
                    else dib4 = dib3;
                }
                else
                { //Es Firma
                    if (_wFirma > 0) dib4 = FreeImage.Rescale(dib3, _wFirma, _hFirma, FREE_IMAGE_FILTER.FILTER_BICUBIC);
                    else dib4 = dib3;
                }
                byte[] buff2 = new byte[FreeImage.GetWidth(dib4) * FreeImage.GetHeight(dib4)];
                MemoryStream ms = new MemoryStream(buff2);
                FreeImage.SaveToStream(dib4, ms, FREE_IMAGE_FORMAT.FIF_JPEG);
                _strret = Convert.ToBase64String(buff2);
                ms.Close();
                Marshal.FreeHGlobal(imgPtr);

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error", ex);
                _strret = null;
            }
            return _strret;
        }

#endregion Implementation of IConnector

        private static string FormatFechaHoraSRCeI(string fechaoperacion)
        {
            string strRet = "";
            try
            {
                strRet = fechaoperacion.Substring(6, 2) + "/" +
                         fechaoperacion.Substring(4, 2) + "/" +
                         fechaoperacion.Substring(0, 4) + " " +
                         fechaoperacion.Substring(8, 2) + ":" +
                         fechaoperacion.Substring(10, 2) + ":" +
                         fechaoperacion.Substring(12, 2);
            }
            catch
            {
                strRet = "";
            }
            return strRet;
        }

        public void Dispose()
        {

        }
    }
}
