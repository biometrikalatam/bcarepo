﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.cl.registrocivil;
//using Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.SRCeI;
using log4net;

namespace Bio.Core.Matcher.Connector.SRCeI.GetInfo2015
{

    internal class SRCeIHelper
    {
        /// <summary>
        /// Generación de variable estática para escritura de Log, de acuerdo
        /// a la documentación de log4net.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(typeof(SRCeIHelper));


        internal static Encabezado CrearEncabezado(int idEmpresa, string user, string ip) //string clave, string ip)
        {
            Encabezado objEncabezado;
            try
            {
                objEncabezado = new Encabezado();
                objEncabezado.Empresa = idEmpresa;
                objEncabezado.IdTransaccion = Convert.ToInt32(DateTime.Now.ToString("yyMMddhhmm"));
                objEncabezado.UsuarioFinal = user;
                objEncabezado.IpEstacionUsuarioFinal = ip;
            }
            catch (Exception ex)
            {
                objEncabezado = null;
                log.Error("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.SRCeIHelper.Encabezado", ex);
            }
            return objEncabezado;
        }
   //DocumentoParametro objDoc0 = new DocumentoParametro();
        //objDoc0.RUN = 21284415;
        //objDoc0.Tipo = TipoDocumentoType.C;
        //objDoc0.TipoSpecified = true;
        internal static AFISImpresionDactilarParametro CrearParametros(RUN run, int dedo, byte[] wsq)
        {
            AFISImpresionDactilarParametro objParameter;
            try
            {
                objParameter = new AFISImpresionDactilarParametro();
                objParameter.ImagenImpresionDactilar = new Datos();
                objParameter.ImagenImpresionDactilar.Formato = FormatoType.Item3;
                objParameter.ImagenImpresionDactilar.Imagen = wsq;
                objParameter.NumeroDedo = dedo;
                objParameter.RUN = run.Mantisa;
            }
            catch (Exception ex)
            {
                objParameter = null;
                log.Error("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.SRCeIHelper.AFISImpresionDactilarParametro", ex);
            }

            return objParameter;
        }

    }
}
