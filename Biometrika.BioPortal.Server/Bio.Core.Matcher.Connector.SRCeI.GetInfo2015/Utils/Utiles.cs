using System;
using System.Net;
using log4net;

namespace Bio.Core.Matcher.Connector.SRCeI.GetInfo2015
{
	/// <summary>
	/// Summary description for Utiles.
	/// </summary>
	public class Utiles
	{

        private static readonly ILog LOG = LogManager.GetLogger(typeof(Utiles));

        internal static string GetIP()
        {
            String sIP = "";
            bool isFirst = true;
            try
            {
                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
                foreach (IPAddress ipAddress in localIPs)
                {
                    if (ipAddress.ToString().Length <= 15)
                    {
                        sIP = ipAddress.ToString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.GetIP Error - " + ex.Message);
            }
            return sIP;
        }


		public static RUN ValidarRUN (string run)
		{
			return RUN.Crear (run);
		}
	}
}
