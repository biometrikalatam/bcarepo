﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Bio.Core.Matcher.Connector.SRCeI.GetInfo2015
{
    class RegistroCivilHeaders
    {
    }
    public class SignedXmlWithId : SignedXml
    {
        public SignedXmlWithId(XmlDocument xml)
            : base(xml)
        {
        }

        public SignedXmlWithId(XmlElement xmlElement)
            : base(xmlElement)
        {
        }

        public override XmlElement GetIdElement(XmlDocument doc, string id)
        {
            // check to see if it's a standard ID reference
            XmlElement idElem = base.GetIdElement(doc, id);

            if (idElem == null)
            {
                // I've just hardcoded it for the time being, but should be using an XPath expression here, and the id that is passed in
                idElem = (XmlElement)doc.GetElementsByTagName("Body", doc.FirstChild.NamespaceURI)[0];
            }

            return idElem;
        }
    }

    class RegistroCivilMessageInspector : IClientMessageInspector
    {
        // PREFIXES
        static string PREFIX_WS_SECURITY = "wsse";
        static string PREFIX_WS_UTILITIES = "wsu";
        static string PREFIX_DIGITAL_SIGNATURE = "dsig";
        // NAMESPACES
        static string NAMESPACE_WS_SECURITY = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
        static string NAMESPACE_DIGITAL_SIGNATURE = "http://www.w3.org/2000/09/xmldsig#";
        static string NAMESPACE_WS_UTILITIES = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";

        /// <summary>
        /// Enables inspection or modification of a message before a request message is sent to a service.
        /// </summary>
        /// <param name="request">The message to be sent to the service.</param>
        /// <param name="channel">The WCF client object channel.</param>
        /// <returns>
        /// The object that is returned as the <paramref name="correlationState " /> argument of
        /// the <see cref="M:System.ServiceModel.Dispatcher.IClientMessageInspector.AfterReceiveReply(System.ServiceModel.Channels.Message@,System.Object)" /> method.
        /// This is null if no correlation state is used.The best practice is to make this a <see cref="T:System.Guid" /> to ensure that no two
        /// <paramref name="correlationState" /> objects are the same.
        /// </returns>
        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
        {
            string xml = request.ToString();
            // load the body of the message in an xml document
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = false;
            doc.LoadXml(xml);

            string ns = doc.FirstChild.NamespaceURI;

            //Get the header element, so that we can add the digital signature to it
            XmlElement headerNode = (XmlElement)doc.GetElementsByTagName("Header", ns)[0];

            XmlElement securityNode = doc.CreateElement("Security", NAMESPACE_WS_SECURITY);
            securityNode.Prefix = PREFIX_WS_SECURITY;
            securityNode.SetAttribute("MustUnderstand", "", "1");

            // Set the ID attribute on the body element, so that we can reference it later
            XmlElement bodyNode = (XmlElement)doc.GetElementsByTagName("Body", ns)[0];

            string id = Guid.NewGuid().ToString();
            bodyNode.RemoveAllAttributes();
            bodyNode.SetAttribute("xmlns:wsu", NAMESPACE_WS_UTILITIES);
            bodyNode.SetAttribute("Id", NAMESPACE_WS_UTILITIES, id);

            XmlWriterSettings settings2 = new XmlWriterSettings();
            settings2.Encoding = new System.Text.UTF8Encoding(false);
// ---------------------------
            string certificateKey = @"-----BEGIN CERTIFICATE-----
MIIFDzCCAvegAwIBAgIINm9RAwAZs7kwDQYJKoZIhvcNAQEFBQAwUTELMAkGA1UE
BhMCQ0wxGjAYBgNVBAoMEVJlcHVibGljIG9mIENoaWxlMREwDwYDVQQLDAhDSElM
RVNJQzETMBEGA1UEAwwKU2VydmVycyBDQTAeFw0xNDA4MTgyMjI5MDBaFw0xNzA4
MTcyMjI5MDBaMIGVMQswCQYDVQQGEwJDTDELMAkGA1UECAwCUk0xETAPBgNVBAcM
CFNhbnRpYWdvMR4wHAYDVQQKDBVzZXJ2aWNpbyBtZWRpY28gbGVnYWwxMjAwBgNV
BAsMKWRlcGFydGFtZW50byBkZSBjb21wdXRhY2lvbiBlIGluZm9ybWF0aWNhMRIw
EAYDVQQDDAlzbWwgZmlybWEwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB
AQCtlm5bT4BCDbVDyjr4HUApaw0ZqVn0EMZpkZqpGcShbI2Ou90KZ2QeWvqX+s6g
Ku9ER/MlVf/H+r4nxPRhrc518C2mIsAfvLIWgHRKbchOf4Vlz2GJYVJ6793kx1iv
c8rEdpmHjW9LXmAEQ2EL1lDiJbNpaoa98rv/SnrGOcG7ltUE5dC6bXPJzkuRiqZk
p8GCrkElB88tdGY/RSAyru7HIQABg44zizksI/sMNrfOS7TBjn8G3NrPqHbyjnSb
NNeo6ry+q2ttBscmIaXg0sGw3HXihZgF60s6e5FcQDIxI6XVcIm8rrYgKPWmm7e7
Ch7r5jJTxEx5BLiDszeKXQelAgMBAAGjgaUwgaIwHQYDVR0OBBYEFIVsW8er2Nxk
uNhozZuG/FbKHW0RMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUaFPYeh/wCPYA
mn/kwzUzMwnDfckwQgYDVR0fBDswOTA3oDWgM4YxaHR0cDovL21vcnBob3BraS5z
aWRpdi5jaGlsZS9jcmwvY3JsU2VydmVyc0NBLmNybDAOBgNVHQ8BAf8EBAMCBsAw
DQYJKoZIhvcNAQEFBQADggIBAGZqzyuKDJFZXYnPIyq2iKlfcDExRUt14jarr2fu
lwFbkti8ZOlw2bsLAKkFY784wDizRN9RBv1VVhIKwWM6IrjMDLY9WRRHf6+vtmh1
qtT4NUH1WYQXdBjHXf8dewBSmMNq//d1xHfaaub0ZAqwe7jdloJJEXkJlloh/4U6
f/KA607VD2J0tw975V5j3pORZCjvYjPMroibnG29aNioeZ/mkunUZ67s2x6sg16Q
Uiyy6qKWPCI3GDEEG2Vpb0RuxnvdRrHBNvM56ZgKDlhLGLe2ZWTfUfYK9YXdpBUN
+Sjy0SCFNYryp/fI+MaPCWIbN0sVqjGcIUiPuDIavYwrvg5s2B173PMVjSdov5wY
DrQUpsoiThfQ7zWWtMXJxpA/xmy0CKjcwpitflyRwaDExK0TZyh1/X40njoP2rc3
LEaw9423duFlCVzZhntMGYPMEb4DHTmZGRtKXIXg537fxivP0LAtcAQTxMF38Xns
MaZ7j8ZCO34cNJPTYRJZvgnQ2+QT7wUgqT7vYMTSX10Xlo46yiplr/7nngOVfOQp
gDY3R6+nbkYQ6NkdqwIk3Lk/aBSRdoZZQ/VmuBZTCdzryvoOv0qx/FYBVxNbK2kF
CQVt92hh0J+0Bki1fyS9+5hvnghHCOkPag4OJ/ILKg9Fz1novB6cXCxbNqXN9Yh1
NNer
-----END CERTIFICATE-----";

            string privateKey = @"-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEArZZuW0+AQg21Q8o6+B1AKWsNGalZ9BDGaZGaqRnEoWyNjrvd
CmdkHlr6l/rOoCrvREfzJVX/x/q+J8T0Ya3OdfAtpiLAH7yyFoB0Sm3ITn+FZc9h
iWFSeu/d5MdYr3PKxHaZh41vS15gBENhC9ZQ4iWzaWqGvfK7/0p6xjnBu5bVBOXQ
um1zyc5LkYqmZKfBgq5BJQfPLXRmP0UgMq7uxyEAAYOOM4s5LCP7DDa3zku0wY5/
Btzaz6h28o50mzTXqOq8vqtrbQbHJiGl4NLBsNx14oWYBetLOnuRXEAyMSOl1XCJ
vK62ICj1ppu3uwoe6+YyU8RMeQS4g7M3il0HpQIDAQABAoIBACfr0LVvMy/9txJA
EA+/qmHcHMcqC2X19auRsN/y7SWysVuCGPOrk3rnsXoCVELBnQU3BtQvUwPKxrCe
uxU7w5+b/OOYWlbyLoK+SABV/xsOeL9RUj0mBLIBOCbFMcAoARntrKFgBZhvu+U/
GbJ/N1rLENfNh+9nQ5USq+mqnsNlK4DNQiZDWhmdKOoVv92fYkZe3SoINd6p/VC3
xL0xGgmocnYB9cNDiPC6Udyt+LeGe9yOHXYB3mPt9PjT5kMH9EGZ/q60I+xDMlAw
1TZSgBxIZRr/TUzv/MaYTwAjDu1lfkQNjMLbQMECX7uFS5rqRzNR2jZvZDsaqgC2
JwFXoAECgYEA5lEqRQXuQ90onlBPXxCc9JDwuSOR5FwbEBb3U4U+kJb3PDH4UhnC
yTME36h5RLMmK+y2zXITiaZE7k/jMcQPYEqWVc7N0FhmsBSSpGSXrDtMUqh8BYp5
zR1IkQ8cSShjjOACcSyK0oqS/fzo+IpK0phRQzA1PnS6bJ4BVtrffeUCgYEAwPHR
PI/8vDSKdSYT+dGb7F74K/+9HuEdEtmto46hbyBb1cX1BajX/ghI6hTujAPJ5KKr
Oe+Q/NEgzQEXcBtjk3gtQ3XL5Ylct59ddAwiPoQX/sDBr5/n0OPFYs68YoEQ4nGy
S2AshilfDxH4Mmn9OVgMR5e1vE9Qu2X2xjKAxsECgYEAlHG7zMEF0Le6dk9M2Pjc
MMU6YhmK7qRuzPAAeNRofsjOkP2kD0aLRSKdAYqUnrLY53aqW8Aq141S4EadKHY1
ctqgCq4/+QkBJnwcB9jN6TRO/Pea9/dIietV2ijjEkP15dZ7vHX5A7pNA2b+wTJh
B1slBSkNKDQT8+3i7xgi9AECgYBycz7VeOfHUDdNVGAHEVI3fQIjP1kUpx4S33LD
kUirXXt8LCeyyrwM4Sc3S2FRRrIM4H1HKZdMkFSvplUb1/NSN7hJyuc+6iNIfMz3
mHb7tGlzYS3DGsxsuZ+Ng+p1V/AHEsBbO9pNXt8XVmZuqTZfPJ5UHWbdJANUliAy
4yRGQQKBgQCbH344SGhNOYTXfVeX9PnX2MEGd9wyhTbjbbUQXBXqKJb4XgTA6v2O
mHKC9WwekX8y93AkRLcs6Ns92BC6uRywDTBJScM3xmh5gVT5+KIwaNsuxNWo7RRr
L6BaxQbILnEAMlMLWW7KIRTtIgaYD/3Ph3+A16Wgm/gWxj2mG3N3vw==
-----END RSA PRIVATE KEY-----";

            Certificate virtualCertificate = new Certificate(certificateKey, privateKey, "");
            X509Certificate2 localCertificate = virtualCertificate.GetCertificateFromPEMstring(false);
// --------------------------
            // load the certificate containing the private key
            //X509Certificate2 localCertificate = new X509Certificate2(ConfigurationManager.AppSettings["certificates.local.sign.path"], ConfigurationManager.AppSettings["certificates.local.sign.password"]);
            // sign the xml document
            SignedXmlWithId signedXml = new SignedXmlWithId(doc);
            signedXml.SigningKey = localCertificate.PrivateKey;
            // Specify a canonicalization method.
            signedXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NTransformUrl;

            Reference reference = new Reference("#" + id);
            // add required transformations
            XmlDsigC14NTransform c14t = new XmlDsigC14NTransform();
            reference.AddTransform(c14t);
            signedXml.AddReference(reference);
            // add certificate info
            KeyInfo keyInfo = new KeyInfo();
            KeyInfoX509Data keyInfoData = new KeyInfoX509Data(localCertificate);
            keyInfo.AddClause(keyInfoData);
            signedXml.KeyInfo = keyInfo;
            // compute the signature
            signedXml.ComputeSignature();
            // change the namespace...just to be nice
            XmlElement xmlDigitalSignature = signedXml.GetXml();
            xmlDigitalSignature.SetAttribute("Id", "Signature-1");

            securityNode.AppendChild(xmlDigitalSignature);

            // UsernameToken
            string passwordType = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";
            // Compute required elements
            string phrase = Guid.NewGuid().ToString();
            string nonce = Helpers.GetSHA1String(phrase);
            string untid = Guid.NewGuid().ToString();


            string username = "usr_medleg";
            string password = "H%E1K!9&rC2U";
            // creation time
            //DateTime created = DateTime.Now;
            //string createdStr = created.ToString("yyyy-MM-ddThh:mm:ssZ");

            XmlElement unt = doc.CreateElement(PREFIX_WS_SECURITY, "UsernameToken", NAMESPACE_WS_SECURITY);
            unt.SetAttribute("xmlns:wsu", NAMESPACE_WS_UTILITIES);
            unt.SetAttribute("Id", NAMESPACE_WS_UTILITIES, untid);
            XmlElement usernameElement = doc.CreateElement(PREFIX_WS_SECURITY, "Username", NAMESPACE_WS_SECURITY);
            usernameElement.InnerText = username;
            XmlElement passwordElement = doc.CreateElement(PREFIX_WS_SECURITY, "Password", NAMESPACE_WS_SECURITY);
            passwordElement.SetAttribute("Type", passwordType);
            passwordElement.InnerText = password;

            unt.AppendChild(usernameElement);
            unt.AppendChild(passwordElement);
            securityNode.AppendChild(unt);

            headerNode.AppendChild(securityNode);

            // Make sure the byte order mark doesn't get written out
            XmlDictionaryReaderQuotas quotas = new XmlDictionaryReaderQuotas();
            Encoding encoderWithoutBOM = new System.Text.UTF8Encoding(false);

            System.IO.MemoryStream ms = new System.IO.MemoryStream(encoderWithoutBOM.GetBytes(doc.InnerXml));

            XmlDictionaryReader xdr = XmlDictionaryReader.CreateTextReader(ms, encoderWithoutBOM, quotas, null);

            //Create the new message, that has the digital signature in the header
            Message newMessage = Message.CreateMessage(xdr, System.Int32.MaxValue, request.Version);
            request = newMessage;

            return null;
        }

        /// <summary>
        /// Enables inspection or modification of a message after a reply message is received but prior to passing it back to the client application.
        /// </summary>
        /// <param name="reply">The message to be transformed into types and handed back to the client application.</param>
        /// <param name="correlationState">Correlation state data.</param>
        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            // Nothing special here
        }
    }

    /// <summary>
    /// Represents a run-time behavior extension for a client endpoint.
    /// </summary>
    public class RegistroCivilEndpointBehavior : IEndpointBehavior
    {
        /// <summary>
        /// Implements a modification or extension of the client across an endpoint.
        /// </summary>
        /// <param name="endpoint">The endpoint that is to be customized.</param>
        /// <param name="clientRuntime">The client runtime to be customized.</param>
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.ClientMessageInspectors.Add(new RegistroCivilMessageInspector());
        }

        /// <summary>
        /// Implement to pass data at runtime to bindings to support custom behavior.
        /// </summary>
        /// <param name="endpoint">The endpoint to modify.</param>
        /// <param name="bindingParameters">The objects that binding elements require to support the behavior.</param>
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
            // Nothing special here
        }

        /// <summary>
        /// Implements a modification or extension of the service across an endpoint.
        /// </summary>
        /// <param name="endpoint">The endpoint that exposes the contract.</param>
        /// <param name="endpointDispatcher">The endpoint dispatcher to be modified or extended.</param>
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            // Nothing special here
        }

        /// <summary>
        /// Implement to confirm that the endpoint meets some intended criteria.
        /// </summary>
        /// <param name="endpoint">The endpoint to validate.</param>
        public void Validate(ServiceEndpoint endpoint)
        {
            // Nothing special here
        }
    }
}
