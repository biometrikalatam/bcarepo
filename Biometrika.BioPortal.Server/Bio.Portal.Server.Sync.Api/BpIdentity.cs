/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace BioPortal.Server.Sync.Api
{         
	/// <summary>
	/// BpIdentity object for NHibernate mapped table 'bp_identity'.
	/// </summary>
	[Serializable]
    public class BpIdentity :  INotifyPropertyChanged
	{
		#region Member Variables
    		protected int _id;
    		protected string _nick;
    		protected string _typeid;
    		protected string _valueid;
    		protected string _name;
    		protected string _patherlastname;
    		protected string _motherlastname;
    		protected string _sex;
    		protected DateTime? _birthdate = new DateTime(1900, 1, 1);
    		protected string _photography;
    		protected string _signatureimage;
            protected string _docimagefront;
            protected string _docimageback;
            protected string _selfie;
            protected DateTime _creation = DateTime.Now;
    		protected string _verificationsource;
    		protected int _companyidenroll;
    		protected ArrayList _bpbir;
    		public event PropertyChangedEventHandler PropertyChanged;
		#endregion

		#region Constructors
    		public BpIdentity() {}
    					
    		public BpIdentity(string nick, string typeid, string valueid, string name, string patherlastname, 
                string motherlastname, string sex, DateTime? birthdate, string photography, string signatureimage, string docimagefront, string docimageback,
                string selfie, string verificationsource, int companyidenroll) 
    		{
                string msgErr;
                this._nick = nick ?? valueid;
    			this._typeid= typeid;
    			this._valueid= valueid;
    			this._name= name;
    			this._patherlastname= patherlastname;
    			this._motherlastname= motherlastname;
    			this._sex= sex;
                this._birthdate = CheckDateTimeMinimal(birthdate);
    			this._photography= photography;
    			this._signatureimage= signatureimage;
                this._docimagefront = docimagefront;
                this._docimageback = docimageback;
                this._selfie = selfie;
                this._creation= DateTime.Now;
                this._verificationsource = verificationsource ?? "NV";
                this._companyidenroll = companyidenroll;
                this._bpbir = new ArrayList();
    		}

            public static DateTime DTMinimal = new DateTime(1900, 1, 1, 0, 0, 0, 1);
            private static DateTime CheckDateTimeMinimal(DateTime? param)
            {
                DateTime dtRet = new DateTime();
                try
                {
                    if (!param.HasValue) return DTMinimal;
                    if (param < DTMinimal) return DTMinimal;
                    dtRet = param.Value;
                }
                catch (Exception ex)
                {
                    //LOG.Error("DateTimeHelper.CheckDateTimeMinimal", ex);
                }
                return dtRet;
            }

    		
		#endregion

		#region Public Properties
    		public  virtual int Id
    		{
    			get { return _id; }
    			set {if (value != this._id){_id= value;NotifyPropertyChanged("Id");}}
    		}
    		public  virtual string Nick
    		{
    			get { return _nick; }
    			set {
    				if ( value != null && value.Length > 80)
    					throw new ArgumentOutOfRangeException("value", value.ToString(), "Nick cannot contain more than 80 characters");
    				if (value != this._nick){_nick= value;NotifyPropertyChanged("Nick");}}
    		}
    		public  virtual string Typeid
    		{
    			get { return _typeid; }
    			set {
    				if ( value != null && value.Length > 10)
    					throw new ArgumentOutOfRangeException("value", value.ToString(), "Typeid cannot contain more than 10 characters");
    				if (value != this._typeid){_typeid= value;NotifyPropertyChanged("Typeid");}}
    		}
    		public  virtual string Valueid
    		{
    			get { return _valueid; }
    			set {
    				if ( value != null && value.Length > 50)
    					throw new ArgumentOutOfRangeException("value", value.ToString(), "Valueid cannot contain more than 50 characters");
    				if (value != this._valueid){_valueid= value;NotifyPropertyChanged("Valueid");}}
    		}
    		public  virtual string Name
    		{
    			get { return _name; }
    			set {
    				if ( value != null && value.Length > 50)
    					throw new ArgumentOutOfRangeException("value", value.ToString(), "Name cannot contain more than 50 characters");
    				if (value != this._name){_name= value;NotifyPropertyChanged("Name");}}
    		}
    		public  virtual string Patherlastname
    		{
    			get { return _patherlastname; }
    			set {
    				if ( value != null && value.Length > 50)
    					throw new ArgumentOutOfRangeException("value", value.ToString(), "Patherlastname cannot contain more than 50 characters");
    				if (value != this._patherlastname){_patherlastname= value;NotifyPropertyChanged("Patherlastname");}}
    		}
    		public  virtual string Motherlastname
    		{
    			get { return _motherlastname; }
    			set {
    				if ( value != null && value.Length > 50)
    					throw new ArgumentOutOfRangeException("value", value.ToString(), "Motherlastname cannot contain more than 50 characters");
    				if (value != this._motherlastname){_motherlastname= value;NotifyPropertyChanged("Motherlastname");}}
    		}
    		public  virtual string Sex
    		{
    			get { return _sex; }
    			set {
    				if ( value != null && value.Length > 1)
    					throw new ArgumentOutOfRangeException("value", value.ToString(), "Sex cannot contain more than 1 characters");
    				if (value != this._sex){_sex= value;NotifyPropertyChanged("Sex");}}
    		}
    		public  virtual DateTime? Birthdate
    		{
    			get { return _birthdate; }
    			set {if (value != this._birthdate){_birthdate= value;NotifyPropertyChanged("Birthdate");}}
    		}
    		public  virtual string Photography
    		{
    			get { return _photography; }
    			set {
    				if ( value != null && value.Length > 2147483647)
    					throw new ArgumentOutOfRangeException("value", value.ToString(), "Photography cannot contain more than 2147483647 characters");
    				if (value != this._photography){_photography= value;NotifyPropertyChanged("Photography");}}
    		}
            public virtual string Selfie
            {
                get { return _selfie; }
                set
                {
                    if (value != null && value.Length > 2147483647)
                        throw new ArgumentOutOfRangeException("value", value.ToString(), "Selfie cannot contain more than 2147483647 characters");
                    if (value != this._photography) { _selfie = value; NotifyPropertyChanged("Selfie"); }
                }
            }
            public  virtual string Signatureimage
    		{
    			get { return _signatureimage; }
    			set {
    				if ( value != null && value.Length > 2147483647)
    					throw new ArgumentOutOfRangeException("value", value.ToString(), "Signatureimage cannot contain more than 2147483647 characters");
    				if (value != this._signatureimage){_signatureimage= value;NotifyPropertyChanged("Signatureimage");}}
    		}
            public virtual string DocImageFront
            {
                get { return _docimagefront; }
                set
                {
                    if (value != null && value.Length > 2147483647)
                        throw new ArgumentOutOfRangeException("value", value.ToString(), "DocImageFront cannot contain more than 2147483647 characters");
                    if (value != this._docimagefront) { _docimagefront = value; NotifyPropertyChanged("DocImageFront"); }
                }
            }
            public virtual string DocImageBack
            {
                get { return _docimageback; }
                set
                {
                    if (value != null && value.Length > 2147483647)
                        throw new ArgumentOutOfRangeException("value", value.ToString(), "DocImageBack cannot contain more than 2147483647 characters");
                    if (value != this._docimageback) { _docimageback = value; NotifyPropertyChanged("DocImageBack"); }
                }
            }
            public  virtual DateTime Creation
    		{
    			get { return _creation; }
    			set {if (value != this._creation){_creation= value;NotifyPropertyChanged("Creation");}}
    		}
    		public  virtual string Verificationsource
    		{
    			get { return _verificationsource; }
    			set {
    				if ( value != null && value.Length > 50)
    					throw new ArgumentOutOfRangeException("value", value.ToString(), "Verificationsource cannot contain more than 50 characters");
    				if (value != this._verificationsource){_verificationsource= value;NotifyPropertyChanged("Verificationsource");}}
    		}


    		public  virtual int Companyidenroll
    		{
    			get { return _companyidenroll; }
    			set {if (value != this._companyidenroll){_companyidenroll= value;NotifyPropertyChanged("Companyidenroll");}}
    		}
            [XmlElement(Type = typeof(BpBir))]
    		public  virtual ArrayList BpBir
    		{
    			get { return _bpbir; }
    			set {_bpbir= value; }
    		}
    		protected void NotifyPropertyChanged(String info)
    		{
    			if (PropertyChanged != null)
    			{
    				PropertyChanged(this, new PropertyChangedEventArgs(info));
    			}
    		}
		#endregion
		
		#region Equals And HashCode Overrides
    		/// <summary>
    		/// local implementation of Equals based on unique value members
    		/// </summary>
    		public override bool Equals( object obj )
    		{
    			if( this == obj ) return true;
    			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
    			BpIdentity castObj = (BpIdentity)obj;
    			return ( castObj != null ) &&
    			this._id == castObj.Id;
    		}
    		/// <summary>
    		/// local implementation of GetHashCode based on unique value members
    		/// </summary>
    		public override int GetHashCode()
    		{
    			int hash = 57;
    			hash = 27 * hash * _id.GetHashCode();
    			return hash;
    		}
		#endregion
		
	}
}
