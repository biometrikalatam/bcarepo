﻿using NecService2021;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NecService2021_Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            byte[] raw = Bio.Core.Imaging.ImageProcessor.BitmapToRaw(new Bitmap(pictureBox1.Image));
            textBox1.Text = CallServiceRestSharp(Convert.ToBase64String(raw), richTextBox1.Text).ToString();
        }

        private int CallServiceRestSharp(string raw, string minutiae)
        {
            var uri = new Uri("http://localhost:1445/nec/match");
            var host = uri.GetLeftPart(System.UriPartial.Authority);
            var verb = uri.ToString().Replace(host.ToString(), String.Empty);

            //Objeto nec con el que se realiza la comparación
            var necDataToCompare = new NecDataToCompare()
            {
                minutiaeTypeSample = 22,
                sample = raw,
                minutiaeTypeNecData = 15,
                necData = minutiae
            };

            var client = new RestClient(host);
            var request = new RestRequest(verb, Method.POST);
            request.RequestFormat = RestSharp.DataFormat.Json;
            request.AddBody(necDataToCompare);
            try
            {
                var respuesta = client.Execute(request);
                return Convert.ToInt32(respuesta.Content);
                //resultado.Text = $"Score: {respuesta.Content}";

            }
            catch (Exception ex)
            {
                //resultado.Text = ex.Message;
                //LOG.Error("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp Error - " + ex.Message);
                return -1;

            }
        }
    }
}
