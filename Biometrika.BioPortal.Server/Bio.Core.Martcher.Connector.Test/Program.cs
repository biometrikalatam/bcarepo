﻿using Bio.Core.Matcher;
using Bio.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bio.Core.Martcher.Connector.Test
{
    static class Program
    {

        /// <summary>
        /// Manejo de conectores para consultas externas.
        /// </summary>
        internal static ConnectorManager CONNECTOR_MANAGER;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //try
            //{
            //    DateTime dt = DateTime.Parse("19 JUL 2012");
            //    DateTime dt1 = DateTime.Parse("19 JUL 2017");
            //    DateTime dt2 = DateTime.Parse("28 SEP 1969");
            //    dt2 = DateTime.Parse("28 SEP 1969");
            //    dt2 = DateTime.Parse("28 JAN 1969");
            //}
            //catch (Exception ex)
            //{
            //    string s = ex.Message;
            //}
            
            //3.- Genero e inicializo ConnectorManager
            CONNECTOR_MANAGER = SerializeHelper.DeserializeFromFile<ConnectorManager>("Connectors.cfg");
            if (CONNECTOR_MANAGER == null || CONNECTOR_MANAGER.ConnectorsConfigured.Count == 0)
            {
                MessageBox.Show("   BioPortal Server Reading Connectors - No Connectors Configured!");
            }
            else
            {
                MessageBox.Show("   BioPortal Server Reading Connectors = OK - Connectors Configured = " +
                CONNECTOR_MANAGER.ConnectorsConfigured.Count.ToString());

                //Si levanto connectors, inicializo creando las instancias
                CONNECTOR_MANAGER.Initialization();

                if (CONNECTOR_MANAGER.QuantityConnectorsAvailables() == 0)
                {
                    MessageBox.Show("   BioPortal Server Initializing Connectors = WARNING - No Connectors Availables!");
                }
                else
                {
                    MessageBox.Show("   BioPortal Server Initializing Connectors = OK  - Connectors Availables = " +
                                   CONNECTOR_MANAGER.QuantityConnectorsAvailables().ToString());
                    MessageBox.Show("   Connectors Availables = " + CONNECTOR_MANAGER.ConnecotorsAvailablesToString());
                }
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
