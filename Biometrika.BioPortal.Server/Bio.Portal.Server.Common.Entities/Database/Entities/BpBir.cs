/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Bio.Core.pki.mentalis;
using Bio.Core.pki.symmetric;
using log4net;

namespace Bio.Portal.Server.Common.Entities.Database
{
    /// <summary>
    /// BpBir object for NHibernate mapped table 'bp_bir'.
    /// </summary>
    [Serializable]
    public class BpBir : INotifyPropertyChanged
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BpBir));

        #region Member Variables
        protected int _id;
        protected BpIdentity _bpidentity;
        protected int _type;
        protected int _authenticationfactor;
        protected int _minutiaetype;
        protected int _bodypart;
        protected string _data;
        protected string _additionaldata;
        protected DateTime _timestamp;
        protected DateTime _creation;
        protected string _signature;
        protected int _companyidenroll;
        protected int _useridenroll;

        //Agregados para firmar y encriptar si fuera necesario.
        Signer _signer;
        Encryptor _encryptor;

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
        #region Constructors

        public BpBir() { }

        public BpBir(BpIdentity bpidentity, int type, int authenticationfactor, int minutiaetype,
            int bodypart, string data, string additionaldata, DateTime timestamp, DateTime creation,
            string signature, int companyidenroll, int useridenroll)
        {
            this._bpidentity = bpidentity;
            this._type = type;
            this._authenticationfactor = authenticationfactor;
            this._minutiaetype = minutiaetype;
            this._bodypart = bodypart;
            this._data = data;
            this._additionaldata = additionaldata;
            this._timestamp = timestamp;
            this._creation = creation;
            this._signature = signature;
            this._companyidenroll = companyidenroll;
            this._useridenroll = useridenroll;
        }

        public BpBir(BpIdentity bpidentity, int type, int authenticationfactor, int minutiaetype,
            int bodypart, string data, string additionaldata)
        {
            this._bpidentity = bpidentity;
            this._type = type;
            this._authenticationfactor = authenticationfactor;
            this._minutiaetype = minutiaetype;
            this._bodypart = bodypart;
            this._data = data;
            this._additionaldata = additionaldata;
        }

        /*public BpBir(int type, int authenticationfactor, int minutiaetype, int bodypart,
                    string data, string additionaldata, object bpidentity, DateTime timestamp, DateTime creation,
                    string signature, int companyidenroll, int useridenroll,
                    Signer signer, Encryptor encryptor)
        {
            //this.Id = id;

            this.Signer = signer;
            this.Encryptor = encryptor;

            this._type = type;
            this._authenticationfactor = authenticationfactor;
            this._bodypart = bodypart;
            this._minutiaetype = minutiaetype;

            //Added 10/2007 
            AddSuFixForOracle(ref data);

            try
            {
                if (encryptor != null)
                {
                    object dataEncrypted = null;
                    if (encryptor.Encrypt(data, true, ref dataEncrypted, true))
                    {
                        this._data = (string)dataEncrypted;
                        this._type = type | Bio.Core.Matcher.Constant.BirType.ENCRYPTED;
                    }
                    else
                    {
                        this._data = data;
                    }
                }
                else
                {
                    this._data = data;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BpBir.BpBir - Encrypt", ex);
                this._data = data;

            }
            this._additionaldata = additionaldata;

            try
            {
                if (this._signature == null && this.Signer != null)
                {
                    object sSignature = null;
                    signer.GetSignature(ref sSignature, data, "SHA1", true);
                    this._signature = (string)sSignature;
                    this._type = type | Bio.Core.Matcher.Constant.BirType.SIGNED;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BpBir.BpBir - Signature", ex);
            }

            this._timestamp = timestamp;
            this._creation = creation;
            string msg = null;

            try
            {
                if (bpidentity.GetType().Equals(typeof(BpIdentity)))
                {
                    this._bpidentity = (BpIdentity)bpidentity;
                }
                else
                {
                    int ierr = AdminBpIdentity.Retrieve((int)bpidentity, out this._bpidentity, out msg);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BpBir.BpBir - Asign BpIdentity", ex);
            }


        }*/

        #endregion
        #region Public Properties
        public virtual int Id
        {
            get { return _id; }
            set { if (value != this._id) { _id = value; NotifyPropertyChanged("Id"); } }
        }
        public virtual BpIdentity BpIdentity
        {
            get { return _bpidentity; }
            set { _bpidentity = value; }
        }
        public virtual int Type
        {
            get { return _type; }
            set { if (value != this._type) { _type = value; NotifyPropertyChanged("Type"); } }
        }
        public virtual int Authenticationfactor
        {
            get { return _authenticationfactor; }
            set { if (value != this._authenticationfactor) { _authenticationfactor = value; NotifyPropertyChanged("Authenticationfactor"); } }
        }
        public virtual int Minutiaetype
        {
            get { return _minutiaetype; }
            set { if (value != this._minutiaetype) { _minutiaetype = value; NotifyPropertyChanged("Minutiaetype"); } }
        }
        public virtual int Bodypart
        {
            get { return _bodypart; }
            set { if (value != this._bodypart) { _bodypart = value; NotifyPropertyChanged("Bodypart"); } }
        }
        public virtual string Data
        {
            get { return _data; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 2147483647 characters");
                if (value != this._data) { _data = value; NotifyPropertyChanged("Data"); }
            }
        }
        public virtual string Additionaldata
        {
            get { return _additionaldata; }
            set
            {
                if (value != null && value.Length > 2048)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 2048 characters");
                if (value != this._additionaldata) { _additionaldata = value; NotifyPropertyChanged("Additionaldata"); }
            }
        }
        public virtual DateTime Timestamp
        {
            get { return _timestamp; }
            set { if (value != this._timestamp) { _timestamp = value; NotifyPropertyChanged("Timestamp"); } }
        }
        public virtual DateTime Creation
        {
            get { return _creation; }
            set { if (value != this._creation) { _creation = value; NotifyPropertyChanged("Creation"); } }
        }
        public virtual string Signature
        {
            get { return _signature; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Signature cannot contain more than 2147483647 characters");
                if (value != this._signature) { _signature = value; NotifyPropertyChanged("Signature"); }
            }
        }
        public virtual int Companyidenroll
        {
            get { return _companyidenroll; }
            set { if (value != this._companyidenroll) { _companyidenroll = value; NotifyPropertyChanged("Companyidenroll"); } }
        }
        public virtual int Useridenroll
        {
            get { return _useridenroll; }
            set { if (value != this._useridenroll) { _useridenroll = value; NotifyPropertyChanged("Useridenroll"); } }
        }
        protected virtual void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        //Agregados para firmar y encriptar si fuera necesario.
        public virtual Signer Signer
        {
            get { return _signer; }
            set { _signer = value; }
        }

        public virtual Encryptor Encryptor
        {
            get { return _encryptor; }
            set { _encryptor = value; }
        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            if ((obj == null) || (obj.GetType() != this.GetType())) return false;
            BpBir castObj = (BpBir)obj;
            return (castObj != null) &&
            this._id == castObj.Id;
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

        #region Method
        //Agregado por tema de CLob en Oracle que rango entre 2000 y 4000 caracteres da error:
        //ORA-01461: s�lo puede enlazar un valor LONG para insertarlo en una columna LONG
        private void AddSuFixForOracle(ref string pdata)
        {
            try
            {
                if (pdata == null || pdata.Length == 0) return;

                int size = pdata.Length;

                string sufix = "NA";

                if (size >= 1996 && size < 4000)
                {
                    sufix = new string('A', 4010 - size);
                }
                pdata = pdata + "|" + sufix;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion Method
    }

    ////Definicion de BioApi.org
    //public class BIR_TYPE
    //{
    //    public const int RAW = 0x01;
    //    public const int INTERMEDIATE_DATA = 0x02;
    //    public const int PROCESSED_DATA = 0x04;
    //    public const int ENCRYPTED = 0x08;
    //    public const int SIGNED = 0x10;
    //    public const int ENCODING_UTF8 = 0x20;
    //    public const int ENCODING_ASCII = 0x40;

    //    public static string GetName(int type)
    //    {
    //        string ret = null;
    //        switch (type)
    //        {
    //            case BIR_TYPE.RAW:  //Minucias
    //                ret = "RAW";
    //                break;
    //            case BIR_TYPE.PROCESSED_DATA:  //Minucias
    //                ret = "Template";
    //                break;
    //            case BIR_TYPE.INTERMEDIATE_DATA: //WSQ
    //                ret = "WSQ";
    //                break;
    //            case BIR_TYPE.PROCESSED_DATA | BIR_TYPE.ENCODING_UTF8:
    //                ret = "Minucias UTF8";
    //                break;
    //            case BIR_TYPE.INTERMEDIATE_DATA | BIR_TYPE.ENCODING_UTF8:
    //                ret = "WSQ UTF8";
    //                break;
    //            case BIR_TYPE.PROCESSED_DATA | BIR_TYPE.ENCRYPTED:
    //                ret = "Minucias Encriptadas";
    //                break;
    //            case BIR_TYPE.INTERMEDIATE_DATA | BIR_TYPE.ENCRYPTED:
    //                ret = "WSQ Encriptadas";
    //                break;
    //            default:
    //                ret = "Desconocido";
    //                break;

    //        }
    //        return ret;
    //    }
    //}
}
