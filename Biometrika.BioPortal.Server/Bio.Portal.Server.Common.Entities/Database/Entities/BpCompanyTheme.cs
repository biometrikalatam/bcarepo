﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Bio.Portal.Server.Common.Entities.Database
{
    [Serializable]
    public class BpCompanyTheme
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BpCompanyTheme));

        #region Member Variables
        protected int _id;
        protected int _companyid;
        protected string _name;
        protected string _jsonconfig;

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Constructors

        public BpCompanyTheme() { }

        public BpCompanyTheme(int companyid, string name, string jsonconfig)
        {
            this._companyid = companyid;
            this._name = name;
            this._jsonconfig = jsonconfig;
        }

        #endregion

        #region Public Properties
        public virtual int Id
        {
            get { return _id; }
            set { if (value != this._id) { _id = value; NotifyPropertyChanged("Id"); } }
        }
        public virtual int CompanyId
        {
            get { return _companyid; }
            set { if (value != this._companyid) { _companyid = value; NotifyPropertyChanged("CompanyId"); } }
        }
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Name cannot contain more than 50 characters");
                if (value != this._name) { _name = value; NotifyPropertyChanged("Name"); }
            }
        }
        public virtual string JsonConfig
        {
            get { return _jsonconfig; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "JsonConfig cannot contain more than 2147483647 characters");
                if (value != this._jsonconfig) { _jsonconfig = value; NotifyPropertyChanged("JsonConfig"); }
            }
        }
        protected virtual void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            if ((obj == null) || (obj.GetType() != this.GetType())) return false;
            BpBir castObj = (BpBir)obj;
            return (castObj != null) &&
            this._id == castObj.Id;
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }
}
