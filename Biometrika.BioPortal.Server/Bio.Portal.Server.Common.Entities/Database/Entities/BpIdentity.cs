/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Bio.Core.Utils;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Common.Entities.Database
{         
	/// <summary>
	/// BpIdentity object for NHibernate mapped table 'bp_identity'.
	/// </summary>
	[Serializable]
    public class BpIdentity : INotifyPropertyChanged
    {
        #region Member Variables
        protected int _id;
        protected string _nick;
        protected string _typeid;
        protected string _valueid;
        protected string _name;
        protected string _patherlastname;
        protected string _motherlastname;
        protected string _sex;
        protected string _documentseriesnumber;
        protected DateTime? _documentexpirationdate = new DateTime(2999, 1, 1);
        protected string _visatype;
        protected DateTime? _birthdate = new DateTime(1900, 1, 1);
        protected string _birthplace;
        protected string _nationality;
        protected string _photography;
        protected string _signatureimage;
        protected string _docimagefront;
        protected string _docimageback;
        protected string _selfie;
        protected string _profession;
        protected string _dynamicdata;
        protected string _enrollinfo;
        protected DateTime _creation = DateTime.Now;
        protected string _verificationsource;
        protected BpCompany _companyidenroll;
        protected int _useridenroll;
        protected IList<BpBir> _bpbir;
        protected IList<BpIdentityDynamicdata> _bpidentitydynamicdata;
        protected IList<BpIdentity3ro> _bpidentity3ro;
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Constructors
        public BpIdentity() { }

        /*	public BpIdentity(string nick, string typeid, string valueid, string name, string patherlastname, 
                string motherlastname, string sex, string documentseriesnumber, DateTime? documentexpirationdate, 
                string visatype, DateTime? birthdate, string birthplace, string nationality, 
                string photography, string signatureimage, string profession, string dynamicdata, 
                string enrollinfo, DateTime creation, string verificationsource, int companyidenroll, int useridenroll) 
    		{
                string msgErr;
                this._nick = nick ?? valueid;
    			this._typeid= typeid;
    			this._valueid= valueid;
    			this._name= name;
    			this._patherlastname= patherlastname;
    			this._motherlastname= motherlastname;
    			this._sex= sex;
                this._documentseriesnumber = documentseriesnumber;
                this._documentexpirationdate = DateTimeHelper.CheckDateTimeMinimal(documentexpirationdate);
    			this._visatype= visatype;
                this._birthdate = DateTimeHelper.CheckDateTimeMinimal(birthdate);
    			this._birthplace= birthplace;
    			this._nationality= nationality;
    			this._photography= photography;
    			this._signatureimage= signatureimage;
    			this._profession= profession;
    			this._dynamicdata= dynamicdata;
    			this._enrollinfo= enrollinfo;
    			this._creation= DateTime.Now;
                this._verificationsource = verificationsource ?? "NV";
                BpCompany company = AdminBpCompany.GetCompanyById(companyidenroll,out msgErr);
    			//this._companyidenroll= companyidenroll;
                this._companyidenroll = company;
    			this._useridenroll= useridenroll;

                this._bpbir = new List<BpBir>();
    		}*/

        public BpIdentity(BpCompany companyidenroll, string typeid, string valueid)
        {
            this._typeid = typeid;
            this._valueid = valueid;
            this._nick = valueid;
            this._companyidenroll = companyidenroll;
            this._verificationsource = null;
            this._documentexpirationdate = DateTimeHelper.CheckDateTimeMinimal(null);
            this._birthdate = DateTimeHelper.CheckDateTimeMinimal(null);

        }

        public BpIdentity(string nick)
        {
            this._typeid = "UID";
            this._valueid = nick;
            this._nick = nick;
            this._companyidenroll = null;
            this._verificationsource = null;
            this._documentexpirationdate = DateTimeHelper.CheckDateTimeMinimal(null);
            this._birthdate = DateTimeHelper.CheckDateTimeMinimal(null);

        }


        #endregion

        #region Public Properties
        public virtual int Id
        {
            get { return _id; }
            set { if (value != this._id) { _id = value; NotifyPropertyChanged("Id"); } }
        }
        public virtual string Nick
        {
            get { return _nick; }
            set
            {
                if (value != null && value.Length > 80)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Nick cannot contain more than 80 characters");
                if (value != this._nick) { _nick = value; NotifyPropertyChanged("Nick"); }
            }
        }
        public virtual string Typeid
        {
            get { return _typeid; }
            set
            {
                if (value != null && value.Length > 10)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Typeid cannot contain more than 10 characters");
                if (value != this._typeid) { _typeid = value; NotifyPropertyChanged("Typeid"); }
            }
        }
        public virtual string Valueid
        {
            get { return _valueid; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Valueid cannot contain more than 50 characters");
                if (value != this._valueid) { _valueid = value; NotifyPropertyChanged("Valueid"); }
            }
        }
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Name cannot contain more than 50 characters");
                if (value != this._name) { _name = value; NotifyPropertyChanged("Name"); }
            }
        }
        public virtual string Patherlastname
        {
            get { return _patherlastname; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Patherlastname cannot contain more than 50 characters");
                if (value != this._patherlastname) { _patherlastname = value; NotifyPropertyChanged("Patherlastname"); }
            }
        }
        public virtual string Motherlastname
        {
            get { return _motherlastname; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Motherlastname cannot contain more than 50 characters");
                if (value != this._motherlastname) { _motherlastname = value; NotifyPropertyChanged("Motherlastname"); }
            }
        }
        public virtual string Sex
        {
            get { return _sex; }
            set
            {
                if (value != null && value.Length > 1)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Sex cannot contain more than 1 characters");
                if (value != this._sex) { _sex = value; NotifyPropertyChanged("Sex"); }
            }
        }
        public virtual string Documentseriesnumber
        {
            get { return _documentseriesnumber; }
            set
            {
                if (value != null && value.Length > 30)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Documentseriesnumber cannot contain more than 30 characters");
                if (value != this._documentseriesnumber) { _documentseriesnumber = value; NotifyPropertyChanged("Documentseriesnumber"); }
            }
        }
        public virtual DateTime? Documentexpirationdate
        {
            get { return _documentexpirationdate; }
            set { if (value != this._documentexpirationdate) { _documentexpirationdate = value; NotifyPropertyChanged("Documentexpirationdate"); } }
        }
        public virtual string Visatype
        {
            get { return _visatype; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Visatype cannot contain more than 50 characters");
                if (value != this._visatype) { _visatype = value; NotifyPropertyChanged("Visatype"); }
            }
        }
        public virtual DateTime? Birthdate
        {
            get { return _birthdate; }
            set { if (value != this._birthdate) { _birthdate = value; NotifyPropertyChanged("Birthdate"); } }
        }
        public virtual string Birthplace
        {
            get { return _birthplace; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Birthplace cannot contain more than 50 characters");
                if (value != this._birthplace) { _birthplace = value; NotifyPropertyChanged("Birthplace"); }
            }
        }
        public virtual string Nationality
        {
            get { return _nationality; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Nationality cannot contain more than 50 characters");
                if (value != this._nationality) { _nationality = value; NotifyPropertyChanged("Nationality"); }
            }
        }
        public virtual string Photography
        {
            get { return _photography; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Photography cannot contain more than 2147483647 characters");
                if (value != this._photography) { _photography = value; NotifyPropertyChanged("Photography"); }
            }
        }
        public virtual string Signatureimage
        {
            get { return _signatureimage; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Signatureimage cannot contain more than 2147483647 characters");
                if (value != this._signatureimage) { _signatureimage = value; NotifyPropertyChanged("Signatureimage"); }
            }
        }
        public virtual string Selfie
        {
            get { return _selfie; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Selfie cannot contain more than 2147483647 characters");
                if (value != this._selfie) { _photography = value; NotifyPropertyChanged("Selfie"); }
            }
        }
        public virtual string DocImageFront
        {
            get { return _docimagefront; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "DocImageFront cannot contain more than 2147483647 characters");
                if (value != this._docimagefront) { _docimagefront = value; NotifyPropertyChanged("DocImageFront"); }
            }
        }
        public virtual string DocImageBack
        {
            get { return _docimageback; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "DocImageBack cannot contain more than 2147483647 characters");
                if (value != this._docimageback) { _docimageback = value; NotifyPropertyChanged("DocImageBack"); }
            }
        }
        public virtual string Profession
        {
            get { return _profession; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Profession cannot contain more than 50 characters");
                if (value != this._profession) { _profession = value; NotifyPropertyChanged("Profession"); }
            }
        }
        public virtual string Dynamicdata
        {
            get { return _dynamicdata; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Dynamicdata cannot contain more than 2147483647 characters");
                if (value != this._dynamicdata) { _dynamicdata = value; NotifyPropertyChanged("Dynamicdata"); }
            }
        }
        public virtual string Enrollinfo
        {
            get { return _enrollinfo; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Enrollinfo cannot contain more than 2147483647 characters");
                if (value != this._enrollinfo) { _enrollinfo = value; NotifyPropertyChanged("Enrollinfo"); }
            }
        }
        public virtual DateTime Creation
        {
            get { return _creation; }
            set { if (value != this._creation) { _creation = value; NotifyPropertyChanged("Creation"); } }
        }
        public virtual string Verificationsource
        {
            get { return _verificationsource; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Verificationsource cannot contain more than 2147483647 characters");
                if (value != this._verificationsource) { _verificationsource = value; NotifyPropertyChanged("Verificationsource"); }
            }
        }
        public virtual BpCompany Companyidenroll
        {
            get { return _companyidenroll; }
            set { if (value != this._companyidenroll) { _companyidenroll = value; NotifyPropertyChanged("Companyidenroll"); } }
        }
        public virtual int Useridenroll
        {
            get { return _useridenroll; }
            set { if (value != this._useridenroll) { _useridenroll = value; NotifyPropertyChanged("Useridenroll"); } }
        }
        public virtual IList<BpBir> BpBir
        {
            get { return _bpbir; }
            set { _bpbir = value; }
        }

        public virtual IList<BpIdentityDynamicdata> BpIdentityDynamicdata
        {
            get { return _bpidentitydynamicdata; }
            set { _bpidentitydynamicdata = value; }
        }
        public virtual IList<BpIdentity3ro> BpIdentity3ro
        {
            get { return _bpidentity3ro; }
            set { _bpidentity3ro = value; }
        }
        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            if ((obj == null) || (obj.GetType() != this.GetType())) return false;
            BpIdentity castObj = (BpIdentity)obj;
            return (castObj != null) &&
            this._id == castObj.Id;
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }
}
