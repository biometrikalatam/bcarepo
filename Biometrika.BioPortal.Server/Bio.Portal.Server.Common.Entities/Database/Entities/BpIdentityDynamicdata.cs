/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Bio.Core.pki.mentalis;
using Bio.Core.pki.symmetric;
using log4net;

namespace Bio.Portal.Server.Common.Entities.Database
{
    /// <summary>
    /// BpBir object for NHibernate mapped table 'bp_bir'.
    /// </summary>
    [Serializable]
    public class BpIdentityDynamicdata : INotifyPropertyChanged
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BpIdentityDynamicdata));

        #region Member Variables
        protected int _id;
        protected BpIdentity _bpidentity;
        protected string _ddkey;
        protected string _ddvalue;
        protected DateTime _creation;
        protected DateTime _lastupdate;
        

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Constructors

        public BpIdentityDynamicdata() { }

        public BpIdentityDynamicdata(BpIdentity bpidentity, string ddkey, string ddvalue)
        {
            this._bpidentity = bpidentity;
            this._ddkey = ddkey;
            this._ddvalue = ddvalue;
        }

        public BpIdentityDynamicdata(BpIdentity bpidentity, string ddkey, string ddvalue, DateTime creation, DateTime update)
        {
            this._bpidentity = bpidentity;
            this._ddkey = ddkey;
            this._ddvalue = ddvalue;
            this._creation = creation;
            this._lastupdate = update;
        }
        #endregion

        #region Public Properties
        public virtual int Id
        {
            get { return _id; }
            set { if (value != this._id) { _id = value; NotifyPropertyChanged("Id"); } }
        }
        public virtual BpIdentity BpIdentity
        {
            get { return _bpidentity; }
            set { _bpidentity = value; }
        }
        public virtual string DDKey
        {
            get { return _ddkey; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 50 characters");
                if (value != this._ddkey) { _ddkey = value; NotifyPropertyChanged("DDKey"); }
            }
        }
        public virtual string DDValue
        {
            get { return _ddvalue; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 2147483647 characters");
                if (value != this._ddvalue) { _ddvalue = value; NotifyPropertyChanged("DDValue"); }
            }
        }
        public virtual DateTime Creation
        {
            get { return _creation; }
            set { if (value != this._creation) { _creation = value; NotifyPropertyChanged("Creation"); } }
        }
        public virtual DateTime LastUpdate
        {
            get { return _lastupdate; }
            set { if (value != this._lastupdate) { _lastupdate = value; NotifyPropertyChanged("LastUpdate"); } }
        }
        protected virtual void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
#endregion

#region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            if ((obj == null) || (obj.GetType() != this.GetType())) return false;
            BpBir castObj = (BpBir)obj;
            return (castObj != null) &&
            this._id == castObj.Id;
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
#endregion
    }
}
