/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bio.Portal.Server.Common.Entities.Database
{
	/// <summary>
	/// BpLog object for NHibernate mapped table 'bp_log'.
	/// </summary>
	[Serializable]
    public class BpLog :  INotifyPropertyChanged
	{
		#region Member Variables
		protected int _id;
		protected DateTime _datel;
		protected string _threadl;
		protected string _levell;
		protected string _loggerl;
		protected string _messagel;
		protected string _exceptionl;
		public event PropertyChangedEventHandler PropertyChanged;
		
		#endregion
		#region Constructors
			
		public BpLog() {}
					
		public BpLog(DateTime datel, string threadl, string levell, string loggerl, string messagel, string exceptionl) 
		{
			this._datel= datel;
			this._threadl= threadl;
			this._levell= levell;
			this._loggerl= loggerl;
			this._messagel= messagel;
			this._exceptionl= exceptionl;
		}

		public BpLog(DateTime datel, string threadl, string levell, string loggerl, string messagel)
		{
			this._datel= datel;
			this._threadl= threadl;
			this._levell= levell;
			this._loggerl= loggerl;
			this._messagel= messagel;
		}
		
		#endregion
		#region Public Properties
		public  virtual int Id
		{
			get { return _id; }
			set {if (value != this._id){_id= value;NotifyPropertyChanged("Id");}}
		}
		public  virtual DateTime Datel
		{
			get { return _datel; }
			set {if (value != this._datel){_datel= value;NotifyPropertyChanged("Datel");}}
		}
		public  virtual string Threadl
		{
			get { return _threadl; }
			set {
				if ( value != null && value.Length > 255)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Threadl cannot contain more than 255 characters");
				if (value != this._threadl){_threadl= value;NotifyPropertyChanged("Threadl");}}
		}
		public  virtual string Levell
		{
			get { return _levell; }
			set {
				if ( value != null && value.Length > 50)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Levell cannot contain more than 50 characters");
				if (value != this._levell){_levell= value;NotifyPropertyChanged("Levell");}}
		}
		public  virtual string Loggerl
		{
			get { return _loggerl; }
			set {
				if ( value != null && value.Length > 255)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Loggerl cannot contain more than 255 characters");
				if (value != this._loggerl){_loggerl= value;NotifyPropertyChanged("Loggerl");}}
		}
		public  virtual string Messagel
		{
			get { return _messagel; }
			set {
				if ( value != null && value.Length > 4000)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Messagel cannot contain more than 4000 characters");
				if (value != this._messagel){_messagel= value;NotifyPropertyChanged("Messagel");}}
		}
		public  virtual string Exceptionl
		{
			get { return _exceptionl; }
			set {
				if ( value != null && value.Length > 2000)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Exceptionl cannot contain more than 2000 characters");
				if (value != this._exceptionl){_exceptionl= value;NotifyPropertyChanged("Exceptionl");}}
		}
        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
		
		#endregion
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			BpLog castObj = (BpLog)obj;
			return ( castObj != null ) &&
			this._id == castObj.Id;
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57;
			hash = 27 * hash * _id.GetHashCode();
			return hash;
		}
		#endregion
		
	}
}
