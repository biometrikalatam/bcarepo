/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bio.Portal.Server.Common.Entities.Database
{
	/// <summary>
	/// BpTx object for NHibernate mapped table 'bp_tx'.
	/// </summary>
	[Serializable]
	public class BpTx : INotifyPropertyChanged
	{
		#region Member Variables
		protected int _id;
		protected string _trackid;
        protected int _operationcode;
		protected string _typeid;
        protected string _valueid;
        protected string _taxidcompany;
        protected int _result;
        protected double _score;
        protected double _threshold;
		protected DateTime _timestampstart;
		protected DateTime _timestampend;
		protected int _authenticationfactor;
		protected int _minutiaetype;
		protected int _bodypart;
		protected int _actiontype;
		protected BpOrigin _bporigin;
		protected DateTime? _timestampclient;
		protected string _clientid;
		protected string _ipenduser;
		protected string _enduser;
		protected string _dynamicdata;
		protected int _companyidtx;
        protected int _useridtx;
        protected int _operationsource;
        protected string _abs;
        protected DateTime? _consumed;
        //Added 07-2020
        protected int _checklock;
        protected string _estadolock;
        protected string _razonlock;
        protected string _vigenciafromlock;
        protected string _signaturemanual;
        protected string _titlebpweb;
        protected string _messagebpweb;
        protected string _geolocation;
        protected int _checkadult;
        protected int _checkexpired;
        //------------------

        protected IList<BpTxConx> _bptxconx;		
		protected IList<BpVerified> _bpverified;
        public event PropertyChangedEventHandler PropertyChanged;

		
		#endregion
		#region Constructors
			
		public BpTx() {}

        public BpTx(string trackid, int operationcode, string typeid, string valueid, int result, double score, double threshold,
           DateTime timestampstart, DateTime timestampend, int authenticationfactor, int minutiaetype,
           int bodypart, int actiontype, BpOrigin bporigin, DateTime? timestampclient, string clientid,
           string ipenduser, string enduser, string dynamicdata, int companyidtx, int useridtx, int operationsource, string abs)
        {
            this._trackid = trackid;
            this._typeid = typeid;
            this._operationcode = operationcode;
            this._valueid = valueid;
            this._result = result;
            this._score = score;
            this._threshold = threshold;
            this._timestampstart = timestampstart;
            this._timestampend = timestampend;
            this._authenticationfactor = authenticationfactor;
            this._minutiaetype = minutiaetype;
            this._bodypart = bodypart;
            this._actiontype = actiontype;
            this._bporigin = bporigin;
            this._timestampclient = timestampclient;
            this._clientid = !String.IsNullOrWhiteSpace(clientid) ? (clientid.Length>50 ? clientid.Substring(0, 49) :clientid) : null; 
            this._ipenduser = ipenduser;
            this._enduser = !String.IsNullOrWhiteSpace(enduser)? (enduser.Length > 50 ? enduser.Substring(0, 49) : clientid) : null;
            this._dynamicdata = dynamicdata;
            this._companyidtx = companyidtx;
            this._useridtx = useridtx;
            this._operationsource = operationsource;
            this._abs = abs;
        }

       
        public BpTx(string trackid, int operationcode, string typeid, string valueid, int result,
            double score, DateTime timestampstart, DateTime timestampend, int actiontype,
            int operationsource, string abs, BpOrigin bporigin)
        {
            this._trackid = trackid;
            this._typeid = typeid;
            this._operationcode = operationcode;
            this._valueid = valueid;
            this._result = result;
            this._score = score;
            this._timestampstart = timestampstart;
            this._timestampend = timestampend;
            this._actiontype = actiontype;
            this._bporigin = bporigin;
            this._operationsource = operationsource;
            this._abs = abs;
        }
		
		
		#endregion
		#region Public Properties
		public  virtual int Id
		{
			get { return _id; }
			set {if (value != this._id){_id= value;NotifyPropertyChanged("Id");}}
		}
		public  virtual string Trackid
		{
			get { return _trackid; }
			set {
				if ( value != null && value.Length > 100)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Trackid cannot contain more than 100 characters");
				if (value != this._trackid){_trackid= value;NotifyPropertyChanged("Trackid");}}
		}
        public virtual int Operationcode
        {
            get { return _operationcode; }
            set { if (value != this._operationcode) { _operationcode = value; NotifyPropertyChanged("Operationcode"); } }
        }
		public  virtual string Typeid
		{
			get { return _typeid; }
			set {
				if ( value != null && value.Length > 10)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Typeid cannot contain more than 10 characters");
				if (value != this._typeid){_typeid= value;NotifyPropertyChanged("Typeid");}}
		}
		public  virtual string Valueid
		{
			get { return _valueid; }
			set {
				if ( value != null && value.Length > 50)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Valueid cannot contain more than 50 characters");
				if (value != this._valueid){_valueid= value;NotifyPropertyChanged("Valueid");}}
		}
        public virtual string TaxIdCompany
        {
            get { return _taxidcompany; }
            set {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "TaxIdCompany cannot contain more than 50 characters");
                if (value != this._taxidcompany)
                { _taxidcompany = value; NotifyPropertyChanged("TaxIdCompany"); }
            }
        }
        public  virtual int Result
		{
			get { return _result; }
			set {if (value != this._result){_result= value;NotifyPropertyChanged("Result");}}
		}
        public virtual double Score
		{
			get { return _score; }
			set {if (value != this._score){_score= value;NotifyPropertyChanged("Score");}}
		}
        public virtual double Threshold
		{
			get { return _threshold; }
			set {if (value != this._threshold){_threshold= value;NotifyPropertyChanged("Threshold");}}
		}
		public  virtual DateTime Timestampstart
		{
			get { return _timestampstart; }
			set {if (value != this._timestampstart){_timestampstart= value;NotifyPropertyChanged("Timestampstart");}}
		}
		public  virtual DateTime Timestampend
		{
			get { return _timestampend; }
			set {if (value != this._timestampend){_timestampend= value;NotifyPropertyChanged("Timestampend");}}
		}
		public  virtual int Authenticationfactor
		{
			get { return _authenticationfactor; }
            set { if (value != this._authenticationfactor) { _authenticationfactor = value; NotifyPropertyChanged("Authenticationfactor"); } }
		}
		public  virtual int Minutiaetype
		{
			get { return _minutiaetype; }
			set {if (value != this._minutiaetype){_minutiaetype= value;NotifyPropertyChanged("Minutiaetype");}}
		}
		public  virtual int Bodypart
		{
			get { return _bodypart; }
			set {if (value != this._bodypart){_bodypart= value;NotifyPropertyChanged("Bodypart");}}
		}
		public  virtual int Actiontype
		{
			get { return _actiontype; }
			set {if (value != this._actiontype){_actiontype= value;NotifyPropertyChanged("Actiontype");}}
		}
		public  virtual BpOrigin BpOrigin
		{
			get { return _bporigin; }
			set {_bporigin= value; }
		}
		public  virtual DateTime? Timestampclient
		{
			get { return _timestampclient; }
			set {if (value != this._timestampclient){_timestampclient= value;NotifyPropertyChanged("Timestampclient");}}
		}
		public  virtual string Clientid
		{
			get { return _clientid; }
			set {
				if ( value != null && value.Length > 500)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Clientid cannot contain more than 50 characters");
				if (value != this._clientid){_clientid= value;NotifyPropertyChanged("Clientid");}}
		}
		public  virtual string Ipenduser
		{
			get { return _ipenduser; }
			set {
				if ( value != null && value.Length > 200)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Ipenduser cannot contain more than 15 characters");
				if (value != this._ipenduser){_ipenduser= value;NotifyPropertyChanged("Ipenduser");}}
		}
		public  virtual string Enduser
		{
			get { return _enduser; }
			set {
				if ( value != null && value.Length > 500)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Enduser cannot contain more than 50 characters");
				if (value != this._enduser){_enduser= value;NotifyPropertyChanged("Enduser");}}
		}
		public  virtual string Dynamicdata
		{
			get { return _dynamicdata; }
			set {
				if ( value != null && value.Length > 2147483647)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Dynamicdata cannot contain more than 2147483647 characters");
				if (value != this._dynamicdata){_dynamicdata= value;NotifyPropertyChanged("Dynamicdata");}}
		}
		public  virtual int Companyidtx
		{
			get { return _companyidtx; }
			set {if (value != this._companyidtx){_companyidtx= value;NotifyPropertyChanged("Companyidtx");}}
		}
		public  virtual int Useridtx
		{
			get { return _useridtx; }
			set {if (value != this._useridtx){_useridtx= value;NotifyPropertyChanged("Useridtx");}}
		}
        public virtual int Operationsource
        {
            get { return _operationsource; }
            set { if (value != this._operationsource) { _operationsource = value; NotifyPropertyChanged("Operationsource"); } }
        }
        public virtual string Abs
        {
            get { return _abs; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 2147483647 characters");
                if (value != this._abs) { _abs = value; NotifyPropertyChanged("Abs"); }
            }
        }
        public virtual DateTime? Consumed
        {
            get { return _consumed; }
            set { if (value != this._consumed) { _consumed = value; NotifyPropertyChanged("Consumed"); } }
        }
        //Added 07-2020
        public virtual int CheckLock
        {
            get { return _checklock; }
            set { if (value != this._checklock) { _checklock = value; NotifyPropertyChanged("CheckLock"); } }
        }
        public virtual string EstadoLock
        {
            get { return _estadolock; }
            set
            {
                if (value != null && value.Length > 10)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 10 characters");
                if (value != this._estadolock) { _estadolock = value; NotifyPropertyChanged("EstadoLock"); }
            }
        }
        public virtual string RazonLock
        {
            get { return _razonlock; }
            set
            {
                if (value != null && value.Length > 30)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 30 characters");
                if (value != this._razonlock) { _razonlock = value; NotifyPropertyChanged("RazonLock"); }
            }
        }
        public virtual string VigenciaFromLock
        {
            get { return _vigenciafromlock; }
            set
            {
                if (value != null && value.Length > 20)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 20 characters");
                if (value != this._vigenciafromlock) { _vigenciafromlock = value; NotifyPropertyChanged("VigenciaFromLock"); }
            }
        }
        public virtual string SignatureManual
        {
            get { return _signaturemanual; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "SignatureManual cannot contain more than 2147483647 characters");
                if (value != this._signaturemanual) { _signaturemanual = value; NotifyPropertyChanged("SignatureManual"); }
            }
        }
        public virtual string TitleBpWeb
        {
            get { return _titlebpweb; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "TitleBpWeb cannot contain more than 50 characters");
                if (value != this._titlebpweb) { _titlebpweb = value; NotifyPropertyChanged("TitleBpWeb"); }
            }
        }
        public virtual string MessageBpWeb
        {
            get { return _messagebpweb; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "MessageBpWeb cannot contain more than 500 characters");
                if (value != this._messagebpweb) { _messagebpweb = value; NotifyPropertyChanged("MessageBpWeb"); }
            }
        }
        public virtual string GeoLocation
        {
            get { return _geolocation; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "GeoLocation cannot contain more than 50 characters");
                if (value != this._geolocation) { _geolocation = value; NotifyPropertyChanged("GeoLocation"); }
            }
        }
        public virtual int CheckAdult
        {
            get { return _checkadult; }
            set { if (value != this._checkadult) { _checkadult = value; NotifyPropertyChanged("CheckAdult"); } }
        }
        public virtual int CheckExpired
        {
            get { return _checkexpired; }
            set { if (value != this._checkexpired) { _checkexpired = value; NotifyPropertyChanged("CheckExpired"); } }
        }
        //----
        public virtual IList<BpTxConx> BpTxConx
		{
			get { return _bptxconx; }
			set {_bptxconx= value; }
		}
		public  virtual IList<BpVerified> BpVerified
		{
			get { return _bpverified; }
			set {_bpverified= value; }
		}

        

        protected virtual void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
		
		#endregion
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			BpTx castObj = (BpTx)obj;
			return ( castObj != null ) &&
			this._id == castObj.Id;
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57;
			hash = 27 * hash * _id.GetHashCode();
			return hash;
		}
		#endregion
		
	}
}
