﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Bio.Portal.Server.Common.Entities.Database
{
    [Serializable]
    public class BpIdentity3ro
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BpIdentity3ro));

        #region Member Variables
        protected int _id;
        protected BpIdentity _bpidentity;
        protected int _type3roservice;
        //TODO - //Contiene por ahora el TrackId de Jumio o El Facemap de Facetec 
        // Hay que hacer un Matcher FaceTec que permitira enrolar como BIR a Facetec y en esta propiesta de 
        // colocara concatenacion de => Companyid_TypeId_ValueId
        protected string _trackid3ro;  
        protected DateTime _creation;
        protected DateTime _lastupdate;


        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Constructors

        public BpIdentity3ro() { }

        public BpIdentity3ro(BpIdentity bpidentity, int type3roservice, string trackid3ro,
                             DateTime lastupdate, DateTime creation)
        {
            this._bpidentity = bpidentity;
            this._type3roservice = type3roservice;
            this._trackid3ro = trackid3ro;
            this._lastupdate = lastupdate;
            this._creation = creation;
        }

        public BpIdentity3ro(BpIdentity bpidentity, int type3roservice, string trackid3ro)
        {
            this._bpidentity = bpidentity;
            this._type3roservice = type3roservice;
            this._trackid3ro = trackid3ro;
            this._creation = DateTime.Now;
            this._lastupdate = DateTime.Now;
        }

        #endregion

        #region Public Properties
        public virtual int Id
        {
            get { return _id; }
            set { if (value != this._id) { _id = value; NotifyPropertyChanged("Id"); } }
        }
        public virtual BpIdentity BpIdentity
        {
            get { return _bpidentity; }
            set { _bpidentity = value; }
        }
        public virtual int Type3roService
        {
            get { return _type3roservice; }
            set { if (value != this._type3roservice) { _type3roservice = value; NotifyPropertyChanged("Type3roService"); } }
        }
        public virtual string TrackId3ro
        {
            get { return _trackid3ro; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "TrackId3ro cannot contain more than 2147483647 characters");
                if (value != this._trackid3ro) { _trackid3ro = value; NotifyPropertyChanged("TrackId3ro"); }
            }
        }
        public virtual DateTime LastUpdate
        {
            get { return _lastupdate; }
            set { if (value != this._lastupdate) { _lastupdate = value; NotifyPropertyChanged("LastUpdate"); } }
        }
        public virtual DateTime Creation
        {
            get { return _creation; }
            set { if (value != this._creation) { _creation = value; NotifyPropertyChanged("Creation"); } }
        }
        protected virtual void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            if ((obj == null) || (obj.GetType() != this.GetType())) return false;
            BpBir castObj = (BpBir)obj;
            return (castObj != null) &&
            this._id == castObj.Id;
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }
}
