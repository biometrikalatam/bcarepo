﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Bio.Portal.Server.Common.Entities.Database
{
    [Serializable]
    public class BpCompanyForm
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BpCompanyForm));

        #region Member Variables
        protected int _id;
        protected int _companyid;
        protected string _formid;
        protected string _jsonschema;

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Constructors

        public BpCompanyForm() { }

        public BpCompanyForm(int companyid, string formid, string jsonschema)
        {
            this._companyid = companyid;
            this._formid = formid;
            this._jsonschema = jsonschema;
        }

        #endregion

        #region Public Properties
        public virtual int Id
        {
            get { return _id; }
            set { if (value != this._id) { _id = value; NotifyPropertyChanged("Id"); } }
        }
        public virtual int CompanyId
        {
            get { return _companyid; }
            set { if (value != this._companyid) { _companyid = value; NotifyPropertyChanged("CompanyId"); } }
        }
        public virtual string FormId
        {
            get { return _formid; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "FormId cannot contain more than 50 characters");
                if (value != this._formid) { _formid = value; NotifyPropertyChanged("FormId"); }
            }
        }
        public virtual string JsonSchema
        {
            get { return _jsonschema; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "JsonSchema cannot contain more than 2147483647 characters");
                if (value != this._jsonschema) { _jsonschema = value; NotifyPropertyChanged("JsonSchema"); }
            }
        }
        protected virtual void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            if ((obj == null) || (obj.GetType() != this.GetType())) return false;
            BpBir castObj = (BpBir)obj;
            return (castObj != null) &&
            this._id == castObj.Id;
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }
}
