/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bio.Portal.Server.Common.Entities.Database
{
	/// <summary>
	/// BpTx object for NHibernate mapped table 'bp_tx'.
	/// </summary>
	[Serializable]
	public class BpTxToNotarize : INotifyPropertyChanged
	{
		#region Member Variables
		protected int _id;
		protected string _trackid;
 		protected DateTime? _datenotarized;
		public event PropertyChangedEventHandler PropertyChanged;
		
		#endregion
		#region Constructors
			
		public BpTxToNotarize() {}

        public BpTxToNotarize(string trackid)
        {
            this._trackid = trackid;
        }

       
		#endregion
		#region Public Properties
		public  virtual int Id
		{
			get { return _id; }
			set {if (value != this._id){_id= value;NotifyPropertyChanged("Id");}}
		}
		public  virtual string Trackid
		{
			get { return _trackid; }
			set {
				if ( value != null && value.Length > 100)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Trackid cannot contain more than 100 characters");
				if (value != this._trackid){_trackid= value;NotifyPropertyChanged("Trackid");}}
		}

		public  virtual DateTime? DateNotarized
		{
			get { return _datenotarized; }
			set {if (value != this._datenotarized) { _datenotarized = value;NotifyPropertyChanged("DateNotarized");}}
		}

        protected virtual void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
		
		#endregion
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			BpTx castObj = (BpTx)obj;
			return ( castObj != null ) &&
			this._id == castObj.Id;
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57;
			hash = 27 * hash * _id.GetHashCode();
			return hash;
		}
		#endregion
		
	}
}
