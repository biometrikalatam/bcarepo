/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bio.Portal.Server.Common.Entities.Database
{
	/// <summary>
	/// BpOrigin object for NHibernate mapped table 'bp_origin'.
	/// </summary>
	[Serializable]
    public class BpOrigin : INotifyPropertyChanged
	{
		#region Member Variables
		protected int _id;
		protected string _description;
		protected IList<BpTx> _bptx;
		public event PropertyChangedEventHandler PropertyChanged;
		
		#endregion
		#region Constructors
			
		public BpOrigin() {}
					
		public BpOrigin(int id, string description) 
		{
			this._id= id;
			this._description= description;
		}

		#endregion
		#region Public Properties
		public  virtual int Id
		{
			get { return _id; }
			set {if (value != this._id){_id= value;NotifyPropertyChanged("Id");}}
		}
		public  virtual string Description
		{
			get { return _description; }
			set {
				if ( value != null && value.Length > 50)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Description cannot contain more than 50 characters");
				if (value != this._description){_description= value;NotifyPropertyChanged("Description");}}
		}
		public  virtual IList<BpTx> BpTx
		{
			get { return _bptx; }
			set {_bptx= value; }
		}
		protected void NotifyPropertyChanged(String info)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(info));
			}
		}
		
		#endregion
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			BpOrigin castObj = (BpOrigin)obj;
			return ( castObj != null ) &&
			this._id == castObj.Id;
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57;
			hash = 27 * hash * _id.GetHashCode();
			return hash;
		}
		#endregion
		
	}
}
