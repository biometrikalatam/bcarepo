/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bio.Portal.Server.Common.Entities.Database
{
	/// <summary>
	/// BpTxConx object for NHibernate mapped table 'bp_tx_conx'.
	/// </summary>
	[Serializable]
    public class BpTxConx : INotifyPropertyChanged
	{
		#region Member Variables
		protected int _id;
		protected BpTx _bptx;
        /*
            Si es de BPWeb
            tv2d = 1,           //No soportada ahora
            tv3d = 2,           //Facetec
            tv3dplus = 3,       //Jumio Automatico
            tv3dstrong = 4      //Jumio Hibrido
        */
        protected string _consultationtype;
        protected string _connectorid;
        protected string _trackid;
		protected int? _status;
		protected int? _result;
		protected double? _score;
        protected double? _threshold;
		protected string _timestamp;
        //Added 04-07-2020 - Para BPWeb
        protected string _customertrackid;
        protected string _trackid3ro;
        protected string _url3ro;
        protected string _sample3ro;
        //protected int _typeverify;
        protected string _callbackurl;
        protected string _redirecturl;
        protected string _successurl;
        protected int _onboardingmandatory;
        protected int _onboardingtype;
        protected int _signerinclude;
        protected int _videoinclude;
        protected string _videomessage;
        protected string _videourl;
        
        protected int _codereject;
        protected string _descriptionreject;
        //protected int _checklock;
        protected string _theme;

        protected string _session;
        protected string _xuseragent;
        protected string _sample3rosource;
        protected int _sample3rosourcetype;
        protected string _sample3rotarget;
        protected int _sample3rotargettype;
        protected string _audittrailimage;
        protected string _lowqualityaudittrailimage;
        protected string _enrollmentIdentifier;
        protected string _idscan;
        protected string _idscanfrontimage;
        protected string _idscanbackimage;
        protected string _idscanportrait;
        protected string _idscansignature;
        protected string _responseocr;
        protected string _response3ro;
        protected string _workflow;

        protected int _autorizationinclude; //0-No incluye | 1-Si incluye (Solo par adecirle a BPWeb si muestra o no disclaimer
        protected int _autorization;  // 1-Autorizado | -1- No Autorizado (Resultado de umpdate desde BPWeb
        protected string _autorizationmessage;
        protected DateTime? _autorizationdate;

        protected string _selfie2d;
        protected string _idcardfrontimage;
        protected string _idcardbackimage;

        protected int _carregisterimageinclude; //0-No incluye | 1-Si incluye
        protected string _carregisterimagefront;
        protected string _carregisterimageback;
        protected int _writingimageinclude; //0-No incluye | 1-Si incluye
        protected string _writingimage;
        protected string _forminclude; //Null-No incluye | "idform"-Si incluye
        protected string _form;

        public event PropertyChangedEventHandler PropertyChanged;
		
		#endregion
		#region Constructors
			
		public BpTxConx() {}

        public BpTxConx(BpTx bptx, string consultationtype, string connectorid, string trackid, int? status, int? result, double? score, double? threshold, string timestamp) 
		{
			this._bptx= bptx;
			this._consultationtype= consultationtype;
            this._connectorid = connectorid;
			this._trackid= trackid;
            this._status = status;
			this._result= result;
			this._score= score;
			this._threshold= threshold;
			this._timestamp= timestamp;
		}

		public BpTxConx(BpTx bptx, string consultationtype)
		{
			this._bptx= bptx;
			this._consultationtype= consultationtype;
		}

        public BpTxConx(BpTx bptx, string consultationtype, string connectorid)
        {
            this._bptx = bptx;
            this._consultationtype = consultationtype;
            this._connectorid = connectorid;
        }
		
		#endregion
		#region Public Properties
		public  virtual int Id
		{
			get { return _id; }
			set {if (value != this._id){_id= value;NotifyPropertyChanged("Id");}}
		}
		public  virtual BpTx BpTx
		{
			get { return _bptx; }
			set {_bptx= value; }
		}
		public  virtual string Consultationtype
		{
			get { return _consultationtype; }
			set {
				if ( value != null && value.Length > 50)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Consultationtype cannot contain more than 5 characters");
				if (value != this._consultationtype){_consultationtype= value;NotifyPropertyChanged("Consultationtype");}}
		}
        public virtual string Connectorid
        {
            get { return _connectorid; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Connectorid cannot contain more than 5 characters");
                if (value != this._connectorid) { _connectorid = value; NotifyPropertyChanged("Connectorid"); }
            }
        }
		public  virtual string Trackid
		{
			get { return _trackid; }
			set {
				if ( value != null && value.Length > 100)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Trackid cannot contain more than 100 characters");
				if (value != this._trackid){_trackid= value;NotifyPropertyChanged("Trackid");}}
		}
		public  virtual int? Status
		{
			get { return _status; }
			set {if (value != this._status){_status= value;NotifyPropertyChanged("Status");}}
		}
		public  virtual int? Result
		{
			get { return _result; }
			set {if (value != this._result){_result= value;NotifyPropertyChanged("Result");}}
		}
        public virtual double? Score
		{
			get { return _score; }
			set {if (value != this._score){_score= value;NotifyPropertyChanged("Score");}}
		}
        public virtual double? Threshold
		{
			get { return _threshold; }
			set {if (value != this._threshold){_threshold= value;NotifyPropertyChanged("Threshold");}}
		}
		public  virtual string Timestamp
		{
			get { return _timestamp; }
			set {
				if ( value != null && value.Length > 25)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Timestamp cannot contain more than 25 characters");
				if (value != this._timestamp){_timestamp= value;NotifyPropertyChanged("Timestamp");}}
		}

        //Added 04-07-2020
        public virtual string CustomerTrackId
        {
            get { return _customertrackid; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "CustomertrackId cannot contain more than 100 characters");
                if (value != this._customertrackid) { _customertrackid = value; NotifyPropertyChanged("CustomerTrackId"); }
            }
        }
        public virtual string Trackid3ro
        {
            get { return _trackid3ro; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Trackid cannot contain more than 100 characters");
                if (value != this._trackid3ro) { _trackid3ro = value; NotifyPropertyChanged("Trackid3ro"); }
            }
        }
        public virtual string Url3ro
        {
            get { return _url3ro; }
            set
            {
                if (value != null && value.Length > 1024)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Url3ro cannot contain more than 1024 characters");
                if (value != this._url3ro) { _url3ro = value; NotifyPropertyChanged("Url3ro"); }
            }
        }
        public virtual string Sample3ro
        {
            get { return _sample3ro; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Sample3ro cannot contain more than 2147483647 characters");
                if (value != this._sample3ro) { _sample3ro = value; NotifyPropertyChanged("Sample3ro"); }
            }
        }
        //public virtual int TypeVerify
        //{
        //    get { return _typeverify; }
        //    set { if (value != this._typeverify) { _typeverify = value; NotifyPropertyChanged("TypeVerify"); } }
        //}
        public virtual string CallbackUrl
        {
            get { return _callbackurl; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "CallbackUrl cannot contain more than 250 characters");
                if (value != this._callbackurl) { _callbackurl = value; NotifyPropertyChanged("CallbackUrl"); }
            }
        }
        public virtual string RedirectUrl
        {
            get { return _redirecturl; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "RedirectUrl cannot contain more than 250 characters");
                if (value != this._redirecturl) { _redirecturl = value; NotifyPropertyChanged("RedirectUrl"); }
            }
        }
        public virtual string SuccessUrl
        {
            get { return _successurl; }
            set {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "SuccessUrl cannot contain more than 250 characters");
                if (value != this._successurl)
                { _successurl = value; NotifyPropertyChanged("SuccessUrl"); }
            }
        }
        public virtual string Theme
        {
            get { return _theme; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Theme cannot contain more than 50 characters");
                if (value != this._theme) { _theme = value; NotifyPropertyChanged("Theme"); }
            }
        }
        public virtual int OnboardingMandatory
        {
            get { return _onboardingmandatory; }
            set { if (value != this._onboardingmandatory) { _onboardingmandatory = value; NotifyPropertyChanged("OnboardingMandatory"); } }
        }
        public virtual int OnboardingType
        {
            get { return _onboardingtype; }
            set { if (value != this._onboardingtype) { _onboardingtype = value; NotifyPropertyChanged("OnboardingType"); } }
        }
        public virtual int SignerInclude
        {
            get { return _signerinclude; }
            set { if (value != this._signerinclude) { _signerinclude = value; NotifyPropertyChanged("SignerInclude"); } }
        }
        public virtual int VideoInclude
        {
            get { return _videoinclude; }
            set { if (value != this._videoinclude) { _videoinclude = value; NotifyPropertyChanged("VideoInclude"); } }
        }
        public virtual string VideoMessage
        {
            get { return _videomessage; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "VideoMessage cannot contain more than 2147483647 characters");
                if (value != this._videomessage) { _videomessage = value; NotifyPropertyChanged("VideoMessage"); }
            }
        }
        public virtual string VideoUrl
        {
            get { return _videourl; }
            set
            {
                if (value != null && value.Length > 512)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "VideoUrl cannot contain more than 512 characters");
                if (value != this._videourl) { _videourl = value; NotifyPropertyChanged("VideoUrl"); }
            }
        }
        public virtual int CodeReject
        {
            get { return _codereject; }
            set { if (value != this._codereject) { _codereject = value; NotifyPropertyChanged("CodeReject"); } }
        }
        public virtual string DescriptionReject
        {
            get { return _descriptionreject; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "DescriptionReject cannot contain more than 50 characters");
                if (value != this._descriptionreject) { _descriptionreject = value; NotifyPropertyChanged("DescriptionReject"); }
            }
        }
        ////public virtual int CheckLock
        ////{
        ////    get { return _signerinclude; }
        ////    set { if (value != this._checklock) { _checklock = value; NotifyPropertyChanged("CheckLock"); } }
        ////}
        public virtual string Session
        {
            get { return _session; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Session cannot contain more than 100 characters");
                if (value != this._session) { _session = value; NotifyPropertyChanged("Session"); }
            }
        }
        public virtual string XUserAgent
        {
            get { return _xuseragent; }
            set
            {
                if (value != null && value.Length > 1024)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "XUserAgent cannot contain more than 1024 characters");
                if (value != this._xuseragent) { _xuseragent = value; NotifyPropertyChanged("XUserAgent"); }
            }
        }
        public virtual string Sample3roSource
        {
            get { return _sample3rosource; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Sample3roSource cannot contain more than 2147483647 characters");
                if (value != this._sample3rosource) { _sample3rosource = value; NotifyPropertyChanged("Sample3roSource"); }
            }
        }
        public virtual int Sample3roSourceType
        {
            get { return _sample3rosourcetype; }
            set { if (value != this._sample3rosourcetype) { _sample3rosourcetype = value; NotifyPropertyChanged("Sample3roSourceType"); } }
        }
        public virtual string Sample3roTarget
        {
            get { return _sample3rotarget; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Sample3roTarget cannot contain more than 2147483647 characters");
                if (value != this._sample3rotarget) { _sample3rotarget = value; NotifyPropertyChanged("Sample3roTarget"); }
            }
        }
        public virtual int Sample3roTargetType
        {
            get { return _sample3rotargettype; }
            set { if (value != this._sample3rotargettype) { _sample3rotargettype = value; NotifyPropertyChanged("Sample3roTargetType"); } }
        }
        public virtual string AuditTrailImage
        {
            get { return _audittrailimage; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "AuditTrailImage cannot contain more than 2147483647 characters");
                if (value != this._audittrailimage) { _audittrailimage = value; NotifyPropertyChanged("AuditTrailImage"); }
            }
        }
        public virtual string LowQualityAudittrailImage
        {
            get { return _lowqualityaudittrailimage; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "LowQualityAudittrailImage cannot contain more than 2147483647 characters");
                if (value != this._lowqualityaudittrailimage) { _lowqualityaudittrailimage = value; NotifyPropertyChanged("LowQualityAudittrailImage"); }
            }
        }
        public virtual string EnrollmentIdentifier
        {
            get { return _enrollmentIdentifier; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Sample3ro cannot contain more than 100 characters");
                if (value != this._enrollmentIdentifier) { _enrollmentIdentifier = value; NotifyPropertyChanged("Sample3ro"); }
            }
        }
        public virtual string IdScan
        {
            get { return _idscan; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "IdScan cannot contain more than 2147483647 characters");
                if (value != this._idscan) { _idscan = value; NotifyPropertyChanged("IdScan"); }
            }
        }
        public virtual string IdScanFrontImage
        {
            get { return _idscanfrontimage; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "IdScanFrontImage cannot contain more than 2147483647 characters");
                if (value != this._idscanfrontimage) { _idscanfrontimage = value; NotifyPropertyChanged("IdScanFrontImage"); }
            }
        }
        public virtual string IdScanBackImage
        {
            get { return _idscanbackimage; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "IdScanBackImage cannot contain more than 2147483647 characters");
                if (value != this._idscanbackimage) { _idscanbackimage = value; NotifyPropertyChanged("IdScanBackImage"); }
            }
        }
        public virtual string IdScanPortrait
        {
            get { return _idscanportrait; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "IdScanPortrait cannot contain more than 2147483647 characters");
                if (value != this._idscanportrait) { _idscanportrait = value; NotifyPropertyChanged("IdScanPortrait"); }
            }
        }
        public virtual string IdScanSignature
        {
            get { return _idscansignature; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "IdScanSignature cannot contain more than 2147483647 characters");
                if (value != this._idscansignature) { _idscansignature = value; NotifyPropertyChanged("IdScanSignature"); }
            }
        }
        public virtual string ResponseOCR
        {
            get { return _responseocr; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "ResponseOCR cannot contain more than 2147483647 characters");
                if (value != this._responseocr) { _responseocr = value; NotifyPropertyChanged("ResponseOCR"); }
            }
        }
        public virtual string Response3ro
        {
            get { return _response3ro; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Response3ro cannot contain more than 2147483647 characters");
                if (value != this._response3ro) { _response3ro = value; NotifyPropertyChanged("Response3ro"); }
            }
        }
        public virtual string Workflow
        {
            get { return _workflow; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Workflow cannot contain more than 2147483647 characters");
                if (value != this._workflow) { _workflow = value; NotifyPropertyChanged("Workflow"); }
            }
        }
        public virtual int AutorizationInclude
        {
            get { return _autorizationinclude; }
            set { if (value != this._autorizationinclude) { _autorizationinclude = value; NotifyPropertyChanged("AutorizationInclude"); } }
        }
        public virtual int Autorization
        {
            get { return _autorization; }
            set { if (value != this._autorization) { _autorization = value; NotifyPropertyChanged("Autorization"); } }
        }
        public virtual string AutorizationMessage
        {
            get { return _autorizationmessage; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "AutorizationMessage cannot contain more than 2147483647 characters");
                if (value != this._autorizationmessage) { _autorizationmessage = value; NotifyPropertyChanged("AutorizationMessage"); }
            }
        }
        public virtual DateTime? AutorizationDate
        {
            get { return _autorizationdate; }
            set { if (value != this._autorizationdate) { _autorizationdate = value; NotifyPropertyChanged("AutorizationDate"); } }
        }
        public virtual string Selfie2d
        {
            get { return _selfie2d; }
            set {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Selfie2d cannot contain more than 2147483647 characters");
                if (value != this._selfie2d) { _selfie2d = value; NotifyPropertyChanged("Selfie2d"); }
            }
        }
        public virtual string IdCardFrontImage
        {
            get { return _idcardfrontimage; }
            set {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "IdCardFrontImage cannot contain more than 2147483647 characters");
                if (value != this._idcardfrontimage)
                { _idcardfrontimage = value; NotifyPropertyChanged("IdCardFrontImage"); }
            }
        }
        public virtual string IdCardBackImage
        {
            get { return _idcardbackimage; }
            set {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "IdCardBackImage cannot contain more than 2147483647 characters");
                if (value != this._idcardbackimage)
                { _idcardbackimage = value; NotifyPropertyChanged("IdCardBackImage"); }
            }
        }
        public virtual int CarRegisterImageInclude
        {
            get { return _carregisterimageinclude; }
            set { if (value != this._carregisterimageinclude) { _carregisterimageinclude = value; NotifyPropertyChanged("CarRegisterImageInclude"); } }
        }
        public virtual string CarRegisterImageFront
        {
            get { return _carregisterimagefront; }
            set {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "CarRegisterImageFront cannot contain more than 2147483647 characters");
                if (value != this._carregisterimagefront)
                { _carregisterimagefront = value; NotifyPropertyChanged("CarRegisterImageFront"); }
            }
        }
        public virtual string CarRegisterImageBack
        {
            get { return _carregisterimageback; }
            set {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "CarRegisterImageBack cannot contain more than 2147483647 characters");
                if (value != this._carregisterimageback)
                { _carregisterimageback = value; NotifyPropertyChanged("CarRegisterImageBack"); }
            }
        }
        public virtual int WritingImageInclude
        {
            get { return _writingimageinclude; }
            set { if (value != this._writingimageinclude) { _writingimageinclude = value; NotifyPropertyChanged("WritingImageInclude"); } }
        }
        public virtual string WritingImage
        {
            get { return _writingimage; }
            set {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "WritingImage cannot contain more than 2147483647 characters");
                if (value != this._writingimage)
                { _writingimage = value; NotifyPropertyChanged("WritingImage"); }
            }
        }
        public virtual string FormInclude
        {
            get { return _forminclude; }
            set {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "FormInclude cannot contain more than 50 characters");
                if (value != this._forminclude)
                { _forminclude = value; NotifyPropertyChanged("WritingImage"); }
            }
        }
        public virtual string Form
        {
            get { return _form; }
            set {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Form cannot contain more than 2147483647 characters");
                if (value != this._form)
                { _form = value; NotifyPropertyChanged("Form"); }
            }
        }

        protected virtual void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
		
		#endregion
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			BpTxConx castObj = (BpTxConx)obj;
			return ( castObj != null ) &&
			this._id == castObj.Id;
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57;
			hash = 27 * hash * _id.GetHashCode();
			return hash;
		}
		#endregion
		
	}
}
