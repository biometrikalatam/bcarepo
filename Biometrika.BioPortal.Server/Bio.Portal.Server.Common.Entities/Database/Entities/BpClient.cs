﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bio.Portal.Server.Common.Entities.Database
{
    /// <summary>
	/// BpTx object for NHibernate mapped table 'bp_tx'.
	/// </summary>
	[Serializable]
	public class BpClient : INotifyPropertyChanged
    {
#region Member Variables

        private bool _isChanged;
        private bool _isDeleted;
        protected int _id;
		protected int _companyid;
        protected string _clientid;
        protected DateTime? _createddate;
		protected DateTime? _lastmodify;
		protected int _status;
		public event PropertyChangedEventHandler PropertyChanged;
		
#endregion

#region Constructors

		public BpClient() {}

        public BpClient(int companyid, string clientid, int status)
        {
            this._companyid = companyid;
            this._clientid = clientid;
            this._status = status;
        }

#endregion

#region Public Properties
    
        public  virtual int Id
		{
			get { return _id; }
			set {if (value != this._id){_id= value;NotifyPropertyChanged("Id");}}
		}
        public virtual int Companyid
        {
            get { return _companyid; }
            set { if (value != this._companyid) { _companyid = value; NotifyPropertyChanged("Companyid"); } }
        }
        public  virtual string Clientid
		{
            get { return _clientid; }
			set {
				if ( value != null && value.Length > 50)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Clientid cannot contain more than 50 characters");
                if (value != this._clientid) { _clientid = value; NotifyPropertyChanged("Clientid"); }
            }
		}
        public virtual Nullable<DateTime> Createddate
        {
            get { return _createddate; }
            set { _isChanged |= (_createddate != value); _createddate = value; }
        }
        public virtual Nullable<DateTime> Lastmodify
        {
            get { return _lastmodify; }
            set { _isChanged |= (_lastmodify != value); _lastmodify = value; }
        }
        public virtual int Status
		{
            get { return _status; }
            set { if (value != this._status) { _status = value; NotifyPropertyChanged("Status"); } }
		}

        protected virtual void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
#endregion
		
#region Equals And HashCode Overrides

        /// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			BpTx castObj = (BpTx)obj;
			return ( castObj != null ) &&
			this._id == castObj.Id;
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57;
			hash = 27 * hash * _id.GetHashCode();
			return hash;
		}

#endregion
		
    }
}
