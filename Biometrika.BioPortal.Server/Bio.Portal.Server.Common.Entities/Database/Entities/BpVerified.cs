/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bio.Portal.Server.Common.Entities.Database
{
	/// <summary>
	/// BpVerified object for NHibernate mapped table 'bp_verified'.
	/// </summary>
	[Serializable]
    public class BpVerified : INotifyPropertyChanged
	{
		#region Member Variables
		protected int _id;
		protected int _datatype;
        protected string _data;
        protected string _additionaldata;
        protected DateTime _timestamp;
		protected BpTx _bptx;
		protected int? _insertoption;
        protected int? _matchingtype;
		public event PropertyChangedEventHandler PropertyChanged;
		
		#endregion
		#region Constructors
			
		public BpVerified() {}
					
		public BpVerified(int datatype, string data, string additionaldata, DateTime timestamp, BpTx bptx, 
                            int? insertoption, int? matchingtype) 
		{
			this._datatype= datatype;
			this._data= data;
			this._additionaldata= additionaldata;
			this._timestamp= timestamp;
			this._bptx= bptx;
			this._insertoption= insertoption;
            this._matchingtype = matchingtype;
		}

		public BpVerified(int datatype, string data, DateTime timestamp, BpTx bptx)
		{
			this._datatype= datatype;
			this._data= data;
			this._timestamp= timestamp;
			this._bptx= bptx;
		}
		
		#endregion
		#region Public Properties
		public  virtual int Id
		{
			get { return _id; }
			set {if (value != this._id){_id= value;NotifyPropertyChanged("Id");}}
		}
		public  virtual int Datatype
		{
			get { return _datatype; }
			set {if (value != this._datatype){_datatype= value;NotifyPropertyChanged("Datatype");}}
		}
		public  virtual string Data
		{
			get { return _data; }
			set {
				if ( value != null && value.Length > 2147483647)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 2147483647 characters");
				if (value != this._data){_data= value;NotifyPropertyChanged("Data");}}
		}
		public  virtual string Additionaldata
		{
			get { return _additionaldata; }
			set {
				if ( value != null && value.Length > 2147483647)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Data cannot contain more than 2048 characters");
				if (value != this._additionaldata){_data= value;NotifyPropertyChanged("Additionaldata");}}
		}
		public  virtual DateTime Timestamp
		{
			get { return _timestamp; }
			set {if (value != this._timestamp){_timestamp= value;NotifyPropertyChanged("Timestamp");}}
		}
		public  virtual BpTx BpTx
		{
			get { return _bptx; }
			set {_bptx= value; }
		}
		public  virtual int? Insertoption
		{
			get { return _insertoption; }
			set {if (value != this._insertoption){_insertoption= value;NotifyPropertyChanged("Insertoption");}}
		}
        public virtual int? Matchingtype
		{
            get { return _matchingtype; }
            set { if (value != this._matchingtype) { _matchingtype = value; NotifyPropertyChanged("Matchingtype"); } }
		}
        protected virtual void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
		
		#endregion
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			BpVerified castObj = (BpVerified)obj;
			return ( castObj != null ) &&
			this._id == castObj.Id;
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57;
			hash = 27 * hash * _id.GetHashCode();
			return hash;
		}
		#endregion
		
	}
}
