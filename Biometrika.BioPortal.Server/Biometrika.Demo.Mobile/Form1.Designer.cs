﻿namespace Biometrika.Demo.Mobile
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rtbSRCeI = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.rtbBP = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTypeId = new System.Windows.Forms.TextBox();
            this.txtValueId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnHuella = new System.Windows.Forms.Button();
            this.btnFacial = new System.Windows.Forms.Button();
            this.btnGetFP = new System.Windows.Forms.Button();
            this.labWorking = new System.Windows.Forms.Label();
            this.labPaso3RUT = new System.Windows.Forms.Label();
            this.labPaso3Ap = new System.Windows.Forms.Label();
            this.labPaso3Nom = new System.Windows.Forms.Label();
            this.labPaso3Fuente = new System.Windows.Forms.Label();
            this.labPaso3Result = new System.Windows.Forms.Label();
            this.labPaso3Score = new System.Windows.Forms.Label();
            this.picNV = new System.Windows.Forms.PictureBox();
            this.picOK = new System.Windows.Forms.PictureBox();
            this.picPaso3Foto = new System.Windows.Forms.PictureBox();
            this.picHuella = new System.Windows.Forms.PictureBox();
            this.picPaso3 = new System.Windows.Forms.PictureBox();
            this.picPaso2 = new System.Windows.Forms.PictureBox();
            this.picPaso1 = new System.Windows.Forms.PictureBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.picNOOK = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.timerPaso2 = new System.Windows.Forms.Timer(this.components);
            this.timerVerify = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picNV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso3Foto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHuella)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNOOK)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(571, 284);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 42);
            this.label1.TabIndex = 1;
            this.label1.Text = "Paso 2...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(933, 284);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 42);
            this.label2.TabIndex = 2;
            this.label2.Text = "Paso 3...";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.DimGray;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(55, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 25);
            this.label3.TabIndex = 3;
            this.label3.Text = "Verificación...";
            // 
            // rtbSRCeI
            // 
            this.rtbSRCeI.AutoSize = true;
            this.rtbSRCeI.BackColor = System.Drawing.Color.White;
            this.rtbSRCeI.Checked = true;
            this.rtbSRCeI.Location = new System.Drawing.Point(102, 192);
            this.rtbSRCeI.Name = "rtbSRCeI";
            this.rtbSRCeI.Size = new System.Drawing.Size(127, 17);
            this.rtbSRCeI.TabIndex = 4;
            this.rtbSRCeI.TabStop = true;
            this.rtbSRCeI.Text = "Verifica contra SRCeI";
            this.rtbSRCeI.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(72, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Seleccione Fuente...";
            // 
            // rtbBP
            // 
            this.rtbBP.AutoSize = true;
            this.rtbBP.BackColor = System.Drawing.Color.White;
            this.rtbBP.Location = new System.Drawing.Point(102, 215);
            this.rtbBP.Name = "rtbBP";
            this.rtbBP.Size = new System.Drawing.Size(172, 17);
            this.rtbBP.TabIndex = 6;
            this.rtbBP.Text = "Verifica contra BioPortal Server";
            this.rtbBP.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.DimGray;
            this.label5.Location = new System.Drawing.Point(72, 269);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(191, 20);
            this.label5.TabIndex = 7;
            this.label5.Text = "Ingrese Identificador...";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DimGray;
            this.label6.Location = new System.Drawing.Point(84, 302);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 16);
            this.label6.TabIndex = 8;
            this.label6.Text = "Tipo";
            // 
            // txtTypeId
            // 
            this.txtTypeId.Location = new System.Drawing.Point(129, 301);
            this.txtTypeId.Name = "txtTypeId";
            this.txtTypeId.Size = new System.Drawing.Size(165, 20);
            this.txtTypeId.TabIndex = 9;
            this.txtTypeId.Text = "RUT";
            // 
            // txtValueId
            // 
            this.txtValueId.Location = new System.Drawing.Point(129, 327);
            this.txtValueId.Name = "txtValueId";
            this.txtValueId.Size = new System.Drawing.Size(165, 20);
            this.txtValueId.TabIndex = 11;
            this.txtValueId.Text = "21284415-2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(78, 327);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 16);
            this.label7.TabIndex = 10;
            this.label7.Text = "Valor";
            // 
            // btnHuella
            // 
            this.btnHuella.Location = new System.Drawing.Point(59, 399);
            this.btnHuella.Name = "btnHuella";
            this.btnHuella.Size = new System.Drawing.Size(259, 35);
            this.btnHuella.TabIndex = 12;
            this.btnHuella.Text = "Verificar con Huella";
            this.btnHuella.UseVisualStyleBackColor = true;
            this.btnHuella.Click += new System.EventHandler(this.btnHuella_Click);
            // 
            // btnFacial
            // 
            this.btnFacial.Location = new System.Drawing.Point(60, 452);
            this.btnFacial.Name = "btnFacial";
            this.btnFacial.Size = new System.Drawing.Size(259, 35);
            this.btnFacial.TabIndex = 13;
            this.btnFacial.Text = "Verificar con Facial";
            this.btnFacial.UseVisualStyleBackColor = true;
            // 
            // btnGetFP
            // 
            this.btnGetFP.Location = new System.Drawing.Point(602, 163);
            this.btnGetFP.Name = "btnGetFP";
            this.btnGetFP.Size = new System.Drawing.Size(118, 35);
            this.btnGetFP.TabIndex = 16;
            this.btnGetFP.Text = "Tomar Huella";
            this.btnGetFP.UseVisualStyleBackColor = true;
            this.btnGetFP.Visible = false;
            this.btnGetFP.Click += new System.EventHandler(this.btnGetFP_Click);
            // 
            // labWorking
            // 
            this.labWorking.AutoSize = true;
            this.labWorking.BackColor = System.Drawing.Color.White;
            this.labWorking.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labWorking.ForeColor = System.Drawing.Color.SteelBlue;
            this.labWorking.Location = new System.Drawing.Point(487, 412);
            this.labWorking.Name = "labWorking";
            this.labWorking.Size = new System.Drawing.Size(188, 31);
            this.labWorking.TabIndex = 19;
            this.labWorking.Text = "Verificando...";
            this.labWorking.Visible = false;
            // 
            // labPaso3RUT
            // 
            this.labPaso3RUT.AutoSize = true;
            this.labPaso3RUT.BackColor = System.Drawing.Color.White;
            this.labPaso3RUT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPaso3RUT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.labPaso3RUT.Location = new System.Drawing.Point(961, 192);
            this.labPaso3RUT.Name = "labPaso3RUT";
            this.labPaso3RUT.Size = new System.Drawing.Size(105, 20);
            this.labPaso3RUT.TabIndex = 20;
            this.labPaso3RUT.Text = "21284415-2";
            this.labPaso3RUT.Visible = false;
            // 
            // labPaso3Ap
            // 
            this.labPaso3Ap.AutoSize = true;
            this.labPaso3Ap.BackColor = System.Drawing.Color.White;
            this.labPaso3Ap.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPaso3Ap.ForeColor = System.Drawing.Color.DimGray;
            this.labPaso3Ap.Location = new System.Drawing.Point(961, 215);
            this.labPaso3Ap.Name = "labPaso3Ap";
            this.labPaso3Ap.Size = new System.Drawing.Size(54, 16);
            this.labPaso3Ap.TabIndex = 21;
            this.labPaso3Ap.Text = "SUHIT";
            this.labPaso3Ap.Visible = false;
            // 
            // labPaso3Nom
            // 
            this.labPaso3Nom.AutoSize = true;
            this.labPaso3Nom.BackColor = System.Drawing.Color.White;
            this.labPaso3Nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPaso3Nom.ForeColor = System.Drawing.Color.DimGray;
            this.labPaso3Nom.Location = new System.Drawing.Point(961, 235);
            this.labPaso3Nom.Name = "labPaso3Nom";
            this.labPaso3Nom.Size = new System.Drawing.Size(160, 16);
            this.labPaso3Nom.TabIndex = 22;
            this.labPaso3Nom.Text = "GUSTAVO GERARDO";
            this.labPaso3Nom.Visible = false;
            // 
            // labPaso3Fuente
            // 
            this.labPaso3Fuente.AutoSize = true;
            this.labPaso3Fuente.BackColor = System.Drawing.Color.White;
            this.labPaso3Fuente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPaso3Fuente.ForeColor = System.Drawing.Color.DimGray;
            this.labPaso3Fuente.Location = new System.Drawing.Point(889, 134);
            this.labPaso3Fuente.Name = "labPaso3Fuente";
            this.labPaso3Fuente.Size = new System.Drawing.Size(139, 20);
            this.labPaso3Fuente.TabIndex = 23;
            this.labPaso3Fuente.Text = "Fuente:   SRCeI";
            this.labPaso3Fuente.Visible = false;
            // 
            // labPaso3Result
            // 
            this.labPaso3Result.AutoSize = true;
            this.labPaso3Result.BackColor = System.Drawing.Color.White;
            this.labPaso3Result.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPaso3Result.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labPaso3Result.Location = new System.Drawing.Point(938, 344);
            this.labPaso3Result.Name = "labPaso3Result";
            this.labPaso3Result.Size = new System.Drawing.Size(170, 20);
            this.labPaso3Result.TabIndex = 25;
            this.labPaso3Result.Text = "Verificación Positiva";
            this.labPaso3Result.Visible = false;
            // 
            // labPaso3Score
            // 
            this.labPaso3Score.AutoSize = true;
            this.labPaso3Score.BackColor = System.Drawing.Color.White;
            this.labPaso3Score.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPaso3Score.ForeColor = System.Drawing.Color.DimGray;
            this.labPaso3Score.Location = new System.Drawing.Point(952, 378);
            this.labPaso3Score.Name = "labPaso3Score";
            this.labPaso3Score.Size = new System.Drawing.Size(136, 15);
            this.labPaso3Score.TabIndex = 26;
            this.labPaso3Score.Text = "Score: 13210 / 3500";
            this.labPaso3Score.Visible = false;
            // 
            // picNV
            // 
            this.picNV.Image = global::Biometrika.Demo.Mobile.Properties.Resources.Logo_50_50_50p;
            this.picNV.Location = new System.Drawing.Point(1040, 444);
            this.picNV.Name = "picNV";
            this.picNV.Size = new System.Drawing.Size(68, 43);
            this.picNV.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picNV.TabIndex = 27;
            this.picNV.TabStop = false;
            this.picNV.Visible = false;
            this.picNV.Click += new System.EventHandler(this.picNV_Click);
            this.picNV.DoubleClick += new System.EventHandler(this.picNV_DoubleClick);
            // 
            // picOK
            // 
            this.picOK.Image = global::Biometrika.Demo.Mobile.Properties.Resources.Good_Shield_80x80;
            this.picOK.Location = new System.Drawing.Point(853, 327);
            this.picOK.Name = "picOK";
            this.picOK.Size = new System.Drawing.Size(79, 80);
            this.picOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picOK.TabIndex = 24;
            this.picOK.TabStop = false;
            this.picOK.Visible = false;
            // 
            // picPaso3Foto
            // 
            this.picPaso3Foto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picPaso3Foto.Image = global::Biometrika.Demo.Mobile.Properties.Resources.no_picture;
            this.picPaso3Foto.Location = new System.Drawing.Point(824, 169);
            this.picPaso3Foto.Name = "picPaso3Foto";
            this.picPaso3Foto.Size = new System.Drawing.Size(131, 143);
            this.picPaso3Foto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPaso3Foto.TabIndex = 18;
            this.picPaso3Foto.TabStop = false;
            this.picPaso3Foto.Visible = false;
            // 
            // picHuella
            // 
            this.picHuella.BackColor = System.Drawing.Color.White;
            this.picHuella.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picHuella.Image = global::Biometrika.Demo.Mobile.Properties.Resources.FondoBPC_Transp;
            this.picHuella.Location = new System.Drawing.Point(440, 163);
            this.picHuella.Name = "picHuella";
            this.picHuella.Size = new System.Drawing.Size(156, 172);
            this.picHuella.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picHuella.TabIndex = 17;
            this.picHuella.TabStop = false;
            this.picHuella.Visible = false;
            // 
            // picPaso3
            // 
            this.picPaso3.Image = global::Biometrika.Demo.Mobile.Properties.Resources.VerifySRCeI_1;
            this.picPaso3.Location = new System.Drawing.Point(795, 12);
            this.picPaso3.Name = "picPaso3";
            this.picPaso3.Size = new System.Drawing.Size(362, 644);
            this.picPaso3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picPaso3.TabIndex = 15;
            this.picPaso3.TabStop = false;
            this.picPaso3.Visible = false;
            // 
            // picPaso2
            // 
            this.picPaso2.Image = global::Biometrika.Demo.Mobile.Properties.Resources.VerifySRCeI_1;
            this.picPaso2.Location = new System.Drawing.Point(403, 12);
            this.picPaso2.Name = "picPaso2";
            this.picPaso2.Size = new System.Drawing.Size(362, 644);
            this.picPaso2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picPaso2.TabIndex = 14;
            this.picPaso2.TabStop = false;
            this.picPaso2.Visible = false;
            // 
            // picPaso1
            // 
            this.picPaso1.Image = global::Biometrika.Demo.Mobile.Properties.Resources.VerifySRCeI_1;
            this.picPaso1.Location = new System.Drawing.Point(12, 12);
            this.picPaso1.Name = "picPaso1";
            this.picPaso1.Size = new System.Drawing.Size(362, 644);
            this.picPaso1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picPaso1.TabIndex = 0;
            this.picPaso1.TabStop = false;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(910, 504);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(118, 32);
            this.btnReset.TabIndex = 28;
            this.btnReset.Text = "Inicio...";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Visible = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.DimGray;
            this.label8.Location = new System.Drawing.Point(933, 448);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 15);
            this.label8.TabIndex = 29;
            this.label8.Text = "Ver Certificado";
            this.label8.Visible = false;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label9.Location = new System.Drawing.Point(933, 468);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 15);
            this.label9.TabIndex = 30;
            this.label9.Text = "Notario Virtual";
            this.label9.Visible = false;
            // 
            // picNOOK
            // 
            this.picNOOK.Image = global::Biometrika.Demo.Mobile.Properties.Resources.Error_Shield_80x80;
            this.picNOOK.Location = new System.Drawing.Point(853, 328);
            this.picNOOK.Name = "picNOOK";
            this.picNOOK.Size = new System.Drawing.Size(79, 80);
            this.picNOOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picNOOK.TabIndex = 31;
            this.picNOOK.TabStop = false;
            this.picNOOK.Visible = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // timerPaso2
            // 
            this.timerPaso2.Interval = 200;
            this.timerPaso2.Tick += new System.EventHandler(this.timerPaso2_Tick);
            // 
            // timerVerify
            // 
            this.timerVerify.Interval = 200;
            this.timerVerify.Tick += new System.EventHandler(this.timerVerify_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1169, 669);
            this.Controls.Add(this.labWorking);
            this.Controls.Add(this.picHuella);
            this.Controls.Add(this.btnGetFP);
            this.Controls.Add(this.picOK);
            this.Controls.Add(this.picNOOK);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.picNV);
            this.Controls.Add(this.labPaso3Score);
            this.Controls.Add(this.labPaso3Result);
            this.Controls.Add(this.labPaso3Fuente);
            this.Controls.Add(this.labPaso3Nom);
            this.Controls.Add(this.labPaso3Ap);
            this.Controls.Add(this.labPaso3RUT);
            this.Controls.Add(this.picPaso3Foto);
            this.Controls.Add(this.picPaso3);
            this.Controls.Add(this.picPaso2);
            this.Controls.Add(this.btnFacial);
            this.Controls.Add(this.btnHuella);
            this.Controls.Add(this.txtValueId);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtTypeId);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.rtbBP);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.rtbSRCeI);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.picPaso1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.picNV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso3Foto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHuella)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNOOK)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picPaso1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rtbSRCeI;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rtbBP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTypeId;
        private System.Windows.Forms.TextBox txtValueId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnHuella;
        private System.Windows.Forms.Button btnFacial;
        private System.Windows.Forms.PictureBox picPaso2;
        private System.Windows.Forms.PictureBox picPaso3;
        private System.Windows.Forms.Button btnGetFP;
        private System.Windows.Forms.PictureBox picHuella;
        private System.Windows.Forms.PictureBox picPaso3Foto;
        private System.Windows.Forms.Label labWorking;
        private System.Windows.Forms.Label labPaso3RUT;
        private System.Windows.Forms.Label labPaso3Ap;
        private System.Windows.Forms.Label labPaso3Nom;
        private System.Windows.Forms.Label labPaso3Fuente;
        private System.Windows.Forms.PictureBox picOK;
        private System.Windows.Forms.Label labPaso3Result;
        private System.Windows.Forms.Label labPaso3Score;
        private System.Windows.Forms.PictureBox picNV;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox picNOOK;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Timer timerPaso2;
        private System.Windows.Forms.Timer timerVerify;
    }
}

