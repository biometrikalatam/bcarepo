﻿using Bio.Core.Api;
using BioPortal.Server.Api;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Biometrika.Demo.Mobile
{
    public partial class Form1 : Form
    {
        string _CurrentSampleb64;
        string _CurrentReceipt;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnHuella_Click(object sender, EventArgs e)
        {

            SetVisible(true, 2);
           

        }

        private void SetPaso(int paso)
        {
            switch (paso)
            {
                case 1:
                    _CurrentSampleb64 = null;
                    _CurrentReceipt = null;
                    picHuella.Image = Properties.Resources.FondoBPC_Transp;
                    picHuella.Visible = false;
                    btnGetFP.Visible = false;
                    picPaso2.Visible = false;
                    labWorking.Visible = false;
                    labPaso3Fuente.Visible = false;
                    labPaso3RUT.Visible = false;
                    labPaso3Ap.Visible = false;
                    labPaso3Nom.Visible = false;
                    labPaso3Score.Visible = false;
                    picPaso3Foto.Visible = false;
                    label8.Visible = false;
                    label9.Visible = false;
                    picNV.Visible = false;
                    btnReset.Visible = false;
                    picPaso3.Visible = false;
                    labPaso3Result.Visible = false;
                    picOK.Visible = false;
                    picNOOK.Visible = false;
                    this.Refresh();
                    break;
                case 3:
                    labPaso3Fuente.Visible = true;
                    labPaso3RUT.Visible = true;
                    labPaso3Ap.Visible = true;
                    labPaso3Nom.Visible = true;
                    labPaso3Score.Visible = true;
                    picPaso3Foto.Visible = true;
                    labPaso3Result.Visible = true;
                    label8.Visible = true;
                    label9.Visible = true;
                    picNV.Visible = true;
                    btnReset.Visible = true;
                    labWorking.Visible = false;
                    picPaso3.Visible = true;
                    this.Refresh();
                    break;
                default:
                    break;
            }
        }

        private void SetVisible(bool on, int paso)
        {
            switch (paso)
            {
                case 2:
                    picHuella.Visible = on;
                    btnGetFP.Visible = on;
                    picPaso2.Visible = on;
                    this.Refresh();
                    break;
                default:
                    break;
            }
        }

        private void btnGetFP_Click(object sender, EventArgs e)
        {
            
            try
            {
                openFileDialog1.Title = "Seleccione sample en Base 64...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
                    _CurrentSampleb64 = sr.ReadToEnd();
                    sr.Close();

                    Bio.Core.Wsq.Decoder.WsqDecoder wsqdec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                    short w, h;
                    byte[] raw;
                    if (wsqdec.DecodeMemory(Convert.FromBase64String(_CurrentSampleb64), out w, out h, out raw))
                    {
                        picHuella.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw, w, h);
                        timerPaso2.Enabled = true;
                    }
                    else
                    {
                        picHuella.Image = null;
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception = " + ex.StackTrace);
            }
        }

        private void timerPaso2_Tick(object sender, EventArgs e)
        {
            timerPaso2.Enabled = false;
            labWorking.Visible = true;
            labWorking.BringToFront(); 
            this.Refresh();
            timerVerify.Enabled = true;
        }

        private void timerVerify_Tick(object sender, EventArgs e)
        {
            timerVerify.Enabled = false;
            DoSinToken();
        }


        private void DoSinToken()
        {
            int expected = 0;
            int actual;
            string xmlparamout = string.Empty;
            string xmlparamout2 = string.Empty;

            try
            {
                using (BioPortalServerWS.BioPortalServerWS target = new BioPortalServerWS.BioPortalServerWS())
                {
                    target.Timeout = 30000;
                    target.Url = Properties.Settings.Default.Biometrika_Demo_Mobile_BioPortalServerWS_BioPortalServerWS;
                    XmlParamIn pin = new XmlParamIn();

                    pin.Authenticationfactor = 2; //Fingerprint
                    pin.Minutiaetype = 13;
                    Sample sample = new Sample();
                    sample.Minutiaetype = 21; //WSQ
                    sample.Data = _CurrentSampleb64;
                    pin.SampleCollection = new List<Sample>();
                    pin.SampleCollection.Add(sample);
                    pin.Clientid = "IdClienteTest";
                    pin.Companyid = Properties.Settings.Default.Company;
                    pin.Enduser = "EnUserTest";
                    pin.Ipenduser = "127.0.0.1";
                    pin.Matchingtype = 1;
                    pin.Origin = 1;
                    pin.Userid = 0;
                    pin.Verifybyconnectorid = "SRCeI2015";  //SRCeI2018
                    pin.InsertOption = 1;
                    pin.PersonalData = new PersonalData();
                    pin.PersonalData.Typeid = txtTypeId.Text.Trim();
                    pin.PersonalData.Valueid = txtValueId.Text.Trim();
                    if (rtbSRCeI.Checked)
                    {
                        pin.Actionid = 1; //Verify
                        pin.OperationOrder = 3; //Solo Remote
                        pin.Threshold = 3500; //No aplica
                        pin.Bodypart = 1;
                    }
                    else
                    {
                        pin.Bodypart = -1;
                        pin.Actionid = 9; //Verify and Get
                        pin.OperationOrder = 1; //Solo Remote
                        pin.Threshold = 75; //No aplica
                        pin.Minutiaetype = 7;
                    }
                    string xmlparamin = XmlUtils.SerializeAnObject(pin);
                    actual = target.Verify(xmlparamin, out xmlparamout);

                    if (actual != 0)
                    {
                        MessageBox.Show("Verify error [" + actual + "]");
                        SetPaso(1);
                    }
                    else
                    {
                        if (rtbSRCeI.Checked)  //Si es SRCeI debo hacer get aparte
                        {
                            pin.Actionid = 4; //Get
                            pin.Additionaldata = null;
                            pin.Authenticationfactor = 2; //Fingerprint
                            pin.Minutiaetype = 0; //No aplica porque va a 
                            pin.Clientid = "IdClienteTest";
                            pin.Companyid = Properties.Settings.Default.Company;
                            pin.Enduser = "EnUserTest";
                            pin.Ipenduser = "127.0.0.1";
                            pin.Matchingtype = 1;
                            pin.Origin = 1;
                            pin.SampleCollection = null;
                            pin.SaveVerified = 1;
                            pin.Threshold = 0; //No aplica
                            pin.Userid = 0;
                            pin.Verifybyconnectorid = "SRCeI2015";
                            pin.InsertOption = 1;

                            xmlparamin = XmlUtils.SerializeAnObject(pin);
                            actual = target.Get(xmlparamin, out xmlparamout2);

                            if (actual != 0)
                            {
                                MessageBox.Show("Get SRCeI error [" + actual + "]");
                                SetPaso(1);
                            }
                        }

                        XmlParamOut paramout2;
                        XmlParamOut paramout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                        if (!String.IsNullOrEmpty(xmlparamout2))
                        {
                            paramout2 = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout2);
                            paramout.PersonalData = paramout2.PersonalData;
                        }

                        ShowResult(paramout);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception DoSinToken = " + ex.StackTrace);
            }
        }

        private Image GetImage(string photography)
        {
            if (!String.IsNullOrEmpty(photography))
            {
                Image imagen = null;
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(photography)))
                {
                    imagen = new Bitmap(ms);
                }
               return imagen;
            }
            else
            {
                return Properties.Resources.no_picture;
            }
        }

        private void ShowResult(XmlParamOut paramout)
        {
            try
            {
                if (rtbSRCeI.Checked) labPaso3Fuente.Text = "Fuente = SRCeI";
                else labPaso3Fuente.Text = "Fuente = BioPortal Local";
                labPaso3RUT.Text = paramout.PersonalData.Valueid;

                labWorking.Visible = false;
                if (paramout.Result == 1)
                {
                    picOK.Visible = true;
                    picOK.BringToFront();
                    picNOOK.Visible = false;
                    labPaso3Result.Text = "Verificación Positiva";
                    labPaso3Result.ForeColor = System.Drawing.Color.Green;
                    string ap = paramout.PersonalData.Patherlastname + " " + paramout.PersonalData.Motherlastname;
                    if (!String.IsNullOrEmpty(ap) && ap.Length > 22) labPaso3Ap.Text = ap.Substring(0, 21);
                    else labPaso3Ap.Text = ap;
                    string name = paramout.PersonalData.Name;
                    if (!String.IsNullOrEmpty(name) && name.Length > 22) labPaso3Nom.Text = name.Substring(0, 21);
                    else labPaso3Nom.Text = name;
                    labPaso3Score.Text = "Score: " + paramout.Score.ToString() + " / " + paramout.Threshold.ToString();
                    picPaso3Foto.Image = GetImage(paramout.PersonalData.Photography);
                    _CurrentReceipt = GetABS(paramout.Additionaldata);
                }
                else
                {
                    picOK.Visible = false;
                    picNOOK.Visible = true;
                    picNOOK.BringToFront();
                    labPaso3Result.Text = "Verificación Negativa";
                    labPaso3Result.ForeColor = System.Drawing.Color.Red;
                    labPaso3Ap.Text = "??";
                    labPaso3Nom.Text = "??";
                    labPaso3Score.Text = "Score: " + paramout.Score.ToString() + " / " + paramout.Threshold.ToString();
                    picPaso3Foto.Image = GetImage(null);
                    _CurrentReceipt = null;
                }
                SetPaso(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception ShowResult = " + ex.StackTrace);
            }

        }

        private string GetABS(DynamicData additionaldata)
        {
            string rd = null;
            try
            {
                rd = (string)additionaldata.GetValue("BioSignature");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception GetABS = " + ex.StackTrace);
                rd = null;
            }
            return rd;
        }

        private void picNV_DoubleClick(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(_CurrentReceipt)) ShowRD(_CurrentReceipt);
            else MessageBox.Show("No existe recibi Digital para mostrar!");
        }

        private void ShowRD(string rd)
        {
            MessageBox.Show(rd);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            SetPaso(1);
        }

        private void label8_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(_CurrentReceipt)) ShowRD(_CurrentReceipt);
            else MessageBox.Show("No existe recibi Digital para mostrar!");
        }

        private void picNV_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(_CurrentReceipt)) ShowRD(_CurrentReceipt);
            else MessageBox.Show("No existe recibi Digital para mostrar!");
        }
    }
}
