﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestNamkurestApi
{
    public class Location
    {
        public int left { get; set; }
        public int top { get; set; }
        public int right { get; set; }
        public int bottom { get; set; }
    }

    public class DniData
    {
        public int dni_type { get; set; }
        public string names { get; set; }
        public string nacionality { get; set; }
        public string gender { get; set; }
        public string birthday { get; set; }
        public string lastname { get; set; }
        public string face { get; set; }
        public string emission_date { get; set; }
        public string sign { get; set; }
        public string expiration_date { get; set; }
        public string dni_number { get; set; }
        public string doc_number { get; set; }
        public string lastname_2 { get; set; }
    }

    public partial class Data
    {
        public Location location { get; set; }
        public double angle { get; set; }
        public DniData dni_data { get; set; }
        public double tiempo { get; set; }
    }

    public class NamkuResponse
    {
        public string api_version { get; set; }
        public Data data { get; set; }
    }
}

