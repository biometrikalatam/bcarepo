﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestFacePlusPlus
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //最佳图片

            FileStream streamBest = new FileInfo(@"C:\TFSN\TFS - BioPortal Server 5.5\Proyectos\Bio.Portal.Server.v5.8-branch-branch\TestFacePlusPlus\bin\Debug\21284415-2_0.jpg").OpenRead();

            byte[] bufferBest = new byte[streamBest.Length];

            streamBest.Read(bufferBest, 0, Convert.ToInt32(streamBest.Length));

            //全景图片

            FileStream streamEnv = new FileInfo(@"C:\TFSN\TFS - BioPortal Server 5.5\Proyectos\Bio.Portal.Server.v5.8-branch-branch\TestFacePlusPlus\bin\Debug\601014310_face.jpg").OpenRead();

            byte[] bufferEnv = new byte[streamEnv.Length];

            streamEnv.Read(bufferEnv, 0, Convert.ToInt32(streamEnv.Length));

            //参数

            var requst = new RestRequest(Method.POST);

            requst.AddParameter("api_key", "C9vnvwF9q2UEY9EuvF6mwPl_Osh5V0QR");

            requst.AddParameter("api_secret", "xxxxxxxx");

            requst.AddParameter("comparison_type", 1);

            requst.AddParameter("idcard_name", "张三");

            requst.AddParameter("idcard_number", "xxxxxxxxxxxxxxxxxx");

            requst.AddParameter("face_image_type", "meglive");

            requst.AddParameter("delta", "xxxx");

            requst.AddFileBytes("image_best", bufferBest, "image_best");

            requst.AddFileBytes("image_env", bufferEnv, "image_best");

            //请求

            var restClient = new RestClient { BaseUrl = new Uri("https://api.megvii.com/faceid/v2/verify") };

            restClient.ExecuteAsync(requst, (response) =>

            {

                Console.WriteLine(response.StatusCode);

                Console.WriteLine(response.Content);

            });



            Console.ReadLine();
        }
    }
}
