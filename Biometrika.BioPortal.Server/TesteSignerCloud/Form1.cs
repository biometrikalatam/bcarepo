﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TesteSignerCloud
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.IO.FileStream fs = new System.IO.FileStream(textBox1.Text, System.IO.FileMode.Open);
            byte[] byFile = new byte[fs.Length];
            int res = fs.Read(byFile, 0, (int) fs.Length);
            fs.Close();

            string strFile = Convert.ToBase64String(byFile);

            using (cl.servisign.esigner6.WSIntercambiaDocSoapService wssigner = new cl.servisign.esigner6.WSIntercambiaDocSoapService())
            {
                wssigner.Timeout = 60000;
                cl.servisign.esigner6.intercambiaDocEncabezado enc = new cl.servisign.esigner6.intercambiaDocEncabezado();
                enc.User = "biometrika";
                enc.Password = "1234";
                enc.NombreConfiguracion = "BIOPDF453";

                cl.servisign.esigner6.intercambiaDocParametro param = new cl.servisign.esigner6.intercambiaDocParametro();
                param.Documento = strFile;
                param.NombreDocumento = "Entradas.pdf";
                param.MetaData = null;

                cl.servisign.esigner6.intercambiaDocResponseIntercambiaDoc resp = wssigner.intercambiaDoc(enc, param);

                if (resp != null)
                {
                    MessageBox.Show("Resultado => " + resp.IntercambiaDocResult.NombreDocumento + " => " + resp.IntercambiaDocResult.Estado);

                    if (resp.IntercambiaDocResult.Estado.Equals("OK"))
                    {
                        System.IO.FileStream fs1 = new System.IO.FileStream(resp.IntercambiaDocResult.NombreDocumento, System.IO.FileMode.CreateNew);
                        byte[] byArrResp = Convert.FromBase64String(resp.IntercambiaDocResult.Documento);
                        fs1.Write(byArrResp, 0, byArrResp.Length);
                        fs1.Close();
                    }
                    else
                    {
                        MessageBox.Show("Estado => FAIL!");
                    }
                }
                else
                {
                    MessageBox.Show("Resp NULL!");
                }


            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.IO.FileStream fs = new System.IO.FileStream(textBox2.Text, System.IO.FileMode.Open);
            byte[] byFile = new byte[fs.Length];
            int res = fs.Read(byFile, 0, (int)fs.Length);
            fs.Close();

            string strFile = Convert.ToBase64String(byFile);
            using (cl.servisign.esigner6.WSIntercambiaDocSoapService wssigner = new cl.servisign.esigner6.WSIntercambiaDocSoapService())
            {
                wssigner.Timeout = 60000;
                cl.servisign.esigner6.intercambiaDocEncabezado enc = new cl.servisign.esigner6.intercambiaDocEncabezado();
                enc.User = "biometrika";
                enc.Password = "1234";
                enc.NombreConfiguracion = "BIOXML454";

                cl.servisign.esigner6.intercambiaDocParametro param = new cl.servisign.esigner6.intercambiaDocParametro();
                param.Documento = strFile;
                param.NombreDocumento = "test.xml";
                param.MetaData = null;

                 cl.servisign.esigner6.intercambiaDocResponseIntercambiaDoc resp = wssigner.intercambiaDoc(enc, param);

                if (resp != null)
                {
                    MessageBox.Show("Resultado => " + resp.IntercambiaDocResult.NombreDocumento + " => " + resp.IntercambiaDocResult.Estado);

                    if (resp.IntercambiaDocResult.Estado.Equals("OK"))
                    {
                        this.textBox3.Text = resp.IntercambiaDocResult.NombreDocumento;
                        System.IO.FileStream fs1 = new System.IO.FileStream(resp.IntercambiaDocResult.NombreDocumento, System.IO.FileMode.CreateNew);
                        byte[] byArrResp = Convert.FromBase64String(resp.IntercambiaDocResult.Documento);
                        fs1.Write(byArrResp, 0, byArrResp.Length);
                        fs1.Close();
                    }
                    else
                    {
                        MessageBox.Show("Estado => FAIL!");
                    }
                }
                else
                {
                    MessageBox.Show("Resp NULL!");
                }


            }

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(textBox3.Text);
            //bool b = Bio.Core.pki.xmldsig.BKSignerXML.VerifyXml(doc, null);

            int i = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
