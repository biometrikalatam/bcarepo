﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Api;
using Bio.Core.Api.Matcher.Interface;
using Bio.Core.Constant;
using Bio.Core.Matcher.ConnectorBioPortal4.BioPortalServerWS;
using BioPortal.Server.Api;
using log4net;

namespace Bio.Core.Matcher.ConnectorBioPortal4
{
    public class Connector : IConnector
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Connector));

#region Private properties propietario
        
        private string _urlWS;
        private string _urlWSWEB ;
        private int _timeout = 30000;
        private bool _isConfigured; //Solo para control en initialization

#endregion Private properties propietario

#region Public properties propietario

        public string UrlWs
        {
            get { return _urlWS; }
            set { _urlWS = value; }
        }

        public string UrlWsweb
        {
            get { return _urlWSWEB; }
            set { _urlWSWEB = value; }
        }

        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

#endregion Public properties propietario

#region Private Method

        /// <summary>
        /// Inicializa las variables del objeto desde config, para no perder tiempo en buquedas luego
        /// </summary>
        private void Initialization()
        {
            try
            {
                if (_config == null || _config.DynamicDataItems == null)
                {
                    _isConfigured = false;
                    return;
                }

                foreach (DynamicDataItem dd in _config.DynamicDataItems)
                {
                    if (dd.key.Trim().Equals("UrlWS"))
                    {
                        _urlWS = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("UrlWSWEB"))
                    {
                        _urlWSWEB = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("Timeout"))
                    {
                        try
                        {
                            _timeout = Convert.ToInt32(dd.value.Trim());
                        }
                        catch (Exception ex)
                        {
                            _timeout = 30000;
                        }
                    }
                }
                _isConfigured = true;
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.Initialization Error", ex);
                _isConfigured = false;
            }
        }

#endregion Private Method

#region Implementation of IConnector

        private string _connectorId;

        private DynamicData _config;

        /// <summary>
        /// Id identificador del conector
        /// </summary>
        public string ConnectorId
        {
            get { return _connectorId; }
            set { _connectorId = value; }
        }

        /// <summary>
        /// Pares de key/value de configuracion para el conector
        /// </summary>
        public DynamicData Config
        {
            get { return _config; }
            set
            {
                 _config = value;
                 if (!_isConfigured) Initialization();
            }
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */
            
            string msg;
            string xmloutbp;
            int iretremoto;

            DynamicData oXmlout = new DynamicData();
            oXmlout.AddValue("message", "");
            oXmlout.AddValue("trackid", "");
            oXmlout.AddValue("status", "");
            oXmlout.AddValue("result", "");
            oXmlout.AddValue("score", "");
            oXmlout.AddValue("threshold", "");
            oXmlout.AddValue("timestamp", "");

            try
            {
                if (!this._isConfigured)
                {
                    msg = "Bio.Core.Matcher.ConnectorBioPortal4 Error = Connector No Configurado!";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
                }
                using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
                {
                    ws.Timeout = this._timeout;
                    ws.Url = this._urlWS;

                    iretremoto = ws.Verify(xmlinput, out xmloutbp);
                    if (iretremoto == Errors.IERR_OK)
                    {
                        XmlParamOut oXmlParamOut = Utils.XmlUtils.DeserializeObject<XmlParamOut>(xmloutbp);
                        oXmlout.SetValue("message", "");
                        oXmlout.SetValue("status", iretremoto.ToString());
                        oXmlout.SetValue("trackid", oXmlParamOut.Trackid);
                        oXmlout.SetValue("result", oXmlParamOut.Result.ToString());
                        oXmlout.SetValue("score", oXmlParamOut.Score.ToString());
                        oXmlout.SetValue("threshold", oXmlParamOut.Threshold.ToString());
                        oXmlout.SetValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
                        oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(oXmlParamOut.PersonalData));
                    } else
                    {
                        msg = "Bio.Core.Matcher.ConnectorBioPortal4 Error en WS Remoto [" + iretremoto.ToString() + "]";
                        LOG.Fatal(msg);
                        oXmlout.SetValue("message", msg);
                        oXmlout.SetValue("status", iretremoto.ToString());
                        xmloutput = DynamicData.SerializeToXml(oXmlout);
                        return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                    }
                }

                xmloutput = DynamicData.SerializeToXml(oXmlout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.ConnectorBioPortal4.Verify Error [" + ex.Message + "]";
                LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.Verify Error", ex);
                oXmlout.SetValue("message", msg);
                xmloutput = DynamicData.SerializeToXml(oXmlout);
                return Errors.IERR_UNKNOWN;
            }

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */

            string msg;
            string xmloutbp;
            int iretremoto;

            DynamicData oXmlout = new DynamicData();
            oXmlout.AddValue("message", "");
            oXmlout.AddValue("trackid", "");
            oXmlout.AddValue("status", "");
            oXmlout.AddValue("result", "");
            oXmlout.AddValue("score", "");
            oXmlout.AddValue("timestamp", "");
            oXmlout.AddValue("personaldata", "");

            try
            {
                if (!this._isConfigured)
                {
                    msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error = Connector No Configurado!";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
                }

                using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
                {
                    ws.Timeout = this._timeout;
                    ws.Url = this._urlWS;

                    iretremoto = ws.Identify(xmlinput, out xmloutbp);
                    if (iretremoto == Errors.IERR_OK)
                    {
                        XmlParamOut oXmlParamOut = Utils.XmlUtils.DeserializeObject<XmlParamOut>(xmloutbp);
                        oXmlout.SetValue("message", "");
                        oXmlout.SetValue("status", iretremoto.ToString());
                        oXmlout.SetValue("trackid", oXmlParamOut.Trackid);
                        oXmlout.SetValue("result", oXmlParamOut.Result.ToString());
                        oXmlout.SetValue("score", oXmlParamOut.Score.ToString());
                        oXmlout.SetValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
                        oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(oXmlParamOut.PersonalData));
                    }
                    else
                    {
                        msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error en WS Remoto [" + iretremoto.ToString() + "]";
                        LOG.Fatal(msg);
                        oXmlout.SetValue("message", msg);
                        oXmlout.SetValue("status", iretremoto.ToString());
                        xmloutput = DynamicData.SerializeToXml(oXmlout);
                        return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                    }
                }

                xmloutput = DynamicData.SerializeToXml(oXmlout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error [" + ex.Message + "]";
                LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.Identity Error", ex);
                oXmlout.SetValue("message", msg);
                xmloutput = DynamicData.SerializeToXml(oXmlout);
                return Errors.IERR_UNKNOWN;
            }

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de Recuperación. Ingresa información recuperar, 
        /// y se realiza la operacion.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Get(string xmlinput, out string xmloutput)
        {
            /*
                Trackid" column="trackid" type="string" />
                Status" column="status" type="int" />
                Result" column="result" type="int" />
                Score" column="score" type="double" />
                Timestamp" column="timestamp" type="string" />
              
                Message = String 
             */

            string msg;
            string xmloutbp;
            int iretremoto;

            DynamicData oXmlout = new DynamicData();
            oXmlout.AddValue("message", "");
            oXmlout.AddValue("trackid", "");
            oXmlout.AddValue("status", "0");
            oXmlout.AddValue("result", "0");
            oXmlout.AddValue("score", "0");
            oXmlout.AddValue("timestamp", "");
            oXmlout.AddValue("personaldata", "");

            try
            {
                if (!this._isConfigured)
                {
                    msg = "Bio.Core.Matcher.ConnectorBioPortal4.Get Error = Connector No Configurado!";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
                }

                using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
                {
                    ws.Timeout = this._timeout;
                    ws.Url = this._urlWS;

                    iretremoto = ws.Get(xmlinput, out xmloutbp);
                    if (iretremoto == Errors.IERR_OK)
                    {
                        XmlParamOut oXmlParamOut = Utils.XmlUtils.DeserializeObject<XmlParamOut>(xmloutbp);
                        oXmlout.SetValue("message", "");
                        oXmlout.SetValue("status", iretremoto.ToString());
                        oXmlout.SetValue("trackid", oXmlParamOut.Trackid);
                        //oXmlout.SetValue("result", oXmlParamOut.Result.ToString());
                        //oXmlout.SetValue("score", oXmlParamOut.Score.ToString());
                        oXmlout.SetValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
                        oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(oXmlParamOut.PersonalData));
                    }
                    else
                    {
                        msg = "Bio.Core.Matcher.ConnectorBioPortal4.Get Error en WS Remoto [" + iretremoto.ToString() + "]";
                        LOG.Fatal(msg);
                        oXmlout.SetValue("message", msg);
                        oXmlout.SetValue("status", iretremoto.ToString());
                        xmloutput = DynamicData.SerializeToXml(oXmlout);
                        return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                    }
                }

                xmloutput = DynamicData.SerializeToXml(oXmlout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.ConnectorBioPortal4.Get Error [" + ex.Message + "]";
                LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.Get Error", ex);
                oXmlout.SetValue("message", msg);
                xmloutput = DynamicData.SerializeToXml(oXmlout);
                return Errors.IERR_UNKNOWN;
            }

            return Errors.IERR_OK;
        }

#endregion Implementation of IConnector

        public void Dispose()
        {
            
        }
    }
}
