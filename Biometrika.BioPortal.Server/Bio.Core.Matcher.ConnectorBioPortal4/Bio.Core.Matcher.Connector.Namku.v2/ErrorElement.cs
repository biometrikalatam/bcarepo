﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bio.Core.Matcher.Connector.Namku
{
    public class ErrorElement
    {
        [JsonProperty("domain")]
        public string Domain { get; set; } 

        [JsonProperty("reason")]
        public string Reason { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
