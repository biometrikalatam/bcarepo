﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Bio.Core.TokenCI050101.Gen.Utils
{
    internal class Utils
    {
        internal static string GeneraToken(string Typeid, string Valueid, string SerialSensor, short _finger, string _wsq, string templateToken,
                                           string _timestamp, string _ip, short _tokencontant, short _minutiaetype, short _device, short _afactor, 
                                           short _operationtype, ref short errToken)
        {
            string _tokenret = null;
            errToken = 0;
            try
            {
                string _plaintoken = Typeid + "|" +
                                     Valueid + "|" +
                                     SerialSensor + "|" +
                                     _finger + "|" +
                                     _wsq + "|" +
                                     templateToken + "|" +
                                     _timestamp + "|" +
                                     _ip + "|" +
                                     _tokencontant + "|" +
                                     _minutiaetype + "|0|" +
                                     _device + "|" +
                                     _afactor + "|" +
                                     _operationtype;

                //byte[] byplaintoken = UTF8Encoding.UTF8.GetBytes(_plaintoken);
                //byte[] byencryptedtoken = RSAEncrypt(byplaintoken, false);

                //Encrypt token Symetric 3DES generando key
                //Encripto clave simetrica pero con RSA (Asymetric) y concateno
                _tokenret = "CI050101" + Crypto.EncryptWithSymmetricAid(Crypto.PUBLIC_KEY, _plaintoken);//Convert.ToBase64String(byencryptedtoken);
            }
            catch (Exception ex)
            {
                errToken = -1;
                // "UPBlient.Utils.GeneraToken Error [" + ex.Message + "]";
                _tokenret = null;
            }
            return _tokenret;
        }

        static public byte[] RSAEncrypt(byte[] DataToEncrypt, bool DoOAEPPadding) //RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            try
            {
                byte[] encryptedData;
                //Create a new instance of RSACryptoServiceProvider. 
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {

                    //Import the RSA Key information. This only needs 
                    //toinclude the public key information.
                    //                    RSA.ImportParameters(RSAKeyInfo);
                    RSA.FromXmlString("<RSAKeyValue><Modulus>18aLaldVbtjt6eLyKLkOJOVctaPyi4rPRTLBU9Sxj5TF0RSkgiJTcScWVeUNxAIWWLoxHPO51itjdJtqhi/uS44fC+9S3Iao6Wq9kJvWwp4H/C1a8OE0ubT6+2IqV+bw12jjHCDEUiMFUlwJeZ+MWTHb9ZbSVn4TbEvNinwmaDM=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
                    //Encrypt the passed byte array and specify OAEP padding.   
                    //OAEP padding is only available on Microsoft Windows XP or 
                    //later.  
                    encryptedData = RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
                }
                return encryptedData;
            }
            //Catch and display a CryptographicException   
            //to the console. 
            catch (CryptographicException e)
            {
                //Console.WriteLine(e.Message);

                return null;
            }
        }
    }
}
