﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using log4net;

namespace Bio.Core.TokenCI050101.Gen
{
    public class TokenCI050101Generator
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(TokenCI050101Generator));


        public int GenerateTokenCI(string _typeid, string _valueid, string _serialSensor, short _finger, string _wsq, string _pdf417,
                                   short _device, out string _token)
        {
            //string _timestamp, string _ip, short _tokencontant, short _minutiaetype, short _afactor, short _operationtype
            _token = null;
            int _ret = 0;
            try
            {

                /*
                    string Typeid, string Valueid, string SerialSensor, short _finger, string _wsq, string templateToken,
                    string _timestamp, string _ip, short _tokencontant, short _minutiaetype, short _device, short _afactor, short _operationtype, 
                */
                string _ip = GetIP();
                short _ierr = 0;
                _token = Utils.Utils.GeneraToken(_typeid, _valueid, _serialSensor, _finger, _wsq, _pdf417, DateTime.Now.ToString("ddMMyyyyHHmmss"),
                                                 _ip, 0, 21, _device, 2, 1, ref _ierr);
                _ret = _ierr;
            }
            catch (Exception ex)
            {
                _ret = -1;
                LOG.Error("TokenCI050101Generator.GenerateTokenCI Error Ex = " + ex.Message);
            }
            return _ret;
        }


#region Utils

        public string GetMACAddress()
        {
            String sMacAddress = "";
            bool isFirst = true;
            try
            {
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface adapter in nics)
                {
                    //if (sMacAddress == String.Empty)// only return MAC Address from first card
                    //{
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    if (isFirst)
                    {
                        sMacAddress = FormatMAC(adapter.GetPhysicalAddress().ToString());
                        isFirst = false;
                    }
                    else
                    {
                        sMacAddress = sMacAddress + "~" + FormatMAC(adapter.GetPhysicalAddress().ToString());
                    }
                    //}
                }
            }
            catch (Exception ex)
            {
                LOG.Error("TokenCI050101Generator ERROR - GetMACAddress Error - " + ex.Message);
            }
            return sMacAddress;
        }

        private string FormatMAC(string smac)
        {
            String sMacAddress = "";
            try
            {
                if (!String.IsNullOrEmpty(smac))
                {
                    sMacAddress = smac.Substring(0, 2) + "-" +
                                  smac.Substring(2, 2) + "-" +
                                  smac.Substring(4, 2) + "-" +
                                  smac.Substring(6, 2);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("TokenCI050101Generator ERROR - FormatMAC Error - " + ex.Message);
            }
            return sMacAddress;
        }

        private string GetIP()
        {
            String sIP = "";
            bool isFirst = true;
            try
            {
                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
                foreach (IPAddress ipAddress in localIPs)
                {
                    if (ipAddress.ToString().Length <= 15)
                    {
                        if (isFirst)
                        {
                            sIP = ipAddress.ToString();
                            isFirst = false;
                        }
                        else
                        {
                            sIP = sIP + "~" + ipAddress.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("TokenCI050101Generator ERROR - GetMACAddress Error - " + ex.Message);
            }
            return sIP;
        }

#endregion Utils

    }
}
