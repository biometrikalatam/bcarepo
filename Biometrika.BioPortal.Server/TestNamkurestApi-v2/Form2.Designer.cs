﻿namespace TestNamkurestApi
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.btnEnviar = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtTH = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtValueid = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTypeid = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.txtFoto = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btnEndCapture = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.chkF2 = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.chkF1 = new System.Windows.Forms.CheckBox();
            this.btnCaptura2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.imageCam = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnCaptura1 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.grpConfig = new System.Windows.Forms.GroupBox();
            this.txtFNro = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.txtURLWebApi = new System.Windows.Forms.TextBox();
            this.chkWebApiOn = new System.Windows.Forms.CheckBox();
            this.txtCompanyId = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.picFirmaRes = new System.Windows.Forms.PictureBox();
            this.picFotoRes = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEraseResult = new System.Windows.Forms.Button();
            this.labScore = new System.Windows.Forms.Label();
            this.rtbResultado = new System.Windows.Forms.RichTextBox();
            this.labResVerify = new System.Windows.Forms.Label();
            this.picQuestion = new System.Windows.Forms.PictureBox();
            this.picNOOK = new System.Windows.Forms.PictureBox();
            this.picOK = new System.Windows.Forms.PictureBox();
            this.gbCI = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtAppId = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.rbLiviana = new System.Windows.Forms.RadioButton();
            this.rbCompleta = new System.Windows.Forms.RadioButton();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.button15 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.txtQIteraciones = new System.Windows.Forms.TextBox();
            this.txtURLCI = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.btnCIBorrar = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.labChecking = new System.Windows.Forms.Label();
            this.rtbCI3Result = new System.Windows.Forms.RichTextBox();
            this.rtbCI1Result = new System.Windows.Forms.RichTextBox();
            this.button10 = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTokenQRCI = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.picTokenQRCI = new System.Windows.Forms.PictureBox();
            this.button8 = new System.Windows.Forms.Button();
            this.timerCheckToken = new System.Windows.Forms.Timer(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCompareFacePlus = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.grpConfig.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFirmaRes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoRes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQuestion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNOOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOK)).BeginInit();
            this.gbCI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTokenQRCI)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEnviar
            // 
            this.btnEnviar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnviar.Location = new System.Drawing.Point(188, 60);
            this.btnEnviar.Margin = new System.Windows.Forms.Padding(2);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(208, 28);
            this.btnEnviar.TabIndex = 0;
            this.btnEnviar.Text = "Enviar Caras Directo...";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(946, 24);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(246, 27);
            this.button3.TabIndex = 7;
            this.button3.Text = "Enviar Caras x BP Plugin";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(483, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(449, 20);
            this.textBox1.TabIndex = 8;
            this.textBox1.Text = "http://qabpservice.biometrikalatam.com/BioPortal.Server.Plugin.Process.asmx";
            // 
            // txtTH
            // 
            this.txtTH.Location = new System.Drawing.Point(90, 65);
            this.txtTH.Name = "txtTH";
            this.txtTH.Size = new System.Drawing.Size(73, 20);
            this.txtTH.TabIndex = 9;
            this.txtTH.Text = "0,7";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(263, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Value id";
            // 
            // txtValueid
            // 
            this.txtValueid.Location = new System.Drawing.Point(314, 28);
            this.txtValueid.Name = "txtValueid";
            this.txtValueid.Size = new System.Drawing.Size(94, 20);
            this.txtValueid.TabIndex = 13;
            this.txtValueid.Text = "21284415-2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(153, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Type Id";
            // 
            // txtTypeid
            // 
            this.txtTypeid.Location = new System.Drawing.Point(202, 28);
            this.txtTypeid.Name = "txtTypeid";
            this.txtTypeid.Size = new System.Drawing.Size(45, 20);
            this.txtTypeid.TabIndex = 11;
            this.txtTypeid.Text = "RUT";
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(945, 91);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(247, 28);
            this.button4.TabIndex = 15;
            this.button4.Text = "Enviar Cara a Verify contra BD";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtFoto
            // 
            this.txtFoto.Location = new System.Drawing.Point(999, 66);
            this.txtFoto.Name = "txtFoto";
            this.txtFoto.Size = new System.Drawing.Size(20, 20);
            this.txtFoto.TabIndex = 16;
            this.txtFoto.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(603, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 25);
            this.label2.TabIndex = 17;
            this.label2.Text = "1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(854, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 25);
            this.label5.TabIndex = 18;
            this.label5.Text = "2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(41, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Umbral";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(483, 65);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(449, 20);
            this.textBox2.TabIndex = 20;
            this.textBox2.Text = "http://qabpservice.biometrikalatam.com/BioPortal.Server.WS.asmx";
            // 
            // btnEndCapture
            // 
            this.btnEndCapture.Location = new System.Drawing.Point(1084, 771);
            this.btnEndCapture.Margin = new System.Windows.Forms.Padding(2);
            this.btnEndCapture.Name = "btnEndCapture";
            this.btnEndCapture.Size = new System.Drawing.Size(141, 28);
            this.btnEndCapture.TabIndex = 25;
            this.btnEndCapture.Text = "Finalizar";
            this.btnEndCapture.UseVisualStyleBackColor = true;
            this.btnEndCapture.Click += new System.EventHandler(this.btnEndCapture_Click);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(516, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(485, 54);
            this.label7.TabIndex = 151;
            this.label7.Text = "Demostración de verificación facial  contra  documentos con Notarización...";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.chkF2);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.chkF1);
            this.groupBox1.Controls.Add(this.btnCaptura2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.imageCam);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.btnCaptura1);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Location = new System.Drawing.Point(33, 110);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1219, 264);
            this.groupBox1.TabIndex = 152;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Zona de captura...";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(353, 154);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(109, 13);
            this.label16.TabIndex = 164;
            this.label16.Text = ">>>>>>>>>>>>>>>>>";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(353, 141);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 13);
            this.label15.TabIndex = 163;
            this.label15.Text = ">>>>>>>>>>>>>>>>>";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(353, 128);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(109, 13);
            this.label14.TabIndex = 162;
            this.label14.Text = ">>>>>>>>>>>>>>>>>";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(832, 56);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(25, 25);
            this.label12.TabIndex = 161;
            this.label12.Text = "2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(490, 56);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(25, 25);
            this.label13.TabIndex = 160;
            this.label13.Text = "1";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(134, 23);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(113, 21);
            this.button6.TabIndex = 159;
            this.button6.Text = "Stop Preview...";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Visible = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // chkF2
            // 
            this.chkF2.AutoSize = true;
            this.chkF2.Location = new System.Drawing.Point(1104, 25);
            this.chkF2.Name = "chkF2";
            this.chkF2.Size = new System.Drawing.Size(45, 17);
            this.chkF2.TabIndex = 158;
            this.chkF2.Text = "B64";
            this.chkF2.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(965, 22);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(132, 22);
            this.button2.TabIndex = 157;
            this.button2.Text = "Seleccionar Foto 2...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(826, 50);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(335, 201);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 156;
            this.pictureBox2.TabStop = false;
            // 
            // chkF1
            // 
            this.chkF1.AutoSize = true;
            this.chkF1.Location = new System.Drawing.Point(748, 26);
            this.chkF1.Name = "chkF1";
            this.chkF1.Size = new System.Drawing.Size(45, 17);
            this.chkF1.TabIndex = 155;
            this.chkF1.Text = "B64";
            this.chkF1.UseVisualStyleBackColor = true;
            // 
            // btnCaptura2
            // 
            this.btnCaptura2.Enabled = false;
            this.btnCaptura2.Location = new System.Drawing.Point(826, 22);
            this.btnCaptura2.Margin = new System.Windows.Forms.Padding(2);
            this.btnCaptura2.Name = "btnCaptura2";
            this.btnCaptura2.Size = new System.Drawing.Size(134, 22);
            this.btnCaptura2.TabIndex = 31;
            this.btnCaptura2.Text = "Capturar de Preview...";
            this.btnCaptura2.UseVisualStyleBackColor = true;
            this.btnCaptura2.Click += new System.EventHandler(this.btnCaptura2_Click_1);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(614, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 22);
            this.button1.TabIndex = 154;
            this.button1.Text = "Seleccionar Foto 1...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // imageCam
            // 
            this.imageCam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageCam.Location = new System.Drawing.Point(17, 50);
            this.imageCam.Margin = new System.Windows.Forms.Padding(2);
            this.imageCam.Name = "imageCam";
            this.imageCam.Size = new System.Drawing.Size(317, 201);
            this.imageCam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageCam.TabIndex = 30;
            this.imageCam.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(483, 49);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(325, 202);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 153;
            this.pictureBox1.TabStop = false;
            // 
            // btnCaptura1
            // 
            this.btnCaptura1.Enabled = false;
            this.btnCaptura1.Location = new System.Drawing.Point(483, 22);
            this.btnCaptura1.Margin = new System.Windows.Forms.Padding(2);
            this.btnCaptura1.Name = "btnCaptura1";
            this.btnCaptura1.Size = new System.Drawing.Size(126, 22);
            this.btnCaptura1.TabIndex = 29;
            this.btnCaptura1.Text = "Capturar de Preview...";
            this.btnCaptura1.UseVisualStyleBackColor = true;
            this.btnCaptura1.Click += new System.EventHandler(this.btnCaptura1_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(17, 23);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(113, 21);
            this.button5.TabIndex = 28;
            this.button5.Text = "Iniciar Preview...";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = global::TestNamkurestApi.Properties.Resources.Biometrika_Bajada_400x140;
            this.pictureBox3.Location = new System.Drawing.Point(33, 12);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(314, 92);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 150;
            this.pictureBox3.TabStop = false;
            // 
            // grpConfig
            // 
            this.grpConfig.Controls.Add(this.txtFNro);
            this.grpConfig.Controls.Add(this.label19);
            this.grpConfig.Controls.Add(this.button7);
            this.grpConfig.Controls.Add(this.label18);
            this.grpConfig.Controls.Add(this.txtURLWebApi);
            this.grpConfig.Controls.Add(this.chkWebApiOn);
            this.grpConfig.Controls.Add(this.txtFoto);
            this.grpConfig.Controls.Add(this.txtCompanyId);
            this.grpConfig.Controls.Add(this.label11);
            this.grpConfig.Controls.Add(this.label10);
            this.grpConfig.Controls.Add(this.label9);
            this.grpConfig.Controls.Add(this.label8);
            this.grpConfig.Controls.Add(this.btnEnviar);
            this.grpConfig.Controls.Add(this.txtTH);
            this.grpConfig.Controls.Add(this.button3);
            this.grpConfig.Controls.Add(this.label6);
            this.grpConfig.Controls.Add(this.txtValueid);
            this.grpConfig.Controls.Add(this.txtTypeid);
            this.grpConfig.Controls.Add(this.label3);
            this.grpConfig.Controls.Add(this.label4);
            this.grpConfig.Controls.Add(this.textBox1);
            this.grpConfig.Controls.Add(this.button4);
            this.grpConfig.Controls.Add(this.textBox2);
            this.grpConfig.Location = new System.Drawing.Point(33, 381);
            this.grpConfig.Name = "grpConfig";
            this.grpConfig.Size = new System.Drawing.Size(1219, 131);
            this.grpConfig.TabIndex = 153;
            this.grpConfig.TabStop = false;
            this.grpConfig.Text = "Zona de Configuración...";
            // 
            // txtFNro
            // 
            this.txtFNro.Location = new System.Drawing.Point(167, 103);
            this.txtFNro.Name = "txtFNro";
            this.txtFNro.Size = new System.Drawing.Size(20, 20);
            this.txtFNro.TabIndex = 30;
            this.txtFNro.Text = "1";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(118, 106);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 13);
            this.label19.TabIndex = 31;
            this.label19.Text = "Foto Nro:";
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(190, 99);
            this.button7.Margin = new System.Windows.Forms.Padding(2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(206, 27);
            this.button7.TabIndex = 29;
            this.button7.Text = "MRZ Image via BP";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(404, 99);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 28;
            this.label18.Text = "URL WebApi:";
            // 
            // txtURLWebApi
            // 
            this.txtURLWebApi.Location = new System.Drawing.Point(483, 96);
            this.txtURLWebApi.Name = "txtURLWebApi";
            this.txtURLWebApi.Size = new System.Drawing.Size(449, 20);
            this.txtURLWebApi.TabIndex = 27;
            this.txtURLWebApi.Text = "http://qabpwebapiservice.biometrikalatam.com/";
            // 
            // chkWebApiOn
            // 
            this.chkWebApiOn.AutoSize = true;
            this.chkWebApiOn.Location = new System.Drawing.Point(1067, 64);
            this.chkWebApiOn.Name = "chkWebApiOn";
            this.chkWebApiOn.Size = new System.Drawing.Size(125, 17);
            this.chkWebApiOn.TabIndex = 26;
            this.chkWebApiOn.Text = "Consulta via WebApi";
            this.chkWebApiOn.UseVisualStyleBackColor = true;
            // 
            // txtCompanyId
            // 
            this.txtCompanyId.Location = new System.Drawing.Point(90, 28);
            this.txtCompanyId.Name = "txtCompanyId";
            this.txtCompanyId.Size = new System.Drawing.Size(45, 20);
            this.txtCompanyId.TabIndex = 24;
            this.txtCompanyId.Text = "7";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Company Id";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(950, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Foto Nro:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(424, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "URL WS:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(424, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "URL WS:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.picFirmaRes);
            this.groupBox3.Controls.Add(this.picFotoRes);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.btnEraseResult);
            this.groupBox3.Controls.Add(this.labScore);
            this.groupBox3.Controls.Add(this.rtbResultado);
            this.groupBox3.Controls.Add(this.labResVerify);
            this.groupBox3.Controls.Add(this.picQuestion);
            this.groupBox3.Controls.Add(this.picNOOK);
            this.groupBox3.Controls.Add(this.picOK);
            this.groupBox3.Location = new System.Drawing.Point(33, 526);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1219, 240);
            this.groupBox3.TabIndex = 154;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Zona de Resultados...";
            // 
            // picFirmaRes
            // 
            this.picFirmaRes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFirmaRes.Location = new System.Drawing.Point(343, 18);
            this.picFirmaRes.Margin = new System.Windows.Forms.Padding(2);
            this.picFirmaRes.Name = "picFirmaRes";
            this.picFirmaRes.Size = new System.Drawing.Size(81, 56);
            this.picFirmaRes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFirmaRes.TabIndex = 157;
            this.picFirmaRes.TabStop = false;
            // 
            // picFotoRes
            // 
            this.picFotoRes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFotoRes.Location = new System.Drawing.Point(266, 16);
            this.picFotoRes.Margin = new System.Windows.Forms.Padding(2);
            this.picFotoRes.Name = "picFotoRes";
            this.picFotoRes.Size = new System.Drawing.Size(53, 56);
            this.picFotoRes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFotoRes.TabIndex = 156;
            this.picFotoRes.TabStop = false;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.DimGray;
            this.label17.Location = new System.Drawing.Point(23, 159);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(454, 54);
            this.label17.TabIndex = 155;
            this.label17.Text = "Valor de Score mas cercano a 0 (cero) indica positivo mas seguro";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(505, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 16);
            this.label1.TabIndex = 148;
            this.label1.Text = "Detalles";
            // 
            // btnEraseResult
            // 
            this.btnEraseResult.Location = new System.Drawing.Point(122, 0);
            this.btnEraseResult.Margin = new System.Windows.Forms.Padding(2);
            this.btnEraseResult.Name = "btnEraseResult";
            this.btnEraseResult.Size = new System.Drawing.Size(112, 26);
            this.btnEraseResult.TabIndex = 147;
            this.btnEraseResult.Text = "&Borrar Resultados";
            this.btnEraseResult.UseVisualStyleBackColor = true;
            this.btnEraseResult.Click += new System.EventHandler(this.btnEraseResult_Click);
            // 
            // labScore
            // 
            this.labScore.AutoSize = true;
            this.labScore.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labScore.ForeColor = System.Drawing.Color.DimGray;
            this.labScore.Location = new System.Drawing.Point(141, 110);
            this.labScore.Name = "labScore";
            this.labScore.Size = new System.Drawing.Size(124, 18);
            this.labScore.TabIndex = 146;
            this.labScore.Text = "Score = 0 / 0";
            this.labScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labScore.Visible = false;
            // 
            // rtbResultado
            // 
            this.rtbResultado.BackColor = System.Drawing.Color.PapayaWhip;
            this.rtbResultado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbResultado.Location = new System.Drawing.Point(495, 35);
            this.rtbResultado.Margin = new System.Windows.Forms.Padding(2);
            this.rtbResultado.Name = "rtbResultado";
            this.rtbResultado.Size = new System.Drawing.Size(697, 176);
            this.rtbResultado.TabIndex = 145;
            this.rtbResultado.Text = "";
            // 
            // labResVerify
            // 
            this.labResVerify.AutoSize = true;
            this.labResVerify.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResVerify.ForeColor = System.Drawing.Color.Blue;
            this.labResVerify.Location = new System.Drawing.Point(139, 85);
            this.labResVerify.Name = "labResVerify";
            this.labResVerify.Size = new System.Drawing.Size(285, 25);
            this.labResVerify.TabIndex = 144;
            this.labResVerify.Text = "Verifique IDENTIDAD...";
            this.labResVerify.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picQuestion
            // 
            this.picQuestion.Image = global::TestNamkurestApi.Properties.Resources.Question_Shield;
            this.picQuestion.Location = new System.Drawing.Point(42, 57);
            this.picQuestion.Name = "picQuestion";
            this.picQuestion.Size = new System.Drawing.Size(93, 88);
            this.picQuestion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picQuestion.TabIndex = 141;
            this.picQuestion.TabStop = false;
            // 
            // picNOOK
            // 
            this.picNOOK.Image = global::TestNamkurestApi.Properties.Resources.Error_Shield;
            this.picNOOK.Location = new System.Drawing.Point(42, 57);
            this.picNOOK.Name = "picNOOK";
            this.picNOOK.Size = new System.Drawing.Size(93, 88);
            this.picNOOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picNOOK.TabIndex = 143;
            this.picNOOK.TabStop = false;
            this.picNOOK.Visible = false;
            // 
            // picOK
            // 
            this.picOK.Image = global::TestNamkurestApi.Properties.Resources.Good_Shield;
            this.picOK.Location = new System.Drawing.Point(42, 57);
            this.picOK.Name = "picOK";
            this.picOK.Size = new System.Drawing.Size(93, 88);
            this.picOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picOK.TabIndex = 142;
            this.picOK.TabStop = false;
            this.picOK.Visible = false;
            // 
            // gbCI
            // 
            this.gbCI.Controls.Add(this.button17);
            this.gbCI.Controls.Add(this.button16);
            this.gbCI.Controls.Add(this.label27);
            this.gbCI.Controls.Add(this.txtAppId);
            this.gbCI.Controls.Add(this.label26);
            this.gbCI.Controls.Add(this.rbLiviana);
            this.gbCI.Controls.Add(this.rbCompleta);
            this.gbCI.Controls.Add(this.label25);
            this.gbCI.Controls.Add(this.label24);
            this.gbCI.Controls.Add(this.button15);
            this.gbCI.Controls.Add(this.label23);
            this.gbCI.Controls.Add(this.button14);
            this.gbCI.Controls.Add(this.txtQIteraciones);
            this.gbCI.Controls.Add(this.txtURLCI);
            this.gbCI.Controls.Add(this.label22);
            this.gbCI.Controls.Add(this.button13);
            this.gbCI.Controls.Add(this.button12);
            this.gbCI.Controls.Add(this.btnCIBorrar);
            this.gbCI.Controls.Add(this.button11);
            this.gbCI.Controls.Add(this.label21);
            this.gbCI.Controls.Add(this.txtMail);
            this.gbCI.Controls.Add(this.labChecking);
            this.gbCI.Controls.Add(this.rtbCI3Result);
            this.gbCI.Controls.Add(this.rtbCI1Result);
            this.gbCI.Controls.Add(this.button10);
            this.gbCI.Controls.Add(this.label20);
            this.gbCI.Controls.Add(this.txtTokenQRCI);
            this.gbCI.Controls.Add(this.button9);
            this.gbCI.Controls.Add(this.picTokenQRCI);
            this.gbCI.Controls.Add(this.button8);
            this.gbCI.Location = new System.Drawing.Point(1259, 110);
            this.gbCI.Name = "gbCI";
            this.gbCI.Size = new System.Drawing.Size(325, 688);
            this.gbCI.TabIndex = 155;
            this.gbCI.TabStop = false;
            this.gbCI.Text = "CI via NV...";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(254, 71);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(38, 13);
            this.label27.TabIndex = 183;
            this.label27.Text = "App Id";
            // 
            // txtAppId
            // 
            this.txtAppId.Location = new System.Drawing.Point(257, 87);
            this.txtAppId.Name = "txtAppId";
            this.txtAppId.Size = new System.Drawing.Size(60, 20);
            this.txtAppId.TabIndex = 182;
            this.txtAppId.Text = "10";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(20, 236);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(88, 13);
            this.label26.TabIndex = 181;
            this.label26.Text = "Tipo Respuesta :";
            // 
            // rbLiviana
            // 
            this.rbLiviana.AutoSize = true;
            this.rbLiviana.Location = new System.Drawing.Point(186, 234);
            this.rbLiviana.Name = "rbLiviana";
            this.rbLiviana.Size = new System.Drawing.Size(59, 17);
            this.rbLiviana.TabIndex = 180;
            this.rbLiviana.Text = "Liviana";
            this.rbLiviana.UseVisualStyleBackColor = true;
            this.rbLiviana.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // rbCompleta
            // 
            this.rbCompleta.AutoSize = true;
            this.rbCompleta.Checked = true;
            this.rbCompleta.Location = new System.Drawing.Point(111, 234);
            this.rbCompleta.Name = "rbCompleta";
            this.rbCompleta.Size = new System.Drawing.Size(69, 17);
            this.rbCompleta.TabIndex = 179;
            this.rbCompleta.TabStop = true;
            this.rbCompleta.Text = "Completa";
            this.rbCompleta.UseVisualStyleBackColor = true;
            this.rbCompleta.CheckedChanged += new System.EventHandler(this.rbCompleta_CheckedChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(278, 45);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 13);
            this.label25.TabIndex = 178;
            this.label25.Text = "Prod";
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(250, 45);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(22, 13);
            this.label24.TabIndex = 177;
            this.label24.Text = "QA";
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(238, 659);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(74, 23);
            this.button15.TabIndex = 176;
            this.button15.Text = "Stop";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(19, 646);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(132, 13);
            this.label23.TabIndex = 175;
            this.label23.Text = "Cantidad Ciclos Completos";
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(137, 659);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(95, 23);
            this.button14.TabIndex = 174;
            this.button14.Text = "Ejecutar Estres";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // txtQIteraciones
            // 
            this.txtQIteraciones.Location = new System.Drawing.Point(22, 662);
            this.txtQIteraciones.Name = "txtQIteraciones";
            this.txtQIteraciones.Size = new System.Drawing.Size(109, 20);
            this.txtQIteraciones.TabIndex = 173;
            this.txtQIteraciones.Text = "5";
            // 
            // txtURLCI
            // 
            this.txtURLCI.Location = new System.Drawing.Point(65, 22);
            this.txtURLCI.Name = "txtURLCI";
            this.txtURLCI.Size = new System.Drawing.Size(248, 20);
            this.txtURLCI.TabIndex = 172;
            this.txtURLCI.Text = "http://service.enotario.cl/WSCI.asmx";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 25);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 13);
            this.label22.TabIndex = 171;
            this.label22.Text = "URL WS:";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(247, 184);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(72, 23);
            this.button13.TabIndex = 170;
            this.button13.Text = "Check CI";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(153, 259);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(138, 23);
            this.button12.TabIndex = 169;
            this.button12.Text = "Verify Cedula + OCR (API)";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // btnCIBorrar
            // 
            this.btnCIBorrar.Location = new System.Drawing.Point(224, 0);
            this.btnCIBorrar.Name = "btnCIBorrar";
            this.btnCIBorrar.Size = new System.Drawing.Size(89, 23);
            this.btnCIBorrar.TabIndex = 168;
            this.btnCIBorrar.Text = "Borrar";
            this.btnCIBorrar.UseVisualStyleBackColor = true;
            this.btnCIBorrar.Click += new System.EventHandler(this.btnCIBorrar_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(163, 418);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(150, 23);
            this.button11.TabIndex = 167;
            this.button11.Text = "CI via Cedula + Selfie (API)";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(28, 50);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(26, 13);
            this.label21.TabIndex = 166;
            this.label21.Text = "Mail";
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(60, 45);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(151, 20);
            this.txtMail.TabIndex = 165;
            this.txtMail.Text = "gsuhit@gmail.com";
            // 
            // labChecking
            // 
            this.labChecking.AutoSize = true;
            this.labChecking.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labChecking.Location = new System.Drawing.Point(28, 210);
            this.labChecking.Name = "labChecking";
            this.labChecking.Size = new System.Drawing.Size(65, 13);
            this.labChecking.TabIndex = 164;
            this.labChecking.Text = "Cheking...";
            this.labChecking.Visible = false;
            // 
            // rtbCI3Result
            // 
            this.rtbCI3Result.BackColor = System.Drawing.Color.PapayaWhip;
            this.rtbCI3Result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbCI3Result.Location = new System.Drawing.Point(6, 473);
            this.rtbCI3Result.Margin = new System.Windows.Forms.Padding(2);
            this.rtbCI3Result.Name = "rtbCI3Result";
            this.rtbCI3Result.Size = new System.Drawing.Size(307, 166);
            this.rtbCI3Result.TabIndex = 163;
            this.rtbCI3Result.Text = "";
            // 
            // rtbCI1Result
            // 
            this.rtbCI1Result.BackColor = System.Drawing.Color.PapayaWhip;
            this.rtbCI1Result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbCI1Result.Location = new System.Drawing.Point(9, 287);
            this.rtbCI1Result.Margin = new System.Windows.Forms.Padding(2);
            this.rtbCI1Result.Name = "rtbCI1Result";
            this.rtbCI1Result.Size = new System.Drawing.Size(301, 121);
            this.rtbCI1Result.TabIndex = 162;
            this.rtbCI1Result.Text = "";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(13, 259);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(134, 23);
            this.button10.TabIndex = 161;
            this.button10.Text = "Verify Cedula + OCR";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(28, 171);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(73, 13);
            this.label20.TabIndex = 160;
            this.label20.Text = "Token QR CI ";
            // 
            // txtTokenQRCI
            // 
            this.txtTokenQRCI.Location = new System.Drawing.Point(22, 187);
            this.txtTokenQRCI.Name = "txtTokenQRCI";
            this.txtTokenQRCI.Size = new System.Drawing.Size(219, 20);
            this.txtTokenQRCI.TabIndex = 159;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(6, 419);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(151, 23);
            this.button9.TabIndex = 158;
            this.button9.Text = "CI via Cedula + Selfie (WS)";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // picTokenQRCI
            // 
            this.picTokenQRCI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picTokenQRCI.Location = new System.Drawing.Point(116, 69);
            this.picTokenQRCI.Margin = new System.Windows.Forms.Padding(2);
            this.picTokenQRCI.Name = "picTokenQRCI";
            this.picTokenQRCI.Size = new System.Drawing.Size(125, 113);
            this.picTokenQRCI.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picTokenQRCI.TabIndex = 157;
            this.picTokenQRCI.TabStop = false;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(22, 71);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(89, 23);
            this.button8.TabIndex = 0;
            this.button8.Text = "Get Token CI";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // timerCheckToken
            // 
            this.timerCheckToken.Enabled = true;
            this.timerCheckToken.Interval = 1000;
            this.timerCheckToken.Tick += new System.EventHandler(this.timerCheckToken_Tick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnCompareFacePlus);
            this.groupBox2.Location = new System.Drawing.Point(1265, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(307, 91);
            this.groupBox2.TabIndex = 156;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Face++ Compare";
            // 
            // btnCompareFacePlus
            // 
            this.btnCompareFacePlus.Location = new System.Drawing.Point(180, 21);
            this.btnCompareFacePlus.Name = "btnCompareFacePlus";
            this.btnCompareFacePlus.Size = new System.Drawing.Size(89, 23);
            this.btnCompareFacePlus.TabIndex = 1;
            this.btnCompareFacePlus.Text = "Compare";
            this.btnCompareFacePlus.UseVisualStyleBackColor = true;
            this.btnCompareFacePlus.Click += new System.EventHandler(this.btnCompareFacePlus_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(6, 445);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(151, 23);
            this.button16.TabIndex = 184;
            this.button16.Text = "CI via Selfie vs BD (WS)";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(166, 445);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(151, 23);
            this.button17.TabIndex = 185;
            this.button17.Text = "CI via Selfie vs BD (API)";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1596, 810);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gbCI);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.grpConfig);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.btnEndCapture);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bimetrika Facial Demo v2.5 - (w/TokenLogin)...";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.grpConfig.ResumeLayout(false);
            this.grpConfig.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFirmaRes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoRes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQuestion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNOOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOK)).EndInit();
            this.gbCI.ResumeLayout(false);
            this.gbCI.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTokenQRCI)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtTH;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtValueid;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTypeid;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtFoto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btnEndCapture;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.CheckBox chkF2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.CheckBox chkF1;
        private System.Windows.Forms.Button btnCaptura2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox imageCam;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnCaptura1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox grpConfig;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCompanyId;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEraseResult;
        private System.Windows.Forms.Label labScore;
        private System.Windows.Forms.RichTextBox rtbResultado;
        private System.Windows.Forms.Label labResVerify;
        private System.Windows.Forms.PictureBox picQuestion;
        private System.Windows.Forms.PictureBox picNOOK;
        private System.Windows.Forms.PictureBox picOK;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chkWebApiOn;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtURLWebApi;
        private System.Windows.Forms.TextBox txtFNro;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.PictureBox picFirmaRes;
        private System.Windows.Forms.PictureBox picFotoRes;
        private System.Windows.Forms.GroupBox gbCI;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.PictureBox picTokenQRCI;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtTokenQRCI;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.RichTextBox rtbCI1Result;
        private System.Windows.Forms.RichTextBox rtbCI3Result;
        private System.Windows.Forms.Label labChecking;
        private System.Windows.Forms.Timer timerCheckToken;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button btnCIBorrar;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCompareFacePlus;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.TextBox txtURLCI;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.TextBox txtQIteraciones;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.RadioButton rbLiviana;
        private System.Windows.Forms.RadioButton rbCompleta;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtAppId;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
    }
}