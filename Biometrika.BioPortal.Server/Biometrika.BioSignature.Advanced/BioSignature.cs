﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bio.Core.Api;

namespace Biometrika.BioSignature.Advanced
{
    internal class BioSignature
    {
        private string _trakid;
        private DateTime _datetime;
        private string _ddata;
        private string _source;
        private string _score;
        private string _sample;
        private int _sampletype;
        private string _barcode;
        private string _photografy;
    }
}
