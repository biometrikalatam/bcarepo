﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
//using System.Security.Cryptography.Xml;
using System.Xml;
using Bio.Core.pki.mentalis; //Biokey.pki.mentalis;
using log4net;
using System.Security.Cryptography.Xml;
using Bio.Core.Api.Constant;

namespace Biometrika.BioSignature.Advanced
{
    internal class XMLSignatureHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(XMLSignatureHelper));

        internal static int Sign(string xmltosign, out string xmlsigned, out string msgerr)
        {
            xmlsigned = null;
            msgerr = null;
            int ret = 0;

            try
            {
                AsymmetricAlgorithm key = BioSignatureFactory.KEY;
                    //Pkcs12.GetPrivateKeyFromPFX(Properties.Settings.Default.PFX, Properties.Settings.Default.PFXPSW);

                // Create a new XML document.
                XmlDocument xmlDoc = new XmlDocument();

                // Load an XML file into the XmlDocument object.
                xmlDoc.PreserveWhitespace = false;

                //Saco encabezado si trae
                xmltosign = xmltosign.Substring(xmltosign.IndexOf("<BioSignature"));

                xmlDoc.LoadXml(xmltosign);

                // Sign the XML document. 
                if (SignXml(xmlDoc, key))
                {
                    xmlsigned = xmlDoc.FirstChild.OuterXml;
                }
                else
                {
                    ret = -2;
                    msgerr = "Error firmando xml";
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Error", ex);
                msgerr = ex.Message;
                ret = -1;
            }

            return ret;

        }

        // Sign an XML file. 
        // This document cannot be verified unless the verifying 
        // code has the key with which it was signed.
        internal static bool SignXml(XmlDocument Doc, AsymmetricAlgorithm Key) //RSA Key)
        {
            try
            {
                // Check arguments.
                if (Doc == null) return false;
                //throw new ArgumentException("Doc NULL");
                if (Key == null) return false;
                //throw new ArgumentException("Key NULL");

                // Create a SignedXml object.
                SignedXml signedXml = new SignedXml(Doc);

                // Add the key to the SignedXml document.
                signedXml.SigningKey = Key;

                // Create a reference to be signed.
                Reference reference = new Reference();
                reference.Uri = "#IDREF.v1";

                // Add an enveloped transformation to the reference.
                XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
                reference.AddTransform(env);

                // Add the reference to the SignedXml object.
                signedXml.AddReference(reference);

                // Add an RSAKeyValue KeyInfo (optional; helps recipient find key to validate).
                KeyInfo keyInfo = new KeyInfo();
                keyInfo.AddClause(new RSAKeyValue((RSA)Key));
                
                // Load the X509 certificate.
                //X509Certificate MSCert = X509Certificate.CreateFromCertFile(Properties.Settings.Default.CER);

                // Load the certificate into a KeyInfoX509Data object
                // and add it to the KeyInfo object.
                keyInfo.AddClause(new KeyInfoX509Data(BioSignatureFactory.MSCERT));
                signedXml.KeyInfo = keyInfo;

                // Compute the signature.
                signedXml.ComputeSignature();

                // Get the XML representation of the signature and save
                // it to an XmlElement object.
                XmlElement xmlDigitalSignature = signedXml.GetXml();

                // Append the element to the XML document.
                Doc.DocumentElement.AppendChild(Doc.ImportNode(xmlDigitalSignature, true));

            }
            catch (Exception ex)
            {
                LOG.Error("Error XMLSignatureHelper.SignXml", ex);
                return false;
            }

            return true;
        }

        // Verify the signature of an XML file against an asymmetric 
        // algorithm and return the result.
        internal static int VerifyXml(string xml, out bool isverifyok, out string errdesc) //RSA Key)
        {
            isverifyok = false;
            errdesc = null;
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = false;
                xmlDoc.LoadXml(xml);

                // Check arguments.
                if (xmlDoc == null)
                {
                    errdesc = "Error XMLSignatureHelper.VerifyXml parseando xml";
                    LOG.Error("Error XMLSignatureHelper.VerifyXml Parseando xml xmlDoc==null");
                    return Errors.IERR_BAD_PARAMETER;
                }
                    
                // Create a new SignedXml object and pass it
                // the XML document class.
                SignedXml signedXml = new SignedXml(xmlDoc);

                // Find the "Signature" node and create a new
                // XmlNodeList object.
                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("Signature");

                // Throw an exception if no signature was found.
                if (nodeList.Count <= 0)
                {
                    errdesc = "Error XMLSignatureHelper.VerifyXml No existe Signature TAG";
                    LOG.Error("Error XMLSignatureHelper.VerifyXml No existe Signature TAG");
                    return Errors.IERR_VERIFY;
                    //throw new CryptographicException("Verification failed: No Signature was found in the document.");
                }

                // This example only supports one signature for
                // the entire XML document.  Throw an exception 
                // if more than one signature was found.
                if (nodeList.Count >= 2)
                {
                    errdesc = "Error XMLSignatureHelper.VerifyXml Existe mas de un Signature TAG";
                    LOG.Error("Error XMLSignatureHelper.VerifyXml Existe mas de un Signature TAG");
                    return Errors.IERR_VERIFY;
                    //throw new CryptographicException("Verification failed: More that one signature was found for the document.");
                }

                // Load the first <signature> node.  
                signedXml.LoadXml((XmlElement)nodeList[0]);

                // Check the signature and return the result.
                isverifyok = signedXml.CheckSignature();
            }
            catch (Exception ex)
            {
                LOG.Error("Error XMLSignatureHelper.SignXml", ex);
                return Errors.IERR_UNKNOWN;
            }
            return Errors.IERR_OK;
        }


        internal static void LoadKeys(string _pathPFX, string _PFXPSW, string _pathCER, out X509Certificate MSCert, out System.Security.Cryptography.AsymmetricAlgorithm key)
        {
            MSCert = null;
            key = null;
            try
            {
                LOG.Debug("Cargando X509Certificate desde >> " + _pathCER);
                MSCert = X509Certificate.CreateFromCertFile(_pathCER);
                LOG.Debug("X509Certificate >> " + ((MSCert == null)? "NO Cargado!!":"CARGADO!"));

                LOG.Debug("Cargando AsymmetricAlgorithm desde >> " + _pathPFX + " y Psw=" + _PFXPSW);
                key = Pkcs12.GetPrivateKeyFromPFX(_pathPFX, _PFXPSW);
                LOG.Debug("AsymmetricAlgorithm >> " + ((key == null)? "NO Cargado!!":"CARGADO!"));

            }
            catch (Exception ex)
            {
                LOG.Error("Error XMLSignatureHelper.LoadKeys", ex);
            }       
        }
    }
}
