﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using Bio.Core;
using Bio.Core.Api.Constant;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;

namespace Biometrika.BioSignature.Advanced
{
    public class BioSignatureFactory
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BioSignatureFactory));

        public const int SIGNATURE_TYPE_SIMPLE = 1;
        public const int SIGNATURE_TYPE_ADVANCED = 2;

        internal static X509Certificate MSCERT;
        internal static AsymmetricAlgorithm KEY; 
                    

        private string _pathPFX;
        private string _PFXPSW;
        private string _pathCER;
        private string _BarcodeType = "QR";  //QR o PDF417
        private string _URLQR = "http://BioPortal.Servcies.v5/VerifyASP.aspx";

        public string PathPFX
        {
            get { return _pathPFX; }
            set { _pathPFX = value; }
        }
        public string PFXPSW
        {
            get { return _PFXPSW; }
            set { _PFXPSW = value; }
        }
        public string PathCER
        {
            get { return _pathCER; }
            set { _pathCER = value; }
        }
        public string BarcodeType
        {
            get { return _BarcodeType; }
            set { _BarcodeType = value; }
        }
        public string URLQR
        {
            get { return _URLQR; }
            set { _URLQR = value; }
        }

        /*
         * _trakid;
        private DateTime _datetime;
        private DynamicData _ddata;
        private string _source;
        private string _score;
        private string _sample;
        private string _barcode;
        private string _photografy;
         */

        public void InitFactory() {}

        public int InitFactory(string pathPFX, string PFXPSW, string pathCER, string BarcodeType, string URLQR)
        {
            int ret = Errors.IERR_OK;
            try
            {
                _pathPFX = pathPFX;
                _PFXPSW = PFXPSW; 
                _pathCER = pathCER; 
                _BarcodeType = BarcodeType;
                _URLQR = URLQR;

                XMLSignatureHelper.LoadKeys(_pathPFX, _PFXPSW, _pathCER, out MSCERT, out KEY);
                if (MSCERT == null || KEY == null)
                {
                    LOG.Error("Biometrika.BioSignature.Advanced.InitiFactory Claves Nulas! No se configuro correctametne para generar BioSignature!");
                }
                else
                {
                    LOG.Debug("Biometrika.BioSignature.Advanced.InitiFactory - Claves OK - Out OK!");
                }

            }
            catch (Exception ex)
            {
                LOG.Error("Biometrika.BioSignature.Advanced.InitiFactory", ex);
                ret = Errors.IERR_UNKNOWN;
            }
            
            return ret;
        }
        
        public int GetSignature(int signaturetype, string id, string sample, int sampletype, string source, string photografy, string score,
                                      string ddata, out string biosignature, out string errdescription)
        {
            int ret = 0;
            biosignature = null;
            errdescription = null;
            string sxmlABS;
            try
            {
                if (String.IsNullOrEmpty(id) ||
                    String.IsNullOrEmpty(sample))
                {
                    ret = Errors.IERR_BAD_PARAMETER;
                    errdescription = "Error: Id o ImageSample nulos o vacios!";
                }
                else
                {
                    sxmlABS = ArmaXML(id, sample, sampletype, source, photografy, score, ddata);
                    if (signaturetype == SIGNATURE_TYPE_ADVANCED)
                    {
                        LOG.Debug("BIOSIGNATURE_FACTORY.GetSignature entrando a XMLSignatureHelper.Sign x ABS Advanced...");
                        ret = XMLSignatureHelper.Sign(sxmlABS, out biosignature, out errdescription);
                    }
                    else
                    {
                        LOG.Debug("BIOSIGNATURE_FACTORY.GetSignature BioSignature Simple...");
                        biosignature = sxmlABS;
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Biometrika.BioSignature.Advanced.GetSignature", ex);
                ret = Errors.IERR_UNKNOWN;
            }

            return ret;
        }

        public int VerifySignature(string biosignature, out bool isverifyok, out string errdescription)
        {
            int ret = 0;
            errdescription = null;
            isverifyok = false;
            try
            {
                if (String.IsNullOrEmpty(biosignature))
                {
                    ret = Errors.IERR_BAD_PARAMETER;
                    errdescription = "Error: BioSignature nulo o vacio!";
                }
                else
                {
                    ret = XMLSignatureHelper.VerifyXml(biosignature, out isverifyok, out errdescription);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Biometrika.BioSignature.Advanced.VerifySignature", ex);
                ret = Errors.IERR_UNKNOWN;
            }

            return ret;
        }

#region private

        private string ArmaXML(string id, string sample, int sampletype, string source, string photografy, string score, string ddata)
        {
            //Bio.Core.Api.DynamicDataItem item; 
            string ret = null;
            try
            {

                ret = "<BioSignature id=\"IDREF.v1\" >" +
                            "<TrackID>" + id + "</TrackID>" +
                            "<Timestamp>" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") + "</Timestamp>" +
                            "<Source>" + source + "</Source>" +
                            "<Score>" + score + "</Score>" +
                            "<Sample>" + sample + "</Sample>" +
                            "<SampleType>" + sampletype + "</SampleType>" +
                            "<Barcode>" + GenBarcodeB64(id) + "</Barcode>" +
                            "<Photografy>" + photografy + "</Photografy>" +
                            "<DynamicData>" + (String.IsNullOrEmpty(ddata)? "":ddata) + "</DynamicData>";
                ret += "</BioSignature>";           
            }
            catch (Exception ex)
            {
                LOG.Error("Biometrika.BioSignature.Advanced.ArmaXML", ex);
            }
            return ret;
        }

        private string GenBarcodeB64(string id)
        {
            string ret = "";
            try
            {
                FormQR frmQR = new FormQR();
                ret = frmQR.GetImgQRB64(String.IsNullOrEmpty(_URLQR)?Properties.Settings.Default.URLQR:_URLQR + "?id=" + id);
            }
            catch (Exception ex)
            {
               LOG.Error("Biometrika.BioSignature.Advanced.GenBarcodeB64", ex);
               ret = "";
            }
            return ret;
        }

#endregion private

    }
}
