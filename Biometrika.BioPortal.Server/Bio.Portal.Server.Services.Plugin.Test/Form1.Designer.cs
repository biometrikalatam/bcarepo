﻿namespace Bio.Portal.Server.Services.Plugin.Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.rtbISO = new System.Windows.Forms.RichTextBox();
            this.rtbResult = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTypeid = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtValueid = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.chkFacePhi = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.chkSendRaw = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMT = new System.Windows.Forms.TextBox();
            this.chkNamku = new System.Windows.Forms.CheckBox();
            this.chkVerilook = new System.Windows.Forms.CheckBox();
            this.txtFileImage = new System.Windows.Forms.TextBox();
            this.txtTH = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtbISO
            // 
            this.rtbISO.Location = new System.Drawing.Point(25, 67);
            this.rtbISO.Name = "rtbISO";
            this.rtbISO.Size = new System.Drawing.Size(851, 176);
            this.rtbISO.TabIndex = 0;
            this.rtbISO.Text = "";
            // 
            // rtbResult
            // 
            this.rtbResult.Location = new System.Drawing.Point(25, 317);
            this.rtbResult.Name = "rtbResult";
            this.rtbResult.Size = new System.Drawing.Size(851, 264);
            this.rtbResult.TabIndex = 1;
            this.rtbResult.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(126, 260);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(262, 38);
            this.button1.TabIndex = 2;
            this.button1.Text = "Convert...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(115, 12);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(761, 20);
            this.txtURL.TabIndex = 3;
            this.txtURL.Text = "http://localhost:4030/BioPortal.Server.Plugin.Process.asmx";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "URL BioPortal";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(418, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Company id";
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(486, 38);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(73, 20);
            this.txtCompany.TabIndex = 5;
            this.txtCompany.Text = "7";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(587, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Type Id";
            // 
            // txtTypeid
            // 
            this.txtTypeid.Location = new System.Drawing.Point(636, 38);
            this.txtTypeid.Name = "txtTypeid";
            this.txtTypeid.Size = new System.Drawing.Size(73, 20);
            this.txtTypeid.TabIndex = 7;
            this.txtTypeid.Text = "RUT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(731, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Value id";
            // 
            // txtValueid
            // 
            this.txtValueid.Location = new System.Drawing.Point(782, 38);
            this.txtValueid.Name = "txtValueid";
            this.txtValueid.Size = new System.Drawing.Size(94, 20);
            this.txtValueid.TabIndex = 9;
            this.txtValueid.Text = "21284415-2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Templae ISO en Base64";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(772, 587);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(101, 29);
            this.button2.TabIndex = 12;
            this.button2.Text = "Verify ANSI...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(562, 587);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(204, 29);
            this.button3.TabIndex = 13;
            this.button3.Text = "Verify JPEG Cedula + Face Algoritmo";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // chkFacePhi
            // 
            this.chkFacePhi.AutoSize = true;
            this.chkFacePhi.Location = new System.Drawing.Point(298, 587);
            this.chkFacePhi.Name = "chkFacePhi";
            this.chkFacePhi.Size = new System.Drawing.Size(90, 17);
            this.chkFacePhi.TabIndex = 14;
            this.chkFacePhi.Text = "Con FacePHI";
            this.chkFacePhi.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(111, 587);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(170, 29);
            this.button4.TabIndex = 15;
            this.button4.Text = "Extract ISO From WSQ/RAW...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // chkSendRaw
            // 
            this.chkSendRaw.AutoSize = true;
            this.chkSendRaw.Location = new System.Drawing.Point(25, 587);
            this.chkSendRaw.Name = "chkSendRaw";
            this.chkSendRaw.Size = new System.Drawing.Size(80, 17);
            this.chkSendRaw.TabIndex = 16;
            this.chkSendRaw.Text = "Send RAW";
            this.chkSendRaw.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 606);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "MT:";
            // 
            // txtMT
            // 
            this.txtMT.Location = new System.Drawing.Point(54, 603);
            this.txtMT.Name = "txtMT";
            this.txtMT.Size = new System.Drawing.Size(39, 20);
            this.txtMT.TabIndex = 17;
            this.txtMT.Text = "14";
            // 
            // chkNamku
            // 
            this.chkNamku.AutoSize = true;
            this.chkNamku.Location = new System.Drawing.Point(298, 606);
            this.chkNamku.Name = "chkNamku";
            this.chkNamku.Size = new System.Drawing.Size(82, 17);
            this.chkNamku.TabIndex = 19;
            this.chkNamku.Text = "Con Namku";
            this.chkNamku.UseVisualStyleBackColor = true;
            // 
            // chkVerilook
            // 
            this.chkVerilook.AutoSize = true;
            this.chkVerilook.Location = new System.Drawing.Point(387, 587);
            this.chkVerilook.Name = "chkVerilook";
            this.chkVerilook.Size = new System.Drawing.Size(86, 17);
            this.chkVerilook.TabIndex = 20;
            this.chkVerilook.Text = "Con Verilook";
            this.chkVerilook.UseVisualStyleBackColor = true;
            // 
            // txtFileImage
            // 
            this.txtFileImage.Location = new System.Drawing.Point(474, 606);
            this.txtFileImage.Name = "txtFileImage";
            this.txtFileImage.Size = new System.Drawing.Size(73, 20);
            this.txtFileImage.TabIndex = 21;
            this.txtFileImage.Text = "selfie.jpg";
            // 
            // txtTH
            // 
            this.txtTH.Location = new System.Drawing.Point(474, 585);
            this.txtTH.Name = "txtTH";
            this.txtTH.Size = new System.Drawing.Size(73, 20);
            this.txtTH.TabIndex = 22;
            this.txtTH.Text = "0.7";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(474, 260);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(262, 38);
            this.button5.TabIndex = 23;
            this.button5.Text = "Convert Via BVI Proxy";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 628);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.txtTH);
            this.Controls.Add(this.txtFileImage);
            this.Controls.Add(this.chkVerilook);
            this.Controls.Add(this.chkNamku);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtMT);
            this.Controls.Add(this.chkSendRaw);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.chkFacePhi);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtValueid);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTypeid);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtURL);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.rtbResult);
            this.Controls.Add(this.rtbISO);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "BioPortal Plugin Test...";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbISO;
        private System.Windows.Forms.RichTextBox rtbResult;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTypeid;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtValueid;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox chkFacePhi;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckBox chkSendRaw;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMT;
        private System.Windows.Forms.CheckBox chkNamku;
        private System.Windows.Forms.CheckBox chkVerilook;
        private System.Windows.Forms.TextBox txtFileImage;
        private System.Windows.Forms.TextBox txtTH;
        private System.Windows.Forms.Button button5;
    }
}

