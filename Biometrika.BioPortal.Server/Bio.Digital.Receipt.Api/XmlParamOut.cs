﻿using System;
using log4net;

namespace Bio.Digital.Receipt.Api
{
    ///<summary>
    ///</summary>
    public class XmlParamOut : System.ICloneable
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(XmlParamOut));

#region Private

        /// <summary>
        /// Mensaje de error si el retorno es =! 0
        /// </summary>
        private string _message;

        /// <summary>
        /// Track ID de la transacción, para enlazarlo con la transacción
        /// de negocios.
        /// </summary>
        private string _trackid;

        /// <summary>
        /// Timestamp d ela transacción realizada
        /// </summary>
        private DateTime _timestampend;
        
        /// <summary>
        /// Accion realizada
        /// Posibles Valores =
        ///      1-Generate
        ///      2-Verify
        ///      3-Get
        /// </summary>
        private int _actionid;

        /// <summary>
        /// Es usado para enviar datos adicionales
        /// </summary>
        private string _additionaldata;
        
        /// <summary>
        /// Id de Compañia para filtrar si está deshabilitada. 
        /// Si no viene informada o no existe, se rechaza transaccion
        /// </summary>
        private int _companyid;
        /// <summary>
        /// Usuario de la compañia companyid, para referencia.
        /// </summary>
        private int _userid;

        /// <summary>
        /// Id de Origen de la transacción. Si el origen no existe se rechaza la transacción.
        /// </summary>
        private int _origin;

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist 
        /// </summary>
        private string _clientid;
        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        private string _ipenduser;
        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        private string _enduser;

        /// <summary>
        /// XML del recibo generado. Es un Envelope, donde está el XML firmado y el Timestamp
        /// </summary>
        private string _receipt;

        /// <summary>
        /// 0 - Recibo OK | 1 - Recibo existe pero firma inválida | 2 - recibo no existe
        /// </summary>
        private int _resultverify;

        #endregion Private

        #region Public

        public int ExecutionResult { get; set; }

        /// <summary>
        /// Mensaje de error si el retorno es =! 0
        /// </summary>
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        /// <summary>
        /// Track ID de la transacción, para enlazarlo con la transacción
        /// de negocios.
        /// </summary>
        public string Trackid
        {
            get { return _trackid; }
            set { _trackid = value; }
        }

        /// <summary>
        /// Timestamp d ela transacción realizada
        /// </summary>
        public DateTime Timestampend
        {
            get { return _timestampend; }
            set { _timestampend = value; }
        }

        /// <summary>
        /// Accion realizada
        /// Posibles Valores =
        ///      1-Generate
        ///      2-Verify
        ///      3-Get
        /// </summary>
        public int Actionid
        {
            get { return _actionid; }
            set { _actionid = value; }
        }


        /// <summary>
        /// Es usado para enviar datos adicionales 
        /// </summary>
        public string Additionaldata
        {
            get { return _additionaldata; }
            set { _additionaldata = value; }
        }

        /// <summary>
        /// Id de Compañia para filtrar si está deshabilitada. 
        /// Si no viene informada o no existe, se rechaza transaccion
        /// </summary>
        public int Companyid
        {
            get { return _companyid; }
            set { _companyid = value; }
        }

        /// <summary>
        /// Usuario de la compañia companyid, para referencia.
        /// </summary>
        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }
        }

        /// <summary>
        /// Id de Origen de la transacción. Si el origen no existe se rechaza la transacción.
        /// </summary>
        public int Origin
        {
            get { return _origin; }
            set { _origin = value; }
        }

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist 
        /// </summary>
        public string Clientid
        {
            get { return _clientid; }
            set { _clientid = value; }
        }

        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        public string Ipenduser
        {
            get { return _ipenduser; }
            set { _ipenduser = value; }
        }

        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        public string Enduser
        {
            get { return _enduser; }
            set { _enduser = value; }
        }

        /// <summary>
        /// RDEnvelope, conde el sobre inclye el XML del recibo firmado con XMLDSIG y el timestamp
        /// </summary>
        public string Receipt 
        {
            get { return _receipt; }
            set { _receipt = value; }
        }

        public int ResultVerify
        {
            get { return _resultverify; }
            set { _resultverify = value; }
        }

        #endregion Public

        #region Static Public

        static public string GetXML(int action, string trackid, string clientid, string ipenduser, string enduser, 
                                    int companyid, int userid, DateTime end, int originid, string receipt, int resultverify)
        {
            XmlParamOut paramout = new XmlParamOut();
            string xmlret = null;
            try
            {
                paramout.Actionid = action;
                paramout.Trackid = trackid;
                paramout.Clientid = clientid;
                paramout.Ipenduser = ipenduser;
                paramout.Enduser = enduser;
                paramout.Companyid = companyid;
                paramout.Userid = userid;
                paramout.Timestampend = end;
                paramout.Origin = originid;
                paramout.Receipt = receipt;
                paramout.ResultVerify = resultverify;
                //Serializo
                xmlret = XmlUtils.SerializeObject<XmlParamOut>(paramout);
            }
            catch (Exception ex)
            {
                LOG.Error("XmlParamOut.GetXML Exception", ex);
            }
            return xmlret;
        }

 #endregion Static Public

        #region Implementation of ICloneable

        /// <summary>
        /// Crea un nuevo objeto copiado de la instancia actual.
        /// </summary>
        /// <returns>
        /// Nuevo objeto que es una copia de esta instancia.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public object Clone()
        {
            return MemberwiseClone(); 
        }

        #endregion
    }

}