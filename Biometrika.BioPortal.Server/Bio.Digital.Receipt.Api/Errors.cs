﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bio.Digital.Receipt.Api
{
    public class Errors
    {
#region Constantes

        public static int IRET_OK = 0;

        public static int IRET_ERR_DESCONOCIDO = -1;
        public static string SRET_ERR_DESCONOCIDO = "Error desconocido";

        public static int IRET_ERR_PARAM_INCORRECTOS = -2;
        public static string SRET_ERR_PARAM_INCORRECTOS = "Los parametros ingresados son incorrectos";

        public static int IRET_ERR_SCHEMA_RD_INVALID = -3;
        public static int IRET_ERR_SAVE_DB = -4;
        public static string SRET_ERR_SAVE_DB = "Error grabando en base de datos";

        public static int IRET_ERR_RECEIPT_NO_IN_DB = -5;
        public static string SRET_ERR_RECEIPT_NO_IN_DB = "Receipt ingresado no existe en base de datos";

        public static int IRET_ERR_COMPANY_NOT_EXIST = -6;
        public static string SRET_ERR_COMPANY_NOT_EXIST = "Company no existe";

        public static int IRET_ERR_COMPANY_DISABLED = -7;
        public static string SRET_ERR_COMPANY_DISABLED = "Company deshabilitada";

        public static int IRET_ERR_APLICATION_NOT_EXIST = -8;
        public static string SRET_ERR_APLICATION_NOT_EXIST = "Aplicación inexistente";

        public static int IRET_ERR_APLICATION_DISABLED = -9;
        public static string SRET_ERR_APLICATION_DISABLED = "Aplicación deshabilitada";

        public static int IRET_ERR_SIGNATURE = -20;
        public static int IRET_ERR_SIGNATURE_INVALID = -21;

        public static int IRET_ERR_TSA = -40;

        #region 600 - API Error

        static public int IERR_SERIALIZE_XMLPARAMIN = -601;
        static public string SERR_SERIALIZE_XMLPARAMIN = "Error al serializar XmlParamIn";

        static public int IERR_DESERIALIZE_XMLPARAMOUT = -602;
        static public string SERR_DESERIALIZE_XMLPARAMOUT = "Error al deserializar XmlParamOut";

        #endregion

        #endregion Constantes

        static public string GetDescription(int error)
        {
            switch (error)
            {
                case -1:
                    return SRET_ERR_DESCONOCIDO;
                case -2:
                    return SRET_ERR_PARAM_INCORRECTOS;
                case -4:
                    return SRET_ERR_SAVE_DB;
                case -5:
                    return SRET_ERR_RECEIPT_NO_IN_DB;
                case -6:
                    return SRET_ERR_COMPANY_NOT_EXIST;
                case -7:
                    return SRET_ERR_COMPANY_DISABLED;
                case -8:
                    return SRET_ERR_APLICATION_NOT_EXIST;
                case -9:
                    return SRET_ERR_APLICATION_DISABLED;
                case -601:
                    return SERR_SERIALIZE_XMLPARAMIN;
                case -602:
                    return SERR_DESERIALIZE_XMLPARAMOUT;
                default:
                    return "Error No Documentado";
            }

        }
    }
}
