﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bio.Digital.Receipt.Api
{
    public class RdExtensionItem
    {

        private string keyField;

        private string valueField;

        /// <comentarios/>
        public string key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }

        /// <comentarios/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }
}
