﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bio.Core.Api;
using BioPortal.Server.Api;
using BioPortal.Server.Api.Json;
using BioPortal.Server.Api.Json.API;

namespace Test_Verify_BPWS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int actual = -1;
            string xmlparamout;
            try {
                using (BioPortalServerWS.BioPortalServerWS target = new BioPortalServerWS.BioPortalServerWS())
                {
                    target.Timeout = 60000;
                    target.Url = textBox1.Text;
                    this.richTextBox3.Text = "Consultando a " + target.Url + "...";
                    this.richTextBox3.Refresh();
                    BioPortal.Server.Api.XmlParamIn pin = new BioPortal.Server.Api.XmlParamIn();
                    pin.Actionid = Bio.Core.Api.Constant.Action.ACTION_VERIFY;  //Valor 1
                    pin.Additionaldata = null;
                    pin.Authenticationfactor = Bio.Core.Api.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;  //Valor 2
                    pin.Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_NEC; //Valor 1
                    pin.Bodypart = Bio.Core.Api.Constant.BodyPart.BODYPART_DEDOPULGARDERECHO; //Valor 1
                    pin.Clientid = "IdClienteTest";
                    pin.Companyid = Convert.ToInt32(textBox2.Text);
                    pin.Enduser = "EnUserTest";
                    pin.Ipenduser = "127.0.0.1";
                    pin.Matchingtype = 1;
                    pin.Origin = 1;  //Luego en produccion les inidcamos un valor 

                    //Primero agrego la muestra
                    pin.SampleCollection = new List<Bio.Core.Api.Sample>();
                    Bio.Core.Api.Sample sample = new Bio.Core.Api.Sample();
                    sample.Data = this.richTextBox1.Text.Trim();
                    sample.Minutiaetype = Convert.ToInt32(textBox5.Text); // Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_WSQ;  //Valor 21 o RAW Valor 22
                    sample.Additionaldata = null;
                    pin.SampleCollection.Add(sample);

                    //Segundo agrego PDF417
                    Bio.Core.Api.Sample sample1 = new Bio.Core.Api.Sample();
                    sample1.Data = this.richTextBox2.Text.Trim();
                    sample1.Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_PDF417CEDULA;  //Valor 25
                    sample1.Additionaldata = null;
                    pin.SampleCollection.Add(sample1);

                    pin.SaveVerified = 2;
                    pin.Threshold = Convert.ToDouble(textBox4.Text);
                    pin.Userid = 0;
                    pin.Verifybyconnectorid = "DMANec";
                    pin.InsertOption = 2;
                    pin.PersonalData = new Bio.Core.Api.PersonalData();
                    pin.PersonalData.Typeid = "RUT";
                    pin.PersonalData.Valueid = textBox3.Text;
                    pin.OperationOrder = Bio.Core.Api.Constant.OperationOrder.OPERATIONORDER_REMOTEONLY; //Valor 3

                    string xmlparamin = XmlUtils.SerializeAnObject(pin);
                    actual = target.Verify(xmlparamin, out xmlparamout);
                }

                ShowResult(actual, xmlparamout);
            }
            catch (Exception ex)
            {
                this.richTextBox3.Text = ex.Message;
            }
        }

        private void ShowResult(int actual, string xmlparamout)
        {
            BioPortal.Server.Api.XmlParamOut paramout = null;
            try
            {
                this.richTextBox3.Text = "Codigo Retorno=" + actual.ToString() + " [" + Bio.Core.Api.Constant.Errors.GetDescription(actual) + "]";
                if (xmlparamout != null)
                {
                    paramout = XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlparamout);
                    if (paramout != null)
                    {
                        this.richTextBox3.Text += Environment.NewLine + "MsgErr=" + (paramout.Message == null ? "No Message" : paramout.Message);

                    }
                }
                if (actual == 0)
                {
                    this.richTextBox3.Text += Environment.NewLine + "Track Id = " + paramout.Trackid;
                    this.richTextBox3.Text += Environment.NewLine + "Verificación = " + (paramout.Result == 1 ? "Positiva" : "Negativa");
                    this.richTextBox3.Text += Environment.NewLine + "Score = " + paramout.Score + " / " + paramout.Threshold;
                }
            }
            catch (Exception ex)
            {
                this.richTextBox3.Text = "Exception = " + ex.StackTrace; 
            }

        }

        private void ShowResult(BioPortal.Server.Api.Json.XmlParamOut xmlParamOut)
        {
            try
            {
                richTextBox3.Text = "Codigo Retorno=" + xmlParamOut.ExecutionResult + " [" + Bio.Core.Api.Constant.Errors.GetDescription(xmlParamOut.ExecutionResult) + "]";
                if (xmlParamOut != null)
                {
                    richTextBox3.Text += Environment.NewLine + "MsgErr=" + (xmlParamOut.Message == null ? "No Message" : xmlParamOut.Message);
                }
                if (xmlParamOut.ExecutionResult == 0)
                {
                    this.richTextBox3.Text += Environment.NewLine + "Track Id = " + xmlParamOut.Trackid;
                    this.richTextBox3.Text += Environment.NewLine + "Verificación = " + (xmlParamOut.Result == 1 ? "Positiva" : "Negativa");
                    this.richTextBox3.Text += Environment.NewLine + "Score = " + xmlParamOut.Score + " / " + xmlParamOut.Threshold;
                }
            }
            catch (Exception ex)
            {
                this.richTextBox3.Text = "Exception = " + ex.StackTrace;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox6.Text.Trim()))
                MessageBox.Show("Debe ingresar una para URL API", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                API API = new API(textBox6.Text.Trim());

                BioPortal.Server.Api.Json.XmlParamIn xmlParamIn = new BioPortal.Server.Api.Json.XmlParamIn
                {
                    Actionid = Bio.Core.Api.Constant.Action.ACTION_VERIFY,
                    Additionaldata = null,
                    Authenticationfactor = Bio.Core.Api.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                    Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_NEC,
                    Bodypart = Bio.Core.Api.Constant.BodyPart.BODYPART_DEDOPULGARDERECHO,
                    Clientid = "IdClienteTest",
                    Companyid = Convert.ToInt32(textBox2.Text),
                    Enduser = "EnUserTest",
                    Ipenduser = "127.0.0.1",
                    Matchingtype = 1,
                    Origin = 1,
                    SampleCollection = new List<BioPortal.Server.Api.Json.Sample>(),
                    SaveVerified = 1,
                    Threshold = Convert.ToDouble(textBox4.Text),
                    Userid = 0,
                    Verifybyconnectorid = "DMANec",
                    InsertOption = 1,
                    PersonalData = new BioPortal.Server.Api.Json.PersonalData
                    {
                        Typeid = "RUT",
                        Valueid = textBox3.Text
                    },
                    OperationOrder = Bio.Core.Api.Constant.OperationOrder.OPERATIONORDER_REMOTEONLY
                };

                //Primero agrego la muestra
                xmlParamIn.SampleCollection.Add(new BioPortal.Server.Api.Json.Sample
                {
                    Data = richTextBox1.Text.Trim(),
                    Minutiaetype = Convert.ToInt32(textBox5.Text),
                    Additionaldata = null
                });

                //Segundo agrego PDF417
                xmlParamIn.SampleCollection.Add(new BioPortal.Server.Api.Json.Sample
                {
                    Data = richTextBox2.Text.Trim(),
                    Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_PDF417CEDULA,
                    Additionaldata = null
                });

                BioPortal.Server.Api.Json.XmlParamOut xmlParamOut = API.WSVerify(xmlParamIn);
                ShowResult(xmlParamOut);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int result;
            double score;
            string msg;
            using (BVIProxyWS.BVIProxy ws = new BVIProxyWS.BVIProxy())
            {
                ws.Timeout = 60000;
                int ret = ws.Verify(Convert.ToInt32(textBox2.Text), "RUT", textBox3.Text, "127.0.0.1", richTextBox2.Text.Trim(), richTextBox1.Text.Trim(), 
                    Convert.ToInt32(textBox5.Text), Convert.ToInt32(textBox4.Text), out result, out score, out msg);
                richTextBox3.Text = "Ret = " + ret + " - Result = " + result + " - Score = " + score;
            }
        }
    }
}
