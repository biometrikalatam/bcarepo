﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NecService2021
{


    internal class TxLogHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(TxLogHelper));

        private string _NAME_CURRENT_LOG = DateTime.Now.ToString("yyyyMMdd") + "_TxLog.db.log";

        internal TxLog _LOG_TX;

        /// <summary>
        /// Toma la fecha actual, arma nombe de archivo y se fija que tenga un archivo creado para ese dia,
        /// si no es asi lo crea, sino lo lee.
        /// Si el directorio LogTx no está creado lo crea.
        /// </summary>
        /// <returns></returns>
        internal bool Initialize()
        {
            bool ret = false;
            try
            {
                LOG.Info("TxLogHelper.Initialize IN...");
                LOG.Info("TxLogHelper.Initialize - Chequeando directorio...");
                if (!System.IO.Directory.Exists(Properties.Settings.Default.ServiceTxLogPath)) 
                {
                    LOG.Info("TxLogHelper.Initialize - Creando directorio => " +
                              Properties.Settings.Default.ServiceTxLogPath);
                    System.IO.Directory.CreateDirectory(Properties.Settings.Default.ServiceTxLogPath);
                    ret = true;
                } else
                {
                    LOG.Info("TxLogHelper.Initialize - Directorio OK => " +
                              Properties.Settings.Default.ServiceTxLogPath);
                }

                LOG.Info("TxLogHelper.Initialize - Chequeando log actual => " + _NAME_CURRENT_LOG);
                if (System.IO.File.Exists(Properties.Settings.Default.ServiceTxLogPath + _NAME_CURRENT_LOG))
                {
                    LOG.Info("TxLogHelper.Initialize - Log del dia existe => Leyendo =>" +
                              Properties.Settings.Default.ServiceTxLogPath + _NAME_CURRENT_LOG);
                    _LOG_TX = JsonConvert.DeserializeObject<TxLog>
                                    (System.IO.File.ReadAllText(
                                        Properties.Settings.Default.ServiceTxLogPath + _NAME_CURRENT_LOG));
                }
                else
                {
                    LOG.Info("TxLogHelper.Initialize - Log del dia NO Existe => Creando " +
                              Properties.Settings.Default.ServiceTxLogPath + _NAME_CURRENT_LOG);
                    _LOG_TX = new TxLog();
                    lock (Program._OBJECT_TO_BLOCK)
                    {
                        try
                        {
                            System.IO.File.WriteAllText(Properties.Settings.Default.ServiceTxLogPath + _NAME_CURRENT_LOG,
                                                    JsonConvert.SerializeObject(_LOG_TX));
                        }
                        catch (Exception ex)
                        {
                            LOG.Error("TxLogHelper.Initialize - WriteAllText Excp Error: " + ex.Message);
                        }
                    }
                }
                if (_LOG_TX != null)
                {
                    ret = true;
                }
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error(" Error: " + ex.Message);
            }
            LOG.Info("TxLogHelper.Initialize OUT!");
            return ret;
        }

        internal bool Add(NecDataToCompare2021 paramin, string wsq, string timetoresponse, string score)
        {
            bool ret = false;
            try
            {
                LOG.Debug("TxLogHelper.Add IN...");
                Tx transaction = new Tx();
                transaction.id = Guid.NewGuid().ToString("N");
                transaction.date = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                transaction.minutiaeTypeNecData = paramin.minutiaeTypeNecData;
                transaction.sampleNec = paramin.necData;
                transaction.wsq = wsq;
                transaction.timeToResponse = timetoresponse;
                PDF417Data pdfdata = ParsePDF417(paramin.necData);
                if (pdfdata != null)
                {
                    transaction.rut = pdfdata.rut;
                    transaction.name = pdfdata.name;
                    transaction.pais = pdfdata.pais;
                    transaction.numeroDeSerie = pdfdata.numeroDeSerie;
                    transaction.expiracion = pdfdata.expiracion;
                }
                transaction.score = score;

                LOG.Debug("TxLogHelper.Add - Agregando TX Id = " + transaction.id + " | RUT = " + transaction.rut + " | " +
                            transaction.name + " | Score = " + transaction.score);
                
                lock (Program._OBJECT_TO_BLOCK)
                {
                    try
                    {
                        _LOG_TX.Transactions.Add(transaction);
                        System.IO.File.WriteAllText(Properties.Settings.Default.ServiceTxLogPath + _NAME_CURRENT_LOG,
                                                    JsonConvert.SerializeObject(_LOG_TX));
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("TxLogHelper.Initialize - WriteAllText Excp Error: " + ex.Message);
                    }
                }
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("TxLogHelper.Add - Excp Error: " + ex.Message);
            }
            LOG.Debug("TxLogHelper.Add OUT! [ret=" + ret.ToString() + "]");
            return ret;
        }

        /// <summary>
        /// Lee un JSON desde disco con el seel enviado. El Seel es el formateo del dia:
        ///   seel = yyyyMMdd
        /// </summary>
        /// <param name="seel"></param>
        /// <returns></returns>
        internal TxLog Read(string seel)
        {
            TxLog ret = null;
            string logName = null;
            try
            {
                LOG.Info("TxLogHelper.Read IN...");
                if (string.IsNullOrEmpty(seel))
                {
                    logName = _NAME_CURRENT_LOG;
                } else
                {
                    logName = seel + "_TxLog.db.log";
                }

                LOG.Info("TxLogHelper.Read - Chequeando log => " + logName + "...");
                if (System.IO.File.Exists(System.IO.Directory.GetCurrentDirectory() + "\\TxLog\\" + logName))
                {
                    LOG.Info("TxLogHelper.Read - Log existe => Leyendo =>" +
                              Properties.Settings.Default.ServiceTxLogPath + logName);
                    ret = JsonConvert.DeserializeObject<TxLog>
                                    (Properties.Settings.Default.ServiceTxLogPath + _NAME_CURRENT_LOG);
                }
                else
                {
                    LOG.Info("TxLogHelper.Read - Log del dia NO Existe");
                    ret = null;
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("TxLogHelper.Read - Excp Error: " + ex.Message);
            }
            LOG.Info("TxLogHelper.Read OUT!");
            return ret;
        }

        /// <summary>
        /// Parsea PDF417 de cedula Chilena
        /// </summary>
        /// <param name="pdf417"></param>
        /// <param name="datos"></param>
        /// <returns></returns>
        //[ComVisible(true)]
        private PDF417Data ParsePDF417(string pdf417)
        {
            byte[] pdf = null;
            PDF417Data ret = new PDF417Data();
            try
            {
                LOG.Debug("TxLogHelper.ParsePDF417 In...");
                if (!string.IsNullOrEmpty(pdf417))
                {
                    pdf = Convert.FromBase64String(pdf417);

                    ret.rut = Encoding.UTF7.GetString(pdf, 0, 9).Replace('\0', ' ').TrimEnd();
                    ret.name = Encoding.UTF7.GetString(pdf, 19, 30).Replace('\0', ' ').TrimEnd();
                    ret.pais = Encoding.UTF7.GetString(pdf, 49, 3).Replace('\0', ' ').TrimEnd();
                    ret.numeroDeSerie = Encoding.UTF7.GetString(pdf, 58, 10).Replace('\0', ' ').TrimEnd();

                    string aa = Encoding.UTF7.GetString(pdf, 52, 2);
                    string mm = Encoding.UTF7.GetString(pdf, 54, 2);
                    string dd = Encoding.UTF7.GetString(pdf, 56, 2);
                    ret.expiracion = dd + "/" + mm + "/" + aa;

                    //int finger = (pdf[73] << 24) + (pdf[72] << 16) + (pdf[71] << 8) + (pdf[70]);
                    //_pcLen = (pdf[77] << 24) + (pdf[76] << 16) + (pdf[75] << 8) + (pdf[74]);
                    //_PC1Cedula = new Byte[_pcLen];
                    //Buffer.BlockCopy(pdf, 78, _PC1Cedula, 0, _pcLen);

                    LOG.Debug("TxLogHelper.ParsePDF417 - RUT = " + ret.rut + " | Name = " + ret.name);
                }
                else
                {
                    ret = null;
                    LOG.Error("TxLogHelper.ParsePDF417 Error PDF417 es nulo...");
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("TxLogHelper.ParsePDF417 Error - Msg=" + ex.Message + " - Stack=" + ex.StackTrace);
            }

            LOG.Debug("TxLogHelper.ParsePDF417 Out!");
            return ret;
        }
    }

    internal class TxLog
    {
        public List<Tx> Transactions;

        public TxLog()
        {
            Transactions = new List<Tx>();
        }
    }

    internal class Tx
    {
        public string id;
        public string date;
        public string timeToResponse;
        public string rut;
        public string name;
        public string pais;
        public string numeroDeSerie;
        public string expiracion;
        public string wsq;
        public int minutiaeTypeNecData;
        public string sampleNec;
        public string score;
    }

    internal class PDF417Data
    {
        public string rut;
        public string name;
        public string pais;
        public string numeroDeSerie;
        public string expiracion;
    } 
}
