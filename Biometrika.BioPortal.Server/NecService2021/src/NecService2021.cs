﻿using log4net;
using Nancy;
using Nancy.Hosting.Self;
using System;
using System.Configuration;

namespace NecService2021
{
    internal class NecService2021
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NecService2021));
        NancyHost host;
        public NecService2021()
        {
        }

        public void Start()
        {
            try
            {
                string url = $"http://localhost:{Properties.Settings.Default.ServicePort}";
                LOG.Info($"NecService2021.Start - Configurado para ejecutarse en {url}");

                HostConfiguration hostConfigs = new HostConfiguration()
                {
                    UrlReservations = new UrlReservations() { CreateAutomatically = true },
                };
                LOG.Info($"NecService2021.Start - Configuración creada");
                host = new NancyHost(new Uri(url), new BiometrikaNancyBootstrapper(), hostConfigs);
                LOG.Info($"NecService2021.Start - Host instanciado");
                host.Start();
                LOG.Info($"NecService2021.Start - Host corriendo");
            }
            catch(Exception ex)
            {
                LOG.Error("NecService2021.Start - Error al inicializar el servicio", ex);
            }
        }

        public void Stop()
        {
            LOG.Info("NecService2021.Stoping...");
            host.Stop();
            LOG.Info("NecService2021.Stopped!");
        }
    }

    public class BiometrikaNancyBootstrapper : DefaultNancyBootstrapper
    {
        protected override void ApplicationStartup(Nancy.TinyIoc.TinyIoCContainer container, 
            Nancy.Bootstrapper.IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);
            Nancy.Json.JsonSettings.MaxJsonLength = Int32.MaxValue;
        }
    }
}
