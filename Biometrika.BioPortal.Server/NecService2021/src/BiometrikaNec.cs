﻿using Bio.Core.Imaging;
using log4net;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace NecService2021
{
    public static class BiometrikaNec
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BiometrikaNec));

        public static int SAMPLE_RAW = 1;
        public static int SAMPLE_WSQ = 2;
        public static int SAMPLE_JPG = 3;

        public static int Verify(int minutiaetypesample, byte[] _sample,
                                 int minutiaetypeminutiaeTypeNecData, byte[] _necData,
                                 out string wsq)
        {
            int _Score = 0;
            byte[] _rawGenerated = null;
            short w, h;
            wsq = null;
            try
            {
                LOG.Debug("BiometrikaNec.Verify - IN...");
                //1. Procesamos sample
                if (minutiaetypesample == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                {
                    LOG.Debug("BiometrikaNec.Verify - Descomprimiendo WSQ...");
                    Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                    dec.DecodeMemory(_sample, out w, out h, out _rawGenerated);
                    dec = null;
                } else
                {
                    _rawGenerated = _sample;
                }
                if (_rawGenerated == null)
                {
                    LOG.Error("BiometrikaNec.Verify - _rawGenerated NUlo => Sale!");
                    _Score = -10002;  //Error descomprimiendo WSQ o viene nulo raw
                    return _Score;
                }
                LOG.Error("BiometrikaNec.Verify - Verifica que sea de 512x512 sino rellena...");
                _rawGenerated = FormatRAW(_rawGenerated);

                Bio.Core.Wsq.Encoder.WsqEncoder enc = new Bio.Core.Wsq.Encoder.WsqEncoder();
                byte[] byWsq = null;
                if (enc.EncodeMemory(_rawGenerated, 512, 512, out byWsq)) {
                    wsq = Convert.ToBase64String(byWsq);
                }
                //2. Procesamos necData
                //TODO - Luego ver si se necesita MInucia sacarla desde el PDF417 por ahora sigue asi como viene
                string strNecData = Convert.ToBase64String(_necData); 
                //BidCedChk.BidCedChk Bioid = new BidCedChk.BidCedChk();
                LOG.Debug("BiometrikaNec.Verify - Calling Bioid.VerificaID...");
                //LOG.Debug("Calling Bioid.VerificaID - RAW to Image = " + Convert.ToBase64String(_rawGenerated));
                Bitmap imageRAW = Bio.Core.Imaging.ImageProcessor.RawToBitmap(_rawGenerated, 512, 512);
                LOG.Debug("BiometrikaNec.Verify - Calling Bioid.VerificaID - necData = " + 
                            Convert.ToBase64String(_necData));
                string result = Program._BIOID.VerificaID(imageRAW, strNecData);
                LOG.Debug("BiometrikaNec.Verify - Calling Bioid.VerificaID - Result = " + result);
                if (!string.IsNullOrEmpty(result) && result.StartsWith("ok"))
                {
                    _Score = Convert.ToInt32((result.Split(';')[1]));
                }
                else
                {
                    _Score = 0;
                }
                LOG.Debug("BiometrikaNec.Verify - Calling Bioid.VerificaID Score = " + _Score.ToString());
            }
            catch (Exception ex)
            {
                LOG.Error("BiometrikaNec.Verify - Calling Bioid.VerificaID Ex - " + ex.Message);
                _Score = -10001;  //Error consumiendo DMA BioID
            }
            return _Score;
        }


        /// <summary>
        /// Formatea RAW si no es de 512x512
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        static private byte[] FormatRAW(byte[] _rawInput)
        {
            byte[] _rawOutput = null;
            byte[] _rawOutput512x512 = null;
            int wRaw = 0;
            int hRaw = 0;
            try
            {
                LOG.Debug("BiometrikaNec.Verify FormatRAW IN...");

                //Si ya está en 512x512 => Solo asigno
                if (_rawInput.Length == 262144)
                {
                    LOG.Debug("BiometrikaNec.Verify FormatRAW => Input 512x512 => no formatea...");
                    _rawOutput512x512 = _rawInput;
                }
                else
                { //Sino formateo
                  //Si tiene tamaño 357x392 => Solo de completa, sino se transforma. 
                    if (_rawInput.Length == 139944)
                    {
                        LOG.Debug("BiometrikaNec.Verify FormatRAW Len 357x392...");
                        wRaw = 357;
                        hRaw = 392;
                        _rawOutput = _rawInput;
                    }

                    if (_rawInput.Length == 275050 || _rawInput.Length == 275000)
                    {
                        //rawIn = new byte[275000];
                        if (_rawInput.Length == 275050)
                        {
                            LOG.Debug("BiometrikaNec.Verify Len 275500 DP Ansi...");
                            for (int i = 50; i < _rawInput.Length; i++)
                            {
                                _rawInput[i - 50] = _rawInput[i];
                            }
                        }
                        else
                        {
                            LOG.Debug("BiometrikaNec.Verify FormatRAW Len 275000 DP...");
                            _rawOutput = _rawInput;
                        }
                        _rawInput = ConvertImageDPRawTo357x392Raw(_rawInput, false, 500, 550);
                        wRaw = 357;
                        hRaw = 392;
                    }

                    //Secugen Hamster Pro 20 => 300x400
                    if (_rawInput.Length == 120000)
                    {
                        LOG.Debug("BiometrikaNec.Verify FormatRAW Len 300x400 Sec Pro 20...");
                        wRaw = 300;
                        hRaw = 400;
                        _rawOutput = _rawInput;
                    }

                    //Secugen Hamster Pro => 260x300 o Secugen Hamster Plus
                    if (_rawInput.Length == 78000)
                    {
                        LOG.Debug("BiometrikaNec.Verify FormatRAW Len 260x300 Sec Hamster Plus o Sec Hamster Pro...");
                        wRaw = 260;
                        hRaw = 300;
                        _rawOutput = _rawInput;
                    }

                    //Secugen Hamster IV => 258x336
                    if (_rawInput.Length == 86688)
                    {
                        LOG.Debug("BiometrikaNec.Verify FormatRAW Len 258x336 Sec Hamster IV...");
                        wRaw = 258;
                        hRaw = 336;
                        _rawOutput = _rawInput;
                    }

                    //Dispo Multicaja
                    if (_rawInput.Length == 92160)
                    {
                        LOG.Debug("BiometrikaNec.Verify FormatRAW Len 256x360 Dispo Multicaja...");
                        wRaw = 256;
                        hRaw = 360;
                        _rawOutput = _rawInput;
                    }
                    _rawOutput512x512 = Bio.Core.Imaging.ImageProcessor.FillRaw(_rawInput, wRaw, hRaw, 512, 512);
                }
                LOG.Debug("BiometrikaNec.Verify FormatRAW FillRaw OK => " + Convert.ToBase64String(_rawOutput512x512));
            }
            catch (Exception ex)
            {
                LOG.Error("BiometrikaNec.Verify FormatRAW Error", ex);
                _rawOutput512x512 = null;
            }
            LOG.Debug("BiometrikaNec.Verify FormatRAW OUT!");
            return _rawOutput512x512;
        }

        internal static byte[] ConvertImageDPRawTo357x392Raw(byte[] imageDPRaw, bool wsqCompressed, int wRaw, int hRaw)
        {
            //Los parametros wRaw y hRaw vienen seteados si compressed es false, 
            //sino se calculan dentro de WsqTools.Decompress
            int w = 0;
            int h = 0;
            int dpix = 500;
            int dpiy = 500;
            int ww = 0; //Convert.ToInt32(pSample.Height);
            int hh = 0; //Convert.ToInt32(pSample.Width);
            ww = 357; //(ww * dpix) / sensor.Xdpi;
            hh = 392; //(hh * dpiy) / sensor.Ydpi;
            byte[] byDataRawD;

            try
            {
                LOG.Debug("Bio.Core.MatcherNecDMA ConvertImageDPRawTo357x392Raw IN...");
                //Si es Raw comprimido, descomprimo
                if (wsqCompressed)
                {
                    Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                    short sw = 0;
                    short sh = 0;
                    if (!dec.DecodeMemory(imageDPRaw, out sw, out sh, out byDataRawD))
                    {
                        return null;
                    }
                    else
                    {
                        w = sw;
                        h = sh;
                    }
                }
                else
                {
                    byDataRawD = imageDPRaw;
                    w = wRaw;
                    h = hRaw;
                }

                //Creo Bitmap para redimensionar a tamaño comun para comparaciones
                Bitmap img = ImageProcessor.RawToBitmap(byDataRawD, w, h);
                Bitmap bmp = new Bitmap(ww, hh, PixelFormat.Format24bppRgb);
                bmp.SetResolution(dpix, dpiy);
                Graphics gr = Graphics.FromImage(bmp);
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.DrawImage(img, new Rectangle(0, 0, ww, hh), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel);
                gr.Dispose();
                byte[] imgData512 = ImageProcessor.BitmapToRaw(bmp);
                //byte[] imgData512 = ImageProcessor.Adjust(imgData, ww, hh, 512, 512);

                LOG.Error("BiometrikaConverter.ConvertImageDPRawTo357x392Raw OUT!");
                return imgData512;
            }
            catch (Exception ex)
            {
                LOG.Error("BiometrikaConverter.ConvertImageDPRawTo357x392Raw Error - " + ex.Message);
                return null;
            }
        }
    }

    public class Result
    {
        public static int ERR_DESCONOCIDO = -1;
        public static int ERR_PARAMETER = -2;
        public static int ERR_PARAMETER_NULL = -3;

        public static int ERR_DESERIALIZE_FROM_BASE64 = -10;
        public static int ERR_SERIALIZE_TO_BASE64 = -11;

        public static int ERR_GENERATING_MATCHING = -20;
        public static int ERR_GENERATING_EXTRACTOR = -21;
        public static int ERR_EXTRACTING = -22;

        public Result() { }
        public Result(int code, string msg, int s, int q1, int q2)
        {
            Code = code;
            Message = msg;
            Score = s;
            Quality = q1;
            QualityNormalized = q2;
        }

        public int Code;
        public string Message;
        public int NECScore
        {
            get { return Score; }
        }
        public int Score;
        public int Quality;
        public int QualityNormalized;
        public string Minutiae1;
        public byte[] Minutiae1Array;
    }

}