﻿using log4net;
using Nancy;
using Nancy.ModelBinding;
using System;

namespace NecService2021
{
    public class NecModule : NancyModule
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NecModule));
        private string parameters;

        public NecModule() 
        {
            int step = 0;
            Get["/ping"] = _ =>
            {
                try
                {
                    string ret = "<html>" + 
                                    "<head>" + 
                                    "  <title>Biometrika Service NEC 2021 - v " + Program._VERSION + "</title>" +
                                    "</head>" +
                                    "<body font=\"arial\">" +
                                    "  <p><h2><b>Producto : </b>Biometrika NecService 2021<h2></p>" +
                                    "  <p><h3><b>Version : </b> " + Program._VERSION + "</h3></p>" +
                                    "  <p><h3><b>URL : </b>" + $"http://localhost:{Properties.Settings.Default.ServicePort}" + "</h3></p>" +
                                    "</body>" +
                                 "</html>";
                    return ret;
                }
                catch (Exception ex)
                {
                    string ret = "<html>" +
                                    "<head>" +
                                    "  <title>Biometrika Service NEC 2021 - v " + Program._VERSION + "</title>" +
                                    "</head>" +
                                    "<body>" +
                                    "  <p><h2><b>Producto : </b>Biometrika NecService 2021<h2></p>" +
                                    "  <p><b><b>ERROR : </b> " + ex.Message + "</h3></p>" + 
                                    "</body>" +
                                 "</html>";
                    LOG.Error(" Error: " + ex.Message);
                    return ret;
                }
            };
            Get["/log"] = _ =>
            {
                try
                {
                    string ret = "<html>" +
                                    "<head>" +
                                    "  <title>Biometrika Service NEC 2021 - v " + Program._VERSION + "</title>" +
                                    "</head>" +
                                    "<body style=\"font-family: arial\">" +
                                    "  <p><h2><b>Producto : </b>Biometrika NecService 2021<h2></p>" +
                                    "  <p><h3><b>Version : </b> " + Program._VERSION + "</h3></p>" +
                                    "  <p><h3><b>URL : </b>" + $"http://localhost:{Properties.Settings.Default.ServicePort}" + "</h3></p>" +
                                    "  <p><h3><b>Log de Transacciones Actual : " + DateTime.Now.ToString("dd/MM/yyyy") + "</h3></p>" +
                                    "  <table style=\"width: 100%; font-family: arial border-style: solid; border-color: green; margin-left: auto; margin-right: auto; \"border=\"1\"> " +
                                    "    <tr>" +
                                    "      <td bgcolor=\"lightgray\" align=\"center\"><b>ID</b></td>" +
                                    "      <td bgcolor=\"lightgray\" align=\"center\"><b>Fecha</b></td>" +
                                    "      <td bgcolor=\"lightgray\" align=\"center\"><b>Tiempo</b></td>" +
                                    "      <td bgcolor=\"lightgray\" align=\"center\"><b>RUT</b></td>" +
                                    "      <td bgcolor=\"lightgray\" align=\"center\"><b>Apellido</b></td>" +
                                    "      <td bgcolor=\"lightgray\" align=\"center\"><b>Pais</b></td>" +
                                    "      <td bgcolor=\"lightgray\" align=\"center\"><b>Fecha Expiracion</b></td>" +
                                    "      <td bgcolor=\"lightgray\" align=\"center\"><b>Serial</b></td>" +
                                    "      <td bgcolor=\"lightgray\" align=\"center\"><b>Score</b></td>" +
                                    "    </tr>";
                    foreach (Tx item in Program._TXLOG_HELPER._LOG_TX.Transactions)
                    {
                        ret += "    <tr>" +
                                    "      <td>" + item.id + "</td>" +
                                    "      <td>" + item.date + "</td>" +
                                    "      <td>" + item.timeToResponse + "</td>" +
                                    "      <td>" + item.rut + "</td>" +
                                    "      <td>" + item.name + "</td>" +
                                    "      <td>" + item.pais + "</td>" +
                                    "      <td>" + item.expiracion + "</td>" +
                                    "      <td>" + item.numeroDeSerie + "</td>"; 
                        if (Convert.ToInt32(item.score) > 1000)
                        {
                            ret += "      <td style=\"color: green\">" + item.score + "</td>" +
                                   "    </tr>";
                        } else
                        {
                            ret += "      <td style=\"color: red\">" + item.score + "</td>" +
                                   "    </tr>";
                        }
                    }

                    ret += "  </table> " +
                                    "</body>" +
                                 "</html>";
                    return ret;
                }
                catch (Exception ex)
                {
                    string ret = "<html>" +
                                    "<head>" +
                                    "  <title>Biometrika Service NEC 2021 - v " + Program._VERSION + "</title>" +
                                    "</head>" +
                                    "<body>" +
                                    "  <p><h2><b>Producto : </b>Biometrika NecService 2021<h2></p>" +
                                    "  <p><b><b>ERROR : </b> " + ex.Message + "</h3></p>" +
                                    "</body>" +
                                 "</html>";
                    LOG.Error(" Error: " + ex.Message);
                    return ret;
                }
            };
            Post["/nec/match"] = _ =>
            {
                DateTime start, end;
                try
                {
                    start = DateTime.Now;
                    LOG.Debug("NecModule.NecModule POST: /nec/match - IN => Solicitud Recibida para comparar templates");
                    var requestData = this.Bind<NecDataToCompare2021>();
                    step = 1;
                    LOG.Debug("NecModule.NecModule POST: Entrada deserializada => " + 
                              "requestData != Null => " + (requestData != null).ToString());
                    if (requestData == null)
                    {
                        return $"Error: Deserializacion Nula " + " | Step = " + step.ToString();
                    }

                    LOG.Debug("NecModule.NecModule POST: /nec/match - Convirtiendo template de sample de base64 a byte[]...");
                    byte[] sample = Convert.FromBase64String(requestData.sample);
                    step = 2;
                    LOG.Debug("NecModule.NecModule POST: /nec/match - Convirtiendo template de NEC en base64 a byte[]...");
                    byte[] necData = Convert.FromBase64String(requestData.necData);
                    step = 3;
                    LOG.Debug("NecModule.NecModule POST: /nec/match - Templates convertidas!");
                    LOG.Debug("NecModule.NecModule POST: /nec/match - Entrando a hacer match...");
                    string wsq = null;
                    int score = BiometrikaNec.Verify(requestData.minutiaeTypeSample, sample,
                                                     requestData.minutiaeTypeNecData, necData,
                                                     out wsq);
                    end = DateTime.Now;
                    if (Properties.Settings.Default.ServiceGenerateTxLog)
                    {
                        Program._TXLOG_HELPER.Add(requestData, wsq, ((end - start).Milliseconds).ToString(), score.ToString());
                    }

                    LOG.Debug($"NecModule.NecModule POST: /nec/match -- Score obtenido: {score}");
                    return score.ToString();
                }
                catch (Exception ex)
                {
                    LOG.Error("NecModule.NecModule POST: /nec/match - Error al hacer el match", ex);
                    return $"Error: {ex.Message} " + " | Step = " + step.ToString();
                }
            };
        }
    }
}
