﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Bio.Core.Matcher.Connector.SRCeI.BioVerify2020
{
    public class SignedXmlWithId : SignedXml
    {
        public SignedXmlWithId(XmlDocument xml)
            : base(xml)
        {
        }

        public SignedXmlWithId(XmlElement xmlElement)
            : base(xmlElement)
        {
        }

        public override XmlElement GetIdElement(XmlDocument doc, string id)
        {
            // check to see if it's a standard ID reference
            XmlElement idElem = base.GetIdElement(doc, id);

            if (idElem == null)
            {
                // I've just hardcoded it for the time being, but should be using an XPath expression here, and the id that is passed in
                idElem = (XmlElement)doc.GetElementsByTagName("Body", doc.FirstChild.NamespaceURI)[0];
            }

            return idElem;
        }
    }

    class RegistroCivilMessageInspector : IClientMessageInspector
    {
        // PREFIXES
        static string PREFIX_WS_SECURITY = "wsse";
        static string PREFIX_WS_UTILITIES = "wsu";
        static string PREFIX_DIGITAL_SIGNATURE = "dsig";
        // NAMESPACES
        static string NAMESPACE_WS_SECURITY = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
        static string NAMESPACE_DIGITAL_SIGNATURE = "http://www.w3.org/2000/09/xmldsig#";
        static string NAMESPACE_WS_UTILITIES = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";

        /// <summary>
        /// Enables inspection or modification of a message before a request message is sent to a service.
        /// </summary>
        /// <param name="request">The message to be sent to the service.</param>
        /// <param name="channel">The WCF client object channel.</param>
        /// <returns>
        /// The object that is returned as the <paramref name="correlationState " /> argument of
        /// the <see cref="M:System.ServiceModel.Dispatcher.IClientMessageInspector.AfterReceiveReply(System.ServiceModel.Channels.Message@,System.Object)" /> method.
        /// This is null if no correlation state is used.The best practice is to make this a <see cref="T:System.Guid" /> to ensure that no two
        /// <paramref name="correlationState" /> objects are the same.
        /// </returns>
        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
        {
            string xml = request.ToString();
            // load the body of the message in an xml document
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = false;
            doc.LoadXml(xml);

            string ns = doc.FirstChild.NamespaceURI;

            //Get the header element, so that we can add the digital signature to it
            XmlElement headerNode = (XmlElement)doc.GetElementsByTagName("Header", ns)[0];

            XmlElement securityNode = doc.CreateElement("Security", NAMESPACE_WS_SECURITY);
            securityNode.Prefix = PREFIX_WS_SECURITY;
            securityNode.SetAttribute("MustUnderstand", "", "1");

            // Set the ID attribute on the body element, so that we can reference it later
            XmlElement bodyNode = (XmlElement)doc.GetElementsByTagName("Body", ns)[0];

            string id = Guid.NewGuid().ToString();
            bodyNode.RemoveAllAttributes();
            bodyNode.SetAttribute("xmlns:wsu", NAMESPACE_WS_UTILITIES);
            bodyNode.SetAttribute("Id", NAMESPACE_WS_UTILITIES, id);

            XmlWriterSettings settings2 = new XmlWriterSettings();
            settings2.Encoding = new System.Text.UTF8Encoding(false);

            // load the certificate containing the private key
            X509Certificate2 localCertificate = Connector._LocalSignatureCertificate; // new X509Certificate2(ConfigurationManager.AppSettings["certificates.local.sign.path"], ConfigurationManager.AppSettings["certificates.local.sign.password"]);
            // sign the xml document
            SignedXmlWithId signedXml = new SignedXmlWithId(doc);
            signedXml.SigningKey = localCertificate.PrivateKey;
            // Specify a canonicalization method.
            signedXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NTransformUrl;

            Reference reference = new Reference("#" + id);
            // add required transformations
            XmlDsigC14NTransform c14t = new XmlDsigC14NTransform();
            reference.AddTransform(c14t);
            signedXml.AddReference(reference);
            // add certificate info
            KeyInfo keyInfo = new KeyInfo();
            KeyInfoX509Data keyInfoData = new KeyInfoX509Data(localCertificate);
            keyInfo.AddClause(keyInfoData);
            signedXml.KeyInfo = keyInfo;
            // compute the signature
            signedXml.ComputeSignature();
            // change the namespace...just to be nice
            XmlElement xmlDigitalSignature = signedXml.GetXml();
            xmlDigitalSignature.SetAttribute("Id", "Signature-1");

            securityNode.AppendChild(xmlDigitalSignature);

            // UsernameToken
            string passwordType = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";
            // Compute required elements
            string phrase = Guid.NewGuid().ToString();
            string nonce = Helpers.GetSHA1String(phrase);
            string untid = Guid.NewGuid().ToString();
            //string username = ConfigurationManager.AppSettings["username"];  //"usr_medleg"; // ConfigurationManager.AppSettings["username"];
            //string password = ConfigurationManager.AppSettings["password"]; //"H%E1K!9&amp;rC2U"; //ConfigurationManager.AppSettings["password"];
            string username = Connector.USERTOKEN_USERNAME;  //"usr_medleg"; // ConfigurationManager.AppSettings["username"];
            string password = Connector.USERTOKEN_PASSWORD; //"H%E1K!9&amp;rC2U"; //ConfigurationManager.AppSettings["password"];
            // creation time
            //DateTime created = DateTime.Now;
            //string createdStr = created.ToString("yyyy-MM-ddThh:mm:ssZ");

            XmlElement unt = doc.CreateElement(PREFIX_WS_SECURITY, "UsernameToken", NAMESPACE_WS_SECURITY);
            unt.SetAttribute("xmlns:wsu", NAMESPACE_WS_UTILITIES);
            unt.SetAttribute("Id", NAMESPACE_WS_UTILITIES, untid);
            XmlElement usernameElement = doc.CreateElement(PREFIX_WS_SECURITY, "Username", NAMESPACE_WS_SECURITY);
            usernameElement.InnerText = username;
            XmlElement passwordElement = doc.CreateElement(PREFIX_WS_SECURITY, "Password", NAMESPACE_WS_SECURITY);
            passwordElement.SetAttribute("Type", passwordType);
            passwordElement.InnerText = password;

            unt.AppendChild(usernameElement);
            unt.AppendChild(passwordElement);
            securityNode.AppendChild(unt);

            headerNode.AppendChild(securityNode);

            // Make sure the byte order mark doesn't get written out
            XmlDictionaryReaderQuotas quotas = new XmlDictionaryReaderQuotas();
            Encoding encoderWithoutBOM = new System.Text.UTF8Encoding(false);

            System.IO.MemoryStream ms = new System.IO.MemoryStream(encoderWithoutBOM.GetBytes(doc.InnerXml));

            XmlDictionaryReader xdr = XmlDictionaryReader.CreateTextReader(ms, encoderWithoutBOM, quotas, null);

            //Create the new message, that has the digital signature in the header
            Message newMessage = Message.CreateMessage(xdr, System.Int32.MaxValue, request.Version);
            request = newMessage;

            return null;
        }

        /// <summary>
        /// Enables inspection or modification of a message after a reply message is received but prior to passing it back to the client application.
        /// </summary>
        /// <param name="reply">The message to be transformed into types and handed back to the client application.</param>
        /// <param name="correlationState">Correlation state data.</param>
        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            // Nothing special here
        }
    }

    /// <summary>
    /// Represents a run-time behavior extension for a client endpoint.
    /// </summary>
    public class RegistroCivilEndpointBehavior : IEndpointBehavior
    {
        /// <summary>
        /// Implements a modification or extension of the client across an endpoint.
        /// </summary>
        /// <param name="endpoint">The endpoint that is to be customized.</param>
        /// <param name="clientRuntime">The client runtime to be customized.</param>
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.ClientMessageInspectors.Add(new RegistroCivilMessageInspector());
        }

        /// <summary>
        /// Implement to pass data at runtime to bindings to support custom behavior.
        /// </summary>
        /// <param name="endpoint">The endpoint to modify.</param>
        /// <param name="bindingParameters">The objects that binding elements require to support the behavior.</param>
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
            // Nothing special here
        }

        /// <summary>
        /// Implements a modification or extension of the service across an endpoint.
        /// </summary>
        /// <param name="endpoint">The endpoint that exposes the contract.</param>
        /// <param name="endpointDispatcher">The endpoint dispatcher to be modified or extended.</param>
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            // Nothing special here
        }

        /// <summary>
        /// Implement to confirm that the endpoint meets some intended criteria.
        /// </summary>
        /// <param name="endpoint">The endpoint to validate.</param>
        public void Validate(ServiceEndpoint endpoint)
        {
            // Nothing special here
        }
    }
}
