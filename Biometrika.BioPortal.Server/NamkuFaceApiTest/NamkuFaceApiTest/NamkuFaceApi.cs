﻿using Namku.Api.Auth;
using Namku.Api.Face;
using Newtonsoft.Json;
using System;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NamkuFaceApiTest
{
    class NamkuFaceApi
    {
        private const string API_BASE_URI = "https://facial.biometrika.cl/";
        private const string DEFAULT_USER = "facial@biometrika.cl";
        private const string DEFAULT_PASSWORD = "$F4n=hh!WxgHakf-";

        private HttpClient webClient = null;
        private string accessToken = null;

        public NamkuFaceApi()
        {
            this.webClient = new HttpClient();
            this.webClient.BaseAddress = new Uri(API_BASE_URI);
            this.webClient.DefaultRequestHeaders.Accept.Clear();
        }
        public async Task<CompareResponse> Compare(Image source = null, Image target = null, CompareRequest json = null)
        {
            if (await this.LogIn())
            {
                string uri = "v2/recognition/compare";
                string response = await this.PutFilesAsync(uri, source, target, json);
                Console.WriteLine(response);
                return await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<CompareResponse>(response));
            }
            return null;
        }
        public async Task<ExtractResponse> Extract(Image img, bool onlyBiggestFace = false, bool onlyDetectFaces = false)
        {
            if(await this.LogIn())
            {
                string extractionType = onlyDetectFaces ? "face" : "all";
                string biggestFace = onlyBiggestFace ? "&biggest=1" : "";
                string uri = $"v2/recognition/extract?type={extractionType}{biggestFace}";

                string response = await this.PutFilesAsync(uri, source: img);
                Console.WriteLine(response);

                return await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<ExtractResponse>(response));
            }
            return null;
        }

        private async Task<string> PutFilesAsync(string endpoint, Image source = null, Image target = null, CompareRequest json = null)
        {
            using (var content = new MultipartFormDataContent())
            {
                if (source != null)
                {
                    content.Add(
                        new StreamContent(this.ImageToMemoryStream(source)),
                        "source",
                        "source"
                    );
                }
                if (target != null)
                {
                    content.Add(
                        new StreamContent(this.ImageToMemoryStream(target)),
                        "target",
                        "target"
                    );
                }
                if(json != null)
                {
                    var jsonString = await Task.Factory.StartNew(
                       () => JsonConvert.SerializeObject(
                           json,
                           Formatting.Indented,
                           new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }
                       )
                    );
                    byte[] byteArray = Encoding.ASCII.GetBytes(jsonString);
                    MemoryStream stream = new MemoryStream(byteArray);
                    content.Add(
                        new StreamContent(stream),
                        "json",
                        "json"
                    );
                }

                this.webClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(
                    "Bearer",
                    this.accessToken
                );
                using (var message = await this.webClient.PostAsync(endpoint, content))
                {
                    var response = await message.Content.ReadAsStringAsync();
                    return response;
                }
            }
        }
    
        private MemoryStream ImageToMemoryStream(Image img)
        {
            ImageConverter converter = new ImageConverter();
            byte[] imageStream = (byte[])converter.ConvertTo(img, typeof(byte[]));
            return new MemoryStream(imageStream);
        }

        private async Task<bool> LogIn()
        {
            if(this.accessToken != null)
            {
                var token = new JwtSecurityTokenHandler().ReadJwtToken(this.accessToken);
                if (token.ValidTo > DateTime.Now)
                    return true;
            }
            byte[] authBase64 = Encoding.ASCII.GetBytes(DEFAULT_USER + ":" + DEFAULT_PASSWORD);
            this.webClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(authBase64));
            HttpResponseMessage response = await this.webClient.PostAsync("v2/auth/token", null);
            HttpContent content = response.Content;
            string result = await content.ReadAsStringAsync();
            if (result != null && (int)response.StatusCode == 200)
            {
                var authData = await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<AuthResponse>(result));
                this.accessToken = authData.Data.AccessToken;
                return true;
            }
            else
            {
                Console.WriteLine(result);
                return false;
            }
        }
    }
}
