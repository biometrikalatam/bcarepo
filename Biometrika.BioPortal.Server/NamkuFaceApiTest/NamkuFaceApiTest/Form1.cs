﻿using Namku.Api;
using Namku.Api.Face;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NamkuFaceApiTest
{
    public partial class Form1 : Form
    {
        private Image sourceOriginalImage = null;
        private Image targetOriginalImage = null;
        public Form1()
        {
            InitializeComponent();
            this.apiResponseTextBox.ScrollBars = ScrollBars.Vertical;
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {
            PictureBox senderPictureBox = (PictureBox)sender;
            if (senderPictureBox.Image == null)
            {
                if (this.openImageDialog.ShowDialog() == DialogResult.OK)
                {
                    setOriginalImage(senderPictureBox, new Bitmap(this.openImageDialog.FileName));
                    senderPictureBox.Image = new Bitmap(this.openImageDialog.FileName);
                }
            }
            else
            {
                setOriginalImage(senderPictureBox, null);
                senderPictureBox.Image = null;
            }
        }
        private void setOriginalImage(PictureBox pb, Image img)
        {
            if (this.sourcePictureBox == pb)
            {
                this.sourceOriginalImage = img;
            }
            else
            {
                this.targetOriginalImage = img;
            }
        }

        private async void compareOnClick(object sender, EventArgs e)
        {
            this.apiResponseTextBox.Text = "Procesando...";
            try
            {
                CompareRequest json = null;
                if (!string.IsNullOrEmpty(this.jsonFilePathTextBox.Text))
                {
                    json = await Task.Factory.StartNew(
                        () => JsonConvert.DeserializeObject<CompareRequest>(File.ReadAllText(this.jsonFilePathTextBox.Text))
                    );
                }
                CompareResponse response = await new NamkuFaceApi().Compare(
                    this.sourceOriginalImage,
                    this.targetOriginalImage,
                    json
                );
                this.apiResponseTextBox.Text = await Task.Factory.StartNew(
                    () => JsonConvert.SerializeObject(
                        response,
                        Formatting.Indented,
                        new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }
                    )
                );
                if (response.Data != null)
                {
                    if (this.sourceOriginalImage != null && response.Data.Source != null)
                    {
                        this.sourcePictureBox.Image = this.DrawBoundingBoxs(
                            new Bitmap(this.sourceOriginalImage),
                            new BoundingBox[] { response.Data.Source.BoundingBox }
                        );
                    }
                    if (this.targetOriginalImage != null && response.Data.Target != null)
                    {
                        this.targetPictureBox.Image = this.DrawBoundingBoxs(
                            new Bitmap(this.targetOriginalImage),
                            new BoundingBox[] { response.Data.Target.BoundingBox }
                        );
                    }
                }
            }
            catch (ArgumentException)
            {
                this.apiResponseTextBox.Text = "La ruta del json se encuentra vacia...";
            }
            catch (JsonReaderException)
            {
                this.apiResponseTextBox.Text = "El formato del JSON es incorrecto...";
            }
            catch (JsonSerializationException)
            {
                this.apiResponseTextBox.Text = "El formato del JSON es incorrecto...";
            }
        }
        private async void extractOnClick(object sender, EventArgs e)
        {
            this.apiResponseTextBox.Text = "Procesando...";
            ExtractResponse response = await new NamkuFaceApi().Extract(
                this.sourcePictureBox.Image,
                onlyBiggestFace: this.biggestFaceCheckBox.Checked,
                onlyDetectFaces: this.onlyFaceCheckbox.Checked
            );

            this.apiResponseTextBox.Text = await Task.Factory.StartNew(
                () => JsonConvert.SerializeObject(
                    response, 
                    Formatting.Indented,
                    new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }
                )
            );
            if(response.Data != null)
                this.sourcePictureBox.Image = this.DrawBoundingBoxs(new Bitmap(this.sourceOriginalImage), from x in response.Data select x.BoundingBox);
        }
        private Image DrawBoundingBoxs(Image img, IEnumerable<BoundingBox> boundingBoxs)
        {
            using (Graphics graphics = Graphics.FromImage(img))
            {
                float penWidth = Math.Max(
                    1.0f, 
                    (1.0f / 100) * Math.Min(
                        img.Size.Width, 
                        img.Size.Height
                    )
                );
                Console.WriteLine($"Tamaño de imagen: {img.Size}");
                Console.WriteLine($"Ancho de pincel usado: {penWidth}");
                Pen pen = new Pen(Color.DeepSkyBlue, penWidth);
                graphics.DrawRectangles(
                    pen,
                    new List<Rectangle>(
                        from box in boundingBoxs select new Rectangle(box.Left, box.Top, box.Width, box.Height)
                    ).ToArray()
                );
            }
            return img;
        }

        private void searchJsonOnClick(object sender, EventArgs e)
        {
            if (this.openJsonDialog.ShowDialog() == DialogResult.OK)
            {
                this.jsonFilePathTextBox.Text = this.openJsonDialog.FileName;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
