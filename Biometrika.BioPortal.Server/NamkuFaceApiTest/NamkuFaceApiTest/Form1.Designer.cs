﻿namespace NamkuFaceApiTest
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.targetPictureBox = new System.Windows.Forms.PictureBox();
            this.sourcePictureBox = new System.Windows.Forms.PictureBox();
            this.openImageDialog = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.apiResponseTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.onlyFaceCheckbox = new System.Windows.Forms.CheckBox();
            this.biggestFaceCheckBox = new System.Windows.Forms.CheckBox();
            this.jsonFilePathTextBox = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.openJsonDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.targetPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourcePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(368, 191);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "VS";
            // 
            // targetPictureBox
            // 
            this.targetPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.targetPictureBox.Location = new System.Drawing.Point(397, 28);
            this.targetPictureBox.Name = "targetPictureBox";
            this.targetPictureBox.Size = new System.Drawing.Size(350, 350);
            this.targetPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.targetPictureBox.TabIndex = 5;
            this.targetPictureBox.TabStop = false;
            this.targetPictureBox.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // sourcePictureBox
            // 
            this.sourcePictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sourcePictureBox.Location = new System.Drawing.Point(12, 28);
            this.sourcePictureBox.Name = "sourcePictureBox";
            this.sourcePictureBox.Size = new System.Drawing.Size(350, 350);
            this.sourcePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.sourcePictureBox.TabIndex = 6;
            this.sourcePictureBox.TabStop = false;
            this.sourcePictureBox.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // openImageDialog
            // 
            this.openImageDialog.FileName = "openImageDialog";
            this.openImageDialog.Filter = "Imágenes|*.jpg;*.png";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(485, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Haz click en un cuadro para cargar una imagen o para limpiar el cuadro en caso de" +
    " tener una imagen";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(147, 381);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "SOURCE";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(545, 381);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "TARGET";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(889, 415);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Extract (Source)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.extractOnClick);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(989, 415);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Compare";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.compareOnClick);
            // 
            // apiResponseTextBox
            // 
            this.apiResponseTextBox.Location = new System.Drawing.Point(756, 44);
            this.apiResponseTextBox.Multiline = true;
            this.apiResponseTextBox.Name = "apiResponseTextBox";
            this.apiResponseTextBox.ReadOnly = true;
            this.apiResponseTextBox.Size = new System.Drawing.Size(327, 334);
            this.apiResponseTextBox.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(753, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Respuesta API";
            // 
            // onlyFaceCheckbox
            // 
            this.onlyFaceCheckbox.AutoSize = true;
            this.onlyFaceCheckbox.Location = new System.Drawing.Point(12, 421);
            this.onlyFaceCheckbox.Name = "onlyFaceCheckbox";
            this.onlyFaceCheckbox.Size = new System.Drawing.Size(286, 17);
            this.onlyFaceCheckbox.TabIndex = 12;
            this.onlyFaceCheckbox.Text = "Extract: Solo detectar caras (no extrae los embeddings)";
            this.onlyFaceCheckbox.UseVisualStyleBackColor = true;
            // 
            // biggestFaceCheckBox
            // 
            this.biggestFaceCheckBox.AutoSize = true;
            this.biggestFaceCheckBox.Location = new System.Drawing.Point(12, 398);
            this.biggestFaceCheckBox.Name = "biggestFaceCheckBox";
            this.biggestFaceCheckBox.Size = new System.Drawing.Size(179, 17);
            this.biggestFaceCheckBox.TabIndex = 12;
            this.biggestFaceCheckBox.Text = "Extract: Solo la cara más grande";
            this.biggestFaceCheckBox.UseVisualStyleBackColor = true;
            // 
            // jsonFilePathTextBox
            // 
            this.jsonFilePathTextBox.Location = new System.Drawing.Point(397, 418);
            this.jsonFilePathTextBox.Name = "jsonFilePathTextBox";
            this.jsonFilePathTextBox.Size = new System.Drawing.Size(251, 20);
            this.jsonFilePathTextBox.TabIndex = 13;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(654, 416);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(93, 23);
            this.button3.TabIndex = 14;
            this.button3.Text = "Buscar JSON";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.searchJsonOnClick);
            // 
            // openJsonDialog
            // 
            this.openJsonDialog.Filter = "Archivo JSON (/.json) | *.json";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1098, 450);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.jsonFilePathTextBox);
            this.Controls.Add(this.biggestFaceCheckBox);
            this.Controls.Add(this.onlyFaceCheckbox);
            this.Controls.Add(this.apiResponseTextBox);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.targetPictureBox);
            this.Controls.Add(this.sourcePictureBox);
            this.Name = "Form1";
            this.Text = "Namku Face Api Test App";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.targetPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourcePictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox targetPictureBox;
        private System.Windows.Forms.PictureBox sourcePictureBox;
        private System.Windows.Forms.OpenFileDialog openImageDialog;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox apiResponseTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox onlyFaceCheckbox;
        private System.Windows.Forms.CheckBox biggestFaceCheckBox;
        private System.Windows.Forms.TextBox jsonFilePathTextBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.OpenFileDialog openJsonDialog;
    }
}

