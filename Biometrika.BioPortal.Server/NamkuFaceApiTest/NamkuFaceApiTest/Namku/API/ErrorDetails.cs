﻿using Newtonsoft.Json;

namespace Namku.Api
{
    public class ErrorDetails
    {
        [JsonProperty("domain")]
        public string Domain { get; set; }
        [JsonProperty("reason")]
        public string Reason { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}