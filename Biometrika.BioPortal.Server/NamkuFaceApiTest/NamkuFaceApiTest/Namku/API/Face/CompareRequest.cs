﻿using Namku.API.Face;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Namku.Api.Face
{
    internal class CompareRequest
    {
        [JsonProperty("source")]
        public CompareRequestFaceObject Source { get; set; }
        [JsonProperty("target")]
        public List<CompareRequestFaceObject> Target { get; set; }
    }
}