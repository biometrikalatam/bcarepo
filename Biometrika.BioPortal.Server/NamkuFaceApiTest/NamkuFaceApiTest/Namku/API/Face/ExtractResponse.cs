﻿using Namku.Api;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Namku.Api.Face
{
    public class ExtractResponse
    {
        [JsonProperty("api_version")]
        public string ApiVersion { get; set; }
        [JsonProperty("error")]
        public Error Error { get; set; }
        [JsonProperty("data")]
        public List<ExtractResponseData> Data { get; set; }
    }
}