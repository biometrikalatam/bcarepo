﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Namku.Api.Face
{
    public class CompareResponseData
    {

        [JsonProperty("source")]
        public ExtractResponseData Source { get; set; }
        [JsonProperty("target")]
        public ExtractResponseData Target { get; set; }
        [JsonProperty("closest")]
        public CompareResponseFacesDistance Closest { get; set; }
        [JsonProperty("distances")]
        public List<CompareResponseFacesDistance> Distances { get; set; }
    }
}