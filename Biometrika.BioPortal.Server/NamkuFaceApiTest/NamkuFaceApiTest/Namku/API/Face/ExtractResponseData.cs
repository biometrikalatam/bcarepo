﻿using Namku.Api;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Namku.Api.Face
{
    public class ExtractResponseData
    {
        [JsonProperty("bounding_box")]
        public BoundingBox BoundingBox { get; set; }
        [JsonProperty("embeddings")]
        public List<float> Embeddings { get; set; }
    }
}