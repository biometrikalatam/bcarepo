﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Namku.API.Face
{
    public class CompareRequestFaceObject
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("embeddings")]
        public float[] Embeddings { get; set; }
    }
}
