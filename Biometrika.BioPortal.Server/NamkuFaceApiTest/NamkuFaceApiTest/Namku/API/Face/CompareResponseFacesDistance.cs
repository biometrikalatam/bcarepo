﻿using Newtonsoft.Json;

namespace Namku.Api.Face
{
    public class CompareResponseFacesDistance
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("distance")]
        public float Distance { get; set; }
    }
}