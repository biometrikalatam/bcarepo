﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bio.Core.Matcher.Connector.NecDMA
{
    class NecDataToCompare
    {
        public string rawData { get; set; }
        public string necTemplate { get; set; }
    }
}
