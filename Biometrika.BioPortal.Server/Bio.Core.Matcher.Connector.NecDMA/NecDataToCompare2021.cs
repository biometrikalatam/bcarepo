﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bio.Core.Matcher.Connector.NecDMA
{
    class NecDataToCompare2021
    {
        //Puede llegar RAW o WSQ
        public int minutiaeTypeSample { get; set; }
        public string sample { get; set; }

        //Puede llegar Minucia extraida desde PDF417 o el código de barras completo
        public int minutiaeTypeNecData { get; set; }
        public string necData { get; set; }
    }
}
