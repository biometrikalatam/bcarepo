﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Bio.Core.Api;
using Bio.Core.Api.Matcher.Interface;
using Bio.Core.Constant;
using Bio.Core.Wsq.Encoder;
using BioPortal.Server.Api;
using log4net;
using RestSharp;
using System.Drawing.Imaging;
using System.Drawing;
using Bio.Core.Imaging;
using System.Drawing.Drawing2D;

namespace Bio.Core.Matcher.Connector.NecDMA
{
    public class Connector : IConnector
    {
                private static readonly ILog LOG = LogManager.GetLogger(typeof(Connector));

#region Private properties propietario

        //private NECPID.Main oNEC = new NECPID.Main();
        //private Biometrika.NEC.BiometrikaNEC oBKNec;
        private byte[] _rawGenerated;
        private byte[] _wsqGenerated;
        private byte[] _PC1Generated;
        private byte[] _PC1Cedula;
        private int _pcLen;
        private byte[] _pdf417Cedula;
        private string _pdf417;

        private string _trackIdExternal;



        /// <summary>
        /// Indica que tipo de conector DMA usa
        ///      0 - Interno al BioPortal con COM
        ///      1 - Interno al BioPortal con SDK NEW
        ///      2 - DMA NEC WS
        /// </summary>
        private int _typeConnector = 0;

        //RUN _currenRUN;

        private string _urlWS = "http://localhost:1337/nec/match"; //"http://localhost:8500/Biometrika.DMA.WS.asmx";
        private int _timeout = 30000;
        //private int _idEmpresaSRCeI = 61003000;
        //private string _userSRCeI = "usr_medleg";
        //private string _claveSRCeI = "H%E1K!9&rC2U";
        private string _ip;
        private int _threshold = 1000;

        private int _reintentos = 3;  //Cantidad de transacciones en servicio local nuestro antes de consumir 
                                      //el servicio de BioID. 
                                      //    Si es 0 => Todos con servicio nuestro
                                      //    Si es -1 => Todos BioID
        private int _CONTADOR_REINTENTOS = 3; //Sirve para controlar la cantidad para hacer la rotacion, si _reintentos > 0
        private string _urlWS2021 = "http://localhost:1445/nec/match";
        private bool _isConfigured; //Solo para control en initialization
        private string _bioIdClient = "1023";
        private string _bioIdUser = "bioka";
        private string _bioIdPsw = "biokatest";
        //private BidCedChk.BidCedChk _BIOID;

        //private string _pvkey;
        //private string _pathcertifcatekey;
        //private string _certifcatekey;

        #endregion Private properties propietario

        #region Public properties propietario

        //public string PathcertifySRCeI
        //{
        //    get { return _pathcertifySRCeI; }
        //    set { _pathcertifySRCeI = value;
        //          byte[] _certifySRCeI = File.ReadAllBytes(this._pathcertifySRCeI);
        //    }
        //}

        //public string PathCertifcatekey
        //{
        //    get { return _pathcertifcatekey; }
        //    set { _pathcertifcatekey = value;
        //          _certifcatekey = ReadFromHDD(this._pathcertifcatekey);
        //    }
        //}

        //public string PathPvkey
        //{
        //    get { return _pathpvkey; }
        //    set { _pathpvkey = value;
        //          _pvkey = ReadFromHDD(this._pathpvkey);
        //    }
        //}

        public string UrlWs
        {
            get { return _urlWS; }
            set { _urlWS = value; }
        }

        public int Reintentos
        {
            get { return _reintentos; }
            set { _reintentos = value; }
        }

        public string UrlWs2021
        {
            get { return _urlWS2021; }
            set { _urlWS2021 = value; }
        }

        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        //public int IdEmpresaSRCeI
        //{
        //    get { return _idEmpresaSRCeI; }
        //    set { _idEmpresaSRCeI = value; }
        //}

        //public string UserSRCeI
        //{
        //    get { return _userSRCeI; }
        //    set { _userSRCeI = value; }
        //}

        //public string ClaveSRCeI
        //{
        //    get { return _claveSRCeI; }
        //    set { _claveSRCeI = value; }
        //}

        /// <summary>
        /// Indica que tipo de conector DMA usa
        ///      0 - Interno al BioPortal con COM
        ///      1 - Interno al BioPortal con SDK NEW
        ///      2 - DMA NEC WS
        /// </summary>
        public int TypeConnector
        {
            get { return _typeConnector; }
            set { _typeConnector = value; }
        }

        public int Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

#endregion Public properties propietario
         
#region Private Method

        /// <summary>
        /// Inicializa las variables del objeto desde config, para no perder tiempo en buquedas luego
        /// </summary>
        private void Initialization()
        {
            try
            {
                if (_config == null || _config.DynamicDataItems == null)
                {
                    _isConfigured = false;
                    return;
                }

                foreach (DynamicDataItem dd in _config.DynamicDataItems)
                {
                    if (dd.key.Trim().Equals("Reintentos"))
                    {
                        try
                        {
                            _reintentos = Convert.ToInt32(dd.value.Trim());
                            LOG.Debug("Bio.Core.MatcherNecDMA - Set reintentos a " + _reintentos.ToString() + "...");
                        }
                        catch (Exception ex)
                        {
                            _reintentos = 3;
                        }
                    }
                    if (dd.key.Trim().Equals("UrlWS2021"))
                    {
                        _urlWS2021 = dd.value.Trim();
                        LOG.Debug("Bio.Core.MatcherNecDMA - Set _urlWS2021 = " + _urlWS2021);
                    }
                    if (dd.key.Trim().Equals("UrlWS"))
                    {
                        _urlWS = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("Timeout"))
                    {
                        try
                        {
                            _timeout = Convert.ToInt32(dd.value.Trim());
                        }
                        catch (Exception ex)
                        {
                            _timeout = 30000;
                        }
                    }
                    //if (dd.key.Trim().Equals("IdEmpresaSRCeI"))
                    //{
                    //    _idEmpresaSRCeI = Convert.ToInt32(dd.value.Trim());
                    //}
                    //if (dd.key.Trim().Equals("UserSRCeI"))
                    //{
                    //    _userSRCeI = dd.value.Trim();
                    //}
                    //if (dd.key.Trim().Equals("ClaveSRCeI"))
                    //{
                    //    _claveSRCeI = dd.value.Trim();
                    //}
                    if (dd.key.Trim().Equals("Threshold"))
                    {
                        _threshold = Convert.ToInt32(dd.value.Trim());
                        LOG.Debug("Bio.Core.MatcherNecDMA - Set _threshold = " + _threshold.ToString());
                    }

                    if (dd.key.Trim().Equals("BioIDUSer"))
                    {
                        _bioIdUser = dd.value.Trim();
                        LOG.Debug("Bio.Core.MatcherNecDMA - Set _bioIdUser = " + _bioIdUser);
                    }
                    if (dd.key.Trim().Equals("BioIDPsw"))
                    {
                        _bioIdPsw = dd.value.Trim();
                        LOG.Debug("Bio.Core.MatcherNecDMA - Set _bioIdPsw = " + _bioIdPsw);
                    }
                    if (dd.key.Trim().Equals("BioIDClient"))
                    {
                        _bioIdClient = dd.value.Trim();
                        LOG.Debug("Bio.Core.MatcherNecDMA - Set _bioIdClient = " + _bioIdClient);
                    }

                    if (dd.key.Trim().Equals("TypeConnector"))
                    {
                        _typeConnector = Convert.ToInt32(dd.value.Trim());
                        if (_typeConnector == 3) //=> si es 3 => es BioID, inicio sesion para tener 
                        {
                            //_BIOID = new BidCedChk.BidCedChk();
                            //string resp = _BIOID.SetCliente(_bioIdClient, _bioIdUser, _bioIdPsw, "SC");
                            //LOG.Debug("Bio.Core.Matcher.ConnectorNecDMA Initialization _BIOID.SetCliente = " + resp);
                        }
                    }

                }
                //_ip = BioVerify2015.Utiles.GetIP();
                _isConfigured = true;
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.ConnectorNecDMA Initialization Error", ex);
                _isConfigured = false;
            }
        }

        /// <summary>
        /// Hace llamado al servicio anterior con NEC propio
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="minutiae"></param>
        /// <returns></returns>
        private int CallServiceRestSharp(string raw, string minutiae)
        {
            LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp IN...");
            var uri = new Uri(_urlWS);
            var host = uri.GetLeftPart(System.UriPartial.Authority);
            var verb = uri.ToString().Replace(host.ToString(), String.Empty);

            //Objeto nec con el que se realiza la comparación
            LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp - Setting parametert RAW + Minutiae...");
            var necDataToCompare = new NecDataToCompare()
            {
                rawData = raw,
                necTemplate = minutiae
            };

            LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp - Creando client...");
            var client = new RestClient(host);
            var request = new RestRequest(verb, Method.POST);
            request.RequestFormat = RestSharp.DataFormat.Json;
            LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp - Adding parameter in body...");
            request.AddBody(necDataToCompare);
            try
            {
                LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp - Calling client.Execute...");
                var respuesta = client.Execute(request);
                if (respuesta != null)
                {
                    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp - respuesta != null..."); 
                    if (!string.IsNullOrEmpty(respuesta.Content))
                    {
                        LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp - respuesta.Content = " + 
                                   respuesta.Content);
                        return Convert.ToInt32(respuesta.Content);
                    } else
                    {
                        LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp - respuesta.Content = -1");
                        return -1;
                    }
                } else
                {
                    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp - respuesta = NULL");
                    return -1;
                }
            }
            catch(Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp Error - " + ex.Message);
                return -1;
            }
        }

        /// <summary>
        /// Hace llamado al servicio nuevo via BioID, con servicio hecho en 06/2021
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="minutiae"></param>
        /// <returns></returns>
        private int CallServiceRestSharp2021(string raw, string pdf417)
        {
            LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp2021 IN...");
            var uri = new Uri(_urlWS2021);
            LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp2021 - URL = " + _urlWS2021);
            var host = uri.GetLeftPart(System.UriPartial.Authority);
            var verb = uri.ToString().Replace(host.ToString(), String.Empty);
            //Objeto nec con el que se realiza la comparación
            var necDataToCompare2021 = new NecDataToCompare2021()
            {
                minutiaeTypeSample = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW,
                sample = raw,
                minutiaeTypeNecData = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_PDF417CEDULA,
                necData = pdf417
            };
            LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp2021 - Creadno request...");
            var client = new RestClient(host);
            var request = new RestRequest(verb, Method.POST);
            request.RequestFormat = RestSharp.DataFormat.Json;
            request.AddBody(necDataToCompare2021);
            try
            {
                LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp2021 - Envio peticion a servicio...");
                var respuesta = client.Execute(request);
                LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp2021 - Respuesta = " + respuesta);
                return Convert.ToInt32(respuesta.Content);

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Connector.NecDMA.CallServiceRestSharp2021 Error - " + ex.Message);
                return -1;

            }
        }

        /*
        Nombre Tipo Descripción
            RUT         Numérico (8)    RUT de la Persona a verificar
            DV-RUT      String(1)       0 (si no interesa el dato)
            Office      Numérico (4)    0000 (si no interesa el dato) Código de oficina donde se realiza la operación
            Terminal    String(20)      Dirección IP de la terminal donde se realiza la operación 
            Channel     String(4)       0000 (si no interesa el dato) Código de canal (para organizaciones con varios canales de atención)
            Date        Numeric(8)      MMDDYYYY
            Hour        Numeric(8)      HHMMSSCC
            CUT         String(30)      Código único de transacción. 
        */
        private byte[] CreateHeader(string rut, out string idTxExteral)
        {
            LOG.Debug("CreateHeader In...");
            string rutmasdv = rut;
            if (rut.IndexOf("-") > 0)
            {
                rutmasdv = rut.Substring(0, rut.IndexOf("-")) + rut.Substring(rut.IndexOf("-") + 1);
            }
            string sdate = DateTime.Now.ToString("MMddyyyy");
            string shour = DateTime.Now.ToString("HHmmss00");
            //ID 2128441520E2C074D9D2D4BC980668
            string trackid = Guid.NewGuid().ToString("N").Substring(0, 30);
            idTxExteral = trackid;

            string sHeader = rutmasdv + "0000192.168.5.86        0000" + sdate + shour + trackid;
            LOG.Debug("CreateHeader sHeader = " + sHeader);
            LOG.Debug("CreateHeader Out!");
            return ASCIIEncoding.UTF8.GetBytes(sHeader);
        }

        private int ExtraeSamplesFromParamIn(XmlParamIn oXmlIn, out byte[] wsqGenerated, 
                                            out byte[] rawGenerated, out byte[] pc1Cedula, 
                                            out string pdf417,
                                            out int pclen, out string msg)
        {
            byte[] wsqr = null;
            int iErr = 0;
            rawGenerated = null;
            wsqGenerated = null;
            pc1Cedula = null;
            pclen = 0;
            pdf417 = null;
            msg = null;
            short w, h;
            try
            {
                LOG.Debug("ExtraeSamplesFromParamIn In...");

                //Proceso WSQ
                LOG.Debug("ExtraeSamplesFromParamIn oXmlIn.SampleCollection.Count = " + oXmlIn.SampleCollection.Count.ToString());
                if (oXmlIn.SampleCollection.Count != 2) return -2; //Deben ser dos smples, el wsq o raw y las minucias de la cedula.
//             _pdf417Cedula = Convert.FromBase64String(sAux[1].Trim());
                //Added 12-01 - Para servicio 
                if (oXmlIn.SampleCollection[1].Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_PDF417CEDULA)
                {
                    pdf417 = oXmlIn.SampleCollection[1].Data.Trim();
                    int ierr = ParsePDF417(oXmlIn.SampleCollection[1].Data.Trim(), out pc1Cedula, out pclen);
                    if (ierr != 0)
                    {
                        LOG.Error("Bio.Core.MatcherNecDMA ExtraeWSQFromParamIn Error Pareando PDF417 [" + ierr + "]. No prosigue verificación...");
                        return ierr;
                    } 
                }
                else  //es la minucia en si
                {
                    LOG.Debug("ExtraeSamplesFromParamIn pc1Cedula = " + oXmlIn.SampleCollection[1].Data.Trim());
                    pc1Cedula = Convert.FromBase64String(oXmlIn.SampleCollection[1].Data.Trim());
                    LOG.Debug("ExtraeSamplesFromParamIn pclen = " + oXmlIn.SampleCollection[1].Additionaldata.Trim());
                    pclen = Convert.ToInt32(oXmlIn.SampleCollection[1].Additionaldata.Trim());
                }

                //if (pc1Cedula.Length != pclen)
                //{
                //    byte[] byPc1New = new byte[pclen];
                //    Buffer.BlockCopy(pc1Cedula, 0, byPc1New, 0, pclen);
                //    pc1Cedula = byPc1New;
                //}

                if (oXmlIn.SampleCollection[0].Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                {
                    LOG.Debug("ExtraeSamplesFromParamIn Decompress WSQ In...");
                    wsqr = Convert.FromBase64String(oXmlIn.SampleCollection[0].Data.Trim());
                    wsqGenerated = wsqr;
                    Wsq.Decoder.WsqDecoder decoder = new Wsq.Decoder.WsqDecoder();
                    decoder.DecodeMemory(wsqr, out w, out h, out rawGenerated);
                    if (w != 512 || h !=512)
                    {
                        rawGenerated = FormatRAW(rawGenerated);
                    }
                    LOG.Debug("ExtraeSamplesFromParamIn Decompress WSQ Out!");
                }
                else if (oXmlIn.SampleCollection[0].Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW)
                {
                    LOG.Debug("ExtraeSamplesFromParamIn Compress RAW In...");
                    //rawGenerated = Convert.FromBase64String(oXmlIn.SampleCollection[0].Data.Trim());
                    rawGenerated = FormatRAW(Convert.FromBase64String(oXmlIn.SampleCollection[0].Data.Trim()));
                    Wsq.Encoder.WsqEncoder enc = new WsqEncoder();
                    enc.EncodeMemory(rawGenerated, 512, 512, out wsqGenerated);
                    LOG.Debug("ExtraeSamplesFromParamIn Compress RAW Out!");
                }
                LOG.Debug("ExtraeSamplesFromParamIn Out!");
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.MatcherNecDMA ExtraeWSQFromParamIn Error", ex);
            }
            return iErr;
        }

        /// <summary>
        /// Formatea RAW si no es de 512x512
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private byte[] FormatRAW(byte[] _rawInput)
        {
            byte[] _rawOutput = null;
            byte[] _rawOutput512x512 = null;
            int wRaw = 0;
            int hRaw = 0;
            try
            {
                LOG.Debug("Bio.Core.MatcherNecDMA FormatRAW IN...");

                //Si ya está en 512x512 => Solo asigno
                if (_rawInput.Length == 262144)  
                {
                    LOG.Debug("Bio.Core.MatcherNecDMA FormatRAW => Input 512x512 => no formatea...");
                    _rawOutput512x512 = _rawInput;
                } else { //Sino formateo
                      //Si tiene tamaño 357x392 => Solo de completa, sino se transforma. 
                    if (_rawInput.Length == 139944) 
                    {
                        LOG.Debug("Bio.Core.MatcherNecDMA FormatRAW Len 357x392...");
                        wRaw = 357;
                        hRaw = 392;
                        _rawOutput = _rawInput;
                    }

                    if (_rawInput.Length == 275050 || _rawInput.Length == 275000) 
                    {
                        //rawIn = new byte[275000];
                        if (_rawInput.Length == 275050)
                        {
                            LOG.Debug("Bio.Core.MatcherNecDMA FormatRAW Len 275500 DP Ansi...");
                            for (int i = 50; i < _rawInput.Length; i++)
                            {
                                _rawInput[i - 50] = _rawInput[i];
                            }
                        }
                        else
                        {
                            LOG.Debug("Bio.Core.MatcherNecDMA FormatRAW Len 275000 DP...");
                            _rawOutput = _rawInput;
                        }
                        _rawInput = ConvertImageDPRawTo357x392Raw(_rawInput, false, 500, 550); 
                        wRaw = 357;
                        hRaw = 392;
                    }

                    //Secugen Hamster Pro 20 => 300x400
                    if (_rawInput.Length == 120000)
                    {
                        LOG.Debug("Bio.Core.MatcherNecDMA FormatRAW Len 300x400 Sec Pro 20...");
                        wRaw = 300;
                        hRaw = 400;
                        _rawOutput = _rawInput;
                    }

                    //Secugen Hamster Pro => 260x300 o Secugen Hamster Plus
                    if (_rawInput.Length == 78000)
                    {
                        LOG.Debug("Bio.Core.MatcherNecDMA FormatRAW Len 260x300 Sec Hamster Plus o Sec Hamster Pro...");
                        wRaw = 260;
                        hRaw = 300;
                        _rawOutput = _rawInput;
                    }

                    //Secugen Hamster IV => 258x336
                    if (_rawInput.Length == 86688)
                    {
                        LOG.Debug("Bio.Core.MatcherNecDMA FormatRAW Len 258x336 Sec Hamster IV...");
                        wRaw = 258;
                        hRaw = 336;
                        _rawOutput = _rawInput;
                    }

                    //Dispo Multicaja
                    if(_rawInput.Length == 92160)
                    {
                        LOG.Debug("Bio.Core.MatcherNecDMA FormatRAW Len 256x360 Dispo Multicaja...");
                        wRaw = 256;
                        hRaw = 360;
                        _rawOutput = _rawInput;
                    }
                    _rawOutput512x512 = Bio.Core.Imaging.ImageProcessor.FillRaw(_rawInput, wRaw, hRaw, 512, 512);
                }
                LOG.Debug("Bio.Core.MatcherNecDMA FormatRAW FillRaw OK => " + Convert.ToBase64String(_rawOutput512x512));
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.MatcherNecDMA FormatRAW Error", ex);
                _rawOutput512x512 = null;
            }
            LOG.Debug("Bio.Core.MatcherNecDMA FormatRAW OUT!");
            return _rawOutput512x512;
        }

        internal static byte[] ConvertImageDPRawTo357x392Raw(byte[] imageDPRaw, bool wsqCompressed, int wRaw, int hRaw)
        {
            //Los parametros wRaw y hRaw vienen seteados si compressed es false, 
            //sino se calculan dentro de WsqTools.Decompress
            int w = 0;
            int h = 0;
            int dpix = 500;
            int dpiy = 500;
            int ww = 0; //Convert.ToInt32(pSample.Height);
            int hh = 0; //Convert.ToInt32(pSample.Width);
            ww = 357; //(ww * dpix) / sensor.Xdpi;
            hh = 392; //(hh * dpiy) / sensor.Ydpi;
            byte[] byDataRawD;

            try
            {
                LOG.Debug("Bio.Core.MatcherNecDMA ConvertImageDPRawTo357x392Raw IN...");
                //Si es Raw comprimido, descomprimo
                if (wsqCompressed)
                {
                    Wsq.Decoder.WsqDecoder dec = new Wsq.Decoder.WsqDecoder();
                    short sw = 0;
                    short sh = 0;
                    if (!dec.DecodeMemory(imageDPRaw, out sw, out sh, out byDataRawD))
                    {
                        return null;
                    }
                    else
                    {
                        w = sw;
                        h = sh;
                    }
                }
                else
                {
                    byDataRawD = imageDPRaw;
                    w = wRaw;
                    h = hRaw;
                }

                //Creo Bitmap para redimensionar a tamaño comun para comparaciones
                Bitmap img = ImageProcessor.RawToBitmap(byDataRawD, w, h);
                Bitmap bmp = new Bitmap(ww, hh, PixelFormat.Format24bppRgb);
                bmp.SetResolution(dpix, dpiy);
                Graphics gr = Graphics.FromImage(bmp);
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.DrawImage(img, new Rectangle(0, 0, ww, hh), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel);
                gr.Dispose();
                byte[] imgData512 = ImageProcessor.BitmapToRaw(bmp);
                //byte[] imgData512 = ImageProcessor.Adjust(imgData, ww, hh, 512, 512);

                LOG.Error("BiometrikaConverter.ConvertImageDPRawTo357x392Raw OUT!");
                return imgData512;
            }
            catch (Exception ex)
            {
                LOG.Error("BiometrikaConverter.ConvertImageDPRawTo357x392Raw Error - " + ex.Message);
                return null;
            }
        }

#endregion Private Method

#region Implementation of IConnector

        private string _connectorId;

        private DynamicData _config;

        /// <summary>
        /// Id identificador del conector
        /// </summary>
        public string ConnectorId
        {
            get { return _connectorId; }
            set { _connectorId = value; }
        }

        /// <summary>
        /// Pares de key/value de configuracion para el conector
        /// </summary>
        public DynamicData Config
        {
            get { return _config; }
            set
            {
                 _config = value;
                 if (!_isConfigured) Initialization();
            }
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */
            int _ret = Errors.IERR_OK;
            string msg;
            string xmloutbp;
            int iretremoto;
            int _currentThreshold;


            LOG.Debug("Verify In...");
            DynamicData oXmlout = new DynamicData();
            oXmlout.AddValue("message", "");
            oXmlout.AddValue("trackid", "");
            oXmlout.AddValue("status", "");
            oXmlout.AddValue("result", "");
            oXmlout.AddValue("score", "");
            oXmlout.AddValue("threshold", "");
            oXmlout.AddValue("timestamp", "");
            //Added 02-2015 para agregar todas las BpTxConx cuando son varias
            /*
                 Consultationtype > string > nombre metodo
                 Connectorid > string
                 Trackid > string > Trackid externa
                 Status > int > Retorno de servicio 
                 Result > int > 1-Verify Positivo | 2-Verify Negativo (Si es verify si es get 0)
                 Score > double > Score obtenido (Si es verify si es get 0)
                 Threshold > double > Umbral utilizado (Si es verify si es get 0)
                 Timestamp > string > fecha y hora tx externa
                Devuelve un Dynamicdata de la forma:
                    <DynamicData>
                        <DynamicDataItem>
                            <key>tx1</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                      .........  
                        <DynamicDataItem>
                            <key>txN</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                   </DynamicData>
             */
            DynamicData oExternalTxs = new DynamicData();
            oXmlout.AddValue("externaltxs", "");  //Tipo DynamicData

            //oXmlout.SetValue("message", "");
            //oXmlout.SetValue("trackid", "283734647");
            //oXmlout.SetValue("status", "0");
            //oXmlout.SetValue("result", "1");
            //oXmlout.SetValue("score", "1000");
            //oXmlout.SetValue("threshold", _threshold.ToString());
            //oXmlout.SetValue("timestamp", FormatFechaHoraSRCeI("23112013100000"));
            //xmloutput = DynamicData.SerializeToXml(oXmlout);
            //return Errors.IERR_OK;

            try
            {
                LOG.Debug("Bio.Core.MatcherNecDMA - Ingresando...");
                if (!this._isConfigured)
                {
                    msg = "Bio.Core.MatcherNecDMA Error = Connector No Configurado!";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
                }
                else
                {
                    LOG.Debug("Verify Configured...");
                }


                //1.-Deserializo parametros de entrada
                xmlinput = xmlinput.Replace("&#x0;", "");
                LOG.Debug("Bio.Core.MatcherNecDMA - xmlinput = " + xmlinput);
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlinput);
                if (oXmlIn == null)
                {
                    msg = "Bio.Core.MatcherNecDMA Error deserealizando xmlinput";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    LOG.Debug("Verify oXmlIn OK...");
                }

                //Tomo el WSQ si es que viene
                LOG.Debug("Bio.Core.MatcherNecDMA - Extrayendo WSQ y MinNEC from xmlinput...");
                
                int iErr = ExtraeSamplesFromParamIn(oXmlIn, out _wsqGenerated, out _rawGenerated, out _PC1Cedula,
                                                    out _pdf417, out _pcLen, out msg);

                if (_rawGenerated == null || _rawGenerated.Length == 0 ||
                    _PC1Cedula == null || _PC1Cedula.Length == 0)
                {
                    msg = "Bio.Core.MatcherNecDMA.Connector - Error extracción de parametros [len sample = " +
                        _rawGenerated == null ? "null" : _rawGenerated.Length + " - len _PC1Cedula =" +
                        _PC1Cedula == null ? "null" : _PC1Cedula.Length + "]";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout); 
                    return Errors.IERR_VERIFY;
                }
                else
                {
                    LOG.Debug("Verify extract param OK...");
                }

                if (iErr == 0) //Sin error
                {
                    LOG.Debug("Bio.Core.MatcherNecDMA - (raw != null!) = " + (_rawGenerated!=null).ToString()); // Convert.ToBase64String(_rawGenerated));
                    LOG.Debug("Bio.Core.MatcherNecDMA - (_pdf417 != null!) = " + (_pdf417 != null).ToString());

                    _currentThreshold = (oXmlIn.Threshold > 0) ? (int)oXmlIn.Threshold : _threshold;
                    LOG.Debug("Bio.Core.MatcherNecDMA - _currentThreshold = " + _currentThreshold);

                    int _Score = 0;
                    //LOG.Debug("Bio.Core.MatcherNecDMA - _typeConnector = " + _typeConnector.ToString());

                    //Added 08-06-2021 para controlar cantidades de transacciones
                    if (_reintentos == -1)
                    {
                        _typeConnector = 3; //Fuerza por servicio de BioID 
                    } else if (_reintentos == 0)
                    {
                        _typeConnector = 1; //Fuerza por servicio de Biometrika 
                    } else
                    {
                        if (_CONTADOR_REINTENTOS == 0)
                        {
                            LOG.Debug("Bio.Core.MatcherNecDMA - Reset reintentos a " + _reintentos.ToString());
                            _CONTADOR_REINTENTOS = _reintentos; //Reseteo el contador
                            _typeConnector = 3; //Fuerza por servicio de BioID 
                        } else
                        {
                            LOG.Debug("Bio.Core.MatcherNecDMA - Reintentos -1..."); 
                            _CONTADOR_REINTENTOS--;
                            _typeConnector = 1; //Fuerza por servicio de Biometrika
                        }
                    }
                    LOG.Debug("Bio.Core.MatcherNecDMA - _typeConnector = " + _typeConnector.ToString());
                    switch (_typeConnector)
	                {
                        case 0:         //      0 - Interno al BioPortal con COM
                            //iErr = VerifyInternoCOM(_rawGenerated, _pdf417Cedula, out msg, out _Score);
                            //break;
                        case 1:         //      1 - Interno al BioPortal con SDK NEW
                            //TODO
                            //oBKNec = new Biometrika.NEC.BiometrikaNEC();
                            //iErr = VerifyInternoDLL(_rawGenerated, _pdf417Cedula, out msg, out _Score);
                            try
                            {
                                //using (Biometrika.DMA.WS.Biometrika_DMA_WS ws = new Biometrika.DMA.WS.Biometrika_DMA_WS())
                                //{
                                //    ws.Url = _urlWS;
                                //    ws.Timeout = _timeout;
                                //    iErr = ws.MatchingEx(oXmlIn.PersonalData.Valueid, Convert.ToBase64String(_PC1Cedula), 
                                //                         Convert.ToBase64String(_rawGenerated), 1, out msg);
                                //    _Score = iErr >= 0 ? iErr : 0;
                                LOG.Debug("Bio.Core.MatcherNecDMA - (raw != null!) = " + (_rawGenerated != null).ToString()); // Convert.ToBase64String(_rawGenerated));
                                LOG.Debug("Bio.Core.MatcherNecDMA - (_PC1Cedula != null!) = " + (_PC1Cedula != null).ToString()); // + Convert.ToBase64String(_PC1Cedula));

                                _Score = CallServiceRestSharp(Convert.ToBase64String(_rawGenerated), Convert.ToBase64String(_PC1Cedula));
                                LOG.Debug("Bio.Core.MatcherNecDMA - _Score Obtenido  = " + _Score.ToString());
                            } 
                            catch (Exception ex)
                            {
                                LOG.Error("Calling CallServiceRestSharp Ex - " + ex.Message);
                                iErr = -10000;  //Error consumiendo WS DMA
                            }

                            break;
                        case 2:         //      2 - DMA NEC WS
                            try
                            {
                                LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService In...");
                                using (DMANECTransactionManagerWS.TransactionManagerWSService ws = new DMANECTransactionManagerWS.TransactionManagerWSService())
                                {
                                    ws.Url = _urlWS;
                                    LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService URL = " + ws.Url);
                                    ws.Timeout = _timeout;
                                    LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService Timeout = " + ws.Timeout);
                                    DMANECTransactionManagerWS.TransactionIn TxIN = new DMANECTransactionManagerWS.TransactionIn();
                                    LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService oXmlIn.PersonalData.Valueid = " + oXmlIn.PersonalData.Valueid);
                                    byte[] byHeader = CreateHeader(oXmlIn.PersonalData.Valueid, out _trackIdExternal);
                                    LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService _trackIdExternal = " + _trackIdExternal);
                                    TxIN.header = byHeader;
                                    LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxIN.biometricData = " + Convert.ToBase64String(_wsqGenerated));
                                    TxIN.biometricData = _wsqGenerated;
                                    LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxIN.transactionType = FEW1");
                                    TxIN.transactionType = "FEW1";
                                    TxIN.workstations = 0;

                                    byte[] minutiaePC1 = null;
                                    DMANECTransactionManagerWS.TransactionOut TxOUT = ws.generateTransaction(TxIN);
                                    LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService ws.generateTransaction(TxIN) Out!");
                                    if (TxOUT != null)
                                    {
                                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxOut != null");
                                        if ((TxOUT.errorArray == null || TxOUT.errorArray.Length == 0) && (TxOUT.minutia != null))
                                        {
                                            //_Score = iErr >= 0 ? iErr : 0;
                                            minutiaePC1 = TxOUT.minutia;
                                            LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService minutiaePC1 = " + Convert.ToBase64String(minutiaePC1));
                                        }
                                        else
                                        {
                                            msg = "Bio.Core.MatcherNecDMA Error DMANECTransactionManagerWS.TransactionOut (FEW1) " +
                                                TxOUT.errorArray[0] + " - " + TxOUT.errorArray[1];
                                            LOG.Fatal(msg);
                                            oXmlout.SetValue("message", msg);
                                            xmloutput = DynamicData.SerializeToXml(oXmlout);
                                            return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                                        } 
                                    }
                                    else
                                    {
                                        msg = "Bio.Core.MatcherNecDMA Error DMANECTransactionManagerWS.TransactionOut (FEW1) null ";
                                        LOG.Fatal(msg);
                                        oXmlout.SetValue("message", msg);
                                        xmloutput = DynamicData.SerializeToXml(oXmlout);
                                        return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                                    }

                                        //Sigo solo si extrae bien
                                    if (minutiaePC1 != null) 
                                    {
                                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService In Compare...");
                                        byHeader = CreateHeader(oXmlIn.PersonalData.Valueid, out _trackIdExternal);
                                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService _trackIdExternal = " + _trackIdExternal); 
                                        TxIN.header = byHeader;
                                        TxIN.biometricData = minutiaePC1;
                                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxIN.biometricData = " + Convert.ToBase64String(TxIN.biometricData));
                                        TxIN.compareTo = _PC1Cedula; //ExtraePC1FromPDF417(_PC1Cedula);
                                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxIN.compareTo = " + Convert.ToBase64String(TxIN.compareTo));
                                        TxIN.transactionType = "VER11";
                                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxIN.transactionType = VER11");

                                        TxOUT = null;
                                        TxOUT = ws.generateTransaction(TxIN);
                                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService ws.generateTransaction(TxIN) Out!");
                                    }

                                    if (TxOUT != null)
                                    {
                                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxOut != null");
                                        if (TxOUT.errorArray == null || TxOUT.errorArray.Length == 0)
                                        {
                                           _Score = TxOUT.score;
                                           LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService Scrore = " + _Score.ToString());
                                           if (TxOUT.result.Equals("Coincidencia"))
                                           {
                                               iErr = 1;
                                               LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService (TxOUT.result == Coincidencia");
                                           }
                                           else
                                           {
                                               if (TxOUT.result.Equals("No Coincidencia"))
                                               {
                                                   iErr = 2;
                                                   LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService (TxOUT.result == No Coincidencia");
                                               }
                                               else
                                               {
                                                   iErr = -1;
                                                   LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService (TxOUT.result == " + TxOUT.result + ")");
                                               }
                                           }
                                        }
                                        else
                                        {
                                            msg = "Bio.Core.MatcherNecDMA Error DMANECTransactionManagerWS.generateTransaction (VER11) " +
                                                TxOUT.errorArray[0] + " - " + TxOUT.errorArray[1];
                                            LOG.Fatal(msg);
                                            oXmlout.SetValue("message", msg);
                                            xmloutput = DynamicData.SerializeToXml(oXmlout);
                                            return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                                        }

                                    }
                                    else
                                    {
                                        msg = "Bio.Core.MatcherNecDMA Error DMANECTransactionManagerWS.generateTransaction null (VER11) ";
                                        LOG.Fatal(msg);
                                        oXmlout.SetValue("message", msg);
                                        xmloutput = DynamicData.SerializeToXml(oXmlout);
                                        return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                                    }
                                }
                            } 
                            catch (Exception ex )
                            {
                                iErr = -10000;  //Error consumiendo WS DMA
                            }

                            break;
                        case 3:         //      3 - DMA via BioID    
                            try
                            {
                                LOG.Debug("Bio.Core.MatcherNecDMA - Calling a CallServiceRestSharp2021...");
                                _Score = CallServiceRestSharp2021(Convert.ToBase64String(_rawGenerated), _pdf417);
                                LOG.Debug("Bio.Core.MatcherNecDMA - _Score Obtenido  = " + _Score.ToString());

                                //BidCedChk.BidCedChk Bioid = new BidCedChk.BidCedChk();
                                //LOG.Debug("Calling Bioid.VerificaID...");
                                ////LOG.Debug("Calling Bioid.VerificaID - RAW to Image = " + Convert.ToBase64String(_rawGenerated));
                                //Image imageRAW = Bio.Core.Imaging.ImageProcessor.RawToBitmap(_rawGenerated, 512, 512);
                                //LOG.Debug("Calling Bioid.VerificaID - PDF417 = " + oXmlIn.SampleCollection[1].Data.Trim());
                                //string result = _BIOID.VerificaID(imageRAW, oXmlIn.SampleCollection[1].Data.Trim());
                                //LOG.Debug("Calling Bioid.VerificaID - Result = " + result);
                                //if (!string.IsNullOrEmpty(result) && result.StartsWith("ok"))
                                //{
                                //    _Score = Convert.ToInt32((result.Split(';')[1]));
                                //} else
                                //{
                                //    _Score = 0;
                                //}
                                //LOG.Debug("Calling Bioid.VerificaID Score = " + _Score.ToString());
                            }
                            catch (Exception ex)
                            {
                                LOG.Error("Calling Bioid.VerificaID Ex - " + ex.Message);
                                iErr = -10001;  //Error consumiendo DMA BioID
                            }

                            break;
                        default:
                            break;
	                }
 
                    if (iErr >= 0) // Error en verificacion
                    {
                        oXmlout.SetValue("message", "");
                        oXmlout.SetValue("trackid", _trackIdExternal);
                        oXmlout.SetValue("status", "0");
                        oXmlout.SetValue("result", _Score >= _currentThreshold ? "1" : "2");
                        oXmlout.SetValue("score", _Score.ToString());
                        oXmlout.SetValue("threshold", _currentThreshold.ToString());
                        string ts = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                        oXmlout.SetValue("timestamp", ts);
                        int res = (_Score >= _currentThreshold) ? 1 : 2;
                        //oExternalTxs.AddValue("txVerify", GetDDFormExternalTx("AFISImpresionDactilarDS64", response, res, _currentThreshold, Score));
                        
                        //oExternalTxs.AddValue("Consultationtype", "generateTransaction");
                        //oExternalTxs.AddValue("Connectorid", "DMANec");
                        //oExternalTxs.AddValue("Trackid", _trackIdExternal);
                        //oExternalTxs.AddValue("Status", "0");
                        //oExternalTxs.AddValue("Result",  _Score >= _currentThreshold ? "1" : "2");
                        //oExternalTxs.AddValue("Score", _Score.ToString());
                        //oExternalTxs.AddValue("Threshold",  _currentThreshold.ToString());
                        //oExternalTxs.AddValue("Timestamp", ts);

                        oExternalTxs.AddValue("VerifyDMA", GetDDFormExternalTx("generateTransaction", _trackIdExternal,
                                                                               res, _currentThreshold, _Score, ts));
                        string extxml = DynamicData.SerializeToXml(oExternalTxs);
                        oXmlout.SetValue("externaltxs", extxml);
                        LOG.Debug("Verify Out externaltxs = " + extxml);
                    }
                    else
                    {
                        if (iErr == -10000) {
                            msg = "Bio.Core.MatcherNecDMA.Connector - Respuesta Erronea del WS DMA - " +
                                    "Response Error [" + iErr + " - Excp WS]";
                        }
                        else
                        {
                            msg = "Bio.Core.MatcherNecDMA.Connector - Respuesta Erronea del DMA - " +
                                "Response Error [" + iErr + "]";
                        }
                        LOG.Fatal(msg);

                        oXmlout.SetValue("message", msg);
                        xmloutput = DynamicData.SerializeToXml(oXmlout);
                        return Errors.IERR_VERIFY;
                    }
                }
                else
                {
                    msg = "Bio.Core.MatcherNecDMA.Connector Error extrayendo muestras [" + iErr + "]";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
           
                xmloutput = DynamicData.SerializeToXml(oXmlout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.MatcherNecDMA.Verify Error [" + ex.Message + "]";
                LOG.Error("Bio.Core.Matcher.MatcherNecDMA.Verify Error", ex);
                oXmlout.SetValue("message", msg);
                xmloutput = DynamicData.SerializeToXml(oXmlout);
                return Errors.IERR_UNKNOWN;
            }
            LOG.Debug("Verify Out!");
            return _ret;
        }

        private string GetDDFormExternalTx(string method, string trackid,
                                          int result, int th, double score, string ts)
        {
            string _ret = "";
            try
            {
                LOG.Debug("GetDDFormExternalTx In...");
                DynamicData ddTx = new DynamicData();
                ddTx.AddValue("Consultationtype", method);
                ddTx.AddValue("Connectorid", "DMANec");
                ddTx.AddValue("Trackid", trackid);
                ddTx.AddValue("Status", "0");
                ddTx.AddValue("Result", result.ToString());
                ddTx.AddValue("Score", score.ToString());
                ddTx.AddValue("Threshold", th.ToString());
                ddTx.AddValue("Timestamp", ts);
                _ret = DynamicData.SerializeToXml(ddTx);
                LOG.Debug("GetDDFormExternalTx Out - _ret = " + _ret);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.DMANec.GetDDFormExternalTx Error", ex);
                _ret = "";
            }
            return _ret;
        }



        private byte[] ExtraePC1FromPDF417(byte[] _PC1Cedula)
        {
            byte[] ret = null;
            try
            {
                LOG.Debug("ExtraePC1FromPDF417 In...");
                /*
                  string rut = Encoding.UTF7.GetString(pdf, 0, 9).Replace('\0', ' ').TrimEnd();
				string apellido = Encoding.UTF7.GetString(pdf, 19, 30).Replace('\0',' ').TrimEnd();
                //pais = Encoding.UTF7.GetString(pdf, 49, 3).Replace('\0', ' ').TrimEnd();
                //numeroDeSerie = Encoding.UTF7.GetString(pdf, 58, 10).Replace('\0', ' ').TrimEnd();

				string aa = Encoding.UTF7.GetString(pdf, 52, 2);
				string mm = Encoding.UTF7.GetString(pdf, 54, 2);
				string dd = Encoding.UTF7.GetString(pdf, 56, 2);
				DateTime expiracion = new DateTime(Convert.ToInt32(aa)+2000, Convert.ToInt32(mm), Convert.ToInt32(dd));

				int FingerId = (pdf[73] << 24)+ (pdf[72] << 16) + (pdf[71] << 8)+(pdf[70]);
                 */
                //int FingerId = (pdf[73] << 24) + (pdf[72] << 16) + (pdf[71] << 8) + (pdf[70]);
                int pcLen = (_PC1Cedula[77] << 24) + (_PC1Cedula[76] << 16) + (_PC1Cedula[75] << 8) + (_PC1Cedula[74]);
                byte[] pc1 = new Byte[400];
                Buffer.BlockCopy(_PC1Cedula, 78, pc1, 0, pcLen);

                ret = pc1;
                LOG.Debug("ExtraePC1FromPDF417 Out - _ret = " + Convert.ToBase64String(ret));
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.MatcherNecDMA.ExtraePC1FromPDF417 Error", ex);
            }
            return ret;
        }

        private int VerifyInternoDLL(byte[] _rawGenerated, byte[] _pdf417Cedula, out string msg, out int _Score)
        {
            int _ret = 0;
            _Score = 0;

            msg = null;

            try
            {
                //_aRawGenerated = _rawGenerated;
                //_aPdf417Cedula = _pdf417Cedula;

                
                ////_Score = (int)oNEC.Matching_cedula(ref _aPdf417Cedula, ref _aRawGenerated);
                
                ////_aPc1Cedula = _PC1Cedula;
                ////byte[] afis = new byte[400];
                ////_aPc1Generated = afis;
                //string strDatos;
                //Biometrika.NEC.Result _result1 = oBKNec.ParsePDF417(Convert.ToBase64String(_pdf417Cedula), out strDatos);
                //if (_result1.Code != 0)
                //    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL Error oBKNec.ParsePDF417 = " + _result1.Code + " - Msg = " + _result1.Message);
                //else
                //    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL oBKNec.ParsePDF417 OK!");
                //string[] strArr = _result1.Message.Split(new string[1] { "|" }, StringSplitOptions.None);
 
                //Biometrika.NEC.Result _result = oBKNec.Extract(_rawGenerated, 1, 10);
                //if (_result.Code != 0)
                //    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL Error oBKNec.Extract = " + _result.Code + " - Msg = " + _result.Message);
                //else
                //    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL oBKNec.Extract OK!");
 
                //Biometrika.NEC.Result _result2 = oBKNec.Matching(_result.Minutiae1, strArr[9], 10);
                //if (_result2.Code != 0)
                //{
                //    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL Error oBKNec.Matching = " + _result2.Code + " - Msg = " + _result2.Message);
                //    _ret = _result2.Score;
                //}
                //else
                //{
                //    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL oBKNec.Matching OK!");
                //    _ret = _result2.Code;
                //}
                
                
            }
            catch (Exception ex)
            {
                LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL Error - Msg=" + ex.Message + " - Stack=" + ex.StackTrace);
                msg = ex.Message + "[len pdf=" + _pdf417Cedula.Length + " - len raw= " + _rawGenerated.Length + "]";
                _ret = -1;
            }
            return _ret;
        }

        /// <summary>
        /// Hace verificación aqui con COM
        /// </summary>
        /// <param name="_rawGenerated"></param>
        /// <param name="_PC1Cedula"></param>
        /// <param name="_pcLen"></param>
        /// <param name="msg"></param>
        /// <param name="_Score"></param>
        /// <returns></returns>
        //private int VerifyInternoCOM(byte[] _rawGenerated, byte[] _PC1Cedula, int _pcLen, out string msg, out int _Score)
        private int VerifyInternoCOM(byte[] _rawGenerated, byte[] _pdf417Cedula, out string msg, out int _Score)
        {
            int _ret = 0;
            _Score = 0;
          
            msg = null;

            try
            {
//                _aRawGenerated = _rawGenerated;
//                _aPdf417Cedula = _pdf417Cedula;
//                _Score = (int)oNEC.Matching_cedula(ref _aPdf417Cedula, ref _aRawGenerated);

//                //_aPc1Cedula = _PC1Cedula;
//                //byte[] afis = new byte[400];
//                //_aPc1Generated = afis;
//                _ret = oNEC.raw_to_pc1(ref _aRawGenerated, ref _aPc1Generated, ref _pcLenGenerated);
//                LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoCOM - raw_to_pc1=>  Pc1.Len=" + _aPc1Generated.Length +
//                    " - _pcLenGenerated = " + _pcLenGenerated);
////                _ret = oNEC.data_pdf(ref _aPdf417Cedula);
                
//                _aPc1Cedula = new byte[400];
               
//                _ret = oNEC.saca_afis(ref _aPdf417Cedula, ref _aPc1Cedula, ref _pcLenAux);
//                LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoCOM - saca_afis=>  _aPc1Cedula.Len=" + _aPc1Cedula.Length +
//                    " - _pcLenAux = " + _pcLenAux);

//                //_pcLenAux = _pcLenAux - 1;
//                LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoCOM - match_1a1 in...");
//                System.Threading.Thread th = new System.Threading.Thread(Run, 41943040);
//                th.Start();
//                th.Join();

//                //_ret = oNEC.match_1a1(ref _aPc1Generated, ref _pcLenGenerated, ref _aPc1Cedula, ref _pcLenAux);
//                //LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoCOM - match_1a1=>  _ret=" + _ret);
//                //if (_ret == 0)
//                ////{
//                ////    _ret = oNEC.match_1a1(ref _aPc1Generated, _pcLenGenerated, ref _aPc1Cedula, ref _pcLenGenerated);
//                ////    if (_ret != 0) msg = "VerifyInternoCOM - Error proceso matching NEC";
//                ////}
//                ////else
//                //    msg = "VerifyInternoCOM - Error extrayendo minucias NEC";
            }
            catch (Exception ex)
            {
                LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoCOM Error - Msg=" + ex.Message + " - Stack=" + ex.StackTrace);
                msg = ex.Message + "[len pdf=" + _pdf417Cedula.Length + " - len raw= " + _rawGenerated.Length + "]";
                _ret = -1;
            }
            return _ret;
        }
      
        private int _globalRet;
          Array _aRawGenerated;
            Array _aPdf417Cedula;
            Array _aPc1Generated = new byte[400];
            int _pcLenGenerated = 0;
            Array _aPc1Cedula;
         int _pcLenAux = 0;

         void Run()
         {
             try
             {
                 //_globalRet = oNEC.match_1a1(ref _aPc1Generated, ref _pcLenGenerated, ref _aPc1Cedula, ref _pcLenAux);
                 LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoCOM - match_1a1=>  _globalRet=" + _globalRet);
             }
             catch (Exception ex)
             {
                 LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoCOM Error - Msg=" + ex.Message + " - Stack=" + ex.StackTrace);
             }
         }

        private string ConvertToNumber(string status)
        {
            string ret = "0";
            try
            {
                ret = status.Substring(4);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.MatcherNecDMA.ConvertToNumber Error", ex);
            }
            return ret;
        }

        private string ReadFromHDD(string path)
        {
            string sret = null;
            try
            {
                System.IO.StreamReader sr = new StreamReader(path);
                sret = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.MatcherNecDMA.ReadFromHDD Error", ex);
                sret = null;
            }
            return sret;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */

            string msg;
            xmloutput = null;
            int iretremoto;

            //DynamicData oXmlout = new DynamicData();
            //oXmlout.AddValue("message", "");
            //oXmlout.AddValue("trackid", "");
            //oXmlout.AddValue("status", "");
            //oXmlout.AddValue("result", "");
            //oXmlout.AddValue("score", "");
            //oXmlout.AddValue("timestamp", "");
            //oXmlout.AddValue("personaldata", "");

            //try
            //{
            //    if (!this._isConfigured)
            //    {
            //        msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error = Connector No Configurado!";
            //        LOG.Fatal(msg);
            //        oXmlout.SetValue("message", msg);
            //        xmloutput = DynamicData.SerializeToXml(oXmlout);
            //        return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
            //    }

            //    using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
            //    {
            //        ws.Timeout = this._timeout;
            //        ws.Url = this._urlWS;

            //        iretremoto = ws.Identify(xmlinput, out xmloutbp);
            //        if (iretremoto == Errors.IERR_OK)
            //        {
            //            XmlParamOut oXmlParamOut = Utils.XmlUtils.DeserializeObject<XmlParamOut>(xmloutbp);
            //            oXmlout.SetValue("message", "");
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            oXmlout.SetValue("trackid", oXmlParamOut.Trackid);
            //            oXmlout.SetValue("result", oXmlParamOut.Result.ToString());
            //            oXmlout.SetValue("score", oXmlParamOut.Score.ToString());
            //            oXmlout.SetValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
            //            oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(oXmlParamOut.PersonalData));
            //        }
            //        else
            //        {
            //            msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error en WS Remoto [" + iretremoto.ToString() + "]";
            //            LOG.Fatal(msg);
            //            oXmlout.SetValue("message", msg);
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            xmloutput = DynamicData.SerializeToXml(oXmlout);
            //            return Errors.IERR_CONNECTOR_REMOTE_ERROR;
            //        }
            //    }

            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //}
            //catch (Exception ex)
            //{
            //    msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error [" + ex.Message + "]";
            //    LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.Identity Error", ex);
            //    oXmlout.SetValue("message", msg);
            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //    return Errors.IERR_UNKNOWN;
            //}

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de Recuperación. Ingresa información recuperar, 
        /// y se realiza la operacion.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Get(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */

            string msg;
            string xmloutbp;
            int iretremoto;

            xmloutput = null;

            return Errors.IERR_OK;
        }

        //private string GetDDFormExternalTx(string CT, EncabezadoRespuesta response,
        //                                   int result, int th, double score)
        //{
        //    string _ret = "";
        //    try
        //    {
        //        if (response == null) return "";

        //        DynamicData ddTx = new DynamicData();
        //        ddTx.AddValue("Consultationtype", CT);
        //        ddTx.AddValue("Connectorid", "SRCeI.BioVerify2015");
        //        ddTx.AddValue("Trackid", response.IdTransaccion.ToString());
        //        ddTx.AddValue("Status", ConvertToNumber(response.Estado.ToString()));
        //        ddTx.AddValue("Result", result.ToString());
        //        ddTx.AddValue("Score", score.ToString());
        //        ddTx.AddValue("Threshold", th.ToString());
        //        ddTx.AddValue("Timestamp", response.FechaHoraOperacion.ToString());
        //        _ret = DynamicData.SerializeToXml(ddTx);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("Bio.Core.MatcherNecDMA Error", ex);
        //        _ret = "";
        //    }
        //    return _ret;
        //}

        
#endregion Implementation of IConnector

        private static string FormatFechaHoraSRCeI(string fechaoperacion)
        {
            string strRet = "";
            try
            {
                strRet = fechaoperacion.Substring(6, 2) + "/" +
                         fechaoperacion.Substring(4, 2) + "/" +
                         fechaoperacion.Substring(0, 4) + " " +
                         fechaoperacion.Substring(8, 2) + ":" +
                         fechaoperacion.Substring(10, 2) + ":" +
                         fechaoperacion.Substring(12, 2);
            }
            catch
            {
                strRet = "";
            }
            return strRet;
        }

        public void Dispose()
        {
            
        }

        /// <summary>
        /// Parsea PDF417 de cedula Chilena
        /// </summary>
        /// <param name="pdf417"></param>
        /// <param name="datos"></param>
        /// <returns></returns>
        //[ComVisible(true)]
        private int ParsePDF417(string pdf417, out byte[] _PC1Cedula, out int _pcLen)
        {
            int res = 0;
            byte[] pdf = null;
            _pcLen = 0;
            _PC1Cedula = null;

            try
            {
                LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.ParsePDF417 In...");
                if (!string.IsNullOrEmpty(pdf417))
                {
                    pdf = Convert.FromBase64String(pdf417);
                    
                    //string rut = Encoding.UTF7.GetString(pdf, 0, 9).Replace('\0', ' ').TrimEnd();
                    //string apellido = Encoding.UTF7.GetString(pdf, 19, 30).Replace('\0', ' ').TrimEnd();
                    //string pais = Encoding.UTF7.GetString(pdf, 49, 3).Replace('\0', ' ').TrimEnd();
                    //string numeroDeSerie = Encoding.UTF7.GetString(pdf, 58, 10).Replace('\0', ' ').TrimEnd();

                    //string aa = Encoding.UTF7.GetString(pdf, 52, 2);
                    //string mm = Encoding.UTF7.GetString(pdf, 54, 2);
                    //string dd = Encoding.UTF7.GetString(pdf, 56, 2);
                    //string expiracion = dd + "/" + mm + "/" + aa;

                    int finger = (pdf[73] << 24) + (pdf[72] << 16) + (pdf[71] << 8) + (pdf[70]);
                    _pcLen = (pdf[77] << 24) + (pdf[76] << 16) + (pdf[75] << 8) + (pdf[74]);
                    _PC1Cedula = new Byte[_pcLen];
                    Buffer.BlockCopy(pdf, 78, _PC1Cedula, 0, _pcLen);

                    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.ParsePDF417 PCLen = " + _pcLen + " - MinucitaePC1 = " + Convert.ToBase64String(_PC1Cedula));

                    //datos = rut + "|" +
                    //        apellido + "|" +
                    //        pais + "|" +
                    //        numeroDeSerie + "|" +
                    //        expiracion + "|" +
                    //        appNumber + "|" +
                    //        disability + "|" +
                    //        docType + "|" +
                    //        finger.ToString() + "|" +
                    //        Convert.ToBase64String(pc1) + "|" +
                    //        pcLen.ToString();

                    //WriteLog("ParsePDF417 OK - " + datos);

                    res = 0;
                }
                else
                {
                    res = Bio.Core.Constant.Errors.IERR_NULL_TEMPLATE;
                    LOG.Error("Bio.Core.Matcher.Connector.NecDMA.ParsePDF417 Error PDF417 es nulo...");
                }
            }
            catch (Exception ex)
            {
                res = -1;
                LOG.Error("Bio.Core.Matcher.Connector.NecDMA.ParsePDF417 Error - Msg=" + ex.Message + " - Stack=" + ex.StackTrace);
            }

            LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.ParsePDF417 Out!");
            return res;
        }
    }
}
