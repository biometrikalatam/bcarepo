﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using Bio.Core.Serialize;
using log4net;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;

namespace Bio.Core.Matcher.VeriLook
{
    public class Matcher : IMatcher
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Matcher));

#region Implementation of IMatcher

        private int _authenticationFactor;
        private int _minutiaeType;
        private double _threshold;
        private int _matchingType;

        private string _parameters;

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }


        /// <summary>
        /// Tecnologia a utilizar para matching
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de verificación considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Dos posibles valores:
        ///     0 - FIRST   -> El primer matching positivo
        ///     1 - BEST    -> El mejor matching positivo
        /// </summary>
        public int MatchingType
        {
            get { return _matchingType; }
            set { _matchingType = (value == 1 || value == 2) ? value : 1; }
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// y nuevo, parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;

            try
            {
                if (!VfeUtils.IsLicenseConfigured)
                {
                    if (!VfeUtils.ConfigureLicense(_parameters))
                    {
                        msg = "Bio.Core.Matcher.VeriLook.Matcher Error Chequeando Licencia";
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_LICENSE;
                    }
                }

                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                //1. Importo template de registro, creo cada uno y los importo
                try
                {
                    byCurrentTemplate = Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.VeriLook.Matcher Error importando CurrentTemplate " +
                         "[ex=" + ex.Message + "]";
                    LOG.Fatal(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                
                //2.- Genero objeto para matching
                NBiometricClient _matcher = new NBiometricClient();
                // Set paramater to return matching details
                _matcher.MatchingWithDetails = true;
                _matcher.MatchingThreshold = Convert.ToInt32(_threshold);
                LOG.Debug("VeriLook.Verify - _matcher.MatchingThreshold = " + _matcher.MatchingThreshold);
                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                                (string)paramin.GetValue("listBaseTemplates"));
                LOG.Debug("VeriLook.Verify - listTemplatesBase != null => " + (listTemplatesBase != null));
                if (listTemplatesBase != null) LOG.Debug("VeriLook.Verify - listTemplatesBase.count => " + listTemplatesBase.Count);

                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                //Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                // Read probe template
                using (NSubject currentSubject = CreateSubject(byCurrentTemplate, "CurrentSubject"))
                {
                    // Read gallery templates and enroll them
                    NBiometricTask enrollTask = _matcher.CreateTask(NBiometricOperations.Enroll, null);
                    for (int i = 0; i < listTemplatesBase.Count; ++i)
                    {
                        enrollTask.Subjects.Add(CreateSubject(listTemplatesBase[i].Data, string.Format("{0}", i)));
                    }
                    _matcher.PerformTask(enrollTask);
                    NBiometricStatus status = enrollTask.Status;
                    if (status != NBiometricStatus.Ok)
                    {
                        msg = "VeriLook.Verify - Enrollment fallo (enrollTask). Status: " + status;
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_IDENTIFY;
                    }

                    // Identify probe subject
                    status = _matcher.Identify(currentSubject);
                    if (status == NBiometricStatus.Ok)
                    {
                        foreach (var matchingResult in currentSubject.MatchingResults)
                        {
                            //Console.WriteLine("Matched with ID: '{0}' with score {1}", matchingResult.Id, matchingResult.Score);
                            if (matchingResult.MatchingDetails != null)
                            {
                                //Console.WriteLine(MatchingDetailsToString(matchingResult.MatchingDetails));
                                score = matchingResult.Score;
                                if (score >= _threshold)
                                {
                                    verifyOk = true;
                                    //Si es Matching type first => Retorno
                                    if (this._matchingType == 1)
                                    {
                                        bestscore = score;
                                        templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                        break;
                                    }
                                    else
                                    {
                                        if (score > bestscore)
                                        {
                                            templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                            bestscore = score;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        msg = "VeriLook.Verify - _matcher.Identify Status: " + status;
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        //return Errors.IERR_IDENTIFY;
                    }
                }
                
                //3.- Si verifico bien armo salida
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                if (verifyOk)
                {
                    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                }
                else
                {
                    paramout.SetValue("message", "No hubo matching positivo");
                }
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.VeriLook.Matcher.Verify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_VERIFY;
            }

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// y lista contra que verificar, parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;

            try
            {
                ////0.-Deserializo xmlimput
                //paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                ////1. Importo template de registro, creo cada uno y los importo
                //try
                //{
                //    byCurrentTemplate = Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                //}
                //catch (Exception ex)
                //{
                //    msg = "Bio.Core.Matcher.VeriLook.Matcher Error importando CurrentTemplate " +
                //         "[ex=" + ex.Message + "]";
                //    LOG.Fatal(msg);
                //    paramout.SetValue("message", msg);
                //    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                //    return Errors.IERR_BAD_PARAMETER;
                //}

                ////2.- Genero objeto para matching
                //NFMatcher _matcher = new NFMatcher();
                //_matcher.MatchingThreshold = Convert.ToInt32(_threshold);
                //LOG.Debug("VeriLook.Identify - _matcher.MatchingThreshold = " + _matcher.MatchingThreshold);

                ////Deserializo la lista de templates pasados contra que verificar
                //List<Bio.Core.Matcher.Template> listTemplatesBase =
                //    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                //                (string)paramin.GetValue("listBaseTemplates"));
                //LOG.Debug("VeriLook.Identify - listTemplatesBase != null => " + (listTemplatesBase != null));
                //if (listTemplatesBase != null) LOG.Debug("VeriLook.Identify - listTemplatesBase.count => " + listTemplatesBase.Count);

                ////Contendra el template contra el que se verifico positivametne si asi fue,
                ////para datos de bodypart, companyid, etc.
                //Bio.Core.Matcher.Template templateVerified = null;

                ////Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                //_matcher.IdentifyStart(byCurrentTemplate);
                //for (int i = 0; i < listTemplatesBase.Count; ++i)
                //{
                //    score = _matcher.IdentifyNext(listTemplatesBase[i].Data);
                //    if (score >= _threshold)
                //    {
                //        verifyOk = true;
                //        //Si es Matching type first => Retorno
                //        if (this._matchingType == 1)
                //        {
                //            bestscore = score;
                //            templateVerified = listTemplatesBase[i];
                //            break;
                //        }
                //        else
                //        {
                //            if (bestscore > score)
                //            {
                //                templateVerified = listTemplatesBase[i];
                //                bestscore = score;
                //            }
                //        }
                //    }
                //}
                //_matcher.IdentifyEnd();

                ////3.- Si verifico bien armo salida
                //paramout.AddValue("score", bestscore);
                //paramout.AddValue("result", verifyOk ? 1 : 2);
                //paramout.AddValue("threshold", threshold);
                //if (verifyOk)
                //{
                //    paramout.AddValue("id", templateVerified == null ? 0 : templateVerified.Id);
                //    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                //    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                //    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                //    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                //}
                //else
                //{
                //    paramout.SetValue("message", "No hubo matching positivo");
                //}
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                if (!VfeUtils.IsLicenseConfigured)
                {
                    if (!VfeUtils.ConfigureLicense(_parameters))
                    {
                        msg = "Bio.Core.Matcher.VeriLook.Matcher Error Chequeando Licencia";
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_LICENSE;
                    }
                }

                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                //1. Importo template de registro, creo cada uno y los importo
                try
                {
                    byCurrentTemplate = Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.VeriLook.Matcher Error importando CurrentTemplate " +
                         "[ex=" + ex.Message + "]";
                    LOG.Fatal(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Genero objeto para matching
                NBiometricClient _matcher = new NBiometricClient();
                // Set paramater to return matching details
                _matcher.MatchingWithDetails = true;
                _matcher.MatchingThreshold = Convert.ToInt32(_threshold);
                LOG.Debug("VeriLook.Identify - _matcher.MatchingThreshold = " + _matcher.MatchingThreshold);
                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                                (string)paramin.GetValue("listBaseTemplates"));
                LOG.Debug("VeriLook.Identify - listTemplatesBase != null => " + (listTemplatesBase != null));
                if (listTemplatesBase != null) LOG.Debug("VeriLook.Identify - listTemplatesBase.count => " + listTemplatesBase.Count);

                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                //Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                // Read probe template
                using (NSubject currentSubject = CreateSubject(byCurrentTemplate, "CurrentSubject"))
                {
                    // Read gallery templates and enroll them
                    NBiometricTask enrollTask = _matcher.CreateTask(NBiometricOperations.Enroll, null);
                    for (int i = 1; i < listTemplatesBase.Count; ++i)
                    {
                        enrollTask.Subjects.Add(CreateSubject(listTemplatesBase[i].Data, string.Format("{0}", i)));
                    }
                    _matcher.PerformTask(enrollTask);
                    NBiometricStatus status = enrollTask.Status;
                    if (status != NBiometricStatus.Ok)
                    {
                        msg = "VeriLook.Identify - Enrollment fallo (enrollTask). Status: " + status;
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_IDENTIFY;
                    }

                    // Identify probe subject
                    status = _matcher.Identify(currentSubject);
                    if (status == NBiometricStatus.Ok)
                    {
                        foreach (var matchingResult in currentSubject.MatchingResults)
                        {
                            //Console.WriteLine("Matched with ID: '{0}' with score {1}", matchingResult.Id, matchingResult.Score);
                            if (matchingResult.MatchingDetails != null)
                            {
                                //Console.WriteLine(MatchingDetailsToString(matchingResult.MatchingDetails));
                                score = matchingResult.Score;
                                if (score >= _threshold)
                                {
                                    verifyOk = true;
                                    //Si es Matching type first => Retorno
                                    if (this._matchingType == 1)
                                    {
                                        bestscore = score;
                                        templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                        break;
                                    }
                                    else
                                    {
                                        if (score > bestscore)
                                        {
                                            templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                            bestscore = score;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        msg = "VeriLook.Identify - _matcher.Identify Status: " + status;
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_IDENTIFY;
                    }
                }

                //3.- Si verifico bien armo salida
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                if (verifyOk)
                {
                    paramout.AddValue("id", templateVerified == null ? 0 : templateVerified.Id);
                    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                }
                else
                {
                    paramout.SetValue("message", "No hubo matching positivo");
                }
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.VeriLook.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

        public int Identify(ITemplate templateCurrent, List<Bio.Core.Matcher.Template> listTemplatePeople, 
                            out float score, out int idBir)
        {
            idBir = 0;
            score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;

            try
            {
                byCurrentTemplate = templateCurrent.Data;

                //2.- Genero objeto para matching
                NBiometricClient _matcher = new NBiometricClient();
                // Set paramater to return matching details
                _matcher.MatchingWithDetails = true;
                _matcher.MatchingThreshold = Convert.ToInt32(_threshold);
                LOG.Debug("VeriLook.Identify - _matcher.MatchingThreshold = " + _matcher.MatchingThreshold);
                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase = listTemplatePeople;
                LOG.Debug("VeriLook.Identify - listTemplatesBase != null => " + (listTemplatesBase != null));
                if (listTemplatesBase != null) LOG.Debug("VeriLook.Identify - listTemplatesBase.count => " + listTemplatesBase.Count);

                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                //Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                // Read probe template
                using (NSubject currentSubject = CreateSubject(byCurrentTemplate, "CurrentSubject"))
                {
                    // Read gallery templates and enroll them
                    NBiometricTask enrollTask = _matcher.CreateTask(NBiometricOperations.Enroll, null);
                    for (int i = 1; i < listTemplatesBase.Count; ++i)
                    {
                        enrollTask.Subjects.Add(CreateSubject(listTemplatesBase[i].Data, string.Format("{0}", i)));
                    }
                    _matcher.PerformTask(enrollTask);
                    NBiometricStatus status = enrollTask.Status;
                    if (status != NBiometricStatus.Ok)
                    {
                        msg = "VeriLook.Identify - Enrollment fallo (enrollTask). Status: " + status;
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_IDENTIFY;
                    }

                    // Identify probe subject
                    status = _matcher.Identify(currentSubject);
                    if (status == NBiometricStatus.Ok)
                    {
                        foreach (var matchingResult in currentSubject.MatchingResults)
                        {
                            //Console.WriteLine("Matched with ID: '{0}' with score {1}", matchingResult.Id, matchingResult.Score);
                            if (matchingResult.MatchingDetails != null)
                            {
                                //Console.WriteLine(MatchingDetailsToString(matchingResult.MatchingDetails));
                                score = matchingResult.Score;
                                if (score >= _threshold)
                                {
                                    verifyOk = true;
                                    //Si es Matching type first => Retorno
                                    if (this._matchingType == 1)
                                    {
                                        bestscore = score;
                                        templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                        break;
                                    }
                                    else
                                    {
                                        if (score > bestscore)
                                        {
                                            templateVerified = listTemplatesBase[Convert.ToInt32(matchingResult.Id)];
                                            bestscore = score;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        msg = "VeriLook.Identify - _matcher.Identify Status: " + status;
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_IDENTIFY;
                    }
                }

                //3.- Si verifico bien armo salida
                score = (float)bestscore;
                idBir = templateVerified == null ? 0 : templateVerified.Id;

                //paramout.AddValue("score", bestscore);
                //paramout.AddValue("result", verifyOk ? 1 : 2);
                //paramout.AddValue("threshold", threshold);
                //if (verifyOk)
                //{
                //    paramout.AddValue("id", templateVerified == null ? 0 : templateVerified.Id);
                //    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                //    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                //    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                //    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                //}
                //else
                //{
                //    paramout.SetValue("message", "No hubo matching positivo");
                //}
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.VeriLook.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

        public int Verify(ITemplate template1, ITemplate template2, out float score)
        {
            throw new NotImplementedException();
        }

        private static NSubject CreateSubject(byte[] template, string subjectId)
        {
            var subject = new NSubject();
            subject.SetTemplateBuffer(new Neurotec.IO.NBuffer(template));
            subject.Id = subjectId;

            return subject;
        }

 #endregion

        public void Dispose()
        {
            if (VfeUtils.IsLicenseConfigured)
            {
                VfeUtils.ReleaseLicenses();
            }
        }
    }
}
