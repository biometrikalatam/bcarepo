﻿using System;
using System.Drawing;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using Bio.Core.Wsq.Decoder;
using log4net;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Images;


namespace Bio.Core.Matcher.VeriLook
{
    public class Extractor : IExtractor
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Extractor));
        
#region Implementation of IExtractor

        private int _authenticationFactor;

        private int _minutiaeType;

        private double _threshold;

        private string _parameters;

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        /// <summary>
        /// Tecnologia a utilizar para extraccion
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo d eminucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de extracción considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Extrae desde el template ingresado en el parametro inicial, de acuerdo a 
        /// los adtos de tecnologia y minucias seteados, el template resultante. 
        /// Estos parámetros están serializados en xml, por lo que primero se deserializa.
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="xmloutput">template generado serializado en xml</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out string xmloutput)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero entrega un ITemplate
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out ITemplate templateout)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero ingresa un ITemplate y entrega un ITemplate
        /// </summary>
        /// <param name="templatebase">Template base para extraccion</param>
        /// <param name="destination">Determina si es template para 1-Verify | 2-Enroll </param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(ITemplate templatebase, int destination, out ITemplate templateout)
        {
            int res = Errors.IERR_OK;
            templateout = null;

            try
            {

                if (!VfeUtils.IsLicenseConfigured)
                {
                    if (!VfeUtils.ConfigureLicense(_parameters)) return Errors.IERR_LICENSE;
                }

                if (templatebase == null) return Errors.IERR_NULL_TEMPLATE;
                if (templatebase.Data == null) return Errors.IERR_NULL_TEMPLATE;

                if (templatebase.AuthenticationFactor !=
                    Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL ||
                    (templatebase.MinutiaeType !=
                    Constant.MinutiaeType.MINUTIAETYPE_FACIAL_VERILOOK && 
                    templatebase.MinutiaeType !=
                    Constant.MinutiaeType.MINUTIAETYPE_JPG))
                {
                    return Errors.IERR_INVALID_TEMPLATE;
                }

                //Si lo anterior esta ok, y la minucia es Verifinger solo asigno porque 
                if (templatebase.MinutiaeType == Constant.MinutiaeType.MINUTIAETYPE_FACIAL_VERILOOK)
                {
                    LOG.Error("VeriLook.Extractor.Extract - In Minutiae VF...");
                    templateout = templatebase;
                }
                else //es Constant.MinutiaeType.MINUTIAETYPE_JPG
                {
                    NBiometricClient _biometricClient = new NBiometricClient();
		            NSubject _subject;
                    byte[] raw;
                    bool rawOK = false;
                    if (templatebase.MinutiaeType == Constant.MinutiaeType.MINUTIAETYPE_JPG)
                    {
                        LOG.Error("VeriLook.Extractor.Extract - Crea NFExtractor...");
                        //2.- Extraigo  
                        //NBiometricEngine _extractor = new NBiometricEngine();

                        _subject = new NSubject();
                        LOG.Error("VeriLook.Extractor.Extract - Crea Bitmap...");
                        System.IO.MemoryStream ms = new System.IO.MemoryStream(templatebase.Data);
                        Bitmap imgBMP = new Bitmap(Bitmap.FromStream(ms));
                  //imgBMP.Save(@"C:\tmp\a\bitmapVerilookextractor.bmp");
                        NImage image = NImage.FromBitmap(imgBMP);

                        LOG.Error("VeriLook.Extractor.Extract - Crea NFace...");
                        NFace face = new NFace();
                        face.Image = image;
                       
                        _subject.Faces.Add(face);
                        
                        LOG.Error("VeriLook.Extractor.Extract - Crea template (CreateTemplate Func)...");
                        _biometricClient.FacesDetectBaseFeaturePoints = false;
                        _biometricClient.FacesTemplateSize = NTemplateSize.Large; 
                        
                        var status = _biometricClient.CreateTemplate(_subject);
                        //NBiometricStatus status = _extractor.CreateTemplate(_subject);

                        //System.IO.StreamWriter sr = new StreamWriter(@"D:\temp\a\vf_1.txt");
                        //sr.Write(Convert.ToBase64String(templateout.Data));
                        //sr.Close();

                        //FileStream fs = new FileStream(@"D:\temp\a\Token\vf1.bin", FileMode.Create);
                        //fs.Write(templateout.Data, 0, templateout.Data.Length);
                        //fs.Close();
                        //if (templateout.Data == null)
                        if (status != NBiometricStatus.Ok)
                        {
                            res = Errors.IERR_EXTRACTING;
                            templateout = null;
                            LOG.Error("VeriLook.Extractor.Extract Error [extractionStatus=" + status.ToString() + "]");
                        } else
                        {
                            templateout = new Template
                                              {
                                                  Data = _subject.GetTemplateBuffer().ToArray()
                                               };
                            templateout.AdditionalData = "SizeData=" + templateout.Data.Length.ToString();
                            templateout.AuthenticationFactor = templatebase.AuthenticationFactor;
                            templateout.BodyPart = templatebase.BodyPart;
                            templateout.MinutiaeType = Constant.MinutiaeType.MINUTIAETYPE_FACIAL_VERILOOK;
                            templateout.Type = Constant.BirType.PROCESSED_DATA;
                            //imageRAWtoBMP.Dispose();
                            //NGImage.Dispose();
                        }
                    } 
                }
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("Bio.Core.Matcher.VeriLook.Extract(T, T) Error", ex);
            }
            return res; 
        }

#endregion Implementation of IExtractor

        public void Dispose()
        {
            if (VfeUtils.IsLicenseConfigured)
            {
                VfeUtils.ReleaseLicenses();
            }
        }
    }
}
