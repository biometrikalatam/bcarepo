﻿
using System;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using log4net;

namespace Bio.Core.Matcher.DigitalPersonaW220
{
    public class Extractor : IExtractor
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Extractor));

#region Implementation of IExtractor

        private int _authenticationFactor;

        private int _minutiaeType;

        private double _threshold;

        private string _parameters;

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        /// <summary>
        /// Tecnologia a utilizar para extraccion
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo d eminucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de extracción considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Extrae desde el template ingresado en el parametro inicial, de acuerdo a 
        /// los adtos de tecnologia y minucias seteados, el template resultante. 
        /// Estos parámetros están serializados en xml, por lo que primero se deserializa.
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="xmloutput">template generado serializado en xml</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out string xmloutput)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero entrega un ITemplate
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out ITemplate templateout)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero ingresa un ITemplate y entrega un ITemplate
        /// </summary>
        /// <param name="templatebase">Template base para extraccion</param>
        /// <param name="destination">Determina si es template para 1-Verify | 2-Enroll </param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(ITemplate templatebase, int destination, out ITemplate templateout)
        {
            int res = Errors.IERR_OK;
            templateout = null;

            try
            {
                if (templatebase == null) return Errors.IERR_NULL_TEMPLATE;
                if (templatebase.Data == null &&
                    ((DigitalPersonaW220.Template)templatebase).DataToVerify == null &&
                    ((DigitalPersonaW220.Template)templatebase).DataToEnroll == null) return Errors.IERR_NULL_TEMPLATE;

                if (templatebase.AuthenticationFactor != 
                    Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT ||
                    (templatebase.MinutiaeType != Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW &&
                     templatebase.MinutiaeType != Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONA &&
                     templatebase.MinutiaeType != Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAGOLD))
                {
                    return Errors.IERR_INVALID_TEMPLATE;
                }
                    
                //Si lo anterior esta ok, solo asigno porque en DP no se puede hacer extraction
                //ya viene calculado desde el sensor DP. 
                //Add 07-07 - Si destination = 1 => tomo el template de verificacion, antes de "-septmpl-"
                //            sino destination = 2 => tomo el template de enroll, despues de "-septmpl-"   
                //      Si es un template de DigitalPersonaPLatinum => tiene _datatoverify y _datatoenroll si paso por setdata
                //                                                                                       sino lo hago pasar           
                if (((DigitalPersonaW220.Template)templatebase).DataToVerify == null &&
                    ((DigitalPersonaW220.Template)templatebase).DataToEnroll == null)
                {
                    ((DigitalPersonaW220.Template)templatebase).SetData = templatebase.GetData; 
                } 
                //string[] sep = new string[1] { "-septmpl-" };
                //string[] sa = templatebase.GetData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                templateout = templatebase;
                templateout.Data = (destination == 2) ? ((DigitalPersonaW220.Template)templatebase).DataToEnroll :
                                                        ((DigitalPersonaW220.Template)templatebase).DataToVerify;
                if (templateout.Data != null)
                    templateout.AdditionalData = "SizeData=" + templateout.Data.Length.ToString();
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.DigitalPersonaW220.Extract(T, T) Error", ex);
            }
            return res;
        }

#endregion Implementation of IExtractor

        public void Dispose()
        {
            
        }
    }
}
