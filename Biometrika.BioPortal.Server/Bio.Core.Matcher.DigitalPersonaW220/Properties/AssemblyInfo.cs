﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Bio.Core.Matcher.DigitalPersonaW220")]
[assembly: AssemblyDescription("Implementacion de Bio.Core.IMatcher para Digital Persona Windows 2.2.0")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Bioemtrika")]
[assembly: AssemblyProduct("Bio.Core.Matcher.DigitalPersona")]
[assembly: AssemblyCopyright("Copyright ©  Biometrika 2013")]
[assembly: AssemblyTrademark("Biometrika")]
[assembly: AssemblyCulture("")]


// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("33019407-8150-4718-b411-5b0d1ff6c927")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("5.1.1.0")]
[assembly: AssemblyFileVersion("5.1.1.0")]
