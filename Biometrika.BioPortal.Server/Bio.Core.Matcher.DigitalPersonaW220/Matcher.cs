﻿using System;
using System.Collections.Generic;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using Bio.Core.Serialize;
using DPUruNet;
using log4net;

namespace Bio.Core.Matcher.DigitalPersonaW220
{
    public class Matcher: IMatcher
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Matcher));

#region Implementation of IMatcher

        static int PROBABILITY_ONE = 0x7FFFFFFF;

        private int _authenticationFactor;
        private int _minutiaeType;
        private double _threshold;
        private int _matchingType;
        private string _parameters;

        /// <summary>
        /// Tecnologia a utilizar para matching
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de verificación considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Dos posibles valores:
        ///     0 - FIRST   -> El primer matching positivo
        ///     1 - BEST    -> El mejor matching positivo
        /// </summary>
        public int MatchingType
        {
            get { return _matchingType; }
            set { _matchingType = (value == 1 || value== 2) ? value : 1; }
        }

        /// <summary>
        /// Parametros adicionales al Matcher.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// y nuevo, parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            bool resultVerify = false;
            object oscore = 0;
            object othreshold = _threshold;
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            bool tture = true;
            int bodypart = 0;
            //AIErrors ierr;
            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;

            try
            {
                LOG.Debug("DigitalPersonaW220.Verify in...");
                LOG.Debug("DigitalPersonaW220.Verify xmlinput = " + xmlinput);
                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                LOG.Debug("DigitalPersonaW220.Verify CurrentTemplate Deserialize in...");
                //1. Importo template de registro, creo cada uno y los importo
                DPUruNet.Fmd currentTemplate;
                try
                {
                    //currentTemplate.Deserialize(byCurrentTemplate);
                    currentTemplate = Fmd.DeserializeXml(
                        DPUtils.GeneraXML((string) paramin.GetValue("currentTemplate"),DPUtils.MT_DPVERIFY));
                    
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.DigitalPersonaW220.Matcher.Verify Error importando CurrentTemplate [Fmd.Deserialize]";
                    LOG.Fatal(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                LOG.Debug("DigitalPersonaW220.Verify CurrentTemplate Deserialize out!");

                //2.- Genero objeto para matching
                LOG.Debug("DigitalPersonaW220.Verify listTemplatesBase DeserializeFromXml...");
                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                                (string)paramin.GetValue("listBaseTemplates"));
                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;
                bool verifyOk = false;

                Fmd baseTemplate;
                Fmd[] listFMD = new Fmd[listTemplatesBase.Count];
                int index = 0;
                LOG.Debug("DigitalPersonaW220.Verify listTemplatesBase != null => " + (listTemplatesBase != null));
                if (listTemplatesBase != null) LOG.Debug("DigitalPersonaW220.Verify listTemplatesBase.Count = " + listTemplatesBase.Count);
                LOG.Debug("DigitalPersonaW220.Verify foreach cTemplate in listTemplatesBase in...");
                //foreach (Bio.Core.Matcher.Template cTemplate in listTemplatesBase)
                for (int i = 0; i < listTemplatesBase.Count; i++)
                {
                    try
                    {
                        listFMD[index++] = Fmd.DeserializeXml(DPUtils.GeneraXML(listTemplatesBase[i].GetData, DPUtils.MT_DPENROLL));
                    }
                    catch (Exception ex)
                    {
                        msg =
                            "Bio.Core.Matcher.DigitalPersonaW220.Matcher.Verify Error importando listBaseTemplate [Fmd.Deserialize]";
                        LOG.Warn(msg);
                        LOG.Warn(ex.StackTrace);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_BAD_PARAMETER;
                    }
                }

                //See the SDK documentation for an explanation on threshold scores.
                int thresholdScore = 0;
                try
                {
                    int th = (int)_threshold;
                    th = 10000;
                    if (th > 0) thresholdScore = PROBABILITY_ONE * 1 / th;
                }
                catch (Exception ex)
                {
                    LOG.Error("Bio.Core.Matcher.DigitalPersonaW220.Matcher.Verify Error seteando _threshold = " + thresholdScore.ToString(), ex);
                }

                LOG.Debug("Entrando a Comparison.Verify => Universo = " + listFMD.Length + " - _threshold = " + _threshold 
                            + " - thresholdScore = " + thresholdScore);
                IdentifyResult identifyResult = Comparison.Identify(currentTemplate, 0, listFMD, thresholdScore, 1);
                if (identifyResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
                {
                    msg = "Bio.Core.Matcher.DigitalPersonaW220.Matcher.Identify Error Comparison.Verify [" + 
                            identifyResult.ResultCode + "]";
                    LOG.Warn(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    LOG.Debug("DigitalPersonaW220.Verify Identify.Indexes.Length = " + identifyResult.Indexes.Length.ToString());
                    //Sino veo el tema de Matchingtype 
                    //Si es matching positivo 
                    if (identifyResult.Indexes.Length > 0)
                    {
                        CompareResult compareResult = Comparison.Compare(currentTemplate, 0, listFMD[identifyResult.Indexes[0][0]], 0);
                        score = GetScoreEscalaDP(compareResult.Score, _threshold); //resultv.FARAchieved;
                        //if (score >= _threshold)
                        //{
                            verifyOk = true;
                            bestscore = score;
                            templateVerified = listTemplatesBase[identifyResult.Indexes[0][0]];
                        //}
                    }
                    else
                    {
                        verifyOk = false;
                    }
                }
                
                //3.- Si verifico bien armo salida
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                if (verifyOk)
                {
                    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource); 
                } else
                {
                    paramout.AddValue("message", "No hubo matching positivo");
                }
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                LOG.Debug("DigitalPersonaW220.Verify xmloutput = " + xmloutput);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.DigitalPersonaW220.Matcher.Verify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_VERIFY;
            }

            return Errors.IERR_OK;
        }

      

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// y lista contra que verificar, parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            bool resultVerify = false;
            object oscore = 0;
            object othreshold = _threshold;
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            bool tture = true;
            int bodypart = 0;
            //AIErrors ierr;
            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;

            try
            {
                LOG.Debug("DigitalPersonaW220.Identify in...");
                LOG.Debug("DigitalPersonaW220.Identify xmlinput = " + xmlinput);
                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                LOG.Debug("DigitalPersonaW220.Identify CurrentTemplate Deserialize in...");
                //1. Importo template de registro, creo cada uno y los importo
                Fmd currentTemplate;
               
                try
                {
                    currentTemplate = Fmd.DeserializeXml((string) paramin.GetValue("currentTemplate"));
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.DigitalPersonaW220.Matcher.Identify Error importando CurrentTemplate [Fmd.Deserialize]";
                    LOG.Fatal(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                LOG.Debug("DigitalPersonaW220.Identify CurrentTemplate Deserialize out!");

                    //2.- Genero objeto para matching
                LOG.Debug("DigitalPersonaW220.Identify listTemplatesBase DeserializeFromXml...");
                    //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                                (string)paramin.GetValue("listBaseTemplates"));
                    //Contendra el template contra el que se verifico positivametne si asi fue,
                    //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;
                bool verifyOk = false;
                Fmd baseTemplate;
                Fmd[] listFMD = new Fmd[listTemplatesBase.Count];
                int index = 0;
                LOG.Debug("DigitalPersonaW220.Identify foreach cTemplate in listTemplatesBase in...");
                //foreach (Bio.Core.Matcher.Template cTemplate in listTemplatesBase)
                for (int i = 0; i < listTemplatesBase.Count; i++)
                {
                    try
                    {
                        listFMD[index++] = Fmd.DeserializeXml(listTemplatesBase[i].GetData);
                    }
                    catch (Exception ex)
                    {
                        msg =
                            "Bio.Core.Matcher.DigitalPersonaW220.Matcher.Identify Error importando listBaseTemplate [Fmd.Deserialize]";
                        LOG.Warn(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_BAD_PARAMETER;
                    }
                }

                    //See the SDK documentation for an explanation on threshold scores.
                int thresholdScore = 0;
                try
                {
                    int th = (int)_threshold;
                    //th = 10000;
                    if (th > 0) thresholdScore = PROBABILITY_ONE * 1 / th;
                }
                catch (Exception ex)
                {
                    LOG.Error("Bio.Core.Matcher.DigitalPersonaW220.Matcher.Identify Error seteando _threshold = " + thresholdScore.ToString(), ex);
                }

                LOG.Debug("Entrando a Comparison.Verify => Universo = " + listFMD.Length + " - _threshold = " + _threshold
                             + " - thresholdScore = " + thresholdScore);
                IdentifyResult identifyResult = Comparison.Identify(currentTemplate, 0, listFMD, thresholdScore, 1);
                if (identifyResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
                {
                    msg =
                            "Bio.Core.Matcher.DigitalPersonaW220.Matcher.Identify Error Comparison.Identify [" + identifyResult.ResultCode + "]";
                        LOG.Warn(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_BAD_PARAMETER;
                } else {
                        LOG.Debug("DigitalPersonaW220.Identify Identify.Indexes.Length = " + identifyResult.Indexes.Length.ToString());
                        //Sino veo el tema de Matchingtype 
                        //Si es matching positivo 
                        if (identifyResult.Indexes.Length > 0)
                        {
                            CompareResult compareResult = Comparison.Compare(currentTemplate, 0, listFMD[identifyResult.Indexes[0][0]], 0);
                            score = GetScoreEscalaDP(compareResult.Score, _threshold); //resultv.FARAchieved;
                            //if (score >= _threshold)
                            //{
                                verifyOk = true;
                                bestscore = score;
                                templateVerified = listTemplatesBase[identifyResult.Indexes[0][0]];
                            //}

                            //score = 0; //resultv.FARAchieved;
                            //verifyOk = true;
                            //bestscore = score;
                            //templateVerified = listTemplatesBase[identifyResult.Indexes[0][0]];
                            
                            ////Si es Matching type first => Retorno
                            //if (this._matchingType == 1)
                            //{
                            //    bestscore = score;
                            //    templateVerified = cTemplate;
                            //    break;
                            //}
                            //else
                            //{
                            //    if (bestscore > score)
                            //    {
                            //        templateVerified = cTemplate;
                            //        bestscore = score;
                            //    }
                            //}
                        } else
                        {
                            verifyOk = false;
                        }
                 }
            
                //3.- Si verifico bien armo salida
                if (verifyOk)
                {
                    paramout.AddValue("score", bestscore);
                    paramout.AddValue("result", verifyOk ? 1 : 2);
                    paramout.AddValue("threshold", threshold);
                    paramout.AddValue("id", templateVerified.Id);
                    paramout.AddValue("bodypart", templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified.Verificationsource);
                }
                else
                {
                    paramout.AddValue("message", "No hubo matching positivo");
                }
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                LOG.Debug("DigitalPersonaW220.Identify xmloutput = " + xmloutput);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.DigitalPersonaW220.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

        public int Identify(ITemplate templateCurrent, List<Core.Matcher.Template> listTemplatePeople, out float score, out int idBir)
        {
            score = 0;
            idBir = 0;

            //bool resultVerify = false;
            //object oscore = 0;
            //object othreshold = _threshold;
            //double bestscore = 1;
            //double threshold = _threshold;

            //bool tture = true;
            //int bodypart = 0;
            //string msg;

            //try
            //{
            //    LOG.Debug("DigitalPersonaW220.Identify in...");
            //    LOG.Debug("DigitalPersonaW220.Identify CurrentTemplate Deserialize in...");
            //    //1. Importo template de registro, creo cada uno y los importo
            //    Fmd currentTemplate;

            //    try
            //    {
            //        currentTemplate = Fmd.DeserializeXml(templateCurrent.GetData);
            //    }
            //    catch (Exception ex)
            //    {
            //        msg = "Bio.Core.Matcher.DigitalPersonaW220.Matcher.Identify Error importando CurrentTemplate [Fmd.Deserialize]";
            //        LOG.Fatal(msg);
            //        return Errors.IERR_BAD_PARAMETER;
            //    }
            //    LOG.Debug("DigitalPersonaW220.Identify CurrentTemplate Deserialize out!");

            //    //2.- Genero objeto para matching
            //    LOG.Debug("DigitalPersonaW220.Identify listTemplatesBase DeserializeFromXml...");
            //    //Deserializo la lista de templates pasados contra que verificar
            //    List<Bio.Core.Matcher.Template> listTemplatesBase = listTemplatePeople;
            //    //Contendra el template contra el que se verifico positivametne si asi fue,
            //    //para datos de bodypart, companyid, etc.
            //    Bio.Core.Matcher.Template templateVerified = null;
            //    bool verifyOk = false;
            //    Fmd baseTemplate;
            //    Fmd[] listFMD = new Fmd[listTemplatesBase.Count];
            //    int index = 0;
            //    LOG.Debug("DigitalPersonaW220.Identify foreach cTemplate in listTemplatesBase in...");
            //    //foreach (Bio.Core.Matcher.Template cTemplate in listTemplatesBase)
            //    for (int i = 0; i < listTemplatesBase.Count; i++)
            //    {
            //        try
            //        {
            //            listFMD[index++] = Fmd.DeserializeXml(listTemplatesBase[i].GetData);
            //        }
            //        catch (Exception ex)
            //        {
            //            msg =
            //                "Bio.Core.Matcher.DigitalPersonaW220.Matcher.Identify Error importando listBaseTemplate [Fmd.Deserialize]";
            //            LOG.Warn(msg);
            //            return Errors.IERR_BAD_PARAMETER;
            //        }
            //    }

            //    //See the SDK documentation for an explanation on threshold scores.
            //    int thresholdScore = 0;
            //    try
            //    {
            //        int th = (int)_threshold;
            //        if (th > 0) thresholdScore = PROBABILITY_ONE * 1 / th;
            //    }
            //    catch (Exception ex)
            //    {
            //        LOG.Error("Bio.Core.Matcher.DigitalPersonaW220.Matcher.Identify Error seteando _threshold = " + thresholdScore.ToString(), ex);
            //    }

            //    LOG.Debug("Entrando a Comparison.Identify => Universo = " + listFMD.Length + " - threshold = " + thresholdScore);
            //    IdentifyResult identifyResult = Comparison.Identify(currentTemplate, 0, listFMD, thresholdScore, 1);
            //    if (identifyResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
            //    {
            //        msg =
            //                "Bio.Core.Matcher.DigitalPersonaW220.Matcher.Identify Error Comparison.Identify [" + identifyResult.ResultCode + "]";
            //        LOG.Warn(msg);
            //        return Errors.IERR_BAD_PARAMETER;
            //    }
            //    else
            //    {
            //        LOG.Debug("DigitalPersonaW220.Identify Identify.Indexes.Length = " + identifyResult.Indexes.Length.ToString());
            //        //Sino veo el tema de Matchingtype 
            //        //Si es matching positivo 
            //        if (identifyResult.Indexes.Length > 0)
            //        {
            //            score = 0; //resultv.FARAchieved;
            //            verifyOk = true;
            //            bestscore = score;
            //            templateVerified = listTemplatesBase[identifyResult.Indexes[0][0]];
            //            ////Si es Matching type first => Retorno
            //            //if (this._matchingType == 1)
            //            //{
            //            //    bestscore = score;
            //            //    templateVerified = cTemplate;
            //            //    break;
            //            //}
            //            //else
            //            //{
            //            //    if (bestscore > score)
            //            //    {
            //            //        templateVerified = cTemplate;
            //            //        bestscore = score;
            //            //    }
            //            //}
            //        }
            //        else
            //        {
            //            verifyOk = false;
            //        }
            //    }

            //    //3.- Si verifico bien armo salida
            //    if (verifyOk)
            //    {
            //        paramout.AddValue("score", bestscore);
            //        paramout.AddValue("result", verifyOk ? 1 : 2);
            //        paramout.AddValue("threshold", threshold);
            //        paramout.AddValue("id", templateVerified.Id);
            //        paramout.AddValue("bodypart", templateVerified.BodyPart);
            //        paramout.AddValue("companyidenroll", templateVerified.Companyidenroll);
            //        paramout.AddValue("useridenroll", templateVerified.Useridenroll);
            //        paramout.AddValue("verificationsource", templateVerified.Verificationsource);
            //    }
            //    else
            //    {
            //        paramout.AddValue("message", "No hubo matching positivo");
            //    }
            //    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            //    LOG.Debug("DigitalPersonaW220.Identify xmloutput = " + xmloutput);
            //}
            //catch (Exception ex)
            //{
            //    msg = "Bio.Core.Matcher.DigitalPersonaW220.Matcher.Identify Exception [" + ex.Message + "]";
            //    LOG.Fatal(msg);
            //    paramout.SetValue("message", msg);
            //    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            //    return Errors.IERR_IDENTIFY;
            //}

            return Errors.IERR_OK;
        }

        public int Verify(ITemplate template1, ITemplate template2, out float score)
        {
            throw new NotImplementedException();
        }

        #endregion

#region Digital Persona Platinum Propietary

        public int ConvertImageDPToStandard(byte[] imageDP, bool wsqCompressed, int wRaw, int hRaw,
                                                          out byte[] imageStandard)
        {
            imageStandard = null;
            return 0;
            ////Los parametros wRaw y hRaw vienen seteados si compressed es false, 
            ////sino se calculan dentro de WsqTools.Decompress
            //int w = 0;
            //int h = 0;
            //int dpix = 500;
            //int dpiy = 500;
            //int ww = 0; //Convert.ToInt32(pSample.Height);
            //int hh = 0; //Convert.ToInt32(pSample.Width);
            //ww = 260; //(ww * dpix) / sensor.Xdpi;
            //hh = 300; //(hh * dpiy) / sensor.Ydpi;
            //byte[] byDataRawD;
            //FingerprintSample rFps;

            //try
            //{
            //    //Si es Raw comprimido, descomprimo
            //    if (wsqCompressed)
            //    {
            //        WsqDecoder dec = new WsqDecoder();
            //        short sw = 0;
            //        short sh = 0;
            //        if (!dec.DecodeMemory(imageDPRaw, out sw, out sh, out byDataRawD))
            //        {
            //            return null;
            //        }
            //        else
            //        {
            //            w = sw;
            //            h = sh;
            //        }
            //        //byDataRawD = WsqTools.Decompress(imageDPRaw,ref w,ref h);
            //    }
            //    else
            //    {
            //        byDataRawD = imageDPRaw;
            //        w = wRaw;
            //        h = hRaw;
            //    }

            //    //Creo Bitmap para redimensionar a tamaño comun para comparaciones

            //    //				Bitmap img = RawImage.FromRaw(byDataRawD,w,h); 
            //    Bitmap img =
            //        RawImageManipulator.RawToBitmap(byDataRawD, w, h);
            //    //	img.Save("c:\\aw\\ipaq.bmp");
            //    //Resolusion de UareU por default = 700x700							
            //    //img.SetResolution(700,700);
            //    Bitmap bmp = new Bitmap(ww, hh, PixelFormat.Format24bppRgb);
            //    bmp.SetResolution(dpix, dpiy);
            //    Graphics gr = Graphics.FromImage(bmp);
            //    gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
            //    gr.DrawImage(img, new Rectangle(0, 0, ww, hh), 0, 0, img.Width, img.Width, GraphicsUnit.Pixel);
            //    gr.Dispose();
            //    //	bmp.Save("c:\\aw\\ipaq_bmp.bmp");
            //    //				byte[] imgData = RawImage.ToRaw(bmp);
            //    byte[] imgData = RawImageManipulator.BitmapToRaw(bmp);
            //    byte[] imgData512 = RawImageManipulator.Adjust(imgData, ww, hh, 512, 512);

            //    rFps = new FingerprintSample(imgData512, bmp.Width, bmp.Height, false);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.ToString());
            //    return null;
            //}
            //return rFps;
        }

#endregion Digital Persona Platinum Propietary

        public void Dispose()
        {
            
        }

        private double GetScoreEscalaDP(double scoredp, double th)
        {
            double mantisa = 0;
            double porc = 0;
            double adicional = 0;
            double ret = 0;
            try
            {
                /*
                    2147483     .1%
                    214748      .01%
                    21474       .001%
                    2147        .0001%
                 */

                //if (scoredp == 0)
                //{
                //    if (th >= 1000000) ret = 100;
                //    if (scoredp < 1000000 && scoredp >= 100000) ret = 90;
                //    if (scoredp < 100000 && scoredp >= 10000) ret = 80;
                //    if (scoredp < 10000 && scoredp >= 1000) ret = 70;
                //    if (scoredp < 1000) ret = 60;
                //}
                //else
                //{
                    if (th >= 1000000)
                    {
                        mantisa = 100;
                        porc = 1 - (scoredp / 2147);
                    }

                    if (th < 1000000 && th >= 100000)
                    {
                        mantisa = 90;
                        porc = 1 - (scoredp / 21474);
                    }
                    if (th < 100000 && th >= 10000)
                    {
                        mantisa = 80;
                        porc = 1 - (scoredp / 214748);
                    }
                    if (th < 10000 && th >= 1000)
                    {
                        mantisa = 70;
                        porc = 1 - (scoredp / 2147483);
                    }

                    if (th < 1000)
                    {
                        mantisa = 60;
                        porc = 1 - (scoredp / 2147483647);
                    }

                    adicional = mantisa * porc;
                    ret = (int)(adicional);
                //}

                //if (scoredp <= 2147)
                //{
                //    mantisa = 75;
                //    porc = 1 - (scoredp / 2147);
                //}
                //if (scoredp <= 21474 && scoredp >= 2147)
                //{
                //    mantisa = 50;
                //    porc = 1 - (scoredp / 21474);
                //}
                //if (scoredp <= 214748 && scoredp >= 21474)
                //{
                //    mantisa = 25;
                //    porc = 1 - (scoredp / 214748);
                //}
                //if (scoredp >= 2147483)
                //{
                //    mantisa = 100;
                //}

                //if (mantisa != 100)
                //{
                //    adicional = 25 * porc;
                //}
                //ret = (mantisa + adicional);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.DigitalPersonaW220.Matcher.GetScoreEscalaDP Error " + ex.Message);
                return 0;
            }
            return ret;
        }

        
    }
}
