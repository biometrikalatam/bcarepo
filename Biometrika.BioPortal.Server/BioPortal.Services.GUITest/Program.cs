﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Xml;

namespace BioPortal.Services.GUITest
{
    static class Program
    {
        public static DateTime ParseFecha(string fechain)
        {
            DateTime ret = new DateTime(2000, 1, 1);
            bool _sigue = true;
            try
            {
                try
                {
                    ret = DateTime.ParseExact(fechain, "dd-MM-yyyy", null);
                    _sigue = false;
                }
                catch (Exception)
                {
                    _sigue = true;
                }

                if (_sigue)
                {
                    try
                    {
                        ret = DateTime.ParseExact(fechain, "dd/MM/yyyy", null);
                        _sigue = false;
                    }
                    catch (Exception)
                    {
                        _sigue = true;
                    }
                }

                if (_sigue)
                {
                    try
                    {
                        ret = DateTime.ParseExact(fechain, "MM/dd/yyyy", null);
                        _sigue = false;
                    }
                    catch (Exception)
                    {
                        _sigue = true;
                    }
                }

                if (_sigue)
                {
                    try
                    {
                        ret = DateTime.ParseExact(fechain, "MM-dd-yyyy", null);
                        _sigue = false;
                    }
                    catch (Exception)
                    {
                        _sigue = true;
                    }
                }

            }
            catch (Exception ex)
            {
                ret = new DateTime(2000, 1, 1);
            }
            return ret;

        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            //string s = "1234567890";
            //string s1 = s.Substring(0, 10);
            //s1 = s.Substring(0, 40);
            //s1 = s.Substring(0, 2);


            //string s = "srcei";
            //bool b1 = s.Substring(0, 1) == "<";
            //int i = 0;
            //bool b = "<VerificationSource xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><VerificationSourceItems><VerificationSourceItem><AuthenticationFactor>2</AuthenticationFactor><StatusVerified>0</StatusVerified><TypeVerified>A</TypeVerified></VerificationSourceItem></VerificationSourceItems></VerificationSource>".Substring(0, 1) == "<";
            //i = 0;

            //DateTime d = DateTime.Parse("21/12/2000");
            //d = DateTime.Parse("21-12-2000");


            //DateTime d1 = ParseFecha("21/10/2015");
            //DateTime d2 = ParseFecha("20-11-2015");
            //DateTime d3 = ParseFecha("10/21/2015");
            //DateTime d4 = ParseFecha("11-21-2015");



            //string dt = "24/01/2013";
            //DateTime date = DateTime.ParseExact(dt, "dd/MM/yyyy", null);

            //CultureInfo provider = CultureInfo.InvariantCulture;

            //DateTime d = DateTime.ParseExact("21-12-2000", "dd-MM-yyyy", null);
            //d = DateTime.Parse("12-21-2000");


            //System.IO.StreamReader sr = new System.IO.StreamReader(@"C:\tmp\A\rd.xml");
            //string rd = sr.ReadToEnd();
            //rd = rd.Replace("&gt;", ">").Replace("&lt;", "<");
            //sr.Close();
            //int s = rd.IndexOf("<TrackID>");
            //int e = rd.IndexOf("</TrackID>");
            //string tid = rd.Substring(s + 9, e - s - 9);
            //int a = 0;
            //XmlDocument xmlDocBD = new XmlDocument();
            //xmlDocBD.PreserveWhitespace = false;
            //xmlDocBD.LoadXml(rd);

            //string xm = xmlDocBD.InnerXml;

            //XmlNode nl = xmlDocBD.GetElementsByTagName("ExtensionItem")[4];
            //XmlNode nl1 = nl.LastChild;
            //XmlDocument xmlDocBD1 = new XmlDocument();
            //xmlDocBD1.PreserveWhitespace = false;
            //xmlDocBD1.LoadXml(nl1.InnerXml);





            //Bio.Core.Api.DynamicData dd = new Bio.Core.Api.DynamicData();
            //dd.AddValue("kk", "vv");
            //string s = Bio.Core.Api.DynamicData.SerializeToXml(dd);

            //Bio.Core.Api.DynamicData dd1 = new Bio.Core.Api.DynamicData();
            //dd1 = Bio.Core.Api.DynamicData.DeserializeFromXml(s);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
