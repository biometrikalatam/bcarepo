﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Bio.Core.Api;
using BioPortal.Server.Api;
using BioPortal.Server.Api.Json.API;
using BioPortal.Services.GUITest.Properties;

namespace BioPortal.Services.GUITest
{
    public partial class frmMain : Form
    {
        string abscurrent; 

        public frmMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            rtxResult.Text = "";
            picFoto.Image = null;
            picFirma.Image = null;
            picDocFront.Image = null;
            picDocBack.Image = null;
            b64Foto = null;
            b64Signature = null;
            b64DocFront = null;
            b64DocBack = null;
            labResVerify.Text = "Verifique IDENTIDAD...";
            labResVerify.ForeColor = Color.Blue;
            picQuestion.Visible = true;
            picOK.Visible = false;
            picNOOK.Visible = false;

            //ABS Clear
            picBarcodeABS.Image = null;
            picFotoABS.Image = null;
            picSampleAbs.Image = null;
            labSampleTypeAbs.Text = "";
            grpBioSignature.Text = "";
            labTimestampAbs.Text = "";
            labName.Text = "";
            grpBioSignature.Text = "BioSignature ??";
            this.abscurrent = null;

           


        }

        private void button12_Click(object sender, EventArgs e)
        {
            abscurrent = null;
            labConsulting.Visible = true;
            picFoto.Image = null;
            picFirma.Image = null;
            this.Refresh();

            if (chkCL.Checked) DoConCL();
            else if (this.chkToken.Checked) DoConToken();
                else DoSinToken();
        }

        private void DoConCL()
        {
            int actual;
            string xmlparamout = string.Empty;

            try
            {
                using (BioPortalWSCL.BioPortal_Server_WS_CL target = new BioPortalWSCL.BioPortal_Server_WS_CL())
                {
                    target.Timeout = 60000;
                    target.Url = ExtraeURL() + "BioPortal.Server.WS.CL.asmx";
                    this.rtxResult.Text = "Consultando a " + target.Url + "...";
                    this.rtxResult.Refresh();
                    actual = target.Verify(rtxSample.Text, out xmlparamout);
                }

                rtxResult.Text = "Retorno = " + actual.ToString() + Environment.NewLine +
                                 xmlparamout;
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = ex.Message;
            }
        }

        private void DoSinToken()
        {
            int expected = 0;
            int actual;
            string xmlparamout = string.Empty;

            try
            {
                using (BioPortalServerWS.BioPortalServerWS target = new BioPortalServerWS.BioPortalServerWS())
                {
                    target.Timeout = 60000;
                    target.Url = ExtraeURL() + "BioPortal.Server.WS.asmx";
                    this.rtxResult.Text = "Consultando a " + target.Url + "...";
                    this.rtxResult.Refresh();
                    XmlParamIn pin = new XmlParamIn();
                    pin.Actionid = Convert.ToInt32(txtAction.Text);
                    pin.Additionaldata = null;
                    pin.Authenticationfactor = Convert.ToInt32(txtAF.Text.Trim());
                    pin.Minutiaetype = Convert.ToInt32(txtMT.Text.Trim());
                    pin.Bodypart = Convert.ToInt32(txtBP.Text.Trim());
                    pin.Clientid = "IdClienteTest";
                    pin.Companyid = Convert.ToInt32(txtCompany.Text);
                    pin.Enduser = "EnUserTest";
                    pin.Ipenduser = "127.0.0.1";
                    pin.Matchingtype = 1;
                    pin.Origin = 1;
                   
                    if (chkAddSample.Checked)
                    {
                        pin.SampleCollection = new List<Sample>();
                        Sample sample = new Sample();
                        sample.Data = this.rtxSample.Text.Trim();
                        sample.Minutiaetype = Convert.ToInt32(txtMTSample.Text.Trim());
                        sample.Additionaldata = null;
                        pin.SampleCollection.Add(sample);
                    }
                    pin.SaveVerified = 1;
                    pin.Threshold = Convert.ToDouble(txtTH.Text);
                    pin.Userid = 0;
                    pin.Verifybyconnectorid = txtConnectorId.Text.Trim().Length == 0 ? null :
                        txtConnectorId.Text.Trim();
                    pin.InsertOption = 1;
                    pin.PersonalData = new PersonalData();
                    pin.PersonalData.Typeid = txtID.Text.Trim();
                    pin.PersonalData.Valueid = txtValueID.Text.Trim();

                    pin.PersonalData.Patherlastname = "Ap_" + pin.PersonalData.Valueid;
                    pin.PersonalData.Motherlastname = "Ap2_" + pin.PersonalData.Valueid;
                    pin.PersonalData.Name = "Nom_" + pin.PersonalData.Valueid;
                    pin.PersonalData.Photography = b64Foto;
                    pin.PersonalData.Signatureimage = b64Signature;
                    pin.PersonalData.DocImageFront = b64DocFront;
                    pin.PersonalData.DocImageBack = b64DocBack;
                    pin.PersonalData.Verificationsource = "TEST"; 
                    pin.OperationOrder = Convert.ToInt32(txtOO.Text);  //Bio.Core.Matcher.Constant.OperationOrder.OPERATIONORDER_LOCALFIRST;
   
                    string xmlparamin = XmlUtils.SerializeAnObject(pin);
                    switch (pin.Actionid)
                    {
                        case 1:
                        case 9:
                            actual = target.Verify(xmlparamin, out xmlparamout);
                            break;
                        case 2:
                            actual = target.Identify(xmlparamin, out xmlparamout);
                            break;
                        case 3:
                        case 5:
                        case 7:
                            actual = target.Enroll(xmlparamin, out xmlparamout);
                            break;
                        case 4:
                            actual = target.Get(xmlparamin, out xmlparamout);
                            break;
                        default:
                            actual = target.Verify(xmlparamin, out xmlparamout);
                            break;
                    }
                }

                ShowResultPP(actual, xmlparamout);
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = ex.Message;
            }
        }

        private void ShowResultPP(int actual, string xmlparamout)
        {
            XmlParamOut paramout = null;
            try
            {
                this.rtxResult.Text = "Codigo Retorno=" + actual.ToString() + " [" + Bio.Core.Api.Constant.Errors.GetDescription(actual) + "]";
                if (xmlparamout != null)
                {
                    paramout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                    if (paramout != null)
                    {
                        this.rtxResult.Text += Environment.NewLine + "MsgErr=" + (paramout.Message==null?"No Message":paramout.Message);

                    }
                }
                if (actual == 0)
                {
                    this.rtxResult.Text += Environment.NewLine + "Track Id = " + paramout.Trackid;
                    this.rtxResult.Text += Environment.NewLine + "Verificación = " + (paramout.Result == 1 ? "Positiva" : "Negativa");
                    if (paramout.Result == 1)
                    {
                        labResVerify.Text = "Verificación POSITIVA";
                        labResVerify.ForeColor = Color.Green;
                        picQuestion.Visible = false;
                        picOK.Visible = true;
                        picNOOK.Visible = false;
                    }
                    else
                    {
                        labResVerify.Text = "Verificación NEGATIVA";
                        labResVerify.ForeColor = Color.Red;
                        picQuestion.Visible = false;
                        picOK.Visible = false;
                        picNOOK.Visible = true;
                    }   
                    this.rtxResult.Text += Environment.NewLine + "Score = " + paramout.Score + " / " + paramout.Threshold;
                }
                else
                {
                    labResVerify.Text = "Verifique IDENTIDAD...";
                    labResVerify.ForeColor = Color.Blue;
                    picQuestion.Visible = true;
                    picOK.Visible = false;
                    picNOOK.Visible = false;
                }
                if (paramout != null)
                {
                    if (paramout.PersonalData != null)
                    {

                        this.rtxResult.Text += Environment.NewLine + "TypeId/ValueId = " + paramout.PersonalData.Typeid + "/" +
                            paramout.PersonalData.Valueid;
                        this.rtxResult.Text += Environment.NewLine + "Nombre = " + paramout.PersonalData.Name + " " +
                            paramout.PersonalData.Patherlastname + " " + paramout.PersonalData.Motherlastname;

                        if (paramout.PersonalData.Photography != null && !paramout.PersonalData.Photography.Equals("null"))
                        {
                            Image imagen = null;
                            using (System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(paramout.PersonalData.Photography)))
                            {
                                imagen = new Bitmap(ms);
                            }
                            picFoto.Image = imagen;
                        }
                        else
                        {
                            picFoto.Image = null;
                            this.rtxResult.Text += Environment.NewLine + "Foto = No existe foto para esta identidad!";
                        }

                        if (paramout.PersonalData.Signatureimage != null && !paramout.PersonalData.Signatureimage.Equals("null"))
                        {
                            Image imagen = null;
                            using (System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(paramout.PersonalData.Signatureimage)))
                            {
                                imagen = new Bitmap(ms);
                            }
                            picFirma.Image = imagen;
                        }
                        else
                        {
                            picFoto.Image = null;
                            this.rtxResult.Text += Environment.NewLine + "Foto = No existe firma para esta identidad!";
                        }

                        if (paramout.PersonalData.DocImageFront != null && !paramout.PersonalData.DocImageFront.Equals("null"))
                        {
                            Image imagen = null;
                            using (System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(paramout.PersonalData.DocImageFront)))
                            {
                                imagen = new Bitmap(ms);
                            }
                            picDocFront.Image = imagen;
                        }
                        else
                        {
                            picDocFront.Image = null;
                            this.rtxResult.Text += Environment.NewLine + "DOCFront = No existe foto de frente de doc para esta identidad!";
                        }

                        if (paramout.PersonalData.DocImageBack != null && !paramout.PersonalData.DocImageBack.Equals("null"))
                        {
                            Image imagen = null;
                            using (System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(paramout.PersonalData.DocImageBack)))
                            {
                                imagen = new Bitmap(ms);
                            }
                            picDocBack.Image = imagen;
                        }
                        else
                        {
                            picDocBack.Image = null;
                            this.rtxResult.Text += Environment.NewLine + "DOCBack = No existe foto de reverso de doc para esta identidad!";
                        }
                    }
                }
                //Si viene BioSignature => Lo muestro
                if (paramout != null && paramout.Additionaldata != null)
                {
                    foreach (DynamicDataItem item in paramout.Additionaldata.DynamicDataItems)
                    {
                        this.rtxResult.Text += Environment.NewLine + item.key + "=" + item.value;
                    }

                    
                    string biosignature = (string)paramout.Additionaldata.GetValue("BioSignature");
                    if (!string.IsNullOrEmpty(biosignature))
                    {
                        this.abscurrent = biosignature;
                        //XmlDocument xmlDoc = new XmlDocument();
                        //xmlDoc.PreserveWhitespace = false;
                        //xmlDoc.LoadXml(biosignature);
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.PreserveWhitespace = false;
                        xmlDoc.LoadXml(biosignature);
                        string abs;
                        if (biosignature.StartsWith("<RD"))
                        {
                            XmlNode node = xmlDoc.GetElementsByTagName("ExtensionItem")[4];
                            abs = node.ChildNodes[1].InnerText;
                        }
                        else
                        {
                            abs = biosignature;
                        }
                        XmlDocument xmlDocABS = new XmlDocument();
                        xmlDocABS.PreserveWhitespace = false;
                        xmlDocABS.LoadXml(abs);

                        // Check arguments.
                        if (xmlDoc != null)
                        {
                            //                        string sample = xmlDoc.GetElementsByTagName("Sample")[0].InnerText;
                            string sample = xmlDocABS.GetElementsByTagName("Sample")[0].InnerText;


                            int sampletype = Convert.ToInt32(xmlDocABS.GetElementsByTagName("SampleType")[0].InnerText);
                            labSampleTypeAbs.Text = "Sample Type: " + Bio.Portal.Server.Api.Constant.MinutiaeType.GetDescription(sampletype);
                            //Imagen Smple si aplica
                            Bitmap bmpSample;
                            switch (sampletype)
                            {
                                case 21: //Bio.Portal.Server.Api.Constant.MinutiaeType.MINUTIAETYPE_WSQ:
                                    Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                                    byte[] wsq = Convert.FromBase64String(sample);
                                    short w, h;
                                    byte[] raw;
                                    bool bOK = dec.DecodeMemory(wsq, out w, out h, out raw);
                                    bmpSample = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw, w, h);
                                    break;
                                case 22: //Bio.Portal.Server.Api.Constant.MinutiaeType.MINUTIAETYPE_RAW:
                                    bmpSample = Bio.Core.Imaging.ImageProcessor.RawToBitmap(Convert.FromBase64String(sample), 512, 512);
                                    break;
                                case 41: //Bio.Portal.Server.Api.Constant.MinutiaeType.MINUTIAETYPE_JPEG:
                                    byte[] byImg = Convert.FromBase64String(sample);
                                    System.IO.MemoryStream m = new System.IO.MemoryStream(byImg);
                                    bmpSample = new Bitmap(Image.FromStream(m));
                                    m.Close();
                                    break;
                                default:
                                    bmpSample = null;
                                    break;
                            }
                            picSampleAbs.Image = bmpSample;

                            //Barcode
                            Bitmap barcodeBMP = null;
                            string barcode = xmlDocABS.GetElementsByTagName("Barcode")[0].InnerText;
                            if (!String.IsNullOrEmpty(barcode))
                            {
                                byte[] byImgQR = Convert.FromBase64String(barcode);
                                System.IO.MemoryStream m = new System.IO.MemoryStream(byImgQR);
                                barcodeBMP = new Bitmap(Image.FromStream(m));
                                m.Close();
                            }
                            picBarcodeABS.Image = barcodeBMP;

                            //Foto
                            Bitmap fotoBMP = null;
                            string foto = xmlDocABS.GetElementsByTagName("Photografy")[0].InnerText;
                            if (!String.IsNullOrEmpty(foto))
                            {
                                byte[] byImgFoto = Convert.FromBase64String(foto);
                                System.IO.MemoryStream m = new System.IO.MemoryStream(byImgFoto);
                                fotoBMP = new Bitmap(Image.FromStream(m));
                                m.Close();
                            }
                            this.picFotoABS.Image = fotoBMP;

                            //ID + TS
                            labIdAbs.Text = xmlDocABS.GetElementsByTagName("TrackID")[0].InnerText;
                            this.labTimestampAbs.Text = xmlDocABS.GetElementsByTagName("Timestamp")[0].InnerText;

                            string xml = null;
                            if (!String.IsNullOrEmpty(xmlDocABS.GetElementsByTagName("DynamicData")[0].InnerXml))
                            {
                                xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                                             "<DynamicData xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
                                               "<DynamicDataItems>" +
                                                    xmlDocABS.GetElementsByTagName("DynamicData")[0].InnerXml +
                                                "</DynamicDataItems>" +
                                              "</DynamicData>";
                            }

                            DynamicData ddata = Bio.Core.Api.DynamicData.DeserializeFromXml(xml);

                            if (ddata != null)
                            {
                                grpBioSignature.Text = "BioSignature >> " +
                                                        (!String.IsNullOrEmpty((string)ddata.GetValue("TypeId")) ? (string)ddata.GetValue("TypeId") : "") + " " +
                                                        (!String.IsNullOrEmpty((string)ddata.GetValue("ValueId")) ? (string)ddata.GetValue("ValueId") : "");
                                labName.Text = (!String.IsNullOrEmpty((string)ddata.GetValue("Name")) ? (string)ddata.GetValue("Name") : "") + " " +
                                                (!String.IsNullOrEmpty((string)ddata.GetValue("Patherlastname")) ? (string)ddata.GetValue("Patherlastname") : "") + " " +
                                                (!String.IsNullOrEmpty((string)ddata.GetValue("Motherlastname")) ? (string)ddata.GetValue("Motherlastname") : "");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = "Exception = " + ex.StackTrace; 
            }
            labConsulting.Visible = false;
            labConsulting.Refresh();
        }

        private void DoConToken()
        {
            int expected = 0;
            int actual;
            string xmlparamout = string.Empty;
            try
            {
                using (BioPortalServerWSWEB.BioPortalServerWSWeb target = new BioPortalServerWSWEB.BioPortalServerWSWeb())
                {
                    target.Timeout = 60000;
                    target.Url = ExtraeURL() + "BioPortal.Server.WS.WEB.asmx";
                    this.rtxResult.Text = "Consultando a " + target.Url + "...";
                    this.rtxResult.Refresh();
                    XmlParamIn pin = new XmlParamIn();
                    pin.Actionid = Convert.ToInt32(txtAction.Text);
                    pin.Additionaldata = null;
                    pin.Authenticationfactor = Convert.ToInt32(txtAF.Text.Trim());
                    pin.Minutiaetype = Convert.ToInt32(txtMT.Text.Trim());
                    pin.Bodypart = Convert.ToInt32(txtBP.Text.Trim());
                    pin.Clientid = "IdClienteTest";
                    pin.Companyid = Convert.ToInt32(txtCompany.Text);
                    pin.Enduser = "EnUserTest";
                    pin.Ipenduser = "127.0.0.1";
                    pin.Matchingtype = 1;
                    pin.Origin = 1;

                    if (chkAddSample.Checked)
                    {
                        pin.SampleCollection = new List<Sample>();
                        Sample sample = new Sample();
                        sample.Data = this.rtxSample.Text.Trim();
                        sample.Minutiaetype = 24; //Token
                        sample.Additionaldata = null;
                        pin.SampleCollection.Add(sample);
                        if (chkRepiteToken.Checked)
                        {
                            pin.SampleCollection.Add(sample); //Lo hago para ver comop se comporta en enroll con multiples tokens
                        }
                    }
                    pin.SaveVerified = 1;
                    pin.Threshold = Convert.ToDouble(txtTH.Text);
                    pin.Userid = 0;
                    pin.Verifybyconnectorid = txtConnectorId.Text.Trim().Length == 0 ? null :
                        txtConnectorId.Text.Trim();
                    pin.InsertOption = 1;
                    pin.PersonalData = new PersonalData();
                    pin.PersonalData.Typeid = txtID.Text.Trim();
                    pin.PersonalData.Valueid = txtValueID.Text.Trim();
                    pin.OperationOrder = Convert.ToInt32(txtOO.Text);  //Bio.Core.Matcher.Constant.OperationOrder.OPERATIONORDER_LOCALFIRST;

                    pin.Additionaldata = "idtx=987456";

                    string xmlparamin = XmlUtils.SerializeAnObject(pin);
                    switch (pin.Actionid)
                    {
                        case 1:
                        case 9:
                            actual = target.Verify(xmlparamin, out xmlparamout);
                            break;
                        case 2:
                            actual = target.Identify(xmlparamin, out xmlparamout);
                            break;
                        case 3:
                        case 5:
                        case 7:
                            actual = target.Enroll(xmlparamin, out xmlparamout);
                            break;
                        default:
                            actual = target.Verify(xmlparamin, out xmlparamout);
                            break;
                    }
                }

                ShowResultPP(actual, xmlparamout);
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = ex.Message;
            }

        }

        private string ExtraeURL()
        {
            string ret = "";
            try
            {
                string saux = (string)cboURL.SelectedItem;
                string[] arr = saux.Split(new Char[] {'-'});
                ret = arr[1].Trim();
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = ex.Message;
                ret = "http://localhost:4030/";
            }
            return ret;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.rtxSample.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Seleccione sample en Base 64...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    if (chkEsB64.Checked)
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
                        this.rtxSample.Text = sr.ReadToEnd();
                        sr.Close();
                    }
                    else
                    {
                        byte[] byFile = System.IO.File.ReadAllBytes(openFileDialog1.FileName);
                        this.rtxSample.Text = Convert.ToBase64String(byFile);
                    }
                }
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = "Exception = " + ex.StackTrace;
            }
        }

        private void txtAction_Leave(object sender, EventArgs e)
        {
            labAction.Text = Bio.Portal.Server.Api.Constant.Action.GetName(Convert.ToInt32(txtAction.Text.Trim()));
        }

        private void txtOO_Leave(object sender, EventArgs e)
        {
            labOO.Text = Bio.Portal.Server.Api.Constant.OperationOrder.GetName(Convert.ToInt32(txtOO.Text.Trim()));
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
           
        }

        private void txtAF_Leave(object sender, EventArgs e)
        {
            labAF.Text = Bio.Portal.Server.Api.Constant.AuthenticationFactor.GetName(Convert.ToInt32(txtAF.Text.Trim()));
        }

        private void txtMT_Leave(object sender, EventArgs e)
        {
            labMT.Text = Bio.Portal.Server.Api.Constant.MinutiaeType.GetDescription(Convert.ToInt32(txtMT.Text.Trim()));
        }

        private void txtBP_Leave(object sender, EventArgs e)
        {
            labBP.Text = Bio.Portal.Server.Api.Constant.BodyPart.GetName(Convert.ToInt32(txtBP.Text.Trim()));

        }

        private void btnSetAx_Click(object sender, EventArgs e)
        {
            //this.ubpClientAx1.AuthenticationFactor = 2; //huella Digital
            //this.ubpClientAx1.Device = 1; //Digital Persona
            //this.ubpClientAx1.Model = "4500"; //Digital Persona
            //this.ubpClientAx1.Tokencontent = Convert.ToInt32(txtTokenContent.Text.Trim());
            //this.ubpClientAx1.Operationtype = Convert.ToInt32(txtOT.Text.Trim());
            //this.ubpClientAx1.Minutiaetype = Convert.ToInt32(txtMTSample.Text.Trim());
            //this.ubpClientAx1.Typeid = this.txtID.Text.Trim();
            //this.ubpClientAx1.Valueid = this.txtValueID.Text.Trim();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //if (grpFacial.Visible)
            //{

            //    this.rtxSample.Text = this.ubpClientF1.GetImagenB64();
            //}
            //else
            //{
                //this.rtxSample.Text = this.ubpClientAx1.GetToken();
            //}
            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime d1 = DateTime.Now;
                int a = 0;
                for (int i = 0; i < 10000; i++)
                {
                    a++;
                }
                DateTime d2 = DateTime.Now;
                TimeSpan dif = (d2 - d1);
                string s1 = (d2 - d1).TotalMilliseconds.ToString();
                string s = DateTime.Now.ToString("ss/MM/yyyy HH:mm:ss.fff");

                cboURL.Items.Clear();
                foreach (System.Configuration.SettingsProperty item in Properties.Settings.Default.Properties)
                {
                    if (item.Name.StartsWith("URL"))
                    {
                        cboURL.Items.Add(item.Name + " - " + item.DefaultValue);
                        //this.rtxSample.Text = this.rtxSample.Text + Environment.NewLine + item.Name + " - " + item.DefaultValue;
                    }
                }
                this.rtxSample.Text = "AwAAAETqFGuIDHoLAQMnmpa25EzfV+aSHPT3afleoAzfrEFr670TiKwaK9FSWjaThD/g/Pg0UCMH85uzxBYMyCMXm9kVP8rsqFgDY0+QAZ1xhEjXOkcNxVcza/WG9WqQQsgTaE7G7QRENcyJLnC7cd2+xbr9+56n4ZrU8Ve6T6n48ENXqURy8Jzrn80ufjznlR2T+cMAdbXfQK/ogID6tc/bITjB7+EZohj5dZQcJxtImSQEzwGwOam/HdwBURLCmg0Dd13VaoFNN6FvPxSH6b7l6gdpQkHzO/kGL5XhCboAVR/RhlwUbPj3d5rJqvB4Ol2BU5S/BnaBAQb1AfrzYf3s5PRxxfk2uhSQuWKxdV/7mute1D2uApcvuKKuFnNa2M6Lgi8Fq/JrsoBJQ+ItBC8DeUK0bJxwBoMr8VabpvGRRWk565zU3WN4l/uv5kEIqCW8pblZKqqQDwrVYtsbb92qzYW55y3Oox5gR2f78Uibbp7MRJBOTstzemLgAHJJcLb7y4ODtPB3lEMqFTNUBUdDC4eqnviTRuDcRRRTBjyb3zV34gYb2rDdWWKlzmKLNWM0kylQTKXwMdU1fcdXRGdIBWUBvWjsfYYA0XRPzCxE7GB8aa6LjXKa9EvFPbglWwA38E/3JFhacEOznFK2rvrF0BX9XdATvlVuhPj5bbIkq3R5US7QTHc684RSNZStOcWV0eUAajT3b5M/1GC9qIsN/+qmVVqozg8kSM5tySAXVeGxm7LPpi+w3/M1qRy4j0941PzhH9GJ3lq/CFXLco5m8dWH50lAfzyUB+g7HgcNSW7rT3gMFXSCXuO5xwQ+tXbdhgvoUhugCmbJocnMWHf9zu/oowIK2JXQ2j8ZL859p6lXfdyhZMJfE0b9v525qpY731ca8cr9jGuNJd1F5PZKreq/fLo5/oUp9ZPu2+rTOTzKlmLbvML5Rh+6wf/0NovD9nmaKCnYJH+pV9CLauM7EGAWuAQNkZ1Ja91tDvMA5qr0HZgtlFg7e2JvFoALhLaLuekWMBkUqRoQ50ujy/ruai3eQ/G1jPydnkGnNAP2GmRfivTpmshemU0rUGsQvXtbGGAXzSNGhFQct6BUas/oDYt1R9sTCqyS/zo/DLG4bgvYw0PdSbMzE8d5gDrmWxgy8W9LgjKF5vVM/Ms7FQW0WW05XN67cX2BHxOt0i4LggLiMcmBaeMbvFHwbJGYXdkef+vtbUTGZhqrfwk96n0d+M5bJlYzQSDBCESqJ85vebc3wKE0flXXbHiuEdbGy4g3ZSl7dV7ygTb9+BRr+xLtyjLig+j38eupCq+ucj9bCqJ5jkEGDq4XjBkZMSVDfVZnEb/0NTwVy+Ig/NG3eLdRpl5Nk7vArEn8v35eqfzqJf8Me8KnSVgAQADV1Zy6XIJsR7W7/a5CfaWzq+5MmWLhhKFuVOoL/Faj76+9PHn7wcx0HJUUlh/a5fznIvN54EkhExu/bcNduNKhBgFoBC0jAGkJ/odV1LlHqtQzYy+QN4P0RaR6jHQ8D1smheLHYQatOVSAVJ3XUaLHAV1QZ1TANcJNCuZNFq2xFWGz4gTiWCvlJ8Td6tVTZUCqBy+R2yn3L9RPJWLTipaeBscB9RzIcRrqZtqK9eX803QJHoHFt8smaNEGevYD4XsLRg65KZVLCbpfhgmV4FWtBYlZNjtU30obywB8f8rhn/rB2jKyc5ejotgQzgpaZdm0kY4SlwAkYGegGW0KacKFusTLVvjlpBI3XC3dj+2VNHqbq9NuY0QAgfTGNOmZ5422mD0c30qrkokfZKc5e77zGyO3/oNmGABLxp3Thenm5gN7SWEzxutYQdVkewJaUpACbW7thu01dpwRet61sTiVxxNAv7+jiLh4IgagBmSqOAHreqAX024YqnCtDR0KfWxQ+fXim1gKREUXUw60kM5Ct9fBuS5t6GYPvL6lJ5sVTVG+yUaJscNCCZQlZ5eUsGtFg4r9LjqdpdioKrcVVXuxzA8qlHRtznNyR6NiiqS3/FF1Ss4JtcuwxCoR/5ELM15QksP8y/mj2UCwMnRnxgX8ev3I4tOaKVJqtj0PV/mxEqh7T4q0hyPu95p7qbpp3lMBNBn0KUg2t5km8eOiaF5NuBMInJUCVAYyVkV/9g2qdk4RQo+hcmai+6ll9l8k0JT1n4HTvbieFENAtkzfEGdGuV8U0rJmxLaGN01zRd716zy7zzUi3fGhB6fzYWMnnLOz2vvdqab9uEijFVsKUBBgI0EicbFwkcs0U0hRMHV93UOnxULf2Kcc5N4dG/K3ZNFCKn+WFCfjSF4zAV69slbbHly0sT09pAM4vhkrqSahjxbsajf4W2ql7pOeIPk/lolFdbWQZa7wRIpQbRH32PnZT3fjQ9Agbnw6qhJmsFRX0ADRo8XrK2x+hxUhz6U9Te4HY3oFtUyRvwYNkdGU8X2aI74/YVV+zgQANtXnKBCEQ/2bI6eeoSgsMCH530QSSropdWNDfAY1kIwUklkH4F5mrwzApInZzjY/N4/5J/rSRASaUPZPHFlkHXdCLhaSx5EBxb30LbTJauWx3IpU3El1A2ACi8biuVvvIEHwcCFaOd38BQC1Q1Z9C1+4AyQxxhNfK3pNv4/hCt5/GbKUeWYPfCYWTWqeXW/DfIkYxqSyQXyN8X9cn9t99M4xLoGjCzIwhAGrlFbWZ/GjfV/Iw2IvEo6hIEEq9XFT5TtMPT4hH1LSZjKJ9XxUmN8TWl0Qo9t5yPmtRaHaryeVjTfMNwhnJ/668/obs5go7EXB/vjKRzFS7u0EaNVSTKNWBNNO+7Z99M+lFVbKnmhX158EaS9pK6z9H/q60bX1GSAetPr+fXaPlnh+P8+hoYimb97HXCeDZFsKHgemPwb6s8o5W3qWGf6nVBqbxkg9wUzBlPuDydU2ItfJzOdoBn+VgfxMTUBIrYuwAgxrOfzRrkHr3cX1r6Gm8Rl2OLQ9cdDDaJRSwhyijk+TFRpdK5qAxSFAZF+UicfomsnKs8CVy3DTOdRldZUmWUjzkFkY/bihllmHAPeHiLSQhw2VJ2SAkt5H6m7mNbI1POgSKS0s8cLGyV5ZaBYXn/f/3qWH5VdB+Mz87bdDwv3oAnlormsCLe3VBgd/YYAwFaWDv7Lh85yTzUax8XrWcLP2hSzWMLYi3+1Bo9NjaRvNrvC2Hd5JAJqLcRmwFUNF3ff3zA/SrYON9p8P7KAWJhdQZKg+hcj7c+XHX/G3AI1+hQ/iXC9nqYRoVkIQla3+EfSTlibvURtvaBrDGKxctiDesMW74eQ52I4+NNiXDgOiBwFXokXP07Ka3hPE6MYpAmFhzsNtuVeZrbGF3xo7mQAnzzUfJAPlPKdRnaQPLY8qvFZ/XS/yaoDwmNzheraoosHOjG4Z/yx1ddXDsadEHYv9aL5zV54KmIvogoFxkpPM7Y2Qc5BKcSuXBzFq9XU/Fue4dUavgoI4TnUjHAeLr9ZHc6y3pdg3P1DBVeoFxiQ7CDjE7PsAE7jfLY0bKMyTFRO/UxjYajUMHC9P2qEp09goDHs311rNepfQ6TuheU409bXaw0k4F2kUnBILHW6Tx6HJZayekMv+U20oDsR9OjtyfJZnMbpHYqh/RhL5EVwhUHXikzoHf0Nc5Q4o7Ey2qS/FnfhHq7ZzDdS8/a8vEXhT/0kUVQsUW3IOR1AsPa/rj2Kewcax+VQYMYEm+vmqxFd0d4aCbtmG3hmLrnt+VhT6KGcRUoWtu9UV3ehrLKv78jf0VkSZ5kWYRzjZ5wLUpAV5inwe5CnWeLHfociFABdA5Y7uZhoU6Ubp3VsHbe7L6kCpxZwu06uDiQ8OwOKn1DccehocObow9OMQsaAtqIG8bgZRBWbUZYJBFmLhV2fggnUoHJqSQfV8rSMXKU1ahRkM5tFrUf2UGHn6vgaDHC3BHMJ6Vjdqpgemp9kZBlmpuwsAyEc3lWtrEpwRASA1el588/wrGUT2YZeoUj/yzhkOY3ptzML9B6zhmFVBRZX01Gx6IOja4wvdbfMHYs7TtNrpg01hPCEpqX64Wnst6oLbNhAkwgX+KpKKP6mdzi7QhVpmufGrn7Hwduw/haOAeij+y2guLnShWugInGj5sNTYd95w+SvRhQCz+LkTKYL8dTlXektH66OfnyHT+cuLeCti6MiT4CZZFI6oD4huWHVmhH+m4LPybguCH9KCudRGgu0BDSCbvMqUELBCixPKAPX6pDlqFy9sX8u4WwefWYTQFMB4fIp50hxWJyEujOPH26LW0xJpDs/GLnJswp4QtLS1Pus/ITgpz93e9/4n94kt+8BZe76tebxoN2CdDg9h3jwz9oCKIsHV7PexAktbb/1oWG+xXM7jK2o78rjw9+sCRu4MwTzrSijll/ynQtBKNRHkPnei9M3lBF3SiGJ8+7kamDN2n1TN+R/Y5CKreOMnRQ14cu4KWS5WUhlddUNN6stgj0gdfMKck4m4en4Kdc7jWRt08saUxZriRHZ519QMRxGgycR8mPGO089nx7EUcVB5iwC17rqALnm2Gf9RcVXQLZsW5iUea7JrIbOyUrTA1AaKmhyWz7ZWmciduzh1LjiN8kaLDQE9ZRDE2o2AhfdvUYdIPU4UGmERfK8/yB2Uj4oasQizLbWuyvLGm/kZ04JnJXDJccNCehiPWeIepHVhfFbtPiEtnIlXSGlIqNqv/NRB/AjSwP7D6nMZWw7sdExQ+0S8r2plnnejVxUFA4zN2pNxDgZhWL++ICYgjf8Px+88r/JKzgc+DJHMsmEtKLePla5XQIazr7hE/sAlXxmqNedxW6WUpc2qc41PSNj/s2EArbxFn7CfBI9hYQ0uPSvZeZCm2wZlpq+pLlMlZWa842LHxGxOkd0RJTHv+OQvDwD4DI6dYBGRLgyyBGKm5gFY45FgKsMACi65QAu7zJYVjuZNUxYDiCIPNiiNrdiXHJClP0wcCOmcRSIorGCY5CxyfF/1br3QCbPUE24S+KPYSmCmZ9ZTwScdx0B9yswxtcZHKaizo+q9M5jcGJ9TmAtRrbm3lYsYFD8u9X29Swy76IZF7y21/Rf3HnMlp4TG9hmKRctlo3FbOKdCwbwa3X/8xkvB4BSKUKfjcw8XPGBAq6zwQyn+7bTmrHHrUzquyHNv4MWs1W3L6hQQU4YhVLuIRgTuCM/J+mBW+5brleflE8FdE23WKrp/PJt1yQgQeqVgsXK83VAO0eORcxmNZ1n2lQHxnoxXxdNRF5xckFCbgVfghEbqngR+7UHwSya46jzO+4OmvOUV+Se9HxIXn7+4PigBP/A6eQC0nk3LIBMMJGP/cdqCoL+n51MbOGI5ddmcVMFf4MPpwA8/48l8uVpV8OEMghRfihNAwHkB/sTtaJb+joKXSATJYLvOMw4fO2GFnMQbGoVFvbTEkfx+H0pHsi4VZ7JlEpiPTnnNB3n07aSLB4MKFtwI10aOkiVLr5HC2kHJ0mIL+uAd8pT6svNuUhvjV58JCXImWtAyta6pS2zhCDHrEG2KyTAwDclzG5bxshX6jxpLsWbe7ESNasmWGDowsJMWpbiR3WZBbzBRZrvLHjDj/jy57QlUpEbBBzlS4qujmklkKqh2ft/uWB0zl1xSzztfEqq8h0PQh49i26fOJI/0qIP0YCL15twTW9D/EQlumTae75hMa+M4zZgFsYr5SMgFJ50TL+Dtn+oIEuqw7reOKbE44GWZKtGXiMF81w8O7bXn9tyqHfqfAUWtIE/qYsdLZ7l4EtEQlC/wRUF8CEnacURVhgctAXH58Rai2/zmqi+YESjWmpuEFS5xlB4bt3vG+is+1Z8BIeBXu5mDDZN+8l0UGcJAUvGi6Dc0M+E0Fkv+xt+dXzaFbJLnI57bYu/wgbIRsQNsrTdwF9nDp/DN40WS5hlFhGbgj9kSs2PS9XstcHAVdNByii6xVCiZLQesF6B6Tnz6XiNYY42YlTRyNt6hCWVNbr13sOmQ4rIC5P2dtIAsNg5gScWCinYha8Le1WILaJvrXvt8Na0d4IW851CXVi9lP4Hzb8JbB5/5QRW3KBc1uBw/ccSdW3ctjtEfRushv70OLxTHWr2hue2Vh+stlSHePkoAkL+uxIwFkd3Bb2lprzjU0kxDbRsOvyV3l1XoJ7ZjwenHlRIZtgeIRFro6/n2CycSBXw2xKmT6atUoO5qc1NkU4Z1j29SRvySkkq/q7jmoOaPhxZ5Njr2p0UeShtfIzJ+EsFJVRSDG0mS3bP54sYmxOZxQACTQz5fo/RglqYRakn8rjI9qJJxc7Sex8mhx2ZUkUg/FawavARLeqtGbOnsl/vEhyGrA6LlX2lVWc6WyW229fBunmK/DHxxW9wD42Qr81835OyZOfSqFp1V8Wmc8pN2lHmIMTRlnGqGSyB36VdJG1Z8fgL529cojCrwMEg7H5WDNVL/bjglH3SLk7OrWdOc5CwhgBlwfMkHxi9fEOs3T2TVeHkJNcWwgDmi0PsNz9jZaBVNRWZL3Latlen6yUo8Sr8vQrlI5wTXEFuvulN5IEa/VmPQH0kv0YjZK9bqhz9x735RSacgzp55pvZdSV+6AoxwgU22LeDknq3hR/zwx7ChBo2B6T3FD5ue7ttE9FxBxwKHHDfkrSyPYx0ZGNso30oTtY51YaHXi6APi1oAwjEAjGoliBX//BGgobIJLS3VN3M75+6AnmkhrRPLJ4LbNUEQ3OBGhU20WSaAfzrH81UMs7mjVdXbc1+KCQiUdjKodBeaIImShjepjyMcfuxy+p2gTWF807C3MGbUWaoZRSBXmVP5prRdT3VDOxIhwpxD8m1D0N7CzptxC5+MLJfo0V+t3apGHY1fE3fIRrIb5NTbr2WHbFEcqsT1GyVr0H53Hw66WVm69/q1HWjqiqkg0ZLkj0/AlgUfSpPXR6NTY81yvEMiUEZV/FSNwhwcQzNEQpHLP7yuB9sh8G08+7BkUmpD8FqZKAAmiI6n//DXx1a2GkXqgsIj7PK+N/neu3C9bcbSpZkR6SARMaD5E5HtWU7Gcfba/9xK6QQdVOZ3IKJodQ8z63hT6yCkYYJJELBkKlPzWlPmTRaGYid/lwPMsuhSIo65IKZSwpRgLIWerSB9PAEmdU32Z+WD6zOMGjYmuB+81bSycl+pBKmgr8/ip3deAEkgCGue2OUEegaNCu4+1hteLA3D5QEqsVkhNl5QdCyRR5BuvSgIYXjdMdLgWctkW1kG+WA6mozUZzDMOu6ozwH9s5t5uPKs65A4+AcZqee/ygavnDWhy9C100UdDjAJLfYJmq3h0Qy7BndZz+ZR9URhsPcczwdCSU0yhcU/hgLiEgjzSBhEOzmPI7F0dxBPrdxLUro8/9gi16I1yYuXvOQ+U6YnJ/UyMYwhhzF/NfRgsr5Rdvv9WFoVeekjGdeTuKXw9dsXgw0c5vH2T7J+g7Fb1khg5mTsLFDXtYjTMs/4+p/a+l/nnF1dbdMhW5tlrG+0QmeusCVCR9RuO1Ggmn42zi3ZNGSYaRjNg7IPwyh+Xy3G0J6IuRsKap0ye0Wzr9W2jN1sGEnsg4zYJzmJ2BVRcUQLNy46Fvk4NUpyGVuB2LUTFI9XJtlIJv8moFNr8UDAnRC/o5rMyzSXhf0ksJxd8//HM9o0OvgPyqX7AouTNMQU2+Kc767Rg0L9lJnwiPrFWQUJrZV6UxM8eMTLhORdH0GJgINlWAjmIHN5QZEPHkC2CbZBV6iJFanQEvhi1sniTA+s38T17ejNjnqmEY+exwoQpudWmtpBHSjbHve34kLPpTouHLF6bbzayyXbCWS+CRz6Bo4O/1X7+45+e6AJhgiraNoMLg9sRmVu2VUKdcQMlpSF3Q2hmJyuXSTE3Tn24bK1NNsW9fSqTj971lfbpQvdw2Xu/NV5/KDM/ipWnJliyobCl2IOThVHxgVe0x43H4hIgEpqyCeSOrF4p6ixy5z/cVXWXnokaqNblN9v0YRb5fkXNhdEfm8WMskaFc1fp9dnl7oG3hCrgh9Dy+Bon7JbpbUaL0y+4MtD8TyojHXy0ye+d8Ua5/m7mFqvrIz8v86RMmdpHBPntniYrtLcuszysy4tsebMn+0OnzJmc6rCJkpT5snRsATazzCeyiunzbgeZMvXezjzR1GjxyYP7Lh+oXgslTyGUEqzz7adphZeWXlc01ge5obSd+4Zqe1Svmp6idVbJ9oQ0F+yzJRmQXCOXaMz2SpikRFujnSoMD6l8pSx2YNblQCTiLMxBUsfwnOXDeT5pkQjtquMYBf62N2kG/K/FNSGHHhcCwvBlC/EujB13puwTb4psq0wlUXdlM0e5U8iqFCYK5166mFQHgefKocWDQDsT4zQ1JumwTpVRmICzjZvpQxydvgNUBdMLognstae/mhREQteZBq7cdxrdp9eXtLjXEDimTLpjycJS0xDeIxJrkjt7GMmufzdmVjeq10e6zWaeWfNAfWtogXYMIeJg35BsmTPa3Ov3ArR1oYomR9e0r2W2LYr4QXa8se+4LS02yjdDJkSHdbcd8Eq6pFziHHyMaz3B2eKKcTBXwPeaGfVYwYnHbl9LJCx8Hit/4J6zPJYzhcrb4FcvSii9T2epvlhmcTTl3E0yDQVABd5Wn8NI4Y0EfLG4SGLGqABhzQ5tI1y5eEn/nDAjICMUXDl9NJNtGZFSR76EO/b3k13l5UvYE7Ll29k59NNagSiANLZP1DUeVF+dsw3fMJQsNRdCGJ1tVhAxBoUkyah9BlH+g8wJfZmfXIC9nSIDuQDSYMkZ9CUafcUNGdAb+EakPo4isZK8CtxKVnkpRuQVewEiedZkHc6b1DXD3bHSaJ2ERUAMJUlp8j4kZyN/WKc3DbmbWJRMnd5nb7uSB76w6QTsu81VKjP9wQZWCXirkCnsdI+FAa+96fGOe9c/kfndm1r+d9Gp9izoPTLi0iv2f4SN6s7Q0qmvKkXIEaTaZwpFUAe3mhODowXK9FmRocbnGeVDLU224uIsG7heNMTJtiNheBwLwKKnLhNuXrEeTfy3PvNgDkjXP4W5TdjPDmX5UliDv2iNl8mDAH7cvvQwbKLM00d5QUFkdKvLDVebWxFNp+cizI6mGK90oRjDHj8u1/EYakrWUVxpwXegEFnnHRy69KGUGtl0+FQnGpgf12n8AKUPg7w4fGkPMYMTtKZGvG4qOBvkzltMKzOi8YBFVMNCz9Uy6R+p7kRAQX6G+zp3hvqHWNYfLUsXZDSNgP9qeUqWrD0qYiAbsMwbm2w0WBPYBPl+tDugL9MZ2GBmeYnBGBU8pmp+wE3u6b2sxUcoZh9aPN6dVJys2OvRElZLSAdeRhixfHiJs5jJ/pctZK+Ri0u+tYF6cymH3YDWlljcMfa3M/O8rOj8JkYLgtEajbAI144o3G665mIYiRuesFUBINpRFAChSwvxnWGoc3KYbqULY0FQuST+cYgc9udKx3bHfFV6bl3OuWtYvsTjmvseBgvo8z2PP8GZOqNMQYdogQbsi+WZkhNBDX6v0anrYUotg1WWsj3og1opYjoPEJepQ93kzQ/IWbtxtkljwwS1LiNDpLHgPP1QDuLIOtO8SNVtXNe4NjEez3qIjo31F3rPYavczRjK2HLeo3loaqHCxGSPxuaaRUJkISFA+S5mK29Cta3D3A9G+Y2Wx7d00887BWXLvUubs5pbwM+O+43VtJyWwIP95lcwq378Az02kNko+Opi4ONpkKjvZLLn8MxZl47BWT/DriqNW6jzNGM/r4JBFSmuEd9/zf9V/UIdpOF4AGYfg8jzNiHt8r38C4XksHvC0db/wersHdTtxWnXfV7O98pTH4DBaipL73FfjZpanh4DD6PAOLwQ/dhCja2bJisvSaM2FZlaH43LlWKh5EEzFFZZCJsbrm7u2xZqAfjT7bPgl8IoTprHgHm5oQhafklmazec/i3wTQwZgM0UPUVptkmObGUiam2uVu/JFDpOlFdPCOvA7+ZRojF09rv/tXTDK4AJKoNqD38abPewDNJaLGfcyFVv6m7+q/9drbtudc5x5yHb59rYtPxvgrPJGE/SYniKew/pL289vzTN1k9XhcjikjaaDIMjzDmf/9tFtcGw5HbKJDhU+HTF/NzWxHWzAEU/TNkhA4VQXpS+qHabYiEVwSuk2ppzlbLYSDBppPq6cvA5YUhiGgHtPeXx/DV2TfukAulLVjzkOx0IIvYE8kui9+5LoEBtui5uyvTvpaCpaG+Cje4CWT/adj371B1tVtY7wVyXLg/XvYNMR9YMGd0c9pWWBvm46On+PfvIU+OTbQWTfQHJbzIVYtOCZD/9m9TbX4Ic4UXiR8C41NuJGrCCAauW9n1pfx8fCD+SIJdgs9zQ5qxSUhhMZklzYyHUQijr+nOYMdnzIYzyFUWDU6X6m9t53SG/6BLQInhvE5lprafMZ3Diay8NITHcF9QqvbrzP9iFsrY6rznMIc716IpixxF+lKOG6fGLi4hj6+CqTTuMNfU/fqp6Vv/brmFSDxgI1X9oOOg1OP0fNdzeqhAAh313uMvhHojbHW2AsEc8P5S5qLMnz0N5D1McfIGc1aYT4sV513JR9fZq4fmo3+MxpLb7+GKSTB8pGTV6IfU6EizQY5ND2Y4NU8ZcQ+YbCdbMdabkiuP0p8PEpWCRbi7KmxqI4fHNTJrMBpcTT9EWjZf3WCORaxGCvNRmPI9LzDtHmXrl5r/qHXj6Wr2kvBBmjaJPXKz/F3DheynFnUNqacwL0oYuNJLC/yZ85FFlmIvnlMrMfqVL+MbqsInERo70Toe6Zk5yf/tE8mQwI2qMIPUe62ijUvwv8yAD4oCABzlJzQnwIL9sw8pq+bXr08QOJp+9bmD8AfiPSRuAIOAg4BzPYA50oj28vxtbZXX//dQLSxIxBdv2X/YiDnKBY6WcwNQmkXcE67d2/kTkh3t+BzBE7DclPi8kQYEr92AkKAed6gkrPDv8BxKVYQFZ5MMk8HtjsTnq9d+XGR8CmY1pynEtxrSCjBYG5/3EwJQn324DlHKJGgWCkHp99u5SLuFRe5P5lTOrI+HOqSFRvTrHnl+irXBZiLGfZ7X42nNjuS/5YC6AISe+V4oov6YaD6El0SsDGzSxUzqp0v5JBjoR9vuBMG18ZviDkKshFCMvJw+WUPWJWeOztjOoRo0mN3ReM+XqyMev5h82SbxwmBceaPPADi6Z5fQBZlCfKhTXsf73Zb9PwA9ra4+sMxb8YNxCSQs3S/th9VT3txAoWwEO53kenJ/gh/w8Cwk7f/jBYA7lcjI1/PztTonFrRx1a1SQSk97BRJWffGeD7XW9hDqqgVJkZKvh6+EvBZbT4fW7JlFfXNgkvFu+UKxK7Vb1JKIsXNsJ1XE8avgJHJpqeCVKBiUsrAbRDjZnr35DBn/ZBSdoYNolQzPPJ9AgX2x2r6SoKeLuQ+uTdFSVro5BV4/EeoAknYJPbgQwEpFsDCl7vzC/YRzGl6CJAXHcHoHc4uJ87gE/JCLWdLYsJ6sKpCBBzSrHMbEKX5LI1PYrPLLOUXaPLF0AuK2msMuUqaaG1VpEJkpCMpuYWiQfn5LxCboAnNGp0akGWaZHmm/yj1lvOYMBsg6/ztA5esL5sOegwsNC15qAys8A041x9EzmQ62XtIgo5yZTTnqzs/iyrklXxOMO396Hri0qgJd3oUGB8bMb85cdrjWPqbMyBTMqqrpkGgaSeEf9NhdguQcN4Yxd8Ry/vntD8qu7j01H5AnEs4fEc3zNcHDYYOm8LPLoNyNF9n9UNgkY8WSxUYATPbMno6pLOIH/4RM65M9EcULaIuvyJG3TcA5xP/SX8v7/eMzr/GjyLO/tsRPAIpX253lM8uw6y2qLmKy70gS25mhOaAHWnfxNdIl8Jsl663TnBzoo+e8aDGpkXV5f46tdIWf1l3VKDFyTM0SxGHG6/LylsDC35wnlntGoYq1YSYE4GrBTM+EDSH0G4GDXXcQTzaCGMNMnycDeoBrpApZiuguX0qzA8x0SrnIN0z2RNPZ6Dsh9PER37J6CoTzU4157q8pt8R4ceLQTtOH6WrulBzRuK8ZfXND2vE7JR99UprGmArJ7h6cTQ8GlG1/a+AshFNCP0dqTDRRk2qMzdl7a7htLDvuZ1tgFe5Ivh5tTVkhTBzYCl52fS9Y5EDvgdoq1+aXZ0Pnnj18kLNFYt0AdhHD0CNzrX7Vzosi+YCXnCIQQz9MOK4tftzpVmt/tpVvySn1xVeFh3cDCrJg7+KV7ffnlLq9kVp+vCLSXWaxOpE70Vq3mFP2b6zejGrOYXMhVKvGdMDQ8TObVnUUlN+XvXa84RAYJLXUe+V3808bL2T8BQyOMIiAZQd194V5TrV6gRV3yUiZvgrkS+0voQfaYRIN4G4nZ6RSQF54jl+ytugcsFqLuzgKPgd/W9A8PrlWOCURqXyGHqjBbwDGXX0HD8DKzuaQlsSTvp4rIc2p1nABBlpwCKpccQSf9FGcpQLVi3jB3lCzbxoFNnxpH/GLtF4YjI+aYoh5t0O088k0PpShIPpGNhmOyhLqtFLy3If2aYfoARVsohOA2cn4Vus6meLm73UfNpi4YuXDD2kAvH2dfIOJi9NXxcJArmeuHGU8LjX24REBhxsxr79f0IMC40B79JDqVv/6yc3NR0n57DbveiWDpIwVe3x4Vp9jIzCZb/+Sacx44urnximSR7YUUf7ZIdb/EgrlayOhxA9QVer80tEKheEZ2zss+6PoJYq5e0q2BrhL2d5P8lGIQTiWV35zjfkHgL6TO/XesvNKxGme5iQLeJzX6WxDIBH/ntAlpvaz+N2UpUbeFg5G1LKeDVNlAwmhMIx07s1fU4orIM9wufuxSQnLKQitJ/GJ//XwpObu5zqRPwTbFjfLju4E179Z0nvS0gOX0dgfX2uL92GhrBLO6hnXY+nw/wuR6Qg2uYt1KCP04LeUVv47A+wnMpl/vduf5/UGUK6nA7GGGtvPMx4bq6ZcvCiUV8irdvhz5sknM875Z4CIs500achmlXmcpWmjC1WMGNw9pn3FknKhwyfR1Wt3uH08486Ccs22/QsTeFUC9eJxWwGCekIEXMzbZqpuPAXE+1mkdgIVie93Y1UCn4Ev+oebwQr/b1AeaD8RUhV4W8oLVg3SNd7hQ0PqTXj+XMV23slZn+7fXySRteltjwm87UpfCcVfSfkQ/xQrPeDlNtaYgOVTHFnC/zbsHwFesxqeRy8ANBqWfSNsnaCia9qRqDhOV6GJWG74EtiMm7immwHa4ee2N/DbHSU+6HUF72tp5MsfMhKkO3BEBHDnZIHgBr74bizgbMGdtX2014aXyeH2orqBUwSYih2DEUOmj/TS5HR3LBQO7H1tmtXY58iUcgrYSkPWlVjpn6Fh8hPGlIlck4syt1+bdtHmGWT5pjjTaNObFpKqv5JGXryJ3oUHyGdtPn0hq53GZELd7XVMd4NX2FYDIL5F34WaMAKLQ7YTgIMG6Nm+VzA1atvgLN2+ZoOh9s3TUw93NYtSmo3j9JBy9B6P2Ip43PtSxvJrC5FAzxBGN03afNzcR8arC/eWLRt3NpfCIQadK7/y7GLRH58m1G+MCkrf3HIiiwUq7TJPnIxsaXcRaix8JqgoCl1VXi2iT9YnRAwf2yv9Hz/nEhgplJDIaD9WnDVa0r7QH5VkmJwb/tO+eGz6HZGvs+9TxI+w9PZqVVEbhkKUUYSLZjDNF/LSmWPBzogWeFWEYyQyGGyYwjeEYiuEpsp+L/0p2n4TVVhT/pwFfDEbKWwZ5OR9j5h97+QokmUOQ95KjbvZsmx0E0jz8Wp6nQXdDLIXnLzuz83oTDbiy1kaRI7N6vEspxN6JyYJDNDbUuQBQI44ihE0+gYjboSmVZbUSJUVkPr5i/zaaZCjOPhmZh+PCCm3pOBFURvIlw+Dagu0p9DywTbUW+7mGYZ3+eDUWMQ/IvYwjMOgQtzP7dzF6m35KAxQNMmvyBiTkACwr7VA1UG5A5/kahR3aGb7wyLua2gN+jASSc/sqJaNCY/fACj6twKBe7qU3xnH1C0CG8xsYDkOCCf0hK/31z/KaoabboMx4vFRmmZrt1fV0NcMiRT4HGhpG2CWVc/sHyYLAtOCIN7DmQg6OpMjIDO8BF+fMsVjy2lIMoimsFHFvBOYXw5EizOEm0hsKd2v4SfsyaApcJKJvMTntFapqWMjmgDok36NbOxna0sahfz+aIeZx9Jevi5mS7JztyucpoJZilnScLGJhmrPZRDSPatt6wEH2cBIEJii/R7kgvPiGSF9O1nbBBtSvBxJYuTXtFJc23YuZ/LFito/V3q/8zi4oHHDktXxmc/uTW5WgTn9Jx91p1EIf/wdhM/2tZqSSirtDQOycyCQgEe33/S9L7+EgIkHc0aGHxtMbzLB1KfnjDal8nl/1vya0T77QLnVXQtksr43ZMTXDLB1vDRRtXl/eodhKvtOg3NIPh6jakomgdFAJwTwPXiEFB5ZNgzdotZRyFwTCgzGxjoC6OIM+a9xWg6rue0OIV/qRIjjrLm2wyhikBLt9rwjzcyyuWL9OPZlJ/DQuZes8KawLtZUKYIMbnT0Z/As4osupenM+fvAe0WozNBsNcHLnnR26VN+9w3883VS6heQ2AMjJb/XHVjQLdBBAQhddi07COQzPpCRWa2E6fqZxgkH8BIm+f8D/JJ1g+fYO6MyDKbng3llqwEn0OWLRBcgjx+fV7iDyNELUeiMvuaraLp6KDdgvXn1gXSY9Nnr/FkAd663+Y5RyW3s2NK0JK/csfMQ7O+euKyTi58mxEGfbyXHEpuuq2kMtMnq3QGFyUXftOCYKNNUFt7N64d5MUKaBTUsb7nVqPsC8dMObzC1ugg4k7/tsTku4Q1bm8XH5HubCc9sBJM8HrnFoGoVhKCjx9P/dw0lOEIYPqysOxcnrFbIZ8hEODX07l78TzzR3W0eIkPcUSbssfNSnKlfvf28Qsh2qdRkZx45d0gZzIbnIzdH2TBca6MzHJEUVKgBQFfp1o2MODep/knwezGGXwdaE4RrDVvo9jrYQ7MbsRHUXyGg1DHxU/Tj3UVP1LXVIRRakUjW3Pwh/UB2M9oy6DSwVQxa1NJiukI13/xeLg4PPBzr/BbNPWWASt3nvvmt8MxsjC1P11cq+7hjlBFO2CggZunEJe6vSE2i0IwWIx68d2Ou+v9k9rmDyPaPukg/Xw4HLnupa6+SZHxgh1I+VnozH8s4r1ruZ4BeCdAc7fJ7riN/n12ABDOERMb7pllzM46pmy2Gy1iqO/X/lf62AjDoE0CTgudwa3rAUPOR14hJmOxpid0BObNYO5sT9p40hSzIkMfxJ+AxKZmoGZgDarWc6JwC8fWrNtGnkt1H98OaAbIEGCBY9SW0zSvuaBjJ7LPIT3lMjIDTOBcu1Quq/9/cfz3bJBj1rP4Dr+QP3/10qZw19n0kvFWwgAJ/u0itNheMSUpLbsFLeZPZPFM6NqhIjI3zukGNrKtqcihWyuTp9Tk8FWrlZnc/gh5D09U+59WEPRLBLiGMoa58Syu0dXkOzbV3W0Kf3ITblopUGaO00ve6eWCHQnRDwRerYrkpr9okais3RSM8iHoFlyH31T4+43gBFRMoNe3g9VuptwEzphNhIrcQULAT/MoN6ruDmqtuuupXV11gkSIWfa3jk+KJDM7ueuVKf9yCNdbaOz4pXZnXCZwq/5hokGucNDVShFbVN1aZOLSthEA2o6VQojqs4JZRFG4NQyy/hFmlCNA9LpuDuZiKOhe73VOn1nJCYyK2xPfZ5PCv3LbDmxLbNAnZT+76qt7lDRBd7wKHHaQGQHP0QZ3bsO7aW/sz4L1NRAPUmzbTX+rc3Ispruaoey+KC18KZ6NoUGamMycoXqZKN+ogNVn+oEZ0rgwsjfj7ONMxjqaJhddav+0P3Rw7+tNuqTfOq9P7SLsVCE8WZBd716OO0w/Vfck21WAMOpji1LDQdcaSXJSPh47Pruad36YixUgGisVGdZf8wnN/peeYLTprezpqlOluEKupWhpUxzWcEXXRRWZpNolpxBgAJ7NKMdcf+70yj2LzuNTcZA2DQbgJ3+hZ0b1UZR6LpU+dwrV9Xsriu442VQ9V057i4evQaaSb2Ya5Ec/QaUQQjx62Ta4sVJWavpU00+W8M9Pqn4WU8XLs6/FPNyFpD6snOjDzPW4IYiX/1l56CrG7zUi8pN/XXq9JVoCDOTAxNQa5ek12TNic3CcxKVWxtuEle9chhBp4pLvQng67JKE1fCXb901ogHYope5oQpW5wyrTe45uQOC48pNtomS9dcBh2KB7zOfI7WEQAmhfaf7Jvbo4/PfS9kZ7cVkMdbllAqosvHAfjBLmEFUl4ByU+jdjyuBoz4/6aaXBk6H5FjOSCIhLn7+eysITva9it0oOBGvTSJSf0KgB1G9zoKeEjTPcU1z81BS6eJFo9qtYuCb1gDCi2KR0nkNwAS9MLjoMQJWKLNsAakzltUCtqt1xMOaQ4eKMXW+1NjPrmQ+GD+6Xfx64JQhZolqPwoxObvfm40x6tOPHnyrKSeo8hI6sSEt4HURTI9pkoFbFPuz1C3xfEDoHZXva3x2qikPWOYaRCQKDmaQzI8pioojOSDbFFkwYGYx0Yy8fwiWOp8DWvLlugscjRxhvSyuAoorbY3y9SojIbEz90chMmOJsiVUDBABxgUCEmtMzFAhnEGDyJV81WkO5TAbGhxD+Tz9ITj4/g+BOOWwox6BHqrmFI9Uo6ISWzYIo1Ug46VLXb4ZE5AUW3OFMCXT9BWLPL+Gu8yxFLrFHkSxLUeTo75kuVcQcKdWuvXmCAV2JeAtyP1jn+09gWaEHWfb470A/cJwOxYMwC1fy9h9Jcm4fDlVZoePAqTM1dxlK1AWy5LOdkqkTLxy1s/W9YbCFS23+mo4Jy/nNBi15dxkd0XqKgxT+qZYP8V+nyzunGurrZXflgOdSYL9E3CBT6m/UFh5R/Sm3cgiicMSHzJLxZIrD4FLqRAqLqGWtPui4GNzZZed1sj+vn49usqSqIecWGo2kcdO9pT3dgDtxr1VWTGsA8F5iECaJZTsxYS6cd1HD5qOOJU6RSamqMFj/PJFoXN4SrZt8WEhDIadOEJrFcpdY/vil+8Zf1UIVXEG3ef5G7Nws0SQZmGS1hWTx9df9pZp5aA/tv1AlxR7OC2lbCY2WdymQRjEpLhVbnAhF4QGfRzV+o89k8qjcBOrPIBRPnfTKZcL3IFiSwOyLrtXn3PmVXeaLLJ5I3+vi7qa+H9qe067VAVv00ygIoN6MeoRuH9n2nsyHVyLIboGB6YgW3xx4I/kL4qVAdTTUPPXTca0lDt9KHCDNQ5u8seUZiGJ4KPgYSu5FJOrp61Q52GuNr1aM8pTQai31jjrY60//hO4JCzaYQzgT4q1+1TjYt44EJh/bkOY6FI0OBa6qKeZTJpxaofaazEAGUVGQ/WBj9bi4g0jZIUrtu9b7bjA1P9uvoiEOw4v/SW4FsomGotDe4OLCTfcnfScaAnI+dRQm+ZqZWAIPjxiqjaaK6uwqXcs/y4DfKDxkN0ZHMnd0cLZ0n16uNEmSnQ6Gr6owaQYeWatN3MeAUNNLOVqihGONOao6Xb1YSzzTcHH+OYKRQpaRvyz5IVbyBZdxSUqaCXuLUxEQQgP8eAwIKDc47PD6UP3zmXBMoubQgHw/165MEPt/E+gJ66LAWD57av6R/re8lJMt7IefeWGc6tbe7NLtc2MPrn6e1rnyegto+eQSZiFIYRqgwwdPFD9YWzEHnKpL/RtvxRhQK6Ra0aIMguy8UK/aQ6DVvzMKp2z1r7VhNnURJLfRT/bIU/iNYv2vSaGTRQchFhagQibJn4tKmX3tXGikxvfuK0Auz7fH/QlQ+1O4hVOq/cKLWQZ8X35KuHqqlqfKyeflJ2FeNzj+G8dUT0NxA5hMJsRLTVJW/MHYOcZ2nD/r8hRywiAQEz9iG/2/lCkCvbiSjYYuUbgBFzGjz5uB4HyNZOoF2fWjDJTHoLDmCi7Pa+cgAAeHRlKz4iUZWKhsxjasDQ6rjXT4uHcDncpqK4qrE/6jFFsi/8bu/NkZyaUSxpsO007v2vGaVOrDzvY4o2LzXCFe94U0X53GZgl/08bwmPATKDYU6kmyG4IJJfzONu3fuTcBri7DymqwXA49MHVFfkP4VY7vfJNSLHZ22iQQeB1SAO3qLe41etnlqmbWAZi/9rzO7cz6oKY8HOaAZ0QW84SfY2JIEMu5ul+67v5/nbA8iC68q1ktCwXvgpi55tFDQVcgDS0VJN0WYw58EcM7hT5FujfBxkHyOPhwZdOwa89t1jcQCod6v0WQva/xnWh+kXTFbmxhkgK4VTzHzEZDfmcBZivlE9q94EmrjcWp72WFP49IxqJomqhXBfPp6X+PrTn6dOk+BpoNNjU+GWXkj517pFMk5d5WRyYbjVw5gybNZJ/R6SvL+uj1pDpSllcWSkdAiWnw+4y4zkIZyh48BlxfHT6QQVHyJFFzX1nNHOFKI8LjBnwiQQXpnrv5j3KhV3a4+WP+DtrN/7NfkT+oCWHzi+RY8FvYFmrjJM9mcls0tLQb3YPr65nwzbHe+TkbwNP4aPwe42LS9BNNeqlwxoh3ALeW7tIbmErL9a3PQNRxNnTT7K+HgYysUyWdpexItX0eTD6dttTO7q4+TnWFUpike8ql8MWQ5MNK9hAQPfUBm21QK1quSv1TlnGsOvzDsh5d5lufnEAljtT/j/ALP/G6LbqTqSIIBtvIoB29R2GYiMZ7+CHO5WO7nt9Ffr2cTyVwPDGrVazPp71JZrN/I1pmGGpdxc7cg77SuXUsnWro1QCcHu8r3Lq7ENtWn7qq8Hr9DtEcRUywlYS2siYdVJVXWBTHv1C1lVE7sT66VwDnHeTSU2e4jr7fY6gE7J/NsVVbNSN34UUokfRnNEzGjSGUWofODvpasopbr9eEoMKJeLT0gS7KN3o6MxLzHj0xVrxMCob0k50Z4D4aPDQ8+4eYp5xO3sepWzcfgNEkTBUbTBgdoR/fudNsMUzmPSvoL8OHlfk0SSVv9QQJbL/2NZOzXkHD0a28alUpaxzZJV+FKps9qmjhGZR52FWwXsblcr8nVKlshv/4Ogrgdg485oGhUSpyUhNloEb0JKn3USdikPPh0AHzuSUzuF5nUO+WTGJX0prXFVsG/v56jluYIkXR5mvGKRlxTVuQZqXH/IXLAYxV0XqBBQ95OM41Bh7bLpI1eC0ju2fV6RAzufSkN7N/jBNCkn3KqA3vC+kKy+P0pbmJ15aaxBng6FQzmCMLkeiYdrGpgK8b7hcnOxgxlb/wuO2U27V/TPTzm9iqNv8HKy6E4gpV7WpLDk1n2bF8i5F3QAGSjPuBkBK+HSbefT23qHMBo/ppNF2cOA1WIEL5s4WpS+jwm26mryIwiOcV07fFKuDijGZqxomz7yIGIUQYrDnIOEA8HxEdWVxjt/NWifxIkoWEDni1pPxZvZtQvKla+0vKng0SPRmT4Wfd9+trhDvrJlNQSMrWgavk0Vnt3IvrQPzbjOa2br9BurGqvl4j6OjEE1Xrir0iZIr7HAykco60cSs92JRKK2p0Ttx0A0NPF+SwB+C6MUu9RXC7T4xPirV/NEgQSAGa7zPGp/Jw/s0lBgELqCLzH2QTsz7aYxn1F/5+2tUsIcqd9adXERaqai/sG525cOleY6WT7VN1BpexXlbQoW0NfC8vEIvTe3bMuYZEwMZzka5M7ACqSRW6TvxbxlYna6YwtAEIu5IbvQH/5X6BxjE9kJaRg5lQJYK4D6un8j5Y7Qy1t8FIcK3k+jzAn2C9b0mwMB2s/1QGDyzLFCril9UbW78QD+EAJj4e2WqfMaf/sgHiKTguN4HUhVDz6jCl1ch1OdlYibmkVxnQq6XyH7dTGQeHRPyS2SwxFERslQYYbVN098j0R7s7A0Xill/TPYOWtwUw21oga0e4fCSfQO56snejsiVNj+8ATXfVHV1vXBK9jY3L/ev/ANIFAWFOH+SuG5m31ywS569/e4Hv2fGArP3BC7h+rmNT3kBOgZP/+Kg5zsb0qTT7Lnjg+IDOm+r+NjqiZ5O7PhDXOiSoudAUHpbP2lVJJaEoam3OZUhqfe97zvRhHAogA+AaiQNBXlxmK3t2wqwUCOCBNzJieIfzVYa/AUp72jXcgvceSDj/SW8qhP76JdosInY/7UfA+hXrj479FgEyFGZYEIlMlNu48AP7AetOTILlKb9rvspKFmObLI3AGlcyqgjlKgWth4f3OcZEqNlrIhtWtkJTCf9CjVBJDYGNuFhszWR48JJ4iO+DAwL+vKMzBYF+FuGSKw+d2pz4N0bG6pomyGLJLNllcq6+wxlCa9Ug5zWZrTga1RKNV6Mr6O77ovIrQ93IVMWErak1TwKNwYlUv4YIP/fLKH/37QxwrWml01enqQSCp7xssd6Au5gh0zteWQVcpZ8bhh5VVAjIWhg20/Dcp6PN97bUU6rHfPTenTVn2b8aktt4GURqDj7GcoRxNWok8fhyHfA0xPfVbvMpewRykfZFDKlqI3480H89ElClltd02NWHCzXSnNMU496yeArnhvrlatp7StSh839ZIk7heB0VTPQQaUnyjhemmghMhH/vQ9wXwONtqWs17PLdZ+xQkucXRKKcfRZ9Sabm0Cl/eZGyFKR2h3Y8vm2hxA5+xylhSGLQf+aZM+1Ssc9T6EkHVljXOTrN0E7xMBLDwjOKQFbajvnsJDX0OX/zZJp0o1pjVNCIpVp+GTd+zZ5F52bFqqwSRmEOXAnipvo7/CJ6iRACXFgZNZSr2D1imeFIMZmMonVylZ4Wek0xsTDTn8CLhqAg2ITktG+VX5u8/C9kq6Qfyiir1kDwCH4E8no3YQ/4nuwJPdwpA6BVTVWLNKUTM4uXQwMv0oJGxY6s1Fkdatonfg4NCMylgUpDVjq88169/T8U/Og4c7WSrhO/FPpDCo6fm5ftzdCm8u74yArOnzsI3zPw+eCrgCWs56q0syqVxHhDchXagGh5JvHw4Aph4BrwHf7TXKeyx/8pG4QnK4frYBMf2e2UcO58dyWWd3jwOa+/WXdXILzAFOeyccn3jaBHiPBIwiDhyoB38w/BOimwlAEBFPeYtf3b7BesblLUx+l4eaN42HzJj+ILLqvg1QH+itvmwUM8vr31PV92PhdsQFaUkfhjCKhWjNA08EBdZm7gnHM/6Qywz/EZr6oQTu6K/7dtCdeu/xaVel13w6H9zamjkXgZP6izaCGIIQMCdlZ8sNH5ZccPDN+rZS6EQV694zkKbRFi6Mq4PqIoV6FULpcnAmJONJmZ5m+vX8HzpZJ2KIJ+MByU04H6VXGzcItsfxFkZ7XAQrPQRlD5n9ClkViz0FLnmEe9xZe0qxwf5w1isl8EJcOfX0HAdLCp8s9PJe3ZJOw8Jsmm/L48BAX3rSJUhPyn8JdE9kuJROoXyDTkVIMAhcoySVF+F79cp0TuThS6hUomf33cdjwvNWk4P7X/k7+oKTfIgWTVZ7i0xLk4Kh5jZV0E/MFKuGcrhiuNEgouYoEEV1sa4+y+OdEE8AcFjgzJLq9URvOFpifLJLLq1XQCStixESxpIw5w2+y5aYagYS+LyJgWlQGE9+MXNA/YPqlA3Rx0iZB+O46H/EVy4wwlCvGjxiD2gbIRZHORCtPROgCADQd+S8zP9eRGWTUR6E1hnQ1j/BiTtgKOPtZ7tmxLvbaVOlfHis3MIaWg5yNIypcXbNeH4e5CLRvLiPEYd3AChk8k55qJm4JFhv3v8TA3gjguVTzvB9JUGMkY+iRTMzMuXli2AddTO/hkN3qFty+qumG9zoWUvGTcnbQ28ZWdYZ+rZRTa1WizWf2LKQtyS0zquz11iXpUfohPvwSY1YwjR/Dr2u7u6t5dH8V1UZleuFnDoK6Nc9J2c3DvL40D2Xe8Yb6kgNh97dVTuiAedIceA7nOf7UTV6Es8U1T069M9JJ8q1HTXyB69wpYwa7WRt9sHG7fULuuzcYkkFwYsD77lxF1UESHrQ5wnSeHjhHBhqPaNhbe4EbnnkhfeOpL1YBmmypWtbUP0xK5dzv+TLSPg1i2FqbUIanAx1LqQUR9bFAewOvdhb6EBMR9poaAU+5PIBE4Ew9Qtmn7pFEX60r4tqhmMtwWWVn1PiBMH8rUjU0qy5ZaVqAy3gnSo7C/ZKSuYP8mCOlWpFIlw0ZtqbVf+fRz5JBSPYIP2gtWRi6eGzLWq4SaNe2uwhMcDXFR5D/Mr1Ppij3E822zBmUnk9OJ4VS2qDSgIGqgoaGG5j/E7LiBNh3dlWudLF42NPv2np80XRaz6PeHsazpDj8FL5k0wYeT6+hdIBN46So6NhIUeUuRHxfkxZAJtGEdUhowJqULy4ZYC2fBW97iZQL4YToQMBW/WYS/SXVhsAIeV4zJh6DsJ5IYIimkhaU7C+h8ecKJkFd/wj2dJ3iTxxOSfqZJ3abBOevcYMvGHclrb4oVZKcH5bdp6P4jroqkCF+simJ5/cKENAwF8WPwpWka41v/qpDaaAdJUbw9YyWkGlC6hdUwH9MfB/49w1PE+RpXx7+siTTWVPNMVuzEY0xoZ1yJT9mrZKufEkXIHsLTs+3vG3yBWrxcZA2YK0b/3zjB2YCcNUE2dQdAm4F4ROcW0gEtnAsjIo0n9V6o3HWN+sDaz5yKYdYFFYBJg6kTXI9FazBPmJLUnMJ1opwRVatzTdYqo0aORS6NWoMdcC0Ew5Aj5vZTQXKx+NTApZpkPtBBD21IJeqZrlGxUDL9mKEotpFQGuTwQNui1WriRiOPwp6Ho8Vm8jY61vBqHwW/dd4eRG8PHT7YaO7LV5NJ5mdNPoEUBZwJAgOR2CUlJCsR4MLuk1o1hlJsjBD4ubab1452VQcrkK8RJPQ7CBX8+Yh7yJaOryI9/VPMan3LR7M6cNa9Gfosi3G/ZnRz4zmX0716m0Grg4gxwCqU0/MIgZxntzev2hSVWQlg++nUV1AUC5aX8APMP0mAJyjJ1KSGxdMDOVfZvCc5CsW4396S1HQcvhyiPTMK1HRzyNSrQf2QgMERSdivA8xQmQhfzboDXphtPAxeiSNzQk5WOFJ17htRZpE1TJQ3x3Z35Oxcp0nxYGR69626CIcO/YOB6qEPTYfSB5vvSMSI5u62zgZ8b2nKLgIgDs+HwY7nbieIj7bGS/xztNVJWWLKEED5oq8UcQT9WNWYJxE825bV5d+mN0Syyc25e9TlLsAi03mxOZpa5j9Pcf/4lzbOItzf00KfJFWGwF53C5VQiE/h6mrsvzGzhIsNz55r6HYMg+RdW/Lwg7oSSZjkGt6RHt9fxVZsyO7zXyBWZo5B9SMMFBCFJb+kvktmrNIx5lKcKdi7BZ8rS74Jag2cj9WGBCkHzPjMGXwUJswvb/62TgEJ+H8kFTKtKdo92c63ClRVo1Zmn9VVQqB4R36YrVuFdvCx1AME8k+jlV+pEFubQXZyrpSuBHXsH68cL+m0c+ceRWLmZEGGkpkGoJRJz9/VOpQ3VcKXTXxddB2qIDcZc0EiFCBFZ/8e9RoYL68YWBZGXu3E6jHn6DLJMVbXgFcaE8AHIklTNf6q5VEf5J1KFRLioeev0HNwM+TXva1qBkILf0T/cVm+2QTgiyQQO4ioLZFv0q25esWUsSocDD/AVAgGelxZ8LS8mtk/XoMCPCO1OYmWfp3+WlOz8rVpfF6uHKukEjKPfR4pz3F/pXSrB2mekvJ0dSBgUD/g9GDf1ckUZWLrxLQ5Ck8Tfa3uiXf6kJp3Z1XgusZC4tYgvT8mkPs8T2gBHkaJ/VoqIZR/CoektIEdHsWqf53JQiLAozx7pzWLvEqGSt4Tj57lJKP2eryEBjvdh5cD815T4XOqAHZLlLGCwjOWLSqf78uwhJxz/D2l/s8SFetd/c9xBb2Jb4BysHdpqTZR3iBE7zGzKalfLPZEZxa5w/96DWIBjXB5eI11+vYx89/DMp1A0OM+rEjr59QynQoMuXmBl3RC6y6x9jMp9lOoaCqjezlpa+UCwXaf89UPyS6HmvBkQyLEBI1COFjLZs0isri9bwx2GGGSRcgnUmgEtqpVJUuU5Dbs8v09EQDsPrP15DZLbdulncD+g1SKpGExbIq+z+uij2gM6xSk9qqn5D/hmZZD05IIrUBXYY5XHrbVOizeu8glzIHVPjiXXgKNXo3FCzuP+bz/TkWMEMCD7Hl3q2j1KdRBJL0JTtx0KnK404yIgRsS7Y0o24z9IS1z1IeEtxmOQjJMAFBkfykd0FgWna/GHK4jpTHbc1IAWxIW9k/umHsFpEvBJDIdOzAoeIM0BV9Mwc34sE8c9/v1S0hz1fLCPpVrs8Ryl7heWjYUazyavZ6VkWZTpCETiGUdL3e6LbVJErQ1hRtiUSvgp0Mw9qeJsNZGnxC7EmbxAa9vn+tTc1A5stoBf/57iLC7CKNNzelVclmqlrFyIAYBIrgZOyCuCCD4+IeSQaXetY8MqGn0gBJwKV4ISpYlMbN9+g53RCC1aOwxOO5CnACKMKjzj7tEVDrXBenvxvcCoQhs/wHxj8Z9FlYjtrRsEpKSmXYykuqWbgkfw1y1OncGEVcqV6GiNLBRL5ayNXRweVoUPrNPQaD2zuSj+AS6lxTZELDTYlA3mbOrgWp7cWZ4b7mmT3Rzkobz4YCwWsUjTQJlA5heuHlww7hEil0E4XUseXlR1odjGdIdyuHUeSCB7DVDjPAwjeqs6kYv00A0cvQ/NuiBJwlzu19499E4Fn67GI9spAwcsQH3I3XvK6n4PFgjUj0yk3cnFSfOoBU288wLNToJXUHyepOGGDr97gRnzXjaGRFd7OFUz5xlDUtjBbpgwDITyRp/uqz1W3ioNnPKEEC/nSiAZgr74O/Q+VigG5tZJenHpxW7M3annnyXqPjGqIuvkX3B2UHXl6UBEo+rBgCMhwiOnqE8ehfMZLu72IT8EubVEuPymEAl4QwbQy57FFklQmNxFQMk3cKn/lg7xaz1GF1IuQQXdsFWbhR0NOD5FzzrbjvI7FNl5Tmi5J6UzUlDgNCgUpad4QE+ymGPRVpjpFYJIjNMPvsWNiTeBklcS1SzUHDN3lb0p+d+Jzyy6P5z12XNFO2F5IqRR21HjIt/EI+hga11ERecmuKbpmvowM+fa9Fyu89nOcCiGU7g/9kZOsnAqbe6SZFNeaBfUJvJxflki2cKoSNY4XzWr5wRKvh552BqNS7P9UG9Ud0T8mLqnod+ynVXpapX5FRrMsnFSQ/l1sNi6FMmRZJle2LzCTZxr4AGNv8lp0w6e9zJqx2wBNDiUIiAUacxKguDuy7tAApv1MkNeu/nFPsw8cVrmd02QJ321il/9Q9X0/hxQMbFSQkky2c0Jhh6zgvDRnWXKSxrFPaZrzLnNu2bueCNpyKn724u7MPOGW2dmQqsuTytTnxXPO5K72M3YSpdRmDo+eoCIYHin9zt/DCtxQuMYTVWzuzJaLa7IwbBVBy8zMCYmv1XbcpTu31DQGumLJlQA52a9EPa89kJR3IoGT63iJ6LJiM9/o7nXW0cT2jYyafa/6fuqR4K4zn3kblgd0r+2ZcENh/Ms3mf3P7vHGZ6DPzKQ4V6C5j2EN87WP5eh71+G4dQUXNwR2MPSBSf4y2XzrBUlWn9gL9aU6c4YrN01gOc2J6OuNom7zHu9Ha2r33/fKMyxw4OYNq/IV/zwVNf5PQu79J49HFvNR5YrOa+p0rvNPbLskxkS1kSEEUtUI2398xYmvklhXBR/+TQUb+G4NNr84pyhqw1Sv7sDjFdn47GgcgRXOLSZzRfMFp7PQe34PcbfZr9yGitRqpbPmcCzuo3adsSOHomiwjOKlGa51K1YgkFdWQc1DD6WpzOD5Qo1v/MY6VIMUNanyQFI7zgXm2/Qm2Mvi5KwdpAnsxwVViOvyBa5sQcJVrz2Bwnm0zRkLOYUGAjSM3CPyPDMsMH3Hqrmr0sPUQ9xSVuvIGMMUvXLAimZFV7Z5tww86CfujQuCkaJstb1vZxTbSaooq3LmUjr/1UIhpqLNAahX83VZuJf6shVmfVUVUtNWAvRQMHz5MdEX1FylauDpVIfUZsbdBBFvDPwrAXfsm/AAENE4Hmiggod9nbWP2qYrEUc4/E56rpYD5w4FXfCn6LSekbAUuqV097RBPkgQU6+jBokWwYA6LJFDH5iHudi7Fu1zDXSjukusacHrd2nLCdmr/3yEg4oNHVnQYXvKfzXZoXJPisR9kdvzRUfG2YRB5PpdIUMFwLvtVgQbELsqLzrxhYQ1pKWX8pdwR0tXZd8O1eM8pFsgPt7I/FB2RSTDp+wLHaJgAJZo0yCfVpZGSpH8INr6Lhu6JGwe5k9ekbKG7h8OYeDKuJuTKWf2lxD+m+QRz113/PyusMW16w4/85DJCdwFB3GTOmTjeHX4ki/WcM7WVwJW8oKKykx4gfKZZHEA5MhIU8gl2HCBMOtPyghwuXIZFKOGoDs8zBfydILGOSOZS4ctftKvv/XERRFxxvh5oGLSqiYjXT9O4+kyEvSSDjy5Y+7kEFMljgdKu7gBj+It0Sh8ntpxHBUDZF94FkAeOy+N6qp+gXQCCP7Ip3bvSk2ZC2N+K05euxvFfnyQbIRqnVrHq71Dyl5jiUQ6O3ng108gd6rSwgaUJLuYbtruxx/ecc4YH0gzszpliwLrwEusXfFDQI+XnWZEltu7tH2roLFa79IXbke8bspLOj9dw/Usndc3Tweg/qTVm/R5Ux4v/h0Swyx16IZyR1okr9vWPR7pkq7uFcq5pm5Py2ifSqqsQPXx5MNsIzCR08HFwrRnMiqqESwz1zVr1y0xvUmCGEbqYkA01ndsbaGL4NGDZF+q90TOF9/hBNGqeTuzgqB5fSzpsqa+LlTTSprh6QCBd2AIbJ6e9+GM+Enp8wMD2Pl8O4G59+9BOeJK29ZR3fRDEdw+wgzCrW+VvsmnzkRy18jNcCegOOT4+XfY8sUYnNxbvzhGVoKsL8ZtdXnSeZTFMvdA/cFuk9Bssnv3u7AhV6ADVqZbWd63PldHxGLRPSyiD1ALxbhXxtIHloBftLbfdKdJ5j39BOqU9lEXjXZyhBBdUT3nHG6KtgVPaKzOAbQV9lijHj2mpEPZ/+TDECfD+vC4spgLCSyfXxSPLrkHLKBRegZaJmPT4afKrRee0bZMJpdgAbOTOeE/ks8DWHCRI+WZPJ6e7ipkidy48QSkMVckl5LuV907KZQOQOTuB6k+m/eKuIWzVIkuBZxOu19ji2rXs3LZd5MmRwBVQxwR+W86WbWsph8LuZEJg8uVfFPCHavuztLBHfwasGyFE7wQpMRd5dj2xE4RwLWBh0FTNTILiM5C8bySACwkt9MvSbwo0n6VW/GeK/jQ09Fp9qFDQ/9kx/4XWNkUvMMpCAydopWG7/OKLd6t50wJKzrDVKVZAzrn4+lNyGwFqvikc+pyE5PHVa2xkrJ0s7W8hnL8M92hqPyzbXaTSY5Um8Shosu3ZoxboV5zJ9its91lS8yqAMk+ZFSo8HPFwJlDvBdh0QVdwaKjPGLktl/OgqYFYsSRZ3+SY+1S+ex1Fc1wJcAExNTXbW0RCgpBeuw+3TUxmag8RXHG6Aa89RBBTLGMtp3EM4G3MZeunwr6NLXgNSuYVcIEE8cvug8nRXispUI8MQagg9W6qHt/gVnEma5CNfKoSTsBUOa83HlIAp5kBdalCUB7vxRjpdI5fdfMRGKrsvck/1X/qGdj26qBJrAP5JfuCTiX4BNxdNR5fAcKh23l7vgAYpiUOCCUHY639SDmXLWv+MVwmFjOWJix/HSmgT1iSY5JPpy9wMEFn4kScNdm7shQQHH9s1nrpMLs9+9KLdr3625FRfFCXLJZhXDY4QppSM2C7M6OYQnixOIYvdn/ZHMXo+H63vKx0uhx6sokhubBVUxyC2Hjabr2I0lt3qdtr9vfSZ9Kuiael7j+RVbOPOXKZwZlwJZNlAub6+CcQ1nsuzpjY66izyWQHFMJVXwRfKShqKOtZu0/onVusXj5H3ea/STRFHJsoosxCC4G+AKSQMWYush+OSjjACHTUnpdS/ZYuPYrpVag4i1BFiorvl1bpIH6VF1S2Z5AIqsLrmWb9T8+WAlVg5l6OiyqZMz7knGg6gqC4tqOgKP5YSBGgwQw1w8iP8xgQs6MEyYJVgXkQSD6HQ30hCRmNfagDpuKJXKBwpvEFa6QbAUvSWRPMkO73az5TKdp+L1T0GK4NQEahWusYMvVJaRczPrpXxFEYE49ujvLJ9UIGeqNm1mSB49o253CmkLFgGGXgEUa3iWfIXmUY2PCMRdGZcq/hTcWHh2wQT5RNVpKx2HluTKjLEZ5gxvLF9MGULn3JzNCXH5x3hfidpw9EdQF/+GkRjW30nb0T71+0KI+GKoIuG4KgPGBOdxQn8+waPul3yEwRQtMmIuGHqnNo7ggjNGpE2AzZuc/9tUW22R94IcKh0cGUs1Olu3ETAU4JIGryLfotQG7XK7KaHdp8Bc3068Y9ORAWhwjCPk8f5j7FBNzCx+n7h6cVaW1vajP3489QFtcAZsvYu0uQ0cHUE365m7Q4NeDNUDqCK8xWFpAiPlUPoWW7UOuoKQmQ6XgAhDGh6bMiAt+u9CuRT+9meeF21z5teB6Zp5P4Y53lh/mQJI9BKoDyuf2xOaqIu8+zaFlQ9rM0QjXle9iUL6XhVHBJ0orTtSH2qxElvFpngYSef2Omf5TnTE/ZhQt3Q2gqVWB/qv7wE1r7/eYMRXsfKBuJQ1MPXY2k15o3b5UXTM58zGwCI5fGKnjKP4nv0ap2n98Ns+jKbp5eOrOKZ/YFTogmnQP/bwMjoxxeLTEX2gXnj+UYBM/6oFhRyQFLh0JA+cpEuXmV2WmvluvARtKxpeTyGtS7t+FmoGaGPSDPIRTtSHgn8NFp6KKa8V9ec3rijxtaKj1ZSfSv5pOayueZX9i22mUctPSSL91Hq/FS5dfAlREk5n7VxkLe8H+uiXYtOFo3qHeQRMJJ+Oj7LZz/vaS5Ah6FPuT9fepB7wR/PZiJARbl9AbfOT1NIV5++Npt5Lgq8grMEL90c4qNGr9ERsELobuCMZy8LdhoY0kCFaRO/URaMtwTjX2SOCKbrwQzyx0YCfJOonGsANqQVmstSL7mi+Axr1GRG8URzC4kOPXjbFF3nhdrFdmNr0OEKFM5Ud9SdpcT/4HhxejIdebNA2SgHaCs3OQdiQq2qzH9USWwcXh82nMF87/6pvN7HXx7jOAbEqZl8zZxx1GF5WeQpe3t5xLXQx4wKX7RsoX+HmIJYYJglnyF+9m2on7QjxxScbUWGo+hDg7hMmjGTPks36LIfK223ke0zbqNZ7CfjXM8t5Me3aAMWAXq9bfiCjieI6e6hQ5XaJxH4Ojg7FzsNtYd7h0xV41FRxMI+QqHJPYzeaSfS3G+5tt+ftqVkXjyU89UQbpA9tspaWnXculuvng7osjPLIk3oXHVM76pRYQz0z0auhqpdFcLDy811T9jJxaYFQMQi2h7FDhE/pqsaAUs5+a57CwQgvkX2XHrTDgsM1TTBLPm6ObDX1CWaNPzimp6Xg/O5Yp6r21RUXgUF0nFeVdt8v7udre8doOHQx7az9URRMdIdbGTdt+Hvb1bDHccSsKbtU1tDcuV+H8kJyVt2fdkB4le8B3tRWBRXwzq6UllYobwHDzxnK6rF6v+rQPRQHUFk0/49eqq1EnG/Bqd9Qo7NYMAXiJP8nYi5jf1W9BMT889UpQYmcirZY6gMZrHHqh6p5cFQBqx2PWsK5UmfrosXVxxqXjS3o3FJI2X2rIhaYm6fUPgysrjUW265MenlhqopPr4go6Fv5LVoL9935nMVtleCTQfrjl6fONoKf52bQJPIrdM5EfT8sqb4qGg2Eh9xtv+5KjmNy0NhTOkQY/h6qL8FYRF4AsD8LmSxIQfkowDOScNtEYL3G5Wb7f+valdBBI4x+ano9rtevdAXBLTnypgmNR1x2ZRl/GeGjkkZuouAdkYTu0JsHQkAKjLbpFIbFr4IfO9zcfraXDi2O85XHlQ7gN1SpeTKi37bEsTv8MkMXcA5uHMbxzox31n2DW1SsNcN2nTWQe9LkyRH3wb1TqWobb4FOXnA5i71Rp9Vbp0LVJrxQn1XinIK9EPu6OY4R94ithdT2TBUF5sCzHnGMeacUO8hZCT+5Y4JhDtOGHZtm3ETi43yLLmuEbgAGJJ5TXQ5wxQWA9ZK74PETHz2bp3veEW6IzA+DL7Wm8pIALjY4xYf1EQwLMTcil40uiA1zNUc4kAbPDlQUOekrrEeDPrQ33OVuQXYtg6nvRx3oRRbDMrnDqjfscrAqxJqDyM1yuGE3SIm1ztxCdD/kyZIlas+aJ3oUYH8DEEAxJK1/Qp9J01l6vJ4PCn96HsVAvlwY3PajYUV4P0Jk4UCrjAXG7bL2FVgKl4w3+4i8jfYXfS+ZX1v2CxARg9Rakm3Q0bffJdNmUq5q1HtH2NTe6oTr7SyEpHcsP04UcsKvN3hLusncXclYzBr0ybOqRRvl34Olwo32NKYr8oScBsv6o/MqkWt1nff0DqsjR8IkoJ6vRjX37KAkz7AC5aWyiaxT7rKZHQi76URS/zORxM+XgcuQx/gYgN/ewHrL1T3I1NOVCA9Ve1e+MIIYQGAmhzlCtILn+XLZrJp+CZvfnS1EU1XSFwXOGVykFk1GDiCnjp/HJ8XLuyVoWYgfUsjmpNfPcIkLtZBGSxyKw0lCRwDtil7l4lOMkeIRJ62kYRk+VeB3tOhhWyZz73daZ9UOiXS5w1XFkOWuRwzULCKwFFMcwRmPqX2XW0tAkIvpOGvvtS+wcOSiWQ/dlpGiQwQsYra59CIS9JUaga5prYBt0r2UP1iZqenqLfAOW2yQkQ/4OeaHBynzzkFUoqY/gVROu8qrNukD3bjTSNszJrir/PSLMn0Qp7swS6RCkx+isXBiXBoqo5eh2sVmMVM7SIRjzCdsMqGKAfKZdVE5MuCTWUbKcqajVLB6B+hEvAbnrluRZDwHSx3UE55wxnyrzVU6islzraSc1hDUurHA5/9nCMhPfzBXf91IeGRdsGnq4RD5ZRg3Y5DkByfljmva6WPuKgs3gm0eRo6RYLW1V0JSr5odSbMlxgtRc01DQzWY4cNpsfz4ngadgmNGCEacIsxCFvX0IL0P22tB2Jw+5BojvhJnSiDcnBOJ+OxeWlZXDDFcVAgp7yClSNpNfvWL7tCpi6shYx1P5rJ+XIEkBm7qQgqET2TEnRFQGusQWGl6OWM3sGYphX3Uz22IOg+oAWbF+E3ueF1ExarRLvFPhtX4zvkOUZfc97hviKTXUo/N5VFSDkMS5g6AS+d8e1Tg6fsRoyKh0HjgE1bERu7XfQUj241QjTF03CVzyUWcSQaAWol+plIe+490is+yjXvuPBwUYM7kr6OS5ITs+wOX4qz0zq0eveP7noMKFtIilxd0L6Vu86n3wfMF6cbijScAZEmwqvguWxoZ2aqCGQGu1wMQcKRsy3Ml7PQp583trn3oBFl8hPAz+PoCzpzh+wtCXSFIMwf6PeWfTDEnEmWOyMyAmiEdVnT/sm+WmhjjboQ3jlwp6NP19tCqyEgDCEqgW/xv5fYXiZGLXwc+3hFDfvlDP4K0rBftIDvSJa7whfCz7EtJICyOZFO4E8p3yPvWkVwRhHX76PhgTccIItEuHZQ+N+WmMz1L70Ri2tRt5CkoT5srr1G3HZsn45haFqPN1tjQebx4pXQcb4QjJ/6rp8li67jq9JyGvnxHP7fSsv0PToA1HkFXLheczThfHqpCRFmVmdiSp/J6v/PYzBrSDepNEP0Kvhd+4jVwl9d43UXCVAv0IUnVdUtIins0erDVOmnjDiCBeoqBMd9EnacTd4WqmGY6YD5fENr+4H4Qk1eWFUPl9efkj8nudKM9MpasetcaaYHc7R8T+4FENgjaL6Y55EqDebjy3pcAKM2fsLWMRbJddmcTYUF4QtzD0GZMhjhPKRdwN8ttJv3YZ2v3DfEXJA153kAQpx0cP3wyNftezE0prMbSPdcGCSc/NoDmIWsckz5Kt1szz39NfF20IRNoD+0AKjabR7lZvS2QbfOmBH/JUL0HhblYj6WPCjM76psE4krIyzHSUqNU/akN0fwA30vx9QO1TYhfWTIx8RF+uq90iEVaDeHsTBuf+6l/a0GtGvH5MenBAd+IUPLj1rfyFRjaoV1WLduZWBiBu4uK+APCZvl6nLFEVuHtwYBm+n0wShMq0DG9EUgryaIMD2WDu1EkhRN4bmkk1CHfqeTpP7ylewha3RpVWqgZcyeGbRkXaz/xkg5YKc2WDNPFPgR27GtaPLHPz08YaNE0byW3ZzS+2FsoHwtwBVSqli99TkRoL/JrptzT8n1PGjkpAiY0rZhMsKOUYzcyRf+kxNtjYedShUEz9nzCHeshZsX40n3DvqpRfGkT9bZ4pwdwDzzxfYtTbJVywu+XLaRRXJvVNW4JrFYU4RVomqI9DI+thsn4lAJVITAA3xqifuFJ0+2nJNu1T6kag1GoSuhq6BDa1EiVu6N2WYaKCqz5MStB2h6f7PtoKZU9ZppG2sS86SXtiDnhJSeU8M/ecTj7xMKnLupk++2QiFtNhYnEhKp9DzRRwU5LJnJwW0SFhvTQFeZth1n8THySKUpYqgb4pReovJc1o+/Ut8XFhkf06onsRDq7UkLkLnLS5T76pq5l5Evzcuze9Imvoo1/i3nSyXKnhXyv+WMeoGCe6jB2PWGH88vbUrk8118E7mu2TGOqt/IzpVMTE6nELXaCSAysdgM6WNGCyKXb4x+LIM4S08wdrVZwG/7mE7fhcc/r8AUQq/+PHqgFLUY4/XCXnImfEdK7ATsruOF2a4wjPKNIACBGNKoYISvz2jJRRTaVLgeH8Qy4mq4IcIHi9IK2C0mshyxcgL7X0ZxP+ib5BRntYxWPtti7ZZhlzrw+/YbhakiYdRmOmBj6GA+GJXW2Jp2sTTE7Nn9U1L24bSq4Fn0lRW5SGmy4w4M7zg2nojXeGQGFpqqF9aWxplfANQF/UC7xsl+b4C9+4lzDhwPGt68/lTQwTDNhqRoYEiZpAB9CzXlnqJXD5KclQ6wBaoGn3Eq7cTVZ/Iu3aH3dKKh+XdO2fobZ3JYUCGF6gtvrsTZROqzipTcNMQIQ4CghQYlho5DqNL+9Ytho4T/zO64L90DCY6tG2nMcv3t67zuWVnNU3mu25qW0H5a6bR0p9AwsQ37BKD6xUfuvpXxrlF5yOamOzE/5tbZZ3BvpK3LsUXUgW/FSykEvADeJg9o0b8Aw8GNhZwShRzPAFpzso7D2z9y/71RsdIti1FQrBRJU094wISZJ7oJ27PM9nYagPIs+52RKd/jicbVQHxB5QM1l84hYNOZLpG7EyzcTEKTewC/d6SLBmgK9g+TNmarGofnFGQotRV4+NTPo8XiLvkZQsctDCiwl8Do0epDFXUak9wc2Unlc39gfhj3deM7GoJQi5LSJLW49Y4hoz/Q+wNO/OxzfsB4byvUzg/Dh1l7CsqSs3Qmhnaw5KCRXmG2FoLN8tPOKHXm4sP5liEbsqZrfQJVlc/597UrC4VDmuPJjkP0lbbZYqy4vXGUAgfweI8vmbhMcRqs3yWy33H/snKnwStRBF+QYmq2zluB0lq09Tl5gY63jGXv2ObGlQRFewH+5YTSHMH0cYQQqGGaiwAvHFs9ZEP6aU24cYzko99Jbz6BI+0zdwtDo9kylP12mY+3vHttw9rEv1ekDUiiIdeip4AKaZIUCsrMYql8GyCurnVUGwzyfc4JB3jmU/riJrbzk44xqegrd4G5fe+m+JDoZ9kht8kfsQalrEI4jwxe+AaS7mPqyZAV7mLF3jxfydbdmHn+rU24eFUndXg9o6TaV3xD471nl5lMK4RyI8n8siDvDOY0lXoCA8Ebd+nHQzDqanWjKX914FOWYUEW3KbNK+5WTdp4RHkAF852/aFbZuYWFkZoczbn2iCSLgbQm8vbs18dqcpS3KAeMbE4La2/c03a1yoacz6o4jKst2u2NTHq67pz5hndDycv/r6RM6UT1OmxRP/EVWEpyfZpuV5Y+A9cVhN6BHa1X7Ndu/mI+6S0YYHIpvPMy26kIleVmQNO98hn4DiczN+/G1SpwFl7Jkoi+pQg5ztSkESFpbQ4DPVa1/nifDBtgUkj6WlUvFVnJNRiMgmjsVYGCrvkl/rfnF7WPknH6ZwsmJA8QC/EFQKO5e+eBWj0/309CDTQm4sIBpdNau9DCNuhXwTBTSH4MYaM4fhwT4unzS5gb2SL5lhZpqRp5+BZ9e/zBNkRHWtkoS3Goa/J97EIFDfROgVz2llg6+b5/ejQUMdk/lLXEZj8mJYgwH1Z1HIIQYdKcps83bDoDoezsIOQCzBE2DnFJ2brG8TPB/vLWY/qsy3mks/JRNHgaguXez2VflniIxa8QFF+toPrP8I6gI7uR6J3XjvusXiEvcaaZCHalj8N+6xONrQ2zit6ViD8cSmNwST1/shy+SsTOg+t2sgY0z3ED6h5eLsyqqPnOx+fxpV2+j5TQZvjn4y9I7+4daEJA9v4OnTkwC1TWOxmmfFAhtV6AGQxTvTf7pRRT63VMoTyy2sxEgxdM0FhWD5Vn+kShTecTKYlrg75Ur8FoFMeCyVJGCgr+vS+Af4wyKChP+xnHNVuDDKDLjVFmsPrl8dmQ9w++jm56yzbLPj5V2Ef3/nap+UM7Y7Ie5hVFlCBfqq3uqBC1pjhjXan8w1p3Q6/W8mJd4evNyKrSnCVzIN88n7DJw+NVWl12I9mInGLWeQhdSFZvDaxMpqrFN1PmGZ7tb9dZsS0hZ6wtaGOWJQtAjgP3JJiIG1ip+BTfJY1jTsutSJbEMyZza8cblHT9RiXZmjNmfu3TVQ1i0hHElWrTPW3GcrS2Te+6W0b74Rt765u8bB4oLyt2T18hejUt0x6/vylHi3H5vGaUH6FWRSABdUimoRY6GsH/1e78m3VrJYKB/VkYKq097gzbslBZcJZaGwh/cO9QQaTk469oDSGDA9lz9Lpb/g8M08mvKz/t1yvKqnB4zI47UD7ONJqFEEQGinKjVTRgsqeL1903GO5oNJXf+tRQNkzDczqq68n4Eoj3Q5UGwzAQ0nAKugnf+0WvWOsMUPeswkGm55boMwNb4JYwhgDsNEZrsV2eLSzCBQAE7q6DsJKZti60Dp/cR5ZDVPuf0Ryb79Jnh+w0t4Bfx9Q1F6QdbCnQbQy2x/zrBQ5nR2JmHyUmT63WMSASPIDRFiXlwPgzLUrzDy/ayinOyI09jUWH2OESbUdXoOGx9hbneqLSF7miCb5O+UM/6+c2y8lJ+x6hnpwAVBM9XWEq7BXK+NZgSD3QdUkddASGO4Tc2vZVn14u6+39gbkt9n6lKEnrgHPqy9Zlz5bVrpyv5jhbANQaqbjcoYahBMgpPuHBfn3xbYp+ymihBUUD2W0KDauIECoEvFRxP5NpLKf1vFADAgVStMxtt5YyKLuzXHfKIN93arWzjqjo0lZHepD2MAmIIVyRl8YrmIhu4g6eYh4INnndVGPc4qoK/PmwXgl1JtDoBgAf9zRYcITQ7pfWHrbZO18yXsvoTU49FktQ9TuLTidsWvBeGcv42Yg7UiBsJ7HgBCx4Fq3b5yXMh1yQO/fgLuPbVQJRrOFRmpDWgXRT49ma6O8FIoG6KWhmdOa2F/MvNnZgqsd8lVlwn3ohpbckPygcBTSRYRogTAr8Dm0Spjzocb9+QqNYUiF1gvRO7QmCFl14B1qzIGXlQxORnHgXZBLTfZTV0krC2GcsSox5q6s/awIP6QV1bmDbrnpXXzQ46i2GyQD5ULjuAEWQISn/E7b79m51/bKvxBezHrgbaaYMv5fDw5xJnHmpaqj/ljP3L8M+tBx8EE3Qa9szgI6JvZ8pnDtwe8JqCi0eTWsKsxgyCwgfxOeQglYvszw2YL+ejVhjghWYxmCVhlBS84VoHpMh+Qnwj6iNAj1CA415lYNFmqZAXBxRpVZSozIAVivSyX8MiIG+uKUglwjbVsaW9bv9pQwDq3NXXDZRrHyyE3xnGTyc/gUQg4XA1F4Eld7TOAaTeLTlGW272qpOBRcj/wiGdKKnClgg1k4Gai9GMG/xmtYWy2byUrg4z2Up/JX5JnB+3J5AkmvGPXHE+6EIXIAufRggnzmEcXWt/yFLua6rqqmADvTdDD1Ak6gO1zCsk3ujDEIGAJJwqYNk3ZeBh3CAPs3w95V3AKZA2HUkBpNz6f+mLW59cqpIVFXuFU367Sfg+0sumibC+cdxr4f4RsyXFSIyLbvZl3r2/c1AM2ocxvuiCGwECFIR/zLMm5OgB01nak/ogJzu4bHqOP/QUakv8n0MmHxtXoAtkn/DLHIE/2QZWkN1yB+1cH81+F8cUv+GVHylWh7sKKlr/xlHNiIjvSYRAyplzBAWF5sr0qG7noawv554ZkyOKPAJ1iB5igC/iMZ+H3NwcSSZAQeHtR9LifOOh6/ekAT/03gQkTa0QbkzsFcM8rF41ce7P3armiF8grfgtIpmjgFnh5taUHldbO4A7VlpGLV1l3bxRGJa/5ZhL+DDrF1pfBem0XypsHZWFH0Fv9FZZkZgbkfgwvIMgXMukN7VbXPc079OUh34Dv9nCcM/U5eweaKewwzH/ZqLtoPOt3VnIDAoMuNqKadihBCIxG0MZWMd3i0nYZeUuBB0TFv2g9H/Yxkk8xLxjkZAR2auESFjb9XdHfHZwu+oiqMham7CdJ7osyRN8++IkDtrBrQLW2bW3LQRIyFaSNl/JXmQ4RJZLoG9vHEUu2eOh9aD/vxMFzZaCJYAXUIaX6QxNmmE9gPv91xgngmxbT3GJuaDVCqBewy8hugp8WYP4gyRBr0dFF7PpmgH0saOmOv6QqvE8/8bdfmsK3rwJTj+5IWNekBr+v4du0ZVjsBlJq4XViyA3HCobjAjtMt6bxHna5jveZYhdQXUDXvXqWMomtJnO8r5E9SaPARt1HB51HCZfIafb8qUm4wETO8z8aW6PoEcVUumCZvdaxyn9c29KzMRQFvitTk/Z+iTnIsE59buc84QBhswgt5FKaAAD9PuquqHnUyCUUrlU5oqbTgVt6lxXG7cqETbxXD9rdH4g0FOKxprlbumrRcuoEIwF1lSSDuz0LAVk0e/wWAxgWnaIgQ0JPeec15dOEUBmNLfDTN1oEi+OEvezAmRWurF3alFxJ0jeavmuxfhqMY5adK0j4Y1KYyr+EOLEkpEj7Gr3NUmu4n8RceC95ZOdPf1dOSH3lsatFpyqqbz1pBbLU+RQdczXznyEpxFnrQ9w/9CIYEgvgA0nI6P/79X3kGE9IT966CeVfpwFHuqB53CteZuaRn63aCyFEejEhNXJdmZUPfaXuJ4/sO7gT0cDr7WHySKWGoCCqTGd9s8oY1JIUw9YzsriEa6867F87EIgMAAMFa/H2TAsjVsqSAVFNzqGcjFgDkAHkEPWE+s8y8xxLUbhdXe8E2STq3OYYoGvoeaBTSPkLvzqUDaNvHRnaJHfk9JurgouduvnomqCH2Oro9hxJLSaaCf/leiy+LJdCmGceIubpkiUpMKqOCBcGD19ixBSvyyiHfzl/nDt9bhmx5WSwcJV5EOWSb62OPR4V+kLG5IX9O87yJGwYpKCcPwr5LTd1+dpAcXleR4JPjPZxUlNYDSJjqwsLB/T5/5ZCLRNmK2lxxMo37e8nVPrHrmHkB3IRcUcsICJTnLrQnx9idHvEccFosrOqH4ZxuKu8nvvCPfiWIceyI59lcs04gNjGK2EXPb9ErbNDZ2TT09jYqYhYs4BFfzh7Necsir2J/BVqx2mt1XpDVe8SLBHYglz5L8DZPNSiAl/5aBlnL05tOV6kOl0rdTsSQecxzASHPiEOzjWFSO0tFsh/eii4LhTGOf5VZxooyZca4zdd6mSfq+IMRGtjWax8fxBBSr7ycgHocjNZgdnfZkrSUIIhxSflyYFRjS8DkX8TNhnG0wuhe+LNE/Zk6vXu674RnHJ8GvQBn5xkxtW6XBoS8cxDvfsnhD/ladskMDg2nvIbo2disYButRa6tOkXFwDaIh3a3jbs225rkgDivlTY9Yo1PzrFOIHr6RTtXMFs8dvsDZI+yBE4gnvTF7WKw3ciG3SW+RQh+1SRrNrYVa0MkHaWF0X88rn15R4s43//gvZMC82yzU8jraahpe8ttAD6FVkw/zED7xavlHZlf4ZzoiwNL/j5jh3wII/QvUM5eY6jK7wbwehR5nWR8DL5OzeqcfKrqOhLgpl3is05sAO4hxzVPhDS/+mvhsawmk4T12FWtA2eFqdT+8Yiijmadz15aGG/pBUcZ3NgpXElWA7WOMGR+CNQ1Wfw01GpK0KwiWj4dF/Y8fUveUHYB2k42NzMK1gBAamO5RpVw6+OVVyjykoSg2xXpgqxpSpUmnEo61Sn8Bg1C3b0VdmELCD2Vtf3jIbghxKrMGa16hJ9p7PR9XbljF+cZBIUA/K7Gimue17mg6qWXQIKUYtxh7g0YC6PANs6rF8qrxEVsl3YAB8qmI1v9T2Dfbp8V8Run/Uyr93/9t6F9z03K9cQ1KHDayWrYwMvXc6Y6oCt7FCzRNk6BdK2Q+XzwNE8FSsUZYA5tfU9ZAInQwOiW7pt2GjTeerBjQ0FrxRymiy99DWjk+3cZfjOKoKBVEfxLLEtpqlkK/Ynr5F4mvm4eTkdjz8NbVe9cnyCUDw0ap91on+BundC7TxJfIs7g9dY0kdZqjkYOLSolXnNqYI0jVTUF57xKZvJRSARcVbFqSlcI1RYjBNmQf4klKYqZBg41CWH11A7MGtwQkIf1RFZkokbPIz90xMFDckYNnECO/hP+GhJlEa9SoZ0YHhH99X7nJHgdnWAVerWfD+5ubqB1wM3sts7GAo1W0T3ys5hGFdm/HrEpqD4qmYzr6PBmDEuD5ztE2nUJK76Aal31TR+xn0iIcz6/GXehGUnhYIVGlHkuooCBO/cnleikgpO9OLe7fUAfFu5j871wGoQCY7fJEo3A+BctdS8Mdl3I4O2J5dBl6y0iGa4Y6NJkNy0MZlU0CLKiXBC6P2rtxjYMxszOp6impLB/8VBYdLxSW7I39wuvV6OatDEf57BJTXoGzAPByzwCGcEa7ijnAS22X19OQ5WdPOUkTW89jBoNddQNBPSC6JGJoGg6tHj4c1blKUtIgxDZ9wiu8nq0DvAiYiK7qArI7D6K8GBPYFs0be+t5WVy6qG7GqZMvbeRiL793dlGGkw8YIQ8tWX8CALfEz5kP5BzDKyiKUorlXCG809UNSfe15vest9iSjBQ+sW7UXYdptPZ7IcfUqK7h2dgUpNnFKTFseiN/zjtic3RFXCBtKkGBGDfk02PyM0V2Uhak5z8og/bjZlNLa98GE+toUrBjRThTuThqysc5ApSMw2lCCv8PnAoCdSLYzH60pbXEh2HY7BROJ1hNuCE/uJLBbYc9ky2YGj/nuK5Q4+02Wv7Z8/VkMgdz6fr9EG0jnE5zRgKWxFvI6VPza4doAvSy2D1k4DAeEMAui1JvD3yP/tpzVfZPzn5D/7GTt7hSTuOGfXWtdKiJ/VE1vrvCwwJb8nHv5U5CINmL681Oy0Jzu8sY8U/BY88VRGvym5/R8YWTj2lL9hkozq3nhrFgfH7he3MQKENfy3V0vGd9Su2yxMT44QTn7ZsgFoZXZq96iEm4J3IG4sXa4uFyY4qWLhdvynUjnyMb2c+Bg/RiYCGKdOBbITS2uGAl9tiJ9uPK7r5+BhXQsDEi+G6aitSZObFuggVZKzlzT7du+p+L1JkoKv8ve/1S4g2OQfO0obKtmvEN7Z2GwX+M1Hwrr6FtJrhIA6XiiKPuefr2w5CBmeqxaEBr2TW+hI702eLMuxhwWjwmB14q0FA2bLU6Mo6H8ip34KliLAxEieWLNQcxF0DbMdk9k7iD6ioXNRR8ROIepFGZ4SDTzQQevaO5TXt9M6s6tAThl3TD5v3oxYIl+Sv5UNbhU/TpZ3jwsnISuDsSsQV9PwZsm9d2TdWFtQKA/9YuTCACsRT6Bb+ego6U9v8nH7YaSiSUTgRlWzIamqeLmUUSco1FgVydpOkACY+rm0L9zWUIh1TFDmlBzp++DTTxrBrE8WaMg163p2Y+ytxRoxDcPcnfh4cb0kYishN7JN/SwKKONnJY+lTOoaYWNbyhdFq0xZHGTs7NcXCLytRoUPzWJgaX4toSwzK5GNE09dP2iOmuOa1cBjE6tJs8Ic1t5/XjaVE7tuuTD1PD+dkyUTmKctlaJOUSvtSBrpuWe3dlV7RMoWDAtKX55Lae66BPcIy4sYPJ2Op1NPnwAiDJfU7lPs7H4fYdrtVzoY68we5otcjrPKxeJnndCtb36aPpALWnwFeOar3LHm4vRT/IJElgUmDdO0GbvzWX9TSVktgWwrPvKc6lO8YEmKZ3wCBWjVeu+K+UwNu/5dfOP3r5Ye7CfQKkxPjFBUaU4wD+9NIxrnWhuAYqvE84v/tRMncTJs2CB0ZyQowDpzLhzElxcV/TBXIiaPMUtUycbUr75P8edTJxaDspiSkk429Q2Dq+Pkbm0JFH5tkB0vVURw29yVHW/gdZlKpmmF0RhXHtavFMBx6mHYdhQCeCYfNGALy45AYmnIAAoZiCtGSa5GRWtzZ+7g0TCYA9+bDec1c+oLDzkGz7GjRVYIo7G3k9AwzBu35ujs12LpgZ8MXfCTxbm7IaPPFd/f5rTaefD+ytgpirvygs/KPniplhnabL2GxcJCkvCeCGaihgoFlYMNbCSKTfBIT7nLn59kC6DM1hqeVOnL/t5cj6BwpgbtWV8FL9+VoatxBOPuJsgo3GknC0AztETU2f8uQzhUne3sPkB9zVP29kaOQgcIcycwyQnbEH/+ZvjaB2nC3ZZyTvOQZtsJnVFl9Wq8hffeZ3JZGW/n78krAQ9J1pTTuv2e7EIWrhKQFKCbvq440HlYe5giHE8NoN1lqvIia+soICY79nW59kxEYwbctOmkYYS9uaApL6In/N/894YAy5/qKi9R2tn4qvkwg+ikoLijHj8R7r0Kh8+dXrZg020V1QQiF5BFDMlid3V05Mv/QILl38VECUpRa7tWl7Nb0fsOO9iEsN7mN7WThxzHPEqdJXED1eRHJDX2XIApTPpl2RV7n3Q6CKbF4i2Nu3zI6sBVR2zvXN9UzQbXkK0zqvOS5TLaxKVdqf/BJH/X4mjiRhgZH2e6OTmlUaS0nfwfnHtt7tfQlVOg";
                int op = 3;
                //ubpClientF1.Init(op);
                //ubpClientF1.Init(4);
                //ubpClientF1.Typeid = "RUT";
                //ubpClientF1.Valueid = "21284415-2";
                //ubpClientF1.Minutiaetype = 41;
                //ubpClientF1.AuthenticationFactor = 4;
                //ubpClientF1.Operationtype = 1;
                //ubpClientF1.Tokencontent = 1;
                //ubpClientF1.SetSizeRet(640, 480);
                //this.ubpClientF1.InitCtrl(60);
            }
            catch (Exception)
            {
                cboURL.Items.Add("URL_Local - http://localhost:4030/");
            }
            cboURL.SelectedIndex = 0;
        }

        private void btnOcultar_Click_1(object sender, EventArgs e)
        {
            if (grpFacial.Visible)
            {
                grpFacial.Visible = false;
                btnOcultar.Text = "Mostrar Control Captura Facial...";
            }
            else
            {
                grpFacial.Visible = true;
                btnOcultar.Text = "Ocultar Control Captura Facial...";
            }
            this.Refresh();
        }

        private void txtMTSample_Leave(object sender, EventArgs e)
        {
            labMTSample.Text = Bio.Portal.Server.Api.Constant.MinutiaeType.GetDescription(Convert.ToInt32(txtMTSample.Text.Trim()));
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int expected = 0;
            int actual;
            string xmlparamout = string.Empty;

            try
            {
                using (BioPortalServerWS.BioPortalServerWS target = new BioPortalServerWS.BioPortalServerWS())
                {
                    target.Timeout = 60000;
                    target.Url = ExtraeURL() + "BioPortal.Server.WS.asmx";
                    this.rtxResult.Text = "Consultando a " + target.Url + "...";
                    this.rtxResult.Refresh();
                    XmlParamIn pin = new XmlParamIn();
                    pin.Actionid = Convert.ToInt32(txtAction.Text);
                    pin.Additionaldata = this.abscurrent;
                    //pin.Authenticationfactor = Convert.ToInt32(txtAF.Text.Trim());
                    //pin.Minutiaetype = Convert.ToInt32(txtMT.Text.Trim());
                    pin.Clientid = "IdClienteTest";
                    pin.Companyid = Convert.ToInt32(txtCompany.Text);
                    pin.Enduser = "EnUserTest";
                    pin.Ipenduser = "127.0.0.1";
                    //pin.Matchingtype = 1;
                    pin.Origin = 1;

                    //if (chkAddSample.Checked)
                    //{
                    //    pin.SampleCollection = new List<Sample>();
                    //    Sample sample = new Sample();
                    //    sample.Data = this.rtxSample.Text.Trim();
                    //    sample.Minutiaetype = Convert.ToInt32(txtMTSample.Text.Trim());
                    //    sample.Additionaldata = null;
                    //    pin.SampleCollection.Add(sample);
                    //}
                    //pin.SaveVerified = 1;
                    //pin.Threshold = Convert.ToDouble(txtTH.Text);
                    //pin.Userid = 0;
                    //pin.Verifybyconnectorid = txtConnectorId.Text.Trim().Length == 0 ? null :
                    //    txtConnectorId.Text.Trim();
                    //pin.InsertOption = 1;
                    //pin.PersonalData = new PersonalData();
                    //pin.PersonalData.Typeid = txtID.Text.Trim();
                    //pin.PersonalData.Valueid = txtValueID.Text.Trim();
                    //pin.OperationOrder = Convert.ToInt32(txtOO.Text);  //Bio.Core.Matcher.Constant.OperationOrder.OPERATIONORDER_LOCALFIRST;

                    string xmlparamin = XmlUtils.SerializeAnObject(pin);
                    actual = target.Verify(xmlparamin, out xmlparamout);
                }

                ShowResultVerifyBioSignature(actual, xmlparamout);
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = ex.Message;
            }
        }

        private void ShowResultVerifyBioSignature(int actual, string xmlparamout)
        {
            XmlParamOut paramout = null;
            try
            {
                this.rtxResult.Text = "Codigo Retorno=" + actual.ToString() + " [" + Bio.Core.Api.Constant.Errors.GetDescription(actual) + "]";
                if (xmlparamout != null)
                {
                    paramout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                }
                if (actual == 0)
                {
                    this.rtxResult.Text += Environment.NewLine + "Track Id = " + paramout.Trackid;
                    this.rtxResult.Text += Environment.NewLine + "Verificación BioSignature = " + (paramout.Result == 1 ? "Positiva" : "Negativa");
                    if (paramout.Result == 1)
                    {
                        labResVerify.Text = "Verificación POSITIVA";
                        labResVerify.ForeColor = Color.Green;
                        picQuestion.Visible = false;
                        picOK.Visible = true;
                        picNOOK.Visible = false;
                    }
                    else
                    {
                        labResVerify.Text = "Verificación NEGATIVA";
                        labResVerify.ForeColor = Color.Red;
                        picQuestion.Visible = false;
                        picOK.Visible = false;
                        picNOOK.Visible = true;
                    }
                    this.rtxResult.Text += Environment.NewLine + "Score = " + paramout.Score + " / " + paramout.Threshold;
                }
                else
                {
                    labResVerify.Text = "Verifique...";
                    labResVerify.ForeColor = Color.Blue;
                    picQuestion.Visible = true;
                    picOK.Visible = false;
                    picNOOK.Visible = false;
                }
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = "Exception = " + ex.StackTrace;
            }
            labConsulting.Visible = false;
            labConsulting.Refresh();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            int expected = 0;
            int actual;
            string xmlparamout = string.Empty;

            try
            {
                using (BioPortalserverPROXY.BioPortal_Server_PROXY target = new BioPortalserverPROXY.BioPortal_Server_PROXY())
                {
                    target.Timeout = 60000;
                    target.Url = ExtraeURL() + "BioPortal.Server.PROXY.asmx";
                    this.rtxResult.Text = "Consultando a " + target.Url + "...";
                    this.rtxResult.Refresh();
                    XmlParamIn pin = new XmlParamIn();
                    pin.Actionid = Convert.ToInt32(txtAction.Text);
                    pin.Companyid = Convert.ToInt32(txtCompany.Text);
                    pin.PersonalData = new PersonalData();
                    pin.PersonalData.Typeid = txtID.Text.Trim();
                    pin.PersonalData.Valueid = txtValueID.Text.Trim();
                    pin.Origin = 1;
                    //pin.OperationOrder = Convert.ToInt32(txtOO.Text);  //Bio.Core.Matcher.Constant.OperationOrder.OPERATIONORDER_LOCALFIRST;
                    //pin.Additionaldata = null;
                    //pin.Authenticationfactor = Convert.ToInt32(txtAF.Text.Trim());
                    //pin.Minutiaetype = Convert.ToInt32(txtMT.Text.Trim());
                    //pin.Bodypart = Convert.ToInt32(txtBP.Text.Trim());
                    //pin.Clientid = "IdClienteTest";
                    //pin.Companyid = Convert.ToInt32(txtCompany.Text);
                    //pin.Enduser = "EnUserTest";
                    //pin.Ipenduser = "127.0.0.1";
                    //pin.Matchingtype = 1;
                    //pin.SaveVerified = 1;
                    //pin.Threshold = Convert.ToDouble(txtTH.Text);
                    //pin.Userid = 0;
                    //pin.Verifybyconnectorid = txtConnectorId.Text.Trim().Length == 0 ? null :
                    //    txtConnectorId.Text.Trim();
                    //pin.InsertOption = 1;

                    string xmlparamin = XmlUtils.SerializeAnObject(pin);
                    actual = target.Get(xmlparamin, out xmlparamout);
                }
                this.rtxResult.Text = actual + " - " + xmlparamout;
                //ShowResult(actual, xmlparamout);
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = ex.Message;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            int expected = 0;
            int actual;
            string xmlparamout = string.Empty;

            try
            {
                using (BioPortalserverPROXY.BioPortal_Server_PROXY target = new BioPortalserverPROXY.BioPortal_Server_PROXY())
                {
                    target.Timeout = 60000;
                    target.Url = ExtraeURL() + "BioPortal.Server.PROXY.asmx";
                    this.rtxResult.Text = "Consultando a " + target.Url + "...";
                    this.rtxResult.Refresh();
                    XmlParamIn pin = new XmlParamIn();
                    pin.Actionid = Convert.ToInt32(txtAction.Text);
                    pin.Companyid = Convert.ToInt32(txtCompany.Text);
                    pin.PersonalData = new PersonalData();
                    pin.PersonalData.Typeid = txtID.Text.Trim();
                    pin.PersonalData.Valueid = txtValueID.Text.Trim();
                    pin.Origin = 1;
                    //pin.OperationOrder = Convert.ToInt32(txtOO.Text);  //Bio.Core.Matcher.Constant.OperationOrder.OPERATIONORDER_LOCALFIRST;
                    pin.Additionaldata = txtConnectorId.Text;
                    //pin.Authenticationfactor = Convert.ToInt32(txtAF.Text.Trim());
                    //pin.Minutiaetype = Convert.ToInt32(txtMT.Text.Trim());
                    //pin.Bodypart = Convert.ToInt32(txtBP.Text.Trim());
                    //pin.Clientid = "IdClienteTest";
                    //pin.Companyid = Convert.ToInt32(txtCompany.Text);
                    //pin.Enduser = "EnUserTest";
                    //pin.Ipenduser = "127.0.0.1";
                    //pin.Matchingtype = 1;
                    //pin.SaveVerified = 1;
                    //pin.Threshold = Convert.ToDouble(txtTH.Text);
                    //pin.Userid = 0;
                    //pin.Verifybyconnectorid = txtConnectorId.Text.Trim().Length == 0 ? null :
                    //    txtConnectorId.Text.Trim();
                    //pin.InsertOption = 1;

                    string xmlparamin = XmlUtils.SerializeAnObject(pin);
                    actual = target.Consume(xmlparamin, out xmlparamout);
                }
                this.rtxResult.Text = actual + " - " + xmlparamout;
                //ShowResult(actual, xmlparamout);
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = ex.Message;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (chkPP.Checked)
                APIPluginProcess();
            else if (chkToken.Checked)
                APIToken();
            else
                APINoToken();
        }

        private void APIPluginProcess()
        {
            // Primero instanciamos la clase API que se encarga de todas las conexiones
            API API = new API(Settings.Default.URL_BioPortalAPI);
            // Todos los métodos reciben de entrada un objeto BioPortal.Server.Api.Json.XmlParamIn
            // , XmlParamIn.ActionId determina que es lo que se va a llamar
            // Todos los métodos retornan un objeto BioPortal.Server.Api.Json.XmlParamOut
            // De ser necesario, la clase API permite consumir sus metodos pasando un timeOut custom propio para el método
            // Es un paraemtro opcion y al no pasarlo se utilizan los timeOut por defecto de .NET
            // Este TimeOut se utiliza en el consumo de la API y es pasado a la misma API 
            // para que esta lo utilice en el consumo de BioPortal
            int customTimeOut = 60000;

            // Creamos el objeto Server.Api.Json.XmlParamIn y seteamos sus valores 
            Server.Api.Json.XmlParamIn xmlParamInAPI = new Server.Api.Json.XmlParamIn
            {
                Actionid = Convert.ToInt32(txtAction.Text),                     // REF: Bio.Core.Api.Constant.Action
                Additionaldata = "idtx=987456",
                Authenticationfactor = Convert.ToInt32(txtAF.Text.Trim()),      // REF: Bio.Core.Api.Constant.AuthenticationFactor
                Minutiaetype = Convert.ToInt32(txtMT.Text.Trim()),              // REF: Bio.Core.Api.Constant.MinutiaeType
                Bodypart = Convert.ToInt32(txtBP.Text.Trim()),                  // REF: Bio.Core.Api.Constant.BodyPart
                Clientid = "IdClienteTest",
                Companyid = Convert.ToInt32(txtCompany.Text),
                Enduser = "EnUserTest",
                Ipenduser = "127.0.0.1",
                Matchingtype = 1,
                Origin = 1,
                SaveVerified = 1,
                Threshold = Convert.ToDouble(txtTH.Text),
                Userid = 0,
                Verifybyconnectorid = txtConnectorId.Text.Trim().Length == 0 ? null : txtConnectorId.Text.Trim(),
                InsertOption = 1,
                PersonalData = new Server.Api.Json.PersonalData
                {
                    Typeid = txtID.Text.Trim(),
                    Valueid = txtValueID.Text.Trim()
                },
                OperationOrder = Convert.ToInt32(txtOO.Text)                    // REF: Bio.Core.Api.Constant.OperationOrder
            };

            if (chkAddSample.Checked)
            {
                xmlParamInAPI.SampleCollection = new List<Server.Api.Json.Sample>();
                Server.Api.Json.Sample sample = new Server.Api.Json.Sample
                {
                    Data = rtxSample.Text.Trim(),
                    Minutiaetype = Convert.ToInt32(txtMTSample.Text.Trim()),                                          // REF: Bio.Core.Api.Constant.MinutiaeType
                    Additionaldata = null
                };
                xmlParamInAPI.SampleCollection.Add(sample);
            }
            if (chkAddSample.Checked)
            {
                if (xmlParamInAPI == null) xmlParamInAPI.SampleCollection = new List<Server.Api.Json.Sample>();
                Server.Api.Json.Sample sample2 = new Server.Api.Json.Sample
                {
                    Data = rtxSample2.Text.Trim(),
                    Minutiaetype = Convert.ToInt32(txtMTSample2.Text.Trim()),                                          // REF: Bio.Core.Api.Constant.MinutiaeType
                    Additionaldata = null
                };
                xmlParamInAPI.SampleCollection.Add(sample2);
            }
            Server.Api.Json.XmlParamOut xmlParamOut;

            // Dependiendo del ActionId es el método a consumir
            switch (xmlParamInAPI.Actionid)
            {
                default:
                    xmlParamOut = API.Process(xmlParamInAPI, customTimeOut);
                    break;
                //case 1:
                //case 9:
                //    xmlParamOut = API.WSVerify(xmlParamInAPI, customTimeOut);
                //    break;
                //case 2:
                //    xmlParamOut = API.WSIdentify(xmlParamInAPI, customTimeOut);
                //    break;
                //case 3:
                //case 5:
                //case 7:
                //    xmlParamOut = API.WSEnroll(xmlParamInAPI, customTimeOut);
                //    break;
                //default:
                //    xmlParamOut = API.WSVerify(xmlParamInAPI, customTimeOut);
                //    break;
            }

            // Serializamos la respuesta a un string entendible por el método ShowResult
            string sParamOut = XmlUtils.SerializeObject(xmlParamOut);

            // En el caso del consumo mediante la clase API, todo codigo de respuesta viene en la propiedad Result de Server.Api.Json.XmlParamOut
            // Esto permite que no sea necesario utilizar varibles de salida en los métodos
            ShowResultPP(Convert.ToInt32(xmlParamOut.ExecutionResult), sParamOut);
        }

        private void APINoToken()
        {
            // Primero instanciamos la clase API que se encarga de todas las conexiones
            API API = new API(Settings.Default.URL_BioPortalAPI);
            // Todos los métodos reciben de entrada un objeto BioPortal.Server.Api.Json.XmlParamIn
            // , XmlParamIn.ActionId determina que es lo que se va a llamar
            // Todos los métodos retornan un objeto BioPortal.Server.Api.Json.XmlParamOut
            // De ser necesario, la clase API permite consumir sus metodos pasando un timeOut custom propio para el método
            // Es un paraemtro opcion y al no pasarlo se utilizan los timeOut por defecto de .NET
            // Este TimeOut se utiliza en el consumo de la API y es pasado a la misma API 
            // para que esta lo utilice en el consumo de BioPortal
            int customTimeOut = 60000;

            // Creamos el objeto Server.Api.Json.XmlParamIn y seteamos sus valores 
            Server.Api.Json.XmlParamIn xmlParamInAPI = new Server.Api.Json.XmlParamIn
            {
                Actionid = Convert.ToInt32(txtAction.Text),                     // REF: Bio.Core.Api.Constant.Action
                Additionaldata = "idtx=987456",
                Authenticationfactor = Convert.ToInt32(txtAF.Text.Trim()),      // REF: Bio.Core.Api.Constant.AuthenticationFactor
                Minutiaetype = Convert.ToInt32(txtMT.Text.Trim()),              // REF: Bio.Core.Api.Constant.MinutiaeType
                Bodypart = Convert.ToInt32(txtBP.Text.Trim()),                  // REF: Bio.Core.Api.Constant.BodyPart
                Clientid = "IdClienteTest",
                Companyid = Convert.ToInt32(txtCompany.Text),
                Enduser = "EnUserTest",
                Ipenduser = "127.0.0.1",
                Matchingtype = 1,
                Origin = 1,
                SaveVerified = 1,
                Threshold = Convert.ToDouble(txtTH.Text),
                Userid = 0,
                Verifybyconnectorid = txtConnectorId.Text.Trim().Length == 0 ? null : txtConnectorId.Text.Trim(),
                InsertOption = 1,
                PersonalData = new Server.Api.Json.PersonalData
                {
                    Typeid = txtID.Text.Trim(),
                    Valueid = txtValueID.Text.Trim()
                },
                OperationOrder = Convert.ToInt32(txtOO.Text)                    // REF: Bio.Core.Api.Constant.OperationOrder
            };

            if (chkAddSample.Checked)
            {
                xmlParamInAPI.SampleCollection = new List<Server.Api.Json.Sample>();
                Server.Api.Json.Sample sample = new Server.Api.Json.Sample
                {
                    Data = rtxSample.Text.Trim(),
                    Minutiaetype = 24,                                          // REF: Bio.Core.Api.Constant.MinutiaeType
                    Additionaldata = null
                };

                xmlParamInAPI.SampleCollection.Add(sample);
            }

            Server.Api.Json.XmlParamOut xmlParamOut;

            // Dependiendo del ActionId es el método a consumir
            switch (xmlParamInAPI.Actionid)
            {
                case 1:
                case 9:
                    xmlParamOut = API.WSVerify(xmlParamInAPI, customTimeOut);
                    break;
                case 2:
                    xmlParamOut = API.WSIdentify(xmlParamInAPI, customTimeOut);
                    break;
                case 3:
                case 5:
                case 7:
                    xmlParamOut = API.WSEnroll(xmlParamInAPI, customTimeOut);
                    break;
                default:
                    xmlParamOut = API.WSVerify(xmlParamInAPI, customTimeOut);
                    break;
            }

            // Serializamos la respuesta a un string entendible por el método ShowResult
            string sParamOut = XmlUtils.SerializeObject(xmlParamOut);

            // En el caso del consumo mediante la clase API, todo codigo de respuesta viene en la propiedad Result de Server.Api.Json.XmlParamOut
            // Esto permite que no sea necesario utilizar varibles de salida en los métodos
            ShowResultPP(Convert.ToInt32(xmlParamOut.ExecutionResult), sParamOut);
        }

        private void APIToken()
        {
            
        }

        string b64Foto;
        private void button9_Click(object sender, EventArgs e)
        {
            string s;
            try
            {
                openFileDialog1.Title = "Foto...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    
                    System.IO.FileStream fs = new System.IO.FileStream(openFileDialog1.FileName, System.IO.FileMode.Open);
                    byte[] by = new byte[fs.Length];
                    fs.Read(by, 0, (int)fs.Length);
                    fs.Close();
                    b64Foto = Convert.ToBase64String(by);
                    picFoto.Image = Image.FromFile(openFileDialog1.FileName);
                }
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = "Exception = " + ex.StackTrace;
            }
        }

        string b64Signature;
        private void button10_Click(object sender, EventArgs e)
        {
            string s;
            try
            {
                openFileDialog1.Title = "Firma...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    
                    System.IO.FileStream fs = new System.IO.FileStream(openFileDialog1.FileName, System.IO.FileMode.Open);
                    byte[] by = new byte[fs.Length];
                    fs.Read(by, 0, (int)fs.Length);
                    fs.Close();
                    b64Signature = Convert.ToBase64String(by);
                    picFirma.Image = Image.FromFile(openFileDialog1.FileName);
                }
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = "Exception = " + ex.StackTrace;
            }
        }

        string b64DocFront;
        private void button11_Click(object sender, EventArgs e)
        {
            string s;
            try
            {
                openFileDialog1.Title = "Doc Frente...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    
                    System.IO.FileStream fs = new System.IO.FileStream(openFileDialog1.FileName, System.IO.FileMode.Open);
                    byte[] by = new byte[fs.Length];
                    fs.Read(by, 0, (int)fs.Length);
                    fs.Close();
                    b64DocFront = Convert.ToBase64String(by);
                    picDocFront.Image = Image.FromFile(openFileDialog1.FileName);
                }
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = "Exception = " + ex.StackTrace;
            }
        }

        string b64DocBack;
        private void button13_Click(object sender, EventArgs e)
        {
            string s;
            try
            {
                openFileDialog1.Title = "Doc Back...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    
                    System.IO.FileStream fs = new System.IO.FileStream(openFileDialog1.FileName, System.IO.FileMode.Open);
                    byte[] by = new byte[fs.Length];
                    fs.Read(by, 0, (int)fs.Length);
                    fs.Close();
                    b64DocBack = Convert.ToBase64String(by);
                    picDocBack.Image = Image.FromFile(openFileDialog1.FileName);
                }
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = "Exception = " + ex.StackTrace;
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Seleccione sample en Base 64...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    if (chkEsB64_2.Checked)
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
                        this.rtxSample2.Text = sr.ReadToEnd();
                        sr.Close();
                    } else
                    {
                        byte[] byFile = System.IO.File.ReadAllBytes(openFileDialog1.FileName);
                        this.rtxSample2.Text = Convert.ToBase64String(byFile);
                    }
                }
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = "Exception = " + ex.StackTrace;
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {

        }

        private void button17_Click(object sender, EventArgs e)
        {

        }
    }
}
