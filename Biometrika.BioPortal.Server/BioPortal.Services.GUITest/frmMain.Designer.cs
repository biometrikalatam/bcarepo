﻿namespace BioPortal.Services.GUITest
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.rtxSample = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.button17 = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtURLTestGral = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button16 = new System.Windows.Forms.Button();
            this.chkEsB64 = new System.Windows.Forms.CheckBox();
            this.chkAddSample2 = new System.Windows.Forms.CheckBox();
            this.chkEsB64_2 = new System.Windows.Forms.CheckBox();
            this.button14 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.txtMTSample2 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.rtxSample2 = new System.Windows.Forms.RichTextBox();
            this.chkPP = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.chkRepiteToken = new System.Windows.Forms.CheckBox();
            this.button8 = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.chkCL = new System.Windows.Forms.CheckBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.labMTSample = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMTSample = new System.Windows.Forms.TextBox();
            this.btnOcultar = new System.Windows.Forms.Button();
            this.grpFacial = new System.Windows.Forms.GroupBox();
            this.cboURL = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labBP = new System.Windows.Forms.Label();
            this.labMT = new System.Windows.Forms.Label();
            this.labAF = new System.Windows.Forms.Label();
            this.labOO = new System.Windows.Forms.Label();
            this.labAction = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.chkAddSample = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtBP = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.chkToken = new System.Windows.Forms.CheckBox();
            this.labConsulting = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTH = new System.Windows.Forms.TextBox();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtConnectorId = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtOO = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMT = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtOT = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTokenContent = new System.Windows.Forms.TextBox();
            this.button12 = new System.Windows.Forms.Button();
            this.btnSetAx = new System.Windows.Forms.Button();
            this.txtAF = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtValueID = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtAction = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.grpResult = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.grpBioSignature = new System.Windows.Forms.GroupBox();
            this.labName = new System.Windows.Forms.Label();
            this.labSampleTypeAbs = new System.Windows.Forms.Label();
            this.labTimestampAbs = new System.Windows.Forms.Label();
            this.labIdAbs = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.picSampleAbs = new System.Windows.Forms.PictureBox();
            this.picFotoABS = new System.Windows.Forms.PictureBox();
            this.picBarcodeABS = new System.Windows.Forms.PictureBox();
            this.picQuestion = new System.Windows.Forms.PictureBox();
            this.labResVerify = new System.Windows.Forms.Label();
            this.picNOOK = new System.Windows.Forms.PictureBox();
            this.picOK = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.rtxResult = new System.Windows.Forms.RichTextBox();
            this.button13 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.picDocBack = new System.Windows.Forms.PictureBox();
            this.picDocFront = new System.Windows.Forms.PictureBox();
            this.button9 = new System.Windows.Forms.Button();
            this.picFirma = new System.Windows.Forms.PictureBox();
            this.picFoto = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button15 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grpResult.SuspendLayout();
            this.grpBioSignature.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSampleAbs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoABS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBarcodeABS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQuestion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNOOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocFront)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFirma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).BeginInit();
            this.SuspendLayout();
            // 
            // rtxSample
            // 
            this.rtxSample.Location = new System.Drawing.Point(18, 229);
            this.rtxSample.Name = "rtxSample";
            this.rtxSample.Size = new System.Drawing.Size(767, 180);
            this.rtxSample.TabIndex = 59;
            this.rtxSample.Text = resources.GetString("rtxSample.Text");
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkEsB64);
            this.groupBox1.Controls.Add(this.chkAddSample2);
            this.groupBox1.Controls.Add(this.chkEsB64_2);
            this.groupBox1.Controls.Add(this.button14);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.txtMTSample2);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.rtxSample2);
            this.groupBox1.Controls.Add(this.chkPP);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.chkRepiteToken);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.chkCL);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.labMTSample);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtMTSample);
            this.groupBox1.Controls.Add(this.btnOcultar);
            this.groupBox1.Controls.Add(this.grpFacial);
            this.groupBox1.Controls.Add(this.cboURL);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.labBP);
            this.groupBox1.Controls.Add(this.labMT);
            this.groupBox1.Controls.Add(this.labAF);
            this.groupBox1.Controls.Add(this.labOO);
            this.groupBox1.Controls.Add(this.labAction);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.chkAddSample);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txtBP);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.chkToken);
            this.groupBox1.Controls.Add(this.labConsulting);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.rtxSample);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtTH);
            this.groupBox1.Controls.Add(this.txtURL);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtConnectorId);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtOO);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtMT);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtOT);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtTokenContent);
            this.groupBox1.Controls.Add(this.button12);
            this.groupBox1.Controls.Add(this.btnSetAx);
            this.groupBox1.Controls.Add(this.txtAF);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtCompany);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtValueID);
            this.groupBox1.Controls.Add(this.txtID);
            this.groupBox1.Controls.Add(this.txtAction);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1333, 424);
            this.groupBox1.TabIndex = 93;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parametros...";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBox5);
            this.groupBox2.Controls.Add(this.checkBox4);
            this.groupBox2.Controls.Add(this.checkBox3);
            this.groupBox2.Controls.Add(this.checkBox2);
            this.groupBox2.Controls.Add(this.button17);
            this.groupBox2.Controls.Add(this.richTextBox1);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.txtURLTestGral);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.button16);
            this.groupBox2.Location = new System.Drawing.Point(232, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1101, 711);
            this.groupBox2.TabIndex = 121;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Test General...";
            this.groupBox2.Visible = false;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(905, 24);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(55, 17);
            this.checkBox5.TabIndex = 136;
            this.checkBox5.Text = "Plugin";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(824, 24);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(72, 17);
            this.checkBox4.TabIndex = 135;
            this.checkBox4.Text = "WS WEB";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(758, 23);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(44, 17);
            this.checkBox3.TabIndex = 134;
            this.checkBox3.Text = "WS";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(701, 23);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(51, 17);
            this.checkBox2.TabIndex = 133;
            this.checkBox2.Text = "Todo";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(938, 671);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(140, 23);
            this.button17.TabIndex = 132;
            this.button17.Text = "&Borrar";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox1.Location = new System.Drawing.Point(21, 95);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(1057, 570);
            this.richTextBox1.TabIndex = 123;
            this.richTextBox1.Text = "";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(16, 24);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(94, 13);
            this.label24.TabIndex = 122;
            this.label24.Text = "URL Web Service";
            // 
            // txtURLTestGral
            // 
            this.txtURLTestGral.Location = new System.Drawing.Point(116, 21);
            this.txtURLTestGral.Name = "txtURLTestGral";
            this.txtURLTestGral.Size = new System.Drawing.Size(437, 20);
            this.txtURLTestGral.TabIndex = 121;
            this.txtURLTestGral.Text = "http://qabpservice.biometrikalatam.com/";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(569, 24);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(51, 13);
            this.label25.TabIndex = 104;
            this.label25.Text = "Company";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(626, 21);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(51, 20);
            this.textBox1.TabIndex = 103;
            this.textBox1.Text = "7";
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.Green;
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.ForeColor = System.Drawing.Color.White;
            this.button16.Location = new System.Drawing.Point(115, 50);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(262, 36);
            this.button16.TabIndex = 102;
            this.button16.Text = "Ejecuta Test General";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // chkEsB64
            // 
            this.chkEsB64.AutoSize = true;
            this.chkEsB64.Location = new System.Drawing.Point(115, 206);
            this.chkEsB64.Name = "chkEsB64";
            this.chkEsB64.Size = new System.Drawing.Size(60, 17);
            this.chkEsB64.TabIndex = 168;
            this.chkEsB64.Text = "Es B64";
            this.chkEsB64.UseVisualStyleBackColor = true;
            // 
            // chkAddSample2
            // 
            this.chkAddSample2.AutoSize = true;
            this.chkAddSample2.Checked = true;
            this.chkAddSample2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAddSample2.Location = new System.Drawing.Point(1079, 129);
            this.chkAddSample2.Name = "chkAddSample2";
            this.chkAddSample2.Size = new System.Drawing.Size(92, 17);
            this.chkAddSample2.TabIndex = 167;
            this.chkAddSample2.Text = "Add Sample 2";
            this.chkAddSample2.UseVisualStyleBackColor = true;
            // 
            // chkEsB64_2
            // 
            this.chkEsB64_2.AutoSize = true;
            this.chkEsB64_2.Location = new System.Drawing.Point(1056, 322);
            this.chkEsB64_2.Name = "chkEsB64_2";
            this.chkEsB64_2.Size = new System.Drawing.Size(60, 17);
            this.chkEsB64_2.TabIndex = 166;
            this.chkEsB64_2.Text = "Es B64";
            this.chkEsB64_2.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(1122, 318);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(199, 23);
            this.button14.TabIndex = 165;
            this.button14.Text = "Cargar Sample desde  Archivo...";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(1188, 129);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 13);
            this.label23.TabIndex = 164;
            this.label23.Text = "MinutiaeType";
            // 
            // txtMTSample2
            // 
            this.txtMTSample2.Location = new System.Drawing.Point(1265, 126);
            this.txtMTSample2.Name = "txtMTSample2";
            this.txtMTSample2.Size = new System.Drawing.Size(45, 20);
            this.txtMTSample2.TabIndex = 163;
            this.txtMTSample2.Text = "44";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(997, 135);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(51, 13);
            this.label21.TabIndex = 162;
            this.label21.Text = "Sample 2";
            // 
            // rtxSample2
            // 
            this.rtxSample2.Location = new System.Drawing.Point(990, 152);
            this.rtxSample2.Name = "rtxSample2";
            this.rtxSample2.Size = new System.Drawing.Size(329, 160);
            this.rtxSample2.TabIndex = 161;
            this.rtxSample2.Text = "";
            // 
            // chkPP
            // 
            this.chkPP.AutoSize = true;
            this.chkPP.Location = new System.Drawing.Point(1143, 354);
            this.chkPP.Name = "chkPP";
            this.chkPP.Size = new System.Drawing.Size(96, 17);
            this.chkPP.TabIndex = 160;
            this.chkPP.Text = "Plugin Process";
            this.chkPP.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(1019, 383);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(57, 17);
            this.checkBox1.TabIndex = 159;
            this.checkBox1.Text = "GetOp";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // chkRepiteToken
            // 
            this.chkRepiteToken.AutoSize = true;
            this.chkRepiteToken.Location = new System.Drawing.Point(902, 346);
            this.chkRepiteToken.Name = "chkRepiteToken";
            this.chkRepiteToken.Size = new System.Drawing.Size(91, 17);
            this.chkRepiteToken.TabIndex = 158;
            this.chkRepiteToken.Text = "Repite Token";
            this.chkRepiteToken.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(1137, 377);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(161, 23);
            this.button8.TabIndex = 157;
            this.button8.Text = "Consulta BP via WebAPI";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(201, 182);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 13);
            this.label20.TabIndex = 156;
            this.label20.Text = "CON COMA";
            // 
            // chkCL
            // 
            this.chkCL.AutoSize = true;
            this.chkCL.Location = new System.Drawing.Point(801, 346);
            this.chkCL.Name = "chkCL";
            this.chkCL.Size = new System.Drawing.Size(95, 17);
            this.chkCL.TabIndex = 155;
            this.chkCL.Text = "Cliente Liviano";
            this.chkCL.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(392, 108);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(266, 36);
            this.button7.TabIndex = 152;
            this.button7.Text = "Consume a BioPortal via Proxy...";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(392, 73);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(266, 36);
            this.button6.TabIndex = 151;
            this.button6.Text = "Consulta a BioPortal via Proxy...";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // labMTSample
            // 
            this.labMTSample.AutoSize = true;
            this.labMTSample.Location = new System.Drawing.Point(559, 208);
            this.labMTSample.Name = "labMTSample";
            this.labMTSample.Size = new System.Drawing.Size(50, 13);
            this.labMTSample.TabIndex = 148;
            this.labMTSample.Text = "F7 Facial";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(431, 209);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 147;
            this.label7.Text = "MinutiaeType";
            // 
            // txtMTSample
            // 
            this.txtMTSample.Location = new System.Drawing.Point(508, 206);
            this.txtMTSample.Name = "txtMTSample";
            this.txtMTSample.Size = new System.Drawing.Size(45, 20);
            this.txtMTSample.TabIndex = 146;
            this.txtMTSample.Text = "42";
            this.txtMTSample.Leave += new System.EventHandler(this.txtMTSample_Leave);
            // 
            // btnOcultar
            // 
            this.btnOcultar.BackColor = System.Drawing.Color.DarkSalmon;
            this.btnOcultar.Location = new System.Drawing.Point(1110, 95);
            this.btnOcultar.Name = "btnOcultar";
            this.btnOcultar.Size = new System.Drawing.Size(176, 23);
            this.btnOcultar.TabIndex = 145;
            this.btnOcultar.Text = "Mostar Control Captura Facial...";
            this.btnOcultar.UseVisualStyleBackColor = false;
            this.btnOcultar.Click += new System.EventHandler(this.btnOcultar_Click_1);
            // 
            // grpFacial
            // 
            this.grpFacial.Location = new System.Drawing.Point(952, 47);
            this.grpFacial.Name = "grpFacial";
            this.grpFacial.Size = new System.Drawing.Size(34, 175);
            this.grpFacial.TabIndex = 150;
            this.grpFacial.TabStop = false;
            this.grpFacial.Text = "Facial";
            this.grpFacial.Visible = false;
            // 
            // cboURL
            // 
            this.cboURL.FormattingEnabled = true;
            this.cboURL.Items.AddRange(new object[] {
            "http://localhost:4030/"});
            this.cboURL.Location = new System.Drawing.Point(115, 16);
            this.cboURL.Name = "cboURL";
            this.cboURL.Size = new System.Drawing.Size(441, 21);
            this.cboURL.TabIndex = 143;
            this.cboURL.Text = "http://nvprod.cloudapp.net/BioPortal.Services.v5.5/";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(559, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 142;
            this.label5.Text = "Biometrika";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(791, 272);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(144, 40);
            this.button4.TabIndex = 141;
            this.button4.Text = "<< Cargar Sample desde BioPortal Client...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(855, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 140;
            this.label4.Text = "1-Verify | 2-Enroll]";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(752, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(194, 13);
            this.label3.TabIndex = 139;
            this.label3.Text = "[0-Both | 1-Solo WSQ | 2-Solo Minucias]";
            // 
            // labBP
            // 
            this.labBP.AutoSize = true;
            this.labBP.Location = new System.Drawing.Point(502, 176);
            this.labBP.Name = "labBP";
            this.labBP.Size = new System.Drawing.Size(29, 13);
            this.labBP.TabIndex = 137;
            this.labBP.Text = "Cara";
            // 
            // labMT
            // 
            this.labMT.AutoSize = true;
            this.labMT.Location = new System.Drawing.Point(502, 150);
            this.labMT.Name = "labMT";
            this.labMT.Size = new System.Drawing.Size(50, 13);
            this.labMT.TabIndex = 136;
            this.labMT.Text = "F7 Facial";
            // 
            // labAF
            // 
            this.labAF.AutoSize = true;
            this.labAF.Location = new System.Drawing.Point(201, 150);
            this.labAF.Name = "labAF";
            this.labAF.Size = new System.Drawing.Size(35, 13);
            this.labAF.TabIndex = 135;
            this.labAF.Text = "Facial";
            // 
            // labOO
            // 
            this.labOO.AutoSize = true;
            this.labOO.Location = new System.Drawing.Point(201, 100);
            this.labOO.Name = "labOO";
            this.labOO.Size = new System.Drawing.Size(57, 13);
            this.labOO.TabIndex = 134;
            this.labOO.Text = "Local Solo";
            // 
            // labAction
            // 
            this.labAction.AutoSize = true;
            this.labAction.Location = new System.Drawing.Point(203, 73);
            this.labAction.Name = "labAction";
            this.labAction.Size = new System.Drawing.Size(74, 13);
            this.labAction.TabIndex = 133;
            this.labAction.Text = "Verify and Get";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(179, 203);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(246, 23);
            this.button3.TabIndex = 131;
            this.button3.Text = "Cargar Sample desde  Archivo...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(721, 209);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(64, 23);
            this.button2.TabIndex = 130;
            this.button2.Text = "Borrar...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // chkAddSample
            // 
            this.chkAddSample.AutoSize = true;
            this.chkAddSample.Checked = true;
            this.chkAddSample.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAddSample.Location = new System.Drawing.Point(18, 196);
            this.chkAddSample.Name = "chkAddSample";
            this.chkAddSample.Size = new System.Drawing.Size(83, 17);
            this.chkAddSample.TabIndex = 129;
            this.chkAddSample.Text = "Add Sample";
            this.chkAddSample.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(360, 176);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(50, 13);
            this.label19.TabIndex = 128;
            this.label19.Text = "BodyPart";
            // 
            // txtBP
            // 
            this.txtBP.Location = new System.Drawing.Point(416, 173);
            this.txtBP.Name = "txtBP";
            this.txtBP.Size = new System.Drawing.Size(80, 20);
            this.txtBP.TabIndex = 127;
            this.txtBP.Text = "16";
            this.txtBP.Leave += new System.EventHandler(this.txtBP_Leave);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 150);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 13);
            this.label18.TabIndex = 126;
            this.label18.Text = "AuthenticatorFactor";
            // 
            // chkToken
            // 
            this.chkToken.AutoSize = true;
            this.chkToken.Location = new System.Drawing.Point(562, 20);
            this.chkToken.Name = "chkToken";
            this.chkToken.Size = new System.Drawing.Size(79, 17);
            this.chkToken.TabIndex = 124;
            this.chkToken.Text = "Con Token";
            this.chkToken.UseVisualStyleBackColor = true;
            // 
            // labConsulting
            // 
            this.labConsulting.AutoSize = true;
            this.labConsulting.Location = new System.Drawing.Point(807, 408);
            this.labConsulting.Name = "labConsulting";
            this.labConsulting.Size = new System.Drawing.Size(75, 13);
            this.labConsulting.TabIndex = 123;
            this.labConsulting.Text = "Consultando...";
            this.labConsulting.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(27, 213);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 13);
            this.label17.TabIndex = 122;
            this.label17.Text = "Sample";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(241, 47);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 121;
            this.label16.Text = "ValueId";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(69, 47);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 120;
            this.label15.Text = "TypeId";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 13);
            this.label14.TabIndex = 119;
            this.label14.Text = "URL Web Service";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(61, 180);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 118;
            this.label13.Text = "Treshold";
            // 
            // txtTH
            // 
            this.txtTH.Location = new System.Drawing.Point(115, 177);
            this.txtTH.Name = "txtTH";
            this.txtTH.Size = new System.Drawing.Size(80, 20);
            this.txtTH.TabIndex = 117;
            this.txtTH.Text = "0,3";
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(972, 21);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(231, 20);
            this.txtURL.TabIndex = 116;
            this.txtURL.Text = "http://192.168.5.99/BioPortal.Services.v5/";
            this.txtURL.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(44, 125);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 115;
            this.label12.Text = "ConnectorId";
            // 
            // txtConnectorId
            // 
            this.txtConnectorId.Location = new System.Drawing.Point(115, 122);
            this.txtConnectorId.Name = "txtConnectorId";
            this.txtConnectorId.Size = new System.Drawing.Size(155, 20);
            this.txtConnectorId.TabIndex = 114;
            this.txtConnectorId.Text = "NONE";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 100);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 13);
            this.label11.TabIndex = 113;
            this.label11.Text = "Operation Order";
            // 
            // txtOO
            // 
            this.txtOO.Location = new System.Drawing.Point(115, 97);
            this.txtOO.Name = "txtOO";
            this.txtOO.Size = new System.Drawing.Size(80, 20);
            this.txtOO.TabIndex = 112;
            this.txtOO.Text = "1";
            this.txtOO.Leave += new System.EventHandler(this.txtOO_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(339, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 13);
            this.label10.TabIndex = 110;
            this.label10.Text = "MinutiaeType";
            // 
            // txtMT
            // 
            this.txtMT.Location = new System.Drawing.Point(416, 147);
            this.txtMT.Name = "txtMT";
            this.txtMT.Size = new System.Drawing.Size(80, 20);
            this.txtMT.TabIndex = 109;
            this.txtMT.Text = "42";
            this.txtMT.Leave += new System.EventHandler(this.txtMT_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(777, 162);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 108;
            this.label9.Text = "Operation Type";
            // 
            // txtOT
            // 
            this.txtOT.Location = new System.Drawing.Point(863, 159);
            this.txtOT.Name = "txtOT";
            this.txtOT.Size = new System.Drawing.Size(80, 20);
            this.txtOT.TabIndex = 107;
            this.txtOT.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(863, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 106;
            this.label8.Text = "Tipo Token";
            // 
            // txtTokenContent
            // 
            this.txtTokenContent.Location = new System.Drawing.Point(866, 108);
            this.txtTokenContent.Name = "txtTokenContent";
            this.txtTokenContent.Size = new System.Drawing.Size(80, 20);
            this.txtTokenContent.TabIndex = 105;
            this.txtTokenContent.Text = "0";
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Green;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Location = new System.Drawing.Point(801, 369);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(212, 36);
            this.button12.TabIndex = 100;
            this.button12.Text = "Consulta a BioPortal...";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // btnSetAx
            // 
            this.btnSetAx.Location = new System.Drawing.Point(833, 208);
            this.btnSetAx.Name = "btnSetAx";
            this.btnSetAx.Size = new System.Drawing.Size(112, 23);
            this.btnSetAx.TabIndex = 99;
            this.btnSetAx.Text = "Set >>>";
            this.btnSetAx.UseVisualStyleBackColor = true;
            this.btnSetAx.Click += new System.EventHandler(this.btnSetAx_Click);
            // 
            // txtAF
            // 
            this.txtAF.Location = new System.Drawing.Point(115, 146);
            this.txtAF.Name = "txtAF";
            this.txtAF.Size = new System.Drawing.Size(80, 20);
            this.txtAF.TabIndex = 97;
            this.txtAF.Text = "4";
            this.txtAF.Leave += new System.EventHandler(this.txtAF_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(445, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 72;
            this.label2.Text = "Company";
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(502, 44);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(51, 20);
            this.txtCompany.TabIndex = 71;
            this.txtCompany.Text = "7";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 70;
            this.label1.Text = "Operación";
            // 
            // txtValueID
            // 
            this.txtValueID.Location = new System.Drawing.Point(287, 44);
            this.txtValueID.Name = "txtValueID";
            this.txtValueID.Size = new System.Drawing.Size(112, 20);
            this.txtValueID.TabIndex = 69;
            this.txtValueID.Text = "21284415-2";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(115, 44);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(80, 20);
            this.txtID.TabIndex = 68;
            this.txtID.Text = "RUT";
            // 
            // txtAction
            // 
            this.txtAction.Location = new System.Drawing.Point(115, 70);
            this.txtAction.Name = "txtAction";
            this.txtAction.Size = new System.Drawing.Size(80, 20);
            this.txtAction.TabIndex = 67;
            this.txtAction.Text = "9";
            this.txtAction.Leave += new System.EventHandler(this.txtAction_Leave);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::BioPortal.Services.GUITest.Properties.Resources.logo_bpclient_Enroll_Verify;
            this.pictureBox1.Location = new System.Drawing.Point(664, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(282, 62);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 132;
            this.pictureBox1.TabStop = false;
            // 
            // grpResult
            // 
            this.grpResult.Controls.Add(this.groupBox2);
            this.grpResult.Controls.Add(this.button5);
            this.grpResult.Controls.Add(this.grpBioSignature);
            this.grpResult.Controls.Add(this.picQuestion);
            this.grpResult.Controls.Add(this.labResVerify);
            this.grpResult.Controls.Add(this.picNOOK);
            this.grpResult.Controls.Add(this.picOK);
            this.grpResult.Controls.Add(this.button1);
            this.grpResult.Controls.Add(this.rtxResult);
            this.grpResult.Controls.Add(this.button13);
            this.grpResult.Controls.Add(this.button11);
            this.grpResult.Controls.Add(this.button10);
            this.grpResult.Controls.Add(this.picDocBack);
            this.grpResult.Controls.Add(this.picDocFront);
            this.grpResult.Controls.Add(this.button9);
            this.grpResult.Controls.Add(this.picFirma);
            this.grpResult.Controls.Add(this.picFoto);
            this.grpResult.Location = new System.Drawing.Point(12, 442);
            this.grpResult.Name = "grpResult";
            this.grpResult.Size = new System.Drawing.Size(1333, 221);
            this.grpResult.TabIndex = 94;
            this.grpResult.TabStop = false;
            this.grpResult.Text = "Resultado...";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(1200, 198);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(124, 23);
            this.button5.TabIndex = 139;
            this.button5.Text = "Verify BioSignature...";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // grpBioSignature
            // 
            this.grpBioSignature.Controls.Add(this.labName);
            this.grpBioSignature.Controls.Add(this.labSampleTypeAbs);
            this.grpBioSignature.Controls.Add(this.labTimestampAbs);
            this.grpBioSignature.Controls.Add(this.labIdAbs);
            this.grpBioSignature.Controls.Add(this.label22);
            this.grpBioSignature.Controls.Add(this.label6);
            this.grpBioSignature.Controls.Add(this.picSampleAbs);
            this.grpBioSignature.Controls.Add(this.picFotoABS);
            this.grpBioSignature.Controls.Add(this.picBarcodeABS);
            this.grpBioSignature.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBioSignature.Location = new System.Drawing.Point(1011, 27);
            this.grpBioSignature.Name = "grpBioSignature";
            this.grpBioSignature.Size = new System.Drawing.Size(316, 188);
            this.grpBioSignature.TabIndex = 138;
            this.grpBioSignature.TabStop = false;
            this.grpBioSignature.Text = "BioSignature ??";
            // 
            // labName
            // 
            this.labName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labName.Location = new System.Drawing.Point(123, 83);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(187, 40);
            this.labName.TabIndex = 136;
            this.labName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labSampleTypeAbs
            // 
            this.labSampleTypeAbs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSampleTypeAbs.Location = new System.Drawing.Point(18, 123);
            this.labSampleTypeAbs.Name = "labSampleTypeAbs";
            this.labSampleTypeAbs.Size = new System.Drawing.Size(269, 19);
            this.labSampleTypeAbs.TabIndex = 135;
            // 
            // labTimestampAbs
            // 
            this.labTimestampAbs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTimestampAbs.Location = new System.Drawing.Point(84, 164);
            this.labTimestampAbs.Name = "labTimestampAbs";
            this.labTimestampAbs.Size = new System.Drawing.Size(224, 21);
            this.labTimestampAbs.TabIndex = 134;
            // 
            // labIdAbs
            // 
            this.labIdAbs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labIdAbs.Location = new System.Drawing.Point(36, 142);
            this.labIdAbs.Name = "labIdAbs";
            this.labIdAbs.Size = new System.Drawing.Size(267, 13);
            this.labIdAbs.TabIndex = 132;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(11, 164);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 13);
            this.label22.TabIndex = 133;
            this.label22.Text = "Timestamp";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(14, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 13);
            this.label6.TabIndex = 131;
            this.label6.Text = "Id";
            // 
            // picSampleAbs
            // 
            this.picSampleAbs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picSampleAbs.Location = new System.Drawing.Point(17, 19);
            this.picSampleAbs.Name = "picSampleAbs";
            this.picSampleAbs.Size = new System.Drawing.Size(100, 101);
            this.picSampleAbs.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSampleAbs.TabIndex = 130;
            this.picSampleAbs.TabStop = false;
            // 
            // picFotoABS
            // 
            this.picFotoABS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFotoABS.Location = new System.Drawing.Point(225, 25);
            this.picFotoABS.Name = "picFotoABS";
            this.picFotoABS.Size = new System.Drawing.Size(62, 57);
            this.picFotoABS.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFotoABS.TabIndex = 129;
            this.picFotoABS.TabStop = false;
            // 
            // picBarcodeABS
            // 
            this.picBarcodeABS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBarcodeABS.Location = new System.Drawing.Point(149, 25);
            this.picBarcodeABS.Name = "picBarcodeABS";
            this.picBarcodeABS.Size = new System.Drawing.Size(62, 57);
            this.picBarcodeABS.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBarcodeABS.TabIndex = 128;
            this.picBarcodeABS.TabStop = false;
            // 
            // picQuestion
            // 
            this.picQuestion.Image = global::BioPortal.Services.GUITest.Properties.Resources.Question_Shield;
            this.picQuestion.Location = new System.Drawing.Point(508, 31);
            this.picQuestion.Name = "picQuestion";
            this.picQuestion.Size = new System.Drawing.Size(93, 88);
            this.picQuestion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picQuestion.TabIndex = 127;
            this.picQuestion.TabStop = false;
            // 
            // labResVerify
            // 
            this.labResVerify.AutoSize = true;
            this.labResVerify.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResVerify.ForeColor = System.Drawing.Color.Blue;
            this.labResVerify.Location = new System.Drawing.Point(405, 147);
            this.labResVerify.Name = "labResVerify";
            this.labResVerify.Size = new System.Drawing.Size(285, 25);
            this.labResVerify.TabIndex = 137;
            this.labResVerify.Text = "Verifique IDENTIDAD...";
            this.labResVerify.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picNOOK
            // 
            this.picNOOK.Image = global::BioPortal.Services.GUITest.Properties.Resources.Error_Shield;
            this.picNOOK.Location = new System.Drawing.Point(492, 31);
            this.picNOOK.Name = "picNOOK";
            this.picNOOK.Size = new System.Drawing.Size(93, 88);
            this.picNOOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picNOOK.TabIndex = 129;
            this.picNOOK.TabStop = false;
            this.picNOOK.Visible = false;
            // 
            // picOK
            // 
            this.picOK.Image = global::BioPortal.Services.GUITest.Properties.Resources.Good_Shield;
            this.picOK.Location = new System.Drawing.Point(448, 31);
            this.picOK.Name = "picOK";
            this.picOK.Size = new System.Drawing.Size(93, 88);
            this.picOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picOK.TabIndex = 128;
            this.picOK.TabStop = false;
            this.picOK.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1170, -1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 23);
            this.button1.TabIndex = 124;
            this.button1.Text = "Borrar...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // rtxResult
            // 
            this.rtxResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.rtxResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtxResult.Location = new System.Drawing.Point(6, 19);
            this.rtxResult.Name = "rtxResult";
            this.rtxResult.ReadOnly = true;
            this.rtxResult.Size = new System.Drawing.Size(393, 176);
            this.rtxResult.TabIndex = 64;
            this.rtxResult.Text = "";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(766, 197);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(43, 23);
            this.button13.TabIndex = 163;
            this.button13.Text = ">>";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(766, 0);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(43, 23);
            this.button11.TabIndex = 162;
            this.button11.Text = ">>";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(647, 124);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(43, 23);
            this.button10.TabIndex = 161;
            this.button10.Text = ">>";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // picDocBack
            // 
            this.picDocBack.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picDocBack.Location = new System.Drawing.Point(815, 110);
            this.picDocBack.Name = "picDocBack";
            this.picDocBack.Size = new System.Drawing.Size(171, 94);
            this.picDocBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDocBack.TabIndex = 160;
            this.picDocBack.TabStop = false;
            // 
            // picDocFront
            // 
            this.picDocFront.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picDocFront.Location = new System.Drawing.Point(815, 15);
            this.picDocFront.Name = "picDocFront";
            this.picDocFront.Size = new System.Drawing.Size(171, 89);
            this.picDocFront.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDocFront.TabIndex = 159;
            this.picDocFront.TabStop = false;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(659, 27);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(43, 23);
            this.button9.TabIndex = 158;
            this.button9.Text = ">>";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // picFirma
            // 
            this.picFirma.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picFirma.Location = new System.Drawing.Point(696, 125);
            this.picFirma.Name = "picFirma";
            this.picFirma.Size = new System.Drawing.Size(98, 66);
            this.picFirma.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFirma.TabIndex = 126;
            this.picFirma.TabStop = false;
            // 
            // picFoto
            // 
            this.picFoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picFoto.Location = new System.Drawing.Point(708, 27);
            this.picFoto.Name = "picFoto";
            this.picFoto.Size = new System.Drawing.Size(86, 92);
            this.picFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFoto.TabIndex = 125;
            this.picFoto.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.ForeColor = System.Drawing.Color.Black;
            this.button15.Location = new System.Drawing.Point(12, 687);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(212, 36);
            this.button15.TabIndex = 101;
            this.button15.Text = "Open Test General";
            this.button15.UseVisualStyleBackColor = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1357, 750);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpResult);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BioPortal Services GUI Test...";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grpResult.ResumeLayout(false);
            this.grpResult.PerformLayout();
            this.grpBioSignature.ResumeLayout(false);
            this.grpBioSignature.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSampleAbs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoABS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBarcodeABS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQuestion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNOOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDocFront)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFirma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtxSample;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labConsulting;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTH;
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtConnectorId;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtOO;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMT;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtOT;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTokenContent;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button btnSetAx;
        private System.Windows.Forms.TextBox txtAF;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtValueID;
        private System.Windows.Forms.TextBox txtAction;
        private System.Windows.Forms.GroupBox grpResult;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox rtxResult;
        private System.Windows.Forms.CheckBox chkToken;
        private System.Windows.Forms.PictureBox picFirma;
        private System.Windows.Forms.PictureBox picFoto;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtBP;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chkAddSample;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labAction;
        private System.Windows.Forms.Label labOO;
        private System.Windows.Forms.Label labBP;
        private System.Windows.Forms.Label labMT;
        private System.Windows.Forms.Label labAF;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3; 
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboURL;
        private System.Windows.Forms.GroupBox grpFacial;
        //private Bio.Portal.ClientF.v5.UBPClientF ubpClientF1;
        private System.Windows.Forms.Button btnOcultar;
        private System.Windows.Forms.Label labMTSample;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMTSample;
        private System.Windows.Forms.PictureBox picQuestion;
        private System.Windows.Forms.PictureBox picOK;
        private System.Windows.Forms.PictureBox picNOOK;
        private System.Windows.Forms.Label labResVerify;
        private System.Windows.Forms.GroupBox grpBioSignature;
        private System.Windows.Forms.PictureBox picFotoABS;
        private System.Windows.Forms.PictureBox picBarcodeABS;
        private System.Windows.Forms.Label labTimestampAbs;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label labIdAbs;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox picSampleAbs;
        private System.Windows.Forms.Label labSampleTypeAbs;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        //private Bio.Portal.Client.v5.UBPClientAx ubpClientAx1;
        private System.Windows.Forms.CheckBox chkCL;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.PictureBox picDocBack;
        private System.Windows.Forms.PictureBox picDocFront;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.CheckBox chkRepiteToken;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox chkPP;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.RichTextBox rtxSample2;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtMTSample2;
        private System.Windows.Forms.CheckBox chkEsB64_2;
        private System.Windows.Forms.CheckBox chkAddSample2;
        private System.Windows.Forms.CheckBox chkEsB64;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtURLTestGral;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        //private Bio.Portal.Client.v5.UBPClient ubpClient1;
    }
}

