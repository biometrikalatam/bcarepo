﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BioPortal.Services.GUITest.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "17.2.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.5.99/BioPortal.Services.v5/BioPortal.Server.WS.asmx")]
        public string BioPortal_Services_GUITest_BioPortalServerWS_BioPortalServerWS {
            get {
                return ((string)(this["BioPortal_Services_GUITest_BioPortalServerWS_BioPortalServerWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.5.99/BioPortal.Services.v5/")]
        public string URL_Biometrika_Demo_Interno {
            get {
                return ((string)(this["URL_Biometrika_Demo_Interno"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://190.196.216.107/BioPortal.Services.v5/")]
        public string URL_Biometrika_Demo_Externo {
            get {
                return ((string)(this["URL_Biometrika_Demo_Externo"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://demoin.biometrika.cl/BioPortal.Services.v5/")]
        public string URL_Demo_Local {
            get {
                return ((string)(this["URL_Demo_Local"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://10.234.16.43/BioPortal.Services.v5/")]
        public string URL_Pronto_ZENN_Produccion {
            get {
                return ((string)(this["URL_Pronto_ZENN_Produccion"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://10.234.31.41:8324/BioPortal.Services.v5/")]
        public string URL_Pronto_Sharweb02_PreProduccion {
            get {
                return ((string)(this["URL_Pronto_Sharweb02_PreProduccion"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://10.234.31.53/BioPortal.Services.v5/")]
        public string URL_Pronto_Galactus_Testing {
            get {
                return ((string)(this["URL_Pronto_Galactus_Testing"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://jano.biometrika.cl/BioPortal.Services.v5/")]
        public string URL_Azure_Jano {
            get {
                return ((string)(this["URL_Azure_Jano"]));
            }
            set {
                this["URL_Azure_Jano"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:4030/")]
        public string URL_Dev_Localhost {
            get {
                return ((string)(this["URL_Dev_Localhost"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://190.98.233.230/BioPortal.Services.v5/")]
        public string URL_SML {
            get {
                return ((string)(this["URL_SML"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.17.102/BioPortal.Services.v5.8/")]
        public string URL_QA_Localhost {
            get {
                return ((string)(this["URL_QA_Localhost"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:4030/BioPortal.Server.PROXY.asmx")]
        public string BioPortal_Services_GUITest_BioPortalserverPROXY_BioPortal_Server_PROXY {
            get {
                return ((string)(this["BioPortal_Services_GUITest_BioPortalserverPROXY_BioPortal_Server_PROXY"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://190.40.131.119/BioPortal.Services.v5/")]
        public string URL_Biometrika_PERU {
            get {
                return ((string)(this["URL_Biometrika_PERU"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:4030/BioPortal.Server.WS.CL.asmx")]
        public string BioPortal_Services_GUITest_BioPortalWSCL_BioPortal_Server_WS_CL {
            get {
                return ((string)(this["BioPortal_Services_GUITest_BioPortalWSCL_BioPortal_Server_WS_CL"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://nvprod.cloudapp.net/BioPortal.Services.v5/BioPortal.Server.WS.WEB.asmx")]
        public string BioPortal_Services_GUITest_BioPortalServerWSWEB_BioPortalServerWSWeb {
            get {
                return ((string)(this["BioPortal_Services_GUITest_BioPortalServerWSWEB_BioPortalServerWSWeb"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://nvprod.cloudapp.net/BioPortal.Services.v5.5/")]
        public string URL_NVProd {
            get {
                return ((string)(this["URL_NVProd"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://190.98.233.230/BioPortal.Services.v5.5/")]
        public string URL_SML_v55 {
            get {
                return ((string)(this["URL_SML_v55"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost/BPSWebAPI/")]
        public string URL_BioPortalAPI {
            get {
                return ((string)(this["URL_BioPortalAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://nvprod.cloudapp.net/BioPortal.Services.v5.8/")]
        public string URL_NV_Test_58 {
            get {
                return ((string)(this["URL_NV_Test_58"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://35.161.36.13/BioPortal.Services.v5.8/")]
        public string URL_AFEX_QA_CAMBIOSDB {
            get {
                return ((string)(this["URL_AFEX_QA_CAMBIOSDB"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://54.186.118.182/BioPortal.Services.v5.8/")]
        public string URL_AFEX_QA_GIROSDB {
            get {
                return ((string)(this["URL_AFEX_QA_GIROSDB"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://35.163.46.89/BioPortal.Services.v5.8/")]
        public string URL_AFEX_DES_CAMBIOSDB {
            get {
                return ((string)(this["URL_AFEX_DES_CAMBIOSDB"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://34.209.221.86/BioPortal.Services.v5.8/")]
        public string URL_AFEX_DES_GIROSDB {
            get {
                return ((string)(this["URL_AFEX_DES_GIROSDB"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost/BioPortal.Services.v5.8/")]
        public string URL_AFEX_Prod {
            get {
                return ((string)(this["URL_AFEX_Prod"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://nv2.southcentralus.cloudapp.azure.com/BioPortal.Services.v5.8/")]
        public string URL_NV2 {
            get {
                return ((string)(this["URL_NV2"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:8212/")]
        public string URL_Galactus_v58 {
            get {
                return ((string)(this["URL_Galactus_v58"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9106/")]
        public string URL_Pronto_ZENN_Produccion_v58 {
            get {
                return ((string)(this["URL_Pronto_ZENN_Produccion_v58"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost/BioPortal.Services.v5/")]
        public string LocalBP {
            get {
                return ((string)(this["LocalBP"]));
            }
            set {
                this["LocalBP"] = value;
            }
        }
    }
}
