﻿using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Bio.Portal.Server.Services.WebApi
{
    public class BaseTest
    {
        protected IRestClient Client;
        string host = "http://localhost:58512/";

        [SetUp]
        public void Setup()
        {
            Client = new RestClient(host);
        }
    }
}
