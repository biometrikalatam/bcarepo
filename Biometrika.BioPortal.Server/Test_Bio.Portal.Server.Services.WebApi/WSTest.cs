﻿//using BCR_BioPortal.Domain;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BioPortal.Server.Api;
using Bio.Core.Api;

namespace Test_Bio.Portal.Server.Services.WebApi
{
    public class WSTest : BaseTest
    {
        [Test]
        public void WSEnroll()
        {
            var request = new RestRequest("api/WS/Enroll/", Method.POST);
            request.RequestFormat = DataFormat.Json;
            // TODO: Se pasa un ActionId = 1 generico, se debe reemplazar por un valor coherente con el desarrollo propio del controlador
            request.AddBody(new XmlParamIn { Actionid = 1 });

            IRestResponse response = Client.Execute(request);
            var content = response.Content;
            XmlParamOut xmlParamOut = JsonConvert.DeserializeObject<XmlParamOut>(content);

            // TODO: La igualdad tiene un valor referencial inicial, se debe cambiar por un valor coherente con el desarrollo propio del controlador
            Assert.AreEqual(3, xmlParamOut.Actionid);
        }

        [Test]
        public void WSIdentify()
        {
            var request = new RestRequest("api/WS/Identify/", Method.POST);
            request.RequestFormat = DataFormat.Json;
            // TODO: Se pasa un ActionId = 1 generico, se debe reemplazar por un valor coherente con el desarrollo propio del controlador
            request.AddBody(new XmlParamIn { Actionid = 1 });

            IRestResponse response = Client.Execute(request);
            var content = response.Content;
            XmlParamOut xmlParamOut = JsonConvert.DeserializeObject<XmlParamOut>(content);

            // TODO: La igualdad tiene un valor referencial inicial, se debe cambiar por un valor coherente con el desarrollo propio del controlador
            Assert.AreEqual(2, xmlParamOut.Actionid);
        }

        [Test]
        public void WSVerify()
        {
            var request = new RestRequest("api/WS/Verify/", Method.POST);
            request.RequestFormat = DataFormat.Json;
            // TODO: Se pasa un ActionId = 1 generico, se debe reemplazar por un valor coherente con el desarrollo propio del controlador

            XmlParamIn pin = new XmlParamIn();
            pin.Actionid = Bio.Portal.Server.Api.Constant.Action.ACTION_VERIFY;  //1
            pin.Additionaldata = null;
            pin.Authenticationfactor = 2; //Fingerprint
            pin.Minutiaetype = 21; //WSQ
            pin.Bodypart = Bio.Portal.Server.Api.Constant.BodyPart.BODYPART_ALL;  //todos
            pin.Clientid = "TestWebApiClienteId";
            pin.Companyid = 7;
            pin.Enduser = "TestWebApiEnduser";
            pin.Ipenduser = "127.0.0.1"; //ip estación de trabajo
            pin.Matchingtype = 1;
            pin.Origin = 1;  
            pin.SampleCollection = new List<Sample>();
            Sample sample = new Sample();
            sample.Data = "/6D/pAA6CQcACTLTJc0ACuDzGZoBCkHv8ZoBC44nZM0AC+F5ozMACS7/VgABCvkz0zMBC/KHIZoACiZ32jP/pQGFAgAsA1ZwA2e6A1ZwA2e6A1ZwA2e6A1ZwA2e6A3FcA4gIA10sA2/OA1xlA27fA2YfA3qMA1xTA27JA1hlA2oTA1YVA2dMA1dSA2jJA1nSA2vJA2NUA3cyA2F2A3T0A1wEA25rA2LdA3ajA2tLA4DBA2sOA4B4A2QXA3gcA3BQA4bGA2QDA3gDA28bA4VUA3coA479A30gA5YmA3UoA4yWA4G7A5utA3I+A4kXA3cQA47gA3f3A4/1A3dHA48iA3wMA5TbA4rUA6aYA3otA5KcA411A6nAA3y9A5WvA3T0A4xYA3eQA495A3V4A4z3A3mbA5HtA3mVA5HmA4LnA50VA4PCA54dA4jQA6QtA4FvA5tRA46sA6s1A5baA7UFA5JEA6+EA4v5A6f4A5rjA7ndA5mPA7hFA5mtA7hpA3qlA5MsA8ZkA+4RA5GJA66kAhs8AiCuA4hJA6OLA5o9A7kWA7AEA9M5A/IwAh0QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/6IAEQD/AgACAAJXPwREKe0nv/+mAIUAAAADBAMGCg4RCwsIGwAAAAGztbGytrevsLgCra65us4ODxARq6y7vL2+Aw0SE2amqKmqv8DBwsMEBQYHCBRqoKKkp8TFxsjKzQoMFRabn6GjpcfLF5iZmpyeyczP0NJrb3mAg4aX1QkYGTk6R2xyc3R1eHx/gYSFh4yQkpOWndTW2f+jAAMA+W+2Xf2935/n8/pnnnnn407GUt3/AJ/L+Oeeeee79n/n19Oz21+X5/TPPPPPH+7+3/t/h936/u8fh9M8888/f/v6Er/b/wAv+P8A0/sHhnnnnnn8/wDV0KfQm9Cd/r/3dCf/AIbs88888/5/8OhS6FfoT/6uhL/q/wC/DPPPPPP+n/XoYehk6F/oTuhN6Ez9njnnnnnn/wDf7+hc6G3oXuhU6E3/AGfs+Weeeeef+P8A46GToZuhc6E3+v8As/d+n0zzzzz/AKfu6GjoWOhP/r/t/uPzzzzzzz/j4/3/APP7vv8Aw/Du/TPPPPPPPP8Ann/L+Wf8s888888888/4/wAfp9Pp9M8888888888888888888888888888888888888881DxAh/RQFG1Ln82yMYE35E+b8sIDhbqnzUDQ6tV15rVMCMjjJ8veYdXUyk+XxrCDJ7R5vlg9nlNvXm9+2bRCbf5+H7nDK1H877d1UxNfqJC/wA6IPkXodS6P1IIxYI+Y3OE7egkk2FUvPt3cJ3TAfzJnZEJLzPx3q57SvOVV7odegwoeD5y/Xw2mH9CX5+PHhu7pXoZnd3/ANEiFyLzl4QY+hTMXKPQygg/UzhTNLz51juVr6FLzRjMiGK89YexVF/krQqJxR85lCfqOECVe/oUO7pBf6XCClci/wAquJIX6iVFyRBXOi6DBHkfyn63kEq43J7+eAUrpIaYtrkImC5d0/kWjIKDdCSc3LVJBDlSTJd21Y1ooRKO9VQIWhXw5YJQQ4eMUeVbG2wiVhMMoFSeU0BDQghMiUDoNU6QLsQ7KWC5SZRNi6xKysCVykMRNoRerFSCuU71dEpwrnJRXKeElxQhOzMnk6DvJMQbSjDk2Og5XiCWY3Q8Vg6Y9xk3RITwCTpL2klOEEVB1Gzq5G6AhB1kIOgfIfUAiRBdM4JJ50UEnQIcHmQhJ+REELXCCuiYEBwtcB4omrBmMX5mNcWDlEwRiQ2hoIkqz3JzRHTF6rAdGziXROpYIS1jNcQVAR0LG1YKsLJxLSdCMDHbD0baaWh8joIecePjwt1tfHbxtYRoTTMYizTWcTvC0lTYsk9jLmEloN9A6l0GuyUHSr773KoRWHqiTyo4yy2Vuh4ZMtbiW2/lLDsaZJB0LZjx9naWsqhwyOlU4dT/AC8OnJxuw3MdcS/df3jC+J4Cz657/Dx/I8C2GG6dwWo45R7umznZ2BRzkkk3EEn1AIi4gLkQdc6KZ/I4mVzRjiEDdZG1lrnKwgmLYo24RriWfKQ6lXQkDoomdG2DyC9oSWirijRM41vU8JYLRZOb7LiaRKjqcLQYaYhsRkMIwvkHS4ZVYTTaHw3Kr6I3WN1UMbmrauGk0kKqnjV593ZAiDomHhwq4SNm9YB9NXEQ78K4rHKinUU6drbjbj27/DdrmtHv2b+J3fDx+HjDJcqw6dov2ce/d7/4d3Hd4zr69pqYO74eFcMu5HTjWjRN+/HjX3TJfVKL4w26h2oPTWt6fag8UHEbmOuwN7uIqiE61tCFIR5Czr1AJeSzpXIFec/WkgQkESfsc3FXIHnJiVcgbkvKvrJBcpxDweesuYL2LtuDPrYII0kNNT2VWqGhlbKRlRGZQWmYkokdlkxotWJtCunZvy7ONtkBHlh5chU3+6VXFBLQhWUacfZ3DDfDhJaItjRC1nEgxZJcr0eoT2eUTi9Fpc3NLiHD2F4Wk4RKuQcUdhOpYFyGiWpFnErVCqa7gQi8RGuJBmICrKiAdUvuRv5HUB7HUhIMQLI3PzuwlSUSYeNZylyDLkOifSQrl5F6gERVxughwV6UUCQQQjzkvCIcKyfynlkhhDtcqP5T9ac1cVq5KG/yn62Z09JBKxLci0Mq2Qoqkixx1rDJpm0qXQUc3FB8DZjHaz9+uzU349+PsaVsy2/lQFH64b4rb+32/tysVTuyeXWil9Ia7vwol2XYhcpE07B8/l+PcXU2Yk8rx07HHD7+/BJ77jpOPHtE/myciQxk6le+HWhlRIRUHQq77TWRNNxctEajWb2vq4eqOLnXvI3K57EkpallAKYFOIEzzNcjDhhAsvSyORSTvRHVF9VbEkgghc5QQKCC9QCJRBBBIX22hQlcUHPNjIPkJNofm3xtiYBYwt+B18Ts9+CE4iIDvrPHZ49VoNhfjhwYI6Pd8fdv3BqbL8sqT0yDoV+Tfw/G3Xs+GPV3fv34nTak0tTft7q7eL7fCmqsU437iK7MeP4fmOnFaLK2RaxVmhsdhxB5Xw32cxafb8favl+WLvoTMDS3ZwG/2fzw9fgtJtugHq/Tr30rv/X4eAWgvTJOe79vHZffs7aytK6fHq49tvw4xtwps651bK/L8O23a4qqu2BWknx/BnsbRvDvidU8O2rG7HfizSdduAsWoo4Mzk64TTZ0KqoLLWrPXEMF5J53DqbpQRn1AJkkEEIP2pXIqLl9ktcXi5xNfMfryKspCuraljr44cHgx5G6vBHUer5dPHbIjs3v9/bJEaOrx6Tvwk7/AMeGHd9/fWNKx9f9MoZ+7s29PwfLZLaJ9vzP4/s6uz59XH8fX4VZieVvfT7/AL7+L+3u91cvyv8Af2aZ6+v50e/Zu2dv6/hh/wCtxdcpx7eP6VfdG3HsVuzctKZ8e/493Y38Pu+HXThtxc8px7vZ93Xt8Pd8/v8Aj+/hv7AtBPr7P/njf6+2/hx91q5UWuO+/wDn68WbKhrubSmesju2U3KkYvjzHDutuLRQvcx1K1OAd0qSg7HSU23KLnEM5ErUmBJciXdwue0Ay6iHujngQyBhJT6gExVzpQIKuXMvIxl0QudOQ4Ly6cnmh1BlzyGAtcPaDBKBEMiDoSg2B7mgqcMdZh4ek9MJBxMBLTNmd09yiDAOlPYSvLFZdE6IiuQgME9xrDrQbnojR2NbKLo5U9mW13a0mlIutpQdXFCCpEArS4LCqYPF0l9UM9YBDF4pjzbnGOEu0tuueI1m+dnTxh6ud1VpK227fb9IwRd1SyK1L2b/APH+ns3tOWzi7xpO/wB2H6f+/Bdmzc3b19tTqx8f/wBx+72fML8dvdHbK1O3X4/w9nu499ur2VvS1veeG5yN2Jo5516EvUAiKQaCbna086yxgIhxBC5pxDuHfHE09NorTduNodlWFrmFkL8urYJBRS0qBJywx3OmmkI6SYa1cPjY1dSuYjHGjm6pGOJB0FDLJsmDJgonURElO7glBQx0vMhCzsFDkuX5VSMgzxFhFTAqFoSDCS4m2ClqPqkuQwK3zjF951KHNV22y+O7wjA6jWHN47lNseA7+J1bd989t/s35U6/vw3VOg49tqdm/wBmQr+Hr/Pu/fWNMeHb1d+3uy6/Z+/4Ze7K2p/e3G9uP5Rfw4+3vZaiMjDDjD9uzj1pa3mqpBa1SKo60INzguEvUAmRcqsxDhzcdaQdKzBfaXujZe8I/ZFyhqbmIS9LxL9vayLE85JeeyshzJi6NcVh7yVKQOucXYTYEkK1x0uz3MkTKGElaVdEYvAIThwVoNZqGwBuMWxhNpiKKkkGS7ODHMYMQnLqG1kQIcKEKkEHTLwGhw5aJUnSaMEk5BJLHXZGHq8qwiSjqdZNjkbnFKErXbrxjrxGQMZVxOo9PHbelVgZ2906lXK/v7r9nC0OeJK1TQjbkuqgyH4s+spPF6kVZb5hayzxBIhByfUAiCFIjkc2j7Lb8cAnlM/pR2dTS0VaIXOYnHuaz25FzpxN5rNS4XMmTjdlLxAMoOdMMGrso0urFgloItYG1IRV0ILS6Ra8KU4tWuuJnGLF4GMCxc6VIvMY2LGS6haVERuV7XQzlyDoVrFmtcrnuS1Yy6D0LsoxROoqErCloLsYWkmCnEJkIiHOl7ltYMxg4KrrSsUssMvf2l5t7pnXQcdoXr9dMVs4bqalvDy43e+0149dlqWBGKh+qOyzw+s7nEPCTQLQDrcHG6CEivsXIfUAi6l3UAhAnnQVlcgWmx58OrAFObTTijrXYu90rcW3YWxOuNnfX2mWOWwYytdm3Yd81bq4UyCROqW3Y+1Y3tTI2bW8Uxq/x40ZCxwkLQmizSMWl1ZrArQbSHDw9sMaYcWK0E2pyFWwm8WwgLSqiAZvl3xilgdCSDis4duyN98W0mbkRGPX39PGmyI1lB7/AAwtuXfxm99KbdB39X6W6/h3+Hb7xRaDb9Ns7umf0y3ePcuPdGrH4bYi1/Zj3th2TMHTKwrLHutajYknUqkwZp2ddJco6jtmru53zYhOuZSuRkhDekoglOj6gEzQQRuR+1XEFIJXRzG5OVyPJPO+RhPBrlfCjWntfvwImuWzGGuWia7urg8u5VFjWNbqvT3VxrlDWeNZVOI49is1RbaKJaajhGN8zUEP7KnSYyPXRKcWxnC4nQR1N2JhKUbus8DK0HbijN23J7YVtSFoU799g5qKAhNGgyoulqb0013MlpXDsyBVffupwrjVaU/DZ44Vq/HrQbF7HQVHZ8et+4VViJTrQUNsRZois3QWWlHGk1gNLQUp1vaHDEQiIK53giLk6hHmUqpJKK+xOEbj6gE1Qq4jzH9S8pC9KBQJRgE+mW7CbQ7I/Y53Sa1SeDzuJfB5glMgdRhGvHez3JPctccjKErqn0wRWIFQRKB0xL+RRLzDIrQoeU7pXYh0QdMPc1zSSMSg+mEyQhG5wQdJSlB0CriXjW6JmKS8G1x1PjcpE4VMCOZ2BwhsO+ztUhaiirYRHVBjdZzzRLF+rGyVWha7ZO5abOmQc6lQF1D0LukuaCDEGClEc6BBBdEQftPkPqARVEHyn7SCkFcj+pBwjyH7DdNxTpGOZAwgZKZO55kmeYYqEJi46CphIJGGTLmUUswYsIBe46oDREEFXYoo6EwlO6TwHkI6Ewd4Tkk1SyK0pWFrC1AcU5B1VioeFyFZPY6UzG4gyVLw6Wk2U4CI6WvesA6oQwoTfvfKxsdRCdZWWNcbpPNaKEWSl1dB1s+ISGKCb0xgIWEsDEkg6mJsEYkublzKyLIOQj6XgiEY/wAxC9QCM/+mAIgBAAIBAQMECQkJDR8QBA8AALO1AQIDsrYEBbG3BgcQERITabC4CAkKDxQVaq+5CwwOFhcYGq66DRkbHR9HS0xOT1OtuxweICEkQkNFRkhKTVBRUlRVWFlaW11eYWSpq6y8vcEiJiw+P0BBRElWV1xfYqrAJSpgpysuMDc6Ozxjo6WmqMLExf+jAAMBswo+AosevFDkcCjGKG+HYedYXxdwmIcFl9Oi4l8GGjtQbqzGl9Tga3l70Tnz04XvHTS+DEdZzIejH8p4n1fx/m3n8C/jHtx7en1aaf8Az4/w8v40VcfRj3f/AFp9X/f/AL/UMxjW8v6H6f8A78j/APfb7uZrfTAm5/pz/f0006QKvjfg8v6f/h0vjEu40v2nTV/f481nR54Nd9+fN8vLR6Yxhbp6G819uvTCy+g8fE/grpcrHOG7E55MfG4R3M1caEvTMcRvDATFteBjmXxWLEeL4AJ+lHwsH32h+AikGni4xMQcR0e4VuYq4cEx43oukvqb7y/lA54SBwa5zyvDBh46muJf+enQx056F9xDGn1Yu6c5rAO1ekv4zoI043mL/wAf+fq0SjAbiF5p42M3tHBp0wWxgvwvjxfbdpZeO5LkDFyry/A1l8GGMIm/VZjQjAVN2BG6lL3aOlXwUOpwOilDT6WHgAnLEYU+sjClu9xHW6DMC79ZrozSm+OeOGunu5t9Ze+P5y+KPRz0x/Crs9s16c7m6/8AP/jy93Qv0h+8/kYH0P8AE/p/Tpf2/TjX3c/L3e43eXl/H6fp+m+ml7/Vz0Xdf9/8f+P+ZjXT6vHH9DHNfRe8/fzntenjrp06X91+27/z/wD0+on8vHpfGt3dj+XSfz+nE9rNB1d/8v8Ai/8A8dOfl+8xHTpHd00/5/liHjg0mjvNb+X8EroTGg8Pb08W9zGV9dzrp011Bjgod2t3F4IgJvWjWDe7c49HEavdlzucmFD4AJmfZTM7xzYnFhFIVh4umC+LiQvfhgxesXbxu8NMaTpeGs15404aY5xvqTyL401DtxOl9ecPKadPHGt+PPGnuHWaXdHeYv5XH3GPHDz1Dt59H26LjQus1N1/dfppOd+mms8dXgczy9uOfu01vfV3hfy+n+VzppBuu48enj4+Wlc7aMO0U8sYmL4vTvW7rfoQs3N2rsbX4hAYMvAjuY3EoInFl7EI+m6kTJ8AE7P/ADAItjvLi2WPesSz3gpjEId7egpUvwwuIXC9zN9F7BMYuuFp9CGlfVWDGip2uJq6Y93jBcU776X+nTGMS/NY7rw0KwhhIu4v7fF1mHF3BvxcwwpaMY3c8YLsxk9xDVpI92HS85ikv3EdYaYblYeF267Gnc4cYlxgMOOHMsvG8cKRgeADLNNnisYxsQOAWBLPcN7CsODEyVgcCw40jB4pMUsUiu8a54/jreMDiNHS+IBfgYJp4jkUm4GcxyOLWA1gORuJePjmExwL1iBljvxgpCjiYEiMYx34vdMUWIb7wByEeLe8LvviK0ngAmj8TZcnvYNNjuWARwEw8cQCNMON41dgt40G7BSTWMeATn0Y6KVd3roY51iEON46F5h6M1b8MY6PjjXWOMVjcXw4nP220xjEO0MTF/p0GLeB2oTy+qiOsMbzQnQ6Nm7Twxi8vMYmOKFMcV0ww3p41fXXEZddyy7ZsdwoQul4BuWnJhHvb0U0d2GxT4ALsRNj3FGR6XIwI95RYpDJ7UsEGzvJdJchFXilXvSwxwWPStERmODOkuiRjxvi18VejW7v0hOkSrwXcviOYd7pcNJekj3DGjCHBl5pkvqWxYo3u0CPEYZkIO+6CU2O8aT30XwATtPiIZPeU5sZjiS7CDMHqLwdMXvrjgy/i4vNMaBxE8cBfSaJY9BA6RdeeOeJ5UbruuuIaHtxGHaMv9WNS7fni/0puTxx5QQMPtwHaa/TzEv5I3vrHtZ7ed8PPDNG8dzNRMF/GfSpff0NNNOd5q3jc3Xbvtml/K6N+GMDcg6M1mvAnjpiExMaLxwDeFMvxwq219QQY3zOLkRfUuR4AJ2/AFOadxMNGT3GIRoTHFmNIpgVThdv0upiN+5xNL4b0mDeM1v5N50vG+OF4k5x0DS+o9phjGc/pGAx7TW/TCGhBvDHaDgGvcmpA7SBprjUNEGO80xMExpgI7iXXQl9S19+nlOntxrpHDiXd2rp7rzVvWJjuJgIxBe8IxjB4CkQup3pHMe8vZ+tc8AE7dp75mneDYafSbSHqxGn0g3pfYTWJHDE4k0ZeaGJez26RL1zzvwHIb41mMbxW+okIhR6G+ARMtWO8JrGKadDXnjdre5L4uDpNIbrmNJ5X6dHyKxuZreB4zF9NGntG+l5rAjLpvxWHTGBx6b3jiPQMXTfoWu0A8UmhiMfTdCFijuEaaT0vgAkD9c+ssYlPr0ifWcFkicCMYwFe5ZeNXH0jiMxiXHgwuoaFmntZdZgw3WG8trL0twJgPeXFnEYXDF8dpYgONUgX3NML8/E6OlPa6sueTLzRad3Sn+N+l3Ev3E9xpL40a1d5jUYATTDwcTGkJiNMdyQiS9X7x0wMA9TcMyPcR+I8AE9Yv11yI97tY9yzQDN4hG9ghxHGIMWXPTg1xccUdzFx7koo3OL3xXk4voBDtY3mji+i3R3LperzVwxHcQ0vic9JhYG++JjGPLGYb7sQosvAXS7qkcavBwMFwAcXW+kbg0dywVhHAblWaGS8QjljK/cUWKD1lHgAzBmUnedhxdiHqvQMEhxEhkBijc3mrBKx3XrDQ0wjuNbzEZi14bmxe8uwgcLy63Mr0b71igRpOLNEMjvZrGOT3kPgJqWIBHiQzbPBgYs+lj9c8AFfPsvxEYR9ZFbPrL0VjHdcFjgiEd6uaX7y+L0ZPExeYL4LBx0vLxmMEaTtK6NaHPxQvF9Dd6Ql116QHeWXGmly5wbt8JMY537y8L6asKxwusb3wwhFO1Jr5EcYhL33pHDiDZd6zS9F7jjiTGFjimHpLFKvdhCNMDgFnN8AF0fvHsKcj1sGMe5EsjZ7gKuWEp3YwkQG5DeXoYkBo34uwYxhZ3YmMxKd4EuhZjxbLDFPFuRjdhY4IXYkG/EXJsd6MMh7sEIbDiU0fAG08AFUYi4+BNcJ75CN2g7yPPRLPcCrRQ9+LxhADuWsWux4hggkbO4xemYUCPAaaItBvKUiQYuHc4mNJezixuQFjCYXDuuxWi4EDdgEhMOMarvbkEl7xLm9iy8GYpN+pMYzwvdr0NL2IN+e9hz0NWlwPAwaumJi5Di3IYvbFziLG9m8O5dr4AIoXGMPfdafgW7kR4rOZCMPTeYdMMKeDiXJ5S5gmGBuRUpi34Xl8ikl49pNRhdoJfjziF6LkIbhlzJMXJh77tXSzuWrzDa8ThcL6FmHAo0ms0xBi71cVfGAJe+4NaF589B6DuvdZ7sY6U6e3i1g0wl4e67uvz+p5n1YmJeXxwKW86E1unAvMaYLsxPHvZqYRPABNiH1xfrNix75kK97TgoDiOS+wBuVjHeQXFil4kIZvFYt4UZO8gCQpo34L3zJoEd6iZipvIgwihHeS6Mab8WxZgIb7pLikIX4LNMa3i4DinjqakYGt+D4/UV5AmNMG86c/K8v5dNJd7r3OmmkNTne5wVpWsPgAmhmR9WMhgcUDJInpFo9Y4vzL5nEGJiHqZeXuRoeJMMxcLPFmtlip3lmmNXN96IQAuKdpMUwywxe1jFs1ejegYLM1YG9ZpFmLDuBmua3jvWa4ZoXvfSF9xV9L+3Wac57r6bxnMdb87nS7jg8+nSY9uNdb6HG/R10xrzux07731GGZxLEfABNiO19NxyfW4xZPUzTTMo7hUiMeLCGhGkeBWIWcRO4iQpTuauxwEI+lbJovDAXblMR4mL3yG+IJ2oI3l2akF7cEYkWnHBg4wulIDuwU6k6aQwcAJfBL61pfgRvHHQxGXd6aeOIe3xNTEDde+CX1xitYG81xGgC/e3GDGn2MGPgAmY/E7T2KWPSYsA+piFEO5oyLJ3gxgEOKS9MW7DgTRml4QrV3t7y6xQlzgTGZLq6b0jDFyMuX3u1jeXdzcEY0YMLuvddNIeLowd+jMXehjXXGHhrcLumrbBwHSLNJzuJwve8QnidL4O4G2Nb4jw6RjEp7280mHM78BSeACrkMyPcU5p3ilmxxI3cN7p6QmoZr34vfIPVgxMWJg4ECx62GRCFO8Yxeo3ER2tB2tmkwBxY53Sg3jEjMTDhhvxYcXEi72Gs05zoQI8MVq4NG6w9SUwjwC2kEs8LuCsYI4OJDLBD1th8AFXH+4IPrEaIFMPWTHrQZiLRxGIGw7nGGLTDghV2JQ3d4DCYjLqnaMXJIvpGwxg7mgbxxdKd7Fiag+/hxCHetYNJqg8bzSXuQEgb7t22lCL3rLhY4uuo0QE4ETIjA8AF3PiY2X0kMj1kLPqaXMeAlMcg9IxcjjeF2mjvZiiLTTvVVycdwMVLYo4NNmzTveojF3AFjY70yCGDu1jZGNy/BYRjB9KDi9nvC+ri5WL0cLoXbOCBvc1H30LAeAC7nwrFjD0lOYdy1iEfU5Ys+ogYsR7yzYXvxRGNL3BLxiwhvUuU2I8QMixDhcCxkMe1sJTDEO4csMXgBG6jqxxxu4nMFInfg0q+kw8HBzmNLhdmL8HnEnNWGO9Zi+w7wo8AE7Prv2D67/gH/oEcj4mx8BR3mTCDYdwG0h3qxs08Bj9kjYidzkHfexsDi5BEaeBer3Ul4HdiNj1l9MWSg7tVs0rxbuxMd74AKA/YP7j4GmPsMj2OT8D1rxT4V7CG8j1vrNqLvQj8LDaO887vaWA0B3lFNIcLpWIwO9FhTR6QCiCd6BCz3EVoj4AJwfcPY9T3r1vpMyL6cB7Gkdg8GOZGnuMjJae0dqR4jZsjDiuxPS5NimO5zYNmO8ywRfUtYaSMN+ItLYjwdVYlDxxjEKKbjxSYC94se7EbGT76+ACtI+wyfYQgx9ZDJ9/Ah7CDEpae5IxyPSANFHcjBswjwSjJgx7jNhiPFsnrLBCXhB3Fi9mIvAoMlp34mBotg9TApO/BHAojx0KSGLvexG8UId7mw9Z4AM4xPhT4n4j67D7Cf8AR7zsHubGadz1jxYfZIkPUbQjwchCK8Wzj4blOT7Cx3MM2jHEhGmx6TwAb9+wx99sWPrGR6Qs7HiUPwtD7CNmiMYbmMNrxP6hoojxMnYcDMjk+ljCh72wQp+HEO5g5PqL0bB9L4AqWcvg5O71lijk7kOUGZnJ3OT+9xyZR9B7D/QYew7D9bY+s/Qpm2O0h1P6WOTtPY/mcwydrk5H6SGRmec5NA07Sn6x8zsbMMimOxo7HJ+Y63sEph1j1Jk/kaGNnrKLMbNGw+Y6ymjYNHnIx6j9z2NLtPobJsfQ5uRmn4SjrKSmwWIbTYx+VyNl7MLrDqXeWOw+4w2kxRZsUcESPygq7SlHMs7Xzp8xtNhCFMGEfecj6GD5nJI2f1GZYhkZMNoZm0/G9bFpyKPOwycnsI/eIZnW+8RyKYRDrP7mhbA1iELJ6EIy7khmUQ+27UibSz5hsOw7Cz94ohYI5HUUhCI9ZkQ/xae4o2EaPO/KBDJDcZMCLCjJKYfcvkxgkY5MbNk8xGzkUH3XIhHJD0OTtOts0fGtDeN9jZ6iFNnqYNEPuuSObmFmFhs5kMmFrlEKP7zNida+d2tjzNB9thkxg5lOTtGMUpWl9B/ekLPU9jtKLXyLFj7xDJsUQ94LESLFLNmmj4zGMlpyYWOtYuQkQsfMBsDNIUEbJEjZpHJhkf4tJDrbJRGEYQEyKNjZ++NkeszUoFikcghGj8LmU0EavQmbkYLLV2FrwpfvMSzsbNLYyFRgkWHW0fbY0O12FhjSEAppLP4wps5mSFELMIg0lmxRT902OZZKBaRYDTDaWabHykaKabMGGLkxSUxyYRCmnYf4DTA2sc1iUhkxIU5HyFGTCNm4RgllBg0ZjtPkHIpDFnBeI3W8TYllPMfGgg0RMjEI1dl4xjZI0DmfI0TARgpBvjIRsZEELORH5my3YEbCxhDJsljznxuRYEi0ZCWRo7GJH8YwmEGJEsEGNJi7SZXooyPjcgYA2YUxopSN39rZtgYbSwwYRoHsT5m9NhKaMkSxhNhCERyfwGSKWHrHNpIxpMiyfgcymMMdbRHzmZ85CFFEYdZkQdj+ZsYpsdZZoSCMczMsfiPYQs+d+dzNgQYWIMSNOTGnY/kY5lHYp+toobN3J7Gw2Mk+gp2kGnzJB2NGxMfKGTHrDrdrRR+hoofMUnJqxDk9JZDMo+A/MiIkfgf1vcU9o/jYsLMOTsNkIx7Hqf2kOT8Q8BJs/6MAAwHwBA8o8AETPOcuA8AEbf1H2mx/7nmOTOv2SHzPvHvlHWn5XIMjN9ZHk3tEbP43YdTtcgj7xmfjXi7jifbDN2YzLMMj9rHeHmMiFn8oWCmkyYZGT2GZD5wyMldj6AyX5na0PYU0GS7HY0/hOtKMmyuxgdbkx/E2XYRzWMc2OZHafGC5sYRgscZsepLFMIw+cyWA5JY2HWRYQ+YpL0hYhYvFLGxpKacQpRD8BRTcaQybMDNcxwkVoh8gU5LtXaOOpgdZ1h/gZMYQzYowgMKIgZhTR/kWIRwlJL3orGDMNoFLhsxs/cbEQBs4WDDGGOIFXozYWI2PjwwisAuU00DhoyKYrTSsYU/gbA7QgXGi+zDS5tlbGwI/3tgKYAIWQCMLEabYppj2v/qFmBeMIRq9wgRspGFNFm9Fggv3GC5JRhVNLoWCijYxoitr0QP7wI5MIMYhi9wFh1lPoWxTH/2KaWlYRwGAIsIvZeYhTQQjQOR/aZrLlwiiWUswYG1aV2HxuwLl4i1eOy4dTBXNV2B9toI3hRhxTGiEauRQjZgZK/MNM0KxCLEAjYzXIyCMYbS/9rsaaVLkY3FydpRtNi2U/wAWxtKQpGmgoyWzh2EIv37jljEYGMMUGCqXIRhRBhQUWY/daMkmC+Cxm5JtWGxc37hCFIECYLt72QjRGn3jIj90oKXIXUuwCMbLS5L+YzKcOMWvesVgfOPnbNj42xTEogxx1A9bCO1+QGiwTEvAiARjHuNr9tjCFMZhlzWMOoycwitFFn/FsC5K4C94Ew5LL2CnaUWX5WEbCsuR2lK9jStj/IybMQmKCEYWaGykOpfzBHEKewzfMZh8pYyvFaaKLMdj1Lho+VAhS2IlnY+ci0fOFFsQbDikKe162FB8ZEpgGZmxoozLPvB8wFkCOIQ2NB1P5XMjRCNsYI8X8q0ZBRfYrtdrlgPoDJcmnNzaXMhsf8zYqtGwsZH6QyKaB6zaxY0H6Ai0+kyYUdR8o5B8JmFja/GY3rwI/qTIooIX2rYo3v31iGbmbyjJ/KsPgNi0r9C7Snre1/W7WzR9g+Vps0wzbHEDNYfgDqYbzew/0V2OTm7GzybHc+Z/MFEdjyb3I6g3v/Zj1hDtf9UIws0fE/kXY9RtXk2vvHmf2ngQwD4AMA+ACEvJxMnrOo5ODYhyeRLHJ9PgOUCf6P2nlmvUPU8n45RzyfU5PDybGxsbI8nF8zyazqPtP+zyeHIzeT8MOTueAECicv05Pp4AX08vY5RB7x4Efc+AF+NHKAYeAC6tHgAwz8T+49htOTKcH9Icnl7Xk4Fjk9KFnsdx2Ef2Mdjycn+s/E73lHv7DrNh8B+M6j1OT+1eTphYZBwfMvofvBm8nU8zT/1dryaTrKeWa0d7Di/mOAbTqKOTW9T3J8hsfQdhydH/AHLOxdx+5+0eg+Q2HW7AzCiL+o7Q8z1n530Ecg87sPnMijadZHk/Pqcil+gsda8oBp87+Ves+0/iI5mbDMsWeTQ2LHUdr+1o5OZTCxkbApi5BsX6HM2C8nMpzNpsNhHqPmOxybHJ0cjN7HYf6uws5K8nV5PofA/6vwMdjY/3MzlFvJuKOU4ctk99hyaDc8mU5P5Qf1vgA7BseXq2PBZ/Dk/PKCcmnl6MPABeDa07Xl0vY2fiPoPMZnecmY5N7sIZPJ6OTe5lnM7Duf2mR7z2nykP/E/IUU9bkcusg07DY8m85Px5z3jePmP7XIY9gMIe+/6DyeB9byZTI5PBRkMfgesfnafhPzkHsT67mdp/abH6ye8fIG0esyfrOT+Y7nzMYtGR/c+Y3MffKPuL1FjYtnJj5kMizkffepe9zfM5n23N2O54NnM+8xGizk5ux7EyY5G15X7DM6z2FFnZeHJ6KPvmRDYfEn6zaZBAhtKDI/a5neZn4X/qsfoX1ObHNPxH9hD8icGG1yaV2PJ5bP5FM2FjM7CNl2mZT91jGnvLMaPOMPxObZLOR6HqIU/qMnzjy33qT0vW7D5zaR5ObmsGww6kjRTHN/Q5McdTQ+doj+J2LtI0jtPMWD5zsMh8zZetsWPyOSsSxYc3/ZooyaNjtLHnaPwrTEgkaWxSdQ5mYOT8ZGztYQDie8/gCw2drRk5GwclgbH/ACMmFgKLNDkWKOosAfKwixhkdQbSFHWWfwMZiDnhYo2KbOwswzPwlnC2aIWIRbCdTGDCBH8Ny9lhkxpjAyOxYGT8pCIXoMYV2kUppLKLGxH8WGgyPeOwphR+VYQKLLByLGTsYWY/O2CGahsIrSvmfoadpcYbw6mH6CEPOAWdi/oLGTHsLEWjaEP9kyIdhDMAo/UbDk5tjY9R7zmf5lgyeTuUeBF7vgAip4AbE+c5Pz/U7H/Z718x8h/5Ow5Y5/4Gw2vLIcj+x/C+ACMvacuJ7T9hxLNj0H6HlJmTHIs8oY/O9Z2FH7DreUMcnFyKaepzep5ej6DluHKFNpy7Wz2FjI7H/c3lnk9PnP8As09THM5djk8nR5Px4AIO8v5NhycXlBPKEfABAXl/ngLur4AgGZHgAgJkcnY5Obm05kdrykjkyngAvjDk+kfABADl5PJ/IeAEEvgA+BtfsH53c+AC5nKseTMep++/2PgAsByo0858Z9p5QZy/n6HwAQl5Tzy5nlHvJoM3cZFix9D/AFH7jqPQWLG0/ccmY2HL8dh5h2NG05ND5j1H62xsN58zDMs2LGxphkfpKacw8zkbSwZFOT8hkda9bm9j+I6mNig7z/UDk9tlsAWCy5GRkZlNH4DIyIRWORmWNx/m08nlh2lPrPwtjJXzh1P+4Ux7A2uZmfoXadTyajqPsh+dp2Hc/sXI4nJxdz+0hseUeeAE0tPgWO74Evn/AP+h";
            sample.Minutiaetype = 21; //WSQ
            sample.Additionaldata = null;
            pin.SampleCollection.Add(sample);

            pin.SaveVerified = 1;
            pin.Threshold = 10000; //Definido por BioPortal
            pin.Userid = 0;
            pin.Verifybyconnectorid = "NONE"; //Fijo
            pin.InsertOption = 2;  //No
            pin.PersonalData = new PersonalData();
            pin.PersonalData.Typeid = "RUT";
            pin.PersonalData.Valueid = "21284415-2";
            pin.PersonalData.Name = "Gustavo";
            pin.PersonalData.Patherlastname = "Suhit";
            pin.PersonalData.Motherlastname = "Gallucci";
            pin.PersonalData.Dynamicdata = new DynamicDataItem[1];
            DynamicDataItem item = new DynamicDataItem();
            item.key = "TipoIdentity";
            item.value = "Usuario";
            pin.PersonalData.Dynamicdata[0] = item;
            pin.OperationOrder = 1;  //Solo Local
            request.AddBody(pin);
            //request.AddBody(new XmlParamIn { Actionid = 1 });
            
            IRestResponse response = Client.Execute(request);
            var content = response.Content;
            XmlParamOut xmlParamOut = JsonConvert.DeserializeObject<XmlParamOut>(content);

            // TODO: La igualdad tiene un valor referencial inicial, se debe cambiar por un valor coherente con el desarrollo propio del controlador
            Assert.AreEqual(1, xmlParamOut.Actionid);
        }

        [Test]
        public void WSGet()
        {
            var request = new RestRequest("api/WS/Get/", Method.POST);
            request.RequestFormat = DataFormat.Json;
            // TODO: Se pasa un ActionId = 1 generico, se debe reemplazar por un valor coherente con el desarrollo propio del controlador
            request.AddBody(new XmlParamIn { Actionid = 1 });

            IRestResponse response = Client.Execute(request);
            var content = response.Content;
            XmlParamOut xmlParamOut = JsonConvert.DeserializeObject<XmlParamOut>(content);

            // TODO: La igualdad tiene un valor referencial inicial, se debe cambiar por un valor coherente con el desarrollo propio del controlador
            Assert.AreEqual(5, xmlParamOut.Actionid);
        }
    }
}
