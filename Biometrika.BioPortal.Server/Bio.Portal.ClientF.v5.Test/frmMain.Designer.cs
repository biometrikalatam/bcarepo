﻿namespace Bio.Portal.ClientF.v5.Test
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCopyFoto = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCopyFirma = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.ubpClientF2 = new Bio.Portal.ClientF.v5.UBPClientF();
            this.ubpClientF1 = new Bio.Portal.ClientF.v5.UBPClientF();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCopyFoto);
            this.groupBox1.Controls.Add(this.ubpClientF1);
            this.groupBox1.Location = new System.Drawing.Point(30, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(246, 189);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Capture Foto...";
            // 
            // btnCopyFoto
            // 
            this.btnCopyFoto.Enabled = false;
            this.btnCopyFoto.Location = new System.Drawing.Point(157, 38);
            this.btnCopyFoto.Name = "btnCopyFoto";
            this.btnCopyFoto.Size = new System.Drawing.Size(75, 23);
            this.btnCopyFoto.TabIndex = 2;
            this.btnCopyFoto.Text = "&Copiar";
            this.btnCopyFoto.UseVisualStyleBackColor = true;
            this.btnCopyFoto.Click += new System.EventHandler(this.btnCopyFoto_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnCopyFirma);
            this.groupBox2.Controls.Add(this.ubpClientF2);
            this.groupBox2.Location = new System.Drawing.Point(302, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(244, 189);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Capture Firma...";
            // 
            // btnCopyFirma
            // 
            this.btnCopyFirma.Enabled = false;
            this.btnCopyFirma.Location = new System.Drawing.Point(157, 38);
            this.btnCopyFirma.Name = "btnCopyFirma";
            this.btnCopyFirma.Size = new System.Drawing.Size(75, 23);
            this.btnCopyFirma.TabIndex = 3;
            this.btnCopyFirma.Text = "&Copiar";
            this.btnCopyFirma.UseVisualStyleBackColor = true;
            this.btnCopyFirma.Click += new System.EventHandler(this.btnCopyFirma_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(81, 246);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(111, 117);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(106, 217);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(259, 233);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(259, 262);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(123, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // ubpClientF2
            // 
            this.ubpClientF2.AuthenticationFactor = 2;
            this.ubpClientF2.BackColor = System.Drawing.SystemColors.Control;
            this.ubpClientF2.ControlType = 2;
            this.ubpClientF2.DEBUG = false;
            this.ubpClientF2.Location = new System.Drawing.Point(6, 19);
            this.ubpClientF2.Minutiaetype = 41;
            this.ubpClientF2.Name = "ubpClientF2";
            this.ubpClientF2.Operationtype = 1;
            this.ubpClientF2.PATHDEBUG = "c:\\";
            this.ubpClientF2.Size = new System.Drawing.Size(145, 160);
            this.ubpClientF2.TabIndex = 2;
            this.ubpClientF2.Timeout = 3000;
            this.ubpClientF2.Tokencontent = 1;
            this.ubpClientF2.Typeid = "RUT";
            this.ubpClientF2.Umbralcalidad = 400;
            this.ubpClientF2.Valueid = null;
            this.ubpClientF2.OnCompleteEvent += new System.EventHandler(this.ubpClientF2_OnCompleteEvent);
            this.ubpClientF2.OnClearEvent += new System.EventHandler(this.ubpClientF2_OnClearEvent);
            // 
            // ubpClientF1
            // 
            this.ubpClientF1.AuthenticationFactor = 2;
            this.ubpClientF1.BackColor = System.Drawing.SystemColors.Control;
            this.ubpClientF1.ControlType = 1;
            this.ubpClientF1.DEBUG = false;
            this.ubpClientF1.Location = new System.Drawing.Point(6, 19);
            this.ubpClientF1.Minutiaetype = 41;
            this.ubpClientF1.Name = "ubpClientF1";
            this.ubpClientF1.Operationtype = 1;
            this.ubpClientF1.PATHDEBUG = "c:\\";
            this.ubpClientF1.Size = new System.Drawing.Size(145, 160);
            this.ubpClientF1.TabIndex = 1;
            this.ubpClientF1.Timeout = 3000;
            this.ubpClientF1.Tokencontent = 1;
            this.ubpClientF1.Typeid = "RUT";
            this.ubpClientF1.Umbralcalidad = 400;
            this.ubpClientF1.Valueid = null;
            this.ubpClientF1.OnCompleteEvent += new System.EventHandler(this.ubpClientF1_OnCompleteEvent);
            this.ubpClientF1.OnClearEvent += new System.EventHandler(this.ubpClientF1_OnClearEvent);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 215);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Biometrika BioPortal Helper - Get Images...";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private UBPClientF ubpClientF1;
        private System.Windows.Forms.Button btnCopyFoto;
        private System.Windows.Forms.GroupBox groupBox2;
        private UBPClientF ubpClientF2;
        private System.Windows.Forms.Button btnCopyFirma;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}