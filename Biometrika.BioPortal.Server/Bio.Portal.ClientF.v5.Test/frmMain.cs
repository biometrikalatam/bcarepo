﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bio.Portal.ClientF.v5.Test
{
    public partial class frmMain : Form
    {

        public string _strFotoB64;
        public string _strFirmaB64;

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            btnCopyFoto.Enabled = false;
            btnCopyFirma.Enabled = false;

            ubpClientF1.Init(1);
            ubpClientF1.Typeid = "Tome";
            ubpClientF1.Valueid = "Foto";
            ubpClientF1.Init(4);
            ubpClientF1.SetSizeRet(200, 200);
            this.ubpClientF1.InitCtrl(60);


            ubpClientF2.Init(2);
            ubpClientF1.Typeid = "Tome";
            ubpClientF1.Valueid = "Firma";
            ubpClientF1.SetSizeRet(120, 80);

        }

        private void ubpClientF1_OnCompleteEvent(object sender, EventArgs e)
        {
            _strFotoB64 = ubpClientF1.GetImagenB64();
            if (String.IsNullOrEmpty(_strFotoB64)) btnCopyFoto.Enabled = false;
            else btnCopyFoto.Enabled = true;
        }

        private void ubpClientF2_OnCompleteEvent(object sender, EventArgs e)
        {
            _strFirmaB64 = ubpClientF2.GetImagenB64();
            if (String.IsNullOrEmpty(_strFirmaB64)) btnCopyFirma.Enabled = false;
            else btnCopyFirma.Enabled = true;
        }

        private void ubpClientF1_OnClearEvent(object sender, EventArgs e)
        {
            _strFotoB64 = null;
            btnCopyFoto.Enabled = true;
        }

        private void ubpClientF2_OnClearEvent(object sender, EventArgs e)
        {
            _strFirmaB64 = null;
            btnCopyFirma.Enabled = true;
        }

        private void btnCopyFoto_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(_strFotoB64)) {
                Clipboard.SetDataObject(_strFotoB64);
                MessageBox.Show("Foto Copiada!", "Aviso...", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnCopyFirma_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(_strFirmaB64))
            {
                Clipboard.SetDataObject(_strFirmaB64);
                MessageBox.Show("Firma Copiada!", "Aviso...", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = ubpClientF1.GetImagen();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pictureBox2.Image = ubpClientF2.GetImagen();
        }
    }
}
