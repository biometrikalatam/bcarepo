﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Bio.Portal.ClientF.v5.Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ubpClientF1.InitCtrl(60);
        }

        private void btnGetImage_Click(object sender, EventArgs e)
        {
            this.pictureBox1.Image = ubpClientF1.GetImagen();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int op = Convert.ToInt32(textBox1.Text.Trim());
            ubpClientF1.Init(op);
            ubpClientF1.Typeid = "RUT";
            ubpClientF1.Valueid = "21284415-2";
            ubpClientF1.Minutiaetype = 41;
            ubpClientF1.AuthenticationFactor = 4;
            ubpClientF1.Operationtype = 1;
            ubpClientF1.Tokencontent = 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ubpClientF1.BackColor = Color.FromName(textBox2.Text.Trim());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ubpClientF1.SetColorImageBack(textBox3.Text.Trim());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string b64 = null;
            using (FileStream fileStream = File.OpenRead(textBox4.Text.Trim()))
            {
                MemoryStream memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
                b64 = Convert.ToBase64String(memStream.ToArray());
            }
            ubpClientF1.SetImage(b64);
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string im = ubpClientF1.GetImagenB64();
            if (im!=null)
            {
                richTextBox1.Text = im;
                MessageBox.Show(im.Substring(1,25),"Foto B64");
            } 
            else
            {
                richTextBox1.Text = "Foto Nula";
                MessageBox.Show("Foto Nula", "Foto B64");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ubpClientF1.SetSizeRet(Convert.ToInt32(this.txtXR.Text), Convert.ToInt32(this.txtYR.Text));
        }
    }
}
