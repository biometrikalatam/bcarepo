﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.ServiceModel.Security.Tokens;
using System.Text;
using Bio.Core.Api;
using Bio.Core.Api.Matcher.Interface;
using Bio.Core.Constant;
using Bio.Core.Wsq.Encoder;
using BioPortal.Server.Api;
using log4net;
using System.Data;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.cl.registrocivil;
using FreeImageAPI;
using System.Runtime.InteropServices;
using System.Globalization;

namespace Bio.Core.Matcher.Connector.SRCeI.BioVerify2015
{
    public class Connector : IConnector
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Connector));

#region Private properties propietario
        RUN _currenRUN;
        bool _getInfo;
        bool _getFoto;
        bool _getFirma;

        private string _urlWS = "https://terceros.sidiv.registrocivil.cl:9443/InteroperabilityPlateform/TercerosDS64ProxyService";
        private int _qReintentosVerify = 3;
        private int _timeout = 30000;
        private int _idEmpresaSRCeI = 61003000;
        private string _userSRCeI = "usr_medleg";
        private string _claveSRCeI = "H%E1K!9&rC2U";
        private string _ip;
        private int _threshold = 3500;
        //0 - Todo 
        //1 - Solo Info
        //2 - Solo Foto
        //3 - Solo Firma
        //4 - Info + Foto
        //5 - Info + Firma
        //6 - Foto + Firma
        private int _typeget = 0;
        private int _wFoto = 140;
        private int _hFoto = 140;
        private int _wFirma = 200;
        private int _hFirma = 85;


        private bool _isConfigured; //Solo para control en initialization
        private string _pathcertifySRCeI;
        private byte[] _certifySRCeI;
        private string _pathpvkey;
        private string _pvkey;
        private string _pathcertifcatekey;
        private string _certifcatekey;
       
#endregion Private properties propietario

#region Public properties propietario
        public int WFoto
        {
            get { return _wFoto; }
            set { _wFoto = value; }
        }
        public int HFoto
        {
            get { return _hFoto; }
            set { _hFoto = value; }
        }
        public int WFirma
        {
            get { return _wFirma; }
            set { _wFirma = value; }
        }
        public int HFirma
        {
            get { return _hFirma; }
            set { _hFirma = value; }
        }
        
        public string PathcertifySRCeI
        {
            get { return _pathcertifySRCeI; }
            set { _pathcertifySRCeI = value;
                  byte[] _certifySRCeI = File.ReadAllBytes(this._pathcertifySRCeI);
            }
        }

        public string PathCertifcatekey
        {
            get { return _pathcertifcatekey; }
            set { _pathcertifcatekey = value;
                  _certifcatekey = ReadFromHDD(this._pathcertifcatekey);
            }
        }

        public string PathPvkey
        {
            get { return _pathpvkey; }
            set { _pathpvkey = value;
                  _pvkey = ReadFromHDD(this._pathpvkey);
            }
        }

        public string UrlWs
        {
            get { return _urlWS; }
            set { _urlWS = value; }
        }

        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        public int IdEmpresaSRCeI
        {
            get { return _idEmpresaSRCeI; }
            set { _idEmpresaSRCeI = value; }
        }

        public string UserSRCeI
        {
            get { return _userSRCeI; }
            set { _userSRCeI = value; }
        }

        public string ClaveSRCeI
        {
            get { return _claveSRCeI; }
            set { _claveSRCeI = value; }
        }

        public string Ip
        {
            get { return _ip; }
            set { _ip = value; }
        }

        public int Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        #endregion Public properties propietario

#region Private Method

        /// <summary>
        /// Inicializa las variables del objeto desde config, para no perder tiempo en buquedas luego
        /// </summary>
        private void Initialization()
        {
            try
            {
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization IN...");
                if (_config == null || _config.DynamicDataItems == null)
                {
                    _isConfigured = false;
                    return;
                }

                foreach (DynamicDataItem dd in _config.DynamicDataItems)
                {
                    if (dd.key.Trim().Equals("UrlWS"))
                    {
                        _urlWS = dd.value.Trim();
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization URL = " + _urlWS);
                    }
                    if (dd.key.Trim().Equals("QReintentosVerify"))
                    {
                        _qReintentosVerify = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("Timeout"))
                    {
                        try
                        {
                            _timeout = Convert.ToInt32(dd.value.Trim());
                        }
                        catch (Exception ex)
                        {
                            _timeout = 30000;
                        }
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization Timeout = " + _timeout.ToString());
                    }
                    if (dd.key.Trim().Equals("IdEmpresaSRCeI"))
                    {
                        _idEmpresaSRCeI = Convert.ToInt32(dd.value.Trim());
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization IdEmpresaSRCeI = " + _idEmpresaSRCeI.ToString());
                    }
                    if (dd.key.Trim().Equals("UserSRCeI"))
                    {
                        _userSRCeI = dd.value.Trim();
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization UserSRCeI = " + _userSRCeI);
                    }
                    if (dd.key.Trim().Equals("ClaveSRCeI"))
                    {
                        _claveSRCeI = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("Threshold"))
                    {
                        _threshold = Convert.ToInt32(dd.value.Trim());
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization TH = " + _threshold.ToString());
                    }
                    if (dd.key.Trim().Equals("WFoto"))
                    {
                        _wFoto = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("HFoto"))
                    {
                        _hFoto = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("WFirma"))
                    {
                        _wFirma = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("HFirma"))
                    {
                        _hFirma = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("TypeGet"))
                    {
                        _typeget = Convert.ToInt32(dd.value.Trim());
                        if (_typeget < 0 || _typeget > 6) _typeget = 0;
                        if (_typeget == 0 || _typeget == 1 || _typeget == 4 || _typeget == 5)
                        {
                            _getInfo = true;
                        }
                        if (_typeget == 0 || _typeget == 2 || _typeget == 4 || _typeget == 6)
                        {
                            _getFoto = true;
                        }
                        if (_typeget == 0 || _typeget == 3 || _typeget == 5 || _typeget == 6)
                        {
                            _getFirma = true;
                        }
                    }
                    if (dd.key.Trim().Equals("PathCertifySRCeI"))
                    {
                        _pathcertifySRCeI = dd.value.Trim();
                        _certifySRCeI = File.ReadAllBytes(this._pathcertifySRCeI);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization PathCertifySRCeI = " + _pathcertifySRCeI);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization _certifySRCeI.len = " + _certifySRCeI.Length);  
                    }
                    if (dd.key.Trim().Equals("PathPvKey"))
                    {
                        _pathpvkey = dd.value.Trim();
                        _pvkey = ReadFromHDD(this._pathpvkey);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization PathPvKey = " + _pathpvkey);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization _pvkey.len = " + _pvkey.Length);
                    }
                    if (dd.key.Trim().Equals("PathCertificateKey"))
                    {
                        _pathcertifcatekey = dd.value.Trim();
                       _certifcatekey = ReadFromHDD(this._pathcertifcatekey);
                       LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization PathCertificateKey = " + _pathcertifcatekey);
                       LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization _certifcatekey.len = " + _certifcatekey.Length);
                    }
                }
                _ip = BioVerify2015.Utiles.GetIP();
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization IP = " + _ip);
                _isConfigured = true;
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Initialization OUT!");
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015 Initialization Error", ex);
                _isConfigured = false;
            }
        }

        private CustomBinding GetCustomHttpBinding()
        {
            CustomBinding binding = new CustomBinding();
            try
            {
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.GetCustomHttpBinding IN...");
                binding = new CustomBinding();
                // Open and Close = 20s 
                binding.OpenTimeout = new TimeSpan(0, 0, this._timeout);
                binding.CloseTimeout = new TimeSpan(0, 0, this._timeout);
                // Send and Receive = 300s
                binding.SendTimeout = new TimeSpan(0, 0, this._timeout + 10);
                binding.ReceiveTimeout = new TimeSpan(0, 0, this._timeout + 10);
                // ++ Setting security binding ++
                var userNameToken = new UserNameSecurityTokenParameters();
                userNameToken.InclusionMode = SecurityTokenInclusionMode.AlwaysToRecipient;

                SecurityBindingElement securityElement;
                securityElement = new AsymmetricSecurityBindingElement();
                (securityElement as AsymmetricSecurityBindingElement).RecipientTokenParameters = new X509SecurityTokenParameters(X509KeyIdentifierClauseType.IssuerSerial, SecurityTokenInclusionMode.Never);
                (securityElement as AsymmetricSecurityBindingElement).InitiatorTokenParameters = new X509SecurityTokenParameters(X509KeyIdentifierClauseType.IssuerSerial, SecurityTokenInclusionMode.AlwaysToRecipient);
                securityElement.DefaultAlgorithmSuite = SecurityAlgorithmSuite.Basic256;
                securityElement.SecurityHeaderLayout = SecurityHeaderLayout.Strict;

                //securityElement.MessageProtectionOrder = MessageProtectionOrder.SignBeforeEncrypt;
                //otra forma: securityElement = new TransportSecurityBindingElement();

                securityElement.AllowInsecureTransport = false;
                securityElement.EnableUnsecuredResponse = true;
                securityElement.IncludeTimestamp = false;
                securityElement.LocalClientSettings.DetectReplays = false;
                securityElement.SetKeyDerivation(true);
                securityElement.EndpointSupportingTokenParameters.Signed.Add(userNameToken);
                securityElement.MessageSecurityVersion = MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10; //.WSSecurity11WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10;
                binding.Elements.Add(securityElement);

                // ++ Setting message encoding binding ++
                var encodingElement = new TextMessageEncodingBindingElement();
                encodingElement.MessageVersion = MessageVersion.Soap11;
                encodingElement.WriteEncoding = Encoding.UTF8;
                //encodingElement.MaxReadPoolSize = 50000000;
                //encodingElement.MaxWritePoolSize = 50000000;
                encodingElement.ReaderQuotas.MaxArrayLength = 50000000;
                encodingElement.ReaderQuotas.MaxStringContentLength = 50000000;

                binding.Elements.Add(encodingElement);

                // ++ Setting https transport binding ++
                var httpsElement = new HttpsTransportBindingElement();
                // Messagge buffer size
                httpsElement.MaxBufferSize = 50000000;
                httpsElement.MaxReceivedMessageSize = 50000000;
                httpsElement.MaxBufferPoolSize = 50000000;

                // Others
                httpsElement.UseDefaultWebProxy = true;
                binding.Elements.Add(httpsElement);
            }
            catch (Exception ex)
            {
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.GetCustomHttpBinding Error", ex);
            }
            LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.GetCustomHttpBinding OUT!");
            return binding;
        }

        // callback used to validate the certificate in an SSL conversation
        private static bool ValidateRemoteCertificate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors policyErrors)
        {
            bool result = false;
            string lower = cert.Subject.ToLower();
            if (lower.Contains("terceros.sidiv.registrocivil.cl") || lower.Contains("servers ca") || lower.Contains("technical root ca"))
            {
                result = true;
            }

            return result;
        }

        private byte[] ExtraeWSQFromParamIn(XmlParamIn oXmlIn)
        {
            byte[] wsqr = null;
            try
            {
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.ExtraeWSQFromParamIn IN...");
                foreach (var sample in oXmlIn.SampleCollection)
                {
                    if (sample.Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                    {
                        wsqr = Convert.FromBase64String(sample.Data);
                        break;
                    } else if (sample.Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW)
                    {
                        byte[] raw = Convert.FromBase64String(sample.Data);
                        WsqEncoder encoder = new WsqEncoder();
                        encoder.EncodeMemory(raw, 512, 512, out wsqr);
                        break;
                    }  
                }
                 
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.BioVerify2015 ExtraeWSQFromParamIn Error", ex);
            }
            LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.ExtraeWSQFromParamIn OUT!");
            return wsqr;
        }

#endregion Private Method

#region Implementation of IConnector

        private string _connectorId;

        private DynamicData _config;

        /// <summary>
        /// Id identificador del conector
        /// </summary>
        public string ConnectorId
        {
            get { return _connectorId; }
            set { _connectorId = value; }
        }

        /// <summary>
        /// Pares de key/value de configuracion para el conector
        /// </summary>
        public DynamicData Config
        {
            get { return _config; }
            set
            {
                 _config = value;
                 if (!_isConfigured) Initialization();
            }
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */
            int _ret = Errors.IERR_OK;
            string msg;
            string xmloutbp;
            int iretremoto;
            int _currentThreshold;


            DynamicData oXmlout = new DynamicData();
            oXmlout.AddValue("message", "");
            oXmlout.AddValue("trackid", "");
            oXmlout.AddValue("status", "");
            oXmlout.AddValue("result", "");
            oXmlout.AddValue("score", "");
            oXmlout.AddValue("threshold", "");
            oXmlout.AddValue("timestamp", "");
            //Added 02-2015 para agregar todas las BpTxConx cuando son varias
            /*
                 Consultationtype > string > nombre metodo
                 Connectorid > string
                 Trackid > string > Trackid externa
                 Status > int > Retorno de servicio 
                 Result > int > 1-Verify Positivo | 2-Verify Negativo (Si es verify si es get 0)
                 Score > double > Score obtenido (Si es verify si es get 0)
                 Threshold > double > Umbral utilizado (Si es verify si es get 0)
                 Timestamp > string > fecha y hora tx externa
                Devuelve un Dynamicdata de la forma:
                    <DynamicData>
                        <DynamicDataItem>
                            <key>tx1</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                      .........  
                        <DynamicDataItem>
                            <key>txN</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                   </DynamicData>
             */
            DynamicData oExternalTxs = new DynamicData();
            oXmlout.AddValue("externaltxs", "");  //Tipo DynamicData

            //oXmlout.SetValue("message", "");
            //oXmlout.SetValue("trackid", "283734647");
            //oXmlout.SetValue("status", "0");
            //oXmlout.SetValue("result", "1");
            //oXmlout.SetValue("score", "1000");
            //oXmlout.SetValue("threshold", _threshold.ToString());
            //oXmlout.SetValue("timestamp", FormatFechaHoraSRCeI("23112013100000"));
            //xmloutput = DynamicData.SerializeToXml(oXmlout);
            //return Errors.IERR_OK;

            try
            {
                LOG.Debug("Bio.Core.Matcher.SRCeI.BioVerify2015 - Ingresando...");
                if (!this._isConfigured)
                {
                    msg = "Bio.Core.Matcher.SRCeI.BioVerify2015 Error = Connector No Configurado!";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
                }


                //1.-Deserializo parametros de entrada
                xmlinput = xmlinput.Replace("&#x0;", "");
                LOG.Debug("Bio.Core.Matcher.SRCeI.BioVerify2015 - xmlinput = " + xmlinput);
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlinput);
                if (oXmlIn == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.BioVerify2015 Error deserealizando xmlinput";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //Tomo el WSQ si es que viene
                LOG.Debug("Bio.Core.Matcher.SRCeI.BioVerify2015 - Extrayendo WSQ from xmlinput...");
                byte[] wsq = ExtraeWSQFromParamIn(oXmlIn);

                if (wsq != null)
                {
                    LOG.Debug("Bio.Core.Matcher.SRCeI.BioVerify2015 - (wsq != null!");

                    _currentThreshold = (oXmlIn.Threshold > 0) ? (int)oXmlIn.Threshold : _threshold;
                    //Trust all certificates
                    System.Net.ServicePointManager.ServerCertificateValidationCallback =
                        ((senderA, certificate, chain, sslPolicyErrors) => true);

                    // trust sender
                    System.Net.ServicePointManager.ServerCertificateValidationCallback
                                    = ((senderA, cert, chain, errors) =>
                                    {
                                        string lower = cert.Subject.ToLower();
                                        return (lower.Contains("terceros.sidiv.registrocivil.cl") || lower.Contains("servers ca") || lower.Contains("technical root ca"));
                                    });

                    // validate cert by calling a function
                    System.Net.ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateRemoteCertificate);

                    //var b = GetCustomHttpBinding();
                    
                    //EndpointIdentity identity = EndpointIdentity.CreateDnsIdentity("terceros.sign"); //("terceros.sidiv.registrocivil.cl")
                    //EndpointAddress endpoint = new EndpointAddress(new Uri(this._urlWS), identity);
                    //DS64PortTypeClient client = new DS64PortTypeClient(b, endpoint);
                    //LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Connector Consultando a = " + endpoint.Uri);
                    //client.ChannelFactory.Endpoint.Behaviors.Remove<System.ServiceModel.Description.ClientCredentials>();
                    //client.ChannelFactory.Endpoint.Behaviors.Add(new UsernameTokenCredentials());
                    DS64PortTypeClient client = new DS64PortTypeClient();
                    client.Endpoint.ListenUri = new Uri(this._urlWS);
                    client.ChannelFactory.Endpoint.Behaviors.Add(new RegistroCivilEndpointBehavior());

                    //var vs = client.Endpoint.Behaviors.Where((i) => i.GetType().Namespace.Contains("VisualStudio"));
                    //if (vs != null)
                    //    client.Endpoint.Behaviors.Remove((System.ServiceModel.Description.IEndpointBehavior)vs.Single());

                    client.ClientCredentials.UserName.UserName = this._userSRCeI;
                    client.ClientCredentials.UserName.Password = this._claveSRCeI;

                    string certificateKey = this._certifcatekey;
                    string privateKey = this._pvkey;

                    if (String.IsNullOrEmpty(certificateKey) == null || String.IsNullOrEmpty(privateKey))
                    {
                        msg = "Bio.Core.Matcher.SRCeI.BioVerify2015 Error Certificado o Key Nulos";
                        LOG.Fatal(msg);
                        oXmlout.SetValue("message", msg);
                        xmloutput = DynamicData.SerializeToXml(oXmlout);
                        return Errors.IERR_BAD_PARAMETER;
                    }
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Connector Certificado y key levantados");
                    Certificate virtualCertificate = new Certificate(certificateKey, privateKey, "");
                    X509Certificate2 localCertificate = virtualCertificate.GetCertificateFromPEMstring(false);
                    //client.ClientCredentials.ClientCertificate.Certificate = localCertificate;
                    client.ChannelFactory.Credentials.ClientCertificate.Certificate = localCertificate;

                    X509Certificate2 remoteCertificate = new X509Certificate2(this._certifySRCeI);
                    //client.ClientCredentials.ServiceCertificate.DefaultCertificate = remoteCertificate;
                    client.ChannelFactory.Credentials.ServiceCertificate.DefaultCertificate = remoteCertificate;
                    client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Connector client.ClientCredentials Configurado");
                    
                    Encabezado objEncabezado = SRCeIHelper.CrearEncabezado(this._idEmpresaSRCeI, this._userSRCeI, Utiles.GetIP());
                    RUN run = RUN.Convertir(oXmlIn.PersonalData.Valueid);
                    AFISImpresionDactilarParametro objParameter = SRCeIHelper.CrearParametros(run, oXmlIn.Bodypart, wsq);
                    if (objEncabezado == null || objParameter == null)
                    {
                        msg = "Bio.Core.Matcher.SRCeI.BioVerify2015.Connector - Error creando Encabezado y/o parametros";
                        LOG.Fatal(msg);
                        oXmlout.SetValue("message", msg);
                        xmloutput = DynamicData.SerializeToXml(oXmlout);
                        return Errors.IERR_BAD_PARAMETER;
                    }
                    else
                    {
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Connector Encabezado y Parametros creados");
                    }

                    AFISImpresionDactilarDS64Request request = new AFISImpresionDactilarDS64Request(objEncabezado, objParameter);
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Connector Creado request...");
                    //AFISImpresionDactilarDS64Response response = client.AFISImpresionDactilarDS64(request);
                    int Score;
                    RespuestaAFISType resp;

                    //Added 16-11-2017 - reintentos por intermitencia en servicio de SRCeI
                    int _contadorReintentos = 0;
                    bool _verificoOK = false;

                    EncabezadoRespuesta response = null;
                    Score = 0;
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Connector Conectando a AFISImpresionDactilarDS64 en URL = " + client.Endpoint.ListenUri);
                    while (_contadorReintentos < this._qReintentosVerify && !_verificoOK)
                    {
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Connector Nro Intento = " + _contadorReintentos + "...");
                        response = client.AFISImpresionDactilarDS64(objEncabezado, objParameter, out Score, out resp);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2015.Connector Recibido response intento " + _contadorReintentos + "...");
                        if (response != null && response.Estado == EstadoType.Item000 && Score >= _currentThreshold)
                        {
                            _verificoOK = true;
                        } 
                        _contadorReintentos++;
                    }

                    if (response != null) // && response..EncabezadoRespuesta != null)
                    {
                        /*
                            201 (Error técnico)
                            206 (Solicitud de formato invalido)    
                            207 (Error Seguridad)
                            301 (Datos inexistentes)
                            304 (Mala Calidad)
                        */

                        if (response.Estado == EstadoType.Item000)
                        {
                            oXmlout.SetValue("message", "");
                            oXmlout.SetValue("trackid", objEncabezado.IdTransaccion.ToString());
                            oXmlout.SetValue("status", ConvertToNumber(response.Estado.ToString()));
                            oXmlout.SetValue("result", Score >= _currentThreshold ? "1" : "2");
                            oXmlout.SetValue("score", Score.ToString());
                            oXmlout.SetValue("threshold", _currentThreshold.ToString());
                            oXmlout.SetValue("timestamp", response.FechaHoraOperacion);
                        }
                        else //!= Item000 => Error
                        {
                            oXmlout.SetValue("message", "Error indicado por SRCeI = " + response.Estado.ToString());
                            oXmlout.SetValue("trackid", objEncabezado.IdTransaccion.ToString());
                            oXmlout.SetValue("status", ConvertToNumber(response.Estado.ToString()));
                            oXmlout.SetValue("result", "2");
                            oXmlout.SetValue("score", "0");
                            oXmlout.SetValue("threshold", _currentThreshold.ToString());
                            oXmlout.SetValue("timestamp", response.FechaHoraOperacion);
                            //_ret = Errors.IERR_CONNECTOR_MALFORMED_ANSWER;
                        }
                        int res = (Score >= _currentThreshold) ? 1 : 2;
                        oExternalTxs.AddValue("txVerify", GetDDFormExternalTx("AFISImpresionDactilarDS64", response, res, _currentThreshold, Score));
                        oXmlout.SetValue("externaltxs", DynamicData.SerializeToXml(oExternalTxs));
                    }
                    else
                    {
                        msg = "Bio.Core.Matcher.SRCeI.BioVerify2015.Connector - Respuesta Erronea del SRCeI - " +
                            "Response=" + (response==null?"NULL":"Not NULL");
                        LOG.Fatal(msg);
                        oXmlout.SetValue("message", msg);
                        xmloutput = DynamicData.SerializeToXml(oXmlout);
                        return Errors.IERR_CONNECTOR_MALFORMED_ANSWER;
                    }
                }
                else
                {
                    msg = "Bio.Core.Matcher.SRCeI.BioVerify2015.Connector Falta huella para enviar";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
           
                xmloutput = DynamicData.SerializeToXml(oXmlout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.SRCeI.BioVerify2015.Verify Error [" + ex.Message + "]";
                LOG.Error("Bio.Core.Matcher.SRCeI.BioVerify2015.Verify Error", ex);
                oXmlout.SetValue("message", msg);
                xmloutput = DynamicData.SerializeToXml(oXmlout);
                return Errors.IERR_UNKNOWN;
            }

            return _ret;
        }

        private string ConvertToNumber(string status)
        {
            string ret = "0";
            try
            {
                ret = status.Substring(4);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.BioVerify2015.ConvertToNumber Error", ex);
            }
            return ret;
        }

        private string ReadFromHDD(string path)
        {
            string sret = null;
            try
            {
                System.IO.StreamReader sr = new StreamReader(path);
                sret = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.BioVerify2015.ReadFromHDD Error", ex);
                sret = null;
            }
            return sret;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */

            string msg;
            xmloutput = null;
            int iretremoto;

            //DynamicData oXmlout = new DynamicData();
            //oXmlout.AddValue("message", "");
            //oXmlout.AddValue("trackid", "");
            //oXmlout.AddValue("status", "");
            //oXmlout.AddValue("result", "");
            //oXmlout.AddValue("score", "");
            //oXmlout.AddValue("timestamp", "");
            //oXmlout.AddValue("personaldata", "");

            //try
            //{
            //    if (!this._isConfigured)
            //    {
            //        msg = "Bio.Core.Matcher.SRCeI.BioVerify2015.Identify Error = Connector No Configurado!";
            //        LOG.Fatal(msg);
            //        oXmlout.SetValue("message", msg);
            //        xmloutput = DynamicData.SerializeToXml(oXmlout);
            //        return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
            //    }

            //    using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
            //    {
            //        ws.Timeout = this._timeout;
            //        ws.Url = this._urlWS;

            //        iretremoto = ws.Identify(xmlinput, out xmloutbp);
            //        if (iretremoto == Errors.IERR_OK)
            //        {
            //            XmlParamOut oXmlParamOut = Utils.XmlUtils.DeserializeObject<XmlParamOut>(xmloutbp);
            //            oXmlout.SetValue("message", "");
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            oXmlout.SetValue("trackid", oXmlParamOut.Trackid);
            //            oXmlout.SetValue("result", oXmlParamOut.Result.ToString());
            //            oXmlout.SetValue("score", oXmlParamOut.Score.ToString());
            //            oXmlout.SetValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
            //            oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(oXmlParamOut.PersonalData));
            //        }
            //        else
            //        {
            //            msg = "Bio.Core.Matcher.SRCeI.BioVerify2015.Identify Error en WS Remoto [" + iretremoto.ToString() + "]";
            //            LOG.Fatal(msg);
            //            oXmlout.SetValue("message", msg);
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            xmloutput = DynamicData.SerializeToXml(oXmlout);
            //            return Errors.IERR_CONNECTOR_REMOTE_ERROR;
            //        }
            //    }

            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //}
            //catch (Exception ex)
            //{
            //    msg = "Bio.Core.Matcher.SRCeI.BioVerify2015.Identify Error [" + ex.Message + "]";
            //    LOG.Error("Bio.Core.Matcher.SRCeI.BioVerify2015.Identity Error", ex);
            //    oXmlout.SetValue("message", msg);
            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //    return Errors.IERR_UNKNOWN;
            //}

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de Recuperación. Ingresa información recuperar, 
        /// y se realiza la operacion.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Get(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */

            string msg;
            string xmloutbp;
            int iretremoto;

            DynamicData oXmlout = new DynamicData();
            oXmlout.AddValue("message", "");
            oXmlout.AddValue("trackid", "");
            oXmlout.AddValue("status", "");
            oXmlout.AddValue("result", "0");
            oXmlout.AddValue("score", "0");
            oXmlout.AddValue("threshold", "0");
            oXmlout.AddValue("timestamp", "");
            oXmlout.AddValue("personaldata", "");
                //Added 02-2015 para agregar todas las BpTxConx cuando son varias
            /*
                 Consultationtype > string > nombre metodo
                 Connectorid > string
                 Trackid > string > Trackid externa
                 Status > int > Retorno de servicio 
                 Result > int > 1-Verify Positivo | 2-Verify Negativo (Si es verify si es get 0)
                 Score > double > Score obtenido (Si es verify si es get 0)
                 Threshold > double > Umbral utilizado (Si es verify si es get 0)
                 Timestamp > string > fecha y hora tx externa
                Devuelve un Dynamicdata de la forma:
                    <DynamicData>
                        <DynamicDataItem>
                            <key>tx1</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                      .........  
                        <DynamicDataItem>
                            <key>txN</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                   </DynamicData>
             */
            DynamicData oExternalTxs = new DynamicData();
            oXmlout.AddValue("externaltxs", "");  //Tipo DynamicData


            //oXmlout.SetValue("message", "");
            //oXmlout.SetValue("trackid", "283734647");
            //oXmlout.SetValue("status", "0");
            //oXmlout.SetValue("result", "1");
            //oXmlout.SetValue("score", "1000");
            //oXmlout.SetValue("threshold", _threshold.ToString());
            //oXmlout.SetValue("timestamp", FormatFechaHoraSRCeI("23112013100000"));
            //xmloutput = DynamicData.SerializeToXml(oXmlout);
            //return Errors.IERR_OK;
            LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015 IN...");
            try
            {
                if (!this._isConfigured)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015 Error = Connector No Configurado!";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
                }
                else
                {
                    LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015 Connector Configurado!");
                }


                //1.-Deserializo parametros de entrada
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlinput);
                if (oXmlIn == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015 Error deserealizando xmlinput";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015 XmlParamin <> null");
                }

                if (oXmlIn.PersonalData == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015 Error PersonalData Nulo en xmlinput";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    try
                    {
                        LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015 tomando RUT...");
                        _currenRUN = RUN.ConvertirEx(oXmlIn.PersonalData.Valueid.Trim());
                        if (_currenRUN == null)
                        {
                            msg = "Bio.Core.Matcher.SRCeI.GetInfo2015 Error Parseando RUT PersonalData.ValueId (_currenRUN == null) en xmlinput";
                            LOG.Fatal(msg);
                            oXmlout.SetValue("message", msg);
                            xmloutput = DynamicData.SerializeToXml(oXmlout);
                            return Errors.IERR_BAD_PARAMETER;
                        }
                        else
                        {
                            LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015 RUT para consultar = " + _currenRUN.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Bio.Core.Matcher.SRCeI.GetInfo2015 Error Parseando RUT PersonalData.ValueId en xmlinput";
                        LOG.Fatal(msg);
                        oXmlout.SetValue("message", msg);
                        xmloutput = DynamicData.SerializeToXml(oXmlout);
                        return Errors.IERR_UNKNOWN;
                    }
                }


                //Trust all certificates
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((senderA, certificate, chain, sslPolicyErrors) => true);

                // trust sender
                System.Net.ServicePointManager.ServerCertificateValidationCallback
                                = ((senderA, cert, chain, errors) =>
                                {
                                    string lower = cert.Subject.ToLower();
                                    return (lower.Contains("terceros.sidiv.registrocivil.cl") || lower.Contains("servers ca") || lower.Contains("technical root ca"));
                                });

                // validate cert by calling a function
                System.Net.ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateRemoteCertificate);
                DS64PortTypeClient client = new DS64PortTypeClient();
                LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015 Configurando URL = " + this._urlWS);
                client.Endpoint.ListenUri = new Uri(this._urlWS);
                client.ChannelFactory.Endpoint.Behaviors.Add(new RegistroCivilEndpointBehavior());
                //var vs = client.Endpoint.Behaviors.Where((i) => i.GetType().Namespace.Contains("VisualStudio"));
                //if (vs != null)
                //    client.Endpoint.Behaviors.Remove((System.ServiceModel.Description.IEndpointBehavior)vs.Single());

                client.ClientCredentials.UserName.UserName = this._userSRCeI;
                client.ClientCredentials.UserName.Password = this._claveSRCeI;
                LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015 User ingresados = " + this._userSRCeI);

                string certificateKey = this._certifcatekey;
                string privateKey = this._pvkey;

                if (String.IsNullOrEmpty(certificateKey) == null || String.IsNullOrEmpty(privateKey))
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015 Error Certificado o Key Nulos";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Configurando certificados...");

                Certificate virtualCertificate = new Certificate(certificateKey, privateKey, "");
                X509Certificate2 localCertificate = virtualCertificate.GetCertificateFromPEMstring(false);
                client.ChannelFactory.Credentials.ClientCertificate.Certificate = localCertificate;
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Certificado y key levantados! [Certificado=" + localCertificate.FriendlyName + "]");

                X509Certificate2 remoteCertificate = new X509Certificate2(this._certifySRCeI);
                client.ChannelFactory.Credentials.ServiceCertificate.DefaultCertificate = remoteCertificate;
                client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector client.ClientCredentials Configurado! [Remote Certificado=" + remoteCertificate .FriendlyName + "]");

                Encabezado objEncabezado = SRCeIHelper.CrearEncabezado(this._idEmpresaSRCeI, this._userSRCeI, Utiles.GetIP());
                DatosDocumento objDocRet = new DatosDocumento();
                DocumentoParametro objDocParam = new DocumentoParametro();
                objDocParam.RUN = _currenRUN.Mantisa;
                objDocParam.Tipo = TipoDocumentoType.C;
                Datos dataFoto = null;
                Datos dataFirma = null;
                RunParametro runparam = new RunParametro();
                runparam.RUN = _currenRUN.Mantisa;
                EncabezadoRespuesta responseDatos = null;
                EncabezadoRespuesta responseFoto = null;
                EncabezadoRespuesta responseFirma = null;

                if (objEncabezado == null || objDocParam == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015.Connector - Error creando Encabezado y/o parametros";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Encabezado y Parametros creados");
                }

                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Ingresando a consultas..._typeget = " + _typeget);
                //0 - Todo 
                //1 - Solo Info
                //2 - Solo Foto
                //3 - Solo Firma
                //4 - Info + Foto
                //5 - Info + Firma
                //6 - Foto + Firma
                if (_getInfo)
                { //Tomo info
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Ingresando a consulta Datos en URL = " + client.Endpoint.ListenUri);
                    responseDatos = client.ConsultaDocumentoDS64(objEncabezado, objDocParam, out objDocRet);
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Salio consulta Datos!");
                }

                if (_getFoto)
                { //Tomo Foto
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Ingresando a consulta Foto en URL = " + client.Endpoint.ListenUri);
                    responseFoto = client.ConsultaFotografiaDS64(objEncabezado, runparam, out dataFoto);
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Salio consulta Foto!");
                }

                if (_getFirma)
                { //Tomo Firma
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Ingresando a consulta Firma en URL = " + client.Endpoint.ListenUri);
                    responseFirma = client.ConsultaFirmaDS64(objEncabezado, runparam, out dataFirma);
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connector Salio consulta Firma!");
                }

                if (responseDatos == null && responseFoto == null && responseFirma == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015.Connector - Respuesta Erronea del SRCeI - " +
                      "ResponseDatos=" + (responseDatos == null ? "NULL" : "Not NULL") + " | " +
                      "ResponseFoto=" + (responseFoto == null ? "NULL" : "Not NULL") + " | " +
                      "ResponseFirma=" + (responseFirma == null ? "NULL" : "Not NULL");
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_MALFORMED_ANSWER;
                }

                if (_getInfo && responseDatos != null && responseDatos.Estado != EstadoType.Item000)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015.Connector - Respuesta Erronea del SRCeI - " +
                      "ResponseDatos.Estado=" + responseDatos.Estado.ToString();
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    oXmlout.SetValue("status", ConvertToNumber(responseDatos.Estado.ToString()));
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                }

                if (_getFoto && responseFoto != null && responseFoto.Estado != EstadoType.Item000)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015.Connector - Respuesta Erronea del SRCeI - " +
                      "ResponseFoto.Estado=" + responseFoto.Estado.ToString();
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    oXmlout.SetValue("status", ConvertToNumber(responseFoto.Estado.ToString()));
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                }

                if (_getFirma && responseFirma != null && responseFirma.Estado != EstadoType.Item000)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2015.Connector - Respuesta Erronea del SRCeI - " +
                      "ResponseFirma.Estado=" + responseFirma.Estado.ToString();
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    oXmlout.SetValue("status", ConvertToNumber(responseFirma.Estado.ToString()));
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                }


                //else
                //{
                /*
                    201 (Error técnico)
                    206 (Solicitud de formato invalido)    
                    207 (Error Seguridad)
                    301 (Datos inexistentes)
                */
                string _satusConcat = "0"; //Concatena estados de todas las consultas
                string _msgerrConcat = ""; //Concatena msg errores de todas las consultas
                PersonalData pdataOut = new PersonalData();
                string _fechaOp = "";
                pdataOut.Typeid = "RUT";
                pdataOut.Valueid = _currenRUN.Mantisa + "-" + _currenRUN.Dv;
                pdataOut.Verificationsource = "SRCeI";
                if (_getInfo)
                { //Proceso info
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getInfo IN...");
                    if (responseDatos != null)
                    {
                        if (responseDatos.Estado == EstadoType.Item000)
                        {
                            LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getInfo armando retorno pdataOut...");
                            //_satusConcat = "0"; //"EstadoDatos:" + responseDatos.Estado;
                            oExternalTxs.AddValue("txDatos", GetDDFormExternalTx("ConsultaDocumentoDS64", responseDatos,0,0,0));
                            _fechaOp = responseDatos.FechaHoraOperacion;
                            LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getInfo responseDatos.FechaHoraOperacion = " + _fechaOp);
                            if (objDocRet != null)
                            {
                                pdataOut.Name = objDocRet.Nombres;
                                pdataOut.Patherlastname = objDocRet.PrimerApellido;
                                pdataOut.Motherlastname = objDocRet.SegundoApellido;
                                pdataOut.Sex = objDocRet.Sexo.ToString();
                                pdataOut.Documentseriesnumber = objDocRet.Serie;
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getInfo objDocRet.FechaVencimiento = " + objDocRet.FechaVencimiento);
                                pdataOut.Documentexpirationdate = ParseFecha(objDocRet.FechaVencimiento);
                                pdataOut.Visatype = objDocRet.TipoVisa;
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getInfo objDocRet.FechaNacimiento = " + objDocRet.FechaNacimiento);
                                pdataOut.Birthdate = ParseFecha(objDocRet.FechaNacimiento);
                                pdataOut.Birthplace = objDocRet.LugarNacimiento;
                                pdataOut.Nationality = objDocRet.PaisNacionalidad;
                                pdataOut.Profession = objDocRet.Profesion;
                            }
                            else
                            {
                                _msgerrConcat = "Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error [objDocRet=NULL]";
                                LOG.Error(_msgerrConcat);
                            }
                        }
                        else
                        {
                            _satusConcat = ConvertToNumber(responseDatos.Estado.ToString());
                            oExternalTxs.AddValue("txDatos", GetDDFormExternalTx("ConsultaDocumentoDS64", responseDatos,0,0,0));
                        }
                    }
                    else
                    {
                        _satusConcat = "-1";
                    }
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getInfo OUT!");
                }

                if (_getFoto)
                { //Proceso Foto
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getFoto IN...");
                    if (responseFoto != null)
                    {
                        if (responseFoto.Estado == EstadoType.Item000)
                        {
                            oExternalTxs.AddValue("txFoto", GetDDFormExternalTx("ConsultaFotografiaDS64", responseDatos,0,0,0));
                            _fechaOp = responseDatos.FechaHoraOperacion;
                            if (dataFoto != null)
                            {
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getFoto GeneraImagenB64FromBytes IN...");
                                pdataOut.Photography = GeneraImagenB64FromBytes(1, dataFoto.Imagen, dataFoto.Formato);
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getFoto GeneraImagenB64FromBytes OUT!");
                            }
                            else
                            {
                                _msgerrConcat = (String.IsNullOrEmpty(_msgerrConcat)) ?
                                    "Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error [dataFoto=NULL]" :
                                    _msgerrConcat + "|Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error [dataFoto=NULL]";
                                LOG.Error(_msgerrConcat);
                            }
                        }
                        else
                        {
                            _satusConcat = ConvertToNumber(responseFoto.Estado.ToString());
                            oExternalTxs.AddValue("txFoto", GetDDFormExternalTx("ConsultaFotografiaDS64", responseDatos,0,0,0));
                        }
                    }
                    else
                    {
                        _satusConcat = "-1"; 
                    }
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getFoto OUT!");
                }

                if (_getFirma)
                { //Proceso Firma
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getFirma IN...");
                    if (responseFirma != null)
                    {
                        if (responseFirma.Estado == EstadoType.Item000)
                        {
                            oExternalTxs.AddValue("txFirma", GetDDFormExternalTx("ConsultaFirmaDS64", responseDatos,0,0,0));
                            _fechaOp = responseDatos.FechaHoraOperacion;
                            if (dataFoto != null)
                            {
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getFirma GeneraImagenB64FromBytes IN...");
                                pdataOut.Signatureimage = GeneraImagenB64FromBytes(2, dataFirma.Imagen, dataFirma.Formato);
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getFirma GeneraImagenB64FromBytes OUT!");
                            }
                            else
                            {
                                _msgerrConcat = (String.IsNullOrEmpty(_msgerrConcat)) ?
                                    "Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error [dataFoto=NULL]" :
                                    _msgerrConcat + "|Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error [dataFirma=NULL]";
                                LOG.Error(_msgerrConcat);
                            }
                        }
                        else
                        {
                            _satusConcat = ConvertToNumber(responseFirma.Estado.ToString());
                            oExternalTxs.AddValue("txFoto", GetDDFormExternalTx("ConsultaFirmaDS64", responseDatos, 0,0,0));
                        }
                    }
                    else
                    {
                        _satusConcat = "-1";
                    }
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2015.Connecto _getFirma OUT!");
                }

                oXmlout.SetValue("message", _msgerrConcat);
                oXmlout.SetValue("status", _satusConcat);
                oXmlout.SetValue("trackid", objEncabezado.IdTransaccion.ToString());
                oXmlout.SetValue("timestamp", (!String.IsNullOrEmpty(_fechaOp) ? _fechaOp : DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(pdataOut));
                oXmlout.SetValue("externaltxs", DynamicData.SerializeToXml(oExternalTxs));
                //}
                xmloutput = DynamicData.SerializeToXml(oXmlout);
                
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error [" + ex.Message + "]";
                LOG.Error("Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error", ex);
                oXmlout.SetValue("message", msg);
                xmloutput = DynamicData.SerializeToXml(oXmlout);
                return Errors.IERR_UNKNOWN;
            }
            LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015.Get OUT! => xmloutput = " + xmloutput);
            return Errors.IERR_OK;
        }

        private DateTime ParseFecha(string _date)
        {
            DateTime ret = new DateTime();
            LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015.ParseFecha IN...");
            try
            {
                LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015.ParseFecha => Parseando = " + _date);
                ret = DateTime.ParseExact(_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015.ParseFecha => Fecha Parseada = " + ret.ToString("dd/MM/yyyy"));
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.GetInfo2015.ParseFecha Error", ex);
            }
            LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015.ParseFecha OUT!");
            return ret;
        }

        private string GetDDFormExternalTx(string CT, EncabezadoRespuesta response,
                                           int result, int th, double score)
        {
            string _ret = "";
            try
            {
                if (response == null) return "";

                DynamicData ddTx = new DynamicData();
                ddTx.AddValue("Consultationtype", CT);
                ddTx.AddValue("Connectorid", "SRCeI.BioVerify2015");
                ddTx.AddValue("Trackid", response.IdTransaccion.ToString());
                ddTx.AddValue("Status", ConvertToNumber(response.Estado.ToString()));
                ddTx.AddValue("Result", result.ToString());
                ddTx.AddValue("Score", score.ToString());
                ddTx.AddValue("Threshold", th.ToString());
                ddTx.AddValue("Timestamp", response.FechaHoraOperacion.ToString());
                _ret = DynamicData.SerializeToXml(ddTx);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.Bioverify2015 Error", ex);
                _ret = "";
            }
            return _ret;
        }

        /// <summary>
        /// Dado un arreglo de byte[] copn una imagen que viene del SRCeI, 
        /// Lo transforma a 
        /// </summary>
        /// <param name="p"></param>
        /// <param name="nullable"></param>
        /// <returns></returns>
        private string GeneraImagenB64FromBytes(int imageType, byte[] byImage, FormatoType? formato)
        {
            string _strret = null;
            try
            {
                LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015.Get GeneraImagenB64FromBytes IN...");
                FIBITMAP dib = new FIBITMAP();
                FreeImage.UnloadEx(ref dib);
                IntPtr imgPtr = Marshal.AllocHGlobal(byImage.Length);
                Marshal.Copy(byImage, 0, imgPtr, byImage.Length);
                FIMEMORY fiMStream = FreeImage.OpenMemory(imgPtr, (uint)byImage.Length);
                // get the file type
                FREE_IMAGE_FORMAT fiFormat = FreeImage.GetFileTypeFromMemory(fiMStream, 0);
                // load an image from the memory stream
                // mFileHandle is a static uint property of the class
                dib = FreeImage.LoadFromMemory(fiFormat, fiMStream, 0);
                //Bitmap bFoto = FreeImage.GetBitmap(dib);
                FIBITMAP dib2;
                if (imageType == 1) //es Foto
                {
                    if (_wFoto > 0) dib2 = FreeImage.Rescale(dib, _wFoto, _hFoto, FREE_IMAGE_FILTER.FILTER_BICUBIC);
                    else dib2 = dib;
                }
                else
                { //Es Firma
                    if (_wFirma > 0) dib2 = FreeImage.Rescale(dib, _wFirma, _hFirma, FREE_IMAGE_FILTER.FILTER_BICUBIC);
                    else dib2 = dib;
                }
                byte[] buff = new byte[FreeImage.GetWidth(dib2) * FreeImage.GetHeight(dib2)];
                MemoryStream ms = new MemoryStream(buff);
                FreeImage.SaveToStream(dib2, ms, FREE_IMAGE_FORMAT.FIF_JPEG);
                _strret = Convert.ToBase64String(buff);
                ms.Close();
                Marshal.FreeHGlobal(imgPtr);

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.GetInfo2015.Get Error", ex);
                _strret = null;
            }
            LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo2015.Get GeneraImagenB64FromBytes OUT!");
            return _strret;
        }

#endregion Implementation of IConnector

        private static string FormatFechaHoraSRCeI(string fechaoperacion)
        {
            string strRet = "";
            try
            {
                strRet = fechaoperacion.Substring(6, 2) + "/" +
                         fechaoperacion.Substring(4, 2) + "/" +
                         fechaoperacion.Substring(0, 4) + " " +
                         fechaoperacion.Substring(8, 2) + ":" +
                         fechaoperacion.Substring(10, 2) + ":" +
                         fechaoperacion.Substring(12, 2);
            }
            catch
            {
                strRet = "";
            }
            return strRet;
        }

        public void Dispose()
        {
            
        }
    }
}
