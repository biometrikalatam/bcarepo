using System;
using System.Globalization;

namespace Bio.Core.Matcher.Connector.SRCeI.BioVerify2015
{
	/// <summary>
	/// Clase para manejar Rol Unico Nacional Chileno
	/// Esto permite usarlo como un objeto de datos concreto lo que permite
	/// realizar operaciones en forma mas natural con los ruts
	/// </summary>
	/// <disclaimer>
	/// (c) 2004 Biokey Identity Technologies
	/// Este codigo es parte de Biokey Foundations y esta prohibida su copia
	/// y uso sin autorizacion expresa de Biokey
	/// </disclaimer>
	[Serializable]
	public class RUN
	{
		
		#region constructores
		/// <summary>
		/// RUT Vacio, se puede llenar despues usando las propiedades
		/// Mantisa y DV
		/// </summary>
		public RUN()
		{
			
		}
		/// <summary>
		/// Crea un RUT con mantisa y digito verificador 
		/// No se puede crear un rut vac
		/// </summary>
		/// <param name="mantisa">Mantisa del RUT (la parte num�rica)</param>
		/// <param name="dv">D�gito Verificador</param>
		public RUN(int mantisa, char dv)
		{
			this.mantisa = mantisa;
			this.dv = dv;
		}

		/// <summary>
		/// Factory method para crear un RUN
		/// Puede que sea eliminado
		/// </summary>
		/// <param name="mantisa">mantisa del RUN</param>
		/// <param name="dv">digito verificador</param>
		/// <returns></returns>
		public static RUN Crear(int mantisa, char dv)
		{
			return new RUN(mantisa, dv);
		}

		/// <summary>
		/// Crea un RUN pero en base al string, intentando parsearlo
		/// </summary>
		/// <param name="run">el run como un string</param>
		/// <returns>objeto RUN creado a partir del parsing del string dado</returns>
		public static RUN Crear(string run)
		{
			return Convertir(run);
			
		}
		#endregion

		#region operadores

		/// <summary>
		/// Comparador, retorna true si obj es un RUN y tiene la misma
		/// mantisa y el mismo DV
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if (obj is RUN)
			{
				RUN run2 = (RUN) obj;
				return dv == run2.dv && mantisa == run2.mantisa;
			}
			return false;
		}

		/// <summary>
		/// Compara 2 RUN y retorna true si son iguales 
		/// </summary>
		/// <param name="run1"></param>
		/// <param name="run2"></param>
		/// <returns>true si son iguales</returns>
		/// 
		public static bool operator ==(RUN run1, RUN run2)
		{
			if (Object.ReferenceEquals(run1, run2))
				return true;
			if (Object.ReferenceEquals(run1, null))
				return false;
			if (Object.ReferenceEquals(run2, null))
				return false;
				
			return run1.Equals(run2);
		}
			
		/// <summary>
		/// Compara 2 RUN y retorna true si son distintos
		/// </summary>
		/// <param name="run1"></param>
		/// <param name="run2"></param>
		/// <returns>true si son distintos</returns>
		public static bool operator !=(RUN run1, RUN run2)
		{
			if (Object.ReferenceEquals(run1, run2))
				return false;
			if (Object.ReferenceEquals(run1, null))
				return true;
			if (Object.ReferenceEquals(run2, null))
				return true;
			return !run1.Equals(run2);
		}

		public static bool operator < (RUN run1, RUN run2)
		{
			return run1.mantisa < run2.mantisa;
		}

		public static bool operator <= (RUN run1, RUN run2)
		{
			return run1.mantisa <= run2.mantisa;
		}

		public static bool operator > (RUN run1, RUN run2)
		{
			return run1.mantisa > run2.mantisa;
		}

		public static bool operator >= (RUN run1, RUN run2)
		{
			return run1.mantisa >= run2.mantisa;
		}

		#endregion

		#region metodos de formateo a string
		public override string ToString()
		{
			return Formatear(mantisa, dv, SEP, GUION, 0);
		}

		public string ToString(bool usarGuion)
		{
			return Formatear(mantisa, dv, (char) 0, (char) (usarGuion ? GUION : 0), 0);
		}

		public string ToString(bool usarGuion, int ancho)
		{
			return Formatear(mantisa, dv, (char) 0, (char) (usarGuion ? GUION : 0), ancho);
		}
		

		/// <summary>
		/// Retorna un string formateado
		/// </summary>
		/// <param name="mantisa">mantisa del run</param>
		/// <param name="dv">digito verificador</param>
		/// <param name="sep">caracter separador de miles, si sep == 0 no va el separador de miles</param>
		/// <param name="guion">caracter usado como guion, si guion == 0 no va el guion</param>
		/// <param name="ancho">ancho que tendr� el campo, se rellena con ceros a la izquierda</param>
		/// <returns></returns>
		public static string Formatear(int mantisa, char dv, char sep, char guion, int ancho)
		{
			NumberFormatInfo nf = new NumberFormatInfo();
			nf.NumberDecimalDigits = 0;
			nf.NumberGroupSeparator = sep == 0 ? "" : sep.ToString();
			string s = mantisa.ToString("n",nf)+(guion == 0 ? String.Empty : guion.ToString())+dv.ToString();
			return s.PadLeft(ancho, '0');
		}

		
		#endregion

		#region Propiedades

		/// <summary>
		/// Devuelve el digito verificador del RUN
		/// </summary>
		/// <remarks>Debe asignarse despu�s de la mantisa, de lo contrario
		/// se producir� una excepci�n.
		/// Adem�s valida que este valor corresponda a la mantisa
		/// </remarks>
		public char Dv
		{
			get { return dv; }
			set
			{
				dv = value; 
				if (!Validar(mantisa, dv))
				{
					throw new ErrorRUN("digito veririficador: "+dv+" no corresponde para la mantisa: "+mantisa);
				}
			}
		}


		/// <summary>
		/// Asigna la mantisa del run.
		/// Calcula autom�ticamente el D�gito Verificador <see cref="Dv"/>
		/// </summary>
		/// <remarks>Esto permite que no sea necesario asignar el digito verificador</remarks>
		public int Mantisa
		{
			get { return mantisa; }
			set 
			{ 
				mantisa = value; 
				dv = CalcularDv(mantisa);
			}		
		}


		#endregion

		public override int GetHashCode()
		{
			return mantisa;
		}


		#region metodos estaticos
	
		/// <summary>
		/// Retorna true si dv corresponde la mantisa 
		/// </summary>
		/// <param name="mantisa">expresada como numero entero</param>
		/// <param name="dv">digito verificado a chequear</param>
		/// <returns>true si dv es igual al digitio verificador que corresponde a la mantisa dada</returns>
		public static bool Validar(int mantisa, char dv)
		{
			
			return CalcularDv(mantisa) == dv;
		}


		/// <summary>
		/// Convierte un string en un RUT, si la conversion no es posible
		/// retorna null.
		/// Si se quiere controlar un poco mejor la conversion se recomienda
		/// la funcion ConvertirEx, que arroja una excepcion
		/// </summary>
		/// <param name="str">El string con el rut que se quiere convertir,
		/// este puede venir en formato 99999999-X, con separadores (definidos
		/// en SEP <see cref="SEP"/>) y guion definidos en GUION <see cref="GUION"/>, o todo junto 99999999X, donde X es el digito verificador
		/// </param>
		/// <returns>el RUN parseado, o null si falla el parsing</returns>
		public static RUN Convertir(string str)
		{
			RUN result = null;
			try
			{
				result = ConvertirEx(str);
			}
			catch
			{
				result = null;
			}
			return result;
		}


		/// <summary>
		/// Es similar a Convertir, pero aqui se pasan expl�citamente los 
		/// caracteres que se quieren usar como separador de miles y guion.
		/// </summary>
		/// <param name="str">string con el run</param>
		/// <param name="sep">caracter separador de miles</param>
		/// <param name="guion">caracter guion usado</param>
		/// <returns>El RUN parseado, o null si no puede parsearlo</returns>
		public static RUN Convertir(string str, char sep, char guion)
		{
			RUN result = null;
			try
			{
				result = ConvertirEx(str, sep, guion);
			}
			catch
			{
				result = null;
			}
			return result;
		}

		/// <summary>
		/// Convierte un string en un RUN, usando SEP y GUION como separadores de
		/// miles y guion.
		/// </summary>
		/// <param name="str">El string con el rut que se quiere convertir,
		/// este puede venir en formato 99999999-X, con separadores (definidos
		/// en SEP <see cref="SEP"/>) y guion definidos en GUION <see cref="GUION"/>, o todo junto 99999999X, donde X es el digito verificador
		/// </param>
		/// <returns>El RUN parseado, si hay problemas se genera una excepci�n</returns>
		/// <exception cref="ErrorRUN" >Si hay problemas en el formato se genera esta excepci�n</exception>
		public static RUN ConvertirEx(string str)
		{
			return ConvertirEx(str, SEP, GUION);
		}

		/// <summary>
		/// Convierte un string en run, usando los caracteres dados como separador de miles
		/// y gui�n. 
		/// </summary>
		/// <param name="str">El string con el rut que se quiere convertir,
		/// este puede venir en formato 99999999-X, con separadores (definidos
		/// en sep y guion definidos en guion, o todo junto 99999999X, donde X es el digito verificador
		/// </param>
		/// <param name="sep">caracter separador de miles</param>
		/// <param name="guion">caracter usado como guion</param>
		/// <returns></returns>
		public static RUN ConvertirEx(string str, char sep, char guion)
		{
			if (str == null || str.Length == 0)
			{
			    return null;
				//throw new ErrorRUN("El string no puede ser nulo");
			}

			int mantisa = 0;
			char dv;

			str = str.Trim();
			char[] chars = str.ToCharArray();
			int len = chars.Length;
			int i;
			
			for (i = 0; i < len-1; i++)
			{
				char c = chars[i];
				if (char.IsDigit(c))
				{
					mantisa = mantisa*10 + (c - '0');
				}
				else if (c == guion)
				{
					break;
				}
				else if (c != sep)
				{
					throw new ErrorRUN("Caracter inv�lido: {0} en posici�n {1}", c, i);
				}
			}

			// Aqui se cumple que i es = len-1 o chars[i] == guion 
			dv = (chars[i] == guion) ? chars[i+1] : chars[i];

			if (!Validar(mantisa, dv))
			{
				throw new ErrorRUN("El string: {0} no es un RUN v�lido", str);
			}

			return new RUN(mantisa, dv);
		}
		#endregion

		#region Calculo del Digito Verificador
		
		/// <summary>
		/// Calcula el digito verificador para una mantisa dada
		/// </summary>
		/// <param name="mantisa">mantisa del RUT como un numero entero</param>
		/// <returns>el digito verificador correspondiente</returns>
		public static char CalcularDv(int mantisa)
		{
			int mult = 2;
			int mant = mantisa;
			int sum = 0;
			while (mant > 0)
			{
				sum += (mant % 10)*mult;
				mant /= 10;
				mult = mult == 7 ? 2 : mult+1;
			}
			return digitos[11 - (sum % 11)];
		}

		static char[] digitos = {'0','1','2','3','4','5','6','7','8','9','K','0'};
		
		#endregion

		#region variables privadas

		private int mantisa;
		private char dv;

		#endregion

		#region variables estaticas

		private static Char sEP = '.';
		/// <summary>
		/// Caracter separador por defecto
		/// </summary>
		public static Char SEP
		{
			get
			{
				return sEP;
			}
		}


		private static Char gUION = '-';
		/// <summary>
		/// guion por defecto
		/// </summary>
		public static Char GUION
		{
			get
			{
				return gUION;
			}
		}


		#endregion

	}

	/// <summary>
	/// Excepci�n generada por diversos m�todos del RUN cuando algo falla.
	/// </summary>
	public class ErrorRUN : Exception
	{
		public ErrorRUN(string msg) : base(msg)
		{
			
		}
		public ErrorRUN(string format, params object[] args) : base(String.Format(format, args))
		{	
		}
	}

}
