﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bio.Core.Matcher.Connector.SRCeI.BioVerify2015
{
    public enum PemStringType
    {
        Certificate,
        RsaPrivateKey
    }
}
