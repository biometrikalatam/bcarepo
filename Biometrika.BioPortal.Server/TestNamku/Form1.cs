﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestNamku
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string queryString = "?src=FEM&TypeId=" + txtTypeId.Text +
                                 "&ValueId=" + txtValueId.Text +
                                 "&Mail=" + txtMail.Text +
                                 "&DocumentId=" + txtDocumentId.Text +
                                 "&MsgToRead=" + txtMsgToRead.Text;

            string absoluteURL = "http://localhost:9191";  //ConfigurationManager.AppSettings["AbsoluteURL"];
            if (!absoluteURL.EndsWith("/"))
                absoluteURL = absoluteURL + "/";

            absoluteURL = absoluteURL + queryString;

            //txtResponse.Text = txtResponse.Text + absoluteURL + Environment.NewLine;

            WebRequest request = WebRequest.Create(absoluteURL);
            //request.Headers.Add("Authorization", "Bearer c86fab79169a4f23f45dfbefe27578c3");
            request.Timeout = 13000000;

            string json;
            using (WebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                    //fEMResponse = JsonConvert.DeserializeObject<FEMResponse>(json);
                }
            }
        }
    }
}
