﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biometrika.Regula.WebAPI.Api.Test
{
    public partial class Form1 : Form
    {

        private Capture _CAPTURECAM;
        //RegulaHandler _REGULA_HANDLER = new RegulaHandler();

        Biometrika.Regula.WebAPI.Api._2021.RegulaHandler _REGULA_HANDLER_v2021 = new _2021.RegulaHandler();
        FoxId.Api.ApiFunctions _FOXID_API = new FoxId.Api.ApiFunctions();

        Biometrika.Tech5.Api.ABISTech5Client _T5_CLIENT = new Tech5.Api.ABISTech5Client();

        string _STR_IMAGE1;
        string _STR_IMAGE2;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //_CAPTURECAM = new Capture();
            //_CAPTURECAM.SelectCamera(0);
            //_REGULA_HANDLER.Init("https://api.regulaforensics.com/webapi/", "testuser", "Regul@SdkTest");

            //_REGULA_HANDLER_v2021.Init("https://api.regulaforensics.com/",
            //    @"D:\Biometrika\BioPartners\Regula\SW\2021\OL79677\regula.license");
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                _CAPTURECAM = new Capture(Convert.ToInt32(txtCamara.Text.Trim())); //(HelperCamera.SelectCamera(_DRCONFIG.CAMCameraDocSelected));
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 1280);
                _CAPTURECAM.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 720);
                Application.Idle += ShowCamStream;
            }
            catch (Exception ex)
            {
                //LOG.Error("DRUI.labCamStart1_Click Error: " + ex.Message);
            }
        }

        private void labCamStop_Click(object sender, EventArgs e)
        {
            StopCamStream();
        }

        private void ShowCamStream(object sender, EventArgs e)
        {
            try
            {
                //paperDocument.DrawRectangle(pencil, 10, 10, imageCam.Im.Width - 10, imageCam.Height - 10);
                //paperDocument.DrawRectangle(pencil, 10, 10, 430, 250);
                Mat mat = new Mat();
                _CAPTURECAM.Retrieve(mat);
                Image<Bgr, Byte> img = mat.ToImage<Bgr, Byte>();
                //Image<Bgr, Byte> frame = img.Resize(1024, 768, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR).Copy();
                imageCam.Image = img.ToBitmap();
            }
            catch (Exception ex)
            {
                //LOG.Error("DRUI.ShowCamStream Error = " + ex.Message, ex);
            }
        }

        private void StopCamStream()
        {
            //LOG.Info("DRUI.StopCamStream IN...");
            try
            {
                Application.Idle -= ShowCamStream;
                _CAPTURECAM.Stop();
                _CAPTURECAM.Dispose();
            }
            catch (Exception ex)
            {
                //LOG.Error("DRUI.StopCamStream Error = " + ex.Message, ex);
            }
            //LOG.Info("DRUI.StopCamStream OUT!");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            StopCamStream();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            picImage1.Image = imageCam.Image;
            _STR_IMAGE1 = GetB64FromPictureBox(picImage1);
        }

        private void btnCapture2_Click(object sender, EventArgs e)
        {
            picImage2.Image = imageCam.Image;
            _STR_IMAGE2 = GetB64FromPictureBox(picImage2);
        }

        private string GetB64FromPictureBox(PictureBox pic)
        {
            return ImageToBase64(pic.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        public string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            string base64String = null;
            //LOG.Debug("DRUI.ImageToBase64 IN...");
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    image.Save(ms, format);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    base64String = Convert.ToBase64String(imageBytes);
                    //LOG.Debug("CIUIAdapter.ImageToBase64 base64String = " + base64String);
                }
            }
            catch (Exception ex)
            {
                //LOG.Error("CIUIAdapter.ImageToBase64 Error = " + ex.Message, ex);
                base64String = null;
            }
            //LOG.Debug("CIUIAdapter.ImageToBase64 OUT!");
            return base64String;
        }

        private void btnProcesar_Click(object sender, EventArgs e)
        {
            //Document _Document;
            //string msgerr;
            //try
            //{
                
            //    int ret = _REGULA_HANDLER.RecognizeDocument(_STR_IMAGE1, _STR_IMAGE2, 508, out _Document, out msgerr);
            //    if (ret == 0)
            //    {
            //        if (_Document != null)
            //        {
            //            if (_Document._DocInfo != null)
            //            {
            //                richTextBox1.Text = "Tipo Documento = " + _Document._DocInfo.ft_Document_Class_Code + Environment.NewLine;

            //                richTextBox1.Text += "Nro Documento = " + _Document._DocInfo.ft_Document_Number + Environment.NewLine;
            //                richTextBox1.Text += "Nombre = " + _Document._DocInfo.ft_Given_Names + Environment.NewLine;
            //                richTextBox1.Text += "Fecha Nacimiento = " + _Document._DocInfo.ft_Date_of_Birth + 
            //                                     " [Edad=" + _Document._DocInfo.ft_Age + "]" + Environment.NewLine;
            //                richTextBox1.Text += "Fecha Expiracion = " + _Document._DocInfo.ft_Date_of_Expiry + Environment.NewLine;
            //                richTextBox1.Text += "Sexo = " + _Document._DocInfo.ft_Sex + Environment.NewLine;
            //                richTextBox1.Text += "Nacionalidad = " + _Document._DocInfo.ft_Nationality + Environment.NewLine;
            //                richTextBox1.Text += "Profesion = " + _Document._DocInfo.ft_Profesion;
            //            }

            //            if (_Document._DocImages != null)
            //            {
            //                for (int i = 0; i < _Document._DocImages.Count; i++)
            //                {
            //                    switch (i)
            //                    {
            //                        case 0:
            //                            pic1.Image = GetImage(_Document._DocImages[i].b64image);
            //                            break;
            //                        case 1:
            //                            pic2.Image = GetImage(_Document._DocImages[i].b64image);
            //                            break;
            //                        case 2:
            //                            pic3.Image = GetImage(_Document._DocImages[i].b64image);
            //                            break;
            //                        case 3:
            //                            pic4.Image = GetImage(_Document._DocImages[i].b64image);
            //                            break;
            //                        case 4:
            //                            pic5.Image = GetImage(_Document._DocImages[i].b64image);
            //                            break;
            //                        case 5:
            //                            pic6.Image = GetImage(_Document._DocImages[i].b64image);
            //                            break;
            //                        case 6:
            //                            pic7.Image = GetImage(_Document._DocImages[i].b64image);
            //                            break;
            //                        case 7:
            //                            pic8.Image = GetImage(_Document._DocImages[i].b64image);
            //                            break;
            //                        case 8:
            //                            pic9.Image = GetImage(_Document._DocImages[i].b64image);
            //                            break;
            //                        //case 9:
            //                        //    pic2.Image = GetImage(_Document._DocImages[i].b64image);
            //                        //    break;
            //                        default:
            //                            break;
            //                    }
            //                }
            //            }
            //        }

            //    } else
            //    {
            //        richTextBox1.Text = ret.ToString() + " - " + msgerr;
            //        //MessageBox.Show(ret.ToString() + " - " + msgerr);
            //    }
            //}
            //catch (Exception ex)
            //{

            //    //LOG.Error(" Error: " + ex.Message);
            //}
        }

        private Image GetImage(string b64image)
        {
            Image imgRet;
            try
            {
                MemoryStream ms;
                byte[] byImage = Convert.FromBase64String(b64image);
                ms = new MemoryStream(byImage);
                imgRet = Image.FromStream(ms);
                ms.Close();
            }
            catch (Exception ex)
            {
                imgRet = null;
                //LOG.Error(" Error: " + ex.Message);
            }
            return imgRet;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            pic1.Image = null;
            pic2.Image = null;
            pic3.Image = null;
            pic4.Image = null;
            pic5.Image = null;
            pic6.Image = null;
            pic7.Image = null;
            pic8.Image = null;
            pic9.Image = null;
            labPic1.Text = "??";
            labPic2.Text = "??";
            labPic3.Text = "??";
            labPic4.Text = "??";
            labPic5.Text = "??";
            labPic6.Text = "??";
            labPic7.Text = "??";
            labPic8.Text = "??";
            labPic9.Text = "??";
            rtbResult.Text = "";
        }

        private void btnProcesar_Click_1(object sender, EventArgs e)
        {
            //Document _Document;
            //string msgerr = "";
            //int ret = 0;
            //try
            //{
            //    if (!string.IsNullOrEmpty(_STR_IMAGE1) || !string.IsNullOrEmpty(_STR_IMAGE2))
            //    {
            //        button5_Click(null, null);
            //        richTextBox1.Text = "Procesando... Espere por favor...";

            //        ret = _REGULA_HANDLER.RecognizeDocument(_STR_IMAGE1, _STR_IMAGE2, 508, out _Document, out msgerr);
            //        if (ret == 0)
            //        {
            //            if (_Document != null)
            //            {
            //                if (_Document._DocInfo != null)
            //                {
            //                    richTextBox1.Text = "Tipo Documento = " + _Document._DocInfo.ft_Document_Class_Code + Environment.NewLine;

            //                    richTextBox1.Text += "Nro Documento = " + _Document._DocInfo.ft_Personal_Number + Environment.NewLine;
            //                    richTextBox1.Text += "Serial Documento = " + _Document._DocInfo.ft_Document_Number + Environment.NewLine;
            //                    richTextBox1.Text += "Nombre = " + _Document._DocInfo.ft_Surname_And_Given_Names + Environment.NewLine;
            //                    richTextBox1.Text += "Fecha Nacimiento = " + _Document._DocInfo.ft_Date_of_Birth +
            //                                         " [Edad=" + _Document._DocInfo.ft_Age + "]" + Environment.NewLine;
            //                    richTextBox1.Text += "Fecha Expiracion = " + _Document._DocInfo.ft_Date_of_Expiry + Environment.NewLine;
            //                    richTextBox1.Text += "Sexo = " + _Document._DocInfo.ft_Sex + Environment.NewLine;
            //                    richTextBox1.Text += "Nacionalidad = " + _Document._DocInfo.ft_Nationality;
            //                    //+ Environment.NewLine;
            //                    //richTextBox1.Text += "Profesion = " + _Document._DocInfo.ft_Profesion;
            //                }

            //                if (_Document._DocImages != null)
            //                {
            //                    for (int i = 0; i < _Document._DocImages.Count; i++)
            //                    {
            //                        switch (i)
            //                        {
            //                            case 0:
            //                                labPic1.Text = _Document._DocImages[i].name;
            //                                pic1.Image = GetImage(_Document._DocImages[i].b64image);
            //                                break;
            //                            case 1:
            //                                labPic2.Text = _Document._DocImages[i].name;
            //                                pic2.Image = GetImage(_Document._DocImages[i].b64image);
            //                                break;
            //                            case 2:
            //                                labPic3.Text = _Document._DocImages[i].name;
            //                                pic3.Image = GetImage(_Document._DocImages[i].b64image);
            //                                break;
            //                            case 3:
            //                                labPic4.Text = _Document._DocImages[i].name;
            //                                pic4.Image = GetImage(_Document._DocImages[i].b64image);
            //                                break;
            //                            case 4:
            //                                labPic5.Text = _Document._DocImages[i].name;
            //                                pic5.Image = GetImage(_Document._DocImages[i].b64image);
            //                                break;
            //                            case 5:
            //                                labPic6.Text = _Document._DocImages[i].name;
            //                                pic6.Image = GetImage(_Document._DocImages[i].b64image);
            //                                break;
            //                            case 6:
            //                                labPic7.Text = _Document._DocImages[i].name;
            //                                pic7.Image = GetImage(_Document._DocImages[i].b64image);
            //                                break;
            //                            case 7:
            //                                labPic8.Text = _Document._DocImages[i].name;
            //                                pic8.Image = GetImage(_Document._DocImages[i].b64image);
            //                                break;
            //                            case 8:
            //                                labPic9.Text = _Document._DocImages[i].name;
            //                                pic9.Image = GetImage(_Document._DocImages[i].b64image);
            //                                break;
            //                            //case 9:
            //                            //    pic2.Image = GetImage(_Document._DocImages[i].b64image);
            //                            //    break;
            //                            default:
            //                                break;
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //        else
            //        {
            //            richTextBox1.Text = ret.ToString() + " - " + msgerr;
            //            //MessageBox.Show(ret.ToString() + " - " + msgerr);
            //        }
            //    }
            //    else
            //    {
            //        richTextBox1.Text = "Debe tener capturada al menos una imágen...";
            //        //MessageBox.Show(ret.ToString() + " - " + msgerr);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    richTextBox1.Text = "Exception = " + ex.Message;
            //    //LOG.Error(" Error: " + ex.Message);
            //}
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            picImage1.Image = null;
            _STR_IMAGE1 = null;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            picImage2.Image = null;
            _STR_IMAGE2 = null;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            _2021.Document _Document;
            string msgerr = "";
            int ret = 0;
            try
            {
                if (!string.IsNullOrEmpty(_STR_IMAGE1) || !string.IsNullOrEmpty(_STR_IMAGE2))
                {
                    button5_Click(null, null);
                    rtbResult.Text = "Procesando... Espere por favor... ";

                    DateTime dtStart = DateTime.Now;
                    ret = _REGULA_HANDLER_v2021.RecognizeDocument(_STR_IMAGE1, _STR_IMAGE2, out _Document, out msgerr);
                    DateTime dtStop = DateTime.Now;
                    double dtDif = (dtStop - dtStart).TotalMilliseconds;
                    string SEMILLA = DateTime.Now.ToString("yyyyMMddHHmmss");
                    if (ret == 0)
                    {

                        if (_Document != null)
                        {
                            if (_Document._DocInfo != null)
                            {
                                rtbResult.Text = "Tiempo de Proceso = " + dtDif.ToString() + " milisegundos " + Environment.NewLine;
                                rtbResult.Text += "Tipo Documento = " + _Document._DocInfo.ft_Document_Class_Code + Environment.NewLine;

                                rtbResult.Text += "Nro Documento = " + _Document._DocInfo.ft_Personal_Number + Environment.NewLine;
                                rtbResult.Text += "Serial Documento = " + _Document._DocInfo.ft_Document_Number + Environment.NewLine;
                                rtbResult.Text += "Nombre = " + _Document._DocInfo.ft_Surname_And_Given_Names + Environment.NewLine;
                                rtbResult.Text += "Fecha Nacimiento = " + _Document._DocInfo.ft_Date_of_Birth +
                                                     " [Edad=" + _Document._DocInfo.ft_Age + "]" + Environment.NewLine;
                                rtbResult.Text += "Fecha Expiracion = " + _Document._DocInfo.ft_Date_of_Expiry + Environment.NewLine;
                                rtbResult.Text += "Sexo = " + _Document._DocInfo.ft_Sex + Environment.NewLine;
                                rtbResult.Text += "Nacionalidad = " + _Document._DocInfo.ft_Nationality + Environment.NewLine;
                                rtbResult.Text += "Profesión = " + _Document._DocInfo.ft_Profesion + Environment.NewLine;
                                rtbResult.Text += "Visa = " + _Document._DocInfo.ft_Visa;

                                if (chkSaveHDD.Checked)
                                {
                                    System.IO.File.WriteAllText("\\data\\" + _Document._DocInfo.ft_Personal_Number + "_" +
                                                                SEMILLA + "_data.txt", rtbResult.Text);
                                }
                                //+ Environment.NewLine;
                                //richTextBox1.Text += "Profesion = " + _Document._DocInfo.ft_Profesion;
                            }

                            if (_Document._DocImages != null)
                            {
                                for (int i = 0; i < _Document._DocImages.Count; i++)
                                {
                                    if (chkSaveHDD.Checked)
                                    {
                                        System.IO.File.WriteAllText("\\data\\" + _Document._DocInfo.ft_Personal_Number + "_" +
                                                                SEMILLA + "_" + 
                                                               _Document._DocImages[i].name + ".b64.txt",
                                                               _Document._DocImages[i].b64image);

                                        System.IO.File.WriteAllBytes("\\data\\" + _Document._DocInfo.ft_Personal_Number + "_" +
                                                               SEMILLA + "_" +
                                                               _Document._DocImages[i].name + ".jpg",
                                                               Convert.FromBase64String(_Document._DocImages[i].b64image));
                                        //PictureBox GetImage(_Document._DocImages[i].b64image)
                                        //    .Save("\\data\\" + _Document._DocInfo.ft_Personal_Number + "_" +
                                        //                       DateTime.Now.ToString("yyyyMMddHHmmss") + "_" +
                                        //                       _Document._DocImages[i].name + ".jpg",
                                        //                       System.Drawing.Imaging.ImageFormat.Jpeg);
                                    }
                                    switch (i)
                                    {
                                        case 0:
                                            labPic1.Text = _Document._DocImages[i].name;
                                            pic1.Image = GetImage(_Document._DocImages[i].b64image);
                                            break;
                                        case 1:
                                            labPic2.Text = _Document._DocImages[i].name;
                                            pic2.Image = GetImage(_Document._DocImages[i].b64image);
                                            break;
                                        case 2:
                                            labPic3.Text = _Document._DocImages[i].name;
                                            pic3.Image = GetImage(_Document._DocImages[i].b64image);
                                            break;
                                        case 3:
                                            labPic4.Text = _Document._DocImages[i].name;
                                            pic4.Image = GetImage(_Document._DocImages[i].b64image);
                                            break;
                                        case 4:
                                            labPic5.Text = _Document._DocImages[i].name;
                                            pic5.Image = GetImage(_Document._DocImages[i].b64image);
                                            break;
                                        case 5:
                                            labPic6.Text = _Document._DocImages[i].name;
                                            pic6.Image = GetImage(_Document._DocImages[i].b64image);
                                            break;
                                        case 6:
                                            labPic7.Text = _Document._DocImages[i].name;
                                            pic7.Image = GetImage(_Document._DocImages[i].b64image);
                                            break;
                                        case 7:
                                            labPic8.Text = _Document._DocImages[i].name;
                                            pic8.Image = GetImage(_Document._DocImages[i].b64image);
                                            break;
                                        case 8:
                                            labPic9.Text = _Document._DocImages[i].name;
                                            pic9.Image = GetImage(_Document._DocImages[i].b64image);
                                            break;
                                        //case 9:
                                        //    pic2.Image = GetImage(_Document._DocImages[i].b64image);
                                        //    break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        rtbResult.Text = ret.ToString() + " - " + msgerr;
                        //MessageBox.Show(ret.ToString() + " - " + msgerr);
                    }
                }
                else
                {
                    rtbResult.Text = "Debe tener capturada al menos una imágen...";
                    //MessageBox.Show(ret.ToString() + " - " + msgerr);
                }
            }
            catch (Exception ex)
            {
                rtbResult.Text = "Exception = " + ex.Message;
                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void btnLoad1_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Seleccione sample en Base 64 o jpg...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    //System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
                    //image1 = sr.ReadToEnd();
                    //sr.Close();

                    if (!ckkIsB641.Checked)
                    {
                        byte[] byimage1 = System.IO.File.ReadAllBytes(openFileDialog1.FileName);
                        picImage1.Image = Image.FromFile(openFileDialog1.FileName);
                    }
                    else
                    {
                        string image1 = System.IO.File.ReadAllText(openFileDialog1.FileName);
                        System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(image1));
                        byte[] by = ms.ToArray();
                        picImage1.Image = Bitmap.FromStream(ms);
                    }
                    _STR_IMAGE1 = GetB64FromPictureBox(picImage1);
                }
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void btnLoad2_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Seleccione sample en Base 64 o jpg...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    //System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
                    //image1 = sr.ReadToEnd();
                    //sr.Close();

                    if (!ckkIsB642.Checked)
                    {
                        byte[] byimage1 = System.IO.File.ReadAllBytes(openFileDialog1.FileName);
                        picImage2.Image = Image.FromFile(openFileDialog1.FileName);
                    }
                    else
                    {
                        string image1 = System.IO.File.ReadAllText(openFileDialog1.FileName);
                        System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(image1));
                        byte[] by = ms.ToArray();
                        picImage2.Image = Bitmap.FromStream(ms);
                    }
                    _STR_IMAGE2 = GetB64FromPictureBox(picImage2);
                }
            }
            catch (Exception ex)
            {

                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            int ret = _REGULA_HANDLER_v2021.Init(txtUrlService.Text, "regula.license", Convert.ToInt32(txtType.Text));
            rtbResult.Text = "Set service en: " + txtUrlService.Text + " => ret = " + ret.ToString();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            int ret = _FOXID_API.Initialize(txtURLFoxId.Text, "info@biometrika.cl", "B1ometrik@", 
                                            "web_app", "42dbc72e-bf12-4773-bea8-bb31692625eb", 60000);
            rtbResult.Text = "Set service en: " + txtURLFoxId.Text + " => ret = " + ret.ToString();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            FoxId.Api.Report _Report;
            string msgerr = "";
            int ret = 0;
            try
            {
                if (!string.IsNullOrEmpty(_STR_IMAGE1) || !string.IsNullOrEmpty(_STR_IMAGE2))
                {
                    button5_Click(null, null);
                    rtbResult.Text = "Procesando... Espere por favor... ";

                    List<FoxId.Api.Picture> listpic = new List<FoxId.Api.Picture>();
                    if (!string.IsNullOrEmpty(_STR_IMAGE1))
                    {
                        FoxId.Api.Picture pic = new FoxId.Api.Picture();
                        pic.Base64ImageString = _STR_IMAGE1;
                        pic.Format = ".jpg";
                        pic.Side = FoxId.Api.Picture.SIDE_FRONT;
                        listpic.Add(pic);
                    }
                    if (!string.IsNullOrEmpty(_STR_IMAGE2))
                    {
                        FoxId.Api.Picture pic = new FoxId.Api.Picture();
                        pic.Base64ImageString = _STR_IMAGE2;
                        pic.Format = ".jpg";
                        pic.Side = FoxId.Api.Picture.SIDE_BACK;
                        listpic.Add(pic);
                    }

                    DateTime dtStart = DateTime.Now;
                    ret = _FOXID_API.Process(listpic, out _Report, out msgerr);
                    DateTime dtStop = DateTime.Now;
                    double dtDif = (dtStop - dtStart).TotalMilliseconds;
                    if (ret == 0)
                    {
                        if (_Report != null)
                        {
                            rtbResult.Text += "Tiempo de Proceso = " + dtDif.ToString() + " milisegundos " + Environment.NewLine;
                            rtbResult.Text = "Status = " + _Report.status + Environment.NewLine 
                                             + "finalVerificationsResult = " + _Report.data.results.finalVerificationsResult.ToString()
                                             + Environment.NewLine;
                            if (_Report.data.mrzData != null)
                            {
                               
                                rtbResult.Text += "Tipo Documento = " + _Report.data.documentData.doc_type + Environment.NewLine;
                                rtbResult.Text += "Nro Documento = " + _Report.data.mrzData.personal_number + Environment.NewLine;
                                rtbResult.Text += "Serial Documento = " + _Report.data.mrzData .card_number+ Environment.NewLine;
                                rtbResult.Text += "Nombre = " + _Report.data.mrzData.surname_and_name + Environment.NewLine;
                                rtbResult.Text += "Fecha Nacimiento = " + _Report.data.mrzData.date_of_birth +
                                                     " [Edad=" + _Report.data.personalData.age + "]" + Environment.NewLine;
                                rtbResult.Text += "Fecha Expiracion = " + _Report.data.mrzData.date_of_expiry + Environment.NewLine;
                                rtbResult.Text += "Sexo = " + _Report.data.mrzData.gender + Environment.NewLine;
                                rtbResult.Text += "Nacionalidad = " + _Report.data.mrzData.nationality + Environment.NewLine;
                                //rtbResult.Text += "Profesión = " + _Report.data.mrzData + Environment.NewLine;
                                //rtbResult.Text += "Visa = " + _Document._DocInfo.ft_Visa;

                                //+ Environment.NewLine;
                                //richTextBox1.Text += "Profesion = " + _Document._DocInfo.ft_Profesion;
                            }




                            //if (_Document._DocImages != null)
                            //{
                            //    for (int i = 0; i < _Document._DocImages.Count; i++)
                            //    {
                            //        switch (i)
                            //        {
                            //            case 0:
                            //                labPic1.Text = _Document._DocImages[i].name;
                            //                pic1.Image = GetImage(_Document._DocImages[i].b64image);
                            //                break;
                            //            case 1:
                            //                labPic2.Text = _Document._DocImages[i].name;
                            //                pic2.Image = GetImage(_Document._DocImages[i].b64image);
                            //                break;
                            //            case 2:
                            //                labPic3.Text = _Document._DocImages[i].name;
                            //                pic3.Image = GetImage(_Document._DocImages[i].b64image);
                            //                break;
                            //            case 3:
                            //                labPic4.Text = _Document._DocImages[i].name;
                            //                pic4.Image = GetImage(_Document._DocImages[i].b64image);
                            //                break;
                            //            case 4:
                            //                labPic5.Text = _Document._DocImages[i].name;
                            //                pic5.Image = GetImage(_Document._DocImages[i].b64image);
                            //                break;
                            //            case 5:
                            //                labPic6.Text = _Document._DocImages[i].name;
                            //                pic6.Image = GetImage(_Document._DocImages[i].b64image);
                            //                break;
                            //            case 6:
                            //                labPic7.Text = _Document._DocImages[i].name;
                            //                pic7.Image = GetImage(_Document._DocImages[i].b64image);
                            //                break;
                            //            case 7:
                            //                labPic8.Text = _Document._DocImages[i].name;
                            //                pic8.Image = GetImage(_Document._DocImages[i].b64image);
                            //                break;
                            //            case 8:
                            //                labPic9.Text = _Document._DocImages[i].name;
                            //                pic9.Image = GetImage(_Document._DocImages[i].b64image);
                            //                break;
                            //            //case 9:
                            //            //    pic2.Image = GetImage(_Document._DocImages[i].b64image);
                            //            //    break;
                            //            default:
                            //                break;
                            //        }
                            //    }
                            //}
                        }
                    }
                    else
                    {
                        rtbResult.Text = ret.ToString() + " - " + msgerr;
                        //MessageBox.Show(ret.ToString() + " - " + msgerr);
                    }
                }
                else
                {
                    rtbResult.Text = "Debe tener capturada al menos una imágen...";
                    //MessageBox.Show(ret.ToString() + " - " + msgerr);
                }
            }
            catch (Exception ex)
            {
                rtbResult.Text = "Exception = " + ex.Message;
                //LOG.Error(" Error: " + ex.Message);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            string msg;
            int ret = _T5_CLIENT.Initialize(txtUrlT5.Text, "info@biometrika.cl", "B1ometrik@",
                                           "web_app", "42dbc72e-bf12-4773-bea8-bb31692625eb", out msg, 60000);
            rtbResult.Text = "Set service Tech5 en: " + txtUrlT5.Text + " => ret = " + ret.ToString() + " => [msgFeedback=" +
                                (string.IsNullOrEmpty(msg)?"":msg) + "]";
        }
    }
}
