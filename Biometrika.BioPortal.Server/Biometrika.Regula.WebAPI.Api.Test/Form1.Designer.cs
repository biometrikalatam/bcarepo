﻿namespace Biometrika.Regula.WebAPI.Api.Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.imageCam = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnCapture1 = new System.Windows.Forms.Button();
            this.picImage1 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.picImage2 = new System.Windows.Forms.PictureBox();
            this.btnCapture2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.labPic9 = new System.Windows.Forms.Label();
            this.labPic8 = new System.Windows.Forms.Label();
            this.labPic7 = new System.Windows.Forms.Label();
            this.labPic6 = new System.Windows.Forms.Label();
            this.labPic5 = new System.Windows.Forms.Label();
            this.labPic4 = new System.Windows.Forms.Label();
            this.labPic3 = new System.Windows.Forms.Label();
            this.labPic2 = new System.Windows.Forms.Label();
            this.labPic1 = new System.Windows.Forms.Label();
            this.rtbResult = new System.Windows.Forms.RichTextBox();
            this.pic9 = new System.Windows.Forms.PictureBox();
            this.pic6 = new System.Windows.Forms.PictureBox();
            this.pic3 = new System.Windows.Forms.PictureBox();
            this.pic8 = new System.Windows.Forms.PictureBox();
            this.pic5 = new System.Windows.Forms.PictureBox();
            this.pic2 = new System.Windows.Forms.PictureBox();
            this.pic7 = new System.Windows.Forms.PictureBox();
            this.pic4 = new System.Windows.Forms.PictureBox();
            this.pic1 = new System.Windows.Forms.PictureBox();
            this.btnProcesar = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCamara = new System.Windows.Forms.TextBox();
            this.btnLoad1 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.ckkIsB641 = new System.Windows.Forms.CheckBox();
            this.ckkIsB642 = new System.Windows.Forms.CheckBox();
            this.btnLoad2 = new System.Windows.Forms.Button();
            this.txtUrlService = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.txtType = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.txtURLFoxId = new System.Windows.Forms.TextBox();
            this.chkSaveHDD = new System.Windows.Forms.CheckBox();
            this.button10 = new System.Windows.Forms.Button();
            this.txtUrlT5 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage2)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).BeginInit();
            this.SuspendLayout();
            // 
            // imageCam
            // 
            this.imageCam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageCam.Location = new System.Drawing.Point(35, 37);
            this.imageCam.Name = "imageCam";
            this.imageCam.Size = new System.Drawing.Size(467, 318);
            this.imageCam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageCam.TabIndex = 0;
            this.imageCam.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(508, 37);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnCapture1
            // 
            this.btnCapture1.Location = new System.Drawing.Point(679, 8);
            this.btnCapture1.Name = "btnCapture1";
            this.btnCapture1.Size = new System.Drawing.Size(75, 23);
            this.btnCapture1.TabIndex = 2;
            this.btnCapture1.Text = "Capture";
            this.btnCapture1.UseVisualStyleBackColor = true;
            this.btnCapture1.Click += new System.EventHandler(this.button2_Click);
            // 
            // picImage1
            // 
            this.picImage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picImage1.Location = new System.Drawing.Point(667, 37);
            this.picImage1.Name = "picImage1";
            this.picImage1.Size = new System.Drawing.Size(386, 264);
            this.picImage1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picImage1.TabIndex = 3;
            this.picImage1.TabStop = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(508, 66);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Stop";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // picImage2
            // 
            this.picImage2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picImage2.Location = new System.Drawing.Point(1078, 37);
            this.picImage2.Name = "picImage2";
            this.picImage2.Size = new System.Drawing.Size(386, 264);
            this.picImage2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picImage2.TabIndex = 6;
            this.picImage2.TabStop = false;
            // 
            // btnCapture2
            // 
            this.btnCapture2.Location = new System.Drawing.Point(1090, 8);
            this.btnCapture2.Name = "btnCapture2";
            this.btnCapture2.Size = new System.Drawing.Size(75, 23);
            this.btnCapture2.TabIndex = 5;
            this.btnCapture2.Text = "Capture";
            this.btnCapture2.UseVisualStyleBackColor = true;
            this.btnCapture2.Click += new System.EventHandler(this.btnCapture2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.labPic9);
            this.groupBox1.Controls.Add(this.labPic8);
            this.groupBox1.Controls.Add(this.labPic7);
            this.groupBox1.Controls.Add(this.labPic6);
            this.groupBox1.Controls.Add(this.labPic5);
            this.groupBox1.Controls.Add(this.labPic4);
            this.groupBox1.Controls.Add(this.labPic3);
            this.groupBox1.Controls.Add(this.labPic2);
            this.groupBox1.Controls.Add(this.labPic1);
            this.groupBox1.Controls.Add(this.rtbResult);
            this.groupBox1.Controls.Add(this.pic9);
            this.groupBox1.Controls.Add(this.pic6);
            this.groupBox1.Controls.Add(this.pic3);
            this.groupBox1.Controls.Add(this.pic8);
            this.groupBox1.Controls.Add(this.pic5);
            this.groupBox1.Controls.Add(this.pic2);
            this.groupBox1.Controls.Add(this.pic7);
            this.groupBox1.Controls.Add(this.pic4);
            this.groupBox1.Controls.Add(this.pic1);
            this.groupBox1.Controls.Add(this.btnProcesar);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Location = new System.Drawing.Point(35, 375);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1429, 411);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resultado";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(955, 0);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(126, 23);
            this.button9.TabIndex = 35;
            this.button9.Text = "Procesar FoxId";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(98, 0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(126, 23);
            this.button6.TabIndex = 34;
            this.button6.Text = "Procesar v2021";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // labPic9
            // 
            this.labPic9.BackColor = System.Drawing.Color.White;
            this.labPic9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labPic9.Location = new System.Drawing.Point(404, 376);
            this.labPic9.Name = "labPic9";
            this.labPic9.Size = new System.Drawing.Size(173, 16);
            this.labPic9.TabIndex = 33;
            this.labPic9.Text = "??";
            this.labPic9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labPic8
            // 
            this.labPic8.BackColor = System.Drawing.Color.White;
            this.labPic8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labPic8.Location = new System.Drawing.Point(215, 376);
            this.labPic8.Name = "labPic8";
            this.labPic8.Size = new System.Drawing.Size(173, 16);
            this.labPic8.TabIndex = 32;
            this.labPic8.Text = "??";
            this.labPic8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labPic7
            // 
            this.labPic7.BackColor = System.Drawing.Color.White;
            this.labPic7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labPic7.Location = new System.Drawing.Point(29, 378);
            this.labPic7.Name = "labPic7";
            this.labPic7.Size = new System.Drawing.Size(173, 16);
            this.labPic7.TabIndex = 31;
            this.labPic7.Text = "??";
            this.labPic7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labPic6
            // 
            this.labPic6.BackColor = System.Drawing.Color.White;
            this.labPic6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labPic6.Location = new System.Drawing.Point(404, 253);
            this.labPic6.Name = "labPic6";
            this.labPic6.Size = new System.Drawing.Size(173, 16);
            this.labPic6.TabIndex = 30;
            this.labPic6.Text = "??";
            this.labPic6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labPic5
            // 
            this.labPic5.BackColor = System.Drawing.Color.White;
            this.labPic5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labPic5.Location = new System.Drawing.Point(215, 253);
            this.labPic5.Name = "labPic5";
            this.labPic5.Size = new System.Drawing.Size(173, 16);
            this.labPic5.TabIndex = 29;
            this.labPic5.Text = "??";
            this.labPic5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labPic4
            // 
            this.labPic4.BackColor = System.Drawing.Color.White;
            this.labPic4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labPic4.Location = new System.Drawing.Point(29, 253);
            this.labPic4.Name = "labPic4";
            this.labPic4.Size = new System.Drawing.Size(173, 16);
            this.labPic4.TabIndex = 28;
            this.labPic4.Text = "??";
            this.labPic4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labPic3
            // 
            this.labPic3.BackColor = System.Drawing.Color.White;
            this.labPic3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labPic3.Location = new System.Drawing.Point(404, 130);
            this.labPic3.Name = "labPic3";
            this.labPic3.Size = new System.Drawing.Size(173, 16);
            this.labPic3.TabIndex = 27;
            this.labPic3.Text = "??";
            this.labPic3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labPic2
            // 
            this.labPic2.BackColor = System.Drawing.Color.White;
            this.labPic2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labPic2.Location = new System.Drawing.Point(213, 130);
            this.labPic2.Name = "labPic2";
            this.labPic2.Size = new System.Drawing.Size(177, 16);
            this.labPic2.TabIndex = 26;
            this.labPic2.Text = "??";
            this.labPic2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labPic1
            // 
            this.labPic1.BackColor = System.Drawing.Color.White;
            this.labPic1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labPic1.Location = new System.Drawing.Point(28, 130);
            this.labPic1.Name = "labPic1";
            this.labPic1.Size = new System.Drawing.Size(176, 16);
            this.labPic1.TabIndex = 25;
            this.labPic1.Text = "??";
            this.labPic1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // rtbResult
            // 
            this.rtbResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.rtbResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbResult.Location = new System.Drawing.Point(603, 29);
            this.rtbResult.Name = "rtbResult";
            this.rtbResult.ReadOnly = true;
            this.rtbResult.Size = new System.Drawing.Size(813, 365);
            this.rtbResult.TabIndex = 24;
            this.rtbResult.Text = "";
            // 
            // pic9
            // 
            this.pic9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic9.Location = new System.Drawing.Point(402, 275);
            this.pic9.Name = "pic9";
            this.pic9.Size = new System.Drawing.Size(177, 117);
            this.pic9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic9.TabIndex = 23;
            this.pic9.TabStop = false;
            // 
            // pic6
            // 
            this.pic6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic6.Location = new System.Drawing.Point(402, 152);
            this.pic6.Name = "pic6";
            this.pic6.Size = new System.Drawing.Size(177, 117);
            this.pic6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic6.TabIndex = 22;
            this.pic6.TabStop = false;
            // 
            // pic3
            // 
            this.pic3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic3.Location = new System.Drawing.Point(402, 29);
            this.pic3.Name = "pic3";
            this.pic3.Size = new System.Drawing.Size(177, 117);
            this.pic3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic3.TabIndex = 21;
            this.pic3.TabStop = false;
            // 
            // pic8
            // 
            this.pic8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic8.Location = new System.Drawing.Point(213, 275);
            this.pic8.Name = "pic8";
            this.pic8.Size = new System.Drawing.Size(177, 117);
            this.pic8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic8.TabIndex = 20;
            this.pic8.TabStop = false;
            // 
            // pic5
            // 
            this.pic5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic5.Location = new System.Drawing.Point(213, 152);
            this.pic5.Name = "pic5";
            this.pic5.Size = new System.Drawing.Size(177, 117);
            this.pic5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic5.TabIndex = 19;
            this.pic5.TabStop = false;
            // 
            // pic2
            // 
            this.pic2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic2.Location = new System.Drawing.Point(213, 29);
            this.pic2.Name = "pic2";
            this.pic2.Size = new System.Drawing.Size(177, 117);
            this.pic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic2.TabIndex = 18;
            this.pic2.TabStop = false;
            // 
            // pic7
            // 
            this.pic7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic7.Location = new System.Drawing.Point(27, 277);
            this.pic7.Name = "pic7";
            this.pic7.Size = new System.Drawing.Size(177, 117);
            this.pic7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic7.TabIndex = 17;
            this.pic7.TabStop = false;
            // 
            // pic4
            // 
            this.pic4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic4.Location = new System.Drawing.Point(27, 152);
            this.pic4.Name = "pic4";
            this.pic4.Size = new System.Drawing.Size(177, 117);
            this.pic4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic4.TabIndex = 16;
            this.pic4.TabStop = false;
            // 
            // pic1
            // 
            this.pic1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic1.Location = new System.Drawing.Point(27, 29);
            this.pic1.Name = "pic1";
            this.pic1.Size = new System.Drawing.Size(177, 117);
            this.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic1.TabIndex = 15;
            this.pic1.TabStop = false;
            // 
            // btnProcesar
            // 
            this.btnProcesar.Location = new System.Drawing.Point(6, 0);
            this.btnProcesar.Name = "btnProcesar";
            this.btnProcesar.Size = new System.Drawing.Size(75, 23);
            this.btnProcesar.TabIndex = 12;
            this.btnProcesar.Text = "Procesar";
            this.btnProcesar.UseVisualStyleBackColor = true;
            this.btnProcesar.Click += new System.EventHandler(this.btnProcesar_Click_1);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(1341, 0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 13;
            this.button5.Text = "Borrar";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(978, 307);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "Borrar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1389, 307);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 10;
            this.button4.Text = "Borrar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Camara: ";
            // 
            // txtCamara
            // 
            this.txtCamara.Location = new System.Drawing.Point(119, 15);
            this.txtCamara.Name = "txtCamara";
            this.txtCamara.Size = new System.Drawing.Size(100, 20);
            this.txtCamara.TabIndex = 12;
            this.txtCamara.Text = "0";
            // 
            // btnLoad1
            // 
            this.btnLoad1.Location = new System.Drawing.Point(873, 8);
            this.btnLoad1.Name = "btnLoad1";
            this.btnLoad1.Size = new System.Drawing.Size(75, 23);
            this.btnLoad1.TabIndex = 13;
            this.btnLoad1.Text = "Load Image";
            this.btnLoad1.UseVisualStyleBackColor = true;
            this.btnLoad1.Click += new System.EventHandler(this.btnLoad1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // ckkIsB641
            // 
            this.ckkIsB641.AutoSize = true;
            this.ckkIsB641.Location = new System.Drawing.Point(786, 13);
            this.ckkIsB641.Name = "ckkIsB641";
            this.ckkIsB641.Size = new System.Drawing.Size(77, 17);
            this.ckkIsB641.TabIndex = 14;
            this.ckkIsB641.Text = "Es Base64";
            this.ckkIsB641.UseVisualStyleBackColor = true;
            // 
            // ckkIsB642
            // 
            this.ckkIsB642.AutoSize = true;
            this.ckkIsB642.Location = new System.Drawing.Point(1174, 12);
            this.ckkIsB642.Name = "ckkIsB642";
            this.ckkIsB642.Size = new System.Drawing.Size(77, 17);
            this.ckkIsB642.TabIndex = 16;
            this.ckkIsB642.Text = "Es Base64";
            this.ckkIsB642.UseVisualStyleBackColor = true;
            // 
            // btnLoad2
            // 
            this.btnLoad2.Location = new System.Drawing.Point(1261, 7);
            this.btnLoad2.Name = "btnLoad2";
            this.btnLoad2.Size = new System.Drawing.Size(75, 23);
            this.btnLoad2.TabIndex = 15;
            this.btnLoad2.Text = "Load Image";
            this.btnLoad2.UseVisualStyleBackColor = true;
            this.btnLoad2.Click += new System.EventHandler(this.btnLoad2_Click);
            // 
            // txtUrlService
            // 
            this.txtUrlService.Location = new System.Drawing.Point(524, 320);
            this.txtUrlService.Name = "txtUrlService";
            this.txtUrlService.Size = new System.Drawing.Size(263, 20);
            this.txtUrlService.TabIndex = 35;
            this.txtUrlService.Text = "http://localhost:8089/";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(793, 320);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 36;
            this.button7.Text = "Set Service";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // txtType
            // 
            this.txtType.Location = new System.Drawing.Point(524, 349);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(141, 20);
            this.txtType.TabIndex = 37;
            this.txtType.Text = "1";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(1101, 348);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(113, 23);
            this.button8.TabIndex = 39;
            this.button8.Text = "Set Service FoxId";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // txtURLFoxId
            // 
            this.txtURLFoxId.Location = new System.Drawing.Point(891, 346);
            this.txtURLFoxId.Name = "txtURLFoxId";
            this.txtURLFoxId.Size = new System.Drawing.Size(204, 20);
            this.txtURLFoxId.TabIndex = 38;
            this.txtURLFoxId.Text = "https://foxid5.einzelnet.com";
            // 
            // chkSaveHDD
            // 
            this.chkSaveHDD.AutoSize = true;
            this.chkSaveHDD.Location = new System.Drawing.Point(691, 352);
            this.chkSaveHDD.Name = "chkSaveHDD";
            this.chkSaveHDD.Size = new System.Drawing.Size(122, 17);
            this.chkSaveHDD.TabIndex = 40;
            this.chkSaveHDD.Text = "Save in HDD Result";
            this.chkSaveHDD.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(1290, 332);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(93, 23);
            this.button10.TabIndex = 42;
            this.button10.Text = "Set T5";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // txtUrlT5
            // 
            this.txtUrlT5.Location = new System.Drawing.Point(1078, 309);
            this.txtUrlT5.Name = "txtUrlT5";
            this.txtUrlT5.Size = new System.Drawing.Size(305, 20);
            this.txtUrlT5.TabIndex = 41;
            this.txtUrlT5.Text = "http://gn-testapi.tech5.tech:8080/T5CloudService/1.0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1503, 810);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.txtUrlT5);
            this.Controls.Add(this.chkSaveHDD);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.txtURLFoxId);
            this.Controls.Add(this.txtType);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.txtUrlService);
            this.Controls.Add(this.ckkIsB642);
            this.Controls.Add(this.btnLoad2);
            this.Controls.Add(this.ckkIsB641);
            this.Controls.Add(this.btnLoad1);
            this.Controls.Add(this.txtCamara);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.picImage2);
            this.Controls.Add(this.btnCapture2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.picImage1);
            this.Controls.Add(this.btnCapture1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.imageCam);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Demo Document Reader v1.0...";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox imageCam;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnCapture1;
        private System.Windows.Forms.PictureBox picImage1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox picImage2;
        private System.Windows.Forms.Button btnCapture2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox rtbResult;
        private System.Windows.Forms.PictureBox pic9;
        private System.Windows.Forms.PictureBox pic6;
        private System.Windows.Forms.PictureBox pic3;
        private System.Windows.Forms.PictureBox pic8;
        private System.Windows.Forms.PictureBox pic5;
        private System.Windows.Forms.PictureBox pic2;
        private System.Windows.Forms.PictureBox pic7;
        private System.Windows.Forms.PictureBox pic4;
        private System.Windows.Forms.PictureBox pic1;
        private System.Windows.Forms.Button btnProcesar;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label labPic9;
        private System.Windows.Forms.Label labPic8;
        private System.Windows.Forms.Label labPic7;
        private System.Windows.Forms.Label labPic6;
        private System.Windows.Forms.Label labPic5;
        private System.Windows.Forms.Label labPic4;
        private System.Windows.Forms.Label labPic3;
        private System.Windows.Forms.Label labPic2;
        private System.Windows.Forms.Label labPic1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCamara;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button btnLoad1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.CheckBox ckkIsB641;
        private System.Windows.Forms.CheckBox ckkIsB642;
        private System.Windows.Forms.Button btnLoad2;
        private System.Windows.Forms.TextBox txtUrlService;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox txtURLFoxId;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.CheckBox chkSaveHDD;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TextBox txtUrlT5;
    }
}

