﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bio.Portal.Server.Manager.Core.libs;

namespace Bio.Portal.Server.Manager
{
    public partial class ErrorEx : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!(Page.IsPostBack))
                {
                    Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                    if (Session["mensajeerror"] != null)
                    {
                        txtEx.Text = Session["exception"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                txtEx.Text = ex.Message;
            }
        }
    }
}