﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="SessionExpired.aspx.cs" Inherits="Bio.Portal.Server.Manager.SessionExpired1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<table width="90%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center" class="titulos0">
            Expiró tiempo de sesión.
        </td>
    </tr>
    <tr>
        <td class="tdBottom">
            Debe volver a iniciar sesión<br />
            <asp:HyperLink runat="server" ID="hyperlink" NavigateUrl="~/Logon.aspx">Ingresar Nuevamente</asp:HyperLink>
        </td>
    </tr>
</table>
</asp:Content>
