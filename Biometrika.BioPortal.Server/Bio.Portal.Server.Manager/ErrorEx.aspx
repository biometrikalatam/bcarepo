﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorEx.aspx.cs" Inherits="Bio.Portal.Server.Manager.ErrorEx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BioPortal Error...</title>
</head>
<body>
    <form id="form1" runat="server">
  <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="titulos0">Detalle de la Excepción</td>
            </tr>
            <tr>
                <td class="tdBottom">
                    <asp:TextBox ID="txtEx" runat="server" Height="290px" Width="610px" Rows="30" 
                        TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
