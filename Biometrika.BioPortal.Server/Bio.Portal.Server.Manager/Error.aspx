﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="Bio.Portal.Server.Manager.SessionExpired" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
 <table width="90%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center" class="titulos0">
            Mensaje Importante
        </td>        
    </tr>
    
    
    <tr>
        <td class="tdBottom">
        <asp:Label runat="server" ID="mensaje"></asp:Label>&nbsp;&nbsp;
            <img border="0" alt="Ver detalle del error..." title="Ver detalle del error..." src="Images/zoom.png" onclick="PopUpInfo('ErrorEx.aspx');" />
            
        </td>
    </tr>
    <tr>
        <td class="tdBottom">
        <asp:HyperLink runat="server" ID="hyperlink" NavigateUrl="~/Welcome.aspx">Volver</asp:HyperLink>
            <asp:TextBox Visible="false" runat="server" ID="txtexception" Columns="0" Rows="0" 
                ReadOnly="true" Height="0px" TextMode="MultiLine" Width="0px"></asp:TextBox>
        </td>
    </tr>
</table>
</asp:Content>
