﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using log4net;
using Bio.Portal.Server.Common.Common;
using Bio.Portal.Server.Manager.Core.Database;
using System.Xml;
using System.IO;


namespace Bio.Portal.Server.Manager
{
    public partial class Logon : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Logon));
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Si no hay usuarios agregados a la aplicación, seleccionamos uno.
                if (Membership.GetAllUsers().Count == 0)
                {

                    Response.Redirect("Core/Admin/CreateUser.aspx?user=0");
                }

            }

        }

        protected void Login1_LoggedIn(object sender, EventArgs e)
        {

                log.Debug("Ingreso a Login1_LoggedIn :" + Login1.UserName);
                //Tomamos el nombre de usuario y dominio y lo dejamos en un arreglo.
                String[] usuario = Login1.UserName.Split('@');

                //usuario[0]=username / usuario[1]=dominio
                //Buscamos los datos de la empresa.
                string msgErr; 
                CompanyId = DatabaseHelper.Retrieve(usuario[1], out msgErr).Id;
                if (msgErr.Equals("S/C"))
                {
                    Username = usuario[0];
                    Domain = usuario[1];
                    UserNameFull = Login1.UserName;
                    log.Debug("Usuario logeado correctamente :" + Username);
                    Response.Redirect("Welcome.aspx");
                }
                else
                {
                    log.Error(msgErr);
                    //redireccionamos a página de error.
                }
            
           
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Bio.Portal.Server.Common.Config.BpConfig config = new Bio.Portal.Server.Common.Config.BpConfig();
            config.InsertOption = 0;
            config.Matchingtype = 1;
            config.MinutiaetypeDefault = 4;
            //config.MinutiaetypeToEnroll=5;
            config.SaveVerified = 0;
            config.TimeoutWSdefault = 1000;

            //Serializamos el objeto.
            string xml =BioPortal.Server.Api.XmlUtils.SerializeAnObject(config);
            Services.BioPortalServerAdmin test = new Services.BioPortalServerAdmin();
            string msg = "";
            test.UpdateConfig(xml, out msg);
            ////Bio.Core.Utils.SerializeHelper.SerializeToFile(config, "C:\\Biometrika\\BioPortal.Server.Config.cfg");
           
                       
        }

        protected void cmdtest_Click(object sender, EventArgs e)
        {
            int iErr;
            int res = Bio.Core.Constant.Errors.IERR_OK;
            string msg;            
            List<Bio.Portal.Server.Common.Entities.Database.BpBir> list;
            
            BioPortal.Server.Api.XmlParamIn oparamin = Bio.Core.Utils.SerializeHelper.DeserializeFromFile<BioPortal.Server.Api.XmlParamIn>("c:\\Biometrika\\acolomam.xml");
            iErr = Bio.Portal.Server.Common.Entities.AdminBpBir.RetrieveWithFilter(0, oparamin.Companyid,
                                                    oparamin.PersonalData != null ? oparamin.PersonalData.Typeid : null,
                                                    oparamin.PersonalData != null ? oparamin.PersonalData.Valueid : null,
                                                    oparamin.Authenticationfactor, oparamin.Minutiaetype, oparamin.Bodypart,
                                                    out list);
        }

        //protected void txtboton_Click(object sender, EventArgs e)
        //{
        //    Bio.Portal.Server.Common.Database.BpIdentity oBpIdentity;
        //    int iErr;
        //    int res = Bio.Core.Constant.Errors.IERR_OK;
        //    string msg;
        //    string xml = PasarXMLaString("c:\\Biometrika\\vero.xml");
        //    BioPortal.Server.Api.XmlParamIn oparamin = Bio.Core.Utils.SerializeHelper.DeserializeFromFile<BioPortal.Server.Api.XmlParamIn>("c:\\Biometrika\\vero.xml");
        //    //BioPortal.Server.Api.XmlParamIn oparamin = Bio.Core.Utils.SerializeHelper.DeserializeFromXml<BioPortal.Server.Api.XmlParamIn>(xml);
        //    iErr = Bio.Portal.Server.Common.Database.AdminBpIdentity.Retrieve(oparamin.PersonalData.Companyidenroll, oparamin.PersonalData.Typeid, oparamin.PersonalData.Valueid,
        //                                       out oBpIdentity, out msg);
        //    res = Bio.Portal.Server.Common.Database.AdminBpIdentity.MergeIdentity(oparamin.PersonalData, oBpIdentity, out oBpIdentity, out msg);
        //}
        /// <summary>
        /// Pasa un archivo XML a String.
        /// Utilizado para realizar la deserialización del archivo.
        /// </summary>
        /// <param name="ruta">define la ruta absoluta donde se encuentra el archivo a deserializar</param>
        /// <returns>retorna un string con la estructura del XML.</returns>
        public static String PasarXMLaString(string ruta)
        {

            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(ruta);
            }
            catch (XmlException e)
            {
                Console.WriteLine(e.Message);
            }

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xmlDoc.WriteTo(xw);
            return sw.ToString();
        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            //log.Debug("Ingreso a Login1_LoggedIn :" + Login1.UserName);
            ////Tomamos el nombre de usuario y dominio y lo dejamos en un arreglo.
            //String[] usuario = Login1.UserName.Split('@');

            ////usuario[0]=username / usuario[1]=dominio
            ////Buscamos los datos de la empresa.
            //string msgErr;
            //CompanyId = DatabaseHelper.Retrieve(usuario[1], out msgErr).Id;
            //if (msgErr.Equals("S/C"))
            //{
            //    Username = usuario[0];
            //    Domain = usuario[1];
            //    UserNameFull = Login1.UserName;
            //    log.Debug("Usuario logeado correctamente :" + Username);
            //    Response.Redirect("Welcome.aspx");
            //}
            //else
            //{
            //    log.Error(msgErr);
            //    //redireccionamos a página de error.
            //}
        }
    }
}