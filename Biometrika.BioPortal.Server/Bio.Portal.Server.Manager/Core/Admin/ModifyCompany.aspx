﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="ModifyCompany.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Admin.ModifyCompany" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td colspan="3" class="titulos0">
            <asp:Label runat="server" ID="lbltitulo"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="">
            <asp:ObjectDataSource ID="ObjectDataSourceCompany" runat="server" 
                onselecting="ObjectDataSourceCompany_Selecting" SelectMethod="Retrieve" 
                TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper" 
                InsertMethod="Create" oninserted="ObjectDataSourceCompany_Inserted" 
                UpdateMethod="Modify" onupdating="ObjectDataSourceCompany_Updating" 
                DeleteMethod="Delete" onupdated="ObjectDataSourceCompany_Updated" 
                ondeleted="ObjectDataSourceCompany_Deleted" 
                ondeleting="ObjectDataSourceCompany_Deleting" 
                oninserting="ObjectDataSourceCompany_Inserting">
                <DeleteParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="rut" Type="String" />
                    <asp:Parameter Name="address" Type="String" />
                    <asp:Parameter Name="name" Type="String" />
                    <asp:Parameter Name="phone" Type="String" />
                    <asp:Parameter Name="domain" Type="String" />
                    <asp:Parameter Name="phone2" Type="String" />
                    <asp:Parameter Name="fax" Type="String" />
                    <asp:Parameter Name="contactname" Type="String" />
                    <asp:Parameter Name="status" Type="Int32" />
                </InsertParameters>
                <SelectParameters>
                    <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" 
                        Type="Int32" />
                    <asp:Parameter Direction="Output" Name="msgerr" Type="String" />
                </SelectParameters>               
                <UpdateParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                    <asp:Parameter Name="rut" Type="String" />
                    <asp:Parameter Name="address" Type="String" />
                    <asp:Parameter Name="name" Type="String" />
                    <asp:Parameter Name="phone" Type="String" />
                    <asp:Parameter Name="domain" Type="String" />
                    <asp:Parameter Name="phone2" Type="String" DefaultValue="" />
                    <asp:Parameter Name="fax" Type="String" DefaultValue="" />
                    <asp:Parameter Name="contactname" Type="String" DefaultValue="" />
                    <asp:Parameter Name="status" Type="Int32" />
                </UpdateParameters>
               
            </asp:ObjectDataSource>
            <asp:Label ID="lblmsg" runat="server" Visible="False"></asp:Label>
            <br />
            <asp:DetailsView ID="DetailsViewCompany" SkinID="DetailsViewIngreso1" 
                runat="server" AutoGenerateRows="False" 
                DataSourceID="ObjectDataSourceCompany" EnableModelValidation="True" 
                Height="50px" Width="254px" onitemcommand="DetailsViewCompany_ItemCommand" 
                ondatabound="DetailsViewCompany_DataBound" DefaultMode="Insert" 
                onprerender="DetailsViewCompany_PreRender" 
                onpageindexchanging="DetailsViewCompany_PageIndexChanging">                
                <Fields>
                    <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" 
                        Visible="False" />
                    <asp:TemplateField HeaderText="Rut" SortExpression="Rut">
                        <EditItemTemplate>
                            <asp:TextBox ID="rut" runat="server" MaxLength="20" Text='<%# Bind("Rut") %>' OnKeyPress="return  onlyNumbers(event)"  ></asp:TextBox>(*)(sin puntos ni dígito verificador)                            
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="rut" runat="server" MaxLength="20" Text='<%# Bind("Rut") %>' OnKeyPress="return  onlyNumbers(event)" ></asp:TextBox>(*)(sin puntos ni dígito verificador)                            
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Rut") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nombre" SortExpression="Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="name" runat="server" MaxLength="100" Width="250" Text='<%# Bind("Name") %>'></asp:TextBox>(*)                           
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="name" runat="server" MaxLength="100" Width="250" Text='<%# Bind("Name") %>' ></asp:TextBox>(*)                            
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Width="250" Text='<%# Bind("Name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Dominio" SortExpression="Domain">
                        <EditItemTemplate>
                            <asp:TextBox ID="domain" MaxLength="15" runat="server" Width="250" Text='<%# Bind("Domain") %>'></asp:TextBox>(*)                            
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="domain" MaxLength="15" runat="server" Width="250" Text='<%# Bind("Domain") %>'></asp:TextBox>(*)                           
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Width="250" Text='<%# Bind("Domain") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Dirección" SortExpression="Address">
                        <EditItemTemplate>
                            <asp:TextBox ID="address" MaxLength="80" Width="250" runat="server" Text='<%# Bind("Address") %>'></asp:TextBox>(*)                            
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="address" MaxLength="80" Width="250" runat="server" Text='<%# Bind("Address") %>'></asp:TextBox>(*)                            
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Width="250" Text='<%# Bind("Address") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Teléfono" SortExpression="Phone">
                        <EditItemTemplate>
                            <asp:TextBox ID="phone" MaxLength="20" runat="server" Text='<%# Bind("Phone") %>'></asp:TextBox>(*)                           
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="phone"  MaxLength="20" runat="server" Text='<%# Bind("Phone") %>'></asp:TextBox>(*)                           
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("Phone") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Teléfono 2" SortExpression="Phone2">
                        <EditItemTemplate>
                            <asp:TextBox ID="phone2" runat="server" Text='<%# Bind("Phone2") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="phone2" runat="server" Text='<%# Bind("Phone2") %>'></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("Phone2") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fax" SortExpression="Fax">
                        <EditItemTemplate>
                            <asp:TextBox ID="fax" runat="server" Text='<%# Bind("Fax") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="fax" runat="server" Text='<%# Bind("Fax") %>'></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("Fax") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nombre contacto" SortExpression="Contactname">
                        <EditItemTemplate>
                            <asp:TextBox ID="contactname" runat="server" Width="250" Text='<%# Bind("Contactname") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="contactname" runat="server" Width="250" Text='<%# Bind("Contactname") %>'></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label9" runat="server" Width="250" Text='<%# Bind("Contactname") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Estado" SortExpression="Status">
                        <EditItemTemplate>
                            <asp:DropDownList runat="server" ID="status" SelectedIndex='<%# FormatSelectedIndex((int)Eval("Status")) %>'>
                                <asp:ListItem Value="0">Activa</asp:ListItem>
                                <asp:ListItem Value="1">Inactiva</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                             <asp:DropDownList runat="server" ID="status">
                                <asp:ListItem Value="0" Selected="True">Activa</asp:ListItem>
                                <asp:ListItem Value="1">Inactiva</asp:ListItem>
                            </asp:DropDownList>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" Visible="false" runat="server" Text='<%# FormatStatus((int)Eval("Status")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>     
                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:Button runat="server" ID="cmdModificar" Text="Modificar" SkinID="BottonAccion2" BorderStyle="None" 
                                OnClientClick="javascript:return ValidarFormularioCompanyIngreso();" 
                                onclick="cmdModificar_Click"  />
                            <asp:Button runat="server" ID="cmdCancelar" Text="Cancelar" SkinID="BottonAccion2" BorderStyle="None" 
                            onclick="cmdCancelar_Click" />
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:Button runat="server" ID="cmdInsertar" Text="Grabar" SkinID="BottonAccion2" BorderStyle="None" 
                             OnClientClick="javascript:return ValidarFormularioCompanyIngreso();" 
                                onclick="cmdInsertar_Click" />                            
                             <asp:Button runat="server" ID="cmdCancelar" Text="Cancelar" SkinID="BottonAccion2" BorderStyle="None" 
                             onclick="cmdCancelar_Click" />
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Button runat="server" ID="cmdEliminar" Text="Eliminar" SkinID="BottonAccion2" BorderStyle="None"  
                                onclick="cmdEliminar_Click" OnClientClick="javascript:return ValidarEliminar('Empresa');" />
                            <asp:Button runat="server" ID="cmdCancelar" Text="Cancelar" SkinID="BottonAccion2" BorderStyle="None" 
                            onclick="cmdCancelar_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                   
                </Fields>
            </asp:DetailsView>
            
        </td>
    </tr>    
</table>
</asp:Content>
