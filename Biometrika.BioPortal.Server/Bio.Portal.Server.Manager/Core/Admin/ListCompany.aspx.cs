﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using Bio.Portal.Server.Common.Common;
using Bio.Portal.Server.Manager.Core.libs;
using System.Web.UI.HtmlControls;
using System.Text;

namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class ListCompany : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ListCompany));
        /// <summary>
        /// Método load, carga los javascript en memorias necesario para validar los campos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/menuVertical.js"));
                this.Page.Header.Controls.Add(myJs);
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 1)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);

                ////Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);

                log.Debug("Se ingresa a visualizar las empresas usuario:" + UserNameFull);
                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                Page.ClientScript.RegisterClientScriptInclude("JQUERY", ResolveClientUrl("../../Scripts/menuVertical.js"));

                txtRut.Attributes.Add("onkeypress", "javascript:return onlyNumbers(event)");
                txtRazonSocial.Attributes.Add("onkeypress", "javascript:return RestrictChar()");
                RenderJSArrayWithCliendIds(txtRut, txtRazonSocial, DropDownListEstado, indice);

                if (Session["mensaje"] != null)
                {
                    mensaje.Text = Session["mensaje"].ToString();
                    mensaje.Visible = true;
                    Session["mensaje"] = null;
                }
                else
                    mensaje.Visible = false;
            }
            catch (Exception exe)
            {

                log.Error("Se ha producido un error en ListCompany.aspx en evento Load" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ListCompany.Aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }
        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }
        protected void GridViewCompany_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    //int index = Convert.ToInt32(e.CommandArgument);
                    //GridViewRow selectedRow = GridViewCompany.Rows[index];
                    //TableCell company = selectedRow.Cells[0];
                    string id = indice.Value; //company.Text;
                    log.Info("El usuario solicita modificar empresa : " + Username);
                    Response.Redirect("ModifyCompany.aspx?oper=mod&id=" + id);

                }
                else if (e.CommandName == "Delete")
                {
                    //int index = Convert.ToInt32(e.CommandArgument);
                    //GridViewRow selectedRow = GridViewCompany.Rows[index];
                    //TableCell company = selectedRow.Cells[0];
                    string id = indice.Value; //company.Text;
                    log.Info("El usuario solicita dar de baja la empresa : " + Username);
                    Response.Redirect("ModifyCompany.aspx?oper=eli&id=" + id);

                }
            }
            catch (Exception exe)
            {
                if (exe.GetType().FullName != "System.Threading.ThreadAbortException")
                {
                    log.Error("Se ha producido un error en ListCompany.aspx al momento de ejecutar una acción sobre la grilla" , exe);
                    Session["mensajeerror"] = "Se ha producido un error en ListCompany.aspx al momento de ejecutar una acción sobre la grilla";
                    Session["exception"] = exe;
                    Response.Redirect("~/Error.aspx");
                }
            }
        }

        protected void cmdNuevo_Click(object sender, EventArgs e)
        {
            log.Debug("El usuario a presionado el botón nuevo:" + Username);
            Response.Redirect("ModifyCompany.aspx");
        }

        protected void ObjectDataSourceCompany_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            try
            {
                e.InputParameters["rut"] = this.txtRut.Text;
                e.InputParameters["razonsocial"] = this.txtRazonSocial.Text;
                e.InputParameters["estado"] = Convert.ToInt32(this.DropDownListEstado.SelectedItem.Value);
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ListCompany.aspx al momento de seleccionar una compañía" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ListCompany.aspx al momento de seleccionar una compañía";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
           
        }

        protected void cmdBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                ObjectDataSourceCompany.Select();
                GridViewCompany.DataBind();
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error al momento de buscar una compañía" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ListCompany.aspx al momento de seleccionar una compañía";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }
    }
}