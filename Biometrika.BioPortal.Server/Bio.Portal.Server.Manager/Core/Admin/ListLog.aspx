﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="ListLog.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Admin.ListLog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<table cellpadding="0" cellspacing="0" width="80%" border="0" align="center">
    <tr>
        <td colspan="4" class="titulos0">Filtro para Visualizar Log de Operaciones</td>
    </tr>
     <tr>
        <td class="tdBottom">Nivel</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:DropDownList runat="server" ID="DropDownListLevel">
                <asp:ListItem Value="Todos">Todos</asp:ListItem>
                <asp:ListItem Value="DEBUG">DEBUG</asp:ListItem>
                <asp:ListItem Value="INFO">INFO</asp:ListItem>
                <asp:ListItem Value="WARN">WARN</asp:ListItem>
                <asp:ListItem Value="ERROR">ERROR</asp:ListItem>
                <asp:ListItem Value="FATAL">FATAL</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td class="tdBottom"></td>
    </tr>
        <tr>
        <td class="tdBottom">Fecha Desde<br />[Formato (dd/mm/yyyy)]</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox ID="txtfecha" runat="server" onfocus="javascript:vDateType='3'" onkeyup="javascript:DateFormat(document.getElementById(MyClientID[0]),document.getElementById(MyClientID[0]).value,event,false,'3')" onBlur="javascript:DateFormat(document.getElementById(MyClientID[0]),document.getElementById(MyClientID[0]).value,event,true,'3');ValidarFecha(document.getElementById(MyClientID[0]),document.getElementById(MyClientID[0]).value)"></asp:TextBox>
            <asp:Button runat="server" ID="cmdCalendario"  SkinID="BottonCal"
                UseSubmitBehavior="False" />
        </td>
        <td class="tdBottom"></td>
    </tr>
     <tr>
        <td class="tdBottom">Fecha Hasta<br />[Formato (dd/mm/yyyy)]</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox ID="txtfechah" runat="server" onfocus="javascript:vDateType='3'" onkeyup="javascript:DateFormat(document.getElementById(MyClientID[1]),document.getElementById(MyClientID[1]).value,event,false,'3')" onBlur="javascript:DateFormat(document.getElementById(MyClientID[1]),document.getElementById(MyClientID[1]).value,event,true,'3');ValidarFecha(document.getElementById(MyClientID[0]),document.getElementById(MyClientID[0]).value)"></asp:TextBox>
            <asp:Button runat="server" ID="cmdCalendarioH"  SkinID="BottonCal"
                UseSubmitBehavior="False" />
        </td>
         <td class="tdBottom"></td>
    </tr>
     <tr>
        <td colspan="4">          
            <asp:HiddenField runat="server" ID="fechadesdeaux" />
            <asp:HiddenField runat="server" ID="fechahastaaux" />   
            <asp:Button runat="server" ID="cmdVerLog" value="Buscar" SkinID="BotonAccion" 
                 Text="Buscar" BorderStyle="None" OnClick="cmdVerLog_Click" OnClientClick="javascript:return ValidarFechas(document.getElementById(MyClientID[0]),document.getElementById(MyClientID[1]),document.getElementById(MyClientID[2]),document.getElementById(MyClientID[3]));" />
        </td>
    </tr>
</table>
</asp:Content>
