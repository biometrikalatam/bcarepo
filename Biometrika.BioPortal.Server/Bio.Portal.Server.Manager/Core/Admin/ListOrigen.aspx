﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="ListOrigen.aspx.cs" EnableEventValidation="false" Inherits="Bio.Portal.Server.Manager.Core.Admin.ListOrigen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td colspan="3" class="titulos0">Listado de Origenes</td>
    </tr>
    <tr>
        <td>           
            <asp:HiddenField runat="server" ID="indice" Value="" />   
            <asp:Label runat="server" ID="lblMensaje" Visible="false"></asp:Label> 
        </td>    
    </tr>
    <tr>
        <td class="">
            <asp:Button runat="server" BorderStyle="None" ID="cmdAgregar" SkinID="BotonAccion" Text="Nuevo Origen" 
                onclick="cmdAgregar_Click" />
        </td>
    </tr>
    <tr>
        <td class="">
            <asp:ObjectDataSource ID="ObjectDataSourceOrigen2" runat="server" 
                SelectMethod="RetrieveAllOrigenes" 
                TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper" 
                onselected="ObjectDataSourceOrigen2_Selected">
                <SelectParameters>
                    <asp:Parameter Direction="Output" Name="result" Type="Object" />
                    <asp:Parameter Direction="Output" Name="msgErr" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:GridView ID="GridViewOrigen" runat="server" AutoGenerateColumns="False"  
                onrowcommand="GridViewOrigen_RowCommand" onrowediting="GridViewOrigen_RowEditing"
                OnPageIndexChanging="GridViewOrigen_PageIndexChanging" 
                 Width="100%" 
                EnableModelValidation="True" SkinID="GridView1">
               <Columns>
                    <asp:TemplateField>
                      <ItemTemplate>
                        <asp:ImageButton runat="server" ID="EditButton" 
                          CommandName="Edit"  
                          ImageUrl="~/Images/origen_.gif" OnClientClick='<%# ImagenScript((int)Eval("Id")) %>' />                          
                      </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center"  />
                    </asp:TemplateField>                
                     <asp:TemplateField>
                      <ItemTemplate>
                        <asp:ImageButton runat="server" ID="DeleteButton" 
                          CommandName="Delete"  
                          ImageUrl="~/Images/origen_elimina.gif" OnClientClick='<%# ImagenScript((int)Eval("Id")) %>' />                          
                      </ItemTemplate>
                         <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>                       
                   
                    <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
                    <asp:BoundField DataField="Description" HeaderText="Descripción" SortExpression="Description" />
               </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
</asp:Content>
