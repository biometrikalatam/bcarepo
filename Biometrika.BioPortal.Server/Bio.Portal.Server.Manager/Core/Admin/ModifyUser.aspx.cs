﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Bio.Portal.Server.Common.Common;
using log4net;
using Bio.Portal.Server.Common.BioMembership;
using System.Web.Security;

namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class ModifyUser : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ModifyCompany));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string oper = Request["oper"] ?? String.Empty;
                string id = Request["id"];

                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/menuVertical.js"));
                this.Page.Header.Controls.Add(myJs);
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);
                //Verificamos accesos
                if (User.IsInRole("Super Administrador"))
                    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                else if (User.IsInRole("Administrador"))
                    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                if (User.IsInRole("Enrolador"))
                    BioPortalServer.VisualizarMenuEnrolador(Master);
                if (User.IsInRole("Verificador"))
                    BioPortalServer.VisualizarMenuVerificar(Master);

                if (EsNumerico(id) == true)
                {
                    if (oper.Equals("mod"))
                    {
                        DetailsViewUser.DefaultMode = DetailsViewMode.Edit;
                        lbltitulo.Text = "Modificación Usuario";
                        lbltitulo.Visible = true;
                        //CommandField commandField = DetailsViewUser.Fields[DetailsViewUser.Rows.Count - 1] as CommandField;
                        //if (commandField != null)
                        //{
                        //    commandField.ShowCancelButton = true;
                        //    commandField.ShowDeleteButton = false;
                        //    commandField.ShowEditButton = true;
                        //    commandField.ShowInsertButton = false;
                        //}

                    }
                    else if (oper.Equals("eli"))
                    {
                        DetailsViewUser.DefaultMode = DetailsViewMode.ReadOnly;
                        lbltitulo.Text = "Eliminación Usuario";
                        lbltitulo.Visible = true;
                        //CommandField commandField = DetailsViewUser.Fields[DetailsViewUser.Rows.Count - 1] as CommandField;

                        //if (commandField != null)
                        //{
                        //    commandField.ShowCancelButton = true;
                        //    commandField.ShowDeleteButton = true;
                        //    commandField.ShowEditButton = false;
                        //    commandField.ShowInsertButton = false;
                        //}


                    }
                    else
                    {
                        DetailsViewUser.DefaultMode = DetailsViewMode.Insert;

                        //CommandField commandField = DetailsViewUser.Fields[DetailsViewUser.Rows.Count - 1] as CommandField;

                        //if (commandField != null)
                        //{
                        //    commandField.ShowCancelButton = true;
                        //    commandField.ShowDeleteButton = false;
                        //    commandField.ShowEditButton = false;
                        //    commandField.ShowInsertButton = true;
                        //}
                    }
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyUser.aspx la modificación de un usuario" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyUser.aspx la modificación de un usuario";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceUser_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            try
            {
                e.InputParameters["userid"] = Convert.ToInt32(Request["id"]);
                //e.InputParameters["id"] = Convert.ToInt32(Request["id"]);
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyOrigin.aspx la selección de un usuario" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyOrigin.aspx la selección de un usuario";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }


        protected void ObjectDataSourceUser_Selected(object sender, EventArgs e)
        {
            object o = sender;
            o = e;
        }


        protected void ObjectDataSourceUser_DataBound(object sender, EventArgs e)
        {
            DropDownList ddl = DetailsViewUser.FindControl("IsApproved") as DropDownList;
            if (ddl != null)
            {
                ddl.SelectedIndex = 1;
            }

        }


        protected void ObjectDataSourceUser_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                //No hay error al grabar.
                if (e.ReturnValue.ToString() == "0")
                    Response.Redirect("ListUsers.aspx");
                else
                {
                    lblmsg.Text = "Se ha producido un error al grabar los datos";
                    lblmsg.Visible = true;
                }
            }
            else
            {
               
                log.Error("Se ha producido un error en ModifyUser.aspx la modificación de un usuario" + e.ReturnValue);
                Session["mensajeerror"] = "Se ha producido un error en ModifyUser.aspx la modificación de un usuario";
                Session["exception"] = e.ReturnValue;
                Response.Redirect("~/Error.aspx");
            
            }

        }

        protected void ObjectDataSourceUser_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            try
            {

                e.InputParameters["id"] = Convert.ToInt32(Request["id"]);
                e.InputParameters["Email"] = ((TextBox)DetailsViewUser.FindControl("Email")).Text;
                e.InputParameters["Name"] = ((TextBox)DetailsViewUser.FindControl("Name")).Text;
                e.InputParameters["Surname"] = ((TextBox)DetailsViewUser.FindControl("SurName")).Text;
                e.InputParameters["Phone"] = ((TextBox)DetailsViewUser.FindControl("Phone")).Text;
                e.InputParameters["PostalCode"] = ((TextBox)DetailsViewUser.FindControl("PostalCode")).Text;
                e.InputParameters["IsApproved"] = ((DropDownList)DetailsViewUser.FindControl("IsApproved")).SelectedItem.Value;
                //e.InputParameters["IsApproved"] = true; // ((CheckBox)DetailsViewUser.FindControl("Activo")).Checked;
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyUser.aspx al modificar un usuario" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyUser.aspx al modificar un usuario";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceUser_Deleting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            try
            {

                CustomRoleProvider roleProvider = (CustomRoleProvider)Roles.Providers["RoleCustomProviderFull"];

                if (roleProvider == null)
                {
                }
                e.InputParameters["roles"] = roleProvider;
                e.InputParameters["id"] = Convert.ToInt32(Request["id"]);
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyUser.aspx la eliminación de un usuario:" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyUser.aspx la eliminación de un usuario";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceUser_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                //No hay error al grabar.
                if (e.ReturnValue.ToString() == "0")
                    Response.Redirect("ListUsers.aspx");
                else
                {
                    lblmsg.Text = "Se ha producido un error al tratar de eliminar al usuario";
                    lblmsg.Visible = true;
                }
            }
            else
            {
                log.Error("Se ha producido un error en ModifyUser.aspx la eliminación de un usuario de un usuario" + e.ReturnValue);
                Session["mensajeerror"] = "Se ha producido un error en ModifyUser.aspx la eliminación de un usuario";
                Session["exception"] = e.ReturnValue;
                Response.Redirect("~/Error.aspx");
            }

        }

        protected void cmdInsertar_Click(object sender, EventArgs e)
        {
            ObjectDataSourceUser.Insert();
        }

        protected void cmdEliminar_Click(object sender, EventArgs e)
        {
            ObjectDataSourceUser.Delete();
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Core/Admin/ListUsers.aspx");
        }

        protected void cmdModificar_Click(object sender, EventArgs e)
        {
            ObjectDataSourceUser.Update();
        }
    }
}