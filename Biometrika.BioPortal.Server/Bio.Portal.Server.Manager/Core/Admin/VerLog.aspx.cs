﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using log4net;
using Bio.Portal.Server.Manager.Core.libs;

namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class VerLog : System.Web.UI.Page
    {
        private ArrayList criterio;
        private static readonly ILog log = LogManager.GetLogger(typeof(VerLog));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                criterio = (ArrayList)Session["params"];

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 3)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);
                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en VerLog.aspx la selección del log" , exe);
                Session["mensajeerror"] = "Se ha producido un error en VerLog.aspx la selección del log";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceLog_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            try
            {
                if (criterio != null)
                {
                    e.InputParameters["level"] = criterio[0].ToString();
                    e.InputParameters["fechad"] = criterio[1].ToString();
                    e.InputParameters["fechah"] = criterio[2].ToString();
                }
                else
                {
                    log.Info("No hay criterios para realizar el filtro: VerLog.aspx");
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en VerLog.aspx al seleccionar los logs" , exe);
                Session["mensajeerror"] = "Se ha producido un error en VerLog.aspx al seleccionar los logs";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }

        }

        protected void cmdVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Core/Admin/ListLog.aspx", true);
        }
    }
}