﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="ListUsers.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Admin.ListUsers" %>
<%@ Import Namespace="Bio.Portal.Server.Common.Entities.Database" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
        <td colspan="4" class="titulos0">
             Listado de Usuarios
            <asp:Label ID="mensaje" runat="server" text="" Visible="false"></asp:Label>
            <asp:HiddenField runat="server" ID="indice" Value="" />
            <asp:HiddenField runat="server" ID="userid" Value="" />
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Nombre Usuario</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="username" MaxLength="50"></asp:TextBox>
            <asp:HiddenField runat="server" ID="hiddenfiltro" />
        </td>
        <td class="tdBottom">&nbsp;</td>
    </tr>
    <asp:PlaceHolder ID="porempresa" runat="server">
    <tr>
        <td class="tdBottom">Empresa</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            
                <asp:DropDownList runat="server" ID="DropDownListCompany" 
                DataSourceID="ObjectDataSourceCompany" DataTextField="Name" 
                 DataValueField="Id" ondatabound="DropDownListCompany_DataBound"></asp:DropDownList>
        </td>
        <td class="tdBottom">&nbsp;</td>
    </tr>
    </asp:PlaceHolder>
    
    <tr>
        <td colspan="4" align="right">
             
            <asp:Button runat="server" ID="cmdBuscar" Text="Buscar" BorderStyle="None" 
                OnClientClick="javascript:return FiltrarBusquedaUsuarios();" 
                onclick="cmdBuscar_Click" SkinID="BotonAccion"  />

        </td>
    </tr>
    <tr>
        <td colspan="4">
             <asp:ObjectDataSource ID="ObjectDataSourceCompany" runat="server" 
                SelectMethod="RetrieveAllCompanyActive" 
                TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper">
            </asp:ObjectDataSource>
              <asp:Button ID="Button1" runat="server" onclick="Button1_Click" BorderStyle="None" 
                Text="Crear Usuario" SkinID="BotonAccion" />
        </td>    
    </tr>
    <tr>
        <td colspan="4">
            <asp:ObjectDataSource ID="ObjectDataSourceUser" runat="server" 
                onselecting="ObjectDataSourceUser_Selecting" SelectMethod="RetrieveAllUser" 
                TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper">
                <SelectParameters>
                    <asp:Parameter DefaultValue="0" Name="companyid" Type="Int32" />
                    <asp:Parameter DefaultValue="" Name="username" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
            <asp:GridView ID="GridViewUsers" runat="server" 
                AutoGenerateColumns="False" DataKeyNames="Id" 
                DataSourceID="ObjectDataSourceUser" EnableModelValidation="True" 
                onrowcommand="GridViewUsers_RowCommand" Width="100%" SkinID="GridView1">
                <Columns>
                   <asp:TemplateField>
                      <ItemTemplate>
                        <asp:ImageButton runat="server" ID="EditButton" 
                          CommandName="Edit" ToolTip="Modificar Usuarios" ImageAlign="AbsMiddle" 
                          ImageUrl="~/Images/usu_edit.gif" OnClientClick='<%# ImagenEditarCompany((int)Eval("Id")) %>' />                          
                      </ItemTemplate>
                    </asp:TemplateField>                
                    <asp:TemplateField>
                      <ItemTemplate>
                        <asp:ImageButton runat="server" ID="DeleteButton" 
                          CommandName="Delete" ToolTip="Dar de Baja Usuarios"  ImageAlign="AbsMiddle"
                          ImageUrl="~/Images/usu_elimina.gif" OnClientClick='<%# ImagenEliminarCompany((int)Eval("Id")) %>'  />                          
                      </ItemTemplate>
                    </asp:TemplateField>      
                    <asp:TemplateField>
                      <ItemTemplate>
                        <asp:ImageButton runat="server" ID="ChangePwdButton" 
                          CommandName="ChangePassword" ToolTip="Cambiar Password del Usuario"  ImageAlign="Middle"
                          ImageUrl="~/Images/usu_edit_psw.gif" OnClientClick='<%# ImagenCambiarPassword((int)Eval("Id"),(String)Eval("UserName")) %>'  />                          
                      </ItemTemplate>
                   </asp:TemplateField>
                    <asp:TemplateField>
                      <ItemTemplate>
                        <asp:ImageButton runat="server" ID="ChangeRoleButton" 
                          CommandName="ChangeRole" ToolTip="Cambiar Roles del Usuario"  ImageAlign="Middle"
                          ImageUrl="~/Images/usu_edit_roles.gif" OnClientClick='<%# ImagenCambiarRoles((int)Eval("Id"),(String)Eval("UserName")) %>'  />                          
                      </ItemTemplate>
                   </asp:TemplateField>
                    <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" 
                        SortExpression="Id" />
                    <asp:BoundField DataField="Username" HeaderText="Username" 
                        SortExpression="Username" />
                    <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                    <asp:BoundField DataField="CreationDate" HeaderText="Alta" 
                        SortExpression="CreationDate" />
                     <asp:TemplateField HeaderText="Compañía" SortExpression="CompanyId">                       
                        <ItemTemplate>
                             <asp:Label ID="LabelCompany" runat="server" Text='<%# ((BpCompany)Eval("CompanyId")).Name %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>    
                    <asp:TemplateField HeaderText="Estado" SortExpression="IsApproved">                       
                        <ItemTemplate>
                             <asp:Label ID="LabelEstado" runat="server" Text='<%# FormatIsApproved((bool)Eval("IsApproved")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>  
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
</asp:Content>
