﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="ModifyRole.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Admin.ModifyRole" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<table border="0" cellpadding="0" cellspacing="0" width="80%" align="center">
   <tr>
    <td colspan="2" class="titulos0">Editar Roles de Usuario</td>
    </tr>
    <tr>
        <td colspan="1" class="tdBottom">
            Usuario : 
        </td>
        <td colspan="1" class="tdBottom">
            <asp:Label runat="server" ID="LabUser" Value="" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="tdBottom">
            <asp:CheckBoxList ID="chlrol" runat="server"    >
            </asp:CheckBoxList>
            <asp:HiddenField runat="server" ID="hiddenTotal" Value="0" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button runat="server" ID="cmdGuardar" Text="Modificar" SkinID="BotonAccion" onclick="cmdGuardar_Click" BorderStyle="None" />
            <asp:Button runat="server" ID="cmdVolver" Text="Volver" onclick="cmdVolver_Click" SkinID="BotonAccion" BorderStyle="None" />
            <asp:Label ID="labError" runat="server" Font-Names="Arial" ForeColor="Red" 
                Text="Debe seleccionar al menos un rol..." Visible="False"></asp:Label>
        </td>
    </tr>
</table>
</asp:Content>
