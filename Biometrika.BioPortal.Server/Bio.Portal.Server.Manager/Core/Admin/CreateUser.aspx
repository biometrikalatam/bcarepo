﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="CreateUser.aspx.cs" 
Inherits="Bio.Portal.Server.Manager.Core.Admin.CreateUser" %>


<%@ Register assembly="Bio.Portal.Server.Common.BioMembership" namespace="Bio.Portal.Server.Common.BioMembership" tagprefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<table border="0" width="100%" cellpadding="0" cellspacing="0" align="center">
<tr><td align="center">
    <table border="0" width="80%" cellpadding="0" cellspacing="0" align="center">
    <tr><td align="left">
    <cc1:CreateUserExWizard ID="CreateUserExWizard1" runat="server" Width="80%" 
		OnCreateUserEx="CreateUserExWizard1_CreateUserEx"  
		LoginCreatedUser="False" ContinueDestinationPageUrl="~/Core/Admin/ListUsers.aspx" 
        oncreateduser="CreateUserExWizard1_CreatedUser" 
        CancelButtonText="Cancelar" CreateUserButtonText="Crear Usuario" 
        DuplicateEmailErrorMessage="El mail ingresado ya esta en uso por otro usuario. Favor cambie el mail." 
        DuplicateUserNameErrorMessage="El usuario ingresado ya esta en uso por otro usuario de la misma empresa. Ingrese otro usuario." 
        FinishCompleteButtonText="Finalizar" 
        InvalidAnswerErrorMessage="Favor ingrese una respuesta diferente" 
        InvalidEmailErrorMessage="Debe ingresar un e-mail válido" 
        InvalidPasswordErrorMessage="El largo mínimo del password es: {0}. es necesario ingresar caracteres: {1}." 
        InvalidQuestionErrorMessage="Favor ingresar una pregunta de seguridad distinta." 
        UnknownErrorMessage="La cuenta no pudo ser creado. Reintente.">
		<CreateUserButtonStyle CssClass="inBotonEnviar" />
        <CancelButtonStyle CssClass="inBotonEnviar" />
        <FinishCompleteButtonStyle CssClass="inBotonEnviar" />        
		<WizardSteps>
			<asp:CreateUserWizardStep runat="server">
				<ContentTemplate>
					<table border="0" width="100%" cellpadding="0" cellspacing="0" align="center">
						<tbody>
							<tr>
								<td align="left" colspan="2" class="titulos0">
									Registro de Usuarios
								</td>
							</tr>
							<tr>
								<td align="left" colspan="2" class="tdBottom">
									<strong>Complete los siguientes datos para crear un usuario</strong>
								</td>
							</tr>
							<tr>
								<td class="tdBottom">
									<span class="red">*</span><asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Nombre Usuario:</asp:Label>
								</td>
								<td class="tdBottom">
									<asp:TextBox ID="UserName" runat="server" CssClass="txtEstilo2" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
									<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ValidationGroup="CreateUserExWizard1" ControlToValidate="UserName" ErrorMessage="Debe ingresar un nombre de usuario." Display="Dynamic"></asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdBottom">
									<span class="red">*</span><asp:Label ID="PasswordLabel"  runat="server" AssociatedControlID="Password">Password:</asp:Label>
								</td>
								<td class="tdBottom">
									<asp:TextBox ID="Password" runat="server" CssClass="txtEstilo2" TextMode="Password" onkeypress="javascript:return RestrictChar()">
									</asp:TextBox>
									<asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ValidationGroup="CreateUserExWizard1" ControlToValidate="Password" ErrorMessage="Debe ingresar un password." Display="Dynamic"></asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdBottom">
									<span class="red">*</span><asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Repita Password:</asp:Label>
								</td>
								<td class="tdBottom">
									<asp:TextBox ID="ConfirmPassword" runat="server" CssClass="txtEstilo2" TextMode="Password" onkeypress="javascript:return RestrictChar()">
									</asp:TextBox>
									<asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ValidationGroup="CreateUserExWizard1" ControlToValidate="ConfirmPassword" ErrorMessage="Debe confirmar el password." Display="Dynamic"></asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdBottom">
									<span class="red">*</span><asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
								</td>
								<td class="tdBottom"><asp:TextBox ID="Email" runat="server" CssClass="txtEstilo2"></asp:TextBox>
									<asp:RequiredFieldValidator ID="EmailRequired" runat="server" ValidationGroup="CreateUserExWizard1" ControlToValidate="Email" ErrorMessage="E-mail es requerido." Display="Dynamic"></asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="EmailRegularExpressionValidator" runat="server" ValidationGroup="CreateUserExWizard1" ControlToValidate="Email" ErrorMessage="E-Mail no válido." Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdBottom">
									<span class="red">*</span><asp:Label ID="QuestionLabel" runat="server" AssociatedControlID="Question">Pregunta de Seguridad:</asp:Label>
								</td>
								<td class="tdBottom">
									<asp:TextBox ID="Question" runat="server" CssClass="txtEstilo2" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
									<asp:RequiredFieldValidator ID="QuestionRequired" runat="server" ValidationGroup="CreateUserExWizard1" ControlToValidate="Question" ErrorMessage="Debe ingresar una pregunta." Display="Dynamic"></asp:RequiredFieldValidator>
								</td>
							</tr>	
							<tr>
								<td class="tdBottom">
									<span class="red">*</span><asp:Label ID="PasswordAnswerLabel"  runat="server" AssociatedControlID="Answer">Respuestas:</asp:Label>
								</td>
								<td class="tdBottom">
									<asp:TextBox ID="Answer" CssClass="txtEstilo2"  runat="server" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
									<asp:RequiredFieldValidator ID="AnswerRequired" Visible="false" runat="server" ValidationGroup="CreateUserExWizard1" ControlToValidate="Answer" ErrorMessage="Debe ingresar una respuesta." Display="Dynamic"></asp:RequiredFieldValidator>
								</td>
							</tr>
                            <tr>
								<td align="left" colspan="4">&nbsp;
                                </td>
                            </tr>
							<tr>
								<td align="left" colspan="2" class="tdBottom">
									<strong>Seleccione la empresa a la cual pertenece el usuario</strong>
								</td>
							</tr>
                              <asp:PlaceHolder runat="server" ID="empresas" Visible="false">
							<tr>
								<td class="">
									<span class="red">*</span><asp:Label ID="CompanyLabel" runat="server" AssociatedControlID="Company">Empresa:</asp:Label>
								</td>
								<td class="">
									<asp:TextBox ID="Company" CssClass="txtEstilo2" Visible="false" runat="server"></asp:TextBox>
                                    <asp:DropDownList ID="DropDownListCompany" runat="server" 
                                         DataTextField="Name" DataValueField="Id">
                                        <asp:ListItem Value="-1" Selected="True">Seleccione una empresa</asp:ListItem>
                                    </asp:DropDownList>
									<asp:RequiredFieldValidator ID="CompanyRequired" Enabled="false" runat="server" ValidationGroup="CreateUserExWizard1" ControlToValidate="DropDownListCompany" ErrorMessage="Debe ingresar una compañía." InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
								</td>
							</tr>
                            </asp:PlaceHolder>
                            <tr>
								<td align="left" colspan="4">&nbsp;
                                </td>
                            </tr>
							<tr>
								<td align="left" colspan="2" class="tdBottom">
									<strong>Ingrese datos opcionales</strong>
								</td>
							</tr>
							<tr>
								<td class="tdBottom">
									<span class="red"><!--*--></span>
                                    <asp:Label ID="NameLabel" Visible="true" runat="server" AssociatedControlID="Name">Nombre:</asp:Label>
								</td>
								<td class="tdBottom">
									<asp:TextBox ID="Name" CssClass="txtEstilo2" Visible="true" runat="server" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdBottom">
									<span class="red"><!--*--></span>
                                    <asp:Label ID="SurnameLabel" Visible="true" runat="server" AssociatedControlID="Surname">Apellidos:</asp:Label>
								</td>
								<td class="tdBottom">
									<asp:TextBox ID="Surname" CssClass="txtEstilo2" Visible="true" runat="server" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdBottom">
                                    <asp:HiddenField runat="server" ID="HiddenIDCompany" Value="" />
									<asp:Label ID="PhoneLabel" Visible="true" runat="server"  AssociatedControlID="Phone">Teléfono:</asp:Label>
								</td>
								<td class="tdBottom">
									<asp:TextBox ID="Phone" CssClass="txtEstilo2" Visible="true" runat="server" onkeypress="javascript:return onlyNumbers()"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdBottom">
									<asp:Label ID="PostalCodeLabel" runat="server" Visible="true" AssociatedControlID="PostalCode">Código Postal:</asp:Label>
								</td>
								<td class="tdBottom">
									<asp:TextBox ID="PostalCode" CssClass="txtEstilo2" runat="server" Visible="true" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td align="left" colspan="2" class="tdBottom">
									&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2" class="tdBottom">
									<asp:CompareValidator ID="PasswordCompare" runat="server" ValidationGroup="CreateUserExWizard1" ControlToValidate="ConfirmPassword" ErrorMessage="El passsword y confirmación de password debe ser igual." Display="Dynamic" ControlToCompare="Password"></asp:CompareValidator>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td style="color: red" align="left" class="">
									<asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
								</td>
							</tr>
						</tbody>
					</table>
				</ContentTemplate>
				
			</asp:CreateUserWizardStep>
			<asp:WizardStep runat="server" ID="AssignUserToRoles" Title="Seleccione rol(es) del usuario" OnDeactivate="AssignUserToRoles_Deactivate" >
            <table cellpadding="0" cellspacing="0" border="0" width="80%" align="center">
                <tr>
                    <td class="titulos0">Asignar Roles</td>
                </tr>
                <tr>
                    <td class="tdBottom">
                         <asp:CheckBoxList ID="chlrol" runat="server">
                         </asp:CheckBoxList>
                    </td>
                </tr>
            </table>
              
            </asp:WizardStep>
			<asp:CompleteWizardStep runat="server">
				<ContentTemplate>
					<table border="0" cellpadding="0" cellspacing="0" width="80%" align="center">
						<tbody> 
                            <tr>
                                <td class="titulos0">Resultado del Registro</td>
                            </tr>
							<tr>
								<td align="center" class="">
									<p>
										La cuenta ha sido exitósamente creada.
									</p>
								</td>
							</tr>
							<tr>
								<td align="center" class="tdBottom">
									<asp:Button ID="ContinueButton" runat="server" CssClass="button" BorderStyle="None" 
                                        CausesValidation="False" CommandName="Continue" Text="Continue" 
                                        ValidationGroup="CreateUserExWizard1" SkinID="BottonAccion2"></asp:Button>
								</td>
							</tr>
						</tbody>
					</table>
				</ContentTemplate>
			</asp:CompleteWizardStep>
		</WizardSteps>		
    </cc1:CreateUserExWizard>
    
   </td></tr></table>
 </td></tr></table>
   
 </asp:Content>
