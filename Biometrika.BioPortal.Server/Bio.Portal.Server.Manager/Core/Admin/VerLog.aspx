﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="VerLog.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Admin.VerLog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <table cellpadding="0" cellspacing="0" width="100%" border="0" align="center"> 
        <tr>
            <td class="titulos0">Resultado de Log de Operaciones</td>
        </tr>
        <tr>
            <td align="right"> 
                <asp:Button runat="server" ID="cmdVolver" value="Volver" SkinID="BotonAccion" 
                    Text="Volver" onclick="cmdVolver_Click" BorderStyle="None" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ObjectDataSource ID="ObjectDataSourceLog" runat="server" 
                    onselecting="ObjectDataSourceLog_Selecting" SelectMethod="RetrieveLog" 
                    TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper">
                    <SelectParameters>
                        <asp:Parameter Name="level" Type="String" />
                        <asp:Parameter Name="fechad" Type="String" />
                        <asp:Parameter Name="fechah" Type="String" />
                        <asp:Parameter Direction="Output" Name="msg" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="GridViewLog" runat="server" SkinID="GridView1"  
                    DataSourceID="ObjectDataSourceLog" EnableModelValidation="True" Width="80%">
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
