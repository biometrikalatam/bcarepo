﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="ModifyUser.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Admin.ModifyUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<table border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td colspan="3" class="titulos0">
            <asp:Label runat="server" ID="lbltitulo"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="">
            <asp:Label ID="lblmsg" runat="server" Visible="False"></asp:Label>
            <br />
            <asp:ObjectDataSource ID="ObjectDataSourceUser" runat="server" 
                SelectMethod="RetrieveUser" 
                TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper" 
                onselecting="ObjectDataSourceUser_Selecting" UpdateMethod="ModifyUser" OnSelected="ObjectDataSourceUser_Selected" 
                onupdated="ObjectDataSourceUser_Updated" DeleteMethod="DeleteUser" 
                ondeleted="ObjectDataSourceUser_Deleted" onupdating="ObjectDataSourceUser_Updating"
                ondeleting="ObjectDataSourceUser_Deleting" OnDataBinding="ObjectDataSourceUser_DataBound">
                <DeleteParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                    <asp:Parameter Name="roles" Type="Object" />
                </DeleteParameters>
                  <SelectParameters>
                    <asp:QueryStringParameter DefaultValue="0" Name="userid" QueryStringField="id" 
                        Type="Int32" />
                    <asp:Parameter Direction="Output" Name="msgerr" Type="String" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                    <asp:Parameter Name="Email" Type="String" />            
                    <asp:Parameter Name="Name" Type="String" />            
                    <asp:Parameter Name="Surname" Type="String" />            
                    <asp:Parameter Name="Phone" Type="String" />            
                    <asp:Parameter Name="PostalCode" Type="String" />            
                    <asp:Parameter Name="IsApproved" Type="Boolean" />
                </UpdateParameters>
            </asp:ObjectDataSource>
             <asp:DetailsView ID="DetailsViewUser" runat="server" Height="50px" 
                AutoGenerateRows="False" DataKeyNames="Id" 
                DataSourceID="ObjectDataSourceUser" SkinID="DetailsViewIngreso1" 
                EnableModelValidation="True" Width="144px">        
                <Fields>
                    <asp:TemplateField HeaderText="*Email" SortExpression="Email">
                        <EditItemTemplate>
                            <asp:TextBox ID="Email" runat="server" Text='<%# Bind("Email") %>'>*</asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="reqemail" ControlToValidate="Email" ErrorMessage="Debe ingresar un mail"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="Email" runat="server" Text='<%# Bind("Email") %>'>*</asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="reqemail" ControlToValidate="Email" ErrorMessage="Debe ingresar un mail"></asp:RequiredFieldValidator>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nombre" SortExpression="Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="Name" runat="server" Text='<%# Bind("Name") %>' onkeypress="javascript:return RestrictChar('[-_0-9a-zA-Z]')"></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="Name" runat="server" Text='<%# Bind("Name") %>'>*</asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LabelName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Apellidos" SortExpression="SurName">
                        <EditItemTemplate>
                            <asp:TextBox ID="SurName" runat="server" Text='<%# Bind("Surname") %>' onkeypress="javascript:return RestrictChar('[-_0-9a-zA-Z]')"></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="SurName" runat="server" Text='<%# Bind("Surname") %>'>*</asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LabelSueName" runat="server" Text='<%# Bind("Surname") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Telefono" SortExpression="Phone">
                        <EditItemTemplate>
                            <asp:TextBox ID="Phone" runat="server" Text='<%# Bind("Phone") %>' OnKeyPress="return  onlyNumbers(event)"></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="Phone" runat="server" Text='<%# Bind("Phone") %>' OnKeyPress="return  onlyNumbers(event)"></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LabelPhone" runat="server" Text='<%# Bind("Phone") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Codigo Postal" SortExpression="PostalCode">
                        <EditItemTemplate>
                            <asp:TextBox ID="PostalCode" runat="server" Text='<%# Bind("PostalCode") %>' onkeypress="javascript:return RestrictChar('[(-_0-9a-zA-Z)]')"></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="PostalCode" runat="server" Text='<%# Bind("PostalCode") %>'>*</asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LabelPostalCode" runat="server" Text='<%# Bind("PostalCode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Estado" SortExpression="Status">
                        <EditItemTemplate>
                            <asp:DropDownList SelectedIndex='<%# FormatSelectedIndex((bool)Eval("IsApproved")) %>' runat="server" ID="IsApproved">
                                <asp:ListItem Value="true">Activo</asp:ListItem>
                                <asp:ListItem Value="false">Inactivo</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                             <asp:HiddenField runat="server" ID="HiddenIsApproved" Value="0" />
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LabelEst" Visible="true" runat="server" Text='<%# FormatIsApproved((bool)Eval("IsApproved")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>  
                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:Button runat="server" ID="cmdModificar" Text="Modificar" SkinID="BottonAccion2" BorderStyle="None" 
                                onclick="cmdModificar_Click"  />
                            <asp:Button runat="server" ID="cmdCancelar" Text="Cancelar" SkinID="BottonAccion2" BorderStyle="None" 
                            onclick="cmdCancelar_Click" />
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:Button runat="server" ID="cmdInsertar" Text="Grabar" SkinID="BottonAccion2" BorderStyle="None" 
                                onclick="cmdInsertar_Click" />                            
                             <asp:Button runat="server" ID="cmdCancelar" Text="Cancelar" SkinID="BottonAccion2" BorderStyle="None" 
                             onclick="cmdCancelar_Click" />
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Button runat="server" ID="cmdEliminar" Text="Eliminar" SkinID="BottonAccion2" BorderStyle="None"  
                                onclick="cmdEliminar_Click" />
                            <asp:Button runat="server" ID="cmdCancelar" Text="Cancelar" SkinID="BottonAccion2" BorderStyle="None" 
                            onclick="cmdCancelar_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Fields>
            </asp:DetailsView>
       </td>        
    </tr>
</table>
  
   
</asp:Content>
