﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using log4net;
using Bio.Portal.Server.Common.Common;
using Bio.Portal.Server.Manager.Core.libs;
using System.Web.UI.HtmlControls;
using System.Text;

namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class ModifyRole : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ListUsers));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/menuVertical.js"));
                this.Page.Header.Controls.Add(myJs);
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);

                RenderJSArrayWithCliendIds(chlrol,hiddenTotal);

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 2)
                {
                    Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    Session["exception"] = null;
                    Response.Redirect("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);

                log.Debug("El usuario ingresa a modificar el rol");
                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                if (!Page.IsPostBack)
                {
                    if (Session["user"] != null)
                    {
                        String usuario = Session["user"].ToString();
                        LabUser.Text = usuario;

                        string[] roles = Roles.GetAllRoles();

                        int j = 0;
                        if (User.IsInRole("Super Administrador"))
                        {
                            chlrol.DataSource = roles;
                            j = roles.Length;
                        }
                        else
                        {
                            string[] rolesempresas = new string[roles.Length - 1];
                            for (int i = 0; i < roles.Length; i++)
                            {
                                if (roles[i] == "Super Administrador") continue;
                                rolesempresas[j] = roles[i];
                                j++;
                            }
                            chlrol.DataSource = rolesempresas;
                        }
                        chlrol.DataBind();

                        hiddenTotal.Value = j.ToString();

                        //Recorremos las checkbox.
                        for (int i = 0; i < chlrol.Items.Count; i++)
                        {
                            if (Roles.IsUserInRole(usuario, chlrol.Items[i].Text))
                                chlrol.Items[i].Selected = true;
                        }

                    }
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyRole.aspx" + exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyRole.aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }
        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }

        protected void cmdGuardar_Click(object sender, EventArgs e)
        {
            string[] roles = null;
            try
            {
                bool selected = false;
                for (int i = 0; i < chlrol.Items.Count; i++)
                {
                    if (chlrol.Items[i].Selected)
                    {
                        selected = true;
                    }
                }

                if (!selected)
                {
                    labError.Visible = true;
                    return;
                }

                roles = Roles.GetAllRoles();
                //Verificamos los roles adjudicados.
                if (Session["user"] != null)
                {
                    
                    Roles.RemoveUserFromRoles(Session["user"].ToString(), roles);
                    for (int i = 0; i < chlrol.Items.Count; i++)
                    {
                        if (chlrol.Items[i].Selected == true)
                        {
                            Roles.AddUserToRole(Session["user"].ToString(), chlrol.Items[i].Text);
                            log.Debug("El usuario:" + Session["user"].ToString() + " ha sido agregado al rol:" + chlrol.Items[i].Text);
                        }
                    }
                    Session["mensaje"] = "Los cambios fueron guardados correctamente";
                    
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyRole.aspx al agregar un rol a un usuario" + exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyRole.aspx al agregar un rol a un usuario";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
            Response.Redirect("ListUsers.aspx", true);
        }

        protected void cmdVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Core/Admin/ListUsers.aspx", true);
        }
    }
}