﻿using System;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Collections;
using log4net;

namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class VerTx : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(VerTx));
        private ArrayList criterio;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                criterio = (ArrayList)Session["params"];

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 3)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);
                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en VerLogTx.aspx" , exe);
                Session["mensajeerror"] = "Se ha producido un error en VerLogTx.aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceTx_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            try
            {
                if (criterio != null)
                {

                    if (!(criterio[0].Equals(String.Empty)))
                        e.InputParameters["id"] = Convert.ToInt32(criterio[0]);
                    else
                        e.InputParameters["id"] = Convert.ToInt32(0);

                    if (!(criterio[1].Equals(String.Empty)))
                        e.InputParameters["trackid"] = criterio[1].ToString();
                    else
                        e.InputParameters["trackid"] = "";

                    if (!(criterio[2].Equals(String.Empty)))
                        e.InputParameters["origin"] = Convert.ToInt32(criterio[2]);
                    else
                        e.InputParameters["origin"] = 0;

                    if (!(criterio[3].Equals(String.Empty)))
                        e.InputParameters["typeid"] = criterio[3];
                    else
                        e.InputParameters["typeid"] = "";

                    if (!(criterio[4].Equals(String.Empty)))
                        e.InputParameters["valueid"] = criterio[4];
                    else
                        e.InputParameters["valueid"] = "";

                    if (!(criterio[5].Equals(String.Empty)))
                        e.InputParameters["actiontype"] = Convert.ToInt32(criterio[5]);
                    else
                        e.InputParameters["actiontype"] = Convert.ToInt32(0);

                    if (!(criterio[6].Equals(String.Empty)))
                        e.InputParameters["idconx"] = criterio[6];
                    else
                        e.InputParameters["idconx"] = "";

                    if (!(criterio[7].Equals(String.Empty)))
                        e.InputParameters["statusconx"] = Convert.ToInt32(criterio[7]);
                    else
                        e.InputParameters["statusconx"] = Convert.ToInt32(0);

                    if (!(criterio[8].Equals(String.Empty)))
                        e.InputParameters["resultconx"] = Convert.ToInt32(criterio[8]);
                    else
                        e.InputParameters["resultconx"] = Convert.ToInt32(0);

                    if (Convert.ToBoolean(criterio[9]) == false)
                    {
                        if (!(criterio[10].Equals(String.Empty)))
                            e.InputParameters["timestamprcdesde"] = criterio[10];
                        else
                            e.InputParameters["timestamprcdesde"] = "";

                        if (!(criterio[11].Equals(String.Empty)))
                            e.InputParameters["timestamprchasta"] = criterio[11];
                        else
                            e.InputParameters["timestamprchasta"] = "";
                    }
                    else
                    {
                        e.InputParameters["timestamprcdesde"] = "";
                        e.InputParameters["timestamprchasta"] = "";
                    }

                    if (!(criterio[12].Equals(String.Empty)))
                        e.InputParameters["companyid"] = Convert.ToInt32(criterio[12]);
                    else
                        e.InputParameters["companyid"] = Convert.ToInt32(0);

                    if (!(criterio[13].Equals(String.Empty)))
                        e.InputParameters["connectorid"] = criterio[13];
                    else
                        e.InputParameters["connectorid"] = "";
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en VerLogTx.aspx al recuperar los datos" , exe);
                Session["mensajeerror"] = "Se ha producido un error en VerLogTx.aspx al recuperar los datos";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void cmdVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Core/Admin/ListLogTx.aspx", true);
        }

        //Formateo grilla con diversos estilos
        protected void GridViewTx_DataBound(object sender, EventArgs e)
        {
                //Coloreo en amarillo claro lo de conexion esterna
            foreach (GridViewRow row in GridViewTx.Rows)
            {
                row.Cells[12].BackColor = Color.LightGreen;
                row.Cells[13].BackColor = Color.LightGreen;
                row.Cells[14].BackColor = Color.LightGreen;
                row.Cells[15].BackColor = Color.LightGreen;
                row.Cells[16].BackColor = Color.LightGreen;

                   //Coloreo letras en resultado
                row.Cells[6].Font.Bold = true; 
                if (row.Cells[6].Text.Trim().Equals("Negativo"))
                {
                    row.Cells[6].ForeColor = Color.Red;
                }
                else if (row.Cells[6].Text.Trim().Equals("Positivo"))
                {
                    row.Cells[6].ForeColor = Color.Green;
                }

                if (!row.Cells[3].Text.Trim().Equals("0"))
                {
                    row.Cells[3].ForeColor = Color.Red;
                }
            }

        
        }
    }
}