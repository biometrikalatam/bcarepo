﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;

namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class ChangePasswords : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ListUsers));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                log.Debug("El usuario a accedido a Cambio de Password");
                //Verificamos accesos
                if (User.IsInRole("Super Administrador"))
                    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                else if (User.IsInRole("Administrador"))
                    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                if (User.IsInRole("Enrolador"))
                    BioPortalServer.VisualizarMenuEnrolador(Master);
                if (User.IsInRole("Verificador"))
                    BioPortalServer.VisualizarMenuVerificar(Master);
            }
            catch
            {
            }
        }
    }
}