﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="Configuration.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Admin.Configuration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

   <table border="0" cellpadding="0" cellspacing="0" width="80%"  align="center">
        <tr>
            <td class="titulos0">Configuración BioPortal Services</td>
        </tr>
        <tr>
            <td align="left">
               <table border="0" width="100%" align="center">
                    <tr>
                        <td>                          
                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                                SelectMethod="SelectConfiguration" 
                                TypeName="Bio.Portal.Server.Manager.Core.WebServices.WebServicesHelper" 
                                onupdating="ObjectDataSource1_Updating" UpdateMethod="SaveConfiguration" 
                                onupdated="ObjectDataSource1_Updated">
                                <UpdateParameters>
                                    <asp:Parameter Name="insertOption" Type="Int32" />
                                    <asp:Parameter Name="saveVerified" Type="Int32" />
                                    <asp:Parameter Name="minutiaetypeDefault" Type="Int32" />
                                    <asp:Parameter Name="timeoutWSdefault" Type="Int32" />
                                    <asp:Parameter Name="minutiaetypeToEnroll" Type="Object" />
                                    <asp:Parameter Name="matchingtype" Type="Int32" />
                                    <asp:Parameter Name="operationOrder" Type="Int32" />
                                    <asp:Parameter Name="checkAccess" Type="Int32" />
                                    <asp:Parameter Name="connectorId" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>                          
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DetailsView ID="DetailsViewConfig" runat="server" Height="50px" 
                                Width="125px" AutoGenerateRows="False" DataSourceID="ObjectDataSource1" 
                                EnableModelValidation="True" onprerender="DetailsViewConfig_PreRender" SkinID="DetailsViewIngreso1">
                                <Fields>
                                    <asp:TemplateField HeaderText="InsertOption : " HeaderStyle-HorizontalAlign="left" SortExpression="InsertOption">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtInsertOption" Width="20" MaxLength="1" runat="server" Text='<%# Bind("InsertOption") %>' OnKeyPress="return  RestrictChar('[1-2]')"></asp:TextBox>
                                            &nbsp;[1 - Insert | 2 - No Insert]
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="txtInsertOption" Width="20" MaxLength="1" runat="server" Text='<%# Bind("InsertOption") %>' OnKeyPress="return  RestrictChar('[1-2]')"></asp:TextBox>
                                            &nbsp;[1 - Insert | 2 - No Insert]
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Width="20" Text='<%# Bind("InsertOption") %>'></asp:Label>
                                            &nbsp;[1 - Insert | 2 - No Insert]
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SaveVerified" SortExpression="SaveVerified">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtSaveVerified" Width="20" MaxLength="1" runat="server" Text='<%# Bind("SaveVerified") %>' OnKeyPress="return  RestrictChar('[1-2]')"></asp:TextBox>
                                            &nbsp;[1 - Save | 2 - No Save]
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="txtSaveVerified" Width="20" MaxLength="1" runat="server" Text='<%# Bind("SaveVerified") %>' OnKeyPress="return  RestrictChar('[1-2]')"></asp:TextBox>
                                            &nbsp;[1 - Save | 2 - No Save]
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Width="20" Text='<%# Bind("SaveVerified") %>'></asp:Label>
                                            &nbsp;[1 - Save | 2 - No Save]
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MinutiaetypeDefault" 
                                        SortExpression="MinutiaetypeDefault">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtMinutiaetypeDefault" runat="server" Width="20" MaxLength="5"
                                                Text='<%# Bind("MinutiaetypeDefault") %>' OnKeyPress="return  onlyNumbers(event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="txtMinutiaetypeDefault" runat="server" Width="20" MaxLength="5"
                                                Text='<%# Bind("MinutiaetypeDefault") %>' OnKeyPress="return  onlyNumbers(event)"></asp:TextBox>
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Width="20" MaxLength="5" Text='<%# Bind("MinutiaetypeDefault") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TimeoutWSdefault" 
                                        SortExpression="TimeoutWSdefault">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtTimeoutWSdefault" runat="server" Width="50" MaxLength="6"
                                                Text='<%# Bind("TimeoutWSdefault") %>' OnKeyPress="return  onlyNumbers(event)"></asp:TextBox>
                                                &nbsp;[30000 milisegundos / 30 segundos default]
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="txtTimeoutWSdefault" runat="server" Width="50" MaxLength="6"
                                                Text='<%# Bind("TimeoutWSdefault") %>' OnKeyPress="return  onlyNumbers(event)"></asp:TextBox>
                                                 &nbsp;[30000 milisegundos / 30 segundos default]
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Width="50" Text='<%# Bind("TimeoutWSdefault") %>'></asp:Label>
                                             &nbsp;[30000 milisegundos / 30 segundos default]
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Matchingtype" SortExpression="Matchingtype">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtMatchingtype" runat="server" Width="20" MaxLength="1" Text='<%# Bind("Matchingtype") %>' OnKeyPress="return  onlyNumbers(event)"></asp:TextBox>
                                             &nbsp;[1 - First | 2 - Best]
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="txtMatchingtype" runat="server" Width="20" MaxLength="1" Text='<%# Bind("Matchingtype") %>' OnKeyPress="return  onlyNumbers(event)"></asp:TextBox>
                                            &nbsp;[1 - First | 2 - Best]
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Width="20" Text='<%# Bind("Matchingtype") %>'></asp:Label>
                                            &nbsp;[1 - First | 2 - Best]
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OperationOrder" SortExpression="OperationOrder">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtOperationOrder" runat="server" Width="20" MaxLength="1" Text='<%# Bind("OperationOrder") %>' OnKeyPress="return  RestrictChar('[1-3]')"></asp:TextBox>
                                            &nbsp;[1 - Solo Local | 2 - Primero Local | 3 - Solo Remoto]
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="txtOperationOrder" runat="server" Width="20" MaxLength="1" Text='<%# Bind("OperationOrder") %>' OnKeyPress="return  RestrictChar('[1-3]')"></asp:TextBox>
                                            &nbsp;[1 - Solo Local | 2 - Primero Local | 3 - Solo Remoto]
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Width="20" Text='<%# Bind("OperationOrder") %>'></asp:Label>
                                            &nbsp;[1 - Solo Local | 2 - Primero Local | 3 - Solo Remoto]
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CheckAccess" SortExpression="CheckAccess">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtCheckAccess" runat="server" Width="20" MaxLength="1" Text='<%# Bind("CheckAccess") %>' OnKeyPress="return  RestrictChar('[0-3]')"></asp:TextBox>
                                            &nbsp;[0 - No Check | 1 - Company | 2 - User | 3 - Both]
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="txtCheckAccess" runat="server" Width="20" MaxLength="1" Text='<%# Bind("CheckAccess") %>' OnKeyPress="return  RestrictChar('[0-3]')"></asp:TextBox>
                                            &nbsp;[0 - No Check | 1 - Company | 2 - User | 3 - Both]
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Width="20" Text='<%# Bind("CheckAccess") %>'></asp:Label>
                                            &nbsp;[0 - No Check | 1 - Company | 2 - User | 3 - Both]
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ConnectorId Default" SortExpression="ConnectorDefault">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtConnectorDefault" Width="200" MaxLength="20" runat="server" Text='<%# Bind("ConnectorDefault") %>' OnKeyPress="return  RestrictChar('[0-9a-zA-Z_-]')"></asp:TextBox>
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="txtConnectorDefault" Width="200" MaxLength="20" runat="server" Text='<%# Bind("ConnectorDefault") %>' OnKeyPress="return  RestrictChar('[0-9a-zA-Z_-]"></asp:TextBox>
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label8" runat="server" Width="200" Text='<%# Bind("ConnectorDefault") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    
                                    <asp:TemplateField ShowHeader="False">
                                        <EditItemTemplate>
                                            <asp:Button ID="cmdModificar" Text="Modificar" runat="server" SkinID="BotonAccion" 
                                                OnClientClick="javascript:return ValidarFormularioConfiguracion();" 
                                                onclick="cmdModificar_Click"  />
                                            <asp:Button ID="cmdCancelar" Text="Cancelar" runat="server" SkinID="BotonAccion" 
                                             OnClick="cmdCancelar_Click" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>
                        </td>
                    </tr>
               </table>
            </td>
        </tr>

      </table>

       
    
   
    </asp:Content>
