﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Admin.ChangePasswords" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
 <table cellpadding="0" cellspacing="0" width="400" align="center">
   
    <tr>
        <td align="center" width="80%" class="tdBottom">
            <asp:ChangePassword ID="ChangePassword1" SkinID="SampleChangePassword" runat="server"                  
                ConfirmNewPasswordLabelText="Confirmar la nueva Passsword" 
                NewPasswordLabelText="Nueva Password" 
                NewPasswordRequiredErrorMessage="Un nuevo password es requerido." 
                SuccessText="Tu password ha sido cambiado!!!" 
                SuccessTitleText="Cambio Completo" Width="400" CancelDestinationPageUrl="~/Welcome.aspx" 
                ContinueDestinationPageUrl="~/Welcome.aspx">
            </asp:ChangePassword>    
        
        </td>
    </tr>
 </table>
    
</asp:Content>
