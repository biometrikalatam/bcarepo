﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bio.Portal.Server.Manager.Core.libs;
using Bio.Portal.Server.Common.Common;
using System.Web.UI.HtmlControls;
using log4net;
using System.Text;
using System.Collections;

namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class ListLogTx : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ListLogTx));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/menuVertical.js"));
                this.Page.Header.Controls.Add(myJs);
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);
                HtmlGenericControl myJs2 = new HtmlGenericControl();
                myJs2.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs2.Attributes.Add("language", "javascript");
                myJs2.Attributes.Add("src", ResolveUrl("~/Scripts/dhtmlgoodies_calendar.js"));
                this.Page.Header.Controls.Add(myJs2);

                //Chequea si tiene permisos para acceder a esta página
                int levelIdRol = BioPortalServer.GetRolId(User);
                if (levelIdRol > 3)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);
                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);

                log.Debug("Se ingresa a visualizar las empresas usuario:" + UserNameFull);
                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                RenderJSArrayWithCliendIds(txtfecha, txtfechah);
                cmdCalendario.Attributes.Add("onclick", "javascript:displayCalendar(" + txtfecha.ClientID + ",'dd/mm/yyyy',this);return false;");
                cmdCalendarioH.Attributes.Add("onclick", "javascript:displayCalendar(" + txtfechah.ClientID + ",'dd/mm/yyyy',this);return false;");
                //cmdCalendarioH1.Attributes.Add("onclick", "javascript:displayCalendar(" + txtfechah.ClientID + ",'dd/mm/yyyy',this);return false;");
                //txtfecha.Attributes.Add("onkeyup", "javascript:DateFormat(" + this + "," + txtfecha.Text + ",event,false,'3');"); 
                //onBlur="DateFormat(this,this.value,event,true,'3')"

                Page.ClientScript.RegisterClientScriptInclude("JQUERY", ResolveClientUrl("../../Scripts/menuVertical.js"));
                if (!Page.IsPostBack)
                {
                    DropDownListDocumento.DataSource = Global.documentos.documents;
                    DropDownListDocumento.DataValueField = "ID";
                    DropDownListDocumento.DataTextField = "Description";
                    DropDownListDocumento.DataBind();

                    DropDownListOperations.DataSource = Bio.Core.Matcher.Constant.Action.GetActions(); //Global.operations.operations;
                    DropDownListOperations.DataValueField = "Key";
                    DropDownListOperations.DataTextField = "Value";
                    DropDownListOperations.DataBind();

                    DropDownListEstado.DataSource = Global.states.states;
                    DropDownListEstado.DataValueField = "ID";
                    DropDownListEstado.DataTextField = "Description";
                    DropDownListEstado.DataBind();

                    if (levelIdRol == 1)
                    {
                        DropDownListCompany.Visible = true;
                        LabelCompany.Visible = false;
                        HiddenCompany.Value = CompanyId.ToString();
                        LabelCompanyLab.Visible = true;
                        LabelCompanyLab1.Visible = true;
                    } else
                    {
                        DropDownListCompany.Visible = false;
                        LabelCompany.Text = "";
                        LabelCompany.Visible = false;
                        LabelCompanyLab.Visible = false;
                        LabelCompanyLab1.Visible = false;
                    }

                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ListLogTx.aspx" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ListLogTx.aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void DropDownListOrigen_DataBound(object sender, EventArgs e)
        {           
            DropDownListOrigen.Items.Insert(0, new ListItem("Todos", "0"));
        }

        protected void DropDownListDocumento_DataBound(object sender, EventArgs e)
        {
            DropDownListDocumento.Items.Insert(0, new ListItem("Todos", ""));
        }

        protected void DropDownListOperations_DataBound(object sender, EventArgs e)
        {
            DropDownListOperations.Items.Insert(0, new ListItem("Todos", "0"));
        }

        protected void DropDownListEstado_DataBound(object sender, EventArgs e)
        {
            DropDownListEstado.Items.Insert(0, new ListItem("Todos", "-1"));
        }
        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }

        protected void cmdVerTx_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList criterio = new ArrayList();
                //Tomamos los criterios.
                criterio.Add(txtNroTrackId.Text.Trim());
                criterio.Add(txtIdtx.Text.Trim());
                criterio.Add(DropDownListOrigen.SelectedItem.Value.Trim());
                criterio.Add(DropDownListDocumento.SelectedItem.Value.Trim());
                criterio.Add(txtNroDocumento.Text.Trim());
                criterio.Add(DropDownListOperations.SelectedItem.Value.Trim());
                criterio.Add(txtIdRC.Text.Trim());
                criterio.Add(DropDownListEstado.SelectedItem.Value.Trim());
                criterio.Add(DropDownListResultado.SelectedItem.Value.Trim());
                criterio.Add(chkfecha.Checked);
                if (chkfecha.Checked == false)
                {

                    criterio.Add(txtfecha.Text.Trim());
                    criterio.Add(txtfechah.Text.Trim());
                }
                else
                {
                    criterio.Add(null);
                    criterio.Add(null);
                }
                if (DropDownListCompany.Visible)
                {
                    criterio.Add(DropDownListCompany.SelectedValue.ToString());
                } else
                {
                    criterio.Add(CompanyId);    
                }
                criterio.Add(ConnectorId.Text.Trim());

                Session["params"] = criterio;
                Response.Redirect("VerTx.aspx", true);
            }
            catch (Exception exe)
            {
                if (exe.GetType().FullName != "System.Threading.ThreadAbortException")
                {
                    log.Error("Se ha producido un error en ListLogtx.aspx al momento de visualizar los logs de transacciones" , exe);
                    Session["mensajeerror"] = "Se ha producido un error en ListLog.aspx al momento de visualizar los logs de transacciones";
                    Session["exception"] = exe;
                    Response.Redirect("~/Error.aspx");
                }
            }
        }

        protected void DropDownListCompany_DataBound(object sender, EventArgs e)
        {
            DropDownListCompany.Items.Insert(0, new ListItem("Todas", "0"));
        }
    }
}