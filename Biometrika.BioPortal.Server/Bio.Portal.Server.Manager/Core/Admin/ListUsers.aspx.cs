﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using Bio.Portal.Server.Common.Common;
using System.Text;
using Bio.Portal.Server.Manager.Core.libs;
using System.Web.UI.HtmlControls;

namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class ListUsers : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ListUsers));

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/menuVertical.js"));
                this.Page.Header.Controls.Add(myJs);
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 2)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);
                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);

                log.Debug("Se ingresa a visualizar las usuarios :" + UserNameFull);
                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                Page.ClientScript.RegisterClientScriptInclude("JQUERY", ResolveClientUrl("../../Scripts/menuVertical.js"));
                RenderJSArrayWithCliendIds(username, DropDownListCompany, hiddenfiltro, indice, userid);
                //Determinamos que rol es SuperAdministrador o Administrador para determinar porque criterios puede filtrar.
                if (User.IsInRole("Super Administrador"))
                {
                    porempresa.Visible = true;
                    hiddenfiltro.Value = "si";

                }
                else
                {
                    porempresa.Visible = false;
                    hiddenfiltro.Value = "no";


                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ListUsers.aspx" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ListUsers.aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }


        }

        protected void ObjectDataSourceUser_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            try
            {
                log.Debug("Seleccionamos los usuarios asociados a: " + CompanyId);
                if (User.IsInRole("Super Administrador"))
                    e.InputParameters["companyid"] = DropDownListCompany.SelectedItem.Value;
                else
                    e.InputParameters["companyid"] = CompanyId;
                e.InputParameters["username"] = username.Text;
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ListLog.aspx en la inicialización de parámetros de selección" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ListLog.aspx en la inicialización de parámetros de selección";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            log.Debug("El usuario accede a crear usuario");
            Response.Redirect("CreateUser.aspx");
        }

        protected void GridViewUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    string id = indice.Value;
                    log.Info("El usuario solicita modificar empresa : " + Username);
                    Response.Redirect("ModifyUser.aspx?oper=mod&id=" + id);

                }
                else if (e.CommandName == "Delete")
                {

                    string id = indice.Value;
                    log.Info("El usuario solicita dar de baja la empresa : " + Username);
                    Response.Redirect("ModifyUser.aspx?oper=eli&id=" + id);

                }
                else if (e.CommandName == "ChangePassword")
                {
                    Session["iduser"] = indice.Value;
                    Session["user"] = userid.Value;
                    log.Info("El usuario solicita : " + Username + " cambiar la password del usuario id :" + userid.Value);
                    Response.Redirect("ResetPassword.aspx");
                }
                else if (e.CommandName == "ChangePassword")
                {
                    Session["iduser"] = indice.Value;
                    Session["user"] = userid.Value;
                    log.Info("El usuario solicita : " + Username + " cambiar la password del usuario id :" + userid.Value);
                    Response.Redirect("ModifyRole.aspx");
                }
                else if (e.CommandName == "ChangeRole")
                {
                    Session["iduser"] = indice.Value;
                    Session["user"] = userid.Value;
                    log.Info("El usuario " + Username + "solicita actualizar los roles del usuario id :" + userid.Value);
                    Response.Redirect("ModifyRole.aspx");
                }



            }
            catch (Exception exe)
            {
                if (exe.GetType().FullName != "System.Threading.ThreadAbortException")
                {
                    log.Error("Se ha producido un error en ListUsers.aspx en la ejecución de una acción sobre la grilla" , exe);
                    Session["mensajeerror"] = "Se ha producido un error en ListUsers.aspx en la ejecución de una acción sobre la grilla";
                    Session["exception"] = exe;
                    Response.Redirect("~/Error.aspx");
                }
            }
        }
        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }

        protected void DropDownListCompany_DataBound(object sender, EventArgs e)
        {
            DropDownListCompany.Items.Insert(0, new ListItem("Seleccione una Compañía", "-1"));
            DropDownListCompany.Items.Insert(1, new ListItem("Todas", "0"));
        }

        protected void cmdBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                ObjectDataSourceUser.Select();
                GridViewUsers.DataBind();
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ListUsers.aspx en la recuperación de datos en la grilla" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ListUsers.aspx en la recuperación de datos en la grilla";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        
    }
}