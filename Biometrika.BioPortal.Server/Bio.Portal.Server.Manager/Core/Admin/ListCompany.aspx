﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="ListCompany.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Admin.ListCompany" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">    
    <table cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="4" class="titulos0">
             Listado de Compañías
            <asp:Label ID="mensaje" runat="server" text="" Visible="false"></asp:Label>
            <asp:HiddenField runat="server" ID="indice" Value="" />
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Rut</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
           <asp:TextBox runat="server" ID="txtRut" MaxLength="8"></asp:TextBox> Ingrese el rut sin dígito verificador y sin puntos.
        </td>
        <td class="tdDerechaTop"></td>
    </tr>
    <tr>
        <td class="tdBottom">Razón Social</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
           <asp:TextBox runat="server" ID="txtRazonSocial" MaxLength="100"></asp:TextBox> 
        </td>
        <td class="tdDerechaTop"></td>
    </tr>
    <tr>
        <td class="tdBottom">Estado</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:DropDownList runat="server" ID="DropDownListEstado" AutoPostBack="false">
                <asp:ListItem Value="-1">Seleccione un Estado</asp:ListItem>
                <asp:ListItem Value="0">Todas</asp:ListItem>
                <asp:ListItem Value="1">Activas</asp:ListItem>
                <asp:ListItem Value="2">Inactivas</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td class="tdDerechaTop"></td>
    </tr>

    <tr>
        <td colspan="4" align="right">
            <asp:Button runat="server" ID="cmdBuscar" Text="Buscar" BorderStyle="None"  
                OnClientClick="javascript:return ValidarFormularioCompany();" 
                onclick="cmdBuscar_Click" SkinID="BotonAccion"
                 />
        </td>
    </tr>
    <tr>
        <td colspan="4">    
            <asp:ObjectDataSource ID="ObjectDataSourceCompany" runat="server" 
                SelectMethod="RetrieveCompany" 
                TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper" 
                onselecting="ObjectDataSourceCompany_Selecting">
                <SelectParameters>
                    <asp:Parameter Name="rut" Type="String" />
                    <asp:Parameter Name="razonsocial" Type="String" />
                    <asp:ControlParameter ControlID="DropDownListEstado" DefaultValue="1" 
                        Name="estado" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
            <asp:Button ID="cmdNuevo" runat="server" onclick="cmdNuevo_Click" Text="Crear Empresa" BorderStyle="None" 
                Width="120px" SkinID="BotonAccion"  />
            <br />
            <asp:GridView ID="GridViewCompany" runat="server" AutoGenerateColumns="False" 
                DataSourceID="ObjectDataSourceCompany" EnableModelValidation="True" 
                onrowcommand="GridViewCompany_RowCommand" SkinID="GridView1"
                CssClass="" 
                >
                <Columns>
                     <asp:TemplateField>
                      <ItemTemplate>
                        <asp:ImageButton runat="server" ID="EditButton" 
                          CommandName="Edit" ToolTip="Modificar Compañía" 
                          ImageUrl="~/Images/empre_edit.gif" OnClientClick='<%# ImagenEditarCompany((int)Eval("Id")) %>' />                          
                      </ItemTemplate>
                    </asp:TemplateField>                
                     <asp:TemplateField>
                      <ItemTemplate>
                        <asp:ImageButton runat="server" ID="DeleteButton" 
                          CommandName="Delete" ToolTip="Dar de Baja Compañía"  
                          ImageUrl="~/Images/empre_bloq.gif" OnClientClick='<%# ImagenEliminarCompany((int)Eval("Id")) %>'  />                          
                      </ItemTemplate>
                    </asp:TemplateField>      
                    <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
                    <asp:BoundField DataField="Rut" HeaderText="Rut" SortExpression="Rut" />
                    <asp:BoundField DataField="Address" HeaderText="Dirección" 
                        SortExpression="Address" />
                    <asp:BoundField DataField="Name" HeaderText="Nombre" SortExpression="Name" />
                    <asp:BoundField DataField="Domain" HeaderText="Dominio" 
                        SortExpression="Domain" />
                    <asp:BoundField DataField="Phone" HeaderText="Teléfono" 
                        SortExpression="Phone" />
                    <asp:BoundField DataField="Startdate" HeaderText="Alta" 
                        SortExpression="Startdate" />
                    <asp:TemplateField HeaderText="Estado" SortExpression="Status">                       
                        <ItemTemplate>
                             <asp:Label ID="LabelEstado" runat="server" Text='<%# FormatStatus((int)Eval("Status")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Endate" HeaderText="Baja" 
                        SortExpression="Endate" />
                    
                </Columns>
            </asp:GridView>    
        </td>
    </tr>
</table>
</asp:Content>
