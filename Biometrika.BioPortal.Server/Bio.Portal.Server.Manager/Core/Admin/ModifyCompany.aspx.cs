﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bio.Core.Constant;
using Bio.Portal.Server.Manager.Core.WebServices;
using log4net;
using Bio.Portal.Server.Common.Common;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Text;

namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class ModifyCompany : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ModifyCompany));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string oper = Request["oper"] ?? String.Empty;
                string id = Request["id"];

                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/menuVertical.js"));
                this.Page.Header.Controls.Add(myJs);
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 1)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);

                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);

                if (EsNumerico(id) == true)
                {
                    if (oper.Equals("mod"))
                    {
                        //RenderJSArrayWithCliendIds(DetailsViewCompany.FindControl("rut"), txttest);
                        DetailsViewCompany.DefaultMode = DetailsViewMode.Edit;
                        lbltitulo.Text = "Modificar Compañía";
                        lbltitulo.Visible = true;

                    }
                    else if (oper.Equals("eli"))
                    {
                        DetailsViewCompany.DefaultMode = DetailsViewMode.ReadOnly;
                        lbltitulo.Text = "Eliminar Compañía";
                        lbltitulo.Visible = true;

                    }
                    else
                    {
                        DetailsViewCompany.DefaultMode = DetailsViewMode.Insert;
                        lbltitulo.Text = "Agregar Compañía";
                        lbltitulo.Visible = true;

                    }
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyCompany.aspx", exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyCompany.aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceCompany_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            try
            {
                e.InputParameters["id"] = Convert.ToInt32(Request["id"]);
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyCompany.aspx al seleccionar la compañía para editar", exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyCompany.aspx al seleccionar la compañía para editar";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceCompany_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            //No hay error al grabar.
            if (e.ReturnValue.ToString() == "0")
            {
                Session["mensaje"] = "Los cambios fueron guardados correctamente";
                string msg;
                int iErr = WebServicesHelper.RefreshCompaniesInServices(out msg);
                Session["mensaje"] = iErr != Errors.IERR_OK
                                         ? "Error Updating Compañias via WS [" +
                                           iErr.ToString() + "-" + msg + "]"
                                         : null;
                Response.Redirect("ListCompany.aspx");
            }
            else
            {
                lblmsg.Text = "Se ha producido un error al grabar los datos";
                lblmsg.Visible = true;
            }

        }

        protected void ObjectDataSourceCompany_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            try
            {
               
                e.InputParameters["id"] = Convert.ToInt32(Request["id"]);
                e.InputParameters["rut"] = ((TextBox)DetailsViewCompany.FindControl("rut")).Text;
                e.InputParameters["address"] = ((TextBox)DetailsViewCompany.FindControl("address")).Text;
                e.InputParameters["name"] = ((TextBox)DetailsViewCompany.FindControl("name")).Text;
                e.InputParameters["phone"] = ((TextBox)DetailsViewCompany.FindControl("phone")).Text;
                e.InputParameters["phone2"] = ((TextBox)DetailsViewCompany.FindControl("phone2")).Text;
                e.InputParameters["domain"] = ((TextBox)DetailsViewCompany.FindControl("domain")).Text;
                e.InputParameters["fax"] = ((TextBox)DetailsViewCompany.FindControl("fax")).Text;
                e.InputParameters["contactname"] = ((TextBox)DetailsViewCompany.FindControl("contactname")).Text;
                e.InputParameters["status"] = ((DropDownList)DetailsViewCompany.FindControl("status")).SelectedItem.Value;
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyCompany.aspx al modificar una compañía" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyCompany.aspx al modificar una compañía";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceCompany_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            //No hay error al grabar.
            if (e.ReturnValue.ToString() == "0")
            {
                Session["mensaje"] = "Los cambios fueron guardados correctamente";
                string msg;
                int iErr = WebServicesHelper.RefreshCompaniesInServices(out msg);
                Session["mensaje"] = iErr != Errors.IERR_OK
                                         ? "Error Updating Compañias via WS [" +
                                           iErr.ToString() + "-" + msg + "]"
                                         : null;
                Response.Redirect("ListCompany.aspx");
            }
            else
            {
                lblmsg.Text = "Se ha producido un error al grabar los datos";
                lblmsg.Visible = true;
            }

        }

        protected void ObjectDataSourceCompany_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            //No hay error al grabar.
            if (e.ReturnValue.ToString() == "0")
            {
                Session["mensaje"] = "Los cambios fueron guardados correctamente";
                string msg;
                int iErr = WebServicesHelper.RefreshCompaniesInServices(out msg);
                Session["mensaje"] = iErr != Errors.IERR_OK
                                         ? "Error Updating Compañias via WS [" +
                                           iErr.ToString() + "-" + msg + "]"
                                         : null;
                Response.Redirect("ListCompany.aspx");
            }
            else
            {
                lblmsg.Text = "Se ha producido un error al grabar los datos";
                lblmsg.Visible = true;
            }

        }

        protected void ObjectDataSourceCompany_Deleting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            try
            {
                e.InputParameters["id"] = Convert.ToInt32(Request["id"]);
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyCompany.aspx al momento de eliminar una compañía" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyCompany.aspx al momento de eliminar una compañía";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void DetailsViewCompany_ItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            

        }

        protected void DetailsViewCompany_DataBound(object sender, EventArgs e)
        {
            //Seteamos variables javascript para los modos de edición y modificación.
            if ((DetailsViewCompany.CurrentMode == DetailsViewMode.Insert) || (DetailsViewCompany.CurrentMode == DetailsViewMode.Edit))
            {
                
                TextBox name = DetailsViewCompany.FindControl("name") as TextBox;
                name.Attributes.Add("onkeypress", "javascript:return RestrictChar('[-_0-9a-zA-Z]')");
                name = null;

                TextBox address = DetailsViewCompany.FindControl("address") as TextBox;
                address.Attributes.Add("onkeypress", "javascript:return RestrictChar()");
                address = null;

                TextBox phone = DetailsViewCompany.FindControl("phone") as TextBox;
                phone.Attributes.Add("onkeypress", "javascript:return onlyNumbers()");
                phone = null;

                TextBox phone2 = DetailsViewCompany.FindControl("phone2") as TextBox;
                phone2.Attributes.Add("onkeypress", "javascript:return onlyNumbers()");
                phone2 = null;

                TextBox fax = DetailsViewCompany.FindControl("fax") as TextBox;
                fax.Attributes.Add("onkeypress", "javascript:return onlyNumbers()");
                fax = null;

                TextBox domain = DetailsViewCompany.FindControl("domain") as TextBox;
                domain.Attributes.Add("onkeypress", "javascript:return RestrictChar('[-_0-9a-zA-Z]')");
                domain = null;

                TextBox contactname = DetailsViewCompany.FindControl("contactname") as TextBox;
                contactname.Attributes.Add("onkeypress", "javascript:return RestrictChar()");
                contactname = null;
                
                //RenderJSArrayWithCliendIds(DetailsViewCompany.FindControl("rut"), txttest);

            }

        }
        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }

        protected void DetailsViewCompany_PreRender(object sender, EventArgs e)
        {
            if(DetailsViewCompany.CurrentMode!=DetailsViewMode.ReadOnly)
                RenderJSArrayWithCliendIds(DetailsViewCompany.FindControl("rut"), DetailsViewCompany.FindControl("name"), DetailsViewCompany.FindControl("address"), DetailsViewCompany.FindControl("phone"), DetailsViewCompany.FindControl("domain"));
        }

        protected void cmdModificar_Click(object sender, EventArgs e)
        {
            ObjectDataSourceCompany.Update();
        }

        protected void ObjectDataSourceCompany_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            try
            {
                
                e.InputParameters["rut"] = ((TextBox)DetailsViewCompany.FindControl("rut")).Text;
                e.InputParameters["address"] = ((TextBox)DetailsViewCompany.FindControl("address")).Text;
                e.InputParameters["name"] = ((TextBox)DetailsViewCompany.FindControl("name")).Text;
                e.InputParameters["phone"] = ((TextBox)DetailsViewCompany.FindControl("phone")).Text;
                e.InputParameters["phone2"] = ((TextBox)DetailsViewCompany.FindControl("phone2")).Text;
                e.InputParameters["domain"] = ((TextBox)DetailsViewCompany.FindControl("domain")).Text;
                e.InputParameters["fax"] = ((TextBox)DetailsViewCompany.FindControl("fax")).Text;
                e.InputParameters["contactname"] = ((TextBox)DetailsViewCompany.FindControl("contactname")).Text;
                e.InputParameters["status"] = ((DropDownList)DetailsViewCompany.FindControl("status")).SelectedItem.Value;
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyCompany.aspx al momento de insertar una compañía" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyCompany.aspx al momento de insertar una compañía";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void cmdInsertar_Click(object sender, EventArgs e)
        {
            ObjectDataSourceCompany.Insert();
        }

        protected void cmdEliminar_Click(object sender, EventArgs e)
        {
            ObjectDataSourceCompany.Delete();
        }

        protected void DetailsViewCompany_PageIndexChanging(object sender, DetailsViewPageEventArgs e)
        {

        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Core/Admin/ListCompany.aspx");
        }

        
    }
}