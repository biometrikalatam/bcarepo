﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using log4net;
using Bio.Portal.Server.Common.Common;
using System.Web.UI.HtmlControls;
using Bio.Portal.Server.Manager.Core.libs;
using System.Text;


namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class Configuration : Pages
    {
       
        private static readonly ILog log = LogManager.GetLogger(typeof(Configuration));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 1)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);

                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);
                if (!(Page.IsPostBack))
                {
                    //Inicializamos el soporte de javascript del sitio.
                    HtmlGenericControl myJs = new HtmlGenericControl();
                    myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                    myJs.Attributes.Add("language", "javascript");
                    //don't need it usually but for cross browser.
                    myJs.Attributes.Add("src", ResolveUrl("~/Scripts/menuVertical.js"));
                    this.Page.Header.Controls.Add(myJs);
                    myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                    myJs.Attributes.Add("language", "javascript");
                    //don't need it usually but for cross browser.
                    myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                    this.Page.Header.Controls.Add(myJs);

                    log.Debug("Se ingresa a visualizar las empresas usuario:" + UserNameFull);
                    Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                    Page.ClientScript.RegisterClientScriptInclude("JQUERY", ResolveClientUrl("../../Scripts/menuVertical.js"));

                    log.Info("Se ha ingresado a la Configuration.aspx");
                    DetailsViewConfig.DefaultMode = DetailsViewMode.Edit;

                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en Configuration.aspx en evento load" , exe);
                Session["mensajeerror"] = "Se ha producido un error en Configuration.aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
            
        }
        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }
        protected void ObjectDataSource1_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            try
            {
                //Se deben asociar los parámetros a los campos.
                IDictionary paramsFromPage = e.InputParameters;
                paramsFromPage.Clear();
                TextBox txtaux;

                txtaux = this.DetailsViewConfig.FindControl("txtInsertOption") as TextBox;
                paramsFromPage["insertOption"] = Convert.ToInt32(txtaux.Text);

                txtaux = this.DetailsViewConfig.FindControl("txtSaveVerified") as TextBox;
                paramsFromPage["saveVerified"] = Convert.ToInt32(txtaux.Text);

                txtaux = this.DetailsViewConfig.FindControl("txtMinutiaetypeDefault") as TextBox;
                paramsFromPage["minutiaetypeDefault"] = Convert.ToInt32(txtaux.Text);

                txtaux = this.DetailsViewConfig.FindControl("txtTimeoutWSdefault") as TextBox;
                paramsFromPage["timeoutWSdefault"] = Convert.ToInt32(txtaux.Text);

                txtaux = this.DetailsViewConfig.FindControl("txtMatchingtype") as TextBox;
                paramsFromPage["matchingtype"] = Convert.ToInt32(txtaux.Text);

                txtaux = this.DetailsViewConfig.FindControl("txtOperationOrder") as TextBox;
                paramsFromPage["operationOrder"] = Convert.ToInt32(txtaux.Text);

                paramsFromPage["minutiaetypeToEnroll"] = null;

                txtaux = this.DetailsViewConfig.FindControl("txtCheckAccess") as TextBox;
                paramsFromPage["checkAccess"] = Convert.ToInt32(txtaux.Text);

                txtaux = this.DetailsViewConfig.FindControl("txtConnectorDefault") as TextBox;
                paramsFromPage["connectorId"] = txtaux.Text;
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error al momento de modificar la configuración" , exe);
                Session["mensajeerror"] = "Se ha producido un error en Configuration.Aspx al momento de modificar los parámetros.";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }
        private int[] funcion(String[] a)
        {

            int[] ret = new int[a.Length];

            for (int i = 0; i < a.Length; i++)
            {
                ret[i] = int.Parse(a[i]);
            }

            return ret;

        }

        protected void ObjectDataSource1_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                if ((int)(e.ReturnValue) == Bio.Core.Constant.Errors.IERR_OK)
                {
                    MessageBox("La configuración fue guardada con éxito");
                    Response.Redirect("~/Welcome.aspx",true);
                }
                else
                {
                    log.Error("Se ha producido un error en Configuration.aspx al momento de actualizar parámetros de configuración [" + ((int)(e.ReturnValue)).ToString() + "]");
                    Session["mensajeerror"] = "Se ha producido un error en Configuration.aspx al momento de actualizar parámetros de configuración [" + ((int)(e.ReturnValue)).ToString() + "]";
                    Session["exception"] = null; // e.Exception;
                    Response.Redirect("~/Error.aspx");
                    //MessageBox("Existió un error al guardar la configuración. Consulte el log.");
                    //log.Debug("Error al grabar configuración [" + ((int)(e.ReturnValue)).ToString() + "]");
                }
            }
            else
            {
                log.Error("Se ha producido un error en Configuration.aspx al momento de actualizar parámetros de configuración" + e.Exception);
                Session["mensajeerror"] = "Se ha producido un error en Configuration.aspx al momento de actualizar parámetros de configuración";
                Session["exception"] = e.Exception;
                Response.Redirect("~/Error.aspx");
            }
        }
        private void MessageBox(string msg) {
                Label lbl = new Label();
                lbl.Text = "<script language='javascript'> window.alert('" + msg + "')</script>";
                Page.Controls.Add(lbl);
        }
        protected void DetailsViewConfig_PreRender(object sender, EventArgs e)
        {
            if (DetailsViewConfig.CurrentMode != DetailsViewMode.ReadOnly)
            {
                RenderJSArrayWithCliendIds(DetailsViewConfig.FindControl("txtInsertOption"),
                                           DetailsViewConfig.FindControl("txtSaveVerified"),
                                           DetailsViewConfig.FindControl("txtMinutiaetypeDefault"),
                                           DetailsViewConfig.FindControl("txtTimeoutWSdefault"),
                                           DetailsViewConfig.FindControl("txtMatchingtype"),
                                           DetailsViewConfig.FindControl("txtOperationOrder"),
                                           DetailsViewConfig.FindControl("txtCheckAccess"),
                                           DetailsViewConfig.FindControl("txtConnectorDefault"));
            }
        }

        protected void cmdModificar_Click(object sender, EventArgs e)
        {
            ObjectDataSource1.Update();
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Welcome.aspx", true);

        }
    }
}