﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using Bio.Portal.Server.Manager.Core.libs;
using System.Web.UI.HtmlControls;
using Bio.Portal.Server.Common.Common;
using System.Text;
using System.Web.Security;
using System.Drawing;

namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class ResetPassword :Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ResetPassword));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/menuVertical.js"));
                this.Page.Header.Controls.Add(myJs);
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 2)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);

                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);

                log.Debug("Se ingresa a resetear la password:" + UserNameFull);
                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                Page.ClientScript.RegisterClientScriptInclude("JQUERY", ResolveClientUrl("../../Scripts/menuVertical.js"));
                RenderJSArrayWithCliendIds(txtPassword);
                if (!(Page.IsPostBack))
                {
                    if (Session["iduser"] != null)
                    {
                        int id = Convert.ToInt32(Session["iduser"]);
                        String user = Session["user"].ToString();
                        txtusername.Text = user;
                        txtusername.Enabled = false;
                    }
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ResetPassword.aspx en el reset del password de un usuario" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ResetPassword.aspx en el reset del password de un usuario";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }
        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }

        protected void cmdCambiar_Click(object sender, EventArgs e)
        {
            try
            {

                MembershipUser mu = Membership.GetUser(txtusername.Text);
                bool respuesta = mu.ChangePassword(mu.ResetPassword("password"), this.txtPassword.Text);
                if (respuesta == true)
                {
                    lblmensaje.Text = "La password ha sido cambiada con éxito";
                    lblmensaje.Visible = true;
                    lblmensaje.ForeColor = Color.Blue;
                }
                else
                {
                    lblmensaje.Text = "La password no pudo ser cambiada";
                    lblmensaje.Visible = true;
                    lblmensaje.ForeColor = Color.Red;
                }
            }
            catch (Exception exe)
            {
           
                log.Error("Se ha producido un error en ModifyOrigin.aspx  al cambiar el password de un usuario" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyOrigin.aspx al cambiar el password de un usuario";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Core/Admin/ListUsers.aspx",true);
        }
    }
}