﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="ListLogTx.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Admin.ListLogTx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <asp:ObjectDataSource ID="ObjectDataSourceOrigen" runat="server" 
    SelectMethod="RetriveAllOrigen" 
    TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper">
</asp:ObjectDataSource>
<table cellpadding="0" cellspacing="0" border="0" width="90%" align="center">
    <tr>
        <td colspan="4" class="titulos0">
            Filtro para Visualizar Log de Transacciones
         </td>
    </tr>
     <tr>
         <td class="tdBottom"><asp:Label ID="LabelCompanyLab" name="LabelCompany" runat="server" Text="Compañía"></asp:Label></td>
        <td class="tdBottom"><asp:Label ID="LabelCompanyLab1" name="LabelCompany1" runat="server" Text=":"></asp:Label></td>
        <td class="tdBottom">
            <asp:ObjectDataSource ID="ObjectDataSourceCompany" runat="server" 
                SelectMethod="RetrieveAllCompanyActive" 
                TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper">
            </asp:ObjectDataSource>
                <asp:DropDownList ID="DropDownListCompany" runat="server" 
                DataTextField="Name" DataSourceID="ObjectDataSourceCompany"  DataValueField="Id" 
                ondatabound="DropDownListCompany_DataBound">
            </asp:DropDownList>
            <asp:Label ID="LabelCompany" name="LabelCompany" runat="server"></asp:Label>
            <asp:HiddenField ID="HiddenCompany" Value="0" runat="server" />
        </td>
        <td class="tdBottom"></td>
    </tr>
     <tr>
        <td class="tdBottom"># TX BP</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox ID="txtNroTrackId" runat="server" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
        </td>
        <td class="tdBottom"></td>
    </tr>
    <tr>
        <td class="tdBottom">TrackId BP</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox ID="txtIdtx" runat="server" Width="220"></asp:TextBox>
        </td>
        <td class="tdBottom"></td>
    </tr>
    <tr>
        <td class="tdBottom">Origen</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:DropDownList ID="DropDownListOrigen" runat="server" 
                DataSourceID="ObjectDataSourceOrigen" DataTextField="Description" 
                DataValueField="Id" ondatabound="DropDownListOrigen_DataBound">
            </asp:DropDownList>
            
        </td>
        <td class="tdBottom"></td>
    </tr>
    <tr>
        <td class="tdBottom">Tipo Id</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:DropDownList ID="DropDownListDocumento" runat="server" 
                ondatabound="DropDownListDocumento_DataBound">
            </asp:DropDownList>
        </td>
        <td class="tdBottom"></td>
    </tr>
    <tr>
        <td class="tdBottom">Valor Id</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox ID="txtNroDocumento" runat="server" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
        </td>
        <td class="tdBottom"></td>
    </tr>
    <tr>
        <td class="tdBottom">Tipo de Acción</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:DropDownList ID="DropDownListOperations" runat="server" 
                ondatabound="DropDownListOperations_DataBound"></asp:DropDownList>
        </td>
        <td class="tdBottom"></td>
    </tr>
    <tr>
        <td class="tdBottom" colspan="2">Timestamp</td>
        <td class="tdBottom"><asp:CheckBox ID="chkfecha" runat="server" />Sin filtro de fecha</td>     
        <td class="tdBottom"></td>   
    </tr>
    <tr>
        <td class="tdBottom" >Fecha Desde/Hasta</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom" valign="middle">
            <asp:TextBox ID="txtfecha" runat="server" onfocus="javascript:vDateType='3'" onkeyup="javascript:DateFormat(document.getElementById(MyClientID[0]),document.getElementById(MyClientID[0]).value,event,false,'3')" onBlur="javascript:DateFormat(document.getElementById(MyClientID[0]),document.getElementById(MyClientID[0]).value,event,true,'3');ValidarFecha(document.getElementById(MyClientID[0]),document.getElementById(MyClientID[0]).value)"></asp:TextBox>
            <asp:Button runat="server" ID="cmdCalendario"  SkinID="BottonCal" UseSubmitBehavior="False" />
        </td>
        <td class="tdBottom" colspan="1" valign="bottom">
            <asp:TextBox ID="txtfechah" runat="server" onfocus="javascript:vDateType='3'" onkeyup="javascript:DateFormat(document.getElementById(MyClientID[1]),document.getElementById(MyClientID[1]).value,event,false,'3')" onBlur="javascript:DateFormat(document.getElementById(MyClientID[1]),document.getElementById(MyClientID[1]).value,event,true,'3');ValidarFecha(document.getElementById(MyClientID[0]),document.getElementById(MyClientID[0]).value)"></asp:TextBox>
            <asp:Button runat="server" ID="cmdCalendarioH"  Text="" SkinID="BottonCal" 
                UseSubmitBehavior="False" />
        </td>
    </tr>
    <tr>
        <td colspan="4" class="titulos0">Conexión Externa</td>
    </tr>
    <tr>
        <td bgcolor="LightGreen" class="tdBottom" >Connector Id</td>
        <td bgcolor="LightGreen" class="tdBottom">:</td>
        <td bgcolor="LightGreen" class="tdBottom">
            <asp:TextBox runat="server" Width="220" ID="ConnectorId" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
        </td>
        <td bgcolor="LightGreen" class="tdBottom"></td>
    </tr>
    <tr>
        <td bgcolor="LightGreen" class="tdBottom" >TrackId</td>
        <td bgcolor="LightGreen" class="tdBottom">:</td>
        <td bgcolor="LightGreen" class="tdBottom">
            <asp:TextBox runat="server" Width="220" ID="txtIdRC" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
        </td>
        <td bgcolor="LightGreen" class="tdBottom"></td>
    </tr>
    <tr>
        <td bgcolor="LightGreen" class="tdBottom">Estado</td>
        <td bgcolor="LightGreen" class="tdBottom">:</td>
        <td bgcolor="LightGreen" class="tdBottom">
            <asp:DropDownList runat="server" ID="DropDownListEstado" 
                ondatabound="DropDownListEstado_DataBound"></asp:DropDownList>
        </td>
        <td bgcolor="LightGreen" class="tdBottom"></td>
    </tr>
    <tr>
        <td bgcolor="LightGreen" class="tdBottom">Resultado</td>
        <td bgcolor="LightGreen" class="tdBottom">:</td>
        <td bgcolor="LightGreen" class="tdBottom">
            <asp:DropDownList runat="server" ID="DropDownListResultado">
                <asp:ListItem Value="0">Todos</asp:ListItem>
                <asp:ListItem Value="1">RESULTADO_OK</asp:ListItem>
                <asp:ListItem Value="2">RESULTADO_NOOK</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td bgcolor="LightGreen" class="tdBottom"></td>
    </tr>
     <tr>
        <td class="" colspan="4"> </td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:Button runat="server" ID="cmdVerTx" value="Buscar" 
                onclick="cmdVerTx_Click" Text="Buscar" SkinID="BotonAccion" BorderStyle="None" />
        </td>
    </tr>
</table>
</asp:Content>
