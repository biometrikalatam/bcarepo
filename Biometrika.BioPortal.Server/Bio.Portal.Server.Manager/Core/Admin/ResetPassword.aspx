﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Admin.ResetPassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="70%" align="center">
    <tr>
        <td colspan="3" class="titulos0">Cambio de password para usuario</td>
    </tr>
    <tr>
        <td>
            <asp:Label runat="server" ID="lblmensaje" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">User Name</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" id="txtusername" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Nuevo Password</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" id="txtPassword" MaxLength="15" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="tdBottom" align="left" colspan="3">
            <asp:Button runat="server" ID="cmdCambiar" Text="Cambiar" BorderStyle="None" 
                SkinID="BotonAccion" 
                OnClientClick="javascript:return ValidarFormularioPassword();" 
                onclick="cmdCambiar_Click" />
            <asp:Button runat="server" ID="cmdCancelar" Text="Cancelar" BorderStyle="None" 
                SkinID="BotonAccion" onclick="cmdCancelar_Click" />
        </td>
    </tr>
</table>
</asp:Content>
