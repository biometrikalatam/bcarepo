﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Web.UI.HtmlControls;
using Bio.Portal.Server.Manager.Core.libs;
using Bio.Portal.Server.Common.Common;
using System.Text;
using System.Collections;

namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class ListLog : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ListLog));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();

                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);
                HtmlGenericControl myJs2 = new HtmlGenericControl();
                myJs2.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs2.Attributes.Add("language", "javascript");
                myJs2.Attributes.Add("src", ResolveUrl("~/Scripts/dhtmlgoodies_calendar.js"));
                this.Page.Header.Controls.Add(myJs2);

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 3)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);
                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);

                log.Debug("Se ingresa a visualizar las empresas usuario:" + UserNameFull);
                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                RenderJSArrayWithCliendIds(txtfecha, txtfechah, fechadesdeaux, fechahastaaux);
                cmdCalendario.Attributes.Add("onclick", "javascript:displayCalendar(" + txtfecha.ClientID + ",'dd/mm/yyyy',this);return false;");
                cmdCalendarioH.Attributes.Add("onclick", "javascript:displayCalendar(" + txtfechah.ClientID + ",'dd/mm/yyyy',this);return false;");
                //txtfecha.Attributes.Add("onkeyup", "javascript:DateFormat(" + this + "," + txtfecha.Text + ",event,false,'3');"); 
                //onBlur="DateFormat(this,this.value,event,true,'3')"

                Page.ClientScript.RegisterClientScriptInclude("JQUERY", ResolveClientUrl("../../Scripts/menuVertical.js"));
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ListLog.aspx" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ListLog.aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }

        protected void cmdVerLog_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList criterio = new ArrayList();
                criterio.Add(DropDownListLevel.SelectedItem.Value);
                if (txtfecha.Text.Trim().Length == 0)
                    criterio.Add("");
                else
                    criterio.Add(fechadesdeaux.Value);

                if (txtfechah.Text.Trim().Length == 0)
                    criterio.Add("");
                else
                    criterio.Add(fechahastaaux.Value);
                Session["params"] = criterio;
                Response.Redirect("VerLog.aspx");
            }
            catch (Exception exe)
            {
                if (exe.GetType().FullName != "System.Threading.ThreadAbortException")
                {
                    log.Error("Se ha producido un error en ListLog.aspx al momento de visualizar los logs" , exe);
                    Session["mensajeerror"] = "Se ha producido un error en ListLog.aspx al momento de visualizar los logs";
                    Session["exception"] = exe;
                    Response.Redirect("~/Error.aspx");
                }
            }
            
        }
    }
}