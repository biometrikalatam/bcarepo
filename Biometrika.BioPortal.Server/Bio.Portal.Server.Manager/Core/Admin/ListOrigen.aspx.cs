﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Web.UI.HtmlControls;
using Bio.Portal.Server.Manager.Core.libs;
using System.Text;
using Bio.Portal.Server.Common.Common;

namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class ListOrigen : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ListLog));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();

                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);
                HtmlGenericControl myJs2 = new HtmlGenericControl();
                myJs2.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs2.Attributes.Add("language", "javascript");
                myJs2.Attributes.Add("src", ResolveUrl("~/Scripts/dhtmlgoodies_calendar.js"));
                this.Page.Header.Controls.Add(myJs2);
                RenderJSArrayWithCliendIds(GridViewOrigen, indice);
                log.Debug("Se ingresa a visualizar las empresas usuario:" + UserNameFull);

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 2)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);
                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);

                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");

                if (!(Page.IsPostBack))
                {
                    Session["id"] = null;
                    Session["operacion"] = null;
                    ObjectDataSourceOrigen2.Select();
                    if (Session["mensaje"] != null)
                    {
                        lblMensaje.Text = "El origen fue modificado ";
                        lblMensaje.Visible = true;
                    }
                    else
                    {
                        lblMensaje.Text = "";
                        lblMensaje.Visible = false;
                    }
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en LisOrigen.aspx" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ListOrigen.aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
           
        }

        protected void ObjectDataSourceOrigen2_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(e.ReturnValue) == true)
                {
                    GridViewOrigen.DataSource = e.OutputParameters["result"];
                    GridViewOrigen.DataBind();
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ListOrigen.aspx en la seleccion de origenes" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ListOrigen.aspx en la seleccion de origenes";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }
        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }
        protected void GridViewOrigen_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "Sort")
                {
                    if (e.CommandName == "Edit")
                    {
                        string id = indice.Value;
                        log.Debug("El usuario solicita modificar origen : " + Username);
                        Session["id"] = id;
                        Session["operacion"] = "modificar";
                        Response.Redirect("ModifyOrigin.aspx");
                    }
                    if (e.CommandName == "Delete")
                    {
                        string id = indice.Value;
                        log.Debug("El usuario solicita eliminar origen: " + Username);
                        Session["id"] = id;
                        Session["operacion"] = "eliminar";
                        Response.Redirect("ModifyOrigin.aspx");
                    }
                }
            }
            catch (Exception exe)
            {
                if (exe.GetType().FullName != "System.Threading.ThreadAbortException")
                {
                    log.Error("Se ha producido un error en ListOrigen.aspx en la ejecución de una acción sobre la grilla." , exe);
                    Session["mensajeerror"] = "Se ha producido un error en ListOrigen.aspx en la ejecución de una acción sobre la grilla";
                    Session["exception"] = exe;
                    Response.Redirect("~/Error.aspx");
                }
            }
        }
        

       

     
        protected void GridViewOrigen_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void cmdAgregar_Click(object sender, EventArgs e)
        {
            Session["operacion"] = "agregar";
            Response.Redirect("ModifyOrigin.aspx");
        }


        protected void GridViewOrigen_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                ObjectDataSourceOrigen2.Select();
                GridViewOrigen.PageIndex = e.NewPageIndex;
                GridViewOrigen.DataBind();
                int i = 0;
            }
            catch (Exception exe)
            {
                if (exe.GetType().FullName != "System.Threading.ThreadAbortException")
                {
                    log.Error("Se ha producido un error en ListOrigen.aspx en la ejecución de una acción sobre la grilla." , exe);
                    Session["mensajeerror"] = "Se ha producido un error en ListOrigen.aspx en la ejecución de una acción sobre la grilla";
                    Session["exception"] = exe;
                    Response.Redirect("~/Error.aspx");
                }
            }
        }

    }
}