﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="VerTx.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Admin.VerTx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="titulos0">Resultado de Log de Transacciones</td>
    </tr>
    <tr>
        <td align="left"> 
            <asp:Button runat="server" ID="cmdVolver" value="Volver" SkinID="BotonAccion" BorderStyle="None" 
                Text="Volver" onclick="cmdVolver_Click" />
        </td>
    </tr>
    <tr>
        <td class="">
            <asp:ObjectDataSource ID="ObjectDataSourceTx" runat="server" 
                onselecting="ObjectDataSourceTx_Selecting" SelectMethod="RetrieveTx" 
                TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper">
                <SelectParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                    <asp:Parameter Name="trackid" Type="String" />
                    <asp:Parameter Name="origin" Type="Int32" />
                    <asp:Parameter Name="typeid" Type="String" />
                    <asp:Parameter Name="valueid" Type="String" />
                    <asp:Parameter Name="actiontype" Type="String" />
                    <asp:Parameter Name="idconx" Type="String" />
                    <asp:Parameter Name="statusconx" Type="Int32" />
                    <asp:Parameter Name="resultconx" Type="Int32" />
                    <asp:Parameter Name="timestamprcdesde" Type="String" />
                    <asp:Parameter Name="timestamprchasta" Type="String" />
                    <asp:Parameter Name="companyid" Type="Int32" />
                    <asp:Parameter Name="connectorid" Type="String" />
                    <asp:Parameter Direction="Output" Name="msg" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:GridView ID="GridViewTx" runat="server" DataSourceID="ObjectDataSourceTx" SkinId="GridView1" 
                EnableModelValidation="True" OnDataBound="GridViewTx_DataBound" >
            </asp:GridView>
        </td>
    </tr>
</table>
</asp:Content>
