﻿using System;
using System.Web.UI.WebControls;
using Bio.Portal.Server.Common.Entities;
using Bio.Portal.Server.Manager.Core.Database;
using Bio.Portal.Server.Common.BioMembership;
using Bio.Portal.Server.Common.Common;
using System.Web.Security;
using Bio.Portal.Server.Manager.Core.libs;
using log4net;
using System.Web.UI.HtmlControls;


namespace Bio.Portal.Server.Manager.Core.Admin
{
    public partial class CreateUser : Pages
    {
       
        private static readonly ILog log = LogManager.GetLogger(typeof(CreateUser));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session != null)
                {
                    Session["mensajeerror"] = null;
                    Session["exception"] = null;
                }
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/menuVertical.js"));
                this.Page.Header.Controls.Add(myJs);
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);

                //TextBox Phone = (TextBox)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Phone");
                //Phone.Attributes.Add("onkeypress", "javascript:return onlyNumbers()");

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 2)
                {
                    Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    Session["exception"] = null;
                    Response.Redirect("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);

                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);

                log.Debug("Se ingresa a visualizar las empresas usuario:" + UserNameFull);
                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                Page.ClientScript.RegisterClientScriptInclude("JQUERY", ResolveClientUrl("../../Scripts/menuVertical.js"));

                log.Debug("Entramos a la página de creación de usuarios");
                if (!Page.IsPostBack)
                {
                    string domain = "";
                    if (Request["user"] != null)
                        domain = Global.DomainApp;
                    if (User.IsInRole("Super Administrador"))
                        ((PlaceHolder)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("empresas")).Visible = true;
                    else
                    {
                        ((PlaceHolder)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("empresas")).Visible = false;
                        ((HiddenField)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("HiddenIDCompany")).Value = CompanyId.ToString();
                    }
                    ((DropDownList)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("DropDownListCompany")).DataSource = DatabaseHelper.RetrieveAllCompanyActive(domain);
                    ((DropDownList)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("DropDownListCompany")).DataBind();
                    try
                    {
                        string[] roles = Roles.GetAllRoles();

                        if (User.IsInRole("Super Administrador"))
                            ((CheckBoxList)CreateUserExWizard1.WizardSteps[1].FindControl("chlrol")).DataSource = roles;
                        else
                        {
                            int j = 0;
                            string[] rolesempresas = new string[roles.Length - 1];
                            for (int i = 0; i < roles.Length; i++)
                            {
                                if (roles[i] == "Super Administrador") continue;
                                rolesempresas[j] = roles[i];
                                j++;
                            }
                            ((CheckBoxList) CreateUserExWizard1.WizardSteps[1].FindControl("chlrol")).DataSource = rolesempresas;
                        }
                        ((CheckBoxList)CreateUserExWizard1.WizardSteps[1].FindControl("chlrol")).DataBind();
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message);
                    }
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en CreateUser.aspx.cs en evento Load" , exe);
                Session["mensajeerror"] = "Se ha producido un error en CreateUser.Aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }

        }
        /// <summary>
        /// Método del control  Wizard para la creación de usuarios. Es un control extendido, personalizado en el paquete Bio.Portal.Server.Common.        
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
      

        protected void CreateUserExWizard1_CreateUserEx(object sender, CreateUserExEventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    TextBox UserName = (TextBox)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("UserName");
                    TextBox Email = (TextBox)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Email");
                    TextBox Question = (TextBox)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Question");

                    //TextBox Name = (TextBox)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Name");
                    //TextBox Surname = (TextBox)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Surname");
                    DropDownList Company = (DropDownList)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("DropDownListCompany");
                    //TextBox Phone = (TextBox)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Phone");
                    //TextBox PostalCode = (TextBox)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("PostalCode");

                    // now, we're ready to create the user
                    User user = new User();

                    // get the membership provider
                    CustomMembershipProvider membershipProvider = (CustomMembershipProvider)Membership.Providers["BioCustomProviderFull"];

                    if (membershipProvider == null)
                    {
                        throw new Exception("NHCustomProviderFull has not been configured in the web.config file");
                    }

                    // set the user data that can be accessed by the membership provider

                    user.Password = e.EncodedPassword;
                    user.PasswordSalt = e.EncodedPasswordSalt;
                    user.PasswordFormat = membershipProvider.PasswordFormat;
                    user.PasswordQuestion = Question.Text;
                    user.PasswordAnswer = e.EncodedAnswer;
                    user.Email = Email.Text;
                    user.CreationDate = user.LastActivityDate = DateTime.Now;
                    user.IsApproved = !CreateUserExWizard1.DisableCreatedUser;

                    // set the other data
                    String msgErr;
                    TextBox Name = (TextBox)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Name");
                    user.Name = Name != null ? Name.Text : null;
                    TextBox SurName = (TextBox)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Surname");
                    user.Surname = SurName != null ? SurName.Text : null;
                    //TextBox Name = (TextBox)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Name");
                    user.Company = "";

                    TextBox Phone = (TextBox)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Phone");
                    user.Phone = Phone != null ? Phone.Text : null;
                    TextBox PostalCode = (TextBox)CreateUserExWizard1.CreateUserStep.ContentTemplateContainer.FindControl("PostalCode");
                    user.PostalCode = PostalCode != null ? PostalCode.Text : null;
                    user.CompanyId = AdminBpCompany.GetCompanyById(Convert.ToInt32(Company.SelectedValue), out msgErr);
                    user.UserName = UserName.Text + "@" + user.CompanyId.Domain;

                    AdminBpUsers.AddUser(user);
                    Session["usernameaux"] = user.UserName;

                    if (!Roles.RoleExists("Super Administrador"))
                        Roles.CreateRole("Super Administrador");
                    if (!Roles.RoleExists("Administrador"))
                        Roles.CreateRole("Administrador");
                    if (!Roles.RoleExists("Oficial Auditor"))
                        Roles.CreateRole("Oficial Auditor");
                    if (!Roles.RoleExists("Enrolador"))
                        Roles.CreateRole("Enrolador");
                    if (!Roles.RoleExists("Verificador"))
                        Roles.CreateRole("Verificador");

                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en CreateUserExWizard1_CreateUserEx" , exe);
                Session["mensajeerror"] = "Se ha producido un error en CreateUser.Aspx al momento de Crear un usuario";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void CreateUserExWizard1_CreatedUser(object sender, EventArgs e)
        {
            if (Request["user"] == null)
                CreateUserExWizard1.ActiveStepIndex = 1;
            else
            {
                //Lo asociamos al rol superadministrador
                Roles.AddUserToRole(Session["usernameaux"].ToString(), "Super Administrador");
                Session["usernameaux"] = "";
                //lo derivamos al wizard step complete.
                CreateUserExWizard1.ActiveStepIndex = 2;
            }

        }
        public void AssignUserToRoles_Deactivate(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < ((CheckBoxList)CreateUserExWizard1.WizardSteps[1].FindControl("chlrol")).Items.Count; i++)
                {
                    if (((CheckBoxList)CreateUserExWizard1.WizardSteps[1].FindControl("chlrol")).Items[i].Selected)
                        Roles.AddUserToRole(Session["usernameaux"].ToString(), ((CheckBoxList)CreateUserExWizard1.WizardSteps[1].FindControl("chlrol")).Items[i].Value);
                }
                Session["usernameaux"] = "";
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en la asigancion de roles" , exe);
                Session["mensajeerror"] = "Se ha producido un error en CreateUser.Aspx al momento de asignar roles";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }
       
       

     
    }
}