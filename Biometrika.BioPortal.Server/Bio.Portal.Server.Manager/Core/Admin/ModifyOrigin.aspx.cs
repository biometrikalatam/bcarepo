﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Web.UI.HtmlControls;
using Bio.Portal.Server.Manager.Core.libs;
using Bio.Portal.Server.Common.Common;
using System.Text;

namespace Bio.Portal.Server.Manager.Core.Admin
{

    public partial class ModifyOrigin : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ModifyOrigin));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();

                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);
                HtmlGenericControl myJs2 = new HtmlGenericControl();
                myJs2.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs2.Attributes.Add("language", "javascript");
                myJs2.Attributes.Add("src", ResolveUrl("~/Scripts/dhtmlgoodies_calendar.js"));
                this.Page.Header.Controls.Add(myJs2);

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 2)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);
                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);

                log.Debug("Se ingresa a form de ModifyOrigin:" + UserNameFull);

                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");

                if (!(Page.IsPostBack))
                {
                    if (Session["id"] != null)
                    {
                        if (Session["operacion"] != null)
                        {
                            ObjectDataSourceOrigin.Select();

                            if (Session["operacion"].Equals("modificar"))
                            {
                                lbltitulo.Text = "Modificar Origen";
                                DetailsViewOrigin.DefaultMode = DetailsViewMode.Edit;
                                RenderJSArrayWithCliendIds(DetailsViewOrigin.FindControl("txtId"), DetailsViewOrigin.FindControl("txtDescripcion"));
                                TextBox txtid = (TextBox)DetailsViewOrigin.FindControl("txtId");
                                if (txtid != null) {
                                    txtid.ReadOnly = true;
                                }
                            }

                            else if (Session["operacion"].Equals("eliminar"))
                            {
                                lbltitulo.Text = "Eliminar Origen";
                                DetailsViewOrigin.DefaultMode = DetailsViewMode.ReadOnly;

                            }
                        }
                    }
                    else
                    {
                        if (Session["operacion"].Equals("agregar"))
                        {
                            lbltitulo.Text = "Agregar Origen";
                            DetailsViewOrigin.DefaultMode = DetailsViewMode.Insert;
                            RenderJSArrayWithCliendIds(DetailsViewOrigin.FindControl("txtId"), DetailsViewOrigin.FindControl("txtDescripcion"));
                        }
                    }
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyOrigin.aspx" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyOrigin.aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceOrigin_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            try
            {
                if (Session["id"] != null)
                {
                    e.InputParameters["Id"] = Convert.ToInt32(Session["id"]);
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyOrigin.aspx al momento de seleccionar un origen" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyOrigin.aspx al momento de seleccionar un origen";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }
        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }
        protected void ObjectDataSourceOrigin_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue != null)
            {
                log.Debug("Se seleccionó correctamente el Origen ID:" + Session["id"]);
            }
        }

        protected void cmdModificar_Click(object sender, EventArgs e)
        {
            ObjectDataSourceOrigin.Update();
        }

        protected void ObjectDataSourceOrigin_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            try
            {
                e.InputParameters["id"] = Convert.ToInt32(((TextBox)DetailsViewOrigin.FindControl("txtId")).Text);
                e.InputParameters["idori"] = Convert.ToInt32(Session["id"].ToString());
                e.InputParameters["descripcion"] = ((TextBox)DetailsViewOrigin.FindControl("txtDescripcion")).Text;
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyOrigen.aspx al momento de realizar una modificación de origen" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyOrigen.aspx al momento de realizar una modificación de origen";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceOrigin_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                if (Convert.ToBoolean(e.ReturnValue) == false)
                {
                    log.Error("No se ha podido modificar el origen porque crearía valores duplicados");
                    lblMensaje.Text = Bio.Core.Constant.Errors.GetDescription(Bio.Core.Constant.Errors.IERR_DUPLICATE_ORIGIN);
                    lblMensaje.Visible = true;
                }
                else
                {
                    Session["Error"] = Bio.Core.Constant.Errors.IERR_OK;
                    Response.Redirect("ListOrigen.aspx");

                }
            }
            else
            {
           
                log.Error("Se ha producido un error en ModifyOrigin.aspx el momento de realizar la modificación de una compañía" + e.Exception);
                Session["mensajeerror"] = "Se ha producido un error en ModifyOrigin.aspx el momento de realizar la modificación de una compañía";
                Session["exception"] = e.ReturnValue;
                Response.Redirect("~/Error.aspx");
            
            }
        }

        protected void cmdEliminar_Click(object sender, EventArgs e)
        {
            ObjectDataSourceOrigin.Delete();
        }

        protected void ObjectDataSourceOrigin_Deleting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            try
            {
                e.InputParameters["id"] = Convert.ToInt32(Session["id"].ToString());
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyOrigin.aspx la eliminación de un origen" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyOrigin.aspx la eliminación de un origen";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceOrigin_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                if (Convert.ToBoolean(e.ReturnValue) == true)
                {
                    Session["Error"] = Bio.Core.Constant.Errors.IERR_OK;
                }
                else
                {
                    Session["Error"] = Bio.Core.Constant.Errors.GetDescription(Bio.Core.Constant.Errors.IERR_DELETE_ORIGIN);
                    //lblMensaje.Text = "Operacion completada con exito";
                    //lblMensaje.Visible = true;
                }
                Response.Redirect("~/Core/Admin/ListOrigen.aspx");   
            }
            else
            {
                log.Error("Se ha producido un error en ModifyOrigin.aspx la eliminación de un origen" + e.ReturnValue);
                Session["mensajeerror"] = "Se ha producido un error en ModifyOrigin.aspx la eliminación de un origen";
                Session["exception"] = e.ReturnValue;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceOrigin_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            try
            {
                e.InputParameters["id"] =Convert.ToInt32(((TextBox)DetailsViewOrigin.FindControl("txtId")).Text);
                e.InputParameters["descripcion"] = ((TextBox)DetailsViewOrigin.FindControl("txtDescripcion")).Text;
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en ModifyOrigin.aspx la inserción de un origen" , exe);
                Session["mensajeerror"] = "Se ha producido un error en ModifyOrigin.aspx la inserción de un origen";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceOrigin_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                //if (Convert.ToBoolean(e.ReturnValue) == true)
                if (Convert.ToBoolean(!(e.ReturnValue).Equals(null)) == true)
                {
                    Session["Error"] = Bio.Core.Constant.Errors.IERR_OK;
                    Response.Redirect("ListOrigen.aspx");
                }
                else
                {
                    lblMensaje.Text = Bio.Core.Constant.Errors.GetDescription(Bio.Core.Constant.Errors.IERR_DELETE_ORIGIN);
                    lblMensaje.Visible = true;
                }
            }
            else
            {
                
                log.Error("Se ha producido un error en ModifyOrigin.aspx la inserción de un origen" + e.ReturnValue);
                Session["mensajeerror"] = "Se ha producido un error en ModifyOrigin.aspx la inserción de un origen";
                Session["exception"] = e.ReturnValue;
                Response.Redirect("~/Error.aspx");
            
            }
        }

        protected void cmdInsertar_Click(object sender, EventArgs e)
        {
            ObjectDataSourceOrigin.Insert();
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Core/Admin/ListOrigen.aspx", true);
        }
    }
}