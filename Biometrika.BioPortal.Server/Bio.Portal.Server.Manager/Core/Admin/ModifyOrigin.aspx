﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="ModifyOrigin.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Admin.ModifyOrigin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<table border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td class="titulos0">
            <asp:Label runat="server" ID="lbltitulo"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label runat="server" ID="lblMensaje" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="">
            <asp:ObjectDataSource ID="ObjectDataSourceOrigin" runat="server" 
                onselected="ObjectDataSourceOrigin_Selected" 
                onselecting="ObjectDataSourceOrigin_Selecting" SelectMethod="RetriveOrigen" 
                TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper" 
                onupdated="ObjectDataSourceOrigin_Updated" 
                onupdating="ObjectDataSourceOrigin_Updating" UpdateMethod="Modify" 
                DeleteMethod="DeleteOrigen" ondeleted="ObjectDataSourceOrigin_Deleted" 
                ondeleting="ObjectDataSourceOrigin_Deleting" InsertMethod="Create" 
                oninserted="ObjectDataSourceOrigin_Inserted" 
                oninserting="ObjectDataSourceOrigin_Inserting">
                <DeleteParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                    <asp:Parameter Direction="Output" Name="msgErr" Type="String" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                    <asp:Parameter Name="descripcion" Type="String" />
                    <asp:Parameter Direction="Output" Name="msgErr" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                    <asp:Parameter Direction="Output" Name="msgErr" Type="String" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                    <asp:Parameter Name="descripcion" Type="String" />
                    <asp:Parameter Name="idori" Type="Int32" />
                    <asp:Parameter Direction="Output" Name="msgErr" Type="String" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </td>       
    </tr>
    <tr>
        <td class="">
            
            <asp:DetailsView ID="DetailsViewOrigin" runat="server" Height="50px" 
                Width="229px" AutoGenerateRows="False" DataSourceID="ObjectDataSourceOrigin" 
                EnableModelValidation="True" style="margin-left: 0px" SkinID="DetailsViewIngreso1" >
                <Fields>
                    <asp:TemplateField HeaderText="Id" SortExpression="Id">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtId" runat="server" Text='<%# Bind("Id") %>' OnKeyPress="return  onlyNumbers(event)"></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="txtId" runat="server" Text='<%# Bind("Id") %>' OnKeyPress="return  onlyNumbers(event)"></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" SortExpression="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Description") %>' OnKeyPress="return RestrictChar()"></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Description") %>' OnKeyPress="return RestrictChar()"></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:Button runat="server" ID="cmdModificar" Text="Modificar" SkinID="BotonAccion" 
                                OnClientClick="javascript:return ValidarFormularioOrigenIngreso();" onclick="cmdModificar_Click" BorderStyle="None" 
                                />
                            <asp:Button runat="server" ID="cmdCancelar" BorderStyle="None" Text="Cancelar" SkinID="BotonAccion" onclick="cmdCancelar_Click" />
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:Button runat="server" ID="cmdInsertar" Text="Grabar" SkinID="BotonAccion"
                             OnClientClick="javascript:return ValidarFormularioOrigenIngreso();" BorderStyle="None" onclick="cmdInsertar_Click" 
                                />                            
                             <asp:Button runat="server" ID="cmdCancelar" BorderStyle="None" Text="Cancelar" SkinID="BotonAccion" onclick="cmdCancelar_Click" />
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Button runat="server" ID="cmdEliminar" Text="Eliminar" SkinID="BotonAccion" 
                                OnClientClick="javascript:return ValidarEliminarOrigen();" BorderStyle="None"
                                onclick="cmdEliminar_Click" />
                            <asp:Button runat="server" ID="cmdCancelar" Text="Cancelar" SkinID="BotonAccion" BorderStyle="None" onclick="cmdCancelar_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Fields>
            </asp:DetailsView>
        </td>
    </tr>
</table>
</asp:Content>
