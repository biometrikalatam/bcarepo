﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using Bio.Portal.Server.Common.BioMembership;

namespace Bio.Portal.Server.Manager.Core.libs   
{
    public static class Utils
    {
        /// <summary>
        /// Método que determina si el usuario a iniciado sesión o no.
        /// </summary>
        /// <param name="sesion"></param>
        /// <param name="response"></param>
        /// <param name="urlredirect"></param>
        public static void IsSessionValid(System.Web.SessionState.HttpSessionState sesion,
                                  HttpResponse response,
                                  string urlredirect)
        {
            if (sesion["UserName"] == null)
                response.Redirect(urlredirect);
        }

       
    }
}