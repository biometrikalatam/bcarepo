﻿using System;
using System.Collections.Generic;
using System.Web;
using log4net;
using Bio.Portal.Server.Common.Entities.Database;
using Bio.Portal.Server.Common.Entities;
using System.Collections;
using Bio.Portal.Server.Common.BioMembership;
using System.Web.Security;
using Bio.Portal.Server.Common.Config;
using Bio.Core.Constant;
using System.Data;

namespace Bio.Portal.Server.Manager.Core.Database
{
    /// <summary>
    /// Clase destinada a resumir todas las rutinas referidas a accesos a BD.
    /// </summary>
    public class DatabaseHelper
    {
       private static readonly ILog LOG = LogManager.GetLogger(typeof(DatabaseHelper));
       
        #region Métodos estáticos asociados a BpCompany

       /// <summary>
        /// Método que permite recibir todas las compañías activas
        /// </summary>
        /// <returns></returns>
        public static  IList<BpCompany> RetrieveAllCompanyActive()
        {
            string msgerr = "";
            return ((IList<BpCompany>)AdminBpCompany.GetCompanyActive(out  msgerr));
        }
        /// <summary>
        /// Método que permite traer las compañías según criterios en el buscador de compañías.
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="razonsocial"></param>
        /// <param name="estado"></param>
        /// <returns></returns>
        public static IList<BpCompany> RetrieveCompany(string rut, string razonsocial, int estado)
        {
            string msgErr = "";
            return ((IList<BpCompany>)AdminBpCompany.GetCompanys(rut, razonsocial, estado,out msgErr));
        }
        /// <summary>
        /// Método que permite recibir todas las compañías según creterio.
        /// </summary>
        /// <param name="estado">valores de estado 0-todos,1 activos y 2 inactivas</param>
        /// <returns></returns>
        public static IList<BpCompany> RetrieveAllCompanys(int estado)
        {
            string msgerr = "";
            return ((IList<BpCompany>)AdminBpCompany.GetCompanys(estado,out  msgerr));
        }
        /// <summary>
        /// Método que permite recibir las compañías que cumplan con un criterio
        /// </summary>
        /// <param name="domain">dominio en caso de querer filtrar</param>
        /// <returns></returns>
        public static IList<BpCompany> RetrieveAllCompanyActive(string domain)
        {
            string msgerr = "S/C";
            return ((IList<BpCompany>)AdminBpCompany.GetCompanyActive(domain,out  msgerr));
        }
        /// <summary>
        /// Método que permite recibir una compañía por ID
        /// </summary>
        /// <param name="id">Identificador interno de la compañía</param>
        /// <param name="msgerr">mensaje de error</param>
        /// <returns></returns>
        public static BpCompany Retrieve(int id,out String msgerr)
        {
            msgerr = "S/C";
            BpCompany company = AdminBpCompany.GetCompanyById(id,out msgerr);
            if (msgerr.Equals("S/C"))
                return company;
            else
                //Forward to error page.
                return null;
        }
        /// <summary>
        /// Método que permite recibir una compañía por dominio
        /// </summary>
        /// <param name="domain">dominio de la empresa</param>
        /// <param name="msgerr">mensaje de error</param>
        /// <returns></returns>
        public static BpCompany Retrieve(string domain, out String msgerr)
        {
            msgerr = "S/C";
            BpCompany company = AdminBpCompany.GetCompanyByDomain(domain, out msgerr);
            if (msgerr.Equals("S/C"))
                return company;
            else
                //Forward to error page.
                return null;
        }
        /// <summary>
        /// Método que permite crear una nueva compañía
        /// </summary>
        /// <param name="rut">rut</param>
        /// <param name="address">dirección</param>
        /// <param name="name">nombre</param>
        /// <param name="phone">teléfono</param>
        /// <param name="domain">dominio</param>
        /// <param name="phone2">teléfono 2</param>
        /// <param name="fax">fax</param>
        /// <param name="contactname">nombre de contacto</param>
        /// <param name="status">estado</param>
        /// <returns></returns>
        public static int Create(string rut, string address, string name, string phone, string domain,
                                 string phone2, string fax, string contactname, int status)
        {
            string msgErr="S/C";
            int result= AdminBpCompany.Create(rut, address, name, phone, domain, phone2, fax, contactname, status, out msgErr);
            return result;
        }
        /// <summary>
        /// Método que permite modificar una compañía
        /// </summary>
        /// <param name="id">identificador interno de la compañía</param>
        /// <param name="rut">rut</param>
        /// <param name="address">dirección</param>
        /// <param name="name">nombre de la compañía</param>
        /// <param name="phone">teléfono</param>
        /// <param name="domain">dominio</param>
        /// <param name="phone2">teléfono 2</param>
        /// <param name="fax">fax</param>
        /// <param name="contactname">nombre de contacto</param>
        /// <param name="status">estado</param>
        /// <returns></returns>
        public static int Modify(int id,string rut, string address, string name, string phone, string domain,
                                 string phone2, string fax, string contactname, int status)
        {
            string msgErr = "S/C";
            int result = AdminBpCompany.Modify(id,rut, address, name, phone, domain, phone2, fax, contactname, status, out msgErr);
            return result;
        }
        /// <summary>
        /// Método que permite borrar una empresa
        /// </summary>
        /// <param name="id">identificador interno de la compañía</param>
        /// <returns></returns>
        public static int Delete(int id)
        {
            string msgErr = "S/C";
            int result = AdminBpCompany.Delete(id, out msgErr);
            return result;
        }

        #endregion BpCompany

        #region Métodos estáticos asociados a BpUsers
        /// <summary>
        /// Método que permite traer todos los usuarios, dependiendo del rol, será por empresa o no.
        /// </summary>
        /// <param name="companyid"></param>
        /// <returns></returns>
        public static IList<User> RetrieveAllUsers(int companyid)
        {
            string msgerr = "S/C";
            return ((IList<User>)AdminBpUsers.GetAllUser(companyid , out  msgerr));
        }

        public static IList<User> RetrieveAllUser(int companyid, string username)
        {

            string msgErr = "S/C";
            return ((IList<User>)AdminBpUsers.GetAllUser(companyid,username,out msgErr));
        }
        public static User RetrieveUser(int userid,out String msgerr)
        {
            
            return (User)AdminBpUsers.GetUserById(userid,out  msgerr);

        }
        public static int Modify(int id, string Email, bool IsApproved)
        { 
            string msgErr = "S/C";
            int result = AdminBpUsers.ModifyUser(id, Email, IsApproved, out msgErr);
            return result;
        }
        public static int ModifyUser(int id, string Email, string Name, string Surname, 
                                     string Phone, string PostalCode, bool IsApproved)
        {
            string msgErr = "S/C";
            int result = AdminBpUsers.ModifyUser(id, Email, Name, Surname, Phone, 
                                                 PostalCode, IsApproved, out msgErr);
            return result;
        }

        public static int DeleteUser(int id, CustomRoleProvider roles)
        {
            string msgErr = "S/C";
            int result = AdminBpUsers.DeleteUser(id,roles,out msgErr);
            return result;
        }
        #endregion Métodos estáticos asociados a BpUsers



#region Métodos estáticos Origen
        public static IList RetriveAllOrigen()
        {

            return AdminBpOrigin.ListAll;
        }
        public static bool RetrieveAllOrigenes(out BpOrigin[] result,
                                           out string msgErr)
        {
            return AdminBpOrigin.RetrieveAll(out result, out msgErr);
        }
        public static BpOrigin RetriveOrigen(int id, out string msgErr)
        {
            return AdminBpOrigin.Retrieve(id,out msgErr);
        }
        public static BpOrigin Create(int id, string descripcion, out string msgErr)
        {
            return AdminBpOrigin.Create(id, descripcion, out msgErr);
        }
        public static bool Modify(int id, string descripcion, out string msgErr)
        {
            return AdminBpOrigin.Modify(id, descripcion, out msgErr);
        }
        public static bool Modify(int id, string descripcion,int idori, out string msgErr)
        {
            return AdminBpOrigin.Modify(id, descripcion,idori, out msgErr);
        }
        public static bool DeleteOrigen(int id, out string msgErr)
        {
            return AdminBpOrigin.Delete(id, out msgErr);
        }
        #endregion Métodos estáticos Origen

#region Métodos estáticos de Transacciones
        public static DataView RetrieveTx(int id, string trackid, int origin,
                                    string typeid, string valueid, int actiontype,
                                    string idconx, int statusconx, int resultconx,
                                    string timestamprcdesde, string timestamprchasta,
                                    int companyid, string connectorid,
                                    out string msg)
        {
            int res = 0;
            
            string msgErr = "";
            try
            {
                msg = msgErr;
                return AdminBpTx.GetAllTxWithFilterToDataView(id, 
                                                              trackid, 
                                                              origin, 
                                                              typeid, 
                                                              valueid, 
                                                              actiontype, 
                                                              idconx, 
                                                              statusconx, 
                                                              resultconx, 
                                                              timestamprcdesde, 
                                                              timestamprchasta, 
                                                              companyid, 
                                                              connectorid,
                                                              out msgErr);

            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.RetrieveTx", ex);
                msg = "";
                return null;
            }
        }
#endregion Métodos estáticos de Transacciones

#region Métodos estáticos Log
        public static DataView RetrieveLog(string level, string fechad, string fechah, out string msg)
        {
            int res = 0;
            
            string msgErr = "";
            try
            {
                msg = msgErr;
                return AdminBpLog.RetrieveLog(level, fechad, fechah, out msgErr);
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.RetrieveLog", ex);
                msg = "";
                return null;
            }
        }
#endregion Métodos estáticos Origen

        #region Métodos estátidos Identidad

        public static IList GetIdentitiesByFiltro(int companyidenroll,
                                                      string typeid, string valueid,
                                                      string name, string lastname)
        {
            return AdminBpIdentity.GetIdentitiesByFiltro(companyidenroll, typeid, valueid, name, lastname);
        }
        public static IList<BpBir> GetBirs(int id, out string msgerr)
        {
            BpIdentity bpidentity = null;
            int respuesta = 0;
            bpidentity = AdminBpIdentity.Retrieve(id, out msgerr);
            if (bpidentity != null)
                return AdminBpIdentity.GetBIRs(bpidentity);
            else
                return null;
        }
        public static BpIdentity GetIdentity(int id, out string msgerr)
        {
            return AdminBpIdentity.Retrieve(id, out msgerr);

        }

        public static bool DeleteIdentity(int id, out string msgErr)
        {
            return AdminBpIdentity.Delete(id, out msgErr);
        }
#endregion Métodos estáticos Identidad

        #region Métodos estáticos Birs

        #endregion métodos estáticos Birs

       
    }
}