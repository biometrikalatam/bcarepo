﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VerInfo.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Enroll.VerInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="titulos0">Datos Biométricos Enrolados de <asp:Label runat="server" ID="lblMensaje" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td class="tdBottom">
                    
                    <asp:ObjectDataSource ID="ObjectDataSourceBirs" runat="server" 
                        SelectMethod="GetBirs" 
                        TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper" 
                        onselected="ObjectDataSourceBirs_Selected" 
                        onselecting="ObjectDataSourceBirs_Selecting">
                        <SelectParameters>
                            <asp:Parameter Name="id" Type="Int32" />
                            <asp:Parameter Direction="Output" Name="msgerr" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:GridView ID="GridViewBirs" AutoGenerateColumns="false" Width="100%" 
                        PageSize="5" runat="server" SkinID="GridView1"
                        onpageindexchanging="GridViewBirs_PageIndexChanging" 
                       >
                        <Columns>
                            <asp:BoundField DataField="Id" HeaderText="#" SortExpression="Id" />
                            <asp:TemplateField HeaderText="Factor" SortExpression="Factor">
                                <ItemTemplate>
                                    <asp:Label ID="lblHuella" runat="server" Text='<%# FormatAutheticationFactor((int)Eval("Authenticationfactor")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderText="Tipo" SortExpression="Tipo">
                                <ItemTemplate>
                                    <asp:Label ID="lblHuella" runat="server" Text='<%# FormatType((int)Eval("Type")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BodyPart" SortExpression="BodyPart">
                                <ItemTemplate>
                                    <asp:Label ID="lblHuella" runat="server" Text='<%# FormatBodyPart((int)Eval("Bodypart")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderText="MinutiaeType" SortExpression="Minutiaetype">
                                <ItemTemplate>
                                    <asp:Label ID="lblHuella" runat="server" Text='<%# FormatMinutiaeType((int)Eval("MinutiaeType")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:BoundField DataField="Creation" HeaderText="Creación" SortExpression="Creation" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
