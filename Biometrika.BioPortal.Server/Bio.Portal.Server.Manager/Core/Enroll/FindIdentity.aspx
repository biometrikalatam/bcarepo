﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="FindIdentity.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Enroll.FindIdentity" %>
<%@ Import Namespace="Bio.Portal.Server.Common.Entities.Database" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<table cellpadding="0" cellspacing="0" width="90%" align="center">
    <tr>
        <td colspan="3" class="titulos0">Búsqueda de identidades</td>
    </tr>
     <tr>
        <td colspan="3" class="tdBottom" align="right"> 
            <asp:Button runat="server" SkinId="BotonAccion" ID="cmdAddNew" BorderStyle="None" 
                Text="Agregar Identidad"  OnClientClick="return AgregarIdentidad();" onclick="cmdAddNew_Click" />
            <asp:HiddenField runat="server" ID="hiddenOperacion" Value="" />
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Tipo Id</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:DropDownList ID="DropDownListTipoId" runat="server" 
                            ondatabound="DropDownListTipoId_DataBound">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Valor Id</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtNroId" Text="" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Nombre</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtNombre" Text="" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Apellido Paterno</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtApellidoP" Text="" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
        </td>
    </tr>    
    <asp:PlaceHolder runat="server" ID="company">
    <tr>
        <td class="tdBottom">Compañía</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
             <asp:DropDownList ID="DropDownListCompany" runat="server" 
                  DataTextField="Name" DataValueField="Id" OnDataBound="DropDownListCompany_DataBound">
                  <asp:ListItem Value="-1" Selected="True">Seleccione una empresa</asp:ListItem>
             </asp:DropDownList>
        </td>
    </tr>
    </asp:PlaceHolder>
    <tr>
        <td colspan="1" class=""> 
            <asp:Button runat="server" SkinId="BotonAccion" ID="cmdGuardar" Text="Buscar" BorderStyle="None" 
                onclick="cmdGuardar_Click" />
            <asp:HiddenField runat="server" ID="hiddencompanyid" Value="" />
            <asp:HiddenField runat="server" ID="hiddenidentityid" Value="" />
        </td>
        <td colspan="2" class="" align="right"> 
            <asp:Label ID="labMsg" runat="server"></asp:Label> 
        </td>
    </tr>
    <tr>
        <td colspan="3" class="">
            <asp:ObjectDataSource ID="ObjectDataSourceIdentity" runat="server" 
                SelectMethod="GetIdentitiesByFiltro" 
                TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper" 
                onselected="ObjectDataSourceIdentity_Selected" 
                onselecting="ObjectDataSourceIdentity_Selecting">
                <SelectParameters>
                    <asp:Parameter Name="companyidenroll" Type="Int32" />
                    <asp:Parameter Name="typeid" Type="String" />
                    <asp:Parameter Name="valueid" Type="String" />
                    <asp:Parameter Name="name" Type="String" />
                    <asp:Parameter Name="lastname" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:GridView ID="GridViewIdentity" runat="server" Width="100%"
                 EnableModelValidation="True" AutoGenerateColumns="False" SkinID="GridView1"
                  OnPageIndexChanging="GridViewIdentity_PageIndexChanging">
                 <Columns>
                   <asp:TemplateField HeaderText="">
                      <ItemTemplate>                      
                        <asp:ImageButton runat="server" ID="EditButton" ToolTip="Modificar Identidad..." 
                          CommandName="Editar" OnClientClick='<%# ModifyIdentity((int)Eval("Id")) %>'
                          ImageUrl="~/Images/usu_edit.gif" />                          
                      </ItemTemplate>
                       <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>    
                    <asp:TemplateField HeaderText="">
                      <ItemTemplate>
                        <asp:ImageButton runat="server" ID="DeleteButton" ToolTip="Eliminar Identidad..." 
                          CommandName="Delete" OnClientClick='<%# DeleteIdentity((int)Eval("Id")) %>'  
                          ImageUrl="~/Images/usu_elimina.gif" />                          
                      </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                     </asp:TemplateField>       
                     <asp:TemplateField HeaderText="+Birs">
                      <ItemTemplate>
                        <asp:ImageButton runat="server" ID="AddButton" 
                          CommandName="AddBir" OnClientClick='<%# AddBirs((int)Eval("Id"),(String)Eval("TypeId"),(String)Eval("ValueId")) %>'  
                          ImageUrl="~/Images/huella_mas.gif" />                          
                      </ItemTemplate>
                         <ItemStyle HorizontalAlign="Center" />
                     </asp:TemplateField>        
                     <asp:TemplateField HeaderText="Info">
                      <ItemTemplate>
                        <asp:ImageButton runat="server" ID="InfoButton" 
                          CommandName="Info"  OnClientClick='<%# PopUpInfo((int)Eval("Id"),(String)Eval("TypeId"),(String)Eval("ValueId")) %>'
                          ImageUrl="~/Images/huella_info.gif" />                          
                      </ItemTemplate>
                         <ItemStyle HorizontalAlign="Center" />
                     </asp:TemplateField>          
                     <asp:BoundField DataField="Id" HeaderText="#" SortExpression="Id" />
                     <asp:BoundField DataField="TypeId" HeaderText="Id" 
                         SortExpression="TypeId" />
                     <asp:BoundField DataField="ValueId" HeaderText="Valor" 
                         SortExpression="ValueId" />
                     <asp:BoundField DataField="Name" HeaderText="Nombre" SortExpression="Name" />
                     <asp:BoundField DataField="Patherlastname" HeaderText="Paterno" SortExpression="Patherlastname" />
                     <asp:BoundField DataField="Motherlastname" HeaderText="Materno" SortExpression="Motherlastname" />
                     <asp:TemplateField HeaderText="Foto" SortExpression="Signatureimage">                                
                        <ItemTemplate>
                            <asp:Image ID="ImageIco" ImageUrl='<%# FormatoFotoIcoImage((String)Eval("Photography")) %>' runat="server" />
                        </ItemTemplate>
                         <ItemStyle HorizontalAlign="Center" />
                     </asp:TemplateField>     
                     <asp:TemplateField HeaderText="Firma" SortExpression="Signatureimage">                                
                        <ItemTemplate>
                            <asp:Image ID="ImageIcoS" ImageUrl='<%# FormatoFotoIcoImage((String)Eval("Signatureimage")) %>' runat="server" />                           
                        </ItemTemplate>
                         <ItemStyle HorizontalAlign="Center" />
                     </asp:TemplateField>                                                               
                     <asp:BoundField DataField="Verificationsource" HeaderText="Verificado" SortExpression="Verificationsource" />
                     <asp:TemplateField HeaderText="Compañía">
                          <ItemTemplate>
                             <asp:Label ID="LabelCompany" runat="server" Text='<%# ((BpCompany)Eval("Companyidenroll")).Name %>'></asp:Label>
                          </ItemTemplate>              
                     </asp:TemplateField>
                 </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
</asp:Content>
