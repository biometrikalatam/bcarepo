﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Web.UI.HtmlControls;
using Bio.Portal.Server.Common.Common;
using Bio.Portal.Server.Manager.Core.libs;
using BioPortal.Server.Api;
using System.Text;
using Bio.Portal.Server.Manager.Core.Database;
using Bio.Core.Api;
using Bio.Portal.Server.Common.Entities.Database;
using Bio.Core.Constant;

namespace Bio.Portal.Server.Manager.Core.Enroll
{
    public partial class RegisterIdentity : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RegisterIdentity));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();

                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);
                HtmlGenericControl myJs2 = new HtmlGenericControl();
                myJs2.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs2.Attributes.Add("language", "javascript");
                myJs2.Attributes.Add("src", ResolveUrl("~/Scripts/dhtmlgoodies_calendar.js"));
                this.Page.Header.Controls.Add(myJs2);

                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 4)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);

                cmdCalendario.Attributes.Add("onclick", "javascript:displayCalendar(" + this.txtFechaVencimiento.ClientID + ",'dd/mm/yyyy',this);return false;");
                cmdCalendarioFecNac.Attributes.Add("onclick", "javascript:displayCalendar(" + this.txtFecNac.ClientID + ",'dd/mm/yyyy',this);return false;");
                log.Debug("Se ingresa a visualizar las empresas usuario:" + UserNameFull);
                //Enviamos los datos obligatorios a llenar por el usuario
                RenderJSArrayWithCliendIds(rbtnoverificado,//0
                                           rbtotro,//1
                                           rbtverificadoconci,//2
                                           rbtverificadosrcei,//3
                                           DropDownListTipoId,//4
                                           txtNroID,//5
                                           txtFechaVencimiento,//6
                                           txtFecNac,//7
                                           hiddenOperacion,//8
                                           hiddendata,//9
                                           hiddendedo,//10
                                           rbtFemenino,//11
                                           rbtMasculino,//12
                                           hiddenfirma,//13
                                           hiddenfoto, //14
                                           DropDownListAF, //15
                                           DropDownListDevice, //16
                                           DropDownListModel); //17


                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                hiddencompanyid.Value = CompanyId.ToString();
                int identityid = 0;
                if (!Page.IsPostBack)
                {

                    DropDownListTipoId.DataSource = Global.documentos.documents;
                    DropDownListTipoId.DataValueField = "ID";
                    DropDownListTipoId.DataTextField = "Description";
                    DropDownListTipoId.DataBind();
                    if (User.IsInRole("Super Administrador"))
                    {
                        DropDownListCompany.DataSource = DatabaseHelper.RetrieveAllCompanyActive("");//DatabaseHelper.RetrieveAllCompanyActive(Domain);
                        DropDownListCompany.DataBind();
                        company.Visible = true;
                    }
                    else
                    {
                        //En este caso de trata de un usuario administrador de la empresa
                        //almacenamos en un campo Hidden el Company Id del usuario.
                        company.Visible = false;
                        hiddencompanyid.Value = CompanyId.ToString();
                    }

                    //Vemos que operación se trata y vemos que botones se mostrarán 
                    //y que campos partiran con datos en caso de las modificaciones.
                    if (hiddenOperacion.Value != null)
                    {
                        if (hiddenOperacion.Value.Equals("modificar"))
                        {
                            lbltitulo.Text = "Modificando Identidad";
                            cmdModificar.Visible = true;
                            cmdGuardar.Visible = false;
                            if (Request["ctl00$ContentPlaceHolderMain$hiddenidentityid"] != null)
                            {
                                identityid = Convert.ToInt32(Request["ctl00$ContentPlaceHolderMain$hiddenidentityid"].ToString());

                                ObjectDataSourceIdentity.Select();

                            }
                            DropDownListTipoId.Enabled = false;
                            txtNroID.ReadOnly = true;
                        }
                        else
                        {
                            lbltitulo.Text = "Agregando Identidad";
                            cmdGuardar.Visible = true;
                            cmdModificar.Visible = false;
                        }

                    }
                }
                else
                {
                    if (hiddenOperacion.Value != null)
                    {
                        //Recogemos las variables del formulario.                        
                        String NroID = txtNroID.Text;
                        String TipoID = DropDownListTipoId.SelectedValue;
                        String Data = hiddendata.Value;
                        String Finger = hiddendedo.Value;
                        String Nombre = txtNombre.Text;
                        String ApellidoP = txtApellidoP.Text;
                        String ApellidoM = txtApellidoM.Text;
                        String Profesion = txtProfesion.Text;
                        String Nacionalidad = txtNacionalidad.Text;
                        String LugarNacimiento = txtLugarNac.Text;
                        String ImagenFoto = hiddenfoto.Value;
                        String Serie = txtSerie.Text;
                        String ImagenFirma = hiddenfirma.Value;
                        String FechaVencimiento = null;
                        String TipoVisa = txtVisa.Text;
                        //String ClavePin = txtPin.Text;
                        //if (txtFechaVencimiento.Text.Length != 0)
                        if (!String.IsNullOrEmpty(txtFechaVencimiento.Text))
                            FechaVencimiento = txtFechaVencimiento.Text;
                        String FechaNacimiento = null;
                        //if (txtFechaVencimiento.Text.Length != 0)
                        if (!String.IsNullOrEmpty(txtFecNac.Text))
                            FechaNacimiento = txtFecNac.Text;
                        String Verificado = "";
                        if (rbtnoverificado.Checked == true)
                            Verificado = "NV";
                        else if (rbtverificadoconci.Checked == true)
                            Verificado = "VCI";
                        else if (rbtotro.Checked == true)
                            Verificado = txtotro.Text;
                        String sexo;
                        if (rbtFemenino.Checked)
                            sexo = "F";
                        else
                            sexo = "M";

                        int CompanyID = 0;
                        if (User.IsInRole("Super Administrador"))
                            CompanyID = Convert.ToInt32(DropDownListCompany.SelectedValue);
                        else
                            CompanyID = Convert.ToInt32(hiddencompanyid.Value);
                        //Fin de recolección de variables.
                        XmlParamIn pin = new XmlParamIn();

                        using (BioWs.BioPortalServerWSWeb ws = new BioWs.BioPortalServerWSWeb())
                        {
                            ws.Timeout = 99999;
                            if (hiddenOperacion.Value.Equals("modificar"))
                                pin.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_MODIFY;
                            else
                                pin.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_ENROLL;

                            pin.Authenticationfactor = Convert.ToInt32(DropDownListAF.SelectedValue); //Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;
                            pin.Bodypart = Convert.ToInt32(Finger); // Bio.Core.Matcher.Constant.BodyPart.BODYPART_DEDOANULARDERECHO;
                            //if (DropDownListAlgoritmo.SelectedIndex != 0)
                            //{
                            pin.Minutiaetype = 2; // Convert.ToInt32(DropDownListAlgoritmo.SelectedValue); // DropDownListAlgoritmo.SelectedIndex;  //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER;
                            //}
                            pin.Clientid = "Manager";
                            pin.InsertOption = 0; //Default
                            pin.OperationOrder = 0; //Default
                            pin.Companyid = CompanyID;
                            pin.Enduser = "Manager";
                            pin.Ipenduser = Request.ServerVariables["REMOTE_HOST"].ToString();
                            pin.Matchingtype = 1;
                            pin.Origin = 2; //BP Manager

                            if (!String.IsNullOrEmpty(Data))
                            {
                                pin.SampleCollection = new List<Sample>();
                                Sample sample = new Sample
                                                    {
                                                        Data = Data,
                                                        Minutiaetype =
                                                            Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_TOKEN,
                                                        Additionaldata = null
                                                    };
                                pin.SampleCollection.Add(sample);
                            } else
                            {
                                pin.SampleCollection = null;
                            }
                            pin.SaveVerified = 1;
                            pin.Threshold = 0; //!String.IsNullOrEmpty(txtUmbral.Text) ?
                                            //Convert.ToDouble(txtUmbral.Text) : 0;
                            pin.Userid = UserId;
                            //pin.Verifybyconnectorid = txtConnectorId.Text.Equals(null) ||
                            //                      txtConnectorId.Text.Trim().Length == 0 ?
                            //                      "final" : txtConnectorId.Text.Trim();
                            pin.Verifybyconnectorid = String.IsNullOrEmpty(txtConnectorId.Text) ?
                                                  "final" : txtConnectorId.Text.Trim();
                            pin.InsertOption = 0; //Default

                            //Datos de la Persona
                            pin.PersonalData = new PersonalData
                                                   {
                                                       Typeid = TipoID,
                                                       Valueid = NroID,
                                                       Companyidenroll = CompanyID,
                                                       Useridenroll = UserId,
                                                       Patherlastname = ApellidoP,
                                                       Motherlastname = ApellidoM,
                                                       Photography = ImagenFoto,
                                                       Signatureimage = ImagenFirma,
                                                       Name = Nombre,
                                                       Sex = sexo,
                                                       Verificationsource = Verificado,
                                                       Documentseriesnumber = Serie,
                                                       Documentexpirationdate = Convert.ToDateTime(FechaVencimiento),
                                                       Birthdate = Convert.ToDateTime(FechaNacimiento),
                                                       Birthplace = LugarNacimiento,
                                                       Nationality = Nacionalidad,
                                                       Profession = Profesion,
                                                       Visatype = TipoVisa
                                                   };


                            //Serializamos.
                            string xmlparamin = XmlUtils.SerializeAnObject(pin);
                            string xmlparamout = "";

                            int respuesta = ws.Enroll(xmlparamin, out xmlparamout);
                            log.Debug("Saliendo ws.Enroll => respuesta=" + respuesta.ToString() + Environment.NewLine +
                                      "                     xmlparamin=" + xmlparamin + Environment.NewLine +
                                      "                     xmlparamout=" + xmlparamout);
                            //Session["xmlparamout"] = xmlparamout;
                            //Response.Redirect("Message.aspx");

                            if (respuesta == Errors.IERR_OK)
                            {
                                //Session["respuesta"] = respuesta;
                                //Session["xmlparamout"] = xmlparamout;
                                Session["MsgDeleteBir"] = "Operación completada con éxito!";
                            }
                            else
                            {
                                throw new Exception("Error enrolando identidad [" +
                                                     respuesta.ToString() + ": " +
                                                     Errors.GetDescription(respuesta) + "]");
                            }
                            Response.Redirect("FindIdentity.aspx", true);
                        }
                    }

                }
            }
            catch (Exception exe)
            {
                if (exe.GetType().FullName != "System.Threading.ThreadAbortException")
                {
                    log.Error("Se ha producido un error en RegisterIdentity.aspx", exe);
                    Session["mensajeerror"] = "Se ha producido un error en RegisterIdentity.aspx";
                    Session["exception"] = exe;
                    Response.Redirect("~/Error.aspx");

                }
            }
            
        }
        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }

        protected void DropDownListTipoId_DataBound(object sender, EventArgs e)
        {
            DropDownListTipoId.Items.Insert(0, new ListItem("Seleccione un tipo ID", "0"));
        }
        protected void DropDownListCompany_DataBound(object sender, EventArgs e)
        {
            DropDownListCompany.Items.Insert(0, new ListItem("Seleccione una compañía", "0"));
        }
        protected void cmdGuardar_Click(object sender, EventArgs e)
        {
            
            Response.Redirect("RegisterIdentity.aspx");
        }

        protected void ObjectDataSourceIdentity_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            try
            {
                e.InputParameters["id"] = Convert.ToInt32(Request["ctl00$ContentPlaceHolderMain$hiddenidentityid"].ToString());
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en RegisterIdentity.aspx al momento de seleccionar una identidad para modificar" , exe);
                Session["mensajeerror"] = "Se ha producido un error en RegisterIdentity.aspx al momento de seleccionar una identidad para modificar";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceIdentity_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                //Verificamos que el resultado de la búsqueda haya sido exitoso.
                if (e.ReturnValue != null)
                {
                    BpIdentity identidad = (BpIdentity)e.ReturnValue;
                    txtNombre.Text = identidad.Name;
                    txtApellidoM.Text = identidad.Motherlastname;
                    txtApellidoP.Text = identidad.Patherlastname;
                    txtId.Text = identidad.Id.ToString();
                    txtNroID.Text = identidad.Valueid.ToString();
                    txtFecNac.Text = identidad.Birthdate == null ? "" : 
                        identidad.Birthdate.Value.ToShortDateString();

                    txtFechaVencimiento.Text = identidad.Documentexpirationdate == null ? "" : 
                        identidad.Documentexpirationdate.Value.ToShortDateString();

                    txtLugarNac.Text = identidad.Birthplace;
                    txtNacionalidad.Text = identidad.Nationality;
                    txtProfesion.Text = identidad.Profession;
                    txtSerie.Text = identidad.Documentseriesnumber;
                    txtVisa.Text = identidad.Visatype;

                    if (identidad.Photography != null)
                    {
                        try
                        {
                            byte[] byJPG = Convert.FromBase64String(identidad.Photography);
                            System.IO.FileStream fs = 
                                new FileStream(Bio.Portal.Server.Manager.Properties.Settings.Default.PathTmp +
                                                "foto_" + identidad.Valueid.ToString() + ".jpg", FileMode.OpenOrCreate);
                            fs.Write(byJPG, 0, byJPG.Length);
                            fs.Close();
                            hiddenFotoSaved.Value = "1";
                            hiddenFotoPath.Value = Bio.Portal.Server.Manager.Properties.Settings.Default.PathTmp +
                                                "foto_" + identidad.Valueid.ToString() + ".jpg";
                        }
                        catch (Exception ex)
                        {
                            hiddenFotoSaved.Value = "0";
                            hiddenFotoPath.Value = "";
                        }
                        hiddenfoto.Value = identidad.Photography;
                        lblmensajefoto.Text = "La persona ya tiene asociada una foto. Si sube otra foto, la anterior será reemplazada";
                    } else
                    {
                        hiddenfoto.Value = "";
                        lblmensajefoto.Text = "La persona NO tiene asociada una foto.";
                    }

                    if (identidad.Signatureimage != null)
                    {
                        try
                        {
                            byte[] byJPG = Convert.FromBase64String(identidad.Signatureimage);
                            System.IO.FileStream fs =
                                new FileStream(Bio.Portal.Server.Manager.Properties.Settings.Default.PathTmp +
                                                "firma_" + identidad.Valueid.ToString() + ".jpg", FileMode.OpenOrCreate);
                            fs.Write(byJPG, 0, byJPG.Length);
                            fs.Close();
                            hiddenFirmaSaved.Value = "1";
                            hiddenFirmaPath.Value = Bio.Portal.Server.Manager.Properties.Settings.Default.PathTmp +
                                                "firma_" + identidad.Valueid.ToString() + ".jpg";
                        }
                        catch (Exception ex)
                        {
                            hiddenFirmaSaved.Value = "0";
                            hiddenFirmaPath.Value = "";
                        }
                        hiddenfirma.Value = identidad.Signatureimage;
                        lblmensajefirma.Text = "La persona ya tiene asociada una firma. Si sube otra firma, la anterior será reemplazada";
                    }
                    DropDownListTipoId.ClearSelection();
                    DropDownListTipoId.Items.FindByValue(identidad.Typeid.Trim()).Selected = true;

                    if (User.IsInRole("Super Administrador"))
                    {
                        DropDownListCompany.ClearSelection();
                        DropDownListCompany.Items.FindByText(identidad.Companyidenroll.Name).Selected = true;
                    } else
                    {
                        hiddencompanyid.Value = identidad.Companyidenroll.Id.ToString();
                    }

                    SetCheckBox(identidad.Verificationsource);
                    if (identidad.Sex == "F")
                        rbtFemenino.Checked = true;
                    else if (identidad.Sex == "M")
                        rbtMasculino.Checked = true;

                }
            }
            else
            {
              
                log.Error("Se ha producido un error en RegisterIdentity.aspx al momento de recuperar una identidad" + e.ReturnValue);
                Session["mensajeerror"] = "Se ha producido un error en RegisterIdentity.aspx";
                Session["exception"] = e.ReturnValue;
                Response.Redirect("~/Error.aspx");
            
            }
        }

        private void SetCheckBox(string verifysource)
        {
            if (verifysource == "NV")
                rbtnoverificado.Checked = true;
            else if (verifysource == "VCI")
                rbtverificadoconci.Checked = true;
            else
            {
                rbtotro.Checked = true;
                txtotro.Text = verifysource;
            }

        }


    }
}