﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Web.UI.HtmlControls;
using Bio.Portal.Server.Manager.Core.libs;
using Bio.Portal.Server.Common.Entities.Database;
using BioPortal.Server.Api;
using Bio.Core.Api;

namespace Bio.Portal.Server.Manager.Core.Enroll
{
    public partial class DeleteBir : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AddBir));
        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicializamos el soporte de javascript del sitio.
            HtmlGenericControl myJs = new HtmlGenericControl();

            myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
            myJs.Attributes.Add("language", "javascript");
            //don't need it usually but for cross browser.
            myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
            this.Page.Header.Controls.Add(myJs);
            HtmlGenericControl myJs2 = new HtmlGenericControl();
            myJs2.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
            myJs2.Attributes.Add("language", "javascript");
            myJs2.Attributes.Add("src", ResolveUrl("~/Scripts/dhtmlgoodies_calendar.js"));
            this.Page.Header.Controls.Add(myJs2);

            //Chequea si tiene permisos para acceder a esta página
            if (BioPortalServer.GetRolId(User) > 4)
            {
                throw new Exception("No tiene privilegios para ingresar en esta opción.");
                //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                //Session["exception"] = null;
                ////Response.Redirect("~/Error.aspx", true);
                //Server.Transfer("~/Error.aspx");
            }

            //Dibuja Menu
            BioPortalServer.VisualizarMenu(Master, User);
            //Verificamos accesos
            //if (User.IsInRole("Super Administrador"))
            //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
            //else if (User.IsInRole("Administrador"))
            //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

            //if (User.IsInRole("Enrolador"))
            //    BioPortalServer.VisualizarMenuEnrolador(Master);
            //if (User.IsInRole("Verificador"))
            //    BioPortalServer.VisualizarMenuVerificar(Master);

            Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
            int id = 0;
            if (!IsPostBack)
            {
                if (Request["id"] == null) return;
                id = Convert.ToInt32(Request["id"].ToString());
                ObjectDataSourceBir.Select();
            }
            //hiddenorigin.Value = "6";//Preguntar a Gustavo por esto.
        }
        protected void ObjectDataSourceBir_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            try
            {
                e.InputParameters["id"] = Convert.ToInt32(Request["id"].ToString());
            }
            catch (Exception exe)
            {
                log.Error("AddBir.ObjectDataSourceBir.Select" + exe.Message);
            }
        }

        protected void ObjectDataSourceBir_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue != null)
            {
                BpIdentity identity = (BpIdentity)e.ReturnValue;
                txtNombre.Text = identity.Name;
                txtApellidos.Text = identity.Patherlastname + " " + identity.Motherlastname;
                txtNroId.Text = identity.Valueid.ToString();
                txtTipoId.Text = identity.Typeid.ToString();
                txtId.Text = identity.Id.ToString();

                if (identity.Verificationsource == "NV")
                    rbtnoverificado.Checked = true;
                else if (identity.Verificationsource == "VCI")
                    rbtverificadoconci.Checked = true;
                else
                {
                    rbtotro.Checked = true;
                    txtotro.Text = identity.Verificationsource;
                }
                //hiddenTypeId.Value = Global.documentos.FindDocument(identity.Typeid.Trim()).Description;
            }
        }

        //protected void txtDelete_Click(object sender, EventArgs e)
        //{
        //         //Llamamos al servicio de enrolamiento 
        //         XmlParamIn pin=new XmlParamIn();
        //         using (BioPlain.BioPortalServerWS ws=new BioPlain.BioPortalServerWS())
        //         {
        //             ws.Timeout = 99999;
        //             pin.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_DELETE;
        //             pin.Authenticationfactor = Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;
        //             pin.Bodypart = Bio.Core.Matcher.Constant.BodyPart.BODYPART_DEDOANULARDERECHO;
        //             pin.Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER;
        //             pin.Clientid = "Manager";
        //             pin.InsertOption = 1;
        //             pin.OperationOrder = 1;
        //             pin.Companyid = 7;//CompanyID;
        //             pin.Enduser = Request.ServerVariables["REMOTE_HOST"].ToString();//Preguntar a Gus o es el Manager?
        //             pin.Ipenduser = Request.ServerVariables["REMOTE_HOST"].ToString();
        //             pin.Matchingtype = 1;
        //             if (hiddenorigin.Value == "")
        //                 pin.Origin = 1;
        //             else
        //                 pin.Origin = Convert.ToInt32(hiddenorigin.Value);

        //             pin.SaveVerified = 1;
        //             pin.Threshold = 0.00001;
        //             pin.Userid = 0;
        //             pin.Verifybyconnectorid = "0";
        //             pin.InsertOption = 0;

        //             //Datos de la Persona
        //             pin.PersonalData = new PersonalData();
        //             pin.PersonalData.Id = Convert.ToInt32(txtId.Text);
        //             pin.PersonalData.Valueid = txtNroId.Text;
        //             pin.PersonalData.Typeid = hiddenTypeId.Value;

        //             if (hiddenorigin.Value == "")
        //                 pin.Origin = 1;
        //             else
        //                 pin.Origin = Convert.ToInt32(hiddenorigin.Value);

        //             string xmlparamin = XmlUtils.SerializeAnObject(pin);
        //             string xmlparamout = "";
        //             int respuesta = ws.Enroll(xmlparamin, out xmlparamout);
        //             Session["xmlparamout"] = xmlparamout;
        //             Response.Redirect("Message.aspx");
        //         }
                 
        //}

        protected void cmdEliminar_Click(object sender, EventArgs e)
        {
            ObjectDataSourceBir.Delete();
        }

        protected void ObjectDataSourceBir_Deleting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            try
            {
                e.InputParameters["id"] = Convert.ToInt32(txtId.Text.Trim());
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en DeleteBir.aspx la eliminación de una identidad", exe);
                Session["mensajeerror"] = "Se ha producido un error en DeleteBir.aspx la eliminación de una identidad";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceBir_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                if (Convert.ToBoolean(e.ReturnValue) != true)
                {
                    Session["MsgDeleteBir"] = "Error eliminando identidad";
                } else
                {
                    Session["MsgDeleteBir"] = "Identidad Eliminada!";
                }
            }
            else
            {
                log.Error("Se ha producido un error en ModifyOrigin.aspx la eliminación de un origen" + e.ReturnValue);
                Session["mensajeerror"] = "Se ha producido un error en ModifyOrigin.aspx la eliminación de un origen";
                Session["exception"] = e.ReturnValue;
                Response.Redirect("~/Error.aspx");
            }
            Response.Redirect("~/Core/Enroll/FindIdentity.aspx", true);
 
        }

        protected void txtCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Core/Enroll/FindIdentity.aspx",true);
        }
    }
}