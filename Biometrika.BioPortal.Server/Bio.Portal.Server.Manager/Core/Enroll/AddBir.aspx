﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" EnableViewStateMac="false" CodeBehind="AddBir.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Enroll.AddBir" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td colspan="3" class="titulos0">        
            Agregar Datos Biométricos..
            <asp:ObjectDataSource ID="ObjectDataSourceBir" runat="server" 
                onselected="ObjectDataSourceBir_Selected" 
                onselecting="ObjectDataSourceBir_Selecting" SelectMethod="GetIdentity" 
                TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper">
                <SelectParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                    <asp:Parameter Direction="Output" Name="msgerr" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">ID</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtId" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Tipo Id</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtTipoId" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Valor Id</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtNroId" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Nombres</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtNombre" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Apellidos</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtApellidos" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="3" class="titulos0">Información para Verificación</td>
    </tr>
    <tr>
        <td colspan="3" class="tdBottom" align="center">
            <table cellpadding="0" cellspacing="0" border="0" >
                         <tr><td valign="top" align="left">  
                            <table cellpadding="0" cellspacing="0" border="0" >
                                
                                <%-- <tr>
                                    <td class="tdBottom" colspan="2" align="right" valign="top">Contenido Token :</td>
                                    <td class="tdBottom" colspan="2">
                                        <asp:RadioButton ID="rbtwsq" GroupName="grptoken" Text="WSQ" runat="server" /><br />
                                        <asp:RadioButton ID="rbtMinucias" GroupName="grptoken" Text="Minucias" runat="server" /><br />
                                        <asp:RadioButton ID="rbtAmbos" GroupName="grptoken" Text="Ambos" runat="server" Checked="true" />  
                                    </td>
                                </ tr>--%>
                                <tr>
                                    <td colspan="2" class="tdBottom" align="right"><br />   
                                        Factor :</td>
                                    <td colspan="2" class="tdBottom"><br />   
                                        <asp:DropDownList runat="server" ID="DropDownListAF">
                                            <asp:ListItem Value="0">Seleccione Factor</asp:ListItem>
                                            <asp:ListItem Value="1">Clave</asp:ListItem>                            
                                            <asp:ListItem Value="2" Selected="True">Huella Digital</asp:ListItem>
                                            <asp:ListItem Value="11">Certificado Digital</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <%--<tr>
                                    <td colspan="2" class="tdBottom" align="right">
                                        Algoritmo
                                        : </td>
                                    <td colspan="2" class="tdBottom">
                                        <asp:DropDownList runat="server" ID="DropDownListAlgoritmo" onchange="javascript:SetThreshold();">
                                            <asp:ListItem Value="0">Seleccione Algoritmo</asp:ListItem>
                                            <asp:ListItem Value="1">NEC</asp:ListItem>
                                            <asp:ListItem Value="2" Selected="True">Digital Persona</asp:ListItem>
                                            <asp:ListItem Value="3">Digital Persona Gold</asp:ListItem>
                                            <asp:ListItem Value="4">Digital Persona OTW</asp:ListItem>
                                            <asp:ListItem Value="7">Verifinger</asp:ListItem>
                                            <asp:ListItem Value="5">Secugen</asp:ListItem>
                                            <asp:ListItem Value="6">Testech</asp:ListItem>
                                            <asp:ListItem Value="8">Sagem</asp:ListItem>
                                            <asp:ListItem Value="9">Identix</asp:ListItem>
                                            <asp:ListItem Value="10">Upek</asp:ListItem>
                                            <asp:ListItem Value="11">Cogent</asp:ListItem>
                                            <asp:ListItem Value="12">Crossmatch</asp:ListItem>                            
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"  class="tdBottom" align="right">
                                        &nbsp;</td>
                                    <td colspan="2"  class="tdBottom" align="left" valign="middle">
                                        Umbral :<asp:TextBox runat="server" ID="txtUmbral" MaxLength="15" Text="0.0001" width="80" 
                                            onkeypress="javascript:return RestrictChar()"></asp:TextBox>
                                    </td>
                                </tr> --%>
                                <tr>
                                    <td colspan="2"  class="tdBottom" align="right">
                                        Device :</td>
                                    <td colspan="2"  class="tdBottom">
                                        <asp:DropDownList runat="server" ID="DropDownListDevice" onchange="javascript:SetModel();">
                                            <asp:ListItem Value="0">Seleccione Dispositivo</asp:ListItem>
                                            <asp:ListItem Value="1" Selected="True">Digital Persona</asp:ListItem>
                                            <asp:ListItem Value="2">Secugen</asp:ListItem>
                                            <asp:ListItem Value="7">Upek</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="tdBottom">Modelo :</td>
                                    <td colspan="3" class="tdBottom"><asp:DropDownList runat="server" ID="DropDownListModel" onchange="javascript:CheckModel();">
                                            <asp:ListItem Value="">Seleccione Modelo</asp:ListItem>
                                            <asp:ListItem Value="4000" Selected="True">U.are.U 4000</asp:ListItem>
                                            <asp:ListItem Value="4500">U.are.U 4500</asp:ListItem>
                                            <asp:ListItem Value="FDU02">Hamster II / III</asp:ListItem>
                                            <asp:ListItem Value="FDU03">Hamster Plus</asp:ListItem>
                                            <asp:ListItem Value="FDU04">Hamster IV</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="tdBottom">ConnectorId :</td>
                                    <td colspan="3" class="tdBottom">
                                        <asp:TextBox runat="server" ID="txtConnectorId" MaxLength="50" 
                                            onkeypress="javascript:return RestrictChar()"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr><td colspan="4" align="right">
                                    <asp:Button runat="server" ID="cmdInicializar" BorderStyle="None" Text="Inicializar" SkinID="BotonAccion" OnClientClick="return SetBpClientAddBir();" />       
                                </td></tr>
                            </table>
                        </td>
                        <td colspan="1" align="center">
                           <OBJECT ID="BPClientAx1" CLASSID="CLSID:6E7AA4F1-BEBF-4A56-9DAE-8924F4667567" width="280" height="285">
                  				
		                 </OBJECT>
                    </td>
                 </tr>
             </table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:Button runat="server" SkinID="BotonAccion" ID="txtAgregar" BorderStyle="None" 
                Text="Agregar BIR" onclick="txtAgregar_Click" OnClientClick="return AddBir();" />
            <asp:Button runat="server" SkinID="BotonAccion" ID="txtCancelar" Text="Cancerlar"  BorderStyle="None" />
            <asp:HiddenField runat="server" ID="hiddenorigin" Value="" />
            <asp:HiddenField runat="server" ID="hiddendata" Value="" />
            <asp:HiddenField runat="server" ID="hiddendedo" Value="" />
            <asp:HiddenField runat="server" ID="hiddencompanyid" Value="" />
            <asp:HiddenField runat="server" ID="hiddenTypeId" Value="" />
        </td>
    </tr>
</table>
<script type="text/javascript" language="javascript">
    var qrStr = window.location.search;
    var spQrStr = qrStr.substring(1);
    var arrQrStr = new Array();
    var tipoid;
    var nroid;
    // splits each of pair
    var arr = spQrStr.split("&");
    for (var i=0;i<arr.length;i++){
    // splits each of field-value pair
    var index = arr[i].indexOf('=');
    var key = arr[i].substring(0,index);
    var val = arr[i].substring(index + 1);
    if (key == 'typeid')
        tipoid = val;
    if (key == 'valueid')
        nroid = val;
    }

    document.getElementById("BPClientAx1").Operationtype = 2;
    document.getElementById("BPClientAx1").AuthenticationFactor = 2;
    document.getElementById("BPClientAx1").TypeId = tipoid;
    document.getElementById("BPClientAx1").ValueId = nroid;
    document.getElementById("BPClientAx1").Device = 1;
    document.getElementById("BPClientAx1").Model = 4000;
    //document.getElementById("BPClientAx1").Timeout = 5000;
</script>
</asp:Content>
