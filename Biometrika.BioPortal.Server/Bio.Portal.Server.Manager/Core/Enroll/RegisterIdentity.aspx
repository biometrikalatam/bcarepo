﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" EnableViewStateMac="false" CodeBehind="RegisterIdentity.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Enroll.RegisterIdentity" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td>            
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td class="titulos0" colspan="4">
                      <asp:Label runat="server" ID="lbltitulo" Text="Registrando Identidad"></asp:Label>
                      <asp:ObjectDataSource ID="ObjectDataSourceIdentity" runat="server" 
                            onselected="ObjectDataSourceIdentity_Selected" 
                            onselecting="ObjectDataSourceIdentity_Selecting" SelectMethod="GetIdentity" 
                            TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper">
                          <SelectParameters>
                              <asp:Parameter Name="id" Type="Int32" />
                              <asp:Parameter Direction="Output" Name="msgerr" Type="String" />
                          </SelectParameters>
                        </asp:ObjectDataSource>
                    </td>
                </tr>
                <tr>
                    <td class="tdBottom" align="right" width="105">Id :</td>
                    <td class="tdBottom">
                        <asp:TextBox runat="server" ID="txtId" Enabled="false"></asp:TextBox>
                    </td>
                    <td class="tdBottom">
                        
                    </td>
                    <td class="tdBottom">
                        
                    </td>
                </tr>  
                <tr>
                    <td  colspan="1" class="tdBottom" align="right">Verificado :</td>
                    <td  colspan="3" class="tdBottom">
                        <asp:RadioButton ID="rbtnoverificado" GroupName="grpverificado" Text="No Verificado" runat="server" />
                        <asp:RadioButton ID="rbtverificadoconci" GroupName="grpverificado" Text="Verificado con CI" runat="server" />
                        <asp:RadioButton visible="false" ID="rbtverificadosrcei" GroupName="grpverificado" Text="Verificado Connector" runat="server" />
                        <asp:RadioButton ID="rbtotro" GroupName="grpverificado" Text="Otros" runat="server" />
                        <asp:TextBox runat="server" ID="txtotro" Width="50"></asp:TextBox>
                    </td>
                </tr>
                <asp:PlaceHolder runat="server" ID="company">
                <tr>
                    <td class="tdBottom" align="right">Compañía :</td>
                    <td class="tdBottom"> 
                        <asp:DropDownList ID="DropDownListCompany" runat="server" BackColor="#ccff99"
                                         DataTextField="Name" DataValueField="Id" OnDataBound="DropDownListCompany_DataBound">
                                        <asp:ListItem Value="-1" Selected="True">Seleccione una empresa</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td colspan="2" class="tdBottom">
                       
                    </td>
                </tr>
                </asp:PlaceHolder>  
                <tr>
                    <td class="tdBottom" align="right">(*) Tipo Id :</td>
                    <td class="tdBottom">
                        <asp:DropDownList ID="DropDownListTipoId" runat="server" BackColor="#ccff99"  
                            ondatabound="DropDownListTipoId_DataBound">
                        </asp:DropDownList>
                    </td>
                    <td class="tdBottom" align="right">(*) Valor Id :</td>
                    <td class="tdBottom"><asp:TextBox runat="server" ID="txtNroID" BackColor="#ccff99" onkeypress="javascript:return RestrictChar()"></asp:TextBox></td>
                </tr>             
                <tr>
                    <td class="tdBottom" align="right">Nombres :</td>
                    <td class="tdBottom"><asp:TextBox runat="server" ID="txtNombre" onkeypress="javascript:return RestrictChar()"></asp:TextBox></td>
                    <td class="tdBottom" align="right">Apellido Paterno:</td>
                    <td class="tdBottom"><asp:TextBox Width="160" runat="server" ID="txtApellidoP" onkeypress="javascript:return RestrictChar()"></asp:TextBox></td>
                </tr>
                 <tr>
                    <td class="tdBottom" align="right">Apellido Materno :</td>
                    <td class="tdBottom"><asp:TextBox runat="server" ID="txtApellidoM" onkeypress="javascript:return RestrictChar()"></asp:TextBox></td>
                    <td class="tdBottom" align="right">Sexo :</td>
                    <td class="tdBottom">
                        <asp:RadioButton ID="rbtFemenino" GroupName="sexo" Text="Femenino" runat="server" /> 
                        <asp:RadioButton ID="rbtMasculino" GroupName="sexo" Text="Masculino" runat="server" />
                    </td>
                </tr>
                 <tr>
                    <td class="tdBottom" align="right">Serie :</td>
                    <td class="tdBottom"><asp:TextBox runat="server" ID="txtSerie" onkeypress="javascript:return RestrictChar()"></asp:TextBox></td>
                    <td class="tdBottom" align="right">Fecha Vencimiento (dd/mm/yyyy) :</td>
                    <td class="tdBottom">
                        <asp:TextBox runat="server" Width="80" ID="txtFechaVencimiento" onfocus="javascript:vDateType='3'" onkeyup="javascript:DateFormat(document.getElementById(MyClientID[6]),document.getElementById(MyClientID[6]).value,event,false,'3')" onBlur="javascript:DateFormat(document.getElementById(MyClientID[6]),document.getElementById(MyClientID[6]).value,event,true,'3');ValidarFecha(document.getElementById(MyClientID[6]),document.getElementById(MyClientID[6]).value)"></asp:TextBox>
                        <asp:Button runat="server" ID="cmdCalendario"  Text="" SkinID="BottonCal" 
                        UseSubmitBehavior="False" />
                    </td>
                </tr>
                 <tr>
                    <td class="tdBottom" align="right">Fecha Nacimiento :</td>
                    <td class="tdBottom">
                        <asp:TextBox runat="server" Width="80" ID="txtFecNac" onfocus="javascript:vDateType='8'" onkeyup="javascript:DateFormat(document.getElementById(MyClientID[7]),document.getElementById(MyClientID[7]).value,event,false,'3')" onBlur="javascript:DateFormat(document.getElementById(MyClientID[7]),document.getElementById(MyClientID[7]).value,event,true,'3');"></asp:TextBox>
                        <asp:Button runat="server" ID="cmdCalendarioFecNac"  Text="" SkinID="BottonCal" 
                        UseSubmitBehavior="False" />   
                    </td>
                    <td class="tdBottom" align="right">Tipo Visa :</td>
                    <td class="tdBottom"><asp:TextBox runat="server" Width="160" ID="txtVisa"></asp:TextBox></td>
                </tr>
                 <tr>
                    <td class="tdBottom" align="right">Lugar Nacimiento :</td>
                    <td class="tdBottom"><asp:TextBox Width="160" runat="server" ID="txtLugarNac"></asp:TextBox></td>
                    <td class="tdBottom" align="right">Profesión :</td>
                    <td class="tdBottom"><asp:TextBox Width="160" runat="server" ID="txtProfesion"></asp:TextBox></td>
                </tr>

                <tr>
                    <td class="tdBottom" align="right">Nacionalidad :</td>
                    <td class="tdBottom"><asp:TextBox Width="160" runat="server" ID="txtNacionalidad"></asp:TextBox></td>
                    <td class="tdBottom" colspan="2">
                        
                    </td>                    
                </tr>
                <tr>
                    <td class="tdBottom" align="right">Foto :</td>
                    <td class="tdBottom" colspan="1">  
                         <OBJECT ID="AxFoto" CLASSID="CLSID:579D29F0-DE47-49D6-8276-FD7FF74950B1" >
                         </OBJECT>   
			                        &nbsp;<br />
                        <asp:Label runat="server" ID="lblmensajefoto" Text=""></asp:Label>                   
                    </td>
                    <td class="tdBottom" align="right">Firma :</td>
                    <td class="tdBottom" colspan="1">  
                          <OBJECT ID="AxFirma" CLASSID="CLSID:579D29F0-DE47-49D6-8276-FD7FF74950B1" >
                         </OBJECT>  
			                        &nbsp;<br />
                        <asp:Label runat="server" ID="lblmensajefirma" Text=""></asp:Label>                   
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="titulos0">Información para Verificación</td>
                </tr>
                <tr>
                    <td colspan="4" align="left" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" >
                         <tr><td valign="top">    
                            <table cellpadding="0" cellspacing="0" border="0" >
                                
                                <%-- <tr>
                                    <td class="tdBottom" colspan="2" align="right" valign="top">Contenido Token :</td>
                                    <td class="tdBottom" colspan="2">
                                        <asp:RadioButton ID="rbtwsq" GroupName="grptoken" Text="WSQ" runat="server" /><br />
                                        <asp:RadioButton ID="rbtMinucias" GroupName="grptoken" Text="Minucias" runat="server" /><br />
                                        <asp:RadioButton ID="rbtAmbos" GroupName="grptoken" Text="Ambos" runat="server" Checked="true" />  
                                    </td>
                                </ tr>--%>
                                <tr>
                                    <td colspan="2" class="tdBottom" align="right"><br />
                                        Factor :</td>
                                    <td colspan="2" class="tdBottom"><br />
                                        <asp:DropDownList runat="server" ID="DropDownListAF">
                                            <asp:ListItem Value="0">Seleccione Factor</asp:ListItem>
                                            <asp:ListItem Value="1">Clave</asp:ListItem>                            
                                            <asp:ListItem Value="2" Selected="True">Huella Digital</asp:ListItem>
                                            <asp:ListItem Value="11">Certificado Digital</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <%--<tr>
                                    <td colspan="2" class="tdBottom" align="right">
                                        Algoritmo
                                        : </td>
                                    <td colspan="2" class="tdBottom">
                                        <asp:DropDownList runat="server" ID="DropDownListAlgoritmo" onchange="javascript:SetThreshold();">
                                            <asp:ListItem Value="0">Seleccione Algoritmo</asp:ListItem>
                                            <asp:ListItem Value="1">NEC</asp:ListItem>
                                            <asp:ListItem Value="2" Selected="True">Digital Persona</asp:ListItem>
                                            <asp:ListItem Value="3">Digital Persona Gold</asp:ListItem>
                                            <asp:ListItem Value="4">Digital Persona OTW</asp:ListItem>
                                            <asp:ListItem Value="7">Verifinger</asp:ListItem>
                                            <asp:ListItem Value="5">Secugen</asp:ListItem>
                                            <asp:ListItem Value="6">Testech</asp:ListItem>
                                            <asp:ListItem Value="8">Sagem</asp:ListItem>
                                            <asp:ListItem Value="9">Identix</asp:ListItem>
                                            <asp:ListItem Value="10">Upek</asp:ListItem>
                                            <asp:ListItem Value="11">Cogent</asp:ListItem>
                                            <asp:ListItem Value="12">Crossmatch</asp:ListItem>                            
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"  class="tdBottom" align="right">
                                        &nbsp;</td>
                                    <td colspan="2"  class="tdBottom" align="left" valign="middle">
                                        Umbral :<asp:TextBox runat="server" ID="txtUmbral" MaxLength="15" Text="0.0001" width="80" 
                                            onkeypress="javascript:return RestrictChar()"></asp:TextBox>
                                    </td>
                                </tr> --%>
                                <tr>
                                    <td colspan="2"  class="tdBottom" align="right">
                                        Device :</td>
                                    <td colspan="2"  class="tdBottom">
                                        <asp:DropDownList runat="server" ID="DropDownListDevice" onchange="javascript:SetModel();">
                                            <asp:ListItem Value="0">Seleccione Dispositivo</asp:ListItem>
                                            <asp:ListItem Value="1" Selected="True">Digital Persona</asp:ListItem>
                                            <asp:ListItem Value="2">Secugen</asp:ListItem>
                                            <asp:ListItem Value="7">Upek</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="tdBottom">Modelo :</td>
                                    <td colspan="3" class="tdBottom"><asp:DropDownList runat="server" ID="DropDownListModel" onchange="javascript:CheckModel();">
                                            <asp:ListItem Value="">Seleccione Modelo</asp:ListItem>
                                            <asp:ListItem Value="4000" Selected="True">U.are.U 4000</asp:ListItem>
                                            <asp:ListItem Value="4500">U.are.U 4500</asp:ListItem>
                                            <asp:ListItem Value="FDU02">Hamster II / III</asp:ListItem>
                                            <asp:ListItem Value="FDU03">Hamster Plus</asp:ListItem>
                                            <asp:ListItem Value="FDU04">Hamster IV</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="tdBottom">ConnectorId :</td>
                                    <td colspan="3" class="tdBottom">
                                        <asp:TextBox runat="server" ID="txtConnectorId" MaxLength="50" 
                                            onkeypress="javascript:return RestrictChar()"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr><td colspan="4" align="right">
                                    <asp:Button runat="server"  BorderStyle="None" ID="cmdInicializar" Text="Inicializar" SkinID="BotonAccion" OnClientClick="return SetBpClient();" />       
                                </td></tr>
                            </table>
                        </td>
                        <td colspan="1" align="center">
                             <OBJECT ID="BPClientAx1" CLASSID="CLSID:6E7AA4F1-BEBF-4A56-9DAE-8924F4667567" width="280" height="285">
                  			 </OBJECT>
                         </td>
                         </tr></table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:HiddenField runat="server" ID="hiddenOperacion" Value="" />
                        <asp:HiddenField runat="server" ID="hiddendata" Value="" />
                        <asp:HiddenField runat="server" ID="hiddendedo" Value="" />
                        <asp:HiddenField runat="server" ID="hiddencompanyid" Value="" />
                        <asp:HiddenField runat="server" ID="hiddenfirma" Value="" />
                        <asp:HiddenField runat="server" ID="hiddenfoto" Value="" />
                        <asp:HiddenField runat="server" ID="hiddenFotoSaved" Value="" />
                        <asp:HiddenField runat="server" ID="hiddenFotoPath" Value="" />
                        <asp:HiddenField runat="server" ID="hiddenFirmaSaved" Value="" />
                        <asp:HiddenField runat="server" ID="hiddenFirmaPath" Value="" />
                        <br />                      
                        <asp:Button runat="server" SkinId="BotonAccion" ID="cmdGuardar" Text="Agregar" OnClientClick="return ValidarFormularioEnroll('');" 
                            onclick="cmdGuardar_Click"  BorderStyle="None" />
                        <asp:Button runat="server" SkinID="BotonAccion" ID="cmdModificar" BorderStyle="None" 
                            Text="Modificar" OnClientClick="return ValidarFormularioEnroll('modificar');" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript" languaje="Javascript">

    document.getElementById("BPClientAx1").Operationtype = 2; //Enroll
    document.getElementById("AxFoto").Init(1);
    document.getElementById("AxFoto").SetSizeRet(125,140);
    document.getElementById("AxFirma").Init(2);
    document.getElementById("AxFirma").SetSizeRet(125, 70);
    if ('<%=hiddenFotoSaved.Value%>' == '1') {
        //alert('EntraFoto=' + '<%=hiddenfoto.Value%>');
//        document.getElementById("BPImageAx21").LoadImageTocontrol('<%=hiddenFotoPath.Value%>');
        document.getElementById("AxFoto").SetImage('<%=hiddenfoto.Value%>');
    }
    if ('<%=hiddenFirmaSaved.Value%>' == '1') {
        //alert('EntraFirma');
        document.getElementById("AxFirma").SetImage('<%=hiddenfirma.Value%>');
        //document.getElementById("BPImageAx22").LoadImageTocontrol('<%=hiddenFirmaPath.Value%>');
    }     
	
</script>

</asp:Content>
