﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using Bio.Portal.Server.Manager.Core.libs;
using Bio.Portal.Server.Common.Entities.Database;
using Bio.Portal.Server.Common.Common;

namespace Bio.Portal.Server.Manager.Core.Enroll
{
    public partial class VerInfo : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(VerInfo));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 4)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                //BioPortalServer.VisualizarMenu(Master, User);
                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);
                if (Request["tipo"] != null)
                {
                    String tipo = Request["tipo"].ToString();
                    String nroid = Request["valor"].ToString();
                    //Tomamos los valores enviados por javascript y los mostramos.
                    lblMensaje.Text = tipo + " " + nroid;
                    //Enlazamos el ObjectDataSource con la Grilla.
                    ObjectDataSourceBirs.Select();
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en VerInfo.aspx al momento de ver información de una identidad" , exe);
                Session["mensajeerror"] = "Se ha producido un error en VerInfo.aspx al momento de ver información de una identidad";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }

        }

        protected void ObjectDataSourceBirs_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            try
            {
                if (Request["id"] != null)
                {
                    e.InputParameters["id"] = Convert.ToInt32(Request["id"].ToString());
                }
              
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en VerInfo.aspx al momento de recuperar los bir asociado a una identidad" , exe);
                Session["mensajeerror"] = "Se ha producido un error en VerInfo.aspx al momento de recuperar los bir asociado a una identidad";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceBirs_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                //Si no trae resultados
                if (e.ReturnValue != null)
                {
                    IList<BpBir> birs = (IList<BpBir>)e.ReturnValue;
                    if (birs.Count > 0)
                    {
                        GridViewBirs.DataSource = birs;
                        GridViewBirs.DataBind();
                    }
                }
            }
            else
            {
               
                log.Error("Se ha producido un error en VerInfo.aspx al momento de ver información de una identidad" + e.Exception);
                Session["mensajeerror"] = "Se ha producido un error en VerInfo.aspx al momento de ver información de una identidad";
                Session["exception"] = e.Exception;
                Response.Redirect("~/Error.aspx");
            
            }
        }

        protected void GridViewBirs_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            try
            {
                GridViewBirs.PageIndex = e.NewPageIndex;
                ObjectDataSourceBirs.Select();
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en VerInfo.aspx al momento de ver birs de una identidad-paginación" , exe);
                Session["mensajeerror"] = "Se ha producido un error en VerInfo.aspx al momento de ver birs de una identidad-paginación";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }

        }

      
    }
}