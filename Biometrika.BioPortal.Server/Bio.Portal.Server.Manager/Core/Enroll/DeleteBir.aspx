﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" EnableViewStateMac="false" CodeBehind="DeleteBir.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Enroll.DeleteBir" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td colspan="3" class="titulos0">        
            Eliminando Identidad..
            <asp:ObjectDataSource ID="ObjectDataSourceBir" runat="server" 
                onselected="ObjectDataSourceBir_Selected" DeleteMethod="DeleteIdentity" 
                onselecting="ObjectDataSourceBir_Selecting" SelectMethod="GetIdentity" 
                ondeleted="ObjectDataSourceBir_Deleted" 
                ondeleting="ObjectDataSourceBir_Deleting"
                TypeName="Bio.Portal.Server.Manager.Core.Database.DatabaseHelper">
                <SelectParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                    <asp:Parameter Direction="Output" Name="msgerr" Type="String" />
                </SelectParameters>
                 <DeleteParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                    <asp:Parameter Direction="Output" Name="msgerr" Type="String" />
                </DeleteParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">ID</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtId" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="3" class="tdBottom">
              <table cellpadding="0" cellspacing="0">
                  <tr>
                    <td >Verificado</td>
                    <td >:</td>
                    <td >                                    
                        <asp:RadioButton ID="rbtnoverificado" Enabled="false" GroupName="grpverificado" Text="No Verificado" runat="server" />
                        <asp:RadioButton ID="rbtverificadoconci" Enabled="false" GroupName="grpverificado" Text="Verificado con CI" runat="server" />
                        <asp:RadioButton ID="rbtverificadosrcei" Enabled="false" GroupName="grpverificado" Visible="false" Text="Verificado SRCeI Online" runat="server" />
                        <asp:RadioButton ID="rbtotro" GroupName="grpverificado" Enabled="false" Text="Otro" runat="server" />
                        <asp:TextBox runat="server" Enabled="false" ID="txtotro" Width="200"></asp:TextBox>
                    </td>
                  </tr>
              </table>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Nombres</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtNombre" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Apellidos</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtApellidos" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Tipo Id</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtTipoId" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="tdBottom">Valor Id</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtNroId" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr>         
        <td colspan="3">
            <asp:Button runat="server" SkinID="BotonAccion" ID="txtDelete" Text="Eliminar" onclick="cmdEliminar_Click"  BorderStyle="None" />
            <asp:Button runat="server" SkinID="BotonAccion" ID="txtCancelar" Text="Cancerlar" onclick="txtCancelar_Click" BorderStyle="None" />
            <asp:HiddenField runat="server" ID="hiddenorigin" Value="" />
            <asp:HiddenField runat="server" ID="hiddenTypeId" Value="" />
       </td>
    </tr>
</table>
</asp:Content>
