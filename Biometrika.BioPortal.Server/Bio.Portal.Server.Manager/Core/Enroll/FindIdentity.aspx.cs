﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Text;
using System.Web.UI.HtmlControls;
using Bio.Portal.Server.Manager.Core.libs;
using Bio.Portal.Server.Manager.Core.Database;
using Bio.Portal.Server.Common.Common;
using System.Collections;

namespace Bio.Portal.Server.Manager.Core.Enroll
{
    public partial class FindIdentity : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FindIdentity));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();

                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);
                HtmlGenericControl myJs2 = new HtmlGenericControl();
                myJs2.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs2.Attributes.Add("language", "javascript");
                myJs2.Attributes.Add("src", ResolveUrl("~/Scripts/dhtmlgoodies_calendar.js"));
                this.Page.Header.Controls.Add(myJs2);

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 4)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);
                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);

                RenderJSArrayWithCliendIds(hiddenOperacion, hiddencompanyid, hiddenidentityid);

                if (!IsPostBack)
                {
                    labMsg.Text = Session["MsgDeleteBir"] != null ? (string) Session["MsgDeleteBir"] : "";
                    Session["MsgDeleteBir"] = null;
                    DropDownListTipoId.DataSource = Global.documentos.documents;
                    DropDownListTipoId.DataValueField = "ID";
                    DropDownListTipoId.DataTextField = "Description";
                    DropDownListTipoId.DataBind();

                    Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                    if (User.IsInRole("Super Administrador"))
                    {
                        DropDownListCompany.DataSource = DatabaseHelper.RetrieveAllCompanyActive(Domain);
                        DropDownListCompany.DataBind();
                        company.Visible = true;
                    }
                    else
                    {
                        //En este caso de trata de un usuario administrador de la empresa
                        //almacenamos en un campo Hidden el Company Id del usuario.
                        company.Visible = false;
                        hiddencompanyid.Value = CompanyId.ToString();
                    }
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en FindIdentity.aspx" , exe);
                Session["mensajeerror"] = "Se ha producido un error en FindIdentity.aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void DropDownListTipoId_DataBound(object sender, EventArgs e)
        {
            DropDownListTipoId.Items.Insert(0, new ListItem("--Todos--", ""));
        }
        protected void DropDownListCompany_DataBound(object sender, EventArgs e)
        {
            DropDownListCompany.Items.Insert(0, new ListItem("--Todos--", "0"));
        }
        protected void cmdAddNew_Click(object sender, EventArgs e)
        {
            
            Response.Redirect("RegisterIdentity.aspx");
        }
        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }

        protected void cmdGuardar_Click(object sender, EventArgs e)
        {
            
            ObjectDataSourceIdentity.Select();
        }

        protected void ObjectDataSourceIdentity_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            try
            {
                int companyid=0;
                companyid = Convert.ToInt32(User.IsInRole("Super Administrador") ? 
                                            DropDownListCompany.SelectedItem.Value.ToString() : 
                                            hiddencompanyid.Value);
                e.InputParameters["companyidenroll"]=companyid;
                e.InputParameters["typeid"]=DropDownListTipoId.SelectedItem.Value;
                e.InputParameters["valueid"]= txtNroId.Text;
                e.InputParameters["name"]=txtNombre.Text;
                e.InputParameters["lastname"]=txtApellidoP.Text;

            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en FindIdentity.aspx al momento de buscar una identidad" , exe);
                Session["mensajeerror"] = "Se ha producido un error en FindIdentity.aspx al momento de buscar una identidad";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceIdentity_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                if (e.ReturnValue != null)
                {
                    IList resultados = (IList)e.ReturnValue;
                    if (resultados.Count > 0)
                    {
                        GridViewIdentity.DataSource = (IList)e.ReturnValue;
                    } else
                    {
                        GridViewIdentity.DataSource = null;
                    }
                    GridViewIdentity.DataBind();
                }
            }
            else
            {
            
                log.Error("Se ha producido un error en FindIdentity.aspx al momento de recuperar las identidades" + e.Exception);
                Session["mensajeerror"] = "Se ha producido un error en FindIdentity.aspx al momento de recuperar las identidades";
                Session["exception"] = e.Exception;
                Response.Redirect("~/Error.aspx");
            
            }
        }

        protected void GridViewIdentity_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                ObjectDataSourceIdentity.Select();
                GridViewIdentity.PageIndex = e.NewPageIndex;
                GridViewIdentity.DataBind();
            }
            catch (Exception exe)
            {
                if (exe.GetType().FullName != "System.Threading.ThreadAbortException")
                {
                    log.Error("Se ha producido un error en ListOrigen.aspx en la ejecución de una acción sobre la grilla.", exe);
                    Session["mensajeerror"] = "Se ha producido un error en ListOrigen.aspx en la ejecución de una acción sobre la grilla";
                    Session["exception"] = exe;
                    Response.Redirect("~/Error.aspx");
                }
            }
        }
       
    }
}