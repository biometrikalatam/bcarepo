﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bio.Portal.Server.Common.Common;
using log4net;
using Bio.Portal.Server.Manager.Core.libs;
using BioPortal.Server.Api;

namespace Bio.Portal.Server.Manager.Core.Enroll
{
    public partial class Message : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Message));
        protected void Page_Load(object sender, EventArgs e)
        {
            Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");

            //Chequea si tiene permisos para acceder a esta página
            if (BioPortalServer.GetRolId(User) > 4)
            {
                throw new Exception("No tiene privilegios para ingresar en esta opción.");
                //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                //Session["exception"] = null;
                ////Response.Redirect("~/Error.aspx", true);
                //Server.Transfer("~/Error.aspx");
            }

            //Dibuja Menu
            BioPortalServer.VisualizarMenu(Master, User);
            //Verificamos accesos
            //if (User.IsInRole("Super Administrador"))
            //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
            //else if (User.IsInRole("Administrador"))
            //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

            //if (User.IsInRole("Enrolador"))
            //    BioPortalServer.VisualizarMenuEnrolador(Master);
            //if (User.IsInRole("Verificador"))
            //    BioPortalServer.VisualizarMenuVerificar(Master);

            if (Session["xmlparamout"] != null)
            {
                XmlParamOut parametros = Bio.Core.Utils.SerializeHelper.DeserializeFromXml<XmlParamOut>(Session["xmlparamout"].ToString());
                if (parametros.Message==null)
                {
                    lblmensaje.Text = "La persona: " + parametros.PersonalData.Name + " " + parametros.PersonalData.Patherlastname + " " + parametros.PersonalData.Motherlastname + "\n Tipo ID:" + parametros.PersonalData.Typeid +  "\n Nro ID:" + parametros.PersonalData.Valueid + " fue enrolada con éxito";
                }
                else
                {
                    lblmensaje.Text = "";                    
                    lblmensaje.Text = lblmensaje.Text + parametros.Message;
                }
            }
        }
    }
}