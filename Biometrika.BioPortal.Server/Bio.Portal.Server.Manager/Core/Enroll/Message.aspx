﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="Message.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Enroll.Message" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<table cellspacing="0" cellpadding="0">
    <tr>
        <td class="titulos0">Resultados de la Operación</td>
    </tr>
    <tr>
        <td class="tdBottom">
            <asp:Label runat="server" ID="lblmensaje"></asp:Label>
        </td>
    </tr>
</table>
</asp:Content>
