﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bio.Core.Constant;
using Bio.Portal.Server.Common.Common;
using log4net;
using System.Web.UI.HtmlControls;
using Bio.Portal.Server.Manager.Core.libs;
using Bio.Portal.Server.Common.Entities.Database;
using BioPortal.Server.Api;
using Bio.Core.Api;
using System.Text;

namespace Bio.Portal.Server.Manager.Core.Enroll
{
    public partial class AddBir : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AddBir));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();

                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);
                HtmlGenericControl myJs2 = new HtmlGenericControl();
                myJs2.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs2.Attributes.Add("language", "javascript");
                myJs2.Attributes.Add("src", ResolveUrl("~/Scripts/dhtmlgoodies_calendar.js"));
                this.Page.Header.Controls.Add(myJs2);
                RenderJSArrayWithCliendIds(hiddenorigin,//0
                                           hiddendata,//1
                                           hiddendedo,//2
                                           hiddencompanyid,//3
                                           txtTipoId, //4
                                           txtNroId,//5
                                           txtNroId,//6
                                           hiddenTypeId,//7
                                           hiddenTypeId, //hiddenOperacion,//8
                                           hiddendata,//9
                                           hiddendedo,//10
                                           hiddendedo,//11
                                           hiddendedo,//12
                                           hiddendedo,//13
                                           hiddendedo, //14
                                           DropDownListAF, //15
                                           DropDownListDevice, //16
                                           DropDownListModel); //17

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 4)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                    //Session["mensajeerror"] = "No tiene privilegios para ingresar en esta opción.";
                    //Session["exception"] = null;
                    ////Response.Redirect("~/Error.aspx", true);
                    //Server.Transfer("~/Error.aspx");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);
                //Verificamos accesos
                //if (User.IsInRole("Super Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
                //else if (User.IsInRole("Administrador"))
                //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

                //if (User.IsInRole("Enrolador"))
                //    BioPortalServer.VisualizarMenuEnrolador(Master);
                //if (User.IsInRole("Verificador"))
                //    BioPortalServer.VisualizarMenuVerificar(Master);

                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                int id = 0;
                if (!Page.IsPostBack)
                {
                    if (Request["id"] != null)
                    {
                        id = Convert.ToInt32(Request["id"].ToString());
                        ObjectDataSourceBir.Select();
                        hiddenorigin.Value = "2"; //BP Manager
                    }
                }
                else
                {
                    try
                    {
                        if (hiddendata.Value != "")
                        {
                            //Llamamos al servicio de enrolamiento 
                            XmlParamIn pin = new XmlParamIn();
                            using (BioWs.BioPortalServerWSWeb ws = new BioWs.BioPortalServerWSWeb())
                            {
                                ws.Timeout = 99999;
                                pin.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_ADDBIR;
                                pin.Authenticationfactor = Convert.ToInt32(DropDownListAF.SelectedValue);
                                // Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;
                                pin.Bodypart = Convert.ToInt32(hiddendedo.Value);
                                //Bio.Core.Matcher.Constant.BodyPart.BODYPART_DEDOANULARDERECHO;
                                pin.Minutiaetype = 2; // Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER;
                                pin.Clientid = "BPManager";
                                pin.InsertOption = 0;
                                pin.OperationOrder = 0;
                                pin.Companyid = Convert.ToInt32(hiddencompanyid.Value);
                                pin.Enduser = "BPManager";
                                pin.Ipenduser = Request.ServerVariables["REMOTE_HOST"].ToString();
                                pin.Matchingtype = 1;
                                //if (hiddenorigin.Value == "")
                                //    pin.Origin = 1;
                                //else
                                //    pin.Origin = Convert.ToInt32(hiddenorigin.Value);
                                pin.Origin = 2; //BPManager
                                String Data = hiddendata.Value;
                                String Finger = hiddendedo.Value;
                                pin.SampleCollection = new List<Sample>();
                                Sample sample = new Sample
                                                    {
                                                        Data = Data,
                                                        Minutiaetype =
                                                            Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_TOKEN,
                                                        Additionaldata = null
                                                    };
                                pin.SampleCollection.Add(sample);
                                pin.SaveVerified = 0;
                                //pin.Threshold = 0.00001;
                                pin.Userid = UserId;
                                pin.Verifybyconnectorid = txtConnectorId.Text.Equals(null) ||
                                                          txtConnectorId.Text.Trim().Length == 0
                                                              ? "final"
                                                              : txtConnectorId.Text.Trim();

                                //Datos de la Persona
                                pin.PersonalData = new PersonalData();
                                pin.PersonalData.Typeid = hiddenTypeId.Value;
                                pin.PersonalData.Id = Convert.ToInt32(txtId.Text);
                                pin.PersonalData.Valueid = txtNroId.Text;

                                //Serializamos.
                                string xmlparamin = XmlUtils.SerializeAnObject(pin);
                                string xmlparamout = "";
                                int respuesta = ws.Enroll(xmlparamin, out xmlparamout);
                                //Session["xmlparamout"] = xmlparamout;
                                //Response.Redirect("Message.aspx");
                                if (respuesta == Errors.IERR_OK)
                                {
                                    //Session["respuesta"] = respuesta;
                                    //Session["xmlparamout"] = xmlparamout;
                                    Session["MsgDeleteBir"] = "Operación completada con éxito!";
                                }
                                else
                                {
                                    throw new Exception("Error agregando BIR de identidad [" +
                                                        respuesta.ToString() + ": " +
                                                        Errors.GetDescription(respuesta) + "]");
                                }
                                Response.Redirect("FindIdentity.aspx", false);
                            }
                        }
                    }
                    catch (Exception exe)
                    {
                        if (exe.GetType().FullName != "System.Threading.ThreadAbortException")
                        {
                            log.Error("Se ha producido un error en AddBir.aspx al momento de agregar una identidad", exe);
                            Session["mensajeerror"] =
                                "Se ha producido un error en AddBir.aspx al momento de agregar identidad";
                            Session["exception"] = exe;
                            Response.Redirect("~/Error.aspx");
                        }
                    }
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en AddBir.aspx" , exe);
                Session["mensajeerror"] = "Se ha producido un error en AddBir.aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }


        }
        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }
        protected void ObjectDataSourceBir_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            try
            {
                e.InputParameters["id"] = Convert.ToInt32(Request["id"].ToString());
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en AddBir.aspx al momento de seleccionar" , exe);
                Session["mensajeerror"] = "Se ha producido un error en AddBir.aspx al momento de seleccionar";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void ObjectDataSourceBir_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                if (e.ReturnValue != null)
                {
                    BpIdentity identity = (BpIdentity)e.ReturnValue;
                    txtNombre.Text = identity.Name;
                    txtApellidos.Text = identity.Patherlastname + " " + identity.Motherlastname;
                    txtNroId.Text = identity.Valueid.ToString();
                    txtTipoId.Text = identity.Typeid.ToString();
                    txtId.Text = identity.Id.ToString();
                    hiddencompanyid.Value = identity.Companyidenroll.Id.ToString();
                    hiddenTypeId.Value = identity.Typeid.Trim(); // Global.documentos.FindDocument(identity.Typeid.Trim()).Description;
                }
            }
            else
            {
                log.Error("Se ha producido un error en AddBir.aspx al momento de recuperar bir" + e.ReturnValue);
                Session["mensajeerror"] = "Se ha producido un error en AddBir.aspx al momento de recuperar bir";
                Session["exception"] = e.ReturnValue;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void txtAgregar_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (hiddendata.Value != "")
            //    {
            //        //Llamamos al servicio de enrolamiento 
            //        XmlParamIn pin = new XmlParamIn();
            //        using (BioWs.BioPortalServerWSWeb ws = new BioWs.BioPortalServerWSWeb())
            //        {
            //            ws.Timeout = 99999;
            //            pin.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_ADDBIR;
            //            pin.Authenticationfactor = Convert.ToInt32(DropDownListAF.SelectedValue); // Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;
            //            pin.Bodypart = Convert.ToInt32(hiddendedo.Value); //Bio.Core.Matcher.Constant.BodyPart.BODYPART_DEDOANULARDERECHO;
            //            pin.Minutiaetype = 2; // Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER;
            //            pin.Clientid = "BPManager";
            //            pin.InsertOption = 0;
            //            pin.OperationOrder = 0;
            //            pin.Companyid = Convert.ToInt32(hiddencompanyid.Value);
            //            pin.Enduser = "BPManager";
            //            pin.Ipenduser = Request.ServerVariables["REMOTE_HOST"].ToString();
            //            pin.Matchingtype = 1;
            //            //if (hiddenorigin.Value == "")
            //            //    pin.Origin = 1;
            //            //else
            //            //    pin.Origin = Convert.ToInt32(hiddenorigin.Value);
            //            pin.Origin = 2; //BPManager
            //            String Data = hiddendata.Value;
            //            String Finger = hiddendedo.Value;
            //            pin.SampleCollection = new List<Sample>();
            //            Sample sample = new Sample
            //                                {
            //                                    Data = Data,
            //                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_TOKEN,
            //                                    Additionaldata = null
            //                                };
            //            pin.SampleCollection.Add(sample);
            //            pin.SaveVerified = 0;
            //            //pin.Threshold = 0.00001;
            //            pin.Userid = UserId;
            //            pin.Verifybyconnectorid = txtConnectorId.Text.Equals(null) ||  
            //                                      txtConnectorId.Text.Trim().Length == 0 ? 
            //                "final" : txtConnectorId.Text.Trim();

            //            //Datos de la Persona
            //            pin.PersonalData = new PersonalData();
            //            pin.PersonalData.Typeid = hiddenTypeId.Value;
            //            pin.PersonalData.Id = Convert.ToInt32(txtId.Text);
            //            pin.PersonalData.Valueid = txtNroId.Text;

            //            //Serializamos.
            //            string xmlparamin = XmlUtils.SerializeAnObject(pin);
            //            string xmlparamout = "";
            //            int respuesta = ws.Enroll(xmlparamin, out xmlparamout);
            //            //Session["xmlparamout"] = xmlparamout;
            //            //Response.Redirect("Message.aspx");
            //            if (respuesta == Errors.IERR_OK)
            //            {
            //                //Session["respuesta"] = respuesta;
            //                //Session["xmlparamout"] = xmlparamout;
            //                Session["MsgDeleteBir"] = "Operación completada con éxito!";
            //            }
            //            else
            //            {
            //                throw new Exception("Error agregando BIR de identidad [" +
            //                                     respuesta.ToString() + ": " +
            //                                     Errors.GetDescription(respuesta) + "]");
            //            }
            //            Response.Redirect("FindIdentity.aspx", true);
            //        }
            //    }
            //}
            //catch (Exception exe)
            //{
            //    if (exe.GetType().FullName != "System.Threading.ThreadAbortException")
            //    {
            //        log.Error("Se ha producido un error en AddBir.aspx al momento de agregar una identidad", exe);
            //        Session["mensajeerror"] = "Se ha producido un error en AddBir.aspx al momento de agregar identidad";
            //        Session["exception"] = exe;
            //        Response.Redirect("~/Error.aspx");
            //    }
            //}
        }
    }
}