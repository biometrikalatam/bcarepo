﻿using System;
using Bio.Core.Constant;
using Bio.Portal.Server.Common.Config;
using log4net;

namespace Bio.Portal.Server.Manager.Core.WebServices
{

    /// <summary>
    /// Clase destinada a resumir todas las rutinas referidas a accesos a BD.
    /// </summary>
    public class WebServicesHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(WebServicesHelper));

#region Métodos WS Admin de Services

        public static int RefreshCompaniesInServices(out string msg)
        {
            int iErr = Errors.IERR_OK;
            msg = null;

            try
            {
                using (Services.BioPortalServerAdmin wsadmin = new Services.BioPortalServerAdmin())
                {
                    iErr = wsadmin.UpdateCompanys(null, out msg);
                }
            }
            catch (Exception ex)
            {
                iErr = Errors.IERR_UNKNOWN;
                msg = ex.Message;
                LOG.Error("WebServicesHelper.SelectConfiguration Error", ex);
            }
            return iErr;
        }

#region BpConfig

        public static BpConfig SelectConfiguration()
        {
            string xmloutput = "";
            string msg;
            BpConfig config = null;

            try
            {
                using (Services.BioPortalServerAdmin bpconfig = new Services.BioPortalServerAdmin())
                {
                    int i = bpconfig.GetConfig(out xmloutput, out msg);
                    if (i == Errors.IERR_OK)
                    {
                        config = Bio.Core.Utils.SerializeHelper.DeserializeFromXml<BpConfig>(xmloutput);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("WebServicesHelper.SelectConfiguration Error", ex);
            }
            return config;
        }

        /// <summary>
        /// Este método permite grabar los nuevos valores de la configuración.
        /// Estos cambios no se guardan en base de datos, sino que se actualizan vía web services al sitio de services
        /// </summary>
        /// <param name="insertOption"></param>
        /// <param name="saveVerified"></param>
        /// <param name="minutiaetypeDefault"></param>
        /// <param name="timeoutWSdefault"></param>
        /// <param name="minutiaetypeToEnroll"></param>
        /// <param name="matchingtype"></param>
        /// <param name="operationOrder"></param>
        /// <param name="checkAccess"></param>
        /// <param name="connectorId"></param>
        /// <returns></returns>
        public static int SaveConfiguration(int insertOption,
                                            int saveVerified,
                                            int minutiaetypeDefault,
                                            int timeoutWSdefault,
                                            int[] minutiaetypeToEnroll,
                                            int matchingtype,
                                            int operationOrder,
                                            int checkAccess,
                                            string connectorId)
        {
            int res = 0;
            string msg = "";
            try
            {
                //Creamos un objeto BpConfig y actulizamos los datos.
                BpConfig config = new BpConfig
                {
                    InsertOption = insertOption,
                    SaveVerified = saveVerified,
                    MinutiaetypeDefault = minutiaetypeDefault,
                    TimeoutWSdefault = timeoutWSdefault,
                    Matchingtype = matchingtype,
                    OperationOrder = operationOrder,
                    CheckAccess = checkAccess,
                    ConnectorDefault = connectorId
                };
                //config.MinutiaetypeToEnroll = minutiaetypeToEnroll;

                //Generamos el objeto que va a llamar al webservices.
                using (Services.BioPortalServerAdmin servicioconfig = new Services.BioPortalServerAdmin())
                {

                    //Serializamos el objeto.
                    string xmlinput = Bio.Core.Utils.SerializeHelper.SerializeToXml(config);

                    //Llamamos al método del webservices que actualiza la configuración.
                    res = servicioconfig.UpdateConfig(xmlinput, out msg);
                }

            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("WebServicesHelper.SaveConfiguration Error", ex);
            }
            return res;
        }
        #endregion

#endregion Métodos WS Admin de Services
    }
}