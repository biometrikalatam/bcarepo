﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bio.Core.Constant;
using Bio.Core.Matcher.Constant;
using log4net;
using System.Web.UI.HtmlControls;
using System.Text;
using Bio.Portal.Server.Manager.Core.Database;
using Bio.Portal.Server.Common.Common;
using BioPortal.Server.Api;
using Bio.Core.Api;

namespace Bio.Portal.Server.Manager.Core.Verify
{
    public partial class Verify : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Verify));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 5)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);

                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);
                HtmlGenericControl myJs2 = new HtmlGenericControl();
                myJs2.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs2.Attributes.Add("language", "javascript");
                myJs2.Attributes.Add("src", ResolveUrl("~/Scripts/dhtmlgoodies_calendar.js"));
                RenderJSArrayWithCliendIds(txtrut, DropDownListTipoId, hiddendata, hiddendedo, hiddenOperacion,
                                           DropDownListAF, DropDownListAlgoritmo, DropDownListDevice,
                                           DropDownListModel, rbtwsq, rbtMinucias, rbtAmbos, txtUmbral);
                this.Page.Header.Controls.Add(myJs2);
                //cmdCalendarioFecNac.Attributes.Add("onclick", "javascript:displayCalendar(" + this.txtFecVenc.ClientID + ",'dd/mm/yyyy',this);return false;");
                if (!Page.IsPostBack)
                {
                    Session["respuesta"] = 1;
                    Session["xmlparamout"] = null;

                    DropDownListTipoId.DataSource = Global.documentos.documents;
                    DropDownListTipoId.DataValueField = "ID";
                    DropDownListTipoId.DataTextField = "Description";
                    DropDownListTipoId.DataBind();
                    if (User.IsInRole("Super Administrador"))
                    {
                        DropDownListCompany.DataSource = DatabaseHelper.RetrieveAllCompanyActive(Domain);
                        DropDownListCompany.DataBind();
                        company.Visible = true;
                    }
                    else
                    {
                        //En este caso de trata de un usuario administrador de la empresa
                        //almacenamos en un campo Hidden el Company Id del usuario.
                        company.Visible = false;
                        hiddencompanyid.Value = CompanyId.ToString();
                    }

                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en Verify.aspx " , exe);
                Session["mensajeerror"] = "Se ha producido un error en Verify.aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }
        protected void DropDownListTipoId_DataBound(object sender, EventArgs e)
        {
            DropDownListTipoId.Items.Insert(0, new ListItem("Seleccione un tipo ID", "0"));
        }
        protected void DropDownListCompany_DataBound(object sender, EventArgs e)
        {
            DropDownListCompany.Items.Insert(0, new ListItem("Seleccione una compañía", "0"));
        }
        public void RenderJSArrayWithCliendIds(params Control[] wc)
        {
            if (wc.Length > 0)
            {
                StringBuilder arrClientIDValue = new StringBuilder();
                StringBuilder arrServerIDValue = new StringBuilder();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = Page.ClientScript;

                // Now loop through the controls and build the client and server id's
                for (int i = 0; i < wc.Length; i++)
                {
                    arrClientIDValue.Append("\"" + wc[i].ClientID + "\",");
                    arrServerIDValue.Append("\"" + wc[i].ID + "\",");
                }
                // Register the array of client and server id to the client
                cs.RegisterArrayDeclaration("MyClientID", arrClientIDValue.ToString().Remove(arrClientIDValue.ToString().Length - 1, 1));
                cs.RegisterArrayDeclaration("MyServerID", arrServerIDValue.ToString().Remove(arrServerIDValue.ToString().Length - 1, 1));
                // Now register the method GetClientId, used to get the client id of tthe control
                cs.RegisterStartupScript(this.Page.GetType(), "key", "\nfunction GetClientId(serverId)\n{\nfor(i=0; i<MyServerID.length; i++)\n{\nif ( MyServerID[i] == serverId )\n{\nreturn MyClientID[i];\nbreak;\n}\n}\n}", true);
            }
        }

        protected void cmdVerificarBDLocal_Click(object sender, EventArgs e)
        {
            try
            {
                String NroID = txtrut.Text;
                String TipoID = DropDownListTipoId.SelectedValue;
                String Data = hiddendata.Value;
                String Finger = hiddendedo.Value;
                XmlParamIn pin = new XmlParamIn();

                using (BioWs.BioPortalServerWSWeb ws = new BioWs.BioPortalServerWSWeb())
                {
                    ws.Timeout = Bio.Portal.Server.Manager.Properties.Settings.Default.TimeoutWS;
                    //Verificamos si recuperamos mas info o solo es verificacion
                    if (chkrecuperar.Checked)
                    {
                        pin.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_VERIFYANDGET;
                    } else
                    {
                        pin.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_VERIFY;
                    }

                    pin.Authenticationfactor = Convert.ToInt32(DropDownListAF.SelectedValue); // Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;

                    //vERIFICAMOS SI SE IGNORA EL DEDO O NO.
                    if (chkIgnoreDedo.Checked == true)
                        pin.Bodypart = Bio.Core.Matcher.Constant.BodyPart.BODYPART_ALL;
                    else
                        pin.Bodypart = Convert.ToInt32(Finger);

                    //Tomamos el algoritmo seleccionado.

                    if (DropDownListAlgoritmo.SelectedIndex != 0)
                    {
                        pin.Minutiaetype = Convert.ToInt32(DropDownListAlgoritmo.SelectedValue); // DropDownListAlgoritmo.SelectedIndex;  //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER;
                    }
                    pin.Clientid = Request.ServerVariables["REMOTE_HOST"].ToString();
                    pin.InsertOption = 0; //Default
                    pin.OperationOrder = Convert.ToInt32(DropDownListOperationOrder.SelectedValue); //OperationOrder.OPERATIONORDER_DEFAULT; 
                    pin.Companyid = CompanyId;
                    pin.Enduser = UserNameFull;
                    pin.Ipenduser = Request.ServerVariables["REMOTE_HOST"].ToString();
                    pin.Matchingtype = 1;
                    pin.Origin = 2; //BP Manager
                    pin.SampleCollection = new List<Sample>();
                    Sample sample = new Sample
                                        {
                                            Data = Data,
                                            Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_TOKEN,
                                            Additionaldata = null
                                        };
                    pin.SampleCollection.Add(sample);
                    pin.SaveVerified = 0; //Default
                    pin.Threshold = !String.IsNullOrEmpty(txtUmbral.Text) ? 
                                    Convert.ToDouble(txtUmbral.Text) : 0;
                    pin.Userid = UserId;
                    pin.Verifybyconnectorid = !String.IsNullOrEmpty(txtConnectorId.Text) ?
                                              txtConnectorId.Text : null;
                    pin.InsertOption = 0; //Default
                    //Datos de la Persona
                    pin.PersonalData = new PersonalData
                                           {
                                               Typeid = TipoID,
                                               Valueid = NroID,
                                           };


                    //Serializamos.
                    string xmlparamin = XmlUtils.SerializeAnObject(pin);
                    string xmlparamout = "";
                    log.Debug("Verify.aspx - LLamando Verify - " +
                        "       WS = " + ws.Url + Environment.NewLine +  
                        "       xmlparamin = " + xmlparamin); 
                    int respuesta = ws.Verify(xmlparamin, out xmlparamout);
                    log.Debug("Verify.aspx - Verify Return = " + respuesta.ToString() + "- xmlparamout = " + xmlparamout);
                    if (respuesta == Errors.IERR_OK || 
                        respuesta == Errors.IERR_BIR_NOT_FOUND)
                    {
                        Session["respuesta"] = respuesta;
                        Session["xmlparamout"] = xmlparamout;
                    } else
                    {
                        throw new Exception("Error verificando identidad [" +
                                             respuesta.ToString() + ": " +
                                             Errors.GetDescription(respuesta) + "]");
                    }
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error desconocido en Verify.aspx al momento de verificar" , exe);
                Session["mensajeerror"] = "Se ha producido un error desconocido en Verify.aspx al momento de verificar [" 
                                            + exe.Message + "]";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
            Response.Redirect("Result.aspx");
        }

        protected void cmdIdentificarBDLocal_Click(object sender, EventArgs e)
        {
            try
            {
                String NroID = txtrut.Text;
                String TipoID = DropDownListTipoId.SelectedValue;
                String Data = hiddendata.Value;
                String Finger = hiddendedo.Value;
                XmlParamIn pin = new XmlParamIn();

                using (BioWs.BioPortalServerWSWeb ws = new BioWs.BioPortalServerWSWeb())
                {
                    ws.Timeout = Bio.Portal.Server.Manager.Properties.Settings.Default.TimeoutWS;
                    pin.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_IDENTIFY;
                    
                    pin.Authenticationfactor = Convert.ToInt32(DropDownListAF.SelectedValue); // Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;

                    pin.Bodypart = Bio.Core.Matcher.Constant.BodyPart.BODYPART_ALL;
                    
                    //Tomamos el algoritmo seleccionado.
                    if (DropDownListAlgoritmo.SelectedIndex != 0)
                    {
                        pin.Minutiaetype = Convert.ToInt32(DropDownListAlgoritmo.SelectedValue); // DropDownListAlgoritmo.SelectedIndex;  //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER;
                    }
                    pin.Clientid = Request.ServerVariables["REMOTE_HOST"].ToString();
                    pin.InsertOption = 0; //Default
                    pin.OperationOrder = Convert.ToInt32(DropDownListOperationOrder.SelectedValue); //OperationOrder.OPERATIONORDER_DEFAULT; 
                    pin.Companyid = CompanyId;
                    pin.Enduser = UserNameFull;
                    pin.Ipenduser = Request.ServerVariables["REMOTE_HOST"].ToString();
                    pin.Matchingtype = 1;
                    pin.Origin = 2; //BP Manager
                    pin.SampleCollection = new List<Sample>();
                    Sample sample = new Sample
                    {
                        Data = Data,
                        Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_TOKEN,
                        Additionaldata = null
                    };
                    pin.SampleCollection.Add(sample);
                    pin.SaveVerified = 0; //Default
                    pin.Threshold = !String.IsNullOrEmpty(txtUmbral.Text) ?
                                    Convert.ToDouble(txtUmbral.Text) : 0;
                    pin.Userid = UserId;
                    pin.Verifybyconnectorid = !String.IsNullOrEmpty(txtConnectorId.Text) ?
                                              txtConnectorId.Text : null;
                    pin.InsertOption = 0; //Default
                    //Datos de la Persona
                    //pin.PersonalData = new PersonalData
                    //{
                    //    Typeid = TipoID,
                    //    Valueid = NroID,
                    //};


                    //Serializamos.
                    string xmlparamin = XmlUtils.SerializeAnObject(pin);
                    string xmlparamout = "";
                    int respuesta = ws.Identify(xmlparamin, out xmlparamout);

                    if (respuesta == Errors.IERR_OK ||
                        respuesta == Errors.IERR_BIR_NOT_FOUND)
                    {
                        Session["respuesta"] = respuesta;
                        Session["xmlparamout"] = xmlparamout;
                    }
                    else
                    {
                        throw new Exception("Error identificando identidad [" +
                                             respuesta.ToString() + ": " +
                                             Errors.GetDescription(respuesta) + "]");
                    }
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en Verify.aspx al momento de identificar", exe);
                Session["mensajeerror"] = "Se ha producido un error desconocido en Verify.aspx al momento de identificar [" + exe.Message + "]";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
            Response.Redirect("Result.aspx");
            //try
            //{
            //    String Data = hiddendata.Value;
            //    String Finger = hiddendedo.Value;
            //    XmlParamIn pin = new XmlParamIn();

            //    using (BioWs.BioPortalServerWSWeb ws = new BioWs.BioPortalServerWSWeb())
            //    {
            //        ws.Timeout = 99999;
            //        pin.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_IDENTIFY;
            //        pin.Authenticationfactor = Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;

            //        //Tomamos el algoritmo seleccionado.

            //        if (DropDownListAlgoritmo.SelectedIndex != 0)
            //        {
            //            pin.Minutiaetype = DropDownListAlgoritmo.SelectedIndex;  //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER;
            //        }


            //        pin.Clientid = Request.ServerVariables["REMOTE_HOST"].ToString();
            //        pin.InsertOption = 0;
            //        pin.OperationOrder = OperationOrder.OPERATIONORDER_DEFAULT;
            //        pin.Companyid = 7;//CompanyID;
            //        pin.Enduser = "BP_Manager";
            //        pin.Ipenduser = Request.ServerVariables["REMOTE_HOST"].ToString();
            //        pin.Matchingtype = 1;
            //        pin.Origin = 1;
            //        pin.SampleCollection = new List<Sample>();
            //        Sample sample = new Sample();
            //        sample.Data = Data;
            //        sample.Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_TOKEN;
            //        sample.Additionaldata = null;
            //        pin.SampleCollection.Add(sample);
            //        pin.SaveVerified = 1;
            //        pin.Threshold = 0.00001;
            //        pin.Userid = 0;
            //        pin.Verifybyconnectorid = "0";
            //        pin.InsertOption = 0;

            //        //Datos de la Persona
            //        pin.PersonalData = new PersonalData();
            //        pin.PersonalData.Companyidenroll = 7;// CompanyID;
            //        pin.PersonalData.Useridenroll = 0;



            //        //Serializamos.
            //        string xmlparamin = XmlUtils.SerializeAnObject(pin);
            //        string xmlparamout = "";
            //        int respuesta = ws.Identify(xmlparamin, out xmlparamout);
            //        Session["xmlparamout"] = xmlparamout;
            //        Response.Redirect("Message.aspx");

            //    }
            //}
            //catch (Exception exe)
            //{
            //    log.Error("Se ha producido un error en Verify.aspx al momento de identificar una identidad" , exe);
            //    Session["mensajeerror"] = "Se ha producido un error en Verify.aspx al momento de identificar una identidad";
            //    Session["exception"] = exe;
            //    Response.Redirect("~/Error.aspx");
            //}
        }

        protected void cmdInicializar_Click(object sender, EventArgs e)
        {

        }

        //protected void DropDownListAlgoritmo_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    switch (DropDownListAlgoritmo.SelectedIndex)
        //    {
        //        case 1: //NEC
        //            txtUmbral.Text = "750";
        //            break;
        //        case 2: //DP PLatinum  
        //        case 3: //DP Gold
        //        case 4: //DP OTW
        //            txtUmbral.Text = "0.0001";
        //            break;
        //        case 7: //VF
        //            txtUmbral.Text = "63";
        //            break;
        //        default:
        //            txtUmbral.Text = "";
        //            break;
        //    }
            
        //}

    }
}