﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="Result.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Verify.Result" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <table width="640" border="0" cellspacing="0" cellpadding="0">
		<tr>
            <td align="left">
                <asp:Button runat="server" ID="cmdVolver" value="Volver" SkinID="BotonAccion" BorderStyle="None"  
                Text="Volver" onclick="cmdVolver_Click" />
            </td>
        </tr>
        <tr height="5">
            <td align="left">
            </td>
        </tr>
    </table>
	    
    <table width="640" border="0" cellspacing="0" cellpadding="0">
		<tr>
		     <td class="tdCertifica1">
                    <p class="tituloCertifica">Resultado de la Operación </p>
				    <table width="92%" border="0" align="center" cellpadding="0" cellspacing="5">
				        <tr>
                            <td colspan="3" align="center" class="tdBottom">
                                <asp:Image runat="server" Visible="false" ID="imgOKQuestion" Width="41px" 
                                    Height="40px" ImageUrl="~/Images/Question-Shield.png"                             
                                     ImageAlign="AbsMiddle" />
                                &nbsp;<asp:Image runat="server" Visible="false" ID="imgOK" Width="41px" 
                                    Height="40px" ImageUrl="~/Images/Good-Shield.png"                             
                                     ImageAlign="AbsMiddle" />
                                &nbsp;<asp:Image runat="server" Visible="false" ID="imgNOOK" Width="41px" 
                                    Height="40px" ImageUrl="~/Images/Error-Shield.png" 
                                    ImageAlign="AbsMiddle" />
                                &nbsp;&nbsp; 
                                <asp:Label runat="server" ID="lblresult" 
                                    Text="Verificación exitosa" Font-Bold="True" Font-Size="Large"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <table width="100%"><tr><td>
				                <td align="center" width="120px">
                                    Foto<br />				                
			                        <asp:image runat="server" id="imagenFoto" ImagenUrl="#" width="110" 
                                        height="110" ImageAlign="AbsMiddle" BorderStyle="Solid" BorderColor="Gray" BorderWidth="1"  />                              
                                    <br />
                                    <br />
                                    Firma<br />
			                        <asp:image runat="server" id="imagenFirma" ImagenUrl="#" width="110px" BorderStyle="Solid" BorderColor="Gray" BorderWidth="1"
                                        height="60px"  />                              
                                </td>
				                <td>
                                
                                    <table align="center" style="width: 98%; height: 172px;">
                                        <tr>
                                            <td class="tdBottom" align="right" style="font-weight: bold; font-size: medium;" width="120">
                                                Identidad :</td>
                                            <td class="tdBottom" align="left">
                                                <asp:Label runat="server" ID="lblIdentidad" Text="RUT 21284415-2" 
                                                    Visible="True" Font-Bold="True" Font-Size="Medium" ForeColor="#006600"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  class="tdBottom" align="right" style="font-weight: bold" width="120">
                                                Nombre :</td>
                                            <td class="tdBottom" align="left">
    &nbsp;<asp:Label runat="server" ID="lblNombre" Text="Gustavo Gerardo Suhit Gallucci" Visible="True"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdBottom" align="right" style="font-weight: bold" width="120">
                                                Fecha Nacimiento :</td>
                                            <td class="tdBottom" align="left">
                                                <asp:Label runat="server" ID="lblFechaNacimiento" Text="28/09/1969" 
                                                    Visible="True"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdBottom" align="right" style="font-weight: bold" width="120">
                                                Profesión :</td>
                                            <td class="tdBottom" align="left">
                                                <asp:Label runat="server" ID="lblProfesion" Text="Ingeniero de Sistemas" 
                                                    Visible="True"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdBottom" align="right" style="font-weight: bold" width="120">
                                                Fuente :</td>
                                            <td class="tdBottom" align="left">
                                                <strong><font color="maroon"> 
                                                <asp:Label runat="server" ID="lblFuente" Text="VCI"></asp:Label>
                                                </font></strong></td>
                                        </tr>
                                        <tr>
                                            <td class="tdBottom" align="right" style="font-weight: bold" width="120">
                                                Operación :</td>
                                            <td class="tdBottom" align="left">
                                                <strong><font color="blue"> 
                                                <asp:Label runat="server" ID="lblOperacion" Text="Local"></asp:Label>
                                                </font></strong></td>
                                        </tr>
                                    </table>
                                
                                </td>
                                </td></tr></table>
                            </td>
			            </tr>
				        <tr>
				            <td class="tdTop" colspan="2" align="center">Certificado emitido por Biometrika BioPortal Manager Verify, el
                                <asp:Label runat="server" ID="lblTimestamp" Text="30/07/2010" Visible="True" 
                                    Font-Bold="True"></asp:Label>
                            &nbsp;con código 
                                <asp:Label runat="server" ID="lblTrackid" 
                                    Text="kjh3g3gk3g34g3g4k3g4" Visible="True" Font-Bold="True"></asp:Label>, a través de 
                                <asp:Label runat="server" ID="lbltipoverificacion" 
                                    Visible="True" Font-Bold="True"></asp:Label>, 
                                <br />
                                <asp:Label runat="server" ID="lblscore" Visible="True" 
                                    Font-Bold="True"></asp:Label>
                            </td>
			            </tr>
				        
		            </table>
             </td>
	    </tr>
		<tr>
		     <td>
                <img alt="" src="../../Images/fondoPScertificado_bottom.jpg" width="640" height="60" />
             </td>
        </tr>
 
	</table>
</asp:Content>
