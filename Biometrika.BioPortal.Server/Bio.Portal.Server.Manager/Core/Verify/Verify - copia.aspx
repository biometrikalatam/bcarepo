﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="Verify.aspx.cs" Inherits="Bio.Portal.Server.Manager.Core.Verify.Verify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td colspan="4" class="titulos0">        
           Verificar Identidad
           <asp:HiddenField runat="server" ID="hiddencompanyid" />           
           <asp:HiddenField runat="server" ID="hiddendata" Value="" />
           <asp:HiddenField runat="server" ID="hiddendedo" Value="" />
           <asp:HiddenField runat="server" ID="hiddenOperacion" Value="" />
        </td>
    </tr>
     <asp:PlaceHolder runat="server" ID="company">
                <tr>
                    <td class="tdBottom">Compañía</td>
                    <td class="tdBottom">:</td>
                    <td colspan="2" class="tdBottom">
                        <asp:DropDownList ID="DropDownListCompany" runat="server" 
                                         DataTextField="Name" DataValueField="Id" OnDataBound="DropDownListCompany_DataBound">
                                        <asp:ListItem Value="-1" Selected="True">Seleccione una empresa</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                </asp:PlaceHolder>              
    
    <tr>
        <td class="tdBottom">Tipo Id</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
           <asp:DropDownList ID="DropDownListTipoId" runat="server" 
                            ondatabound="DropDownListTipoId_DataBound">
           </asp:DropDownList>
        </td>
        <td class="tdBottom">
                        <asp:CheckBox runat="server" ID="chkrecuperar" Text="Recuperar Datos" />
                    </td>
    </tr>
    <tr>
        <td class="tdBottom">Valor Id</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtrut" MaxLength="50" onkeypress="javascript:return RestrictChar()"></asp:TextBox>
        </td>
        <td class="tdBottom">
                        <asp:CheckBox runat="server" ID="chkIgnoreDedo" Text="Ignorar Dedo en el Token" />
                    </td>
    </tr>
    <tr>
        <td class="tdBottom">Conector Externo</td>
        <td class="tdBottom">:</td>
        <td class="tdBottom">
            <asp:TextBox runat="server" ID="txtConnectorId" MaxLength="50" 
                 onkeypress="javascript:return RestrictChar()"></asp:TextBox>
        </td>
        <td class="tdBottom">&nbsp;</td>
    </tr>
    <tr>
        <td  class="tdBottom" colspan="3">
                            <script type="text/javascript" language="javascript">
                                document.write("<OBJECT ID=\"BPClientAx1\" WIDTH=\"291\" HEIGHT=\"307\""); // style=\"BORDER-RIGHT: gray groove; BORDER-TOP: gray groove; BORDER-LEFT: gray groove; BORDER-BOTTOM: gray groove\"");
                                if (WriteOldVersion() == true) {
                                    document.write(" CLASSID=\"CLSID:95199405-70A6-43CC-A012-7A7EB41A8A64\" CODEBASE=\"BPClientP.CAB#version=3,9,0,1\"");
                                } else {
                                    document.write(" CLASSID=\"CLSID:E176B55F-AF5B-44BE-8D9B-80DC98EB68E5\" CODEBASE=\"BPClient.CAB#version=4,1,0,3\"");
                                }
			                 </script>           
                         
                				<PARAM NAME="_ExtentX" VALUE="7699">
                				<PARAM NAME="_ExtentY" VALUE="8123">
                				<PARAM NAME="StepAnimation" VALUE="10">
                				<PARAM NAME="BackColor" VALUE="16777215">
                				<PARAM NAME="DEBUG" VALUE="False">
                				<PARAM NAME="PathToDEBUG" VALUE="D:\temp\a\Token\">
                				<PARAM NAME="TypeId" VALUE="RUT">
                				<PARAM NAME="ValueId" VALUE="21284415-2">
                				<PARAM NAME="Finger" VALUE="0">
                                <PARAM NAME="Timeout" VALUE="6000">
                                <PARAM NAME="OperationType" VALUE="1">
                				<PARAM NAME="MenuBackColor" VALUE="3632437">
                				<PARAM NAME="MainBackColor" VALUE="13565902">
                				<PARAM NAME="TopBackColor" VALUE="3632436">
                				<PARAM NAME="TopFontColor" VALUE="16777215">
		                </OBJECT>
        </td>
        <td valign="top" class="tdBottom">
            <table cellpadding="0" cellspacing="0">
                
                <tr>
                    <td class="tdBottom" colspan="2" align="right" valign="top">Contenido Token :</td>
                    <td class="tdBottom" colspan="2">
                        <asp:RadioButton ID="rbtwsq" GroupName="grptoken" Text="WSQ" runat="server" /><br />
                        <asp:RadioButton ID="rbtMinucias" GroupName="grptoken" Text="Minucias" runat="server" /><br />
                        <asp:RadioButton ID="rbtAmbos" GroupName="grptoken" Text="Ambos" runat="server" Checked="true" />  
                    </td>
                </tr>
 
                <tr>
                    <td colspan="2" class="tdBottom" align="right">
                        Factor :</td>
                    <td colspan="2" class="tdBottom">
                        <asp:DropDownList runat="server" ID="DropDownListAF">
                            <asp:ListItem Value="0">Seleccione Factor</asp:ListItem>
                            <asp:ListItem Value="1">Clave</asp:ListItem>                            
                            <asp:ListItem Value="2" Selected="True">Huella Digital</asp:ListItem>
                            <asp:ListItem Value="11">Certificado Digital</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
 
                <tr>
                    <td colspan="2" class="tdBottom" align="right">
                        Algoritmo
                        : </td>
                    <td colspan="2" class="tdBottom">
                        <asp:DropDownList runat="server" ID="DropDownListAlgoritmo" onchange="javascript:SetThreshold();">
                            <asp:ListItem Value="0">Seleccione Algoritmo</asp:ListItem>
                            <asp:ListItem Value="4">Digital Persona OTW</asp:ListItem>
                            <asp:ListItem Value="7">Verifinger</asp:ListItem>
                            <%--
                            <asp:ListItem Value="1">NEC</asp:ListItem>
                            <asp:ListItem Value="2" Selected="True">Digital Persona</asp:ListItem>
                            <asp:ListItem Value="3">Digital Persona Gold</asp:ListItem>
                            <asp:ListItem Value="5">Secugen</asp:ListItem>
                            <asp:ListItem Value="6">Testech</asp:ListItem>
                            <asp:ListItem Value="8">Sagem</asp:ListItem>
                            <asp:ListItem Value="9">Identix</asp:ListItem>
                            <asp:ListItem Value="10">Upek</asp:ListItem>
                            <asp:ListItem Value="11">Cogent</asp:ListItem>
                            <asp:ListItem Value="12">Crossmatch</asp:ListItem>                            --%>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"  class="tdBottom" align="right">
                        &nbsp;</td>
                    <td colspan="2"  class="tdBottom" align="left" valign="middle">
                        Umbral :<asp:TextBox runat="server" ID="txtUmbral" MaxLength="15" Text="10000" width="80" 
                            onkeypress="javascript:return RestrictChar()"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"  class="tdBottom" align="right">
                        Device :
                    </td>
                    <td colspan="2"  class="tdBottom">
                        <asp:DropDownList runat="server" ID="DropDownListDevice" onchange="javascript:SetModel();">
                            <asp:ListItem Value="0">Seleccione Dispositivo</asp:ListItem>
                            <asp:ListItem Value="1" Selected="True">Digital Persona</asp:ListItem>
                            <asp:ListItem Value="2">Secugen</asp:ListItem>
                            <asp:ListItem Value="7">Upek</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="tdBottom">Modelo :</td>
                    <td colspan="3" class="tdBottom"><asp:DropDownList runat="server" ID="DropDownListModel" onchange="javascript:CheckModel();">
                            <asp:ListItem Value="">Seleccione Modelo</asp:ListItem>
                            <asp:ListItem Value="4000" Selected="True">U.are.U 4000</asp:ListItem>
                            <asp:ListItem Value="4500">U.are.U 4500</asp:ListItem>
                            <asp:ListItem Value="FDU02">Hamster II / III</asp:ListItem>
                            <asp:ListItem Value="FDU03">Hamster Plus</asp:ListItem>
                            <asp:ListItem Value="FDU04">Hamster IV</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"  class="tdBottom" align="right">
                        Orden de Verificación :</td>
                    <td colspan="2"  class="tdBottom">
                        <asp:DropDownList runat="server" ID="DropDownListOperationOrder">
                            <asp:ListItem Value="0" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="1">Solo Local</asp:ListItem>
                            <asp:ListItem Value="2">Primero Local</asp:ListItem>
                            <asp:ListItem Value="3">Solo Remoto</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td><br />
                    <asp:Button runat="server" ID="cmdInicializar" Text="Inicializar" SkinID="BotonAccion" 
                                OnClientClick="return SetBpClientVerify();" />        
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="" colspan="4" align="center">
                        <asp:Button runat="server" ID="cmdVerificarBDLocal" 
                            Text="   Verificar   " 
                            onclick="cmdVerificarBDLocal_Click" OnClientClick="return VerifyForm('VBDL');" SkinID="BottonAccion3"   />
                    &nbsp;&nbsp;
                        <asp:Button runat="server" ID="cmdIdentificarBDLocal" 
                            Text="   Identificar   " SkinID="BotonCancelar" 
                            onclick="cmdIdentificarBDLocal_Click" Visible="True"
                OnClientClick="return VerifyForm('IBDL');" />
                         <asp:Button runat="server" ID="cmdVerificarSRCEI" 
                Text="Verificar Conector BP" SkinID="BottonAccion3" Visible="False" />
        </td>
    </tr>
</table>
</asp:Content>
