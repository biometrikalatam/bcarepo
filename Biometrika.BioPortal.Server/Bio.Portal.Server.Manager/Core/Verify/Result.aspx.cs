﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bio.Core.Constant;
using log4net;
using Bio.Portal.Server.Manager.Core.libs;
using Bio.Portal.Server.Common.Entities.Database;
using BioPortal.Server.Api;

namespace Bio.Portal.Server.Manager.Core.Verify
{
    public partial class Result :Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Result));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");

                //Chequea si tiene permisos para acceder a esta página
                if (BioPortalServer.GetRolId(User) > 5)
                {
                    throw new Exception("No tiene privilegios para ingresar en esta opción.");
                }

                //Dibuja Menu
                BioPortalServer.VisualizarMenu(Master, User);

                if (Session["xmlparamout"] != null)
                {
                    XmlParamOut parametros = Bio.Core.Utils.SerializeHelper.DeserializeFromXml<XmlParamOut>(Session["xmlparamout"].ToString());
                    int respuesta = (int) Session["respuesta"];

                    if (parametros == null) throw new Exception("Error analizando respuesta (parametros=null)");

                    if (respuesta == Errors.IERR_BIR_NOT_FOUND) //No existen datos biometricos pero identity existe
                    {
                        lblresult.Text = "No existen datos para comparar";
                        lblresult.ForeColor = Color.Blue;
                        imgOK.Visible = false;
                        imgNOOK.Visible = false;
                        imgOKQuestion.Visible = true;
                    } else //es 0 => comparo normal
                    {
                        if (parametros.Result == 1) //Verificacion Positiva
                        {
                            lblresult.Text = parametros.Actionid == Bio.Core.Matcher.Constant.Action.ACTION_IDENTIFY ? 
                                "Identificación Positiva" : "Verificación Positiva";
                            lblresult.ForeColor = Color.Green;
                            imgOK.Visible = true;
                            imgNOOK.Visible = false;
                            imgOKQuestion.Visible = false;
                        }
                        else
                        {
                            lblresult.Text = parametros.Actionid == Bio.Core.Matcher.Constant.Action.ACTION_IDENTIFY ?
                                "Identificación Negativa" : "Verificación Negativa";
                            lblresult.ForeColor = Color.Red;
                            imgOK.Visible = false;
                            imgNOOK.Visible = true;
                            imgOKQuestion.Visible = false;
                        }
                        
                    }

                    //Footer
                    lbltipoverificacion.Text = Bio.Core.Matcher.Constant.AuthenticationFactor.GetName(parametros.Authenticationfactor);
                    if (parametros.Authenticationfactor == Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_PKI ||
                        parametros.Authenticationfactor == Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
                    {
                        lblscore.Text = "";
                        lblscore.Visible = false;
                    } else
                    {
                        if (parametros.Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_VERIFINGER)
                        {
                            lblscore.Text = "Puntaje " + parametros.Score.ToString() +
                                            " sobre " + parametros.Threshold.ToString() + " [" +
                                            Bio.Core.Matcher.Constant.MinutiaeType.SMINUTIAETYPE_VERIFINGER + "]";                                
                        } else
                        {
                            lblscore.Text = "Verificación Positiva [" +
                                            Bio.Core.Matcher.Constant.MinutiaeType.GetDescription(parametros.Minutiaetype) + "]";  
                        }
                    }
                    lblTrackid.Text = parametros.Trackid;
                    lblTimestamp.Text = parametros.Timestampend.ToString("dd/MM/yyyy HH:mm:ss");

                    if (parametros.PersonalData != null)
                    {
                            //Muestro Foto
                        Global.Fotos[parametros.PersonalData.Id.ToString() + "_foto"] =
                            parametros.PersonalData.Photography;
                        imagenFoto.ImageUrl = "MostrarFoto.ashx?rut=" + parametros.PersonalData.Id.ToString() + "&type=foto";
                        Global.Fotos[parametros.PersonalData.Id.ToString() + "_firma"] = 
                            parametros.PersonalData.Signatureimage;
                            //Muestro Firma
                        imagenFirma.ImageUrl = "MostrarFoto.ashx?rut=" + parametros.PersonalData.Id.ToString() + "&type=firma";
                        lblNombre.Text = parametros.PersonalData.Name + " " +
                                     parametros.PersonalData.Patherlastname + " " +
                                     parametros.PersonalData.Motherlastname;
                        lblIdentidad.Text = parametros.PersonalData.Typeid + " " +
                                            parametros.PersonalData.Valueid;
                        lblProfesion.Text = String.IsNullOrEmpty(parametros.PersonalData.Profession) ?
                                            "Sin Información." : parametros.PersonalData.Profession;
                        lblFechaNacimiento.Text = parametros.PersonalData.Birthdate.ToString("dd/MM/yyyy");
                        lblFuente.Text = String.IsNullOrEmpty(parametros.Verificationsource) ?
                                        "Sin Información" : parametros.Verificationsource.Trim();
                        if (parametros.Resultconnector != null)
                        {
                            lblOperacion.Text = String.IsNullOrEmpty((string)parametros.Resultconnector.GetValue("verificationsource")) ?
                                "Fuente Externa" :
                                parametros.Resultconnector.GetValue("verificationsource").ToString();
                        } 
                    } else
                    {
                        imagenFoto.ImageUrl = "MostrarFoto.ashx?rut=null&type=foto";
                        //Muestro Firma
                        imagenFirma.ImageUrl = "MostrarFoto.ashx?rut=no&type=firma";
                        lblNombre.Text = "Sin Información";
                        lblIdentidad.Text = "Sin Información";
                         lblProfesion.Text = "Sin Información";
                         lblFechaNacimiento.Text = "Sin Información";
                    }
                }
            }
            catch (Exception exe)
            {
                log.Error("Se ha producido un error en Result.aspx" , exe);
                Session["mensajeerror"] = "Se ha producido un error en Result.aspx";
                Session["exception"] = exe;
                Response.Redirect("~/Error.aspx");
            }
        }

        protected void cmdVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Core/Verify/Verify.aspx", true);

        }
    }
}