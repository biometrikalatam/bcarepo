﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using Bio.Portal.Server.Common;
using Bio.Portal.Server.Common.Common;
using Bio.Portal.Server.Common.Entities;
using Bio.Portal.Server.Common.Entities.Database;
using log4net;
using Bio.Core.Constant;

namespace Bio.Portal.Server.Manager
{
    /// <summary>
    /// Summary description for BioPortal.Manager.Services
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class BioPortalManagerServices : System.Web.Services.WebService {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(BioPortalManagerServices));
        /// <summary>
        /// Método que permite dejar en memoria todas las compañías que tiene la plataforma, independiente si están activos o no.
        /// </summary>
        /// <param name="xmlparamout">parámetro de salida, que trae el resultado de las compañías resultantes.</param>
        /// <returns></returns>
        [WebMethod]        
        public int GetCompanys(out string xmlparamout) {

            String msgErr="";
            Companys arrcompanias = null;
            int res = 0;
            xmlparamout = "";
            
            try
            {
                
                IList<BpCompany> companias = AdminBpCompany.GetCompanys(0, out msgErr);
                arrcompanias = new Companys();
                foreach (BpCompany company in companias)
                {
                    if(company.Laststatusdate!=null)
                        arrcompanias.AddItem(new Company(company.Id, company.Rut, company.Status, (DateTime)company.Laststatusdate));
                    else
                        arrcompanias.AddItem(new Company(company.Id, company.Rut, company.Status, DateTime.MinValue));
                }
               
                //Serializamos el arreglo de compañías
                xmlparamout = Bio.Core.Utils.SerializeHelper.SerializeToXml(arrcompanias);
                res = 1;
            }
            catch (Exception ex)
            {
                msgErr = ex.Message;
                LOG.Error("BioPortalServerWS.Verify", ex);
                res = Errors.IERR_UNKNOWN;
            }
            return res;
        }

      
    }
}
