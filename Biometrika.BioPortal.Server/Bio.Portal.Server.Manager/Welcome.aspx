﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BioPortalServer.Master" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="Bio.Portal.Server.Manager.Welcome" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<table width="95%" border="0" align="right" cellpadding="0" cellspacing="0">
				 
				  <tr>
				    <td height="440" valign="top" class="Estilo1"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="5">
				      <tr>
				        <td colspan="2" class="tdBottom">El  objetivo de <img src="Images/PS_ic_azl1.jpg" width="7" height="7" /> <strong>BioPortal  Server </strong> <img src="Images/PS_ic_azl1.jpg" width="7" height="7" /> es generar un gateway de conexi&oacute;n a los servicios  biom&eacute;tricos disponibles en una empresa, sumando la opcionalidad de conectarse a  los servicios ofrecidos&nbsp; por el SRCeI u  otra base de datos externa, agregando la capacidad de logging de transacciones,  para sus posteriores chequeos y tarificaci&oacute;n, de ser necesarios. </td>
			          </tr>
				      <tr>
				        <td width="30%" class="tdBottDerecha"><img src="Images/PS_mn1Adm_g.jpg" name="img_Administrac" width="140" height="15" id="img_Administrac2" /></td>
				        <td class="tdBottom"><span class=""><img src="Images/PS_ic1Adm0.jpg" width="65" height="65" align="right" /></span>Permite modificar configuraciones, Accesos, Logging.</td>
			          </tr>
				      <tr>
				        <td class="tdBottDerecha"><img src="Images/PS_mn2Enr_g.jpg" name="img_Enrolamiento" width="140" height="15" id="img_Enrolamiento2" /></td>
				        <td class="tdBottom"><img src="Images/PS_ic2Enrol0.jpg" width="65" height="65" align="right" />Enrolamiento, permite Buscar y Enrolar.</td>
			          </tr>
				      <tr>
				        <td class="tdBottDerecha"><img src="Images/PS_mn3Ver_g.jpg" width="140" height="15" /></td>
				        <td class="tdBottom"><img src="Images/PS_ic3Verif.jpg" width="65" height="65" align="right" />Verificaci&oacute;n.</td>
			          </tr>
				      <tr>
				        <td class="tdBottDerecha"><img src="Images/PS_mn4Rep_g.jpg" width="140" height="15" /></td>
				        <td class="tdBottom"><img src="Images/PS_ic4rep0.jpg" width="65" height="65" align="right" />Transacciones</td>
			          </tr>
			        </table></td>
			      </tr>
				  <tr>
					<td ></td>
				  </tr>
				</table>
</asp:Content>
