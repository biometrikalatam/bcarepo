﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Bio.Portal.Server.Manager.Core.libs;
using log4net;

namespace Bio.Portal.Server.Manager
{
    public partial class SessionExpired : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SessionExpired));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Inicializamos el soporte de javascript del sitio.
                HtmlGenericControl myJs = new HtmlGenericControl();
                myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
                myJs.Attributes.Add("language", "javascript");
                //don't need it usually but for cross browser.
                myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
                this.Page.Header.Controls.Add(myJs);
                if (!(Page.IsPostBack))
                {
                    Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
                    if (Session["mensajeerror"] != null)
                    {
                        mensaje.Text = Session["mensajeerror"].ToString();
                        txtexception.Text = Session["exception"].ToString();
                    }
                }
            }
            catch (Exception exe)
            {

            }
        }
    }
}