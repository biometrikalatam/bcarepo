﻿
var isNav4 = false, isNav5 = false, isIE4 = false
var strSeperator = "/"; 
// If you are using any Java validation on the back side you will want to use the / because 
// Java date validations do not recognize the dash as a valid date separator.
var vDateType = 3; // Global value for type of date format
//                1 = mm/dd/yyyy
//                2 = yyyy/dd/mm  (Unable to do date check at this time)
//                3 = dd/mm/yyyy
var vYearType = 4; //Set to 2 or 4 for number of digits in the year for Netscape
var vYearLength = 2; // Set to 4 if you want to force the user to enter 4 digits for the year before validating.
var err = 0; // Set the error code to a default of zero

if(navigator.appName == "Netscape") {
if (navigator.appVersion < "5") {
isNav4 = true;
isNav5 = false;
}
else
if (navigator.appVersion > "4") {
isNav4 = false;
isNav5 = true;
   }
}
else {
    
isIE4 = true;
}
function SistemaOperativo() {
	if(navigator.userAgent.indexOf('IRIX') != -1) {var SO = "Irix" }
	  else if ((navigator.userAgent.indexOf('Win') != -1) && (navigator.userAgent.indexOf('98') != -1)) {var SO= "Windows 98"}
	  else if ((navigator.userAgent.indexOf('Win') != -1) && (navigator.userAgent.indexOf('95') != -1)) {var SO= "Windows 95"}
	  else if (navigator.appVersion.indexOf("16") !=-1) {var SO= "Windows 3.1"}
	  else if (navigator.userAgent.indexOf("NT 5.1") !=-1) {var SO= "Windows XP"} 
	  else if (navigator.userAgent.indexOf("NT 5.2") !=-1) {var SO= "Windows Server 2003"}
	  else if (navigator.userAgent.indexOf("NT 5") !=-1) {var SO= "Windows 2000"} 
	  else if (navigator.userAgent.indexOf("NT 6") !=-1) {var SO= "Windows Vista"}
	  else if (navigator.appVersion.indexOf("NT") !=-1) {var SO= "Windows NT"}  
	  else if (navigator.appVersion.indexOf("SunOS") !=-1) {var SO= "SunOS"}
	  else if (navigator.appVersion.indexOf("Linux") !=-1) {var SO= "Linux"}  
	  else if (navigator.userAgent.indexOf('Mac') != -1) {var SO= "Macintosh"}
	  else if (navigator.appName=="WebTV Internet Terminal") {var SO="WebTV"}
	  else if (navigator.appVersion.indexOf("HP") !=-1) {var SO="HP-UX"} 
	  else {var SO= "No identificado"}
	  return SO;
	 }

	 function WriteOldVersion() {
	     return false; //Para que use nueva version de DP
//	 	var SO= SistemaOperativo();
//		
//		if ((SO == "Windows XP" || SO == "Windows Server 2003" || SO == "Windows 2000") && window.navigator.cpuClass == "x86" )
//		{
//			return true;
//		} else {
//			return false;
//		}
	 }


function onlyNumbers(evt) {
    var e = evt
    
    if (window.event) { // IE
        var charCode = e.keyCode;
    } else if (e.which) { // Safari 4, Firefox 3.0.4
        var charCode = e.which
    }
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    
    return true;
}

function onlyNumbers() {
//    //get the keycode when the user pressed any key in application 
//    var charCode = String.fromCharCode(window.event.keyCode)

//    if (charCode > 31 && (charCode < 48 || charCode > 57))
//        return false;

    //    return true;
    //get the keycode when the user pressed any key in application 
    var exp = String.fromCharCode(window.event.keyCode)

    //Below line will have the special characters that is not allowed you can add if you want more for some characters you need to add escape sequence 
    var r = new RegExp("[0-9]", "g");

    if (exp.match(r) == null) {
        return false;
    }
    return true;
}


function RestrictChar() 
{ 
    //get the keycode when the user pressed any key in application 
    var exp = String.fromCharCode(window.event.keyCode)

    //Below line will have the special characters that is not allowed you can add if you want more for some characters you need to add escape sequence 
    var r = new RegExp("[-_,/'().0-9a-zA-Z \r]", "g"); 

    if (exp.match(r) == null) 
    {
        return false;
    }
    return true;
}

function RestrictChar(regexp) {
    //get the keycode when the user pressed any key in application 
    var exp = String.fromCharCode(window.event.keyCode)

    //Below line will have the special characters that is not allowed you can add if you want more for some characters you need to add escape sequence 
    var r = new RegExp(regexp, "g");

    if (exp.match(r) == null) {
        return false;
    }
    return true;
}


function ValidarFormularioCompany() {

    var paso = 0;
    
    if (document.getElementById(GetClientId('txtRut')).value != "")
        paso=1;
    if (document.getElementById(GetClientId('txtRazonSocial')).value != "")
        paso = 1;
    var IndexValue = document.getElementById(GetClientId('DropDownListEstado')).selectedIndex;
    var SelectedVal = document.getElementById(GetClientId('DropDownListEstado')).options[IndexValue].value;
    if (SelectedVal!=-1)
        paso = 1;
    if (paso == 0) {
        alert('Debe ingresar al menos un criterio de búsqueda');
        return false;
    }
    else {
//        alert("txtRut=" + document.getElementById(GetClientId('txtRut')).value);
//        alert("txtRazonSocial="+document.getElementById(GetClientId('txtRazonSocial')).value);
//        alert("IndexValue=" + document.getElementById(GetClientId('DropDownListEstado')).selectedIndex);
//        alert("SelectedVal=" + document.getElementById(GetClientId('DropDownListEstado')).options[IndexValue].value);
        return true;
    }

}
function ValidarFormularioCompanyIngreso() {
    
    var paso = 0;
    var msg = "Los siguientes campos son obligatorios:";
    //Validamos si el rut viene con datos.
   
    if (ValidarVacios(document.getElementById(MyClientID[0]).value) == true) {
        msg = msg + "\n Debe Ingresar Rut";
        paso = 1;
    }
  
    //Validamos si la viene la razón social
    
    if (ValidarVacios(document.getElementById(MyClientID[1]).value) == true) {
        msg = msg + "\n Debe Ingresar la Razón Social";
        paso = 1;
    }
   
    //Validamos si la viene la dirección
    
    if (ValidarVacios(document.getElementById(MyClientID[2]).value) == true) {
        msg = msg + "\n Debe Ingresar la Dirección";
        paso = 1;
    }
    
    //Validamos si la viene el telefono
   
    if (ValidarVacios(document.getElementById(MyClientID[3]).value) == true) {
        msg = msg + "\n Debe Ingresar un Teléfono";
        paso = 1;
    }
   
    //Validamos si la viene el domicilio

    if (ValidarVacios(document.getElementById(MyClientID[4]).value) == true) {
        msg = msg + "\n Debe Ingresar el Dominio";
        paso = 1;
    }
   
    if (paso == 1) {
        alert(msg);
        return false;
    }
    else {
        return true;
    }
}
function trim(s) {
    var l = 0; var r = s.length - 1;
    while (l < s.length && s[l] == ' ')
    { l++; }
    while (r > l && s[r] == ' ')
    { r -= 1; }
    return s.substring(l, r + 1);
}

function ValidarVacios(idcontrol)
{
    var texto = trim(idcontrol);   
    if (texto.length != 0)
        return false;
    else
        return true;
}
function ValidarEliminar(modulo) {

    var res=confirm("¿Esta seguro que quiere eliminar " + modulo  + "?");
    if (res == true)
        return true;
    else
        return false;
}
function FiltrarBusquedaUsuarios() {

    filtrousuario = document.getElementById(MyClientID[2]).value;
   
    var paso=0;
    var msg="Debe ingresar al menos un criterio de búsqueda";
    if (filtrousuario == 'si') {
        if (ValidarVacios(document.getElementById(MyClientID[0]).value) == true) {
            paso = 1;
        }
        else {
            paso = 0;
        }

        if (paso == 1) {
            var IndexValue = document.getElementById(MyClientID[1]).selectedIndex;
            var SelectedVal = document.getElementById(MyClientID[1]).options[IndexValue].value;
            if (SelectedVal == -1)
                paso = 1;
            else
                paso = 0;
        }
    }
    else {
        if (ValidarVacios(MyClientID[0]) == true) {
            paso = 1;
        }
    }
    if (paso == 1) {
        alert(msg);
        return false;
    }
    else {
        return true;
    }
}
/*
Validaciones de Formulario de Configuración
*/
function ValidarFormularioConfiguracion() {

    var paso = 0;
    var msg = "Los siguientes campos son obligatorios:";
    //Validamos si el rut viene con datos.

    if (ValidarVacios(document.getElementById(MyClientID[0]).value) == true) {
        msg = msg + "\n Debe Ingresar InsertOption";
        paso = 1;
    } 
//    else {
//            //Si no es 1 o 2, 
//        if (document.getElementById(MyClientID[0]).value != 1 &&
//            document.getElementById(MyClientID[0]).value != 2) {
//            document.getElementById(MyClientID[0]).value = 1; //Fuerzo a grabar
//        }
//    }
    if (ValidarVacios(document.getElementById(MyClientID[1]).value) == true) {
        msg = msg + "\n Debe Ingresar SaveVerified";
        paso = 1;
    }
    if (ValidarVacios(document.getElementById(MyClientID[2]).value) == true) {
        msg = msg + "\n Debe Ingresar MinutiaetypeDefault";
        paso = 1;
    }
    if (ValidarVacios(document.getElementById(MyClientID[3]).value) == true) {
        msg = msg + "\n Debe Ingresar TimeoutWSdefault";
        paso = 1;
    }
    if (ValidarVacios(document.getElementById(MyClientID[4]).value) == true) {
        msg = msg + "\n Debe Ingresar Matchingtype";
        paso = 1;
    }
    if (ValidarVacios(document.getElementById(MyClientID[5]).value) == true) {
        msg = msg + "\n Debe Ingresar OperationOrder";
        paso = 1;
    }
    if (ValidarVacios(document.getElementById(MyClientID[5]).value) == true) {
        msg = msg + "\n Debe Ingresar OperationOrder";
        paso = 1;
    }
    if (ValidarVacios(document.getElementById(MyClientID[6]).value) == true) {
        msg = msg + "\n Debe Ingresar CheckAccess";
        paso = 1;
    }

    if (paso == 1) {
        alert(msg);
        return false;
    }
    else {
        return true;
    }
}
function DateFormat(vDateName, vDateValue, e, dateCheck, dateType) {
    vDateType = dateType;
    
    // vDateName = object name
    // vDateValue = value in the field being checked
    // e = event
    // dateCheck 
    // True  = Verify that the vDateValue is a valid date
    // False = Format values being entered into vDateValue only
    // vDateType
    // 1 = mm/dd/yyyy
    // 2 = yyyy/mm/dd
    // 3 = dd/mm/yyyy
    //Enter a tilde sign for the first number and you can check the variable information.
    if (vDateValue == "~") 
    {
        alert("AppVersion = "+navigator.appVersion+" \nNav. 4 Version = "+isNav4+" \nNav. 5 Version = "+isNav5+" \nIE Version = "+isIE4+" \nYear Type = "+vYearType+" \nDate Type = "+vDateType+" \nSeparator = "+strSeperator);
        vDateName.value = "";
        vDateName.focus();
        return true;
    }
    var whichCode=0;
    if (window.event) { // IE
        var whichCode = e.keyCode;
    } else if (e.which) { // Safari 4, Firefox 3.0.4
        var whichCode = e.which
    }
     
    // Check to see if a seperator is already present.
    // bypass the date if a seperator is present and the length greater than 8

    if (vDateValue.length > 8 && isNav4) 
    {
        if ((vDateValue.indexOf("-") >= 1) || (vDateValue.indexOf("/") >= 1))
            return true;
    }

    //Eliminate all the ASCII codes that are not valid
    var alphaCheck = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-";
    if (alphaCheck.indexOf(vDateValue) >= 1) 
    {
        //No entra acá.
        if (isNav4) {
        vDateName.value = "";
        vDateName.focus();
        vDateName.select();
        return false;
        }
        else 
        {
            vDateName.value = vDateName.value.substr(0, (vDateValue.length-1));
            return false;
        }
    }
    
    if (whichCode == 8) //Ignore the Netscape value for backspace. IE has no value
        return false;
    else 
    {
        //Create numeric string values for 0123456789/
        //The codes provided include both keyboard and keypad values
    
        var strCheck = '47,48,49,50,51,52,53,54,55,56,57,58,59,95,96,97,98,99,100,101,102,103,104,105';
        
        if (strCheck.indexOf(whichCode) != -1) {
    
            if (isNav4) 
            {
    
                if (((vDateValue.length < 6 && dateCheck) || (vDateValue.length == 7 && dateCheck)) && (vDateValue.length >=1)) {
                    alert("Fecha inválida\nReingrese");
                    vDateName.value = "";
                    vDateName.focus();
                    vDateName.select();
                    return false;
                }

                if (vDateValue.length == 6 && dateCheck) 
                {
                    var mDay = vDateName.value.substr(2,2);
                    var mMonth = vDateName.value.substr(0,2);
                    var mYear = vDateName.value.substr(4,4);
                    //Turn a two digit year into a 4 digit year
                    if (mYear.length == 2 && vYearType == 4)
                    {
                        var mToday = new Date();
                        //If the year is greater than 30 years from now use 19, otherwise use 20
                        var checkYear = mToday.getFullYear() + 30; 
                        var mCheckYear = '20' + mYear;
                        if (mCheckYear >= checkYear)
                            mYear = '19' + mYear;
                        else
                            mYear = '20' + mYear;
                    }
                    var vDateValueCheck = mMonth + strSeperator + mDay + strSeperator + mYear;
                    
                    if (!dateValid(vDateValueCheck)) 
                    {
                            alert("Fecha inválida\nReingrese");
                            vDateName.value = "";
                            vDateName.focus();
                            vDateName.select();
                            return false;
                    }
                    return true;    
                }
                else 
                {
                // Reformat the date for validation and set date type to a 1
                    if (vDateValue.length >= 8 && dateCheck) 
                    {
                        if (vDateType == 1) // mmddyyyy
                        {
                            var mDay = vDateName.value.substr(2,2);
                            var mMonth = vDateName.value.substr(0,2);
                            var mYear = vDateName.value.substr(4,4)
                            vDateName.value = mMonth+strSeperator+mDay+strSeperator+mYear;
                        }
                        if (vDateType == 2) // yyyymmdd
                        {
                            var mYear = vDateName.value.substr(0,4)
                            var mMonth = vDateName.value.substr(4,2);
                            var mDay = vDateName.value.substr(6,2);
                            vDateName.value = mYear+strSeperator+mMonth+strSeperator+mDay;
                        }
                        if (vDateType == 3) // ddmmyyyy
                        {
                            
                            var mMonth = vDateName.value.substr(2,2);
                            var mDay = vDateName.value.substr(0,2);
                            var mYear = vDateName.value.substr(4,4)
                            vDateName.value = mDay+strSeperator+mMonth+strSeperator+mYear;
                        }
                        //Create a temporary variable for storing the DateType and change
                        //the DateType to a 1 for validation.
                        var vDateTypeTemp = vDateType;
                        vDateType = 1;
                        var vDateValueCheck = mMonth+strSeperator+mDay+strSeperator+mYear;
                        if (!dateValid(vDateValueCheck)) 
                        {
                            alert("Fecha inválida\nReingrese");
                            vDateType = vDateTypeTemp;
                            vDateName.value = "";
                            vDateName.focus();
                            vDateName.select();
                            return false;
                        }
                        vDateType = vDateTypeTemp;
                        return true;
                    }
                    else 
                    {
                        
                        if (((vDateValue.length < 8 && dateCheck) || (vDateValue.length == 9 && dateCheck)) && (vDateValue.length >= 1)) 
                        {
                            alert("Fecha inválida\nReingrese");
                            vDateName.value = "";
                            vDateName.focus();
                            vDateName.select();
                            return false;
                         }
                    }
                }
            }
    else {
    // Non isNav Check

if (((vDateValue.length < 8 && dateCheck) || (vDateValue.length == 9 && dateCheck)) && (vDateValue.length >=1)) {
    alert("Fecha inválida\nReingrese");
vDateName.value = "";
vDateName.focus();
return true;
}
// Reformat date to format that can be validated. mm/dd/yyyy

if (vDateValue.length >= 8 && dateCheck) {
// Additional date formats can be entered here and parsed out to
// a valid date format that the validation routine will recognize.
if (vDateType == 1) // mm/dd/yyyy
{
var mMonth = vDateName.value.substr(0,2);
var mDay = vDateName.value.substr(3,2);
var mYear = vDateName.value.substr(6,4)
}
if (vDateType == 2) // yyyy/mm/dd
{
var mYear = vDateName.value.substr(0,4)
var mMonth = vDateName.value.substr(5,2);
var mDay = vDateName.value.substr(8,2);
}
if (vDateType == 3) // dd/mm/yyyy
{
    
var mDay = vDateName.value.substr(0,2);
var mMonth = vDateName.value.substr(3,2);
var mYear = vDateName.value.substr(6,4)
}
if (vYearLength == 4) {
if (mYear.length < 4) {
    alert("Fecha inválida\nReingrese");
vDateName.value = "";
vDateName.focus();
return true;
   }
}
// Create temp. variable for storing the current vDateType
var vDateTypeTemp = vDateType;
// Change vDateType to a 1 for standard date format for validation
// Type will be changed back when validation is completed.
vDateType = 1;
// Store reformatted date to new variable for validation.
var vDateValueCheck = mMonth+strSeperator+mDay+strSeperator+mYear;
if (mYear.length == 2 && vYearType == 4 && dateCheck) {
//Turn a two digit year into a 4 digit year
var mToday = new Date();
//If the year is greater than 30 years from now use 19, otherwise use 20
var checkYear = mToday.getFullYear() + 30; 
var mCheckYear = '20' + mYear;
if (mCheckYear >= checkYear)
mYear = '19' + mYear;
else
mYear = '20' + mYear;
vDateValueCheck = mMonth+strSeperator+mDay+strSeperator+mYear;
// Store the new value back to the field.  This function will
// not work with date type of 2 since the year is entered first.
if (vDateTypeTemp == 1) // mm/dd/yyyy
vDateName.value = mMonth+strSeperator+mDay+strSeperator+mYear;
if (vDateTypeTemp == 3) // dd/mm/yyyy
vDateName.value = mDay+strSeperator+mMonth+strSeperator+mYear;
} 
if (!dateValid(vDateValueCheck)) {
    alert("Fecha inválida\nReingrese");
vDateType = vDateTypeTemp;
vDateName.value = "";
vDateName.focus();
return true;
}
vDateType = vDateTypeTemp;
return true;
}
else {
if (vDateType == 1) {
if (vDateValue.length == 2) {
vDateName.value = vDateValue+strSeperator;
}
if (vDateValue.length == 5) {
vDateName.value = vDateValue+strSeperator;
   }
}
if (vDateType == 2) {
if (vDateValue.length == 4) {
vDateName.value = vDateValue+strSeperator;
}
if (vDateValue.length == 7) {
vDateName.value = vDateValue+strSeperator;
   }
} 
if (vDateType == 3) {
if (vDateValue.length == 2) {
vDateName.value = vDateValue+strSeperator;
}
if (vDateValue.length == 5) {
vDateName.value = vDateValue+strSeperator;
   }
}
return true;
   }
}
if (vDateValue.length == 10&& dateCheck) {
if (!dateValid(vDateName)) {
// Un-comment the next line of code for debugging the dateValid() function error messages
//alert(err);  
    alert("Fecha inválida\nReingrese");
vDateName.focus();
vDateName.select();
   }
}
return false;
}
else {
// If the value is not in the string return the string minus the last
// key entered.
if (isNav4) {
vDateName.value = "";
vDateName.focus();
vDateName.select();
return false;
}
else
{

vDateName.value = vDateName.value.substr(0, (vDateValue.length-1));
return false;
         }
      }
   }
}
function dateValid(objName) {
var strDate;
var strDateArray;
var strDay;
var strMonth;
var strYear;
var intday;
var intMonth;
var intYear;
var booFound = false;
var datefield = objName;
var strSeparatorArray = new Array("-"," ","/",".");
var intElementNr;
// var err = 0;
var strMonthArray = new Array(12);
strMonthArray[0] = "Jan";
strMonthArray[1] = "Feb";
strMonthArray[2] = "Mar";
strMonthArray[3] = "Apr";
strMonthArray[4] = "May";
strMonthArray[5] = "Jun";
strMonthArray[6] = "Jul";
strMonthArray[7] = "Aug";
strMonthArray[8] = "Sep";
strMonthArray[9] = "Oct";
strMonthArray[10] = "Nov";
strMonthArray[11] = "Dec";
//strDate = datefield.value;
strDate = objName;
if (strDate.length < 1) {
return true;
}
for (intElementNr = 0; intElementNr < strSeparatorArray.length; intElementNr++) {
if (strDate.indexOf(strSeparatorArray[intElementNr]) != -1) {
strDateArray = strDate.split(strSeparatorArray[intElementNr]);
if (strDateArray.length != 3) {
err = 1;
return false;
}
else {
strDay = strDateArray[0];
strMonth = strDateArray[1];
strYear = strDateArray[2];
}
booFound = true;
   }
}
if (booFound == false) {
if (strDate.length>5) {
strDay = strDate.substr(0, 2);
strMonth = strDate.substr(2, 2);
strYear = strDate.substr(4);
   }
}
//Adjustment for short years entered
if (strYear.length == 2) {
strYear = '20' + strYear;
}
strTemp = strDay;
strDay = strMonth;
strMonth = strTemp;
intday = parseInt(strDay, 10);
if (isNaN(intday)) {
err = 2;
return false;
}
intMonth = parseInt(strMonth, 10);
if (isNaN(intMonth)) {
for (i = 0;i<12;i++) {
if (strMonth.toUpperCase() == strMonthArray[i].toUpperCase()) {
intMonth = i+1;
strMonth = strMonthArray[i];
i = 12;
   }
}
if (isNaN(intMonth)) {
err = 3;
return false;
   }
}
intYear = parseInt(strYear, 10);
if (isNaN(intYear)) {
err = 4;
return false;
}
if (intMonth>12 || intMonth<1) {
err = 5;
return false;
}
if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intday > 31 || intday < 1)) {
err = 6;
return false;
}
if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intday > 30 || intday < 1)) {
err = 7;
return false;
}
if (intMonth == 2) {
if (intday < 1) {
err = 8;
return false;
}
if (LeapYear(intYear) == true) {
if (intday > 29) {
err = 9;
return false;
   }
}
else {
if (intday > 28) {
err = 10;
return false;
      }
   }
}
return true;
}
function LeapYear(intYear) {
if (intYear % 100 == 0) {
if (intYear % 400 == 0) { return true; }
}
else {
if ((intYear % 4) == 0) { return true; }
}
return false;
}
function ValidarFecha(objeto,texto) {

    if (texto.length > 10) {
        alert('El formato de fecha es incorrecto');
        objeto.focus();
        return false;
    }
    else {
        if (texto.length < 10)
            return false;
        if (isValidDate(texto) != true) {
            alert('El formato de fecha no es correcto.');
            objeto.focus();
            return false;
        }
        else {
            return true;
        }
    }
}
function isValidDate(subject) {
    if (subject.match(/^(?:(0[1-9]|1[012])[\- \/.](0[1-9]|[12][0-9]|3[01])[\- \/.](19|20)[0-9]{2})$/)) {
        return true;
    }
    else {
        return false;
    }
}  
function ValidarFechas(fechad,fechah,hiddend,hiddenh)
{
    var SDate = fechad.value;        
    var EDate =  fechah.value;
    //Verificamos que no existan
    
    if ((ValidarVacios(SDate)==false) && (ValidarVacios(SDate)==false)) 
    {
        var firstIndex = SDate.indexOf("/");
        var secondIndex = SDate.lastIndexOf("/");
        var d1 = SDate.substring(0,firstIndex);
        var m1 = SDate.substring(firstIndex+1,secondIndex);
        var y1 = SDate.substring(secondIndex+1,SDate.length);
        var SDateFull = m1 + "/" + d1 + "/" + y1;
        hiddend.value = y1 + "/" + m1 + "/" + d1;;

        var d2 = EDate.substring(0,firstIndex);
        var m2 = EDate.substring(firstIndex+1,secondIndex);
        var y2 = EDate.substring(secondIndex+1,EDate.length);
        var EDateFull = m2 + "/" + d2 + "/" + y2;
        hiddenh.value = y2 + "/" + m2 + "/" + d2;

        var startDate = new Date(SDateFull);
        var endDate = new Date(EDateFull); 
        if(SDate != '' && EDate != '' && startDate > endDate)
        {
            alert('La fecha desde no puede ser mayor que la fecha hasta');
            return false;
        }
    }
    //Si tenemos fecha desde solamente o fecha hasta
    if(ValidarVacios(SDate)==false)
    {
        var firstIndex = SDate.indexOf("/");
        var secondIndex = SDate.lastIndexOf("/");
        var d1 = SDate.substring(0,firstIndex);
        var m1 = SDate.substring(firstIndex+1,secondIndex);
        var y1 = SDate.substring(secondIndex+1,SDate.length);
        var SDateFull=y1 + "/" + m1 + "/" +d1;
        hiddend.value = SDateFull;
    }
    if (ValidarVacios(EDate) == false) {

        var firstIndex = EDate.indexOf("/");
        var secondIndex = EDate.lastIndexOf("/");
        var d2 = EDate.substring(0, firstIndex);
        var m2 = EDate.substring(firstIndex + 1, secondIndex);
        var y2 = EDate.substring(secondIndex + 1, EDate.length);
        var EDateFull = y2 + "/" + m2 + "/" + d2;
        hiddenh.value = EDateFull;
    }
    return true;  

    
}
function ValidarFormularioOrigenIngreso() {

    var paso = 0;
    var msg = "Los siguientes campos son obligatorios:";
    //Validamos si el rut viene con datos.

    if (ValidarVacios(document.getElementById(MyClientID[0]).value) == true) {
        msg = msg + "\n Debe Ingresar Id";
        paso = 1;
    }

    //Validamos si la viene la razón social

    if (ValidarVacios(document.getElementById(MyClientID[1]).value) == true) {
        msg = msg + "\n Debe Ingresar una descripción";
        paso = 1;
    }

   

    if (paso == 1) {
        alert(msg);
        return false;
    }
    else {
        return true;
    }
}
function ModificarCompany(id) {
    document.getElementById(MyClientID[3]).value = id;
    return true;
}
function CambiarPassword(id, user) {
    document.getElementById(MyClientID[3]).value = id;
    document.getElementById(MyClientID[4]).value = user;

    return true;
}
function CambiarRoles(id, user) {
    document.getElementById(MyClientID[3]).value = id;
    document.getElementById(MyClientID[4]).value = user;

    return true;
}
function ValidarFormularioPassword(password) {
    var paso = 0;
    var msg = "Los siguientes campos son obligatorios:";
    //Validamos si el rut viene con datos.
    var pwd = document.getElementById(MyClientID[0]).value;

    if (ValidarVacios(document.getElementById(MyClientID[0]).value) == true) {
        msg = msg + "\n Debe Ingresar una password";
        paso = 1;
    }  
    else if (pwd.length < 6) {
        msg = msg + "\n La password no puede tener 6 caracteres";
        paso = 1;
    }
    if (paso == 1) {
        alert(msg);
        return false;
    }
    else {
        return true;
    }

}
function ModificarOrigen(id) {
    
    if ((id > 1) && id < 50) {
        alert('No se pueden modificar este origen. Es reservado para tareas de administración');
        return false;
    }
    else {
        //alert(document.getElementById(MyClientID[1]).value);
        document.getElementById(MyClientID[1]).value = id;
        //alert(document.getElementById(MyClientID[1]).value);
        return true;
     
    }
}
function EliminarCompany(id) {
    document.getElementById(MyClientID[3]).value = id;
    return true;
}
function ModificarUsuarios(id) {
    document.getElementById(MyClientID[3]).value = id;
    return true;
}
function EliminarOrigen(id) {

    if ((id > 1) && id < 50) {
        alert('No se puede eliminar este origen. Es reservado para tareas de administración');
        return false;
    }
    else {

        document.getElementById(MyClientID[1]).value = id;
        return true;

    }
}
function ValidarEliminarOrigen() {

    var respuesta = confirm("Considere que este origen puede estar asociado a transacciones existentes.\n Si elimina este origen puede perder información de transacción.\n¿Está seguro que desea eliminar este origen?");
    return respuesta;

}

function SetThreshold() {
    var IndexSelected = document.getElementById(MyClientID[6]).selectedIndex;
    if (IndexSelected == 1) {
//        document.getElementById(MyClientID[12]).value = "750";
//    } else if (IndexSelected == 2 || IndexSelected == 3 || IndexSelected == 4) {
        document.getElementById(MyClientID[12]).value = "10000";
    } else if (IndexSelected == 2) {
        document.getElementById(MyClientID[12]).value = "63";
    } else {
        document.getElementById(MyClientID[12]).value = "";
    }
    return;
}

function SetModel() {
    var IndexSelected = document.getElementById(MyClientID[7]).selectedIndex;
    //DP
    if (IndexSelected == 1) {
        document.getElementById(MyClientID[8]).selectedIndex = 1;
    }
    //Secugen
    if (IndexSelected == 2) {
       document.getElementById(MyClientID[8]).selectedIndex = 3;
    }
    //Upek
    if (IndexSelected == 3) {
        document.getElementById(MyClientID[8]).selectedIndex = 0;
    }
    return false;
}

function CheckModel() {
    var IndexSelected = document.getElementById(MyClientID[7]).selectedIndex;
        //DP
    if (IndexSelected == 1) {
        IndexSelected = document.getElementById(MyClientID[8]).selectedIndex;
        if (IndexSelected != 1 && IndexSelected != 2) {
            alert("Modelo incorrecto para el sensor seleccionado!");
            document.getElementById(MyClientID[8]).selectedIndex = 1;
            IndexSelected = 1;
        }        
    }
        //Secugen
    if (IndexSelected == 2) {
        IndexSelected = document.getElementById(MyClientID[8]).selectedIndex;
        if (IndexSelected != 3 && IndexSelected != 4 && IndexSelected != 5) {
            alert("Modelo incorrecto para el sensor seleccionado!");
            document.getElementById(MyClientID[8]).selectedIndex = 3;
            IndexSelected = 2;
        }
    }
        //Upek
    if (IndexSelected == 3) {
        document.getElementById(MyClientID[8]).selectedIndex = 0;
        IndexSelected = 3;
    }
    return false;

}

function SetBpClientVerify() {
    var paso = 0;
    if (ValidarVacios(document.getElementById(MyClientID[0]).value) == true) {
        paso = 1;
    }
    //Validamos si seleccionó un tipo de documento.
    var IndexValue = document.getElementById(MyClientID[1]).selectedIndex;
    var SelectedVal = document.getElementById(MyClientID[1]).options[IndexValue].value;
    if (SelectedVal == 0) {
        paso = 1;
    }

    if (paso == 1) {
        alert('Debe ingresar un Tipo ID y Valor ID para poder inicializar el componente');
        return false;
    }
    else {

        //document.getElementById("BPClientAx1").OperationType = 1;
        IndexValue = document.getElementById(MyClientID[5]).selectedIndex;
        document.getElementById("BPClientAx1").AuthenticationFactor = parseInt(document.getElementById(MyClientID[5]).options[IndexValue].value);
        //Minutiaetype
        IndexValue = document.getElementById(MyClientID[6]).selectedIndex;
        document.getElementById("BPClientAx1").Minutiaetype = parseInt(document.getElementById(MyClientID[6]).options[IndexValue].value);
        
        IndexValue = document.getElementById(MyClientID[1]).selectedIndex;
        //document.getElementById("BPClientAx1").TypeId = document.getElementById(MyClientID[1]).options[IndexValue].value;
        //document.getElementById("BPClientAx1").ValueId = document.getElementById(MyClientID[0]).value;
        document.getElementById("BPClientAx1").SetTypeIdValueId(document.getElementById(MyClientID[1]).options[IndexValue].value,
                                                                document.getElementById(MyClientID[0]).value);
        IndexValue = document.getElementById(MyClientID[7]).selectedIndex;
        document.getElementById("BPClientAx1").Device = document.getElementById(MyClientID[7]).options[IndexValue].value;
        IndexValue = document.getElementById(MyClientID[8]).selectedIndex;
        document.getElementById("BPClientAx1").Model = document.getElementById(MyClientID[8]).options[IndexValue].value;
        //TokenContent [0-Ambas | 1-WSQ Solo | 2-AFIS Solo]
        if (document.getElementById(MyClientID[9]).checked == true)
            document.getElementById("BPClientAx1").TokenContent = 1;
        if (document.getElementById(MyClientID[10]).checked == true)
            document.getElementById("BPClientAx1").TokenContent = 2;
        if (document.getElementById(MyClientID[11]).checked == true)
            document.getElementById("BPClientAx1").TokenContent = 0;

        //document.getElementById("BPClientAx1").Timeout = 5000;

        return false;
    }
}
function SetBpClient() {   
    var paso = 0;
    if (ValidarVacios(document.getElementById(MyClientID[5]).value) == true) {       
        paso = 1;
    }
    //Validamos si seleccionó un tipo de documento.
    var IndexValue = document.getElementById(MyClientID[4]).selectedIndex;
    var SelectedVal = document.getElementById(MyClientID[4]).options[IndexValue].value;
    if (SelectedVal == 0) {       
        paso = 1;
    }
   
    if (paso == 1) {
        alert('Debe ingresar un Tipo ID y Nro ID para poder inicializar el componente');
        return false;
    }
    else {
        //document.getElementById("BPClientAx1").OperationType = 2;
        /*
            DropDownListAF, //15
            DropDownListDevice, //16
            DropDownListModel); //17
        */
        document.getElementById("BPClientAx1").TypeId = document.getElementById(MyClientID[4]).options[IndexValue].value;
        IndexValue = document.getElementById(MyClientID[15]).selectedIndex;
        document.getElementById("BPClientAx1").AuthenticationFactor = parseInt(document.getElementById(MyClientID[15]).options[IndexValue].value);
        document.getElementById("BPClientAx1").ValueId = document.getElementById(MyClientID[5]).value;
        IndexValue = document.getElementById(MyClientID[16]).selectedIndex;
        document.getElementById("BPClientAx1").Device = document.getElementById(MyClientID[16]).options[IndexValue].value;
        IndexValue = document.getElementById(MyClientID[17]).selectedIndex;
        document.getElementById("BPClientAx1").Model = document.getElementById(MyClientID[17]).options[IndexValue].value;
        return false;
    }
}
function SetBpClientAddBir() {
    //document.getElementById("BPClientAx1").OperationType = 2;
    /*
    DropDownListAF, //15
    DropDownListDevice, //16
    DropDownListModel); //17
    */
    var IndexValue = document.getElementById(MyClientID[15]).selectedIndex;
    document.getElementById("BPClientAx1").AuthenticationFactor = parseInt(document.getElementById(MyClientID[15]).options[IndexValue].value);
    document.getElementById("BPClientAx1").TypeId = document.getElementById(MyClientID[4]).value;
    document.getElementById("BPClientAx1").ValueId = document.getElementById(MyClientID[5]).value;
    IndexValue = document.getElementById(MyClientID[16]).selectedIndex;
    document.getElementById("BPClientAx1").Device = document.getElementById(MyClientID[16]).options[IndexValue].value;
    IndexValue = document.getElementById(MyClientID[17]).selectedIndex;
    document.getElementById("BPClientAx1").Model = document.getElementById(MyClientID[17]).options[IndexValue].value;
    return false;
}
function ValidarFormularioEnroll(operacion) {    
    
    var paso = 0;
    var msg = "Los siguientes campos son obligatorios:";
  
    //Validamos si el rut viene con datos.
    if (ValidarVacios(document.getElementById(MyClientID[5]).value) == true) {
        msg = msg + "\n Debe Ingresar un valor para ese tipo de identificación";
        paso = 1;
    }
    //Validamos si seleccionó un tipo de documento.
    var IndexValue = document.getElementById(MyClientID[4]).selectedIndex;
    var SelectedVal = document.getElementById(MyClientID[4]).options[IndexValue].value;
    if (SelectedVal == 0) {
        msg = msg + "\n Debe seleccionar un tipo de identificación";
        paso = 1;
    }
    if (operacion == "" && ValidarVacios(document.getElementById("BPClientAx1").Token)==true) {
        msg = msg + "\n Debe ingresar su huella";
        paso = 1;
    }
    //var firma = document.getElementById("AxFirma").ImageB64;
    //var firma = document.getElementById("BPImageAx22").ImageB64;
    var firma = document.getElementById("AxFirma").GetImagenB64();
    if (firma != null && firma.length > 0) {
        if (firma.length < 4000) {
            msg = msg + "La imagen de la firma debe ser de tamano mayor que 4Kb y menor que 256Kb!";
            paso = 1;
        }
    }
    //var foto = document.getElementById("AxFoto").ImageB64;
    var foto = document.getElementById("AxFoto").GetImagenB64();
    if (foto != null && foto.length > 0) {
        if (foto.length < 4000) {
            msg = msg + "La imagen de la foto debe ser de tamano mayor que 4Kb y menor que 256Kb!";
            paso = 1;
        }
    }
    if (paso == 1) {
        alert(msg);
        return false;
    }
    else {
        if (operacion == "modificar") {
            document.getElementById(MyClientID[8]).value = "modificar";            
        }
        else 
        {
            document.getElementById(MyClientID[8]).value = "agregar";            
        }
        //alert(document.getElementById(MyClientID[8]).value);
        document.getElementById(MyClientID[9]).value = document.getElementById("BPClientAx1").Token;
        document.getElementById(MyClientID[10]).value = document.getElementById("BPClientAx1").Finger;
        //Corresponde a la firma       		
// TODO : Chequear que no es vacio, sino no copio, para no perder la copia desde el Selected
        document.getElementById(MyClientID[13]).value = document.getElementById("AxFirma").GetImagenB64();
        //Corresponde a la foto
// TODO : Chequear que no es vacio, sino no copio, para no perder la copia desde el Selected
        document.getElementById(MyClientID[14]).value = document.getElementById("AxFoto").GetImagenB64();
        document.aspnetForm.method = "post";
        document.aspnetForm.action = "RegisterIdentity.aspx";
        return true;
        
    }

}

function VerifyForm(parametro) {
   
    //alert(parametro);
    var stoken0 = document.BPClientAx1.GetToken();
    var stoken1 = document.BPClientAx1.Token;
    var stoken = document.getElementById("BPClientAx1").Token;
    if (ValidarVacios(stoken) == true) {
        alert("Debe generar un token de verificación!");
        return false;
    }

    document.getElementById(MyClientID[2]).value = document.getElementById("BPClientAx1").Token;
    document.getElementById(MyClientID[3]).value = document.getElementById("BPClientAx1").Finger;
    if (parametro == "VBDL")
        document.getElementById(MyClientID[4]).value = "VBDL";
    if (parametro == "IBDL")
        document.getElementById(MyClientID[4]).value = "IBDL";
    //document.aspnetForm.method = "post";
    //document.aspnetForm.action = "Verify.aspx";
    //document.aspnetForm.submit();
    return true;
}
function AgregarIdentidad() {    
    document.getElementById(MyClientID[0]).value = "agregar";
    document.aspnetForm.method = "post";
    document.aspnetForm.action = "RegisterIdentity.aspx";
    document.aspnetForm.submit();
    return true;
}
function ModificarIdentidad(id) {
    
    document.getElementById(MyClientID[0]).value = "modificar";
    document.getElementById(MyClientID[2]).value = id;
    document.aspnetForm.method = "post";
    document.aspnetForm.action = "RegisterIdentity.aspx";
    document.aspnetForm.submit();
    return true;
}

function PopUpInfo(url) {

    //var url = "VerInfo.aspx?id=" + id + "&tipo=" + tipo + "&valor" + valor;    
    var ventana = window.open(url, 'Datos', 'width=640,height=350,menubar=no,location=no,resizable=no,scrollbars=no,toolbar=no,status=no', false);    
    return false;
}
function AddBirs(id,typeid,valueid) {
    
    document.getElementById(MyClientID[1]).value = id;    
    document.aspnetForm.method = "post";
    document.aspnetForm.action = "AddBir.aspx?id="+id + " &typeid=" + typeid + "&valueid=" + valueid;
    document.aspnetForm.submit();
    return true;
}
function DeleteBirs(id) {

    document.getElementById(MyClientID[1]).value = id;
    document.aspnetForm.method = "post";
    document.aspnetForm.action = "DeleteBir.aspx?id=" + id ;
    document.aspnetForm.submit();
    return true;
}
function AddBir() {

    var paso = 0;
    var msg = "Los siguientes campos son obligatorios:";
    if (ValidarVacios(document.getElementById("BPClientAx1").Token) == true) {
        msg = msg + "\n Debe ingresar su huella";
        paso = 1;
    }
    if (paso == 1) {
        alert(msg);
        return false;
    }
    else {
        document.getElementById(MyClientID[1]).value = document.getElementById("BPClientAx1").Token;
        document.getElementById(MyClientID[2]).value = document.getElementById("BPClientAx1").Finger;
        document.aspnetForm.method = "post";
        document.aspnetForm.action = "AddBir.aspx";
        document.aspnetForm.submit();
        return true;
    }
   
}