﻿
/* **************************************************************************
 * Funciones para manejo de menu general.
 ************************************************************************** */

/* 
    Si esta visible el div de menu (esta abierto), se ocultam se cierra div de 
    header opened y se muestra div header closed. Sino, lo contrario.
*/
function SetMenu() {
    
    if (document.getElementById('divMenu').style.display == '') {
        document.getElementById('divMenu').style.display = 'none';
        document.getElementById('divHeaderMenuClosed').style.display = '';
        document.getElementById('divHeaderMenuOpened').style.display = 'none';
    } else {
        document.getElementById('divMenu').style.display = '';
        document.getElementById('divHeaderMenuClosed').style.display = 'none';
        document.getElementById('divHeaderMenuOpened').style.display = '';
    }

}

/*
    Coloca en Closed el menu vertical.
*/
function SetMenuClosed() {
    
    alert(document.getElementById('divMenu').style);
    document.getElementById('divMenu').style.display = 'none';
    document.getElementById('divHeaderMenuOpened').style.display = 'none';
    document.getElementById('divHeaderMenuClosed').style.display = '';
}
