﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;




namespace Bio.Portal.Server.Manager
{
    public partial class Default : System.Web.UI.Page
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Default));
        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicializamos el soporte de javascript del sitio.
            HtmlGenericControl myJs = new HtmlGenericControl();
            myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
            myJs.Attributes.Add("language", "javascript");
            //don't need it usually but for cross browser.
            myJs.Attributes.Add("src", ResolveUrl("~/Scripts/menuVertical.js"));
            this.Page.Header.Controls.Add(myJs);
            myJs.TagName = "script"; myJs.Attributes.Add("type", "text/javascript");
            myJs.Attributes.Add("language", "javascript");
            //don't need it usually but for cross browser.
            myJs.Attributes.Add("src", ResolveUrl("~/Scripts/BioPortalManager.js"));
            this.Page.Header.Controls.Add(myJs);

            //Verificamos accesos
            if (User.IsInRole("Super Administrador"))
                BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
            else if(User.IsInRole("Administrador"))
                BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

            if (User.IsInRole("Enrolador"))
                BioPortalServer.VisualizarMenuEnrolador(Master);
            if(User.IsInRole("Verificador"))
                BioPortalServer.VisualizarMenuVerificar(Master);

        }
    }
}