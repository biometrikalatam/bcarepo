﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Login.Master" CodeBehind="Logon.aspx.cs" Inherits="Bio.Portal.Server.Manager.Logon" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<table width="90%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">
            <asp:Login ID="Login1" runat="server" 
                FailureText="No se pudo verificar el usuario.Intente nuevamente" 
                LoginButtonText="Ingresar"        
                PasswordRequiredErrorMessage="Debe ingresar su password."                 
                UserNameRequiredErrorMessage="Nombre de Usuario es Requerido." 
                LoginButtonStyle-CssClass="inBotonEnviar"                                 
                TextBoxStyle-Width="150"
                BorderStyle="None"                                                
                LabelStyle-CssClass="loginA"
                UserNameLabelText="User ID"
                PasswordLabelText="Clave"
                TitleText=""             
                DisplayRememberMe="false"   
                onloggedin="Login1_LoggedIn" OnAuthenticate="Login1_Authenticate">
            </asp:Login>               
        </td>
    </tr>
</table>
</asp:Content>


