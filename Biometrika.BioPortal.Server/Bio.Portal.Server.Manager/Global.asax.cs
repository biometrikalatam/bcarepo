﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using log4net;
using log4net.Config;
using System.IO;
using Bio.Portal.Server.Common.Entities.Database;
using Bio.Portal.Server.Common.Common;
using System.Collections;



namespace Bio.Portal.Server.Manager
{
    public class Global : System.Web.HttpApplication
    {
        /// <summary>
        /// LOG del modulo
        /// </summary>
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Global));
        public static String DomainApp="";
        
        public static DocumentTypes documentos;
        public static OperationTypes operations;
        public static StateTypes states;
        public static Hashtable Fotos = new Hashtable();

        protected void Application_Start(object sender, EventArgs e)
        {
            //0.- Inicializo LOG
            XmlConfigurator.Configure(
                new FileInfo(Bio.Portal.Server.Manager.Properties.Settings.Default.PathConfigLog));
            LOG.Info("BioPortal Server Manager Starting...");

            //Obtenemos la empresa por defecto dueña del servicio de super administrador
            DomainApp = Bio.Portal.Server.Manager.Properties.Settings.Default.Domainsetup;

            StateTypes estados = new StateTypes();
            estados.AddItem(new StateType("0", "ESTADO_OK"));
            estados.AddItem(new StateType("1", "ESTADO_USUARIO_NO_VALIDO"));
            estados.AddItem(new StateType("2", "ESTADO_XML_INVALIDO"));
            estados.AddItem(new StateType("3", "ESTADO_NO_EXISTE_INFO"));
            estados.AddItem(new StateType("4", "ESTADO_CALIDAD_DEFICIENTE"));

            Bio.Core.Utils.SerializeHelper.SerializeToFile(estados, Bio.Portal.Server.Manager.Properties.Settings.Default.PathStateType);

            documentos=Bio.Core.Utils.SerializeHelper.DeserializeFromFile<DocumentTypes>(Bio.Portal.Server.Manager.Properties.Settings.Default.PathTipoDocs);
            operations = Bio.Core.Utils.SerializeHelper.DeserializeFromFile<OperationTypes>(Bio.Portal.Server.Manager.Properties.Settings.Default.PathOperType);
            states = Bio.Core.Utils.SerializeHelper.DeserializeFromFile<StateTypes>(Bio.Portal.Server.Manager.Properties.Settings.Default.PathStateType);
            LOG.Info("BioPortal Server Manager Started!");

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

    }
}