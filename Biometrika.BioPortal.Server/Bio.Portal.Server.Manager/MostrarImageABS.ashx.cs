using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Drawing;
using System.IO;
using Bio.Core.Imaging;
using System.Drawing.Imaging;
using Bio.Portal.Server.Manager;

namespace WebAssistance.Cliente
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class MostrarImageABS : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string type = context.Request["type"];
            string rut = context.Request["id"];
            if (rut == null)
                return;
            rut = rut.Trim();
            string imageB64 = Global.Fotos[rut + "_" + context.Request["type"]] as string;

            byte[] bytesFoto = String.IsNullOrEmpty(imageB64) ? null : Convert.FromBase64String(imageB64);
            if (bytesFoto == null)
            {
                if (type.Equals("huella")) {
                    context.Response.Redirect("../../images/FondoBPC_Boton_plano.jpg");
                }
                if (type.Equals("foto")) {
                    context.Response.Redirect("../../Images/User.png");
                }
                if (type.Equals("barcode")) {
                    context.Response.Redirect("../../Images/qr.png");
                }
            }
            else
            {
                try
                {
                    Bio.Core.Imaging.Imagen f = new Imagen(bytesFoto);
                    f.Ajustar(300, 300);
                    //using (Bitmap b = new Bitmap(new MemoryStream(bytesFoto)))
                    using (Bitmap b = new Bitmap(new MemoryStream(f.Data)))
                    {

                        b.Save(context.Response.OutputStream, ImageFormat.Jpeg);
                        context.Response.OutputStream.Flush();
                    }
                }
                catch (Exception e)
                {
                    context.Response.Redirect(type.Equals("firma")
                                              ? "../../Images/PScertificado_nofirma.jpg"
                                              : "../../Images/PScertificado_nofoto.jpg");
                }


            }
            //using (Bitmap b = new Bitmap(new MemoryStream(bytesFoto)))
            //{
            //    context.Response.ContentType = "image/jpeg";
            //    b.Save(context.Response.OutputStream, ImageFormat.Jpeg);
            //    context.Response.OutputStream.Flush();
            //};
            }

            public bool IsReusable
        {
            get
            {
                return false;
            }
        }

       
    }
}
