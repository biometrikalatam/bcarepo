﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;

namespace Bio.Portal.Server.Manager
{
    public partial class BioPortalServer : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        public static void HiddenLinkConfig(MasterPage Master, Boolean value)
        {
            HyperLink link = Master.FindControl("hyperconfiguration") as HyperLink;
            link.Visible = value;
        }
        public static void HiddenLinkUser(MasterPage Master, Boolean value)
        {
            HyperLink link = Master.FindControl("hyperusers") as HyperLink;
            link.Visible = value;
        }
        public static void HiddenLinkCompany(MasterPage Master, Boolean value)
        {
            HyperLink link = Master.FindControl("hyperCompany") as HyperLink;
            link.Visible = value;
        }
        public static void HiddenLinkOrigen(MasterPage Master, Boolean value)
        {
            HyperLink link = Master.FindControl("hyperOrigen") as HyperLink;
            link.Visible = value;
        }
        public static void HiddenLinkChange(MasterPage Master, Boolean value)
        {
            HyperLink link = Master.FindControl("hyperChangePassword") as HyperLink;
            link.Visible = value;
        }
        public static void HiddenLinkLogTx(MasterPage Master, Boolean value)
        {
            HyperLink link = Master.FindControl("hyperLogTx") as HyperLink;
            link.Visible = value;
        }
        public static void HiddenLinkLog(MasterPage Master, Boolean value)
        {
            HyperLink link = Master.FindControl("hyperLog") as HyperLink;
            link.Visible = value;
        }
        public static void HiddenLinkBusqueda(MasterPage Master, Boolean value)
        {
            HyperLink link = Master.FindControl("hyperbusqueda") as HyperLink;
            link.Visible = value;
        }
        public static void HiddenLinkEnrolar(MasterPage Master, Boolean value)
        {
            HyperLink link = Master.FindControl("hyperenrolar") as HyperLink;
            link.Visible = value;
        }
        public static void HiddenLinkVerificar(MasterPage Master, Boolean value)
        {
            HyperLink link = Master.FindControl("hyperverificar") as HyperLink;
            link.Visible = value;
        }
        public static void VisualizarMenuEnrolador(MasterPage Master)
        {
            HiddenLinkBusqueda(Master, true);
            HiddenLinkEnrolar(Master, true);
        }
        public static void VisualizarMenuVerificar(MasterPage Master)
        {
            HiddenLinkVerificar(Master, true);
            
        }
        public static void VisualizarMenuAdministracion(MasterPage Master, string perfil)
        {
            if (perfil.Equals("Super Administrador"))
            {
                HiddenLinkConfig(Master, true);
                HiddenLinkCompany(Master, true);
                HiddenLinkUser(Master, true);
                HiddenLinkOrigen(Master, true);
                HiddenLinkChange(Master, true);  
                HiddenLinkLogTx(Master, true);
                HiddenLinkLog(Master, true);
                HiddenLinkBusqueda(Master, true);
                HiddenLinkEnrolar(Master, true);
                HiddenLinkVerificar(Master, true);
            }
            else if (perfil.Equals("Administrador"))
            {
                HiddenLinkConfig(Master, false);
                HiddenLinkCompany(Master, false);
                HiddenLinkUser(Master, true);
                HiddenLinkOrigen(Master, true);
                HiddenLinkChange(Master, true);
                HiddenLinkLogTx(Master, true);
                HiddenLinkLog(Master, true);
                HiddenLinkBusqueda(Master, true);
                HiddenLinkEnrolar(Master, true);
                HiddenLinkVerificar(Master, true);
           }
           
            
        }

        protected void imgCerrar_Click(object sender, ImageClickEventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect(FormsAuthentication.LoginUrl);
        }

        protected void imgCerrar2_Click(object sender, ImageClickEventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect(FormsAuthentication.LoginUrl);
        }

        //GGS desde 26-07-2010

        public static void VisualizarMenu(MasterPage Master, IPrincipal user)
        {

            if (user == null) return;

            if (user.IsInRole("Super Administrador"))
            {
                HiddenLinkConfig(Master, true);
                HiddenLinkCompany(Master, true);
                HiddenLinkUser(Master, true);
                HiddenLinkOrigen(Master, true);
                HiddenLinkChange(Master, true);
                HiddenLinkLogTx(Master, true);
                HiddenLinkLog(Master, true);
                HiddenLinkBusqueda(Master, true);
                HiddenLinkEnrolar(Master, true);
                HiddenLinkVerificar(Master, true);
            }
            else if (user.IsInRole("Administrador"))
            {
                HiddenLinkConfig(Master, false);
                HiddenLinkCompany(Master, false);
                HiddenLinkUser(Master, true);
                HiddenLinkOrigen(Master, true);
                HiddenLinkChange(Master, true);
                HiddenLinkLogTx(Master, true);
                HiddenLinkLog(Master, true);
                HiddenLinkBusqueda(Master, true);
                HiddenLinkEnrolar(Master, true);
                HiddenLinkVerificar(Master, true);
            }
            else if (user.IsInRole("Oficial Auditor"))
            {
                HiddenLinkConfig(Master, false);
                HiddenLinkCompany(Master, false);
                HiddenLinkUser(Master, false);
                HiddenLinkOrigen(Master, false);
                HiddenLinkChange(Master, true);
                HiddenLinkLogTx(Master, true);
                HiddenLinkLog(Master, true);
                HiddenLinkBusqueda(Master, true);
                HiddenLinkEnrolar(Master, true);
                HiddenLinkVerificar(Master, true);
            }
            else if (user.IsInRole("Enrolador"))
            {
                HiddenLinkConfig(Master, false);
                HiddenLinkCompany(Master, false);
                HiddenLinkUser(Master, false);
                HiddenLinkOrigen(Master, false);
                HiddenLinkChange(Master, true);
                HiddenLinkLogTx(Master, false);
                HiddenLinkLog(Master, false);
                HiddenLinkBusqueda(Master, true);
                HiddenLinkEnrolar(Master, true);
                HiddenLinkVerificar(Master, true);
            }
            else if (user.IsInRole("Verificador"))
            {
                HiddenLinkConfig(Master, false);
                HiddenLinkCompany(Master, false);
                HiddenLinkUser(Master, false);
                HiddenLinkOrigen(Master, false);
                HiddenLinkChange(Master, true);
                HiddenLinkLogTx(Master, false);
                HiddenLinkLog(Master, false);
                HiddenLinkBusqueda(Master, false);
                HiddenLinkEnrolar(Master, false);
                HiddenLinkVerificar(Master, true);
            }
            
        }

        public static int GetRolId(IPrincipal user)
        {
            if (user == null) return -1;
            if (user.IsInRole("Super Administrador")) 
            {
                return 1;
            } else if (user.IsInRole("Administrador"))
            {
                return 2;
            }
            else if (user.IsInRole("Oficial Auditor"))
            {
                return 3;
            }
            else if (user.IsInRole("Enrolador"))
            {
                return 4;
            }
            else if (user.IsInRole("Verificador"))
            {
                return 5;
            }
            return -1;
        }

        
    }
}