﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bio.Portal.Server.Common.Common;
using Bio.Portal.Server.Manager.Core.libs;
using log4net;

namespace Bio.Portal.Server.Manager
{
    public partial class Welcome : Pages
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Welcome));
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session != null)
            {
                Session["mensajeerror"] = null;
                Session["exception"] = null;
            }

            //Dibuja Menu
            BioPortalServer.VisualizarMenu(Master, User);

            //Verificamos accesos
            //if (User.IsInRole("Super Administrador"))
            //    BioPortalServer.VisualizarMenuAdministracion(Master, "Super Administrador");
            //else if (User.IsInRole("Administrador"))
            //    BioPortalServer.VisualizarMenuAdministracion(Master, "Administrador");

            //if (User.IsInRole("Enrolador"))
            //    BioPortalServer.VisualizarMenuEnrolador(Master);
            //if (User.IsInRole("Verificador"))
            //    BioPortalServer.VisualizarMenuVerificar(Master);

            log.Debug("Welcome :" + UserNameFull);
            Utils.IsSessionValid(Session, Response, "~/SessionExpired.aspx");
        }
    }
}