﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using log4net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using Bio.Core.Serialize;
using Biometrika.NamkuFaceAPI;
using Biometrika.NamkuFaceAPI.Face;
using Newtonsoft.Json;

namespace Bio.Core.Matcher.Jpeg
{
    public class Matcher : IMatcher
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Matcher));

#region Implementation of IMatcher

        private int _authenticationFactor;
        private int _minutiaeType;
        private double _threshold;
        private int _matchingType;

        private string _parameters;

        //FPhi.Matcher.Matcher matcher;
        //FPhi.Matcher.MatcherConfigurationManager matcherConfig;

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }


        /// <summary>
        /// Tecnologia a utilizar para matching
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de verificación considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Dos posibles valores:
        ///     0 - FIRST   -> El primer matching positivo
        ///     1 - BEST    -> El mejor matching positivo
        /// </summary>
        public int MatchingType
        {
            get { return _matchingType; }
            set { _matchingType = (value == 1 || value == 2) ? value : 1; }
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// y nuevo, parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;
            byte[] user1 = null;
            NamkuResponseConpareFace oResponse = null;
            TokenNamku tokennamku = null;
            TokenNamku2 token = null;

            try
            {
                if (!NamkuUtils.IsInitialized)
                {
                    if (!NamkuUtils.Initialize(_parameters))
                    {
                        msg = "Bio.Core.Matcher.Namku.Matcher Error Chequeando Parameters";
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_BAD_PARAMETER;
                    }
                }
                else
                {
                    if (!NamkuUtils.CheckValidityToken())
                    {
                        msg = "Bio.Core.Matcher.Namku.Matcher Error Chequeando Token";
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_BAD_PARAMETER;
                    }
                }

                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                //1. Importo template de registro, creo cada uno y los importo
                // Debo tratar diferente si es un JPG o un Embedding ya generado, y considerarlo cuando se hace Compare
                int CurrentTemplateMT = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_JPG;
                try
                {
                    byCurrentTemplate = Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                    try
                    {
                        CurrentTemplateMT = Convert.ToInt32((string)paramin.GetValue("currentTemplateMT"));
                    }
                    catch (Exception ex)
                    {
                        msg = "Bio.Core.Matcher.Namku.Matcher Error obteniendo MT del CurrentTemplate... [" + ex.Message + "]";
                        LOG.Warn(msg);
                        CurrentTemplateMT = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_JPG;
                    }
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.Namku.Matcher Error importando CurrentTemplate " +
                         "[ex=" + ex.Message + "]";
                    LOG.Fatal(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                                (string)paramin.GetValue("listBaseTemplates"));
                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                CompareResponse _CompareResponse = null;

                /* Debo armar el Compare segun los parametros enviados:
                      2.- Solo si son dos JPG => Envio ambos JPG como MemoryStream y JSON nulo, sino no proceso
                */
               if (CurrentTemplateMT == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_JPG &&
                  listTemplatesBase[0].MinutiaeType == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_JPG)
                {
                    _CompareResponse = NamkuUtils.NAMKU_API.Compare(new MemoryStream(byCurrentTemplate), new MemoryStream(listTemplatesBase[0].Data), null);
                }
               

                if (_CompareResponse == null || _CompareResponse.Error != null)
                {
                    LOG.Debug("Bio.Core.Matcher.Namku.Matcher _CompareResponse == NULL => " + (_CompareResponse == null));
                    if (_CompareResponse.Error != null)  // => Hubo error => Lo muestro
                    {
                        LOG.Error("Bio.Core.Matcher.Namku.Matcher Error => " + _CompareResponse.Error.Code + " - " + _CompareResponse.Error.Message);
                    }
                    score = 5;
                    verifyOk = false;
                    bestscore = 5;
                }
                else //No hubo error
                {
                    LOG.Debug("Bio.Core.Matcher.Namku.Matcher tokennamku.Data.Distance => " + (_CompareResponse.Data.Closest.Distance).ToString());

                    score = Convert.ToDouble(_CompareResponse.Data.Closest.Distance);    //(tokennamku.Data != null && tokennamku.Data.Distance != null) ? Convert.ToDouble(tokennamku.Data.Distance, CultureInfo.InvariantCulture) : 5;
                    if (score <= _threshold) //La distancia debe ser menor al threshold indicado, cunado mas cerca de 0 mejor
                    {
                        verifyOk = true;
                        //Si es Matching type first => Retorno
                        if (this._matchingType == 1)
                        {
                            bestscore = score;
                            templateVerified = listTemplatesBase[0];
                            //break;
                        }
                        else
                        {
                            if (score < bestscore)
                            {
                                templateVerified = listTemplatesBase[0];
                                bestscore = score;
                            }
                        }
                    }
                    else
                    {
                        bestscore = score;
                    }
                    LOG.Debug("Bio.Core.Matcher.Namku.Matcher Score = " + score);
                    LOG.Debug("Bio.Core.Matcher.Namku.Matcher bestscore = " + bestscore);
                    LOG.Debug("Bio.Core.Matcher.Namku.Matcher verifyOk = " + verifyOk.ToString());
                }
                //    break; //Salgo del for porque 
                //}

                //3.- Si verifico bien armo salida
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                if (verifyOk)
                {
                    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                }
                else
                {
                    paramout.SetValue("message", "No hubo matching positivo [" + (token != null &&
                                                                                  oResponse.error != null &&
                                                                                  oResponse.error.message != null ? oResponse.error.message : "") + "]");
                }
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                LOG.Debug("Bio.Core.Matcher.Namku.Matcher xmloutput = " + xmloutput);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Namku.Matcher.Verify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_VERIFY;
            }

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// y lista contra que verificar, parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;
            byte[] user1 = null;

            try
            {
                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                //1. Importo template de registro, creo cada uno y los importo
                

                //3.- Si verifico bien armo salida
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                paramout.SetValue("message", "No hubo matching positivo");
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Jpeg.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

        public int Identify(ITemplate templateCurrent, List<Bio.Core.Matcher.Template> listTemplatePeople, 
                            out float score, out int idBir)
        {
            idBir = 0;
            score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;
            byte[] user1 = null;

            try
            {
                byCurrentTemplate = templateCurrent.Data;

               

                //3.- Si verifico bien armo salida
                score = (float)bestscore;
                idBir = 0;

              
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Namku.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

        public int Verify(ITemplate template1, ITemplate template2, out float score)
        {
            throw new NotImplementedException();
        }


        #endregion

        public void Dispose()
        {
        }
    }

    //{"output": {"distance": "0.0"}}
    public class output
    {

        public float distance { get; set; }

    }

    public class error
    {
        public string message { get; set; }
        public string reason { get; set; }

    }

    /*
       {
         "data":null,
         "error":{
              "message":"No se detectaron caras en alguna de las imagenes",
               "reason":"noFaces"},
         "status_code":400,
         "success":false
        }
    */
    public class NamkuResponseConpareFace
    {
        public output output { get; set; }
        public string data { get; set; }
        public string status_code { get; set; }
        public string success { get; set; }
        public error error { get; set; }

    }
}
