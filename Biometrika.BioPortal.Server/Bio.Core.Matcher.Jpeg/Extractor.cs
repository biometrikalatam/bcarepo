﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using log4net;

namespace Bio.Core.Matcher.Jpeg
{
    public class Extractor : IExtractor
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Extractor));
  
#region Implementation of IExtractor

        private int _authenticationFactor;

        private int _minutiaeType;

        private double _threshold;

        private string _parameters;

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        /// <summary>
        /// Tecnologia a utilizar para extraccion
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo d eminucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de extracción considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Extrae desde el template ingresado en el parametro inicial, de acuerdo a 
        /// los adtos de tecnologia y minucias seteados, el template resultante. 
        /// Estos parámetros están serializados en xml, por lo que primero se deserializa.
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="xmloutput">template generado serializado en xml</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out string xmloutput)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero entrega un ITemplate
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out ITemplate templateout)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero ingresa un ITemplate y entrega un ITemplate
        /// </summary>
        /// <param name="templatebase">Template base para extraccion</param>
        /// <param name="destination">Determina si es template para 1-Verify | 2-Enroll </param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(ITemplate templatebase, int destination, out ITemplate templateout)
        {
            int res = Errors.IERR_OK;
            templateout = null;

            try
            {

                if (templatebase == null) return Errors.IERR_NULL_TEMPLATE;
                if (templatebase.Data == null) return Errors.IERR_NULL_TEMPLATE;
                if (templatebase.AuthenticationFactor != Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL)
                {
                    return Errors.IERR_INVALID_TEMPLATE;
                }

                if (templatebase.MinutiaeType != Constant.MinutiaeType.MINUTIAETYPE_JPG)
                {
                    return Errors.IERR_INVALID_TEMPLATE;
                }

                  //Si lo anterior esta ok, y la minucia es Verifinger solo asigno porque 
                if (templatebase.MinutiaeType == Constant.MinutiaeType.MINUTIAETYPE_JPG)
                {
                    //templateout = templatebase;
                    templateout = new Template();
                    //templateout.AdditionalData = templatebase.AdditionalData;
                    templateout.AuthenticationFactor = templatebase.AuthenticationFactor;
                    templateout.BodyPart = templatebase.BodyPart;
                    templateout.SetData = templatebase.GetData;
                    templateout.AdditionalData = "SizeData=" + templateout.Data.Length.ToString(); //templatebase.AdditionalData;
                    templateout.MinutiaeType = templatebase.MinutiaeType;                 
                } else { //No sirve para procesar => Devuelvo nulo

                    templateout = null;
                    LOG.Warn("Bio.Core.Matcher.Jpeg.Extract(T, T) => MinutiaeType != MINUTIAETYPE_JPG");
                    return Errors.IERR_INVALID_TEMPLATE;
                }
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("Bio.Core.Matcher.Jpeg.Extract(T, T) Error", ex);
            }
            return res;
        }

#endregion Implementation of IExtractor

        public void Dispose()
        {
            try
            {
                //ExtractorLicenseManager.ReleaseExtractor();
            }
            catch { }
        }
    }
}
