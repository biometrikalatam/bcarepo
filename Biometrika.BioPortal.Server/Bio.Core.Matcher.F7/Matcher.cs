﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using Bio.Core.Serialize;
using FPhi.Matcher;
using log4net;

namespace Bio.Core.Matcher.F7
{
    public class Matcher : IMatcher
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Matcher));

#region Implementation of IMatcher

        private int _authenticationFactor;
        private int _minutiaeType;
        private double _threshold;
        private int _matchingType;

        private string _parameters;

        FPhi.Matcher.Matcher matcher;
        FPhi.Matcher.MatcherConfigurationManager matcherConfig;

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }


        /// <summary>
        /// Tecnologia a utilizar para matching
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de verificación considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Dos posibles valores:
        ///     0 - FIRST   -> El primer matching positivo
        ///     1 - BEST    -> El mejor matching positivo
        /// </summary>
        public int MatchingType
        {
            get { return _matchingType; }
            set { _matchingType = (value == 1 || value == 2) ? value : 1; }
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// y nuevo, parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;
            byte[] user1 = null;

            try
            {
                if (!F7Utils.IsInitialized)
                {
                    if (!F7Utils.Initialize(_parameters))
                    {
                        msg = "Bio.Core.Matcher.F7.Matcher Error Chequeando Parameters";
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_BAD_PARAMETER;
                    }
                }

                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                //1. Importo template de registro, creo cada uno y los importo
                try
                {
                    byCurrentTemplate = Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.F7.Matcher Error importando CurrentTemplate " +
                         "[ex=" + ex.Message + "]";
                    LOG.Fatal(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                
                //2.- Genero objeto para matching
                matcherConfig = new MatcherConfigurationManager();
                matcherConfig.MatcherSecurityLevel = F7Utils._SecurityLevel;

                matcher = new FPhi.Matcher.Matcher(matcherConfig);
               
                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                                (string)paramin.GetValue("listBaseTemplates"));
                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                //Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                 // Crear el primer usuario.
                user1 = matcher.CreateUser(byCurrentTemplate);

                   // Verificar los resultados de las extracciones.
                AuthenticationResult authenticationResult;

                for (int i = 0; i < listTemplatesBase.Count; ++i)
                {
                    authenticationResult = matcher.Authenticate(user1, listTemplatesBase[i].Data);
                    score = authenticationResult.Similarity;
                    if (score >= _threshold) //(authenticationResult.IsPositiveMatch && score >= _threshold)
                    {
                        verifyOk = true;
                        //Si es Matching type first => Retorno
                        if (this._matchingType == 1)
                        {
                            bestscore = score;
                            templateVerified = listTemplatesBase[i];
                            break;
                        }
                        else
                        {
                            if (score > bestscore)
                            {
                                templateVerified = listTemplatesBase[i];
                                bestscore = score;
                            }
                        }
                    }
                }

                //3.- Si verifico bien armo salida
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                if (verifyOk)
                {
                    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                }
                else
                {
                    paramout.SetValue("message", "No hubo matching positivo");
                }
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.F7.Matcher.Verify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_VERIFY;
            }

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// y lista contra que verificar, parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;
            byte[] user1 = null;

            try
            {
                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                //1. Importo template de registro, creo cada uno y los importo
                try
                {
                    byCurrentTemplate = Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.F7.Matcher Error importando CurrentTemplate " +
                         "[ex=" + ex.Message + "]";
                    LOG.Fatal(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Genero objeto para matching
                matcherConfig = new MatcherConfigurationManager();
                matcherConfig.MatcherSecurityLevel = F7Utils._SecurityLevel;

                matcher = new FPhi.Matcher.Matcher(matcherConfig);

                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                                (string)paramin.GetValue("listBaseTemplates"));
                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                //Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                // Crear el primer usuario.
                user1 = matcher.CreateUser(byCurrentTemplate);

                // Verificar los resultados de las extracciones.
                AuthenticationResult authenticationResult;

                for (int i = 0; i < listTemplatesBase.Count; ++i)
                {
                    authenticationResult = matcher.Authenticate(user1, listTemplatesBase[i].Data);
                    score = authenticationResult.Similarity;
                    if (authenticationResult.IsPositiveMatch && score >= _threshold)
                    {
                        verifyOk = true;
                        //Si es Matching type first => Retorno
                        if (this._matchingType == 1)
                        {
                            bestscore = score;
                            templateVerified = listTemplatesBase[i];
                            break;
                        }
                        else
                        {
                            if (score > bestscore)
                            {
                                templateVerified = listTemplatesBase[i];
                                bestscore = score;
                            }
                        }
                    }
                }

                //3.- Si verifico bien armo salida
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                if (verifyOk)
                {
                    paramout.AddValue("id", templateVerified == null ? 0 : templateVerified.Id);
                    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                }
                else
                {
                    paramout.SetValue("message", "No hubo matching positivo");
                }
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.F7.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

        public int Identify(ITemplate templateCurrent, List<Bio.Core.Matcher.Template> listTemplatePeople, 
                            out float score, out int idBir)
        {
            idBir = 0;
            score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;
            byte[] user1 = null;

            try
            {
                byCurrentTemplate = templateCurrent.Data;

                //2.- Genero objeto para matching
                matcherConfig = new MatcherConfigurationManager();
                matcherConfig.MatcherSecurityLevel = F7Utils._SecurityLevel;

                matcher = new FPhi.Matcher.Matcher(matcherConfig);

                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase = listTemplatePeople;
                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                //Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                // Crear el primer usuario.
                user1 = matcher.CreateUser(byCurrentTemplate);

                // Verificar los resultados de las extracciones.
                AuthenticationResult authenticationResult;

                for (int i = 0; i < listTemplatesBase.Count; ++i)
                {
                    authenticationResult = matcher.Authenticate(user1, listTemplatesBase[i].Data);
                    score = (float)authenticationResult.Similarity;
                    if (authenticationResult.IsPositiveMatch && score >= _threshold)
                    {
                        verifyOk = true;
                        //Si es Matching type first => Retorno
                        if (this._matchingType == 1)
                        {
                            bestscore = score;
                            templateVerified = listTemplatesBase[i];
                            break;
                        }
                        else
                        {
                            if (score > bestscore)
                            {
                                templateVerified = listTemplatesBase[i];
                                bestscore = score;
                            }
                        }
                    }
                }

                //3.- Si verifico bien armo salida
                score = (float)bestscore;
                idBir = templateVerified == null ? 0 : templateVerified.Id;

                //paramout.AddValue("score", bestscore);
                //paramout.AddValue("result", verifyOk ? 1 : 2);
                //paramout.AddValue("threshold", threshold);
                //if (verifyOk)
                //{
                //    paramout.AddValue("id", templateVerified == null ? 0 : templateVerified.Id);
                //    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                //    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                //    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                //    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                //}
                //else
                //{
                //    paramout.SetValue("message", "No hubo matching positivo");
                //}
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.F7.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

        public int Verify(ITemplate template1, ITemplate template2, out float score)
        {
            throw new NotImplementedException();
        }


        #endregion

        public void Dispose()
        {
        }
    }
}
