﻿
   using System;
using System.Globalization;
using System.Reflection;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.IO;
#if !N_PRODUCT_HAS_NO_LICENSES
using FPhi.Matcher;
   using log4net;
#endif

namespace Bio.Core.Matcher.F7
{
    public static class F7Utils
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(F7Utils));

        public static bool IsInitialized;

        public static MatchingSecurityLevel _SecurityLevel = MatchingSecurityLevel.MediumHighSecurityLevel;



        /// <summary>
        /// Inicializa la licencia la primera vez que se usa una clase de este Namespace.
        /// </summary>
        public static bool Initialize(string parameters)
        {

            try
            {

                if (parameters == null)
                {
                    LOG.Error("Bio.Core.Matcher.F7.Initialize Error - Parameters Matcher = NULL");
                    return false;
                }

                LOG.Debug("Bio.Core.Matcher.F7.Initialize - ExtractPath(parameters) de parameters=" + parameters);
                IsInitialized = ExtractParams(parameters, out _SecurityLevel);

                if (!IsInitialized)
                {
                    LOG.Error("Bio.Core.Matcher.F7.Initialize Error - Component Parse parameters error");
                    return false;
                }

                LOG.Debug("Bio.Core.Matcher.F7.Initialize - IsInitialized=" + IsInitialized);

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.F7.Initializee Error", ex);
            }
            return IsInitialized;

        }

 
        //Extrae nivel d eseguridad de matching
        private static bool ExtractParams(string parameters, out MatchingSecurityLevel securitylevel)
        {
            securitylevel = MatchingSecurityLevel.MediumHighSecurityLevel;
            try
            {
                string[] arr = parameters.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                LOG.Debug("ExtractParams arr.length=" + arr.Length.ToString());
                if (arr.Length == 0) return false;

                for (int i = 0; i < arr.Length; i++)
                {
                    LOG.Debug("ExtractParams procesando arr[" + i.ToString() + "]=" + arr[i]);
                    string[] item = arr[i].Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                    LOG.Debug("ExtractParams item.length=" + item.Length.ToString());
                    if (item[0].Equals("SecurityLevel"))
                    {
                        securitylevel = SetSecurityLevel(item[1]);
                        LOG.Debug("ExtractParams securitylevel=" + securitylevel);
                    }
                }

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.F7.ExtractParams Error", ex);
                return false;
            }
            return true;
        }

        private static MatchingSecurityLevel SetSecurityLevel(string sSecurityLevel)
        {
            if (sSecurityLevel.Equals("Medium")) return MatchingSecurityLevel.MediumSecurityLevel;
            if (sSecurityLevel.Equals("High")) return MatchingSecurityLevel.HighSecurityLevel;
            if (sSecurityLevel.Equals("MediumHigh")) return MatchingSecurityLevel.MediumHighSecurityLevel;
            if (sSecurityLevel.Equals("VeryHigh")) return MatchingSecurityLevel.VeryHighSecurityLevel;
            if (sSecurityLevel.Equals("Highest")) return MatchingSecurityLevel.HighestSecurityLevel;
            return MatchingSecurityLevel.MediumHighSecurityLevel;
        }
    }

}
