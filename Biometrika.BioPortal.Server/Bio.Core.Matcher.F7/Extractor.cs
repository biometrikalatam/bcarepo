﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using Bio.Core.Wsq.Decoder;
using FPhi.Extractor;
using log4net;

namespace Bio.Core.Matcher.F7
{
    public class Extractor : IExtractor
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Extractor));

        FPhi.Extractor.ExtractorConfigurationManager _extractorConfig;
        FPhi.Extractor.Extractor _extractor;
        

  
#region Implementation of IExtractor

        private int _authenticationFactor;

        private int _minutiaeType;

        private double _threshold;

        private string _parameters;

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        /// <summary>
        /// Tecnologia a utilizar para extraccion
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo d eminucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de extracción considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Extrae desde el template ingresado en el parametro inicial, de acuerdo a 
        /// los adtos de tecnologia y minucias seteados, el template resultante. 
        /// Estos parámetros están serializados en xml, por lo que primero se deserializa.
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="xmloutput">template generado serializado en xml</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out string xmloutput)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero entrega un ITemplate
        /// </summary>
        /// <param name="xmlinput">xml con datos input</param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(string xmlinput, out ITemplate templateout)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Idem anterior pero ingresa un ITemplate y entrega un ITemplate
        /// </summary>
        /// <param name="templatebase">Template base para extraccion</param>
        /// <param name="destination">Determina si es template para 1-Verify | 2-Enroll </param>
        /// <param name="templateout">template generado</param>
        /// <returns>coidgo de error si existe o 0 si genero ok</returns>
        public int Extract(ITemplate templatebase, int destination, out ITemplate templateout)
        {
            int res = Errors.IERR_OK;
            templateout = null;
            List<ExtractionResult> extResList;
            string msg; 

            try
            {

                if (!F7Utils.IsInitialized)
                {
                    if (!F7Utils.Initialize(_parameters)) return Errors.IERR_BAD_PARAMETER;
                }

                if (templatebase == null) return Errors.IERR_NULL_TEMPLATE;
                if (templatebase.Data == null) return Errors.IERR_NULL_TEMPLATE;

                //Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_JPG 
                //Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL_F7
                if (templatebase.AuthenticationFactor != Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL)
                {
                    return Errors.IERR_INVALID_TEMPLATE;
                }

                if (templatebase.MinutiaeType != Constant.MinutiaeType.MINUTIAETYPE_FACIAL_F7 &&
                    templatebase.MinutiaeType != Constant.MinutiaeType.MINUTIAETYPE_JPG)
                {
                    return Errors.IERR_INVALID_TEMPLATE;
                }

                //if (!ExtractorLicenseManager.IsRegistered)
                //{
                //    ExtractorLicenseManager.ActivateExtractor();
                //}

                //Si lo anterior esta ok, y la minucia es Verifinger solo asigno porque 
                if (templatebase.MinutiaeType == Constant.MinutiaeType.MINUTIAETYPE_FACIAL_F7)
                {
                    //templateout = templatebase;
                    templateout = new Template();
                    templateout.AdditionalData = templatebase.AdditionalData;
                    templateout.AuthenticationFactor = templatebase.AuthenticationFactor;
                    templateout.BodyPart = templatebase.BodyPart;
                    templateout.SetData = templatebase.GetData;
                    templateout.MinutiaeType = templatebase.MinutiaeType;
                }
                else //es Constant.MinutiaeType.JPG (imagen) 
                {
                    //2.- Extraigo  
                    // Configurar extractor y matcher.
                    _extractorConfig = new ExtractorConfigurationManager();
                    _extractor = new FPhi.Extractor.Extractor(_extractorConfig);

                    // Realizar la extracción de la primera imagen. Para ello se ha de iniciar primero la cadena de extracción, indicando el mínimo número
                    // de patrones requerido y el número de muestras para realizar la extracción.
                    _extractor.InitStreamExtraction(1, 1);
                    //extResList1 = extractor.ExtractNext(b1);
                    MemoryStream bmpSample = new MemoryStream(templatebase.Data);
                    Bitmap bmp = new Bitmap(bmpSample);
                    
                    // STEP 2. Detect and extract facial patterns of the left image.
                    // If everything is OK, we will obtain a NOT null list of ExctractionResults and the first diagnostic will be a valid one.
                    extResList = _extractor.ExtractNext(bmp);
                
                    // Si el diagnóstico de la extracción es correcto, crear al usuario y realizamos la extracción de la segunda imagen.
                    if (extResList != null && extResList.Count > 0 &&
                        extResList[0].SampleDiagnostic == SampleDiagnostic.Ok)
                    {
                        templateout = new Template
                        {
                            Data = extResList[0].Template
                        };
                        templateout.AdditionalData = "SizeData=" + templateout.Data.Length.ToString();
                        templateout.AuthenticationFactor = templatebase.AuthenticationFactor;
                        templateout.BodyPart = templatebase.BodyPart;
                        templateout.MinutiaeType = Constant.MinutiaeType.MINUTIAETYPE_FACIAL_F7;
                        templateout.Type = Constant.BirType.PROCESSED_DATA;
                        bmpSample.Dispose();
                        bmpSample = null;
                        bmp.Dispose();
                        bmp = null;
                    }
                    else
                    {
                        res = Errors.IERR_EXTRACTING;
                        templateout = null;
                        LOG.Error("F7.Extractor.Extract Error [SampleDiagnostic=" +
                            extResList!=null && extResList.Count>0 ? 
                            extResList[0].SampleDiagnostic.ToString() : "null" 
                            + "]");

                    }
                }

            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("Bio.Core.Matcher.F7.Extract(T, T) Error", ex);
            }
            return res;
        }

#endregion Implementation of IExtractor

        public void Dispose()
        {
            try
            {
                //ExtractorLicenseManager.ReleaseExtractor();
            }
            catch { }
        }
    }
}
