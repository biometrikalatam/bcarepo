﻿using Bio.Core.Api.Constant;
using Bio.Portal.Server.Services.WebApi.Helpers;
using BioPortal.Server.Api;
using log4net;
using System.Reflection;
using System.Web.Http;

namespace Bio.Portal.Server.Services.WebApi.Controllers
{
    public class WSWEBController : BaseApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [Route("api/WSWEB/Enroll/{customTimeOut?}")]
        [HttpPost]
        public BioPortal.Server.Api.Json.XmlParamOut Enroll([FromBody] BioPortal.Server.Api.Json.XmlParamIn xmlParamIn,
            int customTimeOut = 0)
        {
            Log.Info("WSWEBController.Enroll");
            string sParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);

            if (string.IsNullOrEmpty(sParamIn))
                return new BioPortal.Server.Api.Json.XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
                };

            string sParamOut;
            int ret = BPWSWEBHelper.Enroll(sParamIn, out sParamOut, customTimeOut);
            Log.Info("Codigo respuesta BioPortal: " + ret + " - Mensaje: " + Errors.GetDescription(ret));

            BioPortal.Server.Api.Json.XmlParamOut response = XmlHelper.DeserializeXmlParamOut(sParamOut);
            if (ret != 0)
            {
                response.ExecutionResult = ret;
                response.Message += ", " + Errors.GetDescription(ret);
            }

            return response;
        }

        [Route("api/WSWEB/Identify/{customTimeOut?}")]
        [HttpPost]
        public BioPortal.Server.Api.Json.XmlParamOut Identify([FromBody] BioPortal.Server.Api.Json.XmlParamIn xmlParamIn,
            int customTimeOut = 0)
        {
            Log.Info("WSWEBController.Identify");
            string sParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);

            if (string.IsNullOrEmpty(sParamIn))
                return new BioPortal.Server.Api.Json.XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
                };

            string sParamOut;
            int ret = BPWSWEBHelper.Identify(sParamIn, out sParamOut, customTimeOut);
            Log.Info("Codigo respuesta BioPortal: " + ret + " - Mensaje: " + Errors.GetDescription(ret));

            BioPortal.Server.Api.Json.XmlParamOut response = XmlHelper.DeserializeXmlParamOut(sParamOut);
            if (ret != 0)
            {
                response.ExecutionResult = ret;
                response.Message += ", " + Errors.GetDescription(ret);
            }

            return response;
        }

        [Route("api/WSWEB/Verify/{customTimeOut?}")]
        [HttpPost]
        public BioPortal.Server.Api.Json.XmlParamOut Verify([FromBody] BioPortal.Server.Api.Json.XmlParamIn xmlParamIn,
            int customTimeOut = 0)
        {
            Log.Info("WSWEBController.Verify");
            string sParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);

            if (string.IsNullOrEmpty(sParamIn))
                return new BioPortal.Server.Api.Json.XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
                };

            string sParamOut;
            int ret = BPWSWEBHelper.Verify(sParamIn, out sParamOut, customTimeOut);
            Log.Info("Codigo respuesta BioPortal: " + ret + " - Mensaje: " + Errors.GetDescription(ret));

            BioPortal.Server.Api.Json.XmlParamOut response = XmlHelper.DeserializeXmlParamOut(sParamOut);
            if (ret != 0)
            {
                response.ExecutionResult = ret;
                response.Message += ", " + Errors.GetDescription(ret);
            }

            return response;
        }
    }
}
