﻿using Bio.Portal.Server.Services.WebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Bio.Portal.Server.Services.WebApi.Controllers
{
    /// <summary>
    /// Todos las las variables comunes para los controladores de la API deben ser declarados acá
    /// </summary>
    public class BaseApiController : ApiController
    {
        protected BPPPHelper BPPPHelper => new BPPPHelper(30000);
        protected BPWSHelper BPWSHelper => new BPWSHelper(30000);
        protected BPWSWEBHelper BPWSWEBHelper => new BPWSWEBHelper(30000);
        protected XmlHelper XmlHelper => new XmlHelper();
    }
}
