﻿using System.Web.Http;
using Bio.Portal.Server.Services.WebApi.Helpers;
using BioPortal.Server.Api;
using log4net;
using System.Reflection;
using System;
using Bio.Core.Api.Constant;

namespace Bio.Portal.Server.Services.WebApi.Controllers
{
    public class PluginProcessController : BaseApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [Route("api/PP/Process/{customTimeOut?}")]
        [HttpPost]
        public BioPortal.Server.Api.Json.XmlParamOut Process([FromBody] BioPortal.Server.Api.Json.XmlParamIn xmlParamIn, 
            int customTimeOut = 0)
        {
            Log.Info("PluginProcessController.Process IN...");
            string sParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);

            if (string.IsNullOrEmpty(sParamIn))
                return new BioPortal.Server.Api.Json.XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
                };

            string sParamOut;
            int ret = BPPPHelper.Process(sParamIn, out sParamOut, customTimeOut);
            Log.Info("Codigo respuesta BioPortal: " + ret + " - Mensaje: " + Errors.GetDescription(ret));

            BioPortal.Server.Api.Json.XmlParamOut response = XmlHelper.DeserializeXmlParamOut(sParamOut);
            if (ret != 0)
            {
                response.ExecutionResult = ret;
                response.Message += ", " + Errors.GetDescription(ret);
            }
            Log.Info("PluginProcessController.Process OUT!");
            return response;
        }
    }
}
