﻿using System.Web.Http;
using Bio.Portal.Server.Services.WebApi.Helpers;
using BioPortal.Server.Api;
using log4net;
using System.Reflection;
using System;
using Bio.Core.Api.Constant;

namespace Bio.Portal.Server.Services.WebApi.Controllers
{
    public class WSController : BaseApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [Route("api/WS/Enroll/{customTimeOut?}")]
        [HttpPost]
        public BioPortal.Server.Api.Json.XmlParamOut Enroll([FromBody] BioPortal.Server.Api.Json.XmlParamIn xmlParamIn, 
            int customTimeOut = 0)
        {
            Log.Info("WSController.Enroll");
            string sParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);

            if (string.IsNullOrEmpty(sParamIn))
                return new BioPortal.Server.Api.Json.XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
                };

            string sParamOut;
            int ret = BPWSHelper.Enroll(sParamIn, out sParamOut, customTimeOut);
            Log.Info("Codigo respuesta BioPortal: " + ret + " - Mensaje: " + Errors.GetDescription(ret));

            BioPortal.Server.Api.Json.XmlParamOut response = XmlHelper.DeserializeXmlParamOut(sParamOut);
            if (ret != 0)
            {
                response.ExecutionResult = ret;
                response.Message += ", " + Errors.GetDescription(ret);
            }

            return response;
        }

        [Route("api/WS/Identify/{customTimeOut?}")]
        [HttpPost]
        public BioPortal.Server.Api.Json.XmlParamOut Identify([FromBody] BioPortal.Server.Api.Json.XmlParamIn xmlParamIn,
            int customTimeOut = 0)
        {
            Log.Info("WSController.Identify");
            string sParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);

            if (string.IsNullOrEmpty(sParamIn))
                return new BioPortal.Server.Api.Json.XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
                };

            string sParamOut;
            int ret = BPWSHelper.Identify(sParamIn, out sParamOut, customTimeOut);
            Log.Info("Codigo respuesta BioPortal: " + ret + " - Mensaje: " + Errors.GetDescription(ret));

            BioPortal.Server.Api.Json.XmlParamOut response = XmlHelper.DeserializeXmlParamOut(sParamOut);
            if (ret != 0)
            {
                response.ExecutionResult = ret;
                response.Message += ", " + Errors.GetDescription(ret);
            }

            return response;
        }

        [Route("api/WS/Verify/{customTimeOut?}")]
        [HttpPost]
        public BioPortal.Server.Api.Json.XmlParamOut Verify([FromBody] BioPortal.Server.Api.Json.XmlParamIn xmlParamIn,
            int customTimeOut = 0)
        {
            Log.Info("WSController.Verify");
            string sParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);

            if (string.IsNullOrEmpty(sParamIn))
                return new BioPortal.Server.Api.Json.XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
                };

            string sParamOut;
            int ret = BPWSHelper.Verify(sParamIn, out sParamOut, customTimeOut);
            Log.Info("Codigo respuesta BioPortal: " + ret + " - Mensaje: " + Errors.GetDescription(ret));

            BioPortal.Server.Api.Json.XmlParamOut response = XmlHelper.DeserializeXmlParamOut(sParamOut);
            if (ret != 0)
            {
                response.ExecutionResult = ret;
                response.Message += ", " + Errors.GetDescription(ret);
            }

            return response;
        }

        [Route("api/WS/Get/{customTimeOut?}")]
        [HttpPost]
        public BioPortal.Server.Api.Json.XmlParamOut Get([FromBody] BioPortal.Server.Api.Json.XmlParamIn xmlParamIn,
            int customTimeOut = 0)
        {
            Log.Info("WSController.Get");
            string sParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);

            if (string.IsNullOrEmpty(sParamIn))
                return new BioPortal.Server.Api.Json.XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
                };

            string sParamOut;
            int ret = BPWSHelper.Get(sParamIn, out sParamOut, customTimeOut);
            Log.Info("Codigo respuesta BioPortal: " + ret + " - Mensaje: " + Errors.GetDescription(ret));

            BioPortal.Server.Api.Json.XmlParamOut response = XmlHelper.DeserializeXmlParamOut(sParamOut);
            if (ret != 0)
            {
                response.ExecutionResult = ret;
                response.Message += ", " + Errors.GetDescription(ret);
            }

            return response;
        }
    }
}
