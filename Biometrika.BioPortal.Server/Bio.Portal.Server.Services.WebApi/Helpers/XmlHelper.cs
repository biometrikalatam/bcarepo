﻿using Bio.Core.Api.Constant;
using BioPortal.Server.Api;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Bio.Portal.Server.Services.WebApi.Helpers
{
    public class XmlHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        internal string SerializeXmlParamIn(BioPortal.Server.Api.Json.XmlParamIn xmlParamIn)
        {
            Log.Debug("Seralizando xmlParamIn...");

            try
            {
                return XmlUtils.SerializeObject(xmlParamIn);
            }
            catch (Exception e)
            {
                Log.Error("Error serializando XmlParamIn", e);
                return null;
            }
        }

        internal BioPortal.Server.Api.Json.XmlParamOut DeserializeXmlParamOut(string xmlParamOut)
        {
            Log.Debug("Deseralizando xmlParamOut...");

            try
            {
                return XmlUtils.DeserializeObject<BioPortal.Server.Api.Json.XmlParamOut>(xmlParamOut);
            }
            catch (Exception ex)
            {
                Log.Error("Error deserializando XmlParamOut", ex);
                return new BioPortal.Server.Api.Json.XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_DESERIALIZE_XMLPARAMOUT)
                };
            }
        }
    }
}