﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bio.Portal.Server.Services.WebApi.Helpers
{
    public class BPWSHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public int TimeOut { get; internal set; }

        /// <summary>
        /// Constructor, recibe como parametro de entrada opcional el timeout global para los llamados a los web service
        /// </summary>
        /// <param name="timeOut">TimeOut global para los llamados a los web service, valor por defecto: 30000</param>
        public BPWSHelper(int timeOut = 30000)
        {
            TimeOut = timeOut;
        }

        /// <summary>
        /// Método de verificación contra BioPortalWS
        /// </summary>
        /// <param name="xmlparamin">Xml de entrada</param>
        /// <param name="xmlparamout">Xml de salida</param>
        /// <param name="customTimeOut">Opcional, timeOut propio para este método, se toma cualquier valor superior a 0</param>
        /// <returns></returns>
        internal int Verify(string xmlparamin, out string xmlparamout, int customTimeOut = 0)
        {
            Log.Info("BPWSHelper.Verify");
            int _ret_actual = 0;
            xmlparamout = null;
            try
            {
                Log.Debug("Llamando a BioPortalServerWS.Verify, timeOut: " 
                    + (customTimeOut > 0 ? customTimeOut : TimeOut));
                using (BioPortalServerWS.BioPortalServerWS target = new BioPortalServerWS.BioPortalServerWS())
                {
                    target.Timeout = customTimeOut > 0 ? customTimeOut : TimeOut;
                    target.Url = 
                        Properties.Settings.Default.Bio_Portal_Server_Services_WebApi_BioPortalServerWS_BioPortalServerWS;

                    _ret_actual = target.Verify(xmlparamin, out xmlparamout);
                }
                Log.Info("Respuesta BioPortalServerWS.Verify: " + _ret_actual);
            }
            catch (Exception e)
            {
                _ret_actual = -1;
                Log.Error("Error BPWSHelper.Verify", e);
            }
            return _ret_actual;
        }

        /// <summary>
        /// Método de Enrolamiento contra BioPortalWS
        /// </summary>
        /// <param name="xmlparamin">Xml de entrada</param>
        /// <param name="xmlparamout">Xml de salida</param>
        /// <param name="customTimeOut">Opcional, timeOut propio para este método, se toma cualquier valor superior a 0</param>
        /// <returns></returns>
        internal int Enroll(string xmlparamin, out string xmlparamout, int customTimeOut = 0)
        {
            Log.Info("BPWSHelper.Enroll");
            int _ret_actual = 0;
            xmlparamout = null;
            try
            {
                Log.Debug("Llamando a BioPortalServerWS.Enroll, timeOut: "
                    + (customTimeOut > 0 ? customTimeOut : TimeOut));
                using (BioPortalServerWS.BioPortalServerWS target = new BioPortalServerWS.BioPortalServerWS())
                {
                    target.Timeout = customTimeOut > 0 ? customTimeOut : TimeOut;
                    target.Url = Properties.Settings.Default.Bio_Portal_Server_Services_WebApi_BioPortalServerWS_BioPortalServerWS;
                    _ret_actual = target.Enroll(xmlparamin, out xmlparamout);
                }
                Log.Info("Respuesta BioPortalServerWS.Enroll: " + _ret_actual);
            }
            catch (Exception e)
            {
                _ret_actual = -1;
                Log.Error("Error BPWSHelper.Enroll", e);
            }
            return _ret_actual;
        }

        /// <summary>
        /// Método de identificación contra BioPortalWS
        /// </summary>
        /// <param name="xmlparamin">Xml de entrada</param>
        /// <param name="xmlparamout">Xml de salida</param>
        /// <param name="customTimeOut">Opcional, timeOut propio para este método, se toma cualquier valor superior a 0</param>
        /// <returns></returns>
        internal int Identify(string xmlparamin, out string xmlparamout, int customTimeOut = 0)
        {
            Log.Info("BPWSHelper.Identify");
            int _ret_actual = 0;
            xmlparamout = null;
            try
            {
                Log.Debug("Llamando a BioPortalServerWS.Identify, timeOut: "
                    + (customTimeOut > 0 ? customTimeOut : TimeOut));
                using (BioPortalServerWS.BioPortalServerWS target = new BioPortalServerWS.BioPortalServerWS())
                {
                    target.Timeout = customTimeOut > 0 ? customTimeOut : TimeOut;
                    target.Url = Properties.Settings.Default.Bio_Portal_Server_Services_WebApi_BioPortalServerWS_BioPortalServerWS;
                    _ret_actual = target.Identify(xmlparamin, out xmlparamout);
                }
                Log.Info("Respuesta BioPortalServerWS.Identify: " + _ret_actual);
            }
            catch (Exception e)
            {
                _ret_actual = -1;
                Log.Error("Error BPWSHelper.Identify", e);
            }
            return _ret_actual;
        }

        /// <summary>
        /// Método Get contra BioPortalWS
        /// </summary>
        /// <param name="xmlparamin">Xml de entrada</param>
        /// <param name="xmlparamout">Xml de salida</param>
        /// <param name="customTimeOut">Opcional, timeOut propio para este método, se toma cualquier valor superior a 0</param>
        /// <returns></returns>
        internal int Get(string xmlparamin, out string xmlparamout, int customTimeOut = 0)
        {
            Log.Info("BPWSHelper.Get");
            int _ret_actual = 0;
            xmlparamout = null;
            try
            {
                Log.Debug("Llamando a BioPortalServerWS.Get, timeOut: "
                    + (customTimeOut > 0 ? customTimeOut : TimeOut));
                using (BioPortalServerWS.BioPortalServerWS target = new BioPortalServerWS.BioPortalServerWS())
                {
                    target.Timeout = customTimeOut > 0 ? customTimeOut : TimeOut;
                    target.Url = Properties.Settings.Default.Bio_Portal_Server_Services_WebApi_BioPortalServerWS_BioPortalServerWS;
                    _ret_actual = target.Get(xmlparamin, out xmlparamout);
                }
                Log.Info("Respuesta BioPortalServerWS.Get: " + _ret_actual);
            }
            catch (Exception e)
            {
                _ret_actual = -1;
                Log.Error("Error BPWSHelper.Identify", e);
            }
            return _ret_actual;
        }
    }
}
