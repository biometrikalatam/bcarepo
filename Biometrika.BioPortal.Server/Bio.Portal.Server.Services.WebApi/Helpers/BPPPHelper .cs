﻿using log4net;
using System;
using System.Reflection;

namespace Bio.Portal.Server.Services.WebApi.Helpers
{
    public class BPPPHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public int TimeOut { get; internal set; }

        /// <summary>
        /// Constructor, recibe como parametro de entrada opcional el timeout global para los llamados a los web service
        /// </summary>
        /// <param name="timeOut">TimeOut global para los llamados a los web service, valor por defecto: 30000</param>
        public BPPPHelper(int timeOut = 30000)
        {
            TimeOut = timeOut;
        }

        /// <summary>
        /// Método de verificación contra BioPortalWS
        /// </summary>
        /// <param name="xmlparamin">Xml de entrada</param>
        /// <param name="xmlparamout">Xml de salida</param>
        /// <param name="customTimeOut">Opcional, timeOut propio para este método, se toma cualquier valor superior a 0</param>
        /// <returns></returns>
        internal int Process(string xmlparamin, out string xmlparamout, int customTimeOut = 0)
        {
            Log.Info("BPPPHelper.Process IN...");
            int _ret_actual = 0;
            xmlparamout = null;
            try
            {
                Log.Debug("Llamando a BioPortalServerPP.Process, timeOut: "
                    + (customTimeOut > 0 ? customTimeOut : TimeOut));
                using (BioPortalServerPP.BioPortal_Server_Plugin_Process target = new BioPortalServerPP.BioPortal_Server_Plugin_Process())
                {
                    target.Timeout = customTimeOut > 0 ? customTimeOut : TimeOut;
                    target.Url = 
                        Properties.Settings.Default.Bio_Portal_Server_Services_WebApi_BioPortalServerPP_BioPortal_Server_Plugin_Process;

                    _ret_actual = target.Process(xmlparamin, out xmlparamout);
                }
                Log.Info("Respuesta BioPortalServerPP.Process: " + _ret_actual);
            }
            catch (Exception e)
            {
                _ret_actual = -1;
                Log.Error("Error BPPPHelper.Process", e);
            }
            Log.Info("BPPPHelper.Process OUT!");
            return _ret_actual;
        }
    }
}
