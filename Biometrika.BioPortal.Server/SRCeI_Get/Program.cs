﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace BioPortal.Services.GUITest
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //try
            //{
            //    //string d1 = "28/02/2022";
            //    DateTime t1 = DateTime.ParseExact(d1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //    string do1 = t1.ToString("dd/MM/yyyy");

            //    string d2 = "02/28/2022";
            //    DateTime t2 = DateTime.ParseExact(d2, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            //    string do2 = t2.ToString("dd/MM/yyyy");

            //}
            //catch (Exception ex)
            //{

            //    string s = ex.Message;
            //}
            

            //Bio.Core.Api.DynamicData dd = new Bio.Core.Api.DynamicData();
            //dd.AddValue("kk", "vv");
            //string s = Bio.Core.Api.DynamicData.SerializeToXml(dd);

            //Bio.Core.Api.DynamicData dd1 = new Bio.Core.Api.DynamicData();
            //dd1 = Bio.Core.Api.DynamicData.DeserializeFromXml(s);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
