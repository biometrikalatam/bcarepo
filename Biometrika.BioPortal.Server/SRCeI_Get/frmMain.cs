﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Bio.Core.Api;
using BioPortal.Server.Api;

namespace BioPortal.Services.GUITest
{
    public partial class frmMain : Form
    {

        public frmMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            rtxResult.Text = "";
            picFoto.Image = BioPortal.Services.GUITest.Properties.Resources.User;
            picFirma.Image = BioPortal.Services.GUITest.Properties.Resources.firma;
            picQuestion.Visible = true;
            picOK.Visible = false;
            picNOOK.Visible = false;

            txtNombre.Text = "";
            txtProfesion.Text = "";
            txtSerie.Text = "";
            txtSexo.Text = "";
            txtVenc.Text = "";
            txtFecNac.Text = "";
            txtNacionalidad.Text = "";
            txtVisa.Text = "";
            labScore.Text = "Score ??";
            labScore.Visible = false;
           
        }

        private void button12_Click(object sender, EventArgs e)
        {
            button1_Click(null, null);
            labConsulting.Visible = true;
            //picFoto.Image = null;
            //picFirma.Image = null;
            this.Refresh();
            DoSinToken();   //HAce get de info
        }

        private void DoSinToken()
        {
            int expected = 0;
            int actual;
            string xmlparamout = string.Empty;

            try
            {
                using (BioPortalServerWS.BioPortalServerWS target = new BioPortalServerWS.BioPortalServerWS())
                {
                    target.Timeout = 60000;
                    target.Url = txtURL.Text + "BioPortal.Server.WS.asmx";
                    this.rtxResult.Text += Environment.NewLine + "Consultando a " + target.Url + "...";
                    this.rtxResult.Refresh();
                    XmlParamIn pin = new XmlParamIn();
                    pin.Actionid = 4; //Get
                    pin.Additionaldata = null;
                    pin.Authenticationfactor = 2; //Fingerprint
                    pin.Minutiaetype = 0; //No aplica porque va a 
                    pin.Clientid = "IdClienteTest";
                    pin.Companyid = Convert.ToInt32(txtCompany.Text); //No aplica para esto igual
                    pin.Enduser = "EnUserTest";
                    pin.Ipenduser = "127.0.0.1";
                    pin.Matchingtype = 1;
                    pin.Origin = 1;
                    pin.SampleCollection = null;
                    pin.SaveVerified = 1;
                    pin.Threshold = 0; //No aplica
                    pin.Userid = 0;
                    pin.Verifybyconnectorid = "SRCeI2015";
                    pin.InsertOption = 1;
                    pin.PersonalData = new PersonalData();
                    pin.PersonalData.Typeid = "RUT";
                    pin.PersonalData.Valueid = txtValueID.Text.Trim();
                    pin.OperationOrder = Convert.ToInt32(txtOrderOperation.Text);  //Solo Remote

                    string xmlparamin = XmlUtils.SerializeAnObject(pin);
                    actual = target.Get(xmlparamin, out xmlparamout);
                    ShowResultI(actual, xmlparamout);

                    if (!String.IsNullOrEmpty(rtxSample.Text))
                    {
                        pin.Actionid = 1; //Verify
                        pin.Authenticationfactor = 2; //Fingerprint
                        pin.Minutiaetype = 13; 
                        Sample sample = new Sample();
                        sample.Minutiaetype = 21;
                        sample.Data = rtxSample.Text;
                        pin.SampleCollection = new List<Sample>();
                        pin.SampleCollection.Add(sample);
                        pin.Bodypart = Convert.ToInt32(txtFinger.Text);
                        pin.Threshold = 3500; //No aplica
                        pin.Userid = 0;
                        pin.Verifybyconnectorid = "SRCeI2015";
                        pin.InsertOption = 1;
                        pin.PersonalData = new PersonalData();
                        pin.PersonalData.Typeid = "RUT";
                        pin.PersonalData.Valueid = txtValueID.Text.Trim();
                        pin.OperationOrder = Convert.ToInt32(txtOrderOperation.Text); //Solo Remote

                        xmlparamin = XmlUtils.SerializeAnObject(pin);
                        actual = target.Verify(xmlparamin, out xmlparamout);

                        ShowResultV(actual, xmlparamout);
                    }
                }

                
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = ex.Message;
            }
        }

        private void ShowResultV(int actual, string xmlparamout)
        {
            XmlParamOut paramout = null;
            try
            {
                this.rtxResult.Text += Environment.NewLine + "Codigo Retorno Verify =" + actual.ToString() + " [" + Bio.Core.Api.Constant.Errors.GetDescription(actual) + "]";
                if (xmlparamout != null)
                {
                    paramout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                }
                if (actual == 0)
                {
                    this.rtxResult.Text += Environment.NewLine + "Track Id = " + paramout.Trackid;
                    if (paramout.Result == 1)
                    {
                        picOK.Visible = true;
                        picNOOK.Visible = false;
                        picQuestion.Visible = false;
                    }
                    else
                    {
                        picOK.Visible = false;
                        picNOOK.Visible = true;
                        picQuestion.Visible = false;
                    }
                    labScore.Text = "Score => " + paramout.Score.ToString() + " / " + paramout.Threshold.ToString();
                    labScore.Visible = true;

                    if (paramout.Resultconnector != null)
                    {
                        this.rtxResult.Text += Environment.NewLine + "Retorno SRCeI => ";
                        foreach (var item in paramout.Resultconnector.DynamicDataItems)
                        {
                            this.rtxResult.Text += Environment.NewLine + item.key + "=" + item.value;
                        }
                    }
                }
                else
                {
                    labScore.Text = "";
                    labScore.Visible = false;
                    picQuestion.Visible = true;
                    picOK.Visible = false;
                    picNOOK.Visible = false;
                }
                if (paramout != null)
                {
                    if (paramout.PersonalData != null)
                    {

                        this.rtxResult.Text += Environment.NewLine + "TypeId/ValueId = " + paramout.PersonalData.Typeid + "/" +
                            paramout.PersonalData.Valueid;
                        //this.rtxResult.Text += Environment.NewLine + "Nombre = " + paramout.PersonalData.Name + " " +
                        //    paramout.PersonalData.Patherlastname + " " + paramout.PersonalData.Motherlastname;

                        //if (paramout.PersonalData.Photography != null)
                        //{
                        //    Image imagen = null;
                        //    using (System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(paramout.PersonalData.Photography)))
                        //    {
                        //        imagen = new Bitmap(ms);
                        //    }
                        //    picFoto.Image = imagen;
                        //}
                        //else
                        //{
                        //    picFoto.Image = null;
                        //    //this.rtxResult.Text += Environment.NewLine + "Foto = No existe foto para esta identidad!";
                        //}

                        //if (paramout.PersonalData.Signatureimage != null)
                        //{
                        //    Image imagen = null;
                        //    using (System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(paramout.PersonalData.Signatureimage)))
                        //    {
                        //        imagen = new Bitmap(ms);
                        //    }
                        //    picFirma.Image = imagen;
                        //}
                        //else
                        //{
                        //    picFoto.Image = null;
                        //    //this.rtxResult.Text += Environment.NewLine + "Foto = No existe firma para esta identidad!";
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = "Exception = " + ex.StackTrace; 
            }
            labConsulting.Visible = false;
            labConsulting.Refresh();
        }

        private void ShowResultI(int actual, string xmlparamout)
        {
            XmlParamOut paramout = null;
            try
            {
                this.rtxResult.Text += Environment.NewLine + "Codigo Retorno Info=" + actual.ToString() + " [" + Bio.Core.Api.Constant.Errors.GetDescription(actual) + "]";
                if (xmlparamout != null)
                {
                    paramout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                }
                if (actual == 0)
                {
                    this.rtxResult.Text += Environment.NewLine + "Track Id = " + paramout.Trackid;
                }
                if (paramout != null)
                {
                    if (paramout.PersonalData != null)
                    {

                        this.txtNombre.Text = paramout.PersonalData.Name + " " + paramout.PersonalData.Patherlastname + " " + paramout.PersonalData.Motherlastname;
                        this.txtNacionalidad.Text = paramout.PersonalData.Nationality;
                        this.txtProfesion.Text = paramout.PersonalData.Profession;
                        this.txtSerie.Text = paramout.PersonalData.Documentseriesnumber;
                        this.txtFecNac.Text = paramout.PersonalData.Birthdate.ToString("dd/MM/yyyy");
                        this.txtVenc.Text = paramout.PersonalData.Documentexpirationdate.ToString("dd/MM/yyyy");
                        this.txtSexo.Text = paramout.PersonalData.Sex;
                        this.txtVisa.Text = paramout.PersonalData.Visatype;

                        if (paramout.PersonalData.Photography != null)
                        {
                            byte[] f = Convert.FromBase64String(paramout.PersonalData.Photography);
                            //System.IO.File.WriteAllBytes("C:\\tmp\\bp\\foto.jpg", f); 

                            Bitmap bmp;
                            Image imagen = null;
                            using (System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(paramout.PersonalData.Photography)))
                            {
                                imagen = new Bitmap(ms);
                                bmp = new Bitmap(ms);
                            }
                            picFoto.Image = imagen;
                            //bmp.Save("C:\\tmp\\A\\bp\\foto.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                            //picFoto.Image.Save("C:\\tmp\\A\\bp\\fotoGGS.jpg"); 
                        }
                        else
                        {
                            picFoto.Image = null;
                            this.rtxResult.Text += Environment.NewLine + "Foto = No existe foto para esta identidad!";
                        }

                        if (paramout.PersonalData.Signatureimage != null)
                        {
                         
                            Image imagen = null;
                            using (System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(paramout.PersonalData.Signatureimage)))
                            {
                                imagen = new Bitmap(ms);
                                
                            }
                            
                            picFirma.Image = imagen;
                        }
                        else
                        {
                            picFoto.Image = null;
                            this.rtxResult.Text += Environment.NewLine + "Foto = No existe firma para esta identidad!";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = "Exception = " + ex.StackTrace;
            }
            labConsulting.Visible = false;
            labConsulting.Refresh();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            //this.rtxSample.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Seleccione sample en Base 64...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
                   // this.rtxSample.Text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                this.rtxResult.Text = "Exception = " + ex.StackTrace; 
            }
        }

        private void txtAction_Leave(object sender, EventArgs e)
        {
           // labAction.Text = Bio.Portal.Server.Api.Constant.Action.GetName(Convert.ToInt32(txtAction.Text.Trim()));
        }

        private void txtOO_Leave(object sender, EventArgs e)
        {
           // labOO.Text = Bio.Portal.Server.Api.Constant.OperationOrder.GetName(Convert.ToInt32(txtOO.Text.Trim()));
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
           
        }

        private void txtAF_Leave(object sender, EventArgs e)
        {
            // labAF.Text = Bio.Portal.Server.Api.Constant.AuthenticationFactor.GetName(Convert.ToInt32(txtAF.Text.Trim()));
        }

        private void txtMT_Leave(object sender, EventArgs e)
        {
            // labMT.Text = Bio.Portal.Server.Api.Constant.MinutiaeType.GetDescription(Convert.ToInt32(txtMT.Text.Trim()));
        }

        private void txtBP_Leave(object sender, EventArgs e)
        {
            // labBP.Text = Bio.Portal.Server.Api.Constant.BodyPart.GetName(Convert.ToInt32(txtBP.Text.Trim()));

        }

        private void btnSetAx_Click(object sender, EventArgs e)
        {

            //this.ubpClientAx2.AuthenticationFactor = 2; //huella Digital
            //this.ubpClientAx2.Device = 1; //Digital Persona
            //this.ubpClientAx2.Model = "4500"; //Digital Persona
            //this.ubpClientAx2.Tokencontent = 1; //Solo WSQ
            //this.ubpClientAx2.Operationtype = 1; //Verify
            //this.ubpClientAx2.Minutiaetype = 7; //No se usa
            ////this.ubpClientAx2.Typeid = "RUT";
            ////this.ubpClient1.Valueid = "13434022-3";
            ////this.ubpClient1.SetLabel = this.txtID.Text.Trim();
            //this.ubpClientAx2.SetTypeIdValueId(this.txtID.Text.Trim(), this.txtValueID.Text.Trim());
                //someLabel.Text = newText; // runs on UI thread
            
          
           
            //this.ubpClient1.Typeid = this.txtID.Text.Trim();
            //this.ubpClient1.Valueid = this.txtValueID.Text.Trim();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //if (grpFacial.Visible)
            //{

            //    //this.rtxSample.Text = this.ubpClientF1.GetImagenB64();
            //}
            //else
            //{
            //    //this.rtxSample.Text = this.ubpClient1.GetToken();
            //}
            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
        }

        private void btnOcultar_Click_1(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void txtMTSample_Leave(object sender, EventArgs e)
        {
            //labMTSample.Text = Bio.Portal.Server.Api.Constant.MinutiaeType.GetDescription(Convert.ToInt32(txtMTSample.Text.Trim()));
        }

        private void button5_Click(object sender, EventArgs e)
        {
        }


        private void button2_Click_1(object sender, EventArgs e)
        {
         
        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Seleccione sample en Base 64...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
                    this.rtxSample.Text = sr.ReadToEnd();
                    sr.Close();

                    Bio.Core.Wsq.Decoder.WsqDecoder wsqdec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                    short w, h;
                    byte[] raw;
                    if (wsqdec.DecodeMemory(Convert.FromBase64String(rtxSample.Text), out w, out h, out raw))
                    {
                        picHuella.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw, w, h);
                    }
                    else
                    {
                        picHuella.Image = null;
                    }
                }
                

            }
            catch (Exception ex)
            {
                this.rtxResult.Text = "Exception = " + ex.StackTrace;
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            picHuella.Image = null;
            rtxSample.Text = "";
        }

        private void button2_Click_3(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Seleccione sample en Base 64...";
                if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.Cancel)
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
                    this.rtxSample.Text = sr.ReadToEnd();
                    sr.Close();

                    Bio.Core.Wsq.Decoder.WsqDecoder wsqdec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                    short w, h;
                    byte[] raw;
                    if (wsqdec.DecodeMemory(Convert.FromBase64String(rtxSample.Text), out w, out h, out raw))
                    {
                        picHuella.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw, w, h);
                    }
                    else
                    {
                        picHuella.Image = null;
                    }
                }


            }
            catch (Exception ex)
            {
                this.rtxResult.Text = "Exception = " + ex.StackTrace;
            }
        }

        private void button3_Click_2(object sender, EventArgs e)
        {
            picHuella.Image = null;
            rtxSample.Text = "";
        }
    }
}
