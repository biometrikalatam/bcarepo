﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BPEnrollDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.textBox7.Text = "Enrolando...";
            string _trackidbp;
            string _tipoE = this.radioButton1.Checked ? "Usuario" : "Cliente";
            int i = BPHelper.EnrollBioPortalSinBIR(this.textBox1.Text.Trim(),
                this.textBox2.Text, this.textBox2.Text, this.textBox2.Text, this.textBox2.Text, this.textBox2.Text,
                                                   "IdClienteTest", 
                                                   "EndUser", 
                                                   "127.0.0.1", 
                                                   "HA20987498", this.textBox2.Text,
                                                   1, 
                                                   null,
                                                   1, 
                                                   out _trackidbp);
            this.textBox7.Text = _trackidbp;
        }
    }
}
