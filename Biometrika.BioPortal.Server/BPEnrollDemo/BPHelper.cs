﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bio.Core.Api;
using BioPortal.Server.Api;

namespace BPEnrollDemo
{
    internal static class BPHelper
    {
       
        internal static int EnrollBioPortalSinBIR(string _url, string _typeid, string _valueid, 
                                                  string _nombre, string _apellido, string _2apellido, string _clienteId, 
                                                  string _endUser, string _ipUser, string _serialIdSensor, string _tipoEnroll,
                                                  short _fingerId, string _token, short _typeDevice, out string _trackid)
        {
            int _ret_actual = 0;
            string xmlparamout = string.Empty;
            _trackid = null;

            try
            {
                using (BioPortalServerWSWeb.BioPortalServerWSWeb target = new BioPortalServerWSWeb.BioPortalServerWSWeb())
                {
                    target.Timeout = 30000;  //Timeout de la operacion, pueden modificarlo a gusto. Valor en Milisegundos
                    target.Url = _url;       //URL del web service del BioPortal para enrolar
                    XmlParamIn pin = new XmlParamIn();
                    pin.Actionid = Bio.Portal.Server.Api.Constant.Action.ACTION_ENROLL;  //Enroll
                    
                    pin.Additionaldata = null;
                    pin.Authenticationfactor = 2;   //Fingerprint (No se usa pero iggual se llena)
                    pin.Minutiaetype = 24;          //Token (No se usa pero iggual se llena) 
                    pin.Bodypart = 1;               //pulgar derecho (No se usa pero iggual se llena)
                    pin.Clientid = _clienteId;      //Puedes sacarlo del Id de estacion de trabajo, algun identificador del cliente o mismo Companyid
                    pin.Companyid = 7;              //ID de la compañ+ía definida en BioPortal. 
                    pin.Enduser = _endUser;         //identificador de operador, quiza el ID Sensor. es solo informativo
                    pin.Ipenduser = _ipUser;        //ip estación de trabajo (opcional)
                    pin.Origin = 1;                //Este valor deben definirlo. Es el Origen de la operación, por lo que se debe definir 
                                                   //ese valor fijo en el BioPortal. Por ahora configuralo en 1 = Desconocido (Pero configurable)   
                                                   //Desde el Manager del BioPortal se pueden crear mas origenes, y eso permite luego ver
                                                   //indicadores de que aplicaciones usan mas el BioPortal

                    pin.SampleCollection = null; //Este valor va en null, porque queremos enrolar SIN huellas. Sino se debe descomentar lo de abajo y 
                                                 //rellenarlo con datos biométricos. Token tomado con la ocmpoentne entregada.   
                    //new List<Sample>();
                    //Sample sample = new Sample();
                    //sample.Data = _token;
                    //sample.Minutiaetype = 24; //Token
                    //sample.Additionaldata = null;
                    //pin.SampleCollection.Add(sample);

                    pin.SaveVerified = 0;   //Este valor puede ser 0 - No graba | 1 - Graba => Es para cuando se envian datos biometricos para verificar
                                            //para guardarlos como audiotoria mas allá del proceso que se haga.
                    pin.Verifybyconnectorid = "FINAL"; //Fijo porque no se usa ningun conector externo
                    pin.InsertOption = 2;  //No - Es para agregar cunado se verifica en fuente externa. En Enroll no se usa.

                           //Agrego datos de la persona a enrolar. Verioficar si desean agregar mas datos que estos, en el manual se describen.
                    pin.PersonalData = new PersonalData();
                    pin.PersonalData.Typeid = _typeid;
                    pin.PersonalData.Valueid = _valueid;
                    pin.PersonalData.Name = _nombre;
                    pin.PersonalData.Patherlastname = _apellido;
                    pin.PersonalData.Motherlastname = _2apellido;
                          //Agregamos conmo additional data para identificar si esta persona es Usuario o Cliente 
                    pin.PersonalData.Dynamicdata = new DynamicDataItem[1];
                    DynamicDataItem item = new DynamicDataItem();
                    item.key = "TipoIdentity";
                    item.value = _tipoEnroll;
                    pin.PersonalData.Dynamicdata[0] = item;

                    pin.OperationOrder = 1;  //Solo Local (No trabajan con fuentes de datos externas, de todas formas esto es para Verificacion nopara enroll)

                    string xmlparamin = XmlUtils.SerializeAnObject(pin);

                    _ret_actual = target.Enroll(xmlparamin, out xmlparamout);
                }

                //Si el retorno = 0 => Está ok la ejecución, y hay que ver que resultado de verificación dio
                if (_ret_actual == 0)
                { //Ejecuto ok, deserializopara usar la info
                    XmlParamOut paramout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                    //_matchok = (paramout.Result == 1);//  "Positiva" = 1 : "Negativa" = 2);
                    //_score = paramout.Score;
                    _trackid = paramout.Trackid;
                }
                else
                {
                    _trackid = "Error = " + _ret_actual + " - " + Bio.Core.Api.Constant.Errors.GetDescription(_ret_actual);
                }
            }
            catch (Exception ex)
            {
                _ret_actual = -1;
                //LOG - Agregar para tner log de errores
            }
            return _ret_actual;
        }

    }
}
