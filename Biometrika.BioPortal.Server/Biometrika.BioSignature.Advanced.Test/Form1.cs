﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Bio.Core.pki.mentalis;

namespace Biometrika.BioSignature.Advanced.Test
{
    public partial class Form1 : Form
    {
        BioSignatureFactory factory = new BioSignatureFactory();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ddata = null;
            string biosignature;
            string err;

            string strDynamicData = "<DynamicDataItem>" +
                                        "<key>TypeId</key>" +
                                        "<value>RUT</value>" +
                                    "</DynamicDataItem>"; // +
                                    //"<DynamicDataItem>" +
                                    //    "<key>ValueId</key>" +
                                    //    "<value>" + [rut de la tabla transaction] + "</value>" +
                                    //"</DynamicDataItem>" +
                                    //"<DynamicDataItem>" +
                                    //    "<key>Name</key>" +
                                    //    "<value>" + [name de la tabla transaction] + "</value>" +
                                    //"</DynamicDataItem>" +
                                    //"<DynamicDataItem>" +
                                    //    "<key>Patherlastname</key>" +
                                    //    "<value>" + [patherlastname de la tabla transaction] + "</value>" +
                                    //"</DynamicDataItem>" +
                                    //"<DynamicDataItem>" +
                                    //    "<key>Motherlastname</key>" +
                                    //    "<value>" + [motherlastname de la tabla transaction] + "</value>" +
                                    //"</DynamicDataItem>" +
                                    //"<DynamicDataItem>" +
                                    //    "<key>URLPoder</key>" +
                                    //    "<value>" + [urldocument de la tabla transaction] + "</value>" +
                                    //"</DynamicDataItem>";
            int ierr = factory.GetSignature(
                            BioSignatureFactory.SIGNATURE_TYPE_ADVANCED, 
                            "id1129182",                                    // => codautoriza de la tabla transaction (Es unico cierto? Sino el Id)
                            "/6DakjaksabKJHDKJGKJDGKadadasdasdsG",          // => bir de la tabla transaction
                            21,                                             // => Si el bir es siempre WSQ => valor 21 / Si es RAW = 21 sino el MinutiaeType que sea correcto
                            "SRCeI",                                        // => Valor fijo Cedula Nueva o Cedula Antigua segun corresponda. Si no tenemos esa diferencia, lo podmeos ver con el scrore, en la antigua hay score > 0 y en la nueva no.
                            null,                                           // => Aca no hay foto asi que va en null
                            "13200",                                        // => score de la tabla transaction. Si es cedula nueva, será 0, no importa.                
                            strDynamicData,                                 // => el string que arme antes, con esos datos de la tabla
                            out biosignature, 
                            out err);

            if (ierr == 0)
            {
                richTextBox1.Text = biosignature;
                XMLBrowser.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + biosignature;
            }
            else richTextBox1.Text = err;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool isok = false;
            string err;
            BioSignatureFactory factory = new BioSignatureFactory();
            int ierr = factory.VerifySignature(richTextBox1.Text.Trim(), out isok, out err);

            if (ierr == 0) richTextBox1.Text = "ierr=" + ierr + Environment.NewLine +
                                                "isok=" + isok.ToString() + Environment.NewLine;
            else richTextBox1.Text = err;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Bio.Core.Api.DynamicData ddata = null;
            string biosignature;
            string err;
            int ierr = factory.GetSignature(BioSignatureFactory.SIGNATURE_TYPE_SIMPLE, "id1129182", "/6DakjaksabKJHDKJGKJDGKG",21,
                "SRCeI", null, "13200", "", out biosignature, out err);

            if (ierr == 0)
            {
                richTextBox1.Text = biosignature;
                XMLBrowser.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + biosignature;
            }
            else richTextBox1.Text = err;
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            AsymmetricAlgorithm key = Pkcs12.GetPrivateKeyFromPFX(richTextBox1.Text, "biometrika");
            if (key != null) richTextBox1.Text = "Cargado OK!";
            else richTextBox1.Text = "NO CARGADO !!!!!";

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            factory.InitFactory("GustavoSuhitExportado.pfx", "biometrika", "GustavoSuhit.cer", "QR", "http://bci.biemetrika.cl/WebAudit...");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string xmlenvelope;
            int retget = NVHelper.Notarize("1hj2g23f2fj13j23fj45gf2fj4", "RUT", "21284415-2", richTextBox1.Text, out xmlenvelope);
            if (retget == 0)
            {
                richTextBox1.Text = xmlenvelope;
                XMLBrowser.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" +  xmlenvelope;
            } else  {
                richTextBox1.Text = "Error = " + retget.ToString();
            }

            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string retorno;
            int retget = NVHelper.NVVerify(richTextBox1.Text, 1, out retorno);
            if (retget == 0)
            {
                richTextBox1.Text = retorno;
                XMLBrowser.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + retorno;
            }
            else
            {
                richTextBox1.Text = "Error = " + retget.ToString();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
             try
            {
                XmlDocument rdEnvelope = new XmlDocument();
                rdEnvelope.PreserveWhitespace = false;
                rdEnvelope.LoadXml(richTextBox1.Text);

                XmlNode node = rdEnvelope.GetElementsByTagName("Rd")[0];

                richTextBox1.Text = node.OuterXml;
            }
            catch (Exception ex)
            {
                richTextBox1.Text = "Error ExtractReceiptFromEnvelope" + ex.Message;
            }
        }
    }
}
