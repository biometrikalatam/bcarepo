﻿using Bio.Core.Api.Constant;
//using Bio.Portal.Server.Services.NVWS;
//using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Biometrika.BioSignature.Advanced.Test.NVWS;

namespace Biometrika.BioSignature.Advanced.Test
{
    /// <summary>
    /// 
    /// </summary>
    public class NVHelper
    {
        //private static readonly ILog LOG = LogManager.GetLogger(typeof(NVHelper));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idtx"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="xmlabs"></param>
        /// <param name="xmlRDenvelope"></param>
        /// <returns></returns>
        internal static int Notarize(string idtx, string typeid, string valueid, string xmlabs, out string xmlRDenvelope)
        {
            int ret = Errors.IERR_OK;
            xmlRDenvelope = null;

            try
            {
                ArrayOfRdExtensionItemRdExtensionItem[] arrExt = new ArrayOfRdExtensionItemRdExtensionItem[5];
                ArrayOfRdExtensionItemRdExtensionItem item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "TxID";
                item.value = idtx;
                arrExt[0] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "TypeID";
                item.value = typeid;
                arrExt[1] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "ValueID";
                item.value = valueid;
                arrExt[2] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "Date";
                item.value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                arrExt[3] = item;
                item = new ArrayOfRdExtensionItemRdExtensionItem();
                item.key = "ABS";
                item.value = xmlabs;                                                                                            // "<BioSignature id=\"IDREF.v1\"><TrackID>id1129182</TrackID><Timestamp>31-01-2015 19:10:13.297</Timestamp><Source>SRCeI</Source><Score>13200</Score><ImageSample>/6DakjaksabKJHDKJGKJDGKG</ImageSample><Barcode></Barcode><Photografy></Photografy><DynamicData></DynamicData><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /><SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\" /><Reference URI=\"#IDREF.v1\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\" /></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\" /><DigestValue>FTOHSerLZEz16qcTgBQ3SNtXnno=</DigestValue></Reference></SignedInfo><SignatureValue>d4u+YKSuXzCqIqKPtrVPYh0FF+NS4t8+eJ07u3ErRJaCocQrVUKHoU7Mm9WjaYYxoqlCJ+9IqJtp1P8rfyCf0ZJpCpDqb7DpK6lnaLtG+z8ok7o5CSbVCleoNDkfcCfXqPOndp8PwnSjsT0cgFTk/DalsG4bsth6ZrmXv8U2f8E=</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>t6VZ7kkNFK7lTzHx9kTkexNhnC7MDVc+phMNG28Ye7Geet4cLhhZ6uqJPaBtgv6wFwbXfV4XlS95lOXjEgUSm+izc4qpgrhYC6OfZJG1bpok4UP7wLH80gjWn9b7mXWsMEUQiwMVQlhdFMpgE3JeIZRyFEpBs4OBOaGMmmG8QIM=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue><X509Data><X509Certificate>MIIGBTCCBO2gAwIBAgIKK4HM0wAAAADIeDANBgkqhkiG9w0BAQUFADCB0jELMAkGA1UEBhMCQ0wxHTAbBgNVBAgTFFJlZ2lvbiBNZXRyb3BvbGl0YW5hMREwDwYDVQQHEwhTYW50aWFnbzEUMBIGA1UEChMLRS1DRVJUQ0hJTEUxIDAeBgNVBAsTF0F1dG9yaWRhZCBDZXJ0aWZpY2Fkb3JhMTAwLgYDVQQDEydFLUNFUlRDSElMRSBDQSBGSVJNQSBFTEVDVFJPTklDQSBTSU1QTEUxJzAlBgkqhkiG9w0BCQEWGHNjbGllbnRlc0BlLWNlcnRjaGlsZS5jbDAeFw0xMzA3MjkyMTA1MTBaFw0xNjA3MjgyMTA1MTBaMIHEMQswCQYDVQQGEwJDTDEjMCEGA1UECBMaTUVUUk9QT0xJVEFOQSBERSBTQU5USUFHTyAxETAPBgNVBAcTCFNhbnRpYWdvMSkwJwYDVQQKEyBCQ1IgVEVDTk9MT0dJQSBFIElOTk9WQUNJT04gUy5BLjEKMAgGA1UECwwBKjEhMB8GA1UEAxMYR3VzdGF2byBHZXJhcmRvICBTdWhpdCAuMSMwIQYJKoZIhvcNAQkBFhRnc3VoaXRAYmlvbWV0cmlrYS5jbDCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAt6VZ7kkNFK7lTzHx9kTkexNhnC7MDVc+phMNG28Ye7Geet4cLhhZ6uqJPaBtgv6wFwbXfV4XlS95lOXjEgUSm+izc4qpgrhYC6OfZJG1bpok4UP7wLH80gjWn9b7mXWsMEUQiwMVQlhdFMpgE3JeIZRyFEpBs4OBOaGMmmG8QIMCAwEAAaOCAmswggJnMD0GCSsGAQQBgjcVBwQwMC4GJisGAQQBgjcVCILcgy+Fk4xmhdWdF4Li3CeB7+w8YYHLnhSGqIJYAgFkAgEDMB0GA1UdDgQWBBSA5w018UhLsrsObq8J0cfPWzVhWDALBgNVHQ8EBAMCBPAwHwYDVR0jBBgwFoAUeOE+n9ISs3o8jc0wDlOzQykHs1UwPgYDVR0fBDcwNTAzoDGgL4YtaHR0cDovL2NybC5lLWNlcnRjaGlsZS5jbC9lY2VydGNoaWxlY2FGRVMuY3JsMCMGA1UdEQQcMBqgGAYIKwYBBAHBAQGgDBYKMjEyODQ0MTUtMjAjBgNVHRIEHDAaoBgGCCsGAQQBwQECoAwWCjk2OTI4MTgwLTUwggFNBgNVHSAEggFEMIIBQDCCATwGCCsGAQQBw1IFMIIBLjAtBggrBgEFBQcCARYhaHR0cDovL3d3dy5lLWNlcnRjaGlsZS5jbC9DUFMuaHRtMIH8BggrBgEFBQcCAjCB7x6B7ABDAGUAcgB0AGkAZgBpAGMAYQBkAG8AIABGAGkAcgBtAGEAIABTAGkAbQBwAGwAZQAuACAASABhACAAcwBpAGQAbwAgAHYAYQBsAGkAZABhAGQAbwAgAGUAbgAgAGYAbwByAG0AYQAgAHAAcgBlAHMAZQBuAGMAaQBhAGwALAAgAHEAdQBlAGQAYQBuAGQAbwAgAGgAYQBiAGkAbABpAHQAYQBkAG8AIABlAGwAIABDAGUAcgB0AGkAZgBpAGMAYQBkAG8AIABwAGEAcgBhACAAdQBzAG8AIAB0AHIAaQBiAHUAdABhAHIAaQBvMA0GCSqGSIb3DQEBBQUAA4IBAQBF2HPkNASvnFBt+Acko6B411BPvsEeJPyvg50HNkI5e18pRLV5efeN50mTGh9CFElRvbjNlIDH8VscM/jZ36G/i/GIsa/urIEZP42J+v/0iTlLmI7PJqKDR1f5HQWBY4FviK6jbNf8Jm3GV8l8O74gBTyyDqxQXJRkwGcbsU8LUZMVfy+XR+AL6lZVJ9SzSglIyUNuL/h6jgTB1Z+3kpfUz7t5FtkI2VYkhugaZR7tfwD8K7xjnRnDnLQQLr5kAsj743+B/ljMhakplCnUy7vIx0MnNbE04Y9WDjvSuKN7C2JllAtSBKUNSEFHFGvmq/YcRad/yCI5Fl9HXTK688GT</X509Certificate></X509Data></KeyInfo></Signature></BioSignature>";
                arrExt[4] = item;

                using (NVWS.WSBDR NV = new NVWS.WSBDR())
                {

                    NV.Timeout = 30000;

                                                         //4 - Advanced Signature para BCI
                    RDRetorno rdRet = NV.GenerateReceipt(4, 
                                                         "BCI", 
                                                         "Biometric Advanced Signature Notarize",
                                                         arrExt);

                    if (rdRet.codigo == 0)
                    {
                        xmlRDenvelope = rdRet.xmlret;
                    }
                    ret = rdRet.codigo;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                //LOG.Error("NVHelper.Notarize Error - " + ex.Message);
            }

            return ret;
        }


       
        internal static int NVVerify(string rd, int type, out string retorno, out string msg)
        {
            int ret = Errors.IERR_OK;
            retorno = null;
            msg = null;
            try
            {

                using (NVWS.WSBDR NV = new NVWS.WSBDR())
                {

                    NV.Timeout = 30000;

                    //4 - Advanced Signature para BCI
                    RDRetorno rdRet = NV.VerifyReceipt(rd, type);

                    if (rdRet.codigo == 0)
                    {
                        retorno = rdRet.xmlret;
                    }
                    else
                    {
                        msg = rdRet.codigo + " - " + rdRet.descripcion;
                    }
                    ret = rdRet.codigo;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                //LOG.Error("NVHelper.Notarize Error - " + ex.Message);
            }

            return ret;
        }


    }
}