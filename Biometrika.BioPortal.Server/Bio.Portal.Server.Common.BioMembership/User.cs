﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Web.Security;
using Bio.Portal.Server.Common.BioMembership;
using Bio.Portal.Server.Common.Entities.Database;


namespace Bio.Portal.Server.Common.BioMembership
{
	/// <summary>User Entity.</summary>
	[Serializable]
	public class User : BaseEntity<User, int>
	{
		#region Properties

		[DataObjectField(true)]
		public override int Id { get; set; }
		public virtual string UserName { get; set; }
		public virtual string Email { get; set; }
		public virtual string Password { get; set; }
		public virtual string PasswordSalt { get; set; }
		public virtual MembershipPasswordFormat PasswordFormat { get; set; }
		public virtual string PasswordQuestion { get; set; }
		public virtual string PasswordAnswer { get; set; }
		public virtual int FailedPasswordAttemptCount { get; set; }
		public virtual DateTime? FailedPasswordAttemptWindowStart { get; set; }
		public virtual int FailedPasswordAnswerAttemptCount { get; set; }
		public virtual DateTime? FailedPasswordAnswerAttemptWindowStart { get; set; }
		public virtual DateTime? LastPasswordChangedDate { get; set; }
		public virtual DateTime CreationDate { get; set; }
		public virtual DateTime LastActivityDate { get; set; }
		public virtual bool IsApproved { get; set; }
		public virtual bool IsLockedOut { get; set; }
		public virtual DateTime? LastLockOutDate { get; set; }
		public virtual DateTime? LastLoginDate { get; set; }
		public virtual string Comments { get; set; }

		public virtual string Name { get; set; }
		public virtual string Surname { get; set; }
		public virtual string Company { get; set; }
		public virtual string Phone { get; set; }
		public virtual string PostalCode { get; set; }
        public virtual BpCompany CompanyId { get; set; }

		#endregion
		
		#region Methods
		
		#region Constructors

		/// <summary>Initializes a new instance of the User class.</summary>
		public User() 
		{
		}

		#endregion
		
		#region ToString

		/// <summary>Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.</summary>
		/// <returns>A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.</returns>
		public override string ToString()
		{
			return String.Format(CultureInfo.CurrentCulture, "[{0}] {1}", Id, Name ?? "null");
		}

		#endregion

		#endregion
	}

}
